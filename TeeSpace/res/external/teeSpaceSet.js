"use strict";

checkBrowser();
(function (exports) {
  var tmp_htmlToElement = function tmp_htmlToElement(html) {
    var template = document.createElement('template');
    html = html.trim();
    template.innerHTML = html;
    return template.content.firstChild;
  };

  var overlayTemplate = tmp_htmlToElement('<div class="top-dialog-overlay" style="z-index:65000" />');
  var template = tmp_htmlToElement('<div class="tee-custom-dialog" style="z-index:65001">' + '<div class="tee-custom-content">' + '</div>' + '<div class="button-container">' + '<button class="solid">확인</button>' + '</div>' + '</div>');
  template.querySelector('.tee-custom-content').style.margin = '0 0.75rem 1.13rem 0.75rem';
  template.querySelector('.tee-custom-content').style.fontSize = '0.81rem';
  template.querySelector('.tee-custom-content').style.textAlign = 'center';
  template.querySelector('.tee-custom-content').style.flex = '1';
  template.querySelector('.tee-custom-content').style.width = '100%';

  function open() {
    var _opts = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

    var callback = arguments.length > 1 ? arguments[1] : undefined;
    var opts = {
      content: null
    };
    Object.assign(opts, _opts);
    document.querySelector('body').appendChild(overlayTemplate);

    if (opts.content === null || opts.content === undefined) {
      template.querySelector('.tee-custom-content').textContent = '';
    } else {
      template.querySelector('.tee-custom-content').innerHTML = opts.content;
    }

    document.querySelector('body').appendChild(template);
    template.querySelector('.button-container button').removeEventListener('click', onCloseButtonClicked);
    template.querySelector('.button-container button').addEventListener('click', onCloseButtonClicked);
  }

  function onCloseButtonClicked() {
    close();
  }

  ;

  function close() {
    document.querySelector('body').removeChild(template);
    document.querySelector('body').removeChild(overlayTemplate);
  }

  var TeeDialog = {
    open: open,
    close: close
  };
  exports.TeeDialog = TeeDialog;
})(this); //브라우저 체크



if (_validBtoc()) {
  var gtag = function gtag() {
    dataLayer.push(arguments);
  };

  //카카오톡 og 태그
  var ogText = document.createElement("meta");
  ogText.content = 'property="og:title" content="TeeSpace - 베타 오픈!';
  document.head.appendChild(ogText);
  var ogImage = document.createElement("meta");
  ogImage.content = 'property="og:image" content="https://www.teespace.com/res/metatag_cloudspace.jpg"';
  document.head.appendChild(ogImage); //구글 애널리틱스

  var script = document.createElement("script");
  script.src = "https://www.googletagmanager.com/gtag/js?id=UA-91838757-17";
  document.head.appendChild(script);
  window.dataLayer = window.dataLayer || [];
  gtag('js', new Date());
  gtag('config', 'UA-91838757-17');
} else {//b2b
}

Top.App.onLoad(function () {
  var userInfo = sessionStorage.getItem('userInfo');

  if (!userInfo && location.href.indexOf('demo.teespace.net') > -1) {
    //else if(location.href.indexOf('demo.teespace.net') > -1 ){
    TeeDialog.open({
      content: '<img src="external/Demo_Image001.png" style="width: 23.63rem; height:25.69rem; margin-top: 0.94rem " >'
    });
  }
});

function _validBtoc() {
  if (location.href.indexOf('www.teespace.com') > -1 || location.href.indexOf('www.teespace.net') > -1 || location.href.indexOf('www.teespace.co.kr') > -1 || location.href.indexOf('/TeeSpace/') > -1 || location.href.indexOf('csdev.tmaxcloudspace.com') > -1) {
    return true;
  } else {
    return false;
  }
}

function checkBrowser() {
  var agent = navigator.userAgent.toLowerCase();
  console.log("agent = ", agent);

  if (agent.indexOf("chrome") == -1 && agent.indexOf("togate") == -1 && typeof webkitRTCPeerConnection !== 'function') {
    if (navigator.appName == 'Netscape' && navigator.userAgent.search('Trident') != -1 || agent.indexOf("msie") != -1 || agent.indexOf("firefox") != -1) {
      var root = location.pathname.slice(-1) === '/' ? location.pathname : location.pathname + "/";
      location.replace(location.pathname + "external/NotSupport.html");
    }
  }
}
////// 20.02.28 코드

function officeDestroy(){
	//web
    conn = false;
    dialogStatus = false;
    isdraggingFile = false;
    lang = false;
    wsBufferAmountLimit = 500;
	uniqueUserNumber = 0;
	comlognum = 0;
	copyData = "";
	clipboardUserId = "";
	userModule= "";
	isHotkeySlide = false;
	insertImageUserId = "";
	insertImageUserName = "";
	insertImageWsID = "";
	fileTransportFlag = true;
	rommId = "";
	isFirstOpen = false;
	isInsertImageMenu = false;
	isPress = false;
	globalZoom = 0;
	globalPan = 0;
	prevDistance = 0;
	isMobile = false;
	isBinary = false;
	binaryFileId = "";
	insertFileType = 0;
	
	ws.close();
	clearTimeout(id);
	//clearInterval(intervalReturn); //not used yet
	return;
}

function showLoadingBar() { 
	var maskHeight = $(document).height(); 
	var maskWidth = window.document.body.clientWidth; 
    var mask = "<div id='mask' style='position:absolute; z-index:9000; background-color:#FFFFFF; display:none; left:0; top:0;'></div>";
    var loadingImg = ''; 
    var media = window.matchMedia("(max-device-width: 1024px)");
    if( media.matches ){
       loadingImg += "<div id='loadingImg'; style='position:absolute; left:50%; top:50%; transform: translate(-50%, -50%) display:none; z-index:100000;' >";
    }
    else{
       loadingImg += "<div id='loadingImg'; style='position:absolute; left:50%; top:50%; transform: translate(-50%, -50%); display:none; z-index:100000;' >";
    }
	loadingImg += "<img src='./../../../res/bi_loading.gif'; style='width: 144px' />"; 
	loadingImg += "</div>"; 
//	var loadingMsg = '';
//	loadingMsg += "<div id='loadingMsg'; style='position:absolute; left:45.5%; top:55%; display:none; z-index:100000;'>"; 
//	loadingMsg += "<top-textview id='testview131' layout-width='200px' layout-height='50px' border-width='0px' text='로딩 중...' style='line-height:normal; font-size:18px; color: #000000; text-align:center;'>"; 
//	loadingMsg += "<span id = 'testview131>"
//	loadingMsg += "<a class='top-textview-url'>Loading...</a>";
//	loadingMsg += "</span>";
//	loadingMsg += "</top-textview>";
//	loadingMsg += "</div>";
    $('body').append(mask);
    $('#mask').append(loadingImg); 
//	$('#mask').append(loadingMsg);
	$('#mask').css({ 'width' : maskWidth , 'height': maskHeight}); 
    $('#mask').show(); 
    $('#loadingImg').show(); 
//	$('#loadingMsg').show(); 
}

function hideLoadingBar() { $('#mask, #loadingImg, #loadingMsg').hide(); $('#mask, #loadingImg, #loadingMsg').remove(); }

function alertSlideShow() {
    if(isHotkeySlide == true || isMobile == true) { return; }
    var slideShowMsg = '';
    slideShowMsg += "<div class='slideShowMsg' style='display:none; width:350px; height:20px; height:auto; position:fixed; left:48%; margin-left:-125px; top:33%; z-index:9999; background-color:#383838; color:#F0F0F0; font-size:15px; padding:10px; text-align:center; border-radius:2px; -webkit-box-shadow: 0px 0px 24px -1px rgba(56, 56, 56, 1); -moz-box-shadow: 0px 0px 24px -1px rgba(56, 56, 56, 1); box-shadow: 0px 0px 24px -1px rgba(56, 56, 56, 1);'>";
    slideShowMsg += "전체화면으로 진행하려면 Ctrl+F5 를 누르세요</div>";
    $('body').append(slideShowMsg);
    $('.slideShowMsg').fadeIn(400).delay(1000).fadeOut(400);
	setCursor(1);
}

// 19.09.27 수정 (CS1-1 오현석 연구원)
// 컨퍼런스일 때 office 포탈로 가지않고 컨퍼런스 앱 내의 포탈로 이동해야함
function goBack() { 
//	if(parent.window.location.hash.split('/')[4].indexOf('conference') > -1) { //중간에 roomid 추가 됨에 따라서 한개씩 밀림
//		parent.window.Top.Dom.selectById("conferenceContent").src('./officePortalLayout.html');
//	} 
//	else {
//		parent.window.Top.App.routeTo('/' + parent.window.location.hash.split('/')[1] + '/' + parent.window.location.hash.split('/')[2] + '/' + parent.window.location.hash.split('/')[3]+ '/' + parent.window.location.hash.split('/')[4]);
//	}
	// 19.10.25 수정(gk3-1 소형준)
	// officePeer 객체가 파일을 꺼도 바로 사라지지 않는 이슈 해결
	officePeer.clearAllPeerConnection();
	}

// 19.10.10 gk3-1 소형준 수정
function connectFunc(userId, target, module, docName, userName, wsID, newFileFlag, fileChannelId, fileParentId, fileParentPath, fileId, layoutMode) {
	document.body.style.touchAction = 'none';
	showLoadingBar();
	setCursor(1);
	if (docName == "")
		docName = " ";
    var uri;
    if(window.location.protocol.includes("https")==true)
		uri= "wss://" + connectInfo.uri;
    else
		uri= "ws://" + connectInfo.uri;
	if (ws) {
		var docCloseJson = new ComJson(ComHeaderType.DOC_CLOSE);
		var jsonData = JSON.stringify(docCloseJson) ;
        try { ws.send(jsonData); } catch (e) { };
        try {ws.close();} catch (e) { };
		try {
			//clearInterval(intervalReturn); //not used yet
		} catch (e) {
			// do nothing
		}
	}	
	ws = new WebSocket(uri, 'user-websocket-protocol');
	ws.binaryType = "arraybuffer";
	
	let roomId;

	ws.onopen = function(ev) {
		conn = true;
		document.getElementById( 'myCanvas' ).setAttribute( 'tabindex', '1' );
        document.getElementById( 'myCanvas' ).focus();
	};

	ws.onclose = function(ev) {

		hideLoadingBar();
		officePeer.clearAllPeerConnection(roomId);
		// console.info("## websocket closed");
		//            	
		// _workspace = parent.window._workspace;
		//            	
		// $.ajax({
		// url: _workspace.url + "Office/FileClose?action=",
		// type: 'POST',
		// dataType: 'json',
		// data: JSON.stringify({
		// "dto": {
		// "USER_ID": userIdG,
		// "FILE_ID": fildIdG
		// }
		// }),
		// async: false,
		// cache: false,
		// contentType: 'application/json; charset=utf-8',
		// xhrFields: {withCredentials: true},
		// success: function(data, textStatus, jqXHR){
		// return null;
		// },
		// error: function(jqXHR, textStatus, errorThrown){
		// return null;
		// }
		// })
		if(dialogStatus){
			return;
		}
		
		if(conn){
			parent.window.alertUnstableConnection(comlognum,100);
		}
	};

	ws.onmessage = function(ev) {
		
		var result = JSON.parse(ev.data);
		var type  = result.proto_type;
		var num   = result.magic_number;
		switch (type) {
		case ComHeaderType.IMAGE:
			// demo
			// var imageSize = view.getUint32(8, true);
			// var bin = new Uint8Array(ev.data.slice(12, 12 + imageSize));
			// var d = new Date();
			// var t = d.getTime();
			// var info = { timestamp: t };
			// player.decode(bin, info);
			decodeImage(ev, view);
			break;
		case ComHeaderType.COMPLETE_NEW_WINDOW:
			detectMobile();
			break;
		case ComHeaderType.SHOW_LOADING_BAR:
			showLoadingBar();
			break;
		case ComHeaderType.HIDE_LOADING_BAR:
			hideLoadingBar();
			break;
		case ComHeaderType.INPUT_EVENT:
			break;
		case ComHeaderType.INPUT_CURSOR:
			var cursortype = result.type;
			setCursor(cursortype);
			break;
		case ComHeaderType.INPUT_OPEN_FILE:
			var apptype =  result.apptype;
			recvFileDialog(apptype, 1);
			break;
		case ComHeaderType.INPUT_SAVE_FILE:
			var apptype = result.apptype;
			recvFileDialog(apptype, 2);
			break;
		case ComHeaderType.INPUT_SAVE_FILE_AS:
			var apptype = result.apptype;
			recvFileDialog(apptype, 3);
			break;
		case ComHeaderType.INPUT_PRINT:
			var apptype = result.apptype;
			recvFileDialog(apptype, 4);
			break;
		case ComHeaderType.INPUT_CLOSE_FILE:
			var apptype = result.apptype;
			recvFileDialog(apptype, 5);
			break;
		case ComHeaderType.INPUT_INSERT_MENU:
			insertImageUserId = userId;
			insertImageUserName = userName;
			insertImageWsID = wsID;
			isInsertImageMenu = true;
			insertFileType = result.type;
			openInsertMenu();
            		break;
		case ComHeaderType.CLIPBOARD_DATA:
			var clipboardType = result.type;
			copyData = result.clipboard;
			copyComplete = true; //[로컬체크필요]
			break;
		case ComHeaderType.TRANSPORT_FILE:
			var fileName = result.filename;
            		const pdfData = atob(result.file);
            		const byteNumbers = new Array(pdfData.length);
            		for (let i=0; i<pdfData.length;i++) {
            		byteNumbers[i] = pdfData.charCodeAt(i);
            		}
            		const byteArray = new Uint8Array(byteNumbers);
            		const blob = new Blob([byteArray], {type:'application/pdf'});
            		const blobUrl = URL.createObjectURL(blob);
            		let a = document.createElement("a");
            		document.body.appendChild(a);
            		a.style = "display: none";
            		a.href = blobUrl;
            		a.download = fileName + ".pdf";
            		a.click();
            		document.body.removeChild(a);
            		URL.revokeObjectURL(blobUrl);
            		parent.window.TeeToast.open({text: '인쇄가 완료되었습니다.'});
			break;
        	case ComHeaderType.START_FILE_TRANSPORT:
        		parent.window.alertPrintDialog();
        		break;
		case ComHeaderType.OFFICE_UNUSUAL_DIE:
            		try { ws.close();} catch (e) { } 
            		finally{
            		var errorcode = result.error_code;
            		dialogStatus = true;
            		parent.window.alertUnknownError(comlognum,errorcode);
                	}
			break;
		case ComHeaderType.OFFICE_OVER_POOL_SIZE:
            		console.log("over pool size");
            		try { ws.close();} catch (e) { } 
            		finally{
            			dialogStatus = true;
            			parent.window.alertExceedResource();
            		}
            		break;
		case ComHeaderType.OFFICE_SHARE_DOCUMENT_SIZE:
            console.log("over share document");
            try { ws.close();} catch (e) { } 
            finally{
            	dialogStatus = true;
            	parent.window.alertExceedResource();
            }
            break;
        case ComHeaderType.OFFICE_LOADING_ERROR:
        	parent.window.alertUnsupportedFeature();
            break;
		case ComHeaderType.OFFICE_UNIQUE_NUMBER:
			uniqueUserNumber = result.unique_number
			comlognum = result.com_lognum
			
			// 11.13.19 PK1-3팀 정현수 수정
			if(newFileFlag) {
				// 19.11.21 os3-5 소형준 수정
                try {
                    const userAgent = navigator.userAgent.toLowerCase();
                    if (userAgent.indexOf("iphone") > -1 || userAgent.indexOf("ipad") > -1 || userAgent.indexOf("ipod") > -1) // iOS일 때
                        officeConnect.routToConferenceRoom();
                } catch (err) {
                    // nothing
                }

				// 19.10.10 gk3-1 소형준 수정
				let conferenceService = parent.window.conferenceService;
				conferenceService.room.officeRoomCreate(fileId, userId).then(function(response) {
					roomId = response.data.dto.room_id;
					parent.window.officePeer.setRoomId(roomId);
					console.log(roomId, fileId, userId);
					conferenceService.room.officeRoomUserAttend(roomId, userId, "on", "on").then(function(response){
						console.log(response.data.dto);
						
						var resolJson = ComResolJson();
						var docCipher = "";
						var docOpenJson = ComDocOpenjson(userId, module, docName, userName, wsID, newFileFlag, fileChannelId, fileParentId, fileParentPath, roomId, docCipher, layoutMode);
						try { ws.send(resolJson); } catch (e) { };
						try { ws.send(docOpenJson); } catch (e) { };
					});
				});
			} else {
				var docDownloadJson = ComDocDownloadJson(docName, userId, fileChannelId, wsID);
				try { ws.send(docDownloadJson); } catch (e) { };
			}
			
			clipboardUserId = userId;
			userModule = module;
			isFirstOpen = true;
			
            break;
	    case ComHeaderType.DOC_DOWNLOAD_RESPONSE:
	    	var cryptResult = result.result;
			
			// 19.11.21 os3-5 소형준 수정
			try {
				const userAgent = navigator.userAgent.toLowerCase();
				if (userAgent.indexOf("iphone") > -1 || userAgent.indexOf("ipad") > -1 || userAgent.indexOf("ipod") > -1) // iOS일 때
					officeConnect.routToConferenceRoom();
			} catch (err) {
				// nothing
			}
			
	    	// 19.10.10 gk3-1 소형준 수정
	    	let conferenceService = parent.window.conferenceService;
	    	conferenceService.room.officeRoomCreate(fileId, userId).then(function(response) {
	    	    roomId = response.data.dto.room_id;
	    	    parent.window.officePeer.setRoomId(roomId);
	    	    console.log(roomId, fileId, userId);
	    	    conferenceService.room.officeRoomUserAttend(roomId, userId, "on", "on").then(function(response){
	    	        console.log(response.data.dto);
	    	        
	    	        if(cryptResult == 1) {
	    	            parent.window.alertEncryptedDocOpen(ws, userId, module, docName, userName, wsID, newFileFlag, fileChannelId, fileParentId, fileParentPath, roomId);
	    	        } else if(cryptResult == 0){
                        var docCipher = "";
                        var resolJson = ComResolJson();
						var docOpenJson = ComDocOpenjson(userId, module, docName, userName, wsID, newFileFlag, fileChannelId, fileParentId, fileParentPath, roomId, docCipher, layoutMode);
	    	            try { ws.send(resolJson); } catch (e) { };
	    	            try { ws.send(docOpenJson); } catch (e) { };
	    	        } else { // -1
	    	        	parent.window.alertUnstableConnection(comlognum,301); // po-download-error
	    	        }
	    	    });
	    	});
	    	break;
	    case ComHeaderType.HYPERLINK_OPEN:
            var urlData = result.url;
            window.open(urlData);
			break;
	    case ComHeaderType.START_SLIDE_SHOW:
            setTimeout(alertSlideShow,200);
            break;
        case ComHeaderType.END_SLIDE_SHOW:
            closeFullScreenMode();
            isHotkeySlide = false;
            break;
        case ComHeaderType.OFFICE_CRYPT_RESPONSE:
        	var result = result.result;
        	if(result != 1) {
    	        
            	parent.window.alertEncryptedDocOpen(ws, userId, module, docName, userName, wsID, newFileFlag, fileChannelId, fileParentId, fileParentPath, roomId);
        	} else { isDecrypted = true; }
        	break;
        case ComHeaderType.DOC_BINARY_CONVERT_RESPONSE:
        	var result = result.result; // 변환된 문서의 fileID
        	var fileID = binaryFileId;
        	var fileName = connectInfo.fileName;
        	var fileExt = connectInfo.fileExtension + "x";
        	//////////////////////////////////////////////
    		// CS 1. 새로운 문서로 열어주세요! 
    		//////////////////////////////////////////////
        	let officeData = {
                    "whichChannel": 5,
                    "fileId": fileID,
                    "fileName": fileName,
                    "fileExtension": fileExt,
                    "fileNameWithExtension": fileName + "." + fileExt
                };
        	parent.editOffice(null, null, officeData);
        	break;
		default:
			break;
		}
	};

	ws.onerror = function(ev) {
		parent.window.alertServerNotReady();
	};
}

function detectMobile(){
	if(isFirstOpen) {
		if(isMobile){
			var mobileJson = new ComJson(ComHeaderType.MOBILE_MODE);
			var jsonData = JSON.stringify(mobileJson) ;
			try { ws.send(jsonData); } catch (e) { };
		}
	}
}

function sendDataWebSocket(jsonstring)
{
	if(ws.bufferedAmount <= wsBufferAmountLimit){
		try { ws.send(jsonstring); } catch (e) { };
	}
	else{
	}
}

function getOSInfo() 
{
   var OS = navigator.platform;
   if( OS.indexOf("Win") != -1 ) return "WINDOW"
   else if(OS.indexOf("Mac") != -1) return "MAC"
   else if(OS.indexOf("Linux") != -1) return "LINUX"
   else return "";
}

function isHangul(ch){
    c = ch.charCodeAt(0);
    if( 0x1100<=c && c<=0x11FF ) return true;
    if( 0x3130<=c && c<=0x318F ) return true;
    if( 0xAC00<=c && c<=0xD7A3 ) return true;
    return false;
}

var _appendBuffer = function(buffer1, buffer2) {
	var tmp = new Uint8Array(buffer1.byteLength + buffer2.byteLength);
	tmp.set(new Uint8Array(buffer1), 0);
	tmp.set(new Uint8Array(buffer2), buffer1.byteLength);
	return tmp.buffer;
};

// player function
function updateStatistics() {
	var s = {
		videoStartTime : 0,
		videoPictureCounter : 0,
		windowStartTime : 0,
		windowPictureCounter : 0,
		fps : 0,
		fpsMin : 1000,
		fpsMax : -1000,
		webGLTextureUploadTime : 0
	};

	s.videoPictureCounter += 1;
	s.windowPictureCounter += 1;
	var now = Date.now();
	if (!s.videoStartTime) {
		s.videoStartTime = now;
	}
	var videoElapsedTime = now - s.videoStartTime;
	s.elapsed = videoElapsedTime / 1000;
	if (videoElapsedTime < 1000) {
		return;
	}

	if (!s.windowStartTime) {
		s.windowStartTime = now;
		return;
	} else if ((now - s.windowStartTime) > 1000) {
		var windowElapsedTime = now - s.windowStartTime;
		var fps = (s.windowPictureCounter / windowElapsedTime) * 1000;
		s.windowStartTime = now;
		s.windowPictureCounter = 0;

		if (fps < s.fpsMin)
			s.fpsMin = fps;
		if (fps > s.fpsMax)
			s.fpsMax = fps;
		s.fps = fps;
	}

	var fps = (s.videoPictureCounter / videoElapsedTime) * 1000;
	s.fpsSinceStart = fps;
	this.onStatisticsUpdated(s);
	return;
}

//these functions are tools to make events bitwise
function makeValue(a, b) {
	return (a << 16) | b;
}

function makeModKeyFlag(key, modKeyFlag) {
	switch (key) {
	/* Mouse */
	case EventFlag.EF_LBUTTON_DOWN:
		modKeyFlag |= EventFlag.EF_LBUTTON_DOWN;
		break;
	case EventFlag.EF_RBUTTON_DOWN:
		modKeyFlag |= EventFlag.EF_RBUTTON_DOWN;
		break;
	case EventFlag.EF_MBUTTON_DOWN:
		modKeyFlag |= EventFlag.EF_MBUTTON_DOWN;
		break;
	case EventFlag.EF_BACKWARDBUTTON_DOWN:
		modKeyFlag |= EventFlag.EF_BACKWARDBUTTON_DOWN;
		break;
	case EventFlag.EF_FORWARDBUTTON_DOWN:
		modKeyFlag |= EventFlag.EF_FORWARDBUTTON_DOWN;
		break;
	/* KeyBoard */
	case EventFlag.EF_COMMAND_DOWN:
		modKeyFlag |= EventFlag.EF_COMMAND_DOWN;
		break;
	case EventFlag.EF_LCONTROL_DOWN:
		modKeyFlag |= EventFlag.EF_LCONTROL_DOWN;
		break;
	case EventFlag.EF_RCONTROL_DOWN:
		modKeyFlag |= EventFlag.EF_EXTENDED;
		modKeyFlag |= EventFlag.EF_RCONTROL_DOWN;
		break;
	case EventFlag.EF_LALT_DOWN:
		modKeyFlag |= EventFlag.EF_LALT_DOWN;
		break;
	case EventFlag.EF_RALT_DOWN:
		modKeyFlag |= EventFlag.EF_EXTENDED;
		modKeyFlag |= EventFlag.EF_RALT_DOWN;
		break;
	case EventFlag.EF_LSHIFT_DOWN:
		modKeyFlag |= EventFlag.EF_LSHIFT_DOWN;
		break;
	case EventFlag.EF_RSHIFT_DOWN:
		modKeyFlag |= EventFlag.EF_RSHIFT_DOWN;
		break;
	case EventFlag.EF_CAPS_LOCK_DOWN:
		modKeyFlag |= EventFlag.EF_CAPS_LOCK_DOWN;
		break;
	case EventFlag.EF_SCROLL_LOCK_DOWN:
		modKeyFlag |= EventFlag.EF_SCROLL_LOCK_DOWN;
		break;
	case EventFlag.EF_NUM_LOCK_DOWN:
		modKeyFlag |= EventFlag.EF_NUM_LOCK_DOWN;
		break;
	default:
		break;
	}
	return modKeyFlag;
}

function updateFlag(e, modKeyFlag) {
	if (e.buttons) {
		switch (e.buttons) {
		case 1: // left
			modKeyFlag = makeModKeyFlag(EventFlag.EF_LBUTTON_DOWN, modKeyFlag);
			break;
		case 2: // right
			modKeyFlag = makeModKeyFlag(EventFlag.EF_RBUTTON_DOWN, modKeyFlag);
			break;
		case 3: // left + right
			modKeyFlag = makeModKeyFlag(EventFlag.EF_LBUTTON_DOWN, modKeyFlag);
			modKeyFlag = makeModKeyFlag(EventFlag.EF_RBUTTON_DOWN, modKeyFlag);
			break;
		case 4: // middle
			modKeyFlag = makeModKeyFlag(EventFlag.EF_MBUTTON_DOWN, modKeyFlag);
			break;
		case 7: // left + right + middle
			modKeyFlag = makeModKeyFlag(EventFlag.EF_LBUTTON_DOWN, modKeyFlag);
			modKeyFlag = makeModKeyFlag(EventFlag.EF_MBUTTON_DOWN, modKeyFlag);
			modKeyFlag = makeModKeyFlag(EventFlag.EF_RBUTTON_DOWN, modKeyFlag);
			break;
		case 8: // back
			modKeyFlag = makeModKeyFlag(EventFlag.EF_BACKWARDBUTTON_DOWN,
					modKeyFlag);
			break;
		case 16: // forward
			modKeyFlag = makeModKeyFlag(EventFlag.EF_FORWARDBUTTON_DOWN,
					modKeyFlag);
			break;
		}
	}
	if (e.getModifierState("CapsLock")) {
		modKeyFlag = makeModKeyFlag(EventFlag.EF_CAPS_LOCK_DOWN, modKeyFlag);
	}
	if (e.getModifierState("NumLock")) {
		modKeyFlag = makeModKeyFlag(EventFlag.EF_NUM_LOCK_DOWN, modKeyFlag);
	}
	if (e.getModifierState("ScrollLock")) {
		modKeyFlag = makeModKeyFlag(EventFlag.EF_SCROLL_LOCK_DOWN, modKeyFlag);
	}
	if (e.getModifierState("Control")) {
		modKeyFlag = makeModKeyFlag(EventFlag.EF_LCONTROL_DOWN, modKeyFlag);
	}
	if (e.getModifierState("Shift")) {
		if (!e.location) { // mouse event시에는 location이 undefined
			modKeyFlag = makeModKeyFlag(EventFlag.EF_LSHIFT_DOWN, modKeyFlag);
		}
		if (e.location == 1) {
			modKeyFlag = makeModKeyFlag(EventFlag.EF_LSHIFT_DOWN, modKeyFlag);
		}
		if (e.location == 2) {
			modKeyFlag = makeModKeyFlag(EventFlag.EF_RSHIFT_DOWN, modKeyFlag);
		}
	}
	if (e.getModifierState("Alt")) {
		modKeyFlag = makeModKeyFlag(EventFlag.EF_LALT_DOWN, modKeyFlag);
	}
	if (e.getModifierState("Meta")) { //window 키 넣어야하나?
		modKeyFlag = makeModKeyFlag(EventFlag.EF_COMMAND_DOWN, modKeyFlag);
	}
	//right alt, right control의 js 키코드가 다른 상황에 대해 처리가 필요. 해당키는 한/영, 한자 키로 사용으로 처리
//    if (e.keyCode == 21) { // 오른쪽 alt키 한영키와 동일
//        if(e.code === "AltRight"){
//            modKeyFlag = makeModKeyFlag(EventFlag.EF_RALT_DOWN, modKeyFlag);
//        }
//    }
//    if (e.keyCode == 25) { // 오른쪽 ctrl키 한자키와 동일
//        if(e.code === "ControlRight"){
//            modKeyFlag = makeModKeyFlag(EventFlag.EF_RCONTROL_DOWN, modKeyFlag);
//        }
//    }

	return modKeyFlag;
}

function recvFileDialog(apptype, commandtype) {

	switch (commandtype) {
		case 0: // new_file
			break;
		case 1: // open_file
		    openFileMenu();
			break;
		case 2: // save_file
			break;
		case 3: // save_file_as
			// 2020-03-18 TS2-2
			parent.window.saveOffice(connectInfo.fileId, connectInfo.fileName, connectInfo.fileExtension);
				//.then(function(success){newFileFlag = !success}); //set new file flag to false on successful save					
			break;
		case 4: // print
			break;
		case 5: // close_file
			break;
	}

}

function setCursor(cursortype) {
	// demo
	var imagebody = document.getElementById("myCanvas");
	switch (cursortype) {
	case 0:
		imagebody.style.cursor = 'default';
		break;
	case 1:
		imagebody.style.cursor = 'auto';
		break;
	case 2:
		imagebody.style.cursor = 'text';
		break;
	case 3:
		imagebody.style.cursor = 'pointer';
		break;
	case 4:
		imagebody.style.cursor = 'text';
		break;
	case 5:
		imagebody.style.cursor = 'wait';
		break;
	case 6:
		imagebody.style.cursor = 'help';
		break;
	case 7:
		imagebody.style.cursor = 'e-resize';
		break;
	case 8:
		imagebody.style.cursor = 'n-resize';
		break;
	case 9:
		imagebody.style.cursor = 'ne-resize';
		break;
	case 10:
		imagebody.style.cursor = 'nw-resize';
		break;
	case 11:
		imagebody.style.cursor = 's-resize';
		break;
	case 12:
		imagebody.style.cursor = 'se-resize';
		break;
	case 13:
		imagebody.style.cursor = 'sw-resize';
		break;
	case 14:
		imagebody.style.cursor = 'w-resize';
		break;
	case 15:
		imagebody.style.cursor = 'ns-resize';
		break;
	case 16:
		imagebody.style.cursor = 'ew-resize';
		break;
	case 17:
		imagebody.style.cursor = 'nesw-resize';
		break;
	case 18:
		imagebody.style.cursor = 'nwse-resize';
		break;
	case 19:
		imagebody.style.cursor = 'url("./../../../res/office/officeCursor/columnresize.png") 15 15,auto';
		break;
	case 20:
		imagebody.style.cursor = 'url("./../../../res/office/officeCursor/rowresize.png") 15 15,auto';
		break;
	case 21:
		break;
	case 22:
		break;
	case 23:
		break;
	case 24:
		break;
	case 25:
		break;
	case 26:
		break;
	case 27:
		break;
	case 28:
		break;
	case 29:
		break;
	case 30:
		imagebody.style.cursor = 'move';
		break;
	case 31:
		imagebody.style.cursor = 'vertical-text';
		break;
	case 32:
		imagebody.style.cursor = 'cell';
		break;
	case 33: // context menu
		imagebody.style.cursor = 'context-menu';
		break;
	case 34:
		imagebody.style.cursor = 'alias';
		break;
	case 35:
		imagebody.style.cursor = 'progress';
		break;
	case 36:
		imagebody.style.cursor = 'no-drop';
		break;
	case 37:
		imagebody.style.cursor = 'copy';
		break;
	case 38:
//		imagebody.style.cursor = 'none';
		break;
	case 39:
		imagebody.style.cursor = 'not-allowed';
		break;
	case 40: // 줌인
		imagebody.style.cursor = 'zoom-in';
		break;
	case 41: // 줌 아웃
		imagebody.style.cursor = 'zoom-out';
		break;
	case 42: // grab
		imagebody.style.cursor = 'grab';
		break;
	case 43: // grabbing
		imagebody.style.cursor = 'grabbing';
		break;
	case 44: // custom
		break;
	case 45: // drag move
		imagebody.style.cursor = 'move';
		break;
	 case 46: imagebody.style.cursor = 'no-drop';
	     break;
	 case 47: imagebody.style.cursor = 'url("./../../../res/office/officeCursor/cursor_cut_bottomcenter.png") 25 25,auto';
	     break;
	 case 48: imagebody.style.cursor = 'url("./../../../res/office/officeCursor/cursor_cell_x_axis_control.png") 25 25,auto';
	     break;
	 case 49: imagebody.style.cursor = 'url("./../../../res/office/officeCursor/cursor_rotate.png") 25 25,auto';
	     break;
	 case 50: imagebody.style.cursor = 'url("./../../../res/office/officeCursor/cursor_point_west.png") 25 25,auto';
	     break;
	 case 51: imagebody.style.cursor = 'url("./../../../res/office/officeCursor/cursor_cell_pivot_field_move_3.png") 25 25,auto';
	     break;
	 case 52: imagebody.style.cursor = 'url("./../../../res/office/officeCursor/cursor_rotate_hover.png") 25 25,auto';
	     break;
	 case 53: imagebody.style.cursor = 'url("./../../../res/office/officeCursor/cursor_cut_topcenter.png") 25 25,auto';
	     break;
	 case 54: imagebody.style.cursor = 'url("./../../../res/office/officeCursor/cursor_point_east.png") 25 25,auto';
	     break;
	 case 55: imagebody.style.cursor = 'url("./../../../res/office/officeCursor/cursor_cell_pivot_field_move_5.png") 25 25,auto';
	     break;
	 case 56: imagebody.style.cursor = 'col-resize';
	     break;
	 case 57: imagebody.style.cursor = 'url("./../../../res/office/officeCursor/cursor_align_center.png") 25 25,auto';
	     break;
	 case 58: imagebody.style.cursor = 'url("./../../../res/office/officeCursor/cursor_subdocument.png") 25 25,auto';
	     break;
	 case 59: imagebody.style.cursor = 'url("./../../../res/office/officeCursor/cursor_clear.png") 25 25,auto';
	     break;
	 case 60: imagebody.style.cursor = 'url("./../../../res/office/officeCursor/cursor_cell_sheet_move.png") 25 25,auto';
	     break;
	 case 61: imagebody.style.cursor = 'url("./../../../res/office/officeCursor/cursor_presenter_pen.png") 25 25,auto';
	     break;
	 case 62: imagebody.style.cursor = 'url("./../../../res/office/officeCursor/cursor_control_cell.png") 25 25,auto';
	     break;
	 case 63: imagebody.style.cursor = 'url("./../../../res/office/officeCursor/cursor_cell_y_axis_control.png") 25 25,auto';
	     break;
	 case 64: imagebody.style.cursor = 'url("./../../../res/office/officeCursor/cursor_cut_bottomleft.png")25 25 ,auto';
	     break;
	 case 65: imagebody.style.cursor = 'url("./../../../res/office/officeCursor/cursor_cell_pivot_field_move_1.png") 25 25,auto';
	     break;
	 case 66: imagebody.style.cursor = 'url("./../../../res/office/officeCursor/cursor_cell_pivot_field_move_2.png") 25 25,auto';
	     break;
	 case 67: imagebody.style.cursor = 'url("./../../../res/office/officeCursor/cursor_align_right.png") 25 25,auto';
	     break;
	 case 68: imagebody.style.cursor = 'url("./../../../res/office/officeCursor/cursor_point_diagonal.png") 25 25,auto';
	     break;
	 case 69: imagebody.style.cursor = 'url("./../../../res/office/officeCursor/cursor_format_painter.png") 25 25,auto';
	     break;
	 case 70: imagebody.style.cursor = 'url("./../../../res/office/officeCursor/cursor_contentsbox_move_hover.png") 25 25,auto';
	     break;
	 case 71: imagebody.style.cursor = 'url("./../../../res/office/officeCursor/cursor_cell_diagonal.png") 25 25,auto';
	     break;
	 case 72: imagebody.style.cursor = 'url("./../../../res/office/officeCursor/cursor_presenter_laserpoint.png") 25 25,none';
	     break;
	 case 73: imagebody.style.cursor = 'url("./../../../res/office/officeCursor/cursor_align_left.png") 25 25,auto';
	     break;
	 case 74: imagebody.style.cursor = 'url("./../../../res/office/officeCursor/cursor_point_south.png") 25 25 ,auto';
	     break;
	 case 75: imagebody.style.cursor = 'url("./../../../res/office/officeCursor/cursor_draw_borders.png") 25 25,auto';
	     break;
	 case 76: imagebody.style.cursor = 'url("./../../../res/office/officeCursor/cursor_hwp_object_hover.png") 25 25,auto';
	     break;
	 case 77: imagebody.style.cursor = 'row-resize';
	     break;
	 case 78: imagebody.style.cursor = 'url("./../../../res/office/officeCursor/cursor_point_north.png") 25 25 ,auto';
	     break;
	 case 79: imagebody.style.cursor = 'url("./../../../res/office/officeCursor/cursor_endnote.png") 25 25,auto';
	     break;
	 case 80: imagebody.style.cursor = 'url("./../../../res/office/officeCursor/diagonal_east.png") 9 9,auto';
	     break;
	 case 81: imagebody.style.cursor = 'url("./../../../res/office/officeCursor/cursor_hwp_cell_table_control_impassible.png") 25 25,auto';
	     break;
	 case 82: imagebody.style.cursor = 'url("./../../../res/office/officeCursor/cross_animation.png") 25 25,auto';
	     break;
	 case 83: imagebody.style.cursor = 'url("./../../../res/office/officeCursor/cursor_cut_bottomright.png") 25 25,auto';
	     break;
	 case 84: imagebody.style.cursor = 'url("./../../../res/office/officeCursor/cursor_adjust_handler.png") 25 25,auto';
	     break;
	 case 85: imagebody.style.cursor = 'url("./../../../res/office/officeCursor/cursor_spoid.png") 25 25,auto';
	     break;
	 case 86: imagebody.style.cursor = 'url("./../../../res/office/officeCursor/cursor_cut_topleft.png") 25 25,auto';
	     break;
	 case 87: imagebody.style.cursor = 'url("./../../../res/office/officeCursor/cursor_cell_pivot_field_move_4.png") 25 25,auto';
	     break;
	 case 88: imagebody.style.cursor = 'url("./../../../res/office/officeCursor/cursor_animation_frame.png") 25 25,auto';
	     break;
	 case 89: imagebody.style.cursor = 'url("./../../../res/office/officeCursor/cursor_cut_leftcenter.png") 25 25,auto';
	     break;
	 case 90: imagebody.style.cursor = 'url("./../../../res/office/officeCursor/cursor_hwp_cell_vertex_control_impassible.png") 25 25,auto';
	     break;
	 case 91: imagebody.style.cursor = 'url("./../../../res/office/officeCursor/cursor_cut_rightcenter.png") 25 25,auto';
	     break;
	 case 92: imagebody.style.cursor = 'url("./../../../res/office/officeCursor/cursor_cut_topright.png") 25 25,auto';
	     break;
	 case 93: imagebody.style.cursor = 'url("./../../../res/office/officeCursor/cursor_picture_opacity.png") 25 25,auto';
	     break;
	 case 94: imagebody.style.cursor = 'url("./../../../res/office/officeCursor/angle_edit.png") 25 25,auto';
	     break;
	 case 95: imagebody.style.cursor = 'url("./../../../res/office/officeCursor/cursor_cell_multi_sheet_move.png") 25 25 ,auto';
	     break;
	 case 96: imagebody.style.cursor = 'url("./../../../res/office/officeCursor/cursor_point_drag.png") 25 25,auto';
	     break;
	 case 97: imagebody.style.cursor = 'url("./../../../res/office/officeCursor/cursor_pen.png") 25 25,auto';
	     break;
	 case 98: imagebody.style.cursor = 'url("./../../../res/office/officeCursor/cursor_hwp_headfoot.png") 25 25,auto';
	     break;
	 case 99: imagebody.style.cursor = 'url("./../../../res/office/officeCursor/cursor_highlight.png") 25 25,auto';
	     break;
	 case 100: imagebody.style.cursor = 'url("./../../../res/office/officeCursor/angle_plus.png") 25 25,auto';
	     break;
	default:
		break;
	}

}

function convertMacShortCut(e){
    if(e.keyCode==13) {//alt(option)-Enter(return)
        if(e.altKey){
            return 25; //한자 변환키
        }
    }
    if(e.keyCode==32){ //ctrl-spacebar or command-spacebar = 한/영 변환키
        if(e.ctrlKey || e.metaKey){
            return 21; //한/영 변환키
        }
    }
    if(e.keyCode==37){ //command-left arrow = Home
        if(e.metaKey){
            return 36;
        }
    }
    if(e.keyCode==38){ //command-up arrow = PGUP
        if(e.metaKey){
            return 33;
        }
    }
    if(e.keyCode==39){ //command-right arrow = End
        if(e.metaKey){
            return 35;
        }
    }
    if(e.keyCode==40){ //command-down arrow = PGDN
        if(e.metaKey){
            return 34;
        }
    }
    return e.keyCode;
}

function convertKeyCodeJsToLinux(value) {
	 switch (value) {
     case 8:
         value = 14;
         break;
     case 9:
         value = 15;
         break;
     case 13:
         value = 28;
         break;
     case 16:
         //leftshift key mapping
         value = 42;
         break;
     case 17:
         value = 29;
         break;
     case 18:
         value = 56;
         break;
     case 19:
         value = 119;
         break;
     case 20:
         value = 58;
         break;
     case 21:
         value = 122;
         //need language key mapping
         break;
     case 25:
         value = 123;
         //need language key mapping
         break;
     case 27:
         value = 1;
         break;
     case 32:
         value = 57;
         break;
     case 33:
         value = 104;
         break;
     case 34:
         value = 109;
         break;
     case 35:
         value = 107;
         break;
     case 36:
         value = 102;
         break;
     case 37:
         value = 105;
         break;
     case 38:
         value = 103;
         break;
     case 39:
         value = 106;
         break;
     case 40:
         value = 108;
         break;
     case 45:
         value = 110;
         break;
     case 46:
         value = 111;
         break;
     case 48:
         value = 11;
         break;
     case 49:
         value = 2;
         break;
     case 50:
         value = 3;
         break;
     case 51:
         value = 4;
         break;
     case 52:
         value = 5;
         break;
     case 53:
         value = 6;
         break;
     case 54:
         value = 7;
         break;
     case 55:
         value = 8;
         break;
     case 56:
         value = 9;
         break;
     case 57:
         value = 10;
         break;
     case 59:
         value = 39;
         break;
     case 61:
         value = 13;
         break;
     //65~90 A~Z
     case 65:
         value = 30;
         break;
     case 66:
         value = 48;
         break;
     case 67:
         value = 46;
         break;
     case 68:
         value = 32;
         break;
     case 69:
         value = 18;
         break;
     case 70:
         value = 33;
         break;
     case 71:
         value = 34;
         break;
     case 72:
         value = 35;
         break;
     case 73:
         value = 23;
         break;
     case 74:
         value = 36;
         break;
     case 75:
         value = 37;
         break;
     case 76:
         value = 38;
         break;
     case 77:
         value = 50;
         break;
     case 78:
         value = 49;
         break;
     case 79:
         value = 24;
         break;
     case 80:
         value = 25;
         break;
     case 81:
         value = 16;
         break;
     case 82:
         value = 19;
         break;
     case 83:
         value = 31;
         break;
     case 84:
         value = 20;
         break;
     case 85:
         value = 22;
         break;
     case 86:
         value = 47;
         break;
     case 87:
         value = 17;
         break;
     case 88:
         value = 45;
         break;
     case 89:
         value = 21;
         break;
     case 90:
         value = 44;
         break;
     case 91:
         //window left key
         break;
     case 92:
         //window right key
         break;
     case 93:
         //fucntion key
         break;
     case 96:
         value = 82;
         break;
     case 97:
         value = 79;
         break;
     case 98:
         value = 80;
         break;
     case 99:
         value = 81;
         break;
     case 100:
         value = 75;
         break;
     case 101:
         value = 76;
         break;
     case 102:
         value = 77;
         break;
     case 103:
         value = 71;
         break;
     case 104:
         value = 72;
         break;
     case 105:
         value = 73;
         break;
     case 106:
         value = 55;
         break;
     case 107:
         value = 78;
         break;
     case 109:
         value = 74;
         break;
     case 110:
         value = 83;
         break;
     case 111:
         value = 98;
         break;
     case 112:
         value = 59;
         break;
     case 113:
         value = 60;
         break;
     case 114:
         value = 61;
         break;
     case 115:
         value = 62;
         break;
     case 116:
         value = 63;
         break;
     case 117:
         value = 64;
         break;
     case 118:
         value = 65;
         break;
     case 119:
         value = 66;
         break;
     case 120:
         value = 67;
         break;
     case 121:
         value = 68;
         break;
     case 122:
         value = 87;
         break;
     case 123:
         value = 88;
         break;
     case 144:
         value = 69;
         break;
     case 145:
         value = 70;
         break;
     case 173:
         value = 12;
         break;
     case 186:
         value = 39;
         break;
     case 188:
         value = 51;
         break;
     case 190:
         value = 52;
         break;
     case 191:    // /키
         value = 53;
         break;
     case 192:
         value = 41;
         break;
     case 187:
         value = 13;
         break;
     case 189:
         value = 12;
         break;
     case 219:
         value = 26;
         break;
     case 220:
         value = 43;
         break;
     case 221:
         value = 27;
         break;
     case 222:    // '키
         value = 40;
         break;
     case 229:
         value = 122;
         break;
	 }
	 return value;
};

function copyProcess(e) {
	if(mouseDown)
		return;
    var url;
    if(window.location.protocol.includes("https")==true)
    	url= "https://" + connectInfo.uri;
    else
    	url= "http://" + connectInfo.uri;

    var copyJsonObject = new Object();
    copyJsonObject.type = AjaxType.AJAX_COPY;
    copyJsonObject.wfd = uniqueUserNumber;
    var requestBody = JSON.stringify(copyJsonObject);

    $.ajax({
        crossOrigin: true,
        type: "POST",
        url: url,
        data: requestBody,
        dataType: 'html',
        async: false,
        success: function (data, msg, state) {
            //전역변수에 data setting
            copyData = data;
        },
        error: function (data, msg, state) {
        }
    });
    
	e.preventDefault();
	if(copyData == "ERROR"){
		return;
	}
	
	var result = JSON.parse(copyData);
	
	if(result.html == null)
		result.html = "";
    
	if(result.image)
    {
        var byteCharacters = atob(result.image);
		var byteArrays = []; 
		for(let offset =0; offset < byteCharacters.length; offset += 512)
		{
			const slice = byteCharacters.slice(offset, offset + 512); 
			const byteNumbers = new Array(slice.length); 
			for(let i = 0; i < slice.length; i++)
			{
				byteNumbers[i] = slice.charCodeAt(i); 
			}
			const byteArray = new Uint8Array(byteNumbers); 
			byteArrays.push(byteArray); 
		}

		const blob = new Blob(byteArrays, {type: 'image/png'}); 
		try{
			navigator.clipboard.write([new ClipboardItem({['image/png']: blob}) ]);
	}
		catch(err)
		{
			console.error(err.name,err.message); 
			console.log("image Copy failed");
		}
	}
	else
	{
    	e.clipboardData.setData('text/html', result.html);
		e.clipboardData.setData('text/plain', result.plaintext);
		e.clipboardData.setData('application/json', result.tmax);
	}
}

function pasteProcess(e) {
	//data_json
    var clipboardData, htmlData, textData, tmax;
	var totalInfo = new Object();

    e.stopPropagation();
    e.preventDefault();

    // Get pasted data via clipboard API
    clipboardData = e.clipboardData || window.clipboardData;
    
	tmax = clipboardData.getData('application/json');
    if(tmax == clipboardUserId){
		//여기 들어왔다는 내부 Office server쪽에서 data를 가져와야 하는 경우.
		var clibBoardDataServerJson = ComJson(ComHeaderType.CLIPBOARD_DATA_SERVER);
		var JsonData = JSON.stringify(clibBoardDataServerJson);
		if (ws.bufferedAmount == 0){
			try { ws.send(JsonData); } catch (e) { };
		}
		else{
		}
		return;
	}
    
    htmlData = clipboardData.getData('text/html');
	if(htmlData != "")
		totalInfo.html = htmlData;
	
	textData = clipboardData.getData('text/plain');
	if(textData != "")
		totalInfo.plaintext = textData;
		
	var items = clipboardData.items;
	var itemLength = items.length;
	
	var fileMetaBuffer = new ArrayBuffer(2);
	var view = new DataView(fileMetaBuffer);
	view.setUint8(0, 0, true); // png
	view.setUint8(1, 0, true); // jpeg
	
	var isPng = false;
	var isJpeg = false;
	for(var i = 0; i < itemLength; i++)
	{
		if(items[i].type.indexOf("image") == -1) continue;
		//여기 들어왔다는 것은 무조건 async하게 fileReader를 통해 data를 전송해야 함.
		if(items[i].type.indexOf("png") != -1){
			view.setUint8(0, 1, true); // png
			isPng = true;
		}
		else if(items[i].type.indexOf("jpeg") != -1 || items[i].type.indexOf("jpg") != -1){
			view.setUint8(1, 1, true); // jpeg
			isJpeg = true;
		}
	}
	
	var onlyPng = false;
	//png가 우선순위가 높다
	//둘다 있으면 png
	//하나만 있으면 그것만
	if(isPng == true && isJpeg == true)
		onlyPng = true;
		
	var jsonInfo = JSON.stringify(totalInfo);
	
    var enc = new TextEncoder(); //always utf-8
    var clipBoardBuffer = enc.encode(jsonInfo);
    var comHeader = new ComFileHeader(ComHeaderType.CLIPBOARD_DATA);
    var headerBuffer = new ArrayBuffer(comHeader.getLength);
    comHeader.insertToBuff(headerBuffer);
	
	var headAndFileMetaBuffer = new Uint8Array(headerBuffer.byteLength + 2);
    headAndFileMetaBuffer.set(new Uint8Array(headerBuffer), 0);
    headAndFileMetaBuffer.set(new Uint8Array(fileMetaBuffer), headerBuffer.byteLength);

	var resultBuffer = _appendBuffer(headAndFileMetaBuffer.buffer, clipBoardBuffer);
	var clibBoardDataJson = ComJson(ComHeaderType.CLIPBOARD_DATA);
	var JsonData = JSON.stringify(clibBoardDataJson);
	if (ws.bufferedAmount == 0){
		try { ws.send(resultBuffer); } catch (e) { };
	}
	else{
	}
	
	//file 전송 시작 부분
	for(var i = 0; i < itemLength; i++)
	{
		if(items[i].type.indexOf("image") == -1) continue;
		
		if(onlyPng && (items[i].type.indexOf("jpg") != -1
		&& items[i].type.indexOf("jpeg") != -1))
			continue;
			
		var blob = items[i].getAsFile();
		var fileReader = new FileReader();
		fileReader.onload = function(e) {
			// The file's text will be printed here
			var filetData = e.target.result;
			var fileHeader = new ComFileHeader(ComHeaderType.CLIPBOARD_DATA_FILE);
			var fileHeaderBuffer = new ArrayBuffer(fileHeader.getLength);
			fileHeader.insertToBuff(fileHeaderBuffer);
			var fileResultBuffer = _appendBuffer(fileHeaderBuffer, filetData);
			var clibBoardDataFileJson = ComJson(ComHeaderType.CLIPBOARD_DATA_FILE);
			var JsonData = JSON.stringify(clibBoardDataFileJson);
			ws.send(fileResultBuffer);
		};
		fileReader.readAsArrayBuffer(blob);
	}
}

function cutProcess(e)
{
    var url;
    if(window.location.protocol.includes("https")==true)
    	url= "https://" + connectInfo.uri;
    else
    	url= "http://" + connectInfo.uri;

    var cutJsonObject = new Object();
    cutJsonObject.type = AjaxType.AJAX_CUT;
    cutJsonObject.wfd = uniqueUserNumber;
    var requestBody = JSON.stringify(cutJsonObject);

    $.ajax({
        crossOrigin: true,
        type: "POST",
        url: url,
        data: requestBody,
        dataType: 'html',
        async: false,
        success: function (data, msg, state) {
            copyData = data;
        },
        error: function (data, msg, state) {
        }
    });
	
	e.preventDefault();
	if(copyData == "ERROR"){
		return;
	}
    
	var result = JSON.parse(copyData);

    e.clipboardData.setData('text/html', result.html);
	e.clipboardData.setData('text/plain', result.plaintext);
	e.clipboardData.setData('application/json', result.tmax);
}

function arrayBufferToBase64(buffer) {
	var arrayBuffer = new Uint8Array(buffer);
	let str = new TextDecoder().decode(arrayBuffer);
	return str;
}

function Utf8ArrayToStr(array) {
	var out, i, len, c;
	var char2, char3;

	out = "";
	len = array.length;
	i = 0;
	while (i < len) {
		c = array[i++];
		switch (c >> 4) {
		case 0:
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
		case 6:
		case 7:
			// 0xxxxxxx
			out += String.fromCharCode(c);
			break;
		case 12:
		case 13:
			// 110x xxxx 10xx xxxx
			char2 = array[i++];
			out += String.fromCharCode(((c & 0x1F) << 6) | (char2 & 0x3F));
			break;
		case 14:
			// 1110 xxxx 10xx xxxx 10xx xxxx
			char2 = array[i++];
			char3 = array[i++];
			out += String.fromCharCode(((c & 0x0F) << 12)
					| ((char2 & 0x3F) << 6) | ((char3 & 0x3F) << 0));
			break;
		}
	}

	return out;
}

function decodeImage(ev, view) {
	var imageSize = view.getUint32(8, true);
	var bin = new Uint8Array(ev.data.slice(12, 12 + imageSize));
	var image = new Image();
	var c = document.getElementById("myCanvas");
	var ctx = c.getContext("2d");
	image.onload = function() {
		ctx.drawImage(image, 0, 0);
	}
	image.src = 'data:image/jpeg;base64,' + Utf8ArrayToStr(bin);
}
function ComFileHeader(type) {
	this.magic_number = 0x004d4f43;
	this.type = type;
	this.getLength = 8;
	this.insertToBuff = function(buffer) {
		var view = new DataView(buffer);
		view.setUint32(0, this.magic_number, true);
		view.setUint32(4, this.type, true);
	};
	return this;
}
function ComJson(type) {
	
	var json = new Object() ;		
	json.magic_number = 0x004d4f43 ;
	json.proto_type = type;
	return json;
}

function ComBodyDocBinary(fileID) {
	this.fileID = fileID;
	
	this.headerLength = 1;
	this.bodySize = fileID.length;
	
	this.insertToBuff = function (buffer, headerSize) {
		var value = new Uint16Array(buffer);

        value[headerSize] = fileID.length;
        
        var totalLength = headerSize + 1;

        for (var i = 0; i < fileID.length; i++) {
            value[totalLength + i] = this.fileID.charCodeAt(i);
        }
	}
}

function ComResolJson() {
	var jsonData = ComJson(ComHeaderType.RESOL);
	jsonData.width = screen.width;
	jsonData.height = screen.height;

	var imageView = document.querySelector("#contentContainer");
	jsonData.initWidth = imageView.clientWidth;
	jsonData.initHeight = imageView.clientHeight;
	
    myCanvas.width = imageView.clientWidth;
	myCanvas.height = imageView.clientHeight;
	return JSON.stringify(jsonData);
}

function ComDocOpenjson(userId, module, docName, userName, wsID, newFileFlag, fileChannelId, fileParentId, fileParentPath, roomId, cipher, layoutMode) {
	var jsonData = ComJson(ComHeaderType.DOC_OPEN);
	jsonData.userID = userId;
	jsonData.docID = docName;
	jsonData.module = module;
	jsonData.userName = userName;
	jsonData.wsID = wsID;
	jsonData.newFileFlag = newFileFlag;
	jsonData.fileChannelID = fileChannelId;
	jsonData.fileParentID = fileParentId;
	jsonData.fileParentPath = fileParentPath;   
	jsonData.roomID = roomId;
	jsonData.cipher= cipher;
	jsonData.isMobile= isMobile;
	jsonData.layoutMode= layoutMode;
	return JSON.stringify(jsonData);
}
function ComDocDownloadJson(docName, userId, fileChannelId, wsID) {
	var jsonData = ComJson(ComHeaderType.DOC_DOWNLOAD);
	jsonData.userID = userId;
	jsonData.docID = docName;
	jsonData.wsID = wsID;
	jsonData.fileChannelId = fileChannelId;
	return JSON.stringify(jsonData);
}
function MakeFileMetaDataJson(filename, fileNumber, fileTypeNum){
	var jsonData = ComJson(ComHeaderType.FILE_META_DATA);
	jsonData.fileName = filename;
	jsonData.fileNumber = fileNumber;
	jsonData.fileType = fileTypeNum;
	return JSON.stringify(jsonData);
}
function makeShouldPaintJson(flag)
{
	var jsonData = ComJson(ComHeaderType.SHOULD_PAINT);
	jsonData.flag = flag;
	return JSON.stringify(jsonData);
}
function makeInsertFileJson(insertImageUserId, userModule, image_path, insertImageUserName, insertImageWsID, type)
{
	var jsonData = ComJson(ComHeaderType.OFFICE_INSERT_FILE);
	jsonData.userId = userId;
	jsonData.docID = image_path;
	jsonData.module = userModule;
	jsonData.userName = insertImageUserName;
	jsonData.wsID = insertImageWsID;
	jsonData.type = type;
	return JSON.stringify(jsonData);
}
function makeInputEventJson(flag, value, modKeyFlag, isRepeated)
{
	var jsonData = ComJson(ComHeaderType.INPUT_EVENT);
	jsonData.modKeyFlag = modKeyFlag;
	jsonData.flag = flag;
	jsonData.value = value;
	jsonData.isRepeated = isRepeated;
	return JSON.stringify(jsonData);
}
function makeMobilePinchJson(centerX, centerY, deltaX, deltaY,velocityX,velocityY,pinchType)
{
	var jsonData = ComJson(ComHeaderType.INPUT_MOBILE_PINCH);
	jsonData.centerX = centerX;
	jsonData.centerY = centerY;
	jsonData.deltaX = deltaX;
	jsonData.deltaY = deltaY;
	jsonData.velocityX = velocityX;
	jsonData.velocityY = velocityY;
	jsonData.pinchType = pinchType;
	return JSON.stringify(jsonData);
}
function makeDocBinaryConvertJson(fileID) {
	var jsonData = ComJson(ComHeaderType.DOC_BINARY_CONVERT);
	jsonData.fileId = fileID;

	return JSON.stringify(jsonData);
}
function makeOfficeLayoutModeChangedJson(mode)
{
	var jsonData = ComJson(ComHeaderType.OFFICE_LAYOUT_MODE_CHANGE);
	jsonData.layoutMode = mode;
	return JSON.stringify(jsonData);
}
function makeBrowserSizeJson() {
	var jsonData = ComJson(ComHeaderType.BROWSER_SIZE);
	var imageView = document.querySelector("#contentContainer");
	var width = imageView.clientWidth;
	var height = imageView.clientHeight;
	jsonData.width = width;
	jsonData.height = height;
	setMyCanvas(width, height);

	return JSON.stringify(jsonData);
}

function setMyCanvas(width,height){
	if(myCanvas.width != width || myCanvas.height!= height){
		myCanvas.width = width;
		myCanvas.height = height;
	}
}
function fileRename(fileName) {
    if (ws.bufferedAmount != 0)
        return;
        
//    var url = "http://" + document.getElementById("uri").value;
    var url;
    if(window.location.protocol.includes("https")==true)
    	url= "https://" + connectInfo.uri;
    else
    	url= "http://" + connectInfo.uri;

    fileNameInfo = new Object();
    fileNameInfo.type = AjaxType.AJAX_FILE_RENAME;
    fileNameInfo.fileName = fileName;
    fileNameInfo.wfd = uniqueUserNumber;
    fileNameData = JSON.stringify(fileNameInfo);
    
    var result = false;

    $.ajax({
        crossOrigin: true,
        type: "POST",
        url: url,
        data: fileNameData,
        dataType: 'html',
        async: false,
        success: function (data, msg, state) {
            result = true;
        },
        error: function (data, msg, state) {
            result = false;
        }
    });

    return result;
}

function onOfficeLayoutModeChanged(mode) {
	var officeLayoutModeChangejson = makeOfficeLayoutModeChangedJson(mode);
	sendDataWebSocket(officeLayoutModeChangejson);
}

function browserSize(e) {
	clearTimeout(id);
	id = setTimeout(doneResizing, 500);
}

function doneResizing() {
	var browserSizejson = makeBrowserSizeJson();
	sendDataWebSocket(browserSizejson);
}

function requestSchedulePaint() {
	var schedulePaintJson = ComJson(ComHeaderType.SCHEDULE_PAINT);
	var jsonString = JSON.stringify(schedulePaintJson) ;
	sendDataWebSocket(jsonString);
}

function blockKeyPressed(e){
    //nothing ==> return 0;
    //browser prevented & COM accepted  ==> return 1;
    //browser accepted & COM prevented ==> return 2;
    //browser prevented & COM prevented  ==> return 3;
    if (
        (e.keyCode === 8)  //backspace 뒤로가기 방지
        || (e.ctrlKey && e.keyCode == 116) //ctrl + f5 // f5보다 먼저 막아줘야함
        || (e.keyCode === 9)  //tab
        || (e.keyCode === 32)  //spacebar
        || (e.keyCode === 111)  //Numlock divide '/'
        || (e.keyCode === 118) //F7
        || (e.keyCode === 191)  //General Key divide '/'
        || (e.keyCode === 222)  //single quote '
        || (e.keyCode >= 37 && e.keyCode <= 40) //arrow key event prevented
        || (e.ctrlKey && e.keyCode == 69) //ctrl + e
        || (e.ctrlKey && e.keyCode == 70) //ctrl + f
        || (e.ctrlKey && e.keyCode == 72) //ctrl + h
        || (e.ctrlKey && e.keyCode == 74) //ctrl + j
        || (e.ctrlKey && e.keyCode == 80) //ctrl + p
        || (e.ctrlKey && e.keyCode == 82) //ctrl + r
        || (e.ctrlKey && e.keyCode == 85) //ctrl + u
        || (e.ctrlKey && e.keyCode == 83) //ctrl + s
        || (e.ctrlKey && e.keyCode == 76) //ctrl + l
        || (e.ctrlKey && e.shiftKey && e.keyCode == 67) //ctrl + shift + c
        || (e.ctrlKey && e.shiftKey && e.keyCode == 74) //ctrl + shift + j
        ) 
        {
            return PreventEvent.PE_BROWSER;
        }
        
    if (
        (e.ctrlKey && e.keyCode == 86)  //ctrl + v
        || (e.ctrlKey && e.keyCode == 81) //ctrl + q
        || (e.ctrlKey && e.keyCode == 67)  //ctrl + c
        || (e.ctrlKey && e.keyCode == 88)  //ctrl + x
        || (e.keyCode === 123) //F12
        )
        {
            return PreventEvent.PE_COM;
        }
        
    if (
         (e.ctrlKey && e.keyCode == 78) //ctrl + n
        || (e.ctrlKey && e.keyCode == 79) //ctrl + o
        || (e.ctrlKey && e.keyCode == 87) //ctrl + w
        || (e.ctrlKey && e.shiftKey && e.keyCode == 83) //ctrl + shift + s
        )
        {
            return PreventEvent.PE_BROWSER_COM;
        }
                
    return 0;
}
function officeShortcutCheck(e){
	if(userModule=="ToWord"){
		
	}
	else if(userModule=="ToPoint"){
    	if(e.ctrlKey && e.keyCode == 116){
    		//F5 (slide show)
    		globalModKeyFlagRef = EventFlag.EF_NONE; //Ctrl -> NONE //Ctrl+PageUp (stand alone)
            openFullScreenMode();
        }
	}
	else if(userModule=="ToCell"){
		if( (e.ctrlKey && e.keyCode == 76) ){ //Ctrl+L - 표 만들기
			globalKeyRef = 84; //l -> t //Ctrl+T (stand alone)
			return;
		}
		if( (e.ctrlKey && e.shiftKey && e.keyCode == 33) ){ //Ctrl+Shift+PageUp - 다음 워크시트로 이동
			globalModKeyFlagRef = EventFlag.EF_NONE; //Ctrl+Shift -> Ctrl //Ctrl+PageUp (stand alone)
			globalModKeyFlagRef = makeModKeyFlag(EventFlag.EF_LCONTROL_DOWN, globalModKeyFlagRef);
			return;
		}
		if( (e.ctrlKey && e.shiftKey && e.keyCode == 34) ){ //Ctrl+Shift+PageDown - 다음 워크시트로 이동
			globalModKeyFlagRef = EventFlag.EF_NONE; //Ctrl+Shift -> Ctrl //Ctrl+PageDown (stand alone)
			globalModKeyFlagRef = makeModKeyFlag(EventFlag.EF_LCONTROL_DOWN, globalModKeyFlagRef);
			return;
		}
	}
	else if(userModule=="ToHangul"){
		
	}
	else{
		
	}
}

function closeInsertMenu(flag){
	if(flag) {
		parent.window.Top.Dom.selectById('officeInsertFileMenu').close();
		isInsertImageMenu = false;
	}	
}

function keyPressed(e) {
    closeInsertMenu(isInsertImageMenu);

    switch (blockKeyPressed(e)) {
	    case PreventEvent.PE_NONE:
	    	break;
	    case PreventEvent.PE_BROWSER:
	    	e.preventDefault();
	    	break;
	    case PreventEvent.PE_COM:
	    	return;
	    case PreventEvent.PE_BROWSER_COM:
	    	e.preventDefault();
	    	return;
    }
    
	if(os === "MAC"){
		keyPressedForMac(e);
    }else{
    	keyPressedDefault(e);
    }
}

function keyPressedDefault(e){
	//key being pressed도 구현해야함
	//pressed되어있는 키가 있는지 확인하는 자료구조를 만들어야 할듯
	var keyState = KeyboardEvent.KEY_PRESSED;
	var keycode = e.keyCode;
	globalKeyRef = keycode;
	var value;
	var flag;
	var modKeyFlag = EventFlag.EF_NONE;
	var isRepeated = false;
    
    modKeyFlag = updateFlag(e, modKeyFlag);
    globalModKeyFlagRef = modKeyFlag;
    
    if (agent.search(/togate/i) != -1) {
        if(keycode == 165){ //right alt
            keycode = 21;
            modKeyFlag = EventFlag.EF_NONE;
        }
        if(keycode == 163){ //right ctrl
            keycode = 25;
            modKeyFlag = EventFlag.EF_NONE;
        }
        globalKeyRef = keycode;
        globalModKeyFlagRef = modKeyFlag;
    }

    officeShortcutCheck(e)
	keycode = globalKeyRef;
	modKeyFlag = globalModKeyFlagRef;
    
    flag = makeValue(HID_flag.HID_KEY, keyState);
    value = convertKeyCodeJsToLinux(keycode);
    
    isRepeated = e.repeat;
    var inputEventJson = makeInputEventJson(flag, value, modKeyFlag, isRepeated);
    sendDataWebSocket(inputEventJson);	
}

function keyPressedForMac(e){
	//key being pressed도 구현해야함
	//pressed되어있는 키가 있는지 확인하는 자료구조를 만들어야 할듯
	var keyState = KeyboardEvent.KEY_PRESSED;
	var keycode = e.keyCode;
	globalKeyRef = keycode;
	var value;
	var flag;
	var modKeyFlag = EventFlag.EF_NONE;
	var isRepeated = false;
	
	modKeyFlag = updateFlag(e, modKeyFlag);
    globalModKeyFlagRef = modKeyFlag;

    if (agent.search(/togate/i) != -1) {
    	if(keycode == 165){ //right alt
    		keycode = 21;
    		modKeyFlag = EventFlag.EF_NONE;
    	}
    	if(keycode == 163){ //right ctrl
    		keycode = 25;
    		modKeyFlag = EventFlag.EF_NONE;
    	}
    	globalKeyRef = keycode;
    	globalModKeyFlagRef = modKeyFlag;
    }

	flag = makeValue(HID_flag.HID_KEY, keyState);
	
	if(keycode >= 65 && keycode <= 90){
		if(isHangul(e.key) != lang ){
			lang = !lang;
			value = convertKeyCodeJsToLinux(21); //한/영변환 keycode
			var inputEventJson = makeInputEventJson(flag, value, modKeyFlag, isRepeated);
			sendDataWebSocket(inputEventJson);
		}
	}

	//mac일 경우 단축키 처리 테스트 필요.*******
	officeShortcutCheck(e)
	keycode = globalKeyRef;
	modKeyFlag = globalModKeyFlagRef;
	
	keycode = convertMacShortCut(e); //mac용 단축키가 입력되는 경우 변경.

	if(keycode == 21 || keycode == 25) //한영, 한자인경우
		modKeyFlag = 0;
	value = convertKeyCodeJsToLinux(keycode);
	
	isRepeated = e.repeat;
	var inputEventJson = makeInputEventJson(flag, value, modKeyFlag, isRepeated);
	sendDataWebSocket(inputEventJson);
}

function keyUp(e) {
	var keyState = KeyboardEvent.KEY_RELEASED;
    var keycode = e.keyCode;
    var flag;
    var value;
    var modKeyFlag = EventFlag.EF_NONE;
    var isRepeated = false;
   
    modKeyFlag = updateFlag(e, modKeyFlag);

    if (agent.search(/togate/i) != -1) {
        if(keycode == 165){ //right alt
            keycode = 21;
            modKeyFlag = EventFlag.EF_NONE;
        }
        if(keycode == 163){ //right ctrl
            keycode = 25;
            modKeyFlag = EventFlag.EF_NONE;
        }
    }

    flag = makeValue(HID_flag.HID_KEY, keyState);
    value = convertKeyCodeJsToLinux(keycode);
    isRepeated = e.repeat;
    var inputEventJson = makeInputEventJson(flag, value, modKeyFlag, isRepeated);
	sendDataWebSocket(inputEventJson);
}

function wheelMove(e) {
	e.preventDefault();
    var y = e.deltaY;
    // if(y > 0) 아래방향 스크롤 y<0 위방향 스크롤
    flag = makeValue(HID_flag.HID_WHEEL, 1);
    value = y;

    if (agent.indexOf("chrome") != -1) {
        value = value/100;
    }
    else if (agent.indexOf("firefox") != -1) {
        value = value/3;
    }
    else if ( (navigator.appName == 'Netscape' && agent.indexOf('trident') != -1) || (agent.indexOf("msie") != -1)) {
        value = value/106.5;
    }
    else{ //이경우 deltaY 자체를 support 하지 않을 가능성이 높음
        if(value > 0) value = value / value;
        else if(value < 0)value = value/ (-value);
    }
    var modKeyFlag = EventFlag.EF_NONE;
    modKeyFlag= updateFlag(e,modKeyFlag);
    var inputEventJson = makeInputEventJson(flag, value, modKeyFlag);
	sendDataWebSocket(inputEventJson);
}

function mouseMove(e) {
	var flag;
    var value;
    var modKeyFlag = EventFlag.EF_NONE;
    var resizeOffsetX = 0;
    var resizeOffsetY = 0;

    xPosition = e.clientX;
    yPosition = e.clientY;
    
    if(xPosition < 0) xPosition = 0;
    if(yPosition < 0) yPosition = 0;
    if(xPosition > myCanvas.width) xPosition = myCanvas.width;
    if(yPosition > myCanvas.height) yPosition = myCanvas.height;
    
    theThing.style.left = xPosition + "px";
    theThing.style.top = yPosition + "px";
    
    flag = makeValue(HID_flag.HID_POS, 2);
    value = makeValue(xPosition, yPosition);
    modKeyFlag= updateFlag(e,modKeyFlag);
    var inputEventJson = makeInputEventJson(flag, value, modKeyFlag);
	sendDataWebSocket(inputEventJson);
}

// Our main handler
function handleDown(e) {
	var flag;
    var value;
    var modKeyFlag = EventFlag.EF_NONE;
    isRButton = false;
    mouseDown = true;
    modKeyFlag= updateFlag(e,modKeyFlag);
    closeInsertMenu(isInsertImageMenu);
    switch (e.button) {
        case 0: // Primary button ("left")
            flag = makeValue(HID_flag.HID_LEFT, 1);
            value = 1;
            var inputEventJson = makeInputEventJson(flag, value, modKeyFlag);
        	sendDataWebSocket(inputEventJson);
            break;
        case 1:
            flag = makeValue(HID_flag.HID_MIDDLE, 1);
            value = 1;
            var inputEventJson = makeInputEventJson(flag, value, modKeyFlag);
        	sendDataWebSocket(inputEventJson);
            break;
        case 2: // Secondary button ("right")
            flag = makeValue(HID_flag.HID_RIGHT, 1);
            value = 1;
            isRButton = true;
            var inputEventJson = makeInputEventJson(flag, value, modKeyFlag);
        	sendDataWebSocket(inputEventJson);
            break;
    }
}

function handleUp(e) {
	var flag;
    var value;
    var modKeyFlag = EventFlag.EF_NONE;
    modKeyFlag= updateFlag(e,modKeyFlag);
    mouseDown = false;
    switch (e.button) {
        case 0: // Primary button ("left")
            flag = makeValue(HID_flag.HID_LEFT, 1);
            value = 0;
            var inputEventJson = makeInputEventJson(flag, value, modKeyFlag);
        	sendDataWebSocket(inputEventJson);
            break;
        case 1:
            flag = makeValue(HID_flag.HID_MIDDLE, 1);
            value = 0;
            var inputEventJson = makeInputEventJson(flag, value, modKeyFlag);
        	sendDataWebSocket(inputEventJson);
            break;
        case 2: // Secondary button ("right")
            flag = makeValue(HID_flag.HID_RIGHT, 1);
            value = 0;
            var inputEventJson = makeInputEventJson(flag, value, modKeyFlag);
        	sendDataWebSocket(inputEventJson);
            break;
    }
}

//function mouseout(e){
//    var flag;
//    var value;
//    var modKeyFlag = EventFlag.EF_NONE;
//    modKeyFlag = updateFlag(e, modKeyFlag);
//    switch (e.which) {
//        case 1: // Primary button ("left")
//            flag = makeValue(HID_flag.HID_LEFT, 1);
//            value = 0;
//            var comHeader = new ComHeader(ComHeaderType.INPUT_EVENT);
//            var buffer = selectBody(comHeader);
//            var convert = convertIntToByte(buffer, flag, value, modKeyFlag);
//            sendDataWebSocket(buffer);
//            break;
//        case 2:
//            flag = makeValue(HID_flag.HID_MIDDLE, 1);
//            value = 0;
//            var comHeader = new ComHeader(ComHeaderType.INPUT_EVENT);
//            var buffer = selectBody(comHeader);
//            var convert = convertIntToByte(buffer, flag, value, modKeyFlag);
//            sendDataWebSocket(buffer);
//            break;
//        case 3: // Secondary button ("right")
//            flag = makeValue(HID_flag.HID_RIGHT, 1);
//            value = 0;
//            var comHeader = new ComHeader(ComHeaderType.INPUT_EVENT);
//            var buffer = selectBody(comHeader);
//            var convert = convertIntToByte(buffer, flag, value, modKeyFlag);
//            sendDataWebSocket(buffer);
//            break;
//    }
//} 

function startSlideShow() {
	var slidShowJson = ComJson(ComHeaderType.START_SLIDE_SHOW);
	var jsonString = JSON.stringify(slidShowJson) ;
	try { ws.send(jsonString); } catch (e) { return false; };
	setCursor(1);
	return true;
}

function openFullScreenMode() {
    if (document.getElementById('myCanvas').requestFullscreen)
        document.getElementById('myCanvas').requestFullscreen();
    else if (document.getElementById('myCanvas').webkitRequestFullscreen) // Chrome, Safari (webkit)
        document.getElementById('myCanvas').webkitRequestFullscreen();
    else if (document.getElementById('myCanvas').mozRequestFullScreen) // Firefox
        document.getElementById('myCanvas').mozRequestFullScreen();
    else if (document.getElementById('myCanvas').msRequestFullscreen) // IE or Edge
        document.getElementById('myCanvas').msRequestFullscreen();
    
    parent.window.Top.Mobile.requestFullscreen(document.getElementById('myCanvas'));

    document.getElementById('myCanvas').focus();
}

function closeFullScreenMode() {
    if (document.exitFullscreen)
        document.exitFullscreen();
    else if (document.webkitExitFullscreen) // Chrome, Safari (webkit)
        document.webkitExitFullscreen();
    else if (document.mozCancelFullScreen) // Firefox
        document.mozCancelFullScreen();
    else if (document.msExitFullscreen) // IE or Edge
        document.msExitFullscreen();
    
    parent.window.Top.Mobile.exitFullscreen();

    document.getElementById('myCanvas').focus();
}

function exitHandler() {
    if (!document.fullscreenElement && !document.webkitIsFullScreen && !document.mozFullScreen && !document.msFullscreenElement) {
    	 var keyState = KeyboardEvent.KEY_PRESSED;
         var keycode = 27;
     
         flag = makeValue(HID_flag.HID_KEY, keyState);
         value = convertKeyCodeJsToLinux(keycode);
         var inputEventJson = makeInputEventJson(flag, value, 0, 0);
     	sendDataWebSocket(inputEventJson);
    }
}

function visibilityChange()
{
	if(document.hidden){		
		shouldPaint(false);
	}else{
		shouldPaint(true);
	}
}   

function shouldPaint(flag){
	var shouldPaintJson = makeShouldPaintJson(flag) ;
	try { ws.send(shouldPaintJson); } catch (e) { };
}
function focustout()
{
	var focusOutJson = new ComJson(ComHeaderType.FOCUS_OUT);
	var jsonString = JSON.stringify(focusOutJson);
	sendDataWebSocket(jsonString);
}   

function dropFile(e){
    e.preventDefault();

    if(isdraggingFile)
        return;

    if (e.dataTransfer.items) {
        isdraggingFile = true;
        for (var i = 0; i < e.dataTransfer.items.length; i++) {
            if (e.dataTransfer.items[i].kind === 'file') {
                var file = e.dataTransfer.items[i].getAsFile();
                var fileNumber = i + 1;
                var fileType = file.type;
                var fileTypeNum = 0;
                if(fileType.search("image" != -1)){
                	fileTypeNum = InsertFileType.IMAGE_RIBBON;
                }else if(fileType.search("video" != -1)){
                	fileTypeNum = InsertFileType.VIDEO_RIBBON;
                }else{
                	fileTypeNum = InsertFileType.UNDEFINED_TYPE;
                }
                var fileMetaDataJson = MakeFileMetaDataJson(file.name, fileNumber, fileTypeNum);
                ws.send(fileMetaDataJson);
                fileRead(file,fileNumber);
            }
        }
        isdraggingFile = false;
    }
}

function fileRead(file,fileNumber){
    //file Reader 로 array Buffer로 데이터 읽어서 전송
    var fileReader = new FileReader();
    fileReader.readAsArrayBuffer(file);
    fileReader.onload = function(e) {
    var fileData = e.target.result;
    var fileHeader = new ComFileHeader(ComHeaderType.DRAG_FILE_DATA);
    var fileHeaderBuffer = new ArrayBuffer(fileHeader.getLength);
    fileHeader.insertToBuff(fileHeaderBuffer);

    var fileNumBuffer = new ArrayBuffer(2);
    var view = new DataView(fileNumBuffer);
    view.setUint16(0,fileNumber, true);

    var tmp = new Uint8Array(fileHeaderBuffer.byteLength + fileNumBuffer.byteLength);
    tmp.set(new Uint8Array(fileHeaderBuffer), 0);
    tmp.set(new Uint8Array(fileNumBuffer), fileHeaderBuffer.byteLength);
    fileHeaderBuffer = tmp.buffer;
    var fileResultBuffer = _appendBuffer(fileHeaderBuffer, fileData);
    console.log(fileResultBuffer);
    //sendDataWebSocket(fileResultBuffer);
    ws.send(fileResultBuffer);
    };
}
function getInsertImageConfig() {
    var filteringFileExtensionList = [];
    filteringFileExtensionList = filteringFileExtensionList
                                   .concat(insertImageConfig.extension.dir)
                                   .concat(insertImageConfig.extension.picture);
    
    return filteringFileExtensionList;
}

function getInsertVideoConfig() {
    var filteringFileExtensionList = [];
    filteringFileExtensionList = filteringFileExtensionList
                                   .concat(insertVideoConfig.extension.dir)
                                   .concat(insertVideoConfig.extension.video);
    
    return filteringFileExtensionList;
}

function getInsertAudioConfig() {
    var filteringFileExtensionList = [];
    filteringFileExtensionList = filteringFileExtensionList
                                   .concat(insertAudioConfig.extension.dir)
                                   .concat(insertAudioConfig.extension.audio);
    
    return filteringFileExtensionList;
}

function openImageDialogFromPC() {
	let fileChooser = parent.window.Top.Device.FileChooser.create({
	onBeforeLoad : function(file){
	var fileReader = new FileReader();
	fileReader.onload = function(e) {
	    var fileMetaDataJson = MakeFileMetaDataJson(file.name, 1, insertFileType);
	    ws.send(fileMetaDataJson);
	    fileRead(file,1);
	    insertFileType = 0;
	};
	fileReader.readAsArrayBuffer(file);
	return true; 
	},
	charset : "euc-kr",
	   onFileChoose : function (fileChooser) {        
	   }
	});
	fileChooser.show();
}

function insertImage(fileMeta)     //COM에 이미지 데이터 전송
{
	for(var i=0; i< fileMeta.length; i++){
		var image_id =  fileMeta[i].storageFileInfo.file_id;
		var image_name = fileMeta[i].storageFileInfo.file_name;
		var image_extension = fileMeta[i].storageFileInfo.file_extension;
		var image_path = "/" + image_id + "/"+ image_name + "."+ image_extension;
		var insertFileJson = makeInsertFileJson(insertImageUserId, userModule, image_path, insertImageUserName, insertImageWsID, insertFileType);
		try { ws.send(insertFileJson); } catch (e) { };			
	}
	
	insertImageUserId = "";
	insertImageUserName = "";
	insertImageWsID = "";
	insertFileType = 0;
}

function mobilePan(e){
	if(isPress){
		/*prev mouse Event*/
		var prevEventflag;
		var prevEventValue;
		var prevEventmodKeyFlag = EventFlag.EF_NONE;
		prevEventmodKeyFlag = makeModKeyFlag(EventFlag.EF_LBUTTON_DOWN, prevEventmodKeyFlag);
		xPosition = e.center.x;
		yPosition = e.center.y;
		
		theThing.style.left = xPosition + "px";
		theThing.style.top = yPosition + "px";
		prevEventflag = makeValue(HID_flag.HID_POS, 2);
		prevEventValue = makeValue(xPosition, yPosition);
        var inputEventJson = makeInputEventJson(prevEventflag, prevEventValue,prevEventmodKeyFlag);
    	sendDataWebSocket(inputEventJson);
		/* -- end --*/
		if(e.type.search(/panend/) != -1){
			//mouse release event
			isPress = false;
			var flag;
		    var value;
		    var modKeyFlag = EventFlag.EF_NONE;
		    flag = makeValue(HID_flag.HID_LEFT, 1);
		    value = 0;
            var inputEventJson = makeInputEventJson(flag, value,modKeyFlag);
        	sendDataWebSocket(inputEventJson);
		}
		return;
	}
	
	e.preventDefault();
	if(e.type.search(/panstart/) != -1){
		mobilePrevMouseMoveEvent(e);
		globalPan = 0;
	}

	globalPan++;
	if(globalPan < 5){
		return;
	}
	
	globalPan = 0;
	flag = makeValue(HID_flag.HID_WHEEL, 1);
	var modKeyFlag = EventFlag.EF_NONE;
	switch(e.offsetDirection){
		case 2: //left shift + wheel
			value = 1;
			modKeyFlag = makeModKeyFlag(EventFlag.EF_LSHIFT_DOWN, modKeyFlag);
			break;
		case 4: //right shift + wheel
			modKeyFlag = makeModKeyFlag(EventFlag.EF_LSHIFT_DOWN, modKeyFlag);
			value = -1;
			break;
		case 8: //up wheel up
			value = 1;
			break;
		case 16: //down wheel down
			value = -1;
			break;
		default: return;
	}
    var inputEventJson = makeInputEventJson(flag, value,modKeyFlag);
	sendDataWebSocket(inputEventJson);
}
function mobilePinch(e){
	e.preventDefault();
	if(e.type.search(/pinchstart/) != -1){
		mobilePrevMouseMoveEvent(e);
		globalZoom = 0;
		prevDistance = Math.pow((e.pointers[1].clientX - e.pointers[0].clientX) ,2) + Math.pow((e.pointers[1].clientY - e.pointers[0].clientY) ,2);
	}

	globalZoom++;
	if(globalZoom < 15){
		return;
	}
	globalZoom = 0;
	var pinchType = 0;
	
	var curdistance = Math.pow((e.pointers[1].clientX - e.pointers[0].clientX) ,2) + Math.pow((e.pointers[1].clientY - e.pointers[0].clientY) ,2);
		//((e.pointers[1].clientX - e.pointers[0].clientX) ** 2) + ((e.pointers[1].clientY - e.pointers[0].clientY) ** 2);
	var pinchType = prevDistance > curdistance ? true : false; //in : out
	prevDistance = curdistance;
	
	if(pinchType){
		pinchType = 1;
	}else{
		pinchType = 2;		
	}
	/*
	if(e.additionalEvent.search(/pinchin/) != -1){
		if(e.distance > prevDistance){
			//console.log("in");
			pinchType = 1;
		}else if(e.distance < prevDistance){
			//console.log("out");
			pinchType = 2;
		} 
	}
	else if(e.additionalEvent.search(/pinchout/) != -1){
		if(e.distance > prevDistance){
			//console.log("out");
			pinchType = 2;
		}else if(e.distance < prevDistance){
			//console.log("in");
			pinchType = 1;
		}
	}
	else{
		return;
	}
	prevDistance = e.distance;
	 */
	var mobilePichjson = makeMobilePinchJson(e.center.x,e.center.y,e.deltaX,e.deltaY,e.velocityX,e.velocityY,pinchType);
	sendDataWebSocket(mobilePichjson);
}

function mobilePress(e){
	mobilePrevMouseMoveEvent(e);
	
	if(e.type.search(/pressup/) != -1){
		isPress = false;
		//handleup event
		var flag;
    	var value;
		var modKeyFlag = EventFlag.EF_NONE;
		flag = makeValue(HID_flag.HID_LEFT, 1);
		value = 0;
		var inputEventJson = makeInputEventJson(flag, value, modKeyFlag);
		sendDataWebSocket(inputEventJson);
		return;
	}else{
		isPress = true;	
		//handledown event
		var flag;
		var value;
		var modKeyFlag = EventFlag.EF_NONE;
		flag = makeValue(HID_flag.HID_LEFT, 1);
		value = 1;
		var inputEventJson = makeInputEventJson(flag, value, modKeyFlag);
		sendDataWebSocket(inputEventJson);		
	}
}

function mobilePrevMouseMoveEvent(e){
	/*prev mouse Event*/
	var prevEventflag;
	var prevEventValue;
	var prevEventmodKeyFlag = EventFlag.EF_NONE;
	
	xPosition = e.center.x;
	yPosition = e.center.y;
	
	theThing.style.left = xPosition + "px";
	theThing.style.top = yPosition + "px";
	prevEventflag = makeValue(HID_flag.HID_POS, 2);
	prevEventValue = makeValue(xPosition, yPosition);
    var inputEventJson = makeInputEventJson(prevEventflag, prevEventValue,prevEventmodKeyFlag);
	sendDataWebSocket(inputEventJson);
	/* -- end --*/
}

function mobileDbTab(e){
	e.preventDefault();
//	console.log("DB tab");
//	console.log(e);
}

var Base64 = {
	    // private property
	    _keyStr : "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
	 
	    // public method for decoding
	    decode : function (input) {
	        var output = "";
	        var chr1, chr2, chr3;
	        var enc1, enc2, enc3, enc4;
	        var i = 0;
	 
	        input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
	 
	        while (i < input.length) {
	 
	            enc1 = this._keyStr.indexOf(input.charAt(i++));
	            enc2 = this._keyStr.indexOf(input.charAt(i++));
	            enc3 = this._keyStr.indexOf(input.charAt(i++));
	            enc4 = this._keyStr.indexOf(input.charAt(i++));
	 
	            chr1 = (enc1 << 2) | (enc2 >> 4);
	            chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
	            chr3 = ((enc3 & 3) << 6) | enc4;
	 
	            output = output + String.fromCharCode(chr1);
	 
	            if (enc3 != 64) {
	                output = output + String.fromCharCode(chr2);
	            }
	            if (enc4 != 64) {
	                output = output + String.fromCharCode(chr3);
	            }
	 
	        }
	 
	        output = Base64._utf8_decode(output);
	 
	        return output;
	 
	    },
	 
	    // private method for UTF-8 decoding
	    _utf8_decode : function (utftext) {
	        var string = "";
	        var i = 0;
	        var c = c1 = c2 = 0;
	 
	        while ( i < utftext.length ) {
	 
	            c = utftext.charCodeAt(i);
	 
	            if (c < 128) {
	                string += String.fromCharCode(c);
	                i++;
	            }
	            else if((c > 191) && (c < 224)) {
	                c2 = utftext.charCodeAt(i+1);
	                string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
	                i += 2;
	            }
	            else {
	                c2 = utftext.charCodeAt(i+1);
	                c3 = utftext.charCodeAt(i+2);
	                string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
	                i += 3;
	            }
	 
	        }
	 
	        return string;
	    }
	}

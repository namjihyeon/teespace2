let officeConnect = {
    data: {
        numOfCreatePeer: 0,
        maxNumOfCreatePeer: 10,
        sdpMidFlag: false
    },

    // 19.11.21 os3-5 소형준 수정
    routToConferenceRoom: function () {
        // iOS앱에서는 webrtc를 지원하지 않기 때문에 새롭게 브라우저를 띄워서 회의실로 라우팅해줘야 함.
        const spaceUrl = routeManager.getWorkspaceUrl() ? routeManager.getWorkspaceUrl() : null,
            roomUrl = routeManager.getRoomUrl() ? routeManager.getRoomUrl() : null,
            roomId = conferenceManager.data.currentRoomInfo.room_id ? conferenceManager.data.currentRoomInfo.room_id : null,
            userId = userManager.getLoginUserId() ? userManager.getLoginUserId() : null,
            token = getCookie('token') ? getCookie('token') : null,
            path = 'https://' + window.location.href.split('#!')[0].split('://')[1] + '#!/conferenceFromiOS/' + userId + '?token=' + token + '&spaceUrl=' + spaceUrl + '&roomUrl=' + roomUrl + '&conferenceRoomId=' + roomId;
            
            
        // 새 브라우저를 띄우고 회의실로 라우팅.
        Top.LocalService.acall({
            service: 'tos.service.WebBrowser.loadUrl',
            data: path
        });
    },

    createPeer: function (roomId, userId) {
        return new Promise(function (resolve, reject) {

            let ufrag;
            let pwd;
            let offerOption;

            conferenceService.signaling.getGatewayPublicKey().then(function (response) {
                let answer_hash = response.data.dto.publickey;

                // let answer_port = _workspace.gatewayURL.split(":")[1] == undefined ? 19093 : _workspace.gatewayURL.split(":")[1];
                let answer_port = 19093;

                let pc = new RTCPeerConnection();

                offerOption = {
                    offerToReceiveAudio: 1,
                    offerToReceiveVideo: 1
                };

                officeConnect.certificateOffer(pc, offerOption, roomId, 0).then(function(offer) {
                    offer.sdp.split("\n").forEach(function(line) {
                        if (line.includes("ufrag:")) {
                            ufrag = line.split("ufrag:")[1];
                            ufrag = ufrag.replace(/\n/g, "");
                            ufrag = ufrag.replace(/\r/g, "");
                        }
                        if (line.includes("pwd:")) {
                            pwd = line.split("pwd:")[1];
                            pwd = pwd.replace(/\n/g, "");
                            pwd = pwd.replace(/\r/g, "");
                        }
                    });

                    // togate 대응
                    if (offer.sdp.indexOf("a=mid:0") < 0) {
                        officeConnect.data.sdpMidFlag = true;
                    }
                    if (Top.Util.Browser.isSafari()) {
                        offer.sdp = offer.sdp.replace(offer.sdp.substring(offer.sdp.indexOf('SAVPF', offer.sdp.indexOf('m=video')) + 6, offer.sdp.indexOf('\r', offer.sdp.indexOf('m=video'))), '96 97');
                        offer.sdp = offer.sdp.replace(offer.sdp.substring(offer.sdp.indexOf('a=rtpmap:96 H264/90000'), offer.sdp.indexOf("a=ssrc-group:FID")),
                            `a=rtpmap:96 VP8/90000\r\na=rtcp-fb:96 goog-remb\r\na=rtcp-fb:96 transport-cc\r\na=rtcp-fb:96 ccm fir\r\na=rtcp-fb:96 nack\r\na=rtcp-fb:96 nackpli\r\na=rtpmap:97 rtx/90000\r\na=fmtp:97 apt=96\r\n`
                        );
                    }

                    const ext = new ExtractorFromOffer(offer);
                    pc.setLocalDescription(ext.replaceMid()._offer);
                    pc.addEventListener('icecandidate', function(e){ officeConnect.onIceCandidate(e, roomId, userId, ufrag, pwd, answer_port) });
                    conferenceService.signaling.officeIceCandidateUpdate(roomId, userId, ufrag, pwd);
                    pc.addEventListener('track', function(e) { officeConnect.gotRemoteStream(userId, e) });
                    officeConnect.answerSubmit(userId, answer_hash, answer_port, pc);

                    pc.addEventListener('iceconnectionstatechange', function(e) { officeConnect.onIceStateChange(pc, roomId, userId, e) });
                    pc.addEventListener('connectionstatechange', function(e) { officeConnect.onConnectionStateChange(pc, roomId, userId, e) });

                    officePeer.setPeerConnection(userId, pc);
                    resolve();
                })
            })
        });
    },

    onIceCandidate: function (event, roomId, userId, ufrag, pwd, gatewayPort) {
        // try {
        //   //console.log(event.candidate)
        //   if (event.candidate !== null) {
        //     if (event.candidate.protocol === "udp" && !event.candidate.address.includes("169.254") && !event.candidate.address.includes(":") && event.candidate.address.split(".")[3] !== "1") {
        //       let inputData = {
        //         "dto": {
        //           "room_id": roomId,
        //           "user_id": conferenceService.data.myInfo.userId,
        //           "target_user_id": userId,
        //           "ufrag": ufrag,
        //           "pwd": pwd,
        //           "ice_ip": String(event.candidate.address),
        //           "ice_port": String(event.candidate.port),
        //           "gateway_port": gatewayPort,
        //           //"timestamp": new Date().getTime().toString()
        //           "timestamp": event.candidate.priority.toString()
        //         }
        //       };

        //       conferenceService.signaling.iceCandidateUpdate(inputData);
        //       console.log(event.candidate, userId)
        //     }
        //   }

        // } catch (event) {
        //   console.error(event);
        // }
    },

    onIceStateChange: function (pc, roomId, userId, event) {
        if (pc) {
            console.log('ICE state: ' + pc.iceConnectionState + '\n' + userId);
            //console.log('ICE state change event: ', event);
        }
    },

    onIceGatheringStateChange: function (pc, userId, event) {
        if (pc) {
            console.log('ICE gathering state: ' + pc.iceGatheringState + '\n' + userId);

            if (pc.iceGatheringState) {

            }
        }
    },

    onConnectionStateChange: function (pc, roomId, userId, event) {
        if (pc) {
            console.log('connection state: ' + pc.connectionState + '\n' + userId);
            //console.log('connection state change event:', event)

            if (pc.connectionState == 'connected') {
//            	if (Top.Dom.selectById("officeEditWebView")) {
//                    Top.Dom.selectById("officeEditWebView").getElement().contentWindow.gatewayConnected();
//                }
//            	else if (Top.Dom.selectById("WebViewer")) {
//            		Top.Dom.selectById("WebViewer").getElement().contentWindow.gatewayConnected();
//            	}
            }
            // TODO(jaehyeon_park2, 200109) : 재접속 로직 고려됨. 
            if (pc.connectionState == 'failed') {
                if (Top.Dom.selectById("officeEditWebView")) {
                	parent.window.alertGateWayUnstableConnection(101);
                    Top.Dom.selectById("officeEditWebView").getElement().contentWindow.goBack();
                    officePeer.clearAllPeerConnection();
                }
                
                if (Top.Dom.selectById("WebViewer")) {
                	parent.window.alertGateWayUnstableConnection(101);
                    Top.Dom.selectById("WebViewer").getElement().contentWindow.goBack();
                    officePeer.clearAllPeerConnection();
                }
            }
        }
    },

    gotRemoteStream: function (userId, e) {
        console.log('got remote track');
//        $('#mask, #loadingImg, #loadingMsg').hide(); 
//        $('#mask, #loadingImg, #loadingMsg').remove();

        if (e.track.kind === "audio") {
            /*
            오디오 트랙은 스킵
            */
        } else if (e.track.kind === "video") {
            officePeer.setVideoTrack(userId, e.track);
            const fileStream = new MediaStream([e.track]);
            document.querySelectorAll('iframe#officeEditWebView').forEach(function(item) {
                if (item.contentWindow.document.body.querySelector('#officeVideo')) {
                    item.contentWindow.document.body.querySelector('#officeVideo').srcObject = fileStream;
    
                    processor.doLoad();
                }
            });
            document.querySelectorAll('iframe#WebViewer').forEach(function(item) {
                let targetVideo = item.contentWindow.document.body.querySelector('#officeVideo');
                if ( targetVideo ) {
                    targetVideo.srcObject=fileStream;
                    let targetCanvas = item.contentWindow.document.body.querySelector('#myCanvas');


                    processor.doLoad({ "video" : targetVideo, "canvas" : targetCanvas });
                }
            });
            //e.track.onmute = function() {}
            
        }
    },

    certificateOffer: function (pc, offerOption, roomId, trial) {
        return new Promise(function (resolve, reject) {

            if (Top.Util.Browser.isSafari()) {
                if (offerOption.offerToReceiveAudio === 1) {
                    pc.addTransceiver("audio");
                }
                if (offerOption.offerToReceiveVideo === 1) {
                    pc.addTransceiver("video");
                }
                pc.createOffer().then(function (offer) {
                    if (trial < 5) {
                        let ufrag;
                        offer.sdp.split("\n").forEach(function (line) {
                            if (line.includes("ufrag:")) {
                                ufrag = line.split("ufrag:")[1];
                                ufrag = ufrag.replace(/\n/g, "");
                                ufrag = ufrag.replace(/\r/g, "");
                            }
                        })

                        conferenceService.signaling.ufragUniqueCheck(roomId, ufrag).then(function (response) {
                            // console.log(response.data.dto)
                            if (response.data.dto.is_unique === "true") {
                                resolve(offer);
                            } else {
                                trial += 1;
                                resolve(conferenceManager.certificateOffer(pc, offerOption, roomId, trial));
                            }
                        })
                    } else {
                        // console.log("fail to create offer")
                        reject();
                    }
                })

            } else {
                // console.log(trial)
                pc.createOffer(offerOption).then(function (offer) {
                    if (trial < 5) {
                        let ufrag;
                        offer.sdp.split("\n").forEach(function (line) {
                            if (line.includes("ufrag:")) {
                                ufrag = line.split("ufrag:")[1];
                                ufrag = ufrag.replace(/\n/g, "");
                                ufrag = ufrag.replace(/\r/g, "");
                            }
                        })

                        conferenceService.signaling.ufragUniqueCheck(roomId, ufrag).then(function (response) {
                            // console.log(response.data.dto)
                            if (response.data.dto.is_unique === "true") {
                                resolve(offer);
                            } else {
                                trial += 1;
                                resolve(conferenceManager.certificateOffer(pc, offerOption, roomId, trial));
                            }
                        })
                    } else {
                        // console.log("fail to create offer")
                        reject();
                    }
                })
            }
        })
    },

    answerSubmit: function (userId, answer_hash, answer_port, pc) {
        const isTogate = officeConnect.data.sdpMidFlag;

        const answer_ice_pwd = 'DwD9OYidnTwKdRCM3068ZGsU'
        const answer_msid = 'siN5Lj96LGAbjitb6MlbSE5srxhg8jr4m875'

        conferenceService.signaling.ssrcGet(userId).then(function (response) {
            if (!response.data.dto.ssrc_video_2) {
                response.data.dto.ssrc_video_2 = response.data.dto.ssrc_video_1;
            }

            let answerSDPGenerator =
                new AnswerSDPGenerator(setBrowsercode(), "consumer", isTogate)
                .replaceSSRC(response.data.dto)
                .replaceHashValue(answer_hash)
                .replaceICEPwd(answer_ice_pwd)
                .replaceMsid(answer_msid)
                .replaceMid()
                .replaceSendRecv();

            pc.setRemoteDescription(answerSDPGenerator.getAnswer());
            console.log("set remote description ", userId);

            pc.addIceCandidate(answerSDPGenerator.generateGatewayICECandidateForOffice());
        })
    },
}

window.officePeer = officePeer = {
    data: [],
    roomId: null,

    setRoomId: function(roomId) {
        officePeer.roomId = roomId;
    },

    getRoomId: function() {
        return officePeer.roomId;
    },

    setPeerConnection: function (userId, pc) {
        officePeer.data.push({
            "id": userId,
            "pc": pc
        })
    },

    getPeerConnection: function (userId) {
        return officePeer.data.find(function(peer) { return peer.id === userId});
    },

    clearAllPeerConnection: function () {
        officeConnect.data.numOfCreatePeer = 0;

        officePeer.data.forEach(function(peer) {
            peer.pc.close();
            peer.pc = null;
        })
        officePeer.data = [];
    },

    clearPeerConnection: function (userId) {
        return new Promise(function (resolve, reject) {
            let peer = officePeer.data.find(function(peer) { return peer.id === userId});
            if(peer) {
                peer.pc.close();
                let idx = officePeer.data.indexOf(peer);
                peer.pc = null;
                officePeer.data.splice(idx, 1);
            }
            resolve();
        })
    },

    setVideoTrack: function (userId, video) {
        officePeer.data.find(function(peer) { return peer.id === userId}).videoTrack = video;
    },

    setAudioTrack: function (userId, audio) {
        officePeer.data.find(function(peer) { return peer.id === userId}).audioTrack = audio;
    },

    setStream: function (userId, stream) {
        officePeer.setVideoTrack(userId, stream.getVideoTracks()[0]);
        officePeer.setAudioTrack(userId, stream.getAudioTracks()[0]);
    },

    getVideoTrack: function (userId) {
        return officePeer.data.find(function(peer) { return peer.id === userId}).videoTrack
    },

    getAudioTrack: function (userId) {
        return officePeer.data.find(function(peer) { return peer.id === userId}).audioTrack
    }
}

let processor = {
    doLoad: function() {
        let self = this;
        document.querySelectorAll('iframe').forEach(function(item) {
            if (item.contentWindow.document.body.querySelector('#officeVideo')) {
                self.video = item.contentWindow.document.body.querySelector('#officeVideo');
            }
            if (item.contentWindow.document.body.querySelector('#myCanvas')) {
                self.c1 = item.contentWindow.document.body.querySelector('#myCanvas');
            }
        });
        this.ctx1 = this.c1.getContext('2d');
        this.timerId = setInterval(self.reqImage, 300); //300ms 마다 이미지 요청

        this.video.play().then( function(){
            clearInterval(self.timerId);
            console.log("stop: request image!");
            
            self.width = self.video.videoWidth;
            self.height = self.video.videoHeight;
            self.timerCallback();
            if ( Top.Dom.selectById( "officeEditWebView" ) ) {
                Top.Dom.selectById( "officeEditWebView" ).getElement().contentWindow.hideLoadingBar();

                // pk1-3팀 정현수 연구원 수정 [2019.11.19]
                if ( Top.Dom.selectById( "officeEditWebView" ).getElement().contentWindow.isBinary === true ) {
                    Top.Dom.selectById( "officeEditWebView" ).getElement().contentWindow.parent.window.alertBinarySpecDocument( Top.Dom.selectById( "officeEditWebView" ).getElement().contentWindow.ws );
                }
            }
            if ( Top.Dom.selectById( "WebViewer" ) ) {
                Top.Dom.selectById( "WebViewer" ).getElement().contentWindow.hideLoadingBar();
            }
      }, false);
    },

    reqImage: function () {
        if ( Top.Dom.selectById( "officeEditWebView" ) ) {
            try {
                Top.Dom.selectById( "officeEditWebView" ).getElement().contentWindow.requestSchedulePaint();
            } catch(e){}
            //console.log("officeEditWebView: ", "request image!");
        }
        if ( Top.Dom.selectById( "WebViewer" ) ) {
            try {
                Top.Dom.selectById( "WebViewer" ).getElement().contentWindow.requestSchedulePaint();
            } catch(e){}
            //console.log("WebView: ", "request image!");
        }
    },

    timerCallback: function() {
      if (this.video.paused || this.video.ended) {
        return;
      }
      this.computeFrame();
      let self = this;
      setTimeout(function () {
        self.timerCallback();
      }, 33);
    },
  
    computeFrame: function() {
      this.ctx1.drawImage(this.video, 0, 0);
      return;
    }
  }
  

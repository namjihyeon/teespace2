var serviceWorkerRegistration;
if ('serviceWorker' in navigator) {
	window.addEventListener('load', function() {
		navigator.serviceWorker.register('./service_worker.js')
		.then(function (registration) {
			serviceWorkerRegistration = registration;
			if (registration.installing) {
				console.log(`serviceWorker: installing`);
			} else if (registration.waiting) {
				console.log(`serviceWorker: waiting`);
			} else if (registration.active) {
				console.log(`serviceWorker: activate`);
			}
		}).catch (function (error) {
			console.log(`serviceWorkerError: ${error}`);
		})
	})
}
!function(e) {
    var t = {};
    function i(n) {
        if (t[n])
            return t[n].exports;
        var o = t[n] = {
            i: n,
            l: !1,
            exports: {}
        };
        return e[n].call(o.exports, o, o.exports, i),
        o.l = !0,
        o.exports
    }
    i.m = e,
    i.c = t,
    i.d = function(e, t, n) {
        i.o(e, t) || Object.defineProperty(e, t, {
            enumerable: !0,
            get: n
        })
    }
    ,
    i.r = function(e) {
        "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(e, Symbol.toStringTag, {
            value: "Module"
        }),
        Object.defineProperty(e, "__esModule", {
            value: !0
        })
    }
    ,
    i.t = function(e, t) {
        if (1 & t && (e = i(e)),
        8 & t)
            return e;
        if (4 & t && "object" == typeof e && e && e.__esModule)
            return e;
        var n = Object.create(null);
        if (i.r(n),
        Object.defineProperty(n, "default", {
            enumerable: !0,
            value: e
        }),
        2 & t && "string" != typeof e)
            for (var o in e)
                i.d(n, o, function(t) {
                    return e[t]
                }
                .bind(null, o));
        return n
    }
    ,
    i.n = function(e) {
        var t = e && e.__esModule ? function() {
            return e.default
        }
        : function() {
            return e
        }
        ;
        return i.d(t, "a", t),
        t
    }
    ,
    i.o = function(e, t) {
        return Object.prototype.hasOwnProperty.call(e, t)
    }
    ,
    i.p = "",
    i(i.s = "XJR1")
}({
    "0A7J": function(module, __webpack_exports__, __webpack_require__) {
        "use strict";
        var _modules_colorPicker__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("EjF6");
        __webpack_exports__.a = {
            name: "fontColor",
            add: function(core, targetElement) {
                core.addModule([_modules_colorPicker__WEBPACK_IMPORTED_MODULE_0__.a]);
                const context = core.context;
                context.fontColor = {
                    previewEl: null,
                    colorInput: null,
                    colorList: null
                };
                let listDiv = eval(this.setSubmenu.call(core));
                context.fontColor.colorInput = listDiv.getElementsByClassName("sun-editor-id-submenu-color-input")[0],
                context.fontColor.colorInput.addEventListener("keyup", this.onChangeInput.bind(core)),
                listDiv.getElementsByClassName("sun-editor-id-submenu-color-submit")[0].addEventListener("click", this.submit.bind(core)),
                listDiv.getElementsByClassName("sun-editor-id-submenu-color-default")[0].addEventListener("click", this.remove.bind(core)),
                listDiv.getElementsByTagName("UL")[0].addEventListener("click", this.pickup.bind(core)),
                context.fontColor.colorList = listDiv.getElementsByTagName("UL")[0].querySelectorAll("li button"),
                targetElement.parentNode.appendChild(listDiv),
                listDiv = null
            },
            setSubmenu: function() {
                const e = this.context.colorPicker.colorListHTML
                  , t = this.util.createElement("DIV");
                return t.className = "sun-editor-submenu layer_editor",
                t.style.display = "none",
                t.innerHTML = e,
                t
            },
            on: function() {
                const e = this.context.colorPicker
                  , t = this.context.fontColor;
                e._colorInput = t.colorInput,
                e._defaultColor = "#333333",
                e._styleProperty = "color",
                e._colorList = t.colorList,
                this.plugins.colorPicker.init.call(this, this.getSelectionNode(), null)
            },
            onChangeInput: function(e) {
                const t = e.target.value.trim();
                this.plugins.colorPicker.setCurrentColor.call(this, t ? "#" + t : "")
            },
            remove: function() {
                this.nodeChange(null, ["color"]),
                this.submenuOff(),
                this.focus()
            },
            submit: function() {
                this.plugins.fontColor.applyColor.call(this, this.context.colorPicker._currentColor)
            },
            pickup: function(e) {
                e.preventDefault(),
                e.stopPropagation(),
                this.plugins.fontColor.applyColor.call(this, e.target.getAttribute("data-value"))
            },
            applyColor: function(e) {
                if (!e)
                    return;
                const t = this.util.createElement("SPAN");
                t.style.color = e,
                this.nodeChange(t, ["color"]),
                this.submenuOff(),
                this.focus()
            }
        }
    },
    "1kvd": function(e, t, i) {
        "use strict";
        t.a = {
            name: "dialog",
            add: function(e) {
                const t = e.context;
                t.dialog = {};
                let i = e.util.createElement("DIV");
                i.className = "sun-editor-id-dialogBox sun-editor-common";
                let n = e.util.createElement("DIV");
                n.className = "modal-dialog-background sun-editor-id-dialog-back",
                n.style.display = "none";
                let o = e.util.createElement("DIV");
                o.className = "modal-dialog sun-editor-id-dialog-modal",
                o.style.display = "none",
                i.appendChild(n),
                i.appendChild(o),
                t.dialog.modalArea = i,
                t.dialog.back = n,
                t.dialog.modal = o,
                t.dialog.modal.addEventListener("click", this.onClick_dialog.bind(e)),
                t.element.relative.appendChild(i),
                i = null,
                n = null,
                o = null
            },
            onClick_dialog: function(e) {
                e.stopPropagation(),
                (/modal-dialog/.test(e.target.className) || /close/.test(e.target.getAttribute("data-command"))) && this.plugins.dialog.close.call(this)
            },
            open: function(e, t) {
                if (this.modalForm)
                    return !1;
                this.context.dialog.updateModal = t,
                "full" === this.context.option.popupDisplay ? this.context.dialog.modalArea.style.position = "fixed" : this.context.dialog.modalArea.style.position = "absolute",
                this.context.dialog.kind = e,
                this.modalForm = this.context[e].modal;
                const i = this.context[e].focusElement;
                this.context.dialog.modalArea.style.display = "block",
                this.context.dialog.back.style.display = "block",
                this.context.dialog.modal.style.display = "block",
                this.modalForm.style.display = "block",
                i && i.focus()
            },
            close: function() {
                this.modalForm.style.display = "none",
                this.context.dialog.back.style.display = "none",
                this.context.dialog.modalArea.style.display = "none",
                this.modalForm = null,
                this.context.dialog.updateModal = !1,
                this.plugins[this.context.dialog.kind].init.call(this)
            }
        }
    },
    "3FqI": function(e, t, i) {},
    "50IV": function(module, __webpack_exports__, __webpack_require__) {
        "use strict";
        __webpack_exports__.a = {
            name: "font",
            add: function(core, targetElement) {
                const context = core.context;
                context.font = {
                    _fontList: [],
                    currentFont: ""
                };
                let listDiv = eval(this.setSubmenu.call(core));
                listDiv.getElementsByClassName("list_family")[0].addEventListener("click", this.pickup.bind(core)),
                context.font._fontList = listDiv.getElementsByTagName("UL")[0].querySelectorAll("li button"),
                targetElement.parentNode.appendChild(listDiv),
                listDiv = null
            },
            setSubmenu: function() {
                const e = this.context.option
                  , t = this.lang
                  , i = this.util.createElement("DIV");
                let n, o, l, s;
                i.className = "layer_editor",
                i.style.display = "none";
                let a = e.font ? e.font : ["Arial", "Comic Sans MS", "Courier New", "Impact", "Georgia", "tahoma", "Trebuchet MS", "Verdana"]
                  , r = '<div class="sun-editor-submenu inner_layer list_family">   <ul class="list_editor">       <li><button type="button" class="default_value btn_edit" title="' + t.toolbar.default + '">(' + t.toolbar.default + ")</button></li>";
                for (l = 0,
                s = a.length; l < s; l++)
                    o = (n = a[l]).split(",")[0],
                    r += '<li><button type="button" class="btn_edit" data-value="' + n + '" data-txt="' + o + '" title="' + o + '" style="font-family:' + n + ';">' + o + "</button></li>";
                return r += "   </ul>",
                r += "</div>",
                i.innerHTML = r,
                i
            },
            on: function() {
                const e = this.context.font
                  , t = e._fontList
                  , i = this.commandMap.FONT.getAttribute("title") || "";
                if (i !== e.currentFont) {
                    for (let e = 0, n = t.length; e < n; e++)
                        i === t[e].getAttribute("data-value") ? this.util.addClass(t[e], "on") : this.util.removeClass(t[e], "on");
                    e.currentFont = i
                }
            },
            pickup: function(e) {
                if (!/^BUTTON$/i.test(e.target.tagName))
                    return !1;
                e.preventDefault(),
                e.stopPropagation();
                const t = e.target.getAttribute("data-value");
                if (t) {
                    const e = this.util.createElement("SPAN");
                    e.style.fontFamily = t,
                    this.nodeChange(e, ["font-family"])
                } else
                    this.nodeChange(null, ["font-family"]);
                this.submenuOff(),
                this.focus()
            }
        }
    },
    EjF6: function(e, t, i) {
        "use strict";
        t.a = {
            name: "colorPicker",
            add: function(e) {
                const t = e.context;
                t.colorPicker = {
                    colorListHTML: "",
                    _colorInput: "",
                    _defaultColor: "#000",
                    _styleProperty: "color",
                    _currentColor: "",
                    _colorList: []
                };
                let i = this.createColorList(e.context.option, e.lang);
                t.colorPicker.colorListHTML = i,
                i = null
            },
            createColorList: function(e, t) {
                const i = e.colorList ? e.colorList : ["#ff0000", "#ff5e00", "#ffe400", "#abf200", "#00d8ff", "#0055ff", "#6600ff", "#ff00dd", "#000000", "#ffd8d8", "#fae0d4", "#faf4c0", "#e4f7ba", "#d4f4fa", "#d9e5ff", "#e8d9ff", "#ffd9fa", "#f1f1f1", "#ffa7a7", "#ffc19e", "#faed7d", "#cef279", "#b2ebf4", "#b2ccff", "#d1b2ff", "#ffb2f5", "#bdbdbd", "#f15f5f", "#f29661", "#e5d85c", "#bce55c", "#5cd1e5", "#6699ff", "#a366ff", "#f261df", "#8c8c8c", "#980000", "#993800", "#998a00", "#6b9900", "#008299", "#003399", "#3d0099", "#990085", "#353535", "#670000", "#662500", "#665c00", "#476600", "#005766", "#002266", "#290066", "#660058", "#222222"];
                let n = '<div class="inner_layer"><div class="pallet_color_selector">   <ul class="list_color">';
                for (let e = 0, t = i.length; e < t; e++) {
                    const t = i[e];
                    n += '<li>   <button type="button" data-value="' + t + '" title="' + t + '" style="background-color:' + t + ';"></button></li>'
                }
                return n += '</ul></div><form class="sub-form-group">   <label>#</label>   <input type="text" maxlength="6" class="sun-editor-id-submenu-color-input" />   <button type="submit" class="se-btn-primary sun-editor-id-submenu-color-submit se-tooltip">       <i class="icon-checked"></i>       <span class="se-tooltip-inner"><span class="se-tooltip-text">' + t.dialogBox.submitButton + '</span></span>   </button>   <button type="button" class="btn_editor sun-editor-id-submenu-color-default se-tooltip">       <i class="icon-erase"></i>       <span class="se-tooltip-inner"><span class="se-tooltip-text">' + t.toolbar.removeFormat + "</span></span>   </button></form></div>"
            },
            init: function(e, t) {
                const i = this.plugins.colorPicker;
                let n = t || (i.getColorInNode.call(this, e) || this.context.colorPicker._defaultColor);
                n = i.isHexColor(n) ? n : i.rgb2hex(n);
                const o = this.context.colorPicker._colorList;
                if (o)
                    for (let e = 0, t = o.length; e < t; e++)
                        n === o[e].getAttribute("data-value") ? this.util.addClass(o[e], "on") : this.util.removeClass(o[e], "on");
                i.setInputText.call(this, n)
            },
            setCurrentColor: function(e) {
                this.context.colorPicker._currentColor = e,
                this.context.colorPicker._colorInput.style.borderColor = e
            },
            setInputText: function(e) {
                this.context.colorPicker._colorInput.value = e.replace("#", ""),
                this.plugins.colorPicker.setCurrentColor.call(this, e)
            },
            getColorInNode: function(e) {
                let t = "";
                const i = this.context.colorPicker._styleProperty;
                for (; e && !this.util.isWysiwygDiv(e) && 0 === t.length; )
                    1 === e.nodeType && e.style[i] && (t = e.style[i]),
                    e = e.parentNode;
                return t
            },
            isHexColor: function(e) {
                return /^#[0-9a-f]{3}(?:[0-9a-f]{3})?$/i.test(e)
            },
            rgb2hex: function(e) {
                return (e = e.match(/^rgba?[\s+]?\([\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?/i)) && 4 === e.length ? "#" + ("0" + parseInt(e[1], 10).toString(16)).slice(-2) + ("0" + parseInt(e[2], 10).toString(16)).slice(-2) + ("0" + parseInt(e[3], 10).toString(16)).slice(-2) : ""
            }
        }
    },
    KKur: function(module, __webpack_exports__, __webpack_require__) {
        "use strict";
        var _modules_dialog__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("1kvd")
          , _modules_resizing__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("ee5k")
          , _modules_notice__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("PAX9");
        __webpack_exports__.a = {
            name: "image",
            add: function(core) {
                core.addModule([_modules_dialog__WEBPACK_IMPORTED_MODULE_0__.a, _modules_resizing__WEBPACK_IMPORTED_MODULE_1__.a, _modules_notice__WEBPACK_IMPORTED_MODULE_2__.a]);
                const context = core.context;
                context.image = {
                    _linkElement: null,
                    _container: null,
                    _cover: null,
                    _element: null,
                    _element_w: 1,
                    _element_h: 1,
                    _element_l: 0,
                    _element_t: 0,
                    _origin_w: "auto" === context.option.imageWidth ? "" : context.option.imageWidth,
                    _origin_h: "",
                    _altText: "",
                    _caption: null,
                    captionCheckEl: null,
                    _linkValue: "",
                    _align: "none",
                    _captionChecked: !1,
                    _proportionChecked: !0,
                    _floatClassRegExp: "float\\-[a-z]+",
                    _xmlHttp: null,
                    _resizing: context.option.imageResizing,
                    _defaultAuto: "auto" === context.option.imageWidth
                };
                let image_dialog = eval(this.setDialog.call(core));
                context.image.modal = image_dialog,
                context.image.imgUrlFile = image_dialog.getElementsByClassName("sun-editor-id-image-url")[0],
                context.image.imgInputFile = context.image.focusElement = image_dialog.getElementsByClassName("sun-editor-id-image-file")[0],
                context.image.altText = image_dialog.getElementsByClassName("sun-editor-id-image-alt")[0],
                context.image.imgLink = image_dialog.getElementsByClassName("sun-editor-id-image-link")[0],
                context.image.imgLinkNewWindowCheck = image_dialog.getElementsByClassName("sun-editor-id-linkCheck")[0],
                context.image.captionCheckEl = image_dialog.getElementsByClassName("suneditor-id-image-check-caption")[0],
//                context.image.modal.getElementsByClassName("sun-editor-tab-button")[0].addEventListener("click", this.openTab.bind(core)),
                context.image.modal.getElementsByClassName("se-btn-primary")[0].addEventListener("click", this.submit.bind(core)),
                context.image.imageX = {},
                context.image.imageY = {},
                context.option.imageResizing && (context.image.proportion = image_dialog.getElementsByClassName("suneditor-id-image-check-proportion")[0],
                context.image.imageX = image_dialog.getElementsByClassName("sun-editor-id-image-x")[0],
                context.image.imageY = image_dialog.getElementsByClassName("sun-editor-id-image-y")[0]
//                context.image.imageX.value = context.option.imageWidth,
//                context.image.imageX.addEventListener("change", this.setInputSize.bind(core, "x")),
//                context.image.imageY.addEventListener("change", this.setInputSize.bind(core, "y")),
//                image_dialog.getElementsByClassName("sun-editor-id-image-revert-button")[0].addEventListener("click", this.sizeRevert.bind(core))
                ),
                context.dialog.modal.appendChild(image_dialog),
                image_dialog = null
            },
            setDialog: function() {
                const e = this.context.option
                  , t = this.lang
                  , i = this.util.createElement("DIV");
                i.className = "modal-content sun-editor-id-dialog-image",
                i.style.display = "none";
//                <div class="sun-editor-tab-button">   <button type="button" class="sun-editor-id-tab-link active" data-tab-link="image">' + t.toolbar.image + '</button> </div>
//                <button type="button" class="sun-editor-id-tab-link" data-tab-link="url">' + t.toolbar.link + '</button>
                let n = '<div class="modal-header">   <button type="button" data-command="close" class="close" aria-label="Close" title="' + t.dialogBox.close + '">       <i aria-hidden="true" data-command="close" class="icon-cancel"></i>   </button>   <h5 class="modal-title">' + '<label>이미지 첨부하기</label>' + '</h5></div><form class="editor_image" method="post" enctype="multipart/form-data">   <div class="sun-editor-id-tab-content sun-editor-id-tab-content-image">       <div class="modal-body">';
                return e.imageFileInput &&
                (n += '<div class="form-group"><label>'+ t.dialogBox.imageBox.file +
                '</label><input class="form-control sun-editor-id-image-file" type="file" accept="image/*" multiple="multiple" /></div>'),
                 e.imageUrlInput && (n += '<div class="form-group" style="display:none"><label>' + t.dialogBox.imageBox.url +
                 '</label><input class="form-control sun-editor-id-image-url" type="text" /></div>'),
                 n += '<div class="form-group" style="display:none"><label>' + t.dialogBox.imageBox.altText + '</label><input class="form-control sun-editor-id-image-alt" type="text" /></div>',
                 e.imageResizing && (n += '<div class="form-group" style="display:none"><div class="size-text"><label class="size-w">' + t.dialogBox.width +
                 '</label><label class="size-x">&nbsp;</label><label class="size-h">' + t.dialogBox.height + '</label></div><input class="form-size-control sun-editor-id-image-x" type="number" min="1" ' + ("auto" === e.imageWidth ? "disabled" : "") +
                 ' /><label class="size-x">x</label><input class="form-size-control sun-editor-id-image-y" type="number" min="1" disabled /><label><input type="checkbox" class="suneditor-id-image-check-proportion" style="margin-left: 20px;" checked disabled/>&nbsp;' + t.dialogBox.proportion + '</label>           <button type="button" title="' + t.dialogBox.revertButton +
                 '" class="btn_editor btn-revert sun-editor-id-image-revert-button" style="float: right;"><i class="icon-revert"></i></button></div>'),
                 n += '<div class="form-group-footer" style="display:none"><label><input type="checkbox" class="suneditor-id-image-check-caption" />&nbsp;' + t.dialogBox.caption +
                 '</label></div></div></div><div class="sun-editor-id-tab-content sun-editor-id-tab-content-url" style="display: none"><div class="modal-body"><div class="form-group"><label>' + t.dialogBox.linkBox.url +
                 '</label><input class="form-control sun-editor-id-image-link" type="text" /></div><label><input type="checkbox" class="sun-editor-id-linkCheck"/>&nbsp;' + t.dialogBox.linkBox.newWindowCheck +
                 '</label></div></div><div class="modal-footer" style="display:none;"><div style="float: left;"><label><input type="radio" name="suneditor_image_radio" class="modal-radio" value="none" checked>' + t.dialogBox.basic +
                 '</label><label><input type="radio" name="suneditor_image_radio" class="modal-radio" value="left">' + t.dialogBox.left +
                 '</label><label><input type="radio" name="suneditor_image_radio" class="modal-radio" value="center">' + t.dialogBox.center +
                 '</label><label><input type="radio" name="suneditor_image_radio" class="modal-radio" value="right">' + t.dialogBox.right +
                 '</label></div></div><div class="modal-footer"><button type="submit" class="btn se-btn-primary sun-editor-id-submit-image" title="업로드"><span>' + t.dialogBox.submitButton + "</span></button></div></form>",
                i.innerHTML = n,i
            },
            openTab: function(e) {
                const t = this.context.image.modal
                  , i = "init" === e ? t.getElementsByClassName("sun-editor-id-tab-link")[0] : e.target;
                if (!/^BUTTON$/i.test(i.tagName))
                    return !1;
                const n = i.getAttribute("data-tab-link");
                let o, l, s;
                for (l = t.getElementsByClassName("sun-editor-id-tab-content"),
                o = 0; o < l.length; o++)
                    l[o].style.display = "none";
                for (s = t.getElementsByClassName("sun-editor-id-tab-link"),
                o = 0; o < s.length; o++)
                    this.util.removeClass(s[o], "active");
                return t.getElementsByClassName("sun-editor-id-tab-content-" + n)[0].style.display = "block",
                this.util.addClass(i, "active"),
                "image" === n ? this.context.image.imgInputFile.focus() : "url" === n && this.context.image.imgLink.focus(),
                !1
            },
            onRender_imgInput: function() {
                const e = function(e) {
                    if (e.length > 0) {
                        const t = this.context.option.imageUploadUrl
                          , i = this.context.option.imageUploadHeader
                          , n = this.context.dialog.updateModal ? 1 : e.length;
                        if ("string" == typeof t && t.length > 0) {
                            const o = new FormData;
                            for (let t = 0; t < n; t++)
                                o.append("file-" + t, e[t]);
                            if (this.context.image._xmlHttp = this.util.getXMLHttpRequest(),
                            this.context.image._xmlHttp.onreadystatechange = this.plugins.image.callBack_imgUpload.bind(this, this.context.image._linkValue, this.context.image.imgLinkNewWindowCheck.checked, this.context.image.imageX.value + "px", this.context.image._align, this.context.dialog.updateModal, this.context.image._element),
                            this.context.image._xmlHttp.open("post", t, !0),
                            "object" == typeof i && Object.keys(i).length > 0)
                                for (let e in i)
                                    this.context.image._xmlHttp.setRequestHeader(e, i[e]);
                            this.context.image._xmlHttp.send(o)
                        } else
                            for (let t = 0; t < n; t++)
                                this.plugins.image.setup_reader.call(this, e[t], this.context.image._linkValue, this.context.image.imgLinkNewWindowCheck.checked, this.context.image.imageX.value + "px", this.context.image._align, t, n - 1)
                    }
                }
                .bind(this);
                try {
                    e(this.context.image.imgInputFile.files)
                } catch (e) {
                    throw this.closeLoading(),
                    _modules_notice__WEBPACK_IMPORTED_MODULE_2__.a.open.call(this, '[SUNEDITOR.imageUpload.fail] cause : "' + e.message + '"'),
                    Error('[SUNEDITOR.imageUpload.fail] cause : "' + e.message + '"')
                }
            },
            setup_reader: function(e, t, i, n, o, l, s) {
                const a = new FileReader;
                this.context.dialog.updateModal && (this.context.image._element.setAttribute("data-file-name", e.name),
                this.context.image._element.setAttribute("data-file-size", e.size)),
                a.onload = function(e, r, c) {
                    try {
                        this.plugins.image.create_image.call(this, a.result, t, i, n, o, e, r, c),
                        l === s && this.closeLoading()
                    } catch (e) {
                        throw this.closeLoading(),
                        _modules_notice__WEBPACK_IMPORTED_MODULE_2__.a.open.call(this, '[SUNEDITOR.imageFileRendering.fail] cause : "' + e.message + '"'),
                        Error('[SUNEDITOR.imageFileRendering.fail] cause : "' + e.message + '"')
                    }
                }
                .bind(this, this.context.dialog.updateModal, this.context.image._element, e),
                a.readAsDataURL(e)
            },
            callBack_imgUpload: function(e, t, i, n, o, l) {
                if (4 === this.context.image._xmlHttp.readyState) {
                    if (200 !== this.context.image._xmlHttp.status)
                        throw _modules_notice__WEBPACK_IMPORTED_MODULE_2__.a.open.call(this, "[SUNEDITOR.imageUpload.fail] status: " + this.context.image._xmlHttp.status + ", responseURL: " + this.context.image._xmlHttp.responseURL),
                        this.closeLoading(),
                        Error("[SUNEDITOR.imageUpload.fail] status: " + this.context.image._xmlHttp.status + ", responseURL: " + this.context.image._xmlHttp.responseURL);
                    {
                        const s = JSON.parse(this.context.image._xmlHttp.responseText);
                        if (s.errorMessage)
                            this.closeLoading(),
                            this._imageUploadError(s.errorMessage, s.result) && _modules_notice__WEBPACK_IMPORTED_MODULE_2__.a.open.call(this, s.errorMessage);
                        else {
                            const a = s.result;
                            for (let s = 0, r = o && a.length > 0 ? 1 : a.length; s < r; s++)
                                this.plugins.image.create_image.call(this, a[s].url, e, t, i, n, o, l, {
                                    name: a[s].name,
                                    size: a[s].size
                                })
                        }
                        this.closeLoading()
                    }
                }
            },
            onRender_imgUrl: function() {
                if (0 === this.context.image.imgUrlFile.value.trim().length)
                    return !1;
                try {
                    this.plugins.image.create_image.call(this, this.context.image.imgUrlFile.value, this.context.image._linkValue, this.context.image.imgLinkNewWindowCheck.checked, this.context.image.imageX.value + "px", this.context.image._align, this.context.dialog.updateModal, this.context.image._element)
                } catch (e) {
                    throw _modules_notice__WEBPACK_IMPORTED_MODULE_2__.a.open.call(this, '[SUNEDITOR.imageURLRendering.fail] cause : "' + e.message + '"'),
                    Error('[SUNEDITOR.imageURLRendering.fail] cause : "' + e.message + '"')
                } finally {
                    this.closeLoading()
                }
            },
            onRender_link: function(e, t, i) {
                if (t.trim().length > 0) {
                    const n = this.util.createElement("A");
                    return n.href = /^https?:\/\//.test(t) ? t : "http://" + t,
                    n.target = i ? "_blank" : "",
                    n.setAttribute("data-image-link", "image"),
                    e.setAttribute("data-image-link", t),
                    n.appendChild(e),
                    n
                }
                return e
            },
            setInputSize: function(e) {
                this.context.dialog.updateModal && this.context.image.proportion.checked && ("x" === e ? this.context.image.imageY.value = Math.round(this.context.image._element_h / this.context.image._element_w * this.context.image.imageX.value) : this.context.image.imageX.value = Math.round(this.context.image._element_w / this.context.image._element_h * this.context.image.imageY.value))
            },
            submit: function(e) {
                this.showLoading(),
                e.preventDefault(),
                e.stopPropagation()
                 this.context.image._linkValue = this.context.image.imgLink.value,
                 this.context.image._altText = this.context.image.altText.value,
                 this.context.image._align = this.context.image.modal.querySelector('input[name="suneditor_image_radio"]:checked').value,
                 this.context.image._captionChecked = this.context.image.captionCheckEl.checked,
                 this.context.image._resizing && (this.context.image._proportionChecked = this.context.image.proportion.checked);
                try {
                    this.context.dialog.updateModal && this.plugins.image.update_image.call(this),
                    this.context.image.imgInputFile && this.context.image.imgInputFile.files.length > 0 ? this.plugins.image.onRender_imgInput.call(this) : this.context.image.imgUrlFile && this.context.image.imgUrlFile.value.trim().length > 0 ? this.plugins.image.onRender_imgUrl.call(this) : this.closeLoading()
                } catch (e) {
                    throw this.closeLoading(),
                    _modules_notice__WEBPACK_IMPORTED_MODULE_2__.a.open.call(this, '[SUNEDITOR.image.submit.fail] cause : "' + e.message + '"'),
                    Error('[SUNEDITOR.image.submit.fail] cause : "' + e.message + '"')
                } finally {
                    this.plugins.dialog.close.call(this)
                }
                return !1
            },
            _onload_image: function(e, t) {
                if (e.setAttribute("origin-size", e.naturalWidth + "," + e.naturalHeight),
                e.setAttribute("data-origin", e.offsetWidth + "," + e.offsetHeight),
                !t)
                    return;
                let i = e.getAttribute("data-index");
                i ? (this._variable._imagesInfo[i].name = e.getAttribute("data-file-name"),
                this._variable._imagesInfo[i].size = 1 * e.getAttribute("data-file-size")) : (i = this._variable._imageIndex,
                e.setAttribute("data-index", i),
                this._variable._imagesInfo[i] = {
                    src: e.src,
                    index: i,
                    name: t.name,
                    size: t.size,
                    select: function() {
                        const t = this.plugins.resizing.call_controller_resize.call(this, e, "image");
                        this.plugins.image.onModifyMode.call(this, e, t),
                        e.scrollIntoView()
                    }
                    .bind(this),
                    delete: this.plugins.image.destroy.bind(this, e)
                },
                this._variable._imageIndex++,
                e.setAttribute("data-file-name", t.name),
                e.setAttribute("data-file-size", t.size)),
                this._imageUpload(e, i, !1, this._variable._imagesInfo[i])
            },
            create_image: function(e, t, i, n, o, l, s, a) {
                if (l)
                    return void (s.src = e);
                const r = this.context.image;
                let c = this.util.createElement("IMG");
                c.addEventListener("load", this.plugins.image._onload_image.bind(this, c, a)),
                c.src = e,
                c.setAttribute("data-align", o),
                c.alt = r._altText,
                (c = this.plugins.image.onRender_link.call(this, c, t, i)).setAttribute("data-rotate", "0"),
                r._resizing && (/\d+/.test(n) && (c.style.width = n),
                c.setAttribute("data-proportion", r._proportionChecked));
                const d = this.plugins.resizing.set_cover.call(this, c)
                  , u = this.plugins.resizing.set_container.call(this, d, "sun-editor-id-image-container");
                r._captionChecked && (r._caption = this.plugins.resizing.create_caption.call(this),
                r._caption.setAttribute("contenteditable", !1),
                d.appendChild(r._caption)),
                d.style.margin = "none" !== o ? "auto" : "0",
                this.util.removeClass(u, r._floatClassRegExp),
                this.util.addClass(u, "float-" + o),
                r._resizing && /\d+/.test(n) || (this.context.resizing._resize_plugin = "image",
                r._element = c,
                r._cover = d,
                r._container = u,
                this.plugins.image.setAutoSize.call(this)),
                this.insertComponent(u)
            },
            update_image: function(e) {
                const t = this.context.image
                  , i = t._linkValue;
                let n = t._element
                  , o = t._cover
                  , l = t._container
                  , s = !1;
                const a = 1 * t.imageX.value !== n.offsetWidth || 1 * t.imageY.value !== n.offsetHeight;
                if (null === o && (s = !0,
                n = t._element.cloneNode(!0),
                o = this.plugins.resizing.set_cover.call(this, n)),
                null === l && (o = o.cloneNode(!0),
                s = !0,
                l = this.plugins.resizing.set_container.call(this, o, "sun-editor-id-image-container")),
                s && (l.innerHTML = "",
                l.appendChild(o)),
                n.alt = t._altText,
                t._resizing && (n.setAttribute("data-proportion", t._proportionChecked),
                a && this.plugins.image.setSize.call(this, t.imageX.value, t.imageY.value)),
                t._captionChecked ? t._caption || (t._caption = this.plugins.resizing.create_caption.call(this),
                o.appendChild(t._caption)) : t._caption && (this.util.removeItem(t._caption),
                t._caption = null),
                t._align && "none" !== t._align ? o.style.margin = "auto" : o.style.margin = "0",
                this.util.removeClass(l, this.context.image._floatClassRegExp),
                this.util.addClass(l, "float-" + t._align),
                n.setAttribute("data-align", t._align),
                i.trim().length > 0)
                    if (null !== t._linkElement)
                        t._linkElement.href = i,
                        t._linkElement.target = t.imgLinkNewWindowCheck.checked ? "_blank" : "",
                        n.setAttribute("data-image-link", i);
                    else {
                        let e = this.plugins.image.onRender_link.call(this, n, i, this.context.image.imgLinkNewWindowCheck.checked);
                        o.insertBefore(e, t._caption)
                    }
                else if (null !== t._linkElement) {
                    const e = n;
                    e.setAttribute("data-image-link", "");
                    let i = e.cloneNode(!0);
                    o.removeChild(t._linkElement),
                    o.insertBefore(i, t._caption),
                    n = i
                }
                if (s) {
                    const e = this.util.isRangeFormatElement(t._element.parentNode) || this.util.isWysiwygDiv(t._element.parentNode) ? t._element : this.util.getFormatElement(t._element) || t._element;
                    e.parentNode.insertBefore(l, e),
                    this.util.removeItem(e)
                }
                if (!e && (/\d+/.test(n.style.height) || t._resizing && a || this.context.resizing._rotateVertical && t._captionChecked) && this.plugins.resizing.setTransformSize.call(this, n),
                this.history.push(),
                e) {
                    n = l.querySelector("img"),
                    this.plugins.image.init.call(this);
                    const e = this.plugins.resizing.call_controller_resize.call(this, n, "image");
                    this.plugins.image.onModifyMode.call(this, n, e)
                }
            },
            sizeRevert: function() {
                const e = this.context.image;
                e._origin_w && (e.imageX.value = e._element_w = e._origin_w,
                e.imageY.value = e._element_h = e._origin_h)
            },
            onModifyMode: function(e, t) {
                const i = this.context.image;
                i._linkElement = /^A$/i.test(e.parentNode.nodeName) ? e.parentNode : null,
                i._element = e,
                i._cover = this.util.getParentElement(e, ".sun-editor-figure-cover"),
                i._container = this.util.getParentElement(e, ".sun-editor-id-image-container"),
                i._caption = this.util.getChildElement(i._cover, "FIGCAPTION"),
                i._align = e.getAttribute("data-align") || "none",
                i._element_w = t.w,
                i._element_h = t.h,
                i._element_t = t.t,
                i._element_l = t.l;
                let n = i._element.getAttribute("data-origin");
                n ? (n = n.split(","),
                i._origin_w = 1 * n[0],
                i._origin_h = 1 * n[1]) : (i._origin_w = t.w,
                i._origin_h = t.h,
                i._element.setAttribute("data-origin", t.w + "," + t.h))
            },
            openModify: function(e) {
                const t = this.context.image;
                t.imgUrlFile.value = t._element.src,
                t.altText.value = t._element.alt,
                t.imgLink.value = null === t._linkElement ? "" : t._linkElement.href,
                t.imgLinkNewWindowCheck.checked = t._linkElement && "_blank" === t._linkElement.target,
                t.modal.querySelector('input[name="suneditor_image_radio"][value="' + t._align + '"]').checked = !0,
                t._captionChecked = t.captionCheckEl.checked = !!t._caption,
                t._resizing && (t.proportion.checked = t._proportionChecked = "true" === t._element.getAttribute("data-proportion"),
                t.imageX.value = t._element.offsetWidth,
                t.imageY.value = t._element.offsetHeight,
                t.imageX.disabled = !1,
                t.imageY.disabled = !1,
                t.proportion.disabled = !1),
                e || this.plugins.dialog.open.call(this, "image", !0)
            },
            setSize: function(e, t) {
                const i = this.context.image;
                i._element.style.width = e + "px",
                i._element.style.height = t + "px"
            },
            setAutoSize: function() {
                const e = this.context.image;
                this.plugins.resizing.resetTransform.call(this, e._element),
                this.plugins.image.cancelPercentAttr.call(this),
                e._element.style.maxWidth = "100%",
                e._element.style.width = "100%",
                e._element.style.height = "",
                e._cover.style.width = "",
                e._cover.style.height = ""
            },
            setPercentSize: function(e) {
                const t = this.context.image;
                t._container.style.width = e,
                t._container.style.height = "",
                t._cover.style.width = "100%",
                t._cover.style.height = "",
                t._element.style.width = "100%",
                t._element.style.height = "",
                /100/.test(e) && (this.util.removeClass(t._container, this.context.image._floatClassRegExp),
                this.util.addClass(t._container, "float-center"))
            },
            cancelPercentAttr: function() {
                const e = this.context.image;
                e._element.style.maxWidth = "none",
                e._cover.style.width = "",
                e._cover.style.height = "",
                e._container.style.width = "",
                e._container.style.height = "",
                this.util.removeClass(e._container, this.context.image._floatClassRegExp),
                this.util.addClass(e._container, "float-" + e._align)
            },
            resetAlign: function() {
                const e = this.context.image;
                e._element.setAttribute("data-align", ""),
                e._align = "none",
                e._cover.style.margin = "0",
                this.util.removeClass(e._container, e._floatClassRegExp)
            },
            destroy: function(e) {
                const t = e || this.context.image._element
                  , i = this.util.getParentElement(t, ".sun-editor-id-image-container") || t
                  , n = t.getAttribute("data-index");
                this.util.removeItem(i),
                this.plugins.image.init.call(this),
                this.controllersOff(),
                n && (delete this._variable._imagesInfo[n],
                this._imageUpload(t, n, !0))
            },
            init: function() {
                const e = this.context.image;
                if (e.imgInputFile && (e.imgInputFile.value = ""),
                e.imgUrlFile && (e.imgUrlFile.value = ""),
                e.altText.value = "",
                e.imgLink.value = "",
                e.imgLinkNewWindowCheck.checked = !1,
                e.modal.querySelector('input[name="suneditor_image_radio"][value="none"]').checked = !0,
                e.captionCheckEl.checked = !1,
                e._element = null,
//                this.plugins.image.openTab.call(this, "init"),
                e._resizing) {
                    const t = "auto" === this.context.option.imageWidth;
                    e.proportion.checked = !1,
                    e.imageX.value = t ? "" : this.context.option.imageWidth,
                    e.imageY.value = "",
                    e.imageX.disabled = t,
                    e.imageY.disabled = !0,
                    e.proportion.disabled = !0
                }
            }
        }
    },
    MIhV: function(module, __webpack_exports__, __webpack_require__) {
        "use strict";
        __webpack_exports__.a = {
            name: "fontSize",
            add: function(core, targetElement) {
                const context = core.context;
                context.fontSize = {
                    _sizeList: [],
                    currentSize: ""
                };
                let listDiv = eval(this.setSubmenu.call(core));
                listDiv.getElementsByTagName("UL")[0].addEventListener("click", this.pickup.bind(core)),
                context.fontSize._sizeList = listDiv.getElementsByTagName("UL")[0].querySelectorAll("li button"),
                targetElement.parentNode.appendChild(listDiv),
                listDiv = null
            },
            setSubmenu: function() {
                const e = this.context.option
                  , t = this.lang
                  , i = this.util.createElement("DIV");
                i.className = "sun-editor-submenu layer_editor",
                i.style.display = "none";
                const n = e.fontSize ? e.fontSize : [8, 9, 10, 11, 12, 14, 16, 18, 20, 22, 24, 26, 28, 36, 48, 72];
                let o = '<div class="inner_layer layer_size">   <ul class="list_editor font_size_list">       <li><button type="button" class="default_value btn_edit" title="' + t.toolbar.default + '">(' + t.toolbar.default + ")</button></li>";
                for (let e = 0, t = n.length; e < t; e++) {
                    const t = n[e];
                    o += '<li><button type="button" class="btn_edit" data-value="' + t + '" title="' + t + '" style="font-size:' + t + 'px;">' + t + "</button></li>"
                }
                return o += "   </ul></div>",
                i.innerHTML = o,
                i
            },
            on: function() {
                const e = this.context.fontSize
                  , t = e._sizeList
                  , i = this.commandMap.SIZE.getAttribute("title") || "";
                if (i !== e.currentSize) {
                    for (let e = 0, n = t.length; e < n; e++)
                        i === t[e].getAttribute("data-value") ? this.util.addClass(t[e], "on") : this.util.removeClass(t[e], "on");
                    e.currentSize = i
                }
            },
            pickup: function(e) {
                if (!/^BUTTON$/i.test(e.target.tagName))
                    return !1;
                e.preventDefault(),
                e.stopPropagation();
                const t = e.target.getAttribute("data-value");
                if (t) {
                    const e = this.util.createElement("SPAN");
                    e.style.fontSize = t + "px",
                    this.nodeChange(e, ["font-size"])
                } else
                    this.nodeChange(null, ["font-size"]);
                this.submenuOff(),
                this.focus()
            }
        }
    },
    P6u4: function(e, t, i) {
        "use strict";
        var n, o;
        n = "undefined" != typeof window ? window : this,
        o = function(e, t) {
            const i = {
                toolbar: {
                    default: "Default",
                    save: "Save",
                    font: "Font",
                    formats: "Formats",
                    fontSize: "Size",
                    bold: "Bold",
                    underline: "Underline",
                    italic: "Italic",
                    strike: "Strike",
                    subscript: "Subscript",
                    superscript: "Superscript",
                    removeFormat: "Remove Format",
                    fontColor: "Font Color",
                    hiliteColor: "Hilite Color",
                    indent: "Indent",
                    outdent: "Outdent",
                    align: "Align",
                    alignLeft: "Align left",
                    alignRight: "Align right",
                    alignCenter: "Align center",
                    alignJustify: "Align justify",
                    list: "list",
                    orderList: "Ordered list",
                    unorderList: "Unordered list",
                    horizontalRule: "horizontal line",
                    hr_solid: "solid",
                    hr_dotted: "dotted",
                    hr_dashed: "dashed",
                    table: "Table",
                    link: "Link",
                    image: "Image",
                    video: "Video",
                    fullScreen: "Full screen",
                    showBlocks: "Show blocks",
                    codeView: "Code view",
                    undo: "Undo",
                    redo: "Redo",
                    preview: "Preview",
                    print: "print",
                    tag_p: "Paragraph",
                    tag_div: "Normal (DIV)",
                    tag_h: "Header",
                    tag_quote: "Quote",
                    pre: "Code"
                },
                dialogBox: {
                    linkBox: {
                        title: "Insert Link",
                        url: "URL to link",
                        text: "Text to display",
                        newWindowCheck: "Open in new window"
                    },
                    imageBox: {
                        title: "Insert image",
                        file: "Select from files",
                        url: "Image URL",
                        altText: "Alternative text"
                    },
                    videoBox: {
                        title: "Insert Video",
                        url: "Media embed URL, YouTube"
                    },
                    caption: "Insert description",
                    close: "Close",
                    submitButton: "업로드",
                    revertButton: "Revert",
                    proportion: "constrain proportions",
                    width: "Width",
                    height: "Height",
                    basic: "Basic",
                    left: "Left",
                    right: "Right",
                    center: "Center"
                },
                controller: {
                    edit: "Edit",
                    remove: "Remove",
                    insertRowAbove: "Insert row above",
                    insertRowBelow: "Insert row below",
                    deleteRow: "Delete row",
                    insertColumnBefore: "Insert column before",
                    insertColumnAfter: "Insert column after",
                    deleteColumn: "Delete column",
                    resize100: "Resize 100%",
                    resize75: "Resize 75%",
                    resize50: "Resize 50%",
                    resize25: "Resize 25%",
                    mirrorHorizontal: "Mirror, Horizontal",
                    mirrorVertical: "Mirror, Vertical",
                    rotateLeft: "Rotate left",
                    rotateRight: "Rotate right",
                    maxSize: "Max size",
                    minSize: "Min size"
                }
            };
            return void 0 === t && (e.SUNEDITOR_LANG || (e.SUNEDITOR_LANG = {}),
            e.SUNEDITOR_LANG.en = i),
            i
        }
        ,
        "object" == typeof e.exports ? e.exports = n.document ? o(n, !0) : function(e) {
            if (!e.document)
                throw new Error("SUNEDITOR_LANG a window with a document");
            return o(e)
        }
        : o(n)
    },
    PAX9: function(e, t, i) {
        "use strict";
        t.a = {
            name: "notice",
            add: function(e) {
                const t = e.context;
                t.notice = {};
                let i = e.util.createElement("DIV")
                  , n = e.util.createElement("SPAN")
                  , o = e.util.createElement("BUTTON");
                i.className = "sun-editor-id-notice",
                o.className = "close",
                o.setAttribute("aria-label", "Close"),
                o.setAttribute("title", e.lang.dialogBox.close),
                o.innerHTML = '<i aria-hidden="true" data-command="close" class="icon-cancel"></i>',
                i.appendChild(n),
                i.appendChild(o),
                t.notice.modal = i,
                t.notice.message = n,
                o.addEventListener("click", this.onClick_cancel.bind(e)),
                t.element.editorArea.insertBefore(i, t.element.wysiwyg),
                i = null
            },
            onClick_cancel: function(e) {
                e.preventDefault(),
                e.stopPropagation(),
                this.plugins.notice.close.call(this)
            },
            open: function(e) {
                this.context.notice.message.textContent = e,
                this.context.notice.modal.style.display = "block"
            },
            close: function() {
                this.context.notice.modal.style.display = "none"
            }
        }
    },
    Rp48: function(module, __webpack_exports__, __webpack_require__) {
        "use strict";
        var _modules_dialog__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("1kvd");
        __webpack_exports__.a = {
            name: "link",
            add: function(core) {
                core.addModule([_modules_dialog__WEBPACK_IMPORTED_MODULE_0__.a]);
                const context = core.context;
                context.link = {};
                let link_dialog = eval(this.setDialog.call(core));
                context.link.modal = link_dialog,
                context.link.focusElement = link_dialog.getElementsByClassName("sun-editor-id-link-url")[0],
                context.link.linkAnchorText = link_dialog.getElementsByClassName("sun-editor-id-link-text")[0],
                context.link.linkNewWindowCheck = link_dialog.getElementsByClassName("sun-editor-id-link-check")[0];
                let link_button = eval(this.setController_LinkButton.call(core));
                context.link.linkBtn = link_button,
                context.link._linkAnchor = null,
                link_button.addEventListener("mousedown", function(e) {
                    e.stopPropagation()
                }, !1),
                link_dialog.getElementsByClassName("se-btn-primary")[0].addEventListener("click", this.submit.bind(core)),
                link_button.addEventListener("click", this.onClick_linkBtn.bind(core)),
                context.dialog.modal.appendChild(link_dialog),
                context.element.relative.appendChild(link_button),
                link_dialog = null,
                link_button = null
            },
            setDialog: function() {
                const e = this.lang
                  , t = this.util.createElement("DIV");
                return t.className = "modal-content sun-editor-id-dialog-link",
                t.style.display = "none",
                t.innerHTML = '<form class="editor_link">   <div class="modal-header">       <button type="button" data-command="close" class="close" aria-label="Close" title="' + e.dialogBox.close + '">           <i aria-hidden="true" data-command="close" class="icon-cancel"></i>       </button>       <h5 class="modal-title">' + e.dialogBox.linkBox.title + '</h5>   </div>   <div class="modal-body">       <div class="form-group">           <label>' + e.dialogBox.linkBox.url + '</label>           <input class="form-control sun-editor-id-link-url" type="text" />       </div>       <div class="form-group">           <label>' + e.dialogBox.linkBox.text + '</label><input class="form-control sun-editor-id-link-text" type="text" />       </div>       <label><input type="checkbox" class="sun-editor-id-link-check" />&nbsp;' + e.dialogBox.linkBox.newWindowCheck + '</label>   </div>   <div class="modal-footer">       <button type="submit" class="btn se-btn-primary sun-editor-id-submit-link" title="' + e.dialogBox.submitButton + '"><span>' + e.dialogBox.submitButton + "</span></button>   </div></form>",
                t
            },
            setController_LinkButton: function() {
                const e = this.lang
                  , t = this.util.createElement("DIV");
                return t.className = "se-controller sun-editor-id-link-btn",
                t.style.display = "none",
                t.innerHTML = '<div class="arrow arrow-up"></div><div class="link-content"><span><a target="_blank" href=""></a>&nbsp;</span>   <div class="btn-group">   <button type="button" data-command="delete" tabindex="-1" class="se-tooltip">       <i class="icon-delete"></i>       <span class="se-tooltip-inner"><span class="se-tooltip-text">' + e.controller.remove + "</span></span>   </button>   </div></div>",
                t
//                <button type="button" data-command="update" tabindex="-1" class="se-tooltip">   <i class="icon-link"></i>       <span class="se-tooltip-inner"><span class="se-tooltip-text">' + e.controller.edit + '</span></span>  </button>
            },
            submit: function(e) {
                this.showLoading(),
                e.preventDefault(),
                e.stopPropagation();
                const t = function() {
                    if (0 === this.context.link.focusElement.value.trim().length)
                        return !1;
                    const e = this.context.link.focusElement.value
                      , t = this.context.link.linkAnchorText
                      , i = 0 === t.value.length ? e : t.value;
                    if (this.context.dialog.updateModal)
                        this.context.link._linkAnchor.href = e,
                        this.context.link._linkAnchor.textContent = i,
                        this.context.link._linkAnchor.target = this.context.link.linkNewWindowCheck.checked ? "_blank" : "",
                        this.history.push(),
                        this.setRange(this.context.link._linkAnchor.childNodes[0], 0, this.context.link._linkAnchor.childNodes[0], this.context.link._linkAnchor.textContent.length);
                    else {
                        const t = this.util.createElement("A");
                        t.href = e,
                        t.textContent = i,
                        t.target = this.context.link.linkNewWindowCheck.checked ? "_blank" : "",
                        this.insertNode(t),
                        this.setRange(t.childNodes[0], 0, t.childNodes[0], t.textContent.length)
                    }
                    this.context.link.focusElement.value = "",
                    this.context.link.linkAnchorText.value = ""
                }
                .bind(this);
                try {
                    t()
                } finally {
                    this.plugins.dialog.close.call(this),
                    this.closeLoading(),
                    this.focus()
                }
                return !1
            },
            call_controller_linkButton: function(e) {
                this.editLink = this.context.link._linkAnchor = e;
                const t = this.context.link.linkBtn;
                t.getElementsByTagName("A")[0].href = e.href,
                t.getElementsByTagName("A")[0].title = e.textContent,
                t.getElementsByTagName("A")[0].textContent = e.textContent;
                const i = this.util.getOffset(e);
                t.style.left = i.left - this.context.element.wysiwyg.scrollLeft + "px",
                t.style.top = i.top + e.offsetHeight + 10 + "px",
                t.style.display = "block",
                this.controllersOn(t)
            },
            onClick_linkBtn: function(e) {
                e.stopPropagation();
                const t = e.target.getAttribute("data-command") || e.target.parentNode.getAttribute("data-command");
                t && (e.preventDefault(),
                /update/.test(t) ? (this.context.link.focusElement.value = this.context.link._linkAnchor.href,
                this.context.link.linkAnchorText.value = this.context.link._linkAnchor.textContent,
                this.context.link.linkNewWindowCheck.checked = !!/_blank/i.test(this.context.link._linkAnchor.target),
                this.plugins.dialog.open.call(this, "link", !0)) : (this.util.removeItem(this.context.link._linkAnchor),
                this.context.link._linkAnchor = null,
                this.focus()),
                this.controllersOff())
            },
            init: function() {
                const e = this.context.link;
                e.linkBtn.style.display = "none",
                e._linkAnchor = null,
                e.focusElement.value = "",
                e.linkAnchorText.value = "",
                e.linkNewWindowCheck.checked = !1
            }
        }
    },
    VquE: function(module, __webpack_exports__, __webpack_require__) {
        "use strict";
        __webpack_exports__.a = {
            name: "table",
            add: function(core, targetElement) {
                const context = core.context;
                context.table = {
                    _element: null,
                    _tdElement: null,
                    _trElement: null,
                    _trElements: null,
                    _tdIndex: 0,
                    _trIndex: 0,
                    _tdCnt: 0,
                    _trCnt: 0,
                    _tableXY: [],
                    _maxWidth: !0,
                    resizeIcon: null,
                    resizeText: null,
                    maxText: core.lang.controller.maxSize,
                    minText: core.lang.controller.minSize
                };
                let listDiv = eval(this.setSubmenu.call(core))
                  , tablePicker = listDiv.getElementsByClassName("sun-editor-id-table-picker")[0];
                context.table.tableHighlight = listDiv.getElementsByClassName("sun-editor-id-table-highlighted")[0],
                context.table.tableUnHighlight = listDiv.getElementsByClassName("sun-editor-id-table-unhighlighted")[0],
                context.table.tableDisplay = listDiv.getElementsByClassName("sun-editor-table-display")[0];
                let tableController = eval(this.setController_table.call(core));
                context.table.tableController = tableController,
                context.table.resizeIcon = tableController.querySelector("button > i"),
                context.table.resizeText = tableController.querySelector("button > span > span"),
                tableController.addEventListener("mousedown", function(e) {
                    e.stopPropagation()
                }, !1);
                let resizeDiv = eval(this.setController_tableEditor.call(core));
                context.table.resizeDiv = resizeDiv,
                resizeDiv.addEventListener("mousedown", function(e) {
                    e.stopPropagation()
                }, !1),
                tablePicker.addEventListener("mousemove", this.onMouseMove_tablePicker.bind(core)),
                tablePicker.addEventListener("click", this.appendTable.bind(core)),
                resizeDiv.addEventListener("click", this.onClick_tableController.bind(core)),
                tableController.addEventListener("click", this.onClick_tableController.bind(core)),
                targetElement.parentNode.appendChild(listDiv),
                context.element.relative.appendChild(resizeDiv),
                context.element.relative.appendChild(tableController),
                listDiv = null,
                tablePicker = null,
                resizeDiv = null,
                tableController = null
            },
            setSubmenu: function() {
                const e = this.util.createElement("DIV");
                return e.className = "sun-editor-submenu table-content",
                e.style.display = "none",
                e.innerHTML = '<div class="table-data-form">   <div class="table-picker sun-editor-id-table-picker"></div>   <div class="table-highlighted sun-editor-id-table-highlighted"></div>   <div class="table-unhighlighted sun-editor-id-table-unhighlighted"></div></div><div class="table-display sun-editor-table-display">1 x 1</div>',
                e
            },
            setController_table: function() {
                const e = this.lang
                  , t = this.util.createElement("DIV");
                return t.className = "se-controller sun-editor-id-table",
                t.style.display = "none",
                t.innerHTML = '<div>   <div class="btn-group">       <button type="button" data-command="resize" data-option="up" class="se-tooltip">           <i class="icon-expansion"></i>           <span class="se-tooltip-inner"><span class="se-tooltip-text">' + e.controller.maxSize + '</span></span>       </button>       <button type="button" data-command="remove" class="se-tooltip">           <i class="icon-delete"></i>           <span class="se-tooltip-inner"><span class="se-tooltip-text">' + e.controller.remove + "</span></span>       </button>   </div></div>",
                t
            },
            setController_tableEditor: function() {
                const e = this.lang
                  , t = this.util.createElement("DIV");
                return t.className = "se-controller sun-editor-id-table-edit",
                t.style.display = "none",
                t.innerHTML = '<div class="arrow arrow-up"></div><div>   <div class="btn-group">       <button type="button" data-command="insert" data-value="row" data-option="up" class="se-tooltip">           <i class="icon-insert-row-above"></i>           <span class="se-tooltip-inner"><span class="se-tooltip-text">' + e.controller.insertRowAbove + '</span></span>       </button>       <button type="button" data-command="insert" data-value="row" data-option="down" class="se-tooltip">           <i class="icon-insert-row-below"></i>           <span class="se-tooltip-inner"><span class="se-tooltip-text">' + e.controller.insertRowBelow + '</span></span>       </button>       <button type="button" data-command="delete" data-value="row" class="se-tooltip">           <i class="icon-delete-row"></i>           <span class="se-tooltip-inner"><span class="se-tooltip-text">' + e.controller.deleteRow + '</span></span>       </button>   </div></div><div>   <div class="btn-group">     <button type="button" data-command="insert" data-value="cell" data-option="left" class="se-tooltip">       <i class="icon-insert-column-left"></i>           <span class="se-tooltip-inner"><span class="se-tooltip-text">' + e.controller.insertColumnBefore + '</span></span>       </button>       <button type="button" data-command="insert" data-value="cell" data-option="right" class="se-tooltip">           <i class="icon-insert-column-right"></i>           <span class="se-tooltip-inner"><span class="se-tooltip-text">' + e.controller.insertColumnAfter + '</span></span>       </button>       <button type="button" data-command="delete" data-value="cell" class="se-tooltip">           <i class="icon-delete-column"></i>           <span class="se-tooltip-inner"><span class="se-tooltip-text">' + e.controller.deleteColumn + "</span></span>       </button>   </div></div>",
                t
            },
            appendTable: function() {
                const e = this.util.createElement("TABLE");
                let t = this.context.table._tableXY[0]
                  , i = this.context.table._tableXY[1]
                  , n = "<tbody>";
                for (; i > 0; ) {
                    n += "<tr>";
                    let e = t;
                    for (; e > 0; )
                        n += "<td><div>" + "<p><br></p>" + "</div></td>",
                        --e;
                    n += "</tr>",
                    --i
                }
                n += "</tbody>",
                e.innerHTML = n,
                this.insertComponent(e),
                this.focus(),
                this.plugins.table.reset_table_picker.call(this)
            },
            onMouseMove_tablePicker: function(e) {
                e.stopPropagation();
                let t = Math.ceil(e.offsetX / 18)
                  , i = Math.ceil(e.offsetY / 18);
                t = t < 1 ? 1 : t,
                i = i < 1 ? 1 : i,
                this.context.table.tableHighlight.style.width = t + "em",
                this.context.table.tableHighlight.style.height = i + "em";
                let n = t < 5 ? 5 : t > 9 ? 10 : t + 1
                  , o = i < 5 ? 5 : i > 9 ? 10 : i + 1;
                this.context.table.tableUnHighlight.style.width = n + "em",
                this.context.table.tableUnHighlight.style.height = o + "em",
                this.util.changeTxt(this.context.table.tableDisplay, t + " x " + i),
                this.context.table._tableXY = [t, i]
            },
            reset_table_picker: function() {
                if (!this.context.table.tableHighlight)
                    return;
                const e = this.context.table.tableHighlight.style
                  , t = this.context.table.tableUnHighlight.style;
                e.width = "1em",
                e.height = "1em",
                t.width = "5em",
                t.height = "5em",
                this.util.changeTxt(this.context.table.tableDisplay, "1 x 1"),
                this.submenuOff()
            },
            init: function() {
                const e = this.context.table;
                e._element = null,
                e._tdElement = null,
                e._trElement = null,
                e._trElements = 0,
                e._tdIndex = 0,
                e._trIndex = 0,
                e._trCnt = 0,
                e._tdCnt = 0,
                e._tableXY = [],
                e._maxWidth = !0
            },
            call_controller_tableEdit: function(e) {
                this.plugins.table.init.call(this);
                const t = this.context.table
                  , i = t.resizeDiv
                  , n = t.tableController;
                this.plugins.table.setPositionControllerDiv.call(this, e, !1),
                i.style.display = "block";
                const o = t._element
                  , l = this.util.getOffset(o);
                t._maxWidth = !o.style.width || "100%" === o.style.width,
                this.plugins.table.resizeTable.call(this),
                n.style.left = l.left + o.offsetLeft - this.context.element.wysiwyg.scrollLeft + "px",
                n.style.display = "block",
                n.style.top = l.top + o.offsetTop - n.offsetHeight - 2 + "px",
                this.controllersOn(i, n)
            },
            setPositionControllerDiv: function(e, t) {
                const i = this.context.table
                  , n = i.resizeDiv;
                let o = i._element;
                if (!o) {
                    for (o = e; !/^TABLE$/i.test(o.nodeName); )
                        o = o.parentNode;
                    i._element = o
                }
                i._tdElement !== e && (i._tdElement = e,
                i._trElement = e.parentNode),
                (t || 0 === i._trCnt) && (i._trElements = o.rows,
                i._tdIndex = e.cellIndex,
                i._trIndex = i._trElement.rowIndex,
                i._trCnt = o.rows.length,
                i._tdCnt = i._trElement.cells.length);
                const l = this.util.getOffset(e);
                n.style.left = l.left - this.context.element.wysiwyg.scrollLeft + "px",
                n.style.top = l.top + e.offsetHeight + 12 + "px"
            },
            insertRowCell: function(e, t) {
                const i = this.context.table;
                if ("row" === e) {
                    const e = "up" === t ? i._trIndex : i._trIndex + 1;
                    let n = "";
                    for (let e = 0, t = i._tdCnt; e < t; e++)
                        n += "<td><div>" + "<p><br></p>" + "</div></td>";
                    i._element.insertRow(e).innerHTML = n
                } else {
                    const e = i._trElements
                      , n = "left" === t ? i._tdIndex : i._tdIndex + 1;
                    let o = null;
                    for (let t = 0, l = i._trCnt; t < l; t++)
                        (o = e[t].insertCell(n)).innerHTML = "<div>" + "<p><br></p>" + "</div>"
                }
                this.plugins.table.setPositionControllerDiv.call(this, i._tdElement, !0)
            },
            deleteRowCell: function(e) {
                const t = this.context.table;
                if ("row" === e)
                    t._element.deleteRow(t._trIndex);
                else {
                    const e = t._trElements
                      , i = t._tdIndex;
                    for (let n = 0, o = t._trCnt; n < o; n++)
                        e[n].deleteCell(i)
                }
                this.controllersOff()
            },
            resizeTable: function() {
                const e = this.context.table
                  , t = e.resizeIcon
                  , i = e.resizeText;
                let n = "icon-expansion"
                  , o = "icon-reduction"
                  , l = e.minText
                  , s = "100%";
                e._maxWidth || (n = "icon-reduction",
                o = "icon-expansion",
                l = e.maxText,
                s = "auto"),
                this.util.removeClass(t, n),
                this.util.addClass(t, o),
                this.util.changeTxt(i, l),
                e._element.style.width = s
            },
            onClick_tableController: function(e) {
                e.stopPropagation();
                const t = e.target.getAttribute("data-command") ? e.target : e.target.parentNode
                  , i = t.getAttribute("data-command")
                  , n = t.getAttribute("data-value")
                  , o = t.getAttribute("data-option");
                if (!i)
                    return;
                e.preventDefault();
                const l = this.context.table;
                switch (i) {
                case "insert":
                    this.plugins.table.insertRowCell.call(this, n, o);
                    break;
                case "delete":
                    this.plugins.table.deleteRowCell.call(this, n);
                    break;
                case "resize":
                    l.resizeDiv.style.display = "none",
                    l._maxWidth = !l._maxWidth,
                    this.plugins.table.resizeTable.call(this);
                    break;
                case "remove":
                    this.util.removeItem(l._element),
                    this.controllersOff(),
                    this.focus()
                }
                this.history.push()
            }
        }
    },
    WRt5: function(module, __webpack_exports__, __webpack_require__) {
        "use strict";
        __webpack_exports__.a = {
            name: "align",
            add: function(core, targetElement) {
                const context = core.context;
                context.align = {
                    _alignList: [],
                    currentAlign: ""
                };
                let listDiv = eval(this.setSubmenu.call(core));
                listDiv.getElementsByTagName("UL")[0].addEventListener("click", this.pickup.bind(core)),
                context.align._alignList = listDiv.getElementsByTagName("UL")[0].querySelectorAll("li button"),
                targetElement.parentNode.appendChild(listDiv),
                listDiv = null
            },
            setSubmenu: function() {
                const e = this.lang
                  , t = this.util.createElement("DIV");
                return t.className = "layer_editor",
                t.style.display = "none",
                t.innerHTML = '<div class="sun-editor-submenu inner_layer layer_align">   <ul class="list_editor">       <li><button type="button" class="btn_edit btn_align" data-command="justifyleft" data-value="left" title="' + e.toolbar.alignLeft + '"><span class="icon-align-left"></span>' + e.toolbar.alignLeft + '</button></li>       <li><button type="button" class="btn_edit btn_align" data-command="justifycenter" data-value="center" title="' + e.toolbar.alignCenter + '"><span class="icon-align-center"></span>' + e.toolbar.alignCenter + '</button></li>       <li><button type="button" class="btn_edit btn_align" data-command="justifyright" data-value="right" title="' + e.toolbar.alignRight + '"><span class="icon-align-right"></span>' + e.toolbar.alignRight + '</button></li>       <li><button type="button" class="btn_edit btn_align" data-command="justifyfull" data-value="justify" title="' + e.toolbar.alignJustify + '"><span class="icon-align-justify"></span>' + e.toolbar.alignJustify + "</button></li>   </ul></div>",
                t
            },
            on: function() {
                const e = this.context.align
                  , t = e._alignList
                  , i = this.commandMap.ALIGN.getAttribute("data-focus");
                if (i !== e.currentAlign) {
                    for (let e = 0, n = t.length; e < n; e++)
                        i === t[e].getAttribute("data-value") ? this.util.addClass(t[e], "on") : this.util.removeClass(t[e], "on");
                    e.currentAlign = i
                }
            },
            pickup: function(e) {
                e.preventDefault(),
                e.stopPropagation();
                let t = e.target
                  , i = null;
                for (; !i && !/UL/i.test(t.tagName); )
                    i = t.getAttribute("data-command"),
                    t = t.parentNode;
                i && (this.focus(),
                this.execCommand(i, !1, null),
                this.submenuOff())
            }
        }
    },
    WUQj: function(e, t, i) {},
    WzUB: function(module, __webpack_exports__, __webpack_require__) {
        "use strict";
        __webpack_exports__.a = {
            name: "formatBlock",
            add: function(core, targetElement) {
                let listDiv = eval(this.setSubmenu.call(core));
                listDiv.getElementsByTagName("UL")[0].addEventListener("click", this.pickUp.bind(core)),
                targetElement.parentNode.appendChild(listDiv),
                listDiv = null
            },
            setSubmenu: function() {
                const e = this.lang
                  , t = this.util.createElement("DIV");
                return t.className = "sun-editor-submenu layer_editor",
                t.style.display = "none",
                t.innerHTML = '<div class="inner_layer">   <ul class="list_editor format_list">       <li><button type="button" class="btn_edit" data-command="replace" data-value="P" title="' + e.toolbar.tag_p + '"><span class="p_ex">' + e.toolbar.tag_p + '</span></button></li>       <li><button type="button" class="btn_edit" data-command="replace" data-value="DIV" title="' + e.toolbar.tag_div + '"><span class="div_ex">' + e.toolbar.tag_div + '</span></button></li>       <li><button type="button" class="btn_edit" data-command="range" data-value="BLOCKQUOTE" title="' + e.toolbar.tag_quote + '">               <blockquote class="quote_ex">' + e.toolbar.tag_quote + '</blockquote>           </button>       </li>       <li><button type="button" class="btn_edit" data-command="range" data-value="PRE" title="' + e.toolbar.pre + '">               <pre class="pre_ex">' + e.toolbar.pre + '</pre>           </button>       </li>       <li><button type="button" class="btn_edit" data-command="replace" data-value="H1" title="' + e.toolbar.tag_h + ' 1" style="height:40px;"><span class="h1_ex">' + e.toolbar.tag_h + ' 1</span></button></li>       <li><button type="button" class="btn_edit" data-command="replace" data-value="H2" title="' + e.toolbar.tag_h + ' 2" style="height:34px;"><span class="h2_ex">' + e.toolbar.tag_h + ' 2</span></button></li>       <li><button type="button" class="btn_edit" data-command="replace" data-value="H3" title="' + e.toolbar.tag_h + ' 3" style="height:26px;"><span class="h3_ex">' + e.toolbar.tag_h + ' 3</span></button></li>       <li><button type="button" class="btn_edit" data-command="replace" data-value="H4" title="' + e.toolbar.tag_h + ' 4" style="height:23px;"><span class="h4_ex">' + e.toolbar.tag_h + ' 4</span></button></li>       <li><button type="button" class="btn_edit" data-command="replace" data-value="H5" title="' + e.toolbar.tag_h + ' 5" style="height:19px;"><span class="h5_ex">' + e.toolbar.tag_h + ' 5</span></button></li>       <li><button type="button" class="btn_edit" data-command="replace" data-value="H6" title="' + e.toolbar.tag_h + ' 6" style="height:15px;"><span class="h6_ex">' + e.toolbar.tag_h + " 6</span></button></li>   </ul></div>",
                t
            },
            pickUp: function(e) {
                e.preventDefault(),
                e.stopPropagation();
                let t = e.target
                  , i = null
                  , n = null;
                for (; !i && !/UL/i.test(t.tagName); )
                    i = t.getAttribute("data-command"),
                    n = t.getAttribute("data-value"),
                    t = t.parentNode;
                if (i && n) {
                    if ("range" === i) {
                        const e = this.util.createElement(n);
                        this.applyRangeFormatElement(e)
                    } else {
                        const e = this.getRange()
                          , t = e.startOffset
                          , i = e.endOffset;
                        let o = this.getSelectedElementsAndComponents();
                        if (0 === o.length)
                            return;
                        let l = o[0]
                          , s = o[o.length - 1];
                        const a = this.util.getNodePath(e.startContainer, l)
                          , r = this.util.getNodePath(e.endContainer, s);
                        let c = {}
                          , d = !1
                          , u = !1;
                        const h = function(e) {
                            return !this.isComponent(e)
                        }
                        .bind(this.util);
                        for (let e, t, i, n, a = 0, r = o.length; a < r; a++) {
                            if (i = a === r - 1,
                            t = this.util.getRangeFormatElement(o[a], h),
                            n = this.util.isList(t),
                            !e && n)
                                c = {
                                    r: e = t,
                                    f: [this.util.getParentElement(o[a], "LI")]
                                },
                                0 === a && (d = !0);
                            else if (e && n)
                                if (e !== t) {
                                    const r = this.detachRangeFormatElement(c.r, c.f, null, !1, !0);
                                    d && (l = r.sc,
                                    d = !1),
                                    i && (s = r.ec),
                                    n ? (c = {
                                        r: e = t,
                                        f: [this.util.getParentElement(o[a], "LI")]
                                    },
                                    i && (u = !0)) : e = null
                                } else
                                    c.f.push(this.util.getParentElement(o[a], "LI")),
                                    i && (u = !0);
                            if (i && this.util.isList(e)) {
                                const e = this.detachRangeFormatElement(c.r, c.f, null, !1, !0);
                                (u || 1 === r) && (s = e.ec,
                                d && (l = e.sc || s))
                            }
                        }
                        this.setRange(this.util.getNodeFromPath(a, l), t, this.util.getNodeFromPath(r, s), i);
                        for (let e, t, i = 0, a = (o = this.getSelectedElementsAndComponents()).length; i < a; i++)
                            (e = o[i]).nodeName === n || this.util.isComponent(e) || ((t = this.util.createElement(n)).innerHTML = e.innerHTML,
                            e.parentNode.insertBefore(t, e),
                            this.util.removeItem(e)),
                            0 === i && (l = t || e),
                            i === a - 1 && (s = t || e),
                            t = null;
                        this.setRange(this.util.getNodeFromPath(a, l), t, this.util.getNodeFromPath(r, s), i),
                        this.history.push()
                    }
                    this.submenuOff()
                }
            }
        }
    },
    XJR1: function(e, t, i) {
        "use strict";
        i.r(t);
        i("3FqI"),
        i("WUQj");
        var n = i("WRt5")
          , o = i("50IV")
          , l = i("MIhV")
          , s = i("0A7J")
          , a = i("s0fJ")
          , r = i("g4XY")
          , c = i("gMuy")
          , d = i("VquE")
          , u = i("WzUB")
          , h = i("Rp48")
          , g = i("KKur")
          , m = i("hlhS")
          , p = {
            align: n.a,
            font: o.a,
            fontSize: l.a,
            fontColor: s.a,
            hiliteColor: a.a,
            horizontalRule: r.a,
            list: c.a,
            table: d.a,
            formatBlock: u.a,
            link: h.a,
            image: g.a,
            video: m.a
        }
          , _ = function(e, t) {
            const i = window
              , n = e.context.element.wysiwyg
              , o = e.util
              , l = e.context.tool.undo
              , s = e.context.tool.redo;
            let a = null
              , r = 0
              , c = [{
                contents: e.getContents(),
                s: {
                    path: [0, 0],
                    offset: 0
                },
                e: {
                    path: [0, 0],
                    offset: 0
                }
            }];

            function d() {
                const i = c[r];
                n.innerHTML = i.contents,
                e.setRange(o.getNodeFromPath(i.s.path, n), i.s.offset, o.getNodeFromPath(i.e.path, n), i.e.offset),
                e.focus(),
                0 === r ? (l && l.setAttribute("disabled", !0),
                s && s.removeAttribute("disabled")) : r === c.length - 1 ? (l && l.removeAttribute("disabled"),
                s && s.setAttribute("disabled", !0)) : (l && l.removeAttribute("disabled"),
                s && s.removeAttribute("disabled")),
                t()
            }
            return {
                push: function(flag) {
                    a && i.clearTimeout(a),
                    a = i.setTimeout(function() {
                        i.clearTimeout(a),
                        a = null,
                        function() {
                            const i = e.getContents();
                            if (!i || i === c[r].contents)
                                return;
                            if(!flag)r++;
                            const n = e.getRange();
                            c.length > r && (c = c.slice(0, r),
                            s && s.setAttribute("disabled", !0)),
                            c[r] = {
                                contents: i,
                                s: {
                                    path: o.getNodePath(n.startContainer),
                                    offset: n.startOffset
                                },
                                e: {
                                    path: o.getNodePath(n.endContainer),
                                    offset: n.endOffset
                                }
                            },
                            1 === r && l && l.removeAttribute("disabled"),
                            t()
                        }()
                    }, 100)
                },
                undo: function() {
                    r > 0 && (r--,
                    d())
                },
                redo: function() {
                    c.length - 1 > r && (r++,
                    d())
                },
                reset: function() {
                    c = c[r = 0]
                }
            }
        };
        const f = {
            _d: document,
            _w: window,
            _onlyZeroWidthRegExp: new RegExp("^" + String.fromCharCode(8203) + "+$"),
            _tagConvertor: function(e) {
                const t = {
                    b: "strong",
                    i: "em",
                    var: "em",
                    u: "ins",
                    strike: "del",
                    s: "del"
                };
                return e.replace(/(<\/?)(pre|blockquote|h[1-6]|ol|ul|dl|li|hr|b|strong|var|i|em|u|ins|s|strike|del|sub|sup)\b\s*(?:[^>^<]+)?\s*(?=>)/gi, function(e, i, n) {
                    return i + ("string" == typeof t[n] ? t[n] : n)
                })
            },
            zeroWidthSpace: "​",
            onlyZeroWidthSpace: function(e) {
                return "string" != typeof e && (e = e.textContent),
                "" === e || this._onlyZeroWidthRegExp.test(e)
            },
            getXMLHttpRequest: function() {
                if (!this._w.ActiveXObject)
                    return this._w.XMLHttpRequest ? new XMLHttpRequest : null;
                try {
                    return new ActiveXObject("Msxml2.XMLHTTP")
                } catch (e) {
                    try {
                        return new ActiveXObject("Microsoft.XMLHTTP")
                    } catch (e) {
                        return null
                    }
                }
            },
            createElement: function(e) {
                return this._d.createElement(e)
            },
            createTextNode: function(e) {
                return this._d.createTextNode(e || "")
            },
            getIncludePath: function(e, t) {
                let i = "";
                const n = []
                  , o = "js" === t ? "script" : "link"
                  , l = "js" === t ? "src" : "href";
                let s = "(?:";
                for (let t = 0, i = e.length; t < i; t++)
                    s += e[t] + (t < i - 1 ? "|" : ")");
                const a = new this._w.RegExp("(^|.*[\\/])" + s + "(\\.[^\\/]+)?." + t + "(?:\\?.*|;.*)?$","i")
                  , r = new this._w.RegExp(".+\\." + t + "(?:\\?.*|;.*)?$","i");
                for (let e = this._d.getElementsByTagName(o), t = 0; t < e.length; t++)
                    r.test(e[t][l]) && n.push(e[t]);
                for (let e = 0; e < n.length; e++) {
                    let t = n[e][l].match(a);
                    if (t) {
                        i = t[0];
                        break
                    }
                }
                if ("" === i && (i = n.length > 0 ? n[0][l] : ""),
                -1 === i.indexOf(":/") && "//" !== i.slice(0, 2) && (i = 0 === i.indexOf("/") ? location.href.match(/^.*?:\/\/[^\/]*/)[0] + i : location.href.match(/^[^\?]*\/(?:)/)[0] + i),
                !i)
                    throw "[SUNEDITOR.util.getIncludePath.fail] The SUNEDITOR installation path could not be automatically detected. (name: +" + name + ", extension: " + t + ")";
                return i
            },
            convertContentsForEditor: function(e) {
                let t, i, n = "";
                e = e.trim();
                for (let o = 0, l = (t = this._d.createRange().createContextualFragment(e).childNodes).length; o < l; o++)
                    if (i = t[o].outerHTML || t[o].textContent,
                    3 === t[o].nodeType) {
                        const e = i.split(/\n/g);
                        let t = "";
                        for (let i = 0, o = e.length; i < o; i++)
                            (t = e[i].trim()).length > 0 && (n += "<P>" + t + "</p>")
                    } else
                        n += i;
                const o = {
                    "&": "&amp;",
                    " ": "&nbsp;",
                    "'": "&quot;",
                    "<": "&lt;",
                    ">": "&gt;"
                };
                return e = e.replace(/&|\u00A0|'|<|>/g, function(e) {
                    return "string" == typeof o[e] ? o[e] : e
                }),
                0 === n.length && (n = "<p>" + (e.length > 0 ? e : "<br>") + "</p>"),n
//                this._tagConvertor(n.replace(this._deleteExclusionTags, ""))
            },
            convertHTMLForCodeView: function(e) {
                let t = "";
                const i = this._w.RegExp;
                return function e(n) {
                    const o = n.childNodes;
                    for (let n, l = 0, s = o.length; l < s; l++)
                        if (n = o[l],
                        /^(BLOCKQUOTE|TABLE|THEAD|TBODY|TR|OL|UL|FIGCAPTION)$/i.test(n.nodeName)) {
                            const o = n.nodeName.toLowerCase();
                            t += n.outerHTML.match(i("<" + o + "[^>]*>", "i"))[0] + "\n",
                            e(n),
                            t += "</" + o + ">\n"
                        } else
                            t += 3 === n.nodeType ? /^\n+$/.test(n.data) ? "" : n.data : n.outerHTML + "\n"
                }(e),
                t
            },
            isWysiwygDiv: function(e) {
                return !(!e || 1 !== e.nodeType || !this.hasClass(e, "sun-editor-id-wysiwyg"))
            },
            isFormatElement: function(e) {
                return !(!e || 1 !== e.nodeType || !/^(P|DIV|H[1-6]|LI)$/i.test(e.nodeName) || this.isComponent(e) || this.isWysiwygDiv(e))
            },
            isRangeFormatElement: function(e) {
                return !(!e || 1 !== e.nodeType || !/^(BLOCKQUOTE|OL|UL|PRE|FIGCAPTION|TABLE|THEAD|TBODY|TR|TH|TD)$/i.test(e.nodeName))
            },
            isComponent: function(e) {
                return e && (/sun-editor-id-comp/.test(e.className) || (/sun-editor-content/.test(e.className)) || /^(TABLE|HR)$/.test(e.nodeName))
            },
            getFormatElement: function(e, t) {
                if (!e)
                    return null;
                for (t || (t = function() {
                    return !0
                }
                ); e; ) {
                    if (this.isWysiwygDiv(e))
                        return null;
                    if (this.isRangeFormatElement(e) && e.firstElementChild,
                    this.isFormatElement(e) && t(e))
                        return e;
                    e = e.parentNode
                }
                return null
            },
            getRangeFormatElement: function(e, t) {
                if (!e)
                    return null;
                for (t || (t = function() {
                    return !0
                }
                ); e; ) {
                    if (this.isWysiwygDiv(e))
                        return null;
                    if (this.isRangeFormatElement(e) && !/^(THEAD|TBODY|TR)$/i.test(e.nodeName) && t(e))
                        return e;
                    e = e.parentNode
                }
                return null
            },
            getArrayIndex: function(e, t) {
                let i = -1;
                for (let n = 0, o = e.length; n < o; n++)
                    if (e[n] === t) {
                        i = n;
                        break
                    }
                return i
            },
            nextIdx: function(e, t) {
                let i = this.getArrayIndex(e, t);
                return -1 === i ? -1 : i + 1
            },
            prevIdx: function(e, t) {
                let i = this.getArrayIndex(e, t);
                return -1 === i ? -1 : i - 1
            },
            getPositionIndex: function(e) {
                let t = 0;
                for (; e = e.previousSibling; )
                    t += 1;
                return t
            },
            getNodePath: function(e, t) {
                const i = [];
                let n = !0;
                return this.getParentElement(e, function(e) {
                    return e === t && (n = !1),
                    n && !this.isWysiwygDiv(e) && i.push(e),
                    !1
                }
                .bind(this)),
                i.map(this.getPositionIndex).reverse()
            },
            getNodeFromPath: function(e, t) {
                let i, n = t;
                for (let t = 0, o = e.length; t < o && 0 !== (i = n.childNodes).length; t++)
                    n = i.length <= e[t] ? i[i.length - 1] : i[e[t]];
                return n
            },
            isList: function(e) {
                return e && /^(OL|UL)$/i.test("string" == typeof e ? e : e.nodeName)
            },
            isListCell: function(e) {
                return e && /^LI$/i.test("string" == typeof e ? e : e.nodeName)
            },
            isTable: function(e) {
                return e && /^(TABLE|THEAD|TBODY|TR|TH|TD)$/i.test("string" == typeof e ? e : e.nodeName)
            },
            isCell: function(e) {
                return e && /^(TD|TH)$/i.test("string" == typeof e ? e : e.nodeName)
            },
            isBreak: function(e) {
                return e && /^BR$/i.test("string" == typeof e ? e : e.nodeName)
            },
            getListChildren: function(e, t) {
                const i = [];
                return e && e.children ? (t = t || function() {
                    return !0
                }
                ,
                function n(o) {
                    (e !== o && t(o) || /^BR$/i.test(e.nodeName)) && i.push(o);
                    for (let e = 0, t = o.children.length; e < t; e++)
                        n(o.children[e])
                }(e),
                i) : i
            },
            getListChildNodes: function(e, t) {
                const i = [];
                return e && e.childNodes ? (t = t || function() {
                    return !0
                }
                ,
                function n(o) {
                    (e !== o && t(o) || /^BR$/i.test(e.nodeName)) && i.push(o);
                    for (let e = 0, t = o.childNodes.length; e < t; e++)
                        n(o.childNodes[e])
                }(e),
                i) : i
            },
            getElementDepth: function(e) {
                let t = 0;
                for (e = e.parentNode; e && !this.isWysiwygDiv(e); )
                    t += 1,
                    e = e.parentNode;
                return t
            },
            getParentElement: function(e, t) {
                let i;
                if ("function" == typeof t)
                    i = t;
                else {
                    let e;
                    /\./.test(t) ? (e = "className",
                    t = t.split(".")[1]) : /#/.test(t) ? (e = "id",
                    t = "^" + t.split("#")[1] + "$") : /:/.test(t) ? (e = "name",
                    t = "^" + t.split(":")[1] + "$") : (e = "tagName",
                    t = "^" + t + "$");
                    const n = new this._w.RegExp(t,"i");
                    i = function(t) {
                        return n.test(t[e])
                    }
                }
                for (; e && !i(e); ) {
                    if (this.isWysiwygDiv(e))
                        return null;
                    e = e.parentNode
                }
                return e
            },
            getChildElement: function(e, t, i) {
                let n;
                if ("function" == typeof t)
                    n = t;
                else {
                    let e;
                    /\./.test(t) ? (e = "className",
                    t = t.split(".")[1]) : /#/.test(t) ? (e = "id",
                    t = "^" + t.split("#")[1] + "$") : /:/.test(t) ? (e = "name",
                    t = "^" + t.split(":")[1] + "$") : (e = "tagName",
                    t = "^" + t + "$");
                    const i = new this._w.RegExp(t,"i");
                    n = function(t) {
                        return i.test(t[e])
                    }
                }
                const o = this.getListChildNodes(e, function(e) {
                    return n(e)
                });
                return o[i ? o.length - 1 : 0]
            },
            getEdgeChildNodes: function(e, t) {
                if (e) {
                    for (t || (t = e); e && 1 === e.nodeType && e.childNodes.length > 0 && !this.isBreak(e); )
                        e = e.firstChild;
                    for (; t && 1 === t.nodeType && t.childNodes.length > 0 && !this.isBreak(t); )
                        t = t.lastChild;
                    return {
                        sc: e,
                        ec: t || e
                    }
                }
            },
            getOffset: function(e) {
                let t = 0
                  , i = 0
                  , n = 3 === e.nodeType ? e.parentElement : e;
                for (; !this.isWysiwygDiv(n.parentNode); )
                    (/^(A|TD|TH|FIGURE|FIGCAPTION|IMG|IFRAME)$/i.test(n.nodeName) || /relative/i.test(n.style.position)) && (t += n.offsetLeft,
                    i += n.offsetTop),
                    n = n.parentNode;
                return {
                    left: t,
                    top: i - n.parentNode.scrollTop
                }
            },
            changeTxt: function(e, t) {
                e && t && (e.textContent = t)
            },
            hasClass: function(e, t) {
                if (e)
                    return e.classList.contains(t.trim())
            },
            addClass: function(e, t) {
                if (!e)
                    return;
                new this._w.RegExp("(\\s|^)" + t + "(\\s|$)").test(e.className) || (e.className += " " + t)
            },
            removeClass: function(e, t) {
                if (!e)
                    return;
                const i = new this._w.RegExp("(\\s|^)" + t + "(\\s|$)");
                e.className = e.className.replace(i, " ").trim()
            },
            toggleClass: function(e, t) {
                if (!e)
                    return;
                const i = new this._w.RegExp("(\\s|^)" + t + "(\\s|$)");
                i.test(e.className) ? e.className = e.className.replace(i, " ").trim() : e.className += " " + t
            },
            removeItem: function(e) {
                if (e)
                    try {
//                    	if(e.classList.contains('sun-editor-id-image-container')){
//                    		var imgConfig = e.children[0].children[0];
//                    		if(imgConfig != null){
//                    			if(imgConfig.getAttribute("file_id") != null){
//                    				let deleteFileId = imgConfig.getAttribute("file_id")
//                    				Note.noteFileDelete(deleteFileId)
//                    			}
//                    		}
//                    	}
//                    	if(e.classList.contains('sun-editor-content')){
//                    		var fileConfig = e.children[0].children[0];
//                    		if(fileConfig != null){
//                    			if(fileConfig.id != null){
//                    				let deleteFileID = fileConfig.id;
//                    				Note.noteFileDelete(deleteFileID)
//                    			}
//                    		}
//                    	}
                        e.remove()
                    } catch (t) {
                        e.parentNode.removeChild(e)
                    }
            },
            removeItemAllParents: function(e, t) {
                if (!e)
                    return null;
                let i = null;
                return t || (t = function(e) {
                    const t = e.textContent.trim();
                    return 0 === t.length || /^(\n|\u200B)+$/.test(t)
                }
                ),
                function e(n) {
                    if (!f.isWysiwygDiv(n)) {
                        const o = n.parentNode;
                        o && t(n) && (i = {
                            sc: n.previousElementSibling,
                            ec: n.nextElementSibling
                        },
                        f.removeItem(n),
                        e(o))
                    }
                }(e),
                i
            },
            removeEmptyNode: function(e) {
                const t = this;
                !function i(n) {
                    if (n === e || !t.onlyZeroWidthSpace(n.textContent) || /^BR$/i.test(n.nodeName) || n.firstChild && /^BR$/i.test(n.firstChild.nodeName) || t.isComponent(n)) {
                        const e = n.children;
                        for (let n = 0, o = e.length, l = 0; n < o; n++)
                            e[n + l] && !t.isComponent(e[n + l]) && (l += i(e[n + l]))
                    } else if (n.parentNode)
                        return n.parentNode.removeChild(n),
                        -1;
                    return 0
                }(e),
                0 === e.childNodes.length && (e.innerHTML = this.zeroWidthSpace)
            },
            ignoreNodeChange: function(e) {
                return f.isComponent(e) || f.isFormatElement(e) || /^(IMG|VIDEO)$/i.test(e.nodeName)
            },
            cleanHTML: function(e) {
                const t = new this._w.RegExp("^(meta|script|link|style|[a-z]+:[a-z]+)$","i")
                  , i = this._d.createRange().createContextualFragment(e).children;
                let n = "";
                for (let e = 0, o = i.length; e < o; e++)
                    t.test(i[e].nodeName) || (n += i[e].outerHTML);
                return n = n.replace(/<([a-zA-Z]+\:[a-zA-Z]+|script|style).*>(\n|.)*<\/([a-zA-Z]+\:[a-zA-Z]+|script|style)>/g, "").replace(/(<[a-zA-Z]+)[^>]*(?=>)/g, function(e, t) {
                    const i = e.match(/((?:colspan|rowspan|target|href|src)\s*=\s*"[^"]*")/gi);
                    if (i)
                        for (let e = 0, n = i.length; e < n; e++)
                            t += " " + i[e];
                    return t
                }).replace(this._deleteExclusionTags, ""),
                this._tagConvertor(n || e)
            },
            _deleteExclusionTags: function() {
                const e = "br|p|div|pre|blockquote|h[1-6]|ol|ul|dl|li|hr|figure|figcaption|img|iframe|video|table|thead|tbody|tr|th|td|a|b|strong|var|i|em|u|ins|s|strike|del|sub|sup".split("|");
                let t = "</?(";
                for (let i = 0, n = e.length; i < n; i++)
                    t += "(?!\\b" + e[i] + "\\b)";
                return t += "[^>^<])+>",
                new RegExp(t,"g")
            }()
        };
        var b = f
          , y = i("PAX9");
        var v = {
            init: function(e, t, i, n) {
                "object" != typeof t && (t = {}),
                t.lang = i,
                t.mode = t.mode || "classic",
                t.toolbarWidth = t.toolbarWidth ? /^\d+$/.test(t.toolbarWidth) ? t.toolbarWidth + "px" : t.toolbarWidth : "max-content",
                t.stickyToolbar = /balloon/i.test(t.mode) ? -1 : void 0 === t.stickyToolbar ? 0 : /\d+/.test(t.stickyToolbar) ? 1 * t.stickyToolbar.toString().match(/\d+/)[0] : -1,
                t.resizingBar = !/inline|balloon/i.test(t.mode) && (void 0 === t.resizingBar || t.resizingBar),
                t.showPathLabel = "boolean" != typeof t.showPathLabel || t.showPathLabel,
                t.popupDisplay = t.popupDisplay || "full",
                t.display = t.display || ("none" !== e.style.display && e.style.display ? e.style.display : "block"),
                t.width = t.width ? /^\d+$/.test(t.width) ? t.width + "px" : t.width : e.clientWidth ? e.clientWidth + "px" : "100%",
                t.height = t.height ? /^\d+$/.test(t.height) ? t.height + "px" : t.height : e.clientHeight ? e.clientHeight + "px" : "auto",
                t.minHeight = (/^\d+$/.test(t.minHeight) ? t.height + "px" : t.minHeight) || "",
                t.maxHeight = (/^\d+$/.test(t.maxHeight) ? t.maxHeight + "px" : t.maxHeight) || "",
                t.font = t.font || null,
                t.fontSize = t.fontSize || null,
                t.colorList = t.colorList || null,
                t.imageResizing = void 0 === t.imageResizing || t.imageResizing,
                t.imageWidth = t.imageWidth || "auto",
                t.imageFileInput = void 0 === t.imageFileInput || t.imageFileInput,
                t.imageUrlInput = void 0 === t.imageUrlInput || !t.imageFileInput || t.imageUrlInput,
                t.imageUploadHeader = t.imageUploadHeader || null,
                t.imageUploadUrl = t.imageUploadUrl || null,
                t.videoResizing = void 0 === t.videoResizing || t.videoResizing,
                t.videoWidth = t.videoWidth || 560,
                t.videoHeight = t.videoHeight || 315,
                t.youtubeQuery = t.youtubeQuery || "",
                t.buttonList = t.buttonList || [["undo", "redo"], ["bold", "underline", "italic", "strike", "subscript", "superscript"], ["removeFormat"], ["outdent", "indent"], ["fullScreen", "showBlocks", "codeView"], ["preview", "print"]];
                const o = document
                  , l = o.createElement("DIV");
                l.className = "sun-editor",
                e.id && (l.id = "suneditor_" + e.id),
                l.style.width = t.width,
                l.style.display = t.display;
                const s = o.createElement("DIV");
                s.className = "sun-editor-container";
                const a = this._createToolBar(o, t.buttonList, n, i);
                let r = null;
                /inline|balloon/i.test(t.mode) && (a.element.className += " sun-inline-toolbar",
                a.element.style.width = t.toolbarWidth,
                /balloon/i.test(t.mode) && ((r = o.createElement("DIV")).className = "arrow",
                a.element.appendChild(r)));
                const c = o.createElement("DIV");
                c.className = "sun-editor-sticky-dummy";
                const d = o.createElement("DIV");
                d.className = "sun-editor-id-editorArea";
                const u = o.createElement("DIV");
                u.setAttribute("contenteditable", !0),
                u.setAttribute("id","sun_editorEdit"),
                u.setAttribute("scrolling", "auto"),
                u.className = "input_editor sun-editor-id-wysiwyg sun-editor-editable",
                u.style.display = "block",
                u.innerHTML = b.convertContentsForEditor(e.value),
                u.style.height = t.height,
                u.style.minHeight = t.minHeight,
                u.style.maxHeight = t.maxHeight;
                const h = o.createElement("TEXTAREA");
                h.className = "input_editor sun-editor-id-code",
                h.style.display = "none",
                h.style.height = t.height,
                h.style.minHeight = t.minHeight,
                h.style.maxHeight = t.maxHeight;
                let g = null;
                t.resizingBar && ((g = o.createElement("DIV")).className = "sun-editor-id-resizingBar sun-editor-common");
                const m = o.createElement("DIV");
                m.className = "sun-editor-id-navigation sun-editor-common";
                const p = o.createElement("DIV");
                p.className = "sun-editor-id-loading sun-editor-common",
                p.innerHTML = '<div class="loading-effect"></div>';
                const _ = o.createElement("DIV");
                return _.className = "sun-editor-id-resize-background",
                d.appendChild(u),
                d.appendChild(h),
                s.appendChild(a.element),
                s.appendChild(c),
                s.appendChild(d),
                s.appendChild(_),
                s.appendChild(p),
                g && (g.appendChild(m),
                s.appendChild(g)),
                l.appendChild(s),
                {
                    constructed: {
                        _top: l,
                        _relative: s,
                        _toolBar: a.element,
                        _editorArea: d,
                        _wysiwygArea: u,
                        _codeArea: h,
                        _resizingBar: g,
                        _navigation: m,
                        _loading: p,
                        _resizeBack: _,
                        _stickyDummy: c,
                        _arrow: r
                    },
                    options: t,
                    plugins: a.plugins,
                    pluginCallButtons: a.pluginCallButtons
                }
            },
            _defaultButtons: function(e) {
                return {
                    bold: ["sun-editor-id-bold", e.toolbar.bold , "STRONG", "", '<i class="icon-bold"></i>'],
                    underline: ["sun-editor-id-underline", e.toolbar.underline , "INS", "", '<i class="icon-underline"></i>'],
                    italic: ["sun-editor-id-italic", e.toolbar.italic , "EM", "", '<i class="icon-italic"></i>'],
                    strike: ["sun-editor-id-strike", e.toolbar.strike , "DEL", "", '<i class="icon-strokethrough"></i>'],
                    subscript: ["sun-editor-id-subscript", e.toolbar.subscript, "SUB", "", '<i class="icon-subscript"></i>'],
                    superscript: ["sun-editor-id-superscript", e.toolbar.superscript, "SUP", "", '<i class="icon-superscript"></i>'],
                    removeFormat: ["", e.toolbar.removeFormat, "removeFormat", "", '<i class="icon-erase"></i>'],
                    indent: ["", e.toolbar.indent , "indent", "", '<i class="icon-indent-right"></i>'],
                    outdent: ["sun-editor-id-outdent", e.toolbar.outdent , "outdent", "", '<i class="icon-indent-left"></i>'],
                    fullScreen: ["code-view-enabled", e.toolbar.fullScreen, "fullScreen", "", '<i class="icon-expansion"></i>'],
                    showBlocks: ["", e.toolbar.showBlocks, "showBlocks", "", '<i class="icon-showBlocks"></i>'],
                    codeView: ["code-view-enabled", e.toolbar.codeView, "codeView", "", '<i class="icon-code-view"></i>'],
                    undo: ["sun-editor-id-undo", e.toolbar.undo , "undo", "", '<i class="icon-undo"></i>', !0],
                    redo: ["sun-editor-id-redo", e.toolbar.redo , "redo", "", '<i class="icon-redo"></i>', !0],
                    preview: ["", e.toolbar.preview, "preview", "", '<i class="icon-preview"></i>'],
                    print: ["", e.toolbar.print, "print", "", '<i class="icon-print"></i>'],
                    save: ["sun-editor-id-save", e.toolbar.save, "save", "", '<i class="icon-save"></i>', !0],
                    font: ["btn_editor_select btn_font sun-editor-id-font-family", e.toolbar.font, "font", "submenu", '<span class="txt">' + e.toolbar.font + '</span><i class="icon-arrow-down"></i>'],
                    formatBlock: ["btn_editor_select btn_format", e.toolbar.formats, "formatBlock", "submenu", '<span class="txt sun-editor-id-format">' + e.toolbar.formats + '</span><i class="icon-arrow-down"></i>'],
                    fontSize: ["btn_editor_select btn_size", e.toolbar.fontSize, "fontSize", "submenu", '<span class="txt sun-editor-id-font-size">' + e.toolbar.fontSize + '</span><i class="icon-arrow-down"></i>'],
                    fontColor: ["", e.toolbar.fontColor, "fontColor", "submenu", '<i class="icon-fontColor"></i>'],
                    hiliteColor: ["", e.toolbar.hiliteColor, "hiliteColor", "submenu", '<i class="icon-hiliteColor"></i>'],
                    align: ["btn_align", e.toolbar.align, "align", "submenu", '<i class="icon-align-left sun-editor-id-align"></i>'],
                    list: ["sun-editor-id-list", e.toolbar.list, "list", "submenu", '<i class="icon-list-number"></i>'],
                    horizontalRule: ["btn_line", e.toolbar.horizontalRule, "horizontalRule", "submenu", '<i class="icon-hr"></i>'],
                    table: ["", e.toolbar.table, "table", "submenu", '<i class="icon-grid"></i>'],
                    link: ["", e.toolbar.link, "link", "dialog", '<i class="icon-link"></i>'],
                    image: ["", e.toolbar.image, "image", "dialog", '<i class="icon-image"></i>'],
                    video: ["", e.toolbar.video, "video", "dialog", '<i class="icon-video"></i>']
                }
            },
            _createModuleGroup: function(e) {
                const t = b.createElement("DIV");
                t.className = "tool_module" + (e ? "" : " sun-editor-module-border");
                const i = b.createElement("UL");
                return i.className = "editor_tool",
                i.id = "editor_toolbar",
                t.appendChild(i),
                {
                    div: t,
                    ul: i
                }
            },
            _createButton: function(e, t, i, n, o, l) {
                const s = b.createElement("LI")
                  , a = b.createElement("BUTTON");
                return a.setAttribute("type", "button"),
                a.setAttribute("class", "btn_editor" + ("submenu" === n ? " btn_submenu" : "") + (e ? " " + e : "") + " se-tooltip"),
                a.setAttribute("data-command", i),
                a.setAttribute("data-display", n),
                o += '<span class="se-tooltip-inner"><span class="se-tooltip-text">' + t + "</span></span>",
                l && a.setAttribute("disabled", !0),
                a.innerHTML = o,
                s.appendChild(a),
                {
                    li: s,
                    button: a
                }
            },
            _createToolBar: function(e, t, i, n) {
                const o = e.createElement("DIV");
                o.className = "sun-editor-toolbar-separator-vertical";
                const l = e.createElement("DIV");
                l.className = "sun-editor-id-toolbar sun-editor-common";
                const s = this._defaultButtons(n)
                  , a = {}
                  , r = {};
                if (i) {
                    const e = i.length ? i : Object.keys(i).map(function(e) {
                        return i[e]
                    });
                    for (let t = 0, i = e.length; t < i; t++)
                        r[e[t].name] = e[t]
                }
                let c = null
                  , d = null
                  , u = null
                  , h = null
                  , g = ""
                  , m = !1;
                const p = 1 === t.length;
                for (let i = 0; i < t.length; i++) {
                    const n = t[i];
                    if (u = this._createModuleGroup(p),
                    "object" == typeof n) {
                        for (let e = 0; e < n.length; e++)
                            "object" == typeof (d = n[e]) ? "function" == typeof d.add ? (c = s[g = d.name],
                            r[g] = d) : (g = d.name,
                            c = [d.buttonClass, d.title, d.dataCommand, d.dataDisplay, d.innerHTML]) : (c = s[d],
                            g = d),
                            h = this._createButton(c[0], c[1], c[2], c[3], c[4], c[5]),
                            u.ul.appendChild(h.li),
                            r[g] && (a[g] = h.button);
                        m && l.appendChild(o.cloneNode(!1)),
                        l.appendChild(u.div),
                        m = !0
                    } else if (/^\/$/.test(n)) {
                        const t = e.createElement("DIV");
                        t.className = "tool_module_enter",
                        l.appendChild(t),
                        m = !1
                    }
                }
                const _ = e.createElement("DIV");
                return _.className = "sun-editor-id-toolbar-cover",
                l.appendChild(_),
                {
                    element: l,
                    plugins: r,
                    pluginCallButtons: a
                }
            }
        };
        var x = function(e, t, i) {
            return {
                element: {
                    originElement: e,
                    topArea: t._top,
                    relative: t._relative,
                    toolbar: t._toolBar,
                    resizingBar: t._resizingBar,
                    navigation: t._navigation,
                    editorArea: t._editorArea,
                    wysiwyg: t._wysiwygArea,
                    code: t._codeArea,
                    loading: t._loading,
                    resizeBackground: t._resizeBack,
                    _stickyDummy: t._stickyDummy,
                    _arrow: t._arrow
                },
                tool: {
                    cover: t._toolBar.getElementsByClassName("sun-editor-id-toolbar-cover")[0],
                    bold: t._toolBar.getElementsByClassName("sun-editor-id-bold")[0],
                    underline: t._toolBar.getElementsByClassName("sun-editor-id-underline")[0],
                    italic: t._toolBar.getElementsByClassName("sun-editor-id-italic")[0],
                    strike: t._toolBar.getElementsByClassName("sun-editor-id-strike")[0],
                    subscript: t._toolBar.getElementsByClassName("sun-editor-id-subscript")[0],
                    superscript: t._toolBar.getElementsByClassName("sun-editor-id-superscript")[0],
                    font: t._toolBar.querySelector(".sun-editor-id-font-family .txt"),
                    fontTooltip: t._toolBar.querySelector(".sun-editor-id-font-family .se-tooltip-text"),
                    format: t._toolBar.getElementsByClassName("sun-editor-id-format")[0],
                    fontSize: t._toolBar.getElementsByClassName("sun-editor-id-font-size")[0],
                    align: t._toolBar.getElementsByClassName("sun-editor-id-align")[0],
                    list: t._toolBar.getElementsByClassName("sun-editor-id-list")[0],
                    undo: t._toolBar.getElementsByClassName("sun-editor-id-undo")[0],
                    redo: t._toolBar.getElementsByClassName("sun-editor-id-redo")[0],
                    save: t._toolBar.getElementsByClassName("sun-editor-id-save")[0],
                    outdent: t._toolBar.getElementsByClassName("sun-editor-id-outdent")[0]
                },
                option: {
                    mode: i.mode,
                    toolbarWidth: i.toolbarWidth,
                    stickyToolbar: i.stickyToolbar,
                    resizingBar: i.resizingBar,
                    showPathLabel: i.showPathLabel,
                    popupDisplay: i.popupDisplay,
                    display: i.display,
                    height: i.height,
                    minHeight: i.minHeight,
                    maxHeight: i.maxHeight,
                    font: i.font,
                    fontSize: i.fontSize,
                    colorList: i.colorList,
                    imageResizing: i.imageResizing,
                    imageWidth: i.imageWidth,
                    imageFileInput: i.imageFileInput,
                    imageUrlInput: i.imageUrlInput,
                    imageUploadHeader: i.imageUploadHeader,
                    imageUploadUrl: i.imageUploadUrl,
                    videoResizing: i.videoResizing,
                    videoWidth: i.videoWidth,
                    videoHeight: i.videoHeight,
                    youtubeQuery: i.youtubeQuery.replace("?", ""),
                    callBackSave: i.callBackSave
                }
            }
        }
          , C = i("P6u4")
          , E = i.n(C)
          , k = {
            init: function(e) {
                return {
                    create: function(t, i,callback) {
                        return this.create(t, i, e,callback)
                    }
                    .bind(this)
                }
            },
            create: function(e, t, i,callback) {
                "object" != typeof t && (t = {}),
                i && (t = [i, t].reduce(function(e, t) {
                    return Object.keys(t).forEach(function(i) {
                        e[i] = t[i]
                    }),
                    e
                }, {}));
                const n = "string" == typeof e ? document.getElementById(e) : e;
                if(typeof callback === "function") callback()
                if (!n) {
                    if ("string" == typeof e)
                        throw Error('[SUNEDITOR.create.fail] The element for that id was not found (ID:"' + e + '")');
                    throw Error("[SUNEDITOR.create.fail] suneditor requires textarea's element or id value")
                }
                const o = v.init(n, t, t.lang || E.a, t.plugins);
                if (o.constructed._top.id && document.getElementById(o.constructed._top.id))
                    throw Error('[SUNEDITOR.create.fail] The ID of the suneditor you are trying to create already exists (ID:"' + o.constructed._top.id + '")');
                return n.style.display = "none",
                o.constructed._top.style.display = "block",
                "object" == typeof n.nextElementSibling ? n.parentNode.insertBefore(o.constructed._top, n.nextElementSibling) : n.parentNode.appendChild(o.constructed._top),
                function(e, t, i, n) {
                    const o = document
                      , l = window
                      , s = b
                      , a = {
                        context: e,
                        plugins: i || {},
                        util: s,
                        initPlugins: {},
                        lang: n,
                        submenu: null,
                        _resizingName: "",
                        _submenuName: "",
                        _bindedSubmenuOff: null,
                        submenuActiveButton: null,
                        controllerArray: [],
                        codeViewDisabledButtons: e.element.toolbar.querySelectorAll('.sun-editor-id-toolbar button:not([class~="code-view-enabled"])'),
                        history: null,
                        _bindControllersOff: null,
                        _isInline: /inline/i.test(e.option.mode),
                        _isBalloon: /balloon/i.test(e.option.mode),
                        _inlineToolbarAttr: {
                            width: 0,
                            height: 0,
                            isShow: !1
                        },
                        _notHideToolbar: !1,
                        _imageUpload: function(e, t, i, n) {
                            "function" == typeof c.onImageUpload && c.onImageUpload(e, 1 * t, i, n)
                        },
                        _imageUploadError: function(e, t) {
                            return "function" != typeof c.onImageUploadError || c.onImageUploadError(e, t)
                        },
                        commandMap: {
                            FORMAT: e.tool.format,
                            FONT: e.tool.font,
                            FONT_TOOLTIP: e.tool.fontTooltip,
                            SIZE: e.tool.fontSize,
                            ALIGN: e.tool.align,
                            LI: e.tool.list,
                            STRONG: e.tool.bold,
                            INS: e.tool.underline,
                            EM: e.tool.italic,
                            DEL: e.tool.strike,
                            SUB: e.tool.subscript,
                            SUP: e.tool.superscript,
                            OUTDENT: e.tool.outdent
                        },
                        _variable: {
                            wysiwygActive: !0,
                            isFullScreen: !1,
                            innerHeight_fullScreen: 0,
                            resizeClientY: 0,
                            tabSize: 4,
                            minResizingSize: 65,
                            currentNodes: [],
                            _range: null,
                            _selectionNode: null,
                            _originCssText: e.element.topArea.style.cssText,
                            _bodyOverflow: "",
                            _editorAreaOriginCssText: "",
                            _wysiwygOriginCssText: "",
                            _codeOriginCssText: "",
                            _sticky: !1,
                            _fullScreenSticky: !1,
                            _imagesInfo: [],
                            _imageIndex: 0
                        },
                        callPlugin: function(e, i) {
                            if (!this.plugins[e])
                                throw Error('[SUNEDITOR.core.callPlugin.fail] The called plugin does not exist or is in an invalid format. (pluginName:"' + e + '")');
                            this.initPlugins[e] || (this.plugins[e].add(this, t[e]),
                            this.initPlugins[e] = !0),
                            i()
                        },
                        addModule: function(e) {
                            let t = "";
                            for (let i = 0, n = e.length; i < n; i++)
                                t = e[i].name,
                                this.plugins[t] || (this.plugins[t] = e[i],
                                this.plugins[t].add(this))
                        },
                        submenuOn: function(e) {
                            this._bindedSubmenuOff && this._bindedSubmenuOff();
                            const t = this._submenuName = e.getAttribute("data-command");
                            this.plugins[t].on && this.plugins[t].on.call(this),
                            this.submenu = e.nextElementSibling,
                            this.submenu.style.display = "block",
                            s.addClass(e, "on"),
                            this.submenuActiveButton = e;
                            const i = this.context.element.toolbar.offsetWidth - (e.parentElement.offsetLeft + this.submenu.offsetWidth);
                            this.submenu.style.left = i < 0 ? i + "px" : "1px",
                            this._bindedSubmenuOff = this.submenuOff.bind(this),
                            o.addEventListener("mousedown", this._bindedSubmenuOff, !1)
                        },
                        submenuOff: function() {
                            o.removeEventListener("mousedown", this._bindedSubmenuOff),
                            this._bindedSubmenuOff = null,
                            this.submenu && (this._submenuName = "",
                            this.submenu.style.display = "none",
                            this.submenu = null,
                            s.removeClass(this.submenuActiveButton, "on"),
                            this.submenuActiveButton = null,
                            this._notHideToolbar = !1),
                            this.focus()
                        },
                        controllersOn: function() {
                            this._bindControllersOff && this._bindControllersOff();
                            for (let e = 0; e < arguments.length; e++)
                                arguments[e].style.display = "block",
                                this.controllerArray[e] = arguments[e];
                            this._bindControllersOff = this.controllersOff.bind(this),
                            o.addEventListener("mousedown", this._bindControllersOff, !1),
                            o.addEventListener("keydown", this._bindControllersOff, !1)
                        },
                        controllersOff: function() {
                            o.removeEventListener("mousedown", this._bindControllersOff),
                            o.removeEventListener("keydown", this._bindControllersOff),
                            this._bindControllersOff = null;
                            const e = this.controllerArray.length;
                            if (e > 0) {
                                for (let t = 0; t < e; t++)
                                    this.controllerArray[t].style.display = "none";
                                this.controllerArray = []
                            }
                            this._resizingName = ""
                        },
                        execCommand: function(e, t, i) {
                            o.execCommand(e, t, "formatBlock" === e ? "<" + i + ">" : i),
                            this.history.push()
                        },
                        focus: function() {
                            if ("none" === e.element.wysiwyg.style.display)
                                return;
                            const t = s.getParentElement(this.getSelectionNode(), "figcaption");
                            t ? t.focus() : e.element.wysiwyg.focus(),
                            this._editorRange(),
                            r._findButtonEffectTag()
                        },
                        setRange: function(e, t, i, n) {
                            if (!e || !i)
                                return;
                            t > e.textContent.length && (t = e.textContent.length),
                            n > i.textContent.length && (n = i.textContent.length);
                            const s = o.createRange();
                            s.setStart(e, t),
                            s.setEnd(i, n);
                            const a = l.getSelection();
                            a.removeAllRanges && a.removeAllRanges(),
                            a.addRange(s),
                            this._editorRange()
                        },
                        getRange: function() {
                            return this._variable._range || this._createDefaultRange()
                        },
                        getSelectionNode: function() {
                            return this._variable._selectionNode || e.element.wysiwyg.firstChild
                        },
                        _editorRange: function() {
                            const e = l.getSelection();
                            let t = null
                              , i = null;
                            t = e.rangeCount > 0 ? e.getRangeAt(0) : this._createDefaultRange(),
                            this._variable._range = t,
                            i = t.collapsed ? t.commonAncestorContainer : e.extentNode || e.anchorNode,
                            this._variable._selectionNode = i
                        },
                        _createDefaultRange: function() {
                            const t = o.createRange();
                            return e.element.wysiwyg.firstChild || this.execCommand("formatBlock", !1, "P"),
                            t.setStart(e.element.wysiwyg.firstChild, 0),
                            t.setEnd(e.element.wysiwyg.firstChild, 0),
                            t
                        },
                        getSelectedElements: function(t) {
                            let i = this.getRange();
                            if (s.isWysiwygDiv(i.startContainer)) {
                                const t = e.element.wysiwyg.children;
                                if (0 === t.length)
                                    return null;
                                this.setRange(t[0], 0, t[t.length - 1], t[t.length - 1].textContent.trim().length),
                                i = this.getRange()
                            }
                            const n = i.startContainer
                              , o = i.endContainer
                              , l = i.commonAncestorContainer
                              , a = []
                              , r = s.getListChildren(l, function(e) {
                                return t ? t(e) : s.isFormatElement(e)
                            });
                            if (s.isWysiwygDiv(l) || s.isRangeFormatElement(l) || r.unshift(s.getFormatElement(l)),
                            n === o || 1 === r.length)
                                return r;
                            let c = s.getFormatElement(n)
                              , d = s.getFormatElement(o)
                              , u = null
                              , h = null;
                            const g = function(e) {
                                return !s.isTable(e) || /^TABLE$/i.test(e.nodeName)
                            }
                              , m = s.getRangeFormatElement(c, g)
                              , p = s.getRangeFormatElement(d, g)
                              , _ = m === p;
                            for (let e, t = 0, i = r.length; t < i; t++)
                                if (c === (e = r[t]) || !_ && e === m)
                                    u = t;
                                else if (d === e || !_ && e === p) {
                                    h = t;
                                    break
                                }
                            null === u && (u = 0),
                            null === h && (h = r.length - 1);
                            for (let e = u; e <= h; e++)
                                a.push(r[e]);
                            return a
                        },
                        getSelectedElementsAndComponents: function() {
                            const e = this.getRange().commonAncestorContainer
                              , t = s.getParentElement(e, s.isComponent);
                            return s.isTable(e) ? this.getSelectedElements() : this.getSelectedElements(function(e) {
                                const i = this.getParentElement(e, this.isComponent);
                                return this.isFormatElement(e) && (!i || i === t) || this.isComponent(e) && !this.getFormatElement(e)
                            }
                            .bind(s))
                        },
                        isEdgePoint: function(e, t) {
                            return 0 === t || t === e.nodeValue.length
                        },
                        showLoading: function() {
                            e.element.loading.style.display = "block"
                        },
                        closeLoading: function() {
                            e.element.loading.style.display = "none"
                        },
                        appendFormatTag: function(e, t) {
                            const i = e
                              , n = s.getFormatElement(this.getSelectionNode())
                              , o = t || (s.isFormatElement(n) ? n.nodeName : "P")
                              , l = s.createElement(o);
                            return l.innerHTML = s.zeroWidthSpace,
                            s.isCell(i) ? i.insertBefore(l, e.nextElementSibling) : i.parentNode.insertBefore(l, i.nextElementSibling),
                            l
                        },
                        insertComponent: function(e) {
                            let t = null;
                            const i = this.getSelectionNode()
                              , n = s.getFormatElement(i);
                            if (s.isListCell(n))
                                if (/^HR$/i.test(e.nodeName)) {
                                    const t = s.createElement("LI")
                                      , i = s.createTextNode(s.zeroWidthSpace);
                                    t.appendChild(e),
                                    t.appendChild(i),
                                    n.parentNode.insertBefore(t, n.nextElementSibling),
                                    this.setRange(i, 1, i, 1)
                                } else
                                    this.insertNode(e, i === n ? null : i),
                                    t = s.createElement("LI"),
                                    n.parentNode.insertBefore(t, n.nextElementSibling);
                            else
                                this.insertNode(e, n),
                                t = this.appendFormatTag(e);
                            return t
                        },
                        insertNode: function(e, t) {
                            const i = this.getRange();
                            let n = null;
                            if (t)
                                n = t.parentNode,
                                t = t.nextElementSibling;
                            else {
                                const e = i.startContainer
                                  , o = i.startOffset
                                  , l = i.endContainer
                                  , a = i.endOffset
                                  , r = i.commonAncestorContainer;
                                if (n = e,
                                3 === e.nodeType && (n = e.parentNode),
                                i.collapsed)
                                    3 === r.nodeType ? t = r.splitText(a) : (null !== n.lastChild && s.isBreak(n.lastChild) && n.removeChild(n.lastChild),
                                    t = null);
                                else if (e === l) {
                                    t = this.isEdgePoint(l, a) ? l.nextSibling : l.splitText(a);
                                    let i = e;
                                    this.isEdgePoint(e, o) || (i = e.splitText(o)),
                                    n.removeChild(i)
                                } else
                                    for (this.removeNode(),
                                    n = r,
                                    t = l; t.parentNode !== r; )
                                        t = t.parentNode
                            }
                            try {
                                n.insertBefore(e, t)
                            } catch (t) {
                                n.appendChild(e)
                            } finally {
                                this.history.push()
                            }
                        },
                        removeNode: function() {
                            const e = this.getRange();
                            if (e.deleteContents)
                                return void e.deleteContents();
                            const t = e.startContainer
                              , i = e.startOffset
                              , n = e.endContainer
                              , o = e.endOffset
                              , l = e.commonAncestorContainer;
                            let a = null
                              , r = null;
                            const c = s.getListChildNodes(l);
                            let d = s.getArrayIndex(c, t)
                              , u = s.getArrayIndex(c, n);
                            for (let e = d + 1, n = t; e >= 0; e--)
                                c[e] === n.parentNode && c[e].firstChild === n && 0 === i && (d = e,
                                n = n.parentNode);
                            for (let e = u - 1, t = n; e > d; e--)
                                c[e] === t.parentNode && 1 === c[e].nodeType && (c.splice(e, 1),
                                t = t.parentNode,
                                --u);
                            for (let e = d; e <= u; e++) {
                                const l = c[e];
                                0 === l.length || 3 === l.nodeType && void 0 === l.data ? s.removeItem(l) : l !== t ? l !== n ? (s.removeItem(l),
                                this.history.push()) : (r = 1 === n.nodeType ? s.createTextNode(n.textContent) : s.createTextNode(n.substringData(o, n.length - o))).length > 0 ? n.data = r.data : s.removeItem(n) : (a = 1 === t.nodeType ? s.createTextNode(t.textContent) : s.createTextNode(t.substringData(0, i))).length > 0 ? t.data = a.data : s.removeItem(t)
                            }
                        },
                        applyRangeFormatElement: function(e) {
                            const t = this.getSelectedElementsAndComponents();
                            if (!t || 0 === t.length)
                                return;
                            let i, n, o, l = t[t.length - 1];
                            i = s.isRangeFormatElement(l) || s.isFormatElement(l) ? l : s.getRangeFormatElement(l) || s.getFormatElement(l),
                            s.isCell(i) ? (n = null,
                            o = i) : (n = i.nextSibling,
                            o = i.parentNode);
                            let a = s.getElementDepth(i)
                              , r = null;
                            const c = []
                              , d = function(e, t, i) {
                                let n = null;
                                return e === t || s.isTable(t) || (n = s.removeItemAllParents(t)),
                                n ? n.ec : i
                            };
                            for (let i, l, u, h, g = 0, m = t.length; g < m; g++)
                                if (i = t[g],
                                l = i.parentNode,
                                u = s.getElementDepth(i),
                                s.isList(l)) {
                                    if (null === r && (r = s.createElement(l.nodeName)),
                                    r.innerHTML += i.outerHTML,
                                    c.push(i),
                                    g === m - 1 || !this.util.getParentElement(t[g + 1], function(e) {
                                        return e === l
                                    })) {
                                        const t = this.detachRangeFormatElement(l, c, null, !0, !0);
                                        a >= u ? (a = u,
                                        (n = d(o = t.cc, l, t.ec)) && (o = n.parentNode)) : o === t.cc && (n = t.ec),
                                        o !== t.cc && void 0 !== (h = d(o, t.cc)) && (n = h),
                                        e.appendChild(r),
                                        r = null
                                    }
                                } else
                                    a >= u && (a = u,
                                    o = l,
                                    n = i.nextSibling),
                                    e.appendChild(i),
                                    o !== l && void 0 !== (h = d(o, l)) && (n = h);
                            o.insertBefore(e, n),
                            d(e, n),
                            this.history.push();
                            const u = this.util.getEdgeChildNodes(e.firstElementChild, e.lastElementChild);
                            t.length > 1 ? this.setRange(u.sc, 0, u.ec, u.ec.textContent.length) : this.setRange(u.ec, u.ec.textContent.length, u.ec, u.ec.textContent.length)
                        },
                        detachRangeFormatElement: function(e, t, i, n, o) {
                            const l = this.getRange()
                              , a = l.startOffset
                              , c = l.endOffset
                              , d = e.children
                              , u = e.parentNode;
                            let h = null
                              , g = null
                              , m = e.cloneNode(!1);
                            const p = s.isList(i);
                            let _ = !1;

                            function f(e, t, i) {
                                const n = t.childNodes;
                                let o = t.cloneNode(!1)
                                  , l = null
                                  , a = null;
                                for (; n[0]; )
                                    a = n[0],
                                    s.ignoreNodeChange(a) && !s.isListCell(o) ? (o.childNodes.length > 0 && (l || (l = o),
                                    e.insertBefore(o, i),
                                    o = t.cloneNode(!1)),
                                    e.insertBefore(a, i),
                                    l || (l = a)) : o.appendChild(a);
                                return o.childNodes.length > 0 && (e.insertBefore(o, i),
                                l || (l = o)),
                                l
                            }
                            for (let o, l = 0, a = d.length; l < a; l++)
                                if (o = d[l],
                                n && 0 === l && (h = t && t.length !== a && t[0] !== o ? m : e.previousSibling),
                                t && -1 === t.indexOf(o))
                                    m || (m = e.cloneNode(!1)),
                                    o = o.cloneNode(!0),
                                    m.appendChild(o);
                                else {
                                    if (m && m.children.length > 0 && (u.insertBefore(m, e),
                                    m = null),
                                    !p && s.isListCell(o)) {
                                        const t = o.innerHTML;
                                        (o = s.isCell(e.parentNode) ? s.createElement("DIV") : s.createElement("P")).innerHTML = t
                                    } else
                                        o = o.cloneNode(!0);
                                    n || (i ? (_ || (u.insertBefore(i, e),
                                    _ = !0),
                                    o = f(i, o, null)) : o = f(u, o, e),
                                    t ? (g = o,
                                    h || (h = o)) : h || (h = g = o))
                                }
                            const b = e.parentNode
                              , y = e.nextSibling;
                            m && m.children.length > 0 && b.insertBefore(m, y),
                            s.removeItem(e);
                            const v = n ? {
                                cc: b,
                                sc: h,
                                ec: h && h.parentNode ? h.nextSibling : m && m.children.length > 0 ? m : y || null
                            } : this.util.getEdgeChildNodes(h, g);
                            if (o)
                                return v;
                            this.history.push(),
                            !n && v && (t ? this.setRange(v.sc, a, v.ec, c) : this.setRange(v.sc, 0, v.sc, 0)),
                            r._findButtonEffectTag()
                        },
                        nodeChange: function(t, i, n) {
                            const o = this.getRange();
                            i = !!(i && i.length > 0) && i,
                            n = !!(n && n.length > 0) && n,
                            this._editorRange();
                            const a = !t
                              , r = a && !n && !i;
                            let c, d, u, h;
                            if (a && (t = this.util.createElement("DIV")),
                            !r && o.startContainer === o.endContainer) {
                                let e = o.startContainer;
                                if (r) {
                                    if (s.getFormatElement(e) === e.parentNode)
                                        return
                                } else if (i.length > 0) {
                                    let n = 0;
                                    for (let o = 0; o < i.length; o++)
                                        for (; !s.isFormatElement(e) && !s.isWysiwygDiv(e); )
                                            1 === e.nodeType && (a ? e.style[i[o]] : e.style[i[o]] === t.style[i[o]]) && n++,
                                            e = e.parentNode;
                                    if (!a && n >= i.length)
                                        return;
                                    if (a && 0 === n)
                                        return
                                }
                            }
                            if (c = s.isWysiwygDiv(o.startContainer) ? e.element.wysiwyg.firstChild : o.startContainer,
                            d = o.startOffset,
                            1 === c.nodeType && c.childNodes.length > 0) {
                                for (; c && !s.isBreak(c) && 1 === c.nodeType; ) {
                                    h = [];
                                    for (let e = 0, t = (u = c.childNodes).length; e < t; e++)
                                        h.push(u[e]);
                                    c = h[d] || c.nextElementSibling || c.nextSibling,
                                    d = 0
                                }
                                if (s.isBreak(c)) {
                                    const e = s.createTextNode(s.zeroWidthSpace);
                                    c.parentNode.insertBefore(e, c),
                                    c = e
                                }
                            }
                            let g = c
                              , m = d;
                            if (c = s.isWysiwygDiv(o.endContainer) ? e.element.wysiwyg.lastChild : o.endContainer,
                            d = o.endOffset,
                            1 === c.nodeType && c.childNodes.length > 0) {
                                for (; c && !s.isBreak(c) && 1 === c.nodeType; ) {
                                    h = [];
                                    for (let e = 0, t = (u = c.childNodes).length; e < t; e++)
                                        h.push(u[e]);
                                    c = h[d - 1] || !/FIGURE/i.test(h[0].nodeName) ? h[0] : c.previousElementSibling || c.previousSibling || g
                                }
                                if (d = c.textContent.length,
                                s.isBreak(c)) {
                                    const e = s.createTextNode(s.zeroWidthSpace);
                                    c.parentNode.insertBefore(e, c),
                                    c = e,
                                    d = 0
                                }
                            }
                            let p = c
                              , _ = d;
                            const f = t.nodeName;
                            this.setRange(g, m, p, _);
                            let b, y, v, x = {}, C = {};
                            if (i) {
                                y = "(?:;|^|\\s)(?:" + i[0];
                                for (let e = 1; e < i.length; e++)
                                    y += "|" + i[e];
                                y += ")\\s*:[^;]*\\s*(?:;|$)",
                                y = new l.RegExp(y,"ig")
                            }
                            if (n) {
                                v = "^(?:" + n[0];
                                for (let e = 1; e < n.length; e++)
                                    v += "|" + n[e];
                                v += ")$",
                                v = new l.RegExp(v,"i")
                            }
                            const E = function(e) {
                                if (3 === e.nodeType || s.isBreak(e))
                                    return !0;
                                if (r)
                                    return !1;
                                const t = e.style.cssText;
                                let i = "";
                                if (y && t.length > 0 && (i = t.replace(y, "").trim()),
                                a) {
                                    if (y && v && !i && v.test(e.nodeName))
                                        return !1;
                                    if (y && !i && t)
                                        return !1;
                                    if (v && v.test(e.nodeName))
                                        return !1
                                }
                                return !(!i && e.nodeName === f || (y && t.length > 0 && (e.style.cssText = i),
                                0))
                            }
                              , k = this.getSelectedElements();
                            s.getFormatElement(g) || (g = s.getChildElement(k[0], function(e) {
                                return 3 === e.nodeType
                            }),
                            m = 0),
                            s.getFormatElement(p) || (_ = (p = s.getChildElement(k[k.length - 1], function(e) {
                                return 3 === e.nodeType
                            })).textContent.length);
                            const w = s.getFormatElement(g) === s.getFormatElement(p)
                              , N = k.length - (w ? 0 : 1);
                            if (b = t.cloneNode(!1),
                            w) {
                                const e = this._nodeChange_oneLine(k[0], b, E, g, m, p, _, r, a, o.collapsed);
                                x.container = e.startContainer,
                                x.offset = e.startOffset,
                                C.container = e.endContainer,
                                C.offset = e.endOffset
                            } else
                                x = this._nodeChange_startLine(k[0], b, E, g, m, r, a);
                            for (let e = 1; e < N; e++)
                                b = t.cloneNode(!1),
                                this._nodeChange_middleLine(k[e], b, E, r, a);
                            N > 0 && !w ? (b = t.cloneNode(!1),
                            C = this._nodeChange_endLine(k[N], b, E, p, _, r, a)) : w || (C = x),
                            this.setRange(x.container, x.offset, C.container, C.offset),
                            this.history.push()
                        },
                        _stripRemoveNode: function(e, t) {
                            if (!t || 3 === t.nodeType)
                                return;
                            const i = t.childNodes;
                            for (; i[0]; )
                                e.insertBefore(i[0], t);
                            e.removeChild(t)
                        },
                        _nodeChange_oneLine: function(e, t, i, n, o, a, r, c, d, u) {
                            const h = e
                              , g = t
                              , m = [t]
                              , p = e.cloneNode(!1)
                              , _ = n === a;
                            let f, b, y, v, x = n, C = o, E = a, k = r, w = !1, N = !1;

                            function T(e) {
                                const t = new l.RegExp("(?:;|^|\\s)(?:" + v + "null)\\s*:[^;]*\\s*(?:;|$)","ig");
                                let i = "";
                                return t && e.style.cssText.length > 0 && (i = t.test(e.style.cssText)),
                                !i
                            }
                            if (function e(n, o) {
                                const l = n.childNodes;
                                for (let n = 0, a = l.length; n < a; n++) {
                                    let a = l[n];
                                    if (!a)
                                        continue;
                                    let r, d = o;
                                    if (!w && a === x) {
                                        const e = s.createTextNode(1 === x.nodeType ? "" : x.substringData(0, C))
                                          , n = s.createTextNode(1 === x.nodeType ? "" : x.substringData(C, _ && k >= C ? k - C : x.data.length - C));
                                        for (e.data.length > 0 && o.appendChild(e),
                                        b = a,
                                        f = [],
                                        v = ""; b !== p && b !== h && null !== b; )
                                            i(b) && 1 === b.nodeType && T(b) && (f.push(b.cloneNode(!1)),
                                            v += b.style.cssText.substr(0, b.style.cssText.indexOf(":")) + "|"),
                                            b = b.parentNode;
                                        const l = f.pop() || n;
                                        for (y = b = l; f.length > 0; )
                                            b = f.pop(),
                                            y.appendChild(b),
                                            y = b;
                                        if (t.appendChild(l),
                                        p.appendChild(t),
                                        x = n,
                                        C = 0,
                                        w = !0,
                                        b !== n && b.appendChild(x),
                                        !_)
                                            continue
                                    }
                                    if (N || a !== E) {
                                        if (w) {
                                            if (1 === a.nodeType && !s.isBreak(a)) {
                                                s.ignoreNodeChange(a) ? (t = t.cloneNode(!1),
                                                p.appendChild(a),
                                                p.appendChild(t),
                                                m.push(t),
                                                n--) : e(a, a);
                                                continue
                                            }
                                            for (b = a,
                                            f = [],
                                            v = ""; null !== b.parentNode && b !== h && b !== t; )
                                                1 === b.nodeType && !s.isBreak(a) && (N || i(b)) && T(b) && (f.push(b.cloneNode(!1)),
                                                v += b.style.cssText.substr(0, b.style.cssText.indexOf(":")) + "|"),
                                                b = b.parentNode;
                                            const l = f.pop() || a;
                                            for (y = b = l; f.length > 0; )
                                                b = f.pop(),
                                                y.appendChild(b),
                                                y = b;
                                            l === a ? o = N ? p : t : N ? (p.appendChild(l),
                                            o = b) : (t.appendChild(l),
                                            o = b)
                                        }
                                        r = a.cloneNode(!1),
                                        o.appendChild(r),
                                        1 !== a.nodeType || s.isBreak(a) || (d = r),
                                        e(a, d)
                                    } else {
                                        const e = s.createTextNode(1 === E.nodeType ? "" : E.substringData(k, E.length - k))
                                          , n = s.createTextNode(_ || 1 === E.nodeType ? "" : E.substringData(0, k));
                                        if (e.data.length > 0) {
                                            for (b = a,
                                            v = "",
                                            f = []; b !== p && b !== h && null !== b; )
                                                1 === b.nodeType && T(b) && (f.push(b.cloneNode(!1)),
                                                v += b.style.cssText.substr(0, b.style.cssText.indexOf(":")) + "|"),
                                                b = b.parentNode;
                                            for (r = y = b = f.pop() || e; f.length > 0; )
                                                b = f.pop(),
                                                y.appendChild(b),
                                                y = b;
                                            p.appendChild(r),
                                            b.textContent = e.data
                                        }
                                        for (b = a,
                                        f = [],
                                        v = ""; b !== p && b !== h && null !== b; )
                                            i(b) && 1 === b.nodeType && T(b) && (f.push(b.cloneNode(!1)),
                                            v += b.style.cssText.substr(0, b.style.cssText.indexOf(":")) + "|"),
                                            b = b.parentNode;
                                        const o = f.pop() || n;
                                        for (y = b = o; f.length > 0; )
                                            b = f.pop(),
                                            y.appendChild(b),
                                            y = b;
                                        t.appendChild(o),
                                        E = n,
                                        k = n.data.length,
                                        N = !0,
                                        !c && u && (t = n,
                                        n.textContent = s.zeroWidthSpace),
                                        b !== n && b.appendChild(E)
                                    }
                                }
                            }(e, p),
                            c = c && d) {
                                for (let e = 0; e < m.length; e++) {
                                    let t = m[e]
                                      , i = s.createTextNode(u ? s.zeroWidthSpace : t.textContent);
                                    p.insertBefore(i, t),
                                    p.removeChild(t),
                                    0 === e && (x = i)
                                }
                                u && (C = 1)
                            } else {
                                if (d)
                                    for (let e = 0; e < m.length; e++) {
                                        let t = m[e];
                                        if (u)
                                            for (; t !== g; )
                                                t = t.parentNode;
                                        this._stripRemoveNode(p, t)
                                    }
                                u && (x = E = t,
                                C = 1,
                                k = 1)
                            }
                            const z = s.onlyZeroWidthSpace(t.textContent);
                            return z && (t.textContent = " "),
                            s.removeEmptyNode(p),
                            z && (t.textContent = s.zeroWidthSpace),
                            e.parentNode.insertBefore(p, e),
                            s.removeItem(e),
                            {
                                startContainer: x,
                                startOffset: C,
                                endContainer: c || !E.textContent ? x : E,
                                endOffset: c || !E.textContent ? x.textContent.length : k
                            }
                        },
                        _nodeChange_middleLine: function(e, t, i, n, o) {
                            const l = e.cloneNode(!1)
                              , a = [t];
                            if (function e(n, o) {
                                const r = n.childNodes;
                                for (let n = 0, c = r.length; n < c; n++) {
                                    let c = r[n];
                                    if (!c)
                                        continue;
                                    let d = o;
                                    if (s.ignoreNodeChange(c))
                                        l.appendChild(t),
                                        t = t.cloneNode(!1),
                                        l.appendChild(c),
                                        l.appendChild(t),
                                        a.push(t),
                                        n--;
                                    else {
                                        if (i(c)) {
                                            let e = c.cloneNode(!1);
                                            o.appendChild(e),
                                            1 !== c.nodeType || s.isBreak(c) || (d = e)
                                        }
                                        e(c, d)
                                    }
                                }
                            }(e, t),
                            l.appendChild(t),
                            n && o)
                                for (let e = 0; e < a.length; e++) {
                                    let t = a[e]
                                      , i = s.createTextNode(t.textContent);
                                    l.insertBefore(i, t),
                                    l.removeChild(t)
                                }
                            else if (o)
                                for (let e = 0; e < a.length; e++)
                                    this._stripRemoveNode(l, a[e]);
                            e.parentNode.insertBefore(l, e),
                            s.removeItem(e)
                        },
                        _nodeChange_startLine: function(e, t, i, n, o, l, a) {
                            const r = e
                              , c = [t]
                              , d = e.cloneNode(!1);
                            let u, h, g, m = n, p = o, _ = !1;
                            if (function e(n, o) {
                                const l = n.childNodes;
                                for (let n = 0, a = l.length; n < a; n++) {
                                    const a = l[n];
                                    if (!a)
                                        continue;
                                    let f = o;
                                    if (_ && !s.isBreak(a)) {
                                        if (1 === a.nodeType) {
                                            s.ignoreNodeChange(a) ? (t = t.cloneNode(!1),
                                            d.appendChild(a),
                                            d.appendChild(t),
                                            c.push(t),
                                            n--) : e(a, a);
                                            continue
                                        }
                                        for (h = a,
                                        u = []; null !== h.parentNode && h !== r && h !== t; )
                                            1 === h.nodeType && i(h) && u.push(h.cloneNode(!1)),
                                            h = h.parentNode;
                                        if (u.length > 0) {
                                            const e = u.pop();
                                            for (g = h = e; u.length > 0; )
                                                h = u.pop(),
                                                g.appendChild(h),
                                                g = h;
                                            t.appendChild(e),
                                            o = h
                                        } else
                                            o = t
                                    }
                                    if (_ || a !== m) {
                                        if (!_ || i(a)) {
                                            const e = a.cloneNode(!1);
                                            o.appendChild(e),
                                            1 !== a.nodeType || s.isBreak(a) || (f = e)
                                        }
                                        e(a, f)
                                    } else {
                                        const e = s.createTextNode(1 === m.nodeType ? "" : m.substringData(0, p))
                                          , n = s.createTextNode(1 === m.nodeType ? "" : m.substringData(p, m.length - p));
                                        for (e.data.length > 0 && o.appendChild(e),
                                        h = o,
                                        u = []; h !== d && null !== h; )
                                            1 === h.nodeType && i(h) && u.push(h.cloneNode(!1)),
                                            h = h.parentNode;
                                        const l = u.pop() || o;
                                        for (g = h = l; u.length > 0; )
                                            h = u.pop(),
                                            g.appendChild(h),
                                            g = h;
                                        l !== o ? (t.appendChild(l),
                                        o = h) : o = t,
                                        s.isBreak(a) && t.appendChild(a.cloneNode(!1)),
                                        d.appendChild(t),
                                        m = n,
                                        p = 0,
                                        _ = !0,
                                        o.appendChild(m)
                                    }
                                }
                            }(e, d),
                            l = l && a)
                                for (let e = 0; e < c.length; e++) {
                                    let t = c[e]
                                      , i = s.createTextNode(t.textContent);
                                    d.insertBefore(i, t),
                                    d.removeChild(t),
                                    0 === e && (m = i)
                                }
                            else if (a)
                                for (let e = 0; e < c.length; e++)
                                    this._stripRemoveNode(d, c[e]);
                            return l || 0 !== d.children.length ? (s.removeEmptyNode(d),
                            s.onlyZeroWidthSpace(d.textContent) && (m = d.firstChild,
                            p = 0),
                            e.parentNode.insertBefore(d, e),
                            s.removeItem(e)) : e.childNodes ? m = e.childNodes[0] : (m = s.createTextNode(s.zeroWidthSpace),
                            e.appendChild(m)),
                            {
                                container: m,
                                offset: p
                            }
                        },
                        _nodeChange_endLine: function(e, t, i, n, o, l, a) {
                            const r = e
                              , c = [t]
                              , d = e.cloneNode(!1);
                            let u, h, g, m = n, p = o, _ = !1;
                            if (function e(n, o) {
                                const l = n.childNodes;
                                for (let n = l.length - 1; 0 <= n; n--) {
                                    const a = l[n];
                                    if (!a)
                                        continue;
                                    let f = o;
                                    if (_ && !s.isBreak(a)) {
                                        if (1 === a.nodeType) {
                                            s.ignoreNodeChange(a) ? (t = t.cloneNode(!1),
                                            d.appendChild(a),
                                            d.appendChild(t),
                                            c.push(t),
                                            n--) : e(a, a);
                                            continue
                                        }
                                        for (h = a,
                                        u = []; null !== h.parentNode && h !== r && h !== t; )
                                            i(h) && 1 === h.nodeType && u.push(h.cloneNode(!1)),
                                            h = h.parentNode;
                                        if (u.length > 0) {
                                            const e = u.pop();
                                            for (g = h = e; u.length > 0; )
                                                h = u.pop(),
                                                g.appendChild(h),
                                                g = h;
                                            t.insertBefore(e, t.firstChild),
                                            o = h
                                        } else
                                            o = t
                                    }
                                    if (_ || a !== m) {
                                        if (!_ || i(a)) {
                                            const e = a.cloneNode(!1);
                                            o.insertBefore(e, o.firstChild),
                                            1 !== a.nodeType || s.isBreak(a) || (f = e)
                                        }
                                        e(a, f)
                                    } else {
                                        const e = s.createTextNode(1 === m.nodeType ? "" : m.substringData(p, m.length - p))
                                          , n = s.createTextNode(1 === m.nodeType ? "" : m.substringData(0, p));
                                        for (e.data.length > 0 && o.insertBefore(e, o.firstChild),
                                        h = o,
                                        u = []; h !== d && null !== h; )
                                            i(h) && 1 === h.nodeType && u.push(h.cloneNode(!1)),
                                            h = h.parentNode;
                                        const l = u.pop() || o;
                                        for (g = h = l; u.length > 0; )
                                            h = u.pop(),
                                            g.appendChild(h),
                                            g = h;
                                        l !== o ? (t.insertBefore(l, t.firstChild),
                                        o = h) : o = t,
                                        s.isBreak(a) && t.appendChild(a.cloneNode(!1)),
                                        d.insertBefore(t, d.firstChild),
                                        m = n,
                                        p = n.data.length,
                                        _ = !0,
                                        o.insertBefore(m, o.firstChild)
                                    }
                                }
                            }(e, d),
                            l = l && a)
                                for (let e = 0; e < c.length; e++) {
                                    let t = c[e]
                                      , i = s.createTextNode(t.textContent);
                                    d.insertBefore(i, t),
                                    d.removeChild(t),
                                    e === c.length - 1 && (m = i,
                                    p = i.textContent.length)
                                }
                            else if (a)
                                for (let e = 0; e < c.length; e++)
                                    this._stripRemoveNode(d, c[e]);
                            return l || 0 !== d.childNodes.length ? (s.removeEmptyNode(d),
                            s.onlyZeroWidthSpace(d.textContent) && (m = d.firstChild,
                            p = m.textContent.length),
                            e.parentNode.insertBefore(d, e),
                            s.removeItem(e)) : e.childNodes ? m = e.childNodes[0] : (m = s.createTextNode(s.zeroWidthSpace),
                            e.appendChild(m)),
                            {
                                container: m,
                                offset: p
                            }
                        },
                        commandHandler: function(t, i) {
                            switch (i) {
                            case "selectAll":
                                const n = e.element.wysiwyg
                                  , o = s.getChildElement(n.firstChild, function(e) {
                                    return 0 === e.childNodes.length || 3 === e.nodeType
                                }) || n.firstChild
                                  , l = s.getChildElement(n.lastChild, function(e) {
                                    return 0 === e.childNodes.length || 3 === e.nodeType
                                }, !0) || n.lastChild;
                                this.setRange(o, 0, l, l.textContent.length);
                                break;
                            case "codeView":
                                this.toggleCodeView(),
                                s.toggleClass(t, "on");
                                break;
                            case "fullScreen":
                                this.toggleFullScreen(t),
                                s.toggleClass(t, "on");
                                break;
                            case "indent":
                            case "outdent":
                                this.indent(i);
                                break;
                            case "undo":
                                this.history.undo();
                                break;
                            case "redo":
                                this.history.redo();
                                break;
                            case "removeFormat":
                                this.removeFormat(),
                                this.focus();
                                break;
                            case "preview":
                            case "print":
                                this.openWindowContents(i);
                                break;
                            case "showBlocks":
                                this.toggleDisplayBlocks(),
                                s.toggleClass(t, "on");
                                break;
                            case "save":
                                if ("function" == typeof e.option.callBackSave)
                                    e.option.callBackSave(this.getContents());
                                else {
                                    if ("function" != typeof c.save)
                                        throw Error("[SUNEDITOR.core.commandHandler.fail] Please register call back function in creation option. (callBackSave : Function)");
                                    c.save()
                                }
                                e.tool.save && e.tool.save.setAttribute("disabled", !0);
                                break;
                            default:
                                const a = s.hasClass(this.commandMap[i], "on");
                                "SUB" === i && s.hasClass(this.commandMap.SUP, "on") ? this.nodeChange(null, null, ["SUP"]) : "SUP" === i && s.hasClass(this.commandMap.SUB, "on") && this.nodeChange(null, null, ["SUB"]),
                                this.nodeChange(a ? null : this.util.createElement(i), null, [i]),
                                this.focus()
                            }
                        },
                        removeFormat: function() {
                            this.nodeChange()
                        },
                        indent: function(e) {
                            const t = this.getSelectedElements();
                            let i, n;
                            for (let o = 0, l = t.length; o < l; o++)
                                i = t[o],
                                n = /\d+/.test(i.style.marginLeft) ? 1 * i.style.marginLeft.match(/\d+/)[0] : 0,
                                "indent" === e ? n += 25 : n -= 25,
                                i.style.marginLeft = (n < 0 ? 0 : n) + "px";
                            r._findButtonEffectTag(),
                            this.history.push()
                        },
                        toggleDisplayBlocks: function() {
                            s.toggleClass(e.element.wysiwyg, "sun-editor-show-block")
                        },
                        toggleCodeView: function() {
                            const t = this._variable.wysiwygActive
                              , i = this.codeViewDisabledButtons;
                            for (let e = 0, n = i.length; e < n; e++)
                                i[e].disabled = t;
                            if (this.controllersOff(),
                            t)
                                e.element.code.value = s.convertHTMLForCodeView(e.element.wysiwyg),
                                e.element.code.style.display = "block",
                                e.element.wysiwyg.style.display = "none",
                                this._variable._codeOriginCssText = this._variable._codeOriginCssText.replace(/(\s?display(\s+)?:(\s+)?)[a-zA-Z]+(?=;)/, "display: block"),
                                this._variable._wysiwygOriginCssText = this._variable._wysiwygOriginCssText.replace(/(\s?display(\s+)?:(\s+)?)[a-zA-Z]+(?=;)/, "display: none"),
                                "auto" === e.option.height && (e.element.code.style.height = e.element.code.scrollHeight > 0 ? e.element.code.scrollHeight + "px" : "auto"),
                                this._variable.wysiwygActive = !1,
                                e.element.code.focus();
                            else {
                                const t = e.element.code.value.trim();
                                e.element.wysiwyg.innerHTML = t.length > 0 ? s.convertContentsForEditor(t) : "<p>" + s.zeroWidthSpace + "</p>",
                                e.element.wysiwyg.scrollTop = 0,
                                e.element.code.style.display = "none",
                                e.element.wysiwyg.style.display = "block",
                                this._variable._codeOriginCssText = this._variable._codeOriginCssText.replace(/(\s?display(\s+)?:(\s+)?)[a-zA-Z]+(?=;)/, "display: none"),
                                this._variable._wysiwygOriginCssText = this._variable._wysiwygOriginCssText.replace(/(\s?display(\s+)?:(\s+)?)[a-zA-Z]+(?=;)/, "display: block"),
                                "auto" === e.option.height && (e.element.code.style.height = "0px"),
                                this._variable.wysiwygActive = !0,
                                this.focus()
                            }
                        },
                        toggleFullScreen: function(t) {
                            const i = e.element.topArea
                              , n = e.element.toolbar
                              , a = e.element.editorArea
                              , c = e.element.wysiwyg
                              , d = e.element.code;
                            this._variable.isFullScreen ? (this._variable.isFullScreen = !1,
                            c.style.cssText = this._variable._wysiwygOriginCssText,
                            d.style.cssText = this._variable._codeOriginCssText,
                            n.style.cssText = "",
                            a.style.cssText = this._variable._editorAreaOriginCssText,
                            i.style.cssText = this._variable._originCssText,
                            o.body.style.overflow = this._variable._bodyOverflow,
                            e.option.stickyToolbar > -1 && (s.removeClass(n, "sun-editor-sticky"),
                            r.onScroll_window()),
                            this._variable._fullScreenSticky && (this._variable._fullScreenSticky = !1,
                            e.element._stickyDummy.style.display = "block",
                            s.addClass(n, "sun-editor-sticky")),
                            s.removeClass(t.firstElementChild, "icon-reduction"),
                            s.addClass(t.firstElementChild, "icon-expansion")) : (this._variable.isFullScreen = !0,
                            i.style.position = "fixed",
                            i.style.top = "0",
                            i.style.left = "0",
                            i.style.width = "100%",
                            i.style.height = "100%",
                            i.style.zIndex = "2147483647",
                            "none" !== e.element._stickyDummy.style.display && (this._variable._fullScreenSticky = !0,
                            e.element._stickyDummy.style.display = "none",
                            s.removeClass(n, "sun-editor-sticky")),
                            this._variable._bodyOverflow = o.body.style.overflow,
                            o.body.style.overflow = "hidden",
                            this._variable._editorAreaOriginCssText = a.style.cssText,
                            this._variable._wysiwygOriginCssText = c.style.cssText,
                            this._variable._codeOriginCssText = d.style.cssText,
                            a.style.cssText = n.style.cssText = "",
                            c.style.cssText = c.style.cssText.match(/\s?display(\s+)?:(\s+)?[a-zA-Z]+;/)[0],
                            d.style.cssText = d.style.cssText.match(/\s?display(\s+)?:(\s+)?[a-zA-Z]+;/)[0],
                            n.style.width = c.style.height = d.style.height = "100%",
                            n.style.position = "relative",
                            this._variable.innerHeight_fullScreen = l.innerHeight - n.offsetHeight,
                            a.style.height = this._variable.innerHeight_fullScreen + "px",
                            s.removeClass(t.firstElementChild, "icon-expansion"),
                            s.addClass(t.firstElementChild, "icon-reduction"))
                        },
                        openWindowContents: function(t) {
                            const i = "print" === t
                              , o = l.open("", "_blank");
                            o.mimeType = "text/html",
                            o.document.write('<!doctype html><html><head><meta charset="utf-8" /><meta name="viewport" content="width=device-width, initial-scale=1"><title>' + (i ? n.toolbar.print : n.toolbar.preview) + '</title><link rel="stylesheet" type="text/css" href="' + s.getIncludePath(["suneditor-contents", "suneditor"], "css") + '"></head><body><div class="sun-editor-editable" style="width:' + e.element.wysiwyg.offsetWidth + 'px; margin:auto;">' + this.getContents() + "</div>" + (i ? "<script>window.print();<\/script>" : "") + "</body></html>")
                        },
                        getContents: function() {
                            let t = "";
//                            if (0 === e.element.wysiwyg.innerText.trim().length)
//                                return t;
                            t = a._variable.wysiwygActive ? e.element.wysiwyg.innerHTML : s.convertContentsForEditor(e.element.code.value);
                            const i = s.createElement("DIV");
                            i.innerHTML = t;
                            const n = s.getListChildren(i, function(e) {
                                return /FIGCAPTION/i.test(e.nodeName)
                            });
                            for (let e = 0, t = n.length; e < t; e++)
                                n[e].removeAttribute("contenteditable");
                            return i.innerHTML
                        }
                    }
                      , r = {
                        _directionKeyKeyCode: new l.RegExp("^(8|13|32|46|33|34|35|36|37|38|39|40|46|98|100|102|104)$"),
                        _historyIgnoreRegExp: new l.RegExp("^(9|1[6-8]|20|3[3-9]|40|45|9[1-3]|11[2-9]|12[0-3]|144|145)$"),
                        _onButtonsCheck: new l.RegExp("^(STRONG|INS|EM|DEL|SUB|SUP)$"),
                        _keyCodeShortcut: {
                            65: "A",
                            66: "B",
                            83: "S",
                            85: "U",
                            73: "I",
                            89: "Y",
                            90: "Z",
                            219: "[",
                            9: "]"
                        },
                        _shortcutCommand: function(e, t) {
                            let i = null;
                            switch (r._keyCodeShortcut[e]) {
                            case "A":
                                i = "selectAll";
                                break;
                            case "B":
                                i = "STRONG";
                                break;
                            case "S":
                                t && (i = "DEL");
                                break;
                            case "U":
                                i = "INS";
                                break;
                            case "I":
                                i = "EM";
                                break;
                            case "Z":
                                i = t ? "redo" : "undo";
                                break;
                            case "Y":
                                i = "redo";
                                break;
                            case "[":
                                i = "outdent";
                                break;
                            case "]":
                                i = "indent"
                            }
                            return !!i && (a.commandHandler(a.commandMap[i], i),
                            !0)
                        },
                        _findButtonEffectTag: function() {
                            const t = a.commandMap
                              , i = this._onButtonsCheck
                              , o = []
                              , l = [];
                            let r = !0
                              , c = !0
                              , d = !0
                              , u = !0
                              , h = !0
                              , g = !0
                              , m = !0
                              , p = "";
                            for (let _ = a.getSelectionNode(); !s.isWysiwygDiv(_) && _; _ = _.parentNode)
                                if (1 === _.nodeType && !s.isBreak(_))
                                    if (p = _.nodeName.toUpperCase(),
                                    l.push(p),
                                    s.isFormatElement(_)) {
                                        r && t.FORMAT && (o.push("FORMAT"),
                                        s.changeTxt(t.FORMAT, p),
                                        r = !1);
                                        const e = _.style.textAlign;
                                        c && e && t.ALIGN && (o.push("ALIGN"),
                                        t.ALIGN.className = "icon-align-" + e,
                                        t.ALIGN.setAttribute("data-focus", e),
                                        c = !1),
                                        d && s.isListCell(p) && t.LI && (o.push("LI"),
                                        t.LI.setAttribute("data-focus", _.parentNode.nodeName),
                                        d = !1),
                                        g && _.style.marginLeft && 1 * _.style.marginLeft.match(/\d+/)[0] > 0 && t.OUTDENT && (o.push("OUTDENT"),
                                        t.OUTDENT.removeAttribute("disabled"),
                                        g = !1)
                                    } else {
                                        if (u && _.style.fontFamily.length > 0 && t.FONT) {
                                            o.push("FONT");
                                            const e = (_.style.fontFamily || _.face || n.toolbar.font).replace(/["']/g, "");
                                            s.changeTxt(t.FONT, e),
                                            s.changeTxt(t.FONT_TOOLTIP, e),
                                            u = !1
                                        }
                                        h && _.style.fontSize.length > 0 && t.SIZE && (o.push("SIZE"),
                                        s.changeTxt(t.SIZE, _.style.fontSize),
                                        h = !1),
                                        m && /^A$/.test(p) && null === _.getAttribute("data-image-link") ? (e.link && a.controllerArray[0] === e.link.linkBtn || a.callPlugin("link", function() {
                                            a.plugins.link.call_controller_linkButton.call(a, _)
                                        }),
                                        m = !1) : m && e.link && a.controllerArray[0] === e.link.linkBtn && a.controllersOff(),
                                        i.test(p) && o.push(p)
                                    }
                            for (let e = 0; e < o.length; e++)
                                p = o[e],
                                i.test(p) && s.addClass(t[p], "on");
                            for (let e in t)
                                o.indexOf(e) > -1 || (t.FONT && /^FONT$/i.test(e) ? (s.changeTxt(t.FONT, n.toolbar.font),
                                s.changeTxt(t.FONT_TOOLTIP, n.toolbar.font)) : t.SIZE && /^SIZE$/i.test(e) ? s.changeTxt(t.SIZE, n.toolbar.fontSize) : t.ALIGN && /^ALIGN$/i.test(e) ? (t.ALIGN.className = "icon-align-left",
                                t.ALIGN.removeAttribute("data-focus")) : t.LI && s.isListCell(e) ? t.LI.removeAttribute("data-focus") : t.OUTDENT && /^OUTDENT$/i.test(e) ? t.OUTDENT.setAttribute("disabled", !0) : s.removeClass(t[e], "on"));
                            a._variable.currentNodes = l.reverse(),
                            e.option.showPathLabel && (e.element.navigation.textContent = a._variable.currentNodes.join(" > "))
                        },
                        _cancelCaptionEdit: function() {
                            this.setAttribute("contenteditable", !1),
                            this.removeEventListener("blur", r._cancelCaptionEdit)
                        },
                        onMouseDown_toolbar: function(e) {
                            let t = e.target;
                            if (s.getParentElement(t, ".sun-editor-submenu"))
                                e.stopPropagation(),
                                a._notHideToolbar = !0;
                            else {
                                e.preventDefault();
                                let i = t.getAttribute("data-command")
                                  , n = t.className;
                                for (; !i && !/editor_tool/.test(n) && !/sun-editor-id-toolbar/.test(n); )
                                    i = (t = t.parentNode).getAttribute("data-command"),
                                    n = t.className;
                                i === a._submenuName && e.stopPropagation()
                            }
                        },
                        onClick_toolbar: function(e) {
                            e.preventDefault(),
                            e.stopPropagation();
                            let t = e.target
                              , i = t.getAttribute("data-display")
                              , n = t.getAttribute("data-command")
                              , o = t.className;
                            for (; !n && !/editor_tool/.test(o) && !/sun-editor-id-toolbar/.test(o); )
                                t = t.parentNode,
                                n = t.getAttribute("data-command"),
                                i = t.getAttribute("data-display"),
                                o = t.className;
                            if ((n || i) && !t.disabled) {
                                if (a.focus(),
                                i)
                                    return !/submenu/.test(i) || null !== t.nextElementSibling && t === a.submenuActiveButton ? /dialog/.test(i) ? void a.callPlugin(n, function() {
                                        a.plugins.dialog.open.call(a, n, !1)
                                    }) : void a.submenuOff() : void a.callPlugin(n, function() {
                                        a.submenuOn(t)
                                    });
                                n && a.commandHandler(t, n)
                            }
                        },
                        onMouseUp_wysiwyg: function() {
                            if (a._editorRange(),
                            a._isBalloon) {
                                const e = a.getRange();
                                e.collapsed ? r._hideToolbar() : r._showToolbarBalloon(e)
                            }
                        },
                        onClick_wysiwyg: function(e) {
                            e.stopPropagation();
                            const t = e.target;
                            if (/^IMG$/i.test(t.nodeName))
                                return e.preventDefault(),
                                void a.callPlugin("image", function() {
                                    const e = a.plugins.resizing.call_controller_resize.call(a, t, "image");
                                    a.plugins.image.onModifyMode.call(a, t, e),
                                    s.getParentElement(t, ".sun-editor-id-image-container") || (a.plugins.image.openModify.call(a, !0),
                                    a.plugins.image.update_image.call(a, !0))
                                });
                            if (/sun-editor-id-iframe-inner-resizing-cover/i.test(t.className))
                                return e.preventDefault(),
                                void a.callPlugin("video", function() {
                                    const e = t.parentNode.querySelector("iframe")
                                      , i = a.plugins.resizing.call_controller_resize.call(a, e, "video");
                                    a.plugins.video.onModifyMode.call(a, e, i)
                                });
                            r._findButtonEffectTag();
                            const i = s.getParentElement(t, "FIGCAPTION");
                            if (i && "ture" !== i.getAttribute("contenteditable")) {
                                if (e.preventDefault(),
                                i.setAttribute("contenteditable", !0),
                                i.focus(),
                                a._isInline && !a._inlineToolbarAttr.isShow) {
                                    r._showToolbarInline();
                                    const e = function() {
                                        r._hideToolbar(),
                                        i.removeEventListener("blur", e)
                                    };
                                    i.addEventListener("blur", e)
                                }
                            } else {
                                const e = s.getParentElement(t, s.isCell);
                                e && 0 === a.controllerArray.length && a.callPlugin("table", a.plugins.table.call_controller_tableEdit.bind(a, e))
                            }
                            c.onClick && c.onClick(e)
                        },
                        _showToolbarBalloon: function(t) {
                            const i = t || a.getRange()
                              , n = e.element.toolbar
                              , r = l.getSelection();
                            let c;
                            if (r.focusNode === r.anchorNode)
                                c = r.focusOffset < r.anchorOffset;
                            else {
                                const e = s.getListChildNodes(i.commonAncestorContainer);
                                c = s.getArrayIndex(e, r.focusNode) < s.getArrayIndex(e, r.anchorNode)
                            }
                            let d = i.getClientRects();
                            d = d[c ? 0 : d.length - 1],
                            n.style.display = "block";
                            const u = n.offsetWidth
                              , h = n.offsetHeight;
                            let g = (c ? d.left : d.right) - e.element.topArea.offsetLeft + (l.scrollX || o.documentElement.scrollLeft) - u / 2
                              , m = (c ? d.top - h - 11 : d.bottom + 11) - e.element.topArea.offsetTop + (l.scrollY || o.documentElement.scrollTop);
                            const p = g + u - e.element.topArea.offsetWidth;
                            n.style.left = (g < 0 ? 20 : p < 0 ? g : g - p - 20) + "px",
                            n.style.top = m + "px",
                            c ? (s.removeClass(e.element._arrow, "arrow-up"),
                            s.addClass(e.element._arrow, "arrow-down"),
                            e.element._arrow.style.top = h + "px") : (s.removeClass(e.element._arrow, "arrow-down"),
                            s.addClass(e.element._arrow, "arrow-up"),
                            e.element._arrow.style.top = "-11px");
                            const _ = e.element._arrow.offsetWidth
                              , f = u / 2 + (g < 0 ? g - _ : p < 0 ? 0 : p + _)
                              , b = _ / 2;
                            e.element._arrow.style.left = (f < b ? b : f + b >= u ? f - b : f) + "px"
                        },
                        _showToolbarInline: function() {
                            const t = e.element.toolbar;
                            t.style.display = "block",
                            a._inlineToolbarAttr.width = t.style.width = e.option.toolbarWidth,
                            a._inlineToolbarAttr.top = t.style.top = -1 - t.offsetHeight + "px",
                            r.onScroll_window(),
                            a._inlineToolbarAttr.isShow = !0
                        },
                        _hideToolbar: function() {
                            a._notHideToolbar || (e.element.toolbar.style.display = "none",
                            a._inlineToolbarAttr.isShow = !1),
                            a._notHideToolbar = !1
                        },
                        onKeyDown_wysiwyg: function(t) {
                            const i = t.keyCode
                              , n = t.shiftKey
                              , o = t.ctrlKey || t.metaKey
                              , l = t.altKey;
                            if (a._isBalloon && r._hideToolbar(),
                            o && r._shortcutCommand(i, n))
                                return t.preventDefault(),
                                t.stopPropagation(),
                                !1;
                            const d = a.getSelectionNode()
                              , u = a.getRange()
                              , h = u.startContainer !== u.endContainer;
                            let g, m;
                            if (g = s.getFormatElement(d) || d,
                            m = s.getRangeFormatElement(d),
                            !(h || g && 3 !== g.nodeType && g !== m)) {
                                if (m && (s.isList(m) || /^PRE$/i.test(m.nodeName)) && 8 !== i && 46 !== i) {
                                    const e = s.createElement(s.isList(m) ? "LI" : "P");
                                    e.innerHTML = s.zeroWidthSpace,
                                    m.insertBefore(e, d.nextElementSibling),
                                    a.setRange(e, 0, e, 0)
                                } else
                                    a.execCommand("formatBlock", !1, s.isCell(m) ? "DIV" : "P"),
                                    a.focus();
                                return
                            }
                            const p = a._resizingName;
                            switch (i) {
                            case 8:
                                if (h)
                                    break;
                                if (p) {
                                    t.preventDefault(),
                                    t.stopPropagation(),
                                    a.plugins[p].destroy.call(a),
                                    a.history.push();
                                    break
                                }
                                if (s.isWysiwygDiv(d.parentNode) && !d.previousSibling && s.isFormatElement(d) && !s.isListCell(d))
                                    return t.preventDefault(),
                                    t.stopPropagation(),
                                    d.innerHTML = s.zeroWidthSpace,
                                    !1;
                                const r = u.commonAncestorContainer;
//                                if(r.nodeType === 3 && r.nodeName === "#text"){
//                                	if(r.parentNode !== null){
//                                		if(r.parentNode.previousSibling !== null){
//                                			let isComponentCheck = r.parentNode.previousSibling;
//                                			if(isComponentCheck.classList.contains("sun-editor-id-image-container")){
//                                				if (s.isComponent(isComponentCheck)) {
//                                                    const e = isComponentCheck;
//                                                    s.removeItem(e)
//                                                }
//                                				break;
//                                			}
//                                			if(isComponentCheck.classList.contains("sun-editor-content")){
//                                				if (s.isComponent(isComponentCheck)) {
//                                                    const e = isComponentCheck;
//                                                    s.removeItem(e)
//                                                }
//                                				break;
//                                			}
//                                		}
//                                	}
//                                }
                                if (0 === u.startOffset && 0 === u.endOffset) {
                                    if (m && g && !s.isCell(m) && !/^FIGCAPTION$/i.test(m.nodeName)) {
                                        let e = !0
                                          , i = r;
                                        for (; i && i !== m && !s.isWysiwygDiv(i); ) {
                                            if (i.previousSibling) {
                                                e = !1;
                                                break
                                            }
                                            i = i.parentNode
                                        }
                                        if (e) {
                                            t.preventDefault(),
                                            a.detachRangeFormatElement(m, s.isListCell(g) ? [g] : null, null, !1, !1);
                                            break
                                        }
                                    }
                                    if (s.isComponent(r.previousSibling)) {
                                        const e = r.previousSibling;
                                        s.removeItem(e)
                                    }
                                }
                                break;
                            case 46:
                                if (p) {
                                    t.preventDefault(),
                                    t.stopPropagation(),
                                    a.plugins[p].destroy.call(a),
                                    a.history.push();
                                    break
                                }
                                if ((s.isFormatElement(d) || null === d.nextSibling) && u.startOffset === d.textContent.length) {
                                    let e = g.nextElementSibling;
                                    if (s.isComponent(e)) {
                                        t.preventDefault(),
                                        s.onlyZeroWidthSpace(g) && s.removeItem(g),
                                        (s.hasClass(e, "sun-editor-id-comp") || /^IMG$/i.test(e.nodeName)) && (t.stopPropagation(),
                                        s.hasClass(e, "sun-editor-id-image-container") || /^IMG$/i.test(e.nodeName) ? (e = /^IMG$/i.test(e.nodeName) ? e : e.querySelector("img"),
                                        a.callPlugin("image", function() {
                                            const t = a.plugins.resizing.call_controller_resize.call(a, e, "image");
                                            a.plugins.image.onModifyMode.call(a, e, t),
                                            s.getParentElement(e, ".sun-editor-id-comp") || (a.plugins.image.openModify.call(a, !0),
                                            a.plugins.image.update_image.call(a, !0))
                                        })) : s.hasClass(e, "sun-editor-id-iframe-container") && (t.stopPropagation(),
                                        a.callPlugin("video", function() {
                                            const t = e.querySelector("iframe")
                                              , i = a.plugins.resizing.call_controller_resize.call(a, t, "video");
                                            a.plugins.video.onModifyMode.call(a, t, i)
                                        })),
                                        a.history.push());
                                        break
                                    }
                                }
                                break;
                            case 9:
                                if (t.preventDefault(), o) break;

                                t.preventDefault()
                                t.stopPropagation()
                                if(n){
                                	a.indent("outdent")
                                }else{
                                	a.indent("indent")
                                }
                                break;
//                                if (t.preventDefault(),
//                                o || l)
//                                    break;
//                                a.controllersOff();
//                                let c = d;
//                                for (; !s.isCell(c) && !s.isWysiwygDiv(c); )
//                                    c = c.parentNode;
//                                if (c && s.isCell(c)) {
//                                    const e = s.getParentElement(c, "table")
//                                      , t = s.getListChildren(e, s.isCell);
//                                    let i = n ? s.prevIdx(t, c) : s.nextIdx(t, c);
//                                    i !== t.length || n || (i = 0),
//                                    -1 === i && n && (i = t.length - 1);
//                                    const o = t[i];
//                                    if (!o)
//                                        return !1;
//                                    a.setRange(o, 0, o, 0);
//                                    break
//                                }
//                                const _ = a.getSelectedElements();
//                                if (n)
//                                    for (let e, t = 0, i = _.length; t < i; t++)
//                                        e = _[t].firstChild,
//                                        /^\s{1,4}$/.test(e.textContent) ? s.removeItem(e) : /^\s{1,4}/.test(e.textContent) && (e.textContent = e.textContent.replace(/^\s{1,4}/, ""));
//                                else {
//                                    const e = s.createTextNode(new Array(a._variable.tabSize + 1).join(" "));
//                                    if (1 === _.length)
//                                        a.insertNode(e),
//                                        a.setRange(e, a._variable.tabSize, e, a._variable.tabSize);
//                                    else
//                                        for (let t = 0, i = _.length; t < i; t++)
//                                            _[t].insertBefore(e.cloneNode(!1), _[t].firstChild)
//                                }
//                                a.history.push();
//                                break;
                            case 13:
                                if (h)
                                    break;
                                g = s.getFormatElement(d),
                                m = s.getRangeFormatElement(g);
                                const f = s.getParentElement(m, "FIGCAPTION");
                                if (m && g && !s.isCell(m) && !/^FIGCAPTION$/i.test(m.nodeName) && !a.getRange().commonAncestorContainer.nextElementSibling && s.onlyZeroWidthSpace(g.innerText.trim())) {
                                    t.preventDefault();
                                    const e = a.appendFormatTag(m, s.isCell(m.parentNode) ? "DIV" : s.isListCell(g) ? "P" : null);
                                    s.removeItemAllParents(g),
                                    a.setRange(e, 1, e, 1);
                                    break
                                }
                                if (m && f && s.getParentElement(m, s.isList) && (t.preventDefault(),
                                g = a.appendFormatTag(g),
                                a.setRange(g, 0, g, 0)),
                                p) {
                                    t.preventDefault(),
                                    t.stopPropagation();
                                    const i = e[p]
                                      , n = i._container
                                      , o = n.previousElementSibling || n.nextElementSibling;
                                    let l = null;
                                    s.isListCell(n.parentNode) ? l = s.createElement("BR") : (l = s.createElement(s.isFormatElement(o) ? o.nodeName : "P")).innerHTML = s.zeroWidthSpace,
                                    n.parentNode.insertBefore(l, n),
                                    a.callPlugin(p, function() {
                                        const e = a.plugins.resizing.call_controller_resize.call(a, i._element, p);
                                        a.plugins[p].onModifyMode.call(a, i._element, e)
                                    }),
                                    a.history.push()
                                }
                            }
                            c.onKeyDown && c.onKeyDown(t)
                        },
                        onKeyUp_wysiwyg: function(t) {
                            a._editorRange();
                            const i = a.getSelectionNode()
                              , n = t.keyCode;
                            if (!a._isBalloon || a.getRange().collapsed) {
                                if (8 === n && s.isWysiwygDiv(i) && "" === e.element.wysiwyg.textContent) {
                                    t.preventDefault(),
                                    t.stopPropagation();
                                    const e = s.createElement(s.isFormatElement(a._variable.currentNodes[0]) ? a._variable.currentNodes[0] : "P");
                                    return e.innerHTML = s.zeroWidthSpace,
                                    i.appendChild(e),
                                    void a.setRange(e, 0, e, 0)
                                }
                                r._directionKeyKeyCode.test(n) && r._findButtonEffectTag(),
                                c.onKeyUp && c.onKeyUp(t),
                                r._historyIgnoreRegExp.test(n) || a.history.push()
                            } else
                                r._showToolbarBalloon()
                        },
                        onScroll_wysiwyg: function(e) {
                            a.controllersOff(),
                            c.onScroll && c.onScroll(e)
                        },
                        onDrop_wysiwyg: function(t) {
//                            const i = t.dataTransfer.files;
//                            i.length > 0 && (t.stopPropagation(),
//                            t.preventDefault(),
//                            a.focus(),
//                            a.callPlugin("image", function() {
//                                e.image.imgInputFile.files = i,
//                                a.plugins.image.onRender_imgInput.call(a),
//                                e.image.imgInputFile.files = null
//                            })),
//                            c.onDrop && c.onDrop(t)
                        },
                        onMouseDown_resizingBar: function(t) {
                            t.stopPropagation(),
                            a._variable.resizeClientY = t.clientY,
                            e.element.resizeBackground.style.display = "block",
                            o.addEventListener("mousemove", r._resize_editor),
                            o.addEventListener("mouseup", function t() {
                                e.element.resizeBackground.style.display = "none",
                                o.removeEventListener("mousemove", r._resize_editor),
                                o.removeEventListener("mouseup", t)
                            })
                        },
                        _resize_editor: function(t) {
                            const i = e.element.editorArea.offsetHeight + (t.clientY - a._variable.resizeClientY);
                            e.element.wysiwyg.style.height = e.element.code.style.height = (i < a._variable.minResizingSize ? a._variable.minResizingSize : i) + "px",
                            a._variable.resizeClientY = t.clientY
                        },
                        onResize_window: function() {
                            0 !== e.element.toolbar.offsetWidth && (a._variable.isFullScreen ? (a._variable.innerHeight_fullScreen += l.innerHeight - e.element.toolbar.offsetHeight - a._variable.innerHeight_fullScreen,
                            e.element.editorArea.style.height = a._variable.innerHeight_fullScreen + "px") : a._variable._sticky && (e.element.toolbar.style.width = e.element.topArea.offsetWidth - 2 + "px",
                            r.onScroll_window()))
                        },
                        onScroll_window: function() {
                            if (a._variable.isFullScreen || 0 === e.element.toolbar.offsetWidth)
                                return;
                            const t = e.element
                              , i = t.editorArea.offsetHeight
                              , n = t.topArea.offsetTop - (a._isInline ? t.toolbar.offsetHeight : 0)
                              , l = (this.scrollY || o.documentElement.scrollTop) + e.option.stickyToolbar;
                            l < n ? r._offStickyToolbar(t) : l + a._variable.minResizingSize >= i + n ? (a._variable._sticky || r._onStickyToolbar(t),
                            t.toolbar.style.top = i + n + e.option.stickyToolbar - l - a._variable.minResizingSize + "px") : l >= n && r._onStickyToolbar(t)
                        },
                        _onStickyToolbar: function(t) {
                            a._isInline || (t._stickyDummy.style.height = t.toolbar.offsetHeight + "px",
                            t._stickyDummy.style.display = "block"),
                            t.toolbar.style.top = e.option.stickyToolbar + "px",
                            t.toolbar.style.width = a._isInline ? a._inlineToolbarAttr.width : t.toolbar.offsetWidth + "px",
                            s.addClass(t.toolbar, "sun-editor-sticky"),
                            a._variable._sticky = !0
                        },
                        _offStickyToolbar: function(e) {
                            e._stickyDummy.style.display = "none",
                            e.toolbar.style.top = a._isInline ? a._inlineToolbarAttr.top : "",
                            e.toolbar.style.width = a._isInline ? a._inlineToolbarAttr.width : "",
                            e.editorArea.style.marginTop = "",
                            s.removeClass(e.toolbar, "sun-editor-sticky"),
                            a._variable._sticky = !1
                        },
                        _codeViewAutoScroll: function() {
                            e.element.code.style.height = e.element.code.scrollHeight + "px"
                        },
                        onPaste_wysiwyg: function(e) {
//                            if (!e.clipboardData.getData)
//                                return !0;
//                            const t = s.cleanHTML(e.clipboardData.getData('text/html'));
                        	const t = e.clipboardData;
                            if (!t)
                                return !0;
//                            if (!c._charCount(t.getData("text/plain").length, !0))
//
//                                return e.preventDefault(),
//
//                                e.stopPropagation(),
//
//                                !1;
                            if (t) {
//                                e.stopPropagation();
//                                e.preventDefault();
//                                a.execCommand('insertHTML', false, t);
                            const i = t.getData("text/plain");
                            if(i.length != 0 && i != null){
                            	if(t.types.indexOf("text/plain") != -1 && t.types.indexOf("text/html") == -1) {
                                    e.stopPropagation();
                                    e.preventDefault();
                                	a.execCommand('insertText', false, i);
                            	}else{
                                    	
                                let n = t.getData("text/html");

                                    //스타일 보정 및 img태그 로컬 src일 때 삭제 부분

                                        n = n.replace(/\/><!\[endif]>/gi, "/>");

                                        n = n.replace(/<\/span><!\[endif]>/gi, "</span>");

                                        n = n.replace(/<!\[if !vml]>/gi, "");

                                        n = n.replace(/<!\[if !supportEmptyParas]>&nbsp;<!\[endif]>/gi, "");

                                        n = n.replace(/<!\[endif]>/gi, "");

                                        n = n.replace(/background:#ffffff/gi, "");

                                        n = n.replace(/mso-fareast-/gi, "");

                                        n = n.replace(/mso-hansi-/gi, "");

                                        n = n.replace(/mso-ascii-/gi, "");

                                        n = n.replace(/mso-bidi-/gi, "");

                                        n = n.replace(/mso-/gi, "");

                                        n = n.replace(/<img[^>]+file:\/\/[^>]+>/gi, "");
                                        
                                        n = n.replace(/<!--StartFragment-->/gi, "");
                                        n = n.replace(/<!--EndFragment-->/gi, "");

                                   e.stopPropagation();
                                   e.preventDefault();
                                   a.execCommand('insertHTML', false, n);
//                                 n && (e.stopPropagation(),
//                                 e.preventDefault(),
//                                 a.execCommand("insertHTML", !1, n))
                            	}
                            }
                            
                            //img 붙여넣기
                            var items = e.clipboardData.items;
                            var IMAGE_MINE_REGEX = /^image\/(p?jpeg|gif|png|PNG)$/i;
                            if(IMAGE_MINE_REGEX.test(items[0].type) && items.length == 1) {
                            	var reader = new FileReader();
                                reader.onload = function(e) {
                                    var img = document.createElement('img');
                                    img.src = e.target.result;

                                    var range = window.getSelection().getRangeAt(0);
                                    range.deleteContents();
                                    range.insertNode(img);
                                };
                                reader.readAsDataURL(items[0].getAsFile());
                                setTimeout(function(){
                                       let insertAfterContents = c.getContents();
                                       c.setContents(insertAfterContents+"<p><br></p>")                                  		   
                            	   },100)
                                return;
                            	
                            }
	                           if(e.clipboardData.types[1] != null){
	                               if(e.clipboardData.types[1].indexOf("Files") != -1){
	                            	   setTimeout(function(){
	                                       let insertAfterContents = c.getContents();
	                                       c.setContents(insertAfterContents+"<p><br></p>")                                  		   
	                            	   },100)
	                               }                            	   
	                           }
                            }
                        },
                        _onChange_historyStack: function() {
                            e.tool.save && e.tool.save.removeAttribute("disabled"),
                            c.onChange && c.onChange(a.getContents())
                        }
                    }
                      , c = {
                        onScroll: null,
                        onClick: null,
                        onKeyDown: null,
                        onKeyUp: null,
                        onDrop: null,
                        onChange: null,
                        onImageUpload: null,
                        onImageUploadError: null,
                        noticeOpen: function(e) {
                            a.addModule([y.a]),
                            y.a.open.call(a, e)
                        },
                        noticeClose: function() {
                            a.addModule([y.a]),
                            y.a.close.call(a)
                        },
                        save: function() {
                            e.element.originElement.value = a.getContents()
                        },
                        getContext: function() {
                            return e
                        },
                        getContents: function() {
                            return a.getContents()
                        },
                        getImagesInfo: function() {
                            return a._variable._imagesInfo
                        },
                        insertHTML: function(e) {
                            if (!e.nodeType || 1 !== e.nodeType) {
                                const t = s.createElement("template");
                                t.innerHTML = e,
                                e = t.firstChild || t.content.firstChild
                            }
                            let t = null;
                            (s.isFormatElement(e) || /^(IMG|IFRAME)$/i.test(e.nodeName)) && (t = s.getFormatElement(a.getSelectionNode())),
                            a.insertNode(e, t),
                            a.focus()
                        },
                        setContents: function(t, flag) {
//                            a._variable.wysiwygActive ? e.element.wysiwyg.innerHTML = s.convertContentsForEditor(t) : e.element.code.value = t,
//                            a.history.push()
							if (a._variable.wysiwygActive) {
								const n = s.convertContentsForEditor(t);
								n !== e.element.wysiwyg.innerHTML && (e.element.wysiwyg.innerHTML = n, a.history.push(flag))
							} else {
								const n = s.convertHTMLForCodeView(t);
								n !== e.element.code.value && (e.element.code.value = n)
							}
							let suneditor = document.querySelector('div#sun_editorEdit[contenteditable="true"]');
							let readmodesuneditor = document.querySelector('#sun_editorEdit[contenteditable="false"]');
							if(readmodesuneditor) {
								readmodesuneditor.style.maxHeight = "100%";
							}
							if(suneditor){// contentEditable이 원래 항상 true인 걸 전제로 만들어진 editor인데 읽기모드에서 false로 만들면서 에러를 발생시킴. 읽기모드일땐 range 설정이 필요없으므로 if문 안으로 집어넣음.
								let selection = window.getSelection();
	                        	let range = document.createRange();
	                        	let endLine = suneditor.childNodes[suneditor.childNodes.length-1]
	                        	
	                        	range.setStart(endLine, endLine.childNodes.length)
	                            range.collapse(true)
	                            selection.removeAllRanges();
	                            selection.addRange(range);
	                            a.focus()
//	                            a.setRange(range.startContainer,range.startOffset,range.endContainer,range.endOffset)
							}
                        },
                        appendContents: function(t) {
                            a._variable.wysiwygActive ? e.element.wysiwyg.innerHTML += s.convertContentsForEditor(t) : e.element.code.value += t,
                            a.history.push()
                        },
                        disabled: function() {
                            e.tool.cover.style.display = "block",
                            e.element.wysiwyg.setAttribute("contenteditable", !1),
                            e.element.code.setAttribute("disabled", "disabled")
                        },
                        enabled: function() {
                            e.tool.cover.style.display = "none",
                            e.element.wysiwyg.setAttribute("contenteditable", !0),
                            e.element.code.removeAttribute("disabled")
                        },
                        show: function() {
                            const t = e.element.topArea.style;
                            "none" === t.display && (t.display = e.option.display)
                        },
                        hide: function() {
                            e.element.topArea.style.display = "none"
                        },
                        destroy: function() {
                            l.removeEventListener("resize", r.onResize_window),
                            l.removeEventListener("scroll", r.onScroll_window),
                            s.removeItem(e.element.topArea),
                            l.Object.keys(a).forEach(function(e) {
                                delete a[e]
                            }),
                            l.Object.keys(r).forEach(function(e) {
                                delete r[e]
                            }),
                            l.Object.keys(e).forEach(function(t) {
                                delete e[t]
                            }),
                            l.Object.keys(t).forEach(function(e) {
                                delete t[e]
                            }),
                            l.Object.keys(this).forEach(function(e) {
                                delete this[e]
                            }
                            .bind(this))
                        }
                    };
                    return a.history = _(a, r._onChange_historyStack),
                    e.element.toolbar.addEventListener("mousedown", r.onMouseDown_toolbar, !1),
                    e.element.toolbar.addEventListener("click", r.onClick_toolbar, !1),
                    e.element.wysiwyg.addEventListener("mouseup", r.onMouseUp_wysiwyg, !1),
                    e.element.wysiwyg.addEventListener("click", r.onClick_wysiwyg, !1),
                    e.element.wysiwyg.addEventListener("scroll", r.onScroll_wysiwyg, !1),
                    e.element.wysiwyg.addEventListener("keydown", r.onKeyDown_wysiwyg, !1),
                    e.element.wysiwyg.addEventListener("keyup", r.onKeyUp_wysiwyg, !1),
                    e.element.wysiwyg.addEventListener("drop", r.onDrop_wysiwyg, !1),
                    e.element.wysiwyg.addEventListener("paste", r.onPaste_wysiwyg, !1),
                    "auto" === e.option.height && e.element.code.addEventListener("keyup", r._codeViewAutoScroll, !1),
                    e.element.resizingBar && (/\d+/.test(e.option.height) ? e.element.resizingBar.addEventListener("mousedown", r.onMouseDown_resizingBar, !1) : s.addClass(e.element.resizingBar, "none-resize")),
                    a._isInline && e.element.wysiwyg.addEventListener("focus", r._showToolbarInline, !1),
                    (a._isInline || a._isBalloon) && e.element.wysiwyg.addEventListener("blur", r._hideToolbar, !1),
                    l.addEventListener("resize", r.onResize_window, !1),
                    e.option.stickyToolbar > -1 && l.addEventListener("scroll", r.onScroll_window, !1),
                    c
                }(x(n, o.constructed, o.options), o.pluginCallButtons, o.plugins, o.options.lang)
            }
        };
        window.SUNEDITOR = k.init({
            plugins: p
        })
    },
    ee5k: function(module, __webpack_exports__, __webpack_require__) {
        "use strict";
        __webpack_exports__.a = {
            name: "resizing",
            add: function(core) {
                const context = core.context;
                context.resizing = {
                    _resizeClientX: 0,
                    _resizeClientY: 0,
                    _resize_plugin: "",
                    _resize_w: 0,
                    _resize_h: 0,
                    _origin_w: 0,
                    _origin_h: 0,
                    _rotateVertical: !1,
                    _resize_direction: "",
                    _move_path: null,
                    _isChange: !1
                };
                let resize_div_container = eval(this.setController_resize.call(core));
                context.resizing.resizeContainer = resize_div_container,
                context.resizing.resizeDiv = resize_div_container.getElementsByClassName("modal-resize")[0],
                context.resizing.resizeDot = resize_div_container.getElementsByClassName("resize-dot")[0],
                context.resizing.resizeDisplay = resize_div_container.getElementsByClassName("resize-display")[0];
                let resize_button = eval(this.setController_button.call(core));
                context.resizing.resizeButton = resize_button,
                resize_button.addEventListener("mousedown", function(e) {
                    e.stopPropagation()
                }, !1);
                let resize_handles = context.resizing.resizeHandles = resize_div_container.querySelectorAll(".sun-editor-name-resize-handle");
                context.resizing.resizeButtonGroup = resize_button.getElementsByClassName("sun-editor-id-resize-button-group")[0],
                resize_handles[0].addEventListener("mousedown", this.onMouseDown_resize_handle.bind(core)),
                resize_handles[1].addEventListener("mousedown", this.onMouseDown_resize_handle.bind(core)),
                resize_handles[2].addEventListener("mousedown", this.onMouseDown_resize_handle.bind(core)),
                resize_handles[3].addEventListener("mousedown", this.onMouseDown_resize_handle.bind(core)),
                resize_handles[4].addEventListener("mousedown", this.onMouseDown_resize_handle.bind(core)),
                resize_handles[5].addEventListener("mousedown", this.onMouseDown_resize_handle.bind(core)),
                resize_handles[6].addEventListener("mousedown", this.onMouseDown_resize_handle.bind(core)),
                resize_handles[7].addEventListener("mousedown", this.onMouseDown_resize_handle.bind(core)),
                resize_button.addEventListener("click", this.onClick_resizeButton.bind(core)),
                context.element.relative.appendChild(resize_div_container),
                context.element.relative.appendChild(resize_button),
                resize_div_container = null,
                resize_button = null,
                resize_handles = null
            },
            setController_resize: function() {
                const e = this.util.createElement("DIV");
                return e.className = "modal-resize-container",
                e.style.display = "none",
                e.innerHTML = '<div class="modal-resize">   <div class="resize-display"></div></div><div class="resize-dot">   <span class="tl sun-editor-name-resize-handle"></span>   <span class="tr sun-editor-name-resize-handle"></span>   <span class="bl sun-editor-name-resize-handle"></span>   <span class="br sun-editor-name-resize-handle"></span>   <span class="lw sun-editor-name-resize-handle"></span>   <span class="th sun-editor-name-resize-handle"></span>   <span class="rw sun-editor-name-resize-handle"></span>   <span class="bh sun-editor-name-resize-handle"></span></div>',
                e
            },
            setController_button: function() {
                const e = this.lang
                  , t = this.util.createElement("DIV");
                return t.className = "se-controller controller-resizing",
                t.style.display = "none",
                t.innerHTML = '<div class="arrow arrow-up"></div><div class="btn-group sun-editor-id-resize-button-group">   <button type="button" data-command="percent" data-value="1" class="se-tooltip">       <span class="note-fontsize-10">100%</span>       <span class="se-tooltip-inner"><span class="se-tooltip-text">' + e.controller.resize100 + '</span></span>   </button>   <button type="button" data-command="percent" data-value="0.75" class="se-tooltip">       <span class="note-fontsize-10">75%</span>       <span class="se-tooltip-inner"><span class="se-tooltip-text">' + e.controller.resize75 + '</span></span>   </button>   <button type="button" data-command="percent" data-value="0.5" class="se-tooltip">       <span class="note-fontsize-10">50%</span>       <span class="se-tooltip-inner"><span class="se-tooltip-text">' + e.controller.resize50 + '</span></span>   </button>   <button type="button" data-command="percent" data-value="0.25" class="se-tooltip">       <span class="note-fontsize-10">25%</span>       <span class="se-tooltip-inner"><span class="se-tooltip-text">' + e.controller.resize25 + '</span></span>   </button>   <button type="button" data-command="rotate" data-value="-90" class="se-tooltip">       <i class="icon-rotate-left"></i>       <span class="se-tooltip-inner"><span class="se-tooltip-text">' + e.controller.rotateLeft + '</span></span>   </button>   <button type="button" data-command="rotate" data-value="90" class="se-tooltip">       <i class="icon-rotate-right"></i>       <span class="se-tooltip-inner"><span class="se-tooltip-text">' + e.controller.rotateRight + '</span></span>   </button></div><div class="btn-group">   <button type="button" data-command="mirror" data-value="h" class="se-tooltip">       <i class="icon-mirror-horizontal"></i>       <span class="se-tooltip-inner"><span class="se-tooltip-text">' + e.controller.mirrorHorizontal + '</span></span>   </button>   <button type="button" data-command="mirror" data-value="v" class="se-tooltip">       <i class="icon-mirror-vertical"></i>       <span class="se-tooltip-inner"><span class="se-tooltip-text">' + e.controller.mirrorVertical + '</span></span>   </button>   <button type="button" data-command="revert" class="se-tooltip">       <i class="icon-revert"></i>       <span class="se-tooltip-inner"><span class="se-tooltip-text">' + e.dialogBox.revertButton + '</span></span>   </button>  <button type="button" data-command="delete" class="se-tooltip">       <i class="icon-delete"></i>       <span class="se-tooltip-inner"><span class="se-tooltip-text">' + e.controller.remove + "</span></span>   </button></div>",
                t
            },
            call_controller_resize: function(e, t) {
                const i = this.context.resizing;
                i._resize_plugin = t;
                const n = i.resizeContainer
                  , o = i.resizeDiv
                  , l = this.util.getOffset(e)
                  , s = i._rotateVertical = /^(90|270)$/.test(Math.abs(e.getAttribute("data-rotate")).toString())
                  , a = s ? e.offsetHeight : e.offsetWidth
                  , r = s ? e.offsetWidth : e.offsetHeight
                  , c = l.top
                  , d = l.left - this.context.element.wysiwyg.scrollLeft;
                n.style.top = c + "px",
                n.style.left = d + "px",
                n.style.width = a + "px",
                n.style.height = r + "px",
                o.style.top = "0px",
                o.style.left = "0px",
                o.style.width = a + "px",
                o.style.height = r + "px";
                let u = e.getAttribute("data-align") || "basic";
                u = "none" === u ? "basic" : u,
                this.util.changeTxt(i.resizeDisplay, this.lang.dialogBox[u] + " (" + a + " x " + r + ")");
                const h = this.context[t]._resizing ? "flex" : "none"
                  , g = i.resizeHandles;
                i.resizeButtonGroup.style.display = h;
                for (let e = 0, t = g.length; e < t; e++)
                    g[e].style.display = h;
                this.controllersOn(i.resizeContainer, i.resizeButton);
                const m = this.context.element.relative.offsetWidth - d - i.resizeButton.offsetWidth;
                i.resizeButton.style.top = r + c + 60 + "px",
                i.resizeButton.style.left = d + (m < 0 ? m : 0) + "px",
                i._resize_w = a,
                i._resize_h = r;
                const p = (e.getAttribute("origin-size") || "").split(",");
                return i._origin_w = p[0] || e.naturalWidth,
                i._origin_h = p[1] || e.naturalHeight,
                this._resizingName = t,
                {
                    w: a,
                    h: r,
                    t: c,
                    l: d
                }
            },
            create_caption: function() {
                const e = this.util.createElement("FIGCAPTION");
                return e.setAttribute("contenteditable", !0),
                e.innerHTML = "<div>" + this.lang.dialogBox.caption + "</div>",
                e
            },
            set_cover: function(e) {
                const t = this.util.createElement("FIGURE");
                return t.className = "sun-editor-figure-cover",
                t.appendChild(e),
                t
            },
            set_container: function(e, t) {
                const i = this.util.createElement("DIV");
                return i.className = "sun-editor-id-comp " + t,
                i.setAttribute("contenteditable", !1),
                i.appendChild(e),
                i
            },
            onClick_resizeButton: function(e) {
                e.stopPropagation();
                const t = e.target
                  , i = t.getAttribute("data-command") || t.parentNode.getAttribute("data-command");
                if (!i)
                    return;
                const n = t.getAttribute("data-value") || t.parentNode.getAttribute("data-value")
                  , o = this.context[this.context.resizing._resize_plugin]._element
                  , l = this.plugins[this.context.resizing._resize_plugin];
                if (e.preventDefault(),
                /percent/.test(i)) {
                    this.plugins.resizing.resetTransform.call(this, o),
                    l.setPercentSize.call(this, 100 * n + "%", "auto");
                    const e = this.plugins.resizing.call_controller_resize.call(this, o, this.context.resizing._resize_plugin);
                    l.onModifyMode.call(this, o, e)
                } else if (/mirror/.test(i)) {
                    const e = o.getAttribute("data-rotate") || "0";
                    let t = o.getAttribute("data-rotateX") || ""
                      , i = o.getAttribute("data-rotateY") || "";
                    "h" === n && !this.context.resizing._rotateVertical || "v" === n && this.context.resizing._rotateVertical ? i = i ? "" : "180" : t = t ? "" : "180",
                    o.setAttribute("data-rotateX", t),
                    o.setAttribute("data-rotateY", i),
                    this.plugins.resizing._setTransForm(o, e, t, i)
                } else if (/rotate/.test(i)) {
                    const e = this.context.resizing
                      , t = 1 * o.getAttribute("data-rotate") + 1 * n
                      , i = Math.abs(t) >= 360 ? 0 : t;
                    o.setAttribute("data-rotate", i),
                    e._rotateVertical = /^(90|270)$/.test(Math.abs(i).toString()),
                    this.plugins.resizing.setTransformSize.call(this, o);
                    const s = this.plugins.resizing.call_controller_resize.call(this, o, e._resize_plugin);
                    l.onModifyMode.call(this, o, s)
                } else if (/revert/.test(i)) {
                    l.setAutoSize ? l.setAutoSize.call(this) : (l.resetAlign.call(this),
                    this.plugins.resizing.resetTransform.call(this, o));
                    const e = this.plugins.resizing.call_controller_resize.call(this, o, this.context.resizing._resize_plugin);
                    l.onModifyMode.call(this, o, e)
                } else
                    /update/.test(i) ? (l.openModify.call(this),
                    this.controllersOff()) : /delete/.test(i) && l.destroy.call(this);
                this.history.push()
            },
            resetTransform: function(e) {
                const t = (e.getAttribute("data-origin") || "").split(",");
                this.context.resizing._rotateVertical = !1,
                e.style.transform = "",
                e.style.transformOrigin = "",
                e.setAttribute("data-rotate", ""),
                e.setAttribute("data-rotateX", ""),
                e.setAttribute("data-rotateY", ""),
                e.style.width = t[0] + "px" || !1,
                e.style.height = t[1] + "px" || !1,
                this.plugins.resizing.setTransformSize.call(this, e)
            },
            setTransformSize: function(e) {
                const t = this.util.getParentElement(e, ".sun-editor-figure-cover")
                  , i = this.context.resizing._rotateVertical
                  , n = 1 * e.getAttribute("data-rotate")
                  , o = e.offsetWidth
                  , l = e.offsetHeight
                  , s = i ? l : o
                  , a = i ? o : l;
                this.plugins[this.context.resizing._resize_plugin].cancelPercentAttr.call(this),
                this.plugins[this.context.resizing._resize_plugin].setSize.call(this, o, l),
                t.style.width = s + "px",
                t.style.height = this.context[this.context.resizing._resize_plugin]._caption ? "" : a + "px";
                let r = "";
                if (i) {
                    let e = o / 2 + "px " + o / 2 + "px 0"
                      , t = l / 2 + "px " + l / 2 + "px 0";
                    r = 90 === n || -270 === n ? t : e
                }
                e.style.transformOrigin = r,
                this.plugins.resizing._setTransForm(e, n.toString(), e.getAttribute("data-rotateX") || "", e.getAttribute("data-rotateY") || ""),
                this.plugins.resizing._setCaptionPosition.call(this, e, this.util.getChildElement(this.util.getParentElement(e, ".sun-editor-figure-cover"), "FIGCAPTION"))
            },
            _setTransForm: function(e, t, i, n) {
                let o = (e.offsetWidth - e.offsetHeight) * (/-/.test(t) ? 1 : -1)
                  , l = "";
                if (/[1-9]/.test(t) && (i || n))
                    switch (l = i ? "Y" : "X",
                    t) {
                    case "90":
                        l = i && n ? "X" : n ? l : "";
                        break;
                    case "270":
                        o *= -1,
                        l = i && n ? "Y" : i ? l : "";
                        break;
                    case "-90":
                        l = i && n ? "Y" : i ? l : "";
                        break;
                    case "-270":
                        o *= -1,
                        l = i && n ? "X" : n ? l : "";
                        break;
                    default:
                        l = ""
                    }
                e.style.transform = "rotate(" + t + "deg)" + (i ? " rotateX(" + i + "deg)" : "") + (n ? " rotateY(" + n + "deg)" : "") + (l ? " translate" + l + "(" + o + "px)" : "")
            },
            _setCaptionPosition: function(e, t) {
                t && (t.style.marginTop = (this.context.resizing._rotateVertical ? e.offsetWidth - e.offsetHeight : 0) + "px")
            },
            onMouseDown_resize_handle: function(e) {
                const t = this.context.resizing
                  , i = t._resize_direction = e.target.classList[0];
                e.stopPropagation(),
                e.preventDefault(),
                t._resizeClientX = e.clientX,
                t._resizeClientY = e.clientY,
                this.context.element.resizeBackground.style.display = "block",
                t.resizeButton.style.display = "none",
                t.resizeDiv.style.float = /l/.test(i) ? "right" : /r/.test(i) ? "left" : "none";
                const n = function() {
                    const e = t._isChange;
                    t._isChange = !1,
                    document.removeEventListener("mousemove", o),
                    document.removeEventListener("mouseup", n),
                    this.plugins.resizing.cancel_controller_resize.call(this),
                    e && this.history.push()
                }
                .bind(this)
                  , o = this.plugins.resizing.resizing_element.bind(this, t, i, this.context[t._resize_plugin]);
                document.addEventListener("mousemove", o),
                document.addEventListener("mouseup", n)
            },
            resizing_element: function(e, t, i, n) {
                const o = n.clientX
                  , l = n.clientY;
                let s = i._element_w
                  , a = i._element_h;
                const r = i._element_w + (/r/.test(t) ? o - e._resizeClientX : e._resizeClientX - o)
                  , c = i._element_h + (/b/.test(t) ? l - e._resizeClientY : e._resizeClientY - l)
                  , d = i._element_h / i._element_w * r;
                /t/.test(t) && (e.resizeDiv.style.top = i._element_h - (/h/.test(t) ? c : d) + "px"),
                /l/.test(t) && (e.resizeDiv.style.left = i._element_w - r + "px"),
                /r|l/.test(t) && (e.resizeDiv.style.width = r + "px",
                s = r),
                /^(t|b)[^h]$/.test(t) ? (e.resizeDiv.style.height = d + "px",
                a = d) : /^(t|b)h$/.test(t) && (e.resizeDiv.style.height = c + "px",
                a = c),
                e._resize_w = s,
                e._resize_h = a,
                this.util.changeTxt(e.resizeDisplay, Math.round(s) + " x " + Math.round(a)),
                e._isChange = !0
            },
            cancel_controller_resize: function() {
                const e = this.context.resizing._rotateVertical;
                this.controllersOff(),
                this.context.element.resizeBackground.style.display = "none";
                const t = e ? this.context.resizing._resize_h : this.context.resizing._resize_w
                  , i = e ? this.context.resizing._resize_w : this.context.resizing._resize_h;
                this.plugins[this.context.resizing._resize_plugin].setSize.call(this, t, i, e),
                this.plugins.resizing.setTransformSize.call(this, this.context[this.context.resizing._resize_plugin]._element),
                this.plugins[this.context.resizing._resize_plugin].init.call(this)
            }
        }
    },
    g4XY: function(module, __webpack_exports__, __webpack_require__) {
        "use strict";
        __webpack_exports__.a = {
            name: "horizontalRule",
            add: function(core, targetElement) {
                let listDiv = eval(this.setSubmenu.call(core));
                listDiv.getElementsByTagName("UL")[0].addEventListener("click", this.horizontalRulePick.bind(core)),
                targetElement.parentNode.appendChild(listDiv),
                listDiv = null
            },
            setSubmenu: function() {
                const e = this.lang
                  , t = this.util.createElement("DIV");
                return t.className = "sun-editor-submenu layer_editor",
                t.style.display = "none",
                t.innerHTML = '<div class="inner_layer layer_line">   <ul class="list_editor">       <li>           <button type="button" class="btn_edit btn_line se-tooltip" data-command="horizontalRule" data-value="solid">               <hr style="border-width: 1px 0 0; border-style: solid none none; border-color: black; border-image: initial; height: 1px;" />               <span class="se-tooltip-inner"><span class="se-tooltip-text">' + e.toolbar.hr_solid + '</span></span>           </button>       </li>       <li>           <button type="button" class="btn_edit btn_line se-tooltip" data-command="horizontalRule" data-value="dotted">               <hr style="border-width: 1px 0 0; border-style: dotted none none; border-color: black; border-image: initial; height: 1px;" />               <span class="se-tooltip-inner"><span class="se-tooltip-text">' + e.toolbar.hr_dotted + '</span></span>           </button>       </li>       <li>           <button type="button" class="btn_edit btn_line se-tooltip" data-command="horizontalRule" data-value="dashed">               <hr style="border-width: 1px 0 0; border-style: dashed none none; border-color: black; border-image: initial; height: 1px;" />               <span class="se-tooltip-inner"><span class="se-tooltip-text">' + e.toolbar.hr_dashed + "</span></span>           </button>       </li>   </ul></div>",
                t
            },
            appendHr: function(e) {
                const t = this.util.createElement("HR");
                t.className = e,
                this.focus();
                let i = this.insertComponent(t);
                this.setRange(i, 0, i, 0)
            },
            horizontalRulePick: function(e) {
                e.preventDefault(),
                e.stopPropagation();
                let t = e.target
                  , i = null;
                for (; !i && !/UL/i.test(t.tagName); )
                    i = t.getAttribute("data-value"),
                    t = t.parentNode;
                i && (this.plugins.horizontalRule.appendHr.call(this, i),
                this.submenuOff(),
                this.focus())
            }
        }
    },
    gMuy: function(module, __webpack_exports__, __webpack_require__) {
        "use strict";
        __webpack_exports__.a = {
            name: "list",
            add: function(core, targetElement) {
                const context = core.context;
                context.list = {
                    _list: [],
                    currentList: ""
                };
                let listDiv = eval(this.setSubmenu.call(core));
                listDiv.getElementsByTagName("UL")[0].addEventListener("click", this.pickup.bind(core)),
                context.list._list = listDiv.getElementsByTagName("UL")[0].querySelectorAll("li button"),
                targetElement.parentNode.appendChild(listDiv),
                listDiv = null
            },
            setSubmenu: function() {
                const e = this.lang
                  , t = this.util.createElement("DIV");
                return t.className = "sun-editor-submenu layer_editor",
                t.style.display = "none",
                t.innerHTML = '<div class="inner_layer">   <ul class="list_editor">       <li><button type="button" class="btn_edit se-tooltip" data-command="OL">           <i class="icon-list-number"></i>           <span class="se-tooltip-inner"><span class="se-tooltip-text">' + e.toolbar.orderList + '</span></span>       </button></li>       <li><button type="button" class="btn_edit se-tooltip" data-command="UL">           <i class="icon-list-bullets"></i>           <span class="se-tooltip-inner"><span class="se-tooltip-text">' + e.toolbar.unorderList + "</span></span>       </button></li>   </ul></div>",
                t
            },
            on: function() {
                const e = this.context.list
                  , t = e._list
                  , i = this.commandMap.LI.getAttribute("data-focus") || "";
                if (i !== e.currentList) {
                    for (let e = 0, n = t.length; e < n; e++)
                        i === t[e].getAttribute("data-command") ? this.util.addClass(t[e], "on") : this.util.removeClass(t[e], "on");
                    e.currentList = i
                }
            },
            pickup: function(e) {
                e.preventDefault(),
                e.stopPropagation();
                let t = e.target
                  , i = "";
                for (; !i && !/^UL$/i.test(t.tagName); )
                    i = t.getAttribute("data-command"),
                    t = t.parentNode;
                if (!i)
                    return;
                const n = this.getSelectedElementsAndComponents();
                if (!n || 0 === n.length)
                    return;
                let o = !0
                  , l = null
                  , s = null;
                const a = n[0]
                  , r = n[n.length - 1];
                let c = !this.util.isListCell(a) && !this.util.isComponent(a) || a.previousElementSibling ? a.previousElementSibling : a.parentNode.previousElementSibling
                  , d = !this.util.isListCell(r) && !this.util.isComponent(r) || r.nextElementSibling ? r.nextElementSibling : r.parentNode.nextElementSibling;
                for (let e = 0, t = n.length; e < t; e++)
                    if (!this.util.isList(this.util.getRangeFormatElement(n[e], function(t) {
                        return this.getRangeFormatElement(t) && t !== n[e]
                    }
                    .bind(this.util)))) {
                        o = !1;
                        break
                    }
                if (!o || c && i === c.tagName || d && i === d.tagName) {
                    const e = c ? c.parentNode : c
                      , t = d ? d.parentNode : d;
                    c = e && !this.util.isWysiwygDiv(e) && e.nodeName === i ? e : c,
                    d = t && !this.util.isWysiwygDiv(t) && t.nodeName === i ? t : d;
                    const o = c && c.tagName === i
                      , a = d && d.tagName === i;
                    let r = o ? c : this.util.createElement(i)
                      , u = null
                      , h = null
                      , g = null
                      , m = null;
                    const p = function(e) {
                        return !this.isComponent(e) && !this.isList(e)
                    }
                    .bind(this.util);
                    for (let e, t, l, s, a, c, d, m, _, f = 0, b = n.length; f < b; f++)
                        if (0 !== (t = n[f]).childNodes.length || this.util.ignoreNodeChange(t)) {
                            if (s = n[f + 1],
                            a = t.parentNode,
                            c = s ? s.parentNode : null,
                            l = this.util.isListCell(t),
                            _ = this.util.isRangeFormatElement(a) ? a : null,
                            d = l && !this.util.isWysiwygDiv(a) ? a.parentNode : a,
                            m = l && !this.util.isWysiwygDiv(a) ? s ? a.nextSibling : a : t.nextSibling,
                            e = this.util.createElement("LI"),
                            this.util.isComponent(t)) {
                                const i = /^HR$/i.test(t.nodeName);
                                i || e.appendChild(this.util.createTextNode(this.util.zeroWidthSpace)),
                                e.innerHTML += t.outerHTML,
                                i && e.appendChild(this.util.createTextNode(this.util.zeroWidthSpace))
                            } else
                                e.innerHTML = t.innerHTML;
                            r.appendChild(e),
                            s || (h = r),
                            s && d === c && !this.util.isRangeFormatElement(m) || (u || (u = r),
                            o && s && d === c || s && this.util.isList(c) && c === a || r.parentNode !== d && d.insertBefore(r, m)),
                            this.util.removeItem(t),
                            o && null === g && (g = r.children.length - 1),
                            s && this.util.getRangeFormatElement(c, p) !== this.util.getRangeFormatElement(a, p) && (r = this.util.createElement(i)),
                            _ && 0 === _.children.length && this.util.removeItem(_)
                        } else
                            this.util.removeItem(t);
                    g && (u = u.children[g]),
                    a && (m = r.children.length - 1,
                    r.innerHTML += d.innerHTML,
                    h = r.children[m],
                    this.util.removeItem(d)),
                    l = s = this.util.getEdgeChildNodes(u.firstChild, h.lastChild)
                } else {
                    const e = this.util.getRangeFormatElement(this.getSelectionNode())
                      , t = e && e.tagName === i;
                    let o, a;
                    const r = function(e) {
                        return !this.isComponent(e)
                    }
                    .bind(this.util);
                    t || (a = this.util.createElement(i));
                    for (let e, c, d = 0, u = n.length; d < u; d++)
                        if ((c = this.util.getRangeFormatElement(n[d], r)) && this.util.isList(c)) {
                            if (e)
                                if (e !== c) {
                                    const s = this.detachRangeFormatElement(o.r, o.f, a, !1, !0);
                                    l || (l = s),
                                    t || (a = this.util.createElement(i)),
                                    o = {
                                        r: e = c,
                                        f: [this.util.getParentElement(n[d], "LI")]
                                    }
                                } else
                                    o.f.push(this.util.getParentElement(n[d], "LI"));
                            else
                                o = {
                                    r: e = c,
                                    f: [this.util.getParentElement(n[d], "LI")]
                                };
                            d === u - 1 && (s = this.detachRangeFormatElement(o.r, o.f, a, !1, !0),
                            l || (l = s))
                        }
                }
                this.history.push(),
                n.length > 1 ? this.setRange(l.sc, 0, s.ec, s.ec.textContent.length) : this.setRange(l.ec, l.ec.textContent.length, s.ec, s.ec.textContent.length),
                this.submenuOff(),
                this.focus()
            }
        }
    },
    hlhS: function(module, __webpack_exports__, __webpack_require__) {
        "use strict";
        var _modules_dialog__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("1kvd")
          , _modules_resizing__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("ee5k");
        __webpack_exports__.a = {
            name: "video",
            add: function(core) {
                core.addModule([_modules_dialog__WEBPACK_IMPORTED_MODULE_0__.a, _modules_resizing__WEBPACK_IMPORTED_MODULE_1__.a]);
                const context = core.context;
                context.video = {
                    _container: null,
                    _cover: null,
                    _element: null,
                    _resizingDiv: null,
                    _element_w: context.option.videoWidth,
                    _element_h: context.option.videoHeight,
                    _element_l: 0,
                    _element_t: 0,
                    _origin_w: context.option.videoWidth,
                    _origin_h: context.option.videoHeight,
                    _caption: null,
                    captionCheckEl: null,
                    _captionChecked: !1,
                    _proportionChecked: !0,
                    _align: "none",
                    _floatClassRegExp: "float\\-[a-z]+",
                    _resizing: context.option.videoResizing,
                    _youtubeQuery: context.option.youtubeQuery
                };
                let video_dialog = eval(this.setDialog.call(core));
                context.video.modal = video_dialog,
                context.video.focusElement = video_dialog.getElementsByClassName("sun-editor-id-video-url")[0],
                context.video.captionCheckEl = video_dialog.getElementsByClassName("suneditor-id-video-check-caption")[0],
                video_dialog.getElementsByClassName("se-btn-primary")[0].addEventListener("click", this.submit.bind(core)),
                context.video.videoWidth = {},
                context.video.videoHeight = {},
                context.option.videoResizing && (context.video.videoWidth = video_dialog.getElementsByClassName("sun-editor-id-video-x")[0],
                context.video.videoHeight = video_dialog.getElementsByClassName("sun-editor-id-video-y")[0],
                context.video.proportion = video_dialog.getElementsByClassName("suneditor-id-video-check-proportion")[0],
                context.video.videoWidth.value = context.option.videoWidth,
                context.video.videoHeight.value = context.option.videoHeight,
                context.video.videoWidth.addEventListener("change", this.setInputSize.bind(core, "x")),
                context.video.videoHeight.addEventListener("change", this.setInputSize.bind(core, "y")),
                video_dialog.getElementsByClassName("sun-editor-id-video-revert-button")[0].addEventListener("click", this.sizeRevert.bind(core))),
                context.dialog.modal.appendChild(video_dialog),
                video_dialog = null
            },
            setDialog: function() {
                const e = this.context.option
                  , t = this.lang
                  , i = this.util.createElement("DIV");
                i.className = "modal-content sun-editor-id-dialog-video",
                i.style.display = "none";
                let n = '<form class="editor_video">   <div class="modal-header">       <button type="button" data-command="close" class="close" aria-label="Close" title="' + t.dialogBox.close + '">           <i aria-hidden="true" data-command="close" class="icon-cancel"></i>       </button>       <h5 class="modal-title">' + t.dialogBox.videoBox.title + '</h5>   </div>   <div class="modal-body">       <div class="form-group">           <label>' + t.dialogBox.videoBox.url + '</label>           <input class="form-control sun-editor-id-video-url" type="text" />       </div>';
                return e.videoResizing && (n += '   <div class="form-group">       <div class="size-text"><label class="size-w">' + t.dialogBox.width + '</label><label class="size-x">&nbsp;</label><label class="size-h">' + t.dialogBox.height + '</label></div>       <input type="number" class="form-size-control sun-editor-id-video-x" /><label class="size-x">x</label><input type="number" class="form-size-control sun-editor-id-video-y" />       <label><input type="checkbox" class="suneditor-id-video-check-proportion" style="margin-left: 20px;" checked/>&nbsp;' + t.dialogBox.proportion + '</label>       <button type="button" title="' + t.dialogBox.revertButton + '" class="btn_editor btn-revert sun-editor-id-video-revert-button" style="float: right;"><i class="icon-revert"></i></button>   </div>'),
                n += '       <div class="form-group-footer">           <label><input type="checkbox" class="suneditor-id-video-check-caption" />&nbsp;' + t.dialogBox.caption + '</label>       </div>   </div>   <div class="modal-footer">       <div style="float: left;">           <label><input type="radio" name="suneditor_video_radio" class="modal-radio" value="none" checked>' + t.dialogBox.basic + '</label>           <label><input type="radio" name="suneditor_video_radio" class="modal-radio" value="left">' + t.dialogBox.left + '</label>           <label><input type="radio" name="suneditor_video_radio" class="modal-radio" value="center">' + t.dialogBox.center + '</label>           <label><input type="radio" name="suneditor_video_radio" class="modal-radio" value="right">' + t.dialogBox.right + '</label>       </div>       <button type="submit" class="btn se-btn-primary sun-editor-id-submit-video" title="' + t.dialogBox.submitButton + '"><span>' + t.dialogBox.submitButton + "</span></button>   </div></form>",
                i.innerHTML = n,
                i
            },
            setInputSize: function(e) {
                this.context.video.proportion.checked && ("x" === e ? this.context.video.videoHeight.value = Math.round(this.context.video._element_h / this.context.video._element_w * this.context.video.videoWidth.value) : this.context.video.videoWidth.value = Math.round(this.context.video._element_w / this.context.video._element_h * this.context.video.videoHeight.value))
            },
            submit: function(e) {
                this.showLoading(),
                e.preventDefault(),
                e.stopPropagation(),
                this.context.video._captionChecked = this.context.video.captionCheckEl.checked;
                const t = function() {
                    if (0 === this.context.video.focusElement.value.trim().length)
                        return !1;
                    const e = this.context.video
                      , t = /^\d+$/.test(e.videoWidth.value) ? e.videoWidth.value : this.context.option.videoWidth
                      , i = /^\d+$/.test(e.videoHeight.value) ? e.videoHeight.value : this.context.option.videoHeight;
                    let n = null
                      , o = null
                      , l = null
                      , s = null
                      , a = e.focusElement.value.trim();
                    if (e._align = e.modal.querySelector('input[name="suneditor_video_radio"]:checked').value,
                    /^<iframe.*\/iframe>$/.test(a))
                        o = (new DOMParser).parseFromString(a, "text/html").getElementsByTagName("iframe")[0];
                    else {
                        if (o = this.util.createElement("IFRAME"),
                        /youtu\.?be/.test(a) && (a = a.replace("watch?v=", ""),
                        /^\/\/.+\/embed\//.test(a) || (a = a.replace(a.match(/\/\/.+\//)[0], "//www.youtube.com/embed/")),
                        e._youtubeQuery.length > 0))
                            if (/\?/.test(a)) {
                                const t = a.split("?");
                                a = t[0] + "?" + e._youtubeQuery + "&" + t[1]
                            } else
                                a += "?" + e._youtubeQuery;
                        o.src = a
                    }
                    this.context.dialog.updateModal ? (e._element.src = o.src,
                    s = e._container,
                    l = this.util.getParentElement(e._element, ".sun-editor-figure-cover"),
                    o = e._element,
                    n = e._resizingDiv) : (o.frameBorder = "0",
                    o.allowFullscreen = !0,
                    o.contentDocument,
                    o.onload = function() {
                        this.setAttribute("origin-size", this.offsetWidth + "," + this.offsetHeight),
                        this.setAttribute("data-origin", this.offsetWidth + "," + this.offsetHeight),
                        this.style.height = this.offsetHeight + "px"
                    }
                    .bind(o),
                    e._element = o,
                    l = this.plugins.resizing.set_cover.call(this, o),
                    e._resizingDiv = n = this.util.createElement("DIV"),
                    n.className = "sun-editor-id-iframe-inner-resizing-cover",
                    l.appendChild(n),
                    s = this.plugins.resizing.set_container.call(this, l, "sun-editor-id-iframe-container"));
                    const r = 1 * t !== o.offsetWidth || 1 * i !== o.offsetHeight;
                    e._resizing && (this.context.video._proportionChecked = e.proportion.checked,
                    o.setAttribute("data-proportion", e._proportionChecked)),
                    e._captionChecked ? e._caption || (e._caption = this.plugins.resizing.create_caption.call(this),
                    l.appendChild(e._caption)) : e._caption && (this.util.removeItem(e._caption),
                    e._caption = null),
                    r && this.plugins.video.setSize.call(this, t, i),
                    e._align && "none" !== e._align ? l.style.margin = "auto" : l.style.margin = "0",
                    this.util.removeClass(s, this.context.video._floatClassRegExp),
                    this.util.addClass(s, "float-" + e._align),
                    o.setAttribute("data-align", e._align),
                    this.context.dialog.updateModal ? (/\d+/.test(l.style.height) || e._resizing && r || this.context.resizing._rotateVertical && e._captionChecked) && this.plugins.resizing.setTransformSize.call(this, o) : this.insertComponent(s),
                    this.history.push()
                }
                .bind(this);
                try {
                    t()
                } finally {
                    this.plugins.dialog.close.call(this),
                    this.closeLoading()
                }
                return this.focus(),
                !1
            },
            sizeRevert: function() {
                const e = this.context.video;
                e._origin_w && (e.videoWidth.value = e._element_w = e._origin_w,
                e.videoHeight.value = e._element_h = e._origin_h)
            },
            onModifyMode: function(e, t) {
                const i = this.context.video;
                i._element = e,
                i._cover = this.util.getParentElement(e, ".sun-editor-figure-cover"),
                i._container = this.util.getParentElement(e, ".sun-editor-id-iframe-container"),
                i._caption = this.util.getChildElement(i._cover, "FIGCAPTION"),
                i._resizingDiv = this.util.getChildElement(i._cover, ".sun-editor-id-iframe-inner-resizing-cover"),
                i._align = e.getAttribute("data-align") || "none",
                i._element_w = t.w,
                i._element_h = t.h,
                i._element_t = t.t,
                i._element_l = t.l;
                let n = i._element.getAttribute("data-origin");
                n ? (n = n.split(","),
                i._origin_w = 1 * n[0],
                i._origin_h = 1 * n[1]) : (i._origin_w = t.w,
                i._origin_h = t.h,
                i._element.setAttribute("data-origin", t.w + "," + t.h))
            },
            openModify: function() {
                const e = this.context.video;
                e.focusElement.value = e._element.src,
                e.videoWidth.value = e._element.offsetWidth,
                e.videoHeight.value = e._element.offsetHeight,
                e._captionChecked = e.captionCheckEl.checked = !!e._caption,
                e.modal.querySelector('input[name="suneditor_video_radio"][value="' + e._align + '"]').checked = !0,
                e._resizing && (e.proportion.checked = e._proportionChecked = "true" === e._element.getAttribute("data-proportion"),
                e.proportion.disabled = !1),
                this.plugins.dialog.open.call(this, "video", !0)
            },
            setSize: function(e, t, i) {
                const n = this.context.video;
                n._element.style.width = e + "px",
                n._element.style.height = t + "px",
                n._resizingDiv.style.height = (i ? e : t) + "px"
            },
            setPercentSize: function(e) {
                const t = this.context.video;
                t._container.style.width = e,
                t._container.style.height = "",
                t._cover.style.width = "100%",
                t._cover.style.height = "",
                t._element.style.width = "100%",
                t._element.style.height = t._resizingDiv.style.height = t._origin_h / t._origin_w * t._element.offsetWidth + "px",
                /100/.test(e) && (this.util.removeClass(t._container, this.context.video._floatClassRegExp),
                this.util.addClass(t._container, "float-center"))
            },
            cancelPercentAttr: function() {
                const e = this.context.video;
                e._cover.style.width = "",
                e._cover.style.height = "",
                e._container.style.width = "",
                e._container.style.height = "",
                this.util.removeClass(e._container, this.context.video._floatClassRegExp),
                this.util.addClass(e._container, "float-" + e._align)
            },
            resetAlign: function() {
                const e = this.context.video;
                e._element.setAttribute("data-align", ""),
                e._align = "none",
                e._cover.style.margin = "0",
                this.util.removeClass(e._container, e._floatClassRegExp)
            },
            destroy: function() {
                this.util.removeItem(this.context.video._container),
                this.plugins.video.init.call(this),
                this.controllersOff()
            },
            init: function() {
                const e = this.context.video;
                e.focusElement.value = "",
                e.captionCheckEl.checked = !1,
                e.modal.querySelector('input[name="suneditor_video_radio"][value="none"]').checked = !0,
                e._resizing && (e.videoWidth.value = this.context.option.videoWidth,
                e.videoHeight.value = this.context.option.videoHeight,
                e.proportion.checked = !0,
                e.proportion.disabled = !0)
            }
        }
    },
    s0fJ: function(module, __webpack_exports__, __webpack_require__) {
        "use strict";
        var _modules_colorPicker__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("EjF6");
        __webpack_exports__.a = {
            name: "hiliteColor",
            add: function(core, targetElement) {
                core.addModule([_modules_colorPicker__WEBPACK_IMPORTED_MODULE_0__.a]);
                const context = core.context;
                context.hiliteColor = {
                    previewEl: null,
                    colorInput: null,
                    colorList: null
                };
                let listDiv = eval(this.setSubmenu.call(core));
                context.hiliteColor.colorInput = listDiv.getElementsByClassName("sun-editor-id-submenu-color-input")[0],
                context.hiliteColor.colorInput.addEventListener("keyup", this.onChangeInput.bind(core)),
                listDiv.getElementsByClassName("sun-editor-id-submenu-color-submit")[0].addEventListener("click", this.submit.bind(core)),
                listDiv.getElementsByClassName("sun-editor-id-submenu-color-default")[0].addEventListener("click", this.remove.bind(core)),
                listDiv.getElementsByTagName("UL")[0].addEventListener("click", this.pickup.bind(core)),
                context.hiliteColor.colorList = listDiv.getElementsByTagName("UL")[0].querySelectorAll("li button"),
                targetElement.parentNode.appendChild(listDiv),
                listDiv = null
            },
            setSubmenu: function() {
                const e = this.context.colorPicker.colorListHTML
                  , t = this.util.createElement("DIV");
                return t.className = "sun-editor-submenu layer_editor",
                t.style.display = "none",
                t.innerHTML = e,
                t
            },
            on: function() {
                const e = this.context.colorPicker
                  , t = this.context.hiliteColor;
                e._colorInput = t.colorInput,
                e._defaultColor = "#FFFFFF",
                e._styleProperty = "backgroundColor",
                e._colorList = t.colorList,
                this.plugins.colorPicker.init.call(this, this.getSelectionNode(), null)
            },
            onChangeInput: function(e) {
                this.plugins.colorPicker.setCurrentColor.call(this, "#" + e.target.value)
            },
            remove: function() {
                this.nodeChange(null, ["background-color"]),
                this.submenuOff(),
                this.focus()
            },
            submit: function() {
                this.plugins.hiliteColor.applyColor.call(this, this.context.colorPicker._currentColor)
            },
            pickup: function(e) {
                e.preventDefault(),
                e.stopPropagation(),
                this.plugins.hiliteColor.applyColor.call(this, e.target.getAttribute("data-value"))
            },
            applyColor: function(e) {
                if (!e)
                    return;
                const t = this.util.createElement("SPAN");
                t.style.backgroundColor = e,
                this.nodeChange(t, ["background-color"]),
                this.submenuOff(),
                this.focus()
            }
        }
    }
});

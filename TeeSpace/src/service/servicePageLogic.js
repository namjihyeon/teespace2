(function (exports) {

    const initializeSpaceAnimation = function () {
        window.requestAnimFrame = (function(){   return  window.requestAnimationFrame})();
        var canvas = document.querySelector("#space");
        var c = canvas.getContext("2d");

        var numStars = 1900;
        var radius = '0.'+Math.floor(Math.random() * 9) + 1  ;
        var focalLength = canvas.width *2;
        var warp = 0;
        var centerX, centerY;

        var stars = [], star;
        var i;

        var animate = true;

        initializeStars();

        function executeFrame(){
        
        if(animate)
            requestAnimFrame(executeFrame);
            moveStars();
            drawStars();
        }

        function initializeStars(){
            centerX = canvas.width / 2;
            centerY = canvas.height / 2;
            
            stars = [];
            for(i = 0; i < numStars; i++){
                star = {
                    x: Math.random() * canvas.width,
                    y: Math.random() * canvas.height,
                    z: Math.random() * canvas.width,
                    o: '0.'+Math.floor(Math.random() * 99) + 1
                };
                stars.push(star);
            }
        }

        function moveStars(){
            for(i = 0; i < numStars; i++){
                star = stars[i];
                star.z--;
                
                if(star.z <= 0){
                star.z = canvas.width;
                }
            }
        }

        function drawStars(){
            var pixelX, pixelY, pixelRadius;
            
            // Resize to the screen
            if(canvas.width != window.innerWidth || canvas.width != window.innerWidth){
                canvas.width = window.innerWidth;
                canvas.height = window.innerHeight;
                initializeStars();
            }
            if(warp==0) {
                c.fillStyle = "#0B1D41";
                c.fillRect(0,0, canvas.width, canvas.height);
            }
            c.fillStyle = "rgba(209, 255, 255, "+radius+")";
            for(i = 0; i < numStars; i++){
                star = stars[i];
                
                pixelX = (star.x - centerX) * (focalLength / star.z);
                pixelX += centerX;
                pixelY = (star.y - centerY) * (focalLength / star.z);
                pixelY += centerY;
                pixelRadius = 1 * (focalLength / star.z);
                
                c.fillRect(pixelX, pixelY, pixelRadius, pixelRadius);
                c.fillStyle = "rgba(209, 255, 255, "+star.o+")";
                // c.fill();
            }
        }

        executeFrame();
    };

    const initializeSwipe = function () {
        const serviceSwiper = new Swiper ('.swiper-container', {
            slidesPerView: 'auto',
            centeredSlides: true,
            loop: true, 
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },
            fadeEffect: {
                crossFade: true
              },
        });
    };

    const initializeButtons = function () {
        document.querySelector('.go-to-top').addEventListener('click', function (e) {
            e.preventDefault();
            jQuery('html, body').animate({
                scrollTop: 0
            }, 200);
        });

        document.querySelector('.btn-logo').addEventListener('click', function (e) {
            e.preventDefault();
            jQuery('html, body').animate({
                scrollTop: $('.section-1').offset().top - 100
            }, 200);
        });

        document.querySelector('.btn-about').addEventListener('click', function (e) {
            e.preventDefault();
            jQuery('html, body').animate({
                scrollTop: $('.section-2').offset().top - 100
            }, 200);
        });

        document.querySelector('.btn-contact').addEventListener('click', function (e) {
            e.preventDefault();
            jQuery('html, body').animate({
                scrollTop: $('.section-3').offset().top - 100
            }, 200);
        });

        document.querySelector('.btn-pricing').addEventListener('click', function (e) {
            e.preventDefault();
            jQuery('html, body').animate({
                scrollTop: $('.section-5').offset().top - 100
            }, 200);
        });
    };

    const initializeForm = function () {
        let USER_ETC_VALUE = {personal:"disagree", adokay:"disagree"};
        let model = {
            USER_NAME: '',
            USER_EMAIL: '', 
            USER_PHONE: '',
            AVAILABLE_TIME: '',
            CORP_NAME: '',
            CORP_RANK: '',
            CORP_TYPE: '',
            CORP_NUMBER: '',
            CORP_COMMENT: '',
            USER_ETC: JSON.stringify(USER_ETC_VALUE),
        };

        const validation = function () {
            const emailRegex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            const keys = ['USER_NAME', 'USER_EMAIL', 'USER_PHONE', 'AVAILABLE_TIME'];

            let isValidation = true;
            keys.forEach(function (key) {
                const value = model[key];
                const selector = '#page-service input[data-key='+key+']';
                if (value.length === 0 || (key === 'USER_EMAIL' && !emailRegex.test(value))) {
                    document.querySelector(selector).classList.add('error');
                    isValidation = false;
                } else {
                    document.querySelector(selector).classList.remove('error');
                }
            });
            return isValidation;
        };

        const submittedSuccssful = function () {
            const formElem = document.querySelector('#page-service .contact-form');

            document.querySelector('#page-service .contact-card-body').removeChild(formElem);
            document.querySelector('#page-service .success-submitted').style.display = 'block';

            jQuery('html, body').animate({
                scrollTop: $('.section-4').offset().top - 380
            }, 200);
            jQuery('#page-service .section-4').animate({height: '195px'}, 200);
            jQuery('#page-service .contact-card-body').animate({height: '42px'}, 200);
        };

        document.querySelector('#page-service').addEventListener('input', function (e) {
            const input = e.target.closest('input');

            if (input) {
                const key = e.target.getAttribute('data-key');
                const value = e.target.value;
                
                model[key] = value;
            }
        });

        document.querySelector('#page-service').addEventListener('change', function (e) {
            const input = e.target.closest('input[type=checkbox]');
            if (input) {
                const id = input.getAttribute('id');
                const checked = input.checked;

                if (id === 'agreement-A') {
                    USER_ETC_VALUE.personal = checked === true ? 'agree' : 'disagree';
                } else if (id === 'agreement-B') {
                    USER_ETC_VALUE.adokay = checked === true ? 'agree' : 'disagree';
                } else {
                    input.checked = true;
                }
            }
        });

        document.querySelector('#page-service .btn-send-contact').addEventListener('click', function (e) {
            e.preventDefault();
            if (validation()) {
                model.USER_ETC = JSON.stringify(USER_ETC_VALUE);
                axios.post(_workspace.url + 'Users/CorpUser?action=Create', {
                    dto: model
                })
                .then(function (res) {
                })
                .finally(function () {
                    submittedSuccssful();
                });
            } else {
                jQuery('html, body').animate({
                    scrollTop: $('.section-4').offset().top - 100
                }, 200);
            }
        });
    };

    const onPageLoaded = function () {
        document.body.classList.add('service-page');
        
        initializeSwipe();
        initializeButtons();
        initializeSpaceAnimation();
        initializeForm();
    };

    exports.onServicePageLoaded = onPageLoaded;

})(this);
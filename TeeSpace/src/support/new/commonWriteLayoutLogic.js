Top.Controller.create("commonWriteLayoutLogic", {
    buttonDisabling: false,
    data: {},
    admin_faq: function () {
        var that = this
        var d = that.data
        d._page = { productType: "FAQ", isAnnouncement: false }
        d._titlebar = {
            title: "자주하는 질문(FAQ)",
            desc: undefined,
            button: {
                visible: "none",
                text: undefined,
                onClick: undefined,
            },
        }
        d._tab = {
            list: [
                // 				{ value: 'all', subTab: undefined }, // 전체
                { value: "ss1", subTab: undefined }, // 회원
                { value: "ss2", subTab: "_subTab1" }, // 앱
                { value: "ss3", subTab: undefined }, // 요금
                { value: "ss4", subTab: undefined }, // 장애
                { value: "ss5", subTab: undefined }, // 기타
            ],
        }
        d["_subTab1"] = {
            list: [
                //				{ value: 'all' }, // 전체
                { value: "ss10" }, // TeeTalk
                { value: "ss11" }, // TeeNote
                { value: "ss12" }, // TeeDrive
                { value: "ss14" }, // TeeSchedule
                { value: "ss13" }, // TeeOffice
                { value: "ss17" }, // // TeeMail
            ],
        }
        d._announcement = { visible: "none" }
        d._secret = { visible: "none" }
        d._attach = { visible: "visible" }
    },
    admin_oneonone: function () {
        var that = this
        var d = that.data
        d._page = { productType: "ONEONONE", isAnnouncement: false }
        d._titlebar = {
            title: "1:1 문의",
            desc: "TeeSpace에 관련된 문의사항을 자유롭게 남겨주세요.",
            button: {
                visible: "none",
                text: undefined,
                onClick: undefined,
            },
        }
        d._tab = {
            list: [
                // 				{ value: 'all', subTab: undefined }, // 전체
                { value: "ss1", subTab: undefined }, // 회원
                { value: "ss2", subTab: "_subTab1" }, // 앱
                { value: "ss3", subTab: undefined }, // 요금
                { value: "ss4", subTab: undefined }, // 장애
                { value: "ss5", subTab: undefined }, // 기타
            ],
        }
        d["_subTab1"] = {
            list: [
                //				{ value: 'all' }, // 전체
                { value: "ss10" }, // TeeTalk
                { value: "ss11" }, // TeeNote
                { value: "ss12" }, // TeeDrive
                { value: "ss14" }, // TeeSchedule
                { value: "ss13" }, // TeeOffice
                { value: "ss17" }, // // TeeMail
            ],
        }
        d._announcement = { visible: "none" }
        d._secret = { visible: "none" }
        d._attach = { visible: "visible" }
    },
    admin_qna: function () {
        var that = this
        var d = that.data
        d._page = { productType: "QNA", isAnnouncement: false }
        d._titlebar = {
            title: "Q&A",
            desc: undefined,
            button: {
                visible: "none",
                text: undefined,
                onClick: undefined,
            },
        }
        d._tab = {
            list: [
                // 				{ value: 'all', subTab: undefined }, // 전체
                { value: "ss1", subTab: undefined }, // 회원
                { value: "ss2", subTab: "_subTab1" }, // 앱
                { value: "ss3", subTab: undefined }, // 요금
                { value: "ss4", subTab: undefined }, // 장애
                { value: "ss5", subTab: undefined }, // 기타
            ],
        }
        d["_subTab1"] = {
            list: [
                //				{ value: 'all' }, // 전체
                { value: "ss10" }, // TeeTalk
                { value: "ss11" }, // TeeNote
                { value: "ss12" }, // TeeDrive
                { value: "ss14" }, // TeeSchedule
                { value: "ss13" }, // TeeOffice
                { value: "ss17" }, // TeeMail
            ],
        }
        d._announcement = { visible: "none" }
        d._secret = { visible: "none" }
        d._attach = { visible: "visible" }
    },
    admin_service_announcement: function () {
        var that = this
        var d = that.data
        d._page = { productType: "SERVICE_ANNOUNCEMENT", isAnnouncement: true }
        d._titlebar = {
            title: "서비스 공지",
            desc: undefined,
            button: {
                visible: "none",
                text: undefined,
                onClick: undefined,
            },
        }
        d._tab = {
            list: [
                { value: "ss19", subTab: undefined }, // 고객 지원
                { value: "ss20", subTab: undefined }, // 작업 현황
                { value: "ss21", subTab: undefined }, // TmaxOS
                { value: "ss22", subTab: undefined }, // ToOffice
                { value: "ss23", subTab: undefined }, // Tmax CloudSpace
            ],
        }
        d._announcement = { visible: "none" }
        d._secret = { visible: "none" }
        d._attach = { visible: "visible" }
    },
    admin_update_announcement: function () {
        var that = this
        var d = that.data
        d._page = { productType: "UPDATE_ANNOUNCEMENT", isAnnouncement: true }
        d._titlebar = {
            title: "업데이트 공지",
            desc: undefined,
            button: {
                visible: "none",
                text: undefined,
                onClick: undefined,
            },
        }
        d._tab = {
            list: [
                { value: "ss24", subTab: undefined }, // New
                { value: "ss25", subTab: undefined }, // 업데이트
            ],
        }
        d._announcement = { visible: "none" }
        d._secret = { visible: "none" }
        d._attach = { visible: "visible" }
    },
    spt_oneonone: function () {
        var that = this
        var d = that.data
        d._page = { productType: "ONEONONE", isAnnouncement: false }
        d._titlebar = {
            title: "1:1 문의",
            desc: "TeeSpace에 관련된 문의사항을 자유롭게 남겨주세요.",
            button: {
                visible: "none",
                text: undefined,
                onClick: undefined,
            },
        }
        d._tab = {
            list: [
                //				{ value: 'all', subTab: undefined }, // 전체
                { value: "ss1", subTab: undefined }, // 회원
                { value: "ss2", subTab: "_subTab1" }, // 앱
                { value: "ss3", subTab: undefined }, // 요금
                { value: "ss4", subTab: undefined }, // 장애
                { value: "ss5", subTab: undefined }, // 기타
            ],
        }
        d["_subTab1"] = {
            list: [
                //				{ value: 'all' }, // 전체
                { value: "ss10" }, // TeeTalk
                { value: "ss11" }, // TeeNote
                { value: "ss12" }, // TeeDrive
                { value: "ss14" }, // TeeCalendar
                { value: "ss13" }, // TeeOffice
                { value: "ss17" }, // TeeMail
            ],
        }
        d._announcement = { visible: "none" }
        d._secret = { visible: "none" }
        d._attach = { visible: "visible" }
    },
    spt_qna: function () {
        var that = this
        var d = that.data
        d._page = { productType: "QNA", isAnnouncement: false }
        d._titlebar = {
            title: "Q&A",
            desc: "TeeSpace에 관련된 문의사항을 자유롭게 남겨주세요.",
            button: {
                visible: "none",
                text: undefined,
                onClick: undefined,
            },
        }
        d._tab = {
            list: [
                //				{ value: 'all', subTab: undefined }, // 전체
                { value: "ss1", subTab: undefined }, // 회원
                { value: "ss2", subTab: "_subTab1" }, // 앱
                { value: "ss3", subTab: undefined }, // 요금
                { value: "ss4", subTab: undefined }, // 장애
                { value: "ss5", subTab: undefined }, // 기타
            ],
        }
        d["_subTab1"] = {
            list: [
                //				{ value: 'all' }, // 전체
                { value: "ss10" }, // TeeTalk
                { value: "ss11" }, // TeeNote
                { value: "ss12" }, // TeeDrive
                { value: "ss14" }, // TeeSchedule
                { value: "ss13" }, // TeeOffice
                { value: "ss17" }, // TeeMail
            ],
        }
        d._announcement = { visible: "none" }
        d._secret = { visible: "none" }
        d._attach = { visible: "visible" }
    },

    // ********************************************************************************
    // ********************************************************************************
    // ********************************************************************************
    // ********************************************************************************
    // ********************************************************************************
    // ********************************************************************************
    // ********************************************************************************
    // ********************************************************************************
    // ********************************************************************************
    init: function (widget) {
        let that = this
        let d = that.data
        let cmp = Top.Controller.get("components")
        const u = parseUrl()
        let module = u.module
        let type = u.type
        let mode = u.mode
        let postId = u.postId

        globald = d
        if (u.module === "spt") {
            spt.setLnbColor()
        } else if (u.module === "admin") {
            admin.setLnbColor()
        }
        spt.startLoader()
        if (postId) {
            that.getData(postId)
        }
        // tab
        d._tab.list = spt.fillTextCorrespondingToValue(d._tab.list)
        // subtab
        for (let i = 0; i < d._tab.list.length; i++) {
            let subTabId = d._tab.list[i].subTab
            if (subTabId) {
                d[subTabId].list = spt.fillTextCorrespondingToValue(
                    d[subTabId].list
                )
            }
        }
        // initialize data instance
        repospt.uploadedFileList = [] // 이미 업로드된 첨부파일들
        repospt.detachedFileList = [] // 제외시킬 이미 업로드된 첨부 파일들
        repospt.addedFileList = [] // 새로 추가될 첨부 파일들
        repospt.fileList = [] // 첨부파일 테이블에 바인딩된 데이터
        repospt.postList = [] // 현재 포스트 정보
        repospt.tmpSubCate = undefined // 글 수정시 마지막으로 선택한 subcategory
        spt.setBoardTitlebar(
            d._titlebar.title,
            d._titlebar.desc,
            d._titlebar.button
        )

        // editor
        window["sptEditor"] = undefined
        that.fitEditorSize("stretch")

        // alert
        $$$("alertText1").setText(Top.i18n.map.support.SELECT_CATEGORY)
        $$$("alertText2").setText(Top.i18n.map.support.INPUT_TITLE)
        $$$("alertText3").setText(Top.i18n.map.support.INPUT_PASSWORD)
        $$$("alertText4").setText(Top.i18n.map.support.INPUT_CONTENTS)

        // category
        $$$("cateText").setText(Top.i18n.map.support.CATEGORY)
        $$$("cateSelectBox").setProperties({
            nodes: d._tab.list,
            title: Top.i18n.map.support.SELECT,
            "on-change": that.onChangeCate,
        })

        // subcategory
        $$$("subcateText").setText(Top.i18n.map.support.APP)

        // post title
        $$$("postTitleText").setText(Top.i18n.map.support.TITLE)

        // 'Tab' 버튼으로 편집창 포커싱
        // document.querySelector('button#attachButton').setAttribute('tabindex', '-1');
        // document.querySelectorAll('.sun-editor li>button').forEach((elem) => elem.setAttribute('tabindex', '-1'));

        // spt.CONST.TITLE_MAX_LENGTH: 250
        // .se-wrapper-inner.se-wrapper-wysiwyg.sun-editor-editable.top-htmleditor-editor
        // .se-wrapper-inner.se-wrapper-code.top-htmleditor-textarea
        const { TITLE_MAX_LENGTH } = spt.CONST
        $$$("postTitleField").setProperties({
            hint: "",
            "on-keyup": (event, widget) => {
                let written = $$$("postTitleField").getText()
                if (written.length > TITLE_MAX_LENGTH) {
                    $$$("postTitleField").setText(
                        written.substr(0, TITLE_MAX_LENGTH)
                    )
                    // Top.Controller.get('commonWriteLayoutLogic').setAlert('title', false);
                }
                if (event.key === "Tab") {
                    // $('div.se-wrapper-inner.se-wrapper-wysiwyg').focus();
                    setTimeout(function () {
                        $("div.se-wrapper-inner.se-wrapper-wysiwyg").focus()
                    }, 0)
                }
                if (event.key === "Enter") {
                    $("div.se-wrapper-inner.se-wrapper-wysiwyg").focus()
                }
            },
            "on-focusout": (event, widget) => {
                Top.Controller.get("commonWriteLayoutLogic").setAlert(
                    "title",
                    $$$("postTitleField").getText() === ""
                )
            },
            "on-focus": (event, widget) => {
                Top.Controller.get("commonWriteLayoutLogic").setAlert(
                    "title",
                    false
                )
            },
        })
        document
            .querySelector("input#postTitleField")
            .setAttribute("maxlength", TITLE_MAX_LENGTH)

        // announcement
        $$$("annWrapper").setProperties({ visible: d._announcement.visible })
        $$$("annCheckBox").setProperties({
            text: Top.i18n.map.support.ANNOUNCEMENT,
        })

        // secret
        if (d._secret.visible === "visible") {
            $$$("secretLL").setProperties({ visible: d._secret.visible })
            document
                .querySelector("input#pwField")
                .setAttribute("type", "password")
            document
                .querySelector("input#pwField")
                .setAttribute("name", "password")
            $$$("secretText").setText(Top.i18n.map.support.PRIVATE_POST)
            $$$("publicRadio").setProperties({
                text: Top.i18n.map.support.PUBLIC_POST,
                checked: true,
            })
            $$$("privateRadio").setProperties({
                text: Top.i18n.map.support.PRIVATE_POST,
                checked: false,
                "on-change": function (event, widget) {
                    if (widget.getChecked() === false) {
                        $$$("pwLL").setProperties({ visible: "none" })
                        $$$("pwField").setText("")
                    } else {
                        $$$("pwLL").setProperties({ visible: "visible" })
                    }
                },
            })
            $$$("pwText").setProperties({ text: Top.i18n.map.support.PASSWORD })
            $$$("pwField").setProperties({
                hint: Top.i18n.map.support.INPUT_PASSWORD,
                // 			'on-keyup': ((event, widget) => {this.setAlert('title', (widget.getText() === ''))}),
                // 			'on-keydown': (()=>{}),
                "on-keypress": function (event, widget) {},
                "on-focusout": function (event, widget) {
                    Top.Controller.get("commonWriteLayoutLogic").setAlert(
                        "pw",
                        $$$("postTitleField").getText() === ""
                    )
                },
                "on-focus": function (event, widget) {
                    Top.Controller.get("commonWriteLayoutLogic").setAlert(
                        "pw",
                        false
                    )
                },
            })
        } else {
            $("top-linearlayout#secretLL").remove()
        }

        // attachment
        if (d._attach.visible === "visible") {
            $$$("attachmentTitleLL").setProperties({
                visible: d._attach.visible,
            })
            $$$("attachmentLL").setProperties({
                // visible: 'none',
                visible: "visible",
            })
            $$$("attachText").setText(Top.i18n.map.support.FILE_ATTACHMENT)
            $$$("attachExpandButton").setProperties({
                "on-click": that.onClickAttachExpandButton,
            })
            $$$("attachButton").setProperties({
                text: "내 PC",
                "on-click": that.onClickAttach,
            })
            that.setTotalSize()
            // $$$('attachmentTable').onRender(function () {
            // 	that.setTotalSize();
            // 	setTimeout(function () {
            // 		$('#attachmentTable th.head-cell.headIndex_0').text('파일명');
            // 		$('#attachmentTable th.head-cell.headIndex_1').text('크기');
            // 	}, 0);
            // 	$$$('attachmentTable').setProperties({ emptyMessage: Top.i18n.map.support.NO_ATTACHMENT });
            // });
        } else {
            $("top-linearlayout#attachmentTitleLL").remove()
            $("top-linearlayout#attachmentLL").remove()
        }

        // editor
        $$$("sptHtmlEditor").setProperties({
            "on-keyup": function (event, widget) {
                console.info(event.key)
                // new TextEncoder().encode($$$('sptHtmlEditor').getEditorHTML()).length;
            },
            "on-keypress": function (event, widget) {
                console.info("press")
            },
        })
        // <저장> 버튼
        $$$("saveButton").setProperties({
            text: Top.i18n.map.support.SAVE,
            "on-click": that.onClickSave,
            // disabled: true,
        })
        // <취소> 버튼
        $$$("cancelButton").setProperties({
            text: Top.i18n.map.support.CANCEL,
            "on-click": that.onClickCancel,
        })

        !postId && spt.stopLoader()
    },
    fitEditorSize: function (option) {
        console.info($("div#editorLL").css("height"))

        const attachmentTableHeight = 161
        if (option === "stretch") {
            newHeight =
                String(
                    $$$("editorLL").template.offsetHeight +
                        attachmentTableHeight
                ) + "px"
            $$$("editorLL").setProperties({ "layout-height": newHeight })
        } else if (option === "shrink") {
            // newHeight = String($$$('editorLL').template.offsetHeight - attachmentTableHeight) + 'px';
            newHeight = String($$$("editorLL").template.offsetHeight) + "px"
            $$$("editorLL").setProperties({ "layout-height": newHeight })
        }
    },
    onClickAttachExpandButton: function (event, widget) {
        let that = this
        if ($$$("attachmentLL").getProperties("visible") === "none") {
            $("button#attachExpandButton span").addClass("icon-arrow_filled_up")
            $("button#attachExpandButton span").removeClass(
                "icon-arrow_filled_down"
            )
            $$$("attachmentLL").setProperties({ visible: "visible" })
            that.fitEditorSize("shrink")
        } else {
            $("button#attachExpandButton span").removeClass(
                "icon-arrow_filled_up"
            )
            $("button#attachExpandButton span").addClass(
                "icon-arrow_filled_down"
            )
            $$$("attachmentLL").setProperties({ visible: "none" })
            that.fitEditorSize("stretch")
        }
    },
    getData: function (postId) {
        // 수정하기 모드일때 기존 포스트 내용 불러오기
        let that = this
        let d = that.data
        let inputData = {
            dto: {
                POST_LIST: [
                    { USER_ID: userManager.getLoginUserId(), POST_ID: postId },
                ],
            },
        }
        console.info("BoardPostGet", inputData.dto.POST_LIST[0])

        spt.ajax(
            "Support",
            "BoardPostGet",
            inputData,
            function successCallback(data, xhrStatus, xhr) {
                if (!data.dto.POST_LIST) {
                    return
                }
                let post = data.dto.POST_LIST[0]
                repospt.postList.push(post)

                // 첨부파일
                repospt.uploadedFileList = post.ATTACH_LIST.map((elem) => ({
                    ...elem,
                    fileId: elem.FILE_ID,
                    fileExtension: elem.FILE_EXTENSION,
                    fileNameWithExtension: elem.FILE_EXTENSION
                        ? `${elem.FILE_NAME}.${elem.FILE_EXTENSION}`
                        : elem.FILE_NAME,
                    fileSize: elem.FILE_SIZE,
                    readableFileSize: elem.FILE_SIZE.toReadableSize(),
                    fileIcon: spt.getProperIcon(elem.FILE_EXTENSION),
                    fileImage: "",
                }))
                repospt.setValue("fileList", repospt.uploadedFileList)
                that.renderAttachment()

                // CATEGORY에 해당하는 셀렉트박스 선택
                repospt.tmpSubCate = post.SUB_CATEGORY
                $$$("cateSelectBox").select(post.CATEGORY)
                // 공지글
                $$$("annCheckBox") &&
                    $$$("annCheckBox").setChecked(post.IS_ANN ? true : false)
                // 제목
                typeof post.SUBJECT === "string" &&
                    $$$("postTitleField").setText(post.SUBJECT)
                // 내용
                typeof post.CONTENTS === "string" &&
                    $$$("sptHtmlEditor").setEditorHTML(post.CONTENTS)
                spt.stopLoader()
            },
            function errorCallback(xhr) {
                let errorMsg = "게시글을 불러오는데 실패하였습니다."
                spt.stopLoader()
                TeeToast.open({ text: errorMsg, size: "normal" })
                return
            }
        )
    },
    setTotalSize: function () {
        // total size of files
        let that = this
        let totalSize = repospt.fileList.reduce(
            (acc, cur) => acc + cur.fileSize,
            0
        )
        let totalFileSizeLimit = spt.CONST.TOTAL_FILE_SIZE_LIMIT.toReadableSize(
            0
        )
        // let totalFileSizeLimit = '20MB';
        $$$("attachSizeText").setText(
            `${totalSize.toReadableSize()} / ${totalFileSizeLimit}`
        )
    },
    setAlert: function (className, bool) {
        // 	//    	let arg = Array.from(arguments);
        // 	//    	let showAll = arg.length>0? false:true;
        if (bool || typeof bool === "undefined") {
            $(".cmalert." + className).removeClass("disp-none")
        } else {
            $(".cmalert." + className).addClass("disp-none")
        }
    },
    checkRequired: function () {
        let that = this
        let flag = true
        let alertList = []

        // 카테고리 선택 안한 경우
        var box = $$$("cateSelectBox")
        if (box && !box.getSelectedText()) {
            // 		console.info('## Check category');
            flag = false
            that.setAlert("cate", true)
        }
        // 서브 카테고리 선택 안한 경우(수정필요)
        //		var subRadio = Top.Dom.select('radio')

        // 비밀번호 입력 안한 경우
        var radio = $$$("privateRadio")
        var pw = $$$("pwField")
        if (radio && radio.getChecked() && pw && !pw.getText()) {
            // box2.getText().includes('!@#$%^&')
            // 		console.info('## Check password');
            flag = false
            that.setAlert("pw", true)
        }
        // 제목 입력 안한 경우
        var title = $$$("postTitleField")
        if (title && !title.getText()) {
            // 		console.info('## Check title');
            flag = false
            that.setAlert("title", true)
        }
        // 내용 입력 안한 경우
        //		if (spt.extractTextFromHtml(supportSunEditor.getContents()) === '') {
        if (Top.Dom.selectById("sptHtmlEditor").getEditorText() === "") {
            //ts1-1
            // 		console.info('## Check contents');
            flag = false
            that.setAlert("contents", true)
        }
        // 공개/비공개 선택 안한 경우
        var rad1 = $$$("publicRadio")
        var rad2 = $$$("privateRadio")
        if (rad1 && rad2 && !(rad1.getChecked() ^ rad2.getChecked())) {
            // 		console.info('## Check radio buttons');
            flag = false
        }
        return flag
    },

    onChangeCate: function (event, widget) {
        var that = Top.Controller.get("commonWriteLayoutLogic")
        that.setAlert("cate", false) // turn off
        let currentTab = that.data._tab.list.filter(
            (elem) => elem.value === widget.getValue()
        )

        if (
            currentTab.length > 0 &&
            typeof currentTab[0].subTab !== "undefined"
        ) {
            var subTabId = currentTab[0].subTab
            let cmp = Top.Controller.get("components")
            var targetId = "subcateWrapper"
            $("div#" + targetId).empty() // reset
            //			console.info(repospt.tmpSubCate)
            cmp.radioButtonGroup({
                targetId: targetId,
                list: that.data[subTabId].list,
                initiallyClickedValue: repospt.tmpSubCate
                    ? repospt.tmpSubCate
                    : undefined,
                initiallyClickedIndex: repospt.tmpSubCate ? undefined : 0,
                css: { cursor: "pointer" },
                onClick: function (event, widget) {
                    //					console.info(widget.getValue());
                    repospt.tmpSubCate = widget.getValue()
                },
            })
            $$$("subcateLL").setProperties({ visible: "visible" })
        } else {
            $$$("subcateLL").setProperties({ visible: "none" })
        }
    },
    onClickSave: function (event, widget) {
        // if (widget.getDisabled()) return;
        var that = this
        let d = that.data

        let saveButton = widget
        //		that.buttonDisabling && saveButton.setDisabled(true);

        if (!that.checkRequired()) {
            // 필수 입력 사항 알림
            TeeToast.open({
                text: Top.i18n.map.support.INPUT_REQUIREMENTS,
                size: "normal",
            })
            // that.buttonDisabling && saveButton.setDisabled(false);
            return
        }

        spt.startLoader()
        var post = {}

        // SUB_CATEGORY
        let checked = Top.Dom.select(
            "#subcateLL top-radiobutton"
        ).filter((elem) => elem.getChecked())
        if (checked.length === 1 && checked[0].getText()) {
            // 체크된 버튼이 1개인지 확인 && // 체크된 버튼이 텍스트를 가지고 있는지 확인
            var subTabId = $$$("cateSelectBox").getSelected().subTab
            // 선택한 카테고리가 서브탭을 가지고 있는지 확인
            if (
                subTabId &&
                d[subTabId].list
                    .map((elem) => elem.value)
                    .includes(checked[0].getValue())
            ) {
                // 체크된 버튼이 서브탭 리스트에 들어있는지 확인
                post["SUB_CATEGORY"] = checked[0].getValue()
            }
        }

        // CONTENTS
        let contents = $$$("sptHtmlEditor").getEditorHTML()
        contents = [].filter
            .call(contents, (c) => c.charCodeAt() !== 8203)
            .join("")
        if (
            !contents.startsWith(
                '<div class="input_editor sun-editor-id-wysiwyg sun-editor-editable">'
            )
        ) {
            // to show table
            contents =
                '<div class="input_editor sun-editor-id-wysiwyg sun-editor-editable">' +
                contents +
                "</div>"
        }

        // USER_ID
        var userId = userManager.getLoginUserId()

        post["CONTENTS"] = contents
        post["PRODUCT_TYPE"] = d._page.productType
        post["SUBJECT"] = $$$("postTitleField").getText()
        post["PARENT_POST_ID"] = "root"
        post["USER_ID"] = userId
        post["CATEGORY"] = $$$("cateSelectBox").getValue()
        if ($$$("annWrapper") !== null && $$$("annCheckBox").getChecked()) {
            post["IS_ANN"] = 1
        }
        if ($$$("secretLL") !== null && $$$("privateRadio").getChecked()) {
            post["PW"] = $$$("pwField").getText()
        }

        // 새로 추가하는 첨부파일
        let attachList = []
        for (var i = 0; i < repospt.addedFileList.length; i++) {
            let fileMeta = repospt.addedFileList[i]
            attachList.push({
                FILE_NAME: fileMeta.FILE_NAME,
                FILE_EXTENSION: fileMeta.FILE_EXTENSION,
                FILE_SIZE: fileMeta.FILE_SIZE,
            })
        }
        post["ATTACH_LIST"] = attachList
        // fileChooser만 모아서 리스트로 만들기
        var addedFileChooserList = repospt.addedFileList.map(
            (elem) => elem.fileChooser
        )
        //  인코딩된 파일 데이터 리스트
        let fileDataList = spt.encodeFileChooserList(addedFileChooserList)

        // 파일 메타 데이터는 dto로 전달되고, 실제 파일 데이터는 string으로 인코딩되서 files로 전달
        // 추가하는 파일 정보 -> dto.POST_LIST[0].ATTACH_LIST
        // 삭제하는 파일 정보 -> dto.FILE_LIST
        // 변동없는 파일 정보 -> do nothing

        // 주소에 postId가 있으면 수정 모드, 없으면 새글 모드
        if (parseUrl().postId) {
            var serviceName = "BoardPostUpdate"
            post["POST_ID"] = parseUrl().postId
        } else {
            var serviceName = "BoardPostCreate"
            post["STATUS"] = "OPEN"
        }

        let inputData = {
            dto: {
                POST_LIST: [post],
                FILE_LIST: repospt.detachedFileList,
            },
            files: fileDataList,
        }

        console.info(serviceName, inputData.dto.POST_LIST[0])

        spt.ajax(
            "Support",
            serviceName,
            inputData,
            function successCallback(data, xhrStatus, xhr) {
                // that.buttonDisabling && widget.setDisabled(false);
                that.goToList(function () {
                    spt.stopLoader()
                    if (d._page.isAnnouncement) {
                        // TeeToast.open({
                        // 	text: `공지글이 정상적으로 등록되었습니다.`,
                        // 	size: 'normal',
                        // });
                    } else if (parseUrl().postId) {
                        // do nothing
                        // TeeToast.open({
                        // 	text: `게시글이 정상적으로 수정되었습니다.`,
                        // 	size: 'normal',
                        // });
                    } else if (
                        userManager.getLoginUserInfo().userGrade === null
                    ) {
                        TeeAlarm.open({
                            icon: "icon-prozone_bell",
                            title: "문의가 정상적으로 접수되었습니다.",
                            content: `${d._titlebar.title} 게시판 및 메일로 답변이 발송되오니 확인해주세요.`,
                            cancelButtonText: "확인",
                        })
                    } else {
                        // TeeToast.open({
                        // 	text: `게시글이 정상적으로 등록되었습니다.`,
                        // 	size: 'normal',
                        // });
                    }
                })
            },
            function errorCallback(xhr, xhrStatus, err) {
                // that.buttonDisabling && widget.setDisabled(false);
                spt.stopLoader()
                TeeToast.open({
                    text:
                        "게시글 등록에 실패하였습니다. 다시 시도해주시길 바랍니다.",
                    size: "normal",
                })
            },
            function completeCallback() {}
            // 'multipart/form-data'
        )
    },

    goToList: function (callback) {
        if (typeof callback === "function") {
            callback()
        }
        // history.back();

        const u = parseUrl()
        spt.routeState({
            module: u.module,
            type: u.type,
            mode: "list",
            cate: "all",
            subcate: "all",
            view: "ss30",
            page: "1",
        })
    },

    onClickCancel: function (event, widget) {
        let that = this
        if (
            !parseUrl().postId &&
            spt.extractTextFromHtml($$$("sptHtmlEditor").getEditorHTML()) === ""
        ) {
            // create mode: nothing has written
            that.goToList()
        } else if (
            parseUrl().postId &&
            $$$("sptHtmlEditor").getEditorHTML() ===
                repospt.postList[0].CONTENTS
        ) {
            // modify mode: nothing has changed
            that.goToList()
        } else {
            TeeAlarm.open({
                title: "이 페이지에서 나가시겠습니까?",
                content: "작성된 내용은 저장되지 않습니다.",
                buttons: [
                    {
                        text: "나가기",
                        onClicked: () => {
                            TeeAlarm.close()
                            that.goToList()
                        },
                    },
                ],
            })
            // that.goToList();
        }
    },
    uploadFromDrive: function () {
        OPEN_TDRIVE({
            buttonFn1: undefined,
            buttonFn2: undefined,
            channel_id: workspaceManager.getChannelList(
                workspaceManager.getWorkspaceId(getRoomIdByUrl()),
                TEESPACE_CHANNELS.TSCHEDULE.CHANNEL_TYPE
            ),
            preventMultiSelect: false,
            filteringFileExtensionList: [],
            selectedFile: {},
            successCallback: function (file) {
                // 여러 개 파일을 선택하면 successCallback이 파일 수만큼 온다
                // 파일 업로드를 위한 파일 메타 수정 후 talkData.fileChooserList에 넣어준다
                console.log(file)
                file = file.storageFileInfo
                if (
                    _this._fileValidityCheck({
                        name: `${file.file_name}.${file.file_extension}`,
                        size: file.file_size,
                    })
                ) {
                    _this._onTDriveFileSelected(file)
                }
            },
            cancelCallback: function () {
                // notiFeedback('파일 업로드에 실패하였습니다.');
            },
        })
    },
    uploadFromLocal: function () {
        var that = this

        var fileChooser = Top.Device.FileChooser.create({
            id: parseUrl().postId,
            // 		title: '_____',
            saveAs: "false",
            charset: "utf-8",
            // 		extFilter: '.pdf, .docx, .pptx, .xlsx,',
            // 		extFilter: ['pdf','docx','pptx','xlsx','jpg'].map(elem=>'.'+elem).toString(),
            multiple: true,
            onBeforeLoad: function (fileList) {},
            onFileChoose: function (fileChooserList) {
                // 			console.info(fileChooserList);
                // 첨부된 파일 용량 계산
                let totalSize = repospt.fileList.reduce(
                    (acc, cur) => acc + cur.fileSize,
                    0
                )
                // 지금 추가할 파일 리스트
                let newFileList = fileChooserList.map(
                    (elem, idx) => elem.file[idx]
                )
                for (let i = 0; i < newFileList.length; i++) {
                    if (newFileList[i].size === 0) {
                        TeeToast.open({
                            text: "용량이 0인 파일은 업로드할 수 없습니다.",
                            size: "normal",
                        })
                        return
                    }
                    if (
                        newFileList[i].size > spt.CONST.SINGLE_FILE_SIZE_LIMIT
                    ) {
                        // TeeToast.open({ text: '최대 허용 용량을 초과하였습니다.', size: 'normal' });
                        TeeAlarm.open({
                            title: `파일 첨부는 최대 ${spt.CONST.TOTAL_FILE_SIZE_LIMIT.toReadableSize(                                0                            ).toUpperCase()}까지 가능합니다.`,
                            content: "",
                            cancelButtonText: '확인',
                        })
                        return
                    }
                    totalSize += newFileList[i].size
                    if (totalSize > spt.CONST.TOTAL_FILE_SIZE_LIMIT) {
                        // TeeToast.open({ text: '최대 허용 용량을 초과하였습니다.', size: 'normal' });
                        TeeAlarm.open({
                            title: `파일 첨부는 최대 ${spt.CONST.TOTAL_FILE_SIZE_LIMIT.toReadableSize(0).toUpperCase()}까지 가능합니다.`,
                            content: "",
                        })
                        return
                    }
                }

                var that = Top.Controller.get("commonWriteLayoutLogic")
                let userId = userManager.getLoginUserId()

                newFileMetaList = newFileList.map((file, idx) => ({
                    POST_ID: file.id,
                    FILE_ID: null,
                    FILE_SIZE: file.size,
                    FILE_NAME: file.name.splitIntoNameAndExtension()[0],
                    FILE_EXTENSION: file.name.splitIntoNameAndExtension()[1],
                    USER_ID: userId,
                    fileId: undefined,
                    fileName: file.name.splitIntoNameAndExtension()[0],
                    fileExtension: file.name.splitIntoNameAndExtension()[1],
                    fileNameWithExtension: file.name,
                    fileSize: file.size,
                    readableFileSize: file.size.toReadableSize(),
                    fileIcon: spt.getProperIcon(
                        file.name.splitIntoNameAndExtension()[1]
                    ),
                    tmpFileId: String.prototype.genUUID(), // 업로드 전에 임의로 부여
                    fileChooser: fileChooserList[idx],
                }))

                repospt.addedFileList = repospt.addedFileList.concat(
                    newFileMetaList
                )

                // update total size
                repospt.setValue(
                    "fileList",
                    repospt.uploadedFileList.concat(repospt.addedFileList)
                )
                // that.setTotalSize();
                that.renderAttachment()
            },
        })

        fileChooser.show()
    },
    onClickAttach: function () {
        var that = this
        that.uploadFromLocal()
    },
    downloadAttachment: function (fileId) {
        if (
            repospt.uploadedFileList.map((elem) => elem.fileId).includes(fileId)
        ) {
            storageManager.DownloadFile(
                fileId,
                (chId = spt.CONST.CHANNEL_ID),
                (data, xhrStatus, xhr) => {},
                (xhr) => {
                    TeeToast.open({
                        text: Top.i18n.map.support.FAIL_TO_DOWNLOAD,
                        size: "normal",
                    })
                }
            )
        } else {
            // download browser blob
        }
    },
    detachAttachment: function (fileId) {
        // uploadedFileList: 업로드된 파일
        // addedFileList: 추가된 파일
        // detachedFileList: 제거된 파일
        if (
            repospt.uploadedFileList.map((elem) => elem.fileId).includes(fileId)
        ) {
            // 이미 업로드 된 파일을 제외할때
            let tmpAttachedFileList = []
            let tmpDetachedFileList = []

            for (var i = 0; i < repospt.uploadedFileList.length; i++) {
                if (fileId === repospt.uploadedFileList[i].fileId) {
                    // 제외할 파일은 tmpDetachedFileList 추가
                    tmpDetachedFileList.push(repospt.uploadedFileList[i])
                } else {
                    // 나머지 파일은 tmpAttachedFileList 추가
                    tmpAttachedFileList.push(repospt.uploadedFileList[i])
                }
            }

            repospt.uploadedFileList = tmpAttachedFileList // 축소된 리스트로 대체
            repospt.detachedFileList = repospt.detachedFileList.concat(
                tmpDetachedFileList
            ) // detach 리스트에 제외한 파일 추가
        } else {
            // 새로 추가된 파일을 제외할때
            repospt.addedFileList = repospt.addedFileList.filter(
                (elem) => elem.tmpFileId !== fileId
            )
        }
        repospt.fileList = repospt.uploadedFileList.concat(
            repospt.addedFileList
        )
        console.info(repospt.fileList)
        Top.Controller.get("commonWriteLayoutLogic").renderAttachment()
    },
    renderAttachment: function () {
        $cmp.attachmentChip({
            targetId: "attachmentLL",
            list: repospt.fileList,
            detachCallback: function (event) {
                let fileId = event.target.dataset.fileId
                Top.Controller.get("commonWriteLayoutLogic").detachAttachment(
                    fileId
                )
            },
            downloadCallback: function (event) {
                let fileId = event.target.dataset.fileId
                Top.Controller.get("commonWriteLayoutLogic").downloadAttachment(
                    fileId
                )
            },
            renderCallback: () => {
                Top.Controller.get("commonWriteLayoutLogic").setTotalSize()
            },
        })
    },
    // onClickDetach: function (event, widget) {
    // 	// 업로드된 파일 uploadedFileList / 추가된 파일 addedFileList / 제거된 파일 detachedFileList
    // 	let that = this;
    // 	let idx = widget.getParent().getSelectedIndex();
    // 	let selectedFile = repospt.fileList[idx];
    // 	if (!selectedFile) return;
    // 	if (selectedFile.FILE_ID) {
    // 		// 이미 업로드 된 파일을 제외할때
    // 		let tmpAttach = [];
    // 		let tmpDetach = [];

    // 		for (var i = 0; i < repospt.uploadedFileList.length; i++) {
    // 			if (selectedFile.FILE_ID === repospt.uploadedFileList[i].FILE_ID) {
    // 				tmpDetach.push(repospt.uploadedFileList[i]); // 제외할 파일은 tmpDetach에 추가
    // 			} else {
    // 				tmpAttach.push(repospt.uploadedFileList[i]); // 나머지 파일은 tmpAttach에 추가
    // 			}
    // 		}

    // 		repospt.uploadedFileList = tmpAttach; // 축소된 리스트로 대체
    // 		repospt.detachedFileList = repospt.detachedFileList.concat(tmpDetach); // detach 리스트에 제외한 파일 추가
    // 	} else {
    // 		// 새로 추가된 파일을 제외할때(임시 fileId 필요)
    // 		repospt.addedFileList = repospt.addedFileList.filter((elem) => elem.tmpFileId !== selectedFile.tmpFileId);
    // 	}

    // 	// update table
    // 	// uploadedFileList는 변동없음
    // 	repospt.setValue('fileList', repospt.uploadedFileList.concat(repospt.addedFileList));
    // 	$$$('attachmentTable').render();
    // 	that.setTotalSize();
    // 	console.info('already uploaded: ', repospt.uploadedFileList);
    // 	console.info('newly added: ', repospt.addedFileList);
    // 	console.info('now detached: ', repospt.detachedFileList);
    // },

    renderPostWriter: function () {
        let target = document.querySelector("div#contentHead3_LL")
        let userName
        let divStyle1 = new StyleObject({
            "flex-direction": "row",
            "flex-wrap": "nowrap",
        }).toString()
        let date = "2222/22/22"
        let html = `
				<div id="" class="" style="${divStyle1}">
					<div> <img src="" alt="" title="" /></div>
					<div>
						<div>${userName}</div>
						<div style="flex:">
							<div>${date}</div>
							<div class="horizontal"></div>
							<div>${hits}</div>
						</div>
					</div>
				</div>
			`
        target.appendChild(htmlToElement(html))
    },
    renderAttachmentContextMenu: function () {
        //  iconPosition = 'right';
        // 	classIcon = 'icon-arrow_down';

        // let target = document.querySelector('button#attachButton');
        let target = document.querySelector("div#attachmentTitleLL")
        let menuItem = [
            {
                text: "내 PC에서 첨부",
                onClick: () => {
                    OPEN_TDRIVE({
                        channel_id: talkData.talkId,
                        successCallback: function (...args) {
                            console.info(...args)
                            // 여러 개 파일을 선택하면 successCallback이 파일 수만큼 온다
                            // 파일 업로드를 위한 파일 메타 수정 후 talkData.fileChooserList에 넣어준다
                            // if (driveAttachArr.length < 31) {
                            // 	driveAttachArr.push(fileMeta);
                            // } else {
                            // }
                            // if (
                            // 	driveAttachArr.length == driveAttach.getSelectedFileList().length ||
                            // 	driveAttachArr.length == 30
                            // ) {
                            // 	isFromDriveDialog = true;
                            // 	if (!document.getElementById('top-dialog-root_talk__file-upload-dialog')) {
                            // 		talkOpenAPI.openTalkFileUpload(
                            // 			talkData.workspaceId,
                            // 			workspaceManager.getChannelList(talkData.workspaceId, 'CHN0006'),
                            // 			driveAttachArr
                            // 		);
                            // 	}
                            // 	if (driveAttachArr.length == 30) {
                            // 		TeeToast.open({ text: '파일 전송은 한번에 30개까지 가능합니다.' });
                            // 	}
                            // }
                        },
                        cancelCallback: function () {},
                    })
                },
            },
            {
                text: "TeeDrive에서 첨부",
                onClick: Top.Controller.get("commonWriteLayoutLogic")
                    .onClickAttach,
            },
        ]
        let style = new StyleObject({
            position: "relative",
            top: "1rem",
            left: 0,
            "flex-direction": "column",
            "flex-wrap": "nowrap",
        }).toString()
        let listStyle = new StyleObject({
            "list-style-type": "none",
        }).toString()
        let html = `
			<div class="attach-context-menu" style="${style}">
			<ul>
			${menuItem.reduce(
                (acc, cur) =>
                    acc +
                    `<li style="${listStyle}"><div>${cur.text}</div></li>`,
                ""
            )}
			</ul>
			</div>
		`
        target.appendChild(htmlToElement(html))
        document
            .querySelectorAll("div.attach-context-menu")
            .forEach((elem, idx) =>
                elem.addEventListener("click", menuItem[idx].onClick)
            )
        console.info(html)
    },
})
// Top.Controller.get('commonWriteLayoutLogic').renderAttachmentContextMenu();
// function testWrite(n = 1) {
// 	for (var i = 0; i < n; i++) {
// 		var inputData = {
// 			dto: {
// 				POST_LIST: [
// 					{
// 						PRODUCT_TYPE: 'QNA',
// 						CATEGORY: 'ss2',
// 						SUB_CATEGORY: 'ss13',
// 						/////////////////
// 						// PRODUCT_TYPE: 'FAQ',
// 						// CATEGORY: 'ss2',
// 						// SUB_CATEGORY: 'ss13',
// 						/////////////////
// 						CONTENTS:
// 							'<div class="input_editor sun-editor-id-wysiwyg sun-editor-editable"><p>' +
// 							''.rnd(Number.prototype.rnd(200, 1800)) +
// 							'</p></div>',
// 						SUBJECT: ''.rnd(Number.prototype.rnd(10, 200)),
// 						PARENT_POST_ID: 'root',
// 						// USER_ID: '5a06e80d-ad78-44a5-b825-59de4e2d09d0',
// 						USER_ID: userManager.getLoginUserId(),
// 						ATTACH_LIST: [],
// 						STATUS: 'OPEN',
// 					},
// 				],
// 			},
// 		};
// 		spt.ajax(
// 			'Support',
// 			'BoardPostCreate',
// 			inputData,
// 			() => {
// 				TeeToast.open({ text: '성공' });
// 			},
// 			() => {
// 				TeeToast.open({ text: '실패' });
// 			}
// 		);
// 	}
// }

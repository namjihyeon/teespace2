Top.Controller.create('sptManualLayoutLogic', {
	data: {
		_titlebar: {
			title: '사용자 매뉴얼',
			desc: 'TeeSpace 매뉴얼을 다운로드하세요.',
			button: {
				text: undefined,
				onClick: undefined,
			},
		},
	},
	init: function init(widget) {
		var that = this;
		var d = that.data; // intro
		$$$('titleText').getText() !== d._titlebar.title && $$$('titleText').setText(d._titlebar.title); // title
		$$$('descText').getText() !== d._titlebar.desc && $$$('descText').setText(d._titlebar.desc); // description

		if (d._titlebar.button.text) {
			$$$('titleButton').setProperties({
				text: d._titlebar.button.text,
				'on-click': d._titlebar.button.onClick,
			});
		} else {
			$$$('titleButton').setProperties({
				visible: 'none',
			});
		}

		$$$('downloadButton').setProperties({
			text: Top.i18n.map.support.DOWNLOAD,
			'on-click': that.downloadManual,
		});
	},
	downloadManual: function (event, widget) {
		try {
			spt.ajax(
				'Support',
				'BoardPostGet',
				(inputData = { dto: { POST_LIST: [{ POST_ID: 'MANUAL', USER_ID: userManager.getLoginUserId() }] } }),
				function successCallback(data) {
					var postList = data.dto.POST_LIST;
					if (postList && postList.length > 0) {
						let attachList = postList[0].ATTACH_LIST;
						if (attachList && attachList.length > 0) {
							attachList.forEach((elem, idx) => {
								setTimeout(function () {
									storageManager.DownloadFile(
										elem.FILE_ID,
										spt.CONST.CHANNEL_ID,
										() => {},
										() => {
											console.info(data.dto);
											throw '## storage api fail';
										}
									);
								}, idx);
							});
						} else {
							throw '## no attachment';
						}
					} else {
						console.info(data.dto);
						throw '## no post';
					}
				},
				function errorCallback(xhr) {
					console.info(xhr);
					throw '## ajax fail';
				}
			);
		} catch (e) {
			//			console.info(e);
			TeeToast.open({ text: Top.i18n.map.support.CANNOT_FIND_THE_FILE, size: 'normal' });
		}
	},
});

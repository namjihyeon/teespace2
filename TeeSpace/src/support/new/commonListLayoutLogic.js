// isFileTable: 파일 목록을 보여주는 테이블(매뉴얼, 요금제 등)인지 아닌지

// <글 목록 메타>
// admin/oneonone 체크박스/상태/분류/제목/문의자/등록일(조회수 빠짐)
// admin/qna 체크박스/상태/분류/제목/문의자/등록일/조회수
// support/oneonone 체크박스/상태/분류/제목/등록일(문의자, 조회수 빠짐)
// support/qna 체크박스/상태/분류/제목/문의자/등록일/조회수

Top.Controller.create('commonListLayoutLogic', {
	data: {},

	admin_manual: function () {
		var that = this;
		let d = that.data;
		const u = parseUrl();

		d._page = { productType: 'MANUAL' };
		d._titlebar = {
			title: '사용자 매뉴얼',
			desc: undefined,
			button: {
				visible: 'visible',
				text: '매뉴얼 추가',
				onClick: that.onClickUpload,
			},
		};
		d._tab = {
			visible: 'none',
			list: undefined,
			onClick: function (event, widget) {},
		};
		d._control = {
			checkAll: { visible: 'visible' },
			viewButton: { visible: 'none' },
			deleteButton: { visible: 'visible' },
		};
		d._period = {
			box: { visible: 'none' },
			button: { visible: 'none' },
		};
		d._search = { visible: 'none' };
		d._table = {
			hasStatusFilter: false,
			isFileTable: true,
			emptyMessage: '등록된 매뉴얼이 없습니다.',
			headerOption: {
				0: {
					0: { visible: true, text: '' },
					1: { visible: false, text: '상태' },
					2: { visible: false, text: '분류' },
					3: { visible: true, text: '파일명' },
					4: { visible: false, text: '문의자' },
					5: { visible: true, text: '크기' },
					6: { visible: true, text: '등록일' },
					7: { visible: false, text: '조회수' },
				},
			},
			columnOption: { sortable: true },
		};
	},
	admin_faq: function () {
		var that = this;
		let d = that.data;
		const u = parseUrl();

		d._page = { productType: 'FAQ' };
		d._titlebar = {
			title: '자주하는 질문(FAQ)',
			desc: undefined,
			button: {
				visible: 'visible',
				text: '글쓰기',
				onClick: that.onClickWriteNewPost,
			},
		};
		d._tab = {
			visible: 'visible',
			list: [
				{ value: 'all', subTab: undefined }, // 전체
				{ value: 'ss1', subTab: undefined }, // 회원
				{ value: 'ss2', subTab: '_subTab1' }, // 앱
				{ value: 'ss3', subTab: undefined }, // 요금
				{ value: 'ss4', subTab: undefined }, // 장애
				{ value: 'ss5', subTab: undefined }, // 기타
			],
			onClick: function (event, widget) {
				const u = parseUrl();
				spt.routeState({
					module: u.module,
					type: u.type,
					mode: u.mode,
					cate: widget.getValue(),
					subcate: spt.CONST.SUBCATE_ALL,
					page: u.page,
					view: u.view,
				});
			},
		};
		d._subTab1 = {
			visible: 'visible',
			list: [
				{ value: spt.CONST.SUBCATE_ALL, text: '앱' },
				{ value: 'ss10', text: 'TeeTalk' },
				{ value: 'ss17', text: 'TeeMail' },
				{ value: 'ss11', text: 'TeeNote' },
				{ value: 'ss12', text: 'TeeDrive' },
				{ value: 'ss13', text: 'TeeOffice' },
				{ value: 'ss14', text: 'TeeCalendar' },
			],
			onClick: function (event, widget) {
				const u = parseUrl();
				spt.routeState({
					module: u.module,
					type: u.type,
					mode: u.mode,
					cate: u.cate,
					subcate: widget.getValue(),
					page: u.page,
					view: u.view,
				});
			},
		};
		d._control = {
			checkAll: { visible: 'visible' },
			viewButton: { visible: 'none' },
			deleteButton: { visible: 'visible' },
		};
		d._period = {
			box: { visible: 'none' },
			button: { visible: 'none' },
		};
		d._search = {
			visible: 'visible',
			hint: '자주하는 질문(FAQ) 검색',
			defaultValueText: '제목',
			list: [
				//				{ text: '분류' },
				{ text: '제목' },
			],
		};
		d._table = {
			hasStatusFilter: false,
			isFileTable: false,
			emptyMessage: null,
			headerOption: {
				0: {
					0: { visible: true, text: '' },
					1: { visible: false, text: '상태' },
					2: { visible: true, text: '분류' },
					3: { visible: true, text: '제목' },
					4: { visible: false, text: '문의자' },
					5: { visible: false, text: '크기' },
					6: { visible: true, text: '등록일' },
					7: { visible: false, text: '조회수' },
				},
			},
			columnOption: { sortable: true },
		};
	},
	admin_qna: function () {
		var that = this;
		let d = that.data;
		const u = parseUrl();
		d._page = { productType: 'QNA' };
		d._titlebar = {
			title: 'Q&A',
			desc: undefined,
			button: {
				visible: 'none',
				text: '',
				onClick: function () {},
			},
		};
		d._tab = {
			visible: 'visible',
			list: [
				{ value: 'all', subTab: undefined }, // 전체
				{ value: 'ss1', subTab: undefined }, // 회원
				{ value: 'ss2', subTab: '_subTab1' }, // 앱
				{ value: 'ss3', subTab: undefined }, // 요금
				{ value: 'ss4', subTab: undefined }, // 장애
				{ value: 'ss5', subTab: undefined }, // 기타
			],
			onClick: function (event, widget) {
				const u = parseUrl();
				spt.routeState({
					module: u.module,
					type: u.type,
					mode: u.mode,
					cate: widget.getValue(),
					subcate: spt.CONST.SUBCATE_ALL,
					page: u.page,
					view: u.view,
				});
			},
		};
		d._subTab1 = {
			visible: 'visible',
			list: [
				{ value: spt.CONST.SUBCATE_ALL, text: '앱' },
				{ value: 'ss10', text: 'TeeTalk' },
				{ value: 'ss17', text: 'TeeMail' },
				{ value: 'ss11', text: 'TeeNote' },
				{ value: 'ss12', text: 'TeeDrive' },
				{ value: 'ss13', text: 'TeeOffice' },
				{ value: 'ss14', text: 'TeeCalendar' },
			],
			onClick: function (event, widget) {
				const u = parseUrl();
				spt.routeState({
					module: u.module,
					type: u.type,
					mode: u.mode,
					cate: u.cate,
					subcate: widget.getValue(),
					page: u.page,
					view: u.view,
				});
			},
		};
		d._control = {
			checkAll: { visible: 'visible' },
			viewButton: { visible: 'none' },
			deleteButton: { visible: 'visible' },
		};
		d._period = {
			box: { visible: 'visible' },
			button: { visible: 'visible' },
		};
		d._search = {
			visible: 'visible',
			hint: 'Q&A 검색',
			defaultValueText: '제목',
			list: [
				//				{ text: '분류' },
				{ text: '제목' },
				//				{ text: '등록일' },
				{ text: '작성자' },
			],
		};
		d._table = {
			hasStatusFilter: false,
			isFileTable: false,
			emptyMessage: null,
			headerOption: {
				0: {
					0: { visible: true, text: '' },
					1: { visible: true, text: '상태' },
					2: { visible: true, text: '분류' },
					3: { visible: true, text: '제목' },
					4: { visible: true, text: '문의자' },
					5: { visible: false, text: '크기' },
					6: { visible: true, text: '등록일' },
					7: { visible: true, text: '조회수' },
				},
			},
			columnOption: {
				sortable: true,
			},
		};
	},
	admin_oneonone: function () {
		var that = this;
		let d = that.data;
		const u = parseUrl();

		d._page = { productType: 'ONEONONE' };
		d._titlebar = {
			title: '1:1 문의',
			desc: undefined,
			button: {
				visible: 'none',
				text: '',
				onClick: function () {},
			},
		};
		d._tab = {
			visible: 'visible',
			list: [
				{ value: 'all', subTab: undefined }, // 전체
				{ value: 'ss1', subTab: undefined }, // 회원
				{ value: 'ss2', subTab: '_subTab1' }, // 앱
				{ value: 'ss3', subTab: undefined }, // 요금
				{ value: 'ss4', subTab: undefined }, // 장애
				{ value: 'ss5', subTab: undefined }, // 기타
			],
			onClick: function (event, widget) {
				const u = parseUrl();
				spt.routeState({
					module: u.module,
					type: u.type,
					mode: u.mode,
					cate: widget.getValue(),
					subcate: spt.CONST.SUBCATE_ALL,
					page: u.page,
					view: u.view,
				});
			},
		};
		d._subTab1 = {
			visible: 'visible',
			list: [
				{ value: spt.CONST.SUBCATE_ALL, text: '앱' },
				{ value: 'ss10', text: 'TeeTalk' },
				{ value: 'ss17', text: 'TeeMail' },
				{ value: 'ss11', text: 'TeeNote' },
				{ value: 'ss12', text: 'TeeDrive' },
				{ value: 'ss13', text: 'TeeOffice' },
				{ value: 'ss14', text: 'TeeCalendar' },
			],
			onClick: function (event, widget) {
				const u = parseUrl();
				spt.routeState({
					module: u.module,
					type: u.type,
					mode: u.mode,
					cate: u.cate,
					subcate: widget.getValue(),
					page: u.page,
					view: u.view,
				});
			},
		};
		d._control = {
			checkAll: { visible: 'visible' },
			viewButton: { visible: 'none' },
			deleteButton: { visible: 'visible' },
		};
		d._period = {
			box: { visible: 'visible' },
			button: { visible: 'visible' },
		};
		d._search = {
			visible: 'visible',
			hint: '1:1 문의 검색',
			defaultValueText: '제목',
			list: [
				//				{ text: '분류' },
				{ text: '제목' },
				//				{ text: '등록일' }
			],
		};
		d._table = {
			hasStatusFilter: false,
			isFileTable: false,
			emptyMessage: null,
			headerOption: {
				0: {
					0: { visible: true, text: '' },
					1: { visible: true, text: '상태' },
					2: { visible: true, text: '분류' },
					3: { visible: true, text: '제목' },
					4: { visible: false, text: '문의자' },
					5: { visible: false, text: '크기' },
					6: { visible: true, text: '등록일' },
					7: { visible: false, text: '조회수' },
				},
			},
			columnOption: {
				sortable: true,
			},
		};
	},
	admin_service_announcement: function () {
		var that = this;
		let d = that.data;
		const u = parseUrl();

		d._page = { productType: 'SERVICE_ANNOUNCEMENT' };
		d._titlebar = {
			title: '서비스 공지',
			desc: undefined,
			button: {
				visible: 'visible',
				text: '글쓰기',
				onClick: that.onClickWriteNewPost,
			},
		};
		d._tab = {
			visible: 'visible',
			list: [
				{ value: 'all', subTab: undefined }, // 전체
				{ value: 'ss19', subTab: undefined }, // 고객 지원
				{ value: 'ss20', subTab: undefined }, // 작업 현황
				{ value: 'ss21', subTab: undefined }, // TmaxOS
				{ value: 'ss22', subTab: undefined }, // ToOffice
				{ value: 'ss23', subTab: undefined }, // TeeSpace
			],
			onClick: function (event, widget) {
				const u = parseUrl();
				spt.routeState({
					module: u.module,
					type: u.type,
					mode: u.mode,
					cate: widget.getValue(),
					subcate: u.subcate,
					page: u.page,
					view: u.view,
				});
			},
		};
		d._control = {
			checkAll: { visible: 'visible' },
			viewButton: { visible: 'none' },
			deleteButton: { visible: 'visible' },
		};
		d._period = {
			box: { visible: 'none' },
			button: { visible: 'none' },
		};
		d._search = {
			visible: 'visible',
			hint: '서비스 공지 검색',
			defaultValueText: '제목',
			list: [
				//				{ text: '분류' },
				{ text: '제목' },
				//				{ text: '등록일' }
			],
		};
		d._table = {
			hasStatusFilter: false,
			isFileTable: false,
			emptyMessage: null,
			headerOption: {
				0: {
					0: { visible: true, text: '' },
					1: { visible: false, text: '상태' },
					2: { visible: true, text: '분류' },
					3: { visible: true, text: '제목' },
					4: { visible: false, text: '문의자' },
					5: { visible: false, text: '크기' },
					6: { visible: true, text: '등록일' },
					7: { visible: false, text: '조회수' },
				},
			},
			columnOption: {
				sortable: true,
			},
		};
	},
	admin_update_announcement: function () {
		var that = this;
		const u = parseUrl();
		var d = that.data;
		d._page = { productType: 'UPDATE_ANNOUNCEMENT' };
		d._titlebar = {
			title: '업데이트 공지',
			desc: undefined,
			button: {
				visible: 'visible',
				text: '글쓰기',
				onClick: that.onClickWriteNewPost,
			},
		};
		d._tab = {
			visible: 'visible',
			list: [
				{ value: 'all', subTab: undefined }, // 전체
				{ value: 'ss24', subTab: undefined }, // New
				{ value: 'ss25', subTab: undefined }, // 업데이트
			],
			onClick: function (event, widget) {
				const u = parseUrl();
				spt.routeState({
					module: u.module,
					type: u.type,
					mode: u.mode,
					cate: widget.getValue(),
					subcate: u.subcate,
					page: u.page,
					view: u.view,
				});
			},
		};
		d._control = {
			checkAll: { visible: 'visible' },
			viewButton: { visible: 'none' },
			deleteButton: { visible: 'visible' },
		};
		d._period = {
			box: { visible: 'none' },
			button: { visible: 'none' },
		};
		d._search = {
			visible: 'visible',
			hint: '업데이트 공지 검색',
			defaultValueText: '제목',
			list: [
				//				{ text: '분류' },
				{ text: '제목' },
				//				{ text: '등록일' }
			],
		};
		d._table = {
			hasStatusFilter: false,
			isFileTable: false,
			emptyMessage: null,
			headerOption: {
				0: {
					0: { visible: true, text: '' },
					1: { visible: false, text: '상태' },
					2: { visible: true, text: '분류' },
					3: { visible: true, text: '제목' },
					4: { visible: false, text: '문의자' },
					5: { visible: false, text: '크기' },
					6: { visible: true, text: '등록일' },
					7: { visible: false, text: '조회수' },
				},
			},
			columnOption: {
				sortable: true,
			},
		};
	},
	admin_plan: function () {
		var that = this;
		const u = parseUrl();
		var d = that.data;
		d._page = { productType: 'PLAN' };
		d._titlebar = {
			title: '요금제 관리',
			desc: undefined,
			button: {
				visible: 'visible',
				text: '요금제 추가',
				onClick: that.onClickUpload,
			},
		};
		d._tab = { visible: 'none' };
		d._control = {
			checkAll: { visible: 'visible' },
			viewButton: { visible: 'none' },
			deleteButton: { visible: 'visible' },
		};
		d._period = {
			box: { visible: 'none' },
			button: { visible: 'none' },
		};
		d._search = { visible: 'none' };
		d._table = {
			hasStatusFilter: false,
			isFileTable: true,
			emptyMessage: '등록된 요금제가 없습니다.',
			headerOption: {
				0: {
					0: { visible: true, text: '' },
					1: { visible: false, text: '상태' },
					2: { visible: false, text: '분류' },
					3: { visible: true, text: '파일명' },
					4: { visible: false, text: '문의자' },
					5: { visible: true, text: '크기' },
					6: { visible: true, text: '등록일' },
					7: { visible: false, text: '조회수' },
				},
			},
			columnOption: {
				sortable: true,
			},
		};
	},
	spt_faq: function () {
		var that = this;
		const u = parseUrl();
		var d = that.data;
		d._page = { productType: 'FAQ' };
		d._titlebar = {
			title: '자주하는 질문(FAQ)',
			desc: 'TeeSpace에 자주하는 질문을 확인하여 궁금증을 해결하세요.',
			button: {
				visible: 'none',
				text: '',
				onClick: function (event, widget) {},
			},
		};
		d._tab = {
			visible: 'visible',
			list: [
				{ value: 'all', subTab: undefined }, // 전체
				{ value: 'ss1', subTab: undefined }, // 회원
				{ value: 'ss2', subTab: '_subTab1' }, // 앱
				{ value: 'ss3', subTab: undefined }, // 요금
				{ value: 'ss4', subTab: undefined }, // 장애
				{ value: 'ss5', subTab: undefined }, // 기타
			],
			onClick: function (event, widget) {
				const u = parseUrl();
				spt.routeState({
					module: u.module,
					type: u.type,
					mode: u.mode,
					cate: widget.getValue(),
					subcate: spt.CONST.SUBCATE_ALL,
					page: u.page,
					view: u.view,
				});
			},
		};
		d._subTab1 = {
			visible: 'visible',
			list: [
				{ value: spt.CONST.SUBCATE_ALL, text: '앱' },
				{ value: 'ss10', text: 'TeeTalk' },
				{ value: 'ss17', text: 'TeeMail' },
				{ value: 'ss11', text: 'TeeNote' },
				{ value: 'ss12', text: 'TeeDrive' },
				{ value: 'ss13', text: 'TeeOffice' },
				{ value: 'ss14', text: 'TeeCalendar' },
			],
			onClick: function (event, widget) {
				const u = parseUrl();
				spt.routeState({
					module: u.module,
					type: u.type,
					mode: u.mode,
					cate: u.cate,
					subcate: widget.getValue(),
					page: u.page,
					view: u.view,
				});
			},
		};
		d._control = {
			checkAll: { visible: 'none' },
			viewButton: { visible: 'none' },
			deleteButton: { visible: 'none' },
		};
		d._period = {
			box: { visible: 'none' },
			button: { visible: 'none' },
		};
		d._search = {
			visible: 'visible',
			hint: '자주하는 질문(FAQ) 검색',
			defaultValueText: '제목',
			list: [
				//				{ text: '분류' },
				{ text: '제목' },
				//				{ text: '등록일' }
			],
		};
		d._table = {
			hasStatusFilter: false,
			isFileTable: false,
			emptyMessage: null,
			headerOption: {
				0: {
					0: { visible: false, text: '' },
					1: { visible: false, text: '상태' },
					2: { visible: true, text: '분류' },
					3: { visible: true, text: '제목' },
					4: { visible: false, text: '문의자' },
					5: { visible: false, text: '크기' },
					6: { visible: true, text: '등록일' },
					7: { visible: false, text: '조회수' },
				},
			},
			columnOption: {
				sortable: true,
			},
		};
	},
	spt_oneonone: function () {
		var that = this;
		const u = parseUrl();
		var d = that.data;
		d._page = { productType: 'ONEONONE' };
		d._titlebar = {
			title: '1:1 문의',
			desc: 'TeeSpace에 관련된 문의사항을 자유롭게 남겨주세요.',
			button: {
				visible: 'visible',
				text: '글쓰기',
				onClick: that.onClickWriteNewPost,
			},
		};
		d._tab = {
			visible: 'visible',
			list: [
				{ value: 'all', subTab: undefined }, // 전체
				{ value: 'ss1', subTab: undefined }, // 회원
				{ value: 'ss2', subTab: '_subTab1' }, // 앱
				{ value: 'ss3', subTab: undefined }, // 요금
				{ value: 'ss4', subTab: undefined }, // 장애
				{ value: 'ss5', subTab: undefined }, // 기타
			],
			onClick: function (event, widget) {
				const u = parseUrl();
				spt.routeState({
					module: u.module,
					type: u.type,
					mode: u.mode,
					cate: widget.getValue(),
					subcate: spt.CONST.SUBCATE_ALL,
					page: u.page,
					view: u.view,
				});
			},
		};
		d._subTab1 = {
			visible: 'visible',
			list: [
				{ value: spt.CONST.SUBCATE_ALL, text: '앱' },
				{ value: 'ss10', text: 'TeeTalk' },
				{ value: 'ss17', text: 'TeeMail' },
				{ value: 'ss11', text: 'TeeNote' },
				{ value: 'ss12', text: 'TeeDrive' },
				{ value: 'ss13', text: 'TeeOffice' },
				{ value: 'ss14', text: 'TeeCalendar' },
			],
			onClick: function (event, widget) {
				const u = parseUrl();
				spt.routeState({
					module: u.module,
					type: u.type,
					mode: u.mode,
					cate: u.cate,
					subcate: widget.getValue(),
					page: u.page,
					view: u.view,
				});
			},
		};
		d._control = {
			checkAll: { visible: 'visible' },
			viewButton: { visible: 'none' },
			deleteButton: { visible: 'visible' },
		};
		d._period = {
			box: { visible: 'none' },
			button: { visible: 'none' },
		};
		d._search = {
			visible: 'visible',
			hint: '1:1 문의 검색',
			defaultValueText: '제목',
			list: [
				//					{ text: '분류' },
				{ text: '제목' },
				//				{ text: '등록일' }
			],
		};
		d._table = {
			hasStatusFilter: false,
			isFileTable: false,
			emptyMessage: null,
			headerOption: {
				0: {
					0: { visible: true, text: '' },
					1: { visible: true, text: '상태' },
					2: { visible: true, text: '분류' },
					3: { visible: true, text: '제목' },
					4: { visible: false, text: '문의자' },
					5: { visible: false, text: '크기' },
					6: { visible: true, text: '등록일' },
					7: { visible: false, text: '조회수' },
				},
			},
			columnOption: {
				sortable: true,
			},
		};
	},
	spt_qna: function () {
		var that = this;
		const u = parseUrl();
		var d = that.data;
		d._page = { productType: 'QNA' };
		d._titlebar = {
			title: 'Q&A',
			desc: 'TeeSpace에 관련된 문의사항을 자유롭게 남겨주세요.',
			button: {
				visible: 'visible',
				text: '글쓰기',
				onClick: that.onClickWriteNewPost,
			},
		};
		d._tab = {
			visible: 'visible',
			list: [
				{ value: 'all', subTab: undefined }, // 전체
				{ value: 'ss1', subTab: undefined }, // 회원
				{ value: 'ss2', subTab: '_subTab1' }, // 앱
				{ value: 'ss3', subTab: undefined }, // 요금
				{ value: 'ss4', subTab: undefined }, // 장애
				{ value: 'ss5', subTab: undefined }, // 기타
			],
			onClick: function (event, widget) {
				const u = parseUrl();
				spt.routeState({
					module: u.module,
					type: u.type,
					mode: u.mode,
					cate: widget.getValue(),
					subcate: spt.CONST.SUBCATE_ALL,
					page: u.page,
					view: u.view,
				});
			},
		};
		d._subTab1 = {
			visible: 'visible',
			list: [
				{ value: spt.CONST.SUBCATE_ALL, text: '앱' },
				{ value: 'ss10', text: 'TeeTalk' },
				{ value: 'ss17', text: 'TeeMail' },
				{ value: 'ss11', text: 'TeeNote' },
				{ value: 'ss12', text: 'TeeDrive' },
				{ value: 'ss13', text: 'TeeOffice' },
				{ value: 'ss14', text: 'TeeCalendar' },
			],
			onClick: function (event, widget) {
				const u = parseUrl();
				spt.routeState({
					module: u.module,
					type: u.type,
					mode: u.mode,
					cate: u.cate,
					subcate: widget.getValue(),
					page: u.page,
					view: u.view,
				});
			},
		};
		d._control = {
			checkAll: { visible: 'none' },
			viewButton: { visible: 'visible' },
			deleteButton: { visible: 'none' },
		};
		d._period = {
			box: { visible: 'none' },
			button: { visible: 'none' },
		};
		d._search = {
			visible: 'visible',
			hint: 'Q&A 검색',
			defaultValueText: '제목',
			list: [
				//				{ text: '분류' },
				{ text: '제목' },
				//				{ text: '등록일' },
				{ text: '작성자' },
			],
		};
		d._table = {
			hasStatusFilter: false,
			isFileTable: false,
			emptyMessage: null,
			headerOption: {
				0: {
					0: { visible: false, text: '' },
					1: { visible: true, text: '상태' },
					2: { visible: true, text: '분류' },
					3: { visible: true, text: '제목' },
					4: { visible: true, text: '문의자' },
					5: { visible: false, text: '크기' },
					6: { visible: true, text: '등록일' },
					7: { visible: true, text: '조회수' },
				},
			},
			columnOption: {
				sortable: true,
			},
		};
	},
	spt_service_announcement: function () {
		var that = this;
		const u = parseUrl();
		var d = that.data;
		d._page = { productType: 'SERVICE_ANNOUNCEMENT' };
		d._titlebar = {
			title: '서비스 공지',
			desc: '고객 지원 안내 사항 및 서비스 장애 복구 작업 현황을 안내합니다.',
			button: {
				visible: 'none',
				text: '',
				onClick: function () {},
			},
		};
		d._tab = {
			visible: 'visible',
			list: [
				{ value: 'all', subTab: undefined }, // 전체
				{ value: 'ss19', subTab: undefined }, // 고객 지원
				{ value: 'ss20', subTab: undefined }, // 작업 현황
			],
			onClick: function (event, widget) {
				const u = parseUrl();
				spt.routeState({
					module: u.module,
					type: u.type,
					mode: u.mode,
					cate: widget.getValue(),
					subcate: u.subcate,
					page: u.page,
					view: u.view,
				});
			},
		};
		d._control = {
			checkAll: { visible: 'none' },
			viewButton: { visible: 'none' },
			deleteButton: { visible: 'none' },
		};
		d._period = {
			box: { visible: 'none' },
			button: { visible: 'none' },
		};
		d._search = {
			visible: 'visible',
			hint: '서비스 공지 검색',
			defaultValueText: '제목',
			list: [
				//				{ text: '분류' },
				{ text: '제목' },
				//				{ text: '등록일' }
			],
		};
		d._table = {
			hasStatusFilter: false,
			isFileTable: false,
			emptyMessage: null,
			headerOption: {
				0: {
					0: { visible: false, text: '' },
					1: { visible: false, text: '상태' },
					2: { visible: true, text: '분류' },
					3: { visible: true, text: '제목' },
					4: { visible: false, text: '문의자' },
					5: { visible: false, text: '크기' },
					6: { visible: true, text: '등록일' },
					7: { visible: false, text: '조회수' },
				},
			},
			columnOption: {
				sortable: true,
			},
		};
	},

	spt_update_announcement: function () {
		var that = this;
		const u = parseUrl();
		var d = that.data;
		d._page = { productType: 'UPDATE_ANNOUNCEMENT' };
		d._titlebar = {
			title: '업데이트 공지',
			desc: '신규 서비스 및 기능 업데이트 사항을 안내합니다.',
			button: {
				visible: 'none',
				text: '',
				onClick: function () {},
			},
		};
		d._tab = {
			visible: 'visible',
			list: [
				{ value: 'all', subTab: undefined }, // 전체
				{ value: 'ss24', subTab: undefined }, // New
				{ value: 'ss25', subTab: undefined }, // 업데이트
			],
			onClick: function (event, widget) {
				const u = parseUrl();
				spt.routeState({
					module: u.module,
					type: u.type,
					mode: u.mode,
					cate: widget.getValue(),
					subcate: u.subcate,
					page: u.page,
					view: u.view,
				});
			},
		};

		d._control = {
			checkAll: { visible: 'none' },
			viewButton: { visible: 'none' },
			deleteButton: { visible: 'none' },
		};
		d._period = {
			box: { visible: 'none' },
			button: { visible: 'none' },
		};
		d._search = {
			visible: 'visible',
			hint: '업데이트 공지 검색',
			defaultValueText: '제목',
			list: [
				//				{ text: '분류' },
				{ text: '제목' },
				//				{ text: '등록일' }
			],
		};
		d._table = {
			hasStatusFilter: false,
			isFileTable: false,
			emptyMessage: null,
			headerOption: {
				0: {
					0: { visible: false, text: '' },
					1: { visible: false, text: '상태' },
					2: { visible: true, text: '분류' },
					3: { visible: true, text: '제목' },
					4: { visible: false, text: '문의자' },
					5: { visible: false, text: '크기' },
					6: { visible: true, text: '등록일' },
					7: { visible: false, text: '조회수' },
				},
			},
			columnOption: {
				sortable: true,
			},
		};
	},

	// **************************************************
	// 게시판 종류에 무관, init 이하로는 코드에 데이터가 직접 들어가면 안됨
	// **************************************************

	init: function (widget) {
		var that = this;
		var d = that.data;
		const u = parseUrl();
		var cmp = Top.Controller.get('components'); // initialize data instance
		repospt.postList = []; // 파일 목록

		if (u.module === 'spt') {
			spt.setLnbColor();
		} else if (u.module === 'admin') {
			admin.setLnbColor();
		}
		window['sptEditor'] = undefined;
		spt.setBoardTitlebar(d._titlebar.title, d._titlebar.desc, d._titlebar.button);

		// tab
		if (d._tab.visible === 'visible') {
			$('div#tabLL').empty();
			$$$('tabLL').setProperties({ visible: 'visible' });

			// add 'text' property using 'value' property
			d._tab.list = spt.fillTextCorrespondingToValue(d._tab.list);

			// cate in url is valid
			if (d._tab.list.map((elem) => elem.value).includes(u.cate)) {
				cmp.buttonTab({
					list: d._tab.list,
					onClick: d._tab.onClick,
					targetId: 'tabLL',
					idPrefix: 'buttonTab',
					type: 1,
				});
				$('div#tabLL top-button button').removeClass('selected');
				$('div#tabLL top-button[value="' + u.cate + '"] button').addClass('selected');
			}
		} else {
			$$$('tabLL').setProperties({ visible: 'none' });
			$('div#tabLL').empty();
		}

		// period
		$$$('periodLL').setProperties({
			visible: d._period.box.visible === 'visible' || d._period.button.visible === 'visible' ? 'visible' : 'none',
		});
		if (d._period.box.visible === 'visible') {
			d._period.box.list = [
				{ text: '전체', value: -1 },
				{ text: '1주일', value: 7 },
				{ text: '1개월', value: 31 },
				{ text: '3개월', value: 93 },
				{ text: '사용자 정의', value: -2 },
			];
			$$$('periodBox').setProperties({
				nodes: d._period.box.list,
				//                value: d._period.box.list[0].value,
				'on-change': function (event, widget) {
					var that = this;
					if (widget.getValue() === -2) {
						// 초기화
						$$$('datePickFrom').setDate();
						$$$('datePickTo').setDate();
						$$$('datePickTo').setDisabled(true);

						// select "period between two days"
						$$$('datePickWrapper').setProperties({ visible: 'visible' });
						try {
							$$$('exportButton').setDisabled(false);
						} catch (e) {}
					} else {
						// select "days ago"
						$$$('datePickWrapper').setProperties({ visible: 'none' });
						that.getData();
					}
				},
			});

			$$$('periodBox').select(d._period.box.list[0].value.toString());

			$$$('datePickWrapper').setProperties({ visible: 'none' });
			$$$('datePickFrom').setProperties({
				'on-select': function (event, widget) {
					console.info('date from', event, widget);
					$$$('datePickTo').setDisabled(false);
				},
				'to-datepicker': 'datePickTo',
			});
			$$$('datePickTo').setProperties({
				'on-select': function (event, widget) {
					console.info('date to', event, widget);
					this.getData();
				},
				'from-datepicker': 'datePickFrom',
			});
		} else {
			$('top-linearlayout#periodBoxWrapper').remove();
			$('top-linearlayout#datePickerWrapper').remove();
		}

		if (d._period.button.visible === 'visible') {
			$$$('exportButton').setProperties({
				visible: d._period.button.visible,
				disabled: d._period.button.visible !== 'visible' ? true : false,
				text: Top.i18n.map.support.EXPORT,
				'on-click': that.onClickExport,
			});
		} else {
			$('top-linearlayout#exportButtonWrapper').remove();
		}

		// Remove wrapper when tab, period both do not exist
		if (
			d._tab.visible !== 'visible' &&
			d._period.box.visible !== 'visible' &&
			d._period.button.visible !== 'visible'
		) {
			$('top-linearlayout#tabWrapper').remove();
		}

		// subtab
		if (d._tab.visible === 'visible') {
			var currentTab = d._tab.list.filter(function (elem) {
				return elem.value === u.cate;
			});
		}

		if (typeof currentTab !== 'undefined' && currentTab.length > 0 && typeof currentTab[0].subTab !== 'undefined') {
			var subTabId = currentTab[0].subTab;
			//			// add 'text' property using 'value' property
			//			d[subTabId].list = d[subTabId].list.map(function(elem) {
			//				elem.text = spt.cvt(null, elem.value);
			//				return elem;
			//			});
			$$$('subTabLL').setProperties({ visible: 'visible' });
			$('div#subTabLL').empty();
			cmp.buttonTab({
				list: d[subTabId].list,
				onClick: d[subTabId].onClick,
				targetId: 'subTabLL',
				idPrefix: subTabId,
				type: 2,
			});
			$('div#subTabLL top-button button').removeClass('selected');
			$('div#subTabLL top-button[value="' + u.subcate + '"] button').addClass('selected');
		} else {
			$$$('subTabLL').setProperties({ visible: 'none' });
		}

		// checkbox
		$$$('checkAll').setProperties({
			visible: d._control.checkAll.visible,
			'on-change': that.onChangeCheckAll,
			checked: false,
		});

		// view button(toggle)
		if (u.view === 'ss31') {
			[viewButtonText, viewButtonValue] = ['전체 글 보기', 'ss30'];
		} else if (u.view === 'ss30') {
			[viewButtonText, viewButtonValue] = ['내 글만 보기', 'ss31'];
		} else {
			return;
		}

		if (d._control.viewButton.visible === 'visible') {
			$$$('viewButton').setProperties({
				visible: 'visible',
				text: viewButtonText,
				value: viewButtonValue,
				'on-click': function (event, widget) {
					const u = parseUrl();
					spt.routeState({
						module: u.module,
						type: u.type,
						mode: u.mode,
						cate: u.cate,
						subcate: u.subcate,
						page: u.page,
						view: widget.getValue(),
					});
				},
			});
		} else {
			$('top-button#viewButton').remove();
		}

		// delete button
		if (d._control.deleteButton.visible === 'visible') {
			$$$('deleteButton').setProperties({
				visible: 'visible',
				text: Top.i18n.map.support.DELETE, // 삭제
				'on-click': d._table.isFileTable ? that.onClickDeleteFile : that.onClickDeletePost,
				disabled: true,
			});
		} else {
			$('top-button#deleteButton').remove();
		}

		// search module
		// url 에 sub cate 있으면 subcate으로 서치 박스 , 서치 필드맞추고, 없으면 cate로 맞추기

		if (d._search.visible === 'visible') {
			// 카테고리가 이미 선택된 경우 제목으로만 검색 가능
			// if (u.cate !== spt.CONST.CATE_ALL) {
			// 	d._search.list = d._search.list = [{ text: '제목' }];
			// }

			// 검색 텍스트에 해당하는 코드
			d._search.list = d._search.list.map((elem) => {
				elem.value = spt.cvt(elem.text);
				return elem;
			});

			$$$('searchLL').setProperties({ visible: 'visible' });

			var getSearchTypeAndText = function () {
				const u = parseUrl();
				if (u.cate === spt.CONST.CATE_ALL) {
					// <전체> 카테고리 보기
					return { type: '제목', text: '' };
				} else if (u.cate === 'ss2' && u.subcate === spt.CONST.SUBCATE_ALL) {
					// <앱> 카테고리 전체보기
					return { type: '분류', text: '' };
				} else if (u.cate === 'ss2') {
					// <앱> 카테고리의 특정 서브 카테고리 보기
					return { type: '분류', text: spt.cvt(null, u.subcate) };
				} else {
					return {
						type: '분류',
						text: spt.cvt(null, spt.cvt(null, u.cate)),
					};
				}
			};
			let { type: searchType, text: searchText } = getSearchTypeAndText();
			$$$('searchBox').setProperties({
				nodes: d._search.list,
				value: searchType,
			});
			// 셀렉트박스 선택된 항목의 색상변경
			$$$('searchBox').select(d._search.defaultValueText);
			$$$('searchBox').select(spt.cvt(d._search.defaultValueText));
			$$$('searchField').setProperties({
				// text: searchText,
				hint: d._search.hint,
				'on-click': function (event, widget) {
					//					console.info(event.target, event);
					if (event.target.classList.contains('top-textfield-icon')) {
						this.searchOnBrowser({
							searchTypeCode: $$$('searchBox').getSelected().value,
							searchWord: widget.getText(),
						});
					}
				},
				'on-keyup': function (event, widget) {
					//					console.info(event.keyCode, event);
					if (['Enter'].includes(event.key)) {
						this.searchOnBrowser({
							searchTypeCode: $$$('searchBox').getSelected().value,
							searchWord: widget.getText(),
						});
					}
				},
			});
		} else {
			$$$('searchLL').setProperties({ visible: 'none' });
			$('top-linearlayout#searchLL').remove();
		}

		// if 파일테이블 => 파일목록을 불러오기
		// if 포스트테이블 => 포스트목록 불러오기
		if (that.data._table.isFileTable) {
			that.getFileList();
		} else {
			that.getData();
		}

		// table
		// url보고 필터링 상태 맞추기
		//        tv.get
		//        tv.goToPage(p);
		//        getStartRowNum : 현재화면
		//        getStartRowNum	현재 화면에 보이는 첫번째 row number를 반환한다.		Number
		//        setScrollToIndexsetScrollToIndex	입력한 index를 가진 row의 위치로 스크롤 이동 설정.	Number - row index
		//        getPageInfo	현재 페이지 정보 오브젝트	-	Object
		//        filterColumns	해당 데이터가 존재하는 컬럼 filtering	object - columnObj	-

		window['ptv']   = $$$('ptv'); // global
		let tv = $$$('ptv');
		tv.setProperties({
			'empty-message': '',
			//						'header-option': d._table.headerOption,
			'column-option': d._table.columnOption,
			'on-rowcheck': that.onRowCheck,
			'on-rowclick': that.onRowClick,
			'on-rowdblclick': that.onRowDoubleClick,
			'on-pagechange': that.onPageChange,
			'on-lengthchange': that.onLengthChange,
		});

		u.page && tv.goToPage(Number(u.page)); // go to page

		var invisibleColumnList = Object.keys(d._table.headerOption[0]).filter(
			(elem) => !d._table.headerOption[0][elem].visible
		);
		tv.setColumnsVisible(invisibleColumnList, false);
	},
	// End of init *************************************************
	// End of init *************************************************

	onClickWriteNewPost: function (event, widget) {
		const u = parseUrl();
		spt.routeState({
			module: u.module,
			type: u.type,
			mode: 'write',
		});
	},

	onClickUpload: function (event, widget) {
		const u = parseUrl();
		var that = this;
		let { productType } = that.data._page;
		if (productType === 'MANUAL') {
			var enableMultipleUpload = false;
			var postId = 'MANUAL';
			var extFilter = '.pdf, .docx, .pptx, .xlsx';
			var successMsg = '매뉴얼이 등록되었습니다.';
			var errorMsg = '매뉴얼 등록에 실패하였습니다. 다시 시도해주시길 바랍니다.';
		} else if (productType === 'PLAN') {
			var enableMultipleUpload = false;
			var extFilter = '.png, .jpg, .gif';
			var postId = 'PLAN';
			var successMsg = '요금제가 등록되었습니다.';
			var errorMsg = '요금제 등록에 실패하였습니다. 다시 시도해주시길 바랍니다.';
		} else {
			return;
		}

		Top.Device.FileChooser.create({
			saveAs: 'false',
			charset: 'utf-8',
			extFilter: extFilter,
			multiple: enableMultipleUpload,
			defaultExt: '**',
			onBeforeLoad: function (fileChooser) {
				spt.startLoader();
				//            	allowableFileTypes
				// 용량, 확장자, 파일타입 체크
				//            	if (fileChooser.file[0].type.split('/')[0] == 'image') {
				//
				//            	}
			},
			onFileChoose: function (fileChooserList) {
				//				spt.startLoader();
				if (!fileChooserList.length) {
					fileChooserList = [fileChooserList];
				}
				var userId = userManager.getLoginUserId();
				let attList = [];
				for (var i = 0; i < fileChooserList.length; i++) {
					let file = fileChooserList[i].file[i];
					attList.push({
						FILE_NAME: file.name.splitIntoNameAndExtension()[0],
						FILE_EXTENSION: file.name.splitIntoNameAndExtension()[1],
						FILE_SIZE: file.size,
					});
				}
				let fileDataList = spt.encodeFileChooserList(fileChooserList);

				let inputData = {
					dto: {
						POST_LIST: [
							{
								USER_ID: userId,
								POST_ID: postId,
								PARENT_POST_ID: 'root',
								PRODUCT_TYPE: productType, // 필수!
								STATUS: 'OPEN',
								ATTACH_LIST: attList,
							},
						],
					},
					files: fileDataList,
				};

				console.info(inputData);

				spt.ajax(
					'Support',
					'BoardFileUpload',
					inputData,
					function successCallback(data, xhrStatus, xhr) {
						TeeToast.open({ text: successMsg, size: 'normal' });
						Top.Controller.get('commonListLayoutLogic').getFileList();
					},
					function errorCallback(xhr, xhrStatus, err) {
						TeeToast.open({ text: errorMsg, size: 'normal' });
					},
					function completeCallback() {
						spt.stopLoader();
					}
				);
			},
		}).show();
	},

	onRowClick: function (event, widget, sender) {
		// Row click event
		//		console.dir(event.detail.target);
		//		console.info(sender.target.className)
		if (sender.target.className.includes('check')) return;
		//		if (window.matchMedia('screen and (max-device-width: 1024px)').matches) {
		//			this.onRowDoubleClick(event, widget, sender);
		//			return;
		//		}
		this.onRowDoubleClick(event, widget, sender);
		return;
	},

	onRowDoubleClick: function (event, widget, sender) {
		// Row double click event
		if (sender.target.className.includes('check')) return;

		if (!widget.getClickedData().postId && widget.getClickedData().FILE_ID) {
			storageManager.DownloadFile(widget.getClickedData().FILE_ID, (chId = spt.CONST.CHANNEL_ID));
			return false;
		} else if (!widget.getClickedData().postId) {
			return false;
		}
		event.preventDefault();
		event.stopPropagation();
		const u = parseUrl();
		spt.routeState({
			module: u.module,
			type: u.type,
			mode: 'read',
			postId: widget.getClickedData().postId,
		});

		return false;
	},

	onClickExport: function (event, widget) {
		var that = this;
		var nDaysAgo = Number(
			//			that.data._period.box.list.filter(elem => elem.text === $$$('periodBox').getSelectedText())[0].value
			that.data._period.box.list.filter((elem) => elem.value === $$$('periodBox').getValue())[0].value
		);
		var tableData = repospt.postList;
		let dateFromString = ''; //yyyy.mm.dd
		let dateToString = '';
		if (nDaysAgo == -2) {
			// 사용자 정의
			var dateFrom = $$$('datePickFrom').getDate();
			var dateTo = $$$('datePickTo').getDate();
			if (!dateFrom || !dateTo) {
				TeeToast.open({ text: '날짜를 선택해주세요.', size: 'normal' });
				return;
			}
			tableData = tableData.filter(
				(elem) => dateFrom <= spt.dateConverter(elem.REGI_DATE) && spt.daysAgo(nDaysAgo) <= dateTo
			);
			dateFromString = dateFrom;
			dateToString = dateTo;
		} else if (nDaysAgo === -1) {
			// 전체
			dateFromString = '0000.00.00';
			// dateToString = spt.dateConverter(spt.daysAgo(0));
			dateToString = '9999.12.31';
		} else {
			// predefined period
			tableData = tableData.filter((elem) => spt.daysAgo(nDaysAgo) <= spt.dateConverter(elem.REGI_DATE));
			dateFromString = spt.dateConverter(spt.daysAgo(nDaysAgo));
			// dateToString = spt.dateConverter(spt.daysAgo(0));
			dateToString = spt.dateConverter(spt.daysAgo(0));
		}

		if (tableData.length > 0) {
			// debugger;
			// var tv = Top.Widget.create('top-tableview');
			// tv.setProperties({
			// 	id: 'asdf',
			// 	visible: 'none',
			// 	itemData: tableData,
			// 	pagination: false,
			// 	// 	pageLength="20"
			// 	// pagerLocation="bc"
			// 	// pagerMaxVisible="99"
			// 	// pagerType="basic"
			// });
			// $$$('ptvExport').setProperties({
			// 	itemData: tableData,
			// });
			var init = Top.Excel.init();
			// var excelData = Top.Excel.export('ptv', null, null, null, { dataExport: true });
			let excelData = [];
			// 1st elem => header
			// 2nd and so forth => body
			// (tobe) 	`여기서 excelData를 만들어줘야함

			debugger;
			const { module, type, cate } = parseUrl();
			let title = [
				['조회기간', `${dateFromString} ~ ${dateToString}`],
				['카테고리', `${spt.cvt(null, cate)}`],
			];
			Top.Excel.write(init, title, 'A1');
			Top.Excel.write(init, excelData, -1);

			let today = spt.daysAgo(0).split('-').join('');
			Top.Excel.fileExport(init, `${module}_${type}_${today}`);

			// var tableData = Top.Excel.export('ptv', 'xlsx', null, tableData);
		} else {
			TeeToast.open({ text: '조회하신 기간 중 문의 내역이 없습니다.', size: 'normal' });
		}
	},

	getFileList: function () {
		spt.startLoader();

		var that = this;
		var d = that.data;
		const u = parseUrl();
		let cate = u.cate;
		let subcate = u.subcate;
		let view = u.view;
		var post = {};
		if (d._page.productType === 'MANUAL') {
			var postId = 'MANUAL';
		} else if (d._page.productType === 'PLAN') {
			var postId = 'PLAN';
		}
		let inputData = { dto: { POST_LIST: [{ USER_ID: userManager.getLoginUserId(), POST_ID: postId }] } };
		console.info('BoardPostGet', inputData.dto.POST_LIST[0]);

		spt.ajax(
			'Support',
			'BoardPostGet',
			inputData,
			function successCallback(data, xhrStatus, xhr) {
				var fl = data.dto.POST_LIST[0].ATTACH_LIST; // fileList
				//				debugger;
				if (fl && fl.length > 0) {
					for (var i = 0; i < fl.length; i++) {
						var f = fl[i];
						f.postTitle = f.FILE_NAME + '.' + f.FILE_EXTENSION;
						f.visibleAnn = 'none';
						f.icon = '';
						f.visibleNew = spt.isNew(f.REGI_DATE);
						f.visibleReply = 'none';
						f.nReply = 0;
						f.date = f.REGI_DATE.toReadableDate();
						f.readableFileSize = f.FILE_SIZE.toReadableSize();
					}
				} else {
					fileList = [];
				}
				repospt.setValue('origList', fl);
				repospt.setValue('postList', fl);
			},
			function errorCallback(xhr, xhrStatus, err) {
				var fl = [];
				repospt.setValue('origList', fl);
				repospt.setValue('postList', fl);
			},
			function complete() {
				if (repospt.postList.length === 0) {
					$$$('ptv').setProperties({
						'empty-message': that.data._table.emptyMessage || Top.i18n.map.support.THERE_IS_NO_POST,
					});
					$$$('checkAll').setDisabled(true);
				} else {
					$$$('checkAll').setDisabled(false);
				}
				try {
					$$$('checkAll').setChecked(false);
				} catch (e) {}
				//				try {$$$('deleteButton').setDisabled(repospt.origList.length>0? false:true);}catch (e) {}
				try {
					$$$('exportButton').setDisabled(false);
				} catch (e) {}
				$$$('ptv').onRender(() => that.changeHeaderText());
				spt.stopLoader();
			}
		);
	},
	dataProcessing: function (list) {
		for (var i = 0; i < list.length; i++) {
			var elem = list[i];
			elem.cateText = spt.cvt(null, elem.SUB_CATEGORY ? elem.SUB_CATEGORY : elem.CATEGORY);
			elem.visibleAnn = elem.IS_ANN ? 'visible' : 'none';
			elem.postTitle = elem.SUBJECT;
			elem.icon = elem.HAS_ATTACH ? 'icon-work_img_file' : '';
			elem.visibleNew = spt.isNew(elem.REGI_DATE) ? 'visible' : 'none';
			elem.visibleReply = elem.COMMENT_COUNT > 0 ? 'visible' : 'none';
			elem.nReply = elem.COMMENT_COUNT > 99 ? '99+' : elem.COMMENT_COUNT.toString();
			// elem.userName = elem.USER_NAME;
			// 관리운영자는 USER_NAME 변경
			elem.userName = spt.isAdmin(elem.USER_ID) ? Top.i18n.map.support.SUPPORT_TEAM : elem.USER_NAME;
			elem.readableFileSize = elem.ATTACH_LIST
				? elem.ATTACH_LIST.reduce((acc, cur) => acc + cur.FILE_SIZE).toReadableSize()
				: null;
			// elem.date = elem.MODI_DATE ? elem.MODI_DATE.toReadableDate() : elem.REGI_DATE.toReadableDate();
			elem.date = elem.REGI_DATE.toReadableDate();
			elem.hits = elem.HITS.toString();
			elem.status = elem.STATUS;
			elem.statusText =
				elem.STATUS === 'CLOSED' ? Top.i18n.map.support.STATUS_CLOSED : Top.i18n.map.support.STATUS_OPEN; // 답변대기 답변완료
			elem.statusColor = elem.STATUS === 'CLOSED' ? '#0043A7' : '#000000';
			elem.postId = elem.POST_ID;
			elem.userId = elem.USER_ID;
		}
		return list;
	},
	getData: function (option, callback) {
		//		console.info('get post');
		// debugger;
		var that = this;
		var d = that.data;
		const u = parseUrl();
		let cate = u.cate;
		let subcate = u.subcate;
		let view = u.view;
		var post = {};

		// date
		if ($$$('periodBox') && $$$('datePickFrom') && $$$('datePickTo')) {
			var periodValue = Number($$$('periodBox').getValue());
			if (periodValue === -1) {
				// 전체
			} else if (periodValue === -2) {
				// 사용자 정의 Beginning and ending days of period are given
				post['DATE_FROM'] = $$$('datePickFrom').getDate();
				post['DATE_TO'] = $$$('datePickTo').getDate();
			} else {
				// Number of days ago is given
				post['DATE_FROM'] = spt.daysAgo($$$('periodBox').getValue());
				post['DATE_TO'] = spt.daysAgo(0);
			}
		}

		// status
		var statusFilter = '';
		if (['ONEONONE', 'QNA'].includes(d._page.productType) && !!statusFilter) {
			post['STATUS'] = statusFilter;
		}

		// 전체글보기(기본값  null) / 내글만보기 (일반유저 && ((QNA && 내글만보기) || (1:1 문의)))
		if (!spt.isAdmin()) {
			if ((d._page.productType === 'QNA' && view === 'ss31') || d._page.productType === 'ONEONONE') {
				post['USER_ID'] = userManager.getLoginUserId();
			}
		}
		post['PARENT_POST_ID'] = 'root';
		post['PRODUCT_TYPE'] = d._page.productType;
		cate !== spt.CONST.CATE_ALL && (post['CATEGORY'] = cate);
		subcate !== spt.CONST.SUBCATE_ALL && (post['SUB_CATEGORY'] = subcate);
		//		debugger;
		//        pinfo = ptv.getPageInfo();
		//        page 몇페이지
		//        length: 화면 최대
		//        start, end

		//		console.info($$$('ptv').getPageInfo());
		//		let pinfo = $$$('ptv').getPageInfo();
		//		post['ROW_FROM'] = (pinfo.page - 1) * pinfo.length + 1;
		//		post['ROW_TO'] = pinfo.page * pinfo.length;
		post['ROW_FROM'] = 0;
		post['ROW_TO'] = 0;

		let inputData = { dto: { POST_LIST: [post] } };
		console.info('BoardPostListGet', inputData.dto.POST_LIST[0]);

		spt.startLoader();
		spt.ajax(
			'Support',
			'BoardPostListGet',
			inputData,
			function successCallback(data, xhrStatus, xhr) {
				console.info(data, xhrStatus, xhr);

				let pl = that.dataProcessing(data.dto.POST_LIST);

				// 공지글에 tmaxos, tmaxcloudspace, tooffice 제외(기획변경가능)
				if (!spt.isAdmin(userManager.getLoginUserId())) {
					pl = pl.filter(
						(elem) =>
							!(
								elem.PRODUCT_TYPE.includes('ANNOUNCEMENT') &&
								['ss21', 'ss22', 'ss23'].indexOf(elem.CATEGORY) >= 0
							)
					);
				}

				repospt.setValue('origList', pl);
				repospt.setValue('postList', pl);
			},
			function errorCallback(xhr, xhrStatus, err) {
				var pl = [];
				repospt.setValue('origList', pl);
				repospt.setValue('postList', pl);
			},
			function complete() {
				if (repospt.postList.length === 0) {
					$$$('ptv').setProperties({
						'empty-message': that.data._table.emptyMessage || Top.i18n.map.support.THERE_IS_NO_POST,
					});
					$$$('checkAll').setDisabled(true);
				}
				$$$('checkAll') && $$$('checkAll').setChecked(false);
				$$$('exportButton') && $$$('exportButton').setDisabled(false);
				$$$('ptv').onRender(() => {
					that.changeHeaderText();
					spt.stopLoader();
				});
			}
		);
	},

	onClickDeleteFile: function (event, widget) {
		// 매뉴얼, 플랜 삭제
		var that = this;
		let list = $$$('ptv').getCheckedData();
		if (!(list.length > 0)) return; // 선택한 파일이 없을때
		TeeAlarm.open({
			title: `선택한 ${list.length}개의 파일을 삭제하시겠습니까?`,
			content: '삭제 후에는 복구할 수 없습니다.',
			button: [
				{
					text: '확인',
					onClicked: () => {
						let fileIdList = list.map((elem) => ({ FILE_ID: elem.FILE_ID }));
						let productType = that.data._page.productType;
						let postId;
						if (productType === 'MANUAL') postId = 'MANUAL';
						else if (productType === 'PLAN') postId = 'PLAN';
						else return;
						let userId = userManager.getLoginUserId();
						let inputData = {
							dto: {
								POST_LIST: [{ USER_ID: userId, POST_ID: postId }],
								FILE_LIST: fileIdList,
							},
						};
						console.info('BoardPostUpdate', inputData.dto.POST_LIST, inputData.dto.FILE_LIST);
						spt.ajax(
							'Support',
							'BoardPostUpdate',
							inputData,
							function successCallback(data, xhrStatus, xhr) {
								//						console.info(data, xhrStatus, xhr);
								that.getFileList();
								TeeToast.open({ text: '파일이 삭제되었습니다.', size: 'normal' });
							},
							function errorCallback(xhr, xhrStatus, err) {
								//						console.info(xhr, xhrStatus, err);
								TeeToast.open({
									text: '파일 삭제에 실패하였습니다. 다시 시도해주시길 랍니다.',
									size: 'normal',
								});
							}
						);
					},
				},
			],
		});
	},

	onClickDeletePost: function (event, widget) {
		//		onClickDeletePost: function() {
		//		var that = this;
		//		let d = that.data;
		//		let postIdList = $$$('ptv')
		//			.getCheckedData()
		//			.map(elem=>elem.postId);
		//		that.deletePost(postIdList);
		//	},

		var that = this;
		let list = $$$('ptv').getCheckedData();
		if (!(list.length > 0)) return;

		TeeAlarm.open({
			title:
				list.length === 1
					? '선택한 항목을 삭제하시겠습니까?'
					: `선택한 ${list.length}개 항목을 삭제하시겠습니까?`,
			content: '삭제 후에는 복구할 수 없습니다.',
			button: [
				{
					text: '삭제',
					onClicked: () => {
						let userId = userManager.getLoginUserId();
						let inputData = {
							dto: {
								POST_LIST: list.map((elem) => ({
									USER_ID: userId,
									POST_ID: elem.postId,
								})),
							},
						};
						// console.info('BoardPostDelete', inputData.dto.POST_LIST[0]);
						spt.ajax(
							'Support',
							'BoardPostDelete',
							inputData,
							function successCallback(data, xhrStatus, xhr) {
								console.info(data, xhrStatus, xhr);
								that.getData();
								TeeToast.open({ text: '게시글이 삭제되었습니다.', size: 'normal' });
								$$$('checkAll').setChecked(false);
								const onlyForThisPage = false;
								$$$('ptv').checkAll(false, onlyForThisPage);
							},
							function errorCallback(xhr, xhrStatus, err) {
								TeeToast.open({
									text: '게시글 삭제에 실패하였습니다. 다시 시도해주시길 바랍니다.',
									size: 'normal',
								});
							}
						);
					},
				},
			],
		});
	},

	//    createTable: function () {
	//        debugger;
	//        var that = Top.Controller.get('commonListLayoutLogic');
	//        var d = Top.Controller.get('commonListLayoutLogic').data;
	//        var tv = Top.Wiget.create('top-tableview');
	//        tv.setProperties({
	//            id: 'tv',
	//            columnOption: that.data._table.columnOption,
	//        })
	//        tv.addClass('cmtable');
	//
	//        tv.setProperties({
	//            'data-model': {
	//                'items': window[d._table.data.repo][d._table.data.inst],
	//            },
	//        });
	//
	//        tv.setProperties({
	//            'visible': 'none',
	//            'empty-message': Top.i18n.map.support.THERE_IS_NO_POST,
	//            'header-option': headop,
	//            'column-option': columnop,
	//            'column-resize': 'auto', // true, false, auto,
	//            'sortable': true,
	//            'paginate': true,
	//            'page-length': 20,
	//            'length-menu': [10, 25, 50, All],
	//            'length-change': true,
	//            'pager-type': 'big', // big, basic, input
	//            'pager-max-visible': 10,
	//            'pager-location': 'bc',
	//            'showresult': true,
	//            'order': [[1, 'asc'], [2, 'asc']], // 1열 기준 오름, 2열 기준 오름
	//
	//        })
	//
	//    },
	searchOnBrowser: function (info) {
		var that = this;
		// 카테고리검색은 한글로 부분일치하면 보여줘야하나? '앱'이라고 검색하면어떻게되나?
		// 현재 필터링 된 목록 안에서만 검색되나 아니면 db전체 검색하나?
		// 등록일은 어떻게 검색하나?
		spt.startLoader();
		let { searchTypeCode, searchWord } = info;

		// ss1001: 제목
		// ss1002: 분류
		// ss1003: 등록일
		// ss1004: 작성자
		//		console.info('\n\n\n');
		console.info(searchTypeCode, searchWord);
		// 대소 구분? 공백 무시?
		let filtered = [];
		let keyword = searchWord.toLowerCase().split(' ').join('');
		if (searchTypeCode === 'ss1001') {
			filtered = repospt.origList.filter((elem) => {
				if (elem['cate'].toLowerCase().split(' ').join('').includes(keyword)) {
					return true;
				} else if (elem['subcate'] && elem['subcate'].toLowerCase().split(' ').join('').includes(keyword)) {
					return true;
				}
				return false;
			});
		} else if (searchTypeCode === 'ss1002') {
			filtered = repospt.origList.filter((elem) =>
				elem['postTitle'].toLowerCase().split(' ').join('').includes(keyword)
			);
		} else if (searchTypeCode === 'ss1003') {
			//		filtered = repospt.origList.filter(elem => elem['readableRegiDate'].includes(searchWord));
		} else if (searchTypeCode === 'ss1004') {
			// 작성자
			filtered = repospt.origList.filter((elem) => elem['userName'].includes(searchWord));
		}

		if (filtered.length === 0) {
			//  && repospt.origList.length>0
			$$$('ptv').setProperties({ 'empty-message': Top.i18n.map.support.THERE_IS_NO_RESULT });
		}
		that.changeHeaderText();
		repospt.setValue('postList', filtered);
		spt.stopLoader();
	},
	changeHeaderText: function () {
		// change table header text to Korean 헤더 텍스트 한글로
		var that = this;
		setTimeout(function () {
			Object.values(that.data._table.headerOption[0]).forEach((elem, idx) => {
				if (elem.visible) {
					//					console.info('th#ptv_HeaderColumn' + idx.toString());
					$('th#ptv_HeaderColumn' + idx.toString()).html(elem.text);
					if (elem.text === '상태') {
						//ts1-1
						$('th#ptv_HeaderColumn' + idx.toString()).append(
							`<top-icon id="statusFilterIcon" class="icon-work_align_middle" on-click="openStatusFilterDialog" popover-id="sptOneononeStatusFilterPopover" popover-trigger="click"></top-icon>`
						);
					} else {
					}
				}
			});
		}, 1);
	},

	changeStatusFilter: function (event, widget) {
		//ts1-1
		// 답변대기/답변완료
		console.log(event.statusFilter);
		//		let currentStatusFilter = ['OPEN', 'CLOSED'];
		let currentStatusFilter = [];
		currentStatusFilter.push(event.statusFilter);
		//repo 자체는 변함없이 유지
		let repo_backup = JSON.parse(JSON.stringify(repospt.postList));

		let filtered = repospt.postList.filter((elem) => currentStatusFilter.includes(elem.STATUS));
		repospt.setValue('postList', filtered);
		repospt.postList = repo_backup;
	},

	onChangeCheckAll: function (event, widget) {
		$$$('ptv').checkAll(widget.getChecked());
		if ($$$('deleteButton') && $$$('ptv').getCheckedIndex()) {
			$$$('deleteButton').setDisabled($$$('ptv').getCheckedIndex().length === 0);
		}
	},
	onLengthChange: function () {
		console.info('length change');
		$$$('checkAll').setChecked(false); // 전체선택 해제
		$$$('ptv').checkAll(false, false); // 테이블 모든 페이지 체크 해제
	},
	onPageChange: function (event, widget) {
		console.info('page change');
		$$$('checkAll').setChecked(false); // 전체선택 해제
		$$$('ptv').checkAll(false, false); // 테이블 모든 페이지 체크 해제
	},
	onRowCheck: function (event, widget, sender) {
		var that = this;
		$$$('deleteButton').setDisabled($$$('ptv').getCheckedIndex().length === 0);
		var info = $$$('ptv').getPageInfo();
		try {
			if ($$$('ptv').getCheckedIndex().length === info.end - info.start + 1) {
				// 모든 테이블 체크했을때
				$$$('checkAll').setChecked(true);
			} else if ($$$('checkAll').getChecked()) {
				// 테이블 모두 체크된 상태에서 하나 체크 해제할때
				//				$$$('ptv').getCheckedIndex()
				$('#checkAll label').removeClass('checked');
			}
		} catch (e) {}
		event.stopPropagation();
		return false;
	},
});

Top.Controller.create('TeeSpaceLogic', {
	popoverStatusFilterLayout: function (event, widget) {
		var OPEN = '답변대기';
		var CLOSED = '답변완료';

		$('th#ptv_HeaderColumn1 top-icon#statusFilterIcon div.top-popover h3').remove();
		$('th#ptv_HeaderColumn1 top-icon#statusFilterIcon div.top-popover div.top-popover-arrow').remove();
		if (!$('div.statusFilterRadioWrapper').length) {
			$('th#ptv_HeaderColumn1 top-icon#statusFilterIcon div.top-popover').append(
				`<div class="statusFilterRadioWrapper" style="border:solid">` +
					`<div class="filterRadio">` +
					`<input type="radio" id="OPEN" name="statusFilterRadioBtn" value="OPEN">` +
					`<label for="OPEN">` +
					`<span>${OPEN}</span>` +
					`</label><br>` +
					`</div>` +
					`<div class="filterRadio">` +
					`<input type="radio" id="CLOSED" name="statusFilterRadioBtn" value="CLOSED">` +
					`<label for="CLOSED">` +
					`<span>${CLOSED}</span>` +
					`</label><br>` +
					`<div>` +
					`</div>`
			);
		}

		$('div.filterRadio input[type="radio"]').on('change', function (e) {
			if ($('div.filterRadio input[type="radio"]#OPEN').prop('checked')) {
				event.statusFilter = 'OPEN';
			} else {
				event.statusFilter = 'CLOSED';
			}

			Top.Controller.get('commonListLayoutLogic').changeStatusFilter(event, widget);
		});
	},
});

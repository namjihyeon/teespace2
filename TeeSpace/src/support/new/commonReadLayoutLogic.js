// <글 상세 메타>
// admin/oneonone 상태/분류/제목/문의자/등록일(조회수 빠짐)
// admin/qna 상태/분류/제목/문의자/등록일/조회수
// support/oneonone 분류/제목/등록일(문의자, 조회수 빠짐)
// support/qna 상태/분류/제목/문의자/등록일/조회수

Top.Controller.create('commonReadLayoutLogic', {
	data: {},
	admin_faq: function () {
		var that = this;
		const u = parseUrl();
		var d = that.data;
		d._page = { productType: 'FAQ', enableAnswer: false, enableReply: false };
		d._titlebar = { title: '자주하는 질문(FAQ)', desc: undefined, button: undefined };
		d._meta = {
			list: [
				{ text: '분류', value: 'cate' },
				{ text: '제목', value: 'postTitle' },
				{ text: '등록일', value: 'readableRegiDate' },
			],
		};
		//		d._reaction = {
		//			enableRepToPost: true, // 포스트에 답글을 달 수 있나
		//			enableRepToAns: true, // 답글에 댓글을 달 수 있나
		//			enableRepToRep: true, // 댓글에 답글을 달 수 있나
		//		};
	},
	admin_oneonone: function () {
		var that = this;
		const u = parseUrl();
		var d = that.data;
		d._page = { productType: 'ONEONONE', enableAnswer: true, enableReply: true };
		d._titlebar = { title: '1:1 문의', desc: undefined, button: undefined };
		d._meta = {
			list: [
				{ text: '분류', value: 'cate' },
				{ text: '제목', value: 'postTitle' },
				{ text: '등록일', value: 'readableRegiDate' },
				{ text: '조회수', value: 'hits' },
				{ text: '작성자', value: 'userName' },
				{ text: '상태', value: 'statusText' },
			],
		};
	},
	admin_qna: function () {
		var that = this;
		const u = parseUrl();
		var d = that.data;
		d._page = { productType: 'QNA', enableAnswer: true, enableReply: true };
		d._titlebar = { title: 'Q&A', desc: undefined, button: undefined };
		d._meta = {
			list: [
				{ text: '분류', value: 'cate' },
				{ text: '문의자', value: 'userName' },
				{ text: '제목', value: 'postTitle' },
				{ text: '등록일', value: 'readableRegiDate' },
				{ text: '상태', value: 'statusText' },
			],
		};
	},
	admin_service_announcement: function () {
		var that = this;
		const u = parseUrl();
		var d = that.data;
		d._page = { productType: 'SERVICE_ANNOUNCEMENT', enableAnswer: false, enableReply: false };
		d._titlebar = { title: '서비스 공지', desc: undefined, button: undefined };
		d._meta = {
			list: [
				{ text: '분류', value: 'cate' },
				{ text: '제목', value: 'postTitle' },
				{ text: '등록일', value: 'readableRegiDate' },
			],
		};
	},
	admin_update_announcement: function () {
		var that = this;
		const u = parseUrl();
		var d = that.data;
		d._page = { productType: 'UPDATE_ANNOUNCEMENT', enableAnswer: false, enableReply: false };
		d._titlebar = { title: '업데이트 공지', desc: undefined, button: undefined };
		d._meta = {
			list: [
				{ text: '분류', value: 'cate' },
				{ text: '제목', value: 'postTitle' },
				{ text: '등록일', value: 'readableRegiDate' },
			],
		};
	},
	spt_faq: function () {
		var that = this;
		const u = parseUrl();
		var d = that.data;
		d._page = { productType: 'FAQ', enableAnswer: false, enableReply: false };
		d._titlebar = {
			title: '자주하는 질문(FAQ)',
			desc: 'TeeSpace에 자주하는 질문을 확인하여 궁금증을 해결하세요.',
			button: undefined,
		};
		d._meta = {
			list: [
				{ text: '분류', value: 'cate' },
				{ text: '제목', value: 'postTitle' },
				{ text: '문의자', value: 'userName' },
				{ text: '등록일', value: 'readableRegiDate' },
			],
		};
	},
	spt_oneonone: function () {
		var that = this;
		const u = parseUrl();
		var d = that.data;
		d._page = { productType: 'ONEONONE', enableAnswer: true, enableReply: true };
		d._titlebar = {
			title: '1:1 문의',
			desc: 'TeeSpace에 관련된 문의사항을 자유롭게 남겨주세요.',
			button: undefined,
		};
		d._meta = {
			list: [
				{ text: '분류', value: 'cate' },
				{ text: '제목', value: 'postTitle' },
				{ text: '문의자', value: 'userName' },
				{ text: '조회수', value: 'hits' },
				{ text: '등록일', value: 'readableRegiDate' },
				{ text: '상태', value: 'statusText' },
			],
		};
	},
	spt_qna: function () {
		var that = this;
		const u = parseUrl();
		var d = that.data;
		d._page = { productType: 'QNA', enableAnswer: true, enableReply: true };
		d._titlebar = {
			title: 'Q&A',
			desc: 'TeeSpace에 관련된 문의사항을 자유롭게 남겨주세요.',
			button: undefined,
		};
		d._meta = {
			list: [
				{ text: '분류', value: 'cate' },
				{ text: '제목', value: 'postTitle' },
				{ text: '문의자', value: 'userName' },
				{ text: '조회수', value: 'hits' },
				{ text: '등록일', value: 'readableRegiDate' },
				{ text: '상태', value: 'statusText' },
			],
		};
	},
	spt_service_announcement: function () {
		var that = this;
		const u = parseUrl();
		var d = that.data;
		d._page = { productType: 'SERVICE_ANNOUNCEMENT', enableAnswer: false, enableReply: false };
		d._titlebar = {
			title: '서비스 공지',
			desc: '고객 지원 안내 사항 및 서비스 장애 복구 작업 현황을 안내합니다.',
			button: undefined,
		};
		d._meta = {
			list: [
				{ text: '분류', value: 'cate' },
				{ text: '제목', value: 'postTitle' },
				{ text: '문의자', value: 'userName' },
				{ text: '등록일', value: 'readableRegiDate' },
			],
		};
	},
	spt_update_announcement: function () {
		var that = this;
		const u = parseUrl();
		var d = that.data;
		d._page = { productType: 'UPDATE_ANNOUNCEMENT', enableAnswer: false, enableReply: false };
		d._titlebar = {
			title: '업데이트 공지',
			desc: '신규 서비스 및 기능 업데이트 사항을 안내합니다.',
			button: undefined,
		};
		d._meta = {
			list: [
				{ text: '분류', value: 'cate' },
				{ text: '제목', value: 'postTitle' },
				{ text: '문의자', value: 'userName' },
				{ text: '등록일', value: 'readableRegiDate' },
			],
		};
	},
	// **************************************************
	init: function (widget) {
		var that = this;
		var d = that.data;
		const u = parseUrl();

		globald = d;
		if (u.module === 'spt') {
			spt.setLnbColor();
		} else if (u.module === 'admin') {
			admin.setLnbColor();
		}
		window['sptEditor'] = undefined;
		spt.setBoardTitlebar(d._titlebar.title, d._titlebar.desc, d._titlebar.button);
		$$$('replyLabelText').setProperties({ text: Top.i18n.map.support.REPLY });
		// $$$('replyTitleLL').setProperties({ visible: d._page.enableReply ? 'visible' : 'none' });
		that.getData();
	},
	// **************************************************
	dataProcessing: function (list) {
		for (var i = 0; i < list.length; i++) {
			var elem = list[i];
			if (elem === null) continue;
			elem.contents = elem.CONTENTS;
			elem.postId = elem.POST_ID;
			elem.userId = elem.USER_ID;
			// elem.userName = elem.USER_NAME;
			elem.userName = spt.isAdmin(elem.USER_ID) ? Top.i18n.map.support.SUPPORT_TEAM : elem.USER_NAME; // 관리운영자 username 변경
			elem.readableRegiDate = elem.REGI_DATE.toReadableDate();
			elem.status = elem.STATUS;
			elem.statusText = spt.cvt(null, elem.STATUS);
			elem.postTitle = elem.SUBJECT;
			elem.cate = spt.cvt(null, elem.SUB_CATEGORY || elem.CATEGORY);
			elem.hits = elem.HITS;
		}
		return list;
	},
	getData: function (status, callback) {
		spt.startLoader();
		var that = this;
		var postId = parseUrl().postId;
		var userId = userManager.getLoginUserId();
		var productType = parseUrl().type.toUpperCase();
		var inputData = {
			dto: { POST_LIST: [{ POST_ID: postId, USER_ID: userId, PRODUCT_TYPE: productType, STATUS: 'READ' }] },
		};
		//		repospt.reset();
		console.info('BoardPostGet', inputData.dto.POST_LIST[0]);
		spt.ajax(
			'Support',
			'BoardPostGet',
			inputData,
			function successCallback(data, xhrStatus, xhr) {
				// 데이터 가공
				repospt.postList = that.dataProcessing(data.dto.POST_LIST);
				repospt.fileList = repospt.postList[0].ATTACH_LIST.map((elem) => {
					elem.fileId = elem.FILE_ID;
					elem.fileNameWithExtension = elem.FILE_NAME + '.' + elem.FILE_EXTENSION;
					elem.fileSize = elem.FILE_SIZE;
					return elem;
				});
				if (!status) {
					// 포스트 불러오기 일때만
					that.setPostMetaInfo(); // 포스트 메타 정보
					// that.createAndSetMetaTable(); // 포스트 메타 테이블
					that.showAttachments(); // 첨부파일
					that.setButtonVisibility(); // 컨트롤 버튼들 보이기
					that.showContents(); // 포스트 내용

					// 이전글, 다음글 달기
					repospt.adjacentList = [data.dto.PREV_POST || null, data.dto.NEXT_POST || null];
					that.showAdjacents(repospt.adjacentList);
				}
				// 답글 불러오기
				if (that.data._page.enableAnswer && (!status || status === 'ANSWER')) {
					repospt.ansList = repospt.postList.filter((elem) => elem.STATUS === 'ANSWER');
					that.renderReply('ANSWER', repospt.ansList, 'ans', 'answerLL', ['ans']);
				}
				// 댓글 불러오기
				if (that.data._page.enableReply && (!status || status === 'REPLY')) {
					repospt.repList = repospt.postList.filter((elem) => elem.STATUS === 'REPLY');
					that.renderReply('REPLY', repospt.repList, 'rep', 'replyLL');
				}
				setTimeout(() => {
					spt.stopLoader();
				});
			},
			function errorCallback(xhr) {
				setTimeout(() => {
					spt.stopLoader();
					TeeToast.open({ text: Top.i18n.map.support.FAIL_TO_LOAD_DATA, size: 'normal' });
				});
			},
			function complete() {
				if (typeof callback === 'function') callback();
			}
		);
	},
	setPostMetaInfo: function () {
		var that = this;
		var list = that.data._meta.list.map((elem) => elem.value).filter((elem) => elem !== undefined);
		var { postTitle, userId, userName, cate, readableRegiDate, statusText, hits } = repospt.postList[0];
		//debugger;
		// 분류
		$$$('contentCategory_TV').setProperties({ visible: 'visible', text: cate });
		// 제목
		$$$('contentTitle_TV').setProperties({ visible: 'visible', text: postTitle });
		// 작성자
		$$$('contentWriter_TV').setProperties({ visible: 'visible', text: userName });
		// 날짜
		$$$('contentDate_TV').setProperties({ visible: 'visible', text: readableRegiDate });
		// 조회수
		if (list.includes('hits')) {
			$$$('contentHits_TV').setText(hits);
			$$$('contentHits_Wrapper').setProperties({ visible: 'visible' });
		}
		// 상태
		list.includes('status') && $$$('contentStatus_TV').setProperties({ visible: 'visible', text: statusText });
		// 프로필 이미지
		$$$('contentProfile_IV').setSrc(
			userManager.getUserPhoto(userId, 'small', userManager.getUserInfo(userId).THUMB_PHOTO)
		);
	},

	//	createAndSetMetaTable: function() {
	//		var that = this;
	//		var list = that.data._meta.list;
	//		var widthOfKeyTextView = '100px';
	//		var heightOfRow = '30px';
	//		var post = repospt.postList[0];
	//		if (list.length % 2 !== 0) {
	//			list = list.push({ text: null }); // 짝수길이로 변환
	//		}
	//		// metaTableWrapper
	//		// - metaTableRow_1
	//		// - - metaTableKey_1_1
	//		// - - metaTableVal_1_1
	//		// - - metaTableKey_1_2
	//		// - - metaTableVal_1_2
	//
	//		var targetId = 'metaTableWrapper';
	//		var rowId = 'metaTableRow';
	//		var keyId = 'metaTableKey';
	//		var valId = 'metaTableVal';
	//
	//		var row;
	//		var key;
	//		var val;
	//		$$$(targetId).clear(); // 기존 위젯 초기화
	//
	//		for (var i = 0; i < list.length; i++) {
	//			var nRow = Math.floor(i / 2);
	//			var nCol = i % 2;
	//			// row layout 만들기 -> key text 만들기 -> value text 만들기
	//			if (nCol === 0) {
	//				row = Top.Widget.create('top-linearlayout', {
	//					id: rowId + '_' + nRow,
	//					className: 'row',
	//					value: list[i].postId,
	//					'layout-width': 'match_parent',
	//					'layout-height': 'wrap_content',
	//					'border-width': '0px',
	//					orientation: 'horizontal',
	//				});
	//				$$$(targetId).addWidget(row);
	//			}
	//
	//			if (list[i].text) {
	//				key = Top.Widget.create('top-textview');
	//				key.setProperties({
	//					id: keyId + '_' + nRow + '_' + nCol,
	//					className: 'key',
	//					text: list[i].text,
	//					value: list[i].value,
	//					'layout-height': heightOfRow,
	//					'layout-width': widthOfKeyTextView,
	//					//					margin: '0 5px 0 0',
	//					//                    'padding': '0px 5px 0px 5px',
	//					//					padding: '0px 0px 0px 0px',
	//				});
	//				row.addWidget(key);
	//				val = Top.Widget.create('top-textview');
	//				val.setProperties({
	//					id: valId + '_' + nRow + '_' + nCol,
	//					className: 'val',
	//					text: post[list[i].value],
	//					value: list[i].value,
	//					'layout-height': heightOfRow,
	//					'layout-width': 'calc(50%  - ' + widthOfKeyTextView + ')',
	//				});
	//				// <답변완료>상태 색 변경
	//				if (list[i].value === 'statusText' && post.status === 'CLOSED') {
	//					val.addClass('cm-text-status');
	//				}
	//				row.addWidget(val);
	//				row.complete();
	//			} else {
	//				val.setProperties({
	//					'layout-width': 'calc(100% - ' + widthOfKeyTextView + ')',
	//				});
	//				row.complete();
	//			}
	//		}
	//		$$$(targetId).complete();
	//		$$$(targetId).addClass('meta');
	//	},

	goToList: function (event, widget) {
		// history.back();
		const u = parseUrl();
		spt.routeState({
			module: u.module,
			type: u.type,
			mode: 'list',
			cate: 'all',
			subcate: 'all',
			view: 'ss30',
			page: '1',
		});
	},

	showAdjacents: function (list) {
		let that = this;
		list = that.dataProcessing(list);
		for (var i = 0; i < 2; i++) {
			// 0: prev, 1: next
			var meta = list[i];
			//			console.log(meta);
			if (!meta) {
				$$$('adjLL_' + i).setProperties({ visible: 'none' });
				continue;
			}
			$$$('adjLL_' + i).setProperties({ visible: 'visible' });
			$$$('adjStatus_' + i).setProperties({
				text: meta.statusText,
				visible: that.data._meta.list.map((elem) => elem.value).includes('statusText') ? 'visible' : 'none',
			});
			$$$('adjCate_' + i).setText(meta.cate);
			$$$('postTitle_' + i).setText(meta.SUBJECT);
			$$$('ptvPostAttachIcon_' + i).setProperties({ className: meta.attachIcon });
			$$$('adjDate_' + i).setText(meta.readableRegiDate);
			$$$('adjLL_' + i).setProperties({
				value: meta.postId,
				'on-click': function onClick(event, widget) {
					const u = parseUrl();
					spt.routeState({
						module: u.module,
						type: u.type,
						mode: u.mode,
						postId: widget.getValue(),
					});
				},
			});
		}
	},

	moveToTop: function () {
		document.querySelector('div#commonReadLayout').scrollTop = 0;
	},
	setButtonVisibility: function () {
		// 사용자 권한에 따라 버튼 표시
		var that = Top.Controller.get('commonReadLayoutLogic');
		var d = that.data;

		var postId = repospt.postList[0].postId;
		var postOwnerId = repospt.postList[0].USER_ID; // 게시글 작성자
		var amUser = !!userManager.getLoginUserId();
		var amAdmin = spt.isAdmin();
		var amPostOwner = userManager.getLoginUserId() === postOwnerId;

		//		if (amAdmin && spt.isAdmin(postOwnerId)) {
		//			amPostOwner = true;
		//		}

		// 상단 <목록> 버튼
		$$$('listButtonTop').setProperties({
			visible: 'none',
			text: Top.i18n.map.support.LIST,
			'on-click': that.onClickListButton,
		});
		// <목록> 버튼
		$$$('listButton').setProperties({
			visible: 'visible',
			text: Top.i18n.map.support.LIST,
			'on-click': that.onClickListButton,
		});
		// <맨 위로> 버튼
		$$$('upButton').setProperties({
			visible: 'none',
			text: Top.i18n.map.support.MOVE_TO_TOP_OF_PAGE,
			'on-click': that.moveToTop,
		});

		// deleteButtonTop(포스트 작성자, 오퍼레이터, 어드민)
		// answerButton(오퍼레이터, 어드민)/modifyButton(포스트 작성자)/deleteButton(포스트 작성자, 오퍼레이터, 어드민)
		// openEditorButton(회원)
		// ansReReplyButton(오퍼레이터, 어드민)/ansModifyButton(오퍼레이터, 어드민)/ansDeleteButton(오퍼레이터, 어드민)
		// repReReplyButton(회원)/repModifyButton(댓글 작성자, 오퍼레이터,어드민)/repDeleteButton(댓글 작성자, 오퍼레이터,어드민)

		// 상단위치 게시글 <수정> 버튼
		$$$('modifyButtonTop').setProperties({
			visible: amPostOwner ? 'visible' : 'none',
			text: Top.i18n.map.support.MODIFY,
			'on-click': that.onClickPostModify,
			value: postId,
		});
		// 상단위치 게시글 <삭제> 버튼
		$$$('deleteButtonTop').setProperties({
			visible: amAdmin || amPostOwner ? 'visible' : 'none',
			text: Top.i18n.map.support.DELETE_POST,
			'on-click': that.onClickPostDelete,
			value: postId,
		});
		// 게시글 <수정> 버튼
		$$$('modifyButton').setProperties({
			// visible: amPostOwner ? 'visible' : 'none',
			visible: 'none',
			text: Top.i18n.map.support.MODIFY,
			'on-click': that.onClickPostModify,
			value: postId,
		});
		// 게시글 <삭제> 버튼
		$$$('deleteButton').setProperties({
			// visible: amAdmin || amPostOwner ? 'visible' : 'none',
			visible: 'none',
			text: Top.i18n.map.support.DELETE_POST,
			'on-click': that.onClickPostDelete,
			value: postId,
		});
		// 게시글 <답변> 버튼
		$$$('answerButton').setProperties({
			visible: amAdmin && d._page.enableAnswer ? 'visible' : 'none',
			text: Top.i18n.map.support.ANSWER,
			'on-click': that.onClickPostAnswer,
			value: postId,
			disable: repospt.ansList > 0 ? true : false,
		});

		// <댓글 쓰기> 버튼
		//		$$$('openEditorButton').setProperties({
		//			visible: amUser && !amAdmin && !amOperator && d._page.enableReply ? 'visible' : 'none',
		//			text: Top.i18n.map.support.WRITE_REPLY,
		//			'on-click': that.onClickWriteReply,
		//		});

		$$$('replyTitleLL').setProperties({
			visible: d._page.enableReply ? 'visible' : 'none',
			'on-click': that.onClickToggleRep,
		});
	},
	onClickListButton: function (event, widget) {
		let that = this;
		var editor = window['sptEditor'];
		if (!editor) {
			that.goToList(); // 에디터가 없는 경우
		} else if (editor && spt.extractTextFromHtml(editor.getEditorHTML()) === '') {
			that.goToList(); // 에디터가 있지만 작성된게 없는 경우
		} else {
			TeeAlarm.open({
				title: Top.i18n.map.support.DO_YOU_WANT_TO_LEAVE_THIS_PAGE,
				content: Top.i18n.map.support.WRITTEN_INFORMATION_WILL_NOT_BE_SAVED,
				buttons: [
					{
						text: Top.i18n.map.support.LEAVE,
						onClicked: () => {
							TeeAlarm.close();
							that.goToList();
						},
					},
				],
			});
		}
	},
	showAttachments: function () {
		var targetId = 'attachmentTitleLL';
		if (!repospt.fileList || repospt.fileList.length === 0) {
			$$$(targetId).setProperties({ visible: 'none' });
			return;
		}
		var onClickDownload = function (event, widget, fileId) {
			if (!fileId) fileId = widget.getValue();
			// console.info(fileId);
			storageManager.DownloadFile(
				fileId,
				(chId = spt.CONST.CHANNEL_ID),
				(data, xhrStatus, xhr) => {},
				(xhr) => {
					TeeToast.open({ text: Top.i18n.map.support.FAIL_TO_DOWNLOAD, size: 'normal' });
				}
			);
		};

		var onClickDownloadAll = function (event, widget) {
			for (var i = 0; i < repospt.fileList.length; i++) {
				onClickDownload(event, widget, repospt.fileList[i].fileId);
			}
		};

		var onClickDelist = function (event, widget) {
			// widget.setDisabled(true); // 다운로드 버튼 비활성화
			// fileId = widget.getValue();
			// $('top-linearlayout#attWrapper' + widget.id.split('attDelistButton_').join('')).remove(); // 해당 파일 레이아웃 삭제
			// //            Top.Dom.select('.attach-file-wrapper').forEach(elem=>if(elem.getValue() === widget.getValue()){
			// //            	$('top-linearlayout#attWrapper' + widget.id.split('attCloseButton_').join('')).remove();
			// //            })
			// // 해당 파일 레이아웃 삭제
			// repospt.fileList = repospt.fileList.filter((elem) => elem.fileId !== fileId); // 데이터 삭제
			// $$$('attTitleNum').setText(Top.i18n.map.support.N_FILES.replace('{d}', repospt.fileList.length)); // 전체 파일개수 수정
			// $$$('attTitleSize').setText(
			// 	repospt.fileList
			// 		.reduce((acc, cur) => acc + cur.fileSize, 0) // 전체 파일크기 수정
			// 		.toReadableSize()
			// );
			// if (repospt.fileList.length > 0) {
			// 		$('#attTitleSave span.top-textview-root a.top-textview-url').removeClass('cursor-not-allowed');
			// } else {
			// 		$('#attTitleSave span.top-textview-root a.top-textview-url').addClass('cursor-not-allowed');
			// }
			// console.info(repospt.fileList.reduce((acc, cur) => acc + ` ${cur.fileId.substr(6)}`, '## Attachments'));
		};

		$$$(targetId).clear(); // 기존 위젯 초기화
		Top.Controller.get('components').simpleAttachment({
			targetId: targetId,
			eachFileWrapperHeight: '30px',
			eachFileWrapperWidth: '50%',
			dataRepositoryName: 'repospt',
			dataInstanceName: 'fileList',
			onClickDownload: onClickDownload,
			onClickDownloadAll: onClickDownloadAll,
			onClickDelist: onClickDelist,
		});
	},

	showContents: function () {
		$$$('contentsText').setText(repospt.postList[0].contents);
	},

	onClickPostAnswer: function (event, widget) {
		// 포스트의 <답변하기> 버튼
		if (widget.getDisabled()) return;
		this.openEditor(null, parseUrl().postId, 'ANSWER');
	},
	onClickPostModify: function (event, widget) {
		// 포스트의 <수정> 버튼
		const u = parseUrl();
		spt.routeState({
			module: u.module,
			type: u.type,
			mode: 'write',
			postId: u.postId,
		});
	},
	onClickPostDelete: function (event, widget) {
		// 포스트의 <글삭제> 버튼
		this.deleteReply(parseUrl().postId, null);
	},
	onClickWriteReply: function (event, widget) {
		// <댓글 쓰기> 버튼
		this.openEditor(null, parseUrl().postId, 'REPLY');
	},
	onClickToggleRep: function (event, widget) {
		$$$('showRepIcon').toggleClass('icon-arrow_potab_down');
		$$$('showRepIcon').toggleClass('icon-arrow_potab_up');

		if ($$$('replyLL').getProperties('visible') === 'none') {
			this.openEditor(null, parseUrl().postId, 'REPLY');
			$$$('replyLL').setProperties({ visible: 'visible' });
		} else {
			$$$('replyLL').setProperties({ visible: 'none' });
		}
		// debugger;
		// if ($('#replyEditorLL').length > 0) {
		// collapse
		// $('div#replyLL').empty();
		//			$('#replyEditorLL').remove(); //에디터 창 없애기
		//			$("[id^='rep_']").remove(); //댓글 없애기
		// } else {
		// 	// expand
		// 	this.openEditor(null, parseUrl().postId, 'REPLY');
		// 	repospt.repList = repospt.postList.filter(elem => elem.STATUS === 'REPLY');
		// 	this.renderReply('REPLY', repospt.repList, 'rep', 'replyLL');
		// }
	},

	onClickSave: function (event, widget) {
		// 답변/댓글의 <답글>버튼 또는 수정의 <저장>버튼
		const { status, postId, parentPostId } = widget.getProperties('customData');
		// 순서: createReply/updateReply -> fetchReply -> closeEditor/renderReply -> openEditor

		if (postId) {
			this.updateReply(postId, status);
		} else if (parentPostId) {
			this.createReply(parentPostId, status);
		}
		// debugger;
		// this.openEditor(null, parseUrl().postId, widget.getProperties('customData').status);
	},

	onClickCancel: function (event, widget) {
		// 편집창 <취소> 버튼
		var that = this;
		var widget = widget;
		if (window['sptEditor'].getEditorHTML() === window['sptEditor'].originalContents) {
			// 수정사항이 없으면 원상복구
			that.openEditor(null, parseUrl().postId, widget.getProperties('customData').status);
		} else {
			// 수정사항이 있으면 다이얼로그
			TeeAlarm.open({
				title: '해당 작업 내용을 취소하시겠습니까?',
				content: '작성된 내용은 저장되지 않습니다.',
				buttons: [
					{
						text: '확인',
						onClicked: () => {
							TeeAlarm.close();
							that.openEditor(null, parseUrl().postId, widget.getProperties('customData').status);
						},
					},
				],
			});
		}
	},
	closeEditor: function (editorId) {
		// 편집창 닫기
		//		$('top-linearlayout#replyEditorLL')
		//			.prev()
		//			.removeClass('squeeze');
		//		$('top-linearlayout#replyEditorLL')
		//			.prev()
		//			.addClass('expand');

		if (!!$('#replyEditorLL') && $('#replyEditorLL').length > 0) {
			$('#replyEditorLL').remove();
		}
	},
	openEditor: function (postId, parentPostId, status, focusOnEditor = false) {
		// debugger;
		// 편집창 열기
		// 포스트에 <답변> 버튼 누를때 (null, current post id, 'ANSWER')
		// 포스트에 <댓글> 버튼 누를때 (null, current post id, 'REPLY')
		// 댓글에 <답글> 버튼 누를때 (null, rep id, 'REPLY')
		// 댓글에 <수정> 버튼 누를때 (rep id, null, 'REPLY')
		// debugger;
		if (status === 'ANSWER') {
			var selectorPrefix = 'ans_';
			var dataInstId = 'ansList';
			var targetId = 'answerLL';
		} else if (status === 'REPLY') {
			var selectorPrefix = 'rep_';
			var dataInstId = 'repList';
			var targetId = 'replyLL';
		} else {
			return;
		}

		var that = this;
		var currentId = postId || parentPostId;
		var editorId = `editor-${currentId}`;
		console.info(`postId: ${postId} / parentPostId: ${parentPostId} / status: ${status} / editorId: ${editorId}`);

		var currentContents;
		if (postId) {
			// 수정모드
			currentContents = repospt[dataInstId].filter((elem) => elem.POST_ID === currentId)[0].CONTENTS;
		} else if (parentPostId && status === 'ANSWER') {
			// 쓰기모드(템플릿 있음)
			currentContents =
				'<p>안녕하세요. TeeSpace 고객지원팀입니다.</p><p><br></p><p><br></p><p><br></p><p>감사합니다.</p>';
		} else if (parentPostId) {
			// 쓰기모드(템플릿 없음)
			currentContents = '';
		} else {
			return;
		}

		that.closeEditor(); // 열려있는 편집창 닫기

		// widget 생성
		// replyEditorLL
		// - replyEditorWrapper
		// - - textarea(맨마지막)
		// - replyButtonWrapper
		// - - replySaveButton
		// - - replyCancelButton

		//		var replyEditorRoot = Top.Widget.create('top-linearlayout', {
		//			id: 'replyEditorRoot',
		//			'layout-width': 'match_parent',
		//			'layout-height': 'wrap_content',
		//			'min-height': '300px',
		//			orientation: 'horizontal',
		//			'border-width': '0px',
		//		});

		//		var replyEditorPad = Top.Widget.create('top-linearlayout', {
		//			id: 'replyEditorPad',
		//			'layout-width': '100px',
		//			'layout-height': 'wrap_content',
		//			orientation: 'vertical',
		//			'border-width': '0px',
		//		});
		//		replyEditorRoot.addWidget(replyEditorPad);

		var replyEditorLL = Top.Widget.create('top-linearlayout', {
			id: 'replyEditorLL',
			//			'layout-width': 'match_parent',
			'layout-width': 'calc(100% - 30px)',
			'layout-height': 'wrap_content',
			orientation: 'vertical',
			'border-width': '0px',
		});

		var replyButtonWrapper = Top.Widget.create('top-linearlayout', {
			id: 'replyButtonWrapper',
			'layout-width': 'match_parent',
			'layout-height': 'wrap_content',
			orientation: 'horizontal',
			'border-width': '0px 0px 0px 0px',
		});
		var replySaveButton = Top.Widget.create('top-button');
		replySaveButton.setProperties({
			id: 'replySaveButton',
			text: '저장',
			className: 'solid',
			'layout-height': 'wrap_content',
			'layout-width': 'wrap_content',
			// disabled: true,
			// disabled: false,
			'on-click': function (event, widget) {
				// if (widget.getDisabled()) return;
				Top.Controller.get('commonReadLayoutLogic').onClickSave(event, widget);
			},
			customData: {
				postId: postId,
				parentPostId: parentPostId,
				status: status,
			},
		});
		replyButtonWrapper.addWidget(replySaveButton);
		var replyCancelButton = Top.Widget.create('top-button', {
			id: 'replyCancelButton',
			text: '취소',
			className: 'outlined',
			'layout-height': 'wrap_content',
			'layout-width': 'wrap_content',
		});
		replyCancelButton.setProperties({
			'on-click': function (event, widget) {
				Top.Controller.get('commonReadLayoutLogic').onClickCancel(event, widget);
			},
			customData: {
				postId: postId,
				parentPostId: parentPostId,
				status: status,
			},
		});

		replyButtonWrapper.addWidget(replyCancelButton);
		replyButtonWrapper.complete();
		replyEditorLL.addWidget(replyButtonWrapper);
		replyEditorLL.complete();
		//		replyEditorRoot.addWidget(replyEditorLL);
		//		replyEditorRoot.complete();

		// 에디터 위치 표시위치를 정하기 위해 몇번째 댓글인지 찾기(주석처리)
		// 에디터가 댓글보다 위에 있기 때문에 repCnt = -1 로 준 것
		// var repCnt = -1;
		var repCnt = 0;
		for (repCnt = 0; repCnt < repospt[dataInstId].length; repCnt++) {
			if (repospt[dataInstId][repCnt].POST_ID === currentId) {
				break;
			}
		}

		// 수정모드면 해당 댓글 안보이게 하기
		$('top-linearlayout#' + selectorPrefix + repCnt.toString()).addClass('squeeze');

		var parentEl = document.querySelector('div#' + targetId);
		var nextEl = document.querySelector('top-linearlayout#' + selectorPrefix + (repCnt + 1).toString());
		// replyEditorLL.template is HTMLElement
		parentEl.insertBefore(replyEditorLL.template, nextEl);

		var tmpTextarea = document.createElement('textarea');
		tmpTextarea.setAttribute('id', editorId);
		document.querySelector('div#replyEditorLL').prepend(tmpTextarea);

		//		//************
		//		var parentEl = document.querySelector('div#replyLL');
		//		var nextEl = document.querySelector('top-linearlayout#' + selectorPrefix + (repCnt + 1).toString());
		//		// replyEditorLL.template is HTMLElement
		//		parentEl.insertBefore(replyEditorRoot.template, nextEl);
		//
		//		var tmpTextarea = document.createElement('textarea');
		//		tmpTextarea.setAttribute('id', editorId);
		//		document.querySelector('div#replyEditorLL').prepend(tmpTextarea);
		//		//************

		that.initSptTopEditor(editorId, currentContents); // window['sptEditor'] 세팅

		setTimeout(function () {
			// set template text
			window['sptEditor'].setEditorHTML(currentContents);
			// move viewport to editor
			if (focusOnEditor) {
				elem = document.getElementById('replyEditorLL');
				elem.scrollIntoView({ behavior: 'smooth', block: 'center' });
			}
		}, 0);
	},

	initSptTopEditor: function (editorId, originalContents) {
		window['sptEditor'] = undefined; // 초기화 먼저
		window['sptEditor'] = Top.Widget.create('top-htmleditor', {
			id: editorId,
			'layout-height': 'match_parent',
			'layout-width': 'match_parent',
			'on-keypress': function (event) {
				console.info('onkeypress');
			},
			'on-keydown': function (event) {
				console.info('onkeydown');
			},
			'on-keyup': (event) => {
				console.info(event);
				// if ($$$(editorid).getEditorHTML() === '') {
				// 	$$$('replySaveButton') && $$$('replySaveButton').setDisabled(true);
				// } else {
				// 	$$$('replySaveButton') && $$$('replySaveButton').setDisabled(false);
				// }
			},
		});
		window['sptEditor'].originalContents = originalContents;
		document.querySelector(`textarea#${editorId}`).replaceWith(window['sptEditor'].template);

		/* <textarea class="autosize" onkeydown="resize(this)" onkeyup="resize(this)" ></textarea>
		function resize(obj) {
		obj.style.height = "1px";
		obj.style.height = (12+obj.scrollHeight)+"px";
		}
		*/

		// 글자수 최대 제한
	},

	fetchReply: function (status, recentPostId) {
		// ANSWER, REPLY 어떤 걸 새로 그릴 것인가
		var that = this;
		that.closeEditor();
		var inputData = {
			dto: {
				POST_LIST: [
					{
						POST_ID: parseUrl().postId,
						USER_ID: userManager.getLoginUserId(),
						STATUS: status,
					},
				],
				MIRROR: status,
			},
		};
		console.info('BoardPostGet', inputData.dto.POST_LIST[0]);
		spt.ajax(
			'Support',
			'BoardPostGet',
			inputData,
			function successCallback(data, xhrStatus, xhr) {
				var status = data.dto.MIRROR;
				if (status === 'ANSWER') {
					repospt['ansList'] = that.dataProcessing(
						data.dto.POST_LIST.filter((elem) => elem.STATUS === status)
					);
					that.renderReply(status, repospt.ansList, 'ans', 'answerLL', ['ans'], recentPostId);
				} else if (status === 'REPLY') {
					repospt['repList'] = that.dataProcessing(
						data.dto.POST_LIST.filter((elem) => elem.STATUS === status)
					);
					that.renderReply(status, repospt.repList, 'rep', 'replyLL', [], recentPostId);
				}
			},
			function errorCallback(xhr) {
				TeeToast.open({ text: Top.i18n.map.support.FAIL_TO_LOAD_REPLY, size: 'normal' });
			}
		);
	},
	editorPreProcess: function (htmlString) {
		// 공백 기호 삭제
		[].filter.call(htmlString, (c) => c.charCodeAt() !== 8203).join('');

		// 테이블 안보이는 현상을 위한 태그 추가
		// const CONTENTS_MUST_BE_WRAPPED_IN_THIS_TAG_1 = '';
		// const CONTENTS_MUST_BE_WRAPPED_IN_THIS_TAG_2 = '';
		const CONTENTS_MUST_BE_WRAPPED_IN_THIS_TAG_1 =
			'<div class="input_editor sun-editor-id-wysiwyg sun-editor-editable">';
		const CONTENTS_MUST_BE_WRAPPED_IN_THIS_TAG_2 = '</div>';
		if (!htmlString.startsWith(CONTENTS_MUST_BE_WRAPPED_IN_THIS_TAG_1)) {
			htmlString = CONTENTS_MUST_BE_WRAPPED_IN_THIS_TAG_1 + htmlString + CONTENTS_MUST_BE_WRAPPED_IN_THIS_TAG_2;
		}
		return htmlString;
	},
	createReply: function (parentPostId, status) {
		if (!['REPLY', 'ANSWER'].includes(status)) return;
		var that = this;
		var successMsg = status === 'ANSWER' ? '답변이 등록되었습니다.' : '댓글이 등록되었습니다.';
		var errorMsg =
			status === 'ANSWER'
				? '답변 생성에 실패하였습니다. 다시 시도해주시길 바랍니다.'
				: '댓글 생성에 실패하였습니다. 다시 시도해주시길 바랍니다.';
		var inputData = {
			dto: {
				POST_LIST: [
					{
						PARENT_POST_ID: parentPostId,
						USER_ID: userManager.getLoginUserId(),
						STATUS: status,
						CONTENTS: that.editorPreProcess(window['sptEditor'].getEditorHTML()),
					},
				],
				MIRROR: status,
			},
		};
		console.info('BoardPostCreate', inputData.dto.POST_LIST);
		spt.ajax(
			'Support',
			'BoardPostCreate',
			inputData,
			function successCallback(data, xhrStatus, xhr) {
				TeeToast.open({ text: successMsg, size: 'normal' });
				that.fetchReply(data.dto.MIRROR, data.dto.POST_LIST[0].POST_ID);
				window['sptEditor'] = undefined;
			},
			function errorCallback(xhr) {
				TeeToast.open({ text: errorMsg, size: 'normal' });
			}
		);
	},
	updateReply: function (postId, status) {
		if (!['REPLY', 'ANSWER'].includes(status)) return;
		var that = this;
		var errorMsg =
			status === 'ANSWER'
				? '답변 수정에 실패하였습니다. 다시 시도해주시길 바랍니다.'
				: '댓글 수정에 실패하였습니다. 다시 시도해주시길 바랍니다.';
		var inputData = {
			dto: {
				POST_LIST: [
					{
						POST_ID: postId,
						USER_ID: userManager.getLoginUserId(),
						CONTENTS: that.editorPreProcess(window['sptEditor'].getEditorHTML()),
					},
				],
				MIRROR: status,
			},
		};
		console.info('BoardPostUpdate', inputData.dto.POST_LIST[0]);
		spt.ajax(
			'Support',
			'BoardPostUpdate',
			inputData,
			function successCallback(data, xhrStatus, xhr) {
				that.fetchReply(data.dto.MIRROR, postId);
			},
			function errorCallback(xhr) {
				TeeToast.open({ text: errorMsg, size: 'normal' });
			}
		);
	},
	deleteReply: function (postId, status) {
		let that = this;
		let userId = userManager.getLoginUserId();
		let title, description;
		if (status === 'ANSWER') {
			title = Top.i18n.map.support.DO_YOU_WANT_TO_DELETE_THE_ANSWER;
			description = Top.i18n.map.support.DELETED_ANSWER_WILL_NOT_BE_RECOVERED;
		} else if (status === 'REPLY') {
			title = Top.i18n.map.support.DO_YOU_WANT_TO_DELETE_THE_REPLY;
			description = Top.i18n.map.support.DELETED_REPLY_WILL_NOT_BE_RECOVERED;
		} else {
			title = Top.i18n.map.support.DO_YOU_WANT_TO_DELETE_THE_INQUIRY;
			description = Top.i18n.map.support.DELETED_INQUIRY_WILL_NOT_BE_RECOVERED;
		}

		TeeAlarm.open({
			title: title,
			content: description,
			buttons: [
				{
					text: Top.i18n.map.support.DELETE,
					onClicked: () => {
						TeeAlarm.close();
						var inputData = { dto: { POST_LIST: [{ USER_ID: userId, POST_ID: postId }], MIRROR: status } };
						console.info('BoardPostDelete', inputData.dto.POST_LIST[0]);
						spt.ajax(
							'Support',
							'BoardPostDelete',
							inputData,
							function successCallback(data, xhrStatus, xhr) {
								var status = data.dto.MIRROR;
								if (status === 'REPLY') {
									// 댓글/답변 삭제인 경우
									let successMsg =
										status === 'ANSWER'
											? Top.i18n.map.support.ANSWER_IS_DELETED
											: Top.i18n.map.support.REPLY_IS_DELETED;
									that.getData(status, () => {
										TeeToast.open({ text: successMsg, size: 'normal' });
									});
								} else {
									// 포스트 삭제인 경우
									const u = parseUrl();
									spt.routeState({
										module: u.module,
										type: u.type,
										mode: 'list',
										cate: 'all',
										subcate: 'all',
										page: 1,
										view: 'ss30',
									});
								}
							},
							function errorCallback(xhr) {
								TeeToast.open({ text: Top.i18n.map.support.FAIL_TO_DELETE, size: 'normal' });
							}
						);
					},
				},
			],
		});
	},

	renderReply: function (status, list, widgetIdPrefix, targetId, customClassList = [], recentPostId = null) {
		if (!list || !widgetIdPrefix || !targetId) {
			return;
		}
		var that = this;

		$(`#${targetId} .rep`).remove(); // 기존의 답변 또는 댓글 삭제
		$('.recent').removeClass('recent');

		if (status === 'REPLY') {
			$$$('replyCountText').setProperties({
				text: list.length,
				// visible: list.length > 0 ? 'visible' : 'none', // 댓글이 1개 이상 있는 경우 댓글 없음 문구 제거
				visible: 'visible',
			});
			$$$('noreplyText').setProperties({
				text: Top.i18n.map.support.THERE_IS_NO_REPLY,
				visible: list.length > 0 ? 'none' : 'visible',
			});
		}

		// <수정> 클릭 시 원래 댓글 축소 애니메이션
		//		var squeezeRep = function(flag, idx) {
		//			//			https://stackoverflow.com/questions/37080838/is-there-a-css-code-for-double-click-element-or-another-way-to-solve-this
		//			//			height:40px
		//			if (flag) {
		//				$('div#rep_' + idx).addClass('squeeze');
		//				$('div#rep_' + idx).removeClass('expand');
		//				//				$$$('repContents_'+idx).setProperties({visible:'none'})
		//			} else {
		//				$('div#rep_' + idx).addClass('expand');
		//				$('div#rep_' + idx).removeClass('squeeze');
		//				//				$$$('repContents_'+idx).setProperties({visible:'visible'})
		//			}
		//		};

		// <답글> 버튼 클릭 이벤트
		var onClickRepReply = function (event, widget) {
			const { postId, status } = widget.getProperties('customData');
			that.openEditor(null, postId, status);
		};
		// <수정> 버튼 클릭 이벤트
		var onClickRepModify = function (event, widget) {
			const { postId, status } = widget.getProperties('customData');
			that.openEditor(postId, null, status);
		};
		// <삭제> 버튼 클릭 이벤트
		var onClickRepDelete = function (event, widget) {
			const { postId, status } = widget.getProperties('customData');
			that.deleteReply(postId, status);
		};

		// ***** widget hierarchy *****
		// rep(value)
		// - repWrapper
		// --- repPad
		// --- repTextviews
		// ----- repNameWrapper
		// ------- repName
		// ------- repOwner
		// ----- repDate
		// ----- repContents
		// -- repButtons
		// --- repReReplyButton
		// --- repModifyButton
		// --- repDeleteButton

		for (var i = 0; i < list.length; i++) {
			var amUser = !!userManager.getLoginUserId();
			var amAdmin = spt.isAdmin();
			var amPostOwner = userManager.getLoginUserId() === repospt.postList[0].USER_ID;
			var amRepOwner = userManager.getLoginUserId() === list[i].USER_ID;
			// 답글/댓글에 대해서 댓글이 가능한지
			var enableCascadeReply = status === 'ANSWER' ? false : true;
			var classNameList = ['rep'];
			status === 'ANSWER' && classNameList.push('ans');
			amAdmin && classNameList.push('admin');
			// var classNameString = 'rep';
			// classNameString = amAdmin ? classNameString + ' admin' : classNameString;
			// classNameString = status === 'ANSWER' ? classNameString + ' ans' : classNameString;
			var rep = Top.Widget.create('top-linearlayout', {
				id: `${widgetIdPrefix}_${i}`,
				// className: classNameString,
				className: classNameList.join(' '),
				value: list[i].postId,
				'layout-height': 'wrap_content',
				'border-width': '0px',
				'layout-width': 'match_parent',
				orientation: 'vertical',
			});

			if (list[i].postId === recentPostId) {
				rep.addClass('recent');
			}

			var repWrapper = Top.Widget.create('top-linearlayout');
			repWrapper.setProperties({
				id: `${widgetIdPrefix}Wrapper_${i}`,
				className: 'wrapper',
				'layout-height': 'wrap_content',
				'border-width': '0px',
				'layout-width': 'match_parent',
				orientation: 'horizontal',
				customData: {
					postId: list[i].POST_ID,
					parentPostId: list[i].PARENT_POST_ID,
					status: list[i].STATUS,
				},
				'on-dblclick': function (event, widget) {
					//					onClickRepReply(event, widget);
				},
			});

			// add left padding. 최대 MAX_REPLY_DEPTH개까지
			const MAX_REPLY_DEPTH = 5;
			const MAX_REPLY_DEPTH_MOBILE = 3;
			if (window.matchMedia('screen and (max-device-width: 1024px)').matches) {
				var maxDepth = Math.min(list[i].DEPTH - 1, MAX_REPLY_DEPTH_MOBILE);
			} else {
				var maxDepth = Math.min(list[i].DEPTH - 1, MAX_REPLY_DEPTH);
			}

			for (var repPadCounter = 0; repPadCounter < maxDepth; repPadCounter++) {
				var repMarkIndent = '25px';
				var repPad = Top.Widget.create('top-imageview');
				repPad.setProperties({
					id: `${widgetIdPrefix}Pad_${i}_${repPadCounter}`,
					className: 'pad',
					'layout-height': 'wrap_content',
					'layout-width': repMarkIndent,
					margin: '0 5px 0 0',
					padding: '0px 0px 0px 0px',
				});
				if (repPadCounter === maxDepth - 1) {
					repPad.setProperties({
						className: 'mark', // substitute the class, not to add
						src: 'res/support/spt_reply.svg',
						'layout-height': '15px',
						'layout-width': '15px',
					});
				}
				repWrapper.addWidget(repPad);
			}
			//댓글 답변 달 때 아이디 옆에 프로필 사진 넣기
			// var repUserId = repospt.postList[i].USER_ID;
			var repUserId = list[i].USER_ID;
			var repProfileImg = Top.Widget.create('top-imageview');
			var profileImg = userManager.getUserPhoto(
				repUserId,
				'small',
				userManager.getUserInfo(repUserId).THUMB_PHOTO
			);

			repProfileImg.setProperties({
				'layout-height': '33px',
				'layout-width': '33px',
				className: 'profileIcon',
				margin: '0 5px 0 0',
				padding: '0px 0px 0px 0px',
				src: profileImg,
			});
			repWrapper.addWidget(repProfileImg);

			// reply textviews wrapper
			var repTextviews = Top.Widget.create('top-linearlayout');
			repTextviews.setProperties({
				id: `${widgetIdPrefix}Textviews_${i}`,
				'layout-height': 'wrap_content',
				'border-width': '0px',
				'layout-width': 'match_parent',
				orientation: 'vertical',
			});

			var repNameWrapper = Top.Widget.create('top-linearlayout');
			repNameWrapper.setProperties({
				id: `${widgetIdPrefix}NameWrapper_${i}`,
				className: 'name-wrapper',
				'layout-height': 'wrap_content',
				'border-width': '0px',
				'layout-width': 'match_parent',
				orientation: 'horizontal',
			});
			repTextviews.addWidget(repNameWrapper);

			// username
			var repName = Top.Widget.create('top-textview');
			repName.setProperties({
				id: `${widgetIdPrefix}Name_${i}`,
				className: 'name',
				text: list[i].userName,
				'layout-height': 'wrap_content',
				'layout-width': 'wrap_content',
			});
			repNameWrapper.addWidget(repName);

			// post owner mark
			if (list[i].USER_ID === repospt.postList[0].USER_ID) {
				var repOwner = Top.Widget.create('top-textview');
				repOwner.setProperties({
					id: `${widgetIdPrefix}Owner_${i}`,
					className: 'owner',
					value: list[i].postId,
					text: Top.i18n.map.support.POST_OWNER,
					'layout-height': 'wrap_content',
					'layout-width': 'wrap_content',
				});
				repNameWrapper.addWidget(repOwner);
			}
			repNameWrapper.complete();

			// date
			var readableDateText = list[i].MODI_DATE
				? `${list[i].MODI_DATE.toReadableDate()} ${Top.i18n.map.support.MODIFIED_MARK_TEXT}`
				: list[i].REGI_DATE.toReadableDate();

			var repDate = Top.Widget.create('top-textview');
			repDate.setProperties({
				id: `${widgetIdPrefix}Date_${i}`,
				className: 'date',
				text: readableDateText,
				'layout-height': 'wrap_content',
				'layout-width': 'wrap_content',
			});
			repTextviews.addWidget(repDate);

			// reply Contents
			var repContents = Top.Widget.create('top-textview');
			repContents.setProperties({
				id: `${widgetIdPrefix}Contents_${i}`,
				text: list[i].CONTENTS,
				//	        'min-height':'100px',
				'layout-height': 'wrap_content',
				'layout-width': 'match_parent',
				'multi-line': true,
			});
			repTextviews.addWidget(repContents);
			repTextviews.complete();
			repWrapper.addWidget(repTextviews);

			repWrapper.complete();
			rep.addWidget(repWrapper);

			// reply button wrapper
			var repButtons = Top.Widget.create('top-linearlayout');
			repButtons.setProperties({
				id: `${widgetIdPrefix}Buttons_${i}`,
				className: 'buttons',
				'border-width': '0px',
				'layout-height': 'wrap_content',
				'layout-width': 'wrap_content',
				orientation: 'horizontal',
			});

			// ansReReplyButton(오퍼레이터, 어드민)/ansModifyButton(오퍼레이터, 어드민)/ansDeleteButton(오퍼레이터, 어드민)
			// repReReplyButton(회원)/repModifyButton(댓글 작성자, 오퍼레이터,어드민)/repDeleteButton(댓글 작성자, 오퍼레이터,어드민)

			// <답글> 버튼
			if (amUser && enableCascadeReply) {
				var repReReplyButton = Top.Widget.create('top-button');
				repReReplyButton.setProperties({
					id: `${widgetIdPrefix}ReReplyButton_${i}`,
					value: list[i].postId,
					text: Top.i18n.map.support.RE_REPLY, // '답글'
					className: 'cmbutton text-line2 margin-right',
					'layout-height': 'wrap_content',
					'layout-width': 'wrap_content',
					'on-click': onClickRepReply,
					customData: {
						postId: list[i].POST_ID,
						parentPostId: list[i].PARENT_POST_ID,
						status: list[i].STATUS,
					},
				});
				repButtons.addWidget(repReReplyButton);
			}

			// <수정> 버튼
			if (amRepOwner) {
				var repModifyButton = Top.Widget.create('top-button');
				repModifyButton.setProperties({
					id: `${widgetIdPrefix}ModifyButton_${i}`,
					value: list[i].postId,
					text: Top.i18n.map.support.MODIFY,
					className: 'cmbutton text-line2 margin-right',
					'layout-height': 'wrap_content',
					'layout-width': 'wrap_content',
					'on-click': onClickRepModify,
					customData: {
						postId: list[i].POST_ID,
						parentPostId: list[i].PARENT_POST_ID,
						status: list[i].STATUS,
					},
				});
				repButtons.addWidget(repModifyButton);
			}

			// <삭제> 버튼
			if (amRepOwner || amAdmin) {
				var repDeleteButton = Top.Widget.create('top-button');
				repDeleteButton.setProperties({
					id: `${widgetIdPrefix}DeleteButton_${i}`,
					value: list[i].postId,
					text: Top.i18n.map.support.DELETE,
					className: 'cmbutton text-line2',
					'layout-height': 'wrap_content',
					'layout-width': 'wrap_content',
					'on-click': onClickRepDelete,
					customData: {
						postId: list[i].POST_ID,
						parentPostId: list[i].PARENT_POST_ID,
						status: list[i].STATUS,
					},
				});
				repButtons.addWidget(repDeleteButton);
			}

			repButtons.complete();
			rep.addWidget(repButtons);
			rep.complete();
			$$$(targetId).addWidget(rep);
		}
		$$$(targetId).complete();

		// 에디터 열기
		if (status === 'REPLY') {
			this.openEditor(null, parseUrl().postId, status);
		}
		// 답변있는 경우 답변버튼 비활성화
		$$$('answerButton') && $$$('answerButton').setDisabled(repospt.ansList.length > 0 ? true : false);

		setTimeout(function () {
			// block(vertical), inline(horizontal): start, center, end
			elem = document.querySelector('.recent');
			elem && elem.scrollIntoView({ behavior: 'smooth', block: 'center', inline: 'start' });
		}, 0);
	},
});

function testchip() {
	var list = [];
	for (var i = 0; i < 5; i++) {
		list[i] = {
			fileId: ''.rnd(5),
			fileExtension: 'png',
			fileNameWithExtension: ''.rnd(10) + '.png',
			readableFileSize: Number.prototype.rnd(100).toReadableSize(),
		};
	}
	$cmp('attachmentChip', {
		targetId: 'attachmentLL',
		list: list,
	});
}

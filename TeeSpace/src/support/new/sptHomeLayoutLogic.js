Top.Controller.create('sptHomeLayoutLogic', {
	data: {
		_titlebar: {
			title: '홈',
			desc: '고객 지원을 통해 편리하고 빠르게 TeeSpace의 궁금증을 해결하세요.',
			button: {
				visible: 'none',
				text: undefined,
				onClick: undefined,
			},
		},
	},
	init: function init(widget) {
		var that = this;
		var d = that.data;
		spt.setLnbColor();
		spt.setBoardTitlebar(d._titlebar.title, d._titlebar.desc, d._titlebar.button);

		$$$('shortcutButton0').setProperties({
			text: '다운로드',
			'on-click': Top.Controller.get('sptManualLayoutLogic').downloadManual,
		});
		$$$('shortcutButton1').setProperties({
			text: '확인하기',
			value: Top.Controller.get('sptMainLayoutLogic').lnbButtonList.filter((elem) => elem.type === 'faq')[0].hash,
			'on-click': function (event, widget) {
				Top.App.routeTo(widget.getValue());
			},
		});
		if (isb2c()) {
			// 1:1 문의
			$$$('shortcutImg2').setProperties({
				src: '@drawable/img_1_1.png',
			});
			$$$('shortcutButton2').setProperties({
				text: '문의하기',
				value: '/spt/oneonone?mode=write',
				'on-click': function (event, widget) {
					Top.App.routeTo(widget.getValue());
				},
			});
		} else {
			// Q&A
			$$$('shortcutImg2').setProperties({
				src: '@drawable/img_qna.png',
			});
			$$$('shortcutButton2').setProperties({
				text: '문의하기',
				value: '/spt/qna?mode=write',
				'on-click': function (event, widget) {
					Top.App.routeTo(widget.getValue());
				},
			});
		}

		// service announcements
		$$$('annTitleText_0').setText('서비스 공지');
		$$$('annMoreButton_0').setProperties({
			text: '더보기',
			'on-click': this.onClickSvcMore,
		});
		$$$('annListView_0').setProperties({ 'on-rowclick': this.onClickSvcPost });
		// update announcements
		$$$('annTitleText_1').setText('업데이트 공지');
		$$$('annMoreButton_1').setProperties({
			text: '더보기',
			'on-click': this.onClickUpdMore,
		});
		$$$('annListView_1').setProperties({ 'on-rowclick': this.onClickUpdPost });

		repospt.postList = [];

		this.setAnnList('SERVICE_ANNOUNCEMENT', 'annListView_0', 'svcAnnList');
		this.setAnnList('UPDATE_ANNOUNCEMENT', 'annListView_1', 'updAnnList');
	},

	setAnnList: function setAnnList(productType, listViewId, dataInstId) {
		let inputData = {
			dto: {
				POST_LIST: [
					{
						PARENT_POST_ID: 'root',
						PRODUCT_TYPE: productType,
						ROW_FROM: 1,
						ROW_TO: 3,
					},
				],
				MIRROR: dataInstId,
			},
		};
		console.info(inputData);

		spt.ajax(
			'Support',
			'BoardPostListGet',
			inputData,
			function successCallback(data, xhrStatus, xhr) {
				var pl = data.dto.POST_LIST;
				var dataInstId = data.dto.MIRROR;
				var inst = [];

				if (pl.length === 0) {
					// If no announcement
					inst.push({
						postTitle: '공지가 없습니다.',
						visibleNew: 'none',
						readableRegiDate: '',
					});
					$$$(listViewId).setProperties({
						'on-rowclick': function () {},
						className: 'empty',
					});
				}

				for (var i = 0; i < pl.length; i++) {
					inst.push({
						postId: pl[i].POST_ID,
						postTitle: pl[i].SUBJECT,
						readableRegiDate: pl[i].REGI_DATE.split(' ')[0].split('-').join('.'),
						visibleNew: spt.isNew(pl[i].REGI_DATE) ? 'visible' : 'none',
						// displayOption = spt.isNew(pl[i].REGI_DATE) ? 'visible' : 'none',
					});

					// aa = `<sup style="color:red;display:${displayOption}">●</sup>`;
				}

				repospt.setValue(dataInstId, inst);
			},

			function errorCallback(xhr, xhrStatus, err) {
				$$$(listViewId).setProperties({
					'on-rowclick': () => {},
				});
				repospt.setValue(dataInstId, [
					{
						postTitle: '공지를 불러올 수 없습니다.',
						visibleNew: 'none',
						readableRegiDate: '',
					},
				]);
			}
		);
	},
	onClickSvcMore: function onClickSvcMore(event, widget) {
		let lnbButtonList = Top.Controller.get('sptMainLayoutLogic').lnbButtonList;
		let hash = lnbButtonList.filter((elem) => elem.type === 'service_announcement')[0].hash;
		Top.App.routeTo(hash);
	},
	onClickSvcPost: function onClickSvcPost(event, widget) {
		console.info(widget);
		Top.App.routeTo('/spt/service_announcement?mode=read&postId=' + widget.getClickedData().postId);
	},
	onClickUpdMore: function onClickUpdMore(event, widget) {
		let lnbButtonList = Top.Controller.get('sptMainLayoutLogic').lnbButtonList;
		let hash = lnbButtonList.filter((elem) => elem.type === 'update_announcement')[0].hash;
		Top.App.routeTo(hash);
	},
	onClickUpdPost: function onClickUpdPost(event, widget) {
		console.info(widget);
		Top.App.routeTo('/spt/update_announcement?mode=read&postId=' + widget.getClickedData().postId);
	},
});

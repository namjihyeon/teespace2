Top.Controller.create('commonDialogLayoutLogic', {
	settings: {
		icon: undefined,

		title: '', // title(String) : dialog title
		titleClass: undefined, // additioanl class

		msg: '', //  msg(String) : dialog message
		msgSubstitute: false,

		input: false, // input(Boolean) : dialog has input text box
		inputDefaultMsg: false, //  inputDefaultMsg(String) : default text of input text box
		inputHint: '', //  inputHint(String) : hint text of input text box

		desc: '', //   desc(String) : detailed description
		descClass: false,
		descSubstitute: '',

		guide: '',
		guideClass: undefined,
		guideSubstitute: false,

		b1: '', // b#(String) : text for button#
		b2: '',
		b3: '',
		b1Func: undefined, //  b#Func(Function) : callback function for button#
		b2Func: undefined,
		b3Func: undefined,
		b1ClassName: undefined,
		b2ClassName: undefined,
		b3ClassName: undefined,

		timeout: false, //  timeout(Number) : set time[ms] to close dialog automatically
		headCallback: undefined, //headCallback(Function) : right after init function beginning
		tailCallback: undefined, // tailCallback(Function) : right before init function ending
		// file chooser
		attBtn: '', // attachment button
		attFunc: undefined,
		fileChooser: true,
		multipleFiles: true,
		allowableFileExtensions: [],
		allowableFileTypes: [],
		fileChooserTooltipMsg: '올바른 파일 형식을 선택해주세요.',
		fileChooserButton: '파일 선택',
	},

	open: function (settings) {
		Top.Controller.get('commonDialogLayoutLogic').settings = settings;
		Top.Dom.selectById('commonDialog').open();
	},

	init: function (widget) {
		// After loading layout
		var dg = $$$('commonDialog');
		var that = Top.Controller.get('commonDialogLayoutLogic');
		var s = that.settings;

		if (typeof s.headCallback === 'function') {
			s.headCallback();
		}
		// base layout
		var ll0 = $$$('commonDialogLayout');

		// dialog title
		dg.setProperties({
			title: s.title ? s.title : '',
		});

		//icon
		if (s.icon) {
			$$$('iconLL').setProperties({
				visible: 'visible',
			});
			$('top-icon#commonDialogIcon').addClass(s.icon);
		} else {
			$$$('msgLL').setProperties({
				visible: 'none',
			});
		}

		// msg
		if (s.msg) {
			$$$('msgLL').setProperties({
				visible: 'visible',
			});
			var tmp = s.msg;
			if (s.msgSubstitute) {
				for (var i = 0; i < s.msgSubstitute; i++) {
					tmp = tmp.replace('{{s}}', String(s.msgSubstitute[i]));
				}
			}
			$$$('msgTv').setText(tmp);
		} else {
			$$$('msgLL').setProperties({
				visible: 'none',
			});
		}

		// input
		if (s.input) {
			$$$('inputLL').setProperties({
				visible: 'visible',
			});
			if (s.inputHint) {
				$$$('inputTf').setProperties({
					hint: s.inputHint,
				});
			}
		} else {
			$$$('inputLL').setProperties({
				visible: 'none',
			});
		}

		// guide
		if (s.guide) {
			$$$('guideLL').setProperties({
				visible: 'visible',
			});
			var tmp = s.guide;
			if (s.guideSubstitute) {
				for (var i = 0; i < s.guideSubstitute; i++) {
					tmp = tmp.replace('{{s}}', String(s.guideSubstitute[i]));
				}
			}
			$$$('guideTv').setText(tmp);
		} else {
			$$$('guideLL').setProperties({
				visible: 'none',
			});
		}

		// desc
		if (s.desc) {
			$$$('descLL').setProperties({
				visible: 'visible',
			});
			var tmp = s.desc;
			if (s.descSubstitute) {
				for (var i = 0; i < s.descSubstitute; i++) {
					tmp = tmp.replace('{{s}}', String(s.descSubstitute[i]));
				}
			}
			$$$('descTv').setText(tmp);
		} else {
			$$$('descLL').setProperties({
				visible: 'none',
			});
		}

		// button
		if (s.b1 || s.b2 || s.b3) {
			$$$('buttonLL').setProperties({ visible: 'visible' });
			if (s.b1) {
				var btn = $$$('button1');
				btn.setProperties({
					visible: 'visible',
					text: s.b1,
					'on-click': s.b1Func,
				});
				btn.addClass('text-solid');
			}
			if (s.b2) {
				var btn = $$$('button2');
				btn.setProperties({
					visible: 'visible',
					text: s.b2,
					'on-click': s.b2Func,
				});
				btn.addClass('text-line');
			}
			if (s.b3) {
				var btn = $$$('button3');
				btn.setProperties({
					visible: 'visible',
					text: s.b3,
					'on-click': s.b3Func,
				});
				btn.addClass('text-line');
			}
		} else {
			$$$('buttonLL').setProperties({ visible: 'none' });
		}

		if (s.timeout) {
			setTimeout(function () {
				if (typeof s.b1Func === 'function') {
					s.b1Func();
				}
			}, s.timeout);
		}

		// tail callback
		setTimeout(function () {
			if (typeof s.tailCallback === 'function') {
				s.tailCallback();
			}
		}, 0);
	},
});

// default event bound to dialog *********************************

Top.Controller.create('TeeSpaceLogic', {
	commonDialogOnOpenDefaultCallback: function (event, widget) {
		var s = Top.Controller.get('commonDialogLayoutLogic').settings;
		typeof s.onOpenCallback === 'function' && s.onOpenCallback(event, widget);
	},
	commonDialogOnCloseDefaultCallback: function (event, widget) {
		var s = Top.Controller.get('commonDialogLayoutLogic').settings;
		typeof s.onCloseCallback === 'function' && s.onCloseCallback(event, widget);
		Top.Controller.get('commonDialogLayoutLogic').settings = {}; // Initialize settings

		//        debugger;
		history.go(-1);
	},
	commonDialogOnOverlayClickDefaultCallback: function (event, widget) {
		console.info('OnOverlayClick');
	},
	commonDialogOnKeyDownDefaultCallback: function (event, widget) {
		var s = Top.Controller.get('commonDialogLayoutLogic').settings;
		typeof s.onKeyDownCallback === 'function' && s.onKeyDownCallback(event, widget);
	},
});

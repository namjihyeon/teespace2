function checkFolderNameSpecialChar(folderName) {
    var regex = /[\\/:*?"<>|]/g;
    return regex.test(folderName);
}

function validateFileName(newFileName) {
    try {
        if (newFileName == undefined || newFileName.trim() == "") {
            return {
                result: false,
                message: String.empty
            };
        }
        //        if(newFileName.length > MAX_FILE_NAME_LENGTH) {
        //            return {
        //                result : false,
        //                message : "파일 이름은 70자를 넘을 수 없습니다." //S("file.MSG_TD_CONTENTS_UPLOADING_HOVER_ERROR")
        //            }
        //        }
        if (checkFolderNameSpecialChar(newFileName)) {
            return {
                result: false,
                message: '특수 기호 \n\\ / : * ? " < > |는 사용할 수 없습니다.' //S("file.MSG_TD_CONTENTS_NEW_FOLDER_POPUP_MESSAGE_ERROR")
            };
        }
    } catch (ex) {
        console.log(ex);
        return {
            result: false,
            message: "Unknown Error"
        };
    }

    return {
        result: true,
        message: String.empty
    };
}

const driveOverWrite_config = {
    isFrom: undefined,
    target: undefined,
    newName: undefined,
    isFolder: undefined,

    parentFolder: undefined,
    fileId: undefined,
    fileExt: undefined,
    success: undefined,
    cancel: undefined,

    user_context_1: undefined,
    user_context_2: undefined,
    user_context_3: undefined
};

function openDriveOverWriteDialog(config) {
    var driveOverWriteLogic = Top.Controller.get("driveOverWriteLayoutLogic");
    if (config == undefined) {
        driveOverWriteLogic.config = driveOverWrite_config;
    } else {
        driveOverWriteLogic.config = config;
    }

    let overWriteDialog = Top.Dom.selectById("driveOverWriteDialog");
    overWriteDialog.open();
    overWriteDialog.adjustPosition();
}

Top.Controller.create("driveNewFolderLayoutLogic", {
    init: function(widget) {
        Top.Dom.selectById("newFolderNameTextField").setProperties({
            hint: "폴더 이름을 입력해 주세요.",
            "max-length": MAX_FILE_NAME_LENGTH
        });

        let newFolderName = "새 폴더";
        let count = 0;
        let folderExist = (Top.Dom.selectById("driveNewFolderDialog").getProperties("from") == "attachDialog")?
            driveAttach.getFileList().filter(function(e) {
                return e.is_folder == "true" && e.folder_name == newFolderName;
            }).length != 0 : 
            drive.getFileList().filter(function(e) {
                return e.is_folder == "true" && e.folder_name == newFolderName;
            }).length != 0;
            
        while (folderExist) {
            count++;
            newFolderName = "새 폴더" + " (" + count + ")";
            folderExist = (Top.Dom.selectById("driveNewFolderDialog").getProperties("from") == "attachDialog")?
                driveAttach.getFileList().filter(function (e) {
                    return e.is_folder == "true" && e.folder_name == newFolderName;
                }).length != 0 :
                drive.getFileList().filter(function (e) {
                    return e.is_folder == "true" && e.folder_name == newFolderName;
                }).length != 0;
        }

        var newFolderNameTextField = Top.Dom.selectById("newFolderNameTextField");
        newFolderNameTextField.setText(newFolderName);
        newFolderNameTextField.getElement("input#newFolderNameTextField")[0].select();

        if (newFolderNameTextField.hasClass("click-placeholder")) {
            newFolderNameTextField.removeClass("click-placeholder");
        }

        Top.Dom.selectById("driveSpecialCharacterErrorPopover").open();
        if (Top.Dom.selectById("errorIcon").hasClass("error-popover")) {
            Top.Dom.selectById("errorIcon").removeClass("error-popover");
        }
    },

    onCreate: function(event, widget) {
        this.success();
    },

    success: function() {
        let newFolderName = Top.Dom.selectById("newFolderNameTextField")
            .getText()
            .trim();
        if (!this.validate(newFolderName)) {
            return;
        }

        let fileFolder;
        if (Top.Dom.selectById("driveNewFolderDialog").getProperties("from") == "attachDialog") {
            fileFolder = driveAttach.getFileList();
        } else {
            fileFolder = drive.getFileList();
        }

        let targetFolder = [];
        for (let i = 0; i < fileFolder.length; i++) {
            if (fileFolder[i].is_folder == "true" && fileFolder[i].folder_name == newFolderName) {
                targetFolder.push(fileFolder[i].file_id);
            }
        }

        if (targetFolder.length != 0) {
            const config = {
                isFrom: "newFolderDialog",
                target: targetFolder,
                newName: newFolderName
            };
            openDriveOverWriteDialog(config);
        } else {
            //StartLoader();
            driveAttach
                .uploadFolder(newFolderName)
                .then(res => {
                    drive.renderView();
                    if (Top.Dom.selectById("driveNewFolderDialog").getProperties("from") == "attachDialog") {
                        driveAttach.renderView();
                    }
                })
                .catch(err => {
                    console.log("DRIVE CREATE FOLDER ERR : ", err);
                })
                .then(() => {
                    Top.Dom.selectById("driveNewFolderDialog").close();
                });
            //        if(response.result == "OK") {
            //           StartLoader(false);
            //        } else {
            //            ALERT("Error");
            //StartLoader(false);
            //        }
        }
    },

    onCancel: function(event, widget) {
        Top.Dom.selectById("driveNewFolderDialog").close();
    },

    newFolderNameKeyReleased: function(event, widget) {
        if (event.code == "Escape" || window.event.keyCode == 27) {
            this.onCancel();
            return;
        }

        let folderName = widget.getText();
        if (folderName.length > MAX_FILE_NAME_LENGTH) {
            let folderNameSub = folderName.substr(0, MAX_FILE_NAME_LENGTH);
            widget.setText(folderNameSub);
            folderName = folderNameSub;
        }

        if (
            this.validate(folderName) &&
            (event.key == "Enter" || window.event.keyCode == 13)
        ) {
            this.success();
        }
    },

    validate: function(newFileName) {
        let valid = validateFileName(newFileName);

        let showRedBorder = valid.result == false && valid.message != String.empty;
        if (showRedBorder && !Top.Dom.selectById("newFolderNameTextField").hasClass("error-textfield")) {
            Top.Dom.selectById("newFolderNameTextField").addClass("error-textfield");
        } else if (!showRedBorder && Top.Dom.selectById("newFolderNameTextField").hasClass("error-textfield")) {
            Top.Dom.selectById("newFolderNameTextField").removeClass("error-textfield");
        }

        if (!valid.result && !Top.Dom.selectById("successBtn").hasClass("error-button")) {
            Top.Dom.selectById("successBtn").addClass("error-button");
            Top.Dom.selectById("successBtn").setDisabled(true);
        } else if (valid.result && Top.Dom.selectById("successBtn").hasClass("error-button")) {
            Top.Dom.selectById("successBtn").removeClass("error-button");
            Top.Dom.selectById("successBtn").setDisabled(false);
        }

        let showMetaphor = valid.message != String.empty ? "visible" : "none";
        Top.Dom.selectById("errorIcon").setVisible(showMetaphor);

        if (!valid.result && Top.Dom.selectById("driveNewFolderDialog").isOpen()) {
            //Top.Dom.selectById("specialCharacterErrorPopover").open();
            if (!Top.Dom.selectById("errorIcon").hasClass("error-popover")) {
                Top.Dom.selectById("errorIcon").addClass("error-popover");
            }
        } else {
            if (Top.Dom.selectById("errorIcon").hasClass("error-popover")) {
                Top.Dom.selectById("errorIcon").removeClass("error-popover");
            }
        }

        Top.Dom.selectById("errorMessage").setText(valid.message);

        return valid.result;
    }
});

Top.Controller.create("driveRenameLayoutLogic", {
    init: function() {
        let targetFile = Top.Dom.selectById("driveFileContextMenu").getProperties("fileInfo");
        var newNameTextView = Top.Dom.selectById("renameTextView");
        var newNameTextField = Top.Dom.selectById("renameTextField");

        if (targetFile.is_folder == "false") {
            newNameTextView.setText("파일 이름");
            newNameTextField.setProperties({
                hint: "파일 이름을 입력해 주세요.",
                "max-length": MAX_FILE_NAME_LENGTH,
                text: targetFile.storageFileInfo.file_name
            });
        } else {
            newNameTextView.setText("폴더 이름");
            newNameTextField.setProperties({
                hint: "폴더 이름을 입력해 주세요.",
                "max-length": MAX_FILE_NAME_LENGTH,
                text: targetFile.folder_name
            });
        }
        Top.Dom.selectById("successBtn").addClass("error-button");
        Top.Dom.selectById("successBtn").setDisabled(true);

        newNameTextField.getElement("input#renameTextField")[0].select();

        if (newNameTextField.hasClass("click-placeholder")) {
            newNameTextField.removeClass("click-placeholder");
        }

        Top.Dom.selectById("driveSpecialCharacterErrorPopover").open();
        if (Top.Dom.selectById("errorIcon").hasClass("error-popover")) {
            Top.Dom.selectById("errorIcon").removeClass("error-popover");
        }
    },

    onSuccess: function(event, widget) {
        this.success();
    },

    success: function() {
        let targetFile = Top.Dom.selectById("driveFileContextMenu").getProperties("fileInfo");
        let newFileFolderName = Top.Dom.selectById("renameTextField")
            .getText()
            .trim();
        let fileFolder = drive.getFileList();
        let targetFileFolder = [];
        let folderCheck;

        if (targetFile.is_folder == "true") {
            for (let i = 0; i < fileFolder.length; i++) {
                if (
                    fileFolder[i].is_folder == "true" &&
                    fileFolder[i].folder_name == newFileFolderName &&
                    fileFolder[i].file_id != targetFile.file_id
                ) {
                    targetFileFolder.push(fileFolder[i].file_id);
                    folderCheck = true;
                }
            }
        } else {
            for (let i = 0; i < fileFolder.length; i++) {
                if (
                    fileFolder[i].is_folder == "false" &&
                    fileFolder[i].storageFileInfo.file_name == newFileFolderName &&
                    fileFolder[i].storageFileInfo.file_extension == targetFile.storageFileInfo.file_extension &&
                    fileFolder[i].storageFileInfo.file_id != targetFile.storageFileInfo.file_id
                ) {
                    targetFileFolder.push(fileFolder[i].storageFileInfo.file_id);
                    folderCheck = false;
                }
            }
        }

        if (targetFileFolder.length != 0) {
            const config = {
                isFrom: "renameDialog",
                target: targetFileFolder,
                newName: newFileFolderName,
                isFolder: folderCheck
            };
            openDriveOverWriteDialog(config);
        } else {
            let response;
            if (targetFile.is_folder == "false") {
                response = drive.renameFile(
                    targetFile.storageFileInfo.file_id,
                    targetFile.storageFileInfo.file_size,
                    newFileFolderName
                );
            } else {
                response = drive.renameFolder(targetFile.file_id, newFileFolderName);
            }
            drive.renderView();
            Top.Dom.selectById("driveRenameDialog").close();
        }
    },

    onFail: function(event, widget) {
        Top.Dom.selectById("driveRenameDialog").close();
    },

    renameKeyReleased: function(event, widget) {
        if (event.code == "Escape" || window.event.keyCode == 27) {
            this.onFail();
            return;
        }

        let fileName = widget.getText();
        if (fileName.length > MAX_FILE_NAME_LENGTH) {
            let fileNameSub = fileName.substr(0, MAX_FILE_NAME_LENGTH);
            widget.setText(fileNameSub);
            fileName = fileNameSub;
        }

        if (
            this.validate(fileName) &&
            (event.code == "Enter" || event.code == "NumpadEnter" || window.event.keyCode == 13)
        ) {
            this.onSuccess();
        }
    },

    validate: function(newFileName) {
        let valid = validateFileName(newFileName);

        let showRedBorder = valid.result == false && valid.message != String.empty;
        if (showRedBorder && !Top.Dom.selectById("renameTextField").hasClass("error-textfield")) {
            Top.Dom.selectById("renameTextField").addClass("error-textfield");
        } else if (!showRedBorder && Top.Dom.selectById("renameTextField").hasClass("error-textfield")) {
            Top.Dom.selectById("renameTextField").removeClass("error-textfield");
        }

        if (!valid.result && !Top.Dom.selectById("successBtn").hasClass("error-button")) {
            Top.Dom.selectById("successBtn").addClass("error-button");
            Top.Dom.selectById("successBtn").setDisabled(true);
        } else if (valid.result && Top.Dom.selectById("successBtn").hasClass("error-button")) {
            Top.Dom.selectById("successBtn").removeClass("error-button");
            Top.Dom.selectById("successBtn").setDisabled(false);
        }

        if (!valid.result && Top.Dom.selectById("driveRenameDialog").isOpen()) {
            if (!Top.Dom.selectById("errorIcon").hasClass("error-popover")) {
                Top.Dom.selectById("errorIcon").addClass("error-popover");
            }
        } else {
            if (Top.Dom.selectById("errorIcon").hasClass("error-popover")) {
                Top.Dom.selectById("errorIcon").removeClass("error-popover");
            }
        }

        let showMetaphor = valid.message != String.empty ? "visible" : "none";
        Top.Dom.selectById("errorIcon").setVisible(showMetaphor);
        Top.Dom.selectById("errorMessage").setText(valid.message);

        return valid.result;
    }
});

Top.Controller.create("driveDeleteLayoutLogic", {
    init: function() {
        let deleteMessage = Top.Dom.selectById("deleteTextView");
        let allFileFolder = 0;
        let allFile = 0;

        if (Top.Dom.selectById("driveDeleteDialog").getProperties("dialogFrom") == "button") {
            const selectedFileList = drive.getSelectedFileList();
            for (let i = 0; i < selectedFileList.length; i++) {
                if (selectedFileList[i].is_folder == "true") {
                    allFileFolder++;
                } else {
                    allFileFolder++;
                    allFile++;
                }
            }

            if (allFileFolder == allFile) {
                deleteMessage.setText("선택한 " + selectedFileList.length + "개 파일을 삭제하시겠습니까?");
            } else if (allFile == 0) {
                deleteMessage.setText("선택한 " + selectedFileList.length + "개 폴더를 삭제하시겠습니까?");
            } else {
                deleteMessage.setText("선택한 " + selectedFileList.length + "개 항목을 삭제하시겠습니까?");
            }
        } else {
            let fileInfo = Top.Dom.selectById("driveFileContextMenu").getProperties("fileInfo");
            if (fileInfo.is_folder == "true") {
                deleteMessage.setText("선택한 1개 폴더를 삭제하시겠습니까?");
            } else {
                deleteMessage.setText("선택한 1개 파일을 삭제하시겠습니까?");
            }
        }
    },

    onSuccess: function(event, widget) {
        const deleteFileList = [];

        if (Top.Dom.selectById("driveDeleteDialog").getProperties("dialogFrom") == "button") {
            const selectedFileList = drive.getSelectedFileList();
            for (let i = 0; i < selectedFileList.length; i++) {
                deleteFileList.push(selectedFileList[i]);
            }
        } else {
            deleteFileList.push(Top.Dom.selectById("driveFileContextMenu").getProperties("fileInfo"));
        }

        drive.deleteFiles(deleteFileList).then(response => {
            if (response.data.dto.result == "OK") {
                drive.renderView();
            }
        });

        Top.Dom.selectById("driveDeleteDialog").close();
    },

    onFail: function(event, widget) {
        Top.Dom.selectById("driveDeleteDialog").close();
    }
});

Top.Controller.create("driveSendFailLayoutLogic", {
    init: function() {},

    onConfirm: function(event, widget) {
        Top.Dom.selectById("driveSendFailDialog").close();
    }
});

Top.Controller.create("driveDetailedInfoLayoutLogic", {
    init: function(widget) {
        let targetFile = Top.Dom.selectById("driveFileContextMenu").getProperties("fileInfo");
        let targetFileInfo = targetFile.storageFileInfo;

        let fileName = targetFileInfo.file_name;
        let fileType = targetFileInfo.file_extension + " 파일";

        let fileLocation = "";
        const paths = drive.getPaths();

        if (paths.length < 4) {
            for (let i = 0; i < paths.length; i++) {
                fileLocation +=
                    paths[i].folder_id != "all"
                        ? paths[i].folder_name
                        : workspaceManager.getWorkspaceName(targetFile.workspace_id) != "" // 워크스페이스 이름이 없으면
                        ? workspaceManager.getWorkspaceName(targetFile.workspace_id)
                        : workspaceManager.getWorkspace(targetFile.workspace_id).User_name // User_name 없으면 (마이 스페이스 이름)
                        ? workspaceManager.getWorkspace(targetFile.workspace_id).User_name
                        : userManager.getLoginUserName() + " (나)";

                if (i != paths.length - 1) {
                    fileLocation += " > ";
                }
            }
        } else {
        	let firstLocation = paths[0].folder_id != "all"
                        ? paths[0].folder_name
                        : workspaceManager.getWorkspaceName(targetFile.workspace_id) != "" // 워크스페이스 이름이 없으면
                        ? workspaceManager.getWorkspaceName(targetFile.workspace_id)
                        : workspaceManager.getWorkspace(targetFile.workspace_id).User_name // User_name 없으면 (마이 스페이스 이름)
                        ? workspaceManager.getWorkspace(targetFile.workspace_id).User_name
                        : userManager.getLoginUserName() + " (나)";
        	       	
            fileLocation = firstLocation + " > ... > " + paths[paths.length - 1].folder_name;
        }

        let fileSize = targetFileInfo.file_size;
        let fileUploadDate = drive.getDriveDetailDateFormat(targetFileInfo.file_created_at);
        let modifiedDate = drive.getDriveDetailDateFormat(targetFileInfo.file_updated_at);
        let modifiedPerson = targetFileInfo.file_last_update_user_id;

        Top.Dom.selectById("fileName").setText(fileName);
        Top.Dom.selectById("fileType").setText(fileType);
        Top.Dom.selectById("fileLocation").setText(fileLocation);
        Top.Dom.selectById("fileSize").setText(getFileSizeText(fileSize));
        Top.Dom.selectById("firstUploadDate").setText(fileUploadDate);
        Top.Dom.selectById("modifiedDate").setText(modifiedDate);
        Top.Dom.selectById("modifiedPerson").setText(modifiedPerson);
    },

    onClose: function(event, widget) {
        Top.Dom.selectById("driveDetailedInfoDialog").close();
    }
});

Top.Controller.create("drivePassFileLayoutLogic", {
    onSuccess: function(event, widget) {
        Top.Dom.selectById("drivePassFileDialog").close();
    },

    onFail: function(event, widget) {
        Top.Dom.selectById("drivePassFileDialog").close();
    }
});

Top.Controller.create("driveOverWriteLayoutLogic", {
    config: driveOverWrite_config,

    init: function() {
        let overWriteDialog = Top.Dom.selectById("driveOverWriteDialog");
        let newName = this.config.newName;

        if (this.config.isFrom == "newFolderDialog") {
            Top.Dom.selectById("overWriteTextView").setText(
                '대상 폴더에 이름이 "' + newName + '" 인 폴더가 이미 있습니다.'
            );
        } else if (this.config.isFrom == "renameDialog") {
            if (this.config.isFolder) {
                Top.Dom.selectById("overWriteTextView").setText(
                    '대상 폴더에 이름이 "' + newName + '" 인 폴더가 이미 있습니다.'
                );
            } else {
                Top.Dom.selectById("overWriteTextView").setText(
                    '대상 폴더에 이름이 "' + newName + '" 인 파일이 이미 있습니다.'
                );
            }
        } else if (this.config.isFrom == "saveAsDialog") {
            Top.Dom.selectById("overWriteTextView").setText(
                '대상 폴더에 이름이 "' + newName + '" 인 파일이 이미 있습니다.'
            );
        } else if (this.config.isFrom == "sendDriveDialog") {
            let sendFileArr = drive.getSendFileArr();
            Top.Dom.selectById("overWriteTextView").setText(
                '대상 폴더에 이름이 "' + sendFileArr[0].fileName + '" 인 파일이 이미 있습니다.'
            );
        } else if (this.config.isFrom === "uploadDialog"){
            Top.Dom.selectById("overWriteTextView").setText(
                '대상 폴더에 이름이 "' + newName + '" 인 파일이 이미 있습니다.'
            );
        }
    },

    onSuccess: function(event, widget) {
        let overWriteDialog = Top.Dom.selectById("driveOverWriteDialog");
        let removeInfo;
        let removeFile;
        let newName;
        let removePrep;

        // 파일 정보 정의
        if (this.config.isFrom === "newFolderDialog" || this.config.isFrom === "renameDialog" || this.config.isFrom === "uploadDialog") {
            removeInfo = this.config.target;
            removeFile = [drive.findFile(removeInfo[0])];
            newName = this.config.newName;
        } else if (this.config.isFrom == "saveAsDialog") {
            removeInfo = this.config.target;
            removePrep = driveServiceCall.removeDuplicateFileFolder(
                this.config.parentFolder,
                removeInfo[0].fileName,
                removeInfo[0].fileExt
            );
            if (removePrep.result == "OK") {
                removeFile = removePrep.teeDriveDtoList[0];
            }
            newName = this.config.newName;
        } else if (this.config.isFrom == "sendDriveDialog") {
            removeInfo = drive.getSendTargetFile()[0];
            removePrep = driveServiceCall.removeDuplicateFileFolder(removeInfo.parent, removeInfo.fileName, removeInfo.fileExt);
            if (removePrep.result == "OK") {
                removeFile = removePrep.teeDriveDtoList[0];
            }
        }

        // 삭제 및 복사
        if (this.config.isFrom == "newFolderDialog") {
            drive.deleteFiles(removeFile).then(response => {
                if (response.data.dto.result == "OK") {
                    drive
                    .uploadFolder(newName)
                    .then(res => {
                        drive.renderView();
                        if (Top.Dom.selectById("driveNewFolderDialog").getProperties("from") == "attachDialog") {
                            driveAttach.renderView();
                        }
                    })
                    .catch(err => {
                        console.log("DRIVE CREATE FOLDER ERR : ", err);
                    })
                    .then(() => {
                        overWriteDialog.close();
                        Top.Dom.selectById("driveNewFolderDialog").close();
                    });
                } else {
                    console.log("makeFolder Error");
                }
            });
        } else if (this.config.isFrom == "renameDialog") {
            drive.deleteFiles(removeFile).then(response => {
                if (response.data.dto.result == "OK") {
                    let targetFile = Top.Dom.selectById("driveFileContextMenu").getProperties("fileInfo");
                    if (this.config.isFolder) {
                        drive.renameFolder(targetFile.file_id, newName);
                    } else {
                        drive.renameFile(
                            targetFile.storageFileInfo.file_id,
                            targetFile.storageFileInfo.file_size,
                            newName
                        );
                    }
                    drive.renderView();
                    overWriteDialog.close();
                    Top.Dom.selectById("driveRenameDialog").close();
                } else {
                    console.log("rename Error");
                }
            });
        } else if (this.config.isFrom == "saveAsDialog") {
            let response = driveServiceCall.deleteOneFile(removeFile.file_id, removeFile.file_parent_id);
            if (response.result == "OK") {
                TDrive_SaveAs(
                    this.config.parentFolder,
                    this.config.fileId,
                    this.config.fileExt,
                    this.config.newName,
                    this.config.user_context_1,
                    this.config.user_context_2,
                    this.config.user_context_3,
                    this.config.success,
                    this.config.cancel
                );
                overWriteDialog.close();
            } else {
                console.log("saveAs Error");
            }
        } else if (this.config.isFrom == "sendDriveDialog") {            
            let copyTarget = drive.getSendFileArr()[0];
            let response = driveServiceCall.copyFile(
                copyTarget.parentFolder,
                copyTarget.fileId,
                copyTarget.fileExt,
                copyTarget.fileName,
                copyTarget.user_context_1,
                copyTarget.user_context_2,
                copyTarget.user_context_3
            );
            if (response.result == "OK") {
                let copyTarget = drive.getSendFileArr()[0];
                let response = driveServiceCall.deleteOneFile(removeFile.file_id, removeFile.file_parent_id);

                if (response.result == "OK") {
                    if (typeof copyTarget.success == "function") {
                        copyTarget.success(response);
                    }
                } else {
                    if (typeof copyTarget.cancel == "function") {
                        copyTarget.cancel(response);
                        return;
                    }
                }

                let sendTargetFile = drive.getSendTargetFile();
                let sendFileArr = drive.getSendFileArr();

                if (sendTargetFile.length > 0 && sendFileArr.length > 0) {
                    sendTargetFile.splice(0, 1);
                    sendFileArr.splice(0, 1);
                    drive.setSendTargetFile(sendTargetFile);
                    drive.setSendFileArr(sendFileArr);
                }

                if (sendTargetFile.length == 0 && sendFileArr.length == 0) {
                    // 마지막 파일이었다면
                    Top.Dom.selectById("driveOverWriteDialog").close();
                    Top.Dom.selectById("driveAttachFileDialog").close();
                } else {
                    Top.Dom.selectById("overWriteTextView").setText(
                        '대상 폴더에 이름이 "' + sendFileArr[0].fileName + '" 인 파일이 이미 있습니다.'
                    );
                }
            } else {
                console.log("sendDrive Error");
            }
        } else if (this.config.isFrom == "uploadDialog") {
            drive.deleteFiles(removeFile).then(response => {
                if (response.data.dto.result == "OK") {
                    this.config.overWriteCallback();
                    overWriteDialog.close();
                } else {
                    console.log("uploadOverwrite Error");
                }
            }); 
        }
    },

    onFail: function(event, widget) {
        if (this.config.isFrom == "sendDriveDialog") {
            let sendTargetFile = drive.getSendTargetFile();
            let sendFileArr = drive.getSendFileArr();

            if (sendTargetFile.length > 0 && sendFileArr.length > 0) {
                drive.setSendTargetFile(sendTargetFile.splice(0, 1));
                drive.setSendFileArr(sendFileArr.splice(0, 1));
            }

            if (sendTargetFile.length == 0 && sendFileArr.length == 0) {
                // 마지막 파일이었다면
                Top.Dom.selectById("driveOverWriteDialog").close();
            } else {
                sendFileArr = drive.getSendFileArr();
                Top.Dom.selectById("overWriteTextView").setText(
                    '대상 폴더에 이름이 "' + sendFileArr[0].fileName + '" 인 파일이 이미 있습니다.'
                );
            }
        } else {
            Top.Dom.selectById("driveOverWriteDialog").close();
        }
    }
});

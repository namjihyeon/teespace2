
Top.Controller.create('TeeSpaceLogic', {
	
	drive_onOpen : function(event, widget) {
		let selectedFile = Top.Dom.selectById("driveFileContextMenu").getProperties('fileInfo');
        const data = {
            fileId: selectedFile.storageFileInfo.file_id,
            fileName: selectedFile.storageFileInfo.file_name,
            fileExtension: selectedFile.storageFileInfo.file_extension,
        }
        officeManager.callOffice(data);
	}, 
	
	drive_onDownload : function(event, widget) {
		let file = Top.Dom.selectById("driveFileContextMenu").getProperties("fileInfo");
		storageManager.DownloadFiles([file.storageFileInfo.file_id], drive.getChannelId(), drive.getWorkspaceId(), function(){}, function(){}, false);
	}, 
	
	drive_onSend_TeeMail : function(event, widget) {
		
	}, 
	
	drive_onPass : function(event, widget) {
		let passFileDialog = Top.Dom.selectById("drivePassFileDialog");
		passFileDialog.open();
		passFileDialog.adjustPosition();
	}, 
	
	drive_onDelete : function(event, widget) {
		let deleteDialog = Top.Dom.selectById("driveDeleteDialog");
		deleteDialog.setProperties({
			dialogFrom : "contextMenu"
		})
		deleteDialog.open();
		deleteDialog.adjustPosition();
	}, 
	
	drive_onRename : function(event, widget) {
		let renameDialog = Top.Dom.selectById("driveRenameDialog");
		renameDialog.open();
		renameDialog.adjustPosition();
	}, 
	
	drive_onViewProperty : function(event, widget) {				
		let detailedInfoDialog = Top.Dom.selectById("driveDetailedInfoDialog");
		detailedInfoDialog.open();
		detailedInfoDialog.adjustPosition();
	}	
});

const TeeDrive = (function() {
    let isDriveExpand = false;
    // 전체 파일 리스트
    let driveFileList = [];

    // 필터 또는 검색된 리스트
    let driveFileList_Filtered = [];

    // 현재 폴더 아이디 (todo)
    let driveCurrentFolderId = null;
    let driveChannelId = null;
    let driveWorkspaceId = null;

    // 현재 선택된 파일들
    let driveSelectedFiles = [];

    // Default Lnb 선택
    let appNumber = "1";

    // 파일 업로드용

    var _fileUploadTmpId = 0;

    // diectroty history용
    const drivePaths = [];

    // 이전 폴더 id 저장
    let drivePreviousFolderId = null;
	
	// ver2
	getFileList = function (FolderId, appNumber) {
		//let serviceUrl = String.format("SODriveFileMeta?action=List&workspaceID={0}&channelID={1}", getWorkspaceId(), getChannelId());
		let serviceUrl = _workspace.fileUrl +
			"DriveList?directoryID=" + FolderId +
			"&appNumber=" + appNumber
		//			"&directoryID=" + driveCurrentFolderId +
		//			"&time=0" +
		//			"&order=ua" +
		//			"&rownum=20";

		//			if(appType){
		//				serviceUrl += "&appNumber=" + appType;
		//			}

		return axios.get(serviceUrl);
	};

	getFilterdFileList = function (filterBy, searchText) {
		// 초기화
		let result_List = [];
		switch (filterBy) {
			case "전체":
				result_List = TeeDrive.driveFileList;
				if (searchText != null) {
					result_List = TeeDrive.driveFileList.filter(function (e) {
						if (e.is_folder == "true")
							return e.folder_name.toLowerCase().indexOf(searchText) != -1;
						else
							return e.storageFileInfo.file_name.toLowerCase().indexOf(searchText) != -1;
					});
				}
				break;
			case "사진/동영상":
				TeeDrive.driveFileList.filter(function (e) {
					fileExt = e.storageFileInfo.file_extension;
					if (fileExt == "jpeg" || fileExt == "png" || fileExt == "jpg" || fileExt == "JPEG" || fileExt == "PNG" || fileExt == "JPG") {
						result_List.push(e);
					}
				})
				if (searchText.length != 0) {
					result_List = result_List.filter(function (e) {
						if (e.is_folder == "true")
							return e.folder_name.toLowerCase().indexOf(searchText) != -1;
						else
							return e.storageFileInfo.file_name.toLowerCase().indexOf(searchText) != -1;
					});
				}
				break;
			case "오피스":
				TeeDrive.driveFileList.filter(function (e) {
					fileExt = e.storageFileInfo.file_extension;
					if (fileExt == "ppt" || fileExt == "pptx" || fileExt == "tpt"
						|| fileExt == "doc" || fileExt == "docx" || fileExt == "toc"
						|| fileExt == "xls" || fileExt == "xlsx" || fileExt == "tls" || fileExt == "csv") {
						result_List.push(e);
					}
				})
				if (searchText.length != 0) {
					result_List = result_List.filter(function (e) {
						if (e.is_folder == "true")
							return e.folder_name.toLowerCase().indexOf(searchText) != -1;
						else
							return e.storageFileInfo.file_name.toLowerCase().indexOf(searchText) != -1;
					});
				}
				break;
			case "기타 파일":
				TeeDrive.driveFileList.filter(function (e) {
					fileExt = e.storageFileInfo.file_extension;
					if (!(fileExt == "jpeg" || fileExt == "png" || fileExt == "jpg" || fileExt == "JPEG" || fileExt == "PNG" || fileExt == "JPG")
						&& !(fileExt == "ppt" || fileExt == "pptx" || fileExt == "tpt"
							|| fileExt == "doc" || fileExt == "docx" || fileExt == "toc"
							|| fileExt == "xls" || fileExt == "xlsx" || fileExt == "tls" || fileExt == "csv")) {
						result_List.push(e);
					}
				})
				if (searchText.length != 0) {
					result_List = result_List.filter(function (e) {
						if (e.is_folder == "true")
							return e.folder_name.toLowerCase().indexOf(searchText) != -1;
						else
							return e.storageFileInfo.file_name.toLowerCase().indexOf(searchText) != -1;
					});
				}
				break;
		}
		return result_List;
	};

	//	uploadFiles = function (files, appNumber) {
	//		let serviceUrl = _workspace.fileUrl + "DriveFileMeta";
	//		let userId = getMyInfo().userId;
	//		
	//		let inputDTO = [];
	//		for (let i = 0; i < files.length; i++) {
	//			const split_arr = files[i].name.split('.');
	//			const file_ext = split_arr[split_arr.length - 1];
	//
	//			split_arr.splice(split_arr.length - 1, 1);
	//			const file_name = split_arr.join('.');
	//
	//			inputDTO.push({
	//				"user_id": userId,
	//				"workspace_id": getWorkspaceId(),
	//				"channel_id": getChannelId(),
	//				"file_parent_id": getCurrentFolderId(),
	//				//"is_folder": "false",
	//				"app_number": appNumber,
	//				
	//				"storageFileInfo": {
	//					"user_id": userId, //"3b8ee905-64a5-49d6-b98a-d61fc8e655b4",
	//					"file_last_update_user_id": userId, //"3b8ee905-64a5-49d6-b98a-d61fc8e655b4",
	//					"file_name": file_name,
	//					"file_extension": file_ext,
	//					"file_size": files[i].size,
	//					//"user_context_1": 0,
	//					//"user_context_2": "false"
	//				}
	//			})
	//		}
	//
	//		return axios.post(serviceUrl, {
	//			"dto": {
	//				"teeDriveDtoList": inputDTO
	//			}
	//		});
	//	};

	deleteFiles = function (files) {
		let serviceUrl = _workspace.fileUrl + "DriveFile?action=Delete";

		let inputDTO = [];
		//		for (let i = 0; i < TeeDrive.driveSelectedFiles.length; i++) {
		//			const fileInfo = findFile(TeeDrive.driveSelectedFiles[i]);
		//			if (fileInfo) {
		//				inputDTO.push({
		//					"workspace_id": getWorkspaceId(),
		//					"channel_id": getChannelId(),
		//					"file_id": TeeDrive.driveSelectedFiles[i],
		//					"file_parent_id": TeeDrive.driveCurrentFolderId,
		//					"is_folder": fileInfo.is_folder == "true" ? "true" : "false",
		//				})
		//			}
		//		}

		for (let i = 0; i < files.length; i++) {
			//const fileInfo = findFile(files[i]);
			if (files[i]) {
				inputDTO.push({
					"workspace_id": getWorkspaceId(),
					"ch_id": getChannelId(),
					"file_id": files[i].is_folder == "false" ? files[i].storageFileInfo.file_id : files[i].file_id,
					"file_parent_id": TeeDrive.driveCurrentFolderId,
					"is_folder": files[i].is_folder //Info.is_folder == "true" ? "true" : "false",
				})
			}
		}

		return axios.post(serviceUrl, {
			"dto": {
				"user_id": userManager.getLoginUserId(),
				"teeDriveDtoList": inputDTO,
			}
		});
	};

	renderView = function (ParentFolderId, AppNumber, FileList) {
        // 버튼 초기화
        changeButtons(false, false);		
        
        TeeDrive.setCurrentFolderId(ParentFolderId);

        // container 초기화
        const container = document.querySelector("div#driveContentsMainLayout");
        if(!container)
            return;

        while(container.hasChildNodes()){
            container.removeChild(container.firstChild);
        }

        // fragment 생성
        const frag = document.createDocumentFragment();

        // 직전 폴더가기
        if(getCurrentFolderId() !== getChannelId()){
            frag.appendChild(previousFolderTemplate());
        }

        // filter 아님
		if (!FileList) {
            
            // 서버에서 받아옴
			TeeDrive.getFileList(ParentFolderId, AppNumber).then(function (res) {
				TeeDrive.driveFileList = res.data.dto.teeDriveDtoList;

				for (let i = 0; i < TeeDrive.driveFileList.length; i++) {
					frag.appendChild(createFileTemplate(TeeDrive.driveFileList[i]));
				}

                container.appendChild(frag.cloneNode(true));
                updateFileTemplate(Top.Controller.get('driveContentsLogic').isGridView);

                // bread crumb 업데이트
                $("div#driveSpaceUpperLayout").html(createBreadCrumb());
            })
            .catch(function(){
                console.log("ERROR : DRIVE LIST");
            })
            .then(function(){
               
            });
        } 
        
         // filter
        else {
			for (let i = 0; i < FileList.length; i++) {
                frag.appendChild(createFileTemplate(FileList[i]));
			}

            container.appendChild(frag.cloneNode(true));
            updateFileTemplate(Top.Controller.get('driveContentsLogic').isGridView);
		}        
	};

	getWorkspaceId = function () {
		return driveWorkspaceId; //workspaceManager.getWorkspaceId(getRoomIdByUrl(), "CHN0006");
	};


	getChannelId = function () {
		return driveChannelId; // workspaceManager.getChannelList(getRoomIdByUrl(), "CHN0006");
	};

	getCurrentFolderId = function () {
		return driveCurrentFolderId;
	};

	
	getPreviousFolderId = function() {
		return drivePreviousFolderId;
	};

	setWorkspaceId = function (value) {
		driveWorkspaceId = value;
	};

	setChannelId = function (value) {
		driveChannelId = value;
	};

	setCurrentFolderId = function (value) {
		driveCurrentFolderId = value;
	};
	
	setPreviousFolderId = function(value) {
		drivePreviousFolderId = value;
	}
	
	getMyInfo = function () {
		return userManager.getLoginUserInfo();
	};

	findFile = (fileID) => {
		for (let i = 0; i < TeeDrive.driveFileList.length; i++) {
			if (TeeDrive.driveFileList[i].is_folder == "true") {
				if (TeeDrive.driveFileList[i].file_id == fileID) {
					return TeeDrive.driveFileList[i];
				}
			} else {
				if (TeeDrive.driveFileList[i].storageFileInfo.file_id == fileID) {
					return TeeDrive.driveFileList[i];
				}
			}

		}
		return null;
	};

    driveUploadFileDTO = function(fileName, fileExt, fileSize, tmpId) {
        if (String.isNullOrEmpty(fileName) || String.isNullOrEmpty(fileExt)) {
            return undefined;
        }
        if (tmpId == undefined || tmpId < 0) {
            return undefined;
        }
        //        if((file.size >= MAX_UPLOAD_FILE_SIZE_MB * 1024 * 1024) || file.size <= 0) {
        //            return undefined;
        //        }

        let loginUserId = userManager.getLoginUserId();
        let loginUserName = userManager.getLoginUserName();
        return {
            dto: {
                user_id: loginUserId,
                workspace_id: getWorkspaceId(),
                ch_id: getChannelId(),
                file_parent_id: getCurrentFolderId(),
                app_number: TeeDrive.appNumber,
                storageFileInfo: {
                    user_id: loginUserId,
                    file_last_update_user_id: loginUserId,
                    file_name: fileName,
                    file_extension: fileExt,
                    file_size: fileSize,
                    user_context_1: tmpId,
                    user_context_2: false.toString()
                }
            }
        };
    };

    driveUploadFolderDTO = function(folderName) {
        if (String.isNullOrEmpty(folderName)) {
            return undefined;
        }

        return {
            dto: {
                user_id: userManager.getLoginUserId(),
                ch_id: getChannelId(),
                file_parent_id: getCurrentFolderId(),
                //is_folder        : "true",
                folder_name: folderName
                //app_number       : 6,
                //storageFileInfo  : {},
            }
        };
    };

    driveFileRenameDTO = function(targetFileId, targetFileSize, newFileName) {
        if (String.isNullOrEmpty(newFileName)) {
            return undefined;
        }

        return {
            dto: {
                file_parent_id: getCurrentFolderId(),
                user_id: userManager.getLoginUserId(),
                app_number: TeeDrive.appNumber,
                teeDriveDtoList: [
                    {
                        workspace_id: getWorkspaceId(),
                        ch_id: getChannelId(),
                        //file_parant_id  : getCurrentFolderId(),
                        is_folder: "false",
                        storageFileInfo: {
                            user_id: userManager.getLoginUserId(),
                            file_last_update_user_id: userManager.getLoginUserId(),
                            file_id: targetFileId,
                            file_name: newFileName,
                            file_size: targetFileSize
                            //user_context_1           : 0,
                            //user_context_2           : "false",
                        }
                    }
                ]
            }
        };
    };

    driveFolderRenameDTO = function(targetFileId, newFolderName) {
        if (String.isNullOrEmpty(newFolderName)) {
            return undefined;
        }

        return {
            dto: {
                file_parent_id: getCurrentFolderId(),
                user_id: userManager.getLoginUserId(),
                app_number: TeeDrive.appNumber,
                teeDriveDtoList: [
                    {
                        file_parant_id: getCurrentFolderId(),
                        is_folder: "true",
                        folder_name: newFolderName,
                        file_id: targetFileId
                    }
                ]
            }
        };
    };

    driveStorageUpdateDTO = function(fileId, userContext1, userContext2) {
        if (String.isNullOrEmpty(fileId)) {
            return undefined;
        }

        return {
            dto: {
                workspace_id: getWorkspaceId(),
                channel_id: getChannelId(),
                user_id: userManager.getLoginUserId(),
                storageFileInfo: {
                    file_id: fileId,
                    //file_last_update_user_id : userManager.getLoginUserId(),
                    user_id: userManager.getLoginUserId(),
                    user_context_1: userContext1,
                    user_context_2: userContext2
                }
            }
        };
    };

    //    driveDeleteDTO = function(fileFolderArr, fileName, fileExt) {
    //    	if(!Array.isArray(fileIDs)) {
    //            return undefined;
    //        }
    //
    //        let file_ids = [];
    //        for(var i=0; i<fileIDs.length; i++) {
    //            file_ids.push({ file_id : fileIDs[i] })
    //        }
    //
    //        return {
    //            dto : {
    //                user_id      	: userManager.getLoginUserId(),
    //                workspace_id 	: getWorkspaceId(),
    //                channel_id   	: getChannelId(),
    //                file_ids     	: file_ids,
    //                file_name	  	: fileName,
    //                file_extension	: fileExt
    //            }
    //        }
    //    };

    _ajaxCall = function(serviceUrl, type, inputDTO) {
        if (type.toUpperCase() == "GET") {
            serviceUrl += "&rn=" + Date.now();
        }

        let outputData = null;
        try {
            Top.Ajax.execute({
                url: String.format("{0}{1}", _workspace.fileUrl, serviceUrl),
                type: type,
                dataType: "json",
                async: false,
                cache: false,
                data: inputDTO,
                contentType: "application/json",
                crossDomain: true,
                xhrFields: {
                    withCredentials: true
                },
                success: function(data) {
                    //                    try {
                    //                        if(typeof data == "string") {
                    //                            data = JSON.parse(data);
                    //                        }
                    //
                    //                        outputData = data.dto;
                    //                        if(outputData.result != undefined) {
                    //                            outputData.result = outputData.result.toUpperCase();
                    //                        }
                    //                    } catch(error) {
                    //                        outputData = {
                    //                            result : "FAIL",
                    //                            message : "UnknownError.(CORS?)... JSON.stringify(error) : " + JSON.stringify(error),
                    //                            data : error
                    //                        }
                    //                    }
                },
                error: function(data) {
                    //                    outputData = {
                    //                        result : "FAIL",
                    //                        message : data.exception != undefined ? data.exception.message : "Error",
                    //                        data : data
                    //                    }
                    //                    if(data.exception != undefined) {
                    //                        console.log(data.exception.message);
                    //                    }
                }
            });
        } catch (ex) {
            //StartLoader(false);
        }

        return outputData;
    };

    _onBeforeLoadFiles = function(files) {
        let newFileList = [];
        let file = {};
        let checkFileSize = {};
        let nomFileFullName = "";
        let fileNameArray = [];
        let fileExt = "";
        for (var i = 0; i < files.length; i++) {
            file = files[i];

            nomFileFullName = Top.Util.Browser.isIE() ? file.name : file.name.normalize("NFC");
            fileNameArray = nomFileFullName.split(".");
            fileExt = fileNameArray.length == 1 ? "" : fileNameArray[fileNameArray.length - 1];
            if (fileExt == "") {
                return {
                    result: false,
                    message: "파일 확장자가 존재하지 않습니다."
                };
            }

            let fileName = nomFileFullName.substring(0, nomFileFullName.lastIndexOf(fileExt) - 1);
            if (fileName.length > MAX_FILE_NAME_LENGTH) {
                return {
                    result: false,
                    message: "파일명이 70자를 초과하면 업로드 할 수 없습니다." // 70자 제한
                };
            }

            newFileList.push({
                file_id: _fileUploadTmpId++,
                iconClassName: getFileIconClass(fileExt),
                file_extension: fileExt,
                fileFullName: nomFileFullName,
                file_status: FileStatus.TRY_UPLOAD,
                is_file: true
            });
        }

        return {
            result: true,
            newFileList: newFileList
        };
    };

    uploadFiles = function(files) {
        let beforeCheck = _onBeforeLoadFiles(files);
        if (!beforeCheck.result) {
            //ALERT(beforeCheck.message);
            return;
        }
        
        //Top.Controller.get("driveUploadStatusLayoutLogic").initUploadStatus(beforeCheck.newFileList);
        
        var inputDTO = {};
        let file = {};
        let nomFileFullName = String.empty;
        let fileNameArray = [];
        let fileName = String.empty;
        let fileExt = String.empty;
        let fileParentId = TeeDrive.getCurrentFolderId();

        let tmpId = -1;
        let _successCallback = function(res) {
            let tmpId = res.dto.storageFileInfoList[0].user_context_1;
            let realFileId = res.dto.storageFileInfoList[0].file_id;
            //Top.Controller.get("driveUploadStatusLayoutLogic").uploadSuccess(tmpId, realFileId);
            
            let storageFileUpdateUrl = String.format("{0}Storage/StorageMeta", _workspace.url);
            let storageFileUpdateDto = driveStorageUpdateDTO(realFileId, tmpId, true.toString());
            Top.Ajax.put(storageFileUpdateUrl, storageFileUpdateDto, undefined, function() {}); //renderView(TeeDrive.getCurrentFolderId(), TeeDrive.appNumber);
            renderView(TeeDrive.getCurrentFolderId(), TeeDrive.appNumber);
        };
        let _errorCallback = function(res) {
            let tmpId = res.dto.storageFileInfoList[0].user_context_1;
            //Top.Controller.get("driveUploadStatusLayoutLogic").uploadFail(tmpId);
        };
        for (var i = 0; i < beforeCheck.newFileList.length; i++) {
            file = files[i];
            nomFileFullName = Top.Util.Browser.isIE() ? file.name : file.name.normalize("NFC");
            fileNameArray = nomFileFullName.split(".");
            fileExt = fileNameArray[fileNameArray.length - 1];
            fileName = String.isNullOrEmpty(fileExt) ? nomFileFullName : nomFileFullName.substring(0, nomFileFullName.lastIndexOf(fileExt) - 1);
            tmpId = beforeCheck.newFileList[i].file_id;
            inputDTO = driveUploadFileDTO(fileName, fileExt, file.size, tmpId);
            storageManager.UploadFile(file, inputDTO, "File", "DriveFile", _successCallback, _errorCallback);
        }
    };

    uploadFolder = function(folderName) {
        return _ajaxCall("DriveFolderMeta", "POST", driveUploadFolderDTO(folderName));
    };

    renameFile = function(targetFileId, targetFileSize, newFileName) {
        let inputDTO = driveFileRenameDTO(targetFileId, targetFileSize, newFileName);
        return _ajaxCall("DriveMeta", "PUT", inputDTO);
    };

    renameFolder = function(targetFileId, newFolderName) {
        let inputDTO = driveFolderRenameDTO(targetFileId, newFolderName);
        return _ajaxCall("DriveMeta", "PUT", inputDTO);
    };
    
    isIOS = function() { // 뒷 부분 IOS 버전 13 이상?
        return (/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream) || (navigator.platform === 'MacIntel' && navigator.maxTouchPoints > 1);
    }


    isSafari = function() {
        return Top.Util.Browser.isSafari();
    };

    isOfficeFile = function(file_extension) {
        if (String.isNullOrEmpty(file_extension)) {
            return false;
        }

        file_extension = file_extension.toLowerCase();

        return (
            file_extension == "ppt" ||
            file_extension == "pptx" ||
            file_extension == "tpt" ||
            file_extension == "doc" ||
            file_extension == "docx" ||
            file_extension == "toc" ||
            file_extension == "xls" ||
            file_extension == "xlsx" ||
            file_extension == "tls" ||
            file_extension == "csv"
        );
    };

    // 필수 구현
    onDragStart = function(e, data) {
        // background template
        const element = document.createElement("div");
        Object.assign(element.style, {
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            width: "100px",
            height: "100px",
            backgroundColor: "pink"
        });
        element.innerText = "FILE!";

        // background set
        dnd.setBackground(element);

        // default modal
        dnd.useDefaultEffect(true);

        // data set
        dnd.setData({ "1": 1, "2": 2, "3": 3 });
    };

    // 필수 구현
    onDrop = function(e, dropData) {
        const { fromApp, data, files } = dropData;
        switch (fromApp) {
            // fropApp 에 따른 처리 (타앱 -> drive)
            // 타 앱에서 이미 업로드된 파일이므로, file id 만 알면 복사 가능할 듯.
            case "talk":
                break;

            case "note":
                break;

            case "mail":
                break;

            case "meeting":
                break;

            case "office":
                break;

            case "schedule":
                break;

            // file 업로드에 대한 처리 (바탕화면 -> drive)
            default:
                if (files && files.length) {
                    TeeDrive.uploadFiles(files, TeeDrive.appNumber);
                }
                break;
        }
    };

    onDragOver = function(e, data) {
        // 드래그 배경 이미지가 영역에 따라 변화해야 할 경우 구현.
    };

    onDragEnter = function(e, data) {
        // 기본 modal 사용 안할경우 구현.
    };

    onDragLeave = function(e, data) {
        // 기본 modal 사용 안할경우 구현.
    };

    return {
        // variable
        isDriveExpand,
        driveFileList,
        driveFileList_Filtered,
        driveSelectedFiles,
        appNumber,
        drivePaths,

        // method
        getFileList,
        getFilterdFileList,
        uploadFiles,
        deleteFiles,
        renderView,

        driveUploadFileDTO,
        driveStorageUpdateDTO,

        driveUploadFolderDTO,
        uploadFolder,

        driveFileRenameDTO,
        renameFile,

        driveFolderRenameDTO,
        renameFolder,

        getWorkspaceId,
        getChannelId,
        getCurrentFolderId,
        getPreviousFolderId,
        setWorkspaceId,
        setChannelId,
        setCurrentFolderId,
        setPreviousFolderId,

        isIOS,
        isSafari,
        isOfficeFile,

        // drag and drop handlers
        onDragStart,
        onDrop,
        onDragOver,
        onDragEnter,
        onDragLeave
    };
})();

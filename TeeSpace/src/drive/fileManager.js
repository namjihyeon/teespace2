function createFileChannel() {
    let result = ajax_call("FileUUID", "", "", "GET", null);
    return result.file_uuid;
}

function clearFileManagerChannelId() {
    fileManager.fileChannelId = "";
}

function setFileManagerChannelId(fileChannelId) {
    fileManager.fileChannelId = fileChannelId;
    fileManager.checkCapacity = true;
}

function disabledCapacityCheck() {
    fileManager.checkCapacity = false;
}

const fileManager = {
    fileChannelId : "",
    checkCapacity : true,
    fileNotiMessage : [
    	"새로운 파일이 등록되었습니다.", 

    	" 파일이 영구 삭제 되었습니다.", 
    	" 폴더가 영구 삭제 되었습니다.",

    	" 파일이 휴지통으로 이동하였습니다.", 
    	" 폴더가 휴지통으로 이동하였습니다.",

    	" 파일이 복원되었습니다.", 
    	" 폴더가 복원되었습니다.",
    	
    	" 파일이 이동되었습니다.",
    	" 폴더가 이동되었습니다.",
    	
    	" 파일이 수정되었습니다.",
    	" 폴더가 수정되었습니다."
    	],

    
	renderChannel : function(targetLayoutId) {
		let layout = Top.Dom.selectById(targetLayoutId);
		if(layout) {
			layout.src('./fileLayout.html', function() {
                Top.Dom.selectById('fileTitleLayout').src('./fileTitleLayout.html');
                Top.Dom.selectById('fileContentsTitleLayout').src('./fileContentsTitleLayout.html');
                Top.Dom.selectById('fileContentsMainLayout').src('./fileContentsTableLayout.html');
                Top.Dom.selectById('fileUploadStatusLayout').src('./fileUploadStatusLayout.html');
                Top.Dom.selectById('fileLnbLayout').src('./fileLnbLayout.html', function() {
                    setTimeout(function() { Top.Controller.get("fileLnbLayoutLogic").loaded(); }, 1);
                });
			});
		}
	},	
	
	getRecentFileList : function(file_extension, num_of_file) {
	    if(file_extension == undefined) {
	        file_extension = "";
	    }
	    if(num_of_file == undefined) {
	        num_of_file = 10;
	    }
	    
		let parameter = "&is_file=1" 
			          + "&file_channel_id=" + this.fileChannelId
					  + "&num_of_file=" + num_of_file;
		if(file_extension != "" && file_extension != undefined) {
			parameter += "&file_extension=" + file_extension
		}
		
		let response = ajax_call("FileMeta", "?action=List", parameter, "GET", null);		
		return response;
	},
	
	noti_file : function(event){
		
	},
	
	deleteFile : function(targetFileIds) {
        let inputData = {
            dto : {
                dtoFileMetaList : [],
                dtoFileMeta : {
                    file_last_update_user_id : userManager.getLoginUserId(),
                    file_channel_id : TDrive.getChannelId()
                }
            }
        }

        if(Array.isArray(targetFileIds)) {
            for(var i=0; i<targetFileIds.length; i++) {
                inputData.dto.dtoFileMetaList.push({ file_id : targetFileIds[i] });
            }
        } else {
            inputData.dto.dtoFileMetaList.push({ file_id : targetFileIds });
        }
        
        let parameter = String.format("?action=Erase&checkCapacity={0}", this.checkCapacity);
        return ajax_call("File", parameter, "", "POST", inputData)
	},
	
	onBeforeLoadFile : function(file) {
        if(file.size >= MAX_UPLOAD_FILE_SIZE_MB * 1024 * 1024) {
            let fileChooser = document.getElementById("MainTopApplicationPage_fileChooser");
            if(fileChooser != null) {
                fileChooser.remove();    
            }   
            return {
                result : false,
                message : MAX_UPLOAD_FILE_SIZE_MB + "MB 이상 파일은 업로드할 수 없습니다."
            }
        } else if(file.size == 0) {
            let fileChooser = document.getElementById("MainTopApplicationPage_fileChooser");
            if(fileChooser != null) {
                fileChooser.remove();    
            }
            return {
                result : false,
                message : "크기가 0인 파일은 올릴 수 없습니다."
            };
        }
        
        return { result : true };
    },
    
    saveTDriveState : function(state) {
        let tdriveState = {};
        if(state == undefined) {
            let tableId = nodeId == "recycleBin" ? "fileRecycleBinTableView" : "fileContentsTableView";
            let tablePageInfo = Top.Dom.selectById(tableId).getPageInfo();
            tdriveState = {
                selectedFolderId : TDrive.getCurrentFolderId(),
                searchedText     : Top.Dom.selectById("fileSearchTextField").getText(),
                pageNum          : tablePageInfo.page,
                rowPerPage       : tablePageInfo.length
            }
        } else {
            if(sessionStorage.hasOwnProperty("TDriveState")) {
                try {
                    tdriveState = JSON.parse(sessionStorage.TDriveState);
                } catch (ex) {
                    tdriveState = {
                        selectedFolderId : "root",
                        searchedText     : String.empty,
                        pageNum          : 1,
                        rowPerPage       : 20
                    }; 
                }
            } else {
                tdriveState = {
                    selectedFolderId : "root",
                    searchedText     : String.empty,
                    pageNum          : 1,
                    rowPerPage       : 20
                };
            } 

            if(state.hasOwnProperty("selectedFolderId")) { tdriveState.selectedFolderId = state.selectedFolderId; }
            if(state.hasOwnProperty("searchedText"))     { tdriveState.searchedText     = state.searchedText; }
            if(state.hasOwnProperty("pageNum"))          { tdriveState.pageNum          = state.selectedFolderId; }
            if(state.hasOwnProperty("rowPerPage"))       { tdriveState.rowPerPage       = state.rowPerPage; }
        }   

        sessionStorage.setItem("TDriveState", JSON.stringify(tdriveState));
    },
}




function file_ajax_call(serviceUrl, type, inputdata, successCallback, errorCallback){

    let outputdata = null;
    let tmpId = -1;
    try {
        Top.Ajax.execute({
            url : storageManager.GetFileUrl("633982e7-3907-4921-b7e8-770e2c7a04c1", TDrive.getChannelId()),
            type : type,
            //dataType : "json",
            async : true,
            cache : false,
            data : inputdata,
            contentType : false,//"application/json",
            crossDomain : true,
            xhrFields : {
                withCredentials: true
            },
            headers: {
                "ProObjectWebFileTransfer":"true"
            },
            success : function(data) {          
                if(successCallback != undefined && (typeof successCallback == "function")) {
                    try {                        
                        if(data.exception != undefined) {
                            if(data.exception.name == "DownloadFileLimitExceedException") {
                                outputdata = {
                                    result : "FAIL",
                                    message : data.exception.message
                                };
                            }
                            successCallback(outputdata);
                            return;
                        }
                        
                        outputdata = data
                    } catch(error) {
                        outputdata = {
                            result : "FAIL",
                            message : "UnknownError.(CORS?)... JSON.stringify(error) : " + JSON.stringify(error),
                            data : error
                        }
                    }
                    
                    successCallback(outputdata, tmpId);    
                }
            },
            error : function(data) {               
                if(errorCallback != undefined && (typeof errorCallback == "function")) {
                    outputdata = {
                        result : "FAIL",
                        message : data.exception != undefined ? data.exception.message : "Error",
                        data : data
                    }     
                    
                    errorCallback(outputdata, tmpId); 
                }
            }
        });
    } catch(ex) {
        console.log(ex);
        outputData = {
            result : "FAIL",
            message : "CORS"
        }
        errorCallback(outputdata, tmpId);
    }
    return outputdata;
}
function isBase64Encoded(fileExt) {
    return fileExt.toLowerCase() == "txt" || fileExt.toLowerCase() == "csv" ? false : true;
}
function getDownloadFileInfo(targetFileId, successCallback, errorCallback) {   
    var _targetFileId = targetFileId;
    var _successCallback = function(response) {
        let fileExt = "jpg";
        let data = atob(response);
        let result = "";
        if(isBase64Encoded(fileExt)) {
            let bytes = new Uint8Array(data.length);
            for(var i=0 ; i<data.length ; i++) {
                bytes[i] = data.charCodeAt(i);
            }
            
            result = bytes;
        } else {
            result = decodeURIComponent(data);
        }
        
        switch(fileExt.toLowerCase()) {
            case "csv":    case "txt":
                result = "\ufeff" + result;
                break;
        }
        
        let contentType = getContentTypeByFileExtension(fileExt);
        let blob = new Blob([result], { type: contentType });
        let returnVal = {
            result : "OK",
            url: window.URL.createObjectURL(blob),
            fileName : response.files[0].filename,
            blobData : blob
        }
        
        successCallback(returnVal);             
    };
    
    var _errorCallback = function(data) {
        if(errorCallback != undefined && (typeof errorCallback == "function")) {
            data.fileMetaList.push({ "file_id" : _targetFileId })
            errorCallback(data);
        }
    };
    
    let parameter = String.format("?file_id={0}", _targetFileId);
    let response = file_ajax_call("", "GET", null, _successCallback, _errorCallback);
}

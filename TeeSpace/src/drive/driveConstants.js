const FileStatus = Object.freeze({
    NORMAL    : 0,
    REMOVED   : 1,
    FAVORITED : 2,
    
    TRY_UPLOAD     : 11,
    UPLOAD_SUCCESS : 12,
    UPLOAD_FAIL    : 13,
});

const EM_DASH                   = '—'

const MAX_FILE_NAME_LENGTH        = 70;
const MAX_UPLOAD_FILE_SIZE_MB     = 100;
const DOWNLOAD_FILE_ZIP_THRESHOLD = 10;

String.empty = "";
String.isNullOrEmpty = function(str) {
    return str == null || str == undefined || (typeof str == "string" && str.trim() == String.empty);
};
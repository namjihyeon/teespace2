// Copyright (C) <2020> TmaxA&C
// Version 1.0.200407

"use strict";

var GridView = function (container) {
  const that = {};
  const self = {};

  self.container_ = {
    object: container,
    width: container.offsetWidth,
    height: container.offsetHeight,
  };

  function gridViewRenderer(subscriptionInfo) {
    self.subscriptionInfo_ = subscriptionInfo;
    const stream = self.subscriptionInfo_.stream;

    const element_div = createVideoRegion(stream);
    subscriptionInfo.container.object = element_div;

    const element_video = document.createElement("video");
    element_video.text = "this browser does not supported video tag";
    element_video.id = stream.id;
    element_video.autoplay = true;
    element_video.controls = false;
    element_video.style.display = "block";
    element_video.className = "tm-gridview-remote-view-renderer";
    element_video.srcObject = stream.mediaStream;

    element_video.style.marginLeft = "auto";
    element_video.style.marginRight = "auto";

    element_video.style.width = self.container_.width;

    element_video.style.backgroundColor = "transparent";

    element_div.appendChild(element_video);

    subscriptionInfo.container.player.push(element_video);
  }

  function gridViewResolutionSelector(resolutions) {
    let target_resolution = null;
    for (const resolution of resolutions) {
      if (target_resolution === null) {
        target_resolution = resolution;
        continue;
      }

      if (
        resolution.width <= self.container_.width &&
        resolution.width >= target_resolution.width &&
        resolution.height <= self.container_.height &&
        resolution.height >= target_resolution.height
      ) {
        target_resolution = resolution;
      }
    }
    return target_resolution;
  }

  function createVideoRegion(stream) {
    const element_div = document.createElement("div");
    element_div.id = `${stream.id}resolutions`;
    element_div.className = "tm-gridview-remote-view";

    self.container_.object.appendChild(element_div);
    return element_div;
  }

  that.APICall = {
    selectResolution: gridViewResolutionSelector,
    render: gridViewRenderer,
  };

  return that;
};

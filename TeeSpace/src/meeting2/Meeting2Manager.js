// LifeCycle 관리와 내부 API 건드리지 않도록 만들었다.
const MEETING_VIEW_TYPE = {
    SIDE_BAR: 0,
    EXPENDED: 1
};
const Meeting2Manager = ( () => {

    // 추후 sidebar 전체로 바뀔 예정, 현재는 video Div를 넘겨준다.
    const meetingDiv = "meeting_layout";

    // 제공 받은 API들.
    let TMAX_OWT_API = null;
    let curretViewIndex = 0;


    const MEETING_VIEW_TYPE_ARRAY = Object.values( MEETING_VIEW_TYPE );
    const MEETING_VIEW_TYPE_LENGTH = MEETING_VIEW_TYPE_ARRAY.length;
    let IS_MEETING_CONNECTION = false;

    const start = ( roomId, viewType ) => {
        stop();
        spaceAPI.spaceInit();
        const meeting_div = document.getElementById( meetingDiv );
        if ( meeting_div ) {
            let url;
            let u = new URL(document.URL);
            if( u.origin.includes("localhost") ){
                url = "https://csdev.tmaxcloudspace.com/OWT";
              }else{
                url = u.origin +"/OWT"; // Get the string before last '/'.
              }
            TMAX_OWT_API = TeeMeetingAPI( meeting_div ,url).APICall;
        } else {
            console.info( `Info : can't find ${meeting_div} element ( init Fail )` );
        }

        // 작은 화면이 나오는게 맞지만 일단 크게 해놓자.. ( OWT가 작은건 너무 작다 )
        TMAX_OWT_API.startMeeting( roomId, () => {
            changeView( viewType );
            IS_MEETING_CONNECTION = true;
        } );
    };

    const stop = () => {
        if ( TMAX_OWT_API ) {
            TMAX_OWT_API.endMeeting();
            TMAX_OWT_API = null;
            IS_MEETING_CONNECTION = false;
            console.log( "Info : clear before MeetingRoom" );

        }
        // OWT API 를 사용해도 localStream 종료 하지 않는다.
        // 강제로 Stop 시켜준다. ㅠㅠ
        //stopLocalStreamTemporary( "local_video" );
    };

    // 이걸 왜 안짜준거지... ㅠㅠ
    const stopLocalStreamTemporary = ( targetVideoDivString ) => {
        const remainLocalVideo = document.getElementById( targetVideoDivString );
        if ( remainLocalVideo ) {
            let stream = remainLocalVideo.srcObject;
            if ( stream ) {
                let tracks = stream.getTracks();

                tracks.forEach( function ( track ) {
                    track.stop();
                } );
                remainLocalVideo.srcObject = null;
                console.info("clear local Stream track ");
            }
        }
    };

    const changeView = ( viewType = MEETING_VIEW_TYPE.EXPENDED ) => {

        if ( TMAX_OWT_API ) {
            if ( !viewType && !MEETING_VIEW_TYPE_ARRAY.includes( viewType ) ) {
                viewType = MEETING_VIEW_TYPE.EXPENDED;
            }
            curretViewIndex = MEETING_VIEW_TYPE_ARRAY.indexOf( viewType );
            TMAX_OWT_API.changeView( viewType );
        }
    };

    const toggleMeetingView = () => {
        if ( TMAX_OWT_API ) {
            const nextViewIndex = increaseNumberWithOverflow( curretViewIndex, MEETING_VIEW_TYPE_LENGTH );
            changeView( MEETING_VIEW_TYPE_ARRAY[ nextViewIndex ] );
        }
    };

    const increaseNumberWithOverflow = ( value, length ) => {
        value++;
        if ( value >= length ) {
            value = 0;
        }
        return value;
    };

    const render = () => {
        document.querySelector("div#AppSplitRightLayout").innerHTML = 
        `<div id='meetingMain' class='meeting2Header' >
            <div class='meeting2Col'>
            </div>
            <div class='meeting2Col'>
                <span id='meeting2ExpandBtn' class='icon-work_expand'></span>
                <span id='meeting2FoldBtn' class='icon-work_collapse'></span>
                <span id=meeting2CancelBtn class='icon-work_cancel'></span>
            </div> </div>
        <div id=meeting_layout></div>`;
        
        document.getElementById("meeting2ExpandBtn").addEventListener('click', Expand);
        document.getElementById("meeting2FoldBtn").addEventListener('click', Fold);
        document.getElementById("meeting2CancelBtn").addEventListener('click', Close);

        if(getSubAppByUrl() === 'meeting') {
			document.getElementById("meeting2ExpandBtn").style.display = "inline-flex";
            document.getElementById("meeting2FoldBtn").style.display = "none";
		} else if(getMainAppByUrl() === 'meeting') {
			document.getElementById("meeting2ExpandBtn").style.display = "none";
            document.getElementById("meeting2FoldBtn").style.display = "inline-flex";
        }

        window.onbeforeunload = function() {
            if(Meeting2Manager.isConnected())
                Meeting2Manager.stop();
        };

        window.onunload = function() {
            if(document.readyState=="complete" && Meeting2Manager.isConnected()){
                Meeting2Manager.stop();
            }
        };
    };
    const Close = () => {
        onCloseButtonClick();
        $("div#AppSplitRightLayout").empty();
    };
    const Fold = () => {
        onFoldButtonClick();
        document.getElementById("meeting2ExpandBtn").style.display = "inline-flex";
        document.getElementById("meeting2FoldBtn").style.display = "none";
    };
    const Expand = () => {
        onExpandButtonClick();
        document.getElementById("meeting2ExpandBtn").style.display = "none";
        document.getElementById("meeting2FoldBtn").style.display = "inline-flex";
    };
    const isConnected = () => {
        return IS_MEETING_CONNECTION;
    };
    return {
        start : start,
        stop : stop,
        changeView : changeView,
        toggleView : toggleMeetingView,
        render : render,
        isConnected : isConnected
    }

} )();
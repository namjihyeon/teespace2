// Copyright (C) <2020> TmaxA&C
// Version 1.0.200402

"use strict";

var VerticalView = function (max_col, container) {
  const that = {};
  const self = {};

  self.maxColumn_ = max_col;
  self.tmp_canvas = document.createElement("canvas");
  self.timerId = null;

  self.container_ = {
    object: container,
    width: container.offsetWidth,
    height: container.offsetHeight,
  };

  self.renderer = function (subscriptionInfo) {
    self.subscriptionInfo_ = subscriptionInfo;
    const stream = self.subscriptionInfo_.stream;
    const resolution = self.subscriptionInfo_.resolution;

    const element_div = createVideoRegion(stream);
    subscriptionInfo.container.object = element_div;

    const element_video = document.createElement("video");
    element_video.text = "this browser does not supported video tag";
    element_video.id = stream.id;
    element_video.autoplay = true;
    element_video.controls = false;
    element_video.style.display = "none";
    element_video.srcObject = stream.mediaStream;

    const element_canvas = document.createElement("canvas");
    element_canvas.id = `${stream.id}canvas`;
    element_canvas.className = "tm-remote-view-renderer";
    element_div.appendChild(element_canvas);

    subscriptionInfo.container.player.push(element_video);
    subscriptionInfo.container.player.push(element_canvas);

    const video_width = Math.ceil(resolution.width / self.maxColumn_);
    const video_height = resolution.height;

    const scaling_factor_width = self.container_.width / video_width;
    const scaling_factor_height = self.container_.height / video_height;

    const scaling_factor = Math.min(
      scaling_factor_width,
      scaling_factor_height
    );

    const canvas_width = Math.floor(video_width * scaling_factor);
    const canvas_height = Math.floor(video_height * scaling_factor);

    element_canvas.width = video_width;
    element_canvas.height = video_height;

    const canvas_ctx = element_canvas.getContext("2d");

    self.tmp_canvas.width = video_width;
    self.tmp_canvas.height = video_height;

    const tmp_canvas_ctx = self.tmp_canvas.getContext("2d");

    function timerCallback() {
      if (element_video.paused || element_video.ended) {
        return;
      }

      tmp_canvas_ctx.drawImage(
        element_video,
        0,
        0,
        resolution.width,
        resolution.height
      );

      const frame = tmp_canvas_ctx.getImageData(
        0,
        0,
        video_width,
        video_height
      );

      canvas_ctx.putImageData(frame, 0, 0);

      setTimeout(function () {
        timerCallback();
      }, 33);
    }

    element_video.addEventListener(
      "play",
      () => {
        timerCallback();
      },
      false
    );
  };

  self.selectResolution = function (resolutions) {
    let target_resolution = null;
    for (const resolution of resolutions) {
      if (target_resolution === null) {
        target_resolution = resolution;
        continue;
      }

      const real_width = resolution.width / self.maxColumn_;
      if (
        real_width <= self.container_.width &&
        resolution.width > target_resolution.width &&
        resolution.height <= self.container_.height &&
        resolution.height > target_resolution.height
      ) {
        target_resolution = resolution;
      }
    }
    return target_resolution;
  };

  function createVideoRegion(stream) {
    const element_div = document.createElement("div");
    element_div.id = `${stream.id}resolutions`;
    element_div.className = "tm-remote-view";

    self.container_.object.appendChild(element_div);
    return element_div;
  }

  that.APICall = {
    selectResolution: self.selectResolution,
    render: self.renderer,
  };

  return that;
};

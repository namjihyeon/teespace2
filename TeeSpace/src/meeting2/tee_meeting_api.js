// Copyright (C) <2020> TmaxA&C
// Version 1.0.200407

const TeeMeetingAPI = function (container = null, address = null) {
  const that = {};
  const self = {};

  const PublicationState = {
    none: 0,
    active: 1,
    inactive: 2,
    switching: 3,
  };

  const PublicationMode = {
    ParticipateOnly: 0,
    CameraAndMic: 1,
    CameraOnly: 2,
    MicOnly: 3,
    ScreenCast: 4,
  };

  const LoginState = {
    disconnected: 0,
    connecting: 1,
    connected: 2,
  };
  
  self.loginstate_ = LoginState.disconnected;

  // default profile
  self.profile_src = "../../res/illust/friend_add.png"
  self.serverAddress_ = "https://teemeeting.tmaxos.com:13004";

  if (address !== null) {
    self.serverAddress_ = address;
  }

  self.restAPI_ = TeeMeetingRest(self.serverAddress_);

  self.globalContainer = null;
  self.isContainerProvided = container !== null;
  if (self.isContainerProvided) {
    self.globalContainer = container;
  }

  self.reset = function () {
    self.isViewSwitching_ = false;

    self.roomInfo_ = {
      name: null,
      id: null,
    };

    self.participantInfo_ = {
      me: {
        name: null,
        id: null,
      },
      participants: {},
    };

    self.streams = {
      local: {},
      mix: {},
      forward: {},
    };

    if (self.publicationInfo_) {
      if (self.publicationInfo_.publicationObject) {
        self.publicationInfo_.publicationObject.stop();
      }

      if (
        self.publicationInfo_.stream &&
        self.publicationInfo_.stream.mediaStream
      ) {
        const tracks = self.publicationInfo_.stream.mediaStream.getTracks();
        for (const track of tracks) {
          track.stop();
        }
      }
    }

    self.publicationInfo_ = {
      publicationObject: null,
      stream: null,
      state: {
        video: PublicationState.none,
        audio: PublicationState.none,
        screen: PublicationState.none,
      },
    };

    if (self.localSubscription_) {
      if (self.localSubscription_.container) {
        self.localSubscription_.container.remove();
      }
    }
    self.localSubscription_ = {
      container: null,
      player: null,
      stream: null,
    };

    if (self.portalAPI_) {
      self.portalAPI_.leave();
    }
    self.portalAPI_ = null;

    self.subscriptionInfo_ = {};

    if (!self.isContainerProvided) {
      if (self.globalContainer !== null) {
        self.globalContainer.remove();
      }
      self.globalContainer = createGlobalContainerForTest();
    }

    self.controlButtons_ = {};
    self.viewIndex = -1;
  };

  //self.isButtonEnabled = false;
  self.isButtonEnabled = true;
  self.reset();

  function subscriptionInfoTemplate() {
    return {
      subscription: null,
      stream: null,
      resolution: null,
      container: {
        object: null,
        player: [],
      },
    };
  }

  var onStreamAdded = (event) => {
    console.log("A new stream is added ", event.stream.id);
    event.stream.addEventListener("ended", () => {
      console.log(event.stream.id + " is ended.");
    });
  };

  function createTokenReq(roomId, callback) {
    var simulcast = false;
    var shareScreen = false;

    var mediaUrl = null;
    var isPublish = null;

    self.roomInfo_.id = roomId;

    self.restAPI_.APICall.createToken(
      self.roomInfo_.id,
      "user",
      "presenter",
      function (response) {
        let token = response;
        self.portalAPI_.join(token).then((resp) => {
          self.roomInfo_.id = resp.id;
          self.participantInfo_.me.id = resp.self.id;

          self.restAPI_.APICall.joinRoom(
            self.roomInfo_.id,
            self.participantInfo_.me.id,
            (response) => {
              console.log(response);
            }
          );

          let audioConstraints = false,
            videoConstraints = false;

          switch (self.publicationMode_) {
            case PublicationMode.ScreenCast:
              // AudioTrackConstraints For ScreenCast
              audioConstraints = new Owt.Base.AudioTrackConstraints(
                Owt.Base.AudioSourceInfo.SCREENCAST
              );
              // VideoTrackConstraints For ScreenCast
              videoConstraints = new Owt.Base.VideoTrackConstraints(
                Owt.Base.VideoSourceInfo.SCREENCAST
              );
              break;
            case PublicationMode.CameraAndMic:
              // AudioTrackConstraints For Mic
              audioConstraints = new Owt.Base.AudioTrackConstraints(
                Owt.Base.AudioSourceInfo.MIC
              );
              // VideoTrackConstraints For Camera
              videoConstraints = new Owt.Base.VideoTrackConstraints(
                Owt.Base.VideoSourceInfo.CAMERA
              );
              break;
            case PublicationMode.CameraOnly:
              // VideoTrackConstraints For Camera
              videoConstraints = new Owt.Base.VideoTrackConstraints(
                Owt.Base.VideoSourceInfo.CAMERA
              );
              break;
            case PublicationMode.MicOnly:
              // AudioTrackConstraints For Mic
              audioConstraints = new Owt.Base.AudioTrackConstraints(
                Owt.Base.AudioSourceInfo.MIC
              );
              break;
            case PublicationMode.ParticipateOnly:
              break;
          }

          let mediaStream = null;
          if (self.publicationMode_ !== PublicationMode.ParticipateOnly) {
            Owt.Base.MediaStreamFactory.createMediaStream(
              new Owt.Base.StreamConstraints(audioConstraints, videoConstraints)
            ).then(
              (stream) => {
                mediaStream = stream;
		
                if (self.publicationMode_ === PublicationMode.MicOnly) {
                  mediaStream.addTrack(
                    createFakeStream(self.profile_src).getVideoTracks()[0]
                  );
                }
                createLocalRenderer().then((param) => {
                  updateLocalSubscriptionInfo(param[0], param[1], mediaStream);
                });

                publish(mediaStream, callback);
              },
              (err) => {
                console.error("Failed to create MediaStream, " + err);
              }
            );
          } else {
            mediaStream = createFakeStream(self.profile_src);
            createLocalRenderer().then((param) => {
              updateLocalSubscriptionInfo(param[0], param[1], mediaStream);
            });

            publish(mediaStream, callback);
          }

          var streams = resp.remoteStreams;
          getStreamInfoByRoomId(self.roomInfo_.id, () => {
            for (const stream of streams) {
              if (stream.source.video === "mixed") {
                self.streams.mix[stream.id].stream = stream;
                continue;
              }

              if (stream.source.video === "forward") {
                self.streams.forward[stream.id].stream = stream;
                continue;
              }
            }
          });

          console.log("Streams in conference:", streams.length);
          let participants = resp.participants;
          console.log("Participants in conference: " + participants.length);
        });
      },
      function (err) {
        console.error(token);
        console.error("server connection failed:", err);
        if (err.message.indexOf("connect_error:") >= 0) {
          const signalingHost = err.message.replace("connect_error:", "");
          const signalingUi = "signaling";
          removeUi(signalingUi);
          let $p = $(`<div id=${signalingUi}> </div>`);
          const anchor = $("<a/>", {
            text: "Click this for testing certificate and refresh",
            target: "_blank",
            href: `${signalingHost}/socket.io/`,
          });
          anchor.appendTo($p);
          $p.appendTo($("body"));
        }
      }
    );
  }

  function removeUi(id) {
    $(`#${id}`).remove();
  }

  function queryRoom(roomName) {
    return new Promise(function (resolve, reject) {
      if (roomName !== null && roomName !== "") {
        resolve();
      }
      reject(new Error("Invalid room name"));
    });
  }

  function startMeeting(roomName = null, callback = null) {
    if(self.loginstate_ === undefined || 
       self.loginstate_ === LoginState.disconnected){
      self.loginstate_ = LoginState.connecting;
    } else {
      console.log("startMeeting already in progress!");
      return ;
    }

    queryRoom(roomName).then(() => {});

    if (self.roomInfo_.id !== null) {
      endMeeting(self.roomInfo_.id);
    }

    console.log(startMeeting.name, "Start meeting");
    self.portalAPI_ = new Owt.Conference.ConferenceClient();
    self.portalAPI_.addEventListener("streamadded", onStreamAdded);

    options = {};
    self.restAPI_.APICall.createRoom(roomName, options, (response) => {
      const data = JSON.parse(response);
      self.roomInfo_.name = data.name;
      self.roomInfo_.id = data._id;

      updatePublicationMode().then(
        (resolve) => {
          self.publicationMode_ = resolve;
          createTokenReq(self.roomInfo_.id, () => {
            if (self.isButtonEnabled) {
              createButtons(self.globalContainer).then((buttons) => {
                self.controlButtons_ = buttons;
                callback && callback();
              });
            } else {
              callback && callback();
            }

            self.loginstate_ = LoginState.connected;
          });
        },
        (err) => {
          console.log(err);
          self.loginstate_ = LoginState.disconnected;
          return false;
        }
      );
    });
    
    return true;
  }

  function changeView(index, callback = null) {
    if (index === self.viewIndex) {
      return;
    }

    if (self.isViewSwitching_ === true) return;
    else {
      self.isViewSwitching_ = true;
    }

    clearViewAll().then(() => {
      const stream_ids = Object.keys(self.streams.mix);
      if (stream_ids.length === 0) return;
      const stream_info = self.streams.mix[stream_ids[index]];

      let renderer = null;

      self.globalContainer.style.display = "grid";
      if (stream_info.label === "common") {
        self.globalContainer.className = "teemeeting-container";

        if (!self.isContainerProvided) {
          self.globalContainer.style.width = "23rem";
          self.globalContainer.style.height = "42.19rem";
        }

        if (self.isButtonEnabled) {
          self.controlButtons_.container.className = "teemeeting-button-footer";
          self.controlButtons_.container.style.display = "";
        }

        self.localSubscription_.container.style.display = "flex";
        renderer = VerticalView(5, self.globalContainer);
      }

      if (stream_info.label === "grid") {
        self.globalContainer.className = "teemeeting-gridview-container";

        if (!self.isContainerProvided) {
          self.globalContainer.style.width = "69.19rem";
          self.globalContainer.style.height = "42.32rem";
        }

        self.localSubscription_.container.style.display = "none";

        if (self.isButtonEnabled) {
          self.controlButtons_.container.className = "teemeeting-button-footer";
          //self.controlButtons_.container.style.display = "none";
        }

        renderer = GridView(self.globalContainer);
      }

      subscribeView(
        stream_info.stream,
        self.globalContainer,
        renderer,
        (result) => {
          if (result === true) {
            self.viewIndex = index;
          }
          self.isViewSwitching_ = false;
          callback && callback();
        }
      );
    });
  }

  function endMeeting(callback = null) {
    if (self.roomInfo_.id === null) {
      return false;
    }

    console.log(endMeeting.name, "client leaves meeting");

    self.restAPI_.APICall.leaveRoom(
      self.roomInfo_.id,
      self.participantInfo_.me.id,
      () => {
        console.log("Leave room");
      }
    );

    clearViewAll().then(() => {
      self.reset();
      callback && callback();
    });

    self.loginstate_ = LoginState.disconnected;

    return true;
  }

  function getResolutions(stream) {
    const resolutions = [];

    const default_resolution = stream.settings.video[0].resolution;
    const ratio = Math.ceil(
      (default_resolution.width * 10) / default_resolution.height
    );

    resolutions.push(default_resolution);

    // Resolutions from extraCapabilities.
    for (const resolution of stream.extraCapabilities.video.resolutions.reverse()) {
      if (resolution) {
        if (Math.ceil((resolution.width * 10) / resolution.height) == ratio) {
          resolutions.push(resolution);
        }
      }
    }

    resolutions.sort((a, b) => {
      if (a.width !== b.width) {
        return a.width - b.width;
      } else if (a.height !== b.height) {
        return a.height - b.height;
      }
      return 0;
    });

    return resolutions;
  }

  function subscribeView(stream, parent_div, renderer, callback) {
    if (renderer === null) return;

    const target_resolution = renderer.APICall.selectResolution(
      getResolutions(stream),
      parent_div
    );

    const videoOptions = {};
    videoOptions.resolution = target_resolution;
    self.portalAPI_
      .subscribe(stream, {
        audio: true,
        video: videoOptions,
      })
      .then(
        (subscription) => {
          const subscriptionInfo = subscriptionInfoTemplate();
          subscriptionInfo.subscription = subscription;
          subscriptionInfo.stream = stream;
          subscriptionInfo.resolution = target_resolution;

          renderer.APICall.render(subscriptionInfo);
          self.subscriptionInfo_[stream.id] = subscriptionInfo;

          callback(true);
        },
        (err) => {
          console.log("subscribe failed", err);
          callback(false);
        }
      );

    stream.addEventListener("ended", () => {
      element_video.remove();
      element_div.remove();
    });

    stream.addEventListener("updated", () => {
      subscribeAndRenderVerticalView(stream, parent_div);
    });
  }

  function clearView(info) {
    if (info.subscription) {
      console.log(`Stream ${info.stream.id} subscription stopped!`);
      info.subscription.stop();
    }
    for (const player of info.container.player) {
      player.remove();
    }
    info.container.object.remove();
  }

  function clearViewAll() {
    for (const id in self.subscriptionInfo_) {
      console.log(`Clear view of stream id: ${id}`);
      clearView(self.subscriptionInfo_[id]);
    }

    return new Promise(function (resolve, reject) {
      resolve();
    });
  }

  function getStreamInfoByRoomId(roomId, callback) {
    self.restAPI_.APICall.getStreamInfo(roomId, function (response) {
      const resp = JSON.parse(response);
      for (const stream of resp) {
        if (stream.type === "mixed") {
          self.streams.mix[stream.id] = {
            label: stream.info.label,
            media: stream.media,
          };
          continue;
        }

        if (stream.type === "forward") {
          self.streams.forward[stream.id] = {
            media: stream.media,
          };
          continue;
        }
      }

      callback();
    });
  }

  function createGlobalContainerForTest() {
    const element_div = document.createElement("div");
    element_div.id = "teemeeting_div_for_test";

    element_div.style.display = "none";
    document.body.appendChild(element_div);

    return element_div;
  }

  function changeButtonUI(button_id, action) {
    button = document.getElementById(button_id);
    button = $(button);
    if (button_id === "element_button_video_onoff") {
      if (action === "on") {
        button.removeClass("teemeeting-video-button-off");
        button.addClass("teemeeting-video-button-on");
      } else if (action === "off") {
        button.removeClass("teemeeting-video-button-on");
        button.addClass("teemeeting-video-button-off");
      }
      return;
    }

    if (button_id === "element_button_audio_onoff") {
      if (action === "on") {
        button.removeClass("teemeeting-audio-button-off");
        button.addClass("teemeeting-audio-button-on");
      } else if (action === "off") {
        button.removeClass("teemeeting-audio-button-on");
        button.addClass("teemeeting-audio-button-off");
      }

      return;
    }
  }

  //Button Partion(White Rectangle)
  function createButtonPartition(id) {

    const element_button_partition = document.createElement("rectangle");
    element_button_partition.id = "element_button_partition" + id;
    $(element_button_partition).addClass("teemeeting-button-partition");

    return element_button_partition;
  }

  function createTimer(id){
    //Timer Text
    const element_timer = document.createElement("div");
    element_timer.id = id;
    element_timer.innerHTML = "0:00";
    $(element_timer).addClass("teemeeting-timer-text-style");
    return element_timer;
  }

  function zeroPadding(n) {
    if (n < 10) {n = "0" + n};
    return n;
  }

  function startTimer(id) {
    
    const timer = document.getElementById(id);
    
    if(timer === null){
      return ;
    }

    var timediff =  new Date() - timer.reference_time;

    timediff /= 1000;
    
    var hour = Math.floor(timediff / (60 * 60));
    var min = Math.floor(timediff / (60)) % 60;
    var sec = Math.floor(timediff % 60);
    sec = zeroPadding(sec);
    min = zeroPadding(min);
 
    if(hour > 0){
      timer.innerHTML = `${hour}:${min}:${sec}`;
    }
    else{
      timer.innerHTML = `${min}:${sec}`;
    }
    var t = setTimeout(startTimer, 500, id);
  }

  function createTimerComponent(){
    const element_div = document.getElementById("teemeeting_div_for_buttons");

    //button partition(front)
    const element_button_partition_front = createButtonPartition("_front");
    element_div.insertBefore(element_button_partition_front, element_div.firstChild);

    //Timer Text
    const element_timer = createTimer("teemeeting_timer");
    element_div.insertBefore(element_timer, element_div.firstChild);
    element_timer.reference_time = new Date();

    startTimer(element_timer.id);
    return element_div;
  }

  function removeTimerComponent(){
    const element_div = document.getElementById(teemeeting_div_for_buttons);

    element_div.removeChild(document.getElementById("element_button_partition_front"));
    element_div.removeChild(document.getElementById("teemeeting_timer"));
  }

  function createButtons(container) {
    const element_div = document.createElement("div");
    element_div.id = "teemeeting_div_for_buttons";
    container.appendChild(element_div);

    //video on/off button
    const element_button_video_onoff = document.createElement("button");
    element_button_video_onoff.id = "element_button_video_onoff";
    $(element_button_video_onoff).addClass("teemeeting-button-common");
    $(element_button_video_onoff).addClass("teemeeting-video-button-on");

    element_button_video_onoff.addEventListener("click", () => {
      const currentState = self.publicationInfo_.state.video;
      if (currentState === PublicationState.active) {
        self.updateVideoStream(PublicationState.inactive);
        changeButtonUI(element_button_video_onoff.id, "off");
      }

      if (currentState === PublicationState.inactive) {
        self.updateVideoStream(PublicationState.active);
        changeButtonUI(element_button_video_onoff.id, "on");
      }
    });
    element_div.appendChild(element_button_video_onoff);

    //audio on/off button
    const element_button_audio_onoff = document.createElement("button");
    element_button_audio_onoff.id = "element_button_audio_onoff";
    $(element_button_audio_onoff).addClass("teemeeting-button-common");
    $(element_button_audio_onoff).addClass("teemeeting-audio-button-on");
    element_button_audio_onoff.addEventListener("click", () => {
      const currentState = self.publicationInfo_.state.audio;
      console.log(`Current audio state: ${currentState}`);
      if (currentState === PublicationState.active) {
        self.updateAudioStream(PublicationState.inactive);
        changeButtonUI(element_button_audio_onoff.id, "off");
        return;
      }

      if (currentState === PublicationState.inactive) {
        self.updateAudioStream(PublicationState.active);
        changeButtonUI(element_button_audio_onoff.id, "on");
        return;
      }
    });
    element_div.appendChild(element_button_audio_onoff);

    //button partition(back)
    const element_button_partition_back = createButtonPartition("_back");
    element_div.appendChild(element_button_partition_back);

    //endmeeting button
    const element_button_endmeeting = document.createElement("button");
    element_button_endmeeting.id = "element_button_endmeeting";
    $(element_button_endmeeting).addClass("teemeeting-button-common");
    $(element_button_endmeeting).addClass("teemeeting-endmeeting-button");

    element_button_endmeeting.addEventListener("click", () => {
      console.log("endmeeting button clicked");
      if (self.roomInfo_.id !== null) {
        endMeeting();
      }
    });

    element_div.appendChild(element_button_endmeeting);

    //meeting timer
    createTimerComponent();

    const buttons = {
      container: element_div,
      video_onoff: element_button_video_onoff,
      audio_onoff: element_button_audio_onoff,
      endmeeting_button: element_button_endmeeting,
    };

    return new Promise(function (resolve) {
      resolve(buttons);
    });
  }

  function createLocalRenderer() {
    const element_div = document.createElement("div");
    element_div.className = "tm-local-view";

    const element_video = document.createElement("video");
    element_video.text = "this browser does not supported video tag";
    element_video.autoplay = true;
    element_video.controls = false;
    element_video.className = "tm-local-view-renderer";
    element_video.muted = "muted";

    $(document).ready(function() {
      $("video").bind("contextmenu",function(){
        return false;
      });
    });
    element_div.appendChild(element_video);

    return new Promise(function (resolve) {
      resolve([element_div, element_video]);
    });
  }

  function updatePublicationMode() {
    if (!navigator.mediaDevices.enumerateDevices) {
      return new Promise(function (reject) {
        reject(new Error("do not support enumerateDevices"));
      });
    } else {
      return new Promise(function (resolve, reject) {
        navigator.mediaDevices.enumerateDevices().then(
          (devices) => {
            let audio = false,
              video = false;
            for (device of devices) {
              console.log(device.kind);
              if (device.kind === "audioinput") {
                audio = true;
              } else if (device.kind === "videoinput") {
                video = true;
              }
            }
            if (audio && video) {
              resolve(PublicationMode.CameraAndMic);
            }
            if (audio && !video) {
              resolve(PublicationMode.MicOnly);
            }
            if (!audio && video) {
              resolve(PublicationMode.CameraOnly);
            }
            if (!audio && !video) {
              resolve(PublicationMode.ParticipateOnly);
            }
          },
          (err) => {
            reject(err);
          }
        );
      });
    }
  }

  function createFakeStream(imgSrc = null) {
    const canvas = document.createElement("canvas");
    canvas.id = "fake_canvas";
    canvas.className = "tm-profile";
    const image = document.createElement("img");
    image.id = "alternative_image";
    image.src = imgSrc;
    // update image after 500ms (timing issue)
    setTimeout(function () {
      canvas
        .getContext("2d")
        .drawImage(image, 0, 0, canvas.width, canvas.height);
    }, 500);
    return canvas.captureStream(25);
  }

  function updateLocalSubscriptionInfo(container, player, stream) {
    self.localSubscription_.container = container;
    self.localSubscription_.player = player;
    self.localSubscription_.stream = stream;

    self.localSubscription_.player.srcObject = stream;

    self.globalContainer.appendChild(self.localSubscription_.container);
  }

  function publish(mediaStream, callback = null) {
    self.publicationInfo_.stream = new Owt.Base.LocalStream(
      mediaStream,
      new Owt.Base.StreamSourceInfo("mic", "camera")
    );

    let publishOption;
    self.portalAPI_
      .publish(self.publicationInfo_.stream, publishOption)
      .then((publication) => {
        self.publicationInfo_.publicationObject = publication;
        const tracks = self.publicationInfo_.stream.mediaStream.getTracks();
        for (const track of tracks) {
          console.log(`Track info: kind=${track.kind}`);
          if (track.kind === "audio") {
            self.publicationInfo_.state.audio = PublicationState.active;
            continue;
          }
          if (track.kind === "video") {
            self.publicationInfo_.state.video = PublicationState.active;
            continue;
          }
        }

        for (const key in self.streams.mix) {
          self.restAPI_.APICall.mixStream(
            self.roomInfo_.id,
            publication.id,
            self.streams.mix[key].label
          );
        }

        callback();

        publication.addEventListener("error", (err) => {
          console.log("Publication error: " + err.error.message);
        });
      });
  }

  self.updateVideoStream = function (nextState) {
    if (
      self.publicationInfo_.state.video === PublicationState.switching ||
      self.publicationInfo_.state.video === nextState
    ) {
      return;
    }

    self.publicationInfo_.state.video = PublicationState.switching;
    if (nextState === PublicationState.active) {
      window.teemeeting.publicationInfo_.publicationObject
        .unmute("video")
        .then(() => {
          self.publicationInfo_.state.video = nextState;
          console.log("Video active");
        });
    }

    if (nextState === PublicationState.inactive) {
      window.teemeeting.publicationInfo_.publicationObject
        .mute("video")
        .then(() => {
          self.publicationInfo_.state.video = nextState;
          console.log("Video inactive");
        });
    }
  };

  self.updateAudioStream = function (nextState) {
    if (
      self.publicationInfo_.state.audio === PublicationState.switching ||
      self.publicationInfo_.state.audio === nextState
    ) {
      return;
    }

    self.publicationInfo_.state.audio = PublicationState.switching;
    if (nextState === PublicationState.active) {
      window.teemeeting.publicationInfo_.publicationObject
        .unmute("audio")
        .then(() => {
          self.publicationInfo_.state.audio = nextState;
          console.log("Audio active");
        });
    }

    if (nextState === PublicationState.inactive) {
      window.teemeeting.publicationInfo_.publicationObject
        .mute("audio")
        .then(() => {
          self.publicationInfo_.state.audio = nextState;
          console.log("Audio inactive");
        });
    }
  };

  that.APICall = {
    startMeeting: startMeeting,
    changeView: changeView,
    endMeeting: endMeeting,
  };

  window.teemeeting = self;

  return that;
};

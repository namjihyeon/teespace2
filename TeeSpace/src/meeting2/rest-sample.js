// Copyright (C) <2020> TmaxA&C
// Version 1.0.200402
"use strict";

const TeeMeetingRest = function (host) {
  const that = {};
  const self = {};

  self.host_ = host;

  self.generateUrl = function (path) {
    let url;
    if (self.host_ !== undefined) {
      url = self.host_ + path; // Use the host user set.
    } else {
      let u = new URL(document.URL);
      url = u.origin + path; // Get the string before last '/'.
    }
    return url;
  };

  self.send = function (method, path, body, callback) {
    var req = new XMLHttpRequest();
    req.onreadystatechange = function () {
      if (req.readyState === 4) {
        callback(req.responseText);
      }
    };
    let url = self.generateUrl(path);
    req.open(method, url, true);
    req.setRequestHeader("Content-Type", "application/json");
    if (body !== undefined) {
      req.send(JSON.stringify(body));
    } else {
      req.send();
    }
  };

  self.mixStream = function (room, stream, view) {
    var jsonPatch = [
      {
        op: "add",
        path: "/info/inViews",
        value: view,
      },
    ];
    self.send(
      "PATCH",
      "/rooms/" + room + "/streams/" + stream,
      jsonPatch,
      onResponse,
      self.host_
    );
  };

  self.startStreamingIn = function (room, inUrl) {
    var options = {
      url: inUrl,
      media: {
        audio: "auto",
        video: true,
      },
      transport: {
        protocol: "udp",
        bufferSize: 2048,
      },
    };
    self.send(
      "POST",
      "/rooms/" + room + "/streaming-ins",
      options,
      onResponse,
      self.host_
    );
  };

  self.createToken = function (room, user, role, callback) {
    var body = {
      room: room,
      user: user,
      role: role,
    };
    self.send("POST", "/tokens/", body, callback, self.host_);
  };

  self.getStreamInfo = function (room, callback) {
    var body = {};
    self.send("GET", "/rooms/" + room + "/streams", body, callback, self.host_);
  };

  self.createRoom = function (name, options, callback) {
    const body = {
      name: name,
      options: options,
    };
    self.send("POST", "/tm_rooms", body, callback);
  };

  self.joinRoom = function (room, participant, callback) {
    const body = {
      owt_id: room,
      user_id: participant,
    };
    self.send("POST", "/tm_rooms/user", body, callback, self.host_);
  };

  self.leaveRoom = function (room, participant, callback) {
    const body = {};
    self.send(
      "DELETE",
      `/tm_rooms/room/${room}/user/${participant}`,
      body,
      callback,
      self.host_
    );
  };

  var onResponse = function (result) {
    if (result) {
      try {
        console.info("Result:", JSON.parse(result));
      } catch (e) {
        console.info("Result:", result);
      }
    } else {
      console.info("Null");
    }
  };

  self.updateStream = function (room, stream, state) {
    var jsonPatch = [
      {
        op: "replace",
        path: "/media/video/status",
        value: state,
      },
    ];
    self.send(
      "PATCH",
      "/rooms/" + room + "/streams/" + stream,
      jsonPatch,
      onResponse,
      self.host_
    );
  };

  that.APICall = {
    mixStream: self.mixStream,
    //updateStream: self.updateStream,
    startStreamingIn: self.startStreamingIn,
    createToken: self.createToken,
    getStreamInfo: self.getStreamInfo,
    createRoom: self.createRoom,
    joinRoom: self.joinRoom,
    leaveRoom: self.leaveRoom,
  };

  return that;
};

var tagDTO;
var tagArray = [];
var tagDelArray = [];
var _ctrEvent, _ctrEditor,_ctrAllNote,_ctrLnbList,_ctrTag, _ctrTrashCan, _ctrLnb, _ctrTable;

Top.Controller.create('noteEditorLayoutLogic', {
	onSave : function(event, widget) {
		UTIL.ID('saveButton').setDisabled(true)
		Top.Controller.get('noteSaveLayoutLogic').saveOkBtn()
	}, 
//	tagToggle : function(event, widget) {
//		if(Top.Dom.selectById('tagTextField').getProperties("visible") == "none"){
//			UTIL.setProperty('tagTextField',{"visible":"visible"})
//			$('#tagTextField').focus()
//		}else if(UTIL.ID('tagTextField').getProperties("visible") == "visible"){
//			UTIL.setProperty('tagTextField',{"visible":"none"})
//		}
//	}, 
	checkExistingTag : function(newTagName){
		let flag = false;
		for(let i=0; i<tag_item.length; i++){
			if(newTagName === tag_item[i].text) flag = true;
		}
		return flag;
	},
	tagDel : function(data){
		tagDelArray.push(data)
	},
	initTagMore : function(event, widget) {
		// 버튼 클릭할때랑 아이콘 클릭할때랑 이벤트가 다른데 왜그러지
		event.stopPropagation();
		let checkVisible = UTIL.ID('tagMoreLayoutCover').getVisible()
		if(checkVisible == "none"){
			UTIL.setProperty('filled_downBtn',{"icon":"icon-arrow_filled_up"})
			UTIL.setProperty('tagMoreLayoutCover', {"visible":"visible"})
			for(let k=0; k<tag_item.length; k++){
				tag_item[k].id = tag_item[k].tag_id;
			}
			noteRepo.setValue('tagMoreList', tag_item)
			UTIL.ID('tagMoreLayoutCover').append('./tagMoreLayout.html',function(){
				for(let j=0; j<tag_item.length; j++){
					let tagListArray = [];
					tagListArray.push(tag_item[j])
					UTIL.setProperty('tagListChip_' + j,{"chip-items": tagListArray})
					document.querySelectorAll('.tagList .top-chip-close')[0].classList = "";
					tagListArray = [];
				}
			})
			$(document).on("click", _ctrEditor.closeTagEvent);
		}else {
			UTIL.setProperty('tagMoreLayoutCover', {"visible":"none"})
			UTIL.setProperty('filled_downBtn',{"icon":"icon-arrow_filled_down"})
		}
	},
	closeTagEvent :function(event,widget){
		if($(event.target).closest('.icon-arrow_filled_up').length !== 1){
			if (!$(event.target).closest(".tagList").length && !$(event.target).closest("#tagMoreList").length){
				_ctrEditor.closeLayout();
	        }
		}		
	},
	closeLayout : function(){
		if(_ctrEditor.checkTagMore()){
			$(document).off("click", _ctrEditor.closeTagEvent);
			$('button#filled_downBtn').trigger("click")
		}
	},
	checkTagMore : function(){
		return UTIL.ID('tagMoreLayoutCover').getVisible() =="visible" ? true : false
	},
	checkTagMoreLayout : function(){
		setTimeout(function(){
			if($('#tag_chip .top-chip-root form').width() > $('#chipLayout').width()){
				UTIL.setProperty('filled_downBtn',{"visible":"visible"})
			}else{
				UTIL.setProperty('filled_downBtn',{"visible":"none"})
			}
		},1)
	}
})

Top.Controller.create('noteSaveLayoutLogic', {
	saveOkBtn : function() {
		var _delCheck = 0
		let noteUpdateInfo = {};
		let title;
		let content;
		let note_id = TNoteData.getCurPageData().note_id;
		// (SJ) TNoteData에서 뽑는 것으로 바꿈
		let p_id = TNoteData.getCurPageData().parent_notebook;
		
		// close btn db 저장 안하게 하기
		let existCloseBtnList = document.querySelectorAll('div#__note-file_close');
		if(existCloseBtnList.length !==0){
			for(let i=0; i< existCloseBtnList.length; i++){
				existCloseBtnList[i].remove();
			}
		}
		/*
		 * Update Data Set
		 */
		title = TNoteEditor.getTitle();
		
		content = document.querySelector('top-htmleditor#noteEditor').getEditorHTML();
		content = [].filter.call( content, function( c ) {
			return c.charCodeAt( 0 ) !== 8203;
			}).join('');
		content = content;
		if(isNaN(content.charCodeAt()) == true){
			content = "<p><br></p>"
		}
		
		
		if(tagDelArray.length != 0){ // 삭제된 태그가 있는 경우
			_delCheck = 1
			for(let i=0; i < tagDelArray.length; i++){
				TNoteServiceCaller.tagDelete(tagDelArray[i].note_id, tagDelArray[i].tag_id)
			}
		}
		if(tagArray.length != 0){ // 새로 생성된 태그가 있는 경우
			for(let i=0; i < tagArray.length; i++){
				TNoteServiceCaller.tagCreate(tagArray[i].note_id, tagArray[i].text)
			}
		}
		if(noteFileDelArray.length != 0){ // 삭제된 파일+이미지가 있는 경우
			for(let i=0; i<noteFileDelArray.length; i++){
				TNoteServiceCaller.noteFileDelete(noteFileDelArray[i]);
			}
		}
		if(document.querySelector('#note .note-tag-add-textfield').style.display = "flex") { 
			document.querySelector('#note .note-tag-add-textfield').style.display = "none";
			document.querySelector('#note .note-tag-add-textfield').value = "";
		}
		noteUpdateInfo["note_id"] = note_id;
    	noteUpdateInfo["note_title"] = title;
    	noteUpdateInfo["note_content"] = content;
    	noteUpdateInfo["parent_notebook"] = p_id;
	    	
    	TNoteServiceCaller.noteUpdate(noteUpdateInfo, function(){
    		spaceAPI.spaceInit()
    		let renderedPageDiv = $('li[content-id="'+ document.getElementById('note-editor-container').getAttribute('rendered-page-id') + '"]')
    		if(renderedPageDiv.length>0){
    			renderedPageDiv[0].classList.add('selected');
        		TNoteController.selectPageMenu(renderedPageDiv[0].getAttribute('content-id'));
    		}
        	TNoteController.initChapterList()
        	tagDelArray = [];
    		tagArray = [];
    		noteFileArray = [];
    		noteFileDelArray = [];
        });

		for(let i=0; i<4; i++){
			UTIL.ID('tag_accordion').closeTab(i)
		}
		
		TNoteController.editorReadMode();
		TNoteController.removeModalMode();
		if(UTIL.ID("permissionRequestLayoutCover").getProperties("visible")==="visible"){
            Top.Controller.get('permissionRequestLayoutLogic').closeRequestPopup();
		}
	},
	saveCancelBtn : function(event, widget) {
		UTIL.closeDialog('noteSaveDialog')
	}
});

TNoteEditor = (function (){
	function getTitle(){
		let title = UTIL.ID('contenteditorTitle').getText();
		if(title.trim() === "") {
			let firstLine = getTitleFromContent();
			firstLine = [].filter.call( firstLine, function( c ) {
				return (c.charCodeAt( 0 ) !== 8203 && c.charCodeAt( 0 ) !== 160);
			}).join(''); // editor가 만든 공백문자열을 제거한다.
			escapedFisrtLine = TNoteUtil.escapeHtml(firstLine);
			title = firstLine === "" ? '제목 없음' : firstLine.substring(0,250);
		}
		return title;
	}
	function getTitleFromContent( tempString ){
		let targetTitle = "";
		let targetNode = null;
		if(tempString) {
			// context 메뉴로 수정 시
			let template = document.createElement('template');
		    template.innerHTML = tempString;
		    targetNode = template.content.childNodes[0];
		}else {
			// 에디터 수정 모드 시
			targetNode = document.querySelector('#note #noteEditor div[contenteditable=true]').firstElementChild;
		}
		targetTitle = innerLogic(targetNode)
		return targetTitle;
	}
	function innerLogic(targetNode){
		let type = targetNode.nodeName;
		let targetString;
		let result = "";
		if(type === "DIV"){ // 업로드된 객체
			if(targetNode.firstElementChild.nodeName === "FIGURE"){ // image case
				result = targetNode.find('img')[0].getAttribute('data-file-name');
			} else if(targetNode.firstElementChild.nodeName === "DIV") { // file case
				result = targetNode.getElementsByClassName('note-file-name')[0].textContent;
			}
		} else if(type === "TABLE"){
			let cells = targetNode.getElementsByTagName("td");
			let findTextFlag = false;
			for(let i=0; i<cells.length; i++){
				targetString = cells[i].textContent.trim();
				if(targetString !== ""){
					result = targetString;
					findTextFlag = true;
					break;
				}
			}
			if(findTextFlag === false) result = innerLogic(targetNode.nextElementSibling);
		} else if(type === "P"){
			targetString = targetNode.textContent.trim();
			if(targetString === "" && targetNode.nextElementSibling !== null) {
				result = innerLogic(targetNode.nextElementSibling);
			}
			else result = targetString;
		}
		if(result.trim() === "") result = "제목 없음"
		return result
	}
	
	return {
		getTitle:getTitle,
		getTitleFromContent:getTitleFromContent
	}
	
})();
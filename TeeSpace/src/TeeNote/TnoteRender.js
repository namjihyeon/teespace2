var TNOTE_MENU = {
    TOTAL_NOTES: "all_note",
    TAG_TREE: "tagTree",
    NOTE_TREE: "noteTree",
    TRASHCAN: "trash_bin",
    FAVORITE: "favorite",
    FILE: "file"
};

var TAG_CATEGORY = {
    KOR:'tag_kor_tablayout',
    ENG:'tag_eng_tablayout',
    NUM:'tag_num_tablayout',
    ETC:'tag_etc_tablayout',
};
TNoteRenderer = ( function () {

    let controller = null;
    let TNOTE_MENU_ARRAY = Object.values( TNOTE_MENU );


    let tagTreeStatus = "closed";
    let noteBookTreeStatus = "opened";

    let renderNoteTemplate = (targetLayout) => {
        TNoteUtil.removeChildrenNodes(targetLayout);

        const _editorCoverDisplay = (noteLnbMenu.isExpanded()) ? "flex" : "none";
        const _closeBtnDisplay = (noteLnbMenu.isExpanded()) ? "none" : "flex";

        const noteTemplate = `
        <div id="note" note-channel-id="${NoteApp.getChannelId()}">
            <div id="note-content-area">
                <div id="note-lnb-menus-cover">
                    <div id="note-lnb-upper-menu">
                        <input id="note-lnb-new-chapter" type="button" value="새 챕터">
                        <div class="note-lnb-search">
                            <span class="icon-search"></span>
                            <input id="note-lnb-search-textfield" type="text" autocomplete="off" placeholder="페이지, 챕터 검색">
                            <img class="note-lnb_search-cancel note-lnb_cancel-img" src="./res/note/ts_cancel@3x.png" />
                        </div>
                        <div class="note-divider__column" style="display:${_closeBtnDisplay}"><div class="note-lnb-divider"></div></div>
                        <div class="note__header__column" style="display:${_closeBtnDisplay}">
                            <span class="note-expand-btn icon-work_expand" onclick="noteLnbMenu.switchExpand(event)"></span>
                            <span class="note-title-close-btn">
                                <img class="note-lnb_cancel-img" src="./res/note/ts_cancel@3x.png">
                            </span>
                        </div>
                    </div>
                    <div id ="note-chapter-cover"></div>
                    <div id="note-tag-cover"></div>
                    <div id="note-shared-page-cover"></div>
                    <div id="note-shared-chapter-cover"></div>
                </div>
                <div id="note-editor-container" style="display:${_editorCoverDisplay}"></div>
                <div id="note-tag-container" style="display:none"></div>
            </div>
        </div>`
        targetLayout.insertAdjacentHTML("beforeend", noteTemplate.trim());

        let _closeBtn = document.querySelector('#note-lnb-menus-cover .note-title-close-btn');
        if (_closeBtn) {
            _closeBtn.addEventListener("click", function() {
                onCloseButtonClick('note',function(){
                    noteLnbMenu.setIsExpanded(false);
                })
            })
        }
        return new Promise((res, rej) => {
            res(targetLayout);
        })
    };

    function initRender( targetLayout, noteController) {
        controller = noteController;
        // 기존 setLnbFrame(첫 번째 then까지)
        renderNoteTemplate(targetLayout)
            .then((targetLayout)=> {
                // 이벤트 달기
                _newChapterButton = document.querySelector("#note-lnb-new-chapter");
                _newChapterButton.addEventListener('click', noteLnbMenu.clickNewChapterButtonEvent);
                
                _searchCloseIcon = document.querySelector(".note-lnb_search-cancel");
                _searchCloseIcon.addEventListener('click',function(){
                    document.getElementById('note-lnb-search-textfield').value = "";
                    TNoteSearch.setLnbMenusVisibility('block')
                    $("#note-lnb-new-chapter").removeAttr("disabled");
                });

                TNoteSearch.addKeyEventHandler();

                return new Promise((res,rej) => {
                    res(targetLayout);
                })
            })
            .then((targetLayout) => {
                TNoteRenderer.initEditorRender( document.getElementById('note-editor-container'));
                TNoteRenderer.initTagRender(document.getElementById('note-tag-container'));
            })
    }

    function initEditorRender(targetLayout){
    	let emptyLayout = document.createElement('div');
        emptyLayout.setAttribute('id','note-empty-div');
        document.getElementById('note-editor-container').insertBefore(emptyLayout, document.getElementById('note-editor-container').firstElementChild);
        
        const _isExpanded = noteLnbMenu.isExpanded() ? "icon-work_collapse" : "icon-work_expand";
        const _preBtnVisibility = noteLnbMenu.isExpanded() ? "hidden" : "visible";

        let template = `
                <div class="note-editor_page-none-header">
                    <img class="note_pre-btn" src="./res/note/back.svg" style="visibility:${_preBtnVisibility}" />
                    <div class="note__header__column">
                        <span class="note-expand-btn ${_isExpanded}" onclick="noteLnbMenu.switchExpand(event)"></span>
                        <span class="note-title-close-btn">
                            <img class="note-lnb_cancel-img" src="./res/note/ts_cancel@3x.png">
                        </span>
                    </div>
                </div>
				<div class="note-editor_page-none">
					<div style='font-size:1.5rem; font-weight:bold; width:auto; height:1.5rem;'>페이지가 없습니다.</div>
					<div style='font-size:1rem; width:auto; height:1rem; padding-top: 1rem;'>시작하려면 "새 페이지 추가" 버튼을 클릭하세요.</div>
				</div>
				<div id='note-empty-illustration' style='width:100%; height:70%;'>
				</div>
				`;
        emptyLayout.insertAdjacentHTML("beforeend", template);
        emptyLayout.querySelector('.note_pre-btn').addEventListener("click", function() {
            if(!noteLnbMenu.isExpanded()){
    			noteLnbMenu.setLnbVisibility('flex')
    			noteLnbMenu.setEditorVisibility('none')
    		}
        });

        emptyLayout.querySelector('.note-lnb_cancel-img').addEventListener("click", function() {
            onCloseButtonClick('note',function(){
				noteLnbMenu.setIsExpanded(false);
        	})
        });
    	
    	let _editorDiv =  Top.Widget.create('top-linearlayout');
    	let _foldingDiv =  document.createElement("div");
    	_foldingDiv.setAttribute('id','note-expand-fold')
    	_foldingDiv.style.display = 'none';
    	_foldingDiv.setAttribute('class','note-folding-sub')
    	_foldingDiv.setAttribute('onClick', 'noteLnbMenu.switchEditorExpand(event)');
    	_editorDiv.setProperties({"id":"editorDIV"});
    	_editorDiv.template.children[0].setAttribute("style","height: 100%; width: 100%; border-top:0px solid #000000;");
    	$('div#AppSplitRightLayout').css("line-height","normal")
    	targetLayout.append(_editorDiv.template)
    	targetLayout.append(_foldingDiv)
    	UTIL.ID('editorDIV').src('noteEditorLayout.html', function(){
    		if(appManager.isExpanded()) {
    			Note.checkDOM('#notePreBtn', function(){
    				noteLnbMenu.setFoldingVisibility('flex');
    			})
            }
            controller.initChapterList();
            controller.initTagMenu();
        });
    }

    // taragetLayout : note-tag-container
    function initTagRender( targetLayout ){
    	let _tagDiv =  Top.Widget.create('top-linearlayout');
    	_tagDiv.setProperties({"id":"tagDIV"});
    	_tagDiv.template.children[0].classList.add("note-tag_top-layout");
        
        // not-tag-tagLayout
    	let _tagLayout = document.createElement('div');
    	_tagLayout.setAttribute('id','note-tag-tagLayout');
    	let _tagTitleLayout = document.createElement('div');
        _tagTitleLayout.setAttribute('id','note-tag-tagTitleLayout');

        // tag가 none일 때도 title이 필요할 것 같다
        targetLayout.appendChild(_tagTitleLayout);        

    	let _tagLayout_search = document.createElement('div')
    	_tagLayout_search.setAttribute('id','tagLayout_search')
    	let _tagPreBtnLayout = document.createElement('div')
        _tagPreBtnLayout.setAttribute('id','note-tag-PreBtnLayout')
    	
    	let _tagPreBtnButton = document.createElement('input')
    	_tagPreBtnButton.setAttribute('class','top-imagebutton-root')
        _tagPreBtnButton.setAttribute('type','image')
        _tagPreBtnButton.setAttribute('id','tagPreBtn')
        _tagPreBtnButton.setAttribute("src","res/note/back.svg")
        _tagPreBtnButton.addEventListener("click", function() {
        	noteLnbMenu.setLnbVisibility('flex');
        	noteLnbMenu.setTagVisibility('none');
        	noteLnbMenu.setEditorVisibility('none');
        	noteLnbMenu.setFoldingVisibility('none');
        })
        
        let _tagsearchContainer = document.createElement('span');        
        let _tagsearchTextField = document.createElement('input');        
    	_tagsearchTextField.setAttribute('id','note-Tag-search-textfield');
    	_tagsearchTextField.setAttribute('type','text')
    	_tagsearchTextField.setAttribute('placeholder','검색')
    	_tagsearchContainer.setAttribute('id','note-Tag-search')
    	_tagsearchIcon = document.createElement('span')
    	_tagsearchIcon.classList.add('icon-search')
    	_tagsearchContainer.appendChild(_tagsearchIcon)
    	_tagsearchContainer.appendChild(_tagsearchTextField)
    	
    	_tagPreBtnLayout.appendChild(_tagPreBtnButton)
    	_tagLayout_search.appendChild(_tagsearchContainer)
        
    	_tagTitleLayout.appendChild(_tagPreBtnLayout)
        _tagTitleLayout.appendChild(_tagLayout_search);
        
        let _expandCancelBtn = `
        <div class="note-tag__btn-container">
            <div class="note-divider__column"><div class="note-lnb-divider"></div></div>
            <div class="note__header__column">
                <span class="note-expand-btn icon-work_expand" onclick="noteLnbMenu.switchExpand(event)"></span>
                <span class="note-title-close-btn">
                    <img class="note-lnb_cancel-img" src="./res/note/ts_cancel@3x.png">
                </span>
            </div>
        </div>`;

        _tagTitleLayout.insertAdjacentHTML("beforeend", _expandCancelBtn);
        _tagTitleLayout.querySelector('.note-title-close-btn').addEventListener("click", function() {
            onCloseButtonClick('note',function(){
				noteLnbMenu.setIsExpanded(false);
        	})
        })
        // 여기까지 tagTitle 만들기
        //////////////////////////////////////////////////////////////////////////////

    	_tagLayout.append(_tagDiv.template)
    	targetLayout.append(_tagLayout)
    	UTIL.ID('tagDIV').src('tagLayout.html');
    	$('#tagDIV')[0].style.width = "100%"
        TNoteSearch.addTagKeyEventHandler();
        
        // 태그가 없습니다 페이지 : title 추가해줌(확장/축소 및 닫기 버튼, 뒤로 가기 버튼 있도록)
    	let emptyLayout = document.createElement('div');
        emptyLayout.setAttribute('id','note-tag-empty-div');

        let template = `
            <div style='font-size:1.5rem; font-weight:bold; width:auto; height:1.5rem;'>태그가 없습니다.</div>
            <div style='font-size:1rem; width:auto; height:1rem; padding-top: 1rem;'>페이지 하단에 태그를 입력하여 추가하거나</div>
            <div style='font-size:1rem; width:auto; height:1rem; padding-top: 0.5rem;'>태그 목록을 검색하세요.</div>
            <div id='note-empty-tag-illustration' style='width:100%; height:70%;'></div>`;
			
        emptyLayout.insertAdjacentHTML('beforeend', template);
        // titleLayout 다음으로 삽입해줌
        targetLayout.insertBefore(emptyLayout, _tagLayout);
    }

    function checkTagOpenStatus() {

        if ( tagTreeStatus === "opened" ) {
            UTIL.ID( 'tagTree' ).expandAll()
        } else {
            UTIL.ID( 'tagTree' ).collapseAll()
        }
    }
    function activateMenuButton( targetMenu ) {
//        if ( isMobile() ) {
//            return;
//        }
        // CheckDom 이 필요할지 판단
        TNOTE_MENU_ARRAY.forEach( function ( menu ) {
            if ( menu === targetMenu ) {
                if ( !UTIL.ID( menu ).hasClass( "active" ) ) {
                    UTIL.ID( menu ).addClass( "active" )
                }
                if ( TNOTE_MENU.TAG_TREE === menu || TNOTE_MENU.NOTE_TREE === menu ) {
                    // Tree 에 해당하는 select는 내부 로직에서 수행한다.
                    // UTIL.ID( menu ).selectNode( 0 );
                }
            } else {
                if ( TNOTE_MENU.TAG_TREE === menu || TNOTE_MENU.NOTE_TREE === menu ) {
                    UTIL.ID( menu ).deselectNode();
                }
                UTIL.ID( menu ).removeClass( "active" )
            }
        } );
    }
    function getActivatedMenu( ) {
        for( let index = 0; index<TNOTE_MENU_ARRAY.length; index++){
            if ( UTIL.ID( TNOTE_MENU_ARRAY[index] ).hasClass( "active" ) ) {
                return TNOTE_MENU_ARRAY[index];
            }
        }
    }
    function renderCategory ( layout, tagItemList ){
        UTIL.ID(layout).clear();
        let tagCnt=0;
        if(tagItemList !== undefined){
            for(let i=0;i<tagItemList.length;i++) {
                let _tagListTitle = Top.Widget.create('top-textview');
                let _tagChip = Top.Widget.create('top-chip');
                UTIL.ID(layout).addWidget(_tagListTitle);
                _tagListTitle.setProperties({
                    "text" : tagItemList[i].KEY,
                    "text-color" : "#000000",
                    "text-size" : "1rem",
                    "layout-width" : "match_parent",
                    "layout-height": "2rem",
                    "padding" : "0.313rem 0rem 0rem 1.25rem"
                });
                UTIL.ID(layout).addWidget(_tagChip);
                _tagChip.setProperties({ // 칩에 들어갈 내용은 받아온 태그 리스트 중 KEY 하나가 가진 배열
                    "class" : "tag_layout",
                    "chip-items" : tagItemList[i].tag_indexdto.tagList,
                    "layout-width" : "100%",
                    "border-left-width" : "0px",
                    "readonly":"true",
                    "on-click" : function(event, widget){
                        Top.Controller.get('tagLayoutLogic').setTagChipOnClickEvent(event, widget)
                    }
                });

                let targetDiv =$('div#'+layout+' .top-chip-box');
                let tagList = tagItemList[i].tag_indexdto.tagList;

                for(let index=0; index<tagList.length; index++){
                    let tagId = tagList[index].tag_id;
                    let noteCount = TNoteData.getNoteCountInTag(tagId);
                    let tagNoteNum = document.createElement('p');
                    tagNoteNum.setAttribute("style", "font-size: 0.75rem; margin-left: 0.5rem; font-weight: bold; font-family: NotoSansCJKkr-Bold; align-self: center; color: #008CC8;");
                    tagNoteNum.textContent = noteCount;
                    targetDiv[tagCnt++].appendChild(tagNoteNum);
                }
                let tagEllipsisAddArr = document.querySelectorAll('div.top-chip-content')
                for(let j=0; j< tagEllipsisAddArr.length; j++){ 
                	if(tagEllipsisAddArr[j].innerText.length > 30) tagEllipsisAddArr[j].classList.add("tagEllipsis")
                }

            }
        }
    }

	function renderContextMenu(e) {
        let noteContextMenu = document.getElementById('__noteContextMenu');
        if(noteContextMenu.style.display === 'none'){
    		e.preventDefault();
            e.stopPropagation();

            TNoteUtil.removeChildrenNodes(noteContextMenu);
    		noteContextMenu.style.display = 'block';
			let renameMenu = document.createElement('div');
            renameMenu.classList.add("note-context-menu__rename");
            renameMenu.textContent = "이름 변경";
            noteContextMenu.appendChild(renameMenu);

            let deleteMenu = null;
            if (e.currentTarget.parentElement.parentElement.classList.contains("default")) {
                noteContextMenu.style.height = "auto";
            } else {
                deleteMenu = document.createElement('div');
                deleteMenu.classList.add("note-context-menu__delete");
                deleteMenu.textContent = "삭제";
                noteContextMenu.appendChild(deleteMenu);
            }       
        	
//        	let x = document.body.offsetWidth - document.getElementById('AppSplitRightLayout').getWidth() + e.currentTarget.offsetLeft - noteContextMenu.offsetWidth  + 'px'
//        	let y = e.currentTarget.offsetTop + document.getElementById('AppGnbRightLayout').getHeight() + 'px';
        	noteContextMenu.style.left = e.clientX - noteContextMenu.offsetWidth - 10 + 'px';
            noteContextMenu.style.top = e.clientY + 'px';
        	
        	noteContextMenu.setAttribute('target-content-name',e.currentTarget.parentElement.innerText);
            
            // 챕터일 때
            if (e.currentTarget.parentElement.parentElement.getAttribute('chapter-id')) {
                noteContextMenu.setAttribute('target-content-id',e.currentTarget.parentElement.parentElement.getAttribute('chapter-id'));
                renameMenu.addEventListener("click", function() {
                    noteLnbMenu.clickRenameContextMenu("chapter");
                });

                if (deleteMenu) {
                    deleteMenu.addEventListener("click", function() {
                        let contextMenu = document.getElementById('__noteContextMenu');
                        let dialog = UTIL.ID('noteDeleteDialog');
                        dialog.setProperties({
                            'on-open':function(){
                                UTIL.ID('noteDelTextView').setText('챕터를 삭제하시겠습니까?');
                                UTIL.ID('note_delete_ok').setProperties({
                                    'on-click':function(){
                                        TNoteController.deleteNoteBook(contextMenu.getAttribute('target-content-id'));        						
                                        dialog.close(true);
                                    }
                                })
                            }
                        })
                        UTIL.ID('noteDeleteDialog').open()
                    });
                }

            } 
            // 페이지일 때
            else {
                noteContextMenu.setAttribute('target-content-id',e.currentTarget.parentElement.parentElement.getAttribute('content-id'));
                renameMenu.addEventListener("click", function() {
                    noteLnbMenu.clickRenameContextMenu("page");
                });
                deleteMenu.addEventListener("click", function() {
                                    /*
                    * TODO
                    * 페이지 삭제 다이얼로그 오픈해주기
                    * */
                    let contextMenu = document.getElementById('__noteContextMenu')
                    let dialog = UTIL.ID('noteDeleteDialog');
                    dialog.setProperties({
                        'on-open':function(){
                            UTIL.ID('noteDelTextView').setText('페이지를 삭제하시겠습니까?')
                            UTIL.ID('noteDelTextView_1').setVisible('none');
                            UTIL.ID('note_delete_ok').setProperties({
                                'on-click':function(){
                                    let targetPageId = contextMenu.getAttribute('target-content-id')
                                    let tempData;
                                    let noteData;
                                    let noteType;
                                    if(document.querySelector('li[content-id="'+ targetPageId + '"]').type === 'chapter'){
                                        tempData = TNoteServiceCaller.noteInfoList(targetPageId);
                                        noteType='chapter';
                                    } else {
                                        tempData = TNoteServiceCaller.shareNoteInfoList(targetPageId);             // 임시로 형태만 짜둠
                                        noteType='shared';
                                    }
                                    noteData = tempData[0];
                                    if(noteData.is_edit && noteData.is_edit !== userManager.getLoginUserId()){
                                        let editingUserName = userManager.getUserName(noteData.is_edit)
    //                                    alert(editingUserName+'님이 수정 중 입니다.')
                                        TeeAlarm.open({title: '삭제할 수 없습니다.', content: editingUserName + "님이 수정 중 입니다."});
                                        dialog.close(true);
                                        return;
                                    } else {
                                        let tempObject = {};
                                        tempObject.note_id = targetPageId
                                        TNoteController.deleteNote([tempObject], noteType);
                                        dialog.close(true);
                                    }
                                }
                            })
                        }
                    })
                    UTIL.ID('noteDeleteDialog').open()
                });
            }

            // 이벤트 바인딩
            
        }    
    }

    // chapter랑 page 선택 효과 주기
    function selectChapterPage(targetChapterId, targetPageId=null) {
        let lnbTargetChapter = document.querySelector("ul.note-lnb-ul[chapter-id='"+ targetChapterId +"']");
        
        // select되어 있던 페이지 풀어주기
        let selectedPage = document.querySelector(".note-lnb-li.selected");
        if (selectedPage) {
            selectedPage.classList.remove("selected");
        }

        // select page
        let lnbTargetPage;
        // 두번째 인자 : targetPageId, 없으면 첫번재 페이지
        if (!targetPageId) {
            lnbTargetPage = lnbTargetChapter.querySelector("li.note-lnb-li");
        } else {
            lnbTargetPage = document.querySelector('li.note-lnb-li[content-id="'+targetPageId+'"]');
        }

        // 페이지가 없는 챕터인 경우
        if (lnbTargetPage) {
            lnbTargetPage.classList.add('selected');
            targetPageId = lnbTargetPage.getAttribute("content-id");
        }
        
        // (SJ) TODO. 나중에 renderer 말고 다른 부분에서 set하도록 바꿔야할 듯
        TNoteData.setCurPageData(targetChapterId, targetPageId);
       
        // 챕터 선택
		let selectedChapterList = document.querySelectorAll(".note-lnb_selected-chapter");
		for (let i=0;i<selectedChapterList.length;i++) {
			selectedChapterList[i].classList.remove("note-lnb_selected-chapter");
		}		
		lnbTargetChapter.querySelector('span.note-chapter-text').classList.add("note-lnb_selected-chapter");
    }

    /*
        search 관련 render 함수
    */

   function renderChapterSearchResult(chapterList, targetLayout){	
        let targetColor, chapterItem, chapterItemTemplate;

        for(let i=0; i<chapterList.length; i++){
            targetColor = noteLnbMenu.getRandomColor();
            chapterItem = chapterList[i];
            // chapter-id로 바꿔도 되나?
            chapterItemTemplate = `
                <div class="search-chapter-div" content-id="${chapterItem['id']}">
                    <span class="note-lnb-ul-icon">
                        <span class="note-lnb-chapter-color" style="background-color:${targetColor};color:${targetColor};"></span>
                    </span>
                    <a class="note-lnb-li-text">${chapterItem.text}</a>
                </div>
            `
            targetLayout.insertAdjacentHTML("beforeend", chapterItemTemplate.trim());
        }
        
        let chapterItems = document.querySelectorAll(".search-chapter-div");
        Array.from(chapterItems).forEach(chapterItem => {
            chapterItem.addEventListener("click", function(e) {
		        let targetChapterId = e.currentTarget.getAttribute("content-id");
                TNoteSearch.clickLnbChapterEvent(e, targetChapterId);
            })
        })

        return new Promise((res, rej) => {
            res(targetLayout);
        });
    }

    function renderPageSearchResult(pageList, targetLayout){
        for(let i=0; i<pageList.length; i++){
            if(pageList[i].length!==0){
                for(let j=0; j<pageList[i].length; j++){
                    let targetData = pageList[i][j];
                    let pageItemTemplate = `
                        <div class="search-page-div" content-id="${targetData.id}">
                            <div class="note-search_page-item">${targetData.text}</div>
                            <div class="note-search_page-parent">${targetData.parent_name}</div>
                        </div>
                    `;
                    targetLayout.insertAdjacentHTML("beforeend", pageItemTemplate.trim());
                }
            }
        };

        // 생성 후 한꺼번에 이미지 달기
        let pageItems = document.querySelectorAll(".search-page-div");
        Array.from(pageItems).forEach(pageItem => {
            pageItem.addEventListener("click", function(e) {
                noteLnbMenu.liNodeClickEventHandler(e, this);
            })
        });        
    }
    
    return {
        initRender: initRender,
        initEditorRender:initEditorRender,
        initTagRender:initTagRender,
//        renderAllNote: function ( targetNoteList ) {
//            activateMenuButton( TNOTE_MENU.TOTAL_NOTES );
//            let selectedNoteId = TNoteData.getSelectedNoteId();
//            let option = _noteListView.renderOptionBuilder().setNewButtonVisible().build();
//            _noteListView.renderListView( "전체 페이지", targetNoteList, option );
//        },

        renderPage : function( targetData ){
        	if(!noteLnbMenu.isExpanded()){
        		noteLnbMenu.setLnbVisibility('none')
                noteLnbMenu.setEditorVisibility('flex')
                noteLnbMenu.setFoldingVisibility('none')
                noteLnbMenu.setTagVisibility('none')
                document.getElementById('notePreBtn').style.display = "inline"
                TNoteController.editorEventBind();
        		noteLnbMenu.setEditorTitleContainer("none")
        	}else{
                noteLnbMenu.setEditorVisibility('flex')
                noteLnbMenu.setFoldingVisibility('flex')
                noteLnbMenu.setTagVisibility('none')
                noteLnbMenu.setEditorTitleContainer("flex")
        	}
        },
        selectChapterPage:selectChapterPage,
        renderTagMenu: function () {
        	
        	for(let i=0; i<4; i++){
				UTIL.ID('tag_accordion').selectTab(i)
			}
        	let changeIcon = document.querySelectorAll('#note .top-accordionlayout-title .top-accordionlayout-icon')
        	for(let i=0; i< changeIcon.length; i++){
        		changeIcon[i].classList.add('icon-arrow_up')
        		changeIcon[i].classList.remove('top-accordionlayout-icon')
        	}
        	
        },
        renderTag: function ( tagName, targetNoteList ) {
        	// targetNoteList로 LNB Update
        },
        renderContextMenu:renderContextMenu,
        renderLNBNoteBookTree: function ( noteBookList ) {
        	/**
        	 * ChapterList Set
        	 */
        },
        renderLNBTagTree: function ( tagList ) {
            UTIL.ID( 'tagTree' ).addNodes( 'tagroot', tagList );
            checkTagOpenStatus();
        },
        toggleTagTreeStatus: function () {
            tagTreeStatus = ("closed"===tagTreeStatus ) ? "opened" : "closed";
        },
        renderCategory : renderCategory,
        selectTagTreeById: function( targetId ){
            Top.Dom.selectById('tagTree').selectNode(targetId);
            Top.Dom.selectById('tagTree').trigger("click");
        },
        getActivatedMenu:getActivatedMenu,

        /*
            search 관련 render 함수
        */
        renderChapterSearchResult:renderChapterSearchResult,
        renderPageSearchResult:renderPageSearchResult
    }
} )();
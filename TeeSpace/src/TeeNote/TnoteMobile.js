TNoteMobile = (function(){
	
	function setMode(type){
	        switch(type){
		    case "listview" :
		        if(!$('div#noteLNB').hasClass("lnb-hide-mode")){
		            $('div#noteLNB').addClass('lnb-hide-mode');
		            $('div#content_editor').addClass('editor-hide-mode');
		            $('div#content_list').addClass('listview-wide-mode');
		            $('div#contents_title').addClass('listview-wide-mode');
		        }else if($('div#noteLNB').hasClass("lnb-hide-mode")){
		        	$('div#content_editor').addClass('editor-hide-mode');
		            $('div#content_list').addClass('listview-wide-mode');
		            $('div#contents_title').addClass('listview-wide-mode');
		        }
		    break;
		    case "editor" :
		        if($('div#content_list').hasClass('listview-wide-mode')){
		            $('div#content_editor').removeClass('editor-hide-mode');
		            $('div#content_list').removeClass('listview-wide-mode');
		            $('div#contents_title').removeClass('listview-wide-mode');
		            $('div#content_editor').addClass('editor-wide-mode');
		            $('div#content_list').addClass('listview-hide-mode');
		            //11-26
		            UTIL.ID('contents_title').setVisible("none");
		            UTIL.ID('content_main').setProperties({'layout-height' : "100%"});
		            setTimeout(function(){
		                UTIL.ID('noteUpperMenu').setVisible('none')
		            },1)
		        }
		    break;
		    case "first" :
		        if($('div#noteLNB').hasClass("lnb-hide-mode")){
		            $('div#noteLNB').removeClass('lnb-hide-mode');
		            $('div#noteLNB').addClass('lnb-wide-mode')
		            $('div#content_editor').removeClass('editor-hide-mode');
		            $('div#content_list').removeClass('listview-wide-mode');
		        }
		    break;
		    case "empty" :
		        if(!$('div#noteLNB').hasClass("lnb-hide-mode")){
		            $('div#noteLNB').addClass('lnb-hide-mode');
		        }
		    break;
		    case "table":
		        if(!$('div#noteLNB').hasClass("lnb-hide-mode")){
		            $('div#noteLNB').addClass('lnb-hide-mode');
		            $('div#noteContent').addClass('lnb-wide-mode');
		        }
		    break;
		}
	}
	function getMode(){
		if($('div#content_editor').hasClass("editor-hide-mode") && $('div#content_list').hasClass("listview-wide-mode") && $('div#noteLNB').hasClass("lnb-hide-mode")) return "listview"
		if($('div#content_editor').hasClass("editor-wide-mode") && $('div#content_list').hasClass("listview-hide-mode") && $('div#noteLNB').hasClass("lnb-hide-mode")) return "editor"
		if($('div#noteLNB').hasClass("lnb-hide-mode") && $('div#noteContent').hasClass('lnb-wide-mode')) return "table"
		else return "LNB"
	}
	function keyboardLayout(){

			$('.sun-editor .sun-editor-id-toolbar').css('position','fixed')
			$('.sun-editor .sun-editor-id-toolbar').css('top', $('#mobileLnbLayout').height() + $('#content_title').height() + $('#editorTitle').height())
    		$('#sun_editorEdit').css('overflow','scroll')
    		$('#sun_editorEdit').css('position','fixed')
	    	$('#sun_editorEdit').css('top', $('#mobileLnbLayout').height() + $('#content_title').height() + $('#editorTitle').height() + $('.sun-editor .sun-editor-id-toolbar').height())
	    	//11-26
    		$('#sun_editorEdit').css('height', $('div.sun-editor-container').height() - $('div.sun-editor-id-toolbar.sun-editor-common').height());
			// 태그 영역 보이는 부분 안보이는 부분 
			$(window).resize(function (){
		    	if($('#LinearLayout15').height() > 500){
		    		$('#LinearLayout15')[0].style.height = "calc(100% - 96px)"
		    		$('#LinearLayout66').show()
		    	}else if($('#LinearLayout15').height() < 500){
		    		if(UTIL.ID('tagTextField').getVisible() == "none"){
		    			$('#LinearLayout15')[0].style.height = "calc(100% - 50px)"
		    			$('#LinearLayout66').hide()
		    		}else if(UTIL.ID('tagTextField').getVisible() == "visible"){
		    			$('#LinearLayout15')[0].style.height = "calc(100% - 96px)"
		    			$('#LinearLayout66').show()			    			
		    		}	
		    	}
			})
	}

	return {
		setMode:setMode,
		getMode:getMode,
		keyboardLayout:keyboardLayout
	}
})();
TNoteSearch = ( function () {
	
	function searchItem(searchString, searchTargetList, searchKey){
		return searchTargetList.filter(function(node){
			return node[searchKey].toLowerCase().indexOf(searchString.toLowerCase())!==-1;
		})
	}
	
	function setSearchResultPage(text){
		setLnbMenusVisibility('none')
		$('#note-lnb-upper-menu').after('<div id="searchResultLayout"><div class="empty_div"><span class="resultText">'+ text +'</span></div></div>')
	}
	function setLnbMenusVisibility(value){
		$('div#searchResultLayout').remove()
		document.getElementById('note-chapter-cover').style.display = value;
		document.getElementById('note-tag-cover').style.display = value;
//		document.getElementById('note-shared-page-cover').style.display = value;
//		document.getElementById('note-shared-chapter-cover').style.display = value;
	}
	
	function search(){
		$('#note-lnb-new-chapter').attr("disabled",true) // 일단 새챕터  버튼은 disabled처리 해준다.
		let searchString = document.getElementById('note-lnb-search-textfield').value;
		let searchTargetChapterList = TNoteServiceCaller.noteChapterList();
		let searchChapterResult = [];
		let searchPageResult = [];
		
		searchChapterResult = searchItem(searchString, searchTargetChapterList, 'text');
		for(let i=0; i<searchTargetChapterList.length; i++){
			for(let j=0; j<searchTargetChapterList[i].children.length; j++){
				searchTargetChapterList[i].children[j].parent_name = searchTargetChapterList[i].text;
			}
			if(searchItem(searchString, searchTargetChapterList[i].children, 'text').length !== 0)
			searchPageResult.push(searchItem(searchString, searchTargetChapterList[i].children, 'text'))
		}
		
		if(searchChapterResult.length === 0 && searchPageResult.length === 0){
			setSearchResultPage('검색 결과가 존재하지 않습니다.')
			let resultImage = document.createElement("img");
			resultImage.src = './res/note/img_no_results.png'
			$('.empty_div').append(resultImage)
		} else {
			setSearchResultPage('')
			$('.empty_div').remove();

			// (sj) 꼭 chapter 먼저 나와야할 수도 있으므로 chapterList 다 그린 후 page 그리도록 수정
			if(searchChapterResult.length !== 0) {
				TNoteRenderer.renderChapterSearchResult(searchChapterResult, document.getElementById('searchResultLayout'))
					.then((targetLayout)=> {
						if(searchPageResult.length !== 0)	TNoteRenderer.renderPageSearchResult(searchPageResult, targetLayout)
					})
			} else {
				if(searchPageResult.length !== 0)	TNoteRenderer.renderPageSearchResult(searchPageResult, document.getElementById('searchResultLayout'));
			}
		}
	}
	
	function addKeyEventHandler(){
		$('input#note-lnb-search-textfield').off().on('keyup', function(event){
			if(event.keyCode === 13) {
				if(document.getElementById('note-lnb-search-textfield').value=== ""){
					return;
				} else {
					setSearchResultPage('검색 실행중입니다.')
					let loadingImage = document.createElement("img");
					loadingImage.src = './res/note/loading_taka.gif';
					loadingImage.classList.add("note-lnb_loading-img");
					document.querySelector('.empty_div').appendChild(loadingImage);
					setTimeout(function(){
						search();
					},500)
				}
//			} else if(event.keyCode === 27 || event.keyCode === 8 && document.getElementById('note-lnb-search-textfield').value === "") {  ESC는 기획에 없던 거니까 뺌
			} else if(event.keyCode === 8 && document.getElementById('note-lnb-search-textfield').value === "") { 
				document.getElementById('note-lnb-search-textfield').value = "";
				setLnbMenusVisibility('block')
				$("#note-lnb-new-chapter").removeAttr("disabled");
			}
		})
		$('#note-lnb-search .icon-search').off().on('click',function(e){search()});
	}
	
	function Tagsearch(){
	let searchString = document.getElementById('note-Tag-search-textfield').value;
	let searchTargetTagList = TNoteServiceCaller.alltagList()
	let tagKeywordList = TNoteServiceCaller.tagSortList() 
	
	searchTagResult = searchItem(searchString, searchTargetTagList, 'text');

	if(searchTagResult.length == 0){
		setTagSearchResultPage('검색 결과가 존재하지 않습니다.')
		let resultImage = document.createElement("img");
		resultImage.src = './res/note/img_no_results.png'
		$('.Tagempty_div').append(resultImage)
	} else{
		// 검색된 tagKeywordList 만들기
		for(let i=0;i<tagKeywordList.length;i++){ 
			for(let j=0;j<tagKeywordList[i].tag_indexdto.tagList.length;j++){
				let search = tagKeywordList[i].tag_indexdto.tagList[j].text.toLowerCase().indexOf(searchString.toLowerCase())
				if(search === -1){
					tagKeywordList[i].tag_indexdto.tagList.splice(j,1)
					j--
				}
			}
			if(tagKeywordList[i].tag_indexdto.tagList.length === 0){
				tagKeywordList.splice(i,1)
				i--
			}
		}
		
        setTagSearchResultPage('')
        $('.Tagempty_div').remove();
        for(let i=0; i<4; i++){//Tab 초기화
			UTIL.ID('tag_accordion').closeTab(i)
		}
        settagLayoutVisibility('block')
        TNoteData.setAllTagList(searchTagResult);
        let SortedTagList = TNoteData.setSortedTagList(tagKeywordList);
        if(SortedTagList.tag_kor_tablayout.length == 0)$('top-accordiontab#tag_kor_tab').css('display',"none");
        else $('top-accordiontab#tag_kor_tab').css('display',"block");
        if(SortedTagList.tag_eng_tablayout.length == 0)$('top-accordiontab#tag_eng_tab').css('display',"none");
        else $('top-accordiontab#tag_eng_tab').css('display',"block");
        if(SortedTagList.tag_num_tablayout.length == 0)$('top-accordiontab#tag_num_tab').css('display',"none");
        else $('top-accordiontab#tag_num_tab').css('display',"block");
        if(SortedTagList.tag_etc_tablayout.length == 0)$('top-accordiontab#tag_etc_tab').css('display',"none");
        else $('top-accordiontab#tag_etc_tab').css('display',"block");
        TNoteRenderer.renderTagMenu();
	}
	
	}
	function setTagSearchResultPage(text){
		settagLayoutVisibility('none');
		$('#note-tag-tagTitleLayout').after('<div id="TagsearchResultLayout"><div class="Tagempty_div"><span class="resultText">'+ text +'</span></div></div>')	
	}
	function settagLayoutVisibility(value){
		$('div#TagsearchResultLayout').remove()
		//document.getElementById('tagLayout').style.display = value
	}
	function addTagKeyEventHandler(){
		$('input#note-Tag-search-textfield').off().on('keyup', function(event){
			if(event.keyCode === 13) {
				if(document.getElementById('note-Tag-search-textfield').value=== ""){
					return;
				} else{
					setTagSearchResultPage('검색 실행중입니다.')
					let loadingImage = document.createElement("img");
					loadingImage.src = './res/note/loading_taka.gif';
					loadingImage.classList.add("note-lnb_loading-img");
					document.querySelector('.Tagempty_div').appendChild(loadingImage);
					setTimeout(function(){
						Tagsearch();
					},500)
				}
			} else if(event.keyCode === 27 || event.keyCode === 8 && document.getElementById('note-Tag-search-textfield').value === "") {
				TNoteController.selectTagMenu()
			} 
		})
		$('#note-Tag-search .icon-search').off().on('click',function(e){Tagsearch()});
	}

	// search result인 chapter 클릭 이벤트
	function clickLnbChapterEvent(e, targetChapterId) {
		let lnbTargetChapter = document.querySelector("ul.note-lnb-ul[chapter-id='"+ targetChapterId +"']");
		let lnbTargetPage = lnbTargetChapter.querySelector(".note-lnb-li");
		let targetPageId;
		
		// 페이지가 없는 경우
		if (!lnbTargetPage) {
			TNoteRenderer.renderPage();
			TNoteRenderer.selectChapterPage(targetChapterId, null);
			return
		} 

		// 페이지가 있는 경우 첫 번째 페이지를 보여준다
		// search cancel button 코드 가지고 옴
		document.getElementById('note-lnb-search-textfield').value = "";
		// lnb 보이기
		TNoteSearch.setLnbMenusVisibility('block')
		$("#note-lnb-new-chapter").removeAttr("disabled");
		
		lnbTargetChapter.scrollIntoView({behavior:"smooth", block:"center"});

		// 두번째 인자 : targetPageId, 없으면 첫번재 페이지
		TNoteRenderer.selectChapterPage(targetChapterId, null);
		targetPageId = lnbTargetPage.getAttribute("content-id");
		
		// editor에 해당 페이지 띄워주기
		TNoteController.selectPageMenu(targetPageId);
	}

    return {
    	search:search,
    	setLnbMenusVisibility:setLnbMenusVisibility,
    	setSearchResultPage:setSearchResultPage,
    	addKeyEventHandler:addKeyEventHandler,
    	Tagsearch:Tagsearch,
    	setTagSearchResultPage:setTagSearchResultPage,
    	settagLayoutVisibility:settagLayoutVisibility,
		addTagKeyEventHandler:addTagKeyEventHandler,
		clickLnbChapterEvent:clickLnbChapterEvent
    }
})();

TNoteData = ( function () {


    let allTagList = [];
    // let allTagListData = mobx.observable.array();
    // let allNoteMapData = mobx.observable.map();
    let noteBookList = [];
    let fileList = [];
    let currentChapterList = [];
    let currentNoteList = [];
    let currentNoteBookList = [];
    let defaultNotebookId = null;
    let selectedNoteId={};
	let selectedMultipleNoteList = [];
	let selectedMultipleNoteObj={};
    let searchTargetNoteList = [];
    let stackMap = new Map();
    let makeList = [];

    // for TagRender
    let sortedTagListSet ={};

    // pageData를 인자로 받았는데 chapterId, pageId만 써서 인자 고침
    function setCurPageData(targetChapterId, targetPageId) {
        selectedMultipleNoteList = [];
        selectedNoteId.note_id = targetPageId;
        selectedNoteId.parent_notebook = targetChapterId;
        // (SJ) 일단 임시, setEditorVisibility쪽 보고 고치기
        if (!targetPageId) {
            document.getElementById('note-empty-div').style.display = "block";
            document.querySelector('top-linearlayout#editorDIV').style.display = "none";
            document.getElementById('note-editor-container').setAttribute('rendered-page-id', null);
        } else {
            document.getElementById('note-empty-div').style.display = "none";
            document.querySelector('top-linearlayout#editorDIV').style.display = "flex";
            document.getElementById('note-editor-container').setAttribute('rendered-page-id', targetPageId);
        }
        
    }

    function setMultiplePageData( targetPageList ){
    	selectedNoteId = {};
    	selectedMultipleNoteList = [];
    	if(targetPageList.length !== 0){
    		for(let index=0; index < targetPageList.length; index++){
    			selectedMultipleNoteObj = {};
    			selectedMultipleNoteObj.id = targetPageList[index].getAttribute("content-id")
    			selectedMultipleNoteObj.text = targetPageList[index].innerText;
    			selectedMultipleNoteList.push(selectedMultipleNoteObj)
    		}
    	}else{
    		selectedMultipleNoteList = [];
    	}
    }
    function checkDuplicateChapter (input){
    	currentNoteBookList = TNoteServiceCaller.noteChapterList();
    	let result = currentNoteBookList.filter(function(obj){return obj["text"] === input})
    	count = result ? result.length : 0;
    	if(count === 0) return false;
    	else return true;
    }
    
    function makeSnapShotData(){
    	let checkList = document.querySelectorAll('#note-chapter-cover .note-lnb-ul .note-lnb-ul-div');
    	let checkColorList = document.querySelectorAll('#note-chapter-cover .note-lnb-ul .note-lnb-ul-div .note-lnb-chapter-color');
    	if(checkList){
    		for(let index=0; index< checkList.length; index++){
    	    	let makeObj = {};
    			makeObj.id = checkList[index].getAttribute("chapter-id");
    			makeObj.color = checkColorList[index].style.backgroundColor;
    			makeList.push(makeObj);
    		}
    	}

    	TNoteChaperShorter.updateSorter(noteChannel_Id, makeList);
    	makeList = [];    	
    }
    
//    function setNoteBookListData( targetNoteBookList ) {
//        noteBookList = checkNoteBookListData( targetNoteBookList );
//        return noteBookList;
//    }

//    function checkNoteBookListData( noteBookList ) {
//        // noteBookList에는 TotalNoteBook의 ID가 필요하다. 해당 항목은 그려서는 안된다.
//        // Splice를 하는 이유
//
//        let totalNoteBook = noteBookList.find( function ( notebook ) {
//            return ( notebook[ NOTEBOOK_DTO_KEY.TYPE ] === "allNote" );
//        } );
//        noteBookList.splice( noteBookList.indexOf( totalNoteBook ), 1 );
//
//
//        // noteBookList에는 render에 필요한 id가 이미 포함되어 있어서 tag랑은 다르다.
//        // DefaultNoteBook ID는 Event를 위해 Set한다.
//        defaultNotebookId = totalNoteBook[ NOTEBOOK_DTO_KEY.ID ];
//        // 전역 변수로 갖고 있길래 set해줬다.
//
//        return noteBookList;
//    }


    function setAllTagList( targetTagList ) {
        allTagList = checkTagListData( targetTagList );
        return allTagList;
        // loop
        //addTagCount
    }
    function checkTagListData( tagList ) {
        let targetTagList = [];
//        TNoteUtil.log( " INPUT TagList= ", tagList );

        // tagList가 중복으로 정렬되어서 도착하기에 중복을 제거한다.
        // tagList를 그리기 위해서는 id라는 항목이 필요해서 data[id]를 Set한다.
        // tagList[index].id =  이렇게 구현해도 되지만 undefined 오류를 회피하기 위해 [][]를 사용했다.
        let uniqueTagIndex = 0;
        for ( let index = 0; index < tagList.length; index++ ) {
            let thisTag = tagList[index];
            let targetNoteId = thisTag["note_id"];
            let noteIndex = currentNoteList.findIndex( function( targetNote ){
                return targetNote["note_id"]===targetNoteId;
            });

            if ( ( index > 0 && tagList[ index - 1 ][ "tag_id" ] !== tagList[ index ][ "tag_id" ] ) || index === 0 ) {
                uniqueTagIndex = index;
                tagList[ index ][ "NOTE_COUNT" ] = 0;
                if( noteIndex >= 0){
                    tagList[ index ][ "NOTE_COUNT" ] ++;
                }
                // 직전 TAG랑 다를 경우
                tagList[ index ][ "type" ] = "tag";
                tagList[ index ][ "id" ] = tagList[ index ][ "tag_id" ];
                targetTagList.push( tagList[ index ] );
            }else{
                if( noteIndex >= 0){
                    targetTagList[targetTagList.length -1 ]["NOTE_COUNT"]+=1;
                }
            }
        }
        return targetTagList;
    }

    function getNoteCountInTag( tagId ) {
        let targetTag = allTagList.find( function( tag ){
            return tag["tag_id"] === tagId;
        });
        return targetTag?targetTag["NOTE_COUNT"]:0;
        // loop
        //addTagCount
    }

    function setNoteList( targetNoteList, ignoreFilter ) {
//        if( !ignoreFilter ){
//            targetNoteList= targetNoteList.filter(function(targetNote) {
//                return targetNote["is_erased"] !== 1;
//            });
//        }
        currentNoteList=targetNoteList;
        return currentNoteList;
    }
    
    function setFileList(targetFileList){
        fileList = targetFileList
    	return fileList;
    }
    function setSortedTagList( sortedTagList ){

        sortedTagListSet={};

        sortedTagList.sort(function (a, b){
            return a.KEY.charCodeAt(0) - b.KEY.charCodeAt(0);
        });

        sortedTagListSet[TAG_CATEGORY.KOR]=[];
        sortedTagListSet[TAG_CATEGORY.ENG]=[];
        sortedTagListSet[TAG_CATEGORY.NUM]=[];
        sortedTagListSet[TAG_CATEGORY.ETC]=[];

        for(i=0;i<sortedTagList.length;i++) {
            // 추후에 serviceCall이 정리 되면 이 부분은 지워도 된다.
            sortedTagList[ i ].tag_indexdto.tagList = TNoteUtil.checkSameAndRemove( sortedTagList[ i ].tag_indexdto.tagList, "tag_id" );

            if( sortedTagList[i].KEY.charCodeAt(0) >= 12593 && sortedTagList[i].KEY.charCodeAt(0) < 55203 ){
                sortedTagListSet[TAG_CATEGORY.KOR].push(sortedTagList[i]);
            }
            else if(sortedTagList[i].KEY.charCodeAt(0) > 64 && sortedTagList[i].KEY.charCodeAt(0) < 123){
                sortedTagListSet[TAG_CATEGORY.ENG].push(sortedTagList[i]);
            }
            else if(sortedTagList[i].KEY.charCodeAt(0) >= 48 && sortedTagList[i].KEY.charCodeAt(0) <= 57){
                sortedTagListSet[TAG_CATEGORY.NUM].push(sortedTagList[i]);
                // sortedTagListSet["NUM"][numTagCount++] = sortedTagList[i];
            }
            else {
                sortedTagListSet[TAG_CATEGORY.ETC].push(sortedTagList[i]);
            }
        }
        return sortedTagListSet;
    }
    function setParentNameData(){
    	currentChapterList = TNoteServiceCaller.noteChapterList();
    }
    function getParentNameByParentId(p_id){
    	let targetParent = currentChapterList.find(function(obj){return obj.id===p_id})
    	return targetParent.text;
    }

    return {
//        setNoteBookList: function ( noteBookList ) {
//            return setNoteBookListData( noteBookList );
//        },
//        getNoteBookList: function () {
//            return noteBookList;
//        },
        setNoteList: setNoteList,
        getNoteList: function () {
            return currentNoteList;
        },
        setCurPageData :setCurPageData,
        getCurPageData :function (){
        	return selectedNoteId;
        },
        setMultiplePageData :setMultiplePageData,
        getMultiplePageData : function(){
        	return selectedMultipleNoteList;
        },
        setAllTagList: setAllTagList,
        getAllTagList: function () {
            return allTagList;
        },
        setParentNameData:setParentNameData,
        getParentNameByParentId:getParentNameByParentId,
//        setFileList: setFileList,
//        getFileList : function(){
//        	return fileList
//        },
//        getDefaultNoteBookId : function(){
//            return defaultNotebookId;
//        },
//        getSelectedNoteId: function(){
//            return selectedNoteId;
//        },
//        setSelectedNoteId: function( targetNoteId ){
//            selectedNoteId=targetNoteId;
//        },
//        getSearchNoteList: function(){
//            return currentNoteList;
//        },
//        setSearchNoteBookList: function( targetNoteBookList){
//            currentNoteBookList=targetNoteBookList;
//        },
//        getSearchNoteBookList: function(){
//            return currentNoteBookList;
//        },
//        isFavoriteNote : function ( noteId ){
//            let targetNote = currentNoteList.find( function ( targetNote ){
//                return ( targetNote["note_id"]===noteId )
//            });
//            return !!targetNote["USER_ID"];
//        },
//        setFavoriteNote : function( noteId , boolFavorite){
//            let targetNote = currentNoteList.find( function ( targetNote ){
//                return ( targetNote["note_id"]===noteId )
//            });
//            if( boolFavorite){
//                targetNote["USER_ID"]="ON";
//            }else{
//                targetNote["USER_ID"]=null;
//            }
//        },
        getNoteCountInTag:getNoteCountInTag,
        setSortedTagList:setSortedTagList,
        getSortedTagSet:function (languageType) {
            return sortedTagListSet[languageType];
        },
        checkDuplicateChapter:checkDuplicateChapter,
        makeSnapShotData:makeSnapShotData


    }

} )();
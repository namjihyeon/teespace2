var dragNote_id;
Top.Controller.create('noteLnbListLayoutLogic', {
    setListViewDraggable : function(event, widget){
    	let listItem = document.querySelectorAll('.tmp-li')
//    	let listTextView = document.querySelectorAll('.multi-line')
    	for(i=0; i<listItem.length; i++){
    		listItem[i].setAttribute("draggable",true)
    		if(listItem[i].getAttribute("ondragstart") == null){
    			listItem[i].setAttribute("ondragstart", 'NoteDragDrop.onDragStart(event)')
    		}
    	}
//    	for(i=0; i<listTextView.length; i++){
//    		if(listTextView[i].getAttribute("onmouseenter") == null){
//    			listTextView[i].setAttribute("onmouseenter", 'NoteDragDrop.onMouseEnter(event)')
//    		}
//    	}
    },
    setTreeDropZone : function(event, widget){
    	let treeNotebookItem = document.querySelectorAll('.top-treeview-item')
    	
    	for(i=0; i<treeNotebookItem.length; i++){
    		if(treeNotebookItem[i].getAttribute("ondragenter") == null){
    			treeNotebookItem[i].setAttribute("ondragenter", 'NoteDragDrop.onDragEnter(event)')
    		}
    		if(treeNotebookItem[i].getAttribute("ondragleave") == null){
    			treeNotebookItem[i].setAttribute("ondragleave", 'NoteDragDrop.onDragLeave(event)')
    		}
    		if(treeNotebookItem[i].getAttribute("ondrop") == null){
    			treeNotebookItem[i].setAttribute("ondrop", 'NoteDragDrop.onDropTree(event)')
    		}
    	}
    },
    removeTreeDropZone : function(event,widget){
    	let treeNotebookItem = document.querySelectorAll('.top-treeview-item')
    	
    	for(i=0; i<treeNotebookItem.length; i++){
    		if(treeNotebookItem[i].getAttribute("ondragenter") != null){
    			treeNotebookItem[i].removeAttribute("ondragenter")
    		}
    		if(treeNotebookItem[i].getAttribute("ondragleave") != null){
    			treeNotebookItem[i].removeAttribute("ondragleave")
    		}
    		if(treeNotebookItem[i].getAttribute("ondrop") != null){
    			treeNotebookItem[i].removeAttribute("ondrop")
    		}
    	}
    },
    isPermissionAvailable : function(note_id){
        let result ;
        Note.getNoteData(note_id, function(data){       
               result =  data.is_favorite === "none" ? true : false
        })
        return result;
    },
    
    
//    removeModalExpandMode : function(){
//        var calWidth = $('#contents_title').width() - 230;
//    	$('.noteEditor_overlay1').css({"width": $('#mainContentsLayout').width() - $('#content_editor').width(), "height":"100%"})
//        $('.noteEditor_overlay2').css({"width": calWidth, "height":$('#contents_title').height(), "margin-left": 230 });
//    }
});
var newDropdown = [
    {"id":"stack","type":"stack","text":"새 북"},
    {"id":"notebook","type":"notebook","text":"새 챕터"}
]
TNoteTableView = (function() {
    function init( title, tableData, type ){
    	if(type === "file"){
    		UTIL.ID('noteContent').src('./noteFileLayout.html'+verCsp())
    		tableData = setTableOption(type, tableData);
    		renderTableView( title, tableData, type )
    	}else {
            UTIL.ID("noteContent").src("./noteTableLayout.html", function(){
            	$('div#table_title_rest_area').append('<span id="table_search_button" style="margin-left:5px; font-size:18px;" class="icon-search" onclick="TNoteSearch.toggleTableViewSearch()"></span>')
            });
            Note.checkDOM("div#noteTableLayout", function(){
                UTIL.setProperty('notebookTable',{
                    "column-option" : stack_table_option
                });
                setTableOption(type);
                renderTableView( title, tableData, type );
            });    		
    	}
    }
    function renderTableView( title,  tableData, type ){
//        TNoteUtil.log(" TABLE_DATA ", tableData);

        if(type === "menu" || type === "stack"){
        	UTIL.ID('bindingText').setText(title);
            UTIL.ID('notebookMoveBtn').setDisabled(true);
            UTIL.ID("noteDeleteBtn").setDisabled(true);
            UTIL.ID("favoriteBtn").setDisabled(true);
            UTIL.ID("notebookTable").template.emptyMessage = "챕터가 없습니다. 챕터 목록을 생성하려면, [새 챕터]버튼을 눌러 주세요.";
            if(isMobile()){
                UTIL.ID("notebookTable").template.emptyMessage = "챕터가 없습니다." + "</br>" + "챕터 목록을 생성하려면, [새 챕터]버튼을 눌러 주세요.";
            }
            noteRepo.setValue('noteTableList', tableData);
            UTIL.ID('notebookTable').render();
            noteRepo.noteTableList.length === 0 ? UTIL.ID("rootCheckBox").setDisabled(true) : UTIL.ID("rootCheckBox").setDisabled(false);
            _ctrLnbList.setTreeDropZone()
            NoteDragDrop.onTableDrag()
//            if($('#dragSpan').length == 0) $("#notebookTable").append('<span id="dragSpan" class="top-treeview-draggedNode">')
        }
    	if(type === "file")	{
    		if(tableData === null) tableData = [];
    		noteRepo.setValue('noteFileList', tableData)
    	}
        
        if (isMobile()) {
            if (Top.App.isWidgetAttached('notebookTable')) {
                var notebookTable = Top.Dom.selectById('notebookTable');
                notebookTable.setColumnsVisible(3, false);
                TNoteMobile.setMode("table")
                //10-16 임시 코드. 모바일에선 타이틀 영역에 로고를 제거한다.
                UTIL.ID('note-text-logo').setVisible('none')
            }
        }
    }
    function setTableOption(type, tableData){
    	switch(type){
			case "menu":
	            UTIL.setProperty('noteDrop',{"items":newDropdown,"on-select":dropdownSelect})
	            UTIL.setProperty('dropLayout',{"visible":"visible"});
	            UTIL.setProperty('bookcreateBtn',{"visible":"none"});
	            UTIL.ID('noteDrop').close()
				break;
			case "stack":
	            UTIL.setProperty('dropLayout',{"visible":"none"});
	            UTIL.setProperty('bookcreateBtn',{"visible":"visible"});
				break;
			case "file":
				 let fileInfoList = tableData.StorageFileOutput[0].storageFileInfoList;
				 let fileNoteList = tableData.noteList
				 if(tableData.StorageFileOutput[0].resultMsg === "Success" && fileInfoList !== null){
					 for(var i=0; i <fileInfoList.length; i++){
						 fileInfoList[i].FileiconName = getFileIconClass(fileInfoList[i].file_extension);
						 fileInfoList[i].setFileSize = getFileSizeText(fileInfoList[i].file_size);
						 fileInfoList[i].file_last_update_user_name = userManager.getUserName(fileInfoList[i].file_last_update_user_id);
						 fileInfoList[i].fileFullName = String.format("{0}.{1}", fileInfoList[i].file_name, fileInfoList[i].file_extension);
					 }
					 // note List에 따른 노트 정보
					 for(var i=0; i < fileNoteList.length; i++){
						 fileInfoList[i].note_id = fileNoteList[i].note_id;
						 fileInfoList[i].note_title = fileNoteList[i].note_title;
						 fileInfoList[i].parent_notebook = fileNoteList[i].parent_notebook; 
					 }
				 }
				return fileInfoList;
    	}
    }
    function dropdownSelect(event,widget) {
        var menuId = widget.getSelectedMenuId();
        if(menuId == "notebook"){
            type = widget.getSelectedMenuData().type;
            UTIL.ID('notebookAddDialog').open()
            UTIL.ID('notebookAddDialog').addClass('noteappDialog')
            _ctrEvent.setDialogInputBind("notebook");
        }else if (menuId == "stack"){
            type = widget.getSelectedMenuData().type;
            UTIL.ID('notebookStackAddDialog').open()
            UTIL.ID('notebookStackAddDialog').addClass('noteappDialog')
            _ctrEvent.setDialogInputBind("notebook");
        }
    }
    
    return{
        renderTableView: function( title, tableData, type ){
            // 다시 그리지 않게 하기
            // 추후 최적화 하면 좋겠다.
            if(!UTIL.ID('noteTableLayout')) {
                init( title,  tableData, type );
            }else{
            	if(UTIL.ID('noteTableLayout') && !UTIL.ID('noteFileLayout') && type === "file") {
            		init( title,  tableData, type );
            	}else{
                	setTableOption(type, tableData);
                    renderTableView( title,  tableData, type );           		
            	}
            }
        },
        setTableOption:setTableOption
    }
})();
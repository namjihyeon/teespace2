var ws_id ;
var p_Id;
var note_id;
//var CheckboxList = [];
var sortFlag = 1
var type ;
var editor;
var tagName;
//var inputCheck;
//var note_del_type
//var Sharecount = 0 ;
var curTagId
var curDialog;
//var etcNoteID;
//var delChildList;
//var noteUpdateFrom; // "tag", "trashcan", "notebook", "allnote"
var curTime;
var fileId;
//var tagTreeStatus = "closed"; // "opened", "closed"

Top.Controller.create('TeeSpaceLogic', {
	createNote : function(event, widget) {
		UTIL.ID('noteAddDialog').open();
        UTIL.ID('noteAddDialog').addClass('noteappDialog')
        _ctrEvent.setDialogInputBind("note");
    },
	saveAndLeaveButton : function(event, widget){
		UTIL.ID('saveButton').setDisabled(true);
		Top.Controller.get('noteSaveLayoutLogic').saveOkBtn();
		setTimeout(function(){
			UTIL.ID('noteIsEditDialog').close(true);
		},1)
	},
    getNoteTitle : function() {
    	let noteName = UTIL.ID('noteName').getText();
        noteName = noteName.replace(/(<([^>]+)>)/ig,"");
        noteName = noteName.replace(/\"/gi,"");
        return noteName;
    },
//    isFileSelected : function() {
//    	return UTIL.ID('file').hasClass("active") ? true : false
//    },
//    isAllNoteSelected : function() {
//    	return UTIL.ID('all_note').hasClass("active") ? true : false
//    },
//    isTrashCanSelected : function() {
//    	return UTIL.ID('trash_bin').hasClass("active") ? true : false
//    },
//    isFavoriteNoteSelected : function() {
//        return UTIL.ID('favorite').hasClass("active") ? true : false
//    },
//    isNoteBookEmpty : function(){
//    	return noteRepo.noteList.length === 0 ? true : false
//    },        
//    noteokBtn : function(event, widget) {
//        let noteName = _ctrEvent.getNoteTitle();
//        let parentId = null;
//        let isNotebook = false;
//        let selectedLnbMenu =_ctrLnb.getLnbMenuType(); 
//        if( selectedLnbMenu === "notebook" ){
//        	let selectedNode = UTIL.ID('noteTree').getSelectedNode();
//        	isNotebook = true;
//            if( selectedNode !== undefined && selectedNode["id"]) parentId = selectedNode.id;
//        }
//        if( parentId === null ) parentId = TNoteData.getDefaultNoteBookId();
//        
//    	TNoteController.createNote( noteName , parentId , isNotebook);
//        UTIL.ID('noteAddDialog').close(true)
//        if($('#blankLayout').length!==0)$('#blankLayout').remove();
//    },
//    notecloseBtn : function(event, widget) {
//        UTIL.ID('noteAddDialog').close(true)
//    },
//    
//    okBtn : function(event, widget) {
//        let noteBookName = UTIL.ID( 'notebookName' ).getText();
//        noteBookName = noteBookName.replace( /(<([^>]+)>)/ig, "" );
//        noteBookName = noteBookName.replace( /\"/gi, "" );
//
//        let selectedNode = UTIL.ID( 'noteTree' ).getSelectedNode();
//        TNoteController.createNoteBook( noteBookName, selectedNode.id);
//
//        this.closeBtn();
//    },
    closeBtn : function(event, widget) {
//        UTIL.ID('notebookStackAddDialog').close(true)
//        UTIL.ID('notebookAddDialog').close(true)
        UTIL.ID('noteDeleteDialog').close(true)
//        UTIL.ID('tagCreateDialog').close(true)
//        UTIL.ID('noteRestoreDialog').close(true)
    },
//    contextmenu_open : function(event, widget){
//    	let selectedRow = UTIL.ID('notebookTable').getSelectedData()
//    	let selectedNode = selectedRow.id;
//        if(selectedRow.type=="stack"){
//            TNoteController.selectTreeStack(selectedNode, selectedRow.text, selectedRow.children)
//            UTIL.ID( 'noteTree' ).selectNode(selectedNode);
//        }
//        else if(selectedRow.type=="notebook"){
//        	TNoteController.selectTreeNoteBook(selectedNode, selectedRow.text)
//        	UTIL.ID( 'noteTree' ).selectNode(selectedNode);
//        }
//    },
//    contextmenu_edit : function(event, widget) {        
//    	let selectedRow = UTIL.ID('notebookTable').getSelectedData()
//    	let selectedType = selectedRow.type;
//       
//        TNoteController.contextMenuEditDialog(selectedType, selectedRow)
//        UTIL.ID('noteEditTitleDialog').open();
//        UTIL.ID('noteEditTitleDialog').addClass('noteappDialog')
//        Note.checkDOM('input#noteName' , function(){
//			let noteInput = $('input#noteName')
//		  	noteInput.on('input', function(e){
//		  		_ctrEvent.setEditDialogHint(e);
//		  	})
//		})
//    },
//    contextmenu_delete : function(event, widget) {
//    	let selectedRow = UTIL.ID('notebookTable').getSelectedData();
//    	inputCheck = [];
//    	inputCheck.push(selectedRow);
//    	_ctrEvent.delBtn();
//    },
//    filecontextmenu_delete : function(event, widget) {
//    	var fileMeta = Top.Controller.get("filePreviewLayoutLogic").previewFileMeta;
//    	TNoteServiceCaller.noteFileDelete(fileMeta.file_id);
//    	TNoteController.selectFileMenu()
//    	UTIL.ID("filePreviewDialog").close(true)
//    },
//    filecontextmenu_download : function(event, widget) {
//    	var fileMeta = Top.Controller.get("filePreviewLayoutLogic").previewFileMeta;
//    	TNoteFileController.onFileDownload(fileMeta.file_id);
//    },
//    getCheckedList : function(){
//    	let checkList = []
//    	for(i=1; i<Top.Dom.select('top-checkbox').length; i++){
//            if(Top.Dom.select('top-checkbox')[i].isChecked() == true){
//                checkList.push(noteRepo.noteList[i-1]);
//            }
//        }
//    	return checkList;
//    },
//    getSelectedLnbMenu : function(){
//        let dto = { // 향 후 좀 더 내용이 들어갈 수 있을 것 같아서 dto타입으로 정의함
//            "type" : "",
//        }
//        if(_ctrEvent.isAllNoteSelected()){
//            dto.type = "allnote";
//        } else if(_ctrEvent.isFavoriteNoteSelected()){
//            dto.type = "favorite";
//        } else if(_ctrEvent.isTrashCanSelected()){
//        	dto.type = "trashcan"
//        } else if(_ctrEvent.isFileSelected()){
//        	dto.type = "file"
//        } else {
//            if(UTIL.ID('tagTree').getSelectedNode() != undefined && UTIL.ID('tagTree').getSelectedNode().length !== 0){
//            	if(UTIL.ID('tagTree').getSelectedNode().id == 'tagroot') dto.type = "tagroot"
//            	if(UTIL.ID('tagTree').getSelectedNode().path == "tagroot") dto.type = "tag"
//            }else { //이건 노트북
//                dto.type = UTIL.ID('noteTree').getSelectedNode().type
//            }
//        } return dto;
//    },
//    setDialogInputBind : function(type){
//    	if(type === "note") type = 'noteName';
//    	if(type === "notebook") type = "notebookName";
//        Note.checkDOM('input#' + type , function(){
//        	let noteInput = $('input#' + type)
//	      	noteInput.on('input', function(e){
//	      		_ctrEvent.setCreateDialogHint(e);
//	      	})
//        })
//    },
//    setCreateDialogFocus : function(event, dialog){
//        let TF;
//        UTIL.ID(dialog.id).adjustPosition();
//        switch(dialog.id){
//            case "noteAddDialog":
//                TF = UTIL.ID('noteName')
//                curDialog='noteCreate'
//                $('input#noteName')[0].select();
//                break;
//            case "notebookAddDialog":
//                TF = UTIL.ID('notebookName')
//                curDialog='nbCreate'
//                $('input#notebookName')[0].select();
//                let input = TF.getText()
//                let _pNode = UTIL.ID('noteTree').getSelectedNode()
//                if(_pNode.children.length>0){
//                    for(i=0;i<_pNode.children.length;i++){
//                        if(_pNode.children[i].type=="notebook" && input == _pNode.children[i].text){
//                            UTIL.ID('nbCreateOkBtn').setDisabled(true)
//                            i=_pNode.children.length
//                        }
//                    }
//                }
//                break;
//            case "notebookStackAddDialog":
//                TF = UTIL.ID('notebookName')
//                curDialog='stackCreate'
//                $('input#notebookName')[0].select();
//                break;
//        }
//    },
//    setCreateDialogHint : function(event, widget){
//        let OkBtn
//        if(event.type === "input"){
//    		if(event.target.value === "") input = "";
//    		else{
//    			input = event.target.value.toString(); 
//    		}
//    	}
//
//        switch(curDialog){
//            case 'noteCreate':
//                OkBtn = UTIL.ID('noteCreateOkBtn')
//                TV = UTIL.ID('noteCreateTV')
//                if(input==""){
//                	$('input#noteName').focus();
//                    OkBtn.setDisabled(true)                
//                }
//                else if(input.length>250){
//                    tmp = input.substr(0,250)
//                    widget.setText(tmp)
//                }
//                else if(input.indexOf("\"") !== -1 || input.indexOf(">") !== -1 || input.indexOf("<") !== -1){
//                    widget.focus();
//                    OkBtn.setDisabled(true)
//                    TV.setText("특수문자 \" , > , < 는 입력할 수 없습니다.")
//                }
//                else{
//                    OkBtn.setDisabled(false)
//                    TV.setText("")
//                }
//                break;
//            case 'nbCreate':
//                OkBtn = UTIL.ID('nbCreateOkBtn')
//                TV = UTIL.ID('nbCreateTV')
//                let _pNode = UTIL.ID('noteTree').getSelectedNode()
//                OkBtn.setDisabled(false)
//                TV.setText("")
//                if(input == ""){
//                	$('input#noteName').focus();
//                    OkBtn.setDisabled(true)
//                } else if(input.length>250) {
//                    tmp = input.substr(0,250)
//                    widget.setText(tmp)
//                } else if(input.indexOf("\"") !== -1 || input.indexOf(">") !== -1 || input.indexOf("<") !== -1){
//                    widget.focus();
//                    OkBtn.setDisabled(true)
//                    TV.setText("특수문자 \" , > , < 는 입력할 수 없습니다.")
//                }
//                if(_pNode.children.length>0){
//                    for(i=0;i<_pNode.children.length;i++){
//                        if(_pNode.children[i].type=="notebook" && input == _pNode.children[i].text){
//                            widget.focus();
//                            OkBtn.setDisabled(true)
//                            i=_pNode.children.length
//                        }
//                    }
//                }                
//                break;
//            case 'stackCreate':
//                OkBtn = UTIL.ID('stackCreateOkBtn')
//                TV = UTIL.ID('stackCreateTV')
//                if(input==""){
//                    widget.focus();
//                    OkBtn.setDisabled(true)
//                }
//                else if(input.length>250){
//                    tmp = input.substr(0,250)
//                    widget.setText(tmp)
//                }
//                else if(input.indexOf("\"") !== -1 || input.indexOf(">") !== -1 || input.indexOf("<") !== -1){
//                    widget.focus();
//                    OkBtn.setDisabled(true)
//                    TV.setText("특수문자 \" , > , < 는 입력할 수 없습니다.")
//                }
//                else{
//                    OkBtn.setDisabled(false)
//                    TV.setText("")
//                }
//                break;
//        }
//        if(event.keyCode == "13"){
//        	if(OkBtn.getProperties("disabled")==true){
//                
//            }else{
//                OkBtn.trigger("click")
//            }
//        }
//    },
//    setEditDialogHint : function(event, widget) {
//    	TF = UTIL.ID('noteName');
//    	OkBtn = UTIL.ID('noteEditTitleOkBtn');
//    	if(event.type === "input" || event.type === "keyup"){
//    		if(event.target.value === "") input = "";
//    		else{
//    			input = event.target.value.toString(); 
//    		}
//    	}
//        if(input=="") OkBtn.setDisabled(true)
//        else if(input.length>250){
//            tmp = input
//            tmp = tmp.substr(0,250)
//            TF.setText(tmp)
//        }
//        else if(input.indexOf("\"") !== -1 || input.indexOf(">") !== -1 || input.indexOf("<") !== -1){
//        	$('input#noteName').focus();
//            OkBtn.setDisabled(true)
//            TV.setText("특수문자 \" , > , < 는 입력할 수 없습니다.")
//        }
//        else {
//            OkBtn.setDisabled(false)
//            TV.setText("")
//        }                    
//	},
//    setNBDialogFocus : function(){
//        UTIL.ID('noteAddDialog').adjustPosition();        
//        let tmp = UTIL.ID('noteName').getText();
//        UTIL.ID('noteName').setText('');
//        UTIL.ID('noteName').setText(tmp);
//        UTIL.ID('noteName').focus();
//    },
//    nbRefresh : function(event, widget) {
//    	let lnbSelected = _ctrLnb.getLnbMenuType()
//    	switch(lnbSelected){
//    	case 'allnote':
//    		TNoteController.selectTotalNoteBookMenu();
//    		break;
//    	case 'favorite':
//    		_ctrLnb.favNoteRender()
//    		break;
//    	case 'tag':
//    	case 'tagroot':
//    		UTIL.ID('tagTree').trigger("click")
//    		break;
//    	case 'stack':
//    	case 'notebook':
//    	case 'menu':
//    		UTIL.ID('noteTree').trigger("click")
//    		break;
//    	case 'trashcan':
//    		TNoteController.selectTrashCanMenu();
//    		break;
//    	}
//    },
//    tagRefreshBtn : function(event, widget) {
//        UTIL.ID("tagTree").trigger('click')
//    },
//    noteupdateCancel : function(event, widget) {
//        UTIL.ID('noteEditTitleDialog').close(true);
//    },
//    deleteOnMobileEditor : function(){
//        if(_noteListView.getData().length!==1) _ctrEditor.editorPreBtn();
//        let data = _noteListView.getSelectedData();
//        TNoteController.moveToTrashCan([data]);
//    },
//    delBtn : function(event, widget){
//        let dialog = UTIL.ID('noteDeleteDialog')
//        let checkType = _ctrLnb.checkBtnCase();
//        switch(checkType){
//            case "listview":
//            	let CheckedList = _noteListView.getCheckedData()
//            		TNoteController.moveToTrashCan(CheckedList)
//            	
//                if(Top.Dom.select('top-checkbox')[0].isChecked()== true){
//                    Top.Dom.select('top-checkbox')[0].setProperties({checked: false})
//                }
//        		return;
//            case "table":
//            	let selectedTree = UTIL.ID( 'noteTree' ).getSelectedNode().id
//                dialog.setProperties({"on-open":function(){
//                    if(inputCheck.length<=1){
//                		UTIL.setProperty("noteDelTextView",{"text": inputCheck[0].text});
//                        UTIL.ID('noteDeleteSystemMessage').setText("을(를) 삭제하시겠습니까?");
//                    }
//                    else{
//                        UTIL.setProperty("noteDelTextView",{"text": inputCheck.length});
//                        UTIL.ID('noteDeleteSystemMessage').setText("개의 북/챕터를 삭제하시겠습니까?");
//                    }
//                    UTIL.setProperty("noteDelTextView_1",{"text": "삭제 후에는 복구할 수 없습니다."});
//                    UTIL.setProperty("note_delete_ok",{
//                        "on-click":function(){
//                            TNoteController.deleteNoteBook(selectedTree,inputCheck)
//                            if(Top.Dom.select('top-checkbox')[0].isChecked()== true) {Top.Dom.select('top-checkbox')[0].setProperties({checked: false})}
//                            dialog.close(true)
//                       	}
//                    })
//                }})
//                break;
//            case "wide" :
//                let wideSelectData = _noteListView.getSelectedData();
//                let wideArray = [];
//                wideArray.push(wideSelectData)
//            	TNoteController.moveToTrashCan(wideArray);
//                UTIL.ID('notebookRefreshBtn').trigger('click');										  
//                UTIL.ID('ImageButton180').trigger("click")
//                if($('.noteEditor_overlay1'))  $('.noteEditor_overlay1').remove();
//                if($('.noteEditor_overlay2')) $('.noteEditor_overlay2').remove();
//                if($('.noteEditor_overlay3')) $('.noteEditor_overlay3').remove();
//                if($('.noteEditor_overlay4')) $('.noteEditor_overlay4').remove(); 
//            	
//                return;
//            /*
//             *  TODO 첨부 파일함 오픈 때 
//             */
//            case "file" :
//            	dialog.setProperties({"on-open":function(){
//            		let inputFileIdArr = [];
//            		for(var i=0; i< inputCheck.length; i++) inputFileIdArr.push(inputCheck[i].file_id)
//                    if(inputCheck.length<=1){
//                		UTIL.setProperty("noteDelTextView",{"text": inputCheck[0].file_name})
//                        UTIL.ID('noteDeleteSystemMessage').setText("을(를) 삭제하시겠습니까?");
//                    } else {
//                        UTIL.setProperty("noteDelTextView",{"text": inputCheck.length});
//                        UTIL.ID('noteDeleteSystemMessage').setText("개의 파일을 삭제하시겠습니까?");
//                    }
//                    UTIL.setProperty("noteDelTextView_1",{"text": "삭제 후에는 복구할 수 없습니다."});
//                    UTIL.setProperty("note_delete_ok",{
//                        "on-click":function(){
//                        	TNoteController.deleteFile(inputCheck);
//							UTIL.ID("noteFileSelectBox").setDisabled(true);
//							UTIL.ID("noteFileDeleteBtn").setDisabled(true);
//							UTIL.ID('noteFileTable').render();
//							UTIL.ID('noteFileCheckBox').setChecked(false);                        	
//							inputCheck = []
//                            inputFileIdArr = []
//	                        dialog.close(true)
//                        }})
//                }})
//                break;
//
//        }
//        dialog.addClass('noteappDialog')
//        dialog.open()
//    },
//    checkAll : function(event, widget) {
//        if(widget.id == "rootCheckBox"){//이건 스택이나 메뉴
//        	let notebookTable = UTIL.ID("notebookTable")
//            widget.isChecked()==true? notebookTable.checkAll(true, false) : notebookTable.checkAll(false, false);
//            if(notebookTable.getCheckedIndex()!== undefined && notebookTable.getCheckedIndex().length !== 0) {
//                UTIL.ID("noteDeleteBtn").setDisabled(false);
//            	let moveCheck = true;
//            	for(let i=0; i<notebookTable.getCheckedIndex().length; i++){
//            		if(noteRepo.noteTableList[notebookTable.getCheckedIndex()[i]].type === "stack") moveCheck = false;
//            	}
//            	UTIL.ID('notebookMoveBtn').setDisabled(moveCheck? false : true);                             
////                Top.Dom.selectById("favoriteBtn").setDisabled(false);
//            } else {
//                UTIL.ID("noteDeleteBtn").setDisabled(true);
//                UTIL.ID("favoriteBtn").setDisabled(true);
//                UTIL.ID('notebookMoveBtn').setDisabled(true);
//            }
//            inputCheck = UTIL.ID('notebookTable').getCheckedData();
//        }
//        else if(widget.id == "noteCheckBox"){//이건 listview 노트들
//            _noteListView.checkAll(widget)
//        }
//        else if(widget.id == "trashCanCheckBox"){
//        	_noteListView.checkAll(widget)
//        }
//        else if(widget.id == "noteFileCheckBox"){
//          	 widget.isChecked()==true? UTIL.ID("noteFileTable").checkAll(true, false) : UTIL.ID("noteFileTable").checkAll(false, false);
//          	 if(UTIL.ID("noteFileTable").getCheckedIndex() !== undefined && UTIL.ID("noteFileTable").getCheckedIndex().length !== 0) {
//          		   UTIL.ID("noteFileSelectBox").setDisabled(false);
//                  UTIL.ID("noteFileDeleteBtn").setDisabled(false);
//          	}
//          	else{
//                  UTIL.ID("noteFileSelectBox").setDisabled(true);
//                  UTIL.ID("noteFileDeleteBtn").setDisabled(true);
//              }
//          	inputCheck = UTIL.ID('noteFileTable').getCheckedData();
//        }
//    },
//    customWidgetInit: function() {
//        //SUNEDITOER create 실행
//        // target : textarea ID
//        Note.checkDOM("textarea#noteTest", function(){
//            editor = SUNEDITOR.create("noteTest", {
//                fontSize : [
//                    8, 10, 14, 18, 24, 36
//                ],
//                font : [
//                    'Arial',
//                    'tahoma',
//                    'Courier New,Courier'
//                ],
//                width : '100%',
//                height : 'auto',
//                maxHeight: 'auto',
//                videoWidth : 360,
//                videoHeight : 115,
//                youtubeQuery : 'autoplay=1&mute=1&enablejsapi=1',
//                imageWidth : 150,
//                stickyToolbar : false,
//                popupDisplay: 'local',
//                resizingBar: false,
//                isFullScreen: false,
//                buttonList : [
//                    ['undo', 'redo','font', 'fontSize', 'formatBlock',
//                    'bold', 'underline', 'italic', 'strike', 'subscript', 'superscript',
//                    'fontColor', 'hiliteColor',
//                    'indent', 'outdent',
//                    'align', 'horizontalRule', 'list', 'table']
//                ],
//                /*'image', 'video', 'fullScreen'*/ /*'print', 'save'*/                
//                callBackSave: function (contents) {}					   					   
//						  
//										   
//            });
//            editor.onScroll = function (e) {}
//            editor.onClick = function (e) {
//                var editor_vertical = $('.sun-editor-container').height() - $('.sun-editor-id-toolbar').height() + 'px'
//                $('.sun-editor-id-wysiwyg').css('max-height', editor_vertical)
//                if(e.target.id == "sun_editorEdit" || e.target.nodeName == "P"){
//                    let fileActive = document.querySelectorAll('.sun-editor-file-modal');
//                    let fileContextMenu = $('#fileContextUploadBtn');
//                    if(fileActive != null){
//                        for(let i=0; i< fileActive.length; i++){
//                            fileActive[i].classList = "sun-editor-file-body"
//                        }
//                        $('#file-removeController').remove()
//                    }
//                    if(fileContextMenu.length !== 0) $('#fileContextUploadBtn').remove();
//                }else{
//                	
//                }
//            }
//            editor.onKeyDown = function (e) {}
//            editor.onKeyUp = function (e) {}
//            editor.onDrop = function (e) {}
//            editor.onImageUpload = function (targetImgElement, index, isDelete, imageInfo) {
//                // file Upload 서비스 콜
//                if(isDelete == false){
//                    if(targetImgElement.getAttribute('file_id') == null){
//                    	let SelectNote = _noteListView.getSelectedData();
//                        let _successCallback = function(data) {
//                            fileId = data.dto.storageFileInfoList[0].file_id;
//                            targetImgElement.setAttribute('file_id', fileId)
//                            targetImgElement.setAttribute('img_Index', index)
//                            targetImgElement.setAttribute('onerror', "this.src='./res/not-load-image.png'")
//                            let targetSRC = _workspace.url + "Storage/StorageFile?action=Download" + "&fileID=" + fileId + "&workspaceID=" + routeManager.getWorkspaceId() + "&channelID=" + noteChannel_Id + "&userID=" + userManager.getLoginUserId();
//                            targetImgElement.setAttribute('src', targetSRC)
//                        };    
//                        let _errorCallback = function(data) {
//                            console.log(data.message);
//                        };
//                    }                               
//                }
//            }
//            editor.onImageUploadError = function (errorMessage, result) {}
//        })
//        setTimeout(function(){
//            Top.Controller.get('TopApplicationLogic')._isImage()
//        },1)
//    },
//    _isImage : function(){
//        let imageElement = $('#sun_editorEdit').find('img');
//
//        for(let index=0; index< imageElement.length; index++){
//            let beforeSrc = imageElement[index].getAttribute('src');
//            if( !beforeSrc || !beforeSrc.length || beforeSrc==="./res/not-load-image.png" ){
//                let targetFileId = imageElement[index].getAttribute("file_id");
//                let targetSRC = _workspace.url + "Storage/StorageFile?action=Download&fileID=" + targetFileId + "&workspaceID=" + ws_id + "&channelID=" + noteChannel_Id + "&userID=" + userManager.getLoginUserId();
//                imageElement[index].setAttribute('src', targetSRC);
//            }
//        }
//    },
//    _isFile : function(){ // 삭제 된 파일일 경우 손상되게 표현한 로직, 현재는 쓰이는 곳 지워졌지만 우선 놔 둠 
//    	let fileElement = document.querySelectorAll('.sun-editor-file-content')
//    	let _SuccessCallback = function(data){
////    		console.log(data)
//    	}
//     	if(fileElement.length > 0){
//     		for(let j=0; j < fileElement.length; j++){
//     			/*
//     			 * file_id, ws_id, ch_id, user_id
//     			 */     			
//     			Note.noteFileMetaGet(fileElement[j].id, routeManager.getWorkspaceId(), noteChannel_Id, userManager.getLoginUserId(), function(data){
//     				if(data.dto.resultMsg === "Fail"){
//     					$('#' + fileElement[j].id)[0].classList.remove('sun-editor-file-content');
//     					$('#' + fileElement[j].id).empty();
//     					$('#' + fileElement[j].id)[0].classList.add('sun-editor-file-empty');
//     				}
//     			})
//     		}
//     	}else return;
//    },
//    clickMoveMobile : function(){
//    	let dialogOption = TNoteController.moveDialogOptionBuilder([_noteListView.getSelectedData()]).setNoteOkButton().setNoteNodeClickEvent().build();
//    	TNoteController.openMoveDialog(dialogOption);
//    },
//    clickMoveButton : function(event, widget) {         
//        let lnbSelected = _ctrLnb.getLnbMenuType();
//        let dialogOption;
//        if(lnbSelected === "stack"){
//        	dialogOption = TNoteController.moveDialogOptionBuilder(UTIL.ID('notebookTable').getCheckedData()).setNoteBookOkButton().setNoteBookNodeClickEvent().build();
//        } else if(lnbSelected === "menu"){
//    		dialogOption= TNoteController.moveDialogOptionBuilder(UTIL.ID('notebookTable').getCheckedData()).setNoteBookOkButton().setMenuNodeClickEvent().build();
//        } else {
//        	dialogOption = TNoteController.moveDialogOptionBuilder(_noteListView.getCheckedData()).setNoteOkButton().setNoteNodeClickEvent().build();
//        }
//        if(UTIL.ID('noteLNB').getProperties("visible")==="none") movingObject = [_noteListView.getSelectedData()];
//        TNoteController.openMoveDialog(dialogOption);
//    },
//    contextmenu_move : function(event, widget){
//    	let lnbSelected = _ctrLnb.getLnbMenuType();
//    	let dialogOption;
//    	let movingObject = UTIL.ID('notebookTable').getSelectedData();
//    	if(lnbSelected === "stack"){
//    		dialogOption = TNoteController.moveDialogOptionBuilder([movingObject]).setNoteBookOkButton().setNoteBookNodeClickEvent().build();
//        } else if(lnbSelected === "menu"){
//        	dialogOption= TNoteController.moveDialogOptionBuilder([movingObject]).setNoteBookOkButton().setMenuNodeClickEvent().build();
//        }
//    	TNoteController.openMoveDialog(dialogOption);
//    },
//    shareCreate : function(event, widget) {
//        UTIL.ID('shareDialog').open();
//        UTIL.ID('shareDialog').addClass('noteappDialog')
//        Note.shareWSList();
//    },
//    sortBtn : function(event, widget) {
//    	let newData;
//    	let sortType = _ctrLnb.getLnbMenuType();
//    	if(sortType === 'trashcan') {
//            if(sortFlag == 1){
//                newData= _noteListView.getData().sort(function(a,b){return a.deleted_date < b.deleted_date ? -1 : 1})
//                sortFlag = 0
//            }
//            else{
//                newData= _noteListView.getData().sort(function(a,b){return a.deleted_date < b.deleted_date ? 1 : -1})
//                sortFlag = 1
//            }
//    	} else {
//            if(sortFlag == 1){
//                newData = _noteListView.getData().sort(function(a,b){return a.modified_date < b.modified_date ? -1 : 1})    
//                sortFlag = 0
//            }
//            else{
//            	newData = _noteListView.getData().sort(function(a,b){return a.modified_date < b.modified_date ? 1 : -1})
//                sortFlag = 1
//            }
//        }
//        _noteListView.setValue(newData)
//        if(Top.Dom.select('top-checkbox')[0].isChecked()== true) Top.Dom.select('top-checkbox')[0].setProperties({checked: false})
//    },
//    // upperMenu 합치며 일단 여기로 이사.
//    restore : function(event, widget) {
//    	if(isMobile()){
//    		if(TNoteMobile.getMode() === "listview"){
//    			note_del_type = "listview_note"
//            } else {
//            	note_del_type = "editor_note"
//            }
//    	}
//		let dialog = UTIL.ID('noteRestoreDialog')
//		if(note_del_type == "editor_note"){
//			let SelData = _noteListView.getSelectedData();
//    		dialog.setProperties({"on-open":function(){
//				UTIL.setProperty("noteRestoreView",{"text": SelData.note_title});
//				UTIL.ID('noteRestoreSystemMessage').setText('을(를) 복원하시겠습니까?')
//				UTIL.setProperty("TextView83",{"text": "해당 페이지가 원래 위치로 이동됩니다."});
//				UTIL.setProperty("TextView254",{"text": "원래 위치가 삭제된 경우에는 전체 페이지로 이동됩니다."});
//				UTIL.setProperty("note_restore_ok",{
//					"on-click":function(){
//						noteUpdateFrom = "trashcan"
//						let p_id = SelData.parent_notebook;
//						Note.bookList(noteChannel_Id, function(notebookList){
//							let flag = 0;
//							for(let i=0;i<notebookList.length; i++){
//								if(SelData.parent_notebook === notebookList[i].id) flag = 1;
//							}
//							SelData.parent_notebook = flag === 1 ? p_id : defaultNotebookID;
//							SelData.WS_ID = routeManager.getWorkspaceId();
//							SelData.USER_ID = JSON.parse(sessionStorage.getItem('userInfo')).userId
//							SelData.note_channel_id = noteChannel_Id
//							SelData.CH_TYPE = "CHN0003"
//							Note.noteRestoreUpdate(SelData.note_id, SelData.parent_notebook, false) // 여기 T/F 체크
//							Note.noteTrashDelete([SelData], function(){
//								if(isMobile()) $('div#noteLNB').removeClass('lnb-hide-mode')
//								_ctrLnb.trashCanRender();
//							})
//						})
//						dialog.close(true)
//		        		if(!isMobile()) UTIL.ID('del_wideimagebtn').trigger("click")
//					}
//				})
//			}})
//		}
//		else{
//			dialog.setProperties({"on-open":function(){			
//				let checkedTrashList = _noteListView.getCheckedData()
//				if(checkedTrashList.length<=1) {
//					UTIL.setProperty("noteRestoreView",{"text": checkedTrashList[0].note_title});
//					UTIL.ID('noteRestoreSystemMessage').setText('을(를) 복원하시겠습니까?')
//				}
//				else{
//					UTIL.setProperty("noteRestoreView",{"text": "선택한 " + checkedTrashList.length});
//					UTIL.ID('noteRestoreSystemMessage').setText('개의 페이지를 복원하시겠습니까?')
//				}
//				UTIL.setProperty("TextView83",{"text": "해당 페이지가 원래 위치로 이동됩니다."});
//				UTIL.setProperty("TextView254",{"text": "원래 위치가 삭제된 경우에는 전체 페이지로 이동됩니다."});
//				UTIL.setProperty("note_restore_ok",{
//					"on-click":function(){
//						for(let j=0; j<checkedTrashList.length; j++){
//							let p_id = checkedTrashList[j].parent_notebook;
//							let flag = 0;
//							Note.bookList(noteChannel_Id, function(notebookList){
//								for(z=0;z<notebookList.length;z++){
//									if(checkedTrashList[j].parent_notebook == notebookList[z].id) flag = 1;
//								}
//							})							
//							checkedTrashList[j].parent_notebook = flag == 1 ? p_id : defaultNotebookID;
//							checkedTrashList[j].WS_ID = routeManager.getWorkspaceId();
//							checkedTrashList[j].USER_ID = JSON.parse(sessionStorage.getItem('userInfo')).userId
//							checkedTrashList[j].CH_TYPE = "CHN0003"
//							checkedTrashList[j].note_channel_id = noteChannel_Id
//	        				noteUpdateFrom = "trashcan"
//	        				Note.noteRestoreUpdate(checkedTrashList[j].note_id, checkedTrashList[j].parent_notebook, false)
//						}
//						Note.noteTrashDelete(checkedTrashList, function(){
//							if(isMobile()) $('div#noteLNB').removeClass('lnb-hide-mode')
//    						_ctrLnb.trashCanRender()
//    					})
//	           			dialog.close(true)
//					}
//				})
//			},
//		})}
//		dialog.addClass('noteappDialog')
//		dialog.open();
//	}, 
//	noteDelete : function(event, widget) {
//		if(isMobile()){
//    		if(TNoteMobile.getMode() === "listview"){
//    			note_del_type = "listview_note"
//            } else {
//            	note_del_type = "editor_note"
//            }
//    	}
//    	let dialog = UTIL.ID('noteDeleteDialog')
//    	if(note_del_type == "editor_note") {
//    		var SelData = _noteListView.getSelectedData();
//    		dialog.setProperties({"on-open":function(){
//					UTIL.setProperty("noteDeleteDialog",{"title":"영구 삭제" })
//					UTIL.setProperty("noteDelTextView",{"text": SelData.note_title});
//					UTIL.ID('noteDeleteSystemMessage').setText("을(를) 영구 삭제하시겠습니까?");
//					UTIL.setProperty("noteDelTextView_1",{"text": "삭제 후에는 복구할 수 없습니다."});
//					UTIL.setProperty("note_delete_ok",{
//						"on-click":function(){
//							TNoteController.deleteNote([SelData])
//							dialog.close(true)
//			        		if(!isMobile()) UTIL.ID('del_wideimagebtn').trigger("click")	        	
//						}
//					})
//			}})
//		} else {
//    		dialog.setProperties({
//    			"on-open":function(){
//    				let checkedTrashList = _noteListView.getCheckedData()
//    				UTIL.setProperty("noteDeleteDialog",{"title":"영구 삭제" })
//			    	if(checkedTrashList.length<=1) {
//						UTIL.setProperty("noteDelTextView",{"text": checkedTrashList[0].note_title});
//						UTIL.ID('noteDeleteSystemMessage').setText("을(를) 영구 삭제하시겠습니까?");
//					}
//					else{
//						UTIL.setProperty("noteDelTextView",{"text": "선택한 " + checkedTrashList.length});
//						UTIL.ID('noteDeleteSystemMessage').setText("개의 페이지를 영구 삭제하시겠습니까?");
//					}
//    				UTIL.setProperty("noteDelTextView_1",{"text": "삭제 후에는 복구할 수 없습니다."});
//					UTIL.setProperty("note_delete_ok",{
//						"on-click":function(){
//							TNoteController.deleteNote(checkedTrashList)
//							dialog.close(true);
//						}
//					})
//    			},
//    		})
//		}
//    	dialog.addClass('noteappDialog')
//	    dialog.open();
//    }
});

//Top.Controller.create('shareCreateLogic', {
//    init : function(){
//        
//        noteRepo.setValue('workspaceList', _noteShareWSList.dto.userWSList);    
//        
//        UTIL.ID('shareTable').update()
//  
//    },
//    
//    WS_Add : function(event, widget) {
//        
//        var WS_Name;
//        
//        WS_Name = UTIL.ID('SelectBox632').getSelectedText()
//
//        var columnWS = { "TB_WS_NAME" : WS_Name }
//
//        noteRepo.addValue('tableWSList', columnWS);
//        
//        Sharecount++
//        
//        for(i=Sharecount-1;i>=0;i--){
//            
//            if(WS_Name == noteRepo.tableWSList[i].TB_WS_NAME )
//                
//                {
//                UTIL.ID('SharePlusButton').setDisabled(true)
//                i=-1
//                }
//            else
//                {
//                UTIL.ID('SharePlusButton').setDisabled(false)
//                }
//        }
//    },
//    OnShareClose : function(event, widget) {
//        
//        noteRepo.tableWSList.splice(0,Sharecount)
//        
//       UTIL.ID('shareDialog').close(true);
//      
//        Sharecount = 0;
//   },
//    WS_Sub : function(event, widget) {
//        
//        let rowNum = event.path[11].getClickedIndex()[0]
//        
//        noteRepo.tableWSList.splice(rowNum,1)
//        
//        UTIL.ID('shareTable').update()
//        
//        Sharecount--
//    },
//    
//    WS_Overlap : function(event, widget) {
//        
//        var WS_Name;
//        
//        WS_Name = UTIL.ID('SelectBox632').getSelectedText()
//        
//        for(i=Sharecount-1;i>=0;i--){
//            
//            if(WS_Name == noteRepo.tableWSList[i].TB_WS_NAME )
//                
//                {
//                UTIL.ID('SharePlusButton').setDisabled(true)
//                i=-1
//                }
//            else
//                {
//                UTIL.ID('SharePlusButton').setDisabled(false)
//                }
//        }
//    }
//})
NoteRoute = (function(){
	/**
	 *  노트 LNB 메뉴 선택시 선택 된 메뉴의 type을 얻어와 URL에 Set 
	 *  @parameter type : LNB 메뉴 타입
	 */
	function setURL(type){
		let beforeCheck ;
		selectId = {};
		title = '';
		state = {};
		if(UTIL.ID('noteTree').getSelectedNode() != undefined){
			notebookID = UTIL.ID('noteTree').getSelectedNode().id
		}
		if(Top.curRoute.path == "/space") return;
		switch(type){
			case 'menu' : 
			case 'notebook':
			case 'stack':
				selectId = notebookID
				state = {
					path : "#!" + Top.curRoute.path,
					menu : type,
					id : selectId
				}
				Top.App.pushState(state,"","#!" + Top.curRoute.path + "/" + type)
				break;
			case 'tag':
			case 'tagroot':
				selectId = UTIL.ID('tagTree').getSelectedNode().id
				state = {
					path : "#!" + Top.curRoute.path,
					menu : type,
					id : selectId
				}
				Top.App.pushState(state,"","#!" + Top.curRoute.path + "/" + type)			
				break;	
			case 'menu':
			case 'allnote':
			case 'trashcan':
			case 'favorite':
				selectId = type;
				state = {
					path : "#!" + Top.curRoute.path,
					menu : type,
					id : type
				}
				Top.App.pushState(state,"","#!" + Top.curRoute.path + "/" + type)
				break;
		}
	}

	/**
	 *  뒤로가기 이벤트시 얻어온 type과 data 로 Node Select 
	 *  @parameter type : LNB 메뉴 타입  / data : LNB 메뉴 타입에 따른 Id
	 */
	function setMenuNode(type, data){
		let noteTree = UTIL.ID('noteTree')
		let tagTree = UTIL.ID('tagTree')
		switch(type){
			case 'menu':
				setTimeout(function(){
					noteTree.selectNode('root')
					noteTree.trigger("click")
				},1)				
				break;
			case 'stack':
			case 'notebook':
				setTimeout(function(){
					noteTree.selectNode(data)
					noteTree.trigger("click")
				},1)				
				break;
			case 'tagroot':
			case 'tag':
				setTimeout(function(){
					tagTree.selectNode(data)
					tagTree.trigger("click")
				},1)			
			break;
			case 'allnote':
			case 'favorite':
			case 'trashcan':
				if(data == 'allnote') data = 'all_note'
				if(data == 'trashcan') data = 'trash_bin'
				$('div#' + data).click()
				break;
		}
	}
	
	return {
		setURL :function(type) {
			return setURL(type)
		},
		setMenuNode :function(type,data) {
			return setMenuNode(type,data)
		}
	}
}())

/**
 *  뒤로가기 이벤트 catch 해서 예전 URL type과 data Get 
 *  
 */
//window.onhashchange = function(e) {
//	/* 2020-01-20 현재 url을 통한 state 관리를 적용하지 않은 상황이라 뒤로가기 버튼으로 화면 이동 시 modal이 씌워져 있는 채로 나가지는 걸 막기 위한 임시 코드로 대체함.
//	let changeURL = e.newURL;
//	let routeURL = changeURL.split('#!')[1];
//	let undoType = routeURL.split('/')[4];
//	let undoID = routeURL.split('/')[5];
//	if(routeManager.getApp() === "note"){
//        // 저장 버튼이 있는 상태에서 routing을 탔다는 것은 비정상 동작을 의미한다.
//        let saveButton= UTIL.ID('saveButton');
//        if( saveButton && saveButton.getText() === "완료"){
////            Note.noteEditOnClose();
//        	Top.Controller.get('noteSaveLayoutLogic').saveOkBtn(); // 2020-01 기획변경으로 브라우저를 이용한 앱 외적인 화면 이동 시 수정중이던 노트를 저장하고 나간다. 
//        }
//	}
//	*/
//	let oldURL = e.oldURL;
//	let routeURL = oldURL.split('#!')[1];
//	let beforeApp = routeURL.split('/')[4].split('?')[0];
//	if(beforeApp === 'note'){
//		let saveButton= UTIL.ID('saveButton');
//        if( saveButton && saveButton.getText() === "완료"){
//        	Top.Controller.get('noteSaveLayoutLogic').saveOkBtn(); // 2020-01 기획변경으로 브라우저를 이용한 앱 외적인 화면 이동 시 수정중이던 노트를 저장하고 나간다. 
//        }
//	}
//}

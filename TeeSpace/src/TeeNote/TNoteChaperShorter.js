
const ChapterColorGenerator =( () =>{
    // Colors
    const COLORS = {
        1 : "rgb(251,119,72)",
        2 : "rgb(251,137,27)",
        3 : "rgb(231,180,0)",
        4 : "rgb(180,204,27)",
        5 : "rgb(101,208,63)",
        6 : "rgb(20,192,179)",
        7 : "rgb(0,198,230)",
        8 : "rgb(74,153,245)",
        9 : "rgb(148,130,255)",
        10: "rgb(231,128,255)",
        11: "rgb(255,123,168)",
    };
    const COLOR_ARRAY =Object.values(COLORS);

    return {
        getRandomColor : function() {
            return COLOR_ARRAY[Math.floor(Math.random() * COLOR_ARRAY.length)];
        },
        getDifferentRandom : function( usedColorSet ) {

            if( usedColorSet.size >= COLOR_ARRAY.length ){
                usedColorSet.clear();
            }
            let remainColor=COLOR_ARRAY;
            if( usedColorSet && usedColorSet.size ) {
                remainColor = COLOR_ARRAY.filter( ( element ) => {
                    if ( usedColorSet )
                        return !usedColorSet.has( element );
                    else {
                        return true;
                    }
                } );
            }
            return remainColor[Math.floor(Math.random() * remainColor.length)];
        }
    }
})();

const TNoteChaperShorter = ( () => {

    const targetStorage = localStorage;
    // storage indexKey
    const STORAGE_KEY_PREFIX = "TeeNoteSortData_";

    // Using DTO Key
    const DATA_KEY_FOR_ID ="id";
    const DATA_KEY_FOR_COLOR ="color";
    // 인덱스의 경우 sort 용 에서만 사용한다.
    const DATA_KEY_FOR_INDEX ="chapterIndex";

    /**
        메모리에 전체 Chapter의 Sorting 정보를 들고 있다.
        KEY     : ChannelID
        Value   : MAP
          ==> ChapterSavingFormat 의 MAP
                KEY     : ChapterID
                VALUE   : ChapterSavingFormat
    */
    const totalChaptersSortingData = new Map();

    // 전체 스토리지에서 key가 있는지를 찾는다.
    // 추후 1개의 key로 저장할 경우 loop를 없애야 한다.
    const loadFromStorage = () => {

        storageForEach( ( storageSavedKey ) => {
            // PreFix가 있는지 체크한다.
            if(  storageSavedKey.includes( STORAGE_KEY_PREFIX) ){


                const loadingJson = targetStorage.getItem(  storageSavedKey );

                // ChannelID 만 뽑기
                const channelId =  storageSavedKey.substring( STORAGE_KEY_PREFIX.length );

                const chapterSavingMap = new Map( JSON.parse(loadingJson) );
                totalChaptersSortingData.set(channelId, chapterSavingMap);
            }
        });
    };

    const sortingChapter = ( channelId, serverChapterList ) => {

        let chapterSortingMap = getNoteSortingData( channelId );

        // 있을 경우 체크 필요
        if( !chapterSortingMap ){
            chapterSortingMap = new Map();
            setNoteSortingData( channelId, chapterSortingMap);
        }

        // 덮어 써도 상관 없으나 이해를 돕기 위해 새로 할당
        const applyChapterList = applySortingData( serverChapterList, chapterSortingMap);

        updateSortingData( channelId, applyChapterList );
        return applyChapterList;
    };

    // return이 없기 때문에 async로 불리면 좋겠다.
    const updateSortingData = (channelId, applyChapterList ) => {

        // 덮어 쓰기 때문에 새로 할당 하자.
        let newChapterSortingMap = new Map();

        let thisIndex = 0;
        // ApplyChapterList는 변경이 된 데이터로 이 순서로 다시 저장해야 한다.
        applyChapterList.forEach( (chapterData) => {
            const chapterId = chapterData[DATA_KEY_FOR_ID];
            const targetChapterData = makeSaveChapterFormat( chapterId, chapterData[DATA_KEY_FOR_COLOR], thisIndex);
            newChapterSortingMap.set(chapterId, targetChapterData);
            thisIndex++;
        });

        totalChaptersSortingData.set(channelId, newChapterSortingMap);
        saveToStorage();
    };


    // Keys 메소드가 없어 임시 작
    const storageForEach = ( callback ) => {
        for (let keyIndex = 0; keyIndex < targetStorage.length; keyIndex++) {
            callback(targetStorage.key(keyIndex));
        }
    };

    const makeSaveChapterFormat= ( targetId, targetColor, targetNumber ) =>{

        return {
            [DATA_KEY_FOR_ID]       : targetId,
            [DATA_KEY_FOR_COLOR]    : targetColor,
            [DATA_KEY_FOR_INDEX]    : targetNumber
        }
    };
    
    const saveToStorage = () => {

        // Map ForEach 참조 : value, Key, Map이 파라미터 이다.
        // 저장은 ChannelId 별로 한다.
        totalChaptersSortingData.forEach( (chapterSortingMap, targetChannelId ) => {
            const jsonText = JSON.stringify(Array.from(chapterSortingMap.entries()));

            targetStorage.setItem(`${STORAGE_KEY_PREFIX}${targetChannelId}`, jsonText);
        });
    };

    const applySortingData = ( inputChapterList, savedSortingMap ) =>{
        const usedColorSet = new Set();
        const noColorChapterArray = [];
        // targetNoteData는 Array다.
        inputChapterList.forEach( (inputChapterData) => {
            const chapterId = inputChapterData[DATA_KEY_FOR_ID];
            const savedChapterInfo = savedSortingMap.get(chapterId);
            if( savedChapterInfo ){
                inputChapterData[DATA_KEY_FOR_INDEX]    = savedChapterInfo[DATA_KEY_FOR_INDEX];
                inputChapterData[DATA_KEY_FOR_COLOR]    = savedChapterInfo[DATA_KEY_FOR_COLOR];
                usedColorSet.add(inputChapterData[DATA_KEY_FOR_COLOR]);
            }else{
                inputChapterData[DATA_KEY_FOR_INDEX]    = 0;
                noColorChapterArray.push(chapterId);
                // inputChapterData[DATA_KEY_FOR_COLOR]    = ChapterColorGenerator.getRandomColor() ;
            }
        });
        if( noColorChapterArray.length ){
            // List가 map이였다면 loop는 필요 없을거 같다. noColorChapterArray만 뽑아서 하면 좋을텐데..
            // 일단 Loop로 구현한다.
            inputChapterList.forEach( (inputChapterData) => {
                const chapterId = inputChapterData[DATA_KEY_FOR_ID];
                if( noColorChapterArray.includes( chapterId ) ){
                    const remainColor = ChapterColorGenerator.getDifferentRandom(usedColorSet);
                    inputChapterData[DATA_KEY_FOR_COLOR]    = remainColor;
                    usedColorSet.add(remainColor);
                }
            });
        }
        inputChapterList.sort( (first, second )=>{
            if (first[DATA_KEY_FOR_INDEX]  > second[DATA_KEY_FOR_INDEX]) {
                return 1;
            }
            if (first[DATA_KEY_FOR_INDEX] < second[DATA_KEY_FOR_INDEX]) {
                return -1;
            }
            return 0;
        });
        return  inputChapterList;
    };
    const getNoteSortingData = ( channelId ) => {
        return totalChaptersSortingData.get(channelId);
    };

    const setNoteSortingData = ( channelId, savedData) =>{
        totalChaptersSortingData.set(channelId, savedData);
    };

    return {
        initSorter: loadFromStorage,
        sortingChapter: sortingChapter,
        updateSorter: updateSortingData
    }

} )();


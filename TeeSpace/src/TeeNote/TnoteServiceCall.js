NOTEBOOK_DTO_KEY = {
    ID: "id",
    CHILDREN: "children",
    MODIFY_DATE: "modified_date",
    NOTE_CHANNEL_ID: "note_channel_id",
    NOTEBOOK_INDEX: "notebook_index",
    PARENT_NOTEBOOK: "parent_notebook",
    TEXT: "text",
    TYPE: "type",
    USER_NAME: "user_name",
    WS_ID: "ws_id"
};

TNoteServiceCaller = (function(){
    let noteChannelId;
    let workspaceId;
    let serviceCallUrl;
    const METHOD_TYPE = {
        GET: "GET",
        POST: "POST",
        DELETE: "DELETE",
        PUT: "PUT",
        OPTION: "OPTION",
    };
    const METHOD_ARRAY = Object.values(METHOD_TYPE);
    let SERVICE_CALL={};
    METHOD_ARRAY.forEach( function (methodName) {
        SERVICE_CALL[methodName]= ( function( methodName ) {
            function async( serviceParameter, inputJson, successFunction, failCallBack ) {
                return serviceCall( serviceParameter, methodName, true, inputJson, successFunction, failCallBack );
            }
            function sync ( serviceParameter, inputJson, successFunction, failCallBack ) {
                return serviceCall( serviceParameter, methodName, false, inputJson, successFunction,failCallBack );
            }
            function autoCall ( serviceParameter, inputJson, callback, dtoName ){
                if(typeof callback === "function"){
                    async(
                        serviceParameter,
                        inputJson,
                        function(response) {
                            response = JSON.parse(response);
                            if( dtoName ) {
                                callback(response["dto"][dtoName])
                            }else{
                                callback(response["dto"]);
                            }
                        },
                        function( response ){
                            callback(response);
                        }
                    );
                }else{
                    let result = null;
                    sync(
                        serviceParameter,
                        inputJson,
                        function( response ){
                            response = JSON.parse(response);
                            if( dtoName ){
                                result = response["dto"][dtoName];
                            }else{
                                result = response["dto"];
                            }
                        },
                        function( response ){
                            result = response;
                        }
                    );
                    return result;
                }
            }

            let outerCall;
            if( methodName === METHOD_TYPE.GET){
                outerCall = function ( serviceParameter, callback, dtoName ){
                    return autoCall( serviceParameter, null, callback, dtoName );
                }
            } else{
                outerCall = function ( serviceParameter, inputJson, callback, dtoName ){
                    return autoCall( serviceParameter, inputJson, callback, dtoName );
                }
            }
            return {
                autoCall: outerCall
            }
        })(methodName);
    });


    function serviceCall( serviceParameter, methodType, sync, inputJson, successFunction,failCallBack ){

        let ajaxCallServiceCall = {
            url: serviceCallUrl+serviceParameter,
            type: methodType,
            // dataType: 'json',
            contentType: 'application/json; charset=UTF-8',
            async:sync,
            success: successFunction,
            error: failCallBack
        };
        if( inputJson ){
            let targetJson = { "dto" : inputJson };
            ajaxCallServiceCall["data"] = JSON.stringify(targetJson);
        }
        Top.Ajax.execute(ajaxCallServiceCall );

    }

    function init(spaceId, channelId){
        serviceCallUrl= _workspace.url
        workspaceId = spaceId;
        noteChannelId = channelId;
    }

    return{
        initServiceCaller: init,
        noteBookCreate : function( name, callback ){
            return SERVICE_CALL[METHOD_TYPE.POST].autoCall(
                'Note/notebooks',
                {
                    "id" : "",
                    "note_channel_id" : noteChannel_Id,
                    "text" : name,
                    "children" : [],
                    "type" : "notebook",
                    "USER_ID": userManager.getLoginUserId(),
                    "user_name": JSON.parse(sessionStorage.getItem('userInfo')).userName
                },
                callback,
                null
            ); 
        },
        noteBookDelete : function(noteBookId, callback ){
            return SERVICE_CALL[METHOD_TYPE.DELETE].autoCall(
                'Note/notebook?action=Delete&id=' +noteBookId +'&note_channel_id='+noteChannel_Id+'&USER_ID='+userManager.getLoginUserId(),
                callback,
                null
            );
        },
        noteBookList : function ( callback ){
            return SERVICE_CALL[METHOD_TYPE.GET].autoCall(
                "Note/notebooks?action=List&note_channel_id=" + noteChannelId,
                callback,
                "notbookList" );
        },
        getBookTypeList : function ( callback ){
            return SERVICE_CALL[METHOD_TYPE.GET].autoCall(
                "Note/notebook?action=List&note_channel_id=" + noteChannelId+'&type=notebook'+ "&t=" + new Date().getTime().toString(),
                callback,
                "notbookList" );
        },
        getStackTypeList : function ( callback ){
            return SERVICE_CALL[METHOD_TYPE.GET].autoCall(
                'Note/notebook?action=List&note_channel_id=' + noteChannelId + '&type=stack'+ "&t=" + new Date().getTime().toString(),
                callback,
                "notbookList" );
        },
        noteChapterList : function( callback ){
            return SERVICE_CALL[METHOD_TYPE.GET].autoCall(
                'Note/noteChapter?action=List&note_channel_id=' + noteChannelId + "&t=" + new Date().getTime().toString(),
                callback,
                "notbookList" );
        }, 
        noteBookParentUpdate : function(notebookId, callback){
            return SERVICE_CALL[METHOD_TYPE.PUT].autoCall(
                'Note/notebooks?action=Update',
                {
                    "id" : notebookId,
                    "parent_notebook" : ""
                },
                callback,
                null
            );
        },
        noteBookUpdate : function (noteId, contents, parentId, callback){
            SERVICE_CALL[METHOD_TYPE.PUT].autoCall(
                'Note/notebooks?action=Update&id=' + noteId,
                {
                    "id" : noteId,
                    "text": contents,
                    "parent_notebook": parentId,
                    "user_name": JSON.parse(sessionStorage.getItem('userInfo')).userName,
                    "note_channel_id": noteChannelId,
                    "USER_ID": userManager.getLoginUserId(),
                },
                callback,
                null
            );
        },// NoteCreate일때 workspaceId를 제외하자.
        noteCreate : function(noteName, parent, callback){
            return SERVICE_CALL[METHOD_TYPE.POST].autoCall(
                'Note/note',
                {
                    "WS_ID": workspaceId,
                    "note_channel_id": noteChannelId,
                    "CH_TYPE": "CHN0003",
                    "parent_notebook": parent,
                    "note_title": noteName,
                    "USER_ID": userManager.getLoginUserId(),
                    "user_name":userManager.getLoginUserName(),
                    "is_edit": userManager.getLoginUserId()
                },
                callback,
                null
            );
        },
        noteList : function (targetNoteBookId, callback){
            return SERVICE_CALL[METHOD_TYPE.GET].autoCall(
                'Note/note?action=List&note_channel_id=' + noteChannelId + '&parent_notebook=' + targetNoteBookId + "&USER_ID=" + userManager.getLoginUserId() + "&t=" + new Date().getTime().toString(),
                callback,
                "noteList"
            );
        },
        noteDelete: function (noteList, callback){
            return SERVICE_CALL[METHOD_TYPE.POST].autoCall(
                'Note/note?action=Delete',
                {
                    "noteList" : noteList
                },
                callback,
                null
            );
        },
        noteTrash : function(noteList, callback){
            return SERVICE_CALL[METHOD_TYPE.POST].autoCall(
                'Note/noteTrash',
                {
                    "noteList" : noteList
                },
                callback,
                null
            );
        },
        noteTrashList : function (callback){
            // wsid --> channelID
            // let workspaceId= Top.curRoute.before.split('/')[2]
            return SERVICE_CALL[METHOD_TYPE.GET].autoCall(
                'Note/noteTrash?action=List&note_channel_id=' + noteChannelId + '&WS_ID=' + workspaceId + "&t=" + new Date().getTime().toString(),
                callback,
                "noteList"
            );
        },
        noteTrashDelete : function (noteList, callback){
            return SERVICE_CALL[METHOD_TYPE.POST].autoCall(
                'Note/noteTrash?action=Delete',
                {
                    "noteList" : noteList
                },
                callback,
                null
            );
        },
        noteTrashUpdate : function(note_id, p_id, callback){
            return SERVICE_CALL[METHOD_TYPE.PUT].autoCall(
                'Note/note?action=Update',
                {
                    "WS_ID" : workspaceId,
                    "USER_ID": userManager.getLoginUserId(),
                    "note_id": note_id,
                    "is_erased" : 1,
                    "parent_notebook":p_id,
                    "is_favorite": userId,
                    "TYPE": "default"
                },
                callback,
                null
            );
        },
        noteParentUpdate : function(noteId, parentNoteBook, callback){
            // wsid --> channelID
            // let workspaceId= Top.curRoute.before.split('/')[2];
            return SERVICE_CALL[METHOD_TYPE.PUT].autoCall(
                'Note/note?action=Update',
                {
                    "WS_ID": workspaceId,
                    "CH_TYPE": "CHN0003",
                    "note_id": noteId,
                    "parent_notebook": parentNoteBook,
                    "note_channel_id" : noteChannelId,
                    "user_name": userManager.getLoginUserName(),
                    "USER_ID": userManager.getLoginUserId(),
                    "TYPE": "MOVE"
                },
                callback,
                null
            );
        },
        noteTitleUpdate : function(noteId, noteTitle, pId, callback){
        	return SERVICE_CALL[METHOD_TYPE.PUT].autoCall(
    			'Note/note?action=Update',
    			{
    				"WS_ID": workspaceId,
    				"CH_TYPE": "CHN0003",
    				"note_id": noteId,
    				"note_channel_id" : noteChannelId,
    				"note_title" : noteTitle,
    				"parent_notebook": pId,
    				"USER_ID": userManager.getLoginUserId(),
    				"TYPE": "RENAME"
    			},
    			callback,
    			null
        	);
        },
        noteEditUpdate : function(noteId,parentNoteBook, callback){ // 수정버튼을 눌렀을 때 수정 중으로 업데이트 하기 위한 서비스 콜
            // let workspaceId= Top.curRoute.before.split('/')[2];
            return SERVICE_CALL[METHOD_TYPE.PUT].autoCall(
                'Note/note?action=Update',
                {
                    "WS_ID": workspaceId,
                    "CH_TYPE": "CHN0003",
                    "note_channel_id": noteChannelId,
                    "USER_ID": userManager.getLoginUserId(),
                    "note_id": noteId,
                    "user_name": userManager.getLoginUserName(),
                    "is_edit": userManager.getLoginUserId(),
                    "parent_notebook": parentNoteBook,
                    "TYPE": "EDIT_START"
                },
                callback,
                null);
        },
        noteNonEditUpdate : function(noteId,parentNoteBook, callback){ // 수정 중 나가기 했을때 업데이트 하기 위한 서비스 콜

            // let workspaceId= Top.curRoute.before.split('/')[2];
            return SERVICE_CALL[METHOD_TYPE.PUT].autoCall(
                'Note/note?action=Update',
                {
                    "WS_ID": workspaceId,
                    "CH_TYPE": "CHN0003",
                    "USER_ID": userManager.getLoginUserId(),
                    "note_channel_id" : noteChannelId,
                    "note_title": "",
                    "note_id": noteId,
                    "is_edit": "",
                    "note_content": "",
                    "parent_notebook":parentNoteBook,
                    "TYPE": "NONEDIT",
                    "is_favorite":"none"
                },
                callback,null);
        },// 이걸로 에디트는 통일한다.
        noteUpdate : function (noteInfo, callback ){ // 기존에 사용하던 저장 시 업데이트 치는 함수임.
            // let workspaceId= Top.curRoute.before.split('/')[2];
            let inputDto = {};
            inputDto["WS_ID"] = workspaceId;
            inputDto["CH_TYPE"] = "CHN0003";
            inputDto["USER_ID"] = userManager.getLoginUserId();
            inputDto["user_name"] =  userManager.getLoginUserName();
            inputDto["note_id"] = noteInfo["note_id"];
            inputDto["note_title"] = noteInfo["note_title"]
            inputDto["note_content"] = noteInfo["note_content"]
            inputDto["parent_notebook"] = noteInfo["parent_notebook"]
            inputDto["note_channel_id"] = noteChannelId;
            inputDto["is_edit"] = "";
            inputDto["TYPE"] = "EDIT_DONE"
            if( noteInfo ) {
                if( noteInfo["title"] ) {
                    inputDto["note_title"] = noteInfo["title"];
                }
            }
            return SERVICE_CALL[METHOD_TYPE.PUT].autoCall(
                'Note/note?action=Update',
                inputDto,
                callback,
                null);
        },
        allnoteList : function(type, callback ){
            return SERVICE_CALL[METHOD_TYPE.GET].autoCall(
                'Note/allnote?action=List&note_channel_id=' + noteChannelId + "&USER_ID=" + userManager.getLoginUserId() + "&TYPE="+ type +"&t=" + new Date().getTime().toString(),
                callback,
                "noteList");
        },
        noteInfoList : function(note_id, callback){
        	return SERVICE_CALL[METHOD_TYPE.GET].autoCall(
        			'Note/noteinfo?action=List&note_id=' + note_id + '&note_channel_id=' + noteChannelId + "&t=" + new Date().getTime().toString(), 
                    callback,
                    "noteList");
        },
        tagCreate : function(noteId, tagName, callback){
            return SERVICE_CALL[METHOD_TYPE.POST].autoCall(
                'Note/tag',
                {
                    "tag_id" : "",
                    "text" : tagName,
                    "icon":"",
                    "note_id": noteId
                },
                callback,
                null);
        },
        tagList : function(noteId, callback){ // 마찬가지로 렌더안하고 데이터만 얻는 경우를 위해 임시로 renderFlag 적용. 일일이 순회하며 수정하지 않고, renderFlag가 없으면 렌더를 하고 "don't Render" 등의 파라미터를 넘길 경우엔 데이터만 받는 식으로 활용할 예정
            return SERVICE_CALL[METHOD_TYPE.GET].autoCall(
                'Note/tag?action=List&note_id=' + noteId + "&t=" + new Date().getTime().toString(),
                callback,
                null);
        },
        notetagList : function(noteId, callback){
            return SERVICE_CALL[METHOD_TYPE.GET].autoCall(
                'Note/tag?action=List&note_id=' + noteId + "&t=" + new Date().getTime().toString(),
                callback,
                null);
        },
        tagDelete : function(noteId, tagId, callback){
            return SERVICE_CALL[METHOD_TYPE.POST].autoCall(
                'Note/tag?action=Delete&tag_id=' + tagId,
                {
                    "tag_id" : tagId,
                    "note_id" : noteId
                },
                callback,
                null);
        },
        alltagList : function( callback){
            return SERVICE_CALL[METHOD_TYPE.GET].autoCall(
                'Note/alltag?action=List&note_channel_id=' + noteChannel_Id,
                callback,
                "tagList");
        },
        tagNoteList : function(tagId, callback){
            return SERVICE_CALL[METHOD_TYPE.GET].autoCall(
                'Note/tagnote?action=List&tag_id=' + tagId + "&USER_ID=" + userManager.getLoginUserId() + "&note_channel_id=" + noteChannelId,// + "&t=" + new Date().getTime().toString(),
                callback,
                "noteList");
        },
        tagSortList : function( callback ){
            return SERVICE_CALL[METHOD_TYPE.GET].autoCall(
                'Note/tagSort?action=List&note_channel_id=' + noteChannelId + "&t=" + new Date().getTime().toString(),
                callback,
                "tag_index_list_dto");
        },
        noteFavoriteCreate : function(noteId, callback){
            return SERVICE_CALL[METHOD_TYPE.POST].autoCall(
                'Note/notefavorite',
                {
                    "note_channel_id" : noteChannelId,
                    "note_id" : noteId,
                    "USER_ID" : userManager.getLoginUserId()
                },
                callback,
                null);
        },
        noteFavoriteList : function(callback){
            return SERVICE_CALL[METHOD_TYPE.GET].autoCall(
                'Note/notefavorite?action=List&USER_ID=' + userManager.getLoginUserId() + '&note_channel_id=' + noteChannelId + "&t=" + new Date().getTime().toString(),

                callback,
                "noteList");
        },
        noteFavoriteDelete : function(noteId, callback){
            return SERVICE_CALL[METHOD_TYPE.POST].autoCall(
                'Note/notefavorite?action=Delete',
                {
                    "note_id" : noteId,
                    "USER_ID" : userManager.getLoginUserId()
                },
                callback,
                null);
        },
        // 여기부터 share 서비스콜
        noteShareCreate : function(shareNoteList, callback){
            return SERVICE_CALL[METHOD_TYPE.POST].autoCall(
                'Note/noteshare',
                {
                    "noteList" : shareNoteList
                },
                callback,
                null
            );
        },
        noteShareDelete : function(shareDelNoteList, callback){
            return SERVICE_CALL[METHOD_TYPE.POST].autoCall(
                'Note/noteshare?action=Delete',
                {
                    "noteList" : shareDelNoteList
                },
                callback,
                null
            );
        },
        noteShareUpdate : function(noteInfo, callback){
        	/*
        	 * 추후 수정 필요
        	 */
            let inputDto = {}; 
            inputDto["USER_ID"] = userManager.getLoginUserId();
            inputDto["user_name"] =  userManager.getLoginUserName();
            inputDto["note_id"] = noteInfo["note_id"];
            inputDto["note_title"] = noteInfo["note_title"]
            inputDto["note_content"] = noteInfo["note_content"]
            inputDto["parent_notebook"] = noteInfo["parent_notebook"]
            inputDto["is_edit"] = noteInfo["is_edit"];
            inputDto["TYPE"] = "default";
            	
            return SERVICE_CALL[METHOD_TYPE.PUT].autoCall(
                'Note/noteshare?action=Update',
                inputDto,
                callback,
                null
            );
        },
        chapterShareList : function(callback){
            return SERVICE_CALL[METHOD_TYPE.GET].autoCall(
                'Note/chaptershare?action=List' + '&note_channel_id=' + noteChannelId + "&t=" + new Date().getTime().toString(),
                callback,
                null
            );
        },
        chapterShareCreate : function(shareChapterList, callback){
            return SERVICE_CALL[METHOD_TYPE.POST].autoCall(
                'Note/chaptershare',
                {
                    "notbookList" : shareChapterList
                },
                callback,
                null
            );
        },
        chapterShareDelete : function(shareChapterDelList, callback){
            return SERVICE_CALL[METHOD_TYPE.POST].autoCall(
                'Note/chaptershare?action=Delete',
                {
                    "notbookList" : shareChapterDelList
                },
                callback,
                null
            );
        },
        chapterShareUpdate : function(shareChapterList, callback){
            return SERVICE_CALL[METHOD_TYPE.PUT].autoCall(
                'Note/chaptershare?action=Update',
                {
                    "id" : noteId,
                    "text": contents,
                    "USER_ID": userManager.getLoginUserId(),
                    "user_name": userManager.getLoginUserName()
                },
                callback,
                null
            );
        },
        // 여기까지 share
    	noteFileList : function ( callback ){
    		return SERVICE_CALL[METHOD_TYPE.GET].autoCall(
    			'Note/noteFile?action=List&WS_ID=' + workspaceId + '&note_channel_id=' + noteChannelId + "&t=" + new Date().getTime().toString(),
                callback,
                null);
    	},
    	noteFileDelete : function (fileId, callback){
    		return SERVICE_CALL[METHOD_TYPE.DELETE].autoCall(
				'Storage/StorageFile',
                {
					"workspace_id": workspaceManager.getWorkspaceId(getRoomIdByUrl(), "CHN0003"),
    		        "channel_id": noteChannel_Id,
    		        "storageFileInfo": {
	    		        "user_id": "",
	    		        "file_last_update_user_id": "",
	    		        "file_id": fileId,
	    		        "file_name": "",
	    		        "file_extension": "",
	    		        "file_created_at": "",
	    		        "file_updated_at": "",
	    		        "user_context_1": "",
	    		        "user_context_2": "",
	    		        "user_context_3": ""
    		        }
                },
                callback,
                null);
    	}
    }
})();
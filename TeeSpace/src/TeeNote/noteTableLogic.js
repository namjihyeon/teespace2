var temp2 = [] //클릭된 노트북,스택 테이블 RAW 데이터
var temp_test = [];
Top.Controller.create('noteFileLayoutLogic', {
	notepreviewFile : function() {
		let fileMeta = Top.Dom.selectById("noteFileTable").getSelectedData();
		let previewFileList = noteRepo.noteFileList;
		var filePreviewLayoutLogic = Top.Controller.get("filePreviewLayoutLogic");
	    filePreviewLayoutLogic.previewFileMeta = fileMeta;
	    filePreviewLayoutLogic.curChannelId = noteChannel_Id;
	    for(var i=0; i<previewFileList.length; i++) {
            filePreviewLayoutLogic.srcFiles.push({ 
                fileMeta : previewFileList[i],
                blobUrl : String.empty
            });
        }
	    UTIL.ID("filePreviewDialog").open(true)
	},
	notefiledownload : function(event, widget) {
		if(widget.getSelected().id == "select2" ){
			for(i=0;i<inputCheck.length;i++){
				TNoteFileController.onFileDownload(inputCheck[i].file_id)
			}
		}
	}	
});

Top.Controller.create('noteTitleLayoutLogic', {
	// 기현 <--- 추후에 이메일 공유 할때
	Send_email : function(event, widget) {
		let CheckedList = _noteListView.getCheckedData();
		var note_contents = [];
		var note_receiverAddr = [];
		var note_subject = [];
		var note_sender = [];
		note_receiverAddr.push('qwer2@cs.co.kr');
		note_sender = 'qwer1@cs.co.kr';
		if(workspaceManager.getChannelList(workspaceManager.getMySpaceId(), CLOUDSPACE_CHANNELS.TMAIL.CHANNEL_TYPE) != undefined){
			accountId = workspaceManager.getChannelList(workspaceManager.getMySpaceId(), CLOUDSPACE_CHANNELS.TMAIL.CHANNEL_TYPE);
		}
		else{
			alert("메일계정이 없습니다!");
		}
		if(CheckedList.length == 1){
			note_contents = CheckedList[0].note_content;
			note_subject = CheckedList[0].note_title;
			SendMail(note_sender, accountId, note_receiverAddr, note_subject, note_contents);
		}
		else{
			
			for(i=0;i<CheckedList.length;i++){
				note_contents += CheckedList[i].note_content;
				if(i != CheckedList.length-1){
					note_contents += "<hr size=20px color=black></hr>";
				}
			}
			note_subject = CheckedList[0].note_title + " 외 " + (CheckedList.length-1) + "개";
			SendMail(note_sender, accountId, note_receiverAddr, note_subject, note_contents);
			
		}
	}
});
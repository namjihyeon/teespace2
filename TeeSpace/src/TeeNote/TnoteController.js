TNoteController = (function () {

    let workspaceId = null;
    let noteChannelId = null;
    let renderer = null;
    let serviceCaller = null;
    
    const MODAL_KEY ={
            LEFT:'div#AppLnbLayout',
            TOP:'div#AppGnbLayout',
//            SIDEBAR:'#sidebarParentLayout',
            TITLE_SEARCH:'top-imagebutton#noteSearchBtn',
            CLOSE_BUTTON:'span.note-title-close-btn',
            PRE_BUTTON:'top-imagebutton#notePreBtn',
            LNB_LIST:'div#note-lnb-menus-cover',
        }
    const MODAL_ARRAY = {
        'dialogOverlay1' : document.createElement("div"),
        'dialogOverlay2' : document.createElement("div"),
        'dialogOverlay3' : document.createElement("div"),
        'dialogOverlay4' : document.createElement("div"),
        'dialogOverlay5' : document.createElement("div"),
        'dialogOverlay6' : document.createElement("div"),
    }

    /************************************************************************/
    
    /** Controller & Data Init                                             **/
    
    /************************************************************************/
    function init(spaceId, channelId) {
        workspaceId = spaceId;
        noteChannelId = channelId;
        serviceCaller = TNoteServiceCaller;
        renderer = TNoteRenderer;
        controllerSet2();
        TNoteChaperShorter.initSorter();
    }

    function loadController(topRenderer) {
        renderer = topRenderer;
        controllerSet2();
//        initTagTreeOption();
    }

    async function initChapterList() {
    	if(document.getElementById('chapter-name-textfield')){ document.getElementById('chapter-name-textfield').classList.add('exception') }
    	if(document.getElementById('page-name-textfield')){ document.getElementById('page-name-textfield').classList.add('exception') }
    	let localNoteBookList = serviceCaller.noteChapterList();
    	let targetChapterList = TNoteChaperShorter.sortingChapter(noteChannelId,localNoteBookList);

		let targetDiv = document.getElementById('note-chapter-cover');
		await noteLnbMenu.renderChapters(targetDiv, targetChapterList)
		if(noteLnbMenu.isExpanded()){ // 확장된 경우
			noteLnbMenu.selectPageAfterUpdate()
		}
		return Promise.resolve("");
	}
	
    function initTagMenu(callback) {
//        let allTagList = serviceCaller.alltagList();
//        allTagList = TNoteData.setAllTagList(allTagList);
        
        let targetDiv = document.getElementById('note-tag-cover')
        noteLnbMenu.renderTag(targetDiv, {}, 'tag')
//        tagTreeNodes[0].children = [];
//        setTagTreeNodes(allTagList);
        
        if (typeof callback === "function") {
            callback();
        }
    }
    function initSharedMenu(){
    	if(document.getElementById('chapter-name-textfield')){ document.getElementById('chapter-name-textfield').classList.add('exception') }
    	if(document.getElementById('page-name-textfield')){ document.getElementById('page-name-textfield').classList.add('exception') }
    	//targetDIV, targetData, type
    	TNoteServiceCaller.chapterShareList(function(data){
    		if(data.noteList.length > 0){
    			let targetDiv1 = document.getElementById('note-shared-page-cover');
    			noteLnbMenu.renderSharedPage(targetDiv1, data.noteList);
    		}
    		if(data.chapterList.length>0){
//            	let targetDiv2 = document.getElementById('note-shared-chapter-cover');
//            	noteLnbMenu.renderSharedChapter(targetDiv2, data.chapterList);    			
    		}
    	})
    }
    
    function setTagTreeNodes(tagList) {
      renderer.renderLNBTagTree(tagList);
    }
    
    function controllerSet2() {
        _ctrEvent = Top.Controller.get('TopApplicationLogic');
        _ctrAllNote = Top.Controller.get('allNoteLayoutLogic');
        _ctrLnbList = Top.Controller.get('noteLnbListLayoutLogic');
        _ctrTag = Top.Controller.get('tagLayoutLogic');
        _ctrTrashCan = Top.Controller.get('trashCanLayoutLogic');
        _ctrLnb = Top.Controller.get('noteLnbLayoutLogic');
        _ctrTable = Top.Controller.get('noteTableLayoutLogic');
        _ctrEditor = Top.Controller.get('noteEditorLayoutLogic');
    }
    
    

    /************************************************************************/
    
    /** LNB Menu Render                                                    **/

    /************************************************************************/
    function checkVisibilityWidth(selector){
    	if($(selector)[0].style.display==='none') return 0;
    	else return $(selector).width();
	}
	
    function checkVisibilityHeight(selector){
    	if($(selector)[0].style.display==='none') return 0;
    	else return $(selector).height();
	}
	
    function setModalSize(){
    	$('.noteEditor_overlay1').css({"width": checkVisibilityWidth(MODAL_KEY.LEFT), "height":checkVisibilityHeight(MODAL_KEY.LEFT), "margin-top":checkVisibilityHeight(MODAL_KEY.TOP)})
        $('.noteEditor_overlay2').css({"width": checkVisibilityWidth(MODAL_KEY.TOP), "height": checkVisibilityHeight(MODAL_KEY.TOP)})
        $('.noteEditor_overlay3').css({"width": checkVisibilityWidth(MODAL_KEY.TITLE_SEARCH), "height": checkVisibilityHeight(MODAL_KEY.TITLE_SEARCH)})
        $('.noteEditor_overlay4').css({"width": checkVisibilityWidth(MODAL_KEY.CLOSE_BUTTON), "height": checkVisibilityHeight(MODAL_KEY.CLOSE_BUTTON)})
        $('.noteEditor_overlay5').css({"width": checkVisibilityWidth(MODAL_KEY.PRE_BUTTON), "height": checkVisibilityHeight(MODAL_KEY.PRE_BUTTON)})
        $('.noteEditor_overlay6').css({"width": checkVisibilityWidth(MODAL_KEY.LNB_LIST), "height": checkVisibilityHeight(MODAL_KEY.LNB_LIST)})
    }

    /**
	 * LNB PAGE 메뉴
	 */
    
    function selectPageMenu(targetId, isNew) {
    	let pageInfo = serviceCaller.noteInfoList(targetId);
    	let selectData = pageInfo[0];
		TNoteData.setCurPageData(selectData.parent_notebook, selectData.note_id);
        TNoteRenderer.renderPage(selectData);
		
		// (sj) TODO-check. 이게 여기 있어야 하나?
        document.querySelector('#noteEditor div.se-wrapper-wysiwyg').setAttribute("id","editorDIV");
        
        if(isNew){
        	UTIL.ID('contenteditorTitle').setText("");
        	document.querySelector('#note li.selected').style.color = "#c6ced6"
        }else if(selectData.note_title === "제목 없음"){
        	UTIL.ID('contenteditorTitle').setText("");
        }else {
        	UTIL.ID('contenteditorTitle').setText(`${selectData.note_title}`);
        }
    	
    	document.querySelector('top-htmleditor#noteEditor').setEditorHTML(selectData.note_content);
		
		UTIL.ID('noteEditorModifiedUser').setText(TNoteUtil.getUserName(selectData));
    	UTIL.ID('noteEditorTimeStamp').setText(TNoteUtil.getNoteDateFormat(selectData.modified_date));
        Note.tagList(selectData.note_id, function(){
        	noteTag.updateTagMoreButton();
        });
        
        if(selectData.is_edit && selectData.is_edit !== userManager.getLoginUserId()){
        	setCannotEditMode(TNoteUtil.getUserName(selectData))
        } else {
        	editorReadMode()
		}
		// (SJ) 나중에 필요한지 체크해서 필요없으면 지우기
        if($('#note div#editorTitle .note-divider__column').length === 0) noteLnbMenu.addCommonButtonContainer($('#note div#editorTitle')[0])
		
		// 원래 myIsExpanded로 되어있던 것 수정-> 아직 url 바뀌지 않았을 때 로직을 타서
		noteLnbMenu.setExpandedIcon(noteLnbMenu.isExpanded());
//        Note.checkDOM('#note .tags-length-checker', function(){
//        	noteTag.updateTagMoreButton();
//            $(window).resize(function(){
//            	noteTag.updateTagMoreButton();
//            })
//        })
        
    }
    /**
     * LNB 태그 메뉴
     */
    function selectTagMenu() {
    	if(noteLnbMenu.isExpanded()){
    		noteLnbMenu.setLnbVisibility('flex')
            noteLnbMenu.setEditorVisibility('none')
            noteLnbMenu.setFoldingVisibility('none')
            noteLnbMenu.setTagVisibility('flex')
            document.getElementById('tagPreBtn').style.display = "none"
    	}else {
    		noteLnbMenu.setLnbVisibility('none')
            noteLnbMenu.setEditorVisibility('none')
            noteLnbMenu.setFoldingVisibility('none')
            noteLnbMenu.setTagVisibility('flex')
            document.getElementById('tagPreBtn').style.display = "inline"
        	TNoteController.tagEventBind();
    	}
    	
    	let allnoteList = serviceCaller.allnoteList('Tree');
    	TNoteData.setNoteList(allnoteList);
        let allTagList = TNoteServiceCaller.alltagList();
        if(allTagList.length>0){
        	TNoteData.setAllTagList(allTagList);
            let tagKeywordList = TNoteServiceCaller.tagSortList();
            TNoteData.setSortedTagList(tagKeywordList);
//            if(document.getElementById('note-Tag-search-textfield').value != ""){
            	document.getElementById('note-Tag-search-textfield').value = ""
            	$('div#TagsearchResultLayout').remove()
            	$('div#searchResultLayout').remove()
            	//document.getElementById('tagLayout').style.display = "block"
            	$('top-accordiontab#tag_kor_tab').css('display',"block")
        		$('top-accordiontab#tag_eng_tab').css('display',"block")
        		$('top-accordiontab#tag_num_tab').css('display',"block")
        		$('top-accordiontab#tag_etc_tab').css('display',"block")
        		for(let i=0; i<4; i++){//Tab 초기화
        			UTIL.ID('tag_accordion').closeTab(i)
        		}
            //}
        	document.getElementById('note-tag-tagLayout').style.display = 'block';
			document.getElementById('note-tag-empty-div').style.display = 'none';
			document.querySelector('#note-tag-tagTitleLayout #note-Tag-search').style.display = "";
			let divider = document.querySelector('#note-tag-tagTitleLayout .note-divider__column');
			if (divider) {
				divider.style.display = "";
			}
        } else {
        	document.getElementById('note-tag-tagLayout').style.display = 'none';
			document.getElementById('note-tag-empty-div').style.display = 'flex';			
			document.querySelector('#note-tag-tagTitleLayout #note-Tag-search').style.display = "none";
			let divider = document.querySelector('#note-tag-tagTitleLayout .note-divider__column');
			if (document.getElementById('note-tag-empty-div').style.display !== "none" && divider) {
				divider.style.display = "none";
			}
        }
        renderer.renderTagMenu();
        noteLnbMenu.setExpandedIcon(noteLnbMenu.isExpanded());
    }

    /**
     * LNB 공유 받은 페이지 메뉴
     */
    function selectSharedMenu(){
    	
    }
    
    // 필요할수도 ?
    function setNoteBookTreeNodes(noteBookList) {
//        TNoteUtil.log("Set LNB NoteBookTree= ", noteBookList);
        renderer.renderLNBNoteBookTree(noteBookList);
    }

    function openTagCategory(layout, languageType) {
        let targetLanguageSet = TNoteData.getSortedTagSet(TAG_CATEGORY[languageType]);
        renderer.renderCategory(layout, targetLanguageSet);
    }

    function selectTreeTag(targetTagId, tagName) {
        let noteList = serviceCaller.tagNoteList(targetTagId);
        noteList = TNoteData.setNoteList(noteList);
        renderer.renderTag(tagName, noteList);
    }
    function selectFileMenu(){
    	let fileList = serviceCaller.noteFileList();
    	fileList = TNoteData.setFileList(fileList);
    	renderer.renderFile(fileList)
    }
    
    
    /************************************************************************/
    
    /** Event Binding                                                       **/

    /************************************************************************/
    
    function editorEventBind(){
    	let preBtn = $('#notePreBtn');
    	preBtn.on("click", function(e){
    		if(!noteLnbMenu.isExpanded()){
    			noteLnbMenu.setLnbVisibility('flex')
    			noteLnbMenu.setEditorVisibility('none')
    		}
    	})
    }
    function tagEventBind(){
    	let preBtn = $('#tagPreBtn');
    	preBtn.on("click", function(e){
    		if(!noteLnbMenu.isExpanded()){
    			noteLnbMenu.setLnbVisibility('flex')
    			noteLnbMenu.setTagVisibility('none')
    		}
    	})
    }
    /*
     *  TextField 이벤트 관리
     *  keyup이벤트는 마우스로 입력한 경우를 캐치하지 못하여 input 이벤트를 이용하기로 함.
     *  input이벤트는 IE8이하는 미작동, IE9는 버그를 포함한다.만약 문제가 될 시, 
     *  1. keyup 이벤트와 blur 이벤트를 함께 수신하는 식으로 해결 가능
     *  2. polyfill - slendid textchange 플러그인. input 이벤트를 흉내내는 textchange라는 합성 이벤트를 제공한다.
     */

    function setEditorTitleTextFieldEvent() {
    	$("input#contenteditorTitle").off('input').on("input", function(event) {
			let text = event.target.value;
    		if(text.length>249)	$("input#contenteditorTitle")[0].value = text.substr(0,250);
		});
	}    
	
	function addTagEvent(tagName) {
		if(_ctrEditor.checkExistingTag(tagName) || tagName.trim()===""){
			document.querySelector('#note .note-tag-add-textfield').value = "";
			document.querySelector('#note .note-tag-add-textfield').style.display = "none";
			noteTag.updateTagMoreButton();
		} else {
			let tagDTO = {"id":"","text": tagName,"note_id": note_id}
			document.querySelector('#note .note-tag-add-textfield').style.display = "none";
			document.querySelector('#note .note-tag-add-textfield').value = "";
			document.querySelector('.note-tag-add-icon svg path').setAttribute('fill','#75757F')
			// Create할 Chip데이터
			tagArray.push(tagDTO)
			// 화면에 보여지기 위한 Chip데이터
			let frag = document.createDocumentFragment();
			noteTag.renderTagChipNode(tagDTO, frag);
			$('#note .note-tag-add-textfield').after(frag);
			noteTag.setEventOnTagChip();
			noteTag.updateTagMoreButton();
		}
	}

    function setTagAddTextFieldEvent() {
    	//IMS 222979 아래 코드로 대체함
//    	$('#note input.note-tag-add-textfield').off("blur").on("blur", function(event) {
//    		event.stopPropagation();
//    		event.preventDefault();
//			addTagEvent(event.target.value);
//			document.querySelector('.note-tag-add-icon svg path').setAttribute('fill','#75757F')
//    	});
    	/*
    	 * IMS 222979 태그의 텍스트 필드 닫히지 않는 이슈. blur event로 텍스트필드 외부 클릭 시 수정 마무리를 시키는 방식으로는 해결이 안되어,
    	 * 클릭이벤트에서 태그 추가 버튼이나, 태그 추가 텍스트필드 안쪽을 클릭하는 경우를 제외한 경우 마무리 하도록 수정했음.
    	 * 이게 정확히 맞는 구현인지는 아직 확신은 서지 않는다.
    	 * 2020-04-20 유재웅
    	 * */
    	document.getElementById('note').addEventListener('click', function(e){
    		if(e.path.includes(document.querySelector('#noteTagContainer svg')) || e.path.includes(document.querySelector('input#tagTextField'))){
    			return;
    		}  else {
    			addTagEvent(document.querySelector('input#tagTextField').value);
    			document.querySelector('.note-tag-add-icon svg path').setAttribute('fill','#75757F')
    		}
		})
    	$('#note input.note-tag-add-textfield').off("keyup").on("keyup", function(event) {
			switch(event.keyCode){
			case 13 :
				addTagEvent(event.target.value);
			case 27 :
				document.querySelector('#note .note-tag-add-textfield').value = "";
				document.querySelector('#note .note-tag-add-textfield').style.display = "none";
				document.querySelector('.note-tag-add-icon svg path').setAttribute('fill','#75757F')
				break;
			}
		});
    }
    function createChip(){
    	let tagChip = Top.Widget.create('top-chip');
    	tagChip.setProperties({
    		"id" : "tag_chip",
            "layout-width": "auto",
            "layout-height": "30px",
            "margin": "7px 0px 7px 0px",
            "border-width": "0px",
            "layout-tab-disabled": false,
            "class": "linear-child-vertical",
            "actual-height": "44",
            "style" : "text-align: center;"
        });
    	return tagChip;
    }
    
    function setModalMode(){
        
        let modalPopup = UTIL.ID('noteIsEditDialog');
        for(let k=1; k < 7; k++){ // add class on modal and bind event for button
            MODAL_ARRAY['dialogOverlay' + k].className = "noteEditor_overlay" + k
            MODAL_ARRAY['dialogOverlay' + k].addEventListener("click", function(){
                modalPopup.open();
                modalPopup.addClass('noteappDialog')            
                modalPopup.setProperties({"on-open":function(){
                    UTIL.ID('note_save_out_btn').setProperties({"on-click" : function(){
                        Top.Controller.get('noteSaveLayoutLogic').saveOkBtn();
                        modalPopup.close(true);
                    }})
                    UTIL.setProperty("note_out_btn",{"on-click":function(){
                    	Top.Loader.start()
                        Note.emptyFileQueue();
                        TNoteServiceCaller.noteNonEditUpdate(TNoteData.getCurPageData().note_id, TNoteData.getCurPageData().parent_notebook, function(){
                        	TNoteController.removeModalMode();
                        	if(document.querySelector('#note .note-tag-add-textfield').style.display = "flex") { 
                    			document.querySelector('#note .note-tag-add-textfield').style.display = "none";
                    			document.querySelector('#note .note-tag-add-textfield').value = "";
                    		}
                        	if(noteLnbMenu.myIsExpanded()===true){
                        		TNoteController.initChapterList()  
                        	} else {
                        		TNoteController.initChapterList()
                        		TNoteController.selectPageMenu(TNoteData.getCurPageData().note_id)
                        	}
                            modalPopup.close(true);
                            Top.Loader.stop();
                            noteFileDelArray = [];
                        });
                    }})
                    UTIL.setProperty("note_cancel",{"on-click":function(){
                        modalPopup.close(true);
                    }})
                }})
            });
        }
        document.querySelector(MODAL_KEY.LEFT).appendChild(MODAL_ARRAY['dialogOverlay1']);
        document.querySelector(MODAL_KEY.TOP).appendChild(MODAL_ARRAY['dialogOverlay2']);
        document.querySelector('div#editor_title_container_2').appendChild(MODAL_ARRAY['dialogOverlay3']);
        // 위치가 바뀌며 같은 셀렉터로 접근하는 경우가 두 가지가 됨. 일단 요렇게 넣어둠 04-21 재웅
        if(noteLnbMenu.myIsExpanded()) document.querySelector('#note .note__header__column').appendChild(MODAL_ARRAY['dialogOverlay4']);
        else document.querySelectorAll('#note .note__header__column')[1].appendChild(MODAL_ARRAY['dialogOverlay4']);
        document.querySelector('div#editor_title_container_1').appendChild(MODAL_ARRAY['dialogOverlay5']);
        document.querySelector('div#note-content-area').appendChild(MODAL_ARRAY['dialogOverlay6']);
        
        TNoteController.setModalSize();
        $(window).resize(function (){
        	TNoteController.setModalSize();
			noteTag.updateTagMoreButton();
			if (document.querySelector('.note-tag-more-cover')) {
				document.querySelector('.note-tag-more-cover').style.display = "none";
			}
        })
    }
    function removeModalMode(){
        $('.noteEditor_overlay1').remove();
        $('.noteEditor_overlay2').remove();
        $('.noteEditor_overlay3').remove();
        $('.noteEditor_overlay4').remove();
        $('.noteEditor_overlay5').remove();
        $('.noteEditor_overlay6').remove();
        
//        $(window).resize(function(){
//            _ctrEditor.checkTagMoreLayout()
//        })

//        if(UTIL.ID("permissionRequestLayoutCover").getProperties("visible")==="visible"){
//            Top.Controller.get('permissionRequestLayoutLogic').closeRequestPopup();
//        }
    }
    
    /************************************************************************/
    
    /** Create / Delete / Rename / Update

    /************************************************************************/
    
    function createNoteBook(noteBookName, parentNoteBookId) { // 아직 연동 안하고 TnoteLnbMenu에 구현해 둠. 해당 로직 나중에 여기로 옮기자!
    	if(parentNoteBookId == "root") parentNoteBookId = "";
        TNoteServiceCaller.noteBookCreate(noteBookName, parentNoteBookId);
        initChapterList();
         
        TNoteRenderer.selectNoteTreeById(parentNoteBookId);

        // before select
    }
    
    function createNote( noteName, parentId ){
        TNoteServiceCaller.noteCreate(noteName, parentId, function(noteData){
        	spaceAPI.spaceInit()
        	noteData.id = noteData.note_id;
        	noteData.text = noteData.note_title;
    		noteLnbMenu.addNode(parentId, noteData, 'chapter');
    		let liNode = document.querySelector('[content-id='+'"' + noteData.id +'"'+ ']');
    		let liList = document.querySelectorAll(".note-lnb-li.selected");
    		for (let i = 0; i < liList.length; i++) {
                liList[i].classList.remove("selected");
            }
    		document.querySelector('ul.tag').classList.remove('selected')
    		liNode.classList.add('selected');
    		TNoteController.selectPageMenu(noteData.id, true); // 새로 만들 때는 true 파라미터를 붙여서 제목에 표시 안해주도록 한다.
        	setTimeout(function(){
        		TNoteController.editorEditMode();
            },1);
        });
    }
    function deleteFileWhenNoteDeleted( deletedNote ) {
    	let fileInfo = deletedNote.fileList[0].storageFileInfoList;
    	if(fileInfo !== null){
    		for(let i=0; i<fileInfo.length; i++){
    			serviceCaller.noteFileDelete(fileInfo[i].file_id);
    		}
    	}
    }
//    function deleteNoteInternal(targetNote) {
//        // delete 즐겨찾기
//        // delete tag
//    	// delete file
//        let tagList = targetNote["tagList"];                      //                  cascade 걸어놨기 때문에 굳이 지금 신경쓰지 않아도 된다. test하기
//        if (tagList && tagList.length > 0) {
//            tagList.forEach(function (tag) {
//                TNoteServiceCaller.tagDelete(tag.note_id, tag.tag_id);
//            })
//        }
//        deleteFileWhenNoteDeleted(targetNote);
//    }
    function deleteNote(checkedNoteList, type) {
    	for (let noteIndex = 0; noteIndex < checkedNoteList.length; noteIndex++) {
            let thisNote = checkedNoteList[noteIndex];
            checkedNoteList[noteIndex].WS_ID = workspaceId;
            checkedNoteList[noteIndex].USER_ID = userManager.getLoginUserId();
            checkedNoteList[noteIndex].user_name = userManager.getLoginUserName();
            checkedNoteList[noteIndex].note_channel_id = noteChannel_Id;
//            deleteNoteInternal(thisNote);
        }
    	if(type === 'chapter'){
//            initTagMenu();
            TNoteServiceCaller.noteDelete(checkedNoteList);
            initChapterList();
//            TNoteRenderer.selectBeforeMenu();
    	} else {
    		TNoteServiceCaller.noteShareDelete(checkedNoteList);
    		initSharedMenu();
    	}

    }
    function _deleteNoteBook(chapterId, noteList) {
//        let noteList = serviceCaller.noteList(chapterId);
        deleteNote(noteList);
        TNoteServiceCaller.noteBookDelete(chapterId);
        initChapterList();
    }
    function deleteNoteBook(chapterId) { // ver2에 맞게 수정함
        let deniedDataList = [];
    	let childList = serviceCaller.noteList(chapterId);
        let result = childList.filter(function (note) {
            return note["is_edit"] !== null;
        });
        count = result ? result.length : 0;
        if (count === 0) {
        	_deleteNoteBook(chapterId,childList);
//        	initChapterList();
//            initTagMenu();
        } else {
        	alert('다른 유저가 하위의 페이지를 수정 중입니다.')
        	return;
        }
	}
	  
    function deleteFile(checkedFileList){
    	for(let index = 0 ; index < checkedFileList.length ; index++){
    		serviceCaller.noteFileDelete(checkedFileList[index].file_id)
    	}
    	let allFileList = serviceCaller.noteFileList();

		TNoteRenderer.renderFile(allFileList)
    }
    function noteTitleUpdate(targetPageId, text, pId, type){
    	if(type === 'chapter'){
    		TNoteServiceCaller.noteTitleUpdate(targetPageId, text, pId, function(){
        		TNoteController.initChapterList()
        	})
    	} else {
    		let noteInfo = {};
    		noteInfo.note_id = targetPageId;
    		noteInfo.note_title = text;
    		TNoteServiceCaller.noteShareUpdate(noteInfo, function(){
        		TNoteController.initSharedMenu()
        	})
    	}
    }
    function shareNoteCreate(shareTargetSpaceId, shareTargetNoteList){
    	let targetCHId = workspaceManager.getChannelList(shareTargetSpaceId, "CHN0003");
    	let targetUSERId = userManager.getLoginUserId();
    	// 페이지만 공유 일 때
    	if(shareTargetNoteList.length>0){
    		for(let index=0; index<shareTargetNoteList.length; index++) {
    			shareTargetNoteList[index].note_id = shareTargetNoteList[index].id;
    			shareTargetNoteList[index].note_channel_id = targetCHId;
    			shareTargetNoteList[index].USER_ID = targetUSERId;
    		}
    		
    		console.log(shareTargetNoteList)
    		TNoteServiceCaller.noteShareCreate(shareTargetNoteList,function(data){
    			console.log(data);
    		})
    	}
//    	// 챕터 공유 일 때
//    	else if(){
//    		
//    	}
    }
  
    /************************************************************************/
    
    /** Editor 읽기모드 / 수정모드                                              **/

    /************************************************************************/
    
    function checkEditorMode(){
    	// 로직 더 추가가 필요
		let selectData = TNoteData.getCurPageData();
		if (Object.keys(TNoteData.getCurPageData()).length && document.querySelector('#readTitle')) {
			TNoteController.selectPageMenu(selectData.note_id);
		}
    }
    
    function editorInit(){
    	editor.destroy()
    	_ctrEvent.customWidgetInit();
    	_ctrEvent._isFile();
    }
    function addTagEllipsis(){
    	let tagEllipsisAddArr = document.querySelectorAll('div.top-chip-content')
        for(var i=0; i< tagEllipsisAddArr.length; i++){ 
        	if(tagEllipsisAddArr[i].offsetWidth > 200) tagEllipsisAddArr[i].classList.add("tagEllipsis")
        }
    }
    function removeTagEllipsis(){
    	let tagEllipsisRemoveArr = document.querySelectorAll('div.tagEllipsis')
        for(var i=0; i< tagEllipsisRemoveArr.length; i++){ 
        	tagEllipsisRemoveArr[i].classList.remove("tagEllipsis")
        }	        	
    }
    function editorReadMode(type){
    	document.querySelector('top-htmleditor#noteEditor').style.height = "100%"
		document.querySelector('textarea#noteEditor').style.display = "none"
		addTagEllipsis();
		setEditorOption("Read")
    	editorReadLayout()
		UTIL.setProperty('saveButton',{
			"text" : "수정",
			"on-click" : function(event){
				if(UTIL.ID('saveButton').getProperties("disabled")===true){
					event.stopPropagation();
					return;
				} else {
					let p_id = TNoteData.getCurPageData().parent_notebook;
    				targetNoteId = TNoteData.getCurPageData().note_id
					TNoteServiceCaller.noteEditUpdate(targetNoteId, p_id);
					// (sj) 순서 나중으로 바꿈 : editmode 수행할 때 태그 더블클릭 이벤트 다는데 수정중인지 업데이트하고 해야할 것 같아
    				editorEditMode()
				}
			}
		})
        let imgList = $('div.se-image-container')
    	let fileList = $('div.sun-editor-content')
		for(let i=0; i<fileList.length; i++) fileList.off('DOMNodeRemoved');    		
		for(let i=0; i<imgList.length; i++)  imgList.off('DOMNodeRemoved');
		// 여기도 일단 잠시
//		fileAttachListener();
    }
    function editorEditMode(callback){
		note_id = TNoteData.getCurPageData().note_id;
    	Note.tagList(note_id,function(){ // 정렬문제로 taglist는 Note.으로 써야함
    		removeTagEllipsis();
    	});
//			TNoteFileController.fileUploadInit();
	    	setModalMode();	    	
    		// 아직 파일첨부 숨기기
//	    	UTIL.ID('noteEditor').addButtons([{
//	    		id:"fileattach",
//	    		title: "파일 첨부",
//			    buttonClass: "",
//			    innerHTML: '<i class="icon-work_attachfile"></i>',
//			    dataCommand: '',
//			    dataDisplay: '',
//			    onClick: function() {
//			    	TNoteFileController.fileContextMenu(event);
//			    }
//	    	}])
	    	setEditorProperty()
    		setEditorOption("Edit");
    		
    		let tempData = TNoteServiceCaller.noteInfoList(note_id)
        	let targetContent = tempData[0].note_content;
    		document.querySelector('top-htmleditor#noteEditor').setEditorHTML(targetContent);
        	UTIL.setProperty('saveButton',{
    			"text" : "저장",
    			"on-click" : function(){ _ctrEditor.onSave()}
    		})
    		TNoteFileController.setFileDelListener();
        	TNoteUtil.setFocusEnd();        	
        	if(callback && typeof callback === "function") callback();
        	//파일 첨부 layout
//        	fileAttachListener();
    }
    
    function setEditorProperty(){
    	/*
    	 *  새로만든 property toolbar 순서 변경
    	 */
    	let divBorder = document.createElement("li");
    	let divBorder2 = document.createElement("li");
    	let divBorder3 = document.createElement("li");
    	let undoli = document.createElement("li");
    	let redoli = document.createElement("li");
    	divBorder.setAttribute("class","note-div-border");
    	divBorder2.setAttribute("class","note-div-border");
    	divBorder3.setAttribute("class","note-div-border");
    	document.querySelector('textarea.top-htmleditor-root').setAttribute("id","noteEditor")
    	$('#noteEditor div.se-wrapper-wysiwyg').attr("id","editorDIV")
    	
    	// undo - redo 위치 change

    	undoli.append(document.querySelector('#noteEditor ._se_command_undo'))
    	redoli.append(document.querySelector('#noteEditor ._se_command_redo'))
    	$('#noteEditor .se-menu-list').prepend(redoli)
    	$('#noteEditor .se-menu-list').prepend(undoli)

    	if(document.querySelectorAll('.note-div-border').length === 0){
        	document.querySelector('#noteEditor ._se_command_font_family').parentNode.before(divBorder)
        	document.querySelector('#noteEditor .se-btn-tool-size').parentNode.after(divBorder2)
        	document.querySelector('#noteEditor ._se_command_superscript').parentNode.after(divBorder3)    		
    	}
    	
    	// 필요없는 btn 제거    	
    	if(document.querySelector("#noteEditor .se-btn[data-command='image']")) document.querySelector("#noteEditor .se-btn[data-command='image']").remove();
    	if(document.querySelector("#noteEditor .se-btn[data-command='video']")) document.querySelector("#noteEditor .se-btn[data-command='video']").remove();
    	// undo - redo change
    	if(document.querySelector("._se_command_undo .se-icon-undo")) document.querySelector("._se_command_undo .se-icon-undo").classList.add("icon-work_undo");
    	if(document.querySelector("._se_command_undo .se-icon-undo")) document.querySelector("._se_command_undo .icon-work_undo").classList.remove("se-icon-undo");
    	if(document.querySelector("._se_command_redo .se-icon-redo")) document.querySelector("._se_command_redo .se-icon-redo").classList.add("icon-work_redo");
    	if(document.querySelector("._se_command_redo .se-icon-redo")) document.querySelector("._se_command_redo .icon-work_redo").classList.remove("se-icon-redo");
    }
    
    function setCannotEditMode(curUser){
    	$('div#editorDIV')[0].contentEditable = "false"
        TNoteController.editorReadLayout();
        UTIL.ID('contenteditorTitle').setDisabled(true)
        noteTag.removeEventOnTagAddIcon();
        UTIL.ID('saveButton').setDisabled(true)
        UTIL.ID('saveButton').setText("수정 중")
        UTIL.ID('noteEditorModifiedUser').setText("");
        UTIL.ID('noteEditorTimeStamp').setText(curUser+"님이 수정 중 입니다.")
    }
    function removeTagCloseButton(){
    	let closeList = document.querySelectorAll('.note-tag-chip-icon');
		if(closeList.length != null){
			for(var i=0; i<closeList.length; i++){
				closeList[i].style.display = "none";
			}			
		}
    }
    function editorReadLayout(){
		let layout = 
			'<div id="readTitle" class="readMode" style="width:100%; height: 2.81rem; border-bottom:1px solid rgb(218, 218, 218); display:flex; flex-direction: row;">'
					+'<img class="note-editor_read-title" src="./res/note/ts_lock@3x.png">'
					+'<span id="readTooltip" note-tooltip-text="" style="max-height:20px; margin:0px 0px 0px 5px; background-color:#ffffff; color:#999999; font-size:0.75rem; display:flex; flex:auto; align-self:center;">  읽기 모드 </span>'
			+'</div>';
		
		if($('#readTitle').length === 0) $('#noteEditor .se-container').prepend(layout)        	
    		
		$('#tag_chip .top-chip-box').css("cursor","not-allowed");
		// editor 툴바 지우고 읽기모드 layout 적용
		$('#noteEditor .sun-editor-common.se-toolbar').css('display',"none");		
		removeTagCloseButton()
		let readmode_vertical = $('.se-container').height() - $('#readTitle').height() + 'px'
		$('#noteEditor div.se-wrapper-wysiwyg').css('max-height', $('.se-container').height());
		document.querySelector('div#editorDIV').style.height = "100%"
		setTimeout(function(){
			$('.sun-editor-font-fix2').attr("style","font-size: 0.63rem !important;");
//			$('.sun-editor-editable .htmleditor-table').css("pointer-events","none");
			$('.sun-editor-editable table').css("pointer-events","none");
			$('.sun-editor-editable img').css("pointer-events","none");
			let readModeImgCount = $('.sun-editor-figure-cover').length;
			if(readModeImgCount != 0){
				for(var i=0; i < readModeImgCount; i++){
					if($('.sun-editor-figure-cover')[i].children[0].getAttribute("file_id") != null){
						$('.sun-editor-figure-cover')[i].setAttribute("onclick","TNoteFileController.getImageFileID(event)")
						$('.sun-editor-figure-cover')[i].style.cursor = "pointer"
					}						
				}
			}
		},1)


//		let selectedLnb = _ctrLnb.getLnbMenuType();
//		setTimeout(function(){ 파일 업로드 관련 추가하면 다시 고려하기
//			$('.sun-editor-editable table').css("pointer-events","none");
//			$('.sun-editor-editable img').css("pointer-events","none");
//			let readModeImgCount = $('.sun-editor-figure-cover').length;
//			if(readModeImgCount != 0){
//				for(var i=0; i < readModeImgCount; i++){
//					if($('.sun-editor-figure-cover')[i].children[0].getAttribute("file_id") != null){
//						$('.sun-editor-figure-cover')[i].setAttribute("onclick","TNoteFileController.getImageFileID(event)")
//						$('.sun-editor-figure-cover')[i].style.cursor = "pointer"
//					}						
//				}
//			}
//		},1)
		let tooltipTitle = $('#readTooltip')		
		tooltipTitle.attr("note-tooltip-text", "읽기 모드에서는 페이지를 수정할 수 없습니다.");
	}

	// 읽기 모드에서 제목 더블클릭시 이름 변경 가능한 기능 : 제거
	// const renameEventInReadMode = () => {
	// 	UTIL.ID('contenteditorTitle').setDisabled(false);
	// 	_input = document.querySelector('input#contenteditorTitle');
	// 	TNoteUtil.setCaretToTheEnd(_input);
		
	// 	let _originalName = _input.value;

	// 	// enter event와 blur event 둘 다 사용
	// 	let _saveOrNot = function() {
	// 		let _input = document.querySelector('input#contenteditorTitle');
	// 		// 이름 변경 안했으면 그대로 return
	// 		if (_originalName === _input.value) {
	// 			UTIL.ID('contenteditorTitle').setDisabled(true);
	// 			return
	// 		}
			
	// 		// 유효한 이름 변경시에만 사용하도록 afterCallback 추가(shared일때 nono)
	// 		let afterCallback = function(){
	// 			UTIL.ID('contenteditorTitle').setDisabled(false);
	// 		}
	// 		noteLnbMenu.changePageName(TNoteData.getCurPageData()["note_id"], _input, afterCallback);	
	// 	}

	// 	_input.addEventListener("keyup", function(e) {
	// 		switch(e.keyCode) {
	// 			case 13: {
	// 				_saveOrNot();
	// 				break;	
	// 			}
	// 			case 27 : {
	// 				_input.value = _originalName;
	// 				UTIL.ID('contenteditorTitle').setDisabled(true);
	// 				break;
	// 			}
	// 			default:
	// 				break;
	// 		}
	// 	});

	// 	_input.addEventListener("blur", function(e) {
	// 		_saveOrNot();
	// 	})

	// }

    function setEditorOption(editorMode){
    	switch(editorMode){
    		case "Read":
    			document.querySelector('div#editorDIV').setAttribute('contentEditable', 'false');
				let readmode_vertical = $('#noteEditor .se-container').height() - $('#readTitle').height() + 'px'
				$('#noteEditor .se-wrapper')[0].style.height = readmode_vertical
				
				UTIL.ID('contenteditorTitle').setDisabled(true);
				// 제목 부분 더블 클릭하면 저장(기능 삭제)
				// document.querySelector('top-textfield#contenteditorTitle').addEventListener("dblclick", renameEventInReadMode);

                document.querySelector('#note .note-tag-add-icon').classList.add('readmode');
                UTIL.ID('saveButton').setVisible("visible");
                UTIL.ID("saveButton").setDisabled(false);
                noteTag.removeEventOnTagChip();
                noteTag.removeEventOnTagAddIcon();
		    	TNoteFileController.noteFileDropDown('off');
				break;
				
    		case "Edit":
    			let editmode_vertical = ($('#noteEditor .se-container').height() - $('#noteEditor .se-toolbar').height() - 20) + 'px'
    	        $('#noteEditor div.se-wrapper-wysiwyg')[0].style.maxHeight = editmode_vertical
    			document.querySelector('div#editorDIV').setAttribute('contentEditable', "true");
				document.querySelector('div#editorDIV').style.height = "75%";
				// 읽기모드 표시줄
				// addButton 들어갔을 땐 읽기모드 title 지우는거 없애야함 
                let readTitle = document.querySelector('div#readTitle');
                if(readTitle) readTitle.remove();
    	    	document.querySelector('.sun-editor-common.se-toolbar').style.display = "block";
    		    UTIL.ID("saveButton").setDisabled(false);
				UTIL.ID('contenteditorTitle').setDisabled(false);
				// 읽기모드에서 달았던 dblclick 이벤트 제거(제거된 이벤트)
				// document.querySelector('top-textfield#contenteditorTitle').removeEventListener("dblclick", renameEventInReadMode);

    		    setEditorTitleTextFieldEvent();
    		    setTagAddTextFieldEvent();
    		    noteTag.setEventOnTagChip();
    		    noteTag.updateTagMoreButton()
		    	TNoteFileController.noteFileDropDown('on');
    		    noteTag.setEventOnTagAddIcon();
                document.querySelector('#note .note-tag-add-icon').classList.remove('readmode');
    			break;
    	}
    }
    
    function fileAttachListener(){
		let hoverImageList = document.querySelectorAll('div.sun-editor-file-body')
		for(let i=0; i<hoverImageList.length; i++) {
			hoverImageList[i].addEventListener("mouseenter",function(){
				TNoteFileController.hoverImageContext(this)
			})
			hoverImageList[i].addEventListener("mouseleave", function() {
				TNoteFileController.leaveImageContext(this)
			})
			hoverImageList[i].addEventListener("click",function(e){
				TNoteFileController.clickImageContext(this,e)
			})
		}
		attchCloseBtn()
    }
    function attchCloseBtn(){
    	let readMode = $('#readTitle');
    	let existCloseBtnList = document.querySelectorAll('div#__note-file_close');
    	let existFileList = document.querySelectorAll('div.sun-editor-file-body');
    	if(readMode.length === 1){
    		if(existCloseBtnList.length !==0){
    			for(let i=0; i< existCloseBtnList.length; i++){
    				existCloseBtnList[i].remove();
    			}
    		}
    	}else{
        	if(existFileList.length !==0){
        		for(let i=0; i< existFileList.length; i++){
        	    	let closeBtnDiv = document.createElement("div");
        	    	let closeBtn = document.createElement("i");
        	    	closeBtnDiv.setAttribute("id","__note-file_close");
        	    	closeBtn.setAttribute("class","icon-work_cancel note-file-body_close");
        	    	closeBtnDiv.appendChild(closeBtn)
        	    	if(existFileList[i].querySelector('#__note-file_close') === null) existFileList[i].append(closeBtnDiv)
        	    	closeBtnDiv.addEventListener("click",function(e){
        	    		let targetName = e.target.parentNode.previousSibling.querySelector('span.note-file-name').innerText;
        	    		let targetId = e.target.parentNode.previousSibling.id;
        	    		let targetDiv = e.target.closest('.sun-editor-content');
        	    		
						TeeAlarm.open({title: '선택한 ' + targetName + ' 을 삭제 하시겠습니까?',
							content: '삭제 후에는 복구 할 수 없습니다.',
							buttons:[{"text":"삭제","onClicked": function(){ 
								TeeAlarm.close();
								targetDiv.remove();
							}}],
							cancelButtonText:"취소",
							onCancelButtonClicked: function(){ TeeAlarm.close();}});
					})
        		}
        	}    		
    	}

    }
    
    /************************************************************************/
    
    /** Context 수정,삭제 다이얼로그                                            **/
    
    /************************************************************************/
        
    return {
        initController: init,
        loadController: loadController,
        initChapterList: initChapterList,
        initTagMenu: initTagMenu,
        initSharedMenu:initSharedMenu,
        setTagTreeNodes:setTagTreeNodes,
        setModalSize:setModalSize,
        setModalMode:setModalMode,
        removeModalMode:removeModalMode,

        // MENU EVENT
        selectPageMenu: selectPageMenu,
        selectTagMenu: selectTagMenu,
        selectSharedMenu:selectSharedMenu,

        // TAG MENU
        selectTreeTag: selectTreeTag,
        selectFileMenu:selectFileMenu,
        // EVENT
        openTagCategory: openTagCategory,
        editorEventBind: editorEventBind,
        tagEventBind:tagEventBind,

        createNoteBook: createNoteBook,
        createNote: createNote,

        deleteNote: deleteNote,
        deleteNoteBook: deleteNoteBook,
        deleteFile:deleteFile,
        shareNoteCreate:shareNoteCreate,
        
        noteTitleUpdate:noteTitleUpdate,
        
        // EDITOR
        editorInit:editorInit,
        editorReadMode:editorReadMode,
        editorReadLayout:editorReadLayout,
        editorEditMode:editorEditMode,
        setCannotEditMode:setCannotEditMode,
        setEditorOption:setEditorOption,
        checkEditorMode:checkEditorMode,
        attchCloseBtn:attchCloseBtn
        
    }

})();
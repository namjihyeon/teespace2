//const LISTVIEW_OPTION = {
//	RENDER_TYPE:"renderType",
//	NEW_BUTTON_VISIBLE: "newButtonVisible",
//	SELECTED_NOTE_ID: "selectedNoteId",	
//};
LISTVIEW_RENDER_TYPE = {
	COMMON:"common",
	FAVORITE:"favorite",
	TAG:"tag",
	TRASHCAN: "trashcan",
};

_noteListView = (function() {
       let List;
       let noteList = [];
       let checkedData = [];
       let selectedData = [];
       let li_cnt;
       
       let lnbListLayout;

       function renderListView( title, targetNoteList, option, callback ) {
           //이게 뭐하는지 찾아 없애라!
           note_del_type = "listview_note";
//           let targetIndex = findTargetIndex( targetNoteList, selectedNoteId );
           // 노트 리스트가 존재할 경우
           if ( targetNoteList && targetNoteList.length > 0 ) {
               renderNoteBookFrame( title, option.renderType, function () {
                   setValue( targetNoteList );
                   // if(isMobile()){
                   //     _ctrLnb.setMobileMode("listview");
                   //     UTIL.ID('saveButton').setDisabled(false);
                   // }
                   updateNewButtonStatus(option.newButtonVisible);
                   if( typeof callback === "function"){
                       callback();
                   }
                   if(isMobile) {
                	   if(TNoteMobile.getMode() === "editor") return;
                	   TNoteMobile.setMode("listview")
                   }
               } );
           } else {
               renderEmptyNoteBookFrame(title, option.renderType, function(type){
                   updateNewButtonStatus(option.newButtonVisible);
                   switch(type){
                       case LISTVIEW_RENDER_TYPE.COMMON :
                           break;
                       case LISTVIEW_RENDER_TYPE.FAVORITE :
                           setEmptyMessage('즐겨 찾기 페이지가 없습니다.','');
                           break;
                       case LISTVIEW_RENDER_TYPE.TAG :
                           setEmptyMessage('이 태그를 사용 중인 페이지가 없습니다.','');
                           break;
                       case LISTVIEW_RENDER_TYPE.TRASHCAN :
                           setEmptyMessage('휴지통이 비어있습니다.','');
                           break;
                       default:
                           break;
                   }
                 if(isMobile) TNoteMobile.setMode("empty")  
               })
           }
       }
       function findTargetIndex ( targetNoteList, selectedNoteId){
           // default Index = 0;
           let targetIndex = 0;
           if( targetNoteList && targetNoteList.length ){
               targetIndex =targetNoteList.findIndex( function( targetNote ){
                   return ( selectedNoteId===targetNote["note_id"] );
               });
           }
           return targetIndex;
       }
       function renderNoteBookFrame( title, type, callback ) {
           if ( !UTIL.ID( 'noteContentLayout' ) ) {
               UTIL.ID( "noteContent" ).src( "./noteContentLayout.html" )
           } else if ( !UTIL.ID( 'content_list' ) ) {
               Note.checkDOM( 'div#content_main', function () {
                   UTIL.ID( 'content_main' ).src( "./content_main.html" )
               } )
           }
           if ( !UTIL.ID( 'noteTitleLayout' ) ) {
               Note.checkDOM( 'div#content_title', function () {
                   UTIL.ID( "content_title" ).src( "./noteTitleLayout.html")
               })
           }
           Note.checkDOM('div#noteTitleLayout', function(){
               setTitle( title );
               setUpperMenu(type);
               if($('#list_search_button').length === 0) $('div#list_title_rest_area').append('<span id="list_search_button" style="margin-left:5px; font-size:15px;" class="icon-search" onclick="TNoteSearch.toggleListViewSearch()"></span>')
           })
           if ( !UTIL.ID( 'noteListView' ) ) {
               Note.checkDOM( 'div#content_list', function () {
                   UTIL.ID( "content_list" ).src( "./noteListView.html" )
               } )
           }
           if ( !UTIL.ID( 'noteEditorLayout' ) ) {
               Note.checkDOM( 'div#content_editor', function () {
                   UTIL.ID( "content_editor" ).src( "./noteEditorLayout.html" )
               } )
           }
           if ( typeof callback === "function" ) {
               Note.checkDOM( "div#noteListView", function () {
                   callback()
               } )
           }
       }
       function renderEmptyNoteBookFrame(title, renderType, callback){
           UTIL.ID('noteContent').src('./noteContentLayout.html', function(){
               UTIL.ID('content_title').src('./noteTitleLayout.html', function(){
                   setTitle( title );
                   if($('#list_search_button').length === 0) $('div#list_title_rest_area').append('<span id="list_search_button" style="margin-left:5px; font-size:15px;" class="icon-search" onclick="TNoteSearch.toggleListViewSearch()"></span>')
               });
               setTimeout(function(){
                   UTIL.ID('content_main').src('emptynoteLayout.html', function(){
                       if(typeof callback === "function"){
                           callback(renderType);
                       }
                   });
               },0)
           });
       }
       function setEmptyMessage(msg1, msg2){
           UTIL.ID('empty_note_TV1').setText(msg1)
           UTIL.ID('empty_note_TV2').setText(msg2)
       }
       function setUpperMenu(type) {
    	   if(type !== LISTVIEW_RENDER_TYPE.TRASHCAN) {
    		   UTIL.ID( 'noteUpperMenu' ).setVisible( "visible" );
    		   UTIL.ID('trashcan_uppermenu').setVisible("none");
    	   } else {
    		   UTIL.ID('noteUpperMenu').setVisible("none")
    		   UTIL.ID('trashcan_uppermenu').setVisible("visible")
    	   }
    	   setTimeout(function(){
    		   let targetHeight = $('div#noteContentLayout').height() - $('div#contents_title').height();
    		   $('div#content_editor').css({"height": targetHeight});
    	   },0);
    	   let totalCheckBox = Top.Dom.select('top-checkbox')[0];
    	   if(totalCheckBox.isChecked()=== true){
    		   totalCheckBox.setProperties({checked: false})
    	   }
    	   UTIL.ID('sortBtn').setDisabled(false);
    	   UTIL.ID('trashcan_sortBtn').setDisabled(false);
       }
       function setTitle( title ) {
           if ( title ) UTIL.ID( 'notebookTitle' ).setText( title );
       }
       function updateNewButtonStatus(flag){
           if( flag ){
               UTIL.ID('newNoteBtn').setVisible("visible");
           } else {
               UTIL.ID('newNoteBtn').setVisible("none");
           }
       }
       
       
       function checkAll(widget){
              let checkboxList = document.querySelectorAll('.note-list-checkbox-i')
              if(widget.isChecked()==true){
                     for(let i=0; i<checkboxList.length;i++){
                            if(checkboxList[i].classList.contains("checked")===false) checkboxList[i].click();
                     }
                     if(widget.id === "trashCanCheckBox"){
                    	 UTIL.ID('trashcan_restore').setDisabled(false);
         	        	 UTIL.ID('trashcan_delete').setDisabled(false);
                     }
                     else if(widget.id === "noteCheckBox"){
                    	 UTIL.ID("noteDeleteBtn").setDisabled(false);
                         UTIL.ID('noteMoveBtn').setDisabled(false);
                     }
              } else {
                     let targetObjects = document.querySelectorAll('.note-list-checkbox-i.checked');
                     for(let i=0; i<targetObjects.length;i++){
                           targetObjects[i].click();
                     }
                     if(widget.id === "trashCanCheckBox"){
                    	 UTIL.ID('trashcan_restore').setDisabled(true);
         	        	 UTIL.ID('trashcan_delete').setDisabled(true);
                     } else if(widget.id === "noteCheckBox"){
                    	 UTIL.ID("noteDeleteBtn").setDisabled(true);
                         UTIL.ID('noteMoveBtn').setDisabled(true);
                     }
              }              
       }
       function flushData(){
    	   data = [];
       }
       function selectDataByIndex(index){
              document.querySelectorAll('.tmp-li')[index].click()
       }
       function selectDataById(noteId){
    	   Note.checkDOM('div#listviewRenderCheck',function(){
    		   document.querySelectorAll("li[note-id="+"'"+noteId+"']")[0].click();
           })
       }
       
       /*
        * timeConvertFlag는 검색에서 setValue하는 상황에 _bindData() 함수에서 temp2 변수에 현재 데이터 입력하기와 시간 설정 로직을 수행하지 않도록 하기위해 설정한 flag이다. true일 경우 수행되고, false일 경우 수행하지 않는다.
        */
       function setValue( targetNoteList , targetIndex) {
    	   if(document.getElementById('tmp-ul')) document.getElementById('tmp-ul').remove()
    	   li_cnt = 0;
    	   checkedData = [];
    	   Top.Dom.select('top-checkbox')[0].setChecked('false')
    	   Top.Dom.select('top-checkbox')[1].setChecked('false')
    	   Top.Dom.select('top-checkbox')[0].setDisabled('false')
    	   Top.Dom.select('top-checkbox')[1].setDisabled('false')
    	   lnbListLayout = $('div#noteListView');
    	   noteList = targetNoteList;
    	   if(_ctrEvent.getSelectedLnbMenu().type==="trashcan") List = $('div#trashcan_list')
    	   _makeContainer(function(){
               _bindData();
               setAdditionalOptions(noteList, targetIndex);
           });
           function setAdditionalOptions(noteList, targetIndex){
	           _ctrLnb.fav_check_render(noteList);
	           _ctrLnbList.setListViewDraggable();
           Note.checkDOM('div#sun_editorEdit', function(){
        	   if(!isMobile()){
        		   if(targetIndex) selectDataByIndex(targetIndex)
            	   else selectDataByIndex(0)
        	   } else { // 모바일일 경우
        		   let listviewTitle = document.querySelectorAll('.note-list-title')
        		   let listviewPreview = document.querySelectorAll('.note-list-preview')
        		   let listviewDate = document.querySelectorAll('.note-list-time')
        		   for(var i=0; i < listviewDate.length; i++){
        			   listviewTitle[i].style.width = "79%"
        			   listviewPreview[i].style.width = "84%"
        			   listviewDate[i].style.width = "84%"
        		   }
        	   }
           })
       }
//           Note.checkDOM('div#listviewRenderCheck',function(){
//        	   callback();
//           })
       }
       function getCheckedData(){return checkedData}
       function getSelectedData(){return selectedData}
       function getSelectedIndex(){
              let tmpList = document.querySelectorAll('.tmp-li')
              for(let i=0; i<tmpList.length; i++){
                     if(tmpList[i].classList.contains('selected')) return i;
              }
              return -1;
       }
       function _selectData(object, noteData){
              if(object.classList.contains("selected")){
            	  if(isMobile()){
            		  if(_ctrEvent.getSelectedLnbMenu().type === "trashcan"){
            			  TNoteMobile.setMode("editor")
            		  } else {
            			  TNoteMobile.setMode("editor")
            		  }
            	  }
//                     return;
              } else {
                     let targetObjects = document.querySelectorAll('.tmp-li.selected');
                     for(let i=0; i<targetObjects.length; i++){
                            document.querySelectorAll('.tmp-li.selected')[i].classList.remove('selected')
                     }
                     object.classList.add('selected')
                     selectedData = noteData;
                     if(_ctrEvent.getSelectedLnbMenu().type === "trashcan"){
                         TNoteController.noteSelect("trash", _noteListView)
                     } else {
                         TNoteController.noteSelect("common", _noteListView)
                     }
              }
       }
       function _bindData(){
           let frag = document.createDocumentFragment();
           noteList = _makePreview(noteList);
           for(let i=0; i<noteList.length;i++){
               _renderNode(noteList[i], frag);
           }
           document.getElementById('tmp-ul').appendChild(frag);
       }
       function _makeContainer(callback){
              let ul_tmp = document.createElement('ul')
              ul_tmp.setAttribute('id','tmp-ul')
		        lnbListLayout.append(ul_tmp);
		        Note.checkDOM('ul#tmp-ul', function(){
		            callback();
		        })
       }
       function _makePreview(noteData){
    	   for(let i=0; i< noteData.length; i++){
           		noteData[i].note_preview = noteData[i].note_content.replace(/(<([^>]+)>)/ig,"");
           		noteData[i].note_preview = noteData[i].note_preview.replace(/&lt;/gi,"<").replace(/&gt;/gi,">").replace(/&nbsp;/gi," ").replace(/&amp;/gi,"&")
           }
    	   return noteData;
       }
       function getNoteContentById(id){
    	   let result = "Content get error. not valid ID";
    	   for(let i = 0; i< data.length; i++){
    		   if(data[i].note_id === id){
    			   result = data[i].note_content;
    		   }
    	   }
    	   return result;
       }
       function _renderNode(noteData, frag){
              //default layer
              let li_tmp = document.createElement('li')
              li_tmp.addEventListener('click', function(){
                     _selectData(li_tmp, noteData)
              })
              li_tmp.setAttribute('class','tmp-li')
              li_tmp.setAttribute('data-index',li_cnt++)
              li_tmp.setAttribute('note-id',noteData.note_id);
              frag.appendChild(li_tmp)
              let span_tmp = document.createElement('span')
              span_tmp.setAttribute('class','note-list-span-1')
              let span_tmp2 = document.createElement('span')
              span_tmp2.setAttribute('class','note-list-span-2')
              let span_tmp3 = document.createElement('span')
              span_tmp3.setAttribute('class','note-list-span-3')            
              li_tmp.appendChild(span_tmp)
              li_tmp.appendChild(span_tmp2)
              li_tmp.appendChild(span_tmp3)
              
              //checkbox
              let check_tmp = document.createElement('input')
              let check_container = document.createElement('span')
              let check_label = document.createElement('label')
              let check_i = document.createElement('i')
              
              check_tmp.setAttribute('class','input-checkbox')
              check_tmp.setAttribute('type','checkbox')              
              check_container.setAttribute('class','note-list-checkbox')
              check_label.setAttribute('for','input-checkbox')                            
              check_i.setAttribute('class','note-list-checkbox-i')
              
              check_label.appendChild(check_i)
              check_container.appendChild(check_tmp)
              check_container.appendChild(check_label)
              span_tmp.appendChild(check_container)
              
              check_i.addEventListener('click', function(){
                     if(check_i.classList.contains("checked") === false){
                           checkedData.push(noteData)
                           check_i.classList.add('checked')
                     } else {
                           checkedData.splice(checkedData.indexOf(noteData),1)
                           check_i.classList.remove('checked')
                     }
                     _ctrLnbList.listviewCheckBoxClick(event)
              })
              
              //book-mark icon
              if(_ctrEvent.getSelectedLnbMenu().type !== "trashcan"){
            	  let star_tmp = document.createElement('span')
                  star_tmp.setAttribute('class','icon-star')
                  star_tmp.setAttribute('style','width:15px;')
                  star_tmp.setAttribute('id','listview-bookmark-icon')
                  span_tmp.appendChild(star_tmp)
                  star_tmp.addEventListener('click', function(){
                	  _ctrLnbList.fav_check(event, star_tmp);
                  })
              }
              
              //title
              let title_tmp = document.createElement('p')
              title_tmp.setAttribute('class','note-list-title')
              title_tmp.innerText = noteData.note_title
              span_tmp.appendChild(title_tmp)
              
              //preview
              let p_tmp2 = document.createElement('p')
              p_tmp2.setAttribute('class','note-list-preview')
              p_tmp2.innerText = noteData.note_preview
              span_tmp2.appendChild(p_tmp2)
              
              //modified time
              let targetDate;
              if( "trashcan" === _ctrEvent.getSelectedLnbMenu().type){
                  targetDate = TNoteUtil.getNoteDateFormat(noteData.deleted_date)
              }else{
                  targetDate = TNoteUtil.getNoteDateFormat(noteData.modified_date)
              }
              let time_tmp3 = document.createElement('p');
              time_tmp3.setAttribute('class','note-list-time');
              time_tmp3.innerText =targetDate;
              span_tmp3.appendChild(time_tmp3);
      																		
              //tag icon
              let tag_icon_tmp = document.createElement('img');
              tag_icon_tmp.setAttribute('src','res/work_tag.svg');
              tag_icon_tmp.setAttribute('class','note-list-tag-icon');
              span_tmp3.appendChild(tag_icon_tmp);

              //tag count
              let tag_count_tmp = document.createElement('span');
              tag_count_tmp.setAttribute('class','note-list-tag-count');
              tag_count_tmp.innerText = noteData.tagList.length
              span_tmp3.appendChild(tag_count_tmp)
              if(noteData.fileList != undefined && noteData.fileList.length !== 0){
                  if(noteData.fileList[0].storageFileInfoList !== null){
                	  //file icon
                	  let file_icon_tmp = document.createElement('img')
                      file_icon_tmp.setAttribute('src','res/work_attachfile.svg')
                      file_icon_tmp.setAttribute('class','note-list-tag-icon')
                      span_tmp3.appendChild(file_icon_tmp)
                      
                      //file count
                      let file_count_tmp = document.createElement('span')
                      file_count_tmp.setAttribute('class','note-list-tag-count')
                      file_count_tmp.innerText = noteData.fileList[0].storageFileInfoList.length
                      span_tmp3.appendChild(file_count_tmp)
                  }            	  
              }
              $('.note-list-time').css("width","80%")
       }
       return {
           renderListView:renderListView,
           setValue:setValue,
           getSelectedData:getSelectedData,
           selectDataByIndex:selectDataByIndex,
           getCheckedData:getCheckedData,
           getData:function(){return noteList},
           getSelectedIndex:getSelectedIndex,
           checkAll:checkAll,
           setUpperMenu : setUpperMenu,
           renderOptionBuilder: function(){
               let optionList = {
                   newButtonVisible : false,
                   renderType : "common",
                   selectedNoteId : null,
               }
               return{
                   setSelectedNoteId : function(noteId){
                       optionList.selectedNoteId = noteId;
                       return this;
                   },
                   setNewButtonVisible : function(){
                       optionList.newButtonVisible = true;
                       return this;
                   },
                   setRenderType : function(type){
                       optionList.renderType = type;
                       return this;
                   },
                   build : function(){
                       return optionList;
                   }
               }
           },
       }
})()
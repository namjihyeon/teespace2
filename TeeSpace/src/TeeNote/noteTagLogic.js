function tagProxy(input){
	function getSelectedIdx () {
		
	}
	let handler = {
			get : function(target, property){
				return target[property];
			},
			set : function(target, property, newValue){
				// Tag의 text에만 반응 하도록 한다.
				if( property ==="text") {
                    let oldValue = target[property];
                    if (oldValue !== newValue) {
                    	let tempTag = tagArray.find(function(tag){ return tag.text === oldValue })
                    	if(tempTag){
                    		tagArray.splice(tagArray.indexOf(tempTag),1);
                    		let createDTO = {
                                    "note_id": target.note_id,
                                    "text": newValue
                                };
                                tagArray.push(createDTO);
                    	} else {
                    		let copiedTarget = JSON.parse(JSON.stringify(target));
                    		tagDelArray.push(copiedTarget);
                            let createDTO = {
                                "note_id": target.note_id,
                                "text": newValue
                            };
                            tagArray.push(createDTO);
                    	}
                    	target[property]=newValue;
                    }
                }else{
                	target[property]=newValue;
                }
			}
	};
	return new Proxy(input, handler);
}


Top.Controller.create('tagLayoutLogic', {    
    onAccordTabOpen : function(widget) {
    	let tagCnt=0;
    	let LanguageType = widget.split("_")[1].toUpperCase();
		TNoteController.openTagCategory(TAG_CATEGORY[LanguageType], LanguageType);
		let targetLayout = document.querySelector('#' + widget + ' .top-accordionlayout-title .icon-arrow_down');
		if (targetLayout) {
			targetLayout.classList.add('icon-arrow_up');
			targetLayout.classList.remove('icon-arrow_down');	   
		}
	},
    onAccordTabClose : function(widget) {
		let targetLayout = document.querySelector('#' + widget + ' .top-accordionlayout-title .icon-arrow_up');
		if (targetLayout) {
			targetLayout.classList.add('icon-arrow_down');
			targetLayout.classList.remove('icon-arrow_up');	   
		}
    }, 
    tagAddDialogOpen : function(event, widget) {
    	UTIL.openDialog('tagCreateDialog')
        UTIL.ID('tagCreateDialog').addClass('noteappDialog')

	}, 
	renderTagNote : function(targetTagId, tagData){
		if(document.querySelector('.tag-chip-for-search')) document.querySelector('.tag-chip-for-search').remove()
		let noteList = TNoteServiceCaller.tagNoteList(targetTagId);
		TNoteData.setParentNameData();
//		TNoteData.getParentNameByParentId()
		if(noteList.length!==0){
			//선행작업
			if(!noteLnbMenu.isExpanded()){
				noteLnbMenu.setLnbVisibility('flex')
    			noteLnbMenu.setTagVisibility('none')
			}
			
			$('#note-lnb-new-chapter').attr("disabled",true) // 일단 새챕터  버튼은 disabled처리 해준다.
			document.getElementById('note-lnb-search-textfield').style.display = 'none';
			document.querySelector('.note-lnb-search .icon-work_cancel').style.display = 'none';
			
			// 검색 창 쪽에 chip으로 표현해주기 			
			let tagChip = document.createElement('span');
			tagChip.classList.add('tag-chip-for-search');
			let tagText = document.createElement('span');
			tagText.innerText = tagData.text;
			tagText.classList.add('tag-chip-for-search-text');
			let closeIcon = document.createElement('span');
			closeIcon.classList.add('icon-close');
			closeIcon.addEventListener('click',function(){
				$("#note-lnb-new-chapter").removeAttr("disabled");
				TNoteSearch.setLnbMenusVisibility('block')
				document.getElementById('note-lnb-search-textfield').style.display = 'flex';
				document.querySelector('.note-lnb-search .icon-work_cancel').style.display = 'flex';
				$(tagChip).remove();
			})
			tagChip.appendChild(tagText);
			tagChip.appendChild(closeIcon);
			document.querySelector('.note-lnb-search').appendChild(tagChip);
			
			// lnb menu에 검색 결과를 표시하는 로직
			TNoteSearch.setSearchResultPage('');
			document.querySelector('.empty_div').remove();

			// 기존에 renderPageSearchResult에 인자로 주는 양식대로 만들어줌
			let searchPageResult = [];
			let _targetNode = document.getElementById('searchResultLayout');

			for(let i=0; i<noteList.length; i++){
				let pageItem = [];
				pageItem.push({
					id : noteList[i].note_id,
					text : noteList[i].note_title,
					parent_name : TNoteData.getParentNameByParentId(noteList[i].parent_notebook)
				})
				
				searchPageResult.push(pageItem);
			}

			TNoteRenderer.renderPageSearchResult(searchPageResult, _targetNode)			
		}
		
	},
	setTagChipOnClickEvent : function(event, widget){
		if(event.target.className==="top-chip-root"){
			
		} else {
			let tmpTarget = event.path.find(function(n){return n.classList.contains('top-chip-box')})
			let _clickedName = tmpTarget.getElementsByTagName('span')[0].innerText
			let _clickedNode
			let _clickedNodeChunk = widget.getProperties("chip-items")
			for(i=0;i<_clickedNodeChunk.length;i++){
				if(_clickedName == _clickedNodeChunk[i].text){
					_clickedNode = _clickedNodeChunk[i]
					curTagId = _clickedNode.tag_id
				}
			}
			
//			UTIL.ID('tagTree').selectNode(curTagId)
			_ctrTag.renderTagNote(curTagId, _clickedNode)
			
			
			
		}
	}, 
	// (sj) check : 안 쓰는 건가?
    preBtn : function(event, widget) {
    	if($('div#content_editor').hasClass('editor-wide-mode')){
    		$('div#content_editor').removeClass('editor-wide-mode')
    		$('div#content_editor').addClass('editor-hide-mode')
    		$('div#content_list').removeClass('listview-hide-mode');
    		$('div#content_list').addClass('listview-wide-mode');
    	}else if($('div#content_editor').hasClass('editor-hide-mode')){
    		$('div#noteLNB').removeClass('lnb-hide-mode');
    		$('div#noteLNB').addClass('')
    		$('div#content_editor').removeClass('editor-wide-mode')
    		$('div#content_editor').addClass('editor-hide-mode')
    		$('div#content_list').removeClass('listview-hide-mode');
    		$('div#content_list').removeClass('listview-wide-mode');
    		UTIL.setProperty('notePreBtn',{"visible":"none"})
//    		UTIL.setProperty('notebookTitle', {"margin": "20px 0px 0px 20px"})
    	}else {
    		$('div#noteLNB').removeClass('lnb-hide-mode');
    		$('div#noteLNB').addClass('')
    		$('div#content_editor').removeClass('editor-wide-mode')
    		$('div#content_editor').addClass('editor-hide-mode')
    		$('div#content_list').removeClass('listview-hide-mode');
    		$('div#content_list').removeClass('listview-wide-mode');
    		UTIL.setProperty('tagPreBtn',{"visible":"none"})
//    		UTIL.setProperty('TextView69_1', {"margin": "20px 0px 0px 20px"})
    	}
	}
		 
})

const noteTag = (function(){
	function initEditorTag(){
		$('div#noteTagContainer').empty();
		let outterContainer = document.querySelector('div#noteTagContainer');
		
		//SVG의 색깔 수정이 어려워서 일단 인라인으로 넣음. 임시구현. 아이더러워
		let tagSVG = `
			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="1.25rem" height="1.25rem" viewBox="0 0 20 20" version="1.1">
			<title>Icon/system/tag_add</title>
			<desc>Created with Sketch.</desc>
			<g id="Icon/system/tag_add" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
			<path d="M16,12 C16.5522848,12 17,12.4477153 17,13 L16.9996194,15 L19,15 C19.5522848,15 20,15.4477153 20,16 C20,16.5522848 19.5522848,17 19,17 L16.9996194,17 L17,19 C17,19.5522848 16.5522848,20 16,20 C15.4477153,20 15,19.5522848 15,19 L14.9996194,17 L13,17 C12.4477153,17 12,16.5522848 12,16 C12,15.4477153 12.4477153,15 13,15 L14.9996194,15 L15,13 C15,12.4477153 15.4477153,12 16,12 Z M8.34023349,1.00074646 L14.3094032,1.23974807 C15.0221643,1.26919481 15.6289873,1.87289763 15.6577193,2.58806415 L15.8973704,8.5532031 C15.9079389,8.89539977 15.7927278,9.20762807 15.5687793,9.43947123 L13.6616194,11.346 L9.53741848,15.4710789 C9.08530775,15.9231896 8.3387291,15.9245472 7.81438626,15.4959105 L7.70561696,15.3974858 L3.80461938,11.496 L3.02365047,12.2861419 L9.12199782,18.3939493 L10.9866194,16.474 L12.0926194,17.534 L10.0106081,19.6093089 C9.5775224,20.0686785 8.874178,20.0836069 8.38701228,19.6654142 L8.28610575,19.5691763 L1.91528974,13.1731645 C1.4614885,12.6931552 1.41548103,11.9467627 1.78862933,11.4492151 L1.87500948,11.3465943 L2.72861938,10.421 L1.49998163,9.19185043 C0.975025961,8.66689476 0.941984154,7.84445326 1.42638851,7.36004891 L7.44361108,1.34282634 C7.67684281,1.10959461 7.99493852,0.989308278 8.34023349,1.00074646 Z M8.4872346,2.49877551 L2.66190168,8.32410843 L8.57335896,14.2355657 L14.3986919,8.41023278 L14.1711831,2.72712965 L8.4872346,2.49877551 Z M11.4271541,4.00000003 C11.9794389,4.00000003 12.4271541,4.44771528 12.4271541,5.00000003 C12.4271541,5.55228478 11.9794389,6.00000003 11.4271541,6.00000003 C10.8748694,6.00000003 10.4271541,5.55228478 10.4271541,5.00000003 C10.4271541,4.44771528 10.8748694,4.00000003 11.4271541,4.00000003 Z" id="Combined-Shape" fill="#75757F"/>
			</g>
			</svg>
		`
		
		let tagAddIcon = document.createElement('div');
		tagAddIcon.innerHTML = tagSVG;
		tagAddIcon.classList.add('note-tag-add-icon')
		tagAddIcon.setAttribute('id','noteTagAdd');
		
		let lengthChecker = document.createElement('div');
		let innerContainer = document.createElement('div');
		innerContainer.classList.add('tags-inner-container');
		lengthChecker.classList.add('tags-length-checker')
		lengthChecker.appendChild(innerContainer);
		
		let tagAddTextField = document.createElement('input');
		tagAddTextField.setAttribute('id','tagTextField');
		tagAddTextField.classList.add('note-tag-add-textfield');
		// 입력 글자 수 50자 제한
		tagAddTextField.setAttribute('maxlength', '50');
		innerContainer.appendChild(tagAddTextField)
		
		let tagMoreButton = document.createElement('i');
		tagMoreButton.classList.add('tag-more-button');
		tagMoreButton.classList.add('icon-arrow_filled_up');
		$(tagMoreButton).off('click').on('click',function(e){
			onClickTagMoreButton(e);
		})
		
		outterContainer.appendChild(tagAddIcon);
		outterContainer.appendChild(lengthChecker);
		outterContainer.appendChild(tagMoreButton);
	}
	
	function renderTagChips(tagList, targetDiv){
		let frag = document.createDocumentFragment();
		for(let i=tagList.length-1; i>=0; i--){
			renderTagChipNode(tagList[i],frag)
		}
		targetDiv.appendChild(frag);
		
	}
	function renderTagChipNode(tagData, frag){
		let container = document.createElement('div');
		let contents = document.createElement('div');
		let tagName = document.createElement('span');
		container.setAttribute('tag_id',tagData.tag_id);
		container.setAttribute('note_id',tagData.note_id);
		container.setAttribute('text',tagData.text);
		
		// (SJ) note-real-tag-name 지움		
		
		let closeIcon = document.createElement('i');
		container.appendChild(contents);
		contents.appendChild(tagName);
		contents.appendChild(closeIcon);
		
		container.classList.add('note-tag-chip-container');
		$(container).off('click').on('click', function(e){
			onClickFocusTag(e);
		})
		contents.classList.add('note-tag-chip-content');
		tagName.classList.add('note-tag-chip-text');
		tagName.classList.add('note-real-tag-name');
		tagName.classList.add('talk-tooltip');
		closeIcon.classList.add('note-tag-chip-icon');
		closeIcon.classList.add('icon-work_cancel');
		$(closeIcon).off('click').on('click', function(e){
			onClickCloseIcon(e);
		})
		
		// (SJ) text 넣는 태그 수정
		tagName.textContent = tagData.text;
		frag.appendChild(container);
		return frag;
	}
	function onClickCloseIcon(e){
		e.stopPropagation();

		let tagDTO = {};
		let _targetChip = e.target.closest('.note-tag-chip-container');
		tagDTO.tag_id = _targetChip.getAttribute('tag_id');
		tagDTO.note_id = _targetChip.getAttribute('note_id');
		tagDTO.text = _targetChip.getAttribute('text');
		if(tagDTO.tag_id === 'undefined'){ // 새로 만든 걸 지울 경우. tagArray에서 삭제해준다.
			let targetTag = tagArray.find(function(tag){ return tag.text===tagDTO.text})
			let targetIdx = tagArray.indexOf(targetTag);
			if(targetIdx > -1) tagArray.splice(targetIdx,1);
		} else { // 기존에 있던 걸 지우는 경우. tagDelArray에 추가해준다.
			tagDelArray.push(tagDTO);
		}
		
		// (SJ) 더보기 태그일때 일단 note-tag-more-inner-div 안에 있는 거 수정
		// 둘 다 note-tag-chip-container 클래스를 가지고 있어서 더보기 태그인지 먼저 체크
		if (e.target.closest('.note-tag-more-inner-div')) {
			e.target.closest('.note-tag-more-inner-div').remove();
			// (SJ) 더보기 태그 다 지우면 더보기 컨테이너 display : none시키기
			let moreTagContainer = document.querySelector('.note-tag-more-cover');
			if (!moreTagContainer.childElementCount) {
				moreTagContainer.style.display = "none";
			}
		} 
		// 더보기 태그든 아니든 원래 태그 지워야 함
		let _deleteTag = document.querySelector('.note-tag-chip-container[text="'+tagDTO.text+'"]');
		_deleteTag.remove();

		updateTagMoreButton();
	}
	
	function onClickTagMoreButton(e){
		let tagMoreContainer = document.getElementById('__noteTagMoreCover');
		if(tagMoreContainer.style.display === 'none'){
			e.preventDefault();
			e.stopPropagation();
			// (sj) tagMoreDiv가 있는 경우 화살표 아이콘 아래방향, 기획 default : 위로 있었음
			e.target.classList.remove("icon-arrow_filled_up");
			e.target.classList.add("icon-arrow_filled_down");
			TNoteUtil.removeChildrenNodes(tagMoreContainer);
			tagMoreContainer.style.display = 'flex';
			let chipList = document.querySelectorAll('#note .note-tag-chip-container');
			for(let i=0; i<chipList.length; i++){
				if (TNoteUtil.isInViewport(chipList[i], '.tags-inner-container')) {
					continue
				}
			    let _noteTemp = chipList[i].cloneNode(true);
			    let _div = document.createElement('div');
			    _div.classList.add('note-tag-more-inner-div');
				_div.appendChild(_noteTemp);				
				tagMoreContainer.appendChild(_div);
				_div.querySelector('.icon-work_cancel').addEventListener('click', onClickCloseIcon);
			}
			// (sj) render 다한 후 그리도록 이벤트 생성 뒤로 미룸
			// 수정 중인지 확인해야한다
			noteTag.setEventOnTagChip();
		}else {
			// (sj) tagMoreDiv가 있는 경우 화살표 아이콘 새로 넣기
			// 이벤트 전파되어서 noteUtil의 tagMoreCover 쪽 이벤트 탄다(tagMoreContainer 안보임)
			e.target.classList.remove("icon-arrow_filled_down");
			e.target.classList.add("icon-arrow_filled_up");
		}
	}
	function onClickTagAddButton(e){
		e.stopPropagation();

		let tagAddTextField = document.querySelector('#note .note-tag-add-textfield');
		if(document.querySelector('#note .note-tag-add-textfield').style.display === 'flex'){
			document.querySelector('.note-tag-add-icon svg path').setAttribute('fill','#75757F')
			tagAddTextField.style.display = "none";
		} else {
			tagAddTextField.style.display = "flex";
			document.querySelector('.note-tag-add-icon svg path').setAttribute('fill','#008CC8')
			tagAddTextField.focus();
		}
	}
	function onClickFocusTag(e){
		let _tagChip = e.target.closest('.note-tag-chip-container');
		let isExistFocus = document.querySelectorAll('#note .note-tag-chip-container-focus');
		if(isExistFocus.length !== 0) {
			for(let i=0; i< isExistFocus.length; i++) isExistFocus[i].classList.remove("note-tag-chip-container-focus");
		}
		if(!_tagChip.classList.contains("note-tag-chip-container-focus")) {
			_tagChip.classList.add("note-tag-chip-container-focus")
			_tagChip.scrollIntoView(false);
		}
		else _tagChip.classList.remove("note-tag-chip-container-focus")
	}

	// 태그 이름 검사 추가
	function modifyTagName(targetChip, nameChangeInput) {
		// 사용할 수 있는 태그 이름인가
		if(!document.contains(nameChangeInput)) {
			// 혹시 nameChangeInput이 없어져있을 경우
			return
		} else if (nameChangeInput.value === targetChip.querySelector('.note-real-tag-name').textContent) {
			// 수정하지 않았으면 그대로 return
			nameChangeInput.replaceWith(targetChip);
			return
		} else if (nameChangeInput.value.trim() === "") {
			nameChangeInput.replaceWith(targetChip);
			TeeAlarm.open({title: '태그명을 입력해주세요', content: ""});								
			return
		}
		else if (_ctrEditor.checkExistingTag(nameChangeInput.value)) {
			nameChangeInput.replaceWith(targetChip);
			TeeAlarm.open({title: '이미 있는 태그 이름입니다', content: ""});								
			return
		} 

		// 오류 검사 후 이름 바꾸기
		let targetTagId = targetChip.getAttribute('tag_id');
		let targetTagProxy = tag_item.find(function(tag){ return tag.tag_id === targetTagId });

		if(targetTagProxy){
			targetTagProxy.text = nameChangeInput.value;
		} else {
			targetTag = tagArray.find(function(tag){ return tag.text === targetChip.textContent});
			let createDTO = {
				"note_id": TNoteData.getCurPageData().note_id,
				"text": nameChangeInput.value
			};
			// 순서 안바뀌도록 replace
			let _idx = tagArray.indexOf(targetTag);
			if ( _idx !== -1) {
				tagArray[_idx] = createDTO;
			}
		}

		// 더보기 컨테이너 안에 있는 태그는 원본 태그도 고쳐야 한다
		let _originalTag = document.querySelector('.note-tag-chip-container[tag_id="'+targetTagId+'"]');
		if (_originalTag) {
			_originalTag.querySelector('.note-real-tag-name').textContent = nameChangeInput.value;
		}

		targetChip.querySelector('.note-real-tag-name').textContent = nameChangeInput.value
		nameChangeInput.replaceWith(targetChip)
		
		// 자리가 생겨서 원본 태그가 보이면 더보기 태그에 있던 것은 지워야 한다
		if (_originalTag && TNoteUtil.isInViewport(_originalTag)) {
			targetChip.closest('.note-tag-more-inner-div').remove(); // 원본 태그가 보이면 더보기 컨테이너에서 지운다
		}
	}

	function onDoubleClickTagChip(e){
		let targetChip = e.target.closest('.note-tag-chip-container');
		let nameChangeInput = document.createElement('input');
		nameChangeInput.classList.add('tag-name-change-input');
		nameChangeInput.setAttribute('maxlength','50');
		
		nameChangeInput.addEventListener("blur", function(e) {
			e.stopPropagation();
			modifyTagName(targetChip, nameChangeInput);
		});

		nameChangeInput.addEventListener('keyup', function(e){
			e.stopPropagation();
			if(e.keyCode===13){	
				modifyTagName(targetChip, nameChangeInput);
			} else if(e.keyCode === 27){
				nameChangeInput.replaceWith(targetChip)
			}
		})
		nameChangeInput.value = targetChip.querySelector('.note-real-tag-name').textContent;
		// (SJ) 기존 태그 길이보다 길어지면 보이는 곳 바깥으로 나가 안보인다. 크기 제한
		nameChangeInput.style.width = targetChip.offsetWidth+"px";
		targetChip.replaceWith(nameChangeInput); 
		nameChangeInput.select();
	}

	function setEventOnTagAddIcon(){
		document.querySelector('#note #noteTagAdd').addEventListener('click', function(e){
			onClickTagAddButton(e)
		})
	}
	function removeEventOnTagAddIcon(){
		document.querySelector('#note #noteTagAdd').removeEventListener('click', function(e){
			onClickTagAddButton(e)
		})
	}
	function setEventOnTagChip(){
		let _editorDiv = document.querySelector('div#note-editor-container');
		let noteData = TNoteServiceCaller.noteInfoList(_editorDiv.getAttribute('rendered-page-id'))[0];
		// (SJ) 자기가 수정중일 때만 이벤트 달기
		if (noteData["is_edit"] !== userManager.getLoginUserId()) {
			return
		}
		// 더보기 버튼시 나오는 tag들은 #note밖에 있어서 이벤트가 달리지 않았음
		let chips = document.querySelectorAll('#note .note-tag-chip-container, .note-tag-more-inner-div .note-tag-chip-container');
		for(let i=0; i<chips.length; i++){
			chips[i].addEventListener('dblclick',function(e){
				onDoubleClickTagChip(e);
			});
		}
	}
	function removeEventOnTagChip(){
		let chips = document.querySelectorAll('#note .note-tag-chip-container');
		for(let i=0; i<chips.length; i++){
			chips[i].removeEventListener('dblclick',function(e){
				onDoubleClickTagChip(e);
			})
		}
	}
	function updateTagMoreButton(){
		if(document.querySelector('.tags-length-checker').offsetWidth <= document.querySelector('.tags-inner-container').offsetWidth) {
			document.querySelector('#note .tag-more-button').style.display = 'flex';
		} else {
			document.querySelector('#note .tag-more-button').style.display = 'none';
		}
	}
	return{
		renderTagChips:renderTagChips,
		renderTagChipNode:renderTagChipNode,
		setEventOnTagChip:setEventOnTagChip,
		setEventOnTagAddIcon:setEventOnTagAddIcon,
		removeEventOnTagAddIcon:removeEventOnTagAddIcon,
		removeEventOnTagChip:removeEventOnTagChip,
		initEditorTag:initEditorTag,
		updateTagMoreButton:updateTagMoreButton
		
	}
})();

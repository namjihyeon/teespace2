/**
*  Sun Editor 파일 로직
*  Editor toolbar 생성, 이미지 / 파일 분기 후 element 생성, File DomNode Event binding, Selection Node
*  @author soohyun kim 
*/
var noteFileArray = [];
var noteFileDelArray = [];

TNoteFileController = (function(){
	//12-31 file upload by drag and drop	
	function noteFileDropDown(flag){
		if(flag === "on"){
	        //Drag기능 
			let noteFileDropZone = $("div#editorDIV[contenteditable=true]");
	        noteFileDropZone.on('dragenter',function(e){
	            e.stopPropagation();
	            e.preventDefault();
	            // 드롭다운 영역 css
	            noteFileDropZone.css('background-color','#E3F2FC');
	        });
	        noteFileDropZone.on('dragleave',function(e){
	            e.stopPropagation();
	            e.preventDefault();
	            // 드롭다운 영역 css
	            noteFileDropZone.css('background-color','#FFFFFF');
	        });
	        noteFileDropZone.on('dragover',function(e){
	            e.stopPropagation();
	            e.preventDefault();
	            // 드롭다운 영역 css
	            noteFileDropZone.css('background-color','#E3F2FC');
	        });
	        noteFileDropZone.on('drop',function(e){
	        	e.stopPropagation();
	            e.preventDefault();
	            // 드롭다운 영역 css
	            noteFileDropZone.css('background-color','#FFFFFF');
	            
	            let draggedFiles = e.originalEvent.dataTransfer.files;
	            if(draggedFiles != null){
	                if(draggedFiles.length < 1){
	                    return;
	                }
	                selectFile(draggedFiles)
	            }else{
	                console.warn("ERROR")
	            }
	        });			
		} else {
			let noteFileDropZone = $("div#editorDIV[contenteditable=false]");
			noteFileDropZone.off('dragenter').off('dragleave').off('dragover').off('drop')
		}        
    }
 
    // 파일 선택시
    function selectFile(files){
        // 다중파일 등록
        for(let fileNum=0; fileNum<files.length; fileNum++){
        	onFileSelect(files[fileNum]);
        }
    }
    // 12-31 여기까지
	
	function fileUploadInit(event,widget){
        let savedSelection = null;
        let savedRange = null;
		var filenode = document.createElement("LI");                 // Create a <li> node
		var filetextnode = document.createTextNode("File");         // Create a text node
		var fileButton = document.createElement("button")
		var iconClass = '<i class="se-icon-link"></i>'
		var tooltip = '<span class="se-tooltip-inner"><span class="se-tooltip-text">' + 'File' + "</span></span>"
		filenode.setAttribute("id", "file_btn_node")
		filenode.appendChild(filetextnode); 

		fileButton.setAttribute("type", "button"),
		fileButton.setAttribute("id", "file_upload"),
		fileButton.setAttribute("onclick","TNoteFileController.fileUploadBtnInit(event)")
		fileButton.setAttribute("class", "se-btn se-tooltip"),
		fileButton.setAttribute("data-command","")
		fileButton.innerHTML = iconClass,
		fileButton.innerHTML += tooltip,
		filenode.appendChild(fileButton);
		document.querySelector('.se-menu-list').appendChild(filenode);
	}
	function fileContextMenu(event){
		/*
		 *  새로 context 버튼 
		 */
		
    	let listLayer = document.createElement('div');
    	let submenu = document.createElement('div');
    	let listUL = document.createElement('ul');
    	let listLI = document.createElement('li');
    	let localBTN = document.createElement('button');
    	let textlocalBtn = document.createTextNode("내 로컬에서 첨부");
    	let texttdriveBtn = document.createTextNode("TeeDrive에서 첨부"); 
    	let tdriveBTN = document.createElement('button');
    	listLayer.setAttribute('id','fileContextUploadBtn');
    	listLayer.setAttribute('class','se-list-layer');
    	listLayer.setAttribute('style','display: block; left: 1px;');
    	submenu.setAttribute('class','se-submenu se-list-inner');
    	listUL.setAttribute('class','se-list-basic');
    	localBTN.setAttribute('class','se-btn-list');
    	localBTN.setAttribute('type','button');
    	localBTN.setAttribute('data-command','');
    	localBTN.setAttribute('data-value','');
    	localBTN.setAttribute('title','내 로컬에서 첨부');
    	localBTN.setAttribute('onclick','TNoteFileController.fileUploadBtnInit(event)');
    	localBTN.appendChild(textlocalBtn)
    	
    	tdriveBTN.setAttribute('class','se-btn-list');
    	tdriveBTN.setAttribute('type','button');
    	tdriveBTN.setAttribute('data-command','');
    	tdriveBTN.setAttribute('data-value','');
    	tdriveBTN.setAttribute('title','T-Drive에서 첨부');
    	tdriveBTN.setAttribute('onclick','TNoteFileController.TdridvefileUploadBtnInit(event)');
    	tdriveBTN.appendChild(texttdriveBtn);
    	
    	listLI.appendChild(localBTN)
    	listLI.appendChild(tdriveBTN)
    	
    	listUL.appendChild(listLI)
    	
    	submenu.appendChild(listUL)
    	listLayer.appendChild(submenu)
    	
		let isBtn = $('#fileContextUploadBtn');
		if(isBtn.length === 0)	{
			$('.userButton_0').addClass('on');
			$('.userButton_0')[0].parentNode.appendChild(listLayer)
			$('.se-menu-list li').bind("click", function(e){
				if(!e.currentTarget.children[0].classList.contains("userButton_0")) {
					$('.userButton_0').removeClass('on');
					$('#fileContextUploadBtn').remove() 				
				}
			})
		}
		else {
			
			$('.se-menu-list li').unbind()
			$('.userButton_0').removeClass('on');
			isBtn.remove()
		}
	}
	function fileUploadBtnInit(event,widget){
		let noteFileChooser = Top.Device.FileChooser.create({
			onBeforeLoad : function() {
                // TNoteFileUploader.checkAndUploadFiles( uploadFiles );
				let fileCheck = fileManager.onBeforeLoadFile(this.file);
				if(!fileCheck.result) {
		            ALERT(fileCheck.message);
		            return false;
		        }
				return true;
			},
			onFileChoose : function(file){
				onFileSelect(this.file[0])
			}
		})
		noteFileChooser.show();
	}
	function TdridvefileUploadBtnInit(event,widget){
		let SelectedNote = _noteListView.getSelectedData();
		var config = {
				channel_id : noteChannel_Id,
				user_context_1: SelectedNote.note_id,
				successCallback : function(fileMeta){
					if(isImage(fileMeta) == true){ // 이미지 일 경우					
			            fileId = fileMeta.storageFileInfo.file_id;
			            let targetSRC = _workspace.url + "Storage/StorageFile?action=Download" + "&fileID=" + fileId + "&workspaceID=" + workspaceManager.getWorkspaceId(getRoomIdByUrl(), "CHN0003") + "&channelID=" + noteChannel_Id + "&userID=" + userManager.getLoginUserId();
//			            let imagesInfo = TNoteFileController.createImageElement(targetSRC, fileMeta.storageFileInfo.file_name, fileMeta.storageFileInfo.file_size, fileId);
//			            document.querySelector('top-htmleditor#noteEditor').onImageUpload(imagesInfo[0], imagesInfo[1] ,false, imagesInfo[2]);
			            noteFileArray.push(fileId)
					}else{ // 파일 일 경우	
						let notefileMeta = {
					     		   "dto": 
					     		   {
					     		        "workspace_id": routeManager.getWorkspaceId(),
					     		        "channel_id": noteChannel_Id,
					     		        "storageFileInfoList": [{
					     		        "user_id": userManager.getLoginUserId(),
					     		        "file_last_update_user_id": userManager.getLoginUserId(),
					     		        "file_id": fileMeta.storageFileInfo.file_id,
					     		        "file_name": fileMeta.storageFileInfo.file_name,
					     		        "file_extension": fileMeta.storageFileInfo.file_extension,
					     		        "file_created_at": fileMeta.storageFileInfo.file_created_at,
					     		        "file_updated_at": fileMeta.storageFileInfo.file_updated_at, 
					     		        "file_size": fileMeta.storageFileInfo.file_size,
					     		        "user_context_1": SelectedNote.note_id,
					     		        "user_context_2": "",
					     		        "user_context_3": ""
					     		        }]
					     		    }
					        };
						TNoteFileController.createFileContent(notefileMeta);
					}
				}
		}
		OPEN_TDRIVE(config)
		UTIL.ID('fcDialog').setProperties({"on-close": function(){
			if(Top.curRoute.params.note !== undefined) TNoteController.setModalMode();
		}})
		TNoteController.removeModalMode();
	}
	function onFileSelect(uploadFile){
//		let uploadFile = noteFileChooser.file[0];
		let SelectedNote = _noteListView.getSelectedData();
		
		// file 이름에서 확장자 찾는 로직. 나중에 함수화 해서 빼기
        let tmpName = uploadFile.name;
        let fileName = tmpName;
        let fileExtension = 'file';
        let ws_id = workspaceManager.getWorkspaceId(getRoomIdByUrl(), "CHN0003");
        let dotIndex = tmpName.lastIndexOf('.');
        if(dotIndex !== -1){
     	   fileExtension = tmpName.substring(dotIndex+1,tmpName.length);
     	   fileName = tmpName.substring(0,dotIndex);
        }
		let inputDTO = {
     		   "dto": 
     		   {
     		        "workspace_id": ws_id,
     		        "channel_id": noteChannel_Id,
     		        "storageFileInfo": {
     		        "user_id": userManager.getLoginUserId(),
     		        "file_last_update_user_id": userManager.getLoginUserId(),
     		        "file_id": "",
     		        "file_name": fileName,
     		        "file_extension": fileExtension,
     		        "file_created_at": "",
     		        "file_updated_at": "", 
     		        "file_size": uploadFile.size,
     		        "user_context_1": SelectedNote.note_id,
     		        "user_context_2": "",
     		        "user_context_3": ""
     		        }
     		    }
        };
		
		//
		
		if(TNoteFileController.isImage(uploadFile)){
			let _successCallback = function(data) {
                fileId = data.dto.storageFileInfoList[0].file_id;
                let targetSRC = _workspace.url + "Storage/StorageFile?action=Download" + "&fileID=" + fileId + "&workspaceID=" + workspaceManager.getWorkspaceId(getRoomIdByUrl(), "CHN0003") + "&channelID=" + noteChannel_Id + "&userID=" + userManager.getLoginUserId();
//                let imagesInfo = TNoteFileController.createImageElement(targetSRC, uploadFile.name, uploadFile.size, fileId);
//                document.querySelector('top-htmleditor#noteEditor').onImageUpload(imagesInfo[0], imagesInfo[1] ,false, imagesInfo[2])
                let attibuteImg = {
            		"id": fileId,
                	"file_id": fileId,
                	"data-file-name": fileName,
                	"width": "100%",
                	"height": "auto"
                }
                UTIL.ID('noteEditor').addImage(targetSRC,attibuteImg)
                setDomNodeHandler(fileId)
            };    
            let _errorCallback = function(data) {
                console.log(data.message);
            };
			storageManager.UploadFile(uploadFile, inputDTO , "Note" , "noteFile" , _successCallback ,_errorCallback);
		}else{
			storageManager.UploadFile(uploadFile, inputDTO , "Note" , "noteFile" , TNoteFileController.addFileImage ,TNoteFileController._errorCallBack);
		}		
	}
	function addFileImage(data){
		let SelectNote = _noteListView.getSelectedData();
    	let fileId = data.dto.storageFileInfoList[0].file_id;
    	let notefileExtension = data.dto.storageFileInfoList[0].file_extension;
    	let fileName = data.dto.storageFileInfoList[0].file_name;
    	let fileCreatedTime = data.dto.storageFileInfoList[0].file_created_at;
    	let fileSize = data.dto.storageFileInfoList[0].file_size;
    	let fileIcon;
    	/* DATA HANDLE */
    	fileSize = getFileSizeText(fileSize);
    	notefileExtension = fileExtension.getInfo(notefileExtension)["iconImage"];
    	fileIcon = notefileExtension;
    	switch (notefileExtension) {
			case "./res/drive/file_icons/drive_image.svg":
				fileIcon = './res/drive/file_icons/drive_image.svg'
				break;
			case "./res/drive/file_icons/drive_tocell.svg":
				fileIcon = './res/drive/file_icons/drive_tocell.svg'
				break;
			case "./res/drive/file_icons/drive_topoint.svg":
				fileIcon = './res/drive/file_icons/drive_topoint.svg'
				break;
			case "./res/drive/file_icons/drive_file.svg":
				fileIcon = './res/drive/file_icons/drive_file.svg'
				break;
			case "./res/drive/file_icons/drive_zip.svg":
				fileIcon = './res/drive/file_icons/drive_zip.svg'
				break;
			case "./res/drive/file_icons/drive_toword.svg":
				fileIcon = './res/drive/file_icons/drive_toword.svg'
				break;
    	}
    	
    	/*
    	 *  새로 만든 버튼쓰
    	 */
		document.querySelector('top-htmleditor#noteEditor').addFileImage({

			  id: fileId,

			  imagePath: fileIcon,

			  name: fileName,

			  updatedTime: fileCreatedTime,

			  size: fileSize
			  
		});
		setTimeout(function(){
			console.log(('#' + fileId + '.sun-editor-font-fix2'))
			$('#' + fileId + ' .sun-editor-font-fix2').attr("style","font-size: 0.63rem !important;");
			$('#' + fileId + ' .sun-editor-font-fix')[0].classList.add('note-file-icon');
			$('#' + fileId + ' .sun-editor-font-fix')[0].classList.remove('sun-editor-font-fix');
			$('#' + fileId + ' div.sun-editor-dataArea .sun-editor-font-fix')[0].classList.add('note-file-name');
			$('#' + fileId + ' div.sun-editor-dataArea .sun-editor-font-fix')[0].classList.remove('sun-editor-font-fix');
//			$('div.note-file-icon .sun-editor-font-fix .sun-editor-font-fix')[0].removeAttribute("style");
			$('#' + fileId )[0].parentNode.addEventListener("mouseenter",function(){
				hoverImageContext(this)
			})
			$('#' + fileId )[0].parentNode.addEventListener("mouseleave", function() {
        		leaveImageContext(this)
        	})
        	$('#' + fileId )[0].parentNode.addEventListener("click",function(e){
				TNoteFileController.clickImageContext(this,e)
			})
        	TNoteController.attchCloseBtn();
		},1)		
	}
	function hoverImageContext( targetElement ){
		let noteCheckFileIcon = targetElement.querySelector('.note-file-icon');
		if(noteCheckFileIcon) $(noteCheckFileIcon).attr("style","display: none !important;")
		let noteFileHoverIcon = targetElement.querySelector('.note-file-download');
		if(!noteFileHoverIcon){
			let downloadBtn = document.createElement("div");
			downloadBtn.classList.add("icon-work_download")
			downloadBtn.classList.add("note-file-download")
			noteCheckFileIcon.before(downloadBtn);
		}

	}
	function leaveImageContext( targetElement ){
		$('.note-file-icon').attr("style","display: inline-block !important;");
		let noteFileLeaveIcon = targetElement.querySelector('.note-file-download');
		if(noteFileLeaveIcon) noteFileLeaveIcon.remove();
	}
	function clickImageContext ( targetElement, e ){
		if(e.target.className === "sun-editor-font-fix2") event.stopPropagation();
		else if(e.target.classList.contains("note-file-download")) {
			fileContextMenuHandler( e );
		}
	}
	function fileContextMenuHandler( e ){
		let targetfileId = e.target.closest(".sun-editor-file-content").id
		
		let fileTempDiv = document.getElementById('__fileAttachContextMenu')
    	if(fileTempDiv.style.display === 'none'){
    		e.preventDefault();
        	e.stopPropagation();
    		$(fileTempDiv).empty();
    		fileTempDiv.style.display = 'block';
//    		let div1 = document.createElement('div')
        	let div2 = document.createElement('div')
//        	div1.innerText = "TeeDrive에 저장"
//    		div1.setAttribute("id","__tdrive-down")
    		div2.innerText = "내 PC에 저장"
			div2.setAttribute("id","__local-down")
			// tdrive는 일단 킵
//			fileTempDiv.appendChild(div1)
        	fileTempDiv.appendChild(div2)
        	fileTempDiv.style.left = e.clientX + 'px';
    		fileTempDiv.style.top = e.clientY + 10 + 'px';
//        	div1.disabled = true;
        	div2.addEventListener("click",function(){
        		onFileDownload(targetfileId)
        	})
    	}
	}
	
	function setDomNodeHandler( target ){
		//history가 500 시간 이 후 div-component 만듬 
		setTimeout(function(){
			let targetElement =  document.getElementById(target);
			targetElement.closest('div.se-component').addEventListener('DOMNodeRemoved', function(e){
            	console.log(e)
            	let deleteImgFileId = e.target.children[0].firstElementChild.id
    			if(deleteImgFileId != null){
    				noteFileDelArray.push(deleteImgFileId)
    			}
            })
		},500)
	}
	
	function onFileDownload(fileId){
        let downLoadSuccess = function (response) {
            if (response.dto.resultMsg != "Success") {
                return;
            }
//            if (Top.Util.Browser.isIE()) {
//                window.navigator.msSaveBlob(response.blobData, response.fileName);
//            } else {
//                let a = document.createElement("a");
//                a.href = response.url;
//                a.download = response.fileName;
//                a.click();
//            }
//            URL.revokeObjectURL(response.url);
        };
        let downLoadError = function (response) {
        	if (response.dto.resultMsg === "Fail"){
        		if(response.dto.resultDetail === "invalid file_id") notiFeedback("손상되었거나 삭제된 파일입니다.")
        	}
        };
        
//        fileManager.getDownloadFileInfo(fileId, successCallback, errorCallback);
        
        // file id , ch_id , ws_id , success , fail
        storageManager.DownloadFile(fileId, noteChannel_Id, workspaceManager.getWorkspaceId(getRoomIdByUrl(), "CHN0003"), downLoadSuccess, downLoadError);
	}
	function getImageFileID(e){
		let imgContentID = e.target.children[0].getAttribute("file_id")
		TNoteFileController.onFileDownload(imgContentID)
	}
	function createImageElement(fileSrc, fileName, fileSize, fileId){
		// create IMG , DIV , FIGURE element
		let imgElement = document.createElement("img")
		let divElement = document.createElement("DIV");
		let figureElement = document.createElement("figure");
		let imageInfoObj = [];   // SunEditor 용 ImageInfo
		let imageIndexCount;	 // SunEditor 용 index
		let returnObj = [];
		
		// element set Attribute
		divElement.setAttribute("class","sun-editor-id-comp sun-editor-id-image-container float-none")
		divElement.setAttribute("contenteditable", false);		
		figureElement.setAttribute("class","sun-editor-figure-cover")
		figureElement.setAttribute("style","margin: 0px;")
		
		// imgelement set Attribute
		imgElement.setAttribute("src", fileSrc)
		imgElement.setAttribute("data-file-name", fileName)
		imgElement.setAttribute("data-file-size", fileSize)
        imgElement.setAttribute("file_id", fileId)
//        imgElement.setAttribute('onerror', "this.src='./res/not-load-image.png'")
        
		imageIndexCount = $('#noteEditor .se-wrapper-wysiwyg').find('img').length;
        imgElement.setAttribute('data-index', imageIndexCount)
        imgElement.setAttribute('img_Index', imageIndexCount)
		imageInfoObj = {
                src: fileSrc,
                name: fileName,
                size: fileSize,
                file_id : fileId
		}
		// parentElement Set
		figureElement.appendChild(imgElement)
		divElement.appendChild(figureElement)
		returnObj.push(imgElement)
		returnObj.push(imageIndexCount);
		returnObj.push(imageInfoObj)

		// insertNode set
		let startRange = window.getSelection().getRangeAt(0);
		let startSelection = window.getSelection()
		let afterNode;
		let parentNode;
		parentNode = startRange.startContainer;
		if(startRange.startContainer.nodeType === 3){
			parentNode = startRange.startContainer.parentNode;
		}
		if(startRange.commonAncestorContainer.nodeType === 3){
			// text 사이에 넣으려고 할때
			afterNode = startRange.commonAncestorContainer.splitText(startRange.endOffset)
			afterNode =  afterNode.nextSibling;
			parentNode.insertBefore(divElement, afterNode);
			startRange.setStart(startRange.startContainer, startRange.startOffset);
			startRange.setStartAfter(divElement);
			startRange.setEnd(startRange.endContainer, startRange.endOffset);
			startRange.setEndAfter(divElement);
			startSelection.removeAllRanges();
			startSelection.addRange(startRange);
			let pElement = document.createElement('p')
			let brElement = document.createElement('br')
			pElement.appendChild(brElement)
			startRange.commonAncestorContainer.parentNode.appendChild(pElement);
		}else{
			if(document.querySelector('top-htmleditor#noteEditor').getEditorHTML().length === 0){// 내용이 하나도 없는 경우
				startRange.insertNode(divElement);
	    		startRange.setStart(startRange.startContainer, startRange.startOffset);
	    		startRange.setStartAfter(divElement);
	    		startRange.setEnd(startRange.endContainer, startRange.endOffset);
	    		startRange.setEndAfter(divElement);
	    		startSelection.removeAllRanges();
	    		startSelection.addRange(startRange);
	            document.execCommand("insertHTML", false, "<p><br></p>");	
			}else{
				let pElement = document.createElement('p')
				let brElement = document.createElement('br')
				pElement.appendChild(brElement)
				startRange.commonAncestorContainer.parentNode.appendChild(divElement);
				startRange.commonAncestorContainer.parentNode.appendChild(pElement);
			}
		}
        $('#fileContextUploadBtn').remove();
        TNoteFileController.saveSelectNode();
        
        divElement.addEventListener('DOMNodeRemoved', function(e){
        	let deleteImgFileId = e.target.children[0].children[0].getAttribute("file_id")
			if(deleteImgFileId != null){
				noteFileDelArray.push(deleteImgFileId)
			}
        })
		
		return returnObj;
	}
	function isImage(file){
		if(file['type'] == undefined){
			let ImageExt = ["jpg","gif","jpeg","tiff" ,"bmp" ,"bpg","png"]
			if(ImageExt.includes((file.storageFileInfo.file_extension.toLowerCase()))){
				return true;
			}
			else{
				return false;
			}
		}else{
			return file['type'].split('/')[0] == 'image'
		}
		
	}
	function checkFocusNode(node){
		let focusNode = node;
		if(focusNode.commonAncestorContainer[0] !== undefined){
			if(focusNode.commonAncestorContainer[0].id == "tagTextField" || focusNode.commonAncestorContainer[0].id == "contenteditorTitle"){
				return false;
			}
		}else{
			if(focusNode.commonAncestorContainer.id == "footer__text-area"){
				return false;
			}
		}
	}
	function createFileContent(data){ // 여기 작업하기V
    	
		/* CALLBACK DATA*/
		let SelectNote = _noteListView.getSelectedData();
    	let fileId = data.dto.storageFileInfoList[0].file_id;
    	let notefileExtension = data.dto.storageFileInfoList[0].file_extension;
    	let fileName = data.dto.storageFileInfoList[0].file_name;
    	let fileUpdatedTime = data.dto.storageFileInfoList[0].file_updated_at;
    	let fileSize = data.dto.storageFileInfoList[0].file_size;
    	let fileIcon = null;
    	/* DATA HANDLE */
    	fileSize = getFileSizeText(fileSize);
	    /*
	     *  새로 만든 extension
	     */
//    	notefileExtension = fileExtension.getInfo(notefileExtension)["iconImage"];
//    	switch (notefileExtension) {
//			case "./res/drive/file_icons/drive_image.svg":
//				fileIcon = './res/drive/file_icons/drive_image.svg'
//				break;
//			case "./res/drive/file_icons/drive_tocell.svg":
//				fileIcon = './res/drive/file_icons/drive_tocell.svg'
//				break;
//			case "./res/drive/file_icons/drive_topoint.svg":
//				fileIcon = './res/drive/file_icons/drive_topoint.svg'
//				break;
//			case "./res/drive/file_icons/drive_file.svg":
//				fileIcon = './res/drive/file_icons/drive_file.svg'
//				break;
//			case "./res/drive/file_icons/drive_zip.svg":
//				fileIcon = './res/drive/file_icons/drive_zip.svg'
//				break;
//			case "./res/drive/file_icons/drive_toword.svg":
//				fileIcon = './res/drive/file_icons/drive_toword.svg'
//				break;
//    	}
    		
    	Note.noteFileCreate(data.dto.storageFileInfoList[0], SelectNote.note_id)
        let template =   '<div class="sun-editor-file-body" onclick="TNoteFileController.setSelectNode(event)">' +
                       	    '<div id=' + fileId + ' class="sun-editor-file-content" style="display: flex; line-height: 20px; cursor: pointer;">' +
	                       		'<div class="sun-editor-font-fix" style="display: inline-block !important; margin-right: 5px; margin-top: 3px; vertical-align:top !important; overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">' + 
	                       			'<div id="fileDocument" class="sun-editor-font-fix" style="display : inline-block !important;">' +
	                       				'<img src="./res/' + fileExtension + '2.svg"' + ' class="sun-editor-font-fix" style="display : inline-block !important; pointer-events: none;">' +
	                       			'</div>' + 
	                       		'</div>' +
	                       		'<div class="sun-editor-dataArea" style="display: inline-block !important; overflow: hidden; text-overflow: ellipsis; white-space: nowrap; height: 40px !important;">' +
	                       			'<div class="sun-editor-dataArea" style="display: flex; flex-direction: column; justify-content: center; overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">' +
		                       			'<span id="UploadfileName" class="sun-editor-font-fix" style="width: 100%; display: block; overflow: hidden; text-overflow: ellipsis; white-space: nowrap;" >' + 
		                       				fileName +
		                       			'</span>' +
		                       		'</div>' +
		                       		'<div class="sun-editor-dataArea" style="display: inline-block !important; overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">' +
		                       			'<span id="UploadfileTime" class="sun-editor-font-fix2" style="display: block; overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">' +
		                       				fileUpdatedTime + " , " + fileSize +  
		                        		'</span>' +
		                        	'</div>' +
	                        	'</div>' +
	                         '</div>' +
                          '</div>' 
    	
	    /*
	     *  새로 만든 template
	     */
	                         
//      let filetemplate = 
//    	  
//    	  '<div class="note-file-content">'+
//    	  	'<div id=' + fileId + ' class="note-file-body">'+					
//    	  		'<img class="note-file-icon" src=' + fileIcon + '>'+
//    	  		'<div class="note-file-text">'+						
//    	  			'<div class="note-file-title">'+							
//    	  				'<span class="note-file-name">' + fileName + '</span>' +							
//	  				'</div>'+
//	  				'<div class="note-file-title">'+	
//    	  				'<span class="note-file-size">' + fileUpdatedTime + " , " + fileSize +   '</span>' +			            
//    	  			'</div>'+			            			   
//    	  		'</div>'+	
//          '</div>'
        let targetNode= document.createElement('div');
        targetNode.setAttribute("class", "sun-editor-content")
        targetNode.setAttribute("class", "note-editor-content")
        targetNode.setAttribute("contenteditable", false)
        targetNode.innerHTML = template;
        
        let successFlag = true;
		let startRange = window.getSelection().getRangeAt(0);
		let startSelection = window.getSelection()
		successFlag = TNoteFileController.checkFocusNode(startRange);
		if(successFlag == false){
			TNoteServiceCaller.noteFileDelete(fileId);
			$('#fileContextUploadBtn').remove()
			return;
		}else{
			if(startRange.commonAncestorContainer.nodeType === 3 || startRange.commonAncestorContainer.nodeType === 1){
				// text 사이에 넣으려고 할때
	            startRange.insertNode(targetNode);
			}else{
				if(editor.getContents().length === 0){
					startRange.insertNode(targetNode);
				}else{
					editor.getContext().element.wysiwyg.insertBefore(targetNode,startRange.commonAncestorContainer)	
				}
			}
			noteFileArray.push(fileId);
			
			// range 다시 set 11-04
			startRange.setStart(startRange.startContainer, startRange.startOffset);
			startRange.setStartAfter(targetNode);
			startRange.setEnd(startRange.endContainer, startRange.endOffset);
			startRange.setEndAfter(targetNode);
			startSelection.removeAllRanges();
			startSelection.addRange(startRange);
	        
	        document.execCommand("insertHTML", false, "<p><br></p>");
	        targetNode.addEventListener('DOMNodeRemoved', function(e){
				let deleteFileId = e.path[0].id;
				if(deleteFileId != null){
					noteFileDelArray.push(deleteFileId)
				}
	        },false)

	        $('#fileContextUploadBtn').remove()
	        TNoteFileController.saveSelectNode();			
		}
    }
	function setFileDelListener(){
    	let imgList = $('div.se-image-container')
    	let fileList = $('div.sun-editor-content')
    	if(imgList.length != 0){
    		for(var i=0; i< imgList.length; i++){
    			// imgContent Listener binding
    			imgList[i].addEventListener('DOMNodeRemoved', function(e){

    				let deleteImgFileId = e.target.children[0].children[0].getAttribute("file_id")
    				
    				if(deleteImgFileId != null){
    					noteFileDelArray.push(deleteImgFileId)
    				}
    	        })
    		}
    	}
    	if(fileList.length != 0){
    		for(var j=0; j< fileList.length; j++){
    			// fileContent Listener binding
    			fileList[j].addEventListener('DOMNodeRemoved', function(e){
    				if(e.target.classList.contains("note-file-download")) e.stopPropagation();
    				else if(e.target.id === "__note-file_close") e.stopPropagation();
    				else{
        				let deleteFileId = e.path[0].children[0].children[0].id
        				if(deleteFileId != null){
        					noteFileDelArray.push(deleteFileId)
        				}    					
    				}
    	        })
    		}
    	}
    }
	function _errorCallBack(data){
    	console.log(data)
    }
	function setSelectNode(event){
    	let fileBody = $(event.target).closest(".sun-editor-file-body")
    	let fileContentID = $(event.target).closest(".sun-editor-file-content")[0];
    	let fileEmptyContent = $(event.target).closest(".sun-editor-file-empty");
    	let selectfileID = fileBody[0].children[0].id
    	let isEditModeflag = $('#readTitle').length;
    	if(fileEmptyContent.length != 0){
    		return;
    	}else{
        	if(isEditModeflag== 1){ // 읽기모드는 다운로드
        		onFileDownload(fileContentID.id);// url 박기
        	}else if(isEditModeflag == 0){ // 수정모드 
        		if(!fileBody.hasClass('sun-editor-file-modal')){
           			fileBody.addClass('sun-editor-file-modal')
           			TNoteFileController.set_Button(event)
            	}else {
            		fileBody.removeClass('sun-editor-file-modal')
            		$('#file-removeController').remove()
            	}
        	}    		
    	}   		
    }
	function set_Button(event) {
    	let appendTarget = $(event.target).closest(".sun-editor-content")
        const tooltipBtn = document.createElement("DIV");
        tooltipBtn.id ="file-removeController"
        tooltipBtn.className = "se-controller",
        tooltipBtn.style.display = "block",
        tooltipBtn.style.margin ="11px 0px 0px 0px",
        tooltipBtn.innerHTML = '' +
        '<div class="arrow arrow-up"></div>' +
        '<div class="btn-group sun-editor-id-resize-button-group">' +
        '   <button type="button" onclick="TNoteFileController.fileRemove(event)" class="se-tooltip">' +
        '       <i class="icon-delete"></i>' +
        '       <span class="se-tooltip-inner"><span class="se-tooltip-text">' + 'Remove' + '</span></span>' +
        '   </button>' +
        '</div>'
        if($('#file-removeController').length == 0) appendTarget.append(tooltipBtn)
    }
	function saveSelectNode() {
        if (window.getSelection) {
            //non IE Browsers            
            savedSelection = window.getSelection();
         if (savedSelection.getRangeAt && savedSelection.rangeCount) {
                savedRange = savedSelection.getRangeAt(0);
         }
        }else if (document.selection) {
            //IE
            savedSelection = document.selection;
            savedRange = document.selection.createRange();
        }
        return savedRange;
    }
    function fileRemove(event){
    	let fileRemoveController = $(event.target).closest(".se-controller")
    	let deleteFileID = $(fileRemoveController)[0].previousSibling.children[0].id
    	let deleteDIV =  $(event.target).closest(".sun-editor-content")
//    	fileManager.deleteFile(deleteFileID)
    	deleteDIV.remove()
//    	let fileDeleteDTO = [{
//    		"file_id" : deleteFileID
//    	}]  리스트 서비스가 아닌 우선 단일 타겟 제거 서비스콜 중.
//    	Note.noteFileDelete(deleteFileID);
    }
    function isFileSelect(){
    	let file_modal = document.querySelector('div.sun-editor-file-modal')
    	if(file_modal != null){
    		file_modal.classList.remove('sun-editor-file-modal')
    		$('#file-removeController').remove()
    	}else return;
    }
    
    function print() {
        const iframe = document.createElement('IFRAME');
        iframe.style.display = 'none';
        document.body.appendChild(iframe);

        const printDocument = getIframeDocument(iframe);
        const contentsHTML = document.querySelector('top-htmleditor#noteEditor').getEditorHTML(true);
        const wDoc = document;
        const options = [];
        
        if (options.iframe) {
            const arrts = options.fullPage ? getAttributesToString(wDoc.body, ['contenteditable']) : 'class="sun-editor-editable"';

            printDocument.write('' +
                '<!DOCTYPE html><html>' +
                '<head>' +
                wDoc.head.innerHTML +
                '<style>' + getPageStyle(wDoc) + '</style>' +
                '</head>' +
                '<body ' + arrts + '>' + contentsHTML + '</body>' +
                '</html>'
            );
        } else {
            const contents = document.createElement('DIV');
            const style = document.createElement('STYLE');

            style.innerHTML = getPageStyle(wDoc);
            contents.className = 'sun-editor-editable';
            contents.innerHTML = contentsHTML;

            printDocument.head.appendChild(style);
            printDocument.body.appendChild(contents);
            console.log(printDocument.body)
        }

        try {
            iframe.focus();
            // IE or Edge
            if (!!document.documentMode || !!window.StyleMedia) {
                try {
                    iframe.contentWindow.document.execCommand('print', false, null);
                } catch (e) {
                    iframe.contentWindow.print();
                }
            } else {
                // Other browsers
            	console.log(iframe)
            	  var pdf = new jsPDF('p', 'pt', 'letter');
				  pdf.canvas.height = 72 * 11;
				  pdf.canvas.width = 72 * 8.5;
				  source = $(printDocument.body)[0];
				  var malgun = 'https://www.giftofspeed.com/base64-encoder/'
//				  pdf.addFileToVFS('malgun.ttf',malgun)
				  pdf.addFont('malgun.ttl','malgun','normal');
				  pdf.setFont('malgun')
				  console.log(source)
				  pdf.fromHTML(source);
				
				  pdf.save('test.pdf');
            	
                iframe.contentWindow.print();
            }
        } catch (error) {
            throw Error('[SUNEDITOR.core.print.fail] error: ' + error);
        } finally {
            removeItem(iframe);
        }
    }
    function getPageStyle(doc) {
        let cssText = '';
        const sheets = (doc).styleSheets;
        
        for (let i = 0, len = sheets.length, rules; i < len; i++) {
            try {
                rules = sheets[i].cssRules;
            } catch (e) {
                continue;
            }
            
            for (let c = 0, cLen = rules.length; c < cLen; c++) {
                cssText += rules[c].cssText;
            }
        }

        return cssText;
    }
    function getIframeDocument(iframe) {
        let wDocument = iframe.contentWindow || iframe.contentDocument;
        if (wDocument.document) wDocument = wDocument.document;
        return wDocument;
    }
    function getAttributesToString(element, exceptAttrs) {
        if (!element.attributes) return '';

        const attrs = element.attributes;
        let attrString = '';

        for (let i = 0, len = attrs.length; i < len; i++) {
            if (exceptAttrs && exceptAttrs.indexOf(attrs[i].name) > -1) continue;
            attrString += attrs[i].name + '="' + attrs[i].value + '" ';
        }

        return attrString;
    }
    function removeItem(item) {
        if (!item) return;
        try {
            item.remove();
        } catch (e) {
            item.parentNode.removeChild(item);
        }
    }
    
    return {
    	fileUploadInit : fileUploadInit,
    	fileContextMenu:fileContextMenu,
    	fileUploadBtnInit:fileUploadBtnInit,
    	onFileSelect:onFileSelect,
    	onFileDownload:onFileDownload,
    	getImageFileID:getImageFileID,
    	createImageElement:createImageElement,
    	isImage:isImage,
    	checkFocusNode:checkFocusNode,
    	addFileImage:addFileImage,
    	hoverImageContext:hoverImageContext,
    	leaveImageContext:leaveImageContext,
    	clickImageContext:clickImageContext,
    	fileContextMenuHandler:fileContextMenuHandler,
    	createFileContent:createFileContent,
    	setFileDelListener:setFileDelListener,
    	_errorCallBack:_errorCallBack,
    	setSelectNode:setSelectNode,
    	set_Button:set_Button,
    	fileRemove:fileRemove,
    	isFileSelect:isFileSelect,
    	saveSelectNode:saveSelectNode,
    	noteFileDropDown:noteFileDropDown,
    	TdridvefileUploadBtnInit:TdridvefileUploadBtnInit,
    	print:print
    }
})()
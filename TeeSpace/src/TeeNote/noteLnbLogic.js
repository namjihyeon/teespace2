/*
 * LNB 메뉴들을 렌더하는 로직을 관리함.
 */
Top.Controller.create('noteLnbLayoutLogic', {
    trashCanRender : function(event, widget) {
    	TNoteController.selectTrashCanMenu();
    }, 
    allNoteClick : function(event, widget) {
    	TNoteController.selectTotalNoteBookMenu();
     },
	noteFileRender : function(event, widget) {
		TNoteController.selectFileMenu()
	},
	favNoteRender : function(event, widget) {
		TNoteController.selectFavoriteMenu();
	}
})
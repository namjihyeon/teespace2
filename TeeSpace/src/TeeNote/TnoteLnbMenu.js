noteLnbMenu = (function() {
    let li_cnt = 0;
    let isExpanded = false;
    let isEditorExpanded = false;
    let nameChangeBeforeDiv  = null;
    let nameChangeTargetId = null;
    let nameChangeUlIcon = null;
    let foldedChapterList = [];
    let colorList = ['#FB7748','#FB891B','#E7B400','#B4CC1B','#65D03F','#14C0B3','#00C6E6','#4A99F5','#9482FF','#E780FF','#FF7BA8'];
    let usedColor = [false,false,false,false,false,false,false,false,false,false,false]
    function hotfixFunction(){
		setEditorVisibility('none');
		setTagVisibility('none');
	 	setFoldingVisibility('none');
		setIsExpanded(noteLnbMenu.myIsExpanded());
		setExpandedIcon(noteLnbMenu.myIsExpanded());
    	setLnbVisibility('flex');
	}
	
    function switchExpand(e){
		// (sj) 필요하면 나중에 넣어야지
		// let event = e || window.event;
		// event.stopPropagation();

    	if(isExpanded){
    		// 접기 비활성화, 에디터 보이게, lnb 줄이고
			onFoldButtonClick('note',function(){ // 접을 떄
				noteLnbMenu.setIsExpanded(false);
				setExpandedIcon(false);

    			if(getTagVisibility() === "flex" || $('.note-lnb-ul.tag.selected').length === 1){ // 태그를 보고있었을 때,
					setLnbVisibility('none');
               		setEditorVisibility('none');
               		setTagVisibility('flex');
            		setFoldingVisibility('none');
            		document.getElementById('tagPreBtn').style.display = "inline"
				}
				else { // 에디터를 보고있었을 때
					setLnbVisibility('none');
               		setEditorVisibility('flex');
               		setTagVisibility('none');
					setFoldingVisibility('none');
					 
					// 페이지가 없습니다 아닐 때
					if (document.querySelector('#note-empty-div').style.display === "none") {
						let targetPage = document.querySelector('li[content-id].note-lnb-li')
						if(!targetPage){
							setLnbVisibility('flex');
							setEditorVisibility('none');
						}
					}
            		setEditorTitleContainer("none");
                    TNoteController.editorEventBind();
    			}
    			TNoteController.setModalSize()
         	})
    	} else{
    		// 반대로
			onExpandButtonClick('note',function(){ // 펼칠 때
				noteLnbMenu.setIsExpanded(true);
				setExpandedIcon(true);

    			if(getTagVisibility() === "flex" || $('.note-lnb-ul.tag.selected').length === 1){ // 태그를 보고있었을 때,
    				setLnbVisibility('flex');
            		document.getElementById('tagPreBtn').style.display = "none"
        			setTimeout(function(){
        				setTagVisibility('flex');
        				setEditorVisibility('none');
                		setFoldingVisibility('none');
                		TNoteController.setModalSize()
        			},1)        			
    			}else{ // 에디터를 보고있었을 때
    				let renderedPageDiv = $('li[content-id="'+ document.getElementById('note-editor-container').getAttribute('rendered-page-id') + '"]')
					let curChapterPage = TNoteData.getCurPageData();
					// 챕터는 선택되었으나 페이지가 없습니다인 경우
					if (curChapterPage["parent_notebook"] && !curChapterPage["note_id"]) {

					}
					else if(renderedPageDiv.length===0){ 
					// 에디터에 렌더되어있는 page가 Lnb Menu에 존재하지 않을 경우-----> 
					// 축소된 상태에서 셀렉된 애를 뒤로가기 해서 지우고, 에디터엔 지워진 값이 남아있는 상태에서 화면을 확대한 경우
						let targetPage = document.querySelector('li[content-id].note-lnb-li');
						let isChapter = document.querySelector('ul.note-lnb-ul.chapter');

	            		if(targetPage){
	            			let targetPageId = document.querySelector('li[content-id].note-lnb-li').getAttribute('content-id');
							document.querySelector('li[content-id="'+ targetPageId +'"]').classList.add('selected');
							// 이안에서 선택된 챕터, 페이지 아이디 저장함
			        		TNoteController.selectPageMenu(targetPageId);
						} 
						// 페이지는 없고 챕터만 있는 경우
						else if (isChapter && isChapter.getAttribute('chapter-id')) {
							TNoteData.setCurPageData(isChapter.getAttribute('chapter-id'), null);
						} 
						// 페이지도 없고 챕터도 없는 경우
						else {
							TNoteData.setCurPageData(null, null);
						}
						
	            	} else {
						TNoteController.checkEditorMode();
	            	}
	        		setLnbVisibility('flex');
	        		setTimeout(function(){
	            		setEditorVisibility('flex');
	            		setFoldingVisibility('flex');
	            		setTagVisibility('none');
	            		setEditorTitleContainer("flex");
	            		TNoteController.setModalSize()
	        		},1)
	        		
    			}
        	})
		}
	}
	// 다 찾아서 바꿔주는 것으로 바꿈
	// (note-TODO) 페이지 뒤로가기는 다른 곳에서 처리해줄 것
    function setExpandedIcon(value){
		let pageNonePreBtn = document.querySelector('.note-editor_page-none-header img.note_pre-btn');
		let expandBtns = document.querySelectorAll('.note-expand-btn');
    	if(value) {
			for (let i=0;i < expandBtns.length;i++) {
				expandBtns[i].classList.add('icon-work_collapse');
				expandBtns[i].classList.remove('icon-work_expand');
			}
			if (pageNonePreBtn) {
				pageNonePreBtn.style.visibility = "hidden";
			}
    	}
    	else {
    		for (let i=0;i < expandBtns.length;i++) {
				expandBtns[i].classList.add('icon-work_expand');
				expandBtns[i].classList.remove('icon-work_collapse');
			}
			if (pageNonePreBtn) {
				pageNonePreBtn.style.visibility = "visible";
			}
    	}
    }
    function setLnbVisibility(value){
		const lnb = document.querySelector('div#note-lnb-menus-cover');
		lnb.style.display = value;
		if (noteLnbMenu.isExpanded()) {
			lnb.querySelector('.note-divider__column').style.display = "none";
			lnb.querySelector('.note__header__column').style.display = "none";
		} else {
			lnb.querySelector('.note-divider__column').style.display = "";
			lnb.querySelector('.note__header__column').style.display = "";
		}
    }
    function setEditorVisibility(value){
    	document.getElementById('note-editor-container').style.display = value;
    }
    function setTagVisibility(value){
    	document.getElementById('note-tag-container').style.display = value;
    }
    function getTagVisibility(){
    	return document.getElementById('note-tag-container').style.display
    }
    function setFoldingVisibility(value){
    	if(value === "flex"){
    		setTimeout(function(){ 
    			document.getElementById('note-expand-fold').style.display = value;
            	UTIL.ID('notePreBtn').setProperties({"visible":"none"})
			},1)
    	}else if(value === "none"){
        	document.getElementById('note-expand-fold').style.display = value;
        	UTIL.ID('notePreBtn').setProperties({"visible":"inline"})
    	}

    }
    function setEditorTitleContainer(value){
    	if(value == "none") {
        	$('div#editor_title_container_2').css("display",value)
        	$('div#editor_title_container_2').css("width","0%")   		
    	}else if(value === "flex"){
    		$('div#editor_title_container_2').css("display",value)
        	$('div#editor_title_container_2').css("width","100%")
    	}

    }
    
    function initExpandNote(){
//    	isExpanded = true;
//    	setExpandedIcon(true);
//    	setTimeout(function(){
//        	setFoldingVisibility('flex');
//    		let targetPageId = document.querySelector('li[content-id].note-lnb-li').getAttribute('content-id');
//    		document.querySelector('li[content-id="'+ targetPageId +'"]').classList.add('selected');
//        	TNoteController.selectPageMenu(targetPageId);
//    	},1)
    }
    
    function switchEditorExpand(e){
    	if(isEditorExpanded){
    		if(statusEditorExpand()) {
    			document.querySelector('div#note-lnb-menus-cover').style.display = "flex";
    			$('.note-folding-sub').css("transform", "translate(-0.9rem, 0px) rotate(0deg)" );
    		}
    		isEditorExpanded = false;
    	} else{
    		if(!statusEditorExpand()) {
    			document.querySelector('div#note-lnb-menus-cover').style.display = "none";
    			$('.note-folding-sub').css("transform", "rotate(180deg)" );
    		}
    		isEditorExpanded = true;
    	}
    	TNoteController.setModalSize()
    }
    function statusEditorExpand(){
    	if(document.querySelector('div#note-lnb-menus-cover').style.display === "flex") return false;
    	else if(document.querySelector('div#note-lnb-menus-cover').style.display === "none") return true;
	}

	// contextMenu click event
	// type = "chapter" or "page"
	function clickRenameContextMenu(type) {                                // 챕터 이름 바꾸기 로직!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		if(document.querySelector('.note-lnb-name-change-div')){
			TNoteController.initChapterList()
		}
		
		let targetId = document.getElementById('__noteContextMenu').getAttribute('target-content-id');
		let targetName = document.getElementById('__noteContextMenu').getAttribute('target-content-name');
		let targetDiv;
		setNameChangeTargetId(targetId); // 다른 컨텍스트 메뉴를 껏다 켜도 target Id가 바뀌지 않도록 하기 위함
		
		let _id, _placeholder, _func,_escKeyEvent, _chapterColor, template, _frag, nameChangeDiv, _input;

		const createTextField = () => {
			template = `
				<div class="note-lnb-name-change-div">
					<input id="${_id}" class="note-lnb-temp-ul-text" type="text" value="${targetName}" placeholder="${_placeholder}">
				</div>`

			_frag = document.createElement('template');
			_frag.insertAdjacentHTML("beforeend", template.trim());

			nameChangeDiv = _frag.firstElementChild;
			_input = nameChangeDiv.querySelector('input');
			targetDiv.replaceWith(nameChangeDiv);
			_input.select();
			_input.focus();
		};

		switch (type) {
			case "chapter" : {
				targetDiv = document.querySelectorAll('div[chapter-id="'+targetId+'"]')[0];
				_chapterColor = targetDiv.querySelector(".note-lnb-chapter-color").cloneNode(true);
				
				_id = "chapter-name-textfield";
				_placeholder = "새 챕터";
				_func = () => {
					changeChapterName(targetName);
				};

				_escKeyEvent = () => {
					TNoteController.initChapterList()
				};

				createTextField();
				// chapter에는 targetId 안 쓰여 있었음
				nameChangeDiv.insertBefore(_chapterColor, nameChangeDiv.firstElementChild);
				break;
			}
			case "page" : {
				targetDiv = document.querySelector('[content-id="'+targetId+'"]');
				_id = "page-name-textfield";
				_placeholder = "새 페이지";
				_func = () => {
					changePageName(targetId, _input);
				};

				_escKeyEvent = () => {
					if(document.querySelector('div[content-id="'+ getNameChangeTargetId() + '"]').parentNode.classList.contains('chapter')){
						TNoteController.initChapterList()
					} else {
						TNoteController.initSharedMenu()
					}
				};

				createTextField();
				nameChangeDiv.setAttribute("content-id", targetId);
				nameChangeDiv.style.marginLeft = "30px";
				break;
			}
		};

		_input.addEventListener('blur', function(e){
			if(!_input.classList.contains('exception')){
				_func();
			} else {
				_input.classList.remove('exception');
			}
		});
		_input.addEventListener('keyup', function(e){
			if(e.keyCode === 13){
				_func();
			} else if(e.keyCode === 27){
				_escKeyEvent();
			}
		}); 
	}

	// newChapter 버튼 클릭시 새 챕터 생성 이벤트
	function clickNewChapterButtonEvent(e) {
		e.stopPropagation();
		if (!document.querySelector('.note-lnb-temp-ul')){
			let tempUL = `
				<ul class="chapter note-lnb-temp-ul">
					<div class="note-lnb-ul-div">
						<span class="note-lnb-ul-icon">
							<span class="note-lnb-chapter-color" style="background-color:#c6ced6;color:#c6ced6"></span>
						</span>
						<input class="note-lnb-temp-ul-text" type="text">
					</div>
				</ul>`;

			document.getElementById('note-chapter-cover').insertAdjacentHTML('afterbegin', tempUL.trim());
			let textField = document.querySelector('.note-lnb-temp-ul-text');
			textField.setAttribute('placeholder','새 챕터')
			textField.focus();

			const inputComplete = function() {
				let input = $('.note-lnb-temp-ul-text')[0].value;
				let _regBlank = /^\s+|\s+$/g;
				if($('.note-lnb-temp-ul-text')[0].value.replace( _regBlank, '' ) == "" ) TeeAlarm.open({title: '',content: '이름을 입력해주세요.',cancelButtonText:"확인", onCancelButtonClicked: function(){ TeeAlarm.close(); textField.focus(); }});
				else if(TNoteData.checkDuplicateChapter(input))	{
					TeeAlarm.open({
						title: '중복된 이름이 있습니다.',
						content: '다른 이름을 입력해주세요.',
						cancelButtonText:"확인",
						onCancelButtonClicked: function(){ TeeAlarm.close(); textField.focus(); }
					});

					// style 먹이기
					let _dialog = document.querySelector(".tee-custom-dialog");
					_dialog.classList.add("note-alarm");
					
					let _infoMark = _dialog.querySelector('top-icon.icon-chart_error');
					let infoMark = document.createElement("img");
					if (_infoMark) {
						infoMark.setAttribute("src", "./res/note/ts_popup_info_alert@3x.png");
						infoMark.classList.add("note-alarm_info-mark");
						_infoMark.replaceWith(infoMark);
					}
				}
				else{
					TNoteServiceCaller.noteBookCreate(input, async function(noteInfo){
						spaceAPI.spaceInit();
						await TNoteController.initChapterList();
						let targetPageId = noteInfo["children"][0]["id"];
						TNoteRenderer.selectChapterPage(noteInfo["id"], targetPageId);
						// editor에 해당 페이지 띄우기, 이 안에서 setData도 한다
						TNoteController.selectPageMenu(targetPageId, false);						
					});
					document.removeEventListener("click", inputCompleteByBlur)
				}
			};

			const inputCompleteByBlur = function(e) {
				let tempChapterItem = document.querySelector('.chapter.note-lnb-temp-ul');
				if (!tempChapterItem) {
					try {// 혹시나 에러날까봐
						document.removeEventListener("click", inputCompleteByBlur);
					} catch(e) {
						console.log(e)
					}
				}
				if (e.target.getAttribute("id") === "note-lnb-new-chapter") return;
				// 중복된 이름있다는 알림 확인 버튼 클릭시
				if (e.target.closest(".tee-custom-dialog")) return;
				if (tempChapterItem.contains(e.target)) return;
				inputComplete();
			};

			textField.addEventListener('keyup', function(e){
				if(e.keyCode === 13){
					inputComplete();
				} else if(e.keyCode === 27){
					document.querySelector('.note-lnb-temp-ul').remove();
					document.removeEventListener("click", inputCompleteByBlur);
				}
			});

			document.removeEventListener("click", inputCompleteByBlur);
			document.addEventListener("click", inputCompleteByBlur);
		} else {
			document.querySelector('.note-lnb-temp-ul').remove();
		}
	}

    function drawLnbTitle(targetLayout){
//        let _frag = document.createDocumentFragment()
//        let _titleBar = document.createElement('div')
//        _titleBar.setAttribute('id','note-title-bar')
//        _frag.appendChild(_titleBar)
//
//        let _title__col = document.createElement('div')
//        
//        let _title_logo = document.createElement('div')
//        _title_logo.setAttribute('class','note__header__logo')
//        let _logo = document.createElement('img')
//        _logo.setAttribute('id','note-app-logo')
//        _logo.src = "./res/note/TeeNote_logo.svg"
//    	_title_logo.appendChild(_logo)
//        let _appName = document.createElement('span')
//        _appName.setAttribute('id','note-app-name')
//        _appName.innerText = 'TeeNote'
//    	_title__col.appendChild(_title_logo)
//    	_title__col.appendChild(_appName)
//    	_title__col.setAttribute('class','note__header__column')
//        
//        _titleBar.appendChild(_title__col)
////        _titleBar.appendChild(_title__btn_col)
//        
//        targetLayout.appendChild(_frag)
    }
    
    function addCommonButtonContainer( targetLayout ){
    	
    	let _dividerContainer = document.createElement('div')
    	let _divider = document.createElement('div')
    	let _title__btn_col = document.createElement('div')
        let _expandButton = document.createElement('span')
        
        _expandButton.classList.add('note-expand-btn')
        _dividerContainer.setAttribute("class","note-divider__column")
        _divider.setAttribute("class","note-lnb-divider")
        _dividerContainer.appendChild(_divider)
        
        let defaultIcon = null;
        defaultIcon = noteLnbMenu.isExpanded() ? 'icon-work_collapse' : 'icon-work_expand'; 
        _expandButton.classList.add(defaultIcon)
        _expandButton.onclick=function(e){
        	switchExpand(e);
        }
		
		// hover시 여백 필요해서 span tag 안에 img tag 넣음
		let _closeButton = document.createElement('span');
        _closeButton.classList.add('note-title-close-btn');
		
		let _closeImg = document.createElement('img');
		_closeImg.classList.add('note-lnb_cancel-img');
		_closeImg.setAttribute("src", "./res/note/ts_cancel@3x.png");

		_closeButton.appendChild(_closeImg);
        _closeButton.onclick=function(){
        	onCloseButtonClick('note',function(){
				noteLnbMenu.setIsExpanded(false);
        	})
        }
        
        _title__btn_col.appendChild(_expandButton)
        _title__btn_col.appendChild(_closeButton)
        _title__btn_col.setAttribute('class','note__header__column')
        
        targetLayout.appendChild(_dividerContainer)
        targetLayout.appendChild(_title__btn_col)
	}
	
	function renderLnbIconArrow(targetData, targetNode) {
		let _ulFolder = document.createElement("span");
		let _ul = targetNode.closest("ul");

		_ulFolder.setAttribute("class", "note-lnb-ul-folder");
		
		if(foldedChapterList.indexOf(targetData.id) === -1){
			_ulFolder.classList.add("icon-arrow_up");
		} else {
			_ulFolder.classList.add("icon-arrow_down");
			_ul.classList.add("folded");
		}
		_ulFolder.addEventListener("click", function(e) {
			let _ul = e.currentTarget.closest("ul");
			if (_ul.classList.contains("folded")) {
				this.classList.add("icon-arrow_up");
				this.classList.remove("icon-arrow_down");
				_ul.classList.remove("folded");
				foldedChapterList.splice(foldedChapterList.indexOf(_ul.getAttribute('chapter-id')),1);
			} else {
				this.classList.remove("icon-arrow_up");
				this.classList.add("icon-arrow_down");
				_ul.classList.add("folded");
				foldedChapterList.push(_ul.getAttribute('chapter-id'))
			}
		});

		targetNode.appendChild(_ulFolder);

		return new Promise((res,rej) => {
			res(targetNode);
		});
	}

    async function renderChapters(targetDIV, chapterList) {
		TNoteUtil.removeChildrenNodes(targetDIV);
        for(let i=0; i<chapterList.length; i++){
            await renderChapter(targetDIV, chapterList[i], 'chapter')
		}
		return Promise.resolve("");
    }
    
    async function renderChapter(targetDIV, targetData, type) {
		let isDefault = targetData["type"] === "notebook" ? "" : "default";

		let chapterItem = 
		`<ul class="note-lnb-ul chapter" chapter-id="${targetData.id}">
			<div class="note-lnb-ul-div ${isDefault}" chapter-id="${targetData.id}" type="container">
				<span class="note-lnb-ul-icon">
					<span class="note-lnb-chapter-color" style="color:${targetData.color};background-color:${targetData.color}"></span>
				</span>
				<span class="note-lnb-ul-text">
					<span class="note-chapter-text">${targetData.text}</span>
					<span class="note-lnb-chapter-contextmenu icon-ellipsis_vertical_small"></span>
				</span>
			</div>
		</ul>`;
		let frag = document.createElement('template');
		frag.insertAdjacentHTML('beforeend', chapterItem);
	
		// 그린 후 이벤트 바인딩하기
		let contextMenu = frag.querySelector(".note-lnb-chapter-contextmenu");
		contextMenu.addEventListener('click', TNoteRenderer.renderContextMenu);
		if (targetData.children.length > 0) {
			renderLnbIconArrow(targetData, frag.querySelector(".note-lnb-ul-div"))
		}

		await addChildren(frag.firstElementChild, targetData.children, "chapter");
			
		frag.firstElementChild.querySelector('.note-lnb-ul-text')
			.addEventListener("click", function(e) {
				let targetChapterId = e.currentTarget.closest("[chapter-id]").getAttribute("chapter-id");
				TNoteSearch.clickLnbChapterEvent(e,targetChapterId);
			});
				
		targetDIV.appendChild(frag.firstElementChild);
	}
	
    function renderTag(targetDIV, targetData, type) {
		makeUL("태그", targetData, type)
			.then((temp) => {
				targetDIV.appendChild(temp);
			})
//        addChildren(temp, targetData, type);
    }
    function renderSharedPage(targetDIV, targetData) {
		TNoteUtil.removeChildrenNodes(targetDIV);
    	for(let i=0; i<targetData.length; i++){
    		targetData[i].id = targetData[i].note_id;
    		targetData[i].text = targetData[i].note_title;
    	}
		makeUL("공유 받은 페이지", targetData, 'shared')
			.then((temp) => {
				addChildren(temp, targetData, 'shared');
			})
			.then((temp) => {
				targetDIV.appendChild(temp);
			})
    }
    function renderSharedChapter(targetDIV, targetData) {
		makeUL("공유 받은 챕터", targetData, 'shared')
			.then((temp) => {
				addChildren(temp, targetData, 'shared')
			})
			.then((temp) => {
				targetDIV.appendChild(temp);
			})
	}   
	// chapter일 때는 따로 그림(renderChapter에서)
    function makeUL(menuName, targetData, type) {
		// type === "chapter"인 경우 뺐음
        let _ul = document.createElement("ul");
        _ul.setAttribute("class", "note-lnb-ul");
        _ul.classList.add(type);
        _ulDiv = document.createElement("div");
        _ulDiv.setAttribute("class", "note-lnb-ul-div");
        _ulDiv.setAttribute('chapter-id',targetData.id);
        _ulDiv.setAttribute('type',"container");
        if(type === "tag") _ulDiv.setAttribute('draggable',false);
        // _ulDiv.setAttribute('id',menuName)

        // ul-icon
        let _ulIcon = document.createElement("span");
        _ulIcon.classList.add("note-lnb-ul-icon");
        switch (type) {
            case "tag":
            	_ul.setAttribute('id',"tag");
                let _tagIcon = document.createElement("span");
                let _tagImage = document.createElement("img");
                _tagImage.src = './res/note/tag.svg'
                _ulIcon.appendChild(_tagImage);
                break;
            case "shared":
//            	_ul.setAttribute('content-id',targetData.id);
                let _sharedIcon = document.createElement("span");
                _sharedIcon.setAttribute("class", "icon-shared");
                _ulIcon.appendChild(_sharedIcon);
                break;
            default:
        }

        // ul-text
        let _ulText = document.createElement("span");
        _ulText.setAttribute("class", "note-lnb-ul-text");
        let _contextMenu = addContextMenu('note-lnb-chapter-contextmenu', TNoteRenderer.renderContextMenu)
        
        let innerText = document.createElement("span");
        innerText.classList.add('note-chapter-text');
        innerText.innerText = menuName;
        _ulText.appendChild(innerText);
        _ulText.appendChild(_contextMenu);

        _ulDiv.appendChild(_ulIcon);
        _ulDiv.appendChild(_ulText);
//        if ( targetData.length>0 || targetData.children && targetData.children.length > 0) {
        if ( type === 'shared' && targetData.length > 0) {
            renderLnbIconArrow(targetData, _ulDiv);
        }
        if ( type === "tag" ){
        	_ul.addEventListener("click", function() {
        		TNoteController.selectTagMenu()
        		_ul.classList.add('selected')
        		let liList = document.querySelectorAll(".note-lnb-li.selected");
                for (let i = 0; i < liList.length; i++) {
                    liList[i].classList.remove("selected");
                }
        	})
        }
        _ul.appendChild(_ulDiv);

        return Promise.resolve(_ul);
    }
    function addNode(parentId, data, type){
    	let frag = document.createDocumentFragment();
    	renderNode(data, frag, type); 
    	$('[chapter-id='+'"'+parentId+'"'+'] div')[0].after(frag)
    	noteDndUpdate();
    }
    function noteDndUpdate(){
    	// drag and drop 테스트
        // room/1//note 라고 가정
        $("div#note")
//            .addClass("dnd__container")
//            .attr("data-dnd-app", "note")
            .attr('type',"app"); 
        $("#note-chapter-cover .note-lnb-ul")
            .addClass("dnd__container")
            .attr("data-dnd-app", "note");
        $("#note-chapter-cover .note-lnb-li").addClass("dnd__item");
        $('.note-lnb-ul-div').addClass("dnd__item");
        $('#note-tag-cover .note-lnb-ul')
        	.addClass("dnd__container")
        	.attr("data-dnd-app", "note");
        dnd.update();
        // dragEnd 이벤트
        let dnd_containers = document.querySelectorAll('#note .dnd__container .dnd__item')
        dnd_containers.forEach(function(dnd_container){ 
        	dnd_container.addEventListener("dragend", function(e) {
        		NoteDragDrop.onDragEnd(e);
        	})
        });
        
    }
    function addChildren(parent, data, type) {
    	
        let frag = document.createDocumentFragment();
        for (let i = 0; i < data.length; i++) {
            renderNode(data[i], frag, type);
        }
        parent.appendChild(frag);

        if (type === "chapter") {
            let _newPage = document.createElement("span");
            let _newPageBlock = document.createElement("p");
            let _newPageText = document.createElement("span");
            let _emptyPage = document.createElement('div');
            _emptyPage.setAttribute('id','emptyDiv');
            _newPage.setAttribute("class", "note-new-page");
            _newPageBlock.setAttribute("class","note-new-page-block");
            _newPageText.setAttribute("class", "note-new-page-text");
            _newPageText.innerText = "+ 새 페이지 추가";
            _newPageBlock.addEventListener("click", function(e) {                                                              // note create!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            	let parentId = e.target.closest('ul.chapter').getAttribute('chapter-id')
            	TNoteController.createNote( "제목 없음" , parentId );
            });
            _newPage.appendChild(_newPageBlock);
            _newPageBlock.appendChild(_newPageText);
            parent.appendChild(_emptyPage);
            parent.appendChild(_newPage);
        }
        // drag and drop 테스트
        setTimeout(function() {
        	noteDndUpdate();
		}, 1);
		return Promise.resolve(parent)
    }
    
    function liNodeClickEventHandler(e,_li){
    	if(!e.target.classList.contains('icon-ellipsis_vertical_small')){
    		if (e.ctrlKey) {
        		if(e.currentTarget.classList.contains('selected')){
        			e.currentTarget.classList.remove('selected');
        		} else {
        			_li.classList.add("selected");
        		}
        		let selectPageList = document.querySelectorAll(".note-lnb-li.selected");
        		let selectedSearchPageList = document.querySelectorAll(".search-page-div.selected");
        		if(selectPageList.length>0) {
        			TNoteData.setMultiplePageData(selectPageList);
        		} else if(selectedSearchPageList.length>0) {
        			TNoteData.setMultiplePageData(selectedSearchPageList);
        		} else{
        			TNoteData.setMultiplePageData([]);
        		}
            } else {
                let liList = document.querySelectorAll(".note-lnb-li.selected");
            	for (let i = 0; i < liList.length; i++) {
                    liList[i].classList.remove("selected");
                }
            	let selectedSearchPageList = document.querySelectorAll(".search-page-div.selected");
            	for(let i=0; i< selectedSearchPageList.length; i++){
            		selectedSearchPageList[i].classList.remove("selected");
            	}
                let ulTag = document.querySelectorAll(".note-lnb-ul.selected");;
                for (let i = 0; i < ulTag.length; i++) {
                	ulTag[i].classList.remove("selected");
				}
				// 챕터 선택 style 제거
				let selectedChapterList = document.querySelectorAll(".note-lnb_selected-chapter");
				for (let i=0;i<selectedChapterList.length;i++) {
					selectedChapterList[i].classList.remove("note-lnb_selected-chapter");
				}        
                TNoteData.setMultiplePageData([]);
                let clickId = _li.getAttribute("content-id")
                TNoteController.selectPageMenu(clickId)
                _li.classList.add("selected");
            }
    	}
    }

    function renderNode(noteData, frag, type) {
        //default layer
        let _li = document.createElement("li");
        _li.setAttribute('type',type)
        _li.addEventListener("mouseup", function(e){
        	liNodeClickEventHandler(e,_li);
        })
        _li.setAttribute("class", "note-lnb-li");
        _li.setAttribute("data-index", li_cnt++);
        _li.setAttribute("content-id", noteData.id);
        frag.appendChild(_li);

        let _icon = document.createElement("span");
        _icon.setAttribute("class", "note-lnb-li-icon");

        let _textCover = document.createElement("span");
        _textCover.classList.add("note-lnb-li-text-cover");
//        _textCover.addEventListener("click", function() {
//        	let clickId = _li.getAttribute("content-id")
//            TNoteController.selectPageMenu(clickId)
//        });
        let _text = document.createElement("a");
        _text.setAttribute("class", "note-lnb-li-text");
        _text.innerText = noteData.text;
//        if(type === 'tag'){
//        	_text.addEventListener("click", function() {
//            	let clickId = _li.getAttribute("content-id")
////                TNoteController.selectPageMenu(clickId)
//            	// TODO
//            	// tagSelect 함수
//            });
//        } else {
//        	_text.addEventListener("click", function() {
////            	let clickId = _li.getAttribute("content-id")
////                TNoteController.selectPageMenu(clickId)
//            });
//        }
        
        let _contextMenu = addContextMenu('note-lnb-page-contextmenu',TNoteRenderer.renderContextMenu);
        _textCover.appendChild(_text);
        _textCover.appendChild(_contextMenu)
        _li.appendChild(_icon);
        _li.appendChild(_textCover);
    }    

    function getRandomColor() {
//        var letters = "0123456789ABCDEF";
//        var color = "#";
//        for (var i = 0; i < 6; i++) {
//            color += letters[Math.floor(Math.random() * 16)];
//        }
//        return color;
    	// colorList, usedColor
    	let colorIndex = Math.floor(Math.random()*11); // 0~10의 랜덤 정수를 만든다
    	let targetColor = '#000000';
    	if(usedColor[colorIndex] === false){
    		usedColor[colorIndex] = true;
    		targetColor = colorList[colorIndex];
    	} else {
    		if(usedColor.indexOf(false)=== -1) usedColor = [false,false,false,false,false,false,false,false,false,false,false];
    		targetColor = getRandomColor();
    	}
    	return targetColor;
    	
    }
    
    function addContextMenu(className, clickEventHandler){
    	let _contextMenu = document.createElement('span');
    	_contextMenu.classList.add(className);
    	_contextMenu.classList.add('icon-ellipsis_vertical_small');
    	_contextMenu.addEventListener('click', clickEventHandler)
    	
    	return _contextMenu;
	}

    function changeChapterName(oldName){
    	let _ulText = document.getElementById('chapter-name-textfield');
    	let input = _ulText.value;
		//noteId, contents, parentId
		let _regBlank = /^\s+|\s+$/g;
		if( _ulText.value.replace( _regBlank, '' ) == "" ) {
			TeeAlarm.open({title: '',content: '이름을 입력해주세요.',cancelButtonText:"확인",onCancelButtonClicked: function(){ TeeAlarm.close(); _ulText.focus(); }});
			_ulText.classList.add('exception');
			_ulText.blur();
		} else if(TNoteData.checkDuplicateChapter(input) && input !== oldName) {
			TeeAlarm.open({title: '중복된 이름이 있습니다.',content: '다른 이름을 입력해주세요.',cancelButtonText:"확인",onCancelButtonClicked: function(){ TeeAlarm.close(); _ulText.focus();}});
			_ulText.classList.add('exception');
			_ulText.blur();
		} else {
			TNoteServiceCaller.noteBookUpdate(getNameChangeTargetId(), input, "", function(){
        		TNoteController.initChapterList()
        	})            				
		}
    }
    function changePageName(targetPageId, targetInputTag, afterCallback=null){
		let tempData;
		let noteData;
		let noteType;
		let temp1 = document.querySelector('div[content-id="'+ targetPageId + '"]');
		let temp2 = document.querySelector('li[content-id="'+ targetPageId + '"]');
		// .note-lnb-ul, lnb에서 수정시
		if(temp1 && temp1.parentNode.classList.contains('chapter')){
			tempData = TNoteServiceCaller.noteInfoList(targetPageId);
			noteType='chapter';
		} 
		// editor에서 title 수정시
		else if (temp2 && temp2.parentNode.classList.contains('chapter')) {
			tempData = TNoteServiceCaller.noteInfoList(targetPageId);
			noteType='chapter';
		} else {
			TeeAlarm.open({title: '알림', content: "서비스 제공 예정입니다."});
			TNoteController.initSharedMenu()
			return;
//			tempData = TNoteServiceCaller.shareNoteInfoList(targetPageId); 			// 임시로 형태만 짜둠 아직 서비스콜 없음
//			noteType='shared';
		}
		noteData = tempData[0];
		if(noteData.is_edit){
			TeeAlarm.open({title: '알림', content: "다른 사용자가 해당 페이지를 수정 중 입니다.",cancelButtonText:"확인",
				onCancelButtonClicked: function() { 
					TeeAlarm.close(); 
					targetInputTag.focus();
				}
			});
			TNoteController.initChapterList() // 이거 수정하기
		} else {
			let input = targetInputTag.value;
			if(input.trim() === ""){
				input = TNoteEditor.getTitleFromContent(noteData.note_content);
			} 
			TNoteController.noteTitleUpdate(targetPageId, input, noteData.parent_notebook, noteType)
		}
		// (SJ) 추가
		if (afterCallback) {
			afterCallback();
		}
    }
    function setNameChangeTargetId(id){
    	nameChangeTargetId = id;
    }
    function getNameChangeTargetId(){
    	return nameChangeTargetId;
    }
    function myIsExpanded(){
    	let curURL = window.location.href;
    	let lastIndex = curURL.split('/').length-1;
    	if(curURL.split('/')[lastIndex].indexOf('note')===0){ // expand 된 상황
    		isExpanded = true;
    		return true;
    	} else {
    		isExpanded = false;
    		return false;
    	}
	}
	
	function setIsExpanded(expanding) {
		if (expanding) {
			isExpanded = true;
		} else {
			isExpanded = false;
		}
	}
    
    return {
        //initLnbMenu:initLnbMenu,
        isExpanded:function(){
        	return isExpanded;
		},
		// isExpand 값 갱신하고 lnb에 확장/닫기 버튼 flex/none 갱신
		setIsExpanded: setIsExpanded,
		getRandomColor:getRandomColor,
		changePageName:changePageName,
        myIsExpanded:myIsExpanded,
        setExpandedIcon:setExpandedIcon,
        setEditorVisibility:setEditorVisibility,
        setLnbVisibility:setLnbVisibility,
        setFoldingVisibility:setFoldingVisibility,
        setTagVisibility:setTagVisibility,
        setEditorTitleContainer:setEditorTitleContainer,
        addCommonButtonContainer:addCommonButtonContainer,
        switchExpand:switchExpand,
        switchEditorExpand:switchEditorExpand,
        hotfixFunction:hotfixFunction,
        renderChapters:renderChapters,
        renderTag:renderTag,
        renderSharedPage:renderSharedPage,
        renderSharedChapter:renderSharedChapter,
        addNode:addNode,
        selectPageAfterUpdate:function(){ // 임시로 추가함. 그려져 있던 노트 삭제 시에 lnbMenu 최상단에 위치한 노트를 그리도록 한다. 임시임시 개임시
        	/*
        	 *  만약에 보고 있던 페이지를 삭제하면 에디터 영역엔 지워진 페이지가 그대로 남아있기 때문에 다른 화면을 보이도록 해야한다.
        	 *  그렇지 않으면 수정 버튼으로 수정을 시도한다거나 원치 않는 상황이 발생할 수 있음.
        	 *  이 함수는 노트 앱이 확장된 상태일 때만 불리면 된다.
        	 *  새로고침 역할을 하는 initChapterList 함수에서 호출하는 용도로 쓰려고 함
        	 *  2020-04-17 재웅
        	 * */
    		let renderedPageDiv = $('li[content-id="'+ document.getElementById('note-editor-container').getAttribute('rendered-page-id') + '"]')
        	if(renderedPageDiv.length>0){ // 에디터에 렌더되어있는 page가 Lnb Menu에 존재할 경우
        		renderedPageDiv[0].classList.add('selected');
        		TNoteController.selectPageMenu(renderedPageDiv[0].getAttribute('content-id'));
        	} else { // 에디터에 렌더되어있던 페이지가 지워졌을 경우
        		let targetPage = document.querySelector('li[content-id].note-lnb-li');
        		if(targetPage){ // 페이지가 한 개 이상 남아 있을 경우
        			let targetPageId = document.querySelector('li[content-id].note-lnb-li').getAttribute('content-id');
	        		document.querySelector('li[content-id="'+ targetPageId +'"]').classList.add('selected');
	        		setTimeout(function(){TNoteController.selectPageMenu(targetPageId)},1)
        		} else { // 페이지가 한 개도 없을 경우
        			document.getElementById('editorDIV').style.display = 'none';
        			document.getElementById('note-empty-div').style.display = 'block';
        		}
        	}
        	
        },
        getSelectedPageNodes: function() {
            return document.querySelectorAll(".chapter .selected");
        },
        noteDragStartHandler: function(e, data) {
            dnd.setData({ app: "note", note_id: e.target.getAttribute("content-id"), note_title: e.target.innerText, chapter_id: e.target.getAttribute("chapter-id") });
        },

        noteDropHandler: function(e, data) {
            console.log(data);
        },

        noteDragOverHandler: function(e, data) {
            // if(dnd.getData().app === "talk"){
            //     dnd.setData({none: true});
            // }
        },

        noteDragEnterHandler: function(e, data) {
            // console.log(data);
        },

        noteDragLeaveHandler: function(e, data) {
            // console.log(data);
		},
		// lnb click 이벤트
		clickNewChapterButtonEvent:clickNewChapterButtonEvent,
		clickRenameContextMenu:clickRenameContextMenu,

		// search 관련
		liNodeClickEventHandler:liNodeClickEventHandler
    };
})();
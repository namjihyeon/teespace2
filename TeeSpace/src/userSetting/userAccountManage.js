
Top.Controller.create('userAccountManageLayoutLogic', {
init : function(event, widget) {	
	//국제전화번호 노드 연결
	const data = COUNTRY_CODES.map(value => {
		return {
			text: `${value.code} ${value.name}`,
			value: value.code
		}
	});
	
	
	
	
	
		let _USER_ID = userManager.getLoginUserId();
		if(!userManager.isBtoc()) {
			Top.Dom.selectById("LinearLayout31_1_save_manage").setProperties({"display":"none"});
			Top.Dom.selectById("LinearLayout154_manage_save_manage").setProperties({"display":"none"});
			Top.Dom.selectById("LinearLayout154_2_save_manage").setProperties({"display":"none"});
			Top.Dom.selectById("LinearLayout154_3_save_manage").setProperties({"display":"none"});
			Top.Dom.selectById("LinearLayout154_4_save_manage").setProperties({"display":"none"});
			Top.Dom.selectById("LinearLayout31_b2b_org_phone").setProperties({"visible" : "visible"})
			tds('btobEmailLayout').setProperties({"visible":"visible"})
			tds('btobCompanyLayout').setProperties({"visible":"visible"})
			tds('btobCompanyLayout3').setProperties({"visible":"visible"})
			tds('LinearLayout31_4_save_manage').setProperties({"display":"none"})			
			tds('LinearLayout_b2b_org_NationalCode').setProperties({"visible":"visible"})
		}
			
					$.ajax({
				          url: _workspace.url+"Users/BothProfile", //Service Object
				          type: 'GET',
				          dataType: 'json', 
				          crossDomain: true,
				          contentType: 'application/json; charset=utf-8',
				          xhrFields: {

				              withCredentials: true

				            },
				            data: JSON.stringify({
						          "dto": {
						            "USER_ID": _USER_ID,
						          }
						        }),
						   success : function(result){
							   
							   ///////////////////////////////////////////////////b2c
								if(userManager.isBtoc()){
									
									 if(result.dto.USER_NICK){
										   Top.Dom.selectById("TextView93_save_nick_manage").setText(result.dto.USER_NICK) //별명 설정
										   tds('userAccountTitleNick').setText(result.dto.USER_NICK + " 님")
									   }else{
										   tds('userAccountTitleNick').setText(result.dto.USER_LOGIN_ID + " 님")
										   Top.Dom.selectById("TextView93_save_nick_manage").setText(result.dto.USER_LOGIN_ID)
									   }
									 Top.Dom.selectById("TextView93_save_mail_manage").setText(result.dto.USER_EMAIL) // 이메일 주소 설정					
									 
									   //국가 코드 번호
									   if(result.dto.USER_PHONE){ 					  
										   let pe = result.dto.USER_PHONE //핸드폰 번호 설정 
										   let nCode = result.dto.NATIONAL_CODE
										   Top.Dom.selectById("TextView93_save_mobile_manage").setText(nCode+pe)
								
									   }else{
										   Top.Dom.selectById("TextView93_save_mobile_manage").setText("—")
									   }
								}
								/////////////////////////////////////////////////////b2b
								else{
									
									
									var NcodeArray = data.filter(function(element, index, array){
									if(!result.dto.NATIONAL_CODE){
										return data.valueOf()[196].text
									}
									if(element.value === result.dto.NATIONAL_CODE)    
										return element.text 
									});
									
									if(!result.dto.NATIONAL_CODE){
										tds('TextView93_b2b_org_NationalCode').setText(data.valueOf()[196].text)
									}else{
										tds('TextView93_b2b_org_NationalCode').setText(NcodeArray[0].text)

									}
																			
								
									
									 tds('userAccountTitleNick').setText(result.dto.USER_NAME + " 님")//이름설정
									tds('btobViewMailTextView').setText(result.dto.USER_EMAIL)
								
									   				  
									   if(result.dto.USER_COM_NUM){
										   Top.Dom.selectById("TextView93_b2b_org_phone").setText(result.dto.USER_COM_NUM)// 회사전화 설정
										   }else{
											   Top.Dom.selectById("TextView93_b2b_org_phone").setText("—")
										   }
									  
									//직위/직책/ 소속회사/부서
									 if(result.dto.USER_JOB === null){
										   var btobjob = "—"
									   }else{
										   btobjob = result.dto.USER_JOB;
									   }
									   if(result.dto.USER_POSITION === null){
										   var btobPosition = "—"
									   }else{
										   btobPosition = result.dto.USER_POSITION
									   }
									   
									   tds('btobCompanyTextview_save_manage').setText(result.dto.COMPANY_CODE +" / "+ result.dto.ORG_NAME)
									   tds('btobJobTextview_save_manage').setText(btobjob+" / "+btobPosition)
									
									   
									   //국가 코드 번호
									   if(result.dto.USER_PHONE){ 					  
										   let pe = result.dto.USER_PHONE //핸드폰 번호 설정 
										   let nCode = result.dto.NATIONAL_CODE
										   Top.Dom.selectById("TextView93_save_mobile_manage").setText(pe)
								
									   }else{
										   Top.Dom.selectById("TextView93_save_mobile_manage").setText("—")
									   }
									}	
							   
				
							   //////////////////////////////////////////////////b2b/b2c공용
							   if(result.dto.THUMB_PHOTO == null){//프로필 이미지	  		   	
			                       Top.Dom.selectById('userAccountManageImageButton').setSrc(userManager.getUserDefaultPhotoURL(userManager.getLoginUserId()));
			  		        	 }else{
			   		        		 Top.Dom.selectById("userAccountManageImageButton").setSrc(userManager.getUserPhoto(userManager.getLoginUserId(),'medium', result.dto.THUMB_PHOTO))		
			  		        	 }
							   Top.Dom.selectById("userAccountTitleId").setText(result.dto.USER_LOGIN_ID) // 아이디 설정
							   
							   if(result.dto.USER_BIRTH){
							    	  Top.Dom.selectById("TextView93_save_birth_manage").setText(result.dto.USER_BIRTH)     //생년월일 설정
								   }else{
									   Top.Dom.selectById("TextView93_save_birth_manage").setText("—")
								   }
							   
							  
							   
							   //etc
							   let etc = JSON.parse(result.dto.USER_ETC)
							   if(!result.dto.USER_ETC){
								   Top.Dom.selectById("TextView93_save_sex_manage").setText("—")	
								   Top.Dom.selectById("TextView93_save_job_manage").setText("—")
								   Top.Dom.selectById("TextView93_save_group_manage").setText("—")
								   Top.Dom.selectById("TextView_save_info_agree_manage").setText("—")
								   Top.Dom.selectById("TextView_save_event_agree_manage").setText("—")
							   }else{
								   if(etc.sex){
									   if(etc.sex == "남자")  Top.Dom.selectById("TextView93_save_sex_manage").setText("남자")							//성별 설정
									   else if(etc.sex == "여자") Top.Dom.selectById("TextView93_save_sex_manage").setText("여자")	
								   }
								   else{Top.Dom.selectById("TextView93_save_sex_manage").setText("—")	}
								   
								   if(etc.job) Top.Dom.selectById("TextView93_save_job_manage").setText(etc.job)
								   else{Top.Dom.selectById("TextView93_save_job_manage").setText("—")	}
								   																										//직업 설정
								   if(etc.depart) Top.Dom.selectById("TextView93_save_group_manage").setText(etc.depart)
								      else{Top.Dom.selectById("TextView93_save_group_manage").setText("—")	}								//소속 설정
								   
								   if(etc.personal){
									   if(etc.personal == "agree") Top.Dom.selectById("TextView_save_info_agree_manage").setText("동의함")
									   else{Top.Dom.selectById("TextView_save_info_agree_manage").setText("동의하지 않음")}
								   	}
								   else{Top.Dom.selectById("TextView_save_info_agree_manage").setText("—")	}		
								   
								   
								   if(etc.adokay){
									   if(etc.adokay == "agree") Top.Dom.selectById("TextView_save_event_agree_manage").setText("동의함")
									   else{Top.Dom.selectById("TextView_save_event_agree_manage").setText("동의하지 않음")}
								   	}
								   else{Top.Dom.selectById("TextView_save_event_agree_manage").setText("—")	}		
							   }
							   //상세주소 설정
							   var detailAddress;
							   if(result.dto.USER_ADDRESS_DETAIL === null || result.dto.USER_ADDRESS_DETAIL.length === 0 ){detailAddress = "" }
							   else{detailAddress = result.dto.USER_ADDRESS_DETAIL}
							  
							   if(result.dto.USER_ADDRESS != null) {
								   if(result.dto.USER_ADDRESS.indexOf(undefined) === -1){
									   Top.Dom.selectById("TextView93_save_address_manage").setText(result.dto.USER_ADDRESS.split(" ")[0]) 			//주소 설정
									   Top.Dom.selectById("TextView93_save_address_1_manage").setText(result.dto.USER_ADDRESS.split(" ")[1]);
									   let rad = "";
									   for(var k=2; k<result.dto.USER_ADDRESS.split(" ").length;k++){
										   if(k==2) rad += result.dto.USER_ADDRESS.split(" ")[k];
										   else{ rad += " "+result.dto.USER_ADDRESS.split(" ")[k]}
									   }
									   Top.Dom.selectById("TextView93_save_address_2_manage").setText(rad+detailAddress);
								   }else if(result.dto.USER_ADDRESS.indexOf(undefined) != 0){ 
									   // 시도 정보 잇음																					
									   Top.Dom.selectById("TextView93_save_address_manage").setText(result.dto.USER_ADDRESS.split(" ")[0]) 
									   if(result.dto.USER_ADDRESS.split(" ")[1]) {
										   let ind =Top.Dom.selectById("TextView93_save_address_manage").getProperties("selected-index")+1
										   Top.Dom.selectById("TextView93_save_address_1_manage").setProperties({"nodes": "area"+ind, "disabled":"false"})
										   Top.Dom.selectById("TextView93_save_address_1_manage").setText(result.dto.USER_ADDRESS.split(" ")[1]);
									   }
									   if(result.dto.USER_ADDRESS.split(" ")[2]) Top.Dom.selectById("TextView93_save_address_2_manage").setText(result.dto.USER_ADDRESS.split(" ")[2]);
								   }
								   else{Top.Dom.selectById("TextView93_save_address_manage").setText("—")	}		
							   } else{Top.Dom.selectById("TextView93_save_address_manage").setText("—")}
							    
							   
								
							   
						   }
					});
			
			
			
			

		
		
		
		
	},
	 to_fix_page : function(event, widget) {
			let idp_save =Top.Dom.selectById("userAccountTitleId").getText();
			var pwp_save = Top.Dom.selectById("TextField361_pwd_manage").getText();
			if(!userManager.isBtoc()){
				idp_save = Top.Dom.selectById("userAccountTitleId").getText();
			}
			
			$.ajax({
	            url: _workspace.url+'Users/User?action=AuthCheck', //Service Object
	            type: 'POST',
	            dataType: 'json',
	            crossDomain: true,
	            contentType: 'application/json; charset=utf-8',
	            xhrFields: {
	                withCredentials: true
	              },
	            data: JSON.stringify({
	              "dto": {
	                "USER_LOGIN_ID": idp_save.trim(),
	                "USER_PW": SHA256(idp_save.trim()+pwp_save.trim())
	              }
	            }),
	            success: function(result) {
	            	if(result.dto.RESULT_CD == "RST0001"){
	            		Top.Dom.selectById("Button336_save_manage").setProperties({"disabled":"false"});
	            		tds('Button336_save_manage').removeClass('signup-disabled')
	        			tds('Button336_save_manage').addClass('solid')
	            		Top.Dom.selectById("TextField361_pwd_manage").removeClass("alert");
	            		Top.Dom.selectById("TextField361_pwd_manage").addClass("solid");
	            		Top.Dom.selectById("Icon93_pwd_manage").setProperties({"visible":"none"});
	            		Top.Dom.selectById("userAccountManageContentLayout").src("userAccountFixLayout.html"+verCsp()); 
	            	}
	            	else{
	                	Top.Dom.selectById("Button336_save_manage").setProperties({"disabled":"true"})
	                	tds('Button336_save_manage').removeClass('solid')
	                	tds('Button336_save_manage').addClass('signup-disabled')
	    	        	Top.Dom.selectById("TextField361_pwd_manage").removeClass("solid");
	        			Top.Dom.selectById("TextField361_pwd_manage").addClass("alert");
	    	        	Top.Dom.selectById("Icon93_pwd_manage").setProperties({"visible":"visible"});
	    	        	$('#Icon93_pwd_manage.icon-chart_error.linear-child-horizontal').css({"visibility":"hidden"})
	    	        	$('#TextField361_pwd_manage .top-textfield-icon').addClass('account-alert')
	    	        	setTimeout(function() {
	    					Top.Dom.selectById("CurPw2Popover").open();
	    				}, 100);
	            		
	            	}
		        	
		        },
		        error: function(error) {    	
		    		Top.Dom.selectById("Button336_save_manage").setProperties({"disabled":"true"})
		    		tds('Button336_save_manage').removeClass('solid')
	               	tds('Button336_save_manage').addClass('signup-disabled')
		        	Top.Dom.selectById("TextField361_pwd_manage").removeClass("solid");
	    			Top.Dom.selectById("TextField361_pwd_manage").addClass("alert");
		        	Top.Dom.selectById("Icon93_pwd_manage").setProperties({"visible":"visible"});
		        	$('#Icon93_pwd_manage.icon-chart_error.linear-child-horizontal').css({"visibility":"hidden"})
		        	$('#TextField361_pwd_manage .top-textfield-icon').addClass('account-alert')
		        	setTimeout(function() {
						Top.Dom.selectById("CurPw2Popover").open();
					}, 100);
		        	
		    
		        }
				});		
			
		},
	//비밀번호 유효성 검사 userauthcheck
	pwd_check_main : function(event, widget) {		
		tds('TextField361_pwd_manage').removeClass('alert')
		$('#TextField361_pwd_manage .top-textfield-icon').removeClass('account-alert')
		tds('Icon93_pwd_manage').setProperties({"visible":"none"})
		
		
		let idp_save =Top.Dom.selectById("userAccountTitleId").getText();
		var pwp_save = Top.Dom.selectById("TextField361_pwd_manage").getText();
		var k_2 = new Array(2);
		
		if(pwp_save.length >= 9 && pwp_save.length <= 20){
			k_2[0]=1;
		}
		else {}
		if((pattern_num.test(pwp_save)) && (pattern_engs.test(pwp_save)) && (pattern_spc2.test(pwp_save))){
			k_2[1]=1;
		}
		else if((pattern_num.test(pwp_save)) && (pattern_engs.test(pwp_save)) && (pattern_engb.test(pwp_save))){
			k_2[1]=1;
		}
		else if((pattern_engs.test(pwp_save)) && (pattern_spc2.test(pwp_save)) && (pattern_engb.test(pwp_save))){
			k_2[1]=1;
		}
		else{}
		if( k_2[0]==1 && k_2[1]==1){
			Top.Dom.selectById("Button336_save_manage").setProperties({"disabled":"false"});
			tds('Button336_save_manage').removeClass('signup-disabled')
			tds('Button336_save_manage').addClass('solid')
		}else{
			Top.Dom.selectById("Button336_save_manage").setProperties({"disabled":"true" })
			tds('Button336_save_manage').removeClass('solid')
			tds('Button336_save_manage').addClass('signup-disabled')
		}
	}, 
	//텍스트필드 아이콘 
	iconclick_main : function(event, widget) {
			let fieldid = widget.id;
			if(Top.Dom.selectById(fieldid).getProperties("password")){
				Top.Dom.selectById(fieldid).setProperties({"icon": "icon-ic_password_show", "password":"false"});// 보이기모드
			}else{
				Top.Dom.selectById(fieldid).setProperties({"icon": "icon-ic_password_hide", "password":"true"});//숨기기 모드
			}
		}
});

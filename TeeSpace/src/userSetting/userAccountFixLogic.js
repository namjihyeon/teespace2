
Top.Controller.create('userAccountFixLayoutLogic', {
	init : function(event, widget){
		//계정 수정 시, 버튼 보이게
		tds('AccountBtnLayout').setProperties({"visible":"visible"})
		let _USER_ID = userManager.getLoginUserId(); 
		
		//국제전화번호 노드 연결
		const data = COUNTRY_CODES.map(value => {
			return {
				text: `${value.code} ${value.name}`,
				value: value.code
			}
		});
		
		//b2b일때 포함되는
		if(!userManager.isBtoc()) {
			Top.Dom.selectById("LinearLayout31_1").setProperties({"display":"none"});
			tds('btobFixEmailLayout').setProperties({"visible":"visible"})
			
			Top.Dom.selectById("LinearLayout154_1").setProperties({"display":"none"});
			Top.Dom.selectById("LinearLayout154_2").setProperties({"display":"none"});
			Top.Dom.selectById("LinearLayout154_3").setProperties({"display":"none"});
			Top.Dom.selectById("LinearLayout154_4").setProperties({"display":"none"});
			Top.Dom.selectById("LinearLayout252").setProperties({"display":"none"});
			Top.Dom.selectById("LinearLayout154_b2b").setProperties({"visible" : "visible"})
			tds('LinearLayout31_4').setProperties({"display":"none"})
			tds('btobCompanyFixLayout').setProperties({"visible":"visible"})
			tds('btobCompanyFixLayout3').setProperties({"visible":"visible"})
			tds('fixNationalMobileSelectBox').setProperties({"display":"none"})
			tds('TextView52').setProperties({"display":"none"})
			tds('fixLinearLayout_b2b_NationalCode').setProperties({"visible":"visible"})
			tds('fixNCSelectBox').setProperties({"nodes":data})
			
			
			
			
			
		}else{//b2c 일때만...
			
			tds('fixMobileTextField').setProperties({"layout-width":"10.63rem"})
			
			
		}
		//b2b b2c 모두 다 공통
		
		
		
		
		Top.Dom.selectById("SelectBox228").setProperties({"nodes" : jobs});
		Top.Dom.selectById("SelectBox228_1").setProperties({"nodes" : area0});
		//이메일 도메인 노드 연결
		Top.Dom.selectById('accountDomainSelectBox').setProperties({"nodes" : "signupDomains"});
		
		tds("fixNationalMobileSelectBox").setProperties({"nodes":data})
		
		
		$.ajax({
	          url: _workspace.url+"Users/BothProfile", //Service Object
	          type: 'GET',
	          dataType: 'json', 
	          crossDomain: true,
	          contentType: 'application/json; charset=utf-8',
	          xhrFields: {

	              withCredentials: true

	            },
	            data: JSON.stringify({
			          "dto": {
			            "USER_ID": _USER_ID,
			          }
			        }),
			   success : function(result){
				   if(result.dto.THUMB_PHOTO == null){//프로필 이미지	  		   	
                       Top.Dom.selectById('userAccountFixImageButton').setSrc(userManager.getUserDefaultPhotoURL(userManager.getLoginUserId()));
  		        	 }else{
   		        		 Top.Dom.selectById("userAccountFixImageButton").setSrc(userManager.getUserPhoto(userManager.getLoginUserId(),'medium', result.dto.THUMB_PHOTO))		
  		        	 }
				   
				   if(result.dto.USER_NICK){
					   Top.Dom.selectById("TextField106").setText(result.dto.USER_NICK) //별명 설정
					    tds('fixTitleNick').setText(result.dto.USER_NICK+ " 님")
				   }else{
					   Top.Dom.selectById("TextField106").setText(result.dto.USER_LOGIN_ID)
					   if(!userManager.isBtoc()){
						   tds('fixTitleNick').setText(result.dto.USER_NAME+ " 님")
					   }else{
						   tds('fixTitleNick').setText(result.dto.USER_LOGIN_ID+ " 님")
					   }
				   }
				 //별명 텍스트란에 글자 수 표기하는 함수
					$(function(){
					$('.top-textfield-root #TextField106').keyup(function(e){
						var nickLength = $(this).val();
						$('.top-textfield-root #TextField106').css({"padding-right": "50px"})
				        $('#TextField106 .top-textfield-icon').html(nickLength.length + '/20');
				        $('#TextField106 .top-textfield-icon').css({"display":"inline","font-size":"0.69rem","position":"absolute","top":"0.4rem","right":"0.7rem","color":"#C5C5C8"})
				      });
				      $('.top-textfield-root #TextField106').keyup();
				});
				   Top.Dom.selectById("TextField106_1").setText(result.dto.USER_BIRTH) //생년월일 설정
				 
				   tds('fixTitleId').setText(result.dto.USER_LOGIN_ID)
				   //이메일 도메인 주소
				   for(let mailLeng=0; mailLeng<Top.Dom.selectById("accountDomainSelectBox").getProperties("nodes").length;mailLeng++){
					   
					   if(result.dto.USER_EMAIL.split("@")[1] !==  Top.Dom.selectById("accountDomainSelectBox").getProperties("nodes")[mailLeng]){
						   Top.Dom.selectById("accountDomainSelectBox").select("직접입력")
						   Top.Dom.selectById("TextField_account_mail").setText(result.dto.USER_EMAIL.split("@")[0]) // 이메일 주소 설정
						   Top.Dom.selectById("accountDomainTextField").setText(result.dto.USER_EMAIL.split("@")[1])
						  
					   }
					   else{
						   Top.Dom.selectById("accountDomainSelectBox").select(Top.Dom.selectById("accountDomainSelectBox").getProperties("nodes")[mailLeng])
						   Top.Dom.selectById("TextField_account_mail").setText(result.dto.USER_EMAIL.split("@")[0])
						   Top.Dom.selectById("accountDomainTextField").setText(result.dto.USER_EMAIL.split("@")[1])
						
						   break;
					   }
					   
					   
				   }
				   
				   tds('btobFixMailTextView').setText(result.dto.USER_EMAIL)
				   /////////////////////////////////////////////////회사 전화번호
				   if(result.dto.USER_COM_NUM){
					   let pe_com = result.dto.USER_COM_NUM //핸드폰 번호 설정
					   Top.Dom.selectById("TextField206_b2b").setText(pe_com)
				   }else{
					   Top.Dom.selectById("TextField206_b2b").setText("")
				   }
				   ///////////////////////////////////////////////// 국제코드까지
				   ///////////////////////////////////////////////// 휴대폰 번호 
				  
					   let pe = result.dto.USER_PHONE
					   Top.Dom.selectById("fixMobileTextField").setText(pe)
					   if(!userManager.isBtoc()){
						   if(!result.dto.NATIONAL_CODE){
							   tds('fixNCSelectBox').select("+82 South Korea")
						   }else{
							   tds('fixNCSelectBox').select(result.dto.NATIONAL_CODE)
						   }
					   }else{
						   tds('fixNationalMobileSelectBox').select(result.dto.NATIONAL_CODE)
					   }
					
				   
				   
				   //b2b 관련 회사 정보들
				   tds('btobCompanyTextviewFix').setText(result.dto.COMPANY_CODE +" / "+ result.dto.ORG_NAME)
				   if(result.dto.USER_JOB === null){
					   var btobjob = "—"
				   }else{
					   btobjob = result.dto.USER_JOB;
				   }
				   if(result.dto.USER_POSITION === null){
					   var btobPosition = "—"
				   }else{
					   btobPosition = result.dto.USER_POSITION
				   }
				   tds('btobJobTextviewFix_manage').setText(btobjob +" / "+ btobPosition )
				   let etc = JSON.parse(result.dto.USER_ETC)
				   if(result.dto.USER_ETC){
					   
					   if(etc.sex){
						   if(etc.sex == "남자") Top.Dom.selectById("RadioButton219").setChecked(true); 							//성별 설정
						   else if(etc.sex == "여자") Top.Dom.selectById("RadioButton219_1").setChecked(true);
					   }
					   if(etc.job) Top.Dom.selectById("SelectBox228").select(etc.job); 									//직업 설정
					   if(etc.depart) Top.Dom.selectById("TextField239").setText(etc.depart); 							//소속 설정
					   if(etc.personal){ if(etc.personal == "agree") Top.Dom.selectById("RadioButton305").setChecked(true);}
					   if(etc.adokay){ if(etc.adokay == "agree") Top.Dom.selectById("RadioButton305_2").setChecked(true);} 
				   }
				   let detailAddress;
				   if(!result.dto.USER_ADDRESS_DETAIL){
					   detailAddress = ""
				   }else{
					   detailAddress = result.dto.USER_ADDRESS_DETAIL
				   }
				   if(result.dto.USER_ADDRESS ===null){}
				   else if(result.dto.USER_ADDRESS.indexOf(undefined) === -1){
					   Top.Dom.selectById("SelectBox228_1").select(result.dto.USER_ADDRESS.split(" ")[0]) 			//주소 설정
					   Top.Dom.selectById("SelectBox228_1_1").select(result.dto.USER_ADDRESS.split(" ")[1]);
					   let rad = "";
					   for(var k=2; k<result.dto.USER_ADDRESS.split(" ").length;k++){
						   if(k==2) rad += result.dto.USER_ADDRESS.split(" ")[k];
						   else{ rad += " "+result.dto.USER_ADDRESS.split(" ")[k]}
					   }
					   Top.Dom.selectById("TextField239_1").setText(rad+detailAddress);
				   }else if(result.dto.USER_ADDRESS.indexOf(undefined) != 0){
					   // 시도 정보 잇음																					
					   Top.Dom.selectById("SelectBox228_1").select(result.dto.USER_ADDRESS.split(" ")[0]) 
					   if(result.dto.USER_ADDRESS.split(" ")[1]) {
						   let ind =Top.Dom.selectById("SelectBox228_1").getProperties("selected-index")+1
						   Top.Dom.selectById("SelectBox228_1_1").setProperties({"nodes": "area"+ind, "disabled":"false"})
						   Top.Dom.selectById("SelectBox228_1_1").select(result.dto.USER_ADDRESS.split(" ")[1]);
					   }
					   if(result.dto.USER_ADDRESS.split(" ")[2]) Top.Dom.selectById("TextField239_1").setText(result.dto.USER_ADDRESS.split(" ")[2]);
				   }
				 
				   }
		});
		
		
	},
	//휴대폰번호 focuout
	 fixmobileNumValid : function(event, widget) {

			var mobileNum = tds('fixMobileTextField').getText()
			if(mobileNum === null || mobileNum.length === 0){
				tds('Icon617_2').setProperties({"visible":"visible"})
				$('#Icon617_2.icon-chart_error.linear-child-horizontal').css({"visibility":"hidden"})
				tds('Icon832_1_6').setProperties({"visible":"none"})
				tds('fixMobileTextField').addClass('alert')
				setTimeout(function(){
					tds('MobileNullPopover').open()
				},100)
			}else{
				if(pattern_engs.test(mobileNum) || pattern_engb.test(mobileNum)	|| pattern_spc2.test(mobileNum) ||  pattern_kor.test(mobileNum)	||  pattern_blank.test(mobileNum))
					{
						tds("fixMobileTextField").addClass("alert")
						tds("Icon832_1_6").setProperties({"visible":"visible"})
						$('#Icon832_1_6.icon-chart_error.linear-child-horizontal').css({"visibility":"hidden"})
						tds('Icon617_2').setProperties({"visible":"none"})
						setTimeout(function(){
							Top.Dom.selectById("MobileValidPopover").open()
						}, 100)
						return false;
					}
				else{
					tds("fixMobileTextField").removeClass("alert")
					tds("Icon832_1_6").setProperties({"visible":"none"})
					tds('Icon617_2').setProperties({"visible":"none"})
				}
			}
			
	},
	onChangefixNationalMobile : function(event, widget) {
		//$('#fixNationalMobileSelectBox span.top-selectbox-container')[0].lastChild.textContent = $('#fixNationalMobileSelectBox .top-selectbox-container').text().split(' ')[0]
			$('#fixNationalMobileSelectBox span.top-selectbox-container')[0].lastChild.textContent = tds('fixNationalMobileSelectBox').getSelected().value
	},
	onChangefixLocalNumber : function(event, widget){
		$('#fixNCSelectBox span.top-selectbox-container')[0].lastChild.textContent = tds('fixNCSelectBox').getSelected().text
	},
	mobileKeyup : function(event, widget) {
		tds("fixMobileTextField").removeClass("alert")
		tds("Icon832_1_6").setProperties({"visible":"none"})
		tds('Icon617_2').setProperties({"visible":"none"})
	},
	
	 mail_textfied_check : function(event, widget) {
			
		tds('TextField_account_mail').removeClass('alert')
		tds('accountDomainTextField').removeClass('alert')
		tds('Icon_account_mail_error').setProperties({"visible":"none"})
		tds('EmailIdNullIcon').setProperties({"visible":"none"})
		tds('EmailIdValidlIcon').setProperties({"visible":"none"})
		tds('DomainNullIcon').setProperties({"visible":"none"})
		tds('Icon_account_mail_correct').setProperties({"visible":"none"})
		 
		 let mail = Top.Dom.selectById("TextField_account_mail").getText() +"@"+Top.Dom.selectById("accountDomainTextField").getText();
			
			
			if(checkMail(mail)){
				Top.Dom.selectById("Button_account_mail_check").setProperties({"disabled": "false"})
				tds('Button_account_mail_check').removeClass('signup-disabled')
				tds('Button_account_mail_check').addClass('solid')
				if(userManager.getLoginUserInfo().email === mail ){
					
					Top.Dom.selectById("Button_account_mail_check").setProperties({"disabled":"true"})
					tds('Button_account_mail_check').removeClass('solid')
					tds('Button_account_mail_check').addClass('signup-disabled')
					Top.Dom.selectById("Icon_account_mail_error").setProperties({"visible":"none"})
					Top.Dom.selectById("Icon_account_mail_correct").setProperties({"visible":"none"})
					Top.Dom.selectById("TextField_account_mail").removeClass("alert")
					Top.Dom.selectById("TextField_account_mail").addClass("solid")
		    	    return;
				}
				else{
					 Top.Dom.selectById("Button_account_mail_check").setProperties({"disabled":"false"})
					 tds('Button_account_mail_check').removeClass('signup-disabled')
					 tds('Button_account_mail_check').addClass('solid')
					}
			}else{
				Top.Dom.selectById("Button_account_mail_check").setProperties({"disabled": "true"})
				tds('Button_account_mail_check').removeClass('solid')
				tds('Button_account_mail_check').addClass('signup-disabled')
			}
	},
	//메일 도메인 셀렉박스
	onChangeDomain : function(event, widget) {
		tds('Icon_account_mail_error').setProperties({"visible":"none"})
		tds('Icon_account_mail_correct').setProperties({"visible":"none"})
		if(Top.Dom.selectById("accountDomainSelectBox").getSelectedText() === "직접입력"){
			Top.Dom.selectById("accountDomainTextField").setProperties({"disabled":"false"})
			tds('accountDomainTextField').removeClass('signup-disabled')
			tds('accountDomainTextField').addClass('solid')
			Top.Dom.selectById("accountDomainTextField").setText("")

		}
		else{
			Top.Dom.selectById("accountDomainTextField").setText(Top.Dom.selectById('accountDomainSelectBox').getSelectedText())
			Top.Dom.selectById("accountDomainTextField").setProperties({"disabled":"true"})
			tds('accountDomainTextField').removeClass('solid')
			tds('accountDomainTextField').addClass('signup-disabled')
			Top.Controller.get('userAccountFixLayoutLogic').mail_textfied_check()
			
		}		
	},
	//
	mail_check : function(event, widget) {
		let mail = Top.Dom.selectById("TextField_account_mail").getText() +"@"+Top.Dom.selectById("accountDomainTextField").getText()  ;
		$.ajax({
	        url: _workspace.url+"Users/EmailDuplication", //Service Object
	        type: 'GET',
	        dataType: 'json',
	        crossDomain: true,
	        contentType : "application/json",
	        xhrFields: {
	
	            withCredentials: true
	
	          },
	          data: JSON.stringify({
		          "dto": {
		            "USER_EMAIL" : mail
		          }
		        }),
	        success: function(result) {
	        	if(result.dto.USER_ID == "해당 이메일은 존재합니다."){
	        		Top.Dom.selectById("Icon_account_mail_correct").setProperties({"visible":"none"})
	        		Top.Dom.selectById("TextField_account_mail").removeClass("solid")
	        		Top.Dom.selectById("TextField_account_mail").addClass("alert")        		
	        		Top.Dom.selectById("Icon_account_mail_error").setProperties({"visible":"visible"})
	        		$('#Icon_account_mail_error.icon-chart_error.linear-child-horizontal').css({"visibility":"hidden"})
	        		setTimeout(function(){
	        			Top.Dom.selectById("EmailSameErrorPopover").open()
	        			}, 100);
	        		return false;
	        	}else if(result.dto.USER_ID == "해당 이메일은 등록되지 않았습니다."){
	        		Top.Dom.selectById("TextField_account_mail").removeClass("alert")
	        		Top.Dom.selectById("TextField_account_mail").addClass("solid")
	        		
	        		Top.Dom.selectById("Icon_account_mail_error").setProperties({"visible":"none"})
	        		Top.Dom.selectById("Icon_account_mail_correct").setProperties({"visible":"visible"})
	        		$('#Icon_account_mail_correct.icon-circle_check.linear-child-horizontal').css({"visibility":"hidden"})
	        		Top.Dom.selectById("Button_account_mail_check").setProperties({"disabled" : "true"})
	        		tds('Button_account_mail_check').removeClass('solid')
	        		tds('Button_account_mail_check').addClass('signup-disabled')
	        	
	        	  
	        	}	        	
	        },
	        error: function(error) {
	        }
      });
	
	}, nickFocusOut : function(event, widget) {
		let nickLeng = tds('TextField106').getText();
		if(nickLeng === null || nickLeng.length === 0){
			tds('Icon617').setProperties({"visible":"visible"})
			$('#Icon617.icon-chart_error.linear-child-horizontal').css({"visibility":"hidden"})
			tds('TextField106').removeClass('solid')
			tds('TextField106').addClass('alert')
			setTimeout(function(){
				tds('NoNickPopover').open()
			},100)
		}else{
			tds('Icon617').setProperties({"visible":"none"})
			tds('TextField106').removeClass('alert')
			tds('TextField106').addClass('solid')
		}
	},
	//본인 확인 이메일 FocusOut
	 ckmailFocusOut : function(event, widget) {
			let mail =  Top.Dom.selectById('TextField_account_mail').getText()+"@"+ Top.Dom.selectById("accountDomainTextField").getText();
			
			if(tds('TextField_account_mail').getText() === null || tds('TextField_account_mail').getText().length === 0 ){
				tds('TextField_account_mail').removeClass('soild')
				tds('TextField_account_mail').addClass('alert')
				tds('EmailIdNullIcon').setProperties({"visible":"visible"})
				$('#EmailIdNullIcon.icon-chart_error.linear-child-horizontal').css({"visibility":"hidden"})
				tds('Icon_account_mail_error').setProperties({"visible":"none"})
				tds('EmailIdValidlIcon').setProperties({"visible":"none"})
				tds('Icon_account_mail_correct').setProperties({"visible":"none"})
				tds('DomainNullIcon').setProperties({"visible":"none"})
				setTimeout(function(){
					tds('EmailNullPopover').open();
				},100)
			}
			else{
				tds('TextField_account_mail').removeClass('alert')
				tds('TextField_account_mail').addClass('solid')
				tds('accountDomainTextField').removeClass('alert')
				tds('accountDomainTextField').addClass('solid')
				tds('EmailIdNullIcon').setProperties({"visible":"none"})
				tds('EmailIdValidlIcon').setProperties({"visible":"none"})
				tds('DomainNullIcon').setProperties({"visible":"none"})
				tds('Icon_account_mail_error').setProperties({"visible":"none"})	
			}
				
		},
		domainFocusOut : function(event, widget) {
			let mail =  Top.Dom.selectById('TextField_account_mail').getText()+"@"+ Top.Dom.selectById("accountDomainTextField").getText();
			if(tds('accountDomainTextField').getText() === null || tds('accountDomainTextField').getText().length === 0 ){
				tds('accountDomainTextField').removeClass('solid')
				tds('accountDomainTextField').addClass('alert')
				tds('DomainNullIcon').setProperties({"visible":"visible"})
				$('#DomainNullIcon.icon-chart_error.linear-child-horizontal').css({"visibility":"hidden"})
				$('span#DomainNullIcon').css({"left":'-15rem'})
				$('span#DomainNullIcon').css({"top":'-0.3rem'})
				tds('EmailIdNullIcon').setProperties({"visible":"none"})
				tds('Icon_account_mail_error').setProperties({"visible":"none"})
				tds('EmailIdValidlIcon').setProperties({"visible":"none"})
				tds('Icon_account_mail_correct').setProperties({"visible":"none"})
				
				setTimeout(function(){
					tds('EmailNullPopover2').open();
				},100)
			}
			else{
				tds('TextField_account_mail').removeClass('alert')
				tds('TextField_account_mail').addClass('solid')
				tds('accountDomainTextField').removeClass('alert')
				tds('accountDomainTextField').addClass('solid')
				tds('EmailIdNullIcon').setProperties({"visible":"none"})
				tds('EmailIdValidlIcon').setProperties({"visible":"none"})
				tds('DomainNullIcon').setProperties({"visible":"none"})
				tds('Icon_account_mail_error').setProperties({"visible":"none"})

				if(!checkMail(mail)){
							tds('TextField_account_mail').removeClass('solid')
							tds('TextField_account_mail').addClass('alert')
							tds('EmailIdValidlIcon').setProperties({"visible":"visible"})
							$('#EmailIdValidlIcon.icon-chart_error.linear-child-horizontal').css({"visibility":"hidden"})
							tds('DomainNullIcon').setProperties({"visible":"none"})
							tds('EmailIdNullIcon').setProperties({"visible":"none"})
							tds('Icon_account_mail_error').setProperties({"visible":"none"})
							tds('Icon_account_mail_correct').setProperties({"visible":"none"})
							setTimeout(function(){
								tds('EmailValidPopover').open();
							},100)
						}	
				
				
			}
			
		},
		//주소 onChange
		onSelectBoxSet : function(event, widget) {
			let ind = Top.Dom.selectById(widget.id).getClickedIndex()+1;
			Top.Dom.selectById("SelectBox228_1_1").setProperties({"nodes": "area"+ind, "disabled": "false"});
		},
	
	 
});

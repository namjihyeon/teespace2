function resizeContents() {
		var remVal = screen.width / 1366 * 16
	    $("div#SettingMainLayout").height($(window).height()- (10.625 * remVal));
		}
Top.Controller.create('userSettingDialogLayoutLogic', {
	init : function(event, widget){
		//사이즈 조절하기 위해
		$(document).ready(function(){
		    $(window).resize(resizeContents);
		    resizeContents();
		});

		//비밀번호 변경 버튼으로 접근시
		this.pwFlag;
		if(this.pwFlag === 1){
			this.clickedTab = "security";
            tds('SettingRightLayout').src('userSecuritySetup.html'+ verCsp())
		tds('AccountBtnLayout').setProperties({"visible":"none"})
		tds("SafetyButton").removeClass("setting-solid");
		tds("SafetyButton").addClass("setting-act");
		}else{
		    this.clickedTab = "alarm";	
		    tds('SettingRightLayout').src('userAlarmSetup.html'+ verCsp())
		    tds("AlarmButton").removeClass("setting-solid");
		    tds("AlarmButton").addClass("setting-act");
		}
		
	},
	
	accountBtnClick : function(event, widget) {
		this.removeBtnClass();
		this.clickedTab = "account";
		tds('SettingRightLayout').src('userAccountManageLayout.html'+ verCsp())
		tds('AccountBtnLayout').setProperties({"visible":"none"})
		tds("AccountButton").removeClass("setting-solid");
		tds("AccountButton").addClass("setting-act");
	},
	AccountOkButtonSave : function(event, widget) {

		if(this.clickedTab == "account"){
			// 계정정보 변경 탭 클릭
			//계정설정 b2b 개정판
			var flag_mail = 0 ;
			var flag_mail_icon = 0;
			var flag_mail_check = 0;
			
			
			//메일유효성 검사 저장 버튼 누를 떄 검사하도록
			if(Top.Dom.selectById("TextField_account_mail").getText()+"@"+ Top.Dom.selectById("accountDomainTextField").getText() === null || !checkMail(Top.Dom.selectById("TextField_account_mail").getText()+"@"+ Top.Dom.selectById("accountDomainTextField").getText())){
				Top.Dom.selectById("TextField_account_mail").removeClass("solid")
				Top.Dom.selectById("TextField_account_mail").addClass("alert")
				
				Top.Dom.selectById("Icon_account_mail_error").setProperties({"visible":"visible"})		
				$('#Icon_account_mail_error.icon-chart_error.linear-child-horizontal').css({"visibility":"hidden"})
				Top.Dom.selectById("Icon_account_mail_correct").setProperties({"visible":"none"})
				setTimeout(function(){
					Top.Dom.selectById("EmailSameErrorPopover").open();
				} , 100);
				flag_mail = 1;
			}
			if(flag_mail == 0){
				Top.Dom.selectById("TextField_account_mail").removeClass("alert")
				Top.Dom.selectById("TextField_account_mail").addClass("solid")
			}
			
			//빨간 아이콘 보이면 저장 못하도록
			if(Top.Dom.selectById("Icon_account_mail_error").getVisible() === "visible"){
				Top.Dom.selectById("TextField_account_mail").removeClass("solid")
				Top.Dom.selectById("TextField_account_mail").addClass("alert")
				flag_mail_icon = 1;
			}
			//중복확인 눌러야 저장 가능하도록
			if(Top.Dom.selectById("Button_account_mail_check").getDisabled() === false){
				
				Top.Dom.selectById("Icon_account_mail_error").setProperties({"visible":"visible"})
				Top.Dom.selectById("Icon_account_mail_correct").setProperties({"visible":"none"})
				flag_mail_check = 1;
			}
			
					
		
			
			let _USER_ID = userManager.getLoginUserId();
			let eetc = new Object();
			////////////////////////////////////////////////////////////////B2C에서만
			if(userManager.isBtoc()){




				if(Top.Dom.selectById("RadioButton219").isChecked()) eetc.sex = "남자";
				else if(Top.Dom.selectById("RadioButton219_1").isChecked()) eetc.sex = "여자"; //성별
				if(Top.Dom.selectById("SelectBox228").getSelectedText()) eetc.job =Top.Dom.selectById("SelectBox228").getSelectedText(); //직업
				if(Top.Dom.selectById("TextField239").getText()) eetc.depart =Top.Dom.selectById("TextField239").getText(); //소속
			
				if(!Top.Dom.selectById("TextField106").getText()){ //별명 비움
					Top.Dom.selectById("TextField106").removeClass("solid")
					Top.Dom.selectById("TextField106").addClass("alert")
					Top.Dom.selectById("Icon617").setProperties({"visible":"visible"});
					$('#Icon617.icon-chart_error.linear-child-horizontal').css({"visibility":"hidden"})
					Top.Dom.selectById("TextField106").focus()
					setTimeout(function(){
						Top.Dom.selectById("NoNickPopover").open();
					} , 100);
					return;
				}
				Top.Dom.selectById("TextField106").removeClass("alert")
				Top.Dom.selectById("TextField106").addClass("solid")
				Top.Dom.selectById("Icon617").setProperties({"visible":"none"})
				
				
				var nickName = Top.Dom.selectById("TextField106").getText()
				var Ncode = tds('fixNationalMobileSelectBox').getSelected().value
				//생년월일 선택 사항																																	
				if(Top.Dom.selectById('TextField106_1').getText() === null || Top.Dom.selectById('TextField106_1').getText().length === 0 ){
					Top.Dom.selectById('TextField106_1').setText('');
				}else{
					if(pattern_engs.test(Top.Dom.selectById("TextField106_1").getText()) || pattern_engb.test(Top.Dom.selectById("TextField106_1").getText())
						|| pattern_spc2.test(Top.Dom.selectById("TextField106_1").getText()) ||  pattern_kor.test(Top.Dom.selectById("TextField106_1").getText()) 
						||  pattern_blank.test(Top.Dom.selectById("TextField106_1").getText()) || Top.Dom.selectById('TextField106_1').getText().length !== 6)
					{
						Top.Dom.selectById("TextField106_1").removeClass("solid")
						Top.Dom.selectById("TextField106_1").addClass("alert")
						Top.Dom.selectById("Icon617_1").setProperties({"visible":"visible"})
						$('#Icon617_1.icon-chart_error.linear-child-horizontal').css({"visibility":"hidden"})
						Top.Dom.selectById("TextField106_1").focus()
						setTimeout(function(){
							Top.Dom.selectById("BirthValidPopover").open()
						}, 100)
						return false;
						
					}else{
						Top.Dom.selectById("TextField106_1").removeClass("alert")
						Top.Dom.selectById("TextField106_1").addClass("solid")
						Top.Dom.selectById("Icon617_1").setProperties({"visible":"none"})
					}

				}
			
			}else{
				/////////////////////b2b
				nickName = null;
				Ncode = tds('fixNCSelectBox').getSelected().value
			}
			 
			
			if(Top.Dom.selectById("RadioButton305").isChecked()) eetc.personal = "agree"
			else eetc.personal = "disagree"
			if(Top.Dom.selectById("RadioButton305_2").isChecked()) eetc.adokay = "agree"
			else eetc.adokay = "disagree"	
			let wetc = JSON.stringify(eetc)
			
			
			
			//전화번호
			var flag1 = 0 ;
			var flag2 = 0;
			
			
			if(!pattern_num.test(Number(Top.Dom.selectById("fixMobileTextField").getText()))){ 
				Top.Dom.selectById("Icon832_1_6").setProperties({"visible":"visible"}); 
				$('#Icon832_1_6.icon-chart_error.linear-child-horizontal').css({"visibility":"hidden"})
				Top.Dom.selectById("fixMobileTextField").focus()
				setTimeout(function(){
					Top.Dom.selectById("fixMobileTextField").removeClass("solid")
					Top.Dom.selectById("fixMobileTextField").addClass("alert")
					Top.Dom.selectById("MobileValidPopover").open();
				} , 100);
				flag1 = 1;
			}
			if(Top.Dom.selectById("fixMobileTextField").getText() === null || Top.Dom.selectById("fixMobileTextField").getText().length === 0){
				Top.Dom.selectById("Icon617_2").setProperties({"visible":"visible"}); 
				$('#Icon617_2.icon-chart_error.linear-child-horizontal').css({"visibility":"hidden"})
				Top.Dom.selectById("fixMobileTextField").focus()
				setTimeout(function(){
					Top.Dom.selectById("fixMobileTextField").removeClass("solid")
					Top.Dom.selectById("fixMobileTextField").addClass("alert")
					Top.Dom.selectById("MobileNullPopover").open();
				} , 100);
				flag2 = 1;
			}
			if(flag1 == 0){
				Top.Dom.selectById("fixMobileTextField").removeClass("alert")
				Top.Dom.selectById("fixMobileTextField").addClass("solid")
			}
			if(flag2 == 0){
				Top.Dom.selectById("fixMobileTextField").removeClass("alert")
				Top.Dom.selectById("fixMobileTextField").addClass("solid")
			}

			if(flag1 || flag2 || flag_mail || flag_mail_icon || flag_mail_check){ return;}
			
			Top.Dom.selectById("Icon832_1_6").setProperties({"visible":"none"}); 
			//유효성 통과했으니 담기
			let pph= tds('fixMobileTextField').getText();
			
			//회사 전화번호
			let pph_com="";
			var flag1_com = 0 ;
			var flag2_com = 0 ;
			//회사번호 있을때
			if(Top.Dom.selectById("TextField206_b2b").getText()  != undefined){
			
			if(Top.Dom.selectById("TextField206_b2b").getText() == undefined){Top.Dom.selectById("TextField206_b2b").setText("")}
		
			
			
			if(!pattern_num.test(Number(Top.Dom.selectById("TextField206_b2b").getText())) ){ 
				Top.Dom.selectById("TextField206_b2b").removeClass("solid")
				Top.Dom.selectById("TextField206_b2b").addClass("alert")
				Top.Dom.selectById("Icon617_2_b2b").setProperties({"visible":"visible"}); 
				$('#Icon617_2_b2b.icon-chart_error.linear-child-horizontal').css({"visibility":"hidden"})
				
				setTimeout(function(){
					Top.Dom.selectById("NumberValidBtobPopover").open();
				} , 100)
				flag1_com = 1;
			}
		
			if(flag1_com == 0){
				Top.Dom.selectById("TextField206_b2b").removeClass("alert")
				Top.Dom.selectById("TextField206_b2b").addClass("solid")
			}
		
			
			if(flag1_com || flag_mail || flag_mail_icon || flag_mail_check){ return;}
			
			Top.Dom.selectById("Icon617_2_b2b").setProperties({"visible":"none"}); 
			
			
			if(Top.Dom.selectById("TextField206_b2b").getText()) pph_com = Top.Dom.selectById("TextField206_b2b").getText();
			
			}
//			//회사번호 없을 때
//			if(tds('TextField206_b2b').getText() === null || tds('TextField206_b2b').getText().length === 0){
//				Top.Dom.selectById("TextField206_b2b").removeClass("solid")
//				Top.Dom.selectById("TextField206_b2b").addClass("alert")
//				Top.Dom.selectById("IconNullNumb2b").setProperties({"visible":"visible"}); 
//				$('#IconNullNumb2b.icon-chart_error.linear-child-horizontal').css({"visibility":"hidden"})
//				
//				setTimeout(function(){
//					Top.Dom.selectById("NumberNullBtobPopover").open();
//				} , 100)
//				return false;
//			}
//			
			let addr = "";
			//프로필 이미지 담기
			var myimage = tds('userAccountFixImageButton').getSrc();
			if(myimage.indexOf(userManager.getUserDefaultPhotoURL(userManager.getLoginUserId())) === 0 ){
				myimage = null;
			}else{
				myimage = tds('userAccountFixImageButton').getSrc();
			}
			//상세주소
			var detailAdd = Top.Dom.selectById("TextField239_1").getText()
			$.ajax({
				url: _workspace.url+"Users/BothProfile?action=Put", //Service Object
				type: 'POST',
				dataType: 'json', 
				crossDomain: true,
				contentType: 'application/json',
				xhrFields: {

					withCredentials: true

					},
					data: JSON.stringify({
						"dto": {
							"PROFILE_PHOTO" : myimage,
							"USER_ID": _USER_ID,
							"USER_EMAIL" : Top.Dom.selectById("TextField_account_mail").getText()+"@"+Top.Dom.selectById("accountDomainTextField").getText(),
							"USER_NICK" : nickName,
							"USER_BIRTH" : Top.Dom.selectById("TextField106_1").getText(),
							"USER_PHONE" : pph,
							"USER_COM_NUM" : pph_com,
							"USER_ADDRESS" : Top.Dom.selectById("SelectBox228_1").getSelectedText() + " "+ Top.Dom.selectById("SelectBox228_1_1").getSelectedText(),
							"USER_ETC" : wetc,
							"NATIONAL_CODE" : Ncode,
							"USER_ADDRESS_DETAIL" : detailAdd
							
						}
						}),
				success: function(result) {

					try {
						var msg = '계정 정보가 저장되었습니다.';
						TeeToast.open({text: msg});
					} catch (e) {}
	//                 if(location.href.indexOf("landing")>-1){
	//                	 	if(Top.Dom.selectById("lending_fullName2")){
	//                	 	Top.Dom.selectById("lending_fullName2").setText(result.dto.USER_NICK)}
	//                		//Top.Dom.selectById("lending_fullName2").setText(loginUserJson.nick)}
	//		        	 }
					
					userManager.update()
					//편집모드 종료
					Top.Dom.selectById('userAccountFixLayout').src('userAccountManageLayout.html'+ verCsp());
					tds('AccountBtnLayout').setProperties({"visible":"none"})
					
				},
				error: function(error) { 
				try {
						var msg = '계정 정보 저장에 실패했습니다.';
						notiFeedback(msg);
					} catch (e) {}
				}
				});  
		}else if(this.clickedTab == "security"){
			// 로그인 및 보안 탭에서 변경사항 저장 버튼 클릭
			Top.Controller.get("PasswordChangeLayoutLogic").pwsave();
			//tds('AccountBtnLayout').setProperties({"visible":"none"})
		}
	}, AccountCancelButton : function(event, widget) {
		if(this.clickedTab == "security"){
			// 로그인 및 보안 탭일 때
			tds("PasswordSession").src("PasswordDefaultLayout.html" +verCsp());
			this.visibleBtn(false);
		}else if(this.clickedTab === "account"){
			tds('userAccountFixLayout').src('userAccountManageLayout.html'+ verCsp())
			tds('AccountBtnLayout').setProperties({"visible":"none"})
		}
		
	}, securityBtnClick : function(event, widget) {
		this.removeBtnClass();
		this.clickedTab = "security";
		tds('SettingRightLayout').src('userSecuritySetup.html'+ verCsp())
		tds('AccountBtnLayout').setProperties({"visible":"none"})
		tds("SafetyButton").removeClass("setting-solid");
		tds("SafetyButton").addClass("setting-act");
	}, alarmBtnClick : function(event, widget) {
		this.removeBtnClass();
		this.clickedTab = "alarm";
		tds('SettingRightLayout').src('userAlarmSetup.html'+ verCsp())
		tds('AccountBtnLayout').setProperties({"visible":"none"})
		tds("AlarmButton").removeClass("setting-solid");
		tds("AlarmButton").addClass("setting-act");
	}, visibleBtn : function(bool){
		if(bool == true){
			tds('AccountBtnLayout').setProperties({"visible":"visible"})
		}else{
			tds('AccountBtnLayout').setProperties({"visible":"none"})
		}	
	}, removeBtnClass : function(){
		if(this.clickedTab === "alarm"){
			tds("AlarmButton").removeClass("setting-act");
			tds("AlarmButton").addClass("setting-solid");
		}else if(this.clickedTab === "account"){
			tds("AccountButton").removeClass("setting-act");
			tds("AccountButton").addClass("setting-solid");
		}else if(this.clickedTab=== "security"){
			tds("SafetyButton").removeClass("setting-act");
			tds("SafetyButton").addClass("setting-solid");
		}else if(this.clickedTab === "general"){
			tds("GeneralButton").removeClass("setting-act");
			tds("GeneralButton").addClass("setting-solid");
		}

	}
	
	
	
});

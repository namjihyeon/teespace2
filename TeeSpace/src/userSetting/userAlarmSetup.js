var codelist = {"desktop_all" : "allToggle",
		"ttalk_all" : "TalkToggle","ttalk_showContents" : "TalkButton1", 
		"cloudmeeting_all" : "MeetingToggle","cloudmeeting_meetingstart": "MeetingButton1", "cloudmeeting_meetingend":"MeetingButton2",
		"tmail_all" : "MailToggle", "tschedule_all" : "CalendarToggle", "space_invite" : "SpaceToggle"};

function searchAlarm(value){
	let keyy = Object.keys(codelist)
	for(var i=0; i<keyy.length;i++){
		if(codelist[keyy[i]] == value){
			return keyy[i];
		}
	}
}
function setAlarm(temp) {
	$.ajax({
        url: _workspace.url+'Users/UserAlarm?action=SetPut', //Service Object
        type: 'POST',
        data: JSON.stringify({
        	"dto" :{
        		"dtos" : [temp]
        	  }
        }),
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        xhrFields: {	           withCredentials: true
      },
        crossDomain: true,
        success: function(result) {
        	$.ajax({
    	        url: _workspace.url+'Users/UserAlarmSet', //Service Object
    	        type: 'GET',
    	        data: JSON.stringify({
    	        	"dto" :{
    	        		"USER_ID" : userManager.getLoginUserId()
    	        	  }
    	        }),
    	        dataType: 'json',
    	        contentType: 'application/json; charset=utf-8',
    	        xhrFields: {

    	            withCredentials: true

    	          },
    	        crossDomain: true,
    	        contentType: 'application/json; charset=UTF-8',
    	        success: function(result) {
    	        	setUserAlarmConfig(result.dto.dtos); // NotificationManager.updateConfig()가 함수 안에 내장되어있음.
    	        }
        	})
        },
        error: function(error) {
            NotificationManager.updateConfig();
        }
    })
	
}
Top.Controller.create('userAlarmSetupLogic', {
	init: function(event, widget){
		
		//바탕화면 알림 허용 토글추가
		let tmt0 = $('<top-toggle id="allToggle" layout-width="2.63rem" layout-height="1.31rem" layout-vertical-alignment="middle" margin="0px 0px 0px 0px"  class ="alltoggle" tab-index="0" text="" text-decoration="" checked="true" check-position="left" title="" layout-tab-disabled="false" z-index = "2" class="linear-child-horizontal" actual-width="NaN" style="line-height: normal; float: right; outline:none;"></top-toggle>');
		tmt0.on('click',alarmAll)
		$('#userAlarmSetup').find('div#LinearLayout2388').prepend(tmt0);
		
		// 항목별 토글 추가
		let tmt1 = $('<top-toggle id="TalkToggle" layout-width="2.63rem" layout-height="1.31rem" layout-vertical-alignment="middle" margin="0.75rem 0px 0px 0px"  class ="alltoggle" tab-index="0" text="" text-decoration="" checked="true" check-position="left" title="" layout-tab-disabled="false" z-index = "2" class="linear-child-horizontal" actual-width="NaN" style="line-height: normal; float: right; outline:none;"></top-toggle>');
		tmt1.on('click',settingAll)
		$('#userAlarmSetup').find('div#TeeTalkAlarm').append(tmt1);
		
		let tmt2 = $('<top-toggle id="MeetingToggle" layout-width="2.63rem" layout-height="1.31rem" layout-vertical-alignment="middle" margin="0.75rem 0px 0px 0px"  class ="alltoggle" tab-index="0" text="" text-decoration="" checked="true" check-position="left" title="" layout-tab-disabled="false" z-index = "2" class="linear-child-horizontal" actual-width="NaN" style="line-height: normal; float: right; outline:none;"></top-toggle>');
		tmt2.on('click',settingAll)
		$('#userAlarmSetup').find('div#TeeMeetingAlarm').append(tmt2);
		
		let tmt3 = $('<top-toggle id="MailToggle" layout-width="2.63rem" layout-height="1.31rem" layout-vertical-alignment="middle" margin="0.75rem 0px 0px 0px"  class ="alltoggle" tab-index="0" text="" text-decoration="" checked="true" check-position="left" title="" layout-tab-disabled="false" z-index = "2" class="linear-child-horizontal" actual-width="NaN" style="line-height: normal; float: right; outline:none;"></top-toggle>');
		tmt3.on('click',settingAll)
		$('#userAlarmSetup').find('div#TeeMailAlarm').append(tmt3);
		
		let tmt4 = $('<top-toggle id="CalendarToggle" layout-width="2.63rem" layout-height="1.31rem" layout-vertical-alignment="middle" margin="0.75rem 0px 0px 0px"  class ="alltoggle" tab-index="0" text="" text-decoration="" checked="true" check-position="left" title="" layout-tab-disabled="false" z-index = "2" class="linear-child-horizontal" actual-width="NaN" style="line-height: normal; float: right; outline:none;"></top-toggle>');
		tmt4.on('click',settingAll)
		$('#userAlarmSetup').find('div#TeeCalendarAlarm').append(tmt4);
		
		let tmt5 = $('<top-toggle id="SpaceToggle" layout-width="2.63rem" layout-height="1.31rem" layout-vertical-alignment="middle" margin="0.75rem 0px 0px 0px"  class ="alltoggle" tab-index="0" text="" text-decoration="" checked="true" check-position="left" title="" layout-tab-disabled="false" z-index = "2" class="linear-child-horizontal" actual-width="NaN" style="line-height: normal; float: right; outline:none;"></top-toggle>');
		tmt5.on('click',settingAll)
		$('#userAlarmSetup').find('div#SpaceAlarm').append(tmt5);
		
		$.ajax({
	        url: _workspace.url+'Users/UserAlarmSet', //Service Object
	        type: 'GET',
	        data: JSON.stringify({
	        	"dto" :{
	        		"USER_ID" : userManager.getLoginUserId()
	        	  }
	        }),
	        dataType: 'json',
	        contentType: 'application/json; charset=utf-8',
	        xhrFields: {

	            withCredentials: true

	          },
	        crossDomain: true,
	        contentType: 'application/json; charset=UTF-8',
	        success: function(result) {
	        	setUserAlarmConfig(result.dto.dtos);
	        	if(result.dto.dtos.length>0){
	        		for(var i=0; i<result.dto.dtos.length; i++){
	        			let ccde = result.dto.dtos[i].CH_ALARM_CODE;
	        			tds(codelist[ccde]).setProperties({"checked":"false"});
	        		}
	        		// 전체 알람 체크
	        		let allCk = result.dto.dtos.filter(function(item){
	        			return item.CH_ALARM_CODE ==="desktop_all";
	        		});
	        		if(allCk.length>0){
	        			tds("LinearLayout24").setProperties({"visible": "none"});
	        		}
	        		let talkCk = result.dto.dtos.filter(function(item){
	        			return item.CH_ALARM_CODE ==="ttalk_all";
	        		});
	        		if(talkCk.length>0){
	        			tds("TalkAlarmOption").setProperties({"visible": "none"});
	        			tds("TeeTalkAlarm").setProperties({"layout-height" : "3.31rem"})
	        		}
	        		let meetCk = result.dto.dtos.filter(function(item){
	        			return item.CH_ALARM_CODE ==="cloudmeeting_all";
	        		});
	        		if(meetCk.length>0){
	        			tds("MeetingAlarmOption").setProperties({"visible": "none"});
	        			tds("TeeMeetingAlarm").setProperties({"layout-height" : "3.31rem"})
	        		}
	        	}
	        }
	          
		});
	},
	optioncheck : function(event, widget) {
		let userid = userManager.getLoginUserId();
		let wid = widget.id;
		let temp={}; temp.USER_ID = userid;
		let chcode = searchAlarm(wid);
		temp.CH_ALARM_CODE = chcode;
		temp.WS_ID = "all";
		if(Top.Dom.selectById(wid).getChecked()==false){
			temp.RESULT_MSG = "delete";
			setAlarm(temp)
		}else{
			temp.RESULT_MSG = "insert";
			setAlarm(temp)
		}
	}
});


function alarmAll(event, widget){
	let userid = userManager.getLoginUserId();
	if(tds("allToggle").getState()){
		tds("LinearLayout24").setProperties({"visible": "visible"});
		let temp={};
		temp.WS_ID = "all";
		temp.USER_ID = userid;  temp.CH_ALARM_CODE = "desktop_all",temp.RESULT_MSG = "delete";
		setAlarm(temp);
	}else{
		tds("LinearLayout24").setProperties({"visible": "none"});
		let temp={};
		temp.WS_ID = "all";
		temp.USER_ID = userid; temp.CH_ALARM_CODE = "desktop_all",temp.RESULT_MSG = "insert";
		setAlarm(temp);
	}
}

function settingAll(event, widget){
	let userid = userManager.getLoginUserId();
	let wid = event.currentTarget.id;
	if(tds(wid).getState()){
		if(wid == "TalkToggle"){
			tds("TalkAlarmOption").setProperties({"visible": "visible"});
			//tds("TeeTalkAlarm").setProperties({"layout-height" : "4.07rem"})
			let temp={};
			temp.WS_ID = "all";
			temp.USER_ID = userid;  temp.CH_ALARM_CODE = "ttalk_all",temp.RESULT_MSG = "delete";
			setAlarm(temp);
		}else if(wid == "MeetingToggle"){
			tds("MeetingAlarmOption").setProperties({"visible": "visible"});
			//tds("TeeMeetingAlarm").setProperties({"layout-height" : "4.07rem"})
			let temp={};
			temp.WS_ID = "all";
			temp.USER_ID = userid;  temp.CH_ALARM_CODE = "cloudmeeting_all",temp.RESULT_MSG = "delete";
			setAlarm(temp);
		}else if(wid== "MailToggle"){
			let temp={};
			temp.WS_ID = "all";
			temp.USER_ID = userid;  temp.CH_ALARM_CODE = "tmail_all",temp.RESULT_MSG = "delete";
			setAlarm(temp);
		}else if(wid== "CalendarToggle"){
			let temp={};
			temp.WS_ID = "all";
			temp.USER_ID = userid;  temp.CH_ALARM_CODE = "tschedule_all",temp.RESULT_MSG = "delete";
			setAlarm(temp);
		}else if(wid == "SpaceToggle"){
			let temp={};
			temp.USER_ID = userid;  temp.CH_ALARM_CODE = "space_invite",temp.RESULT_MSG = "delete";
			temp.WS_ID = "all";
			setAlarm(temp);
		}
	}else{
		if(wid == "TalkToggle"){
			tds("TalkAlarmOption").setProperties({"visible": "none"});
			//tds("TeeTalkAlarm").setProperties({"layout-height" : "3.31rem"})
			let temp={};
			temp.WS_ID = "all";
			temp.USER_ID = userid;  temp.CH_ALARM_CODE = "ttalk_all",temp.RESULT_MSG = "insert";
			setAlarm(temp);
		}else if(wid == "MeetingToggle"){
			tds("MeetingAlarmOption").setProperties({"visible": "none"});
			//tds("TeeMeetingAlarm").setProperties({"layout-height" : "3.31rem"})
			let temp={};
			temp.WS_ID = "all";
			temp.USER_ID = userid;  temp.CH_ALARM_CODE = "cloudmeeting_all",temp.RESULT_MSG = "insert";
			setAlarm(temp);
		}else if(wid== "MailToggle"){
			let temp={};
			temp.WS_ID = "all";
			temp.USER_ID = userid;  temp.CH_ALARM_CODE = "tmail_all",temp.RESULT_MSG = "insert";
			setAlarm(temp);
		}else if(wid== "CalendarToggle"){
			let temp={};
			temp.WS_ID = "all";
			temp.USER_ID = userid;  temp.CH_ALARM_CODE = "tschedule_all",temp.RESULT_MSG = "insert";
			setAlarm(temp);
		}else if(wid == "SpaceToggle"){
			let temp={};
			temp.WS_ID = "all";
			temp.USER_ID = userid;  temp.CH_ALARM_CODE = "space_invite",temp.RESULT_MSG = "insert";
			setAlarm(temp);
		}
	}
	
}
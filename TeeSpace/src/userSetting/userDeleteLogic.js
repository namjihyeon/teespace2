
Top.Controller.create('userDeleteDefaultLayoutLogic', {
	init:function(event, widget){
		tds("LinearLayout8").src("userDeleteTermsLayout.html"+ verCsp());
	}
});

Top.Controller.create('userDeleteTermsLayoutLogic', {
    init:function(event,widget){
        let termtext = '<p>● 서비스에 대한 이용약관 및 개인정보 제공, 활용에 관한 동의가 모두 철회되며,<br>사용하고 계신 아이디(<span style="display:inline-block; color:#6C56E5">' +userManager.getLoginUserInfo().USER_LOGIN_ID +'</span>)는 재사용 및 복구가 불가능합니다.</p>';
        tds("LinearLayout2243").append(termtext);
    },
	agreeCk : function(event, widget) {
        if(tds("CheckBox62").getState()){
            tds("Button80").setProperties({"disable":"false"});
        }else{
            tds("Button80").setProperties({"disable":"true"});
        }
	}, openTerm : function(event, widget) {
        window.open("/#!/terms-and-conditions")
	}, openPersonal : function(event, widget) {
        window.open("/#!/privacy-policy")
	}, nextPage : function(event, widget) {
        tds("LinearLayout8").src("userDeleteAuthLayout.html"+ verCsp());
    }
});

Top.Controller.create('userDeleteAuthLayoutLogic', {
    init:function(event,widget){
        tds("TextView109").setText(userManager.getLoginUserInfo().USER_LOGIN_ID);

    },
	iconClick : function(event, widget) {
        let wid = widget.id;
		if(Top.Dom.selectById(wid).getProperties("password")){
			Top.Dom.selectById(wid).setProperties({"icon": "icon-ic_password_show", "password":"false"});
		}else{
			Top.Dom.selectById(wid).setProperties({"icon": "icon-ic_password_hide", "password":"true"});
		}
	}, pwCheck : function(event, widget) {
        let wid = tds(widget.id).getText();
        if(wid.length>8){
            tds("Button129_1").setProperties({"disabled":"false"})
        }else{
            tds("Button129_1").setProperties({"disabled":"true"});
        }
	}, back : function(event, widget) {
        tds("LinearLayout8").src("userDeleteTermsLayout.html"+ verCsp());
	}, go : function(event, widget) {
	}
	
	
	
});



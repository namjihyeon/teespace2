Top.Controller.create('userSecuritySetupLogic', {
	init : function(event, widget) {
		Top.Dom.selectById("PasswordSession").src("PasswordDefaultLayout.html" +verCsp());
	}
});


Top.Controller.create('PasswordChangeLayoutLogic', {
	init : function(event, widget) {
		this.aa = 0;
		this.ab = 0;
		this.ac = 0;
		this.curpwck = 0;
		tds("PwErrorIcon2_1").setProperties({"font-size":"1.63rem"});
		tds("PwErrorIcon2_2").setProperties({"font-size":"1.63rem"});
	},
	authCheck : function(event, widget){
		var pwp = Top.Dom.selectById("TextField284").getText();
		var idp = JSON.parse(sessionStorage.getItem("userInfo")).USER_LOGIN_ID;
		if(pwp){
			$.ajax({
	            url: _workspace.url+'Users/User?action=AuthCheck', //Service Object
	            type: 'POST',
	            dataType: 'json',
	            crossDomain: true,
	            contentType: 'application/json; charset=utf-8',
	            xhrFields: {
	                withCredentials: true
	              },
	            data: JSON.stringify({
	              "dto": {
	                "USER_LOGIN_ID": idp.trim(),
	                "USER_PW": SHA256(idp.trim()+pwp.trim()),
	              }
	            }),
	            success: function(result) {
	            	if(result.dto.RESULT_CD == "RST0001"){
	            		// 맞앗음
						Top.Controller.get("PasswordChangeLayoutLogic").ac = 1;
						tds("PwErrorIcon1").setProperties({"visible":"none"});
						tds('TextField284').removeClass('alert')
						$('#TextField284 .top-textfield-icon').removeClass('userSettingPwd-alert')  
						Top.Controller.get("PasswordChangeLayoutLogic").curpwck =1;	
	            	}else{
	            		// 틀림
						Top.Controller.get("PasswordChangeLayoutLogic").ac = 0;
						tds('TextField284').addClass('alert')
						$('#TextField284 .top-textfield-icon').addClass('userSettingPwd-alert')  	
						tds("PwErrorIcon1").setProperties({"visible":"visible"});
						$('#PwErrorIcon1.icon-chart_error.linear-child-horizontal').css({"visibility":"hidden"})
						Top.Controller.get("PasswordChangeLayoutLogic").curpwck =0;
						setTimeout(function(){
							tds('CurPw2Popover').open()
						},100)
	            	}
	            }
			});
		}
		
	}, iconClick : function(event, widget) {
		let wid = widget.id;
		if(Top.Dom.selectById(wid).getProperties("password")){
			Top.Dom.selectById(wid).setProperties({"icon": "icon-ic_password_show", "password":"false"});
		}else{
			Top.Dom.selectById(wid).setProperties({"icon": "icon-ic_password_hide", "password":"true"});
		}
	}, pwFormCheck : function(event, widget) {
		tds("PwErrorIcon2").setProperties({"visible":"none"});
		tds("PwErrorIcon2_1").setProperties({"visible":"none"});
		tds("PwErrorIcon2_2").setProperties({"visible":"none"});
		tds('TextField284_1').removeClass('alert')
		$('#TextField284_1 .top-textfield-icon').removeClass('userSettingPwd-alert')  	
		
		let text = Top.Dom.selectById(widget.id).getText();
		// 비번 길이 체크
		if(!text) return;
		if(text.length >= 9 && text.length <= 20){
			Top.Dom.selectById("Icon325").setProperties({"text-color":"#6c56e5"});
			this.aa=1;
		}else {
			Top.Dom.selectById("Icon325").setProperties({"text-color":"#8D8D8D"});
			this.aa=0;		
		}
		// 문자 체크
		if ( (pattern_engb.test(text) || pattern_engs.test(text)) && pattern_num.test(text) && pattern_spc.test(text) ){
			Top.Dom.selectById("Icon325_1").setProperties({"text-color":"#6c56e5"});
			this.ab=1;
		}else{
			Top.Dom.selectById("Icon325_1").setProperties({"text-color":"#8D8D8D"});
			this.ab=0;
		}
		// 한글잇는지 체크
		for(var i=0;i<text.length;i++){
			if(is_hangul(text[i])) {Top.Dom.selectById("Icon325_1").setProperties({"text-color":"#8D8D8D"}); this.ab=0;}
		}
		
		
	},newpwCheck :function(){
		if(tds("TextField284_1").getText()){
			if(this.aa*this.ab == 1 && (tds("TextField284_1").getText() != tds("TextField284").getText())){
				tds("PwErrorIcon2").setProperties({"visible":"none"});
				tds("PwErrorIcon2_1").setProperties({"visible":"none"});
				tds("PwErrorIcon2_2").setProperties({"visible":"none"});
				tds('TextField284_1').removeClass('alert')
				$('#TextField284_1 .top-textfield-icon').removeClass('userSettingPwd-alert')  	
			}else if(tds("TextField284_1").getText() == tds("TextField284").getText() && this.curpwck == 1) {
				tds("PwErrorIcon2_1").setProperties({"visible":"none"});
				tds("PwErrorIcon2_2").setProperties({"visible":"none"});
				tds("PwErrorIcon2").setProperties({"visible":"visible"});
				tds('TextField284_1').addClass('alert')
				$('#TextField284_1 .top-textfield-icon').addClass('userSettingPwd-alert')  	
				$('#PwErrorIcon2.icon-chart_error.linear-child-horizontal').css({"visibility":"hidden"})
				setTimeout(function(){
					tds('NoUsePwdPopover').open()
				},100)
				
			}else if(this.aa == 0){
				tds("PwErrorIcon2").setProperties({"visible":"none"});
				tds("PwErrorIcon2_2").setProperties({"visible":"none"});
				tds("PwErrorIcon2_1").setProperties({"visible":"visible"});
				tds('TextField284_1').addClass('alert')
				$('#TextField284_1 .top-textfield-icon').addClass('userSettingPwd-alert')  	
			
				$('#PwErrorIcon2_1.icon-chart_error.linear-child-horizontal').css({"visibility":"hidden"})
				setTimeout(function(){
					tds('PwdLengthErrorPopover').open()
				},100)
			}else if(this.ab==0){
				tds("PwErrorIcon2_1").setProperties({"visible":"none"});
				tds("PwErrorIcon2").setProperties({"visible":"none"});
				tds("PwErrorIcon2_2").setProperties({"visible":"visible"});
				tds('TextField284_1').addClass('alert')
				$('#TextField284_1 .top-textfield-icon').addClass('userSettingPwd-alert')  	
			
				$('#PwErrorIcon2_2.icon-chart_error.linear-child-horizontal').css({"visibility":"hidden"})
				setTimeout(function(){
					tds('PwdValidErrorPopover').open()
				},100)
			}else{
				tds("PwErrorIcon2").setProperties({"visible":"none"});
				tds("PwErrorIcon2_1").setProperties({"visible":"none"});
				tds("PwErrorIcon2_2").setProperties({"visible":"none"});
				tds('TextField284_1').removeClass('alert')
				$('#TextField284_1 .top-textfield-icon').removeClass('userSettingPwd-alert')  	
			}
		}
	}, pwsave : function(){
		let idp_b = userManager.getLoginUserInfo().USER_LOGIN_ID;
		var pwp_b = tds("TextField284_1").getText();
		let tmpemail = userManager.getLoginUserInfo().email;
		if(this.ac != 1){
			// 기존 비밀번호 맞는지 체크
			tds('TextField284').addClass('alert')
			$('#TextField284 .top-textfield-icon').addClass('userSettingPwd-alert')  	
			tds("PwErrorIcon1").setProperties({"visible":"visible"});
			$('#PwErrorIcon1.icon-chart_error.linear-child-horizontal').css({"visibility":"hidden"})
			setTimeout(function(){
				tds('CurPw2Popover').open()
			},100)
			return;
		}
		if(this.ab * this.aa == 0){
			// 새 비밀번호 형식 체크
			if(this.aa == 0){
				tds("PwErrorIcon2_2").setProperties({"visible":"none"});
				tds("PwErrorIcon2").setProperties({"visible":"none"});
				tds("PwErrorIcon2_1").setProperties({"visible":"visible"});
				tds('TextField284_1').addClass('alert')
				$('#TextField284_1 .top-textfield-icon').addClass('userSettingPwd-alert')  	
			
				$('#PwErrorIcon2.icon-chart_error.linear-child-horizontal').css({"visibility":"hidden"})
				setTimeout(function(){
					tds('PwdLengthErrorPopover').open()
				},100)
			}else{
				tds("PwErrorIcon2_1").setProperties({"visible":"none"});
				tds("PwErrorIcon2").setProperties({"visible":"none"});
				tds("PwErrorIcon2_2").setProperties({"visible":"visible"});
				tds('TextField284_1').addClass('alert')
				$('#TextField284_1 .top-textfield-icon').addClass('userSettingPwd-alert')  	
			
				$('#PwErrorIcon2.icon-chart_error.linear-child-horizontal').css({"visibility":"hidden"})
				setTimeout(function(){
					tds('PwdValidErrorPopover').open()
				},100)
			}
			return;
		}
		if(pwp_b == tds("TextField284").getText()){
			// 기존 비밀번호와 같은지 체크
			tds("PwErrorIcon2_1").setProperties({"visible":"none"});
			tds("PwErrorIcon2_2").setProperties({"visible":"none"});
			tds("PwErrorIcon2").setProperties({"visible":"visible"});
			tds('TextField284_1').addClass('alert')
			$('#TextField284_1 .top-textfield-icon').addClass('userSettingPwd-alert')  	
		
			$('#PwErrorIcon2.icon-chart_error.linear-child-horizontal').css({"visibility":"hidden"})
			setTimeout(function(){
				tds('NoUsePwdPopover').open()
			},100)
			return;
		}
		
		if(pwp_b != tds("TextField284_2").getText()){
			// 새 비밀번호와 새 비밀번호 확인이 같은지 체크
			tds("PwErrorIcon3").setProperties({"visible":"visible"});
			tds('TextField284_2').addClass('alert')
			$('#TextField284_2 .top-textfield-icon').addClass('userSettingPwd-alert')  	
			tds("PwErrorIcon3").setProperties({"visible":"visible"});
			$('#PwErrorIcon3.icon-chart_error.linear-child-horizontal').css({"visibility":"hidden"})
			setTimeout(function(){
				tds('DifferentPasswordErrorPopover').open()
			},100)
			return;
		}

		//  성공시 레이아웃 변경 다 적용
		tds("PwErrorIcon2").setProperties({"visible":"none"});
		tds("PwErrorIcon2_1").setProperties({"visible":"none"});
		tds("PwErrorIcon2_2").setProperties({"visible":"none"});
		tds('TextField284_1').removeClass('alert')
		$('#TextField284_1 .top-textfield-icon').removeClass('userSettingPwd-alert')  

		tds("PwErrorIcon1").setProperties({"visible":"none"});
		tds('TextField284').removeClass('alert')
		$('#TextField284 .top-textfield-icon').removeClass('userSettingPwd-alert')

		tds("PwErrorIcon3").setProperties({"visible":"none"});
		tds('TextField284_2').removeClass('alert')
		$('#TextField284_2 .top-textfield-icon').removeClass('userSettingPwd-alert')  

		$.ajax({
            url: _workspace.url + 'Users/PwChange?action=Put', // Service
																// Object,
																// http://192.168.158.12:8080/CMS/Users/PwChange?action=Put
            type: 'POST',
            dataType: 'json',
            crossDomain: true,
            contentType: 'application/json; charset=utf-8',
            xhrFields: {

                withCredentials: true

              },
            data: JSON.stringify({
              "dto": {
                "USER_LOGIN_ID": idp_b,
                "USER_PW" : SHA256(idp_b+pwp_b),
	            "USER_ID" : tmpemail 
                
             }
            }),
            success: function(result) {
            	try {
                    var msg = '비밀번호가 저장되었습니다.';
                    notiFeedback(msg);
				} catch (e) {}
				Top.Controller.get('userSettingDialogLayoutLogic').visibleBtn(false);
                tds("PasswordSession").src("PasswordDefaultLayout.html" +verCsp());
            },
            error: function(error) {
            }
      });
	},
	pwCompare : function(event, widget){
		if(tds("TextField284_2").getText() && tds("TextField284_1").getText()){
			if(tds("TextField284_1").getText() != tds("TextField284_2").getText()){
				tds("PwErrorIcon3").setProperties({"visible":"visible"});
				tds('TextField284_2').addClass('alert')
				$('#TextField284_2 .top-textfield-icon').addClass('userSettingPwd-alert')  	
				tds("PwErrorIcon3").setProperties({"visible":"visible"});
				$('#PwErrorIcon3.icon-chart_error.linear-child-horizontal').css({"visibility":"hidden"})
				setTimeout(function(){
					tds('DifferentPasswordErrorPopover').open()
				},100)
			}else{
				tds("PwErrorIcon3").setProperties({"visible":"none"});
				tds('TextField284_2').removeClass('alert')
				$('#TextField284_2 .top-textfield-icon').removeClass('userSettingPwd-alert')  
			}
		}
		
	}, curPwKeyup : function(event, widget) {
		tds("PwErrorIcon1").setProperties({"visible":"none"});
		tds('TextField284').removeClass('alert')
		$('#TextField284 .top-textfield-icon').removeClass('userSettingPwd-alert')  
	}, newConfirmKeyup : function(event, widget) {
		tds("PwErrorIcon3").setProperties({"visible":"none"});
		tds('TextField284_2').removeClass('alert')
		$('#TextField284_2 .top-textfield-icon').removeClass('userSettingPwd-alert')  
	}
	
	
	
	
});


Top.Controller.create('PasswordDefaultLayoutLogic', {
	init: function(event, widget){
		$.ajax({
		 url: _workspace.url+'Users/BothProfile',///'Users/Profile',
	        type: 'GET', 
	        dataType: 'json',
	        async: false,
	        contentType : "application/json",
	        data: JSON.stringify({
	              "dto": {
	                "USER_ID": userManager.getLoginUserId()
	              }
	         }),
	        success: function(ret){
	        	if(ret.dto.USER_PW == null || ret.dto.USER_PW.length < 2){
	        		// 비번 변경 이력 없음
	        		Top.Dom.selectById("PassWordChangeTime").setText("—");
	        	}else{
	        		let pwtime = ret.dto.USER_PW;
	        		let year = pwtime.split(" ")[0].replace(/-/g,'.');
	        		let datetime = pwtime.split(" ")[1];
	        		Top.Dom.selectById("PassWordChangeTime").setText(year + " "+ datetime + " (Local Time)");
	        	}
	        }
		})
		if(Top.Controller.get('userSettingDialogLayoutLogic').pwFlag === 1){
			Top.Controller.get('PasswordDefaultLayoutLogic').sessionChange()
			Top.Controller.get('userSettingDialogLayoutLogic').pwFlag = 0;
		}
	},
	sessionChange : function(event, widget) {
		// 비밀번호 변경 버튼을 눌럿을 때 화면 변경
		tds("PasswordSession").src("PasswordChangeLayout.html"+verCsp());
		Top.Controller.get("userSettingDialogLayoutLogic").visibleBtn(true);
	}
});


var enImage;
var enBackImage;
Top.Controller.create('spaceMyProfileEditLayoutLogic', {
	init : function(event, widget) {
		tds('editSaveButton').setProperties({"on-click":"saveProfile"})
		tds('editCancelButton').setProperties({"on-click":"cancelProfile"})
		tds('profileMobileChangeButton').setProperties({"on-click":"mobileChangeBtnClick"})
	
		//프로필 최적화 숨기기
		tds('editspaceProfileMobileLayout').setProperties({"visible":"hidden"})
		tds('editspaceProfileMailLayout').setProperties({"visible":"hidden"})

		
		
		

		//배경화면 변경시, 후버 설정
		$('#ImageButton253 i').attr('id',"b2cda")
		$('div#editBackgroundLayout #editImageButton706 .top-linearlayout-root').hover(function(){
			$('div#editBackgroundLayout #editImageButton706 .top-linearlayout-root').css({'background-color' : 'rgba(90,95,255,0.8)'})
			},function(){
				$('div#editBackgroundLayout #editImageButton706 .top-linearlayout-root').css({'background-color' : 'rgba(0,0,0,1)'})
			})
		//프로필화면 변경시, 후버 설정
		$('#spaceCameraIcon i').attr('id',"b2cCamera")
		$('div#editprofileInfoLayout #cameraBtnLayout .top-linearlayout-root').hover(function(){
			$('div#editprofileInfoLayout #cameraBtnLayout .top-linearlayout-root').css({'background-color' : 'rgba(90,95,255,0.8)'})
			},function(){
				$('div#editprofileInfoLayout #cameraBtnLayout .top-linearlayout-root').css({'background-color' : 'rgba(0,0,0,1)'})
			})
		
		
			$.ajax({
			    url: _workspace.url + "Users/BothProfile",
			    //Service Object
			    type: 'GET',
			    dataType: 'json',
			    crossDomain: true,
			    contentType: 'application/json',
			    xhrFields: {
			      withCredentials: true
			    },
			    data: JSON.stringify({
			      "dto": {
			        "USER_ID": userManager.getLoginUserId()
			      }
			    //  ttt:  new Date().getTime()
			    }),
			    success: function success(result) {
			    	if(!result.dto.THUMB_PHOTO){
			    		tds('editspaceProfileImageButton').setSrc(userManager.getUserDefaultPhotoURL(userManager.getLoginUserId()));
			    	}else{
			    		tds('editspaceProfileImageButton').setSrc(userManager.getUserPhoto(userManager.getLoginUserId(),'medium', result.dto.THUMB_PHOTO));
			    	}
			    	
			    	   $(function () {
			    		      $('.top-textfield-root #editspaceProflileNick').keyup(function (e) {
			    		        var nickLength = $(this).val();
			    		        $('.top-textfield-root #editspaceProflileNick').css({
			    		        	"padding-right": "50px",
			    		          	"border-width" : "0px 0px 2px 0px",
			    		      		"border-radius" : "0px 0px 0px 0px",
			    		      		"background" : "transparent",
			    		      		"color": "#FFFFFF",
			    		      		 "font-size": "1.5rem"
			    		        });
			    		       
			    		        $('#editspaceProflileNick .top-textfield-icon').html(nickLength.length + '/20');
			    		        $('#editspaceProflileNick .top-textfield-icon').css({
			    		          "display": "inline",
			    		          "font-size": "0.69rem",
			    		          "position": "absolute",
			    		          "top": "22px",
			    		          "right": "10px",
			    		          "color": "#FFFFFF",
			    		          "opacity": "0.75"
			    		        });
			    		      });
			    		      $('.top-textfield-root #editspaceProflileNick').keyup();
			    		    });
			    	if(result.dto.USER_NICK){
			    		tds('editspaceProflileNick').setText(result.dto.USER_NICK)
			    	}else{tds('editspaceProflileNick').setText(result.dto.USER_LOGIN_ID)}
			    	
			    	tds('editspaceProfileName').setText(result.dto.USER_LOGIN_ID)
			    	tds('editspaceProfileMailView').setText(result.dto.USER_EMAIL)
			    	tds('editspaceProfileMobileView').setText(result.dto.NATIONAL_CODE+' ' +result.dto.USER_PHONE)
			   
			    	//프로필 편집 시, 별명란에 포커스
			    	tds('editspaceProflileNick').focus();
			    	
			    	//프로필 최적화 보여주기
			    	tds('editspaceProfileMobileLayout').setProperties({"visible":"visible"})
			    	tds('editspaceProfileMailLayout').setProperties({"visible":"visible"})
			    	    		
			    	
			    },
			    error: function error(_error2) {}
			  });

		
		
		
		  $('div#spaceMyProfileEditLayout').on('click', function (e) {
		      //프로필 사진  > 더보기 메뉴
		    //  if ($('div#spaceMyProfileEditLayout').css('display') == 'inline-block') {
		        picture_list(e, "cameraBtnLayout");
		        background_list(e,"editImageButton706");
		      //}
		    });
	
	
	
	
	},
	//저장 버튼 클릭
	saveProfile : function(event,widget) {
		//서비스 태우기 전 화면에서 막아줄것들 별명,배경화면,사진
			
		
		
		var nickSave = tds('editspaceProflileNick').getText();
		//별명
		if(nickSave === null || nickSave.length === 0){
				tds("profileNickErrorIcon").setProperties({"visible":"visible"})
				$('#profileNickErrorIcon.icon-chart_error.linear-child-horizontal').css({"visibility":"hidden"})
	    		setTimeout(function(){
	    			tds('NoNickPopover').open();
	    		},100)
	    		return false;
	   	}
		//사진
		var profileSave = tds('editspaceProfileImageButton').getSrc();
		if(profileSave.indexOf(userManager.getUserDefaultPhotoURL(userManager.getLoginUserId())) === 0 ){
			profileSave = null;
		}else{
			profileSave = tds('editspaceProfileImageButton').getSrc();
		}
		var mobileSave = tds('editspaceProfileMobileView').getText().split(" ")[1]
		var mail = tds('editspaceProfileMailView').getText()
		//배경화면BACKGROUND_PHOTO
		var backgroundPhoto = tds('spaceMyProfileLayout').getProperties('background-image').split("(")[1].split(")")[0]
		if(backgroundPhoto === "res/profile/profile_BG01.jpg" || backgroundPhoto === "res/profile/profile_BG02.jpg" || backgroundPhoto === "res/profile/profile_BG03.jpg" || backgroundPhoto === "res/profile/profile_BG04.jpg" || backgroundPhoto === "res/profile/profile_BG05.jpg"|| backgroundPhoto === "res/profile/profile_BG06.jpg"
			|| backgroundPhoto === "res/profile/profile_BG07.jpg" || backgroundPhoto === "res/profile/profile_BG08.jpg" || backgroundPhoto === "res/profile/profile_BG09.jpg" || backgroundPhoto === "res/profile/profile_BG010.jpg" || backgroundPhoto === "res/profile/profile_BG011.jpg"|| backgroundPhoto === "res/profile/profile_BG012.jpg"){
	    	backgroundPhoto = enBackImage;
	    }
//		if(backgroundPhoto === "res/profile/profile_empty_background.png"){
//	    	backgroundPhoto = enBackImage;
//	    }
	      $.ajax({
	          url: _workspace.url+"Users/BothProfile?action=Put", //
	          type: 'POST',
	          dataType: 'json', 
	          crossDomain: true,
	          contentType: 'application/json',
	          xhrFields: {

	              withCredentials: true

	            },
	            data: JSON.stringify({
			          "dto": {
			            "USER_ID": userManager.getLoginUserId(),
			            "USER_PHONE" : mobileSave,
			           // "USER_COM_NUM" : pph_com,
			            "PROFILE_PHOTO" : profileSave,
			            "NATIONAL_CODE" : tds('editspaceProfileMobileView').getText().split(" ")[0],
			            "BACKGROUND_PHOTO" : backgroundPhoto,
			            "USER_EMAIL" : mail,
			            "USER_NICK" : nickSave
			          }
			        }),
	          success: function(result) {

	        	  //업데이트 쳐주는
	        	  userManager.update();
				  tds('AppSplitLeftLayout').src('spaceMyProfileLayout.html'+ verCsp())  
				  
				   //실시간으로 갱신되게끔
				  if($('ul.me-list .me img').length != 0){$('ul.me-list .me img').attr('src',profileSave)}//LNB Friend List
				  if($('button#createSpaceButton').length !=0)spaceAPI.drawLnbSpace(workspaceManager.getMySpaceId())	 	  //LNB Space List 
				  renewGnbImage() //GNB Space Name
				  if($('img#gnbAppProfileIcon').length != 0){$('img#gnbAppProfileIcon').attr('src',profileSave)}  //GNB profile menu
//				  if($('img#spaceProfileImageView').length != 0) {											  // Contents Empty Page
//					  $('img#spaceProfileImageView').attr('src',profileSave)
//					  $('img#backgroundLayout').attr('src',backgroundPhoto)
//				  }
				  //토스트팝업
					TeeToast.open({text: '변경 사항이 저장되었습니다.'});
	          },
	          error: function(error) { 
	          }
	        });    
	
			
			
			
		
			
		
	},
	//취소 버튼 클릭
	cancelProfile : function(event,widget){
		  tds('AppSplitLeftLayout').src('spaceMyProfileLayout.html'+ verCsp())
	
	},
	
	choosefile : function(event, widget) {
			let fileChooser = Top.Device.FileChooser.create({
			    onBeforeLoad : function() {
			        let newFileList = [];
					let file = {};
					let tmpId = 0;
			        for(var i=0; i<this.file.length; i++) {		  
			            file = this.file[i];
			            
	    		        let loadFile = fileManager.onBeforeLoadFile(file); 
	    		        if(!loadFile.result) {
	    		            try {
	    						notiFeedback(loadFile.message);
	    		            } catch (e) {}
	    	
	    		        }
	    		        
	    		        let nomalizedFileFullName = Top.Util.Browser.isIE() ? file.name : file.name.normalize('NFC');        
	    		        let fileNameArray = nomalizedFileFullName.split('.');
	    		        let fileExt = fileNameArray.length == 1 ? "" : fileNameArray[fileNameArray.length - 1];        
	    		        if(fileExt == "jpeg" || fileExt == "png" || fileExt == "jpg" || fileExt == "JPEG" || fileExt == "PNG" || fileExt == "JPG") {
	    		    		
	    		        }else{
	    		        	try {
	    		        		TeeAlarm.open({title: 'JPG,JPEG,PNG 형식의 파일만 첨부할 수 있습니다.', content: '',cancelButtonText: '확인'})
	    		        		//Top.Dom.selectById("Icon_profile_edit_error").setProperties({"visible":"visible", "tooltip-top":"JPG, PNG 파일만 가능합니다."}); 
	    		        		return false;
	    				
	    		            } catch (e) {}
	    	
	    		        }
						
						newFileList.push({
	    		            file_id         : tmpId++,
	                        file_extension  : fileExt,
	                        fileFullName    : nomalizedFileFullName,
	                        file_status     : FileStatus.TRY_UPLOAD,
						});
	                }
			      
		        },
				onFileChoose : Top.Controller.get('spaceMyProfileEditLayoutLogic').onFileSelected,
			    charset: "euc-kr",
			    multiple: false,
			    extFilter: ".jpeg, .jpg, .png"
		    });
			fileChooser.show();
	},	onFileSelected : function(fileChooser) {
		let src = fileChooser.src;
        Top.Dom.selectById("editspaceProfileImageButton").setSrc(src)
		let _USER_ID =  userManager.getLoginUserId()
		let Fext = src.split('/')[1].split(";")[0];
		let srcSend = src.split(',')[1];
		let inputDTO ={
            	files : [{
                    filename : 'profilePhoto.' + Fext,
                    contents : srcSend,
                }],
		          dto: {
		            USER_ID: _USER_ID,
		            PROFILE_PHOTO : src 
		          }
		     };
    }, delete_pic : function(event, widget){
    	let _USER_ID =  userManager.getLoginUserId()
    	
		encodeImage(userManager.getUserDefaultPhotoURL(userManager.getLoginUserId()), deletePhoto(enImage))
        
    }, 
    delete_background : function(event, widget){
    	encodeBackImage(getDefaultBackground(userManager.getLoginUserId()), deletePhotoBack(enBackImage))     
    }, 
    
    back_choosefile : function(event, widget) {
		let fileChooser = Top.Device.FileChooser.create({
		    onBeforeLoad : function() {
		        let newFileList = [];
				let file = {};
				let tmpId = 0;
		        for(var i=0; i<this.file.length; i++) {		  
		            file = this.file[i];
		            
    		        let loadFile = fileManager.onBeforeLoadFile(file); 
    		        if(!loadFile.result) {
    		            try {
    						notiFeedback(loadFile.message);
    		            } catch (e) {}
    	
    		        }
    		        
    		        let nomalizedFileFullName = Top.Util.Browser.isIE() ? file.name : file.name.normalize('NFC');        
    		        let fileNameArray = nomalizedFileFullName.split('.');
    		        let fileExt = fileNameArray.length == 1 ? "" : fileNameArray[fileNameArray.length - 1];        
    		        if(fileExt == "jpeg" || fileExt == "png" || fileExt == "jpg" || fileExt == "JPEG" || fileExt == "PNG" || fileExt == "JPG") {
    		    		
    		        }else{
    		        	try {
    		        		TeeAlarm.open({title: 'JPG,JPEG,PNG 형식의 파일만 첨부할 수 있습니다.', content: '',cancelButtonText: '확인'})
    		        		//Top.Dom.selectById("Icon_profile_edit_error").setProperties({"visible":"visible", "tooltip-top":"JPG, PNG 파일만 가능합니다."}); 
    		        		return false;
    				
    		            } catch (e) {}
    	
    		        }
					
					newFileList.push({
    		            file_id         : tmpId++,
                        file_extension  : fileExt,
                        fileFullName    : nomalizedFileFullName,
                        file_status     : FileStatus.TRY_UPLOAD,
					});
                }
		      
	        },
			onFileChoose : Top.Controller.get('spaceMyProfileEditLayoutLogic').back_onFileSelected,
		    charset: "euc-kr",
		    multiple: false,
		    extFilter: ".jpeg, .jpg, .png"
	    });
		fileChooser.show();
},	back_onFileSelected : function(fileChooser) {
	let src = fileChooser.src;
	tds('spaceMyProfileLayout').setProperties({'background-image':'url('+src+')'})
  
	let _USER_ID =  userManager.getLoginUserId()
	let Fext = src.split('/')[1].split(";")[0];
	let srcSend = src.split(',')[1];
	let inputDTO ={
        	files : [{
                filename : 'profilePhoto.' + Fext,
                contents : srcSend,
            }],
	          dto: {
	            USER_ID: _USER_ID,
	            PROFILE_PHOTO : src 
	          }
	     };
},
    nickFocuout : function(event, widget) {
    	if(tds('editspaceProflileNick').getText() === null || tds('editspaceProflileNick').getText().length === 0 ){
    		tds("profileNickErrorIcon").setProperties({"visible":"visible"})
    		$('#profileNickErrorIcon.icon-chart_error.linear-child-horizontal').css({"visibility":"hidden"})
    		setTimeout(function(){
    			tds('NoNickPopover').open();
    		},100)
    	}
    	else{
    		tds("profileNickErrorIcon").setProperties({"visible":"none"})
    	}
	}, nickKeyup : function(event, widget) {
		tds('profileNickErrorIcon').setProperties({"visible":"none"})
	},
	mobileChangeBtnClick : function(event,widget){
		tds('pwdConfirmDialog').open()
	}
	
	
	
	
	
	
	
});



function encodeImage(src) {
    var canvas = document.createElement('canvas'),
        ctx = canvas.getContext('2d'),
        img = new Image();

    img.onload = function() {
        canvas.width = img.width;
        canvas.height = img.height;
        ctx.drawImage(img, 0, 0, img.width, img.height);
        //callback(canvas.toDataURL());
		console.log(canvas.toDataURL())
        enImage =  canvas.toDataURL();
		
    }
    img.src = src;
}
function deletePhoto(data){
	let _USER_ID =  userManager.getLoginUserId()
	Top.Dom.selectById("editspaceProfileImageButton").setSrc(userManager.getUserDefaultPhotoURL(userManager.getLoginUserId()))
}
//프로필 이미지 드롭다운 리스트
function picture_list(e, clickID) {
	  if (e.toElement != undefined) {
	    if (e.toElement.id == clickID || e.toElement.id === "b2cCamera" || e.toElement.id === "spaceCameraIcon") {
	      more_list_picture = '<top-linearlayout id="Sidebar_drop2" class="position-Sidebar_drop2 linear-child-vertical" layout-width="120px" layout-height="auto" layout-horizontal-alignment="center" padding="" margin="" horizontal-alignment="center" vertical-alignment="middle" visible="visible" border-color="rgba(205,205,207,1.0) rgba(205,205,207,1.0) rgba(205,205,207,1.0) rgba(205,205,207,1.0)" border-width="1px 1px 1px 1px" horizontal-scroll="false" vertical-scroll="false" orientation="vertical" position="false" title="" layout-tab-disabled="false">' + '<div id="Sidebar_drop2" class="top-linearlayout-root">' + '<top-button id="Sidebar_drop2_pc" class="noti_drop linear-child-vertical" layout-width="match_parent" layout-height="28px" padding="0px 0px 1px 10px" horizontal-alignment="left" border-color="rgba(205,205,207,1.0) rgba(205,205,207,1.0) rgba(205,205,207,1.0) rgba(205,205,207,1.0)" border-width="0px 0px 1px 0px" tab-index="0" text="PC" text-color="rgba(51,51,51,1.0)" text-decoration="" icon-position="left" title="" layout-tab-disabled="false" actual-height="29">' + '<button id="Sidebar_drop2_pc" class="top-button-root" type="button" tabindex="0">' + '<label class="top-button-text">프로필 사진 변경</label>' + '</button>' + '</top-button>' + 
	      '<top-button id="Sidebar_drop2_delete" class="noti_drop linear-child-vertical" layout-width="match_parent" layout-height="28px" padding="0px 0px 1px 10px" horizontal-alignment="left" border-color="rgba(205,205,207,1.0) rgba(205,205,207,1.0) rgba(205,205,207,1.0) rgba(205,205,207,1.0)" border-width="0px 0px 1px 0px" tab-index="0" text="기본 이미지로 변경" text-color="rgba(51,51,51,1.0)" text-decoration="" icon-position="left" title="" layout-tab-disabled="false" actual-height="29">' + '<button id="Sidebar_drop2_delete" class="top-button-root" type="button" tabindex="0">' + '<label class="top-button-text">기본 이미지로 변경</label>' + '</button>' + '</top-button>' + '</div>' + '</top-linearlayout>';

	      if ($('top-linearlayout#Sidebar_drop2').length == 0) {
	        more_list_picture = $(more_list_picture);
	        more_list_picture.find("button#Sidebar_drop2_pc").on('click', Top.Controller.get('spaceMyProfileEditLayoutLogic').choosefile); //pc
	        more_list_picture.find("button#Sidebar_drop2_delete").on('click', Top.Controller.get('spaceMyProfileEditLayoutLogic').delete_pic); //삭제

	        $('div#editprofileInfoLayout').append(more_list_picture);

	        $('.position-Sidebar_drop2').css({
	            'top': '-16rem',
	        	'left': '5rem'
	        });
	      }
	    } else {
	      if ($('top-linearlayout#Sidebar_drop2').length != 0) {
	        $('top-linearlayout#Sidebar_drop2').remove();
	      }
	    }

	    ;
	  }

	  ;
	}


/////////////////////////////////////배경화면 관련///////////
function background_list(e, clickID) {
	  if (e.toElement != undefined) {
		  if (e.toElement.id == clickID || e.toElement.id === "b2cda"|| e.toElement.id === "ImageButton253"  ) {
	      more_list_background = '<top-linearlayout id="Sidebar_dropBack" class="position-Sidebar_dropBack linear-child-vertical" layout-width="120px" layout-height="auto" layout-horizontal-alignment="center" padding="" margin="" horizontal-alignment="center" vertical-alignment="middle" visible="visible" border-color="rgba(205,205,207,1.0) rgba(205,205,207,1.0) rgba(205,205,207,1.0) rgba(205,205,207,1.0)" border-width="1px 1px 1px 1px" horizontal-scroll="false" vertical-scroll="false" orientation="vertical" position="false" title="" layout-tab-disabled="false">' + '<div id="Sidebar_dropBack" class="top-linearlayout-root">' + '<top-button id="Sidebar_dropBack_pc" class="noti_drop linear-child-vertical" layout-width="match_parent" layout-height="28px" padding="0px 0px 1px 10px" horizontal-alignment="left" border-color="rgba(205,205,207,1.0) rgba(205,205,207,1.0) rgba(205,205,207,1.0) rgba(205,205,207,1.0)" border-width="0px 0px 1px 0px" tab-index="0" text="배경 변경" text-color="rgba(51,51,51,1.0)" text-decoration="" icon-position="left" title="" layout-tab-disabled="false" actual-height="29">' + '<button id="Sidebar_dropBack_pc" class="top-button-root" type="button" tabindex="0">' + '<label class="top-button-text">배경 변경</label>' + '</button>' + '</top-button>' + 
	      '<top-button id="Sidebar_dropBack_delete" class="noti_drop linear-child-vertical" layout-width="match_parent" layout-height="28px" padding="0px 0px 1px 10px" horizontal-alignment="left" border-color="rgba(205,205,207,1.0) rgba(205,205,207,1.0) rgba(205,205,207,1.0) rgba(205,205,207,1.0)" border-width="0px 0px 1px 0px" tab-index="0" text="기본 이미지로 변경" text-color="rgba(51,51,51,1.0)" text-decoration="" icon-position="left" title="" layout-tab-disabled="false" actual-height="29">' + '<button id="Sidebar_dropBack_delete" class="top-button-root" type="button" tabindex="0">' + '<label class="top-button-text">기본 이미지로 변경</label>' + '</button>' + '</top-button>' + '</div>' + '</top-linearlayout>';

	      if ($('top-linearlayout#Sidebar_dropBack').length == 0) {
	    	  more_list_background = $(more_list_background);
	    	  more_list_background.find("button#Sidebar_dropBack_pc").on('click', Top.Controller.get('spaceMyProfileEditLayoutLogic').back_choosefile); //pc
	    	  more_list_background.find("button#Sidebar_dropBack_delete").on('click', Top.Controller.get('spaceMyProfileEditLayoutLogic').delete_background); //삭제

	        $('div#editBackgroundLayout').append(more_list_background);

	        $('.position-Sidebar_dropBack').css({
	          'top': '2.5rem',
	          'left': '2.5rem'
	        });
	      }
	    } else {
	      if ($('top-linearlayout#Sidebar_dropBack').length != 0) {
	        $('top-linearlayout#Sidebar_dropBack').remove();
	      }
	    }

	    ;
	  }

	  ;
	}
function deletePhotoBack(data){
	tds('spaceMyProfileLayout').setProperties({"background-image":"url("+getDefaultBackground(userManager.getLoginUserId())+")"})
	//Top.Dom.selectById("backgroundEditImageView").setSrc("res/profile/profile_empty_background.png")
}
function encodeBackImage(src) {
    var canvas = document.createElement('canvas'),
        ctx = canvas.getContext('2d'),
        img = new Image();

    img.onload = function() {
        canvas.width = img.width;
        canvas.height = img.height;
        ctx.drawImage(img, 0, 0, img.width, img.height);
        //callback(canvas.toDataURL());
		console.log(canvas.toDataURL())
        enBackImage =  canvas.toDataURL();
		
    }
    img.src = src;
}
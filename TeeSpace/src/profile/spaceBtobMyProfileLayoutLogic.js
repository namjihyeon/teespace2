
Top.Controller.create('spaceBtobMyProfileLayoutLogic', {
	init : function(event, widget) {
		//창모드 시 
		var remVal = screen.width / 1366 * 16
		var hRem = 3.19 * remVal;
		$('div#LinearLayout468').css({'min-height': screen.availHeight - (window.outerHeight - window.innerHeight)  - hRem})
		
		//상태 툴팁 이벤트 설정
		document.querySelector('span#BtobspaceProfileStatusIcon i').addEventListener('mouseover', function (e) {
			if($('span#BtobspaceProfileStatusIcon i').css('color') === "rgb(22, 172, 102)"){
				TeeTooltip.open('온라인', {
					x: e.clientX, 
					y: e.clientY,
				});
				}else{
					TeeTooltip.open('오프라인', {
						x: e.clientX, 
						y: e.clientY,
					});
				}
			});
    	document.querySelector('span#BtobspaceProfileStatusIcon i').addEventListener('mouseout', function (e) {
				TeeTooltip.close();	
		});
		
		
		//프로필 최적화 숨기기
		tds('BtobspaceProfileOfficeLayout').setProperties({"visible":"hidden"})
		tds('BtobspaceProfilePhoneLayout').setProperties({"visible":"hidden"})
		tds('BtobspaceProfileMobileLayout').setProperties({"visible":"hidden"})
		tds('BtobspaceProfileMailLayout').setProperties({"visible":"hidden"})
		
		
		
		tds('BtobprofileEditButton').setProperties({"on-click":"b2beditBtnClick"})		
		tds('BtobprofileTalkButton').setProperties({"on-click":"b2bgotoMyTalk"})
		///
		$.ajax({
		    url: _workspace.url + "Users/BothProfile",
		    //Service Object
		    type: 'GET',
		    dataType: 'json',
		    crossDomain: true,
		    contentType: 'application/json',
		    xhrFields: {
		      withCredentials: true
		    },
		    data: JSON.stringify({
		      "dto": {
		        "USER_ID": userManager.getLoginUserId()
		      }
		    //  ttt:  new Date().getTime()
		    }),
		    success: function success(result) {
		    	//status default
//		    	if(result.dto.USER_STATUS === "online"){
//		    		$('span#BtobspaceProfileStatusIcon_'+_UserId).css({'color':'#16AC66'})
//		    	}else{
//		    		$('span#BtobspaceProfileStatusIcon_'+_UserId).css({'color':'#CCCCCC'})
//		    	}
		    	
		    	
		    	
		    	if(!result.dto.THUMB_PHOTO) {
	              tds('BtobspaceProfileImageView').setSrc(userManager.getUserDefaultPhotoURL(userManager.getLoginUserId()));
	            }else{
	             Top.Dom.selectById("BtobspaceProfileImageView").setSrc(userManager.getUserPhoto(userManager.getLoginUserId(),'medium', result.dto.THUMB_PHOTO));
	             // Top.Dom.selectById("BtobspaceProfileImageView").setSrc(result.dto.THUMB_PHOTO);
	            }
		    	if(!result.dto.BACKGROUND_THUMB){
		    		tds('spaceBtobMyProfileLayout').setProperties({"background-image":"url("+getDefaultBackground(userManager.getLoginUserId())+")"})
		    	}else{
		    		//tds('BtobbackgroundLayout').setSrc(result.dto.BACKGROUND_THUMB)
		    		tds('spaceBtobMyProfileLayout').setProperties({"background-image":"url("+userManager.getUserPhoto(userManager.getLoginUserId(),'back', result.dto.BACKGROUND_THUMB)+")"})
		    	}
		    	
		    	
		    	tds('BtobspaceProflileName').setText(result.dto.USER_NAME)
		    	tds('BtobspaceProfileLoginId').setText("("+result.dto.USER_LOGIN_ID+")")
		    	
		    	
			   	var NewPoj = result.dto.USER_JOB != null ? result.dto.USER_JOB : (result.dto.USER_POSITION != null ? result.dto.USER_POSITION : "");
			   	var poj = result.dto.USER_JOB != null ? '·'+result.dto.USER_JOB : (result.dto.USER_POSITION != null ? '·'+result.dto.USER_POSITION : "");
			   	var NewJop="";
			   	var endJop = "";
		    	for(let jop= 1; jop <  NewPoj.split(', ').length-1 ; jop++){
		    		NewJop += result.dto.COMPANY_CODE+"·"+result.dto.ORG_NAME.split(', ')[jop]+"·"+poj.split(', ')[jop]+",<br>" 
		    	    endJop =  result.dto.COMPANY_CODE+"·"+result.dto.ORG_NAME.split(', ')[NewPoj.split(', ').length-1]+"·"+poj.split(', ')[NewPoj.split(', ').length-1] 
		    		}
		    	//소속없을 경우 분기 처리
			    if(NewPoj === "" || poj === ""){
                    tds('BtobspaceProfileOfficeView').setText("—")
			    }else{
			    	 if(NewPoj.split(', ').length === 1){//only one
					   		tds('BtobspaceProfileOfficeView').setText(result.dto.COMPANY_CODE+"·"+result.dto.ORG_NAME+poj)
					   	}else if(NewPoj.split(', ').length === 2){//two job
					   		tds('BtobspaceProfileOfficeView').setText(result.dto.COMPANY_CODE+"·"+result.dto.ORG_NAME.split(', ')[0]+poj.split(', ')[0]+",<br>"
					   				+result.dto.COMPANY_CODE+"·"+result.dto.ORG_NAME.split(', ')[1]+poj.split(', ')[1])
				    	}else if(NewPoj.split(', ').length === 0) {//null
				    		tds('BtobspaceProfileOfficeView').setText(result.dto.COMPANY_CODE+"·"+result.dto.ORG_NAME+poj)
				    	}else{//three job
				    		tds('BtobspaceProfileOfficeView').setText(result.dto.COMPANY_CODE+"·"+result.dto.ORG_NAME.split(', ')[0]+poj.split(', ')[0]+",<br>"+NewJop +endJop)
					    	$('span#BtobspaceProfileOfficeView').css({'height':'2.2rem'})
					   		tds('BtobspaceProfileOfficeView').setProperties({"tooltip-top":(result.dto.COMPANY_CODE+"·"+result.dto.ORG_NAME.split(', ')[0]+poj.split(', ')[0]+",<br>"+NewJop + endJop).replace(/<br>/g,"")})
					   	}
			    } 	   
			    	
			    	
			   
		    	

		    	
		    	var nCode;
		    	if(!result.dto.NATIONAL_CODE){
		    		nCode = "+82"
		    	}else{
		    		nCode = result.dto.NATIONAL_CODE
		    	}
		    	var phoneNum;
		    	if(!result.dto.USER_PHONE){
		    		phoneNum = "—"
		    	}else{
		    		if(result.dto.USER_PHONE[0] === "0"){
		    			phoneNum = result.dto.USER_PHONE.replace(result.dto.USER_PHONE[0],"")
		    		}else{
			    		phoneNum = result.dto.USER_PHONE	
		    		}
		    	}

		    	tds('BtobspaceProfileMobileView').setText(nCode+" "+ phoneNum)
		    	//회사번호
		    	var comOfficeNum;
		    	if(!result.dto.USER_COM_NUM){
		    		comOfficeNum =  "—"
		    	}else{
		    		if(result.dto.USER_COM_NUM[0] === "0"){
		    			comOfficeNum = result.dto.USER_COM_NUM.replace(result.dto.USER_COM_NUM[0],"")
		    		}else{
		    			comOfficeNum = result.dto.USER_COM_NUM	
		    		}
		    	}
		    	tds('BtobspaceProfilePhoneView').setText(nCode+" "+comOfficeNum)
		    	if(!result.dto.USER_EMAIL || result.dto.USER_EMAIL === "null"){
		    		tds('BtobspaceProfileMailView').setText("—")
		    	}else{
			    	tds('BtobspaceProfileMailView').setText(result.dto.USER_EMAIL)
		    	}
		    	
		    	
		    	
		    	//프로필 최적화 보여주기
		    	tds('BtobspaceProfileOfficeLayout').setProperties({"visible":"visible"})
		    	tds('BtobspaceProfilePhoneLayout').setProperties({"visible":"visible"})
		    	tds('BtobspaceProfileMobileLayout').setProperties({"visible":"visible"})
		    	tds('BtobspaceProfileMailLayout').setProperties({"visible":"visible"})
		    	
		    	
		    			    	
		    },
		    error: function error(_error2) {}
		  });


	},
	//프로필 편집 버튼 이벤트
	b2beditBtnClick : function(event,widget){
		$('div#BtobprofileEditButton').addClass("BtnSelected")
		
		tds('BtobmyProfileContentLayout').src('spaceBtobMyProfileEditLayout.html'+ verCsp())
	},
	b2bgotoMyTalk : function(event, widget){
		$('div#BtobprofileEditButton').removeClass("BtnSelected")
		toMyTalk();
	}
});

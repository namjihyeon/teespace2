var BtobEnImage;
var BtobEnBackImage;

Top.Controller.create('spaceBtobMyProfileEditLayoutLogic', {
	init : function(event, widget) {
		//상태 툴팁 이벤트 설정
		document.querySelector('span#BtobeditspaceProfileStatusIcon i').addEventListener('mouseover', function (e) {
			if($('span#BtobeditspaceProfileStatusIcon i').css('color') === "rgb(22, 172, 102)"){
				TeeTooltip.open('온라인', {
					x: e.clientX, 
					y: e.clientY,
				});
				}else{
					TeeTooltip.open('오프라인', {
						x: e.clientX, 
						y: e.clientY,
					});
				}
			});
    	document.querySelector('span#BtobeditspaceProfileStatusIcon i').addEventListener('mouseout', function (e) {
				TeeTooltip.close();	
		});
		
		
		//프로필 최적화 숨기기
		tds('BtobeditspaceProfileOfficeLayout').setProperties({"visible":"hidden"})
		tds('BtobeditspaceProfilePhoneLayout').setProperties({"visible":"hidden"})
		tds('BtobeditspaceProfileMobileLayout').setProperties({"visible":"hidden"})
		tds('BtobeditspaceProfileMailLayout').setProperties({"visible":"hidden"})
		
		this.prfBack;
		
		tds('BtobeditSaveButton').setProperties({"on-click":"b2bsaveProfile"})
		tds('BtobeditCancelButton').setProperties({"on-click":"b2bcancelProfile"})
		
//		//국제전화번호 노드 연결
//		const Ndata = COUNTRY_CODES.map(value => {
//			return {
//				text: `${value.code} ${value.name}`,
//				value: value.code
//			}
//		});
//		//셀렉박스 연결
//		tds("BtobPhoneSelectBox").setProperties({"nodes":Ndata})
//		if(tds("BtobPhoneSelectBox").getSelected() === undefined){
//			tds("BtobPhoneSelectBox").select('+82')
//		}
//		tds("BtobMobileSelectBox").setProperties({"nodes":Ndata})
//		if(tds("BtobMobileSelectBox").getSelected() === undefined){
//			tds("BtobMobileSelectBox").select("+82")
//		}
		
		//배경화면 변경시, 후버 설정
		$('#BtobImageButton253 i').attr('id',"da")
		$('div#BtobeditBackgroundLayout #BtobeditImageButton706 .top-linearlayout-root').hover(function(){
			$('div#BtobeditBackgroundLayout #BtobeditImageButton706 .top-linearlayout-root').css({'background-color' : 'rgba(90,95,255,0.8)'})
			},function(){
				$('div#BtobeditBackgroundLayout #BtobeditImageButton706 .top-linearlayout-root').css({'background-color' : 'rgba(0,0,0,1)'})
			})
		//프로필화면 변경시, 후버 설정
		$('#cameraButtonLayout i').attr('id',"b2bCamera")
		$('div#BtobeditprofileInfoLayout #cameraButtonLayout .top-linearlayout-root').hover(function(){
			$('div#BtobeditprofileInfoLayout #cameraButtonLayout .top-linearlayout-root').css({'background-color' : 'rgba(90,95,255,0.8)'})
			},function(){
				$('div#BtobeditprofileInfoLayout #cameraButtonLayout .top-linearlayout-root').css({'background-color' : 'rgba(0,0,0,1)'})
			})
			
			

			$.ajax({
			    url: _workspace.url + "Users/BothProfile",
			    //Service Object
			    type: 'GET',
			    dataType: 'json',
			    crossDomain: true,
			    contentType: 'application/json',
			    xhrFields: {
			      withCredentials: true
			    },
			    data: JSON.stringify({
			      "dto": {
			        "USER_ID": userManager.getLoginUserId()
			      }
			    //  ttt:  new Date().getTime()
			    }),
			    success: function success(result) {
			    	//status default
//			    	if(result.dto.USER_STATUS === "online"){
//			    		$('span#BtobeditspaceProfileStatusIcon_'+_UserId).css({'color':'#16AC66'})
//			    	}else{
//			    		$('span#BtobeditspaceProfileStatusIcon_'+_UserId).css({'color':'#CCCCCC'})
//			    	}
//			    	
			    	
			    	
			    	if(!result.dto.THUMB_PHOTO){
			    		tds('BtobeditspaceProfileImageButton').setSrc(userManager.getUserDefaultPhotoURL(userManager.getLoginUserId()));
			    	}else{
			    		//tds('BtobeditspaceProfileImageButton').setSrc(result.dto.THUMB_PHOTO);
			    		tds('BtobeditspaceProfileImageButton').setSrc(userManager.getUserPhoto(userManager.getLoginUserId(),'medium', result.dto.THUMB_PHOTO));
			    	}
			    		
		
			    	tds('BtobeditspaceProfileName').setText(result.dto.USER_NAME)
			    	tds('BtobeditspaceProfileLoginId').setText(result.dto.USER_LOGIN_ID)
			    	
			    	
			   	var NewPoj = result.dto.USER_JOB != null ? result.dto.USER_JOB : (result.dto.USER_POSITION != null ? result.dto.USER_POSITION : "");
			   	var poj = result.dto.USER_JOB != null ? '·'+result.dto.USER_JOB : (result.dto.USER_POSITION != null ? '·'+result.dto.USER_POSITION : "");
			   	var NewJop="";
			   	var endJop = "";
		    	for(let jop= 1; jop <  NewPoj.split(', ').length-1 ; jop++){
		    		NewJop += result.dto.COMPANY_CODE+"·"+result.dto.ORG_NAME.split(', ')[jop]+"·"+poj.split(', ')[jop]+",<br>" 
		    	    endJop =  result.dto.COMPANY_CODE+"·"+result.dto.ORG_NAME.split(', ')[NewPoj.split(', ').length-1]+"·"+poj.split(', ')[NewPoj.split(', ').length-1] 
		    		}
		    	//소속없을 경우 분기 처리
			    if(NewPoj === "" || poj === ""){
                    tds('BtobeditspaceProfileOfficeView').setText("—")
			    }else{
				    if(NewPoj.split(', ').length === 1){//only one
				   		tds('BtobeditspaceProfileOfficeView').setText(result.dto.COMPANY_CODE+"·"+result.dto.ORG_NAME+poj)
				   	}else if(NewPoj.split(', ').length === 2){//two job
				   		tds('BtobeditspaceProfileOfficeView').setText(result.dto.COMPANY_CODE+"·"+result.dto.ORG_NAME.split(', ')[0]+poj.split(', ')[0]+",<br>"
				   				+result.dto.COMPANY_CODE+"·"+result.dto.ORG_NAME.split(', ')[1]+poj.split(', ')[1])
			    	}else if(NewPoj.split(', ').length === 0) {//null
			    		tds('BtobeditspaceProfileOfficeView').setText(result.dto.COMPANY_CODE+"·"+result.dto.ORG_NAME+poj)
			    	}else{//three job
			    		tds('BtobeditspaceProfileOfficeView').setText(result.dto.COMPANY_CODE+"·"+result.dto.ORG_NAME.split(', ')[0]+poj.split(', ')[0]+",<br>"+NewJop +endJop)
				    	$('span#BtobeditspaceProfileOfficeView').css({'height':'2.2rem'})
				   		tds('BtobeditspaceProfileOfficeView').setProperties({"tooltip-top":(result.dto.COMPANY_CODE+"·"+result.dto.ORG_NAME.split(', ')[0]+poj.split(', ')[0]+",<br>"+NewJop + endJop).replace(/<br>/g,"")})
				   	}
			    } 	      
			    	
			   
		    	
			    	
			    if(!result.dto.USER_EMAIL || result.dto.USER_EMAIL === "null"){
		    		tds('BtobeditspaceProfileMailView').setText("—")
		    	}else{
			    	tds('BtobeditspaceProfileMailView').setText(result.dto.USER_EMAIL)
		    	}
			    	
			    tds('BtobPhoneTextField').setText(result.dto.USER_COM_NUM)
			    
			    
			    	if(!result.dto.NATIONAL_CODE){
//			    		tds('BtobMobileSelectBox').select('+82')
//			    		tds("BtobPhoneSelectBox").select('+82')
			    		tds('BtobPhoneNationalCode').setText('+82')
			    		tds('BtobMobileNationalCode').setText('+82')
			    	}else{
			    		tds('BtobPhoneNationalCode').setText(result.dto.NATIONAL_CODE)
			    		tds('BtobMobileNationalCode').setText(result.dto.NATIONAL_CODE)
//			    		tds('BtobMobileSelectBox').select(result.dto.NATIONAL_CODE)
//			    		tds("BtobPhoneSelectBox").select(result.dto.NATIONAL_CODE)
			    	}
			    	tds('BtobMobileTextField').setText(result.dto.USER_PHONE)
			    
//			    	if(!result.dto.BACKGROUND_THUMB){
//			    		tds('BtobbackgroundEditImageView').setSrc("res/profile/profile_empty_background.png")
//			    	}else{
//			    		//tds('BtobbackgroundEditImageView').setSrc(result.dto.BACKGROUND_THUMB)
//			    		tds('BtobbackgroundEditImageView').setSrc(userManager.getUserPhoto(userManager.getLoginUserId(),'back', result.dto.BACKGROUND_THUMB))
//			    	}
			    	
			    	
			    	//프로필 최적화 보여주기
			    	tds('BtobeditspaceProfileOfficeLayout').setProperties({"visible":"visible"})
			    	tds('BtobeditspaceProfilePhoneLayout').setProperties({"visible":"visible"})
			    	tds('BtobeditspaceProfileMobileLayout').setProperties({"visible":"visible"})
			    	tds('BtobeditspaceProfileMailLayout').setProperties({"visible":"visible"})
			    	
			    	
			    	
			    },
			    error: function error(_error2) {}
			  });

		
		
		
		  $('div#spaceBtobMyProfileEditLayout').on('click', function (e) {
		      //프로필 사진  > 더보기 메뉴
		    //  if ($('div#spaceMyProfileEditLayout').css('display') == 'inline-block') {
		        Btob_picture_list(e, "cameraButtonLayout");
		        Btob_background_list(e,"BtobeditImageButton706");
		      //}
		    });
	
	},
	//저장 버튼 클릭
	b2bsaveProfile : function(event,widget) {
		//flag들
		var pflag = 0;
		var mflag = 0;
		var pflagValid = 0;
		var mflagValid = 0;
		//서비스 태우기 전 화면에서 막아줄것들 별명,배경화면,사진
		var profileSave = tds('BtobeditspaceProfileImageButton').getSrc();
		if(profileSave.indexOf(userManager.getUserDefaultPhotoURL(userManager.getLoginUserId())) === 0 ){
			profileSave = null;
		}else{
			profileSave = tds('BtobeditspaceProfileImageButton').getSrc();
		}

	
		var mail = tds('BtobeditspaceProfileMailView').getText()
		
		//배경화면BACKGROUND_PHOTO
		var backgroundPhoto = tds('spaceBtobMyProfileLayout').getProperties('background-image').split("(")[1].split(")")[0]
	
		if(backgroundPhoto === "res/profile/profile_BG01.jpg" || backgroundPhoto === "res/profile/profile_BG02.jpg" || backgroundPhoto === "res/profile/profile_BG03.jpg" || backgroundPhoto === "res/profile/profile_BG04.jpg" || backgroundPhoto === "res/profile/profile_BG05.jpg"|| backgroundPhoto === "res/profile/profile_BG06.jpg"
			|| backgroundPhoto === "res/profile/profile_BG07.jpg" || backgroundPhoto === "res/profile/profile_BG08.jpg" || backgroundPhoto === "res/profile/profile_BG09.jpg" || backgroundPhoto === "res/profile/profile_BG010.jpg" || backgroundPhoto === "res/profile/profile_BG011.jpg"|| backgroundPhoto === "res/profile/profile_BG012.jpg"){
			
	    	backgroundPhoto = BtobEnBackImage;
	    }
		
		

		var phoneSave = tds('BtobPhoneTextField').getText()
		var mobileSave = tds('BtobMobileTextField').getText()
		if(phoneSave === null || phoneSave.length === 0){
			pflag = 0;
			pflagValid = 0;
		}else if(!pattern_num.test(Number(phoneSave))){
			pflagValid = 1;
			pflag = 0;
		}
		else{
			pflagValid = 0;
			pflag = 0;
			phoneSave = tds('BtobPhoneTextField').getText()
		}
		if(mobileSave === null || mobileSave.length === 0){
			mflag = 1;
			mflagValid = 0;
		}else if(!pattern_num.test(Number(mobileSave))){
			mflagValid = 1;
			mflag = 0;
		}
		else{
			mobileSave = tds('BtobMobileTextField').getText()
			mflagValid = 0;
			mflag = 0;
		}
		
		//유효성 검사
//		if(pflag === 1){
//			tds('BtobPhoneTextField').addClass('alert')
//			tds('phoneErrorIcon').setProperties({"visible":"visible"})
//			$('#phoneErrorIcon.icon-chart_error.linear-child-horizontal').css({"visibility":"hidden"})
//			setTimeout(function(){
//				tds('NumberNullBtobPopover').open()
//			},100)
//		}
		if(mflag === 1){
			tds('BtobMobileTextField').addClass('alert')
			tds('mobileErrorIcon').setProperties({"visible":"visible"})
			$('#mobileErrorIcon.icon-chart_error.linear-child-horizontal').css({"visibility":"hidden"})
			setTimeout(function(){
				tds('MobileNullPopover').open()
			},100)
		}
		if(pflagValid === 1){
			tds('BtobPhoneTextField').addClass('alert')
			tds('phoneValidIcon').setProperties({"visible":"visible"})
			$('#phoneValidIcon.icon-chart_error.linear-child-horizontal').css({"visibility":"hidden"})
			setTimeout(function(){
				tds('NumberValidBtobPopover').open()
			},100)
		}
		if(mflagValid === 1){
			tds('BtobMobileTextField').addClass('alert')
			tds('mobileValidIcon').setProperties({"visible":"visible"})
			$('#mobileValidIcon.icon-chart_error.linear-child-horizontal').css({"visibility":"hidden"})
			setTimeout(function(){
				tds('MobileValidPopover').open()
			},100)
		}
		if(pflag || mflag || pflagValid || mflagValid){return false;}
	      $.ajax({
	          url: _workspace.url+"Users/BothProfile?action=Put", //
	          type: 'POST',
	          dataType: 'json', 
	          crossDomain: true,
	          contentType: 'application/json',
	          xhrFields: {

	              withCredentials: true

	            },
	            data: JSON.stringify({
			          "dto": {
			            "USER_ID": userManager.getLoginUserId(),
			            "USER_PHONE" : mobileSave,
			            "USER_COM_NUM" : phoneSave,
			            "PROFILE_PHOTO" : profileSave,
			            "NATIONAL_CODE" : tds('BtobMobileNationalCode').getText(),
			            "BACKGROUND_PHOTO" : backgroundPhoto,
			            "USER_EMAIL" : mail,
			            "USER_NICK" : tds('BtobeditspaceProfileName').getText()
			          }
			        }),
	          success: function(result) {

	        	  //업데이트 쳐주는
	        	  userManager.update();
				  tds('AppSplitLeftLayout').src('spaceBtobMyProfileLayout.html'+ verCsp()) 
				  //실시간으로 갱신되게끔
				 
				  if($('ul.me-list .me img').length != 0){$('ul.me-list .me img').attr('src',tds('BtobeditspaceProfileImageButton').getSrc())}//LNB Friend List
				  if($('button#createSpaceButton').length !=0)spaceAPI.drawLnbSpace(workspaceManager.getMySpaceId())	 	  //LNB Space List 
				  //if($('span#AppGnbCurrentText').text().indexOf('(나)') > -1 ){ $('img#AppGnbCurrentImage').attr('src',tds('BtobeditspaceProfileImageButton').getSrc())} //GNB Space Name
				  renewGnbImage()
				  if($('img#gnbAppProfileIcon').length != 0){$('img#gnbAppProfileIcon').attr('src',tds('BtobeditspaceProfileImageButton').getSrc())}  //GNB profile menu
				  if($('img#BtobspaceProfileImageView').length != 0) {											  // Contents Empty Page
					  $('img#BtobspaceProfileImageView').attr('src',tds('BtobeditspaceProfileImageButton').getSrc())
					  $('img#BtobbackgroundLayout').attr('src',backgroundPhoto)
				  }
				  //토스트팝업
					TeeToast.open({text: '변경 사항이 저장되었습니다.'});
				  
				  
	          },
	          error: function(error) { 
	          }
	        });    		
	},
	//취소 버튼 클릭
	b2bcancelProfile : function(event,widget){
		  tds('AppSplitLeftLayout').src('spaceBtobMyProfileLayout.html'+ verCsp())
	
	},
	
	onChangeBtobPhone : function(event, widget) {
		//$('#fixNationalMobileSelectBox span.top-selectbox-container')[0].lastChild.textContent = $('#fixNationalMobileSelectBox .top-selectbox-container').text().split(' ')[0]
			$('#BtobPhoneSelectBox span.top-selectbox-container')[0].lastChild.textContent = tds('BtobPhoneSelectBox').getSelected().value
	},
	onChangeBtobMobile : function(event, widget){
		$('#BtobMobileSelectBox span.top-selectbox-container')[0].lastChild.textContent = tds('BtobMobileSelectBox').getSelected().value
	},
	
	
	
	choosefileBtob : function(event, widget) {
		let fileChooser = Top.Device.FileChooser.create({
		    onBeforeLoad : function() {
		        let newFileList = [];
				let file = {};
				let tmpId = 0;
		        for(var i=0; i<this.file.length; i++) {		  
		            file = this.file[i];
		            
    		        let loadFile = fileManager.onBeforeLoadFile(file); 
    		        if(!loadFile.result) {
    		            try {
    						notiFeedback(loadFile.message);
    		            } catch (e) {}
    	
    		        }
    		        
    		        let nomalizedFileFullName = Top.Util.Browser.isIE() ? file.name : file.name.normalize('NFC');        
    		        let fileNameArray = nomalizedFileFullName.split('.');
    		        let fileExt = fileNameArray.length == 1 ? "" : fileNameArray[fileNameArray.length - 1];        
    		        if(fileExt == "jpeg" || fileExt == "png" || fileExt == "jpg" || fileExt == "JPEG" || fileExt == "PNG" || fileExt == "JPG") {
    		    		
    		        }else{
    		        	try {
    		        		TeeAlarm.open({title: 'JPG,JPEG,PNG 형식의 파일만 첨부할 수 있습니다.', content: '',cancelButtonText: '확인'})
    		        		//Top.Dom.selectById("Icon_profile_edit_error").setProperties({"visible":"visible", "tooltip-top":"JPG, PNG 파일만 가능합니다."}); 
    		        		return false;
    				
    		            } catch (e) {}
    	
    		        }
					
					newFileList.push({
    		            file_id         : tmpId++,
                        file_extension  : fileExt,
                        fileFullName    : nomalizedFileFullName,
                        file_status     : FileStatus.TRY_UPLOAD,
					});
                }
		      
	        },
			onFileChoose : Top.Controller.get('spaceBtobMyProfileEditLayoutLogic').onFileSelectedBtob,
		    charset: "euc-kr",
		    multiple: false,
		    extFilter: ".jpeg, .jpg, .png"
	    });
		fileChooser.show();
	},
	onFileSelectedBtob : function(fileChooser) {
		let src = fileChooser.src;
		Top.Dom.selectById("BtobeditspaceProfileImageButton").setSrc(src)
		let _USER_ID =  userManager.getLoginUserId()
		let Fext = src.split('/')[1].split(";")[0];
		let srcSend = src.split(',')[1];
		let inputDTO ={
        	files : [{
                filename : 'profilePhoto.' + Fext,
                contents : srcSend,
            }],
	          dto: {
	            USER_ID: _USER_ID,
	            PROFILE_PHOTO : src 
	          }
	     	};
	}, 

	delete_picBtob : function(event, widget){
		let _USER_ID =  userManager.getLoginUserId()
		
		BtobEncodeImage(userManager.getUserDefaultPhotoURL(userManager.getLoginUserId()), BtobDeletePhoto(BtobEnImage))
	
	}, 
	delete_backgroundBtob : function(event, widget){
		BtobEncodeBackImage(getDefaultBackground(userManager.getLoginUserId()), BtobDeletePhotoBack(BtobEnBackImage))     
	}, 

	back_choosefileBtob : function(event, widget) {
		let fileChooser = Top.Device.FileChooser.create({
			onBeforeLoad : function() {
				let newFileList = [];
				let file = {};
				let tmpId = 0;
				for(var i=0; i<this.file.length; i++) {		  
					file = this.file[i];
					
					let loadFile = fileManager.onBeforeLoadFile(file); 
					if(!loadFile.result) {
						try {
							notiFeedback(loadFile.message);
						} catch (e) {}
	
					}
		        
		        let nomalizedFileFullName = Top.Util.Browser.isIE() ? file.name : file.name.normalize('NFC');        
		        let fileNameArray = nomalizedFileFullName.split('.');
		        let fileExt = fileNameArray.length == 1 ? "" : fileNameArray[fileNameArray.length - 1];        
		        if(fileExt == "jpeg" || fileExt == "png" || fileExt == "jpg" || fileExt == "JPEG" || fileExt == "PNG" || fileExt == "JPG") {
		    		
		        	}else{
		        		try {
		        			TeeAlarm.open({title: 'JPG,JPEG,PNG 형식의 파일만 첨부할 수 있습니다.', content: '',cancelButtonText: '확인'})
		        		//Top.Dom.selectById("Icon_profile_edit_error").setProperties({"visible":"visible", "tooltip-top":"JPG, PNG 파일만 가능합니다."}); 
		        			return false;
				
		        		} catch (e) {}
	
		        	}
		        
		        	newFileList.push({
		        		file_id         : tmpId++,
		        		file_extension  : fileExt,
		        		fileFullName    : nomalizedFileFullName,
                    	file_status     : FileStatus.TRY_UPLOAD,
		        	});
				}
	      
			},
			onFileChoose : Top.Controller.get('spaceBtobMyProfileEditLayoutLogic').back_onFileSelectedBtob,
			charset: "euc-kr",
			multiple: false,
			extFilter: ".jpeg, .jpg, .png"
		});
		fileChooser.show();
	},
	back_onFileSelectedBtob : function(fileChooser) {
		let src = fileChooser.src;
		tds('spaceBtobMyProfileLayout').setProperties({'background-image':'url('+src+')'})
		//Top.Dom.selectById("BtobbackgroundEditImageView").setSrc(src)
		let _USER_ID =  userManager.getLoginUserId()
		let Fext = src.split('/')[1].split(";")[0];
		let srcSend = src.split(',')[1];
		let inputDTO ={
				files : [{
					filename : 'profilePhoto.' + Fext,
					contents : srcSend,
				}],
				dto: {
        	  USER_ID: _USER_ID,
        	  PROFILE_PHOTO : src 
				}
		};	
	},
	
	///////////////////////////////////////////////////////////////////////////////b2b 백그라운드 프로필 다이얼로그 편집
	pf_back_choosefileBtob : function(event, widget) {
		let fileChooser = Top.Device.FileChooser.create({
			onBeforeLoad : function() {
				let newFileList = [];
				let file = {};
				let tmpId = 0;
				for(var i=0; i<this.file.length; i++) {		  
					file = this.file[i];
					
					let loadFile = fileManager.onBeforeLoadFile(file); 
					if(!loadFile.result) {
						try {
							notiFeedback(loadFile.message);
						} catch (e) {}
	
					}
		        
		        let nomalizedFileFullName = Top.Util.Browser.isIE() ? file.name : file.name.normalize('NFC');        
		        let fileNameArray = nomalizedFileFullName.split('.');
		        let fileExt = fileNameArray.length == 1 ? "" : fileNameArray[fileNameArray.length - 1];        
		        if(fileExt == "jpeg" || fileExt == "png" || fileExt == "jpg" || fileExt == "JPEG" || fileExt == "PNG" || fileExt == "JPG") {
		    		
		        	}else{
		        		try {
		        			TeeAlarm.open({title: 'JPG,JPEG,PNG 형식의 파일만 첨부할 수 있습니다.', content: '',cancelButtonText: '확인'})
		        		//Top.Dom.selectById("Icon_profile_edit_error").setProperties({"visible":"visible", "tooltip-top":"JPG, PNG 파일만 가능합니다."}); 
		        			return false;
				
		        		} catch (e) {}
	
		        	}
		        
		        	newFileList.push({
		        		file_id         : tmpId++,
		        		file_extension  : fileExt,
		        		fileFullName    : nomalizedFileFullName,
                    	file_status     : FileStatus.TRY_UPLOAD,
		        	});
				}
	      
			},
			onFileChoose : Top.Controller.get('spaceBtobMyProfileEditLayoutLogic').pf_back_onFileSelectedBtob,
			charset: "euc-kr",
			multiple: false,
			extFilter: ".jpeg, .jpg, .png"
		});
		fileChooser.show();
	},
	pf_back_onFileSelectedBtob : function(fileChooser) {
		let src = fileChooser.src;
		Top.Controller.get("spaceBtobMyProfileEditLayoutLogic").prfBack = src;
		//안좋은 방법...?
		$("div.top-gpf").css("background-image", "url('" + src.replace(/(\r\n|\n|\r)/gm, "") + "')");
		
		let _USER_ID =  userManager.getLoginUserId()
		let Fext = src.split('/')[1].split(";")[0];
		let srcSend = src.split(',')[1];
		let inputDTO ={
				files : [{
					filename : 'profilePhoto.' + Fext,
					contents : srcSend,
				}],
				dto: {
        	  USER_ID: _USER_ID,
        	  PROFILE_PHOTO : src 
				}
		};	
	},
	pf_back_delete_backgroundBtob : function(event, widget){
		BtobEncodeBackImage(getDefaultBackground(userManager.getLoginUserId()), pf_BtobDeletePhotoBack(BtobEnBackImage))     
	}, 

	///////////////////////////////////////////////////////////////////////////////b2b 이미지 프로필 다이얼로그 편집
	

	pf_image_choosefileBtob : function(event, widget) {
		let fileChooser = Top.Device.FileChooser.create({
			onBeforeLoad : function() {
				let newFileList = [];
				let file = {};
				let tmpId = 0;
				for(var i=0; i<this.file.length; i++) {		  
					file = this.file[i];
					
					let loadFile = fileManager.onBeforeLoadFile(file); 
					if(!loadFile.result) {
						try {
							notiFeedback(loadFile.message);
						} catch (e) {}
	
					}
		        
		        let nomalizedFileFullName = Top.Util.Browser.isIE() ? file.name : file.name.normalize('NFC');        
		        let fileNameArray = nomalizedFileFullName.split('.');
		        let fileExt = fileNameArray.length == 1 ? "" : fileNameArray[fileNameArray.length - 1];        
		        if(fileExt == "jpeg" || fileExt == "png" || fileExt == "jpg" || fileExt == "JPEG" || fileExt == "PNG" || fileExt == "JPG") {
		    		
		        	}else{
		        		try {
		        			TeeAlarm.open({title: 'JPG,JPEG,PNG 형식의 파일만 첨부할 수 있습니다.', content: '',cancelButtonText: '확인'})
		        		//Top.Dom.selectById("Icon_profile_edit_error").setProperties({"visible":"visible", "tooltip-top":"JPG, PNG 파일만 가능합니다."}); 
		        			return false;
				
		        		} catch (e) {}
	
		        	}
		        
		        	newFileList.push({
		        		file_id         : tmpId++,
		        		file_extension  : fileExt,
		        		fileFullName    : nomalizedFileFullName,
                    	file_status     : FileStatus.TRY_UPLOAD,
		        	});
				}
	      
			},
			onFileChoose : Top.Controller.get('spaceBtobMyProfileEditLayoutLogic').pf_image_onFileSelectedBtob,
			charset: "euc-kr",
			multiple: false,
			extFilter: ".jpeg, .jpg, .png"
		});
		fileChooser.show();
	},
	pf_image_onFileSelectedBtob : function(fileChooser) {
		let src = fileChooser.src;
		$('img#imageBtn2').attr("src",src)
		let _USER_ID =  userManager.getLoginUserId()
		let Fext = src.split('/')[1].split(";")[0];
		let srcSend = src.split(',')[1];
		let inputDTO ={
				files : [{
					filename : 'profilePhoto.' + Fext,
					contents : srcSend,
				}],
				dto: {
        	  USER_ID: _USER_ID,
        	  PROFILE_PHOTO : src 
				}
		};	
	},
	pf_image_delete_picBtob : function(event, widget){
	
		
		BtobEncodeImage(userManager.getUserDefaultPhotoURL(userManager.getLoginUserId()), pf_BtobDeletePhoto(BtobEnImage))
	
	}
});



function BtobEncodeImage(src) {
    var canvas = document.createElement('canvas'),
        ctx = canvas.getContext('2d'),
        img = new Image();

    img.onload = function() {
        canvas.width = img.width;
        canvas.height = img.height;
        ctx.drawImage(img, 0, 0, img.width, img.height);
        //callback(canvas.toDataURL());
		console.log(canvas.toDataURL())
        enImage =  canvas.toDataURL();
		
    }
    img.src = src;
}
function BtobDeletePhoto(data){
	let _USER_ID =  userManager.getLoginUserId()
	Top.Dom.selectById("BtobeditspaceProfileImageButton").setSrc(userManager.getUserDefaultPhotoURL(userManager.getLoginUserId()))
}
//프로필 이미지 드롭다운 리스트
function Btob_picture_list(e, clickID) {
	  if (e.toElement != undefined) {
		if (e.toElement.id == clickID || e.toElement.id  === "b2bCamera" || e.toElement.id === "spaceCameraBtobIcon") {
	    Btob_more_list_picture = '<top-linearlayout id="Sidebar_drop3" class="position-Sidebar_drop3 linear-child-vertical" layout-width="120px" layout-height="auto" layout-horizontal-alignment="center" padding="" margin="" horizontal-alignment="center" vertical-alignment="middle" visible="visible" border-color="rgba(205,205,207,1.0) rgba(205,205,207,1.0) rgba(205,205,207,1.0) rgba(205,205,207,1.0)" border-width="1px 1px 1px 1px" horizontal-scroll="false" vertical-scroll="false" orientation="vertical" position="false" title="" layout-tab-disabled="false">' + '<div id="Sidebar_drop3" class="top-linearlayout-root">' + '<top-button id="Sidebar_drop3_pc" class="noti_drop linear-child-vertical" layout-width="match_parent" layout-height="28px" padding="0px 0px 1px 10px" horizontal-alignment="left" border-color="rgba(205,205,207,1.0) rgba(205,205,207,1.0) rgba(205,205,207,1.0) rgba(205,205,207,1.0)" border-width="0px 0px 1px 0px" tab-index="0" text="PC" text-color="rgba(51,51,51,1.0)" text-decoration="" icon-position="left" title="" layout-tab-disabled="false" actual-height="29">' + '<button id="Sidebar_drop3_pc" class="top-button-root" type="button" tabindex="0">' + '<label class="top-button-text">프로필 사진 변경</label>' + '</button>' + '</top-button>' + 
	      '<top-button id="Sidebar_drop3_delete" class="noti_drop linear-child-vertical" layout-width="match_parent" layout-height="28px" padding="0px 0px 1px 10px" horizontal-alignment="left" border-color="rgba(205,205,207,1.0) rgba(205,205,207,1.0) rgba(205,205,207,1.0) rgba(205,205,207,1.0)" border-width="0px 0px 1px 0px" tab-index="0" text="기본 이미지로 변경" text-color="rgba(51,51,51,1.0)" text-decoration="" icon-position="left" title="" layout-tab-disabled="false" actual-height="29">' + '<button id="Sidebar_drop3_delete" class="top-button-root" type="button" tabindex="0">' + '<label class="top-button-text">기본 이미지로 변경</label>' + '</button>' + '</top-button>' + '</div>' + '</top-linearlayout>';

	      if ($('top-linearlayout#Sidebar_drop3').length == 0) {
	    	  Btob_more_list_picture = $(Btob_more_list_picture);
	    	  Btob_more_list_picture.find("button#Sidebar_drop3_pc").on('click', Top.Controller.get('spaceBtobMyProfileEditLayoutLogic').choosefileBtob); //pc
	    	  Btob_more_list_picture.find("button#Sidebar_drop3_delete").on('click', Top.Controller.get('spaceBtobMyProfileEditLayoutLogic').delete_picBtob); //삭제

	        $('div#BtobeditprofileInfoLayout').append(Btob_more_list_picture);

	        $('.position-Sidebar_drop3').css({
	          'top': '-19.2rem',
	          'left': '4.5rem'
	        });
	      }
	    } else {
	      if ($('top-linearlayout#Sidebar_drop3').length != 0) {
	        $('top-linearlayout#Sidebar_drop3').remove();
	      }
	    }

	    ;
	  }

	  ;
	}


/////////////////////////////////////배경화면 관련///////////
function Btob_background_list(e, clickID) {
	  if (e.toElement != undefined) {
		  if (e.toElement.id == clickID || e.toElement.id === "da" || e.toElement.id === "BtobImageButton253" ) {
	    	Btob_more_list_background = '<top-linearlayout id="Sidebar_drop3_Back" class="position-Sidebar_drop3_Back linear-child-vertical" layout-width="120px" layout-height="auto" layout-horizontal-alignment="center" padding="" margin="" horizontal-alignment="center" vertical-alignment="middle" visible="visible" border-color="rgba(205,205,207,1.0) rgba(205,205,207,1.0) rgba(205,205,207,1.0) rgba(205,205,207,1.0)" border-width="1px 1px 1px 1px" horizontal-scroll="false" vertical-scroll="false" orientation="vertical" position="false" title="" layout-tab-disabled="false">' + '<div id="Sidebar_drop3_Back" class="top-linearlayout-root">' + '<top-button id="Sidebar_drop3_Back_pc" class="noti_drop linear-child-vertical" layout-width="match_parent" layout-height="28px" padding="0px 0px 1px 10px" horizontal-alignment="left" border-color="rgba(205,205,207,1.0) rgba(205,205,207,1.0) rgba(205,205,207,1.0) rgba(205,205,207,1.0)" border-width="0px 0px 1px 0px" tab-index="0" text="배경 변경" text-color="rgba(51,51,51,1.0)" text-decoration="" icon-position="left" title="" layout-tab-disabled="false" actual-height="29">' + '<button id="Sidebar_drop3_Back_pc" class="top-button-root" type="button" tabindex="0">' + '<label class="top-button-text">배경 변경</label>' + '</button>' + '</top-button>' + 
	      '<top-button id="Sidebar_drop3_Back_delete" class="noti_drop linear-child-vertical" layout-width="match_parent" layout-height="28px" padding="0px 0px 1px 10px" horizontal-alignment="left" border-color="rgba(205,205,207,1.0) rgba(205,205,207,1.0) rgba(205,205,207,1.0) rgba(205,205,207,1.0)" border-width="0px 0px 1px 0px" tab-index="0" text="기본 이미지로 변경" text-color="rgba(51,51,51,1.0)" text-decoration="" icon-position="left" title="" layout-tab-disabled="false" actual-height="29">' + '<button id="Sidebar_drop3_Back_delete" class="top-button-root" type="button" tabindex="0">' + '<label class="top-button-text">기본 이미지로 변경</label>' + '</button>' + '</top-button>' + '</div>' + '</top-linearlayout>';

	      if ($('top-linearlayout#Sidebar_drop3_Back').length == 0) {
	    	  Btob_more_list_background = $(Btob_more_list_background);
	    	  Btob_more_list_background.find("button#Sidebar_drop3_Back_pc").on('click', Top.Controller.get('spaceBtobMyProfileEditLayoutLogic').back_choosefileBtob); //pc
	    	  Btob_more_list_background.find("button#Sidebar_drop3_Back_delete").on('click', Top.Controller.get('spaceBtobMyProfileEditLayoutLogic').delete_backgroundBtob); //삭제

	        $('div#BtobeditBackgroundLayout').append(Btob_more_list_background);

	        $('.position-Sidebar_drop3_Back').css({
	          'top': '2.5rem',
	          'left': '2.5rem'
	        });
	      }
	    } else {
	      if ($('top-linearlayout#Sidebar_drop3_Back').length != 0) {
	        $('top-linearlayout#Sidebar_drop3_Back').remove();
	      }
	    }

	    ;
	  }

	  ;
	}
function BtobDeletePhotoBack(data){
	tds('spaceBtobMyProfileLayout').setProperties({"background-image":"url("+getDefaultBackground(userManager.getLoginUserId())+")"})
	//Top.Dom.selectById("BtobbackgroundEditImageView").setSrc("res/profile/profile_empty_background.png")
}
function BtobEncodeBackImage(src) {
    var canvas = document.createElement('canvas'),
        ctx = canvas.getContext('2d'),
        img = new Image();

    img.onload = function() {
        canvas.width = img.width;
        canvas.height = img.height;
        ctx.drawImage(img, 0, 0, img.width, img.height);
        //callback(canvas.toDataURL());
		console.log(canvas.toDataURL())
        enBackImage =  canvas.toDataURL();
		
    }
    img.src = src;
}
///////////////////////////////////////////b2b프로필 다이얼로그 배경및 이미지
function pf_BtobDeletePhotoBack(data){

	$('div.top-gpf').css({"background-image":"url("+getDefaultBackground(userManager.getLoginUserId())+")"})
}

function pf_BtobDeletePhoto(data){
	$('img#imageBtn2').attr("src",userManager.getUserDefaultPhotoURL(userManager.getLoginUserId()))
	
}


Top.Controller.create('changeMobileLayoutLogic', {
	
	init : function(event, widget){
		$('#top-dialog-root_pwdConfirmDialog .top-dialog-title').text("휴대폰 번호 변경")
		//이벤트
		tds('changeMobileOkButton').setProperties({"on-click":"changeOkClick"})
		tds('changeMobileCancelButton').setProperties({"on-click":"changeCancelClick"})
		//국제전화번호 노드 연결
		const dataEdit = COUNTRY_CODES.map(value => {
			return {
				text: `${value.code} ${value.name}`,
				value: value.code
			}
		});
		tds("editNationalMobileSelectBox").setProperties({"nodes":dataEdit})
		tds("editNationalMobileSelectBox").select("+82")
		tds('newMobileTextField').focus();
		
		
		
		
	},
	
	
	onChangeProfileNationalMobile : function(event, widget) {
	
	$('#editNationalMobileSelectBox span.top-selectbox-container')[0].lastChild.textContent = tds("editNationalMobileSelectBox").getSelected().value
	
	},
	newMobileFocusOut : function(event, widget) {
	
		var mobileNum = tds('newMobileTextField').getText()
		if(mobileNum === null || mobileNum.length === 0){
			tds('IconEditNullMobile').setProperties({"visible":"visible"})
			$('#IconEditNullMobile.icon-chart_error.linear-child-horizontal').css({"visibility":"hidden"})
			tds('IconEditVaildMobile').setProperties({"visible":"none"})
			tds('newMobileTextField').addClass('alert')
			tds('changeMobileOkButton').setProperties({"disabled":"true"})
			tds('changeMobileOkButton').removeClass("solid")
			tds('changeMobileOkButton').addClass("disabled")
			setTimeout(function(){
				tds('MobileNullPopover').open()
			},100)
		}else{
			if(pattern_engs.test(mobileNum) || pattern_engb.test(mobileNum)	|| pattern_spc2.test(mobileNum) ||  pattern_kor.test(mobileNum)	||  pattern_blank.test(mobileNum))
				{
					tds("newMobileTextField").addClass("alert")
					tds("IconEditVaildMobile").setProperties({"visible":"visible"})
					$('#IconEditVaildMobile.icon-chart_error.linear-child-horizontal').css({"visibility":"hidden"})
					tds('IconEditNullMobile').setProperties({"visible":"none"})
					tds('changeMobileOkButton').setProperties({"disabled":"true"})
					tds('changeMobileOkButton').removeClass("solid")
					tds('changeMobileOkButton').addClass("disabled")
					setTimeout(function(){
						Top.Dom.selectById("MobileValidPopover").open()
					}, 100)
					return false;
				}
			else{
				tds('changeMobileOkButton').setProperties({"disabled":"false"})
				tds('changeMobileOkButton').removeClass("disabled")
				tds('changeMobileOkButton').addClass("solid")
				tds("newMobileTextField").removeClass("alert")
				tds("IconEditVaildMobile").setProperties({"visible":"none"})
				tds('IconEditNullMobile').setProperties({"visible":"none"})
			}
		}
		
		
		
		
	},
	newMobileKeyup : function(event, widget) {
		
		tds("newMobileTextField").removeClass("alert")
		tds("IconEditVaildMobile").setProperties({"visible":"none"})
		tds('IconEditNullMobile').setProperties({"visible":"none"})
	
	},
	
	changeOkClick : function(event, widget){
		
		if(tds('editspaceProfileMobileView')){
            tds('editspaceProfileMobileView').setText(tds("editNationalMobileSelectBox").getSelected().value +" "+tds('newMobileTextField').getText())

		}else{
			$('span.b2c_national_code').html(tds("editNationalMobileSelectBox").getSelected().value)
			$('span.b2c_mobile_text').html(tds('newMobileTextField').getText())
		}
	
		tds('pwdConfirmDialog').close()
	},
	changeCancelClick : function(event, widget){
		tds('pwdConfirmDialog').close()
	}
	
});

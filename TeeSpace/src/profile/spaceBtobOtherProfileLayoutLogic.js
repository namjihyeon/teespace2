
Top.Controller.create('spaceBtobOtherProfileLayoutLogic', {
	init : function(event, widget) {
		//창모드 시 
		var remVal = screen.width / 1366 * 16
		var hRem = 3.19 * remVal;
		$('div#LinearLayout385').css({'min-height': screen.availHeight - (window.outerHeight - window.innerHeight)  - hRem})
		
		
		//status에 _UserId의 uuid 반영 & default 가져오기
		const initialize = function(FRIEND_ID) {
			_UserId = FRIEND_ID;
	    	$('span#BtobspaceotherProfileStatusIcon').attr('id',"BtobspaceotherProfileStatusIcon_"+_UserId)
	    	$('top-icon#BtobspaceotherProfileStatusIcon').attr('id',"BtobspaceotherProfileStatusIcon_"+_UserId)
	  
	    	$('#BtobspaceotherProfileImageView').attr('class',_UserId)
			$('#BtobspaceotherProfileImageView').css({'display':'block'})
			
			
			//프로필 최적화 숨기기
			tds('BtobspaceotherProfileOfficeLayout').setProperties({"visible":"hidden"})
			tds('BtobspaceotherProfilePhoneLayout').setProperties({"visible":"hidden"})
			tds('BtobspaceotherProfileMobileLayout').setProperties({"visible":"hidden"})
			tds('BtobspaceotherProfileMailLayout').setProperties({"visible":"hidden"})
			//증겨찾기 숨기기
			tds('BtobspaceotherFullStarIcon').setProperties({'visible':'none'})
			
			//1대1 talk
			tds('BtobprofileOtherTalkButton').setProperties({"on-click":"b2botherTalkBtnClick"})
			//1대1 meeting
			tds('BtobprofileOtherMeetingButton').setProperties({"on-click":"b2botherMeetingBtnClick"})
			//즐겨찾기
			tds('BtobspaceotherStarLineIcon').setProperties({'on-click':'b2bOther_addFavr'})
			tds('BtobspaceotherFullStarIcon').setProperties({'on-click':'b2bOther_removeFavr'})
			
			//상태 툴팁 이벤트 설정
			document.querySelector('span#BtobspaceotherProfileStatusIcon_'+_UserId +' i').addEventListener('mouseover', function (e) {
				if($('span#BtobspaceotherProfileStatusIcon_'+_UserId).css('color') === "rgb(22, 172, 102)"){
					TeeTooltip.open('온라인', {
						x: e.clientX, 
						y: e.clientY,
					});
					}else{
						TeeTooltip.open('오프라인', {
							x: e.clientX, 
							y: e.clientY,
						});
					}
				});
	    	document.querySelector('span#BtobspaceotherProfileStatusIcon_'+_UserId +' i').addEventListener('mouseout', function (e) {
					TeeTooltip.close();	
			});
			
			//내 친구 리스트의 즐겨찾기 여부 확인
				$.ajax({
				 			url: _workspace.url+'Users/GetFriendList?action=', //Service Object
				 				type: 'POST',
				 				dataType: 'json', 
				 				crossDomain: true,
				 				async: false,
				 				contentType: 'application/json; charset=utf-8',
				 				xhrFields: {
	
				 					withCredentials: true
	
				 				},	
				 				data: JSON.stringify({
				 					"dto": {
				 						"USER_ID": userManager.getLoginUserId()
				 					}
				 				}),
				 				success : function(result){
				 					var friends = result.dto.dtos;
				 					for(let fl=0; fl<friends.length; fl++){
				 						if(friends[fl].FRIEND_ID === _UserId){
				 							//즐겨찾기 on인데 이미 즐겨찾기 되어 있으면 상태 변경 해주어야함
				 							if(friends[fl].FRIEND_FAV === 'FAV0001'){
				 								tds('BtobspaceotherStarLineIcon').setProperties({'visible':'none'})
				 								tds('BtobspaceotherFullStarIcon').setProperties({'visible':'visible'})
				 							}else{
				 								tds('BtobspaceotherStarLineIcon').setProperties({'visible':'visible'})
				 							}
				 						}
				 					}
		            			               			},
		            			error: function(error) { 
		            									}
				 			});
			    	
			
			
			
			
				
				//내가 친구 별명 설정 안했을 경우
				$.ajax({
				    url: _workspace.url + "Users/BothProfile",
				    //Service Object
				    type: 'GET',
				    dataType: 'json',
				    crossDomain: true,
				    contentType: 'application/json',
				    xhrFields: {
				      withCredentials: true
				    },
				    data: JSON.stringify({
				      "dto": {
				        "USER_ID": _UserId
				      }
				    //  ttt:  new Date().getTime()
				    }),
				    success: function success(result) {
				    	//status default
				    	if(result.dto.USER_STATUS === "online"){
				    		$('span#BtobspaceotherProfileStatusIcon_'+_UserId).css({'color':'#16AC66'})
				    	}else{
				    		$('span#BtobspaceotherProfileStatusIcon_'+_UserId).css({'color':'#CCCCCC'})
				    	}
				    	
				    	if(!result.dto.THUMB_PHOTO){
				    		tds('BtobspaceotherProfileImageView').setSrc(userManager.getUserDefaultPhotoURL(result.dto.USER_ID));
				    	}else{
				    	  	//tds('BtobspaceotherProfileImageView').setSrc(result.dto.THUMB_PHOTO);
				    		tds('BtobspaceotherProfileImageView').setSrc(userManager.getUserPhoto(result.dto.USER_ID,'medium', result.dto.THUMB_PHOTO));
				    	}
				    	if(!result.dto.BACKGROUND_THUMB){
	
				    		tds('spaceBtobOtherProfileLayout').setProperties({"background-image":"url("+getDefaultBackground(result.dto.USER_ID)+")"})
				    		//tds('BtobotherbackgroundLayout').setSrc("res/profile/profile_empty_background.png")
				    	}else{
				    		//tds('BtobotherbackgroundLayout').setSrc(result.dto.BACKGROUND_THUMB)
				    		tds('spaceBtobOtherProfileLayout').setProperties({"background-image":"url("+userManager.getUserPhoto(result.dto.USER_ID,'back', result.dto.BACKGROUND_THUMB)+")"})
				    	}
	
						
				    	tds('BtobspaceotherProfileName').setText(result.dto.USER_NAME)
				    	tds('BtobspaceotherProfileLoginId').setText("("+result.dto.USER_LOGIN_ID+")")
				    	
				    	
				    	var NewPoj = result.dto.USER_JOB != null ? result.dto.USER_JOB : (result.dto.USER_POSITION != null ? result.dto.USER_POSITION : "");
				    	var poj = result.dto.USER_JOB != null ? '·'+result.dto.USER_JOB : (result.dto.USER_POSITION != null ? '·'+result.dto.USER_POSITION : "");
				    	var NewJop="";
				    	var endJop = "";
				    	for(let jop= 1; jop <  NewPoj.split(', ').length-1 ; jop++){
				    		NewJop += result.dto.COMPANY_CODE+"·"+result.dto.ORG_NAME.split(', ')[jop]+"·"+poj.split(', ')[jop]+",<br>" 
				    	    endJop =  result.dto.COMPANY_CODE+"·"+result.dto.ORG_NAME.split(', ')[NewPoj.split(', ').length-1]+"·"+poj.split(', ')[NewPoj.split(', ').length-1] 
				    	}
				    	//소속없을 경우 분기 처리
	 				    if(NewPoj === "" || poj === ""){
	                            tds('BtobspaceotherProfileOfficeView').setText("—")
	 				    }else{
	 				    		
				    	    if(NewPoj.split(', ').length === 1){//only one
				        		tds('BtobspaceotherProfileOfficeView').setText(result.dto.COMPANY_CODE+"·"+result.dto.ORG_NAME+poj)
				    	    }else if(NewPoj.split(', ').length === 2){//two job
				    	    	tds('BtobspaceotherProfileOfficeView').setText(result.dto.COMPANY_CODE+"·"+result.dto.ORG_NAME.split(', ')[0]+poj.split(', ')[0]+",<br>"
				    		    		+result.dto.COMPANY_CODE+"·"+result.dto.ORG_NAME.split(', ')[1]+"·"+poj.split(', ')[1])
				    	    }else if(NewPoj.split(', ').length === 0) {//null
				    		    tds('BtobspaceotherProfileOfficeView').setText(result.dto.COMPANY_CODE+"·"+result.dto.ORG_NAME+poj)
				    	    }else{//three job
				    	    	tds('BtobspaceotherProfileOfficeView').setText(result.dto.COMPANY_CODE+"·"+result.dto.ORG_NAME.split(', ')[0]+poj.split(', ')[0]+",<br>"+NewJop +endJop)
				    		    $('span#BtobspaceotherProfileOfficeView').css({'height':'2.2rem'})
				    		    tds('BtobspaceotherProfileOfficeView').setProperties({"tooltip-top":(result.dto.COMPANY_CODE+"·"+result.dto.ORG_NAME.split(', ')[0]+poj.split(', ')[0]+",<br>"+NewJop + endJop).replace(/<br>/g,"")})
				    	    }
				    
				    	
	 				    }
				    	
				    	
				    
				    	
				    	
				    	
				    	
				    	var nCode;
				    	var mNum;
				    	var cNum;
				    	if(!result.dto.NATIONAL_CODE){
				    		nCode = "+82"
				    	}else{
				    		nCode = result.dto.NATIONAL_CODE
				    	}
				    	if(!result.dto.USER_PHONE){
				    		mNum = "—"
				    	}else{
				    		if(result.dto.USER_PHONE[0] === "0"){
				    			mNum = result.dto.USER_PHONE.replace(result.dto.USER_PHONE[0],"")
				    		}else{
				    			mNum = result.dto.USER_PHONE	
				    		}
				    	}
				    	if(!result.dto.USER_COM_NUM){
				    		cNum = "—";
				    	}else{
				    		
				    		if(result.dto.USER_COM_NUM[0] === "0"){
				    			cNum = result.dto.USER_COM_NUM.replace(result.dto.USER_COM_NUM[0],"")
				    		}else{
				    			cNum = result.dto.USER_COM_NUM	
				    		}
				    		
				    	}
				    	
				    	tds('BtobspaceotherProfileMobileView').setText(nCode +" "+mNum)
				    	tds('BtobspaceotherProfilePhoneView').setText(nCode + " " + cNum)
				    	if(!result.dto.USER_EMAIL || result.dto.USER_EMAIL === "null"){
				    		tds('BtobspaceotherProfileMailView').setText("—")
				    	}else{
					    	tds('BtobspaceotherProfileMailView').setText(result.dto.USER_EMAIL)	
				    	}
				    
				    	
				    	
				    	//프로필 최적화 숨기기
				    	tds('BtobspaceotherProfileOfficeLayout').setProperties({"visible":"visible"})
				    	tds('BtobspaceotherProfilePhoneLayout').setProperties({"visible":"visible"})
				    	tds('BtobspaceotherProfileMobileLayout').setProperties({"visible":"visible"})
				    	tds('BtobspaceotherProfileMailLayout').setProperties({"visible":"visible"})
				    },
				    error: function error(_error2) {}
				  });
		};
		
		mobx.reaction(
			() => FriendListModel.isLoaded.get(),
			isLoaded => {
				if (isLoaded) {
					const FRIEND_ID = FriendListModel.findByLoginUserId(location.href.split('f/')[1]).FRIEND_ID;
					initialize(FRIEND_ID);
				}
			}, 
			{
				fireImmediately: true,
				onError() {
					
				},
			}
		);
		
		
		//즐겨찾기 툴팁 이벤트 설정
		document.querySelector('#BtobspaceotherStarLineIcon').addEventListener('mouseover', function (e) {
			
					TeeTooltip.open('즐겨찾기', {
						x: e.clientX, 
						y: e.clientY,
					});
			
			});
    	document.querySelector('#BtobspaceotherStarLineIcon').addEventListener('mouseout', function (e) {
				TeeTooltip.close();	
		});
    	document.querySelector('#BtobspaceotherFullStarIcon').addEventListener('mouseover', function (e) {
			
			TeeTooltip.open('즐겨찾기 해제', {
				x: e.clientX, 
				y: e.clientY,
			});
	
    	});
    	document.querySelector('#BtobspaceotherFullStarIcon').addEventListener('mouseout', function (e) {
    		TeeTooltip.close();	
    	});
		
		
		
		
	},
	//1대1 Talk 라우팅
	b2botherTalkBtnClick : function(){
		var b2botherId = [{"USER_ID":_UserId}]
		spaceAPI.createSpace(null, userManager.getLoginUserId(), 'WKS0002', b2botherId);
	},
	//1대1 Meeting 라우팅
	b2botherMeetingBtnClick : function(){
		var b2botherId = [{"USER_ID":_UserId}]
		spaceAPI.createSpace(null, userManager.getLoginUserId(), 'WKS0002', b2botherId, "MEETING");
	},
	//즐겨찾기 추가
	b2bOther_addFavr : function(event, widget){
		//친구 FAV 업데이트 서비스
		 $.ajax({
		        url: _workspace.url+"Users/UpdateFriend?action=", //Service Object
		        type: 'POST',
		        dataType: 'json', 
		        crossDomain: true,
		        contentType: 'application/json; charset=utf-8',
		        xhrFields: {

		            withCredentials: true

		          },
		          data: JSON.stringify({
		                  "dto": {
		                    "USER_ID": userManager.getLoginUserId(),
		                    "FRIEND_ID": _UserId,
		                    "FRIEND_FAV" : 'FAV0001'
		                  }
		                }),
		           success : function(result){
		        	   	tds('BtobspaceotherStarLineIcon').setProperties({"visible":"none"})
		       			tds('BtobspaceotherFullStarIcon').setProperties({"visible":"visible"})
		       			//즐겨찾기시, 프렌즈 리스트 업데이트
						FriendListModel.fetch(userManager.getLoginUserId());
						TeeToast.open({
							text: '즐겨찾기가 설정되었습니다'
						});
		            
		            			               },
		           error: function(error) { 
		                  }
		    });	
	},
	//즐겨찾기 삭제
	b2bOther_removeFavr : function(event, widget){
		//친구 FAV 삭제하는 서비스
		 $.ajax({
		        url: _workspace.url+"Users/UpdateFriend?action=", //Service Object
		        type: 'POST',
		        dataType: 'json', 
		        crossDomain: true,
		        contentType: 'application/json; charset=utf-8',
		        xhrFields: {

		            withCredentials: true

		          },
		          data: JSON.stringify({
		                  "dto": {
		                    "USER_ID": userManager.getLoginUserId(),
		                    "FRIEND_ID": _UserId,
		                    "FRIEND_FAV" : 'FAV0002'
		                  }
		                }),
		           success : function(result){
		        		tds('BtobspaceotherFullStarIcon').setProperties({"visible":"none"})
		        		tds('BtobspaceotherStarLineIcon').setProperties({"visible":"visible"})
		        		//즐겨찾기시, 프렌즈 리스트 업데이트
						FriendListModel.fetch(userManager.getLoginUserId());
						TeeToast.open({
							text: '즐겨찾기가 해제되었습니다'
						});
		            			               },
		           error: function(error) { 
		                  }
		    });	
	},
	setFavoriteStatus: function (status) {
		if (status === 'off') {
			tds('BtobspaceotherStarLineIcon').setProperties({"visible":"visible"})
			tds('BtobspaceotherFullStarIcon').setProperties({"visible":"none"})
		} else {
			tds('BtobspaceotherStarLineIcon').setProperties({"visible":"none"})
			tds('BtobspaceotherFullStarIcon').setProperties({"visible":"visible"})
		}
	},

});

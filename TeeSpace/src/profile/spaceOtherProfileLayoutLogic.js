 function NameToNick(_tempUserId){
		//친구 정보 가져오는 서비스
		var nickVal;
		var findFrined ;
		$.ajax({
	        url: _workspace.url+'Users/GetFriendList?action=', //Service Object
	        type: 'POST',
	        dataType: 'json', 
	        crossDomain: true,
	        async: false,
	        contentType: 'application/json; charset=utf-8',
	        xhrFields: {

	            withCredentials: true

	          },
	          data: JSON.stringify({
	                  "dto": {
	                    "USER_ID": userManager.getLoginUserId()
	                  }
	                }),
	           success : function(result){
	        	   
	           
	           var friends = result.dto.dtos;
	           
	           for(let fl=0; fl<friends.length; fl++){
	        	   if(friends[fl].FRIEND_ID === _tempUserId){
	        		   var tmp = (friends[fl].FRIEND_NICK != null ? friends[fl].FRIEND_NICK : friends[fl].USER_NICK);
	        		 
	        		   nickVal =  tmp != null ? tmp  : friends[fl].USER_LOGIN_ID;
	        		   findFrined = 1;
	        	   }
  
	           }
	           
	            			               },
	           error: function(error) { 
	                  }
	    });
		
	    if(findFrined === 1) {
	    	return nickVal;
	    }else{
	    	return nickVal = null;
	    }
	}

Top.Controller.create('spaceOtherProfileLayoutLogic', {

	init : function(event, widget) {
		//창모드 시 
		var remVal = screen.width / 1366 * 16
		var hRem = 3.19 * remVal;
		$('div#LinearLayout288').css({'min-height': screen.availHeight - (window.outerHeight - window.innerHeight)  - hRem})
		//프로필 최적화 숨기기
		tds('spaceotherProfileMobileLayout').setProperties({"visible":"hidden"})
		tds('spaceotherProfileMailLayout').setProperties({"visible":"hidden"})
		tds('FriendNickLinearLayout').setProperties({"visible":"hidden"})
		
		const initialize = function(FRIEND_ID) {
		
			_UserId = FRIEND_ID
			//status에 _UserId의 uuid 반영 & default 가져오기
	    	$('span#spaceotherProfileStatusIcon').attr('id',"spaceotherProfileStatusIcon_"+_UserId)
	    	$('top-icon#spaceotherProfileStatusIcon').attr('id',"spaceotherProfileStatusIcon_"+_UserId)
	    	
	    	$('#spaceotherProfileImageView').attr('class',_UserId)
			 $('#spaceotherProfileImageView').css({'display':'block'})
			
			 //증겨찾기 숨기기
			tds('spaceotherFullStarIcon').setProperties({'visible':'none'})
			
			
			
			//1대1 talk
			tds('profileOtherTalkButton').setProperties({"on-click":"otherTalkBtnClick"})
			//1대1 meeting
			tds('profileOtherMeetingButton').setProperties({"on-click":"otherMeetingBtnClick"})
			//이벤트
			tds('FriendNickClickIcon').setProperties({"on-click":"gofrinedNick"})
			tds('editNickSaveButton').setProperties({"on-click":"nickSaveOk"})
			tds('editNickCancelButton').setProperties({'on-click':'nickCancelBtn'})
			tds('spaceotherStarLineIcon').setProperties({'on-click':'Other_addFavr'})
			tds('spaceotherFullStarIcon').setProperties({'on-click':'Other_removeFavr'})
			this.defaultNick;
			
			//상태 툴팁 이벤트 설정
			document.querySelector('span#spaceotherProfileStatusIcon_'+_UserId +' i').addEventListener('mouseover', function (e) {
				if($('span#spaceotherProfileStatusIcon_'+_UserId).css('color') === "rgb(22, 172, 102)"){
					TeeTooltip.open('온라인', {
						x: e.clientX, 
						y: e.clientY,
					});
					}else{
						TeeTooltip.open('오프라인', {
							x: e.clientX, 
							y: e.clientY,
						});
					}
				});
	    	document.querySelector('span#spaceotherProfileStatusIcon_'+_UserId +' i').addEventListener('mouseout', function (e) {
					TeeTooltip.close();	
			});
			
			
			//내 친구 리스트의 즐겨찾기 여부 확인
			$.ajax({
			 			url: _workspace.url+'Users/GetFriendList?action=', //Service Object
			 				type: 'POST',
			 				dataType: 'json', 
			 				crossDomain: true,
			 				async: false,
			 				contentType: 'application/json; charset=utf-8',
			 				xhrFields: {
	
			 					withCredentials: true
	
			 				},	
			 				data: JSON.stringify({
			 					"dto": {
			 						"USER_ID": userManager.getLoginUserId()
			 					}
			 				}),
			 				success : function(result){
			 					var friends = result.dto.dtos;
			 					for(let fl=0; fl<friends.length; fl++){
			 						if(friends[fl].FRIEND_ID === _UserId){
			 							//즐겨찾기 on인데 이미 즐겨찾기 되어 있으면 상태 변경 해주어야함
			 							if(friends[fl].FRIEND_FAV === 'FAV0001'){
			 								tds('spaceotherStarLineIcon').setProperties({'visible':'none'})
			 								tds('spaceotherFullStarIcon').setProperties({'visible':'visible'})
			 							}else{
			 								tds('spaceotherStarLineIcon').setProperties({'visible':'visible'})
			 							}
			 						}
			 					}
	            			               			},
	            			error: function(error) { 
	            									}
			 			});	
			
		
		
		
		
		
		
		//내가 친구 별명 설정 안했을 경우

	    	
		$.ajax({
			    url: _workspace.url + "Users/BothProfile",
			    //Service Object
			    type: 'GET',
			    dataType: 'json',
			    crossDomain: true,
			    contentType: 'application/json',
			    xhrFields: {
			      withCredentials: true
			    },
			    data: JSON.stringify({
			      "dto": {
			        "USER_ID": _UserId
			      }
			    //  ttt:  new Date().getTime()
			    }),
			    success: function success(result) {
			    
			    	//status default
			    	if(result.dto.USER_STATUS === "online"){
			    		$('span#spaceotherProfileStatusIcon_'+result.dto.USER_ID).css({'color':'#16AC66'})
			    	}else{
			    		$('span#spaceotherProfileStatusIcon_'+result.dto.USER_ID).css({'color':'#CCCCCC'})
			    	}
			    			    	
			    	if(!result.dto.THUMB_PHOTO){
			    		tds('spaceotherProfileImageView').setSrc(userManager.getUserDefaultPhotoURL(result.dto.USER_ID));
			    	}else{
			    		tds('spaceotherProfileImageView').setSrc(userManager.getUserPhoto(result.dto.USER_ID,'medium', result.dto.THUMB_PHOTO));
			    	 
			    	}
			    	if(!result.dto.BACKGROUND_THUMB){
			    		tds('spaceOtherProfileLayout').setProperties({"background-image":"url("+getDefaultBackground(result.dto.USER_ID)+")"})
			    		
			    	}else{
			    		tds('spaceOtherProfileLayout').setProperties({"background-image":"url("+userManager.getUserPhoto(result.dto.USER_ID,'back', result.dto.BACKGROUND_THUMB)+")"})
			    		
			    	}

					tds('spaceotherProflileNick').setText(NameToNick(result.dto.USER_ID))   
			    	
			    	tds('spaceotherProfileName').setText(result.dto.USER_LOGIN_ID)
			    	
			    	if(!result.dto.USER_PHONE){
			    		tds('spaceotherProfileMobileView').setText("—")
			    	}else{
			    		tds('spaceotherProfileMobileView').setText(result.dto.NATIONAL_CODE +" "+result.dto.USER_PHONE)
			    	}
			    
			    	tds('spaceotherProfileMailView').setText(result.dto.USER_EMAIL)
			    	
			    	//기본 이름 값 저장
			    	Top.Controller.get("spaceOtherProfileLayoutLogic").defaultNick = result.dto.USER_LOGIN_ID;
			    	
			    	
			    	//프로필 최적화 숨기기
			    	tds('spaceotherProfileMobileLayout').setProperties({"visible":"visible"})
			    	tds('spaceotherProfileMailLayout').setProperties({"visible":"visible"})
			    	tds('FriendNickLinearLayout').setProperties({"visible":"visible"})
			   

			    },
			    error: function error(_error2) {}
			  });
		};

		mobx.reaction(
			() => FriendListModel.isLoaded.get(),
			isLoaded => {
				if (isLoaded) {
					const FRIEND_ID = FriendListModel.findByLoginUserId(location.href.split('f/')[1]).FRIEND_ID;
					initialize(FRIEND_ID);
				}
			}, 
			{
				fireImmediately: true,
				onError() {
					
				},
			}
		);
		//즐겨찾기 툴팁 이벤트 설정
		document.querySelector('#spaceotherStarLineIcon').addEventListener('mouseover', function (e) {
			
					TeeTooltip.open('즐겨찾기', {
						x: e.clientX, 
						y: e.clientY,
					});
			
			});
    	document.querySelector('#spaceotherStarLineIcon').addEventListener('mouseout', function (e) {
				TeeTooltip.close();	
		});
    	document.querySelector('#spaceotherFullStarIcon').addEventListener('mouseover', function (e) {
			
			TeeTooltip.open('즐겨찾기 해제', {
				x: e.clientX, 
				y: e.clientY,
			});
	
    	});
    	document.querySelector('#spaceotherFullStarIcon').addEventListener('mouseout', function (e) {
    		TeeTooltip.close();	
    	});
	},
	
	gofrinedNick : function(event,widget){
		tds('spaceotherProfileStatusIcon_'+_UserId).setProperties({"visible":"none"})
		Top.Dom.selectById("spaceotherProflileNick").setProperties({"visible": "none"})
		
		tds('otherfriendNickField').setProperties({"visible":"visible"})
		
		Top.Dom.selectById("otherfriendNickField").setText(Top.Dom.selectById("spaceotherProflileNick").getText())
		
		tds('editNickSaveButton').setProperties({"visible":"visible"})
		tds('editNickCancelButton').setProperties({"visible":"visible"})
		Top.Dom.selectById("FriendNickClickIcon").setProperties({"visible":"none"})
		//default 힌트
        tds('otherfriendNickField').setProperties({"hint":Top.Controller.get("spaceOtherProfileLayoutLogic").defaultNick})
        //편집 클릭 시, 별명란 포커스
        tds('otherfriendNickField').focus();
      
		$(function(){
			$('.top-textfield-root #otherfriendNickField').keyup(function(e){	
				var nickLength = $(this).val();
				$('.top-textfield-root #otherfriendNickField').css({"padding-right": "50px"})
		        $('#otherfriendNickField .top-textfield-icon').html(nickLength.length + '/20');
		        $('#otherfriendNickField .top-textfield-icon').css({"display":"inline","font-size":"13px","position":"absolute","top":"20px","right":"10px","color":"#C5C5C8"})
		       
		      });
		      $('.top-textfield-root #otherfriendNickField').keyup();
		});
		
	},
	nickSaveOk : function(event,widget){
		var nicktxt = tds('otherfriendNickField').getText()
		if(nicktxt === null || nicktxt.length === 0){
			nicktxt = Top.Controller.get("spaceOtherProfileLayoutLogic").defaultNick
		}
		 $.ajax({
		        url: _workspace.url+"Users/UpdateFriend?action=", //Service Object
		        type: 'POST',
		        dataType: 'json', 
		        crossDomain: true,
		        contentType: 'application/json; charset=utf-8',
		        xhrFields: {

		            withCredentials: true

		          },
		          data: JSON.stringify({
		                  "dto": {
		                    "USER_ID": userManager.getLoginUserId(),
		                    "FRIEND_ID": _UserId,
		                    "FRIEND_NICK" : nicktxt
		                  }
		                }),
		           success : function(result){
		            //userManager.update();
		        	   tds('otherfriendNickField').setProperties({"visible":"none"})
		        	   tds('editNickSaveButton').setProperties({"visible":"none"})
		        	   tds('editNickCancelButton').setProperties({"visible":"none"})
		        	   tds("spaceotherProflileNick").setProperties({"visible":"visible"})
		        	   tds('FriendNickClickIcon').setProperties({"visible":"visible"})
		        	   tds('spaceotherProflileNick').setText(NameToNick(_UserId))   
		        	   tds('spaceotherProfileStatusIcon_'+_UserId).setProperties({"visible":"visible"})
					   FriendListModel.update(_UserId, {USER_NAME: nicktxt});
		            			               },
		           error: function(error) { 
		                  }
		    });	
	},
	nickCancelBtn : function(event,widget){
		tds('spaceotherProfileStatusIcon_'+_UserId).setProperties({"visible":"visible"})
		tds('otherfriendNickField').setProperties({"visible":"none"})
  	   	tds('editNickSaveButton').setProperties({"visible":"none"})
  	   	tds('editNickCancelButton').setProperties({"visible":"none"})
  	   	tds('FriendNickClickIcon').setProperties({"visible":"visible"})
  	   	tds("spaceotherProflileNick").setProperties({"visible":"visible"})
	},
	//1대1 Talk 라우팅
	otherTalkBtnClick : function(){
		var otherId = [{"USER_ID":_UserId}]
		spaceAPI.createSpace(null, userManager.getLoginUserId(), 'WKS0002', otherId);
	},
	//1대1 Meeting 라우팅
	otherMeetingBtnClick : function(){
		var otherId = [{"USER_ID":_UserId}]
		spaceAPI.createSpace(null, userManager.getLoginUserId(), 'WKS0002', otherId, "MEETING");
	},
	//즐겨찾기 추가
	Other_addFavr : function(event, widget){
		//친구 FAV 업데이트 서비스
		 $.ajax({
		        url: _workspace.url+"Users/UpdateFriend?action=", //Service Object
		        type: 'POST',
		        dataType: 'json', 
		        crossDomain: true,
		        contentType: 'application/json; charset=utf-8',
		        xhrFields: {

		            withCredentials: true

		          },
		          data: JSON.stringify({
		                  "dto": {
		                    "USER_ID": userManager.getLoginUserId(),
		                    "FRIEND_ID": _UserId,
		                    "FRIEND_FAV" : 'FAV0001'
		                  }
		                }),
		           success : function(result){
		        	   	tds('spaceotherStarLineIcon').setProperties({"visible":"none"})
		       			tds('spaceotherFullStarIcon').setProperties({"visible":"visible"})
		       			//즐겨찾기시, 프렌즈 리스트 업데이트
						FriendListModel.fetch(userManager.getLoginUserId());
						   
						TeeToast.open({
							text: '즐겨찾기가 설정되었습니다'
						});
		       			
		            			               },
		           error: function(error) { 
		                  }
		    });	
	},
	//즐겨찾기 삭제
	Other_removeFavr : function(event, widget){
		//친구 FAV 삭제하는 서비스
		 $.ajax({
		        url: _workspace.url+"Users/UpdateFriend?action=", //Service Object
		        type: 'POST',
		        dataType: 'json', 
		        crossDomain: true,
		        contentType: 'application/json; charset=utf-8',
		        xhrFields: {

		            withCredentials: true

		          },
		          data: JSON.stringify({
		                  "dto": {
		                    "USER_ID": userManager.getLoginUserId(),
		                    "FRIEND_ID": _UserId,
		                    "FRIEND_FAV" : 'FAV0002'
		                  }
		                }),
		           success : function(result){
		        		tds('spaceotherFullStarIcon').setProperties({"visible":"none"})
		        		tds('spaceotherStarLineIcon').setProperties({"visible":"visible"})
		        		//즐겨찾기시, 프렌즈 리스트 업데이트
						FriendListModel.fetch(userManager.getLoginUserId());
						TeeToast.open({
							text: '즐겨찾기가 해제되었습니다'
						});
		            			               },
		           error: function(error) { 
		                  }
		    });	
	},
	setFavoriteStatus: function (status) {
		if (status === 'off') {
			tds('spaceotherStarLineIcon').setProperties({"visible":"visible"})
			tds('spaceotherFullStarIcon').setProperties({"visible":"none"})
		} else {
			tds('spaceotherStarLineIcon').setProperties({"visible":"none"})
			tds('spaceotherFullStarIcon').setProperties({"visible":"visible"})
		}
	},


});

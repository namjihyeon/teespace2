function _friendManageProfilePopup(_UserId) {
	  //내가 나의 프로필을 볼때 
	  if (_UserId == userManager.getLoginUserId()) {			 
	  }else{//타인의 프로필 볼 경우
		 if(userManager.isBtoc()){
			  tds("friendManageDialog").open()
		 }else{
			
			 tds("friendManageBtobDialog").open()
		 } 
	  }
}

function _friendManageProfilePopupWithPosition(_UserId, x, y) {
	  var remVal = screen.width / 1366 * 16
	  var windowHeight = window.innerHeight;
	  var windowWidth =  window.innerWidth;
	  var _dialogHeight = 29.35 * remVal;
	  var __dialogHeight = 25.63 * remVal;
	  var dialogWidth = 17.5 * remVal;
	  var _x = x; 
	  var _y = y;
	  var __y = y;

	  if (windowHeight < y + _dialogHeight) {
		  _y = windowHeight - _dialogHeight;
	  }
	  if (windowHeight < y + __dialogHeight) {
		  __y = windowHeight - __dialogHeight;
	  }
	  if(windowWidth < x + dialogWidth ){
		  _x = windowWidth - dialogWidth;
	  }

		  Top.Dom.selectById('friendManageBtobDialog').setProperties({
			    'layout-left': _x + 'px',
			    'layout-top': _y + 'px'
			  });
		  Top.Dom.selectById('friendManageDialog').setProperties({
			    'layout-left': _x + 'px',
			    'layout-top': __y + 'px'
			  });
		  
		  _friendManageProfilePopup(_UserId);	  
	  
	}


Top.Controller.create('friendManageBtobDialogLayoutLogic', {
	init : function(event, widget) {
		
		//프로필 최적화 숨기기
		tds('fmBackgroundLinearLayout').setProperties({"visible":"hidden"})
		tds('fmButtonLayout').setProperties({"visible":"hidden"})
		//증겨찾기 숨기기
		tds('fmFullStarBtobIcon').setProperties({'visible':'none'})
		
		
		
		//아이콘에 이벤트 엮끼
		tds('fmCancelIcon').setProperties({"on-click":"closeDialogBtn"})
		tds('fmFriendAddIcon').setProperties({"on-click":"gofmAdd"})
		tds('fmTalkIcon').setProperties({"on-click":"gofmTalk"})
		tds('fmMeetingIcon').setProperties({"on-click":"gofmMeeting"})
		tds('fmStarLineBtobIcon').setProperties({'on-click':'addFavr'})
		tds('fmFullStarBtobIcon').setProperties({'on-click':'removeFavr'})
		
		$('span#fmStatusIcon').attr('id',"fmStatusIcon_"+_UserId)
		$('top-icon#fmStatusIcon').attr('id',"fmStatusIcon_"+_UserId)
		//상태 툴팁 이벤트 설정
		document.querySelector('span#fmStatusIcon_'+_UserId +' i').addEventListener('mouseover', function (e) {
			if($('span#fmStatusIcon_'+_UserId).css('color') === "rgb(22, 172, 102)"){
				TeeTooltip.open('온라인', {
					x: e.clientX, 
					y: e.clientY,
				});
				}else{
					TeeTooltip.open('오프라인', {
						x: e.clientX, 
						y: e.clientY,
					});
				}
			});
    	document.querySelector('span#fmStatusIcon_'+_UserId +' i').addEventListener('mouseout', function (e) {
				TeeTooltip.close();	
		});
    	//즐겨찾기 툴팁 이벤트 설정
		document.querySelector('#fmStarLineBtobIcon').addEventListener('mouseover', function (e) {
			
					TeeTooltip.open('즐겨찾기', {
						x: e.clientX, 
						y: e.clientY,
					});
			
			});
    	document.querySelector('#fmStarLineBtobIcon').addEventListener('mouseout', function (e) {
				TeeTooltip.close();	
		});
    	document.querySelector('#fmFullStarBtobIcon').addEventListener('mouseover', function (e) {
			
			TeeTooltip.open('즐겨찾기 해제', {
				x: e.clientX, 
				y: e.clientY,
			});
	
    	});
    	document.querySelector('#fmFullStarBtobIcon').addEventListener('mouseout', function (e) {
    		TeeTooltip.close();	
    	});

		$.ajax({
		    url: _workspace.url + "Users/BothProfile",
		    //Service Object
		    type: 'GET',
		    dataType: 'json',
		    crossDomain: true,
		    contentType: 'application/json',
		    xhrFields: {
		      withCredentials: true
		    },
		    data: JSON.stringify({
		      "dto": {
		        "USER_ID": _UserId
		      }
		    //  ttt:  new Date().getTime()
		    }),
		    success: function success(result) {
		    	//status에 _UserId의 uuid 반영 & default 가져오기
		    	if(result.dto.USER_STATUS === "online"){
		    		$('span#fmStatusIcon_'+_UserId).css({'color':'#16AC66'})
		    	}else{
		    		$('span#fmStatusIcon_'+_UserId).css({'color':'#CCCCCC'})
		    	}
		    	
		    	
		    	var fmProfile;
		    	if(!result.dto.THUMB_PHOTO){
		    		fmProfile = userManager.getUserDefaultPhotoURL(result.dto.USER_ID)
		    	}else{
		    		fmProfile = userManager.getUserPhoto(result.dto.USER_ID,'medium', result.dto.THUMB_PHOTO)
		    		//fmProfile = result.dto.THUMB_PHOTO;
		    	}
		    	$('#fmTopIconLayout').after(`<img id="fmImageView" class="${result.dto.USER_ID}" src="${fmProfile}"></img><img class="fmIdCardIcon" src="res/profile/Profile_IDcard.svg"></img>`)
		    	if(!result.dto.BACKGROUND_THUMB){
		    		tds('fmBackgroundLinearLayout').setProperties({"background-image":"url("+getDefaultBackground(result.dto.USER_ID)+")"})
		    		//tds('fmBackgroundLinearLayout').setProperties({"background-image":"res/profile/profile_empty_background.png"})
		    	}else{
		    		//$("div#fmBackgroundLinearLayout").css("background-image", "url('" + result.dto.BACKGROUND_THUMB.replace(/(\r\n|\n|\r)/gm, "") + "')");
		    		$("div#fmBackgroundLinearLayout").css("background-image", "url('" + userManager.getUserPhoto(result.dto.USER_ID,'back', result.dto.BACKGROUND_THUMB).replace(/(\r\n|\n|\r)/gm, "") + "')");	
		    	}

				
		    	tds('fmNameTextView').setText(result.dto.USER_NAME)
		    	tds('fmLoginIdTextView').setText("("+result.dto.USER_LOGIN_ID+")")
		    

		    	var NewPoj = result.dto.USER_JOB != null ? result.dto.USER_JOB : (result.dto.USER_POSITION != null ? result.dto.USER_POSITION : "");
		    	var poj = result.dto.USER_JOB != null ? '·'+result.dto.USER_JOB : (result.dto.USER_POSITION != null ? '·'+result.dto.USER_POSITION : "");
		    	var NewJop="";
		    	var endJop = "";
		    	for(let jop= 1; jop <  NewPoj.split(', ').length-1 ; jop++){
		    		NewJop += result.dto.COMPANY_CODE+"·"+result.dto.ORG_NAME.split(', ')[jop]+"·"+poj.split(', ')[jop]+",<br>" 
		    	    endJop =  result.dto.COMPANY_CODE+"·"+result.dto.ORG_NAME.split(', ')[NewPoj.split(', ').length-1]+"·"+poj.split(', ')[NewPoj.split(', ').length-1] 
		    	}
		    	//소속없을 경우 분기 처리
				    if(NewPoj === "" || poj === ""){
                        tds('fmOfficeTextView').setText("—")
				    }else{
				    	if(NewPoj.split(', ').length === 1){//only one
				    		tds('fmOfficeTextView').setText(result.dto.COMPANY_CODE+"·"+result.dto.ORG_NAME+poj)
				    	}else if(NewPoj.split(', ').length === 2){//two job
				    		tds('fmOfficeTextView').setText(result.dto.COMPANY_CODE+"·"+result.dto.ORG_NAME.split(', ')[0]+poj.split(', ')[0]+",<br>"
				    				+result.dto.COMPANY_CODE+"·"+result.dto.ORG_NAME.split(', ')[1]+"·"+poj.split(', ')[1])
				    	}else if(NewPoj.split(', ').length === 0) {//null
				    		tds('fmOfficeTextView').setText(result.dto.COMPANY_CODE+"·"+result.dto.ORG_NAME+poj)
				    	}else{//three job
				    		tds('fmOfficeTextView').setText(result.dto.COMPANY_CODE+"·"+result.dto.ORG_NAME.split(', ')[0]+poj.split(', ')[0]+",<br>"+NewJop +endJop)
				    		$('span#fmOfficeTextView').css({'height':'2rem'})
				    		tds('fmOfficeTextView').setProperties({"tooltip-top":(result.dto.COMPANY_CODE+"·"+result.dto.ORG_NAME.split(', ')[0]+poj.split(', ')[0]+",<br>"+NewJop + endJop).replace(/<br>/g,"")})
				    	}
				    } 	
	
		    	var nCode;
		    	var mNum;
		    	var cNum;
		    	if(!result.dto.NATIONAL_CODE){
		    		nCode = "+82"
		    	}else{
		    		nCode = result.dto.NATIONAL_CODE
		    	}
		    	if(!result.dto.USER_PHONE){
		    		mNum = "—"
		    	}else{
		    		if(result.dto.USER_PHONE[0] === "0"){
		    			mNum = result.dto.USER_PHONE.replace(result.dto.USER_PHONE[0],"")
		    		}else{
		    			mNum = result.dto.USER_PHONE	
		    		}
		    	}
		    	if(!result.dto.USER_COM_NUM){
		    		cNum = "—";
		    	}else{
		    		if(result.dto.USER_COM_NUM[0] === "0"){
		    			cNum = result.dto.USER_COM_NUM.replace(result.dto.USER_COM_NUM[0],"")
		    		}else{
		    			cNum = result.dto.USER_COM_NUM	
		    		}
		    	}
		    	
		    	tds('fmComNumTextView').setText(nCode + " " + cNum)
		    	tds('fmMobileTextView').setText(nCode +" "+mNum)
		    	if(!result.dto.USER_EMAIL || result.dto.USER_EMAIL === "null"){
		    		tds('fmMailTextView').setText("—")
		    	}else{
			    	tds('fmMailTextView').setText(result.dto.USER_EMAIL)	
		    	}
		    	
		    	//프렌즈인지 아닌지 검사
			 	$.ajax({
			 			url: _workspace.url+'Users/GetFriendList?action=', //Service Object
			 				type: 'POST',
			 				dataType: 'json', 
			 				crossDomain: true,
			 				async: false,
			 				contentType: 'application/json; charset=utf-8',
			 				xhrFields: {

			 					withCredentials: true

			 				},	
			 				data: JSON.stringify({
			 					"dto": {
			 						"USER_ID": userManager.getLoginUserId()
			 					}
			 				}),
			 				success : function(result){
	        	   
	           
			 					var friends = result.dto.dtos;
			 					//친구아님!
			 					if(friends.length  === 0){
			 						tds('fmfriendaddLayout').setProperties({"visible":"visible"})
			 							tds('fmTalkLayout').setProperties({"visible":"none"})
			 							tds('fmMeetingLayout').setProperties({"visible":"none"})
			 							
			 							//프로필 최적화 보이기
			 							tds('fmBackgroundLinearLayout').setProperties({"visible":"visible"})
			 							tds('fmButtonLayout').setProperties({"visible":"visible"})
			 							//친구아닌분들은 즐겨찾기 x
			 							tds('fmStarLineBtobIcon').setProperties({'visible':'hidden'})
			 							return;
			 					}
			 					//친구임!!
			 					for(let fl=0; fl<friends.length; fl++){
			 						if(friends[fl].FRIEND_ID === _UserId){
			 						
			 							tds('fmfriendaddLayout').setProperties({"visible":"none"})
			 							tds('fmTalkLayout').setProperties({"visible":"visible"})
			 							tds('fmMeetingLayout').setProperties({"visible":"visible"})
			 							
			 							//프로필 최적화 보이기			 							
			 							tds('fmBackgroundLinearLayout').setProperties({"visible":"visible"})
			 							tds('fmButtonLayout').setProperties({"visible":"visible"})
			 							
			 							
			 							//즐겨찾기 on인데 이미 즐겨찾기 되어 있으면 상태 변경 해주어야함
			 							if(friends[fl].FRIEND_FAV === 'FAV0001'){
			 								tds('fmStarLineBtobIcon').setProperties({'visible':'none'})
			 								tds('fmFullStarBtobIcon').setProperties({'visible':'visible'})
			 							}else{
			 								tds('fmStarLineBtobIcon').setProperties({'visible':'visible'})
			 							}
			 							
			 					
			 							return;
	        		  	 
			 						}
			 						//찬구아님!
			 						else{
			 						
			 							tds('fmfriendaddLayout').setProperties({"visible":"visible"})
			 							tds('fmTalkLayout').setProperties({"visible":"none"})
			 							tds('fmMeetingLayout').setProperties({"visible":"none"})
			 							
			 							//프로필 최적화 보이기
			 							tds('fmBackgroundLinearLayout').setProperties({"visible":"visible"})
			 							tds('fmButtonLayout').setProperties({"visible":"visible"})
			 							//친구아닌분들은 즐겨찾기 x
			 							tds('fmStarLineBtobIcon').setProperties({'visible':'hidden'})
			 						}
  
			 					}
	           
	            			               			},
	            			error: function(error) { 
	            									}
			 			});
		    	
		    	
		    	
		    	
		    	
		    	
		    	
		    	
		    			    	
		    },
		    error: function error(_error2) {}
		  });
		
		
		
		
	},
	
	closeDialogBtn : function(event, widget){
		tds('friendManageBtobDialog').close(true);
	},
	gofmAdd : function(event, widget){
		//친구 추가하는 서비스
		$.ajax({
			url: _workspace.url + "Users/Add?action=Friend", 
	        type: 'POST',
	        dataType: 'json', 
	        crossDomain: true,
	        async: false,
	        contentType: 'application/json; charset=utf-8',
	        xhrFields: {

	            withCredentials: true

	          },
	          data: JSON.stringify({
	            	dto: {
	            		USER_ID: userManager.getLoginUserId(),
	            		FRIEND_ID: _UserId
	            	}
	            }),
	           success : function(result){
	        	   tds('fmfriendaddLayout').setProperties({"visible":"none"})
	        	   tds('fmTalkLayout').setProperties({"visible":"visible"})
	        	   tds('fmMeetingLayout').setProperties({"visible":"visible"})
	        	   //즐겨찾기 on
			 	   tds('fmStarLineBtobIcon').setProperties({'visible':'visible'})
	        	   TeeToast.open({
						text: `프렌즈로 추가되었습니다.`
					});
	        	   
	        	   FriendListModel.fetch(userManager.getLoginUserId())
	          
	           
	          
	            			               },
	           error: function(error) { 
	                  }
	    });
	},
	gofmTalk : function(event, widget){
		var fmotherId = [{"USER_ID":_UserId}]
		spaceAPI.createSpace(null, userManager.getLoginUserId(), 'WKS0002', fmotherId);
		tds('friendManageBtobDialog').close(true)
		
	},
	gofmMeeting : function(event, widget){
		
		var fmotherId = [{"USER_ID":_UserId}]
		spaceAPI.createSpace(null, userManager.getLoginUserId(), 'WKS0002', fmotherId, "MEETING");
		tds('friendManageBtobDialog').close(true)
	},
	//즐겨찾기 추가
	addFavr : function(event, widget){
		//친구 FAV 업데이트 서비스
		 $.ajax({
		        url: _workspace.url+"Users/UpdateFriend?action=", //Service Object
		        type: 'POST',
		        dataType: 'json', 
		        crossDomain: true,
		        contentType: 'application/json; charset=utf-8',
		        xhrFields: {

		            withCredentials: true

		          },
		          data: JSON.stringify({
		                  "dto": {
		                    "USER_ID": userManager.getLoginUserId(),
		                    "FRIEND_ID": _UserId,
		                    "FRIEND_FAV" : 'FAV0001'
		                  }
		                }),
		           success : function(result){
		        	   	tds('fmStarLineBtobIcon').setProperties({"visible":"none"})
		       			tds('fmFullStarBtobIcon').setProperties({"visible":"visible"})
		       			//즐겨찾기시, 프렌즈 리스트 업데이트
		       			FriendListModel.fetch(userManager.getLoginUserId());
		            
		            			               },
		           error: function(error) { 
		                  }
		    });	
	},
	//즐겨찾기 삭제
	removeFavr : function(event, widget){
		//친구 FAV 삭제하는 서비스
		 $.ajax({
		        url: _workspace.url+"Users/UpdateFriend?action=", //Service Object
		        type: 'POST',
		        dataType: 'json', 
		        crossDomain: true,
		        contentType: 'application/json; charset=utf-8',
		        xhrFields: {

		            withCredentials: true

		          },
		          data: JSON.stringify({
		                  "dto": {
		                    "USER_ID": userManager.getLoginUserId(),
		                    "FRIEND_ID": _UserId,
		                    "FRIEND_FAV" : 'FAV0002'
		                  }
		                }),
		           success : function(result){
		        		tds('fmFullStarBtobIcon').setProperties({"visible":"none"})
		        		tds('fmStarLineBtobIcon').setProperties({"visible":"visible"})
		        		//즐겨찾기시, 프렌즈 리스트 업데이트
		       			FriendListModel.fetch(userManager.getLoginUserId());
		            			               },
		           error: function(error) { 
		                  }
		    });	
	}
});

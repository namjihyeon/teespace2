var _UserId;

function _spaceProfilePopup(_UserId) {
	//서브앱 종료
	tds('AppSplitMainLayout').removeClass('limit');
	tds("AppSplitMainLayout").setProperties({ ratio: '1:0' });
	  //내가 나의 프로필을 볼때 
	  if (_UserId == userManager.getLoginUserInfo().USER_LOGIN_ID) {//나의 프로필 
		  if(userManager.isBtoc()){
			  tds('AppSplitLeftLayout').src('spaceMyProfileLayout.html'+ verCsp())
//			  Top.App.routeTo(`/f/${workspaceManager.getMySpaceUrl()}/${getMainAppByUrl()}`);
			 }else{
				tds('AppSplitLeftLayout').src('spaceBtobMyProfileLayout.html'+ verCsp())
//				 Top.App.routeTo(`/f/${workspaceManager.getMySpaceUrl()}/${getMainAppByUrl()}`);
			 }
			 
	  }else{//타인의 프로필 볼 경우
		 if(userManager.isBtoc()){
			  tds('AppSplitLeftLayout').src('spaceOtherProfileLayout.html'+ verCsp())
//			  Top.App.routeTo(`/f/${workspaceManager.getMySpaceUrl()}/${getMainAppByUrl()}`);
		 }else{
			 tds('AppSplitLeftLayout').src('spaceBtobOtherProfileLayout.html'+ verCsp())
//			 Top.App.routeTo(`/f/${workspaceManager.getMySpaceUrl()}/${getMainAppByUrl()}`);
		 }
		 
		  
	  }
	
	
}


function toMyTalk(){
//	let sub = getSubAppByUrl()? `?sub=${appManager.getSubApp()}` : ``;
//	Top.App.routeTo(`/s/${workspaceManager.getMySpaceUrl()}/talk${sub}`);
	changeSpace(workspaceManager.getMySpaceUrl())
}




Top.Controller.create('spaceMyProfileLayoutLogic', {
	init : function(event, widget) {
		//창모드 시 
		var remVal = screen.width / 1366 * 16
		var hRem = 3.19 * remVal;
		$('div#Layout130').css({'min-height': screen.availHeight - (window.outerHeight - window.innerHeight)  - hRem})
		
		tds('profileEditButton').setProperties({"on-click":"editBtnClick"})		
		tds('profileTalkButton').setProperties({"on-click":"gotoMyTalk"})
		
		
		//프로필 최적화 숨기기
		tds('LinearLayout59').setProperties({"visible":"hidden"})
		tds('spaceProfileMobileLayout').setProperties({"visible":"hidden"})
		tds('spaceProfileMailLayout').setProperties({"visible":"hidden"})
		
		
		//상태 툴팁 이벤트 설정
		document.querySelector('span#spaceProfileStatusIcon i').addEventListener('mouseover', function (e) {
			if($('span#spaceProfileStatusIcon i').css('color') === "rgb(22, 172, 102)"){
				TeeTooltip.open('온라인', {
					x: e.clientX, 
					y: e.clientY,
				});
				}else{
					TeeTooltip.open('오프라인', {
						x: e.clientX, 
						y: e.clientY,
					});
				}
			});
    	document.querySelector('span#spaceProfileStatusIcon i').addEventListener('mouseout', function (e) {
				TeeTooltip.close();	
		});
		
		
		//////////////////////////////////////////////////////////b2c
	
			$.ajax({
			    url: _workspace.url + "Users/BothProfile",
			    //Service Object
			    type: 'GET',
			    dataType: 'json',
			    crossDomain: true,
			    contentType: 'application/json',
			    xhrFields: {
			      withCredentials: true
			    },
			    data: JSON.stringify({
			      "dto": {
			        "USER_ID": userManager.getLoginUserId()
			      }
			    //  ttt:  new Date().getTime()
			    }),
			    success: function success(result) {
			    	if(!result.dto.THUMB_PHOTO) {
		              tds('spaceProfileImageView').setSrc(userManager.getUserDefaultPhotoURL(userManager.getLoginUserId()));
		            }else{
		              Top.Dom.selectById("spaceProfileImageView").setSrc(userManager.getUserPhoto(userManager.getLoginUserId(),'medium', result.dto.THUMB_PHOTO));
		            }
			    	if(!result.dto.BACKGROUND_THUMB){
			    		tds('spaceMyProfileLayout').setProperties({"background-image":"url("+getDefaultBackground(userManager.getLoginUserId())+")"})
			    	}else{
			    		tds('spaceMyProfileLayout').setProperties({"background-image":"url("+userManager.getUserPhoto(userManager.getLoginUserId(),'back', result.dto.BACKGROUND_THUMB)+")"})
			    	}
			    	
			    	if(result.dto.USER_NICK){
			    		tds('spaceProflileNick').setText(result.dto.USER_NICK)
			    	}else{tds('spaceProflileNick').setText(result.dto.USER_LOGIN_ID)}
			    	
			    	tds('spaceProfileName').setText(result.dto.USER_LOGIN_ID)
			    	

			    	var nCode;
			    	if(!result.dto.NATIONAL_CODE){
			    		nCode = "+82"
			    	}else{
			    		nCode = result.dto.NATIONAL_CODE
			    	}
			    	var phoneNum;
			    	if(!result.dto.USER_PHONE){
			    		phoneNum = "—"
			    	}else{
			    		if(result.dto.USER_PHONE[0] === "0"){
			    			phoneNum = result.dto.USER_PHONE.replace(result.dto.USER_PHONE[0],"")
			    		}else{
				    		phoneNum = result.dto.USER_PHONE	
			    		}
			    	}

			    	tds('spaceProfileMobileView').setText(nCode+" "+ phoneNum)
			    	tds('spaceProfileMailView').setText(result.dto.USER_EMAIL)
			    			    	
			    	
			    	
			    	
			    	//프로필 최적화 보여주기
			    	tds('LinearLayout59').setProperties({"visible":"visible"})
			    	tds('spaceProfileMobileLayout').setProperties({"visible":"visible"})
			    	tds('spaceProfileMailLayout').setProperties({"visible":"visible"})
			    },
			    error: function error(_error2) {}
			  });

	
		
	},
	//프로필 편집 버튼 이벤트
	editBtnClick : function(event,widget){
		$('div#profileEditButton').addClass("BtnSelected")
		
		tds('myProfileContentLayout').src('spaceMyProfileEditLayout.html'+ verCsp())
	},
	gotoMyTalk : function(event, widget){
		$('div#profileEditButton').removeClass("BtnSelected")
		toMyTalk();
	}
});






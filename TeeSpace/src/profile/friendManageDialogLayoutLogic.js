
Top.Controller.create('friendManageDialogLayoutLogic', {
	init : function(event, widget) {
		this.grapNick;
		this.fmdefalutNick;
		
		//프로필 최적화 숨기기
		tds('fmBackgroundLinearLayout_b2c').setProperties({"visible":"hidden"})
		tds('fmButtonLayout_b2c').setProperties({"visible":"hidden"})
		//증겨찾기 숨기기
		tds('fmFullStarIcon').setProperties({'visible':'none'})
		
		
		
		//아이콘에 이벤트 엮끼
		tds('fmCancelIcon_b2c').setProperties({"on-click":"closeDialogBtn_b2c"})
		tds('fmFriendAddIcon_b2c').setProperties({"on-click":"gofmAdd_b2c"})
		tds('fmTalkIcon_b2c').setProperties({"on-click":"gofmTalk_b2c"})
		tds('fmMeetingIcon_b2c').setProperties({"on-click":"gofmMeeting_b2c"})
		tds('fmEditNickIcon_b2c').setProperties({"on-click":"goEditNick_b2c"})
		tds('fmStarLineIcon').setProperties({'on-click':'addFavr_b2c'})
		tds('fmFullStarIcon').setProperties({'on-click':'removeFavr_b2c'})
		//status에 _UserId의 uuid 반영 
		$('span#fmStatusIcon_b2c').attr('id',"fmStatusIcon_b2c_"+_UserId)
		$('top-icon#fmStatusIcon_b2c').attr('id',"fmStatusIcon_b2c_"+_UserId)
		//상태 툴팁 이벤트 설정
		document.querySelector('span#fmStatusIcon_b2c_'+_UserId +' i').addEventListener('mouseover', function (e) {
			if($('span#fmStatusIcon_b2c_'+_UserId).css('color') === "rgb(22, 172, 102)"){
				TeeTooltip.open('온라인', {
					x: e.clientX, 
					y: e.clientY,
				});
				}else{
					TeeTooltip.open('오프라인', {
						x: e.clientX, 
						y: e.clientY,
					});
				}
			});
    	document.querySelector('span#fmStatusIcon_b2c_'+_UserId +' i').addEventListener('mouseout', function (e) {
				TeeTooltip.close();	
		});
		
		$.ajax({
		    url: _workspace.url + "Users/BothProfile",
		    //Service Object
		    type: 'GET',
		    dataType: 'json',
		    crossDomain: true,
		    contentType: 'application/json',
		    xhrFields: {
		      withCredentials: true
		    },
		    data: JSON.stringify({
		      "dto": {
		        "USER_ID": _UserId
		      }
		    //  ttt:  new Date().getTime()
		    }),
		    success: function success(result) {
		    	//& default 가져오기
		    	if(result.dto.USER_STATUS === "online"){
		    		$('span#fmStatusIcon_b2c_'+_UserId).css({'color':'#16AC66'})
		    	}else{
		    		$('span#fmStatusIcon_b2c_'+_UserId).css({'color':'#CCCCCC'})
		    	}
		    	
		    	
		    	var fmProfile;
		    	if(!result.dto.THUMB_PHOTO){
		    		fmProfile = userManager.getUserDefaultPhotoURL(result.dto.USER_ID)
		    	}else{
		    		fmProfile = userManager.getUserPhoto(result.dto.USER_ID,'medium', result.dto.THUMB_PHOTO)
		    		//fmProfile = result.dto.THUMB_PHOTO;
		    	}
		    	$('#fmTopIconLayout_b2c').after(`<img id="fmImageView_b2c" class="${result.dto.USER_ID}" src="${fmProfile}"></img>`)
		    	if(!result.dto.BACKGROUND_THUMB){
		    		tds('fmBackgroundLinearLayout_b2c').setProperties({"background-image":"url("+getDefaultBackground(result.dto.USER_ID)+")"})
		    		
		    	}else{
		    	
		    		$("div#fmBackgroundLinearLayout_b2c").css("background-image", "url('" + userManager.getUserPhoto(result.dto.USER_ID,'back', result.dto.BACKGROUND_THUMB).replace(/(\r\n|\n|\r)/gm, "") + "')");	
		    	}
		    	if(NameToNick(result.dto.USER_ID) === null){
		    		var otherNick = result.dto.USER_NICK != null ? result.dto.USER_NICK : result.dto.USER_LOGIN_ID;
		    		tds('fmNameTextView_b2c').setText(otherNick)
		    		Top.Controller.get('friendManageDialogLayoutLogic').grapNick = otherNick;
		    	}else{
		    		tds('fmNameTextView_b2c').setText(NameToNick(result.dto.USER_ID))
		    		Top.Controller.get('friendManageDialogLayoutLogic').grapNick = NameToNick(result.dto.USER_ID)	
		    	}
		    	//내가 정한 닉네임 머금기
		    	//Top.Controller.get('friendManageDialogLayoutLogic').grapNick = NameToNick(result.dto.USER_ID)	
		    
		    	tds('fmLoginIdTextView_b2c').setText(result.dto.USER_LOGIN_ID)
		    	Top.Controller.get('friendManageDialogLayoutLogic').fmdefalutNick = result.dto.USER_LOGIN_ID

	
		    	var nCode;
		    	var mNum;

		    	if(!result.dto.NATIONAL_CODE){
		    		nCode = "+82"
		    	}else{
		    		nCode = result.dto.NATIONAL_CODE
		    	}
		    	if(!result.dto.USER_PHONE){
		    		mNum = "—"
		    	}else{
		    		if(result.dto.USER_PHONE[0] === "0"){
		    			mNum = result.dto.USER_PHONE.replace(result.dto.USER_PHONE[0],"")
		    		}else{
		    			mNum = result.dto.USER_PHONE	
		    		}
		    	}

		    	tds('fmMobileTextView_b2c').setText(nCode +" "+mNum)
		    	if(!result.dto.USER_EMAIL || result.dto.USER_EMAIL === "null"){
		    		tds('fmMailTextView_b2c').setText("—")
		    	}else{
			    	tds('fmMailTextView_b2c').setText(result.dto.USER_EMAIL)	
		    	}
		    	
		    	//프렌즈인지 아닌지 검사
			 	$.ajax({
			 			url: _workspace.url+'Users/GetFriendList?action=', //Service Object
			 				type: 'POST',
			 				dataType: 'json', 
			 				crossDomain: true,
			 				async: false,
			 				contentType: 'application/json; charset=utf-8',
			 				xhrFields: {

			 					withCredentials: true

			 				},	
			 				data: JSON.stringify({
			 					"dto": {
			 						"USER_ID": userManager.getLoginUserId()
			 					}
			 				}),
			 				success : function(result){
	        	   
	           
			 					var friends = result.dto.dtos;
			 					
			 					if(friends.length  === 0){
			 						//친구가 단 한명도 없을때
			 						tds('fmfriendaddLayout_b2c').setProperties({"visible":"visible"})
			 							tds('fmTalkLayout_b2c').setProperties({"visible":"none"})
			 							tds('fmMeetingLayout_b2c').setProperties({"visible":"none"})
			 							//친구가 아닐때 정보 가려주기
			 							tds('fmMobileLayout_b2c').setProperties({"visible":"none"})
			 							tds('fmEditNickIcon_b2c').setProperties({"visible":"none"})
			 					
			 							//프로필 최적화 보이기
			 							tds('fmBackgroundLinearLayout_b2c').setProperties({"visible":"visible"})
			 							tds('fmButtonLayout_b2c').setProperties({"visible":"visible"})
			 							//친구아닌분들은 즐겨찾기 x
			 							tds('fmStarLineIcon').setProperties({'visible':'hidden'})
			 							
			 							return;
			 					}
			 					for(let fl=0; fl<friends.length; fl++){
			 						if(friends[fl].FRIEND_ID === _UserId){
			 						
			 							tds('fmfriendaddLayout_b2c').setProperties({"visible":"none"})
			 							tds('fmTalkLayout_b2c').setProperties({"visible":"visible"})
			 							tds('fmMeetingLayout_b2c').setProperties({"visible":"visible"})
			 							//친구일 때  정보 보여주기
			 							tds('fmMobileLayout_b2c').setProperties({"visible":"visible"})
			 							tds('fmEditNickIcon_b2c').setProperties({"visible":"visible"})
			 							//프로필 최적화 보이기
			 							tds('fmBackgroundLinearLayout_b2c').setProperties({"visible":"visible"})
			 							tds('fmButtonLayout_b2c').setProperties({"visible":"visible"})
			 							//즐겨찾기 on인데 이미 즐겨찾기 되어 있으면 상태 변경 해주어야함
			 							if(friends[fl].FRIEND_FAV === 'FAV0001'){
			 								tds('fmStarLineIcon').setProperties({'visible':'none'})
			 								tds('fmFullStarIcon').setProperties({'visible':'visible'})
			 							}else{
			 								tds('fmStarLineIcon').setProperties({'visible':'visible'})
			 							}
			 							
			 							
			 							
			 							
			 							return;
	        		  	 
			 						}else{
			 							//친구 아닐때
			 							tds('fmfriendaddLayout_b2c').setProperties({"visible":"visible"})
			 							tds('fmTalkLayout_b2c').setProperties({"visible":"none"})
			 							tds('fmMeetingLayout_b2c').setProperties({"visible":"none"})
			 							//친구가 아닐때 정보 가려주기
			 							tds('fmMobileLayout_b2c').setProperties({"visible":"none"})
			 							tds('fmEditNickIcon_b2c').setProperties({"visible":"none"})
			 							//프로필 최적화 보이기
			 							tds('fmBackgroundLinearLayout_b2c').setProperties({"visible":"visible"})
			 							tds('fmButtonLayout_b2c').setProperties({"visible":"visible"})
			 							//친구아닌분들은 즐겨찾기 x
			 							tds('fmStarLineIcon').setProperties({'visible':'hidden'})
			 						}
  
			 					}
	           
	            			               			},
	            			error: function(error) { 
	            									}
			 			});

		    },
		    error: function error(_error2) {}
		  });
		//즐겨찾기 툴팁 이벤트 설정
		document.querySelector('#fmStarLineIcon').addEventListener('mouseover', function (e) {
			
					TeeTooltip.open('즐겨찾기', {
						x: e.clientX, 
						y: e.clientY,
					});
			
			});
    	document.querySelector('#fmStarLineIcon').addEventListener('mouseout', function (e) {
				TeeTooltip.close();	
		});
    	document.querySelector('#fmFullStarIcon').addEventListener('mouseover', function (e) {
			
			TeeTooltip.open('즐겨찾기 해제', {
				x: e.clientX, 
				y: e.clientY,
			});
	
    	});
    	document.querySelector('#fmFullStarIcon').addEventListener('mouseout', function (e) {
    		TeeTooltip.close();	
    	});
		
		
	},

	closeDialogBtn_b2c : function(event, widget){
		tds('friendManageDialog').close(true);
	},

	gofmAdd_b2c : function(event, widget){
	//친구 추가하는 서비스
		$.ajax({
			url: _workspace.url + "Users/Add?action=Friend", 
			type: 'POST',
			dataType: 'json', 
			crossDomain: true,
			async: false,
			contentType: 'application/json; charset=utf-8',
			xhrFields: {

				withCredentials: true

			},
			data: JSON.stringify({
            		dto: {
            			USER_ID: userManager.getLoginUserId(),
            			FRIEND_ID: _UserId
            		}
            	}),
            	success : function(result){
            		tds('fmfriendaddLayout_b2c').setProperties({"visible":"none"})
            		tds('fmTalkLayout_b2c').setProperties({"visible":"visible"})
            		tds('fmMeetingLayout_b2c').setProperties({"visible":"visible"})
            		tds('fmMobileLayout_b2c').setProperties({"visible":"visible"})
			 		tds('fmEditNickIcon_b2c').setProperties({"visible":"visible"})
			 		//즐겨찾기 on
			 		tds('fmStarLineIcon').setProperties({'visible':'visible'})
            		TeeToast.open({
            			text: `프렌즈로 추가되었습니다.`
            		});
            		FriendListModel.fetch(userManager.getLoginUserId());
        	
            			               },
           error: function(error) { 
                  }
		});
	},
	
	gofmTalk_b2c : function(event, widget){
		var fmotherId = [{"USER_ID":_UserId}]
		spaceAPI.createSpace(null, userManager.getLoginUserId(), 'WKS0002', fmotherId);
		tds('friendManageDialog').close(true)
	
	},
	gofmMeeting_b2c : function(event, widget){
	
		var fmotherId = [{"USER_ID":_UserId}]
		spaceAPI.createSpace(null, userManager.getLoginUserId(), 'WKS0002', fmotherId, "MEETING");
		tds('friendManageDialog').close(true)
	},
	goEditNick_b2c : function(event,widget){
		//저장 취소 버튼 생성
		  let btnWrap = htmlToElement(  
				  `<div class ="profpop-btn">
				  	<button id = "profpop_saveBtn" class = "solid">저장</button>
				  	<button id = "profpop_canceBtn" class = "outlined">취소</button>
				  </div>`
		  );
		//닉네임 텍스트 필드 생성
		  let nickWrap = htmlToElement(
				  `<div class = "profpop-field">
				  	<input class = "profpop_nick"  maxlength="20" value ="${Top.Controller.get('friendManageDialogLayoutLogic').grapNick}" placeholder="${Top.Controller.get('friendManageDialogLayoutLogic').fmdefalutNick}"><span id = "profpop_nick_length"></span></input>
				  </div>`
		  );
		//별명 글자수 세는 함수
     	 $(function () {
 		      $('.profpop_nick').keyup(function (e) {
 		        var nickLength = $(this).val();
// 		   
 		       
 		        $('#profpop_nick_length').html(nickLength.length + '/20');
 		        $('#profpop_nick_length').css({
 		          "display": "inline",
 		          "font-size": "0.69rem",
 		          "position":"relative",
 		          "right":"2rem",
 		          "color": "#FFFFFF",
 		          "opacity": "0.75"
 		        });
 		      });
 		      $('.profpop_nick').keyup();
 		    });
     	 
		  //html 갈아끼기
		  $('div#fmButtonLayout_b2c').append(btnWrap)
		  tds('fmTalkLayout_b2c').setProperties({"visible":"none"})
		  tds('fmMeetingLayout_b2c').setProperties({"visible":"none"})
		  $('div#toChangeLayout').append(nickWrap)
		  tds('fmStatusLayout_b2c').setProperties({"visible":"none"})
		 //별명 설정 시, 별명란 포커스
		  $('input.profpop_nick').focus();
		 
		  //저장 버튼 클릭 이벤트 
		  document.querySelector('.profpop-btn #profpop_saveBtn').addEventListener('click', function(){
			  var nicktxt = $('input.profpop_nick').val();
			  if(nicktxt === null || nicktxt.length === 0){
				  nicktxt = Top.Controller.get('friendManageDialogLayoutLogic').fmdefalutNick
			  }
				
				 $.ajax({
				        url: _workspace.url+"Users/UpdateFriend?action=", //Service Object
				        type: 'POST',
				        dataType: 'json', 
				        crossDomain: true,
				        contentType: 'application/json; charset=utf-8',
				        xhrFields: {

				            withCredentials: true

				          },
				          data: JSON.stringify({
				                  "dto": {
				                    "USER_ID": userManager.getLoginUserId(),
				                    "FRIEND_ID": _UserId,
				                    "FRIEND_NICK" : nicktxt
				                  }
				                }),
				           success : function(result){
				        	
				            //userManager.update();
				        	  tds('fmTalkLayout_b2c').setProperties({"visible":"visible"})
				 		      tds('fmMeetingLayout_b2c').setProperties({"visible":"visible"})
				 		      $('div.profpop-btn').remove()
				 		      tds('fmStatusLayout_b2c').setProperties({"visible":"visible"})
				 		      $('div.profpop-field').remove() 
				        	   tds('fmNameTextView_b2c').setText(NameToNick(_UserId))  
				        	   $('span#spaceotherProflileNick').text(NameToNick(_UserId))
				        	   //grapNick 반영
				        	   Top.Controller.get('friendManageDialogLayoutLogic').grapNick = nicktxt;
							   FriendListModel.update(_UserId, {USER_NAME: nicktxt});
							   talk.updateUserNick(_UserId,nicktxt );
				            			               },
				           error: function(error) { 
				                  }
				    });	
		  });
		  //취소 버튼 클릭 이벤트
		  document.querySelector('.profpop-btn #profpop_canceBtn').addEventListener('click', function(){
			  tds('fmTalkLayout_b2c').setProperties({"visible":"visible"})
		      tds('fmMeetingLayout_b2c').setProperties({"visible":"visible"})
		       $('div.profpop-btn').remove()
		       tds('fmStatusLayout_b2c').setProperties({"visible":"visible"})
		       $('div.profpop-field').remove() 
		     
		  });
	},
	//즐겨찾기 추가
	addFavr_b2c : function(event, widget){
		//친구 FAV 업데이트 서비스
		 $.ajax({
		        url: _workspace.url+"Users/UpdateFriend?action=", //Service Object
		        type: 'POST',
		        dataType: 'json', 
		        crossDomain: true,
		        contentType: 'application/json; charset=utf-8',
		        xhrFields: {

		            withCredentials: true

		          },
		          data: JSON.stringify({
		                  "dto": {
		                    "USER_ID": userManager.getLoginUserId(),
		                    "FRIEND_ID": _UserId,
		                    "FRIEND_FAV" : 'FAV0001'
		                  }
		                }),
		           success : function(result){
		        	   	tds('fmStarLineIcon').setProperties({"visible":"none"})
		       			tds('fmFullStarIcon').setProperties({"visible":"visible"})
		       			//즐겨찾기시, 프렌즈 리스트 업데이트
		       			FriendListModel.fetch(userManager.getLoginUserId());
		            
		            			               },
		           error: function(error) { 
		                  }
		    });	
	},
	//즐겨찾기 삭제
	removeFavr_b2c : function(event, widget){
		//친구 FAV 삭제하는 서비스
		 $.ajax({
		        url: _workspace.url+"Users/UpdateFriend?action=", //Service Object
		        type: 'POST',
		        dataType: 'json', 
		        crossDomain: true,
		        contentType: 'application/json; charset=utf-8',
		        xhrFields: {

		            withCredentials: true

		          },
		          data: JSON.stringify({
		                  "dto": {
		                    "USER_ID": userManager.getLoginUserId(),
		                    "FRIEND_ID": _UserId,
		                    "FRIEND_FAV" : 'FAV0002'
		                  }
		                }),
		           success : function(result){
		        		tds('fmFullStarIcon').setProperties({"visible":"none"})
		        		tds('fmStarLineIcon').setProperties({"visible":"visible"})
		        		//즐겨찾기시, 프렌즈 리스트 업데이트
		       			FriendListModel.fetch(userManager.getLoginUserId());
		            			               },
		           error: function(error) { 
		                  }
		    });	
	}
});













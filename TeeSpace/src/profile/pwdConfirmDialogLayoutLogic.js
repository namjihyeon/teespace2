
Top.Controller.create('pwdConfirmDialogLayoutLogic', {
	init : function(event, widget) {
		tds('toChangePwdOkButton').setProperties({"on-click":"confirmOk"})
		tds('clickChangePwdOkButton').setProperties({"on-click":"toChangePw"})
		tds('TextField287').focus();
	},
	confirmOk : function(event, widget){
		
		let idp_save = userManager.getLoginUserInfo().USER_LOGIN_ID;
		var pwp_save = Top.Dom.selectById("TextField287").getText()
		
		$.ajax({
            url: _workspace.url+'Users/User?action=AuthCheck', //Service Object
            type: 'POST',
            dataType: 'json',
            crossDomain: true,
            contentType: 'application/json; charset=utf-8',
            xhrFields: {
                withCredentials: true
              },
            data: JSON.stringify({
              "dto": {
                "USER_LOGIN_ID": idp_save.trim(),
                "USER_PW": SHA256(idp_save.trim()+pwp_save.trim())
              }
            }),
            success: function(result) {
            	if(result.dto.RESULT_CD == "RST0001"){
            		Top.Dom.selectById("toChangePwdOkButton").setProperties({"disabled":"false"});
            		tds('toChangePwdOkButton').removeClass('disabled')
        			tds('toChangePwdOkButton').addClass('solid')
            		Top.Dom.selectById("TextField287").removeClass("alert");
            		Top.Dom.selectById("TextField287").addClass("solid");
            		Top.Dom.selectById("pwdConfirmErrorIcon").setProperties({"visible":"none"});
            		tds('pwdConfirmDialogLayout').src('changeMobileLayout.html'+ verCsp())
            	}
            	else{
                	Top.Dom.selectById("toChangePwdOkButton").setProperties({"disabled":"true"})
                	tds('toChangePwdOkButton').removeClass('solid')
                	tds('toChangePwdOkButton').addClass('disabled')
    	        	Top.Dom.selectById("TextField287").removeClass("solid");
        			Top.Dom.selectById("TextField287").addClass("alert");
    	        	Top.Dom.selectById("pwdConfirmErrorIcon").setProperties({"visible":"visible"});
    	        	$('#pwdConfirmErrorIcon.icon-chart_error.linear-child-horizontal').css({"visibility":"hidden"})
    	        	$('#TextField287 .top-textfield-icon').addClass('changepwd-alert')
    	        	setTimeout(function() {
    					Top.Dom.selectById("CurPw2Popover").open();
    					//$('.top-popover').css({"left":"9rem"})
    					//$('.top-popover').css({"top":"4rem"})
    					//$('.top-popover-arrow').css({'left':'90%'})
    				}, 100);
            		
            	}
	        	
	        },
	        error: function(error) {    	
	    		Top.Dom.selectById("toChangePwdOkButton").setProperties({"disabled":"true"})
	    		tds('toChangePwdOkButton').removeClass('solid')
               	tds('toChangePwdOkButton').addClass('disabled')
	        	Top.Dom.selectById("TextField287").removeClass("solid");
    			Top.Dom.selectById("TextField287").addClass("alert");
	        	Top.Dom.selectById("pwdConfirmErrorIcon").setProperties({"visible":"visible"});
	        	$('#pwdConfirmErrorIcon.icon-chart_error.linear-child-horizontal').css({"visibility":"hidden"})
	        	$('#TextField287 .top-textfield-icon').addClass('changepwd-alert')
	        	setTimeout(function() {
					Top.Dom.selectById("CurPw2Popover").open();
					//$('.top-popover').css({"left":"9rem"})
					//$('.top-popover').css({"top":"4rem"})
					//$('.top-popover-arrow').css({'left':'90%'})
				}, 100);
	        	
	    
	        }
			});		
	},
	showpw : function(event, widget) {
		let pw = tds('TextField287')
		if(pw.getProperties("password")){
			pw.setProperties({"password": "false", "icon": "icon-ic_password_show"})
		}else{
			pw.setProperties({"password": "true", "icon": "icon-ic_password_hide"})
		}
	},
	pwdTextKeyup : function(event, widget) {
		tds('TextField287').removeClass('alert')
		$('#TextField287 .top-textfield-icon').removeClass('changepwd-alert')
		tds('pwdConfirmErrorIcon').setProperties({"visible":"none"})
		
		
		let idp_save = userManager.getLoginUserInfo().USER_LOGIN_ID;
		var pwp_save = Top.Dom.selectById("TextField287").getText();
		var k_2 = new Array(2);
		
		if(pwp_save.length >= 9 && pwp_save.length <= 20){
			k_2[0]=1;
		}
		else {}
		if((pattern_num.test(pwp_save)) && (pattern_engs.test(pwp_save)) && (pattern_spc2.test(pwp_save))){
			k_2[1]=1;
		}
		else if((pattern_num.test(pwp_save)) && (pattern_engs.test(pwp_save)) && (pattern_engb.test(pwp_save))){
			k_2[1]=1;
		}
		else if((pattern_engs.test(pwp_save)) && (pattern_spc2.test(pwp_save)) && (pattern_engb.test(pwp_save))){
			k_2[1]=1;
		}
		else{}
		if( k_2[0]==1 && k_2[1]==1){
			Top.Dom.selectById("toChangePwdOkButton").setProperties({"disabled":"false"});
			tds('toChangePwdOkButton').removeClass('disabled')
			tds('toChangePwdOkButton').addClass('solid')
		}else{
			Top.Dom.selectById("toChangePwdOkButton").setProperties({"disabled":"true" })
			tds('toChangePwdOkButton').removeClass('solid')
			tds('toChangePwdOkButton').addClass('disabled')
		}
	},
	toChangePw : function(event,widget){
		
		
	}
	
});

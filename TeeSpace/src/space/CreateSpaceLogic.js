const SpaceAPI = {
	InviteMembers: function (wsId, senderId, userList) {
		 return axios.post( _workspace.url + 'SpaceRoom/SpaceRoomMember', {
			 dto: {
				WS_ID: wsId, 
				SENDER_ID: senderId,
				WsUserList: userList
			 }
		 });
	},
}

InviteMemberDialog = function () {
	let _alreadyInvitedList = undefined;
	let _config = {};
	let wsId = null;
	let mode = 'create'; // create | invite

	function Dialog() {
		const collator = new Intl.Collator('kr', {numeric: true, sensitivity: 'base'});
		const userInfo = JSON.parse(sessionStorage.getItem('userInfo'));
		const model = {
			chips: mobx.observable.array([]),
			filter: mobx.observable.box(''),
			tab: mobx.observable.box(''),
			selectedOrg: mobx.observable.box({}),
			selectedOrgName: mobx.observable.box(''),
			search: mobx.observable.box(''),
			userList: mobx.observable.array([]),
			filteredUserList: mobx.observable.array([]),
		};

		this.model = model;
		this.userInfo = userInfo;
		this.collator = collator;
		this.env = isb2c() ? 'b2c' : 'b2b';

		this.initialize();
	}

	Dialog.prototype.onUserListClicked = function (e) {
		const model = this.model;
		e.preventDefault();
		e.stopPropagation();
		const node = e.target.closest('li');
		const profileImageNode = e.target.closest('img');

		if (node) {
			// 기획 변경으로 인하여 임시로 주석 처리 
			// if (profileImageNode) {
				// const userId = node.getAttribute('data-user-id');

				// if (profileImageNode) {
				// 	_UserId = userId;
				// 	_friendManageProfilePopupWithPosition(_UserId, e.clientX, e.clientY);
				// }
			// } else {
				if (!node.classList.contains('disabled')) {
					const isChecked = node.classList.contains('checked');
					if (isChecked) {
						const FRIEND_ID = node.data.FRIEND_ID ? node.data.FRIEND_ID : node.data.USER_ID;
						const index = model.chips.findIndex(v => 
							v.FRIEND_ID ? v.FRIEND_ID === FRIEND_ID : v.USER_ID === FRIEND_ID
						);
						model.chips.splice(index, 1);
					} else {
						model.chips.push(node.data);
					}
				}
			// }
		}
	}

	Dialog.prototype.DrawUserList = function (parentTemplate, data = {
		filteredFriends: [],
		alreadyInvitedFriendIds: [],
	}) {
		const self = this;
		const model = this.model;
		const collator = this.collator;
		const env = this.env;
		const userInfo = this.userInfo;
		
		let filteredFriends = data.filteredFriends;
		filteredFriends.sort((a, b) => collator.compare(a.USER_NAME, b.USER_NAME));

		const container = parentTemplate.querySelector('ul.friend-list');
		const config = {
			itemHeight: screen.width / 13.66 / 100 * 3 * 16,
			total: filteredFriends.length > 0 ? filteredFriends.length : 1, 
			scrollerTagName: 'div',
			generate: function (index) {
				if (filteredFriends.length === 0) {
					const noneTemplate = htmlToElement(`
							<li class="none">
								검색 결과가 없습니다.
							</li>
					`);
					return noneTemplate;
				}

				const friend = filteredFriends[index];

				const friendTemplate = htmlToElement(`
						<li>
							<img />
							<span class="name"></span>
							<div class="checkbox"><i class="icon-check"></i></div>
						</li>
				`);
				
				const FRIEND_ID = friend.FRIEND_ID ? friend.FRIEND_ID : friend.USER_ID;

				friendTemplate.setAttribute('data-user-id', FRIEND_ID);

				if ( (data.alreadyInvitedFriendIds && data.alreadyInvitedFriendIds.includes(FRIEND_ID)) || FRIEND_ID === userInfo.userId) {
					friendTemplate.classList.add('disabled');
				}
				
				if (model.chips.findIndex(v => v.FRIEND_ID ? v.FRIEND_ID === FRIEND_ID : v.USER_ID === FRIEND_ID) > -1) {
					friendTemplate.classList.add('checked');
				}
				
				let THUMB_PHOTO = friend.THUMB_PHOTO;
				
				if (!THUMB_PHOTO || THUMB_PHOTO.length === 0) {
					THUMB_PHOTO = userManager.getUserDefaultPhotoURL(FRIEND_ID)
				} else {
					THUMB_PHOTO = userManager.getUserPhoto(FRIEND_ID, "small", friend.THUMB_PHOTO);
				}
				
				friendTemplate.querySelector('img').src = THUMB_PHOTO;
				if (env === 'b2c') {
					friendTemplate.querySelector('span.name').textContent = friend.USER_NAME; 
				} else {
					let orgInfo = '';
					if (friend.USER_JOB && friend.USER_POSITION) {
						orgInfo = `(${friend.USER_JOB}-${friend.USER_POSITION})`;
					} else if (friend.USER_JOB && !friend.USER_POSITION) {
						orgInfo = `(${user.USER_JOB})`;
					} else if (!friend.USER_JOB && friend.USER_POSITION) {
						orgInfo = `(${friend.USER_POSITION})`;
					}

					friendTemplate.querySelector('span.name').textContent = `${friend.USER_NAME} ${orgInfo}`;
				}
				
				friendTemplate.data = friend;

				return friendTemplate;
			},
		};

		if (!this.list) {
			this.list = HyperList.create(container, config);
		} else {
			this.list.refresh(container, config);
		}

		parentTemplate.querySelector('span.highlight').textContent = filteredFriends.length + '명';
	}

	Dialog.prototype.FriendsListView = function() {
		const self = this;
		const model = this.model;
		const filteredUserList = mobx.observable.array([]);

		FriendListModel.fetch(userManager.getLoginUserId());

		const placeholder = isb2c() ? '별명 검색' : '이름 검색';

		const template = htmlToElement(`
			<div class="friend-view-container c-loading">
				<div class="friend-search">
					<top-icon class="icon-search tee-search-icon"></top-icon>
					<input type="text" placeholder="${placeholder}" />
				</div>
				<div class="friend-list-container">
					<div class="favorite-friend-container">
						<div class="friend-container-title favorite">
							<span class="head">즐겨찾기</span>
						</div>
						<ul class="favorite-friend-list tee-table">
						</ul>
					</div>
					<div class="friend-container">
						<div class="friend-container-title friends">
							<span class="head">프렌즈</span>
							<span class="highlight"></span>
						</div>
						<ul class="friend-list tee-table">
						</ul>
					</div>
				</div>
				<div class="empty-container">
					<span class="title">프렌즈가 없습니다.</span>
					<span class="body">조직도에서 초대할 멤버를 찾아보세요</span>
					<img src="res/illust/none_friend.png" />
					<button class="solid add-friend">프렌즈 추가</button>
				</div>
			</div>
		`);
		InitializeTeeSearch(template.querySelector('.friend-search'));

		template.addEventListener('click', this.onUserListClicked.bind(this));

		template.querySelector('button.add-friend').addEventListener('click', function (e) {
			e.preventDefault();
			document.querySelector('div#top-dialog-root_CreateSpaceDialog').style.display = 'none';
			AddFriendsDialog.open({
				onClose: function () {
					document.querySelector('div#top-dialog-root_CreateSpaceDialog').style.display = 'flex';
				},
			});
		});

		const userInfo = this.userInfo;
		const drawUserList = (friend, alreadyInvitedFriendIds) => {
			const friendTemplate = htmlToElement(`
				<li>
					<img />
					<span class="name"></span>
					<div class="checkbox"><i class="icon-check"></i></div>
				</li>
			`);
			
			const FRIEND_ID = friend.FRIEND_ID ? friend.FRIEND_ID : friend.USER_ID;

			friendTemplate.setAttribute('data-user-id', FRIEND_ID);

			if ( (alreadyInvitedFriendIds && alreadyInvitedFriendIds.includes(FRIEND_ID)) || FRIEND_ID === userInfo.userId) {
				friendTemplate.classList.add('disabled');
			}
			
			if (model.chips.findIndex(v => v.FRIEND_ID ? v.FRIEND_ID === FRIEND_ID : v.USER_ID === FRIEND_ID) > -1) {
				friendTemplate.classList.add('checked');
			}
			
			let THUMB_PHOTO = friend.THUMB_PHOTO;
			
			if (!THUMB_PHOTO || THUMB_PHOTO.length === 0) {
				THUMB_PHOTO = userManager.getUserDefaultPhotoURL(FRIEND_ID)
			} else {
				THUMB_PHOTO = userManager.getUserPhoto(FRIEND_ID, "small", friend.THUMB_PHOTO);
			}
			
			friendTemplate.querySelector('img').src = THUMB_PHOTO;
			if (isb2c()) {
				friendTemplate.querySelector('span.name').textContent = friend.USER_NAME; 
			} else {
				let orgInfo = '';
				if (friend.USER_JOB && friend.USER_POSITION) {
					orgInfo = `(${friend.USER_JOB}-${friend.USER_POSITION})`;
				} else if (friend.USER_JOB && !friend.USER_POSITION) {
					orgInfo = `(${user.USER_JOB})`;
				} else if (!friend.USER_JOB && friend.USER_POSITION) {
					orgInfo = `(${friend.USER_POSITION})`;
				}

				friendTemplate.querySelector('span.name').textContent = `${friend.USER_NAME} ${orgInfo}`;
			}
			
			friendTemplate.data = friend;

			return friendTemplate;
		};

		// 이미 초대 된 사용자는 제외
		const _alreadyInvitedFriendIds = _alreadyInvitedList !== undefined ? _alreadyInvitedList.map(v => v.FRIEND_ID) : [];
		
		mobx.reaction(
			() => model.filter.get(),
			filter => {
				let friends = FriendListModel.list;
				
				let filteredFriends = friends.filter(v => filter.length === 0 ? true : v.USER_NAME ? v.USER_NAME.indexOf(filter) > -1 : false);
				filteredUserList.replace(filteredFriends);
			}
		);

		mobx.reaction(
			() => FriendListModel.isLoaded.get(),
			isLoaded => {
				if (isLoaded) {
					filteredUserList.replace(FriendListModel.list);
					template.classList.remove('c-loading');

					if (filteredUserList.length === 0) {
						template.classList.add('empty');
						if (isb2c()) {
							template.classList.remove('empty');
							template.querySelector('.empty-container span.title').textContent = '';
							template.querySelector('.empty-container span.body').textContent = '';
						} else {
							template.querySelector('.empty-container span.title').textContent = '프렌즈가 없습니다.';
							template.querySelector('.empty-container span.body').textContent = '조직도에서 초대할 멤버를 찾아보세요';
						}
	
						template.querySelector('.favorite-friend-container').style.display = 'none';
					} else {
						template.classList.remove('empty');
					}

				} else {
					template.classList.add('c-loading');
				}
			},
			{
				fireImmediately: true
			}
		);

		mobx.reaction(
			() => filteredUserList.slice(), 
			obj => {
				let friends = FriendListModel.list;
				let filter = model.filter.get();

				template.querySelector('.favorite-friend-list').innerHTML = '';
				template.querySelector('.friend-list').innerHTML = '';
				
				let filteredFriends = friends.filter(v => filter.length === 0 ? true : v.USER_NAME ? v.USER_NAME.indexOf(filter) > -1 : false);
	
				const favoriteFriendList = filteredFriends.filter(v => v.FRIEND_FAV === 'FAV0001');
				
				filteredFriends.forEach( friend => {
					const el = drawUserList(friend,  _alreadyInvitedFriendIds);
					template.querySelector('.friend-list').appendChild(el);
				});

				if (favoriteFriendList.length === 0 || filter.length > 0) {
					template.querySelector('.favorite-friend-container').style.display = 'none';
					template.classList.add('only-friend');
				} else {
					favoriteFriendList.forEach( friend => {
						const el = drawUserList(friend,  _alreadyInvitedFriendIds);
						template.querySelector('.favorite-friend-list').appendChild(el);
					});

					template.classList.remove('only-friend');
					template.querySelector('.favorite-friend-container').style.display = 'block';
				}

				template.querySelector('span.highlight').textContent = filteredFriends.length;
			},
			{
				fireImmediately: true
			}
		);

		mobx.reaction(
			() => model.chips.length, 
			length => {
				template.querySelectorAll('ul.favorite-friend-list li').forEach(li => {
					const data = li.data;
					const isExist = model.chips.findIndex(v => v.FRIEND_ID ? v.FRIEND_ID === data.FRIEND_ID : v.USER_ID === data.FRIEND_ID) > -1;
					if (isExist) {
						li.classList.add('checked');
					} else {
						li.classList.remove('checked');
					}
				});

				template.querySelectorAll('ul.friend-list li').forEach(li => {
					const data = li.data;
					const isExist = model.chips.findIndex(v => v.FRIEND_ID ? v.FRIEND_ID === data.FRIEND_ID : v.USER_ID === data.FRIEND_ID) > -1;
					if (isExist) {
						li.classList.add('checked');
					} else {
						li.classList.remove('checked');
					}
				});

				const filteredUserListIds = model.filteredUserList.map(v => v.FRIEND_ID ? v.FRIEND_ID : v.USER_ID)
																  .filter(v => v !== userManager.getLoginUserId()) // 본인 제외
																  .filter(v => !_alreadyInvitedFriendIds.includes(v)); // 이미 초대된 멤버 제외 
				const chipUserListIds = model.chips.map(v => v.FRIEND_ID ? v.FRIEND_ID : v.USER_ID);

				const difference = filteredUserListIds.filter(v => !chipUserListIds.includes(v));
				if (difference.length === 0) {
					self.checkedAll();
				} else {
					self.uncheckedAll();
				}
			},
			{
				onError(e) {
					
				}
			}
		);

		template.querySelector('input').addEventListener('input', function (e) {
			const str = e.target.value.trim();
			model.filter.set(str);
		});

		return template;
	}

	Dialog.prototype.OrganizationListView = function() {
		const self = this;
		const userInfo = JSON.parse(sessionStorage.getItem('userInfo'));
		const model = this.model;

		const resetSearch = function () {
			model.search.set('');
			template.querySelector('input[name=search]').value = '';
		};
		
		const template = htmlToElement(`
			<div id="add-friends-by-organization">
				<div class="org-search-container">
					<div class="search-input-container">
						<top-icon class="icon-search tee-search-icon"></top-icon>
						<input type="text" name="search" placeholder="팀 이름, 조직원 이름 검색" />
					</div>
				</div>
				<div class="org-member-list">
					<div class="org-info">
						<div class="org-select">
							<span class="name"></span>
							<top-icon class="handle icon-arrow_potab_down"></top-icon>
						</div>
						<span class="highlight"></span>
						<div class="checkbox check-all"><i class="icon-check"></i></div>
					</div>
					<ul class="friend-list tee-table">
					</ul>
				</div>
			</div>
		`);

		InitializeTeeSearch(template.querySelector('.search-input-container'));

		let checkedAll = function () {
			template.querySelector('.check-all').classList.add('checked');
		};

		let uncheckedAll = function () {
			template.querySelector('.check-all').classList.remove('checked');
		};

		template.addEventListener('click', this.onUserListClicked.bind(this));

		template.querySelector('.check-all').addEventListener('click', function (e) {
			const node = e.target.closest('.checkbox');
			if (node) {
				const isChecked = node.classList.contains('checked');
				const filteredUserList = model.filteredUserList.filter(v => {
					const FRIEND_ID = v.FRIEND_ID ? v.FRIEND_ID : v.USER_ID;
					return FRIEND_ID !== userManager.getLoginUserId();
				});

				if (isChecked) {
					const _filteredUserList = filteredUserList.filter(v => {
						const FRIEND_ID = v.FRIEND_ID ? v.FRIEND_ID : v.USER_ID;
						return !_alreadyInvitedFriendIds.includes(FRIEND_ID);
					});

					mobx.transaction(function () {
						_filteredUserList.forEach(function (v) {
							const FRIEND_ID = v.FRIEND_ID ? v.FRIEND_ID : v.USER_ID;
							const index = self.model.chips.findIndex(i => 
								i.FRIEND_ID ? i.FRIEND_ID === FRIEND_ID : i.USER_ID === FRIEND_ID
							);

							self.model.chips.splice(index, 1);
						});
					});
				} else {
					const _filteredUserList = filteredUserList.filter(v => {
						const FRIEND_ID = v.FRIEND_ID ? v.FRIEND_ID : v.USER_ID;
						return !_alreadyInvitedFriendIds.includes(FRIEND_ID);
					});

					mobx.transaction(function () {
						_filteredUserList.forEach(function (v) {
							const FRIEND_ID = v.FRIEND_ID ? v.FRIEND_ID : v.USER_ID;
							const index = self.model.chips.findIndex(i => 
								i.FRIEND_ID ? i.FRIEND_ID === FRIEND_ID : i.USER_ID === FRIEND_ID
							);

							if (index < 0) {
								self.model.chips.push(v);
							}
						});
					});
				}
			}
		});

		template.querySelector('input[name=search]').addEventListener('keydown', function (e) {
			if( !!(~[9 , 13 , 188].indexOf( e.keyCode ))  ) {
				const value = e.target.value;
				model.search.set(value);
			}
		});

		mobx.autorun(() => {
			const selectedOrgName = model.selectedOrgName.get();
			template.querySelector('.org-select span.name').textContent = selectedOrgName;
		});

		mobx.autorun(() => {
			const search = model.search.get();

			if (search.length > 0) {
				let filteredFriends = model.userList.filter(v => 
																v.USER_NAME.indexOf(search) > -1 
																|| v.USER_EMAIL.indexOf(search) > -1
																|| v.USER_PHONE.indexOf(search) > -1
																|| v.USER_COM_NUM.indexOf(search) > -1);

				model.filteredUserList.replace(filteredFriends);					
			} else {
				model.filteredUserList.replace(model.userList);
			}
		});

		mobx.autorun(() => {
			const selectedOrg = model.selectedOrg.get();

			if (selectedOrg.companyCode === undefined) { 
				axios.post(_workspace.url + 'SpaceRoom/OrgUserDept?action=Get', {
					dto: {
						USER_ID: userManager.getLoginUserId(), 
					}
				})
				.then(res => res.data.dto)
				.then(res => axios.post(_workspace.url + 'Admin/OrgPath?action=Get', {
					dto: {
						COMPANY_CODE: res.COMPANY_CODE,
						DEPARTMENT_CODE: res.DEPARTMENT_CODE, 
						ORG_TYPE: 'ADM0021'
					}
				}).then(path => {
					const pathArray = path.data.dto.ORG_NAME.split(' > ');

					const orgName = pathArray.length > 1 ? pathArray[pathArray.length - 1] : pathArray[0];
					model.selectedOrgName.set(orgName);

					model.selectedOrg.set({
						companyCode: res.COMPANY_CODE,
						departmentCode: res.DEPARTMENT_CODE,
						orgName: orgName
					});
				}))
				.catch(err => {});
			} else {
				axios.post(_workspace.url + 'SpaceRoom/OrgUserList?action=Get', {
					dto: {
						COMPANY_CODE: selectedOrg.companyCode,
						DEPARTMENT_CODE: selectedOrg.departmentCode,
						USER_ID: userManager.getLoginUserId()
					}
				})
				.then(res => res.data.dto)
				.then(res => {
					if (res.UserProfileList === null) {
						model.userList.replace([]);
					} else {
						model.userList.replace(res.UserProfileList);
					}
					model.filteredUserList.replace(model.userList);
				})
				.catch(err => {});
			}
		});

		const _alreadyInvitedFriendIds = _alreadyInvitedList !== undefined ? _alreadyInvitedList.map(v => v.FRIEND_ID) : [];

		mobx.reaction(
			() => model.filteredUserList.slice(),
			obj => {
				self.DrawUserList(template, {
					filteredFriends: model.filteredUserList,
					alreadyInvitedFriendIds: _alreadyInvitedFriendIds,
				});	

				const filteredUserListIds = model.filteredUserList.map(v => v.FRIEND_ID ? v.FRIEND_ID : v.USER_ID)
																  .filter(v => v !== userManager.getLoginUserId()) // 본인 제외
																  .filter(v => !_alreadyInvitedFriendIds.includes(v)); // 이미 초대된 멤버 제외 
				const chipUserListIds = self.model.chips.map(v => v.FRIEND_ID ? v.FRIEND_ID : v.USER_ID);

				const difference = filteredUserListIds.filter(v => !chipUserListIds.includes(v));
				if (filteredUserListIds.length > 0 && difference.length === 0) {
					checkedAll();
				} else {
					uncheckedAll();
				}
			}
		)

		mobx.reaction(
			() => self.model.chips.length, 
			length => {
				template.querySelectorAll('ul.friend-list li').forEach(li => {
					const data = li.data;
					const FRIEND_ID = data.FRIEND_ID ? data.FRIEND_ID : data.USER_ID;
					const isExist = self.model.chips.findIndex(v => v.FRIEND_ID ? v.FRIEND_ID === FRIEND_ID : v.USER_ID === FRIEND_ID) > -1;
					if (isExist) {
						li.classList.add('checked');
					} else {
						li.classList.remove('checked');
					}
				})
			}
		);

		template.querySelector('.org-select').addEventListener('click', function (e) {
			const containerRect = template.querySelector('.org-select').getBoundingClientRect();
			const target = template.querySelector('.org-select .handle');
			const selectedOrg = model.selectedOrg.get();

			template.querySelector('.org-select').classList.add('active');
			if (target.classList.contains('icon-arrow_potab_down')) {
				target.classList.remove('icon-arrow_potab_down');
				target.classList.add('icon-arrow_potab_up');

				OrgTreeSelectView.open(
					{
						x: containerRect.left + 'px',
						y: containerRect.bottom + 5 + 'px',
						width: '20.4rem',
						height: '18.44rem',
					},
					{
						onItemClicked: function (data, orgNameTree) {
							resetSearch();

							model.selectedOrg.set(data);
							target.classList.remove('icon-arrow_potab_up');
							target.classList.add('icon-arrow_potab_down');

							if (orgNameTree.length > 1) {
								model.selectedOrgName.set(orgNameTree[orgNameTree.length-1]);
							} else {
								model.selectedOrgName.set(orgNameTree[0]);
							}

							template.querySelector('.org-select').classList.remove('active');
						},
						onClosed: function () {
							target.classList.remove('icon-arrow_potab_up');
							target.classList.add('icon-arrow_potab_down');
							template.querySelector('.org-select').classList.remove('active');
						},
						selectedId: `${selectedOrg.companyCode}.${selectedOrg.departmentCode}`
					}
				);
			} else {
				target.classList.remove('icon-arrow_potab_up');
				target.classList.add('icon-arrow_potab_down');
				template.querySelector('.org-select').classList.remove('active');
				OrgTreeSelectView.close();
			}
			
		});

		this.checkedAll = checkedAll;
		this.uncheckedAll = uncheckedAll;

		return template;
	}

	Dialog.prototype.TabContainerView = function () {
		const collator = this.collator;
		const userInfo = this.userInfo;
		const model = this.model;

		const friendsListView = this.FriendsListView();
		const organizationListView = this.OrganizationListView();

		model.tab.set('tab-friend-list');

		const template = htmlToElement(`
			<div class="tab-view-container">
				<div id="add-friends-tabs" class="tabs-container">
					<div class="add-friends-tab selected" id="tab-friend-list">
						프렌즈 목록
					</div>
					<div class="add-friends-tab" id="tab-organization">
						조직도 조회
					</div>
				</div>
				<div class="tab-content-container">
				</div>
			</div>
		`);

		template.querySelector('.tabs-container').addEventListener('click', function (e) {
			const target = e.target.closest('.add-friends-tab');

			if (template.querySelector('.selected')) {
				template.querySelector('.selected').classList.remove('selected');
			}
			target.classList.add('selected');
			
			 model.tab.set(target.getAttribute('id'));
		});

		mobx.autorun(function () {
	        let tabId = model.tab.get();

	        const firstChild = template.querySelector('.tab-content-container').firstChild;
	        
	        switch (tabId) {
	        case 'tab-friend-list':
	        	template.querySelector('.tab-content-container').replaceChild(friendsListView, firstChild);
	        	break;
			case 'tab-organization':
				template.querySelector('.tab-content-container').replaceChild(organizationListView, firstChild);
				break;
	        }
	    });

		return template;
	}

	Dialog.prototype.initialize = function () {
		const self = this;
		const userInfo = this.userInfo;
		const model = this.model;

		const template = htmlToElement(`
			<div id="create-space-container">
				<div class="create-space-inner">
					<div class="left-container">
					</div>
					<div class="right-container">
					</div>
				</div>
				<div class="option-container">
					<div class="room-name-checkbox disabled">
						<p>초기 설정하지 않을 시, 멤버 별명으로 나열된 룸이 개설되며,<br/>이후 변경한 룸 이름은 개인에게만 적용됩니다.</p>
						<div id="room-name-form">
							<input maxlength="50" type="text" class="outlined" name="room-name" placeholder="그룹의 목적, 토픽 등이 있다면 입력해 주세요" disabled />
							<span class="room-name-length">0/50</span>
						</div>
					</div>
					<div class="meeting-checkbox"></div>
				</div>
				<div class="no-friends b2c">
					<h3>아직 프렌즈가 없어요.</h3>
					<p>룸을 만들려면<br/>먼저 초대할 프렌즈를 찾아 주세요.</p>
					<img src="res/illust/none_friend.png" />
					<button class="solid add-friend">프렌즈 추가</button>
				</div>
				<div class="container-bottom">
					<button class="solid invite">초대</button>
					<button class="cancel">취소</button>
				</div>
			</div>
		`);

		document.querySelector('div#top-dialog-root_CreateSpaceDialog span.top-dialog-title').textContent = _config.title;
		template.querySelector('button.invite').textContent = _config.confirmedButtonText;

		template.querySelector('button.add-friend').addEventListener('click', function (e) {
			e.preventDefault();
			document.querySelector('div#top-dialog-root_CreateSpaceDialog').style.display = 'none';
			AddFriendsDialog.open({
				onClose: function () {
					document.querySelector('div#top-dialog-root_CreateSpaceDialog').style.display = 'flex';
				},
			});
		});

		if (this.env === 'b2c') {
			template.querySelector('.left-container').appendChild(this.FriendsListView());
			document.querySelector('div#top-dialog-root_CreateSpaceDialog').classList.add('b2c');
		} else {
			template.querySelector('.left-container').appendChild(this.TabContainerView());
			document.querySelector('div#top-dialog-root_CreateSpaceDialog').classList.add('b2b');
		}

		// 미팅 체크박스
		const isMeetingCheckBox = Top.Widget.create('top-checkbox');
		isMeetingCheckBox.setProperties({
			id: 'is-meeting'
		});
		isMeetingCheckBox.template.querySelector('label > span').textContent = '초대 멤버와 바로 TeeMeeting 시작하기';

		// 룸 이름 설정 체크박스 
		const isRoomNameCheckBox = Top.Widget.create('top-checkbox');
		isRoomNameCheckBox.setProperties({
			id: 'is-room-name',
			disabled: 'true',
		});
		isRoomNameCheckBox.template.querySelector('label > span').innerHTML = '<img src="res/icon/system/ts_error_enabled.svg" class="enabled" /><img src="res/icon/system/ts_error_disabled.svg" class="disabled" />그룹 이름 설정하기';
		isRoomNameCheckBox.template.querySelector('label').classList.add('checked');
		isRoomNameCheckBox.setChecked(true);

		// '그룹 이름 설정하기' 미체크 시 이름 변경 불가 
		template.querySelector('input[name=room-name]').addEventListener('focus', (e) => {
			const isChecked = isRoomNameCheckBox.getState();
			if (!isChecked) {
				e.target.blur();
			}
		});

		// TODO: _config.hideTeeMeetingCheckbox 용도 변경 필요 (옵션 박스 사용 유무)
		if (mode === 'create' && _config.hideTeeMeetingCheckbox === false) {
			// 티미팅 체크 박스 추가 
			template.querySelector('.meeting-checkbox').appendChild(isMeetingCheckBox.template);

			// 룸 이름 설정하기 추가
			const roomNameContainer = template.querySelector('.room-name-checkbox');

			roomNameContainer.prepend(isRoomNameCheckBox.template);

			const nameInputValidator = function (e) {
				let length = template.querySelector('input[name=room-name]').value.length;
				if (length > 50) length = 50;
				template.querySelector('span.room-name-length').textContent = length + '/50';
			};
			
			template.querySelector('input[name=room-name]').addEventListener('input', nameInputValidator);
		} else {
			template.querySelector('.option-container').remove();
		}

		const optionContainer = template.querySelector('.option-container');
		const roomNameCheckboxContainer = template.querySelector('.room-name-checkbox');

		const chipContainer = template.querySelector('div.right-container');
		
		chipContainer.addEventListener('click', function (e) {
			const target = e.target.closest('top-icon');

			if (target) {
				const data = target.parentNode.data;
				const isExist = self.model.chips.filter(v => v.FRIEND_ID ? v.FRIEND_ID === data.FRIEND_ID : v.USER_ID === data.FRIEND_ID).length > 0;
				if (isExist) {
					const index = self.model.chips.findIndex(v => v.FRIEND_ID ? v.FRIEND_ID === data.FRIEND_ID : v.USER_ID === data.FRIEND_ID);
					self.model.chips.splice(index, 1);
				}
			}
		});

		const mergedInviteUserList = () => {
			let list = _alreadyInvitedList.map(v => {
				let _v = v;
				_v.type = 'invited';
				return _v;
			});

			if (model.chips.length > 0) {
				const arr = model.chips.map(v => {
					return {
						USER_ID: v.USER_ID,
						FRIEND_ID: v.FRIEND_ID, 
						THUMB_PHOTO: v.THUMB_PHOTO,
						USER_NAME: v.USER_NAME,
					};
				});
				list = list.concat(arr);
			}

			return list;
		};
		
		let allList = [];

		const config = {
			itemHeight: screen.width / 13.66 / 100 * 1.88 * 16 + screen.width / 13.66 / 100 * 0.19 * 16,
			width: null,
			paddingTop: 0.56,
			total: allList.length, 
			scrollerTagName: 'div',
			generate: function (index) {
				const invited = allList[index];
				
				const chipTemplate = htmlToElement(`
					<div class="chip">
						<img />
						<span class="name"></span>
						<top-icon class=""></top-icon>
					</div>
				`);

				const FRIEND_ID = invited.FRIEND_ID ? invited.FRIEND_ID : invited.USER_ID;
				
				let THUMB_PHOTO = invited.THUMB_PHOTO;
				
				if (!THUMB_PHOTO || THUMB_PHOTO.length === 0) {
					THUMB_PHOTO = userManager.getUserDefaultPhotoURL(FRIEND_ID)
				} else {
					THUMB_PHOTO = userManager.getUserPhoto(FRIEND_ID, "small", invited.THUMB_PHOTO);
				}
				
				if (invited.type === 'invited') {
					chipTemplate.classList.add('none');
					chipTemplate.querySelector('top-icon').classList.add('icon-work_check');
				} else {
					chipTemplate.querySelector('top-icon').classList.add('icon-work_cancel');
					chipTemplate.data = {
						FRIEND_ID: FRIEND_ID
					};
				}
				chipTemplate.setAttribute('data-user-id', FRIEND_ID);

				chipTemplate.querySelector('img').src = THUMB_PHOTO;
				chipTemplate.querySelector('span.name').textContent = invited.USER_NAME;
				
				return chipTemplate;
			},
		};

		const disposeLoading = mobx.autorun(() => {
			const isLoaded = FriendListModel.isLoaded.get();
			if (isLoaded) {
				document.querySelector('div#top-dialog-root_CreateSpaceDialog').classList.remove('c-loading');
				InviteMemberDialog.center();
				disposeLoading();
			}
		});

		mobx.reaction(
			() => FriendListModel.list.length,
			length => {
				if (length > 0) {
					document.querySelector('div#top-dialog-root_CreateSpaceDialog').classList.remove('empty');
				} else {
					document.querySelector('div#top-dialog-root_CreateSpaceDialog').classList.add('empty');
				}
			},
			{
				fireImmediately: true,
				onError(e) {

				}
			}
		)

		mobx.reaction(
			() => model.chips.length,
			length => {
				allList = mergedInviteUserList();
				config.total = allList.length;

				if (!this.chipList) {
					this.chipList = HyperList.create(chipContainer, config);
				} else {
					this.chipList.refresh(chipContainer, config);
				}

				if (allList.length > 0) {
					chipContainer.classList.remove('empty');	
				} else {
					this.chipList.destroy();
					this.chipList = null;

					const noneTemplate = htmlToElement(`
						<div class="empty-container">
							<span class="title">초대한 멤버가 없습니다.</span>
							<span class="desc">프렌즈를 선택하세요.</span>
						</div>
					`);
					chipContainer.appendChild(noneTemplate);
					chipContainer.classList.add('empty');	
				} 
				
				// 초대 버튼 활성화 및 비활성화 여부 
				if (model.chips.length === 0) {
					template.querySelector('button.invite').disabled = true;
				} else {
					template.querySelector('button.invite').disabled = false;
				}

				// 룸 이름 설정 체크박스 활성화 및 비활성화 여부
				// 기획 상 두 명 이상부터 활성화
				if (optionContainer && model.chips.length >= 2) {
					if (roomNameCheckboxContainer) {
						roomNameCheckboxContainer.classList.remove('disabled');
						roomNameCheckboxContainer.querySelector('input[type=text]').removeAttribute('disabled');
					}
					isRoomNameCheckBox.setProperties({
						disabled: 'false',
					});
					
				} else {
					if (roomNameCheckboxContainer) {
						roomNameCheckboxContainer.classList.add('disabled');
						roomNameCheckboxContainer.querySelector('input[type=text]').setAttribute('disabled', 'true');
					}
					isRoomNameCheckBox.setProperties({
						disabled: 'true',
					});
				}
			},{
				fireImmediately: true
			}
		);
		
		template.querySelector('button.invite').addEventListener('click', function (e) {
			Dialog.close(true);

			if (_config.onConfirmedButtonClicked !== null && typeof _config.onConfirmedButtonClicked === 'function') {
				const members = model.chips.map(v => {
					return {
						USER_ID: v.FRIEND_ID ? v.FRIEND_ID : v.USER_ID,
						USER_NAME: v.USER_NAME,
						THUMB_PHOTO: v.THUMB_PHOTO,
						USER_EMAIL: v.USER_EMAIL,
					};
				});

				_config.onConfirmedButtonClicked(members, _config.hideTeeMeetingCheckbox === false ? isMeetingCheckBox.getState() : undefined);
				return false;
			}			

			const invites = model.chips.map(v => {
				return {
					USER_ID: v.FRIEND_ID ? v.FRIEND_ID : v.USER_ID
				};
			});

			TeeToast.open({
				text: `${invites.length}명의 멤버가 초대되었습니다.`
			});

			if (mode === 'create') {
				const isMeeting = isMeetingCheckBox.getState();
				const isRoomName = isRoomNameCheckBox.getState() && optionContainer && model.chips.length >= 2 ? true : false;

				let roomName = null;
				if (isRoomName) {
					const value = optionContainer.querySelector('input[name=room-name]').value.trim();
					roomName = value.length > 0 ? value : null;
				}

				setTimeout(function () {
					if (isMeeting) {
						spaceAPI.createSpace(roomName, userInfo.userId, 'WKS0002', invites, "MEETING");
					} else {
						spaceAPI.createSpace(roomName, userInfo.userId, 'WKS0002', invites);
					}
				}, 0);
			} else {
				spaceAPI.inviteMember(wsId, invites);
			}
		});
		
		template.querySelector('button.cancel').addEventListener('click', function (e) {
			Dialog.close();
		});		

		this.template = template;

		return this;
	}

	/**
	 * Dialog Methods
	 */
	Dialog.updateUser = function (data) {
		const elements = document.querySelectorAll(`div#top-dialog-root_CreateSpaceDialog [data-user-id='${data.USER_ID}']`);
		
		let THUMB_PHOTO = data.THUMB_PHOTO;
				
		if (!THUMB_PHOTO || THUMB_PHOTO.length === 0) {
			THUMB_PHOTO = userManager.getUserDefaultPhotoURL(data.USER_ID)
		} else {
			THUMB_PHOTO = userManager.getUserPhoto(data.USER_ID, "small", data.THUMB_PHOTO);
		}

		elements.forEach(v => {
			v.querySelector('img').src = THUMB_PHOTO;
		});
	};

	Dialog.open = function (_opts = {}) {
		let opts = {
			alreadyInvitedList: [], 
			wsId: null, 
			title: '멤버 초대',
			onConfirmedButtonClicked: null,
			confirmedButtonText: '초대',
			hideTeeMeetingCheckbox: false,
		};

		Object.assign(opts, _opts);
		Object.assign(_config, opts);

		_alreadyInvitedList = opts.alreadyInvitedList;
		wsId = opts.wsId;

		if (wsId === null) {
			mode = 'create';
		} else {
			mode = 'invite';
		}

		Top.Dom.selectById('CreateSpaceDialog').open();
	};

	Dialog.close = function (opt) {
		Top.Dom.selectById('CreateSpaceDialog').close(opt);
	};
	
	Dialog.center = function () {
		const node = document.querySelector('div#top-dialog-root_CreateSpaceDialog');
		if (node) {
			node.style.left = `calc(50% - ${node.getBoundingClientRect().width/2}px)`;
			console.log('center');
		}
	};
	
	return Dialog;
}();

/**
 * TOP Controller
 */
Top.Controller.create('CreateSpaceLogic', {
	init : function(event, widget) {
		
	}
});

/**
 * Top On Load
 */
Top.App.onLoad(() => {
	Top.Dom.selectById('CreateSpaceDialog').setProperties({
		'on-open': function () {
			let dialog = new InviteMemberDialog();
			document.querySelector('div#CreateSpaceLayout').appendChild(dialog.template);
		},
		'on-close': function () {
			document.querySelector('div#CreateSpaceLayout').innerHTML = '';
		}
	});
});
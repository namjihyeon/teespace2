const spaceManager = (function(){
	
	let _getSpaceProfile = function(SPACE_ID) {
		
		if(!SPACE_ID) return false;
		
		let spaceInfo = workspaceManager.getWorkspace(SPACE_ID)
		
		if(spaceInfo){
			
			let template;
			let SPACE_NAME;
			let USER_LIST = spaceInfo.USER_LIST? spaceInfo.USER_LIST.ROOM_USER_LIST : '';
            let THUMB_PHOTO0 = USER_LIST[0]? userManager.getUserPhoto(USER_LIST[0].USER_ID, "small", USER_LIST[0].THUMB_PHOTO) : ''
            let THUMB_PHOTO1 = USER_LIST[1]? userManager.getUserPhoto(USER_LIST[1].USER_ID, "small", USER_LIST[1].THUMB_PHOTO) : ''
        	let THUMB_PHOTO2 = USER_LIST[2]? userManager.getUserPhoto(USER_LIST[2].USER_ID, "small", USER_LIST[2].THUMB_PHOTO) : ''
            let THUMB_PHOTO3 = USER_LIST[3]? userManager.getUserPhoto(USER_LIST[3].USER_ID, "small", USER_LIST[3].THUMB_PHOTO) : ''
            let THUMB_PHOTO4 = USER_LIST[4]? userManager.getUserPhoto(USER_LIST[4].USER_ID, "small", USER_LIST[4].THUMB_PHOTO) : ''
			
	        if(spaceInfo.WS_NAME){
	        	SPACE_NAME = spaceInfo.WS_NAME
	        }
	        else{
	        	SPACE_NAME = spaceInfo.User_name
	        }
	        if(!spaceInfo.WS_NAME && !spaceInfo.User_name && workspaceManager.getWorkspace(SPACE_ID).WS_TYPE != 'WKS0001') {
           	    SPACE_NAME = userManager.getLoginUserName();
           }
	        
	    	switch (USER_LIST.length){
	    	case 0:
	    		template = '<div class=thumbDiv>'+
	    				   '<img class=thumbPhoto1_1 src={THUMB_PHOTO1}>'+
	    				   '</div>'
	    		template = template.replace('{THUMB_PHOTO1}','')
	    		break;
	    	case 1:
	    		template = '<div class=thumbDiv>'+
	    				   '<img class=thumbPhoto1_1 src={THUMB_PHOTO1}>'+
	    				   '</div>'
	    		template = template.replace('{THUMB_PHOTO1}',THUMB_PHOTO0)
	    		break;
	    	case 2:
	    		template = '<div class=thumbDiv>'+
	    				   '<img class=thumbPhoto1_1 src={THUMB_PHOTO1}>'+
	    				   '</div>'	
	    		template = template.replace('{THUMB_PHOTO1}',THUMB_PHOTO1)
	    		break;
	    	case 3:
	    		template = '<div class=thumbDiv>'+
	    				   '<img class=thumbPhoto1_2 src={THUMB_PHOTO1}>'+
	    				   '<img class=thumbPhoto2_2 src={THUMB_PHOTO2}>'+
	    				   '</div>'	
	    		template = template.replace('{THUMB_PHOTO1}',THUMB_PHOTO1)
	    		.replace('{THUMB_PHOTO2}',THUMB_PHOTO2)
	    		break;
	    	case 4:
	    		template = '<div class=thumbDiv>'+
	    				   '<img class=thumbPhoto1_3 src={THUMB_PHOTO1}>'+
	    				   '<img class=thumbPhoto2_3 src={THUMB_PHOTO2}>'+
	    				   '<img class=thumbPhoto3_3 src={THUMB_PHOTO3}>'+
	    				   '</div>'	
	    		template = template.replace('{THUMB_PHOTO1}',THUMB_PHOTO1)
	    		.replace('{THUMB_PHOTO2}',THUMB_PHOTO2)
	    		.replace('{THUMB_PHOTO3}',THUMB_PHOTO3)
	    		break;
	    	default:
	    		template = '<div class=thumbDiv>'+
	    				   '<img class=thumbPhoto1_4 src={THUMB_PHOTO1}>'+
	    				   '<img class=thumbPhoto2_4 src={THUMB_PHOTO2}>'+
	    				   '<img class=thumbPhoto3_4 src={THUMB_PHOTO3}>'+
	    				   '<img class=thumbPhoto4_4 src={THUMB_PHOTO4}>'+
	    				   '</div>'	
	    		template = template.replace('{THUMB_PHOTO1}',THUMB_PHOTO1)
	    		.replace('{THUMB_PHOTO2}',THUMB_PHOTO2)
	    		.replace('{THUMB_PHOTO3}',THUMB_PHOTO3)
	    		.replace('{THUMB_PHOTO4}',THUMB_PHOTO4)	
	    	}
	        
	        
			


			let THUMB_DIV = template;
			let output = {"SPACE_NAME":SPACE_NAME,"THUMB_DIV":THUMB_DIV}
			
			return output
			
			
		}

	}
	return {
        getSpaceProfile: function (SPACE_ID) {			// 존재하는지 체크 후 업데이트
            return _getSpaceProfile(SPACE_ID);		// 워크스페이스별 유저 조회후 리턴
        }
        	
		
	}

})();

Top.Controller.create('spaceListLayoutLogic', {
	
	
	init : function(event, widget) {
		InitializeTeeSearch(document.querySelector('top-textfield#spaceListSearch'));

		// 버튼 추가 
		const icon = htmlToElement(`
			<svg width="20px" height="20px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
				<g>
					<path d="M18.3302439,1.15989486 C19.1016999,1.56220711 19.1297773,2.30402912 18.8009497,3.09300158 C18.1930588,4.55154454 17.5817953,5.73405954 16.5060482,7.2510173 C16.739757,7.9694937 16.8672003,8.73495105 16.8672003,9.52993822 C16.8672003,13.5822361 13.5558925,16.8672726 9.4711873,16.8672726 C8.61880818,16.8672726 7.80010711,16.7242238 7.03814584,16.4610049 L6.72478816,16.6934387 C3.85425855,18.7962486 2.4415751,19.4616407 1.50110376,18.6880767 C0.457861504,17.8299808 1.06082416,16.3977716 3.14863627,13.3415022 C2.46787602,12.2299657 2.07517431,10.9254483 2.07517431,9.52993822 C2.07517431,5.47764031 5.38648212,2.1926038 9.4711873,2.1926038 C10.8179022,2.1926038 12.0805488,2.54968757 13.1681731,3.17362286 L13.3580888,3.05010101 C14.7448906,2.15821314 15.8388565,1.54606949 16.6176575,1.24556541 C17.3097518,0.978517534 17.8300686,0.899054786 18.3302439,1.15989486 Z M4.45887749,14.9253851 L4.41559125,14.9910913 C4.11795229,15.4457245 3.86560776,15.8526311 3.65890655,16.2108062 L3.49816721,16.4955144 L3.50865522,16.4898735 C3.95154613,16.2275614 4.48054647,15.8793575 5.09351841,15.4461203 C4.87264876,15.2834131 4.66057788,15.1098942 4.45887749,14.9253851 Z M9.49134039,14.5561965 L9.07292702,14.8963649 C9.2044355,14.9058333 9.33724439,14.9106501 9.4711873,14.9106501 C12.4666378,14.9106501 14.8949302,12.5016233 14.8949302,9.52993822 L14.8915066,9.32852147 C13.4471202,11.0303693 11.6630499,12.7745614 9.49134039,14.5561965 Z M9.4711873,4.14922631 C6.47573683,4.14922631 4.04744444,6.55825309 4.04744444,9.52993822 C4.04744444,11.5294888 5.1468557,13.2742954 6.77869982,14.2018778 L6.32840712,14.54362 C9.82252759,11.9176167 12.4068411,9.39469638 14.2533952,6.9916563 C13.340389,5.2996803 11.541214,4.14922631 9.4711873,4.14922631 Z M16.4747185,3.47223492 L16.3243999,3.55364776 C15.8895907,3.79005335 15.3817384,4.09101293 14.8094251,4.45121967 C15.0516128,4.70196734 15.2765335,4.9700561 15.481624,5.25323094 C15.8104417,4.73879808 16.0837029,4.25645927 16.3312001,3.76607772 L16.4747185,3.47223492 Z"></path>
					<circle id="Oval" opacity="0.400000006" cx="7.5" cy="7.5" r="1.5"></circle>
					<circle id="Oval" opacity="0.400000006" cx="10" cy="6" r="1"></circle>
				</g>
			</svg>
		`);
		document.querySelector('button#createSpaceButton').insertBefore(icon, document.querySelector('button#createSpaceButton').firstChild);

		this.isedit = false;

		this.contextMenuTemp = new ContextMenu();
		const contextMenu = Top.Controller.get('spaceListLayoutLogic').contextMenuTemp
		contextMenu.addItem('룸 상단 고정', function(e) {
			if ($('#' + showmemClickid + ' .icon-work_pin').css('display') == 'none') {
				$('div#favSpaceLayout').prepend($('#' + showmemClickid));
				jQuery.ajax({
					type: "PUT",
					url: _workspace.url + "SpaceRoom/SpaceRoomMember",
					dataType: "json",
					data: JSON.stringify({
						"dto": {
							"WS_ID": showmemClickid,
							"USER_ID": userManager.getLoginUserId(),
							"WS_BOOKMARK": 'COM0005'
						}
					}),
					contentType: "application/json",
					success: function (result) {
						spaceAPI.setBookMark(showmemClickid, "COM0005");						
					}
				})
				$('#' + showmemClickid + ' .icon-work_pin').css({'display' : 'block'});
			}
			else {
				$('div#ourSpaceLayout').prepend($('#' + showmemClickid));
				jQuery.ajax({
					type: "PUT",
					url: _workspace.url + "SpaceRoom/SpaceRoomMember",
					dataType: "json",
					data: JSON.stringify({
						"dto": {
							"WS_ID": showmemClickid,
							"USER_ID": userManager.getLoginUserId(),
							"WS_BOOKMARK": 'COM0006'
						}
					}),
					contentType: "application/json",
					success: function (result) {
						spaceAPI.setBookMark(showmemClickid, "COM0006");
					}
				})
				$('#' + showmemClickid + ' .icon-work_pin').css({'display' : 'none'});
			}
			
			contextMenu.hide();
			
		});
		
		contextMenu.addItem('멤버 보기', function(e) {
			Top.Controller.get('spaceListLayoutLogic').isedit = false;
			contextMenu.hide();
			// if(!workspaceManager.getWorkspace(showmemClickid).WS_ACCESS_TYPE){
				Top.Controller.get('spaceListLayoutLogic').isedit = false;
				Top.Dom.selectById('ShowMemDialog').open();
			// }
			// else{
			// 	spaceAPI.getSpaceMemberListBrief(showmemClickid).then(result => {
			// 			for(var i = 0; i < result.data.dto.UserProfileList.length; i++){
			// 				if(result.data.dto.UserProfileList[i].USER_ID != userManager.getLoginUserId()){
			// 					_UserId = result.data.dto.UserProfileList[i].USER_ID
			// 					break;
			// 				}
			// 			}
			// 			_friendManageProfilePopupWithPosition(_UserId, 800 , 50)
			// 		});
			// } 

		});
		contextMenu.addItem('이름 변경', function(e) {
			Top.Controller.get('spaceListLayoutLogic').isedit = true;
			Top.Dom.selectById('ShowMemDialog').open();
			contextMenu.hide();
		});
		contextMenu.addItem('알림 끄기', function(e) {
			if($('#'+showmemClickid+' .icon-work_noti_off').css('display') == 'none'){ 
				
				$('#'+showmemClickid+' .icon-work_noti_off').css({"display":"block"})
				jQuery.ajax({
					type: "PUT",
					url: _workspace.url + "SpaceRoom/SpaceRoomMember",
					dataType : "json",
					data : JSON.stringify({
						"dto" :{
							"WS_ID":showmemClickid,
							"USER_ID" : userManager.getLoginUserId(),
							"WS_ALARM_YN": 'COM0002'
						}
					}),
					contentType: "application/json",
					success: function (result) {
					}
				})
				let temp={};
				temp.WS_ID = showmemClickid;
				temp.USER_ID = userManager.getLoginUserId(); temp.CH_ALARM_CODE = "space_all",temp.RESULT_MSG = "insert";	
				setAlarm(temp);
			}
			else {
				
				$('#'+showmemClickid+' .icon-work_noti_off').css({"display":"none"});
				jQuery.ajax({
					type: "PUT",
					url: _workspace.url + "SpaceRoom/SpaceRoomMember",
					dataType : "json",
					data : JSON.stringify({
						"dto" :{
							"WS_ID":showmemClickid,
							"USER_ID" : userManager.getLoginUserId(),
							"WS_ALARM_YN": 'COM0001'
						}
					}),
					contentType: "application/json",
					success: function (result) {
					}
				})
				let temp={};
				temp.WS_ID = showmemClickid;
				temp.USER_ID = userManager.getLoginUserId(); temp.CH_ALARM_CODE = "space_all",temp.RESULT_MSG = "delete";	
				setAlarm(temp);
			}
			contextMenu.hide();
		});


		contextMenu.addItem('나가기', function(e) {
			Top.Dom.selectById('OutSpaceDialog').open();
			contextMenu.hide();
		});
		

		document.body.appendChild(contextMenu.containerElem);	
		Top.Dom.selectById("spaceListSearch").removeClass('click-placeholder') 

		
/////////////////////////////////////////////////////////////////////////////////////////////////

		let spaceList = spaceAPI.initRoomList();
//        let spaceList = talkServer2.getMessengerRooms(userManager.getLoginUserId())
//        
//        
//		spaceList.ttalkMessengersList.forEach(ele => {
//
//			if(ele.WS_TYPE == 'WKS0001'){	
//            	$('div#mySpaceLayout').append(spaceAPI.createSpaceDiv(ele));
//				$('div#mySpaceLayout #'+ele.WS_ID).addClass('mySpace')
//				$('#mySpaceLayout .icon-ellipsis_vertical_small').remove();
//			}
//			else if(ele.WS_BOOKMARK != 'COM0005'){
//            	$('div#ourSpaceLayout').append(spaceAPI.createSpaceDiv(ele));
//			}
//            else{
//				$('div#favSpaceLayout').append(spaceAPI.createSpaceDiv(ele));
//            }  
//            })	

//		spaceAPI.afterCreateSpaceDiv()
        
	},	
	
	openCreateSpaceDialog : function(event, widget) {
        InviteMemberDialog.open({title: '룸 만들기', confirmedButtonText: '만들기'});
	}, searchSpaceList : function(event, widget) {
	
//		if(event.keyCode === 13){
			  // Declare variables
			  var input, filter, ul, li, a, i, txtValue;
			  input =  document.querySelector('input#spaceListSearch');
			  filter = input.value.toUpperCase();
			  ul = document.querySelector('div#spaceListLayoutBody');
			  li = ul.querySelectorAll('div.lnbSpaceList');

			  // Loop through all list items, and hide those who don't match the search query
			  for (i = 0; i < li.length; i++) {
				    a = li[i].querySelector('div.wsName');
				    txtValue = a.textContent || a.innerText;
				    if (txtValue.toUpperCase().indexOf(filter) > -1) {
				      li[i].style.display = "";
				      if($(li[i]).hasClass('mySpace')){
				    	  li[i].parentElement.style.display = ""
				      }
				    } else {
				      li[i].style.display = "none";
				      if($(li[i]).hasClass('mySpace')){
				    	  li[i].parentElement.style.display = "none" 
				      }
				    }
				  }				
//		}

	}
});

/* 
 * ( 변경 주소, 메인앱, 서브앱 ) 
 * */
function routeUrl(targetUrl = location.hash.slice(2), fixMain, fixSub){
	
	targetUrl = "#!" + targetUrl;
	let roomId = getRoomIdByUrl(targetUrl);	
	let targetMain = getMainAppByUrl(targetUrl);
	let targetMainFull = getMainAppFullUrl(targetUrl);
	let targetSub = getSubAppByUrl(targetUrl);
	let type = appManager.getType(targetMain, targetSub);
	let subUrl = targetSub? `?sub=${targetSub}&q=${getTimeStamp()}` : `?q=${getTimeStamp()}`; 
	let mains = appManager.getMains();
	
	if(mains.includes(targetMain)){
		appManager.setMainApp(targetMain);
		appManager.setSubApp(targetSub);
		
//		if("talk" === targetMain) {
			Top.App.routeTo(`/s/${roomId}/${targetMainFull}${subUrl}`, {eventType: type.eventType});
//		}
//		//for noti
//		else if("mail" === targetMain){
//	    	Top.App.routeTo(`/m/${roomId}/mail?${getTimeStamp()}`, { eventType: 'close' }); 
//		}
	}
	else{
		if(fixMain) appManager.setMainApp(fixMain);
		if(fixSub) appManager.setSubApp(fixSub);
		Top.App.routeTo(`/s/${roomId}/${fixSub}?q=${getTimeStamp()}`, {eventType: type.eventType});
	}
}


function changeSpace(spaceUrl = getRoomIdByUrl()){
	
	let targetSpace = workspaceManager.getWorkspaceUrl(spaceUrl);
	let hst = historyList.get("space", targetSpace);
	let lnbUrl = getLnbByUrl();
	let sub;
	
	if("s"===lnbUrl){
    	let mains = appManager.getMains();
    	let targetMain = mains.includes(getMainAppByUrl())? "talk" : getMainAppFullUrl(); 
    	let targetUrl = location.hash.slice(2)
    	
    	if(hst){
    		sub = ""===hst.APP_INFO.split("/")[1]? null : hst.APP_INFO.split("/")[1];
    		routeUrl(hst.LAST_URL, "talk", sub);
    	}
    	else{
    		routeUrl(`/s/${targetSpace}/talk`, "talk", null);
    	}
			/*  이전 스페이스의 서브앱 
			 * 		let curSpace = getRoomIdByUrl();
			    	targetUrl = targetUrl.replace(curSpace, targetSpace);
			    	targetUrl = targetUrl.replace(getMainAppFullUrl(), targetMain);
			    	routeUrl(targetUrl, "talk", appManager.getSubApp()); */
	}
	else{
		if(hst){
			let main = hst.APP_INFO.split("/")[0];
			sub = ""===hst.APP_INFO.split("/")[1]? null : hst.APP_INFO.split("/")[1];
			routeUrl(hst.LAST_URL, main, sub);
			
		}
		else{
			routeUrl(`/s/${targetSpace}/talk`, "talk", null);
		}
	}
	
	/*
	
	appManager.setMainApp("talk");

	
	let hst = historyList.get("space", spaceId);
	if(hst){
		let main = hst.APP_INFO.split("/")[0];
		let sub = ""===hst.APP_INFO.split("/")[1]? null : hst.APP_INFO.split("/")[1];
		let mainUrl = getMainAppByUrl(hst.LAST_URL);
		let subUrl = getSubAppByUrl(hst.LAST_URL);
		let type = appManager.getType(mainUrl, subUrl);
		let expandable = "expand" === type.eventType;
		
		appManager.setSubApp(sub);
		appManager.updateExpandState(expandable);
		if("mail" === main) {
		let mailSub = sub? `?sub=${sub}` : "";
		Top.App.routeTo(`/s/${spaceId}/talk${mailSub}`, {eventType: type.eventType});
		}
		else Top.App.routeTo(hst.LAST_URL, {eventType: type.eventType});
		return false;
	}
	else{
		appManager.setSubApp(null);
		appManager.updateExpandState(false);
		target = `/s/${spaceId}/talk`;
		Top.App.routeTo(target, {eventType:'close'});
	}  */
	
}



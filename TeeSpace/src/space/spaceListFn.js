const spaceAPI = (function(){
	

	let _template1 = 
    '<div id={WS_ID}  class="lnbSpaceList dnd__container1" data-dnd-app1="room" >'+		
    	'<div class=thumbDiv>'+
    		'<img class="thumbPhoto1_1 {SPACEIMG1}"src={THUMB_PHOTO1}>'+
		'</div>'+
        '<div class=body >'+
        	'<div class=wsBody>'+ 
            	'<div class=wsName >{WS_NAME}</div>'+
            	'<div class=userCount >{USER_COUNT}</div>'+
				'<button class ="icon-work_pin" style="display : {PIN_DEFAULT}"></button>'+
				'<button class ="icon-work_noti_off" style ="display : {ALARM_DEFAULT}"></button>'+
        	'</div>'+
            '<div class=msgBody></div>'+ //{MSG_BODY}
        '</div>'+
        '<div class=unreadBadge style="visibility:{UNREAD_BADGE};"> '+
            '<div class=unreadCount></div>'+ //{UNREAD_USER_COUNT}
		'</div>'+
		`
        	<div class = sp_menu style="display : none;"> 
        		<button class ="icon-ellipsis_vertical_small" type ="button"></button>
        		<button class ="icon-deploy" type ="button"></button>
        	</div>
        `+
    '</div>'
		
	let _template2 = 
    '<div id={WS_ID}  class="lnbSpaceList dnd__container1" data-dnd-app1="room" >'+
		'<div class=thumbDiv>'+
			'<img class="thumbPhoto1_2 {SPACEIMG1}"src={THUMB_PHOTO1}>'+
			'<img class="thumbPhoto2_2 {SPACEIMG2}"src={THUMB_PHOTO2}>'+
		'</div>'+
		'<div class=body >'+
			'<div class=wsBody>'+ 
				'<div class=wsName >{WS_NAME}</div>'+
				'<div class=userCount >{USER_COUNT}</div>'+
				'<button class ="icon-work_pin" style="display : {PIN_DEFAULT}"></button>'+
				'<button class ="icon-work_noti_off" style ="display : {ALARM_DEFAULT}"></button>'+
			'</div>'+
			'<div class=msgBody></div>'+ //{MSG_BODY}
		'</div>'+
		'<div class=unreadBadge style="visibility:{UNREAD_BADGE};"> '+
			'<div class=unreadCount></div>'+ //{UNREAD_USER_COUNT}
		'</div>'+
		`
        	<div class = sp_menu style="display : none;"> 
        		<button class ="icon-ellipsis_vertical_small" type ="button"></button>
        		<button class ="icon-deploy" type ="button"></button>
        	</div>
        `+
    '</div>'
				
	let _template3 = 
    '<div id={WS_ID}  class="lnbSpaceList dnd__container1" data-dnd-app1="room" >'+
		'<div class=thumbDiv>'+
			'<img class="thumbPhoto1_3 {SPACEIMG1}"src={THUMB_PHOTO1}>'+
			'<img class="thumbPhoto2_3 {SPACEIMG2}"src={THUMB_PHOTO2}>'+
			'<img class="thumbPhoto3_3 {SPACEIMG3}"src={THUMB_PHOTO3}>'+
		'</div>'+
		'<div class=body >'+
			'<div class=wsBody>'+ 
				'<div class=wsName >{WS_NAME}</div>'+
				'<div class=userCount >{USER_COUNT}</div>'+
				'<button class ="icon-work_pin" style="display : {PIN_DEFAULT}"></button>'+
				'<button class ="icon-work_noti_off" style ="display : {ALARM_DEFAULT}"></button>'+
			'</div>'+
			'<div class=msgBody></div>'+ //{MSG_BODY}
 		'</div>'+
		'<div class=unreadBadge style="visibility:{UNREAD_BADGE};"> '+
			'<div class=unreadCount></div>'+ //{UNREAD_USER_COUNT}
		'</div>'+
		`
        	<div class = sp_menu style="display : none;"> 
        		<button class ="icon-ellipsis_vertical_small" type ="button"></button>
        		<button class ="icon-deploy" type ="button"></button>
        	</div>
        `+
    '</div>'
				
	let _template4 = 
    '<div id={WS_ID}  class="lnbSpaceList dnd__container1" data-dnd-app1="room" >'+
		'<div class=thumbDiv>'+
			'<img class="thumbPhoto1_4 {SPACEIMG1}"src={THUMB_PHOTO1}>'+
			'<img class="thumbPhoto2_4 {SPACEIMG2}"src={THUMB_PHOTO2}>'+
			'<img class="thumbPhoto3_4 {SPACEIMG3}"src={THUMB_PHOTO3}>'+
			'<img class="thumbPhoto4_4 {SPACEIMG4}"src={THUMB_PHOTO4}>'+
		'</div>'+
		'<div class=body >'+
			'<div class=wsBody>'+ 
				'<div class=wsName >{WS_NAME}</div>'+
				'<div class=userCount >{USER_COUNT}</div>'+
				'<button class ="icon-work_pin" style="display : {PIN_DEFAULT}"></button>'+
				'<button class ="icon-work_noti_off" style ="display : {ALARM_DEFAULT}"></button>'+
			'</div>'+
			'<div class=msgBody></div>'+ //{MSG_BODY}
		'</div>'+
		'<div class=unreadBadge style="visibility:{UNREAD_BADGE};"> '+
			'<div class=unreadCount></div>'+ //{UNREAD_USER_COUNT}
		'</div>'+
		`
        	<div class = sp_menu style="display : none;"> 
        		<button class ="icon-ellipsis_vertical_small" type ="button"></button>
        		<button class ="icon-deploy" type ="button"></button>
        	</div>
        `+
	'</div>'
	
	let _tempManager = [];
	
	
	let _updateTempManager = function(){
		
		 return axios.post( _workspace.url + 'SpaceRoom/SpaceRoomTempList?action=Get', {
			 dto: {
				ADMIN_ID: userManager.getLoginUserId()
			 },
		 }).then(result => {
			 _tempManager = result.data.dto.WsList;
		 })

	}
	
	let _updateTempManagerSync = function(){
		
		jQuery.ajax({
			type: "POST",
			url: _workspace.url + "SpaceRoom/SpaceRoomTempList?action=Get",
			dataType : "json",
			async : false,
			data : JSON.stringify({
				 dto: {
						ADMIN_ID: userManager.getLoginUserId()
					 }
			}),
				contentType: "application/json",
				success: function (result) {
					_tempManager = result.dto.WsList;
					
				}
		})	
		
	}
	
	let _truncateTempManager = function(WS_ID){
		
		if(_tempManager.length > 0){
			if(WS_ID){
				
				 axios.post( _workspace.url + 'SpaceRoom/SpaceRoomTemp?action=Delete', {
					 dto: {
						WS_ID : WS_ID
					 },
				 }).then(result => {
					 _updateTempManager();
					 deleteObsRoom(WS_ID);
				 })	
				
			}
			
			else{
				_tempManager.forEach(ele => {
					 axios.post( _workspace.url + 'SpaceRoom/SpaceRoomTemp?action=Delete', {
						 dto: {
							WS_ID : ele.WS_ID
						 },
					 }).then(result => {
						 _updateTempManager();
						 deleteObsRoom(ele.WS_ID);
					 })			
				})				
			}	
		}

		

	}
	
	
	let _getSpaceMemberListBrief = function (WS_ID, USER_ID = userManager.getLoginUserId()) {
		
			 return axios.post( _workspace.url + 'SpaceRoom/SpaceRoomMemberListBrief?action=Get', {
				 dto: {
					WS_ID: WS_ID, 
					USER_ID: USER_ID
				 },
			 })
	}

	let _getSpaceProfile = function (SPACE_ID) {
		
		if(!SPACE_ID) return false;
		
		if(workspaceManager.getWorkspace(SPACE_ID)){
			
			let ele = workspaceManager.getWorkspace(SPACE_ID)
			
			let template;       	
			let WS_NAME = ele.WS_NAME ? ele.WS_NAME : '';
			let WS_TYPE = ele.WS_TYPE ? ele.WS_TYPE : '';
			let RANK = ele.RANK ? ele.RANK : 0;
			RANK = Number(RANK)
			
			let UID_LIST = ele.UID_LIST ? ele.UID_LIST.split(',') : [];
			let THUMB_LIST = ele.THUMB_LIST ? ele.THUMB_LIST.split(',') : [];
					
			let THUMB_PHOTO0 = THUMB_LIST[0] && UID_LIST[0] ? userManager.getUserPhoto(UID_LIST[0], "small", THUMB_LIST[0].trim()) : ''
			let THUMB_PHOTO1 = THUMB_LIST[1] && UID_LIST[1] ? userManager.getUserPhoto(UID_LIST[1], "small", THUMB_LIST[1].trim()) : ''
			let THUMB_PHOTO2 = THUMB_LIST[2] && UID_LIST[2] ? userManager.getUserPhoto(UID_LIST[2], "small", THUMB_LIST[2].trim()) : ''
			let THUMB_PHOTO3 = THUMB_LIST[3] && UID_LIST[3] ? userManager.getUserPhoto(UID_LIST[3], "small", THUMB_LIST[3].trim()) : ''
			let THUMB_PHOTO4 = THUMB_LIST[4] && UID_LIST[4] ? userManager.getUserPhoto(UID_LIST[4], "small", THUMB_LIST[4].trim()) : ''
				
				
			if(WS_TYPE === 'WKS0001'){
				WS_NAME = userManager.getLoginUserName() + ' (나)'
			}
	        
			switch (RANK){
			case 1:
				template = '<div class=thumbDiv>'+	 
						   '<img class="thumbPhoto1_1 {SPACEIMG1}"src={THUMB_PHOTO1}>'+
						   '</div>'
				template = template.replace('{THUMB_PHOTO1}',THUMB_PHOTO0)
				.replace('{SPACEIMG1}','SPACE'+UID_LIST[0]);
				break;
			case 2:
				template = '<div class=thumbDiv>'+ 
				 	 	   '<img class="thumbPhoto1_1 {SPACEIMG1}"src={THUMB_PHOTO1}>'+
				 	  	   '</div>'
				template = template.replace('{THUMB_PHOTO1}',THUMB_PHOTO1)
				.replace('{SPACEIMG1}','SPACE'+UID_LIST[1]);
				break;
			case 3:
				template = '<div class=thumbDiv>'+	 
					       '<img class="thumbPhoto1_2 {SPACEIMG1}"src={THUMB_PHOTO1}>'+
					       '<img class="thumbPhoto2_2 {SPACEIMG2}"src={THUMB_PHOTO2}>'+
					       '</div>'
				template = template.replace('{THUMB_PHOTO1}',THUMB_PHOTO1)
				.replace('{THUMB_PHOTO2}',THUMB_PHOTO2)
				.replace('{SPACEIMG1}','SPACE'+UID_LIST[1])
				.replace('{SPACEIMG2}','SPACE'+UID_LIST[2]);
				break;
			case 4:
				template = '<div class=thumbDiv>'+	 
					       '<img class="thumbPhoto1_3 {SPACEIMG1}"src={THUMB_PHOTO1}>'+
						   '<img class="thumbPhoto2_3 {SPACEIMG2}"src={THUMB_PHOTO2}>'+
						   '<img class="thumbPhoto3_3 {SPACEIMG3}"src={THUMB_PHOTO3}>'+
						   '</div>'
				template = template.replace('{THUMB_PHOTO1}',THUMB_PHOTO1)
				.replace('{THUMB_PHOTO2}',THUMB_PHOTO2)
				.replace('{THUMB_PHOTO3}',THUMB_PHOTO3)
				.replace('{SPACEIMG1}','SPACE'+UID_LIST[1])
				.replace('{SPACEIMG2}','SPACE'+UID_LIST[2])
				.replace('{SPACEIMG3}','SPACE'+UID_LIST[3])
				break;
			case 5:
				template = '<div class=thumbDiv>'+	 
					       '<img class="thumbPhoto1_4 {SPACEIMG1}"src={THUMB_PHOTO1}>'+
			  	           '<img class="thumbPhoto2_4 {SPACEIMG2}"src={THUMB_PHOTO2}>'+
				           '<img class="thumbPhoto3_4 {SPACEIMG3}"src={THUMB_PHOTO3}>'+
				           '<img class="thumbPhoto4_4 {SPACEIMG4}"src={THUMB_PHOTO4}>'+
				           '</div>'
				template = template.replace('{THUMB_PHOTO1}',THUMB_PHOTO1)
				.replace('{THUMB_PHOTO2}',THUMB_PHOTO2)
				.replace('{THUMB_PHOTO3}',THUMB_PHOTO3)
				.replace('{THUMB_PHOTO4}',THUMB_PHOTO4)
				.replace('{SPACEIMG1}','SPACE'+UID_LIST[1])
				.replace('{SPACEIMG2}','SPACE'+UID_LIST[2])
				.replace('{SPACEIMG3}','SPACE'+UID_LIST[3])
				.replace('{SPACEIMG4}','SPACE'+UID_LIST[4])	
				break;
			default:
				template = '<div class=thumbDiv>'+	 
						   '<img class="thumbPhoto1_1 {SPACEIMG1}"src={THUMB_PHOTO1}>'+
						   '</div>'



			}

			let output = {"SPACE_NAME":WS_NAME,"THUMB_DIV":template};		
			return output;
		}
		else if(talkServer2.getMessengerRooms(userManager.getLoginUserId(),"true",SPACE_ID).ttalkMessengersList[0]){
					
			let ele = talkServer2.getMessengerRooms(userManager.getLoginUserId(),"true",SPACE_ID).ttalkMessengersList[0]
			
			let template;       	
			let WS_NAME = ele.WS_NAME ? ele.WS_NAME : '';
			let WS_TYPE = ele.WS_TYPE ? ele.WS_TYPE : '';
			let RANK = ele.RANK ? ele.RANK : 0;
			RANK = Number(RANK)
			
			let UID_LIST = ele.UID_LIST ? ele.UID_LIST.split(',') : [];
			let THUMB_LIST = ele.THUMB_LIST ? ele.THUMB_LIST.split(',') : [];
					
			let THUMB_PHOTO0 = THUMB_LIST[0] && UID_LIST[0] ? userManager.getUserPhoto(UID_LIST[0], "small", THUMB_LIST[0].trim()) : ''
			let THUMB_PHOTO1 = THUMB_LIST[1] && UID_LIST[1] ? userManager.getUserPhoto(UID_LIST[1], "small", THUMB_LIST[1].trim()) : ''
			let THUMB_PHOTO2 = THUMB_LIST[2] && UID_LIST[2] ? userManager.getUserPhoto(UID_LIST[2], "small", THUMB_LIST[2].trim()) : ''
			let THUMB_PHOTO3 = THUMB_LIST[3] && UID_LIST[3] ? userManager.getUserPhoto(UID_LIST[3], "small", THUMB_LIST[3].trim()) : ''
			let THUMB_PHOTO4 = THUMB_LIST[4] && UID_LIST[4] ? userManager.getUserPhoto(UID_LIST[4], "small", THUMB_LIST[4].trim()) : ''
				
				
			if(WS_TYPE === 'WKS0001'){
				WS_NAME = userManager.getLoginUserName() + ' (나)'
			}
	        
			switch (RANK){
			case 1:
				template = '<div class=thumbDiv>'+	 
						   '<img class="thumbPhoto1_1 {SPACEIMG1}"src={THUMB_PHOTO1}>'+
						   '</div>'
				template = template.replace('{THUMB_PHOTO1}',THUMB_PHOTO0)
				.replace('{SPACEIMG1}','SPACE'+UID_LIST[0]);
				break;
			case 2:
				template = '<div class=thumbDiv>'+ 
				 	 	   '<img class="thumbPhoto1_1 {SPACEIMG1}"src={THUMB_PHOTO1}>'+
				 	  	   '</div>'
				template = template.replace('{THUMB_PHOTO1}',THUMB_PHOTO1)
				.replace('{SPACEIMG1}','SPACE'+UID_LIST[1]);
				break;
			case 3:
				template = '<div class=thumbDiv>'+	 
					       '<img class="thumbPhoto1_2 {SPACEIMG1}"src={THUMB_PHOTO1}>'+
					       '<img class="thumbPhoto2_2 {SPACEIMG2}"src={THUMB_PHOTO2}>'+
					       '</div>'
				template = template.replace('{THUMB_PHOTO1}',THUMB_PHOTO1)
				.replace('{THUMB_PHOTO2}',THUMB_PHOTO2)
				.replace('{SPACEIMG1}','SPACE'+UID_LIST[1])
				.replace('{SPACEIMG2}','SPACE'+UID_LIST[2]);
				break;
			case 4:
				template = '<div class=thumbDiv>'+	 
					       '<img class="thumbPhoto1_3 {SPACEIMG1}"src={THUMB_PHOTO1}>'+
						   '<img class="thumbPhoto2_3 {SPACEIMG2}"src={THUMB_PHOTO2}>'+
						   '<img class="thumbPhoto3_3 {SPACEIMG3}"src={THUMB_PHOTO3}>'+
						   '</div>'
				template = template.replace('{THUMB_PHOTO1}',THUMB_PHOTO1)
				.replace('{THUMB_PHOTO2}',THUMB_PHOTO2)
				.replace('{THUMB_PHOTO3}',THUMB_PHOTO3)
				.replace('{SPACEIMG1}','SPACE'+UID_LIST[1])
				.replace('{SPACEIMG2}','SPACE'+UID_LIST[2])
				.replace('{SPACEIMG3}','SPACE'+UID_LIST[3])
				break;
			case 5:
				template = '<div class=thumbDiv>'+	 
					       '<img class="thumbPhoto1_4 {SPACEIMG1}"src={THUMB_PHOTO1}>'+
			  	           '<img class="thumbPhoto2_4 {SPACEIMG2}"src={THUMB_PHOTO2}>'+
				           '<img class="thumbPhoto3_4 {SPACEIMG3}"src={THUMB_PHOTO3}>'+
				           '<img class="thumbPhoto4_4 {SPACEIMG4}"src={THUMB_PHOTO4}>'+
				           '</div>'
				template = template.replace('{THUMB_PHOTO1}',THUMB_PHOTO1)
				.replace('{THUMB_PHOTO2}',THUMB_PHOTO2)
				.replace('{THUMB_PHOTO3}',THUMB_PHOTO3)
				.replace('{THUMB_PHOTO4}',THUMB_PHOTO4)
				.replace('{SPACEIMG1}','SPACE'+UID_LIST[1])
				.replace('{SPACEIMG2}','SPACE'+UID_LIST[2])
				.replace('{SPACEIMG3}','SPACE'+UID_LIST[3])
				.replace('{SPACEIMG4}','SPACE'+UID_LIST[4])	
				break;
			default:
				template = '<div class=thumbDiv>'+	 
						   '<img class="thumbPhoto1_1 {SPACEIMG1}"src={THUMB_PHOTO1}>'+
						   '</div>'

			}

			let output = {"SPACE_NAME":WS_NAME,"THUMB_DIV":template};		
			return output;
		}

		
	}

	let _createSpace = function (WS_NAME, ADMIN_ID, WS_TYPE, WsUserList, ROUTING_APP){
		let _result;
		let spaceUrl;
		let target;
		let data;
		if(WsUserList.length == 1){
			data = JSON.stringify({
				"dto" :{
					"WS_NAME" : WS_NAME,
					"ADMIN_ID" : ADMIN_ID,
					"WS_TYPE" : WS_TYPE,
					"WsUserList" : WsUserList,
					"WS_ACCESS_TYPE":"DM"
				}
			})
		}
		else {
			data = JSON.stringify({
				"dto" :{
					"WS_NAME" : WS_NAME,
					"ADMIN_ID" : ADMIN_ID,
					"WS_TYPE" : WS_TYPE,
					"WsUserList" : WsUserList	
				}
			})
		}
		
		
		jQuery.ajax({
			type: "POST",
			url: _workspace.url + "SpaceRoom/SpaceRoom",
			dataType : "json",
			data : data,
			contentType: "application/json",
			success: function (result) {
				_result = result.dto
				spaceUrl = _result.RESULT_URL
				
				if(_result.RESULT_CD == "RST0001"){
					_updateTempManager().then(() => {
						_drawLnbSpace(_result.RESULT_VALUE, "true")
						
						if(ROUTING_APP == "MEETING"){
							appManager.setMainApp('talk')
							appManager.setSubApp('meeting');
							target = `/s/${spaceUrl}/${appManager.getMainApp()}?sub=${appManager.getSubApp()}`;
							Top.App.routeTo(target, {eventType:'fold'})
						}
						else if(ROUTING_APP === "MINITALK"){
							
							openMiniTalk(spaceUrl);
							
						}
						else{
							appManager.setMainApp('talk');
							appManager.setSubApp(null);
							target = `/s/${spaceUrl}/${appManager.getMainApp()}`;
							Top.App.routeTo(target, {eventType: 'close'});
						}
						
//						createObsRoom(토크메신저리스트 조회 정보) --스페이스 생성시
					})				
				}
				
				else if (_result.RESULT_CD == "RST0003") {
					if(WsUserList.length == 1 && workspaceManager.getWorkspace(_result.RESULT_VALUE).MEM_JOB == 'N'){
						jQuery.ajax({
							type: "PUT",
							url: _workspace.url + "SpaceRoom/SpaceRoomMember",
							dataType : "json",
							data : JSON.stringify({
								"dto":
								{
									"WS_ID":_result.RESULT_VALUE,
									"USER_ID":ADMIN_ID,
									"MEM_JOB":"Y"
								}
							}),
								contentType: "application/json",
								success: function (result) {
								if(result.dto.RESULT_CD == "RST0001")
									workspaceManager.update();
									_drawLnbSpace(_result.RESULT_VALUE);
									
									
								}
						})	

					}
					else{					
					}
					
					if(ROUTING_APP == "MEETING"){
						appManager.setMainApp('talk')
						appManager.setSubApp('meeting');
						target = `/s/${spaceUrl}/${appManager.getMainApp()}?sub=${appManager.getSubApp()}`;
						Top.App.routeTo(target, {eventType:'fold'})
					}
					else if(ROUTING_APP === "MINITALK"){
						
						openMiniTalk(spaceUrl);
						
					}
					else{
						appManager.setMainApp('talk');
						appManager.setSubApp(null);
						target = `/s/${spaceUrl}/${appManager.getMainApp()}`;
						//1:1talk history유지
						changeSpace(spaceUrl);
						//Top.App.routeTo(target, {eventType: 'close'});
					}
//					createObsRoom(토크메신저정보)  --스페이스 생성시
				}
			}
		})

	}

	let _inviteMember = function(WS_ID, WsUserList){
		
		if(workspaceManager.getWorkspace(WS_ID)){
			
			let WMele = workspaceManager.getWorkspace(WS_ID)
			let WS_ACCESS_TYPE = WMele.WS_ACCESS_TYPE ? WMele.WS_ACCESS_TYPE : '';
			let WS_TYPE = WMele.WS_TYPE ? WMele.WS_TYPE : '';
			let USER_ID = userManager.getLoginUserId();	
			let UID_LIST = WMele.UID_LIST ? WMele.UID_LIST.split(',') : [];
		
			
			if(WS_ACCESS_TYPE == "DM" || WS_TYPE == 'WKS0001'){
				
				for(i=1; i<UID_LIST.length;i++){
					WsUserList.push({"USER_ID":UID_LIST[i]})	
				}
				_createSpace(null, USER_ID, "WKS0002", WsUserList)
			}
			
			else{
				Top.Ajax.execute({
					url : _workspace.url + "SpaceRoom/SpaceRoomMember",
					type : 'POST',
					dataType : "json",
					async : false,
					cache : false,
					data : JSON.stringify({
						"dto":
						{
							"WS_ID":WS_ID,
							"SENDER_ID":USER_ID,
							"WsUserList":WsUserList
						}
					}), // arg
					contentType : "application/json",
					crossDomain : true,
					success : function(result) {
						workspaceManager.update()
						_drawLnbSpace(WS_ID)
					}
				});	
				
			}

			userManager.updateWorkspaceUser(WS_ID);
			
		}

	}

	let _exitSpace = function(WS_ID){
		
		if(workspaceManager.getWorkspace(WS_ID)){
	
			let WMele = workspaceManager.getWorkspace(WS_ID)
			let WS_ACCESS_TYPE = WMele.WS_ACCESS_TYPE ? WMele.WS_ACCESS_TYPE : '';
			let WS_TYPE = WMele.WS_TYPE ? WMele.WS_TYPE : '';
			let USER_ID = userManager.getLoginUserId();	
			let UID_LIST = WMele.UID_LIST ? WMele.UID_LIST.split(',') : [];
			
			let PREV_WS_ID = $('div.lnbSpaceList#'+WS_ID).prev().attr('id')
			let NEXT_WS_ID = $('div.lnbSpaceList#'+WS_ID).next().attr('id')
			
			if(WS_TYPE == 'WKS0001'){
				return;
			}
			
			if(WS_ACCESS_TYPE == "DM"){
				
				Top.Ajax.execute({
					url : _workspace.url + "SpaceRoom/SpaceRoomMember",
					type : 'PUT',
					dataType : "json",
					async : false,
					cache : false,
					data : JSON.stringify({
						"dto":
						{
							"WS_ID":WS_ID,
							"USER_ID":USER_ID,
							"MEM_JOB":"N"
						}
					}), // arg
					contentType : "application/json",
					crossDomain : true,
					success : function(result) {
						_removeLnbSpace(WS_ID)
						if(workspaceManager.getWorkspaceId(getRoomIdByUrl()) == WS_ID){
							if(NEXT_WS_ID){
								_changeSpace(NEXT_WS_ID)
							}
							else if(PREV_WS_ID){
								_changeSpace(PREV_WS_ID)
							}
							else{
								
							}
							
						}
						workspaceManager.update()
					}
				});	
			}
			else{
				if(UID_LIST.length == 1 && UID_LIST[0] == USER_ID){
					Top.Ajax.execute({
						url : _workspace.url + "SpaceRoom/SpaceRoom",
						type : 'DELETE',
						dataType : "json",
						async : false,
						cache : false,
						data : JSON.stringify({
							"dto":
							{
								"WS_ID":WS_ID
							}
						}), // arg
						contentType : "application/json",
						crossDomain : true,
						success : function(result) {
							_removeLnbSpace(WS_ID)
							if(workspaceManager.getWorkspaceId(getRoomIdByUrl()) == WS_ID){
								if(NEXT_WS_ID){
									_changeSpace(NEXT_WS_ID)
								}
								else if(PREV_WS_ID){
									_changeSpace(PREV_WS_ID)
								}
								else{
									
								}
								
							}
							workspaceManager.update()
						}
					});	
				}
				else{
					Top.Ajax.execute({
						url : _workspace.url + "SpaceRoom/SpaceRoomMember",
						type : 'DELETE',
						dataType : "json",
						async : false,
						cache : false,
						data : JSON.stringify({
							"dto":
							{
								"FLAG1":USER_ID, 
								"WS_ID":WS_ID,
								"WsUserBanList":[{"USER_ID": USER_ID}]
							}
						}), // arg
						contentType : "application/json",
						crossDomain : true,
						success : function(result) {
							_removeLnbSpace(WS_ID)
							if(workspaceManager.getWorkspaceId(getRoomIdByUrl()) == WS_ID){
								if(NEXT_WS_ID){
									_changeSpace(NEXT_WS_ID)
								}
								else if(PREV_WS_ID){
									_changeSpace(PREV_WS_ID)
								}
								else{
									
								}
								
							}
							workspaceManager.update()
							
						}
					});	
					
				}
				
			}
		}
		
		
		
		
	}

	let _spaceInit = function(){
		if(_isTempSpace(getRoomIdByUrl())){
			jQuery.ajax({
				 type: "POST",
				 url: _workspace.url + "SpaceRoom/SpaceRoom?action=Init",
					dataType : "json",
					data : JSON.stringify({
						"dto" :{
							"WS_ID" : workspaceManager.getWorkspaceId(getRoomIdByUrl())
						}
					}),
				 contentType: "application/json",
				 async: false,
				 success: function (result) {
					 workspaceManager.update();
					 spaceAPI.updateTempManager();
				 }
			 })	
		}
	}

	let _isTempSpace = function(SPACE_URL){
		
		let _isTemp = false
		
		for(var i = 0; i < _tempManager.length; i++) {
		    if (_tempManager[i].SPACE_URL == SPACE_URL || _tempManager[i].WS_ID == SPACE_URL) {
				_isTemp = true;
				break;		
		    }
		}
		
		return _isTemp
	}

	let _drawLnbSpace = function(WS_ID, isTemp = "false"){
	
			let _result = talkServer2.getMessengerRooms(userManager.getLoginUserId(),isTemp,WS_ID).ttalkMessengersList[0]
			
			if($('div.lnbSpaceList#'+_result.WS_ID).length == 0){
				createObsRoom(_result);
//				createObsRoom(_result);   스페이스 생성시 들어가야 하는 로직..
			}
			else{
				_updateSpaceDiv(_result);
				_afterCreateSpaceDiv(_result.WS_ID);
			}	
	}

	let _removeLnbSpace = function(WS_ID){
		if($('#AppGnbRoomButton').hasClass('active')){
			if($('div.lnbSpaceList#'+WS_ID)){
				$('div.lnbSpaceList#'+WS_ID).remove();
				return;
			}
			else{
				return;
			}
		}
		else{
			return;
		}
		deleteObsRoom(WS_ID);
	}

	let _updateSpaceDiv = function(ele){
			
			if(!ele) return false;
		
		let template;       	
		let WS_ID = ele.WS_ID ? ele.WS_ID : '';
		let WS_NAME = ele.WS_NAME ? ele.WS_NAME : '';
		let MSG_BODY = ele.MSG_BODY ? ele.MSG_BODY : '';
		let WS_TYPE = ele.WS_TYPE ? ele.WS_TYPE : '';
		let USER_COUNT = ele.USER_COUNT ? ele.USER_COUNT : '';
		let UNREAD_USER_COUNT = ele.UNREAD_USER_COUNT ? ele.UNREAD_USER_COUNT : 0;
		let UNREAD_BADGE = ele.UNREAD_USER_COUNT ? 'visible' : 'hidden';
		let WS_ACCESS_TYPE = ele.WS_ACCESS_TYPE ? ele.WS_ACCESS_TYPE : '';
		let RANK = ele.RANK ? ele.RANK : 0;
		RANK = Number(RANK)
		
		let UID_LIST = ele.UID_LIST ? ele.UID_LIST.split(',') : [];
		let THUMB_LIST = ele.THUMB_LIST ? ele.THUMB_LIST.split(',') : [];
				
		let THUMB_PHOTO0 = THUMB_LIST[0] && UID_LIST[0] ? userManager.getUserPhoto(UID_LIST[0], "small", THUMB_LIST[0].trim()) : ''
		let THUMB_PHOTO1 = THUMB_LIST[1] && UID_LIST[1] ? userManager.getUserPhoto(UID_LIST[1], "small", THUMB_LIST[1].trim()) : ''
		let THUMB_PHOTO2 = THUMB_LIST[2] && UID_LIST[2] ? userManager.getUserPhoto(UID_LIST[2], "small", THUMB_LIST[2].trim()) : ''
		let THUMB_PHOTO3 = THUMB_LIST[3] && UID_LIST[3] ? userManager.getUserPhoto(UID_LIST[3], "small", THUMB_LIST[3].trim()) : ''
		let THUMB_PHOTO4 = THUMB_LIST[4] && UID_LIST[4] ? userManager.getUserPhoto(UID_LIST[4], "small", THUMB_LIST[4].trim()) : ''
			
			
		if(WS_TYPE === 'WKS0001'){
			WS_NAME = userManager.getLoginUserName() + ' (나)'
			USER_COUNT = ''
		}
		if(WS_ACCESS_TYPE == 'DM'){
			USER_COUNT = ''
		}

		
		let spaceDiv = $('div.lnbSpaceList#'+WS_ID)
//		spaceDiv.find('.unreadBadge').css('visibility',UNREAD_BADGE);

		
		switch (RANK){
		case 1:
			template = '<img class="thumbPhoto1_1 {SPACEIMG1}"src={THUMB_PHOTO1}>'
			template = template.replace('{THUMB_PHOTO1}',THUMB_PHOTO0)
			.replace('{SPACEIMG1}','SPACE'+UID_LIST[0]);
			break;
		case 2:
			template = '<img class="thumbPhoto1_1 {SPACEIMG1}"src={THUMB_PHOTO1}>'
			template = template.replace('{THUMB_PHOTO1}',THUMB_PHOTO1)
			.replace('{SPACEIMG1}','SPACE'+UID_LIST[1]);
			break;
		case 3:
			template = '<img class="thumbPhoto1_2 {SPACEIMG1}"src={THUMB_PHOTO1}>'+
				       '<img class="thumbPhoto2_2 {SPACEIMG2}"src={THUMB_PHOTO2}>'
			template = template.replace('{THUMB_PHOTO1}',THUMB_PHOTO1)
			.replace('{THUMB_PHOTO2}',THUMB_PHOTO2)
			.replace('{SPACEIMG1}','SPACE'+UID_LIST[1])
			.replace('{SPACEIMG2}','SPACE'+UID_LIST[2]);
			break;
		case 4:
			template = '<img class="thumbPhoto1_3 {SPACEIMG1}"src={THUMB_PHOTO1}>'+
					   '<img class="thumbPhoto2_3 {SPACEIMG2}"src={THUMB_PHOTO2}>'+
					   '<img class="thumbPhoto3_3 {SPACEIMG3}"src={THUMB_PHOTO3}>'
			template = template.replace('{THUMB_PHOTO1}',THUMB_PHOTO1)
			.replace('{THUMB_PHOTO2}',THUMB_PHOTO2)
			.replace('{THUMB_PHOTO3}',THUMB_PHOTO3)
			.replace('{SPACEIMG1}','SPACE'+UID_LIST[1])
			.replace('{SPACEIMG2}','SPACE'+UID_LIST[2])
			.replace('{SPACEIMG3}','SPACE'+UID_LIST[3])
			break;
		case 5:
			template = '<img class="thumbPhoto1_4 {SPACEIMG1}"src={THUMB_PHOTO1}>'+
		  	           '<img class="thumbPhoto2_4 {SPACEIMG2}"src={THUMB_PHOTO2}>'+
			           '<img class="thumbPhoto3_4 {SPACEIMG3}"src={THUMB_PHOTO3}>'+
			           '<img class="thumbPhoto4_4 {SPACEIMG4}"src={THUMB_PHOTO4}>'
			template = template.replace('{THUMB_PHOTO1}',THUMB_PHOTO1)
			.replace('{THUMB_PHOTO2}',THUMB_PHOTO2)
			.replace('{THUMB_PHOTO3}',THUMB_PHOTO3)
			.replace('{THUMB_PHOTO4}',THUMB_PHOTO4)
			.replace('{SPACEIMG1}','SPACE'+UID_LIST[1])
			.replace('{SPACEIMG2}','SPACE'+UID_LIST[2])
			.replace('{SPACEIMG3}','SPACE'+UID_LIST[3])
			.replace('{SPACEIMG4}','SPACE'+UID_LIST[4])	
			break;
		default:
			template = '<img class="thumbPhoto1_1 {SPACEIMG1}"src={THUMB_PHOTO1}>'
		}
		
		
		spaceDiv.find('.wsName').text(WS_NAME)
		spaceDiv.find('.userCount').text(USER_COUNT)
//		spaceDiv.find('.msgBody').text(MSG_BODY) 
//		spaceDiv.find('.unreadCount').text(UNREAD_USER_COUNT)
		spaceDiv.find('.thumbDiv').html(template)
		
	}

	let _createSpaceDiv = function(ele){
		
			if(!ele) return false;
			
			let template;       	
			let WS_ALARM_YN = ele.WS_ALARM_YN == 'COM0001' ? 'none' : 'block';
			let WS_BOOKMARK = ele.WS_BOOKMARK == 'COM0006' ? 'none' : 'block';
			// let WS_BOOKMARK = 'none';
			let WS_ID = ele.WS_ID ? ele.WS_ID : '';
			let WS_NAME = ele.WS_NAME ? ele.WS_NAME : '';
			let MSG_BODY = ele.MSG_BODY ? ele.MSG_BODY : '';
			let WS_TYPE = ele.WS_TYPE ? ele.WS_TYPE : '';
			let USER_COUNT = ele.USER_COUNT ? ele.USER_COUNT : '';
			let UNREAD_USER_COUNT = ele.UNREAD_USER_COUNT ? ele.UNREAD_USER_COUNT : 0;
			let UNREAD_BADGE = ele.UNREAD_USER_COUNT ? 'visible' : 'hidden';
			let WS_ACCESS_TYPE = ele.WS_ACCESS_TYPE ? ele.WS_ACCESS_TYPE : '';
			let RANK = ele.RANK ? ele.RANK : 0;
			RANK = Number(RANK)
			
			let UID_LIST = ele.UID_LIST ? ele.UID_LIST.split(',') : [];
			let THUMB_LIST = ele.THUMB_LIST ? ele.THUMB_LIST.split(',') : [];
					
			let THUMB_PHOTO0 = THUMB_LIST[0] && UID_LIST[0] ? userManager.getUserPhoto(UID_LIST[0], "small", THUMB_LIST[0].trim()) : ''
			let THUMB_PHOTO1 = THUMB_LIST[1] && UID_LIST[1] ? userManager.getUserPhoto(UID_LIST[1], "small", THUMB_LIST[1].trim()) : ''
			let THUMB_PHOTO2 = THUMB_LIST[2] && UID_LIST[2] ? userManager.getUserPhoto(UID_LIST[2], "small", THUMB_LIST[2].trim()) : ''
			let THUMB_PHOTO3 = THUMB_LIST[3] && UID_LIST[3] ? userManager.getUserPhoto(UID_LIST[3], "small", THUMB_LIST[3].trim()) : ''
			let THUMB_PHOTO4 = THUMB_LIST[4] && UID_LIST[4] ? userManager.getUserPhoto(UID_LIST[4], "small", THUMB_LIST[4].trim()) : ''

				
			if(WS_TYPE === 'WKS0001'){
				WS_NAME = userManager.getLoginUserName() + ' (나)'
				USER_COUNT = ''
			}
			if(WS_ACCESS_TYPE == 'DM'){
				USER_COUNT = ''
			}
			
	
	
		switch (RANK){
			case 1:
				template = _template1; 
				template = template.replace('{THUMB_PHOTO1}',THUMB_PHOTO0)
				.replace('{SPACEIMG1}','SPACE'+UID_LIST[0]);
				break;
			case 2:
				template = _template1; 
				template = template.replace('{THUMB_PHOTO1}',THUMB_PHOTO1)
				.replace('{SPACEIMG1}','SPACE'+UID_LIST[1]);
				break;
			case 3:
				template = _template2; 		
				template = template.replace('{THUMB_PHOTO1}',THUMB_PHOTO1)
				.replace('{THUMB_PHOTO2}',THUMB_PHOTO2)
				.replace('{SPACEIMG1}','SPACE'+UID_LIST[1])
				.replace('{SPACEIMG2}','SPACE'+UID_LIST[2]);
				break;
			case 4:
				template = _template3;
				template = template.replace('{THUMB_PHOTO1}',THUMB_PHOTO1)
				.replace('{THUMB_PHOTO2}',THUMB_PHOTO2)
				.replace('{THUMB_PHOTO3}',THUMB_PHOTO3)
				.replace('{SPACEIMG1}','SPACE'+UID_LIST[1])
				.replace('{SPACEIMG2}','SPACE'+UID_LIST[2])
				.replace('{SPACEIMG3}','SPACE'+UID_LIST[3])
				break;
			case 5:
				template = _template4;
				template = template.replace('{THUMB_PHOTO1}',THUMB_PHOTO1)
				.replace('{THUMB_PHOTO2}',THUMB_PHOTO2)
				.replace('{THUMB_PHOTO3}',THUMB_PHOTO3)
				.replace('{THUMB_PHOTO4}',THUMB_PHOTO4)
				.replace('{SPACEIMG1}','SPACE'+UID_LIST[1])
				.replace('{SPACEIMG2}','SPACE'+UID_LIST[2])
				.replace('{SPACEIMG3}','SPACE'+UID_LIST[3])
				.replace('{SPACEIMG4}','SPACE'+UID_LIST[4])	
				break;
			default:
				template = _template1;

			}
		
			//spaceHtml
			let spaceHTML = template
			.replace(/{WS_ID}/gi,WS_ID)
			.replace('{WS_NAME}',WS_NAME)
			.replace('{MSG_BODY}',MSG_BODY)
			.replace('{UNREAD_USER_COUNT}',UNREAD_USER_COUNT)
			.replace('{UNREAD_BADGE}',UNREAD_BADGE)
			.replace('{USER_COUNT}',USER_COUNT)
			.replace('{ALARM_DEFAULT}', WS_ALARM_YN)
			.replace('{PIN_DEFAULT}', WS_BOOKMARK)
			 
			return spaceHTML;
		}

	let _afterCreateSpaceDiv = function(wsId){

		
			_setSpace(getRoomIdByUrl()); 
			
			$(`div#${CSS.escape(wsId)}.lnbSpaceList`).click(function(){    	
			
				//        	$(this).find('.unreadBadge').css('visibility','hidden');  -- commonCall.js
				//        	$(this).find('.unreadCount').text(0);
//							NotificationManager.removeTitleBadge($(this).attr('id')); 
							
							let lnbUrl = getLnbByUrl();
							let targetSpace = workspaceManager.getWorkspaceUrl($(this).attr('id'));
							_changeSpace(targetSpace);
				})
	
	
			$(`div#${CSS.escape(wsId)}.lnbSpaceList`).on('click', '.thumbDiv', function (e) {
				e.stopPropagation();
				e.preventDefault();
				showmemClickid = $(this).parent().attr('id')
				for(var i = 0 ;  i < spaceAPI.getTempManager().length; i ++) {
                    if(spaceAPI.getTempManager()[i].WS_ID==showmemClickid) return;
				}
				if(workspaceManager.getWorkspace(showmemClickid).WS_ACCESS_TYPE === 'DM'){
					if(workspaceManager.getWorkspace(showmemClickid).UID_LIST.split(',')[0] === userManager.getLoginUserId())
						_UserId = workspaceManager.getWorkspace(showmemClickid).UID_LIST.split(',')[1];
					else _UserId = workspaceManager.getWorkspace(showmemClickid).UID_LIST.split(',')[0];
					_friendManageProfilePopupWithPosition(_UserId, 820, 28);
					return;
				}
				else if(workspaceManager.getWorkspace(showmemClickid) && workspaceManager.getWorkspace(showmemClickid).WS_TYPE === 'WKS0001') return;
				// if(!workspaceManager.getWorkspace(showmemClickid).WS_ACCESS_TYPE){
					Top.Controller.get('spaceListLayoutLogic').isedit = false;
					Top.Dom.selectById('ShowMemDialog').open();
				// }
				// else{
				// 	spaceAPI.getSpaceMemberListBrief(showmemClickid).then(result => {
				// 			for(var i = 0; i < result.data.dto.UserProfileList.length; i++){
				// 				if(result.data.dto.UserProfileList[i].USER_ID != userManager.getLoginUserId()){
				// 					_UserId = result.data.dto.UserProfileList[i].USER_ID
				// 					break;
				// 				}
				// 			}
				// 			_friendManageProfilePopupWithPosition(_UserId, 800 , 50)
				// 		});
				// } 
			});
			
			$(`div#${CSS.escape(wsId)}.lnbSpaceList`).on('click', '.icon-ellipsis_vertical_small', function (e) {
				e.preventDefault();
				e.stopPropagation();
				showmemClickid = $(this).parent().parent().attr('id')
				if(workspaceManager.getMySpaceId()!=showmemClickid){
					// if($('#'+showmemClickid+' .icon-work_noti_off').css('display') == 'none')Top.Controller.get('spaceListLayoutLogic').contextMenuTemp.updateItem(2,'알림 끄기');
					// else Top.Controller.get('spaceListLayoutLogic').contextMenuTemp.updateItem(2,'알림 켜기');
					
					if($('#'+showmemClickid+' .icon-work_noti_off').css('display') == 'none')Top.Controller.get('spaceListLayoutLogic').contextMenuTemp.updateItem(3,'알림 끄기');
					else Top.Controller.get('spaceListLayoutLogic').contextMenuTemp.updateItem(3,'알림 켜기');
					if($('#'+showmemClickid+' .icon-work_pin').css('display') == 'none')Top.Controller.get('spaceListLayoutLogic').contextMenuTemp.updateItem(0,'룸 상단 고정');
					else Top.Controller.get('spaceListLayoutLogic').contextMenuTemp.updateItem(0,'룸 상단 해제');
					if(window.innerHeight < $(this).offset().top + 180)
						Top.Controller.get('spaceListLayoutLogic').contextMenuTemp.show($(this).offset().left, $(this).offset().top-160);
					else Top.Controller.get('spaceListLayoutLogic').contextMenuTemp.show($(this).offset().left, $(this).offset().top);
				}
			});
			$(`div#${CSS.escape(wsId)}.lnbSpaceList`).on('click', '.icon-deploy', function (e) {
				var spaceid = $(this).parent().parent().attr('id');
				var spaceUrl = workspaceManager.getWorkspaceUrl(spaceid);
				e.preventDefault();
				e.stopPropagation();
				openMiniTalk(spaceUrl);
			});
			
			// $(this).children('.sp_menu').children('.icon-ellipsis_vertical_small').click(function(){
			// 	alert('a');
			// 	contextMenu.show($(this).offset().left, $(this).offset().top);
			// })
			$(`div#${CSS.escape(wsId)}.lnbSpaceList`).hover(function(){
				for(var i = 0 ;  i < spaceAPI.getTempManager().length; i ++) {
					if(spaceAPI.getTempManager()[i].WS_ID==$(this).attr('id')) return;
				}
				
				$(this).children('.unreadBadge').css('display','none');
				$(this).children('.sp_menu').css('display','block');
			},
			function(){
				$(this).children('.sp_menu').css('display','none');
				$(this).children('.unreadBadge').css('display','block');
			})
			
	};

	
	
	

	let _setSpace = function(WS_ID){
		let spcId = workspaceManager.getWorkspaceId(WS_ID);
		$(`div#spaceListLayoutBody div.selected`).removeClass('selected');
		$(`div#spaceListLayoutBody div#${spcId}`).addClass('selected');
		
	}
	
	let _changeSpace = function(spaceUrl = getRoomIdByUrl()){
		
		let targetSpace = workspaceManager.getWorkspaceUrl(spaceUrl);
		let hst = historyList.get("space", targetSpace);
		let lnbUrl = getLnbByUrl();
		let sub;
		
		if("s"===lnbUrl){
	    	let mains = appManager.getMains();
	    	let targetMain = mains.includes(getMainAppByUrl())? "talk" : getMainAppFullUrl(); 
	    	let targetUrl = location.hash.slice(2)
	    	
	    	if(hst){
	    		sub = ""===hst.APP_INFO.split("/")[1]? null : hst.APP_INFO.split("/")[1];
	    		routeUrl(hst.LAST_URL, "talk", sub);
	    	}
	    	else{
	    		routeUrl(`/s/${targetSpace}/talk`, "talk", null);
	    	}
				/*  이전 스페이스의 서브앱 
				 * 		let curSpace = getRoomIdByUrl();
				    	targetUrl = targetUrl.replace(curSpace, targetSpace);
				    	targetUrl = targetUrl.replace(getMainAppFullUrl(), targetMain);
				    	routeUrl(targetUrl, "talk", appManager.getSubApp()); */
		}
		else{
			if(hst){
				let main = hst.APP_INFO.split("/")[0];
				sub = ""===hst.APP_INFO.split("/")[1]? null : hst.APP_INFO.split("/")[1];
				routeUrl(hst.LAST_URL, main, sub);
				
			}
			else{
				routeUrl(`/s/${targetSpace}/talk`, "talk", null);
			}
		}
		
	}


	let obsMap = {};
	
	function deleteObsRoom(wsId){
		if(!wsId) return false;
		_setUnreadMsg(wsId, true);
		obsMap[wsId] = null;
	}
	
	function createObsRoom(info, type){
		//todo: 룸리스트 mobx로 변경 
		let targetMessengerInfo = info, obsRoom = null, tmpTotUnread = 0, userId = null, wsType = null, wsBookmark = null,
		msgId = null, targetWsId = null, targetTalkId = null, lastMsg = null, lastMsgId =null, unreadMsg = null;
		
		//기존 lnb 룸 생성 
		if(targetMessengerInfo.WS_TYPE === "WKS0001"){}
		else if(targetMessengerInfo.WS_BOOKMARK != 'COM0005'){
        	if("list" === type) $('div#ourSpaceLayout').append(_createSpaceDiv(targetMessengerInfo));
        	else $('div#ourSpaceLayout').prepend(_createSpaceDiv(targetMessengerInfo));
		}
        else{ 
			$('div#favSpaceLayout').prepend(_createSpaceDiv(targetMessengerInfo));
        }
		_afterCreateSpaceDiv(targetMessengerInfo.WS_ID);
		
		//obs
		targetWsId = targetMessengerInfo.WS_ID;
		targetTalkId = targetMessengerInfo.CH_ID;
		lastMsgTxt = talkContent.convertToMsgBodyText(targetMessengerInfo.LAST_MESSAGE);
		lastMsgId = targetMessengerInfo.MSG_ID;
		unreadMsg = Number(targetMessengerInfo.UNREAD_USER_COUNT);
		tmpTotUnread += unreadMsg;
		userId = targetMessengerInfo.USER_ID;
		wsType = targetMessengerInfo.WS_TYPE;
		wsBookmark = targetMessengerInfo.WS_BOOKMARK;
		obsMap[targetWsId] = new RoomClass(targetWsId, targetTalkId, userId, lastMsgId, lastMsgTxt, unreadMsg, wsType, wsBookmark);
	}
	
	function _initRoomList(){
		let messengerInfo = talkServer2.getMessengerRooms(userManager.getLoginUserId());
		obsMap = {};
		setTalkCount(0);
		
		if(messengerInfo.ttalkMessengersList!=null){

			for(let i=0; i<messengerInfo.ttalkMessengersList.length; i++){
				targetMessengerInfo = messengerInfo.ttalkMessengersList[i];

				/* 함수로 분리 필요*/
				if(targetMessengerInfo.WS_TYPE == 'WKS0001'){
	            	$('div#mySpaceLayout').prepend(_createSpaceDiv(targetMessengerInfo));
					$('div#mySpaceLayout #'+targetMessengerInfo.WS_ID).addClass('mySpace')
					$('#mySpaceLayout .icon-ellipsis_vertical_small').remove();
				}
				createObsRoom(targetMessengerInfo, "list");
			}
//			talkData.totUnreads.set(tmpTotUnread);
//			messengerInfo.map = obsMap;
//			return messengerInfo;
		}
	};
	
	function setBookMark(roomId, type){
		if(obsMap[roomId]) obsMap[roomId].bookMark.set(type);
	}
	
	function setLastMsg(roomId, msg){
		if(obsMap[roomId]) obsMap[roomId].lastMsg.set(msg);
	};
	
	//getUnreadMsg를 클래스 내부에서 수행해야 할지 체크 필요
	function setUnreadMsg(roomId, flag){		
		let value = getUnreadMsg(roomId);
		if(!flag) value += 1; 
		else value = 0;
		
		if(obsMap[roomId]) obsMap[roomId].unreadMsg.set(Number(value));
	}; 

	
	function getLastMsg(roomId){
		if(obsMap[roomId]) return obsMap[roomId].lastMsg.get();
	};
	
	function getUnreadMsg(roomId){
		if(obsMap[roomId]) return obsMap[roomId].unreadMsg.get();
	};

	function setRoomInfo(roomId, msgId, userId, msgType, msgLastTxt, isActive){
		if(msgType == 'create'){
			setUnreadMsg(roomId, isActive);
			setLastMsg(roomId, msgLastTxt);
			obsMap[roomId]['lastMsgId'] = msgId;
		}else if(msgType == 'delete'){
			if(msgId == obsMap[roomId]['lastMsgId'])
				setLastMsg(roomId, msgLastTxt);
		}else if(msgType == 'messageUpdate'){
			if(msgId == obsMap[roomId]['lastMsgId']) 
				setLastMsg(roomId, msgLastTxt);
		}else if(msgType == 'read'){
			if(userId && userId !== userManager.getLoginUserId()) return false;
			NotificationManager.removeTitleBadge(workspaceManager.getWorkspaceId(getRoomIdByUrl()));  // 타이틀 뱃지 삭제 호출
			setUnreadMsg(roomId, true);
		}
	}


	class RoomClass{
	    constructor(wsId, talkId, userId, lastMsgId, lastMsgTxt, unreadMsg, wsType, wsBookMark){
	        this.lastMsg = mobx.observable.box();
	        this.unreadMsg = mobx.observable.box();
	        this.bookMark = mobx.observable.box();
	        this.mobxBind();
	        this.wsId = wsId;
	        this.type = wsType;
	        
	        this.talkId = talkId;
	        this.userId = userId;
	        this.lastMsgId = lastMsgId;
	        this.bookMark.set(wsBookMark);
	        this.lastMsg.set(lastMsgTxt);
	        this.unreadMsg.set(Number(unreadMsg));
	        //this.eventBind();
	    }
	
	    mobxBind(){
	        mobx.observe(this.lastMsg,change=>{
	            let newLastMsg = change.newValue;
	            let lnbRoom = document.querySelector(`div#${CSS.escape(this.wsId)}`);
	            if(lnbRoom == null || lnbRoom == undefined) return;
	            let _target = lnbRoom.querySelector('.msgBody');
	            _target.innerHTML = newLastMsg;

	            if(this.type !== 'WKS0001'){
	            	//새로 온 메시지가 LNB 상단으로 올리는 로직
	            	let lnb = document.querySelector('div#ourSpaceLayout');
	            	let targetRoom = document.querySelector(`div#${CSS.escape(this.wsId)}`);
	            	let firstRoom = lnb.firstElementChild;
	            	if( lnb && targetRoom && (targetRoom !== firstRoom) && 
	            			change.oldValue && ("WKS0001" !== this.type) && (this.bookMark.value != 'COM0005') ) 
	            				lnb.insertBefore(targetRoom, firstRoom);
	            }
	        })
	        /*
	        * * setTalkCount 를 다른곳에서 못부르도록 가져오는게 필요
	        */
	        mobx.observe(this.unreadMsg,change=>{
	            let newUnreadMsg = change.newValue;
	            let oldUnreadMsg = change.oldValue? change.oldValue : 0; 
	            let changed = newUnreadMsg - oldUnreadMsg;
	            let targetLayout = this.bookMark.value === 'COM0005'? 'favSpaceLayout' : 'ourSpaceLayout';
	            let lnb = document.querySelectorAll(`div#${targetLayout}>div#${CSS.escape(this.wsId)}`);
	            if(lnb && lnb.length>0){
	            	lnb[0].querySelector('.unreadBadge').style.visibility = newUnreadMsg && newUnreadMsg>0 ? 'visible':'hidden';
	            	lnb[0].querySelector('.unreadCount').textContent = newUnreadMsg>99? "99+" : newUnreadMsg;
	            }
	            setTalkCount(getTalkCount() + changed );
	        }) 
	    }
	    /*
	    eventBind() {
	        let lnbRoom = document.querySelector(`div#${CSS.escape(this.wsId)}`);
	        lnbRoom.addEventListener('click',(event)=>{
	            event.stopPropagation();
	            event.defaultPrevented;
	            talkData.totUnreads.set(
	                Number(talkData.totUnreads) - Number(this.unreadMsg)
	            );
	            this.unreadMsg.set(Number('0'));        
	        })
	    }*/
	    
	}
	
	
	return {
		getSpaceProfile: function (SPACE_ID) {			// 존재하는지 체크 후 업데이트
			return _getSpaceProfile(SPACE_ID);		// 워크스페이스별 유저 조회후 리턴
		},

		createSpace: function (WS_NAME, ADMIN_ID, WS_TYPE, WsUserList, ROUTING_APP){
			_createSpace(WS_NAME, ADMIN_ID, WS_TYPE, WsUserList, ROUTING_APP);
		},

		inviteMember: function (WS_ID, WsUserList){
			_inviteMember(WS_ID, WsUserList);
		},

		exitSpace: function (WS_ID){
			_exitSpace(WS_ID);
		},
		spaceInit: function (){
			_spaceInit();
		},
		isTempSpace: function (SPACE_URL){
			return _isTempSpace(SPACE_URL);
		},
		drawLnbSpace: function (WS_ID, isTemp){
			_drawLnbSpace(WS_ID, isTemp);
		},
		removeLnbSpace: function (WS_ID){
			_removeLnbSpace(WS_ID);
		},
		updateSpaceDiv: function (ele){
			_updateSpaceDiv(ele);
		},
		createSpaceDiv: function (ele){
			return _createSpaceDiv(ele);
		},
		afterCreateSpaceDiv: function(id){
			_afterCreateSpaceDiv(id)
		},
		setSpace: function (WS_ID){
			_setSpace(WS_ID);
		},
		changeSpace: function(spaceUrl = getRoomIdByUrl()){
			_changeSpace(spaceUrl = getRoomIdByUrl());
		},
		getSpaceMemberListBrief: function(WS_ID, USER_ID = userManager.getLoginUserId()){
			return _getSpaceMemberListBrief(WS_ID, USER_ID = userManager.getLoginUserId())
		},
		getTempManager: function(){
			return _tempManager
		},
		updateTempManager : function(){
			return _updateTempManager();	
		},		
		updateTempManagerSync : function(){
			_updateTempManagerSync();
		},
		truncateTempManager : function(WS_ID){
			_truncateTempManager(WS_ID);	
		},
		initRoomList : _initRoomList,
		setUnreadMsg,
		setLastMsg,
		setRoomInfo,
		setBookMark
	}

})();



//
//
//function createSpace(WS_NAME, ADMIN_ID, WS_TYPE, WsUserList, ROUTING_APP){
//	let _result;
//	let spaceUrl;
//	let target;
//	
//	if(WsUserList.length == 1){
//	    jQuery.ajax({
//	        type: "POST",
//	        url: _workspace.url + "SpaceRoom/SpaceRoom",
//			dataType : "json",
//			data : JSON.stringify({
//				"dto" :{
//					"WS_NAME" : WS_NAME,
//					"ADMIN_ID" : ADMIN_ID,
//					"WS_TYPE" : WS_TYPE,
//					"WsUserList" : WsUserList,
//					"WS_ACCESS_TYPE":"DM"
//				}
//			}),
//	        contentType: "application/json",
//	        async: false,
//	        success: function (result) {
//	            _result = result.dto
//	            if(workspaceManager.getWorkspace(_result.RESULT_VALUE).MEM_JOB == 'N'){
//	        		jQuery.ajax({
//	        			type: "PUT",
//	        			url: _workspace.url + "SpaceRoom/SpaceRoomMember",
//	        			dataType : "json",
//	        			data : JSON.stringify({
//	        				"dto":
//	        				{
//	        					"WS_ID":_result.RESULT_VALUE,
//	        					"USER_ID":ADMIN_ID,
//	        					"MEM_JOB":"Y"
//	        				}
//	        			}),
//	       		     	contentType: "application/json",
//	       		     	async: false,
//	       		     	success: function (result) {
//	        				if(result.dto.RESULT_CD == "RST0001")
//		        				drawLnbSpace(_result.RESULT_VALUE)
//	       		     	}
//	        		})	
//
//	            }
//	        }
//	    })
//	}
//	else{
//	    jQuery.ajax({
//	        type: "POST",
//	        url: _workspace.url + "SpaceRoom/SpaceRoom",
//			dataType : "json",
//			data : JSON.stringify({
//				"dto" :{
//					"WS_NAME" : WS_NAME,
//					"ADMIN_ID" : ADMIN_ID,
//					"WS_TYPE" : WS_TYPE,
//					"WsUserList" : WsUserList
//				}
//			}),
//	        contentType: "application/json",
//	        async: false,
//	        success: function (result) {
//	            _result = result.dto
//	        }
//	    }) 
//	}
//	
//	workspaceManager.update();
//    spaceUrl = _result.RESULT_URL
////    $('div.lnbSpaceList').removeClass('selected')
////    $('#'+_result.RESULT_VALUE).addClass('selected')
//    
//
//    
//    if(ROUTING_APP == "MEETING"){
//    	appManager.setMainApp('talk')
//	    appManager.setSubApp('meeting');
//	    target = `/s/${spaceUrl}/${appManager.getMainApp()}?sub=${appManager.getSubApp()}`;
//	    Top.App.routeTo(target, {eventType:'fold'})
//    }
//    else{
//        appManager.setMainApp('talk');
//        appManager.setSubApp(null);
//        target = `/s/${spaceUrl}/${appManager.getMainApp()}`;
//        Top.App.routeTo(target, {eventType: 'close'});
//    }
//}
//
//function inviteMember(WS_ID, WsUserList){
//	let WS_ACCESS_TYPE = workspaceManager.getWorkspace(WS_ID).WS_ACCESS_TYPE;
//	let WS_TYPE = workspaceManager.getWorkspace(WS_ID).WS_TYPE;
//	let USER_ID = userManager.getLoginUserId();
//	let USER_LIST = workspaceManager.getWorkspace(WS_ID).USER_LIST.ROOM_USER_LIST;
//
//	
//	if(WS_ACCESS_TYPE == "DM" || WS_TYPE == 'WKS0001'){
//		
//		for(i=1; i<USER_LIST.length;i++){
//			WsUserList.push({"USER_ID":USER_LIST[i].USER_ID})
//		}
//		createSpace(null, USER_ID, "WKS0002", WsUserList)
//	}
//	
//	else{
//		Top.Ajax.execute({
//			url : _workspace.url + "SpaceRoom/SpaceRoomMember",
//			type : 'POST',
//			dataType : "json",
//			async : false,
//			cache : false,
//			data : JSON.stringify({
//				"dto":
//				{
//					"WS_ID":WS_ID,
//					"SENDER_ID":USER_ID,
//					"WsUserList":WsUserList
//				}
//			}), // arg
//			contentType : "application/json",
//			crossDomain : true,
//			success : function(result) {
//				workspaceManager.update()
//				drawLnbSpace(WS_ID)
//			}
//		});	
//		
//	}
//}
//
//function exitSpace(WS_ID){
//	
//	let WS_ACCESS_TYPE = workspaceManager.getWorkspace(WS_ID).WS_ACCESS_TYPE;
//	let USER_LIST = workspaceManager.getWorkspace(WS_ID).USER_LIST.ROOM_USER_LIST;
//	let USER_ID = userManager.getLoginUserId();
//	let WS_TYPE =  workspaceManager.getWorkspace(WS_ID).WS_TYPE;
//	let PREV_WS_ID = $('div.lnbSpaceList#'+WS_ID).prev().attr('id')
//	let NEXT_WS_ID = $('div.lnbSpaceList#'+WS_ID).next().attr('id')
//	
//	if(WS_TYPE == 'WKS0001'){
//		return;
//	}
//	
//	if(WS_ACCESS_TYPE == "DM"){
//		
//		Top.Ajax.execute({
//			url : _workspace.url + "SpaceRoom/SpaceRoomMember",
//			type : 'PUT',
//			dataType : "json",
//			async : false,
//			cache : false,
//			data : JSON.stringify({
//				"dto":
//				{
//					"WS_ID":WS_ID,
//					"USER_ID":USER_ID,
//					"MEM_JOB":"N"
//				}
//			}), // arg
//			contentType : "application/json",
//			crossDomain : true,
//			success : function(result) {
//				updateCommonUnread(WS_ID);
//				removeLnbSpace(WS_ID)
//				if(workspaceManager.getWorkspaceId(getRoomIdByUrl()) == WS_ID){
//					if(NEXT_WS_ID){
//						changeSpace(NEXT_WS_ID)
//					}
//					else if(PREV_WS_ID){
//						changeSpace(PREV_WS_ID)
//					}
//					else{
//						
//					}
//					
//				}
//				workspaceManager.update()
//			}
//		});	
//	}
//	else{
//		if(USER_LIST.length == 1 && USER_LIST[0].USER_ID == USER_ID){
//			Top.Ajax.execute({
//				url : _workspace.url + "SpaceRoom/SpaceRoom",
//				type : 'DELETE',
//				dataType : "json",
//				async : false,
//				cache : false,
//				data : JSON.stringify({
//					"dto":
//					{
//						"WS_ID":WS_ID
//					}
//				}), // arg
//				contentType : "application/json",
//				crossDomain : true,
//				success : function(result) {
//					updateCommonUnread(WS_ID);					
//					removeLnbSpace(WS_ID)
//					if(workspaceManager.getWorkspaceId(getRoomIdByUrl()) == WS_ID){
//						if(NEXT_WS_ID){
//							changeSpace(NEXT_WS_ID)
//						}
//						else if(PREV_WS_ID){
//							changeSpace(PREV_WS_ID)
//						}
//						else{
//							
//						}
//						
//					}
//					workspaceManager.update()
//				}
//			});	
//		}
//		else{
//			Top.Ajax.execute({
//				url : _workspace.url + "SpaceRoom/SpaceRoomMember",
//				type : 'DELETE',
//				dataType : "json",
//				async : false,
//				cache : false,
//				data : JSON.stringify({
//					"dto":
//					{
//						"FLAG1":USER_ID, 
//						"WS_ID":WS_ID,
//						"FLAG1":USER_ID,
//						"WsUserBanList":[{"USER_ID": USER_ID}]
//					}
//				}), // arg
//				contentType : "application/json",
//				crossDomain : true,
//				success : function(result) {
//					updateCommonUnread(WS_ID);	
//					removeLnbSpace(WS_ID)
//					if(workspaceManager.getWorkspaceId(getRoomIdByUrl()) == WS_ID){
//						if(NEXT_WS_ID){
//							changeSpace(NEXT_WS_ID)
//						}
//						else if(PREV_WS_ID){
//							changeSpace(PREV_WS_ID)
//						}
//						else{
//							
//						}
//						
//					}
//					workspaceManager.update()
//				}
//			});	
//			
//		}
//		
//	}
//
//	
//	
//	
//	
//}
//
//function tempSpaceInit(){
//	if(isTempSpace(getRoomIdByUrl())){
//		jQuery.ajax({
//		     type: "POST",
//		     url: _workspace.url + "SpaceRoom/SpaceRoomTemp?action=Init",
//				dataType : "json",
//				data : JSON.stringify({
//					"dto" :{
//						"WS_ID" : workspaceManager.getWorkspaceId(getRoomIdByUrl())
//					}
//				}),
//		     contentType: "application/json",
//		     async: false,
//		     success: function (result) {
//		     	workspaceManager.update();
//		     }
//		 })	
//	}
//}
//
//
//	 
//	 
//function isTempSpace(WS_ID){
//	
//	let _result;
//	
//	jQuery.ajax({
//        type: "POST",
//        url: _workspace.url + "SpaceRoom/SpaceRoomTemp?action=Get",
//		dataType : "json",
//		data : JSON.stringify({
//			"dto" :{
//				"WS_ID" : WS_ID,
//			}
//		}),
//        contentType: "application/json",
//        async: false,
//        success: function (result) {
//        	if(result.dto == undefined){
//            	_result = false;
//            }
//            else{
//            	_result = true;
//            }
//        }
//    })
//    
//    return _result;
//}
//	
//
//function drawLnbSpace(WS_ID, isTemp){
//	
//	if($('top-button#AppGnbRoomButton').hasClass('active')){
// 
//		let _result;
//		
//		jQuery.ajax({
//	        type: "GET",
//	        url: _workspace.url + "SpaceRoom/LnbSpace",
//			dataType : "json",
//			data : JSON.stringify({
//				"dto":{
//					"WS_ID":WS_ID,
//					"USER_ID":userManager.getLoginUserId(),
//					"TASK_TYPE":isTemp
//					}
//			}),
//	        contentType: "application/json",
//	        async: false,
//	        success: function (result) {
//	        	_result = result.dto
////	        	_result.USER_LIST.UserProfileList.forEach(function(ele){
////	        		if(!ele.THUMB_PHOTO){
////	        			ele.THUMB_PHOTO = userManager.getUserDefaultPhotoURL(ele.USER_ID);
////	        		}
////	        	})
//	        }
//	    })		
//	    
//	    if($('div.lnbSpaceList#'+_result.WS_ID).length == 0){
//	    	createSpaceDiv(_result)
//	    }
//	    else{
//	    	updateSpaceDiv(_result)
//	    }
//	}
//	else{
//		return;
//	}
//}
//
//function removeLnbSpace(WS_ID){
//	if($('top-button#AppGnbRoomButton').hasClass('active')){
//		if($('div.lnbSpaceList#'+WS_ID)){
//			$('div.lnbSpaceList#'+WS_ID).remove();
//			return;
//		}
//		else{
//			return;
//		}
//	}
//	else{
//		return;
//	}
//}
//
//
//function updateSpaceDiv(ele){
//
//	let template;
//	let WS_ID = ele.WS_ID;
//	let WS_NAME;
//	let MSG_BODY = '';
//	let USER_COUNT = 0;
//	let UNREAD_USER_COUNT = 0;
//	let USER_LIST = ele.USER_LIST? ele.USER_LIST.UserProfileList : '';
//    let THUMB_PHOTO0 = USER_LIST[0]? userManager.getUserPhoto(USER_LIST[0].USER_ID, "small", USER_LIST[0].THUMB_PHOTO) : ''
//    let THUMB_PHOTO1 = USER_LIST[1]? userManager.getUserPhoto(USER_LIST[1].USER_ID, "small", USER_LIST[1].THUMB_PHOTO) : ''
//    let THUMB_PHOTO2 = USER_LIST[2]? userManager.getUserPhoto(USER_LIST[2].USER_ID, "small", USER_LIST[2].THUMB_PHOTO) : ''
//    let THUMB_PHOTO3 = USER_LIST[3]? userManager.getUserPhoto(USER_LIST[3].USER_ID, "small", USER_LIST[3].THUMB_PHOTO) : ''
//    let THUMB_PHOTO4 = USER_LIST[4]? userManager.getUserPhoto(USER_LIST[4].USER_ID, "small", USER_LIST[4].THUMB_PHOTO) : ''
//    let spaceDiv = $('div.lnbSpaceList#'+ele.WS_ID)
//	
//    if(workspaceManager.getWorkspace(WS_ID) && workspaceManager.getWorkspace(WS_ID).WS_TYPE == 'WKS0001'){
//    	WS_NAME = userManager.getLoginUserName() + ' (나)'
//    }
//    else{
//        if(ele.WS_NAME){
//        	WS_NAME = ele.WS_NAME
//        }
//        else{
//        	WS_NAME = ele.User_name
//        }	
//    }
//    
//    if(ele.MSG_BODY){            	
//    	MSG_BODY = ele.MSG_BODY;
//    } 
//    if(ele.USER_COUNT && workspaceManager.getWorkspace(WS_ID).WS_ACCESS_TYPE != 'DM' && workspaceManager.getWorkspace(WS_ID).WS_TYPE != 'WKS0001'){
//    	USER_COUNT = ele.USER_COUNT;
//    }
//    else{
//    	USER_COUNT = ''
//    }
//    
//    if(ele.UNREAD_USER_COUNT){
//        UNREAD_USER_COUNT = ele.UNREAD_USER_COUNT
//        spaceDiv.find('.unreadBadge').css('visibility','visible');
//    }
//    else{
//        spaceDiv.find('.unreadBadge').css('visibility','hidden')
//    }
//    
//    
//	switch (USER_LIST.length){
//	case 0:
//		template = '<img class=thumbPhoto1_1 src={THUMB_PHOTO1}>' 
//		template = template.replace('{THUMB_PHOTO1}','')
//		break;
//	case 1:
//		template = '<img class=thumbPhoto1_1 src={THUMB_PHOTO1}>'
//
//		template = template.replace('{THUMB_PHOTO1}',THUMB_PHOTO0)
//		break;
//	case 2:
//		template = '<img class=thumbPhoto1_1 src={THUMB_PHOTO1}>'		
//		template = template.replace('{THUMB_PHOTO1}',THUMB_PHOTO1)
//		break;
//	case 3:
//		template = '<img class=thumbPhoto1_2 src={THUMB_PHOTO1}>'+
//				   '<img class=thumbPhoto2_2 src={THUMB_PHOTO2}>'
//		template = template.replace('{THUMB_PHOTO1}',THUMB_PHOTO1)
//		.replace('{THUMB_PHOTO2}',THUMB_PHOTO2)
//		break;
//	case 4:
//		template = '<img class=thumbPhoto1_3 src={THUMB_PHOTO1}>'+
//				   '<img class=thumbPhoto2_3 src={THUMB_PHOTO2}>'+
//				   '<img class=thumbPhoto3_3 src={THUMB_PHOTO3}>'
//		template = template.replace('{THUMB_PHOTO1}',THUMB_PHOTO1)
//		.replace('{THUMB_PHOTO2}',THUMB_PHOTO2)
//		.replace('{THUMB_PHOTO3}',THUMB_PHOTO3)
//		break;
//	default:
//		template = '<img class=thumbPhoto1_4 src={THUMB_PHOTO1}>'+
//				   '<img class=thumbPhoto2_4 src={THUMB_PHOTO2}>'+
//				   '<img class=thumbPhoto3_4 src={THUMB_PHOTO3}>'+
//				   '<img class=thumbPhoto4_4 src={THUMB_PHOTO4}>'
//		template = template.replace('{THUMB_PHOTO1}',THUMB_PHOTO1)
//		.replace('{THUMB_PHOTO2}',THUMB_PHOTO2)
//		.replace('{THUMB_PHOTO3}',THUMB_PHOTO3)
//		.replace('{THUMB_PHOTO4}',THUMB_PHOTO4)	
//	}
//    
//    
//	spaceDiv.find('.wsName').text(WS_NAME)
//	spaceDiv.find('.userCount').text(USER_COUNT)
////	spaceDiv.find('.msgBody').text(MSG_BODY) 
//	spaceDiv.find('.unreadCount').text(UNREAD_USER_COUNT)
//	spaceDiv.find('.thumbDiv').html(template)
//	
//}
//
//
//
//
//function createSpaceDiv(ele){
//
//	this.clickid;
//	this.isedit = false;
//	const contextMenu = new ContextMenu();
//	contextMenu.addItem('멤버 보기', function(e) {
//		Top.Controller.get('spaceListLayoutLogic').isedit = false;
//		contextMenu.hide();
//		// if(!workspaceManager.getWorkspace(showmemClickid).WS_ACCESS_TYPE){
//			Top.Controller.get('spaceListLayoutLogic').isedit = false;
//			Top.Dom.selectById('ShowMemDialog').open();
//		// }
//		// else{
//		// 	spaceAPI.getSpaceMemberListBrief(showmemClickid).then(result => {
//		// 			for(var i = 0; i < result.data.dto.UserProfileList.length; i++){
//		// 				if(result.data.dto.UserProfileList[i].USER_ID != userManager.getLoginUserId()){
//		// 					_UserId = result.data.dto.UserProfileList[i].USER_ID
//		// 					break;
//		// 				}
//		// 			}
//		// 			_friendManageProfilePopupWithPosition(_UserId, 800 , 50)
//		// 		});
//		// } 
//	});
//	contextMenu.addItem('이름 변경', function(e) {
//		Top.Controller.get('spaceListLayoutLogic').isedit = true;
//		Top.Dom.selectById('ShowMemDialog').open();
//		contextMenu.hide();
//	});
//	contextMenu.addItem('나가기', function(e) {
//		Top.Dom.selectById('OutSpaceDialog').open();
//		contextMenu.hide();
//	});
//
//	document.body.appendChild(contextMenu.containerElem);
//    	
//	Top.Dom.selectById("spaceListSearch").removeClass('click-placeholder') 
//    let template1 = 
//'<div id={WS_ID}  class=lnbSpaceList>'+
//	'<div class=thumbDiv>'+
//		'<img class=thumbPhoto1_1 src={THUMB_PHOTO1}>'+
//	'</div>'+
//    '<div class=body >'+
//    	'<div class=wsBody>'+ 
//        	'<div class=wsName >{WS_NAME}</div>'+
//        	'<div class=userCount >{USER_COUNT}</div>'+
//    	'</div>'+
//        '<div class=msgBody>{MSG_BODY}</div>'+
//    '</div>'+
//    '<div class=unreadBadge style="visibility:{UNREAD_BADGE};"> '+
//        '<div class=unreadCount>{UNREAD_USER_COUNT}</div>'+
//	'</div>'+
//	`
//    	<div class = sp_menu style="display : none;"> 
//    		<button class ="icon-ellipsis_vertical_small" type ="button"></button>
//    		<button class ="icon-deploy" type ="button"></button>
//    	</div>
//    `+
//'</div>'
//    
//    let template2 = 
//'<div id={WS_ID}  class=lnbSpaceList>'+
//	'<div class=thumbDiv>'+
//		'<img class=thumbPhoto1_2 src={THUMB_PHOTO1}>'+
//		'<img class=thumbPhoto2_2 src={THUMB_PHOTO2}>'+
//	'</div>'+
//	'<div class=body >'+
//		'<div class=wsBody>'+ 
//			'<div class=wsName >{WS_NAME}</div>'+
//			'<div class=userCount >{USER_COUNT}</div>'+
//		'</div>'+
//		'<div class=msgBody>{MSG_BODY}</div>'+
//	'</div>'+
//	'<div class=unreadBadge style="visibility:{UNREAD_BADGE};"> '+
//		'<div class=unreadCount>{UNREAD_USER_COUNT}</div>'+
//	'</div>'+
//	`
//    	<div class = sp_menu style="display : none;"> 
//    		<button class ="icon-ellipsis_vertical_small" type ="button"></button>
//    		<button class ="icon-deploy" type ="button"></button>
//    	</div>
//    `+
//'</div>'
//            
//    let template3 = 
//'<div id={WS_ID}  class=lnbSpaceList>'+
//	'<div class=thumbDiv>'+
//		'<img class=thumbPhoto1_3 src={THUMB_PHOTO1}>'+
//		'<img class=thumbPhoto2_3 src={THUMB_PHOTO2}>'+
//		'<img class=thumbPhoto3_3 src={THUMB_PHOTO3}>'+
//	'</div>'+
//	'<div class=body >'+
//		'<div class=wsBody>'+ 
//			'<div class=wsName >{WS_NAME}</div>'+
//			'<div class=userCount >{USER_COUNT}</div>'+
//		'</div>'+
//		'<div class=msgBody>{MSG_BODY}</div>'+
//	'</div>'+
//	'<div class=unreadBadge style="visibility:{UNREAD_BADGE};"> '+
//		'<div class=unreadCount>{UNREAD_USER_COUNT}</div>'+
//	'</div>'+
//	`
//    	<div class = sp_menu style="display : none;"> 
//    		<button class ="icon-ellipsis_vertical_small" type ="button"></button>
//    		<button class ="icon-deploy" type ="button"></button>
//    	</div>
//    `+
//'</div>'
//            
//    let template4 = 
//'<div id={WS_ID}  class=lnbSpaceList>'+
//	'<div class=thumbDiv>'+
//		'<img class=thumbPhoto1_4 src={THUMB_PHOTO1}>'+
//		'<img class=thumbPhoto2_4 src={THUMB_PHOTO2}>'+
//		'<img class=thumbPhoto3_4 src={THUMB_PHOTO3}>'+
//		'<img class=thumbPhoto4_4 src={THUMB_PHOTO4}>'+
//	'</div>'+
//	'<div class=body >'+
//		'<div class=wsBody>'+ 
//			'<div class=wsName >{WS_NAME}</div>'+
//			'<div class=userCount >{USER_COUNT}</div>'+
//		'</div>'+
//		'<div class=msgBody>{MSG_BODY}</div>'+
//	'</div>'+
//	'<div class=unreadBadge style="visibility:{UNREAD_BADGE};"> '+
//		'<div class=unreadCount>{UNREAD_USER_COUNT}</div>'+
//	'</div>'+
//	`
//    	<div class = sp_menu style="display : none;"> 
//    		<button class ="icon-ellipsis_vertical_small" type ="button"></button>
//    		<button class ="icon-deploy" type ="button"></button>
//    	</div>
//    `+
//'</div>'
//
//$('div.lnbSpaceList').ready(function(){
//	
//	setSpace(getRoomIdByUrl()); 
//	
//	$('div.lnbSpaceList').on('click', '.icon-ellipsis_vertical_small', function (e) {
//		e.preventDefault();
//		e.stopPropagation();
//		showmemClickid = $(this).parent().parent().attr('id')
//		if(workspaceManager.getMySpaceId()!=showmemClickid)contextMenu.show($(this).offset().left, $(this).offset().top);
//	});
//	$('div.lnbSpaceList').on('click', '.icon-deploy', function (e) {
//		var spaceid = $(this).parent().parent().attr('id');
//		e.preventDefault();
//		e.stopPropagation();
//		openMiniTalk(spaceid);
//	});
//	
//	// $(this).children('.sp_menu').children('.icon-ellipsis_vertical_small').click(function(){
//	// 	alert('a');
//	// 	contextMenu.show($(this).offset().left, $(this).offset().top);
//	// })
//	$('div.lnbSpaceList').hover(function(){
//		$(this).children('.unreadBadge').css('display','none');
//		$(this).children('.sp_menu').css('display','block');
//	},
//	function(){
//		$(this).children('.sp_menu').css('display','none');
//		$(this).children('.unreadBadge').css('display','block');
//	})
//});
//
//
//    	
//    	let template;       	
//        let WS_ID = ele.WS_ID;
//        let WS_NAME;
//        let MSG_BODY = '';
//    	let USER_LIST = ele.USER_LIST? ele.USER_LIST.UserProfileList : '';
//        let UNREAD_USER_COUNT = 0;
//        let UNREAD_BADGE = 'visible';
//        let THUMB_PHOTO0 = USER_LIST[0]? userManager.getUserPhoto(USER_LIST[0].USER_ID, "small", USER_LIST[0].THUMB_PHOTO) : ''
//        let THUMB_PHOTO1 = USER_LIST[1]? userManager.getUserPhoto(USER_LIST[1].USER_ID, "small", USER_LIST[1].THUMB_PHOTO) : ''
//        let THUMB_PHOTO2 = USER_LIST[2]? userManager.getUserPhoto(USER_LIST[2].USER_ID, "small", USER_LIST[2].THUMB_PHOTO) : ''
//        let THUMB_PHOTO3 = USER_LIST[3]? userManager.getUserPhoto(USER_LIST[3].USER_ID, "small", USER_LIST[3].THUMB_PHOTO) : ''
//        let THUMB_PHOTO4 = USER_LIST[4]? userManager.getUserPhoto(USER_LIST[4].USER_ID, "small", USER_LIST[4].THUMB_PHOTO) : ''
//        let USER_COUNT = 0;
//
//        if(ele.MSG_BODY){            	
//        	MSG_BODY = ele.MSG_BODY;
//        }
//        
//        if(workspaceManager.getWorkspace(WS_ID) && workspaceManager.getWorkspace(WS_ID).WS_TYPE == 'WKS0001'){
//        	WS_NAME = userManager.getLoginUserName() + ' (나)'
//        }
//        else{
//            if(ele.WS_NAME){
//            }
//            else{
//            	WS_NAME = ele.User_name
//            }	
//        }
//        
//        
//        
//
//        
//        if(ele.UNREAD_USER_COUNT){
//            UNREAD_USER_COUNT = ele.UNREAD_USER_COUNT
//        }
//        else{
//            UNREAD_BADGE = 'hidden'
//        }
//        if(ele.USER_COUNT && workspaceManager.getWorkspace(WS_ID).WS_ACCESS_TYPE != 'DM' && workspaceManager.getWorkspace(WS_ID).WS_TYPE != 'WKS0001'){
//        	USER_COUNT = ele.USER_COUNT;
//        }
//        else{
//        	USER_COUNT = ''
//        }
//        
//
//
//    	switch (USER_LIST.length){
//    	case 0:
//    		template = template1; 
//    		template = template.replace('{THUMB_PHOTO1}','')
//    		break;
//    	case 1:
//    		template = template1; 
//    		template = template.replace('{THUMB_PHOTO1}',THUMB_PHOTO0)
//    		break;
//    	case 2:
//    		template = template1; 		
//    		template = template.replace('{THUMB_PHOTO1}',THUMB_PHOTO1)
//    		break;
//    	case 3:
//    		template = template2;
//    		template = template.replace('{THUMB_PHOTO1}',THUMB_PHOTO1)
//    		.replace('{THUMB_PHOTO2}',THUMB_PHOTO2)
//    		break;
//    	case 4:
//    		template = template3;
//    		template = template.replace('{THUMB_PHOTO1}',THUMB_PHOTO1)
//    		.replace('{THUMB_PHOTO2}',THUMB_PHOTO2)
//    		.replace('{THUMB_PHOTO3}',THUMB_PHOTO3)
//    		break;
//    	default:
//        	template = template4;
//    		template = template.replace('{THUMB_PHOTO1}',THUMB_PHOTO1)
//    		.replace('{THUMB_PHOTO2}',THUMB_PHOTO2)
//    		.replace('{THUMB_PHOTO3}',THUMB_PHOTO3)
//    		.replace('{THUMB_PHOTO4}',THUMB_PHOTO4)	
//    	}
//    	
//    	let _template = template
//        .replace(/{WS_ID}/gi,WS_ID)
//        .replace('{WS_NAME}',WS_NAME)
//        .replace('{MSG_BODY}',talkContent.convertToMsgBodyText(ele))
//        .replace('{UNREAD_USER_COUNT}',UNREAD_USER_COUNT)
//        .replace('{UNREAD_BADGE}',UNREAD_BADGE)
//        .replace('{USER_COUNT}',USER_COUNT)
//
//        $('div#ourSpaceLayout').prepend(_template);
//
//         
//
//        
//
//        $('div.lnbSpaceList').click(function(){    	
//        	
////        	$(this).find('.unreadBadge').css('visibility','hidden');		--commonCall
////        	$(this).find('.unreadCount').text(0);
//        	NotificationManager.removeTitleBadge($(this).attr('id'));
//
//        	let spaceUrl = workspaceManager.getWorkspaceUrl($(this).attr('id')) 	
//
//        	changeSpace(spaceUrl)
//        	
//        })
//
//    
//
//}
//
//function setSpace(id){
//	let spcId = workspaceManager.getWorkspaceId(id);
//	$(`div#spaceListLayoutBody div.selected`).removeClass('selected');
//	$(`div#spaceListLayoutBody div#${spcId}`).addClass('selected');
//	
//}

function roomOnDrop(e, dropData){
	const target = e.target.closest('.dnd__container');
    const {fromApp, data, files} = dropData;
    const id = target.id;
    
    // 내 로컬에서 drag & drop하는 경우
	if (!fromApp && e.dataTransfer["files"].length) {
		if (document.querySelector('#top-dialog-root_talk__file-upload-dialog')) {
			return
		}
		let fileChooserList = e.dataTransfer["files"];
        let fileCount = fileChooserList.length;
        
        if (fileCount) {
    		let temp=[];
    		
            for (let i = 0; i < fileCount; i++) {
                if (talkFileUpload.isFileNameLong(fileChooserList[i]["name"])) {
                	TeeToast.open({text: '파일명 70자 초과 파일은 업로드할 수 없습니다.'});
                } else {
                	talkData.fileChooserList.push(fileChooserList[i]);
                	temp.push(fileChooserList[i]) // addFileChip할 것
                }
                
            	if (talkData.fileChooserList.length >= 30) {
                	TeeToast.open({text: '파일 전송은 한번에 30개까지 가능합니다.'});
            		break;
            	}
            }
            
            if (temp.length) {
    			talkFileUpload.onOpenFileUploadDialog(temp);
            }
        }
        
        return;
	} 
	
    switch(fromApp){
        // fropApp 에 따른 처리 (타앱 -> drive)
        // 타 앱에서 이미 업로드된 파일이므로, file id 만 알면 복사 가능할 듯.
        case "drive" : {
            // create 파일 업로드하는 message
            let tmp_arr = [dropData.data];
            if(tmp_arr.length)
                talkOpenAPI.openTalkFileUpload(talkData.workspaceId,
                     workspaceManager.getChannelList(talkData.workspaceId, "CHN0006"), 
                     tmp_arr);
                }
            break;

        case "note" :
            break;

        case "mail" :
            break;

        case "meeting" :
            break;

        case "office" :
            break;

        case "schedule" : 
            break;

        case "plus" : {
            let tmp_arr = [dropData.data];
            if(tmp_arr.length)
                talkOpenAPI.openTalkFileUpload(talkData.workspaceId,
                     workspaceManager.getChannelList(talkData.workspaceId, "CHN0006"), 
                     tmp_arr);
        }
            break;
        default :
            break;
    }
}
function roomOnDrop(e, dropData){
	const target = e.target.closest('.dnd__container');
    const {fromApp, data, files} = dropData;
    const id = target.id;
    
    // 내 로컬에서 drag & drop하는 경우
	if (!fromApp && e.dataTransfer["files"].length) {
		if (document.querySelector('#top-dialog-root_talk__file-upload-dialog')) {
			return
		}
		let fileChooserList = e.dataTransfer["files"];
        let fileCount = fileChooserList.length;
        
        if (fileCount) {
    		let temp=[];
    		
            for (let i = 0; i < fileCount; i++) {
                if (talkFileUpload.isFileNameLong(fileChooserList[i]["name"])) {
                	TeeToast.open({text: '파일명 70자 초과 파일은 업로드할 수 없습니다.'});
                } else {
                	talkData.fileChooserList.push(fileChooserList[i]);
                	temp.push(fileChooserList[i]) // addFileChip할 것
                }
                
            	if (talkData.fileChooserList.length >= 30) {
                	TeeToast.open({text: '파일 전송은 한번에 30개까지 가능합니다.'});
            		break;
            	}
            }
            
            if (temp.length) {
    			talkFileUpload.onOpenFileUploadDialog(temp);
            }
        }
        
        return;
	} 
	
    switch(fromApp){
        // fropApp 에 따른 처리 (타앱 -> drive)
        // 타 앱에서 이미 업로드된 파일이므로, file id 만 알면 복사 가능할 듯.
        case "drive" : {
            // create 파일 업로드하는 message
            let tmp_arr = [dropData.data];
            if(tmp_arr.length)
                talkOpenAPI.openTalkFileUpload(id,
                     workspaceManager.getChannelList(id, "CHN0006"), 
                     tmp_arr);
                }
            break;

        case "note" :
            break;

        case "mail" :
            break;

        case "meeting" :
            break;

        case "office" :
            break;

        case "schedule" : 
            break;

        case "plus" : {
            let tmp_arr = [dropData.data];
            if(tmp_arr.length)
                talkOpenAPI.openTalkFileUpload(id,
                     workspaceManager.getChannelList(id, "CHN0006"), 
                     tmp_arr);
        }
            break;
        default :
            break;
    }
}



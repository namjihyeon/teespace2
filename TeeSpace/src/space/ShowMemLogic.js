var showmemClickid;

ShowMemDialog = function(){
	function Dialog(){
		const template = htmlToElement(`
		<div id="show-member-container">
			<div class = memtop-container>
				<div class="btn-div">
					<button class="icon-close"></button>
				</div>
				<div class="space-img">
					<span class="thumbDiv"></span>
				</div>
				<div class = "edit-container">
					<div class="space-name">
						<span name="name_span">디자인방</span>
					</div>
					<div class="mem-num">	
						<span name="num_span">4명</span>
					</div>
					<div class="edit-btn">
						<div class ="edit-name edit">
							<i class ="icon-work_edit"></i>
							<span><br>이름 변경</span>						
						</div>
						<div class ="teetalk-div ttalk">
							<i class ="icon-ttalk1"></i>
							<span><br>TeeTalk</span>
						</div>
						<div class ="teemeeting-div tmeeting">
							<i class ="icon-tmeet"></i>
							<span><br>TeeMeeting</span> 
						</div>
					</div>
				</div>
			</div>
			<div class="mem-list-container">
				<ul class="mem-list">
				</ul>
			</div>
			<div class="invite-container">
				<div class ="mem-invite add">
					<i class ="friends_add">멤버 초대</i>
					<span>멤버 초대</span>
				</div>
			</div>
		</div>
		`);
		
		var n = showmemClickid
		var a = $('#'+n+' .thumbDiv').clone().wrapAll("<div/>").parent().html();
		template.querySelector('.space-img').replaceChild(htmlToElement(a),template.querySelector('.thumbDiv'));

		template.querySelector('.icon-close').addEventListener('click',function(e){
			Top.Dom.selectById('ShowMemDialog').close(true);
		});

		const edit_template = htmlToElement(`
			<div class = "edit-container">
					<div class="edit-space-name">
						<input class = "notsp" name="edit_name_span" maxlength = 50 placeholder ="{defaultspace}">디자인방</span>
					</div>
					<div class="mem-num">	
						<span name="num_span">4명</span>
					</div>
					<div class="edit-btn2">
						<div class ="save-btn">
							<button class ="solid save">저장</button>						
						</div>
						<div class ="cancel-btn">
							<button class ="outlined cancel">취소</button>
						</div>
					</div>
				</div>
			`);
		edit_template.querySelector('.edit-space-name input').setAttribute('placeholder',workspaceManager.getWorkspace(showmemClickid).User_name)
		const re_template = htmlToElement(`
		<div class = "edit-container">
		<div class="space-name">
			<span name="name_span">디자인방</span>
		</div>
		<div class="mem-num">	
			<span name="num_span">4명</span>
		</div>
		<div class="edit-btn">
			<div class ="edit-name edit">
				<i class ="icon-work_edit"></i>
				<span><br>이름 변경</span>						
			</div>
			<div class ="teetalk-div ttalk">
				<i class ="icon-ttalk1"></i>
				<span><br>TeeTalk</span>
			</div>
			<div class ="teemeeting-div tmeeting">
				<i class ="icon-tmeet"></i>
				<span><br>TeeMeeting</span> 
			</div>
		</div>
	</div>`)

		template.querySelector('.friends_add').addEventListener('click', function (e) {
		});
		template.addEventListener('click', function(e) {
			const edit = e.target.closest('div.edit');
			const cancel = e.target.closest('button.cancel');
			const save = e.target.closest('button.save');
			const add = e.target.closest('div.add');
			const tmeet = e.target.closest('div.tmeeting');
			const ttalk = e.target.closest('div.ttalk');
			const img = e.target.closest('.mem-list img');
			if (edit) {
				$('#asbf').off('click');
				edit_template.querySelector('.edit-space-name input').value =  template.querySelector('.space-name span').textContent;
				edit_template.querySelector('.mem-num span').textContent =
								($('#' + showmemClickid + ' .userCount').text() != "" ? $('#' + showmemClickid + ' .userCount').text() : 2) + "명";
				template.querySelector('.memtop-container').replaceChild(edit_template, template.querySelector('.edit-container'));
				$('.edit-space-name input').focus();
				Top.Controller.get('spaceListLayoutLogic').isedit = true;
				$(".save-btn button").prop("disabled", true);
				$(".save-btn button").removeClass('solid');
				$(".save-btn button").addClass('disabled');
				$("input.notsp").on("propertychange change keyup paste input", function() {
					$(".save-btn button").prop("disabled", false);
					$(".save-btn button").addClass('solid');
					$(".save-btn button").removeClass('disabled');
				});

			}
			else if (ttalk){
				let targetSpace = workspaceManager.getWorkspaceUrl(showmemClickid);
				Top.App.routeTo('/s/'+targetSpace+'/talk?q='+getTimeStamp());
				Top.Dom.selectById('ShowMemDialog').close(true);
				
			}
			else if (tmeet) {
				
				Top.Dom.selectById('ShowMemDialog').close(true);
				appManager.setSubApp("meeting");
				//      appManager.setMainApp(mainUrl); 

				const mySpace = workspaceManager.getMySpaceUrl();
				let mainApp = appManager.getMainApp();
				const subApp = appManager.getSubApp();
				const mainUrl = getMainAppByUrl();
				const subUrl = getSubAppByUrl();
				const lnbUrl = getLnbByUrl();
				const roomId = ["f", "m"].includes(lnbUrl) ? mySpace : workspaceManager.getWorkspaceUrl(showmemClickid);	// 친구, 메일탭 - 마이스페이스
				let inputLnb;

				if ("f" === lnbUrl) {														// 친구 탭에서 서브앱을 키는 경우 (스페이스/ 토크/ 서브앱) -- 추후 정리
					inputLnb = "s";
					mainApp = "talk";
					appManager.setMainApp("talk");
				}
				else {
					inputLnb = lnbUrl;
				}

				if (subApp == "meeting" && subUrl != "meeting" && mainUrl != "meeting") {
					TeeAlarm.open({
						title: 'TeeMeeting을 시작하시겠습니까?',
						content: '미팅을 시작하면 멤버들에게 참여 알림이 전송됩니다.',
						icon: 'icon-info',
						buttons: [
							{
								text: '미팅 시작',
								onClicked: (e) => {
									if ("talk" === mainUrl || "mail" === mainUrl || (roomId === mySpace && "f" === lnbUrl)) {
										let mainTarget = "f" === lnbUrl ? "talk" : getMainAppFullUrl();
										Top.App.routeTo(`/${inputLnb}/${roomId}/${mainTarget}?sub=${subApp}&q=${getTimeStamp()}`, { eventType: "fold" });          //${mainApp}
									}
									else {
										Top.App.routeTo(`/${inputLnb}/${roomId}/${subApp}?q=${getTimeStamp()}`, { eventType: "expand" });
									}
									TeeAlarm.close();
								}
							}
						],
						cancelButtonText: '취소',
						onCancelButtonClicked: (e) => {
							appManager.setSubApp(getSubAppByUrl());
							TeeAlarm.close();
						},
					});
					return;
				}

			}
			else if (img){
				_UserId = img.id;
				_friendManageProfilePopupWithPosition(_UserId,$('#top-dialog-root_ShowMemDialog').offset().left , $('#top-dialog-root_ShowMemDialog').offset().top)
			}
			else if (add) {
				spaceAPI.getSpaceMemberListBrief(showmemClickid).then(result => {Top.Dom.selectById('ShowMemDialog').close(true);
						var a = [];
						for (var i = 0; i < result.data.dto.UserProfileList.length; i++) {
								a.push({
								FRIEND_ID: result.data.dto.UserProfileList[i].USER_ID,
								THUMB_PHOTO: result.data.dto.UserProfileList[i].THUMB_PHOTO,
								USER_NAME: result.data.dto.UserProfileList[i].USER_NAME,
								})
						}
						InviteMemberDialog.open({
							wsId: showmemClickid,
							alreadyInvitedList: a
						});
					});
			} 
			else {
				if (cancel) {
					$('#asbf').on('click' , function(){Top.Dom.selectById('ShowMemDialog').close(true)});
					re_template.querySelector('.space-name span').textContent = $('#' + showmemClickid + ' .wsName').text();
					re_template.querySelector('.mem-num span').textContent =
						($('#' + showmemClickid + ' .userCount').text() != "" ? $('#' + showmemClickid + ' .userCount').text() : 2) + "명";
					template.querySelector('.memtop-container').replaceChild(re_template, template.querySelector('.edit-container'));
					document.querySelector('#'+showmemClickid)
				} 

				if (save) {
					$('#asbf').on('click' , function(){Top.Dom.selectById('ShowMemDialog').close(true)});
					jQuery.ajax({
						type: "PUT",
						url: _workspace.url + "SpaceRoom/SpaceRoomNick",
						dataType : "json",
						data : JSON.stringify({
							"dto" :{
								"WS_ID":showmemClickid,
								"USER_ID" : userManager.getLoginUserId(),
								"MEM_NICK": edit_template.querySelector('.edit-space-name input').value.trim()
							}
						}),
						contentType: "application/json",
						async: false,
						success: function (result) {
							$('#'+showmemClickid + ' .wsName').text((edit_template.querySelector('.edit-space-name input').value == "" ? workspaceManager.getWorkspace(showmemClickid).User_name :  edit_template.querySelector('.edit-space-name input').value).trim())
							re_template.querySelector('.space-name span').textContent = (edit_template.querySelector('.edit-space-name input').value == "" ? workspaceManager.getWorkspace(showmemClickid).User_name :  edit_template.querySelector('.edit-space-name input').value.trim());
							re_template.querySelector('.mem-num span').textContent =
								($('#' + showmemClickid + ' .userCount').text() != "" ? $('#' + showmemClickid + ' .userCount').text() : 2) + "명";
							template.querySelector('.memtop-container').replaceChild(re_template, template.querySelector('.edit-container'));
							if(workspaceManager.getWorkspaceId(getRoomIdByUrl()) == showmemClickid){
								$('#AppGnbCurrentText a').text((edit_template.querySelector('.edit-space-name input').value == "" ? workspaceManager.getWorkspace(showmemClickid).User_name :  edit_template.querySelector('.edit-space-name input').value))
							}
							
							workspaceManager.update();
						}
					})
				}
			}

		});
		template.querySelector('.space-name span').textContent = $('#'+showmemClickid + ' .wsName').text();
		template.querySelector('.mem-num span').textContent = 
		($('#'+showmemClickid + ' .userCount').text() != "" ? $('#'+showmemClickid + ' .userCount').text() : 2) +"명";
		spaceAPI.getSpaceMemberListBrief(showmemClickid).then(result =>  {
				for (var i = 0; i < result.data.dto.UserProfileList.length; i++) {
					var status_color = result.data.dto.UserProfileList[i].USER_STATUS == 'online' ? '#16AC66' : '#CCCCCC';
					if(result.data.dto.UserProfileList[i].USER_ID == userManager.getLoginUserId()) status_color = '#16AC66';
					const MemberTemplate = htmlToElement(`
							<li>
								<img id=`+result.data.dto.UserProfileList[i].USER_ID +` /><i class="icon-circle" style="color : `+status_color+`"></i>
								<span class="name"></span>
							</li>
					`);
					MemberTemplate.querySelector('img').src = userManager.getUserPhoto(result.data.dto.UserProfileList[i].USER_ID, 'small' , result.data.dto.UserProfileList[i].THUMB_PHOTO);
					if(result.data.dto.UserProfileList[i].USER_ID == userManager.getLoginUserId()){
						MemberTemplate.querySelector('span.name').textContent = result.data.dto.UserProfileList[i].USER_NAME + " (나)";
						template.querySelector('ul.mem-list').insertBefore(MemberTemplate,template.querySelector('ul.mem-list').firstChild)
						continue;
					}
					MemberTemplate.querySelector('span.name').textContent = result.data.dto.UserProfileList[i].USER_NAME ;
					template.querySelector('ul.mem-list').appendChild(MemberTemplate);
				}
			});
		if(Top.Controller.get('spaceListLayoutLogic').isedit){
			edit_template.querySelector('.edit-space-name input').value =  template.querySelector('.space-name span').textContent;
			edit_template.querySelector('.mem-num span').textContent =
			($('#' + showmemClickid + ' .userCount').text() != "" ? $('#' + showmemClickid + ' .userCount').text() : 2) + "명";
			template.querySelector('.memtop-container').replaceChild(edit_template, template.querySelector('.edit-container'));
			
		}
		this.template = template;
		return this;

	}
	return Dialog;
}();

Top.App.onLoad(() => {
	Top.Dom.selectById('ShowMemDialog').setProperties({
		'on-open': function () {
			$('#MainTeeSpacePage').append(`<div id = 'asbf' style="width:`+window.innerWidth+`px; height :`+window.innerHeight+`px; z-index:`+($('#top-dialog-root_ShowMemDialog').css('z-index')-1)+`px; position:absolute; top:0; left:0;" ></div>`);
			$('#asbf').on('click' , function(){Top.Dom.selectById('ShowMemDialog').close(true)});
			let dialog = new ShowMemDialog();
			document.querySelector('div#ShowMemLayout').appendChild(dialog.template);
			$('#top-dialog-root_ShowMemDialog').removeClass('popup_01');
			if(Top.Controller.get('spaceListLayoutLogic').isedit){ 
				$('.edit-space-name input').focus();
				$('#asbf').off('click');
			};
			$(".save-btn button").prop("disabled", true);
			$(".save-btn button").removeClass('solid');
			$(".save-btn button").addClass('disabled');
				$("input.notsp").on("propertychange change keyup paste input", function() {
					$(".save-btn button").prop("disabled", false);
					$(".save-btn button").addClass('solid');
					$(".save-btn button").removeClass('disabled');
				});
			
		},
		'on-close': function () {
			document.querySelector('div#ShowMemLayout').innerHTML = '';
			Top.Controller.get('spaceListLayoutLogic').isedit = false;
			$('#asbf').remove();
		}
	});
});
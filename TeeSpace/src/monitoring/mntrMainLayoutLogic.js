Top.Controller.create('mntrMainLayoutLogic', {
	_type: '',
	_subMenuBtnId: '',
	_measure: '',
	_frequency: '',	
	_isSubMenuBtnClicked: false,
	_isTabClicked: false,
	_isRouting: false,
	
	init : function(event, widget, type) {
		
		
		if(type) {
			this._type = type;
		}
		console.log('main init called');
		// date picker 에 어제 날짜 채우기
		if(Top.Dom.selectById("mntrDatePicker")) {
			Top.Dom.selectById("mntrDatePicker").setDate(formatDate((new Date()).setDate((new Date()).getDate() - 1) ));
			
			//date picker 달력으로 클릭해서 날짜 바꿨을 때 이벤트 달기
			Top.Dom.selectById('mntrDatePicker').setProperties({ 'on-select': function(event, widget){
				var date_ = widget.getDate();
				var year = parseInt(date_.split('-')[0]);
				var month = parseInt(date_.split('-')[1]);
				var day = parseInt(date_.split('-')[2]);
				
				var todayDate = new Date();
				var givenDate = new Date(year, month-1, day);
				if(todayDate < givenDate) {
					alert('선택할 수 없는 날짜입니다.');
					Top.Dom.selectById("mntrDatePicker").setDate(formatDate((new Date()).setDate((new Date()).getDate() - 1) ));
				}
				this.updateInfo(event,widget);
				} });
			
		}
		
		if(this._type && Top.Dom.selectById('mntrContentLayout')) {
			setSubButtons(this._type);
			switch(this._type) {
				case "DAU":
					Top.Dom.selectById('mntrContentLayout').src('./mntrDAULayout.html');
					//dau 업데이트
					setTimeout(() => {this.updateDAU(event, widget) },0 );
					break;
				case "memberAnalysis":
					Top.Dom.selectById('mntrContentLayout').src('./mntrAnalysisLayout.html');
					this._measure = 'session_cnt';
					this._frequency = 'daily';					
					break;
				case "cloudSpaceAnalysis":
					Top.Dom.selectById('mntrContentLayout').src('./mntrAnalysisLayout.html');
					this._measure = 'space_cnt';
					this._frequency = 'daily';					
					break;
			}
		}		
		
		
	},

	onReloadBtnClick: function(event, widget) {
		console.log("mntr reload btn clicked!");
		mntrDAUData.load();
	},
	
	//일단 버튼으로 날짜 바꿀 때 + 달력 클릭해서 날짜 바꿀때  따로따로 call하도록 해놓음.
	updateInfo: function(event, widget){
		console.log('update start');
		if(event.type === 'tabchange'){
			console.log('from tab change')
			return;
		}
		if(this._isRouting){
			return;
		}
		
//		ajax call로 repo 업데이트
		switch(this._type){
			case "DAU":
				console.log('update DAU');
				this.updateDAU(event, widget);
				break;
			case "memberAnalysis":
				console.log('update member analysis');
				this.updateAnalysis(event, widget);
				break;
			case "cloudSpaceAnalysis":
				console.log('update cloudspace analysis');
				this.updateAnalysis(event, widget);
				break;
		}
	},
	
	updateDAU: function(event, widgete){		
		let data={
				dto: {
					dau: {
			            "date": Top.Dom.selectById('mntrDatePicker').getDate() 
			        }
				}
		};
		
		Top.Ajax.execute({
			type : "GET",
			url : _workspace.url+"Monitoring/DAU",
			dataType : "json",
			async : false,
			cache : false,
			data : JSON.stringify(data),
			contentType: "application/json; charset=utf-8",
		    xhrFields: {withCredentials: true},
			crossDomain : true,
			success : function(ret, xhr, status) {
	            try {
	            	var dau = JSON.parse(ret).dto.dau;
	            	
//	        		dau 업데이트
							Top.Dom.selectById('dauSessionCnt').setText(dau.dau_session_cnt); //접속 현황
	        		Top.Dom.selectById('dauUserDiff').setText(dau.dau_user_diff > 0 ? `+${dau.dau_user_diff}` : `${dau.dau_user_diff}`); //멤버 증감수
	        		Top.Dom.selectById('dauUserRemove').setText(dau.dau_user_remove);
	        		Top.Dom.selectById('dauUserAdd').setText(dau.dau_user_diff + dau.dau_user_remove);
	        		Top.Dom.selectById('dauUserCnt').setText(dau.dau_user_cnt); //총 멤버
	        		Top.Dom.selectById('dauSessionDurationAvg').setText(seconds2time(dau.dau_session_duration_avg)); //평균 체류 시간
	        		Top.Dom.selectById('dauSpaceCnt').setText(dau.dau_space_cnt); //총 스페이스 수
	        		Top.Dom.selectById('dauSpaceAvg').setText(parseFloat(dau.dau_space_avg).toFixed(1)); //평균 스페이스 수
	        		Top.Dom.selectById('dauUserAvg').setText(parseFloat(dau.dau_user_avg).toFixed(1)); //1일 평균 사용자 수

	        		//접속 기기 순위
	        		if(dau.dau_device_rank_series !== null){
	        			let deviceChart = Top.Dom.selectById('Chart1170').topcharts(); 
		        		deviceChart.removeSeries();
		        		deviceChart.addSeries(dau.dau_device_rank_series);
		        		deviceChart.update();
	        		}
	        		
	        		
	        		//앱 사용 순위
	        		if(dau.dau_app_usage_rank_series !== null){
	        			let appChart = Top.Dom.selectById('Chart1170_1').topcharts(); 
		        		appChart.removeSeries();
		        		
		        		for(var i=0; i<dau.dau_app_usage_rank_series.length; i++){
		        			for(var key in CLOUDSPACE_CHANNELS){
		        			    if(dau.dau_app_usage_rank_series[i].name === CLOUDSPACE_CHANNELS[key].CHANNEL_TYPE){
		        			    	dau.dau_app_usage_rank_series[i].name = CLOUDSPACE_CHANNELS[key].CHANNEL_NAME
		        			    }
		        			} 
		        		}
		        		appChart.addSeries(dau.dau_app_usage_rank_series);
		        		appChart.update();
	        		}
	        		
	            	
	        		//시간대별 사용자 수
	        		let timeUserChart = Top.Dom.selectById('Chart1295').topcharts(); 
					timeUserChart.removeSeries();
					// 오늘인 경우 현재 시점 이후로는 그래프 안그려지도록
					var date_ = Top.Dom.selectById('mntrDatePicker').getDate();
					var year = parseInt(date_.split('-')[0]);
					var month = parseInt(date_.split('-')[1]);
					var day = parseInt(date_.split('-')[2]);
					
					var todayDate = new Date();
					var givenDate = new Date(year, month-1, day);
					if(formatDate(todayDate) === formatDate(givenDate)) {
						var hour = todayDate.getHours();
						dau.dau_user_cnt_by_time_series.data = dau.dau_user_cnt_by_time_series.data.slice(0, hour);
						dau.dau_user_cnt_by_time_series.name = dau.dau_user_cnt_by_time_series.name.slice(0, hour);
						console.log()
					} 
	        		timeUserChart.addSeries({name: '시간대별 사용자 수', data: dau.dau_user_cnt_by_time_series.data});	        		
	        		let line_option = {
	        				xAxis:{
//	        					show: true,
	        					type: 'category',
	        					categories: dau.dau_user_cnt_by_time_series.name,
//	        					padding: -0.5
	        				}
	        		};
	        		timeUserChart.setOption(line_option);
	        		timeUserChart.update();
		
	            	//시간대별 사용자 수 테이블
	        		let table_repo_1 = [];
	        		let table_repo_2 = [];
	        		
	        		for(i=0; i<dau.dau_user_cnt_by_time_series.name.length/2; i++){
	        			const time1 = dau.dau_user_cnt_by_time_series.name.slice(0,dau.dau_user_cnt_by_time_series.name.length/2)[i]
	        			if(time1 !== undefined) {
	        				table_repo_1.push({
			        			 time: `${Number(time1)}:00 ~ ${Number(time1)+1}:00`,
			        		 user_cnt: String(dau.dau_user_cnt_by_time_series.data.slice(0,dau.dau_user_cnt_by_time_series.name.length/2)[i])
			        		});
	        			}
	        			const time2 = dau.dau_user_cnt_by_time_series.name.slice(dau.dau_user_cnt_by_time_series.name.length/2, dau.dau_user_cnt_by_time_series.name.length)[i];
	        			if(time2 != undefined) {
	        				table_repo_2.push({
		        				time: `${Number(time2)}:00 ~ ${Number(time2)+1}:00`,
		        			user_cnt: String(dau.dau_user_cnt_by_time_series.data.slice(dau.dau_user_cnt_by_time_series.name.length/2, dau.dau_user_cnt_by_time_series.name.length)[i])
		        			});
	        			}
	        			
	        		}
	        		
	        		table_repo_1 = table_repo_1.filter(function(row) {
						return row.time != undefined;
					});
	        		table_repo_2 = table_repo_2.filter(function(row) {
						return row.time != undefined;
					});		
	        		DAURepo.setValue('DayInstance_1', table_repo_1);
	        		DAURepo.setValue('DayInstance_2', table_repo_2);
							
	            } catch (error) {
	               console.log('err');
	               console.log(error);
	               if(JSON.parse(ret).dto.dau === null){
	            	   alert('해당 날짜에는 데이터가 없습니다');
	               }
	            }
			},
			complete : function(ret, xhr, status) {
				if(Top.Dom.selectById("TextView1018_3")) {
					
					var date_ = Top.Dom.selectById('mntrDatePicker').getDate();
					var year = parseInt(date_.split('-')[0]);
					var month = parseInt(date_.split('-')[1]);
					var day = parseInt(date_.split('-')[2]);
					var hour;
					
					var todayDate = new Date();
					var givenDate = new Date(year, month-1, day);
					if(formatDate(todayDate) === formatDate(givenDate)) {
						// today
						hour = new Date().getHours();
					} else {
						givenDate = new Date(year, month-1, day+1);
						hour = '00';
					}
					
					var updateDescription = `${formatDate(givenDate)} ${hour}:00 업데이트`;
					Top.Dom.selectById("TextView1018_3").setText(updateDescription);
					Top.Dom.selectById("dau_update_msg").setText(updateDescription);
					
				}
			},
			error : function(ret, xhr, status) {
				
			}
		});
		
		
	},
	
	updateAnalysis: function(event, widgete){
		console.log('update member analysis called');
		let _this = this;
		let radio_type;
		
		if(_this._measure === 'device_type' || _this._measure === 'app_usage'){
			radio_type =  Top.Dom.selectById('analysisRadioBtn1').getChecked() ? 'recent_trend' : 'all';		
		} else {
			radio_type =  Top.Dom.selectById('analysisRadioBtn1').getChecked() ? 'recent_trend' : 'hourly';
		}
		let frequency  = this._frequency;
		let hourly_time = Top.Dom.selectById('analysisRadioBtn1').getChecked() ? 0 : parseInt(Top.Dom.selectById('hourlySelectBox').getValue().split(' ')[0]);
		
		
		
		let data={
				dto: {
					analysis: {
						"date": Top.Dom.selectById('mntrDatePicker').getDate(),
			            "measure": this._measure,
			            "radio_type": radio_type,
			            "frequency": this._frequency,
			            "hourly_time": hourly_time 
			        }
				}
		};
		
		console.log(data.dto.analysis);
		console.log(this._measure);
		Top.Ajax.execute({
			type : "GET",
			url : _workspace.url+"Monitoring/Analysis",
			dataType : "json",
			async : false,
			cache : false,
			data : JSON.stringify(data),
			contentType: "application/json; charset=utf-8",
		    xhrFields: {withCredentials: true},
			crossDomain : true,
			success : function(ret, xhr, status) {
	            try {
	            	var analysis = JSON.parse(ret).dto.analysis;

	            	$('div#GeneralTableLayout').css('display','none');
        			$('div#DeviceTableLayout').css('display','none');
        			$('div#DeviceTotalTableLayout').css('display','none');
        			$('div#AppTableLayout').css('display','none');
        			$('div#AppTotalTableLayout').css('display','none');
	            	
	            	//시간대별 사용자 수	        		
	            	let timeUserChart;
	            	let lineChart;
	            	let barChart;
	            	var donutChart;
	        		//device_type면 chart type = donut
	        		if(_this._measure === 'device_type' || _this._measure === 'app_usage'){
	        			
	        			//차트 교체
	        			if(Top.Dom.selectById('analysisRadioBtn2').isChecked()){	        				
	        				$('top-chart#Chart400_1').css('display','inline');
		        			$('top-chart#Chart400_2').css('display','inline');
		        			$('top-chart#Chart400').css('display','none');
	        				
	        			} else {
	        				$('top-chart#Chart400_1').css('display','none');
		        			$('top-chart#Chart400_2').css('display','none');
		        			$('top-chart#Chart400').css('display','inline');
	        				
	        			}
	        		}
	        			
	        			
	        			
	        			
	        			//테이블 교체 및 차트/테이블 업데이트
	        			//기기 유형 전체
	        			if(_this._measure === 'device_type' && Top.Dom.selectById('analysisRadioBtn2').isChecked()){
	        				$('div#DeviceTotalTableLayout').css('display','inline');
	        				//차트
	        				donutChart = Top.Dom.selectById('Chart400_1').topcharts();
	        				barChart = Top.Dom.selectById('Chart400_2').topcharts();
	        				
	        				donutChart.removeSeries();
	        				barChart.removeSeries();
	        				
	        				donutChart.addSeries(analysis.result_donut_series[0].SeriesTOPDonutArray);
	        				barChart.addSeries(analysis.result_donut_series[0].SeriesTOPDonutArray);
	        				
	        				donutChart.update();
	        				barChart.update();
	        				
	        				//테이블
	        				
	    	        		
	    	        		if(analysis.result_line_series.length === 1){
	        					var totalCnt = analysis.result_line_series[0].data.reduce( (a,b)=>a+b );
	        				}else {
	        					if(analysis.result_line_series[0].data.length === 1){
	        						var totalCnt = 0;
	        						analysis.result_line_series.forEach(function(ele){totalCnt += ele.data[0]}); 
	        					}else {
	        						var totalCnt = analysis.result_line_series.reduce( (obj1, obj2) => obj1.data.reduce((a,b)=>a+b) + obj2.data.reduce((a,b)=>a+b) );
	        					}
	        				}
	    	        		
	    	        		
	    	        		let tmp = {};
	    	        		let table_series =[];
	    	        		
	    	        		
	    	        		for(var i=0; i<analysis.result_line_series[0].data.length; i++){
	    	        			tmp = {};
	    	        			if(i == analysis.result_line_series[0].data.length - 1){
	    	        				if(_this._frequency == 'daily'){
	    	        					tmp.time = `${analysis.result_line_series[0].name[i]}:00 - ${analysis.result_line_series[0].name[0]}:00`;	    	        				
	    	        				}else{
	    	        					tmp.time = `${analysis.result_line_series[0].name[i]}`;
	    	        				}
	    	        				
	    	        			}else {
	    	        				if(_this._frequency == 'daily'){
	    	        					tmp.time = `${analysis.result_line_series[0].name[i]}:00 - ${analysis.result_line_series[0].name[i+1]}:00`;	    	        				
	    	        				}else{
	    	        					tmp.time = `${analysis.result_line_series[0].name[i]}`;
	    	        				}
	    	        				
	    	        			}
	    	        			
//	    	        			tmp.pc = (analysis.result_line_series.filter(function(element){return element.label == 'PC'})[0].data[i]/totalCnt * 100).toFixed(2) + ' %';
	    	        			tmp.pc = (analysis.result_line_series.filter(function(element){return element.label == 'PC'})[0].data[i]).toFixed(2) + ' %';
	    	        			let mobile_cnt = 0;
//	    	        			analysis.result_line_series.map(function(obj){
//	    	        				if (obj.label != 'PC'){
//	    	        					return obj.data[i];
//	    	        				}
//	    	        				return;
//	    	        			}).forEach(function(element){ mobile_cnt + element });
//	    	        			tmp.mobile = (mobile_cnt * 100).toFixed(2) + ' %';
	    	        			tmp.mobile = 0;
	    	        			analysis.result_line_series.filter(function(element){return element.label != 'PC'}).forEach(function(ele){tmp.mobile += ele.data[i]});
	    	        			tmp.mobile = tmp.mobile.toFixed(2) + ' %';
	    	        			
	    	        			
	    	        			table_series.push(tmp);
	    	        		}
	    	        			    	        		
	    	        		let half_index = parseInt(table_series.length/2);
	    	        		let table_repo_1 = table_series.slice(0,half_index);
	    	        		let table_repo_2 = table_series.slice(half_index,table_series.length);
	    	        		
	    	        		
	    	        		DAURepo.setValue('DeviceTotalInstance_1', table_repo_1);
	    	        		DAURepo.setValue('DeviceTotalInstance_2', table_repo_2);
	    	        		Top.Dom.selectById('dashboardMainLayoutNoticeTableView_1_3').render();
	    	        		Top.Dom.selectById('dashboardMainLayoutNoticeTableView_1_1_1').render();
	    	        		
	    	        		
	        			}else if(_this._measure === 'device_type' && !Top.Dom.selectById('analysisRadioBtn2').isChecked()){
	        				//기기 유형 최근 추이
	        				$('div#DeviceTableLayout').css('display','inline');
	        				
	        				
	        				//차트
	        				lineChart = Top.Dom.selectById('Chart400').topcharts();	        				
	        				lineChart.removeSeries();
	        				
//	        				if(analysis.result_line_series.length === 1){
//	        					var totalCnt = analysis.result_line_series[0].data.reduce( (a,b)=>a+b );
//	        				}else {
//	        					var totalCnt = analysis.result_line_series.reduce( (obj1, obj2) => obj1.data.reduce((a,b)=>a+b) + obj2.data.reduce((a,b)=>a+b) );
//	        				}
	        				
	        				var reformSeries = analysis.result_line_series.map(function(obj){
	        				    var rObj = {};
	        				    for(var key in obj){
	        				        
	        				    	if(key == 'data'){
//	        				    		rObj[key] = obj[key].map(x => 100*(x/totalCnt) );
	        				    		rObj[key] = obj[key];
	        				    	}else {
	        				    		rObj[key] = obj[key];
	        				    	}
	        				        
	        				    }
	        				    return rObj;
	        				});
	        				
	        				for(var i=0; i<reformSeries.length; i++){
	        					reformSeries[i].name = reformSeries[i].label;
	        				}
	        				
	        				lineChart.addSeries(reformSeries);	       
	        				
	        				var chart_name = radio_type === 'recent_trend' ? '최근 추이' : '전체'
	        				
			        		let line_option = {
			        				xAxis:{
		//	        					show: true,
			        					type: 'category',
			        					categories: analysis.result_line_series[0].name,
		//	        					padding: -0.5
			        				}
			        		};
			        		lineChart.setOption(line_option);
	        				
	        				
	        				lineChart.update();
	        				
	        				
	        				//테이블
	        				var reformTable = reformSeries.map(function(obj){
	        					var rObj = {};
	        				    for(var key in obj){
	        				        
	        				    	if(key == 'label'){
	        				    		rObj[key] = obj[key];
	        				    	}else if(key == 'data') {
	        				    		if(_this._frequency == 'daily'){
	        				    			rObj['today'] = ( obj[key].reduce( (a,b) => a+b ).toFixed(2) )/obj[key].map(function(ele){if(ele != '0' ){return 1}else{return 0}}).reduce( (a,b) => a+b ) + ' %';
	        				    		}else {
	        				    			rObj['today'] = obj[key][ obj[key].length-1 ].toFixed(2) + ' %';
	        				    		}
	        				    		rObj['avg'] =  ( obj[key].reduce( (a,b) => a+b )/obj[key].length ).toFixed(2) + ' %';
	        				    	}
	        				        
	        				    }
	        				    return rObj;	        					
	        				});
	        				
	        				
	        				
	        				DAURepo.setValue('DeviceInstance', reformTable);
	        				Top.Dom.selectById('dashboardMainLayoutNoticeTableView_1_2').render();
	        				
	        				
	        			} else if(_this._measure === 'app_usage' && Top.Dom.selectById('analysisRadioBtn2').isChecked()){ 
		        			//앱 분석 전체
	        				$('div#AppTotalTableLayout').css('display','inline');

			        		
	        				//차트
	        				donutChart = Top.Dom.selectById('Chart400_1').topcharts();
	        				barChart = Top.Dom.selectById('Chart400_2').topcharts();
	        				
	        				donutChart.removeSeries();
	        				barChart.removeSeries();

	        				let reformDonutSeries = analysis.result_line_series.map(function(obj){
	        					let rObj = {};
	        					for(var key in obj){
	        				    	if(key == 'label'){
	        				    		switch(obj[key]){
	        				    		case 'CHN0001':
		        				    		rObj.name = 'T-Talk'; 
		        				    		break;
	        				    		case 'CHN0002':
		        				    		rObj.name = 'T-Mail';
		        				    		break;
	        				    		case 'CHN0003':
		        				    		rObj.name = 'T-Note';
		        				    		break;
	        				    		case 'CHN0004':
		        				    		rObj.name = 'CloudOffice';
		        				    		break;
	        				    		case 'CHN0005':
		        				    		rObj.name = 'T-Schedule';
		        				    		break;
	        				    		case 'CHN0006':
		        				    		rObj.name = 'T-Drive';
		        				    		break;
	        				    		case 'CHN0007':
		        				    		rObj.name = 'Conference';
		        				    		break;
	        				    		default:
	        				    			break;
	        				    		}
	        				    		
	        				    		
	        				    	}else if(key == 'data'){
	        				    		rObj[key] = [obj[key][0]];
	        				    	}
	        				        
	        				    }
	        				    return rObj;
	        				});

	        				
	        				donutChart.addSeries(reformDonutSeries);
	        				barChart.addSeries(reformDonutSeries);
	        				
	        				donutChart.update();
	        				barChart.update();
	        				
	        				//테이블
	        				
	    	        		let table_series = []; 
	    	        		analysis.result_line_series[0].name.forEach(function(ele, idx){
	    	        			let rObj = {};
	    	        			rObj.date = ele;
	    	        			analysis.result_line_series.map(function(obj){
	    	        				for(var key in obj){
		        				    	if(key == 'label'){
		        				    		switch(obj[key]){
		        				    		case 'CHN0001':
			        				    		rObj['T-Talk'] = obj['data'][idx].toFixed(2) + ' %'; 
			        				    		break;
		        				    		case 'CHN0002':
			        				    		rObj['T-Mail'] = obj['data'][idx].toFixed(2) + ' %';
			        				    		break;
		        				    		case 'CHN0003':
		        				    			rObj['T-Note'] = obj['data'][idx].toFixed(2) + ' %';			        				    		
			        				    		break;
		        				    		case 'CHN0004':
		        				    			rObj['CloudOffice'] = obj['data'][idx].toFixed(2) + ' %';
			        				    		break;
		        				    		case 'CHN0005':
		        				    			rObj['T-Schedule'] = obj['data'][idx].toFixed(2) + ' %';			        				    		
			        				    		break;
		        				    		case 'CHN0006':
		        				    			rObj['T-Drive'] = obj['data'][idx].toFixed(2) + ' %';
			        				    		break;
		        				    		case 'CHN0007':
		        				    			rObj['Conference'] = obj['data'][idx].toFixed(2) + ' %';
			        				    		break;
		        				    		default:
		        				    			break;
		        				    		}
		        				    	}
	    	        				}
	    	        			});
	    	        			table_series.push(rObj);
	    	        		})
	        				
	    	        		
	    	        		DAURepo.setValue('AppTotalInstance', table_series);
	    	        		Top.Dom.selectById('dashboardMainLayoutNoticeTableView_1_2_2').render();

	        			} else if(_this._measure === 'app_usage' && !Top.Dom.selectById('analysisRadioBtn2').isChecked()){ //앱 분석 최근 추이
	        				$('div#AppTableLayout').css('display','inline');
	        				
	        				//차트
	        				lineChart = Top.Dom.selectById('Chart400').topcharts();
	        				
	        				lineChart.removeSeries();

	        				let totalAppCnt = analysis.result_donut_series[0].SeriesTOPDonutArray.map(function(obj){return obj.data[0]}).reduce( (a,b) => a+b );
	        				
	        				let reformLineSeries = analysis.result_line_series.map(function(obj, idx){
	        					let rObj = {};
	        					for(var key in obj){
	        				    	if(key == 'label'){
	        				    		switch(obj[key]){
	        				    		case 'CHN0001':
		        				    		rObj.name = 'T-Talk'; 
		        				    		break;
	        				    		case 'CHN0002':
		        				    		rObj.name = 'T-Mail';
		        				    		break;
	        				    		case 'CHN0003':
		        				    		rObj.name = 'T-Note';
		        				    		break;
	        				    		case 'CHN0004':
		        				    		rObj.name = 'CloudOffice';
		        				    		break;
	        				    		case 'CHN0005':
		        				    		rObj.name = 'T-Schedule';
		        				    		break;
	        				    		case 'CHN0006':
		        				    		rObj.name = 'T-Drive';
		        				    		break;
	        				    		case 'CHN0007':
		        				    		rObj.name = 'Conference';
		        				    		break;
	        				    		default:
	        				    			break;
	        				    		}
	        				    		
	        				    		
	        				    	}else if(key == 'data'){
	        				    		rObj[key] = obj[key]
	        				    	}
	        				    }
	        					return rObj;
	        				});
	        				
	        				
	        				
	        				
	        				lineChart.addSeries(reformLineSeries);
	        				
	        				lineChart.update();
	        				
	        				//테이블
	        				let table_series = analysis.result_line_series.map(function(obj, idx){
	        					let rObj = {};
	        					for(var key in obj){
	        				    	if(key == 'label'){
	        				    		switch(obj[key]){
	        				    		case 'CHN0001':
		        				    		rObj.name = 'T-Talk'; 
		        				    		break;
	        				    		case 'CHN0002':
		        				    		rObj.name = 'T-Mail';
		        				    		break;
	        				    		case 'CHN0003':
		        				    		rObj.name = 'T-Note';
		        				    		break;
	        				    		case 'CHN0004':
		        				    		rObj.name = 'CloudOffice';
		        				    		break;
	        				    		case 'CHN0005':
		        				    		rObj.name = 'T-Schedule';
		        				    		break;
	        				    		case 'CHN0006':
		        				    		rObj.name = 'T-Drive';
		        				    		break;
	        				    		case 'CHN0007':
		        				    		rObj.name = 'Conference';
		        				    		break;
	        				    		default:
	        				    			break;
	        				    		}
	        				    		
	        				    		
	        				    	}else if(key == 'data'){
	        				    		rObj['today'] = obj[key][obj[key].length-1].toFixed(2) + ' %';
	        				    	}
	        				    }
	        					rObj['avg'] = (analysis.result_donut_series[0].SeriesTOPDonutArray[idx].data[0]/totalAppCnt * 100).toFixed(2) + ' %';
	        				    return rObj;
	        				});

	    	        		
	    	        		DAURepo.setValue('AppInstance', table_series);
	    	        		Top.Dom.selectById('dashboardMainLayoutNoticeTableView_1_2_1').render();
	        				
	        				
	        				
	        				
	        				
	        				
	        				
	        				
	        			} else {
	        			$('top-chart#Chart400').css('display','inline');
	        			$('top-chart#Chart400_1').css('display','none');
	        			$('top-chart#Chart400_2').css('display','none');

	        			$('div#GeneralTableLayout').css('display','inline');
	        			$('div#DeviceTableLayout').css('display','none');
	        			$('div#DeviceTotalTableLayout').css('display','none');
	        			$('div#AppTableLayout').css('display','none');
	        			
	        			if(analysis.result_line_series[0].data !== null){
	        				timeUserChart = Top.Dom.selectById('Chart400').topcharts();
	    	        		
	    	        		
			        		var chart_name = radio_type === 'recent_trend' ? '최근 추이' : '시간대별'
			        		timeUserChart.removeSeries();
			        		timeUserChart.addSeries({name: chart_name, data: analysis.result_line_series[0].data});
			        		let line_option = {
			        				xAxis:{
		//	        					show: true,
			        					type: 'category',
			        					categories: analysis.result_line_series[0].name,
		//	        					padding: -0.5
			        				}
			        		};
			        		timeUserChart.setOption(line_option);
			        		timeUserChart.update();
	        			}

	        			
		
	        			
	        			//시간대별 사용자 수 테이블
		        		let table_repo_1 = [];
		        		let table_repo_2 = [];
		        		
		        		
		        		for(i=0; i<analysis.result_line_series[0].name.length/2; i++){	        			
		        			table_repo_1.push({
			        			 time: analysis.result_line_series[0].name.slice(0,analysis.result_line_series[0].name.length/2)[i],
			        		 user_cnt: String(analysis.result_line_series[0].data.slice(0,analysis.result_line_series[0].name.length/2)[i])
			        		});
		        			
		        			table_repo_2.push({
		        				time: analysis.result_line_series[0].name.slice(analysis.result_line_series[0].name.length/2, analysis.result_line_series[0].name.length)[i],
		        			user_cnt: String(analysis.result_line_series[0].data.slice(analysis.result_line_series[0].name.length/2, analysis.result_line_series[0].name.length)[i])
		        			});
		        		}
		        		
		        		
		        		for(var i=0; i<table_repo_1.length; i++){
		        			if (table_repo_1[i].time === undefined){
		        				table_repo_1.splice(i,1);
		        			}
		        		}
		        		
		        		
		        		DAURepo.setValue('DayInstance_1', table_repo_1);
		        		DAURepo.setValue('DayInstance_2', table_repo_2);
	        			
	        			
	        		}

	            	
	            	
	            	
	            	
	            	
	            	
	            	
	            	
	            } catch (error) {
	               console.log('err');
	               console.log(error);
	               if(JSON.parse(ret).dto.analysis === null){
	            	   alert('해당 날짜에는 데이터가 없습니다');
	               }
	            }
			},
			complete : function(ret, xhr, status) {
				
			},
			error : function(ret, xhr, status) {
				
			}
		});
		
	},
	
	updateCloudSpaceAnalysis: function(event, widgete){
		console.log('update cloudspace analysis called');
	
	},
	
	
	changeType(type) {
		this._type = type;
//		this.__init();
	},

	onSubMenuBtnClick : function(event, widget) {
		this._isSubMenuBtnClicked = true;
		console.log('subMenuBtnClick');
		console.log(widget);
		this._subMenuBtnId = widget.id;
		$("button#" + widget.id).css("border", "#27B1CC");
		$("button#" + widget.id).css("background-color", "#27B1CC");
		setSubButtonsToGrayWithout(widget.id);
		
		Top.Dom.selectById('analysisRadioBtn1').setText('최근 추이');
		Top.Dom.selectById('analysisRadioBtn2').setText('시간대별');
		
		switch(this._subMenuBtnId) {
	
			// 접속 현황
			case "memberAnalysisBtn1":
				setTabs([]);
				this._measure = 'session_cnt';				
				break;
			// 멤버 증감수
			case "memberAnalysisBtn2":
				setTabs([0,1]);
				this._measure = 'member_cnt';
				break;
			// 기기 유형
			case "memberAnalysisBtn3":
				setTabs([]);
				this._measure = 'device_type';
				Top.Dom.selectById('analysisRadioBtn2').setText('전체');
				Top.Dom.selectById('analysisRadioBtn1').setText('최근 추이');
				break;
			// 평균 체류 시간
			case "memberAnalysisBtn4":
				setTabs([]);
				this._measure = 'session_duration_avg	';
				break;
			// 스페이스 통계
			case "cloudSpaceAnalysisBtn1":
				setTabs([2,3]);
				this._measure = 'space_cnt';
				break;
			case "cloudSpaceAnalysisBtn2":
				setTabs([4]);
				this._measure = 'app_usage';
				Top.Dom.selectById('analysisRadioBtn2').setText('전체');
				Top.Dom.selectById('analysisRadioBtn1').setText('최근 추이');
				break;
			case "cloudSpaceAnalysisBtn3":
				setTabs([5,6]);
				this._measure = 'qna';
			break;
		}
		
		//radio, frequency 초기화
		Top.Controller.get('mntrAnalysisLayoutLogic').initializeFrequencyBtn(event, widget);
		Top.Controller.get('mntrAnalysisLayoutLogic').initializeRadioBtn(event, widget);		
		
//		if(event === 'initAnalysis'){
//			return;
//		}else {
//			this.updateInfo(event,widget);
//		}
		this._isSubMenuBtnClicked = false;
	},
	
	onDateChangeBtnClick: function(event, widget){
		
		var date_ = Top.Dom.selectById('mntrDatePicker').getDate();
		var year = parseInt(date_.split('-')[0]);
		var month = parseInt(date_.split('-')[1]);
		var day = parseInt(date_.split('-')[2]);
		
		var toSetDate = new Date(year, month-1, day + (widget.template.icon === 'icon-arrows_right_thin' ? 1 : -1));

		// ims 215551 (2020-01-31) 
		// 현재 날짜 이후로 선택되지 않도록 대응
		this.toggleArrowRightButton(formatDate(new Date()) === formatDate(toSetDate));
				
		Top.Dom.selectById('mntrDatePicker').setDate(formatDate(toSetDate));
		
		this.updateInfo(event,widget);
	},
	
	toggleArrowRightButton: function(disableOrNot) {
		Top.Dom.selectById("mntrChevronNextBtn").setProperties({"disabled": disableOrNot.toString() })
	}
	
	
});

function setSubButtons(str) {
	$("#mntrSubMenuBtnLayout button").each(function(idx, item) {
		if(!item.id.includes(str)) {
			$("#" + item.id).hide();
		}
	})
}

function setSubButtonsToGrayWithout(widgetId) {
	$("#mntrSubMenuBtnLayout button").each(function(idx, item) {
		if(!item.id.includes(widgetId)) {
			$("button#" + item.id).css("border", "");
			$("button#" + item.id).css("background-color", "");
		}
	})
}



function formatDate(date) {
    let d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-');
}

function seconds2time (seconds) {
    var hours   = Math.floor(seconds / 3600);
    var minutes = Math.floor((seconds - (hours * 3600)) / 60);
    var seconds = seconds - (hours * 3600) - (minutes * 60);
    var time = "";

    if (hours != 0) {
      time = hours+"h ";
    }
    if (minutes != 0 || time !== "") {
      minutes = (minutes < 10 && time !== "") ? "0"+minutes : String(minutes);
      time += minutes+"m ";
    }
    if (time === "") {
      time = seconds+"s";
    }
    else {
      time += (seconds < 10) ? "0"+seconds+"s" : String(seconds)+"s";
    }
    return time;
}

Top.Controller.create('mntrDAULayoutLogic', {
	onShowMoreBtnClick : function(event, widget) {
		console.log(widget, event);
		var mainCon = Top.Controller.get('mntrMainLayoutLogic');
		
		switch(widget.id.split('_')[0]){
			case 'todayCnt':				
				location.replace(rootUrl+"admin/stat_member");
				setTimeout(() => {
					mainCon._isRouting = true;
					mainCon.onSubMenuBtnClick(null, Top.Dom.selectById('memberAnalysisBtn1'));
					mainCon._isRouting = false;
					}, 100 );				
				break;
			case 'todayAvgCnt':
				location.replace(rootUrl+"admin/stat_member");
				setTimeout(() => {
					mainCon._isRouting = true;
					mainCon.onSubMenuBtnClick(null, Top.Dom.selectById('memberAnalysisBtn1'));
					mainCon._isRouting = false;
					}, 100 );					
				break;
			case 'todayTimeCnt':
				location.replace(rootUrl+"admin/stat_member");
				setTimeout(() => {
					mainCon._isRouting = true;
					mainCon.onSubMenuBtnClick(null, Top.Dom.selectById('memberAnalysisBtn1'));
					mainCon._isRouting = false;
					}, 100 );				
				break;
			case 'memberCnt':
				location.replace(rootUrl+"admin/stat_member");
				setTimeout(() => {
					mainCon._isRouting = true;
					mainCon.onSubMenuBtnClick(null, Top.Dom.selectById('memberAnalysisBtn2'));
					mainCon._isRouting = false;
					}, 100 );
				break;
			case 'deviceCnt':				
				location.replace(rootUrl+"admin/stat_member");
				setTimeout(() => {
					mainCon._isRouting = true;
					mainCon.onSubMenuBtnClick(null, Top.Dom.selectById('memberAnalysisBtn3'));
					mainCon._isRouting = false;
					}, 100 );
				break;
			case 'durationCnt':				
				location.replace(rootUrl+"admin/stat_member");
				setTimeout(() => {
					mainCon._isRouting = true;
					mainCon.onSubMenuBtnClick(null, Top.Dom.selectById('memberAnalysisBtn4'));
					mainCon._isRouting = false;
					}, 100 );
				break;
			case 'spaceCnt':
				location.replace(rootUrl+"admin/stat_cloudspace");
				setTimeout(() => {
					mainCon._isRouting = true;
					mainCon._isRouting = true;mainCon.onSubMenuBtnClick(null, Top.Dom.selectById('cloudSpaceAnalysisBtn1'));
					mainCon._isRouting = false},100 );				
				break;
			case 'appCnt':
				location.replace(rootUrl+"admin/stat_cloudspace");
				setTimeout(() => {
					mainCon._isRouting = true;
					mainCon._isRouting = true;mainCon.onSubMenuBtnClick(null, Top.Dom.selectById('cloudSpaceAnalysisBtn2'));
					mainCon._isRouting = false},100 );				
				break;
				
		
		}
		//멤버 분석
			//접속 현황
				//접속 현황 session_cnt todayCnt
				//1일 평균 사용자 수  todayAvgCnt_showMore
				//시간대별 사용자 수  todayTimeCnt_showMore
			
			
			//멤버 증감수
				//멤버 증감 수 member_cnt memberCnt
			
			//기기 유형
				//접속 기기 순위 device_type deviceCnt
			
			//평균 체류 시간
				//평균 체류 시간 session_duration_avg durationCnt
		
		
		//CloudSpace 통계
			//스페이스 통계
				//스페이스 수 space_cnt spaceCnt
			//앱 분석
				//앱 사용 순위 app_usage appCnt
		 
		
		
	}
});


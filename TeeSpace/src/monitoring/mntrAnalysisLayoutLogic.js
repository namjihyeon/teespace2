Top.Controller.create('mntrAnalysisLayoutLogic', {
	bar_option : {sd:'12'},
	bar_series : [{name:'pc', data:[1]}, {name:'Mobile 외 기타', data:[2]}, ],	
	
	
	init : function(event, widget) {
		console.log('analysis init called');
		console.log("mntr analysis logic");
//		if(Top.Controller.get('mntrMainLayoutLogic')._type) {
//			switch(Top.Controller.get('mntrMainLayoutLogic')._type) {
//				case "memberAnalysis":
//					Top.Controller.get('mntrMainLayoutLogic').onSubMenuBtnClick('initAnalysis', {id: "memberAnalysisBtn1"});
//					
//					break;
//				case "cloudSpaceAnalysis":
//					Top.Controller.get('mntrMainLayoutLogic').onSubMenuBtnClick('initAnalysis', {id: "cloudSpaceAnalysisBtn1"});
//					break;
//			}
//		}
		
		var hour_nodes = [];
		for (var i=0; i<24; i++){
			hour_nodes.push(`${i} 시`);
		}
		
		//radio btn 최근추이, 시간대별
//		Top.Dom.selectById('analysisRadioBtn1').setProperties({'on-change':function(){this.onChangeRadioBtn(event,widget)}});
		
		Top.Dom.selectById('hourlySelectBox').setProperties({'nodes':hour_nodes, 'selectedIndex':0, 'display':'none'});
		
		//일간 주간 버튼
		for(var i=0; i<4; i++){
			$(`button#analysisTimeType${i+1}`).css('background-color', 'rgba(0,0,0,0)');
		}
			$('button#analysisTimeType1').css('background-color', 'rgba(0,164,195,0.20)');
		Top.Controller.get('mntrMainLayoutLogic')._frequency = 'daily';
		
		
		//시간대별 버튼
		Top.Dom.selectById('hourlySelectBox').setProperties({'on-change':function(){this.onChangeHourlySelectBox(event, widget)}});
		
		if(Top.Controller.get('mntrMainLayoutLogic')._type) {
			switch(Top.Controller.get('mntrMainLayoutLogic')._type) {
				case "memberAnalysis":
					Top.Controller.get('mntrMainLayoutLogic').onSubMenuBtnClick('initAnalysis', {id: "memberAnalysisBtn1"});
					
					break;
				case "cloudSpaceAnalysis":
					Top.Controller.get('mntrMainLayoutLogic').onSubMenuBtnClick('initAnalysis', {id: "cloudSpaceAnalysisBtn1"});
					break;
			}
		}
		
//		setTimeout(() => {Top.Controller.get('mntrMainLayoutLogic').updateAnalysis(event, widget) },0 );
		
		//AppTable
		Top.Dom.selectById("dashboardMainLayoutNoticeTableView_1_2_1").setProperties({
			"column-option" : {
				"0" : {
					layout : function(data, idx) {												
						return '<top-linearlayout orientation="horizontal" border-width="0px" margin="0px 0px 0px 0px" vertical-alignment="center">'
						 + `<top-checkbox text="${data}" margin="5px" on-change="changeAppCheckState" checked="true" id="${data}_checkBox"></top-checkbox>`						 
						 + '</top-linearlayout>'
					}
				}
			}
		});
		
		
	},
	
	onMntrAnalysisTabChange : function(event, widget) {
		console.log('tab changes', event, widget);
		console.log(widget.getSelectedTab());
		Top.Controller.get('mntrMainLayoutLogic')._isTabClicked = true;
		
		switch(widget.getSelectedTab().id){
		case "mntrAnalysisTab_전체멤버_0":
			Top.Controller.get('mntrMainLayoutLogic')._measure = "member_cnt"
			break;
		case "mntrAnalysisTab_회원가입수_1":
			Top.Controller.get('mntrMainLayoutLogic')._measure = "member_diff"
			break;
		case "mntrAnalysisTab_전체스페이스_2":
			Top.Controller.get('mntrMainLayoutLogic')._measure = "space_cnt"
			break;
		case "mntrAnalysisTab_평균스페이스_3":
			Top.Controller.get('mntrMainLayoutLogic')._measure = "space_avg"
			break;
		case "mntrAnalysisTab_사용자수_4":
			Top.Controller.get('mntrMainLayoutLogic')._measure = "app_usage"
			break;
		case "mntrAnalysisTab_Q&A_5":
			Top.Controller.get('mntrMainLayoutLogic')._measure = "qna"
			break;
		case "mntrAnalysisTab_회원탈퇴사유_6":
			Top.Controller.get('mntrMainLayoutLogic')._measure = "user_delete"
			break;
		}
		
		//onSubMenuBtnClicked 함수에서 처리
		if(!Top.Controller.get('mntrMainLayoutLogic')._isSubMenuBtnClicked){
			this.initializeFrequencyBtn(event, widget);
			this.initializeRadioBtn(event, widget);			
		}
		
		
//		Top.Controller.get('mntrMainLayoutLogic').updateInfo(event, widget);
		Top.Controller.get('mntrMainLayoutLogic')._isTabClicked = false;
	},
	
	onChangeRadioBtn(event, widget){
		console.log('radio btn change: ', event, widget);
		if(!Top.Dom.selectById('analysisRadioBtn2').isChecked() && !Top.Dom.selectById('analysisRadioBtn1').isChecked()){
			Top.Dom.selectById('analysisRadioBtn2').setProperties({'checked':true});
		}
		
		var radioBtn1_checked = Top.Dom.selectById('analysisRadioBtn1').getChecked();
		var device_or_app = (Top.Controller.get('mntrMainLayoutLogic')._measure == 'app_usage' || Top.Controller.get('mntrMainLayoutLogic')._measure == 'device_type')
		
		if( (Top.Controller.get('mntrMainLayoutLogic')._measure == 'app_usage') ){
			Top.Dom.selectById('hourlySelectBox').setProperties({'display':'none'});
			Top.Dom.selectById('analysisTimeType1').style('visibility','hidden');
			
			if(Top.Controller.get('mntrMainLayoutLogic')._frequency == 'daily'){
				//일간 주간 버튼
				for(var i=0; i<4; i++){
					$(`button#analysisTimeType${i+1}`).css('background-color', 'rgba(0,0,0,0)');
				}
					$('button#analysisTimeType2').css('background-color', 'rgba(0,164,195,0.20)');
				Top.Controller.get('mntrMainLayoutLogic')._frequency = 'weekly';
			}
			
		}else if(radioBtn1_checked || (!radioBtn1_checked && device_or_app)){
			Top.Dom.selectById('hourlySelectBox').setProperties({'display':'none'});
			Top.Dom.selectById('analysisTimeType1').style('visibility','visible');
			
		}else {
			Top.Dom.selectById('hourlySelectBox').setProperties({'display':'inline'});
			Top.Dom.selectById('analysisTimeType1').style('visibility','hidden');
			
			
			if(Top.Controller.get('mntrMainLayoutLogic')._frequency = 'daily'){
				//일간 주간 버튼
				for(var i=0; i<4; i++){
					$(`button#analysisTimeType${i+1}`).css('background-color', 'rgba(0,0,0,0)');
				}
					$('button#analysisTimeType2').css('background-color', 'rgba(0,164,195,0.20)');
				Top.Controller.get('mntrMainLayoutLogic')._frequency = 'weekly';
			}
		}
		
		Top.Controller.get('mntrMainLayoutLogic').updateInfo(event, widget);
	},
	
	onHourlyBtnClick(event, widget){
		for(var i=0; i<widget.template.parentElement.children.length; i++){
			widget.template.parentElement.children[i].children[0].style.backgroundColor = 'rgba(0,0,0,0)';
		}
		widget.style({'background-color':'rgba(0,164,195,0.20)'});
		
		switch(parseInt(widget.id.split('Type')[1])){
			case 1:
				Top.Controller.get('mntrMainLayoutLogic')._frequency = 'daily';
				break;
			case 2:
				Top.Controller.get('mntrMainLayoutLogic')._frequency = 'weekly';
				break;				
			case 3:
				Top.Controller.get('mntrMainLayoutLogic')._frequency = 'monthly';
				break;
			case 4:
				Top.Controller.get('mntrMainLayoutLogic')._frequency = 'yearly';
				break;		
		}
		
		//radio 초기화
		this.initializeRadioBtn(event, widget);
		
		//update함수 호출
//		Top.Controller.get('mntrMainLayoutLogic').updateInfo(event, widget);
	},
	
	
	//페이지 옵션들 초기화 radio, hourlySelectBox, frequencyBtn 등등
	initializeRadioBtn(event, widget){
		console.log('init radio btn: ',event, widget);
		//radio btn 초기화
		
		if(Top.Controller.get('mntrMainLayoutLogic')._measure == 'app_usage' || Top.Controller.get('mntrMainLayoutLogic')._measure == 'device_type'){
			Top.Dom.selectById('analysisRadioBtn1').setProperties({'checked':false});			
		}else {
			Top.Dom.selectById('analysisRadioBtn1').setProperties({'checked':true});
		}
		
		//hourlySelectBox 초기화
		Top.Dom.selectById('hourlySelectBox').setProperties({'selectedIndex':0});
		
		
	},
	
	initializeFrequencyBtn(event, widget){
		console.log('init freq btn: ', event, widget);
		//frequency btn 초기화
		for(var i=0; i<4; i++){
			$(`button#analysisTimeType${i+1}`).css('background-color', 'rgba(0,0,0,0)');
		}
		if(Top.Controller.get('mntrMainLayoutLogic')._measure == 'app_usage'){
			$('button#analysisTimeType2').css('background-color', 'rgba(0,164,195,0.20)');
			Top.Controller.get('mntrMainLayoutLogic')._frequency = 'weekly';
		}else {
			$('button#analysisTimeType1').css('background-color', 'rgba(0,164,195,0.20)');
			Top.Controller.get('mntrMainLayoutLogic')._frequency = 'daily';
		}
			
		
		
		if(!Top.Controller.get('mntrMainLayoutLogic')._isSubMenuBtnClicked){
			if(!Top.Controller.get('mntrMainLayoutLogic')._isTabClicked){
				this.initializeRadioBtn(event, widget);
			}
		}
		
	},
	
	changeAppCheckState(event, widget){
		console.log('check pressed');
		var talk_check = Top.Dom.selectById('T-Talk_checkBox').isChecked() ? 1 : 0;
		var mail_check = Top.Dom.selectById('T-Mail_checkBox').isChecked() ? 1 : 0;
		var note_check = Top.Dom.selectById('T-Note_checkBox').isChecked() ? 1 : 0;
		var office_check = Top.Dom.selectById('CloudOffice_checkBox').isChecked() ? 1 : 0;
		var schedule_check = Top.Dom.selectById('T-Schedule_checkBox').isChecked() ? 1 : 0;
		var drive_check = Top.Dom.selectById('T-Drive_checkBox').isChecked() ? 1 : 0;
		var conference_check = Top.Dom.selectById('Conference_checkBox').isChecked() ? 1 : 0;
		
		
		//차트
		$('top-chart#Chart400 div.top-chart-root g.TOP-shapes-T-Talk.TOP-lines.TOP-lines-T-Talk path').css('opacity', talk_check);
		$('top-chart#Chart400 div.top-chart-root g.TOP-shapes-T-Mail.TOP-lines.TOP-lines-T-Mail path').css('opacity', mail_check);
		$('top-chart#Chart400 div.top-chart-root g.TOP-shapes-T-Note.TOP-lines.TOP-lines-T-Note path').css('opacity', note_check);
		$('top-chart#Chart400 div.top-chart-root g.TOP-shapes-CloudOffice.TOP-lines.TOP-lines-CloudOffice path').css('opacity', office_check);
		$('top-chart#Chart400 div.top-chart-root g.TOP-shapes-T-Schedule.TOP-lines.TOP-lines-T-Schedule path').css('opacity', schedule_check);
		$('top-chart#Chart400 div.top-chart-root g.TOP-shapes-T-Drive.TOP-lines.TOP-lines-T-Drive path').css('opacity', drive_check);
		$('top-chart#Chart400 div.top-chart-root g.TOP-shapes-Conference.TOP-lines.TOP-lines-Conference path').css('opacity', conference_check);
		
		
	},
	
	onChangeHourlySelectBox(event, widget){
		console.log('on chage hourly select box');
		
		Top.Controller.get('mntrMainLayoutLogic').updateInfo(event, widget);
	}
});

function setTabs(indexArray) {
	if(indexArray.length === 0) {
		$("#mntrAnalysisTab").hide();
	} else {
		$("#mntrAnalysisTab").show();
	}
	
	Top.Dom.selectById("mntrAnalysisTab").getTabs().forEach(function(tab, index) {
		Top.Dom.selectById("mntrAnalysisTab").removeHiddenTabByIndex(index);
		if(!indexArray.includes(index)) Top.Dom.selectById("mntrAnalysisTab").hiddenTabByIndex(index); 
	});
	if(indexArray.length > 0) {
		Top.Dom.selectById("mntrAnalysisTab").select(Top.Dom.selectById("mntrAnalysisTab").getTabs()[indexArray[0]].id);
	}
	
}
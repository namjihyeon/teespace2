var mntrDAUData = {
	//접속 현황
	dauSessionCnt: 1000,
	//멤버 증감 수
	dauUserDiff: 252,
	//총 멤버
	dauUserCnt: 50000,
	//접속 기기 순위
	dauDeviceRankSeries: [{
		name: "Web",
		data: [60],
	}, {
		name: "App",
		data: [40],
	}],
	//평균 체류 시간
	dauSessionDurationAvg: "6h 60m",
	//스페이스 수
	dauSpaceCnt: 500000,
	//평균 스페이스 수
	dauSpaceAvg: 100,
	//앱 사용 순위
	dauAppUsageRankSeries: [{
		name: "T-Talk",
		data: [50],
	}, {
		name: "T-Drive",
		data: [30],
	}, {
		name: "T-Schedule",
		data: [10],
	}, {
		name: "CloudOffice",
		data: [5],
	}],
	load: function() {
		Top.Dom.selectById("dauSessionCnt").setHTML(this.dauSessionCnt);

		if(this.dauUserDiff > 0) {
			Top.Dom.selectById("dauUserDiff").setHTML('+' + this.dauUserDiff);
		} else if(this.dauUserDiff < 0) {
			Top.Dom.selectById("dauUserDiff").setHTML('-' + this.dauUserDiff);
		} else {
			Top.Dom.selectById("dauUserDiff").setHTML('-');
		}
		
		Top.Dom.selectById("dauUserCnt").setHTML(this.dauUserCnt);
		Top.Dom.selectById("dauSessionDurationAvg").setHTML(this.dauSessionDurationAvg);
		Top.Dom.selectById("dauSpaceCnt").setHTML(this.dauSpaceCnt);
		Top.Dom.selectById("dauSpaceAvg").setHTML(this.dauSpaceAvg);
	}
};

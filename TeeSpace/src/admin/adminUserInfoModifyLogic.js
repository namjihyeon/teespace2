
Top.Controller.create('adminUserInfoModifyLogic', {
	
	//selectBoxValueList: [],
	
	
	
	init : function(event, widget) {
		this.user_id;
		this.org_path_array;
		//프로필 수정 시 드롭다운 메뉴(pc/삭제)
		$('div#adminUserInfoModify').on('click', function(e){
			//프로필 사진  > 더보기 메뉴
			if($('div#adminUserInfoModify').css('display') == 'inline-block'){
			
				adminProfile_list(e, "ImageButton_Admin_Profile_Setting");		
			}
		});
		//이벤트 선언
		Top.Dom.selectById('UserInfoModify_UserPasswordContent').setProperties({'on-keyup':'admin_pwd_check'});
	},
	
	init_contents : function(user_id){
	
		this.user_id = user_id;
		
		
		var inputDTO = {
				"dto": {
					"USER_ID": user_id
				}
		};
		
		
		
		
		Top.Ajax.execute({
			url : _workspace.url + "Admin/OrgUser?action=",
			type : 'GET',
			dataType : "json",
			async : false,
			cache : false,
			data : JSON.stringify(inputDTO), // arg
			contentType : "application/json",
			crossDomain : true,
			xhrFields : {
			    withCredentials: true
			},
			headers: {
				"ProObjectWebFileTransfer":"true"
			},
			success : function(data) {
				
				var outputdata;
				data = JSON.parse(data);
				if(data.dto == undefined) {
				    outputdata = {
				        result : "Success",
				        message : data.exception.name + ' ' + data.exception.message,
				        data : data
				    }
				} else {
				    outputdata = data.dto;
				    outputdata.files = data.files;
				}
				
				console.log(outputdata);
				
				
				//companyProfileList 겸임인경우(소속/부서 selectbox 제외)
				for(let count = 0; count< outputdata.companyProfileList.length; count++){				
					
					
		            if(outputdata.companyProfileList[count].USER_AFF === orgUserRepo.orgUserList[Top.Dom.selectById("OrgUserTableView").getClickedIndex()[0]].org_name ){
		            	 Top.Dom.selectById("UserInfoModify_UserCompanyPositionContent").select(outputdata.companyProfileList[count].USER_POSITION)
		 			    Top.Dom.selectById('UserInfoModify_UserEmploymentTypeContent').select(adminCodeToText(outputdata.companyProfileList[count].USER_EMPLOYMENT_TYPE));				
		 			    Top.Dom.selectById('UserInfoModify_UserCompanyIDContent').setText(outputdata.companyProfileList[count].USER_EMPLOYEE_NUM);	
		 			    Top.Dom.selectById('UserInfoModify_UserCompanyContent').setText(outputdata.companyProfileList[count].USER_AFF);
		 				if(outputdata.companyProfileList[count].USER_COM_NUM){
		 					Top.Dom.selectById('UserInfoModify_UserCompanyPhoneNumContent0').setText(outputdata.companyProfileList[count].USER_COM_NUM.split('-')[0]);
		 					Top.Dom.selectById('UserInfoModify_UserCompanyPhoneNumContent1').setText(outputdata.companyProfileList[count].USER_COM_NUM.split('-')[1]);
		 					Top.Dom.selectById('UserInfoModify_UserCompanyPhoneNumContent2').setText(outputdata.companyProfileList[count].USER_COM_NUM.split('-')[2]);
		 				}
		 				Top.Dom.selectById('UserInfoModify_UserInChargeOfContent').setText(outputdata.companyProfileList[count].USER_TASK);
		 				Top.Dom.selectById('UserInfoModify_UserOfficeLocationContent').setText(outputdata.companyProfileList[count].USER_LOCATION);
		 				Top.Dom.selectById("UserInfoModify_UserJobContent").select(outputdata.companyProfileList[count].USER_JOB)
		 				
		 				if(outputdata.companyProfileList[count].USER_EMPLOYMENT_DATE){
		 					Top.Dom.selectById('UserInfoModify_UserAdmittanceDateContent0').setText(outputdata.companyProfileList[count].USER_EMPLOYMENT_DATE.split('-')[0]);
		 					Top.Dom.selectById('UserInfoModify_UserAdmittanceDateContent1').setText(outputdata.companyProfileList[count].USER_EMPLOYMENT_DATE.split('-')[1]);
		 					Top.Dom.selectById('UserInfoModify_UserAdmittanceDateContent2').setText(outputdata.companyProfileList[count].USER_EMPLOYMENT_DATE.split('-')[2]);
		 				}
		            	
		            	
		            }

				
				}
				
				
				
				
				var user_info = outputdata.userInfo;
				//Top.Dom.selectById('UserInfoModify_UserEmploymentTypeContent').select(adminCodeToText(company_info.USER_EMPLOYMENT_TYPE));
				//Top.Dom.selectById('UserInfoModify_UserCompanyPositionContent').select(company_info.USER_POSITION);
				
				
				
				//Top.Dom.selectById('UserInfoModify_UserCompanyIDContent').setText(company_info.USER_EMPLOYEE_NUM);
				Top.Dom.selectById('UserInfoModify_UserFirstNameContent').setText(user_info.USER_NAME);
				//Top.Dom.selectById('UserInfoModify_UserCompanyContent').setText(company_info.USER_AFF);
				Top.Dom.selectById('UserInfoModify_UserIDContent').setText(user_info.USER_LOGIN_ID.split('@')[0]);
				
				if(user_info.USER_EMAIL){
					Top.Dom.selectById('UserInfoModify_UserEmailIDContent').setText(user_info.USER_EMAIL.split('@')[0]);
					Top.Dom.selectById('UserInfoModify_UserEmailDomainContent').setText(user_info.USER_EMAIL.split('@')[1]);
				}
//				if(company_info.USER_COM_NUM){
//					Top.Dom.selectById('UserInfoModify_UserCompanyPhoneNumContent0').setText(company_info.USER_COM_NUM.split('-')[0]);
//					Top.Dom.selectById('UserInfoModify_UserCompanyPhoneNumContent1').setText(company_info.USER_COM_NUM.split('-')[1]);
//					Top.Dom.selectById('UserInfoModify_UserCompanyPhoneNumContent2').setText(company_info.USER_COM_NUM.split('-')[2]);
//				}
				if(user_info.USER_PHONE){
					Top.Dom.selectById('UserInfoModify_UserCellPhoneNumContent0').setText(user_info.USER_PHONE.split('-')[0]);
					Top.Dom.selectById('UserInfoModify_UserCellPhoneNumContent1').setText(user_info.USER_PHONE.split('-')[1]);
					Top.Dom.selectById('UserInfoModify_UserCellPhoneNumContent2').setText(user_info.USER_PHONE.split('-')[2]);
				}
				//Top.Dom.selectById('UserInfoModify_UserInChargeOfContent').setText(company_info.USER_TASK);
				//Top.Dom.selectById('UserInfoModify_UserOfficeLocationContent').setText(company_info.USER_LOCATION);
				
				if(user_info.USER_BIRTH){
					if(user_info.USER_BIRTH.length == 6){
						Top.Dom.selectById('UserInfoModify_UserBirthdayContent0').setText(user_info.USER_BIRTH.substr(0, 2));
						Top.Dom.selectById('UserInfoModify_UserBirthdayContent1').setText(user_info.USER_BIRTH.substr(2, 2));
						Top.Dom.selectById('UserInfoModify_UserBirthdayContent2').setText(user_info.USER_BIRTH.substr(4, 2));
					}
					else if(user_info.USER_BIRTH.length == 8){
						Top.Dom.selectById('UserInfoModify_UserBirthdayContent0').setText(user_info.USER_BIRTH.substr(0, 4));
						Top.Dom.selectById('UserInfoModify_UserBirthdayContent1').setText(user_info.USER_BIRTH.substr(4, 2));
						Top.Dom.selectById('UserInfoModify_UserBirthdayContent2').setText(user_info.USER_BIRTH.substr(6, 2));
						
					}
					else{
						console.log("생일 형식 잘못들어옴")
						
					}
				}
				
//				if(company_info.USER_EMPLOYMENT_DATE){
//					Top.Dom.selectById('UserInfoModify_UserAdmittanceDateContent0').setText(company_info.USER_EMPLOYMENT_DATE.split('-')[0]);
//					Top.Dom.selectById('UserInfoModify_UserAdmittanceDateContent1').setText(company_info.USER_EMPLOYMENT_DATE.split('-')[1]);
//					Top.Dom.selectById('UserInfoModify_UserAdmittanceDateContent2').setText(company_info.USER_EMPLOYMENT_DATE.split('-')[2]);
//				}
//				// dummy data ****************************************
//				// dummy data ****************************************
//				for(var i =0;i<4;i++){
//					outputdata.companyProfileList[i] = outputdata.companyProfileList[0];
//				}
//				// dummy data ****************************************
//				// dummy data ****************************************
				//부서 소속
				for(let ncmp=0; ncmp<outputdata.companyProfileList.length;ncmp++){
					Top.Controller.get('adminUserInfoModifyLogic').set_org_path(outputdata.companyProfileList[ncmp].ORG_PATH, ncmp);
				}
				
				
				//프로필 가져오기
				   if(user_info.THUMB_PHOTO == null){
                       Top.Dom.selectById('ImageButton_Admin_Profile_Setting').setSrc(userManager.getUserDefaultPhotoURL(user_id));
  		        	 }else{
   		        		 Top.Dom.selectById("ImageButton_Admin_Profile_Setting").setSrc(user_info.THUMB_PHOTO)		
  		        	 }
					//소속회사
					$.ajax({
						url : _workspace.url + "Admin/CompanyInfo", 
			            type: 'GET',
			            dataType: 'json',
			            crossDomain: true,
			            contentType: 'application/json; charset=utf-8',
			            xhrFields: {
			                withCredentials: true
			              },
			            data: JSON.stringify({
			              "dto": {
			                "ADMIN_ID": userManager.getLoginUserId()
			              }
			            }),
			            success: function(result) {
			            	Top.Dom.selectById('UserInfoModify_UserCompanyContent').setText(result.dto.COMPANY_NAME)
				        },
				        error: function(error) {        	
				        	
				        }
						});	
					
			},
			error : function(data) {
				var outputdata;
	            outputdata = {
	                result : "Fail",
	                message : data.exception != undefined ? data.exception.message : "Error",
	                data : data
	            }		
	            console.info("fail");
	            console.info(outputdata);
			}
		});
		
		
	},
	
	set_org_path : function(org_path, ncmp){
		
		
		var org_array = org_path.split(' > ');
		var org_tree = orgTreeRepo.orgTree[0].children;
		
		var select_box_array = [];
		
		this.org_path_array = org_array; //save state for this page
		
		var tmp = org_tree;
		for(var i=0; i < org_array.length; i++){
			
			select_box_array.push(tmp.map(a => a.text));
			tmp = tmp.find(function(value){return org_array[i] === value.text;}).children;

		}
		
		this.set_orgpath_selectbox(org_array, select_box_array, ncmp);
		
	},
	
	set_orgpath_selectbox : function(org_array, select_box_array, ncmp){
var tar = Top.Dom.selectById('UserInfoModify_UserOccupationPath1');
		var textview;
		var selectbox;
		var w = Top.Widget.create('top-linearlayout');
		w.setProperties({
			id: 'pathWrapper' + '_' + ncmp,
			className: 'org-path-wrapper',
			orientation: 'horizontal',
			'layout-height': 'wrap_content',
			'layout-width': 'wrap_content',
			'border-width': '0 0 0 0',
		})
		for(var i=0; i < select_box_array.length; i++){
			selectbox = Top.Widget.create('top-selectbox');
			selectbox.setProperties({
				'id': 'path_selectbox_' +ncmp +  '_'+ i,
				'layout-height': 'wrap_content',
				'layout-width': '130px',
				'nodes':select_box_array[i],
				'text-size': '13px',
				'on-change':this.on_change_selectbox
			});
			selectbox.select(org_array[i]);
			w.addWidget(selectbox);
			
			if(i != select_box_array.length - 1){
				textview = Top.Widget.create('top-textview');
				textview.setProperties({
					'id' : 'path_arrowtext_'+ncmp +  '_' + String(i + 1),
					'layout-height': '15',
					'layout-width': '15',
					'text': '>',
					'text-color': '#8C8E92',
					'text-size': '13px'
				});
				w.addWidget(textview);
			}
		}
		w.complete();
		tar.addWidget(w);
		tar.complete();
		
	},
	
	on_change_selectbox : function(event, widget){
		
		//controller(this) is different for selectbox for some reason..
		var ctrl = Top.Controller.get('adminUserInfoModifyLogic');
		
		
		var ncmp = parseInt(widget.id.split('_')[2]); // which company
		var index = parseInt(widget.id.split('_')[3]); // which selectbox
		
		
		//detach '>' and selectbox
		var layout = Top.Dom.selectById('UserInfoModify_UserOccupationPath1');
		var arrowtext;
		var selectbox;
		
		for(var i = index + 1; arrowtext = Top.Dom.selectById('path_arrowtext_' + ncmp + '_' + i); i++){ 
			//loop until widget doesn't exist
			selectbox = Top.Dom.selectById('path_selectbox_' + ncmp + '_' + i);
			layout.removeWidget(selectbox);
			layout.removeWidget(arrowtext);
			ctrl.org_path_array.pop();
		}
		
	
		//update org_path_array for current index
		selectbox = Top.Dom.selectById('path_selectbox_' + ncmp + '_' + index);
		ctrl.org_path_array.splice(index, 1, selectbox.getSelected());
		
		//find org_tree
		var org_tree = orgTreeRepo.orgTree[0].children;
		var org_array = ctrl.org_path_array;
		
		var org_tree_index = org_tree;
		for(var i=0; i < index; i++){
			
			org_tree_index = org_tree_index.find(function(value){return org_array[i] === value.text;}).children;

		}
		
		
		selectbox = Top.Dom.selectById('path_selectbox_' + ncmp + '_' + index);
		var next_node = org_tree_index.find(function(value){return selectbox.getSelected() === value.text;}).children;
		
		//add children
		if(next_node.length != 0){
			arrowtext = Top.Widget.create('top-textview');
			arrowtext.setProperties({
				'id' : 'path_arrowtext_' + (index + 1),
				'layout-height': '15',
				'layout-width': '15',
				'text': '>',
				'text-color': '#8C8E92',
				'text-size': '13px'
			});
			layout.addWidget(arrowtext);
			layout.complete();
			
			selectbox = Top.Widget.create('top-selectbox');
			selectbox.setProperties({
				'id': 'path_selectbox_' + (index + 1),
				'layout-height': 'wrap_content',
				'layout-width': '130px',
				'nodes':next_node.map(a => a.text),
				'text-size': '13px',
				'on-change':ctrl.on_change_selectbox
			});
			
			layout.addWidget(selectbox);
			layout.complete();
		}
		
//		for(let j = 0 ; j<index ; j++){
//			if(!Top.Dom.selectById('path_selectbox_' + ncmp + '_' + (index+1))){
//				
//				this.selectBoxValueList.push(widget.id)
//			}
//			그다음위젯 .getSelectedText() 없으면
//			지금 우젯 값을 this.selectBoxValueList.push()에 넣음 
//		}
		
		
	},
	iconclick : function(event, widget) {
		let fieldid = widget.id;
		var icon = $("#" + widget.id).find("#" + widget.id);

		if(Top.Dom.selectById(fieldid).getProperties("password") == true){
			Top.Dom.selectById(fieldid).setProperties({"icon": "icon-ic_password_show", "password":"false"});
			// 보이기모드
			icon.attr('type','text');
		}else{
			Top.Dom.selectById(fieldid).setProperties({"icon": "icon-ic_password_hide", "password":"true"});
			//숨기기 모드
			icon.attr('type','password')
		}
	},
	click_cancel : function(event, widget) {
		
		var main = Top.Dom.selectById('adminUserInfoMain');
		var ctrl = Top.Controller.get('adminUserInfoViewLogic');
		let user_id = this.user_id;
		main.onSrcLoad(function(){
			ctrl.init_contents(user_id); //USER_ID 키값 넘김
		});
		main.src('adminUserInfoView.html' + verCsp()); //첫 화면은 유저 상세 보기
		
	}, 
	
	click_confirm : function(event, widget) {
				
	//	this.selectBoxValueList
		
		
		//note : org tree에서 바로 ID를 org_path_array에 obj로 집어넣는게 더 효율적이나, 생각하기 편한 방식으로 일단 구현함..
		//var comp_obj = adminGetTeamObjByName(this.org_path_array[this.org_path_array.length - 1]);
		var comp_obj = adminGetTeamObjByName(Top.Dom.selectById("TreeView1305").getSelectedNode().text)
		var role_code;
		
		if(Top.Dom.selectById('UserInfoModify_UserCompanyPositionContent').getSelectedText() === "팀원"){
			role_code = "WKS0005";
		} else{
			role_code = "WKS0004";
		}	
		//프로필이미지
		var image;
		if(Top.Dom.selectById("ImageButton_Admin_Profile_Setting").getSrc() === userManager.getUserDefaultPhotoURL(Top.Controller.get("adminUserInfoModifyLogic").user_id)){
			image = userManager.getUserDefaultPhotoURL(Top.Controller.get("adminUserInfoModifyLogic").user_id)
    	}else{
    		image = Top.Dom.selectById("ImageButton_Admin_Profile_Setting").getSrc();
    	}
		
		
		//비밀번호
		var flag_pwd = 0; 
		if( Top.Dom.selectById("Icon_userInfo_pwd_error").getProperties("text-color")=="#07C25B"){
			Top.Dom.selectById("UserInfoModify_UserPasswordInfoErrorIcon").setProperties({"visible":"none"});
			Top.Dom.selectById("UserInfoModify_UserPasswordContent").setProperties({"border-color":"#dddddd"})
		}else{
			if(Top.Dom.selectById("UserInfoModify_UserPasswordContent").getText() === undefined || Top.Dom.selectById("UserInfoModify_UserPasswordContent").getText() === "" ){
				Top.Dom.selectById("UserInfoModify_UserPasswordInfoErrorIcon").setProperties({"visible":"none"});
				Top.Dom.selectById("UserInfoModify_UserPasswordContent").setProperties({"border-color":"#dddddd"})
				
			}else{
				Top.Dom.selectById("UserInfoModify_UserPasswordInfoErrorIcon").setProperties({"visible":"visible"});
				Top.Dom.selectById("UserInfoModify_UserPasswordContent").setProperties({"border-color":"red"})
				flag_pwd = 1;
				}
		}
		
		//이름
		var flag_name = 0;
		if(Top.Dom.selectById("UserInfoModify_UserFirstNameContent").getText() === ""){
			Top.Dom.selectById("UserInfoModify_UserFirstNameContent").setProperties({"border-color":"red"})
			Top.Dom.selectById("UserInfoModify_UserNameErrorIcon").setProperties({"visible":"visible"})
			flag_name = 1;
		}
		 if(flag_name === 0){
		        Top.Dom.selectById("UserInfoModify_UserFirstNameContent").setProperties({"border-color":"#dddddd "})
		        Top.Dom.selectById("UserInfoModify_UserNameErrorIcon").setProperties({"visible":"none"})
		 }
		//사번
		 
		 var flag_orgnum = 0;
		 if(Top.Dom.selectById("UserInfoModify_UserCompanyIDContent").getText() === ""){
				Top.Dom.selectById("UserInfoModify_UserCompanyIDContent").setProperties({"border-color":"red"})
				Top.Dom.selectById("UserInfoModify_UseOrgnumErrorIcon").setProperties({"visible":"visible"})
				flag_orgnum = 1;
			}
			 if(flag_orgnum === 0){
			        Top.Dom.selectById("UserInfoModify_UserCompanyIDContent").setProperties({"border-color":"#dddddd "})
			        Top.Dom.selectById("UserInfoModify_UseOrgnumErrorIcon").setProperties({"visible":"none"})
			 }
		
		//이메일
		var flag_mail = 0;
		var flag_domain = 0;
		if(Top.Dom.selectById("UserInfoModify_UserEmailIDContent").getText() === ""){
			Top.Dom.selectById("UserInfoModify_UserEmailIDContent").setProperties({"border-color":"red"})
			Top.Dom.selectById("UserInfoModify_UserMailErrorIcon").setProperties({"visible":"visible"})
			flag_mail = 1;
			
		}
		 if(Top.Dom.selectById("UserInfoModify_UserEmailDomainContent").getText() === ""){
		    Top.Dom.selectById("UserInfoModify_UserEmailDomainContent").setProperties({"border-color":"red"})
		 	Top.Dom.selectById("UserInfoModify_UserMailErrorIcon").setProperties({"visible":"visible"})
		 	flag_domain = 1;
		 }
		 
		 if(flag_mail === 0){
		        Top.Dom.selectById("UserInfoModify_UserEmailIDContent").setProperties({"border-color":"#dddddd "})
		        Top.Dom.selectById("UserInfoModify_UserMailErrorIcon").setProperties({"visible":"none"})
		 }
		 if(flag_domain === 0){
		        Top.Dom.selectById("UserInfoModify_UserEmailDomainContent").setProperties({"border-color":"#dddddd "})
		         Top.Dom.selectById("UserInfoModify_UserMailErrorIcon").setProperties({"visible":"none"})
		 }
		 if(flag_mail || flag_domain){
			 Top.Dom.selectById("UserInfoModify_UserMailErrorIcon").setProperties({"visible":"visible"})
		 } 
		
		
		//리턴 처리
		if(flag_pwd || flag_name || flag_orgnum ||flag_mail || flag_domain){ return;}
		
		var userInfo = {
			 "USER_ID":this.user_id,
	         "USER_PW":Top.Dom.selectById('UserInfoModify_UserPasswordContent').getText(),
	         "USER_NAME":Top.Dom.selectById('UserInfoModify_UserFirstNameContent').getText(),
	         "USER_EMAIL":Top.Dom.selectById('UserInfoModify_UserEmailIDContent').getText() + '@' + Top.Dom.selectById('UserInfoModify_UserEmailDomainContent').getText(),
	         "USER_BIRTH":Top.Dom.selectById('UserInfoModify_UserBirthdayContent0').getText() + Top.Dom.selectById('UserInfoModify_UserBirthdayContent1').getText()
	         + Top.Dom.selectById('UserInfoModify_UserBirthdayContent2').getText(),
	         "USER_PHONE":Top.Dom.selectById('UserInfoModify_UserCellPhoneNumContent0').getText() + '-'
	         + Top.Dom.selectById('UserInfoModify_UserCellPhoneNumContent1').getText() + '-'
	         + Top.Dom.selectById('UserInfoModify_UserCellPhoneNumContent2').getText(),
	         "PROFILE_PHOTO": image
		};
		
		
		
		var pathJob = Top.Dom.selectById("path_selectbox_0_3").getSelected()
		
		var companyProfileList = [{

				"USER_EMPLOYEE_NUM":Top.Dom.selectById('UserInfoModify_UserCompanyIDContent').getText(),
	            "USER_JOB":Top.Dom.selectById('UserInfoModify_UserJobContent').getSelectedText(),
	            "USER_POSITION":Top.Dom.selectById('UserInfoModify_UserCompanyPositionContent').getSelectedText(),
	            "USER_AFF":comp_obj.COMPANY_CODE + '_' + comp_obj.DEPARTMENT_CODE,
	            "USER_ROLE":role_code,//Top.Dom.selectById('').getText(),
	            "USER_COM_NUM": Top.Dom.selectById('UserInfoModify_UserCompanyPhoneNumContent0').getText() + '-'
	            + Top.Dom.selectById('UserInfoModify_UserCompanyPhoneNumContent1').getText() + '-' + Top.Dom.selectById('UserInfoModify_UserCompanyPhoneNumContent2').getText(),
	            "USER_EMPLOYMENT_TYPE":adminTextToCode(Top.Dom.selectById('UserInfoModify_UserEmploymentTypeContent').getSelectedText()),//Top.Dom.selectById('').getText(),
	            "USER_TASK":Top.Dom.selectById('UserInfoModify_UserInChargeOfContent').getText(),
	            "USER_LOCATION":Top.Dom.selectById('UserInfoModify_UserOfficeLocationContent').getText(),
	            "USER_EMPLOYMENT_DATE":Top.Dom.selectById('UserInfoModify_UserAdmittanceDateContent0').getText() + '-'
	            + Top.Dom.selectById('UserInfoModify_UserAdmittanceDateContent1').getText() + '-'
	            + Top.Dom.selectById('UserInfoModify_UserAdmittanceDateContent2').getText(),
	            "ORG_NAME" : pathJob
	            //"USER_TRANSFER_AFF" : comp_obj.COMPANY_CODE/*이동할 소속의 회사코드*/ + '_' + comp_obj.DEPARTMENT_CODE
				}];
	
		
		
		
		var inputDTO = {
			dto: {
		        "userInfo": userInfo,
		        "companyProfileList": companyProfileList
		    }
		};
		
		Top.Ajax.execute({
			url : _workspace.url + "Admin/OrgUser",
			type : 'PUT',
			dataType : "json",
			async : false,
			cache : false,
			data : JSON.stringify(inputDTO), // arg
			contentType : "application/json",
			crossDomain : true,
			xhrFields : {
			    withCredentials: true
			},
			headers: {
				"ProObjectWebFileTransfer":"true"
			},
			success : function(data) {
				
		
				//테이블 업데이트			
				var orgUserInfo = {
						user_name: userInfo.USER_NAME,
						org_name: companyProfileList[0].ORG_NAME,
						job: companyProfileList[0].USER_POSITION,
						com_num: companyProfileList[0].USER_COM_NUM,
						email: userInfo.USER_EMAIL,
						status: Top.Dom.selectById('UserInfoModify_UserEmploymentTypeContent').getSelectedText(),
						USER_ID: userInfo.USER_ID,
						affcode: companyProfileList[0].USER_AFF
				};
				orgUserRepo.orgUserList[Top.Dom.selectById("OrgUserTableView").getClickedIndex()[0]]  = orgUserInfo	
				//소속이 바뀔 경우
				
				
				Top.Dom.selectById("OrgUserTableView").update();
				
				//확인 클릭 시 view화면으로 이동
            	Top.Dom.selectById('adminUserInfoMain').onSrcLoad(function(){
	            	Top.Controller.get('adminUserInfoViewLogic').init_contents(userInfo.USER_ID); 
               	});

                   Top.Dom.selectById('adminUserInfoMain').src('adminUserInfoView.html' + verCsp());
				
				
			},
			
			error : function(data){
			}
		});
		
	},
	//프로필 사진 저장
	choosefile : function(event, widget) {
		
			let fileChooser = Top.Device.FileChooser.create({
			    onBeforeLoad : function() {
			        let newFileList = [];
					let file = {};
					let tmpId = 0;
			        for(var i=0; i<this.file.length; i++) {		  
			            file = this.file[i];
			            
	    		        let loadFile = fileManager.onBeforeLoadFile(file); 
	    		        if(!loadFile.result) { 
	    		            try {
	    		            	TeeToast.open({
	    							text: loadFile.message,
	    							size: 'min'
	    						});
	    		            } catch (e) {}
	    		
	    		        }
	    		        
	    		        let nomalizedFileFullName = Top.Util.Browser.isIE() ? file.name : file.name.normalize('NFC');        
	    		        let fileNameArray = nomalizedFileFullName.split('.');
	    		        let fileExt = fileNameArray.length == 1 ? "" : fileNameArray[fileNameArray.length - 1];        
	    		        if(fileExt == "jpeg" || fileExt == "png" || fileExt == "jpg" || fileExt == "JPEG" || fileExt == "PNG" || fileExt == "JPG") {
	    		    		
	    		        }else{
	    		        	try {
	    		        		Top.Dom.selectById("Icon_admin_profile_error").setProperties({"visible":"visible", "tooltip-top":"JPG, PNG 파일만 가능합니다."}); 
	    		        		return false;
	    						
	    		            } catch (e) {}
	    		
	    		        }
						
						newFileList.push({
	    		            file_id         : tmpId++,
	                        file_extension  : fileExt,
	                        fileFullName    : nomalizedFileFullName,
	                        file_status     : FileStatus.TRY_UPLOAD,
						});
	                }
			      
		        },
				onFileChoose : Top.Controller.get('adminUserInfoModifyLogic').onFileSelected,
			    charset: "euc-kr",
			    multiple: true
		    });
			fileChooser.show();
			
		//}else{
			// 티 드라이브에서 열기
//			var config = {
//					buttonText1: '열기',
//					buttonText2: '취소',
//					buttonFn1: function(){},
//					buttonFn2: function(){},
//					onOpenFn: function(){},
//					onCloseFn: function(){},
//					onOverlayClickFn: function(){},
//			}
//			OPEN_TDRIVE(config);
		//}
		
	},	onFileSelected : function(fileChooser) {
		let src = fileChooser[0].src;
        Top.Dom.selectById("ImageButton_Admin_Profile_Setting").setSrc(src)
     
		//let _USER_ID =  userManager.getLoginUserId()
		let Fext = src.split('/')[1].split(";")[0];
		let srcSend = src.split(',')[1];
//		let inputDTO ={
//            	files : [{
//                    filename : 'profilePhoto.' + Fext,
//                    contents : srcSend,
//                }],
//		          dto: {
//		            USER_ID: user_id,
//		            PROFILE_PHOTO : src 
//		          }
//		     };

    }, delete_pic : function(event, widget){ 
    	
    	
		encodeImage(userManager.getUserDefaultPhotoURL(Top.Controller.get("adminUserInfoModifyLogic").user_id), deletePhoto_admin(enImage))
        
    }, admin_pwd_check : function(event, widget) {
    	
    	
    	$("input#UserInfoModify_UserPasswordContent").css("style","ime-mode:active")
    	var pwdtext = Top.Dom.selectById("UserInfoModify_UserPasswordContent").getText()
		
	
			var pwdarry = new Array(2);
		if(pwdtext.length >= 9 && pwdtext.length <= 20){
			pwdarry[0]=1;
		}
		else {
						
		}
		if((pattern_num.test(pwdtext)) && (pattern_engs.test(pwdtext)) && (pattern_spc2.test(pwdtext))){
	
			pwdarry[1]=1;
		}
		else if((pattern_num.test(pwdtext)) && (pattern_engs.test(pwdtext)) && (pattern_engb.test(pwdtext))){
			pwdarry[1]=1;
		}
		else if((pattern_engs.test(pwdtext)) && (pattern_spc2.test(pwdtext)) && (pattern_engb.test(pwdtext))){
			pwdarry[1]=1;
		}
		else{
		}
		
		if( pwdarry[0]==1 && pwdarry[1]==1){
			Top.Dom.selectById("UserInfoModify_UserPasswordInfoErrorIcon").setProperties({"visible":"none"});
			Top.Dom.selectById("UserInfoModify_UserPasswordContent").setProperties({"border-color":"#dddddd"})
			Top.Dom.selectById("Icon_userInfo_pwd_error").setProperties({"text-color":"#07C25B"});
		}else{
			Top.Dom.selectById("UserInfoModify_UserPasswordInfoErrorIcon").setProperties({"visible":"none"});
			Top.Dom.selectById("UserInfoModify_UserPasswordContent").setProperties({"border-color":"#dddddd"})
			Top.Dom.selectById("Icon_userInfo_pwd_error").setProperties({"text-color":"#808080"});
		}
		

	}
	
	
	

});




function adminProfile_list(e, clickID) {
	
	if(e.toElement != undefined){
		if(e.toElement.id == clickID){
			more_list_adminProfile =
				'<top-linearlayout id="Sidebar_drop_adminProfile" class="position-Sidebar_drop_adminProfile linear-child-vertical" layout-width="97px" layout-height="auto" layout-horizontal-alignment="center" padding="" margin="" horizontal-alignment="center" vertical-alignment="middle" visible="visible" border-color="rgba(205,205,207,1.0) rgba(205,205,207,1.0) rgba(205,205,207,1.0) rgba(205,205,207,1.0)" border-width="1px 1px 1px 1px" horizontal-scroll="false" vertical-scroll="false" orientation="vertical" position="false" title="" layout-tab-disabled="false">' +
				'<div id="Sidebar_drop_adminProfile" class="top-linearlayout-root">' +
					'<top-button id="Sidebar_drop_adminProfile_pc" class="noti_drop linear-child-vertical" layout-width="match_parent" layout-height="28px" padding="0px 0px 1px 10px" horizontal-alignment="left" border-color="rgba(205,205,207,1.0) rgba(205,205,207,1.0) rgba(205,205,207,1.0) rgba(205,205,207,1.0)" border-width="0px 0px 1px 0px" tab-index="0" text="PC" text-color="rgba(51,51,51,1.0)" text-decoration="" icon-position="left" title="" layout-tab-disabled="false" actual-height="29">' +
						'<button id="Sidebar_drop_adminProfile_pc" class="top-button-root" type="button" tabindex="0">' +
							'<label class="top-button-text">PC</label>' +
						'</button>' +
					'</top-button>' +
//					'<top-button id="Sidebar_drop2_tdrive" class="noti_drop linear-child-vertical" layout-width="match_parent" layout-height="28px" padding="0px 0px 1px 10px" horizontal-alignment="left" tab-index="0" text="T-Drive" text-color="rgba(51,51,51,1.0)" text-decoration="" icon-position="left" title="" layout-tab-disabled="false" actual-height="29">' +
//						'<button id="Sidebar_drop2_tdrive" class="top-button-root" type="button" tabindex="0">' +
//							'<label class="top-button-text">T-Drive</label>' +
//						'</button>' +
//					'</top-button>' +
					'<top-button id="Sidebar_drop_adminProfile_delete" class="noti_drop linear-child-vertical" layout-width="match_parent" layout-height="28px" padding="0px 0px 1px 10px" horizontal-alignment="left" border-color="rgba(205,205,207,1.0) rgba(205,205,207,1.0) rgba(205,205,207,1.0) rgba(205,205,207,1.0)" border-width="0px 0px 1px 0px" tab-index="0" text="삭제" text-color="rgba(51,51,51,1.0)" text-decoration="" icon-position="left" title="" layout-tab-disabled="false" actual-height="29">' +
						'<button id="Sidebar_drop_adminProfile_delete" class="top-button-root" type="button" tabindex="0">' +
							'<label class="top-button-text">삭제</label>' +
						'</button>' +
					'</top-button>' +
				'</div>' +
				'</top-linearlayout>';
			
			if($('top-linearlayout#Sidebar_drop_adminProfile').length == 0) {

				more_list_adminProfile = $(more_list_adminProfile);
				more_list_adminProfile.find("button#Sidebar_drop_adminProfile_pc").on('click', Top.Controller.get('adminUserInfoModifyLogic').choosefile);	//pc
				//more_list_picture.find("button#Sidebar_drop2_tdrive").on('click', Top.Controller.get('userProfileLayoutLogic').choosefile);				//t-drive
				more_list_adminProfile.find("button#Sidebar_drop_adminProfile_delete").on('click', Top.Controller.get('adminUserInfoModifyLogic').delete_pic);		//삭제

				$('div#UserInfoModify_UserProfilePictureWrapper').append(more_list_adminProfile);
				//if(Top.Dom.selectById('sidebarLayout_tempo0_2').getProperties('visible') == 'visible'){
				$('.position-Sidebar_drop_adminProfile').css({'top':'57px','right':'25px'});
				//}	
			//else{
				//$('.position-Sidebar_drop2').css({'top':'56px','right':'59px'});
				//	}
				}
		}
		else{
				
				if($('top-linearlayout#Sidebar_drop_adminProfile').length != 0) {
			
				$('top-linearlayout#Sidebar_drop_adminProfile').remove();
			}
		};
	};
}

function encodeImage(src) {
    var canvas = document.createElement('canvas'),
        ctx = canvas.getContext('2d'),
        img = new Image();

    img.onload = function() {
        canvas.width = img.width;
        canvas.height = img.height;
        ctx.drawImage(img, 0, 0, img.width, img.height);
        //callback(canvas.toDataURL());
		console.log(canvas.toDataURL())
        enImage =  canvas.toDataURL();
		
    }
    img.src = src;
}
function deletePhoto_admin(data){
	Top.Dom.selectById("ImageButton_Admin_Profile_Setting").setSrc(userManager.getUserDefaultPhotoURL(Top.Controller.get("adminUserInfoModifyLogic").user_id))

}



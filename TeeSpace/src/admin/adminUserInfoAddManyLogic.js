
Top.Controller.create('adminUserInfoAddManyLogic', {
	imageNameArray : [],
	imageSrcArray : [],

	//XLSX 이름 -> data-model 멤버
	name_to_var : {
		
		'이름':'USER_NAME',
		'사번':'USER_EMPLOYEE_NUM',
		'직책':'USER_POSITION',
		'고용 형태':'USER_EMPLOYMENT_TYPE',
		'조직명':'USER_TEAM',
		'ID(회사 메일 계정)':'USER_LOGIN_ID',
		'비밀번호':'USER_PW',
		'회사전화':'USER_COM_NUM',
		'이동전화':'USER_PHONE',
		'담당 업무':'USER_TASK',
		'근무처':'USER_LOCATION',
		'생년월일':'USER_BIRTH',
		'입사일':'USER_EMPLOYMENT_DATE',
		'개인 이메일':'USER_EMAIL'
	},
	
	get_var_by_name : function(name){
		return this.name_to_var[name];
	},
	
	init : function(){

		orgUserRepo.reset();
		this.xlsx_file_obj = {
				
				filename : "",
		        contents : ""
		}
		
	},
	
	click_format_download : function(event, widget) {
		
		
	}, 
	
	click_format_register : function(event, widget) {
		
		
		let employee = [];
		let import_table = Top.Dom.selectById('adminUserInfoAddMany_userTable');
		let ctrl = this;
		
		var fileName_tv = Top.Dom.selectById('adminUserInfoAddMany_chart_filename');
		
		
		selectFormatFile(function(format, file_name, file_contents) {
			ctrl.xlsx_file_obj = {
					filename : file_name,
			        contents : file_contents
			}
			
			fileName_tv.setText(file_name);
			
			var result_cd;
			Top.Ajax.execute({
				url : _workspace.url + "Admin/ImportFileValidate?action=",
				type : 'POST',
				dataType : "json",
				async : false,
				cache : false,
				data : JSON.stringify({files:[ctrl.xlsx_file_obj]}), // arg
				contentType : "application/json",
				crossDomain : true,
				xhrFields : {
				    withCredentials: true
				},
				headers: {
					"ProObjectWebFileTransfer":"true"
				},
				success : function(data) {
					data = JSON.parse(data);
					
					result_cd = data.dto.RESULT_CD
					switch(result_cd) {
					case "RST0001":
						fileName_tv.setProperties({"border-color":"black"})
						Top.Dom.selectById('adminUserInfoAddMany_Icon').setProperties({"visible":"hidden"})
						Top.Dom.selectById('adminUserInfoAddMany_ModifyButton').setProperties({"disabled":"false"});
						break;
//					case "RST0010":
//						
//						break;
					}
				},		
				error : function(data){
					
				}
			});
			
			if(result_cd !== "RST0001") {
				fileName_tv.setProperties({"border-color":"red"})
				orgUserRepo.orgUserImportList = [];
				Top.Dom.selectById('adminUserInfoAddMany_table').update();
				fileName_tv.setProperties({"border-color":"red"})
				Top.Dom.selectById('adminUserInfoAddMany_Icon').setProperties({"visible":"visible"})
				Top.Dom.selectById('adminUserInfoAddMany_ModifyButton').setProperties({"disabled":"true"});
				return;
			}
			
			var range = XLSX.utils.decode_range(format.Sheets['직원']['!ref']).e;
			var tmp_key;
			var tmp_value;
			var tmp_obj;
			
			for(var row = 1; row <= range.r; row++){
				tmp_obj = {};
				
				for(var col = 0; col < range.c; col++){ //excluding picture
					
					
					tmp_key = ctrl.get_var_by_name(format.Sheets['직원'][XLSX.utils.encode_cell({r: 0, c: col})].v);
					tmp_value = format.Sheets['직원'][XLSX.utils.encode_cell({r: row, c: col})];
					tmp_obj[tmp_key] = tmp_value == undefined ? "" : tmp_value.v;
					if(format.Sheets['직원'][XLSX.utils.encode_cell({r: 0, c: col})].v == 'ID(회사 메일 계정)'){
						tmp_obj['USER_EMAIL'] = tmp_value == undefined ? "" : tmp_value.v;
					}
					
				}
				
				employee.push(tmp_obj);
				
			}
			orgUserRepo.reset();
			orgUserRepo.orgUserImportList = employee;
			orgUserRepo.update('orgUserImportList');
			console.log(employee);
			
			
			
			Top.Dom.selectById('adminUserInfoAddMany_table').update();
	    });
	}, 
	
	click_format_sample_download : function(event, widget) {
		
		getFormatFile('OrgUsersImportFormat.xlsx');
	},
	
	click_confirm :  function(event, widget){
		
		var inputDTO = {
			
			files : [this.xlsx_file_obj]
		   
		};
			
		Top.Ajax.execute({
			url : _workspace.url + "Admin/OrgUsersImport?action=",
			type : 'POST',
			dataType : "json",
			async : false,
			cache : false,
			data : JSON.stringify(inputDTO), // arg
			contentType : "application/json",
			crossDomain : true,
			xhrFields : {
			    withCredentials: true
			},
			headers: {
				"ProObjectWebFileTransfer":"true"
			},
			success : function(data) {
				let ctrl = Top.Controller.get('adminUserInfoAddManyLogic');

				if(ctrl.imageNameArray.length > 0) {
					var inputDTO = {	
						dto : {photoList : []}
					};
						
					for(var i = 0; i < ctrl.imageNameArray.length; i++) {
						var obj = {
								PROFILE_PHOTO_NAME : ctrl.imageNameArray[i], 
								PROFILE_PHOTO : ctrl.imageSrcArray[i]
						}
						inputDTO.dto.photoList.push(obj);
					}
						
					Top.Ajax.execute({
						url : _workspace.url + "Admin/OrgUserPhotoImport?action=",
						type : 'POST',
						dataType : "json",
						async : false,
						cache : false,
						data : JSON.stringify(inputDTO), // arg
						contentType : "application/json",
						crossDomain : true,
						xhrFields : {
						    withCredentials: true
						},
						headers: {
							"ProObjectWebFileTransfer":"true"
						},
						success : function(data) {
							TeeToast.open({
								text: '조직원 일괄 추가 성공',
								size: 'min'
							});
						},
						error : function(data){
							TeeToast.open({
								text: '사진 일괄 추가 실패',
								size: 'min'
							});
						}
					});
					
					ctrl.imageNameArray = [];
					ctrl.imageSrcArray = [];
				} else {				
					TeeToast.open({
						text: '조직원 일괄 추가 성공',
						size: 'min'
					});
				}
				Top.Dom.selectById('adminUserAddManyDialog').close();
				window.location.reload();
			},
			
			error : function(data){
				TeeToast.open({
					text: '조직원 일괄 추가 실패',
					size: 'min'
				});
			}
		});
	},
	
	click_cancel : function(event, widget){
		
		Top.Dom.selectById('adminUserAddManyDialog').close();
		
	}, click_photo_register : function(event, widget) {
		let fileChooser = Top.Device.FileChooser.create({
		    onBeforeLoad : function() {
		    	var profilePhoto_name = '';
		    	
		        for(var i=0; i<this.file.length; i++) {		  
		            let file = this.file[i];

    		        Top.Dom.selectById('admin_photo_errorIcon').setProperties({"visible":"none"})
    		        
    		        var nomalizedFileFullName = Top.Util.Browser.isIE() ? file.name : file.name.normalize('NFC');    
    		        
    		        let fileNameArray = nomalizedFileFullName.split('.');
    		        let fileExt = fileNameArray.length == 1 ? "" : fileNameArray[fileNameArray.length - 1];        
    		        if(!(fileExt == "jpeg" || fileExt == "png" || fileExt == "jpg" || fileExt == "JPEG" || fileExt == "PNG" || fileExt == "JPG")) {
    		        	Top.Dom.selectById("admin_photo_errorIcon").setProperties({"visible":"visible"}); 
		        		return false;
    		        }
    		        
    		        Top.Controller.get('adminUserInfoAddManyLogic').imageNameArray.push(nomalizedFileFullName);
					
    		        if(i < this.file.length) {
    		        	if(i < 11)
    		        		profilePhoto_name += nomalizedFileFullName
    		        	if(i != this.file.length - 1)
    		        		profilePhoto_name += ", ";
    		        }

		        }
		        
		        Top.Dom.selectById('admin_photo_name_tv').setText(profilePhoto_name);
	        },
			onFileChoose : function(fileChooser) {
				for(var i = 0; i < fileChooser.length; i++) {
					Top.Controller.get('adminUserInfoAddManyLogic').imageSrcArray.push(fileChooser[i].src)
				}
			},
		    charset: "euc-kr",
		    multiple: true
	    });
		fileChooser.show();

	}
	
	
	
});

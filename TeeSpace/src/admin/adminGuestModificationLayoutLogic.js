Top.Controller.create('adminGuestModificationLayoutLogic', {
	init : function(event, widget) {
		
		$.ajax({
			url : _workspace.url + "Admin/OrgGuest", 
            type: 'GET',
            dataType: 'json',
            crossDomain: true,
            contentType: 'application/json; charset=utf-8',
            xhrFields: {
                withCredentials: true
              },
            data: JSON.stringify({
              "dto": {
                "USER_ID": Top.Dom.selectById('guestGroupTableView').getClickedData().USER_ID
              }
            }),
            success: function(result) {//생일, 이메일, 번호 해야함
            	Top.Dom.selectById('GuestName').setText(result.dto.userInfo.USER_NAME);
            	
            	Top.Dom.selectById('GuestCompansy').setText(result.dto.guestComProfileList[0].COMPANY_NAME);
            	
            	Top.Dom.selectById('GuestDepartment').setText(result.dto.guestComProfileList[0].DEPARTMENT_NAME);
            	
            	Top.Dom.selectById('GuestPosition').setText(result.dto.guestComProfileList[0].USER_JOB);
            	
            	Top.Dom.selectById('Gid').setText(result.dto.userInfo.USER_LOGIN_ID.split('@')[0]);
            	
            	if(result.dto.userInfo.THUMB_PHOTO == null){
	              Top.Dom.selectById('ImageButton_Admin_Profile_Setting_addm').setSrc(userManager.getUserDefaultPhotoURL(userManager.getLoginUserId()));
	          	} else {
	      		  Top.Dom.selectById("ImageButton_Admin_Profile_Setting_addm").setSrc(result.dto.userInfo.THUMB_PHOTO)		
	      	 	} 
            	
            
            		
            	if(result.dto.guestComProfileList[0].USER_COM_NUM != null)
            		Top.Dom.selectById('UserInfoAdd_UserCompanyPhoneNumStarTvm').setText(result.dto.guestComProfileList[0].USER_COM_NUM);
          	
            	if(result.dto.userInfo.USER_PHONE != null)
            		Top.Dom.selectById('UserInfoAdd_UserCompanyPhoneNumStarTvmp').setText(result.dto.userInfo.USER_PHONE);


            	if(result.dto.guestComProfileList[0].FAX_NUM != null)
            		Top.Dom.selectById('UserInfoAdd_UserFAXNumContent2mf').setText(result.dto.guestComProfileList[0].FAX_NUM);
            	            	
            	Top.Dom.selectById('UserInfoAdd_UserOccupationStarTvm').setText(result.dto.userInfo.USER_ADDRESS);
            	
            	if(result.dto.userInfo.USER_EMAIL != null)
	            	Top.Dom.selectById('UserInfoAdd_UserCellPhoneNumStarTvm').setText(result.dto.userInfo.USER_EMAIL);
            	
            	if(result.dto.userInfo.USER_BIRTH != null)
            		Top.Dom.selectById('UserInfoAdd_UserOccupationStarTvmb').setText(result.dto.userInfo.USER_BIRTH);

            	result.dto.guestComProfileList.map(elem=>elem.ORG_PATH).forEach((elem,idx) =>{
            		var ll = Top.Widget.create('top-linearlayout');
            		ll.setProperties({
            			id: 'orgLayout_'+idx,
            			'layout-width': 'match_parent',
            			'layout-height': 'wrap_content',
            			'orientation':'horizontal',
            			'border-width': '0 0 0 0',
            		});
            		var t1 = Top.Widget.create('top-textview');
            		t1.setProperties({
            			id: 'orgText_'+idx,
            			'layout-width': '90px',
            			'layout-height': 'wrap_content',	
            			'className': 'admin-org-t1',
            			'text':'소속 ' + (idx+1).toString(),
            		});
            		ll.addWidget(t1);
            		var t2 = Top.Widget.create('top-textview');
            		t2.setProperties({
            			id: 'orgText_'+idx,
            			'layout-width': 'match_parent',
            			'layout-height': 'wrap_content',
            			'className': 'admin-org-t2',
            			'text': elem,
            		});
            		ll.addWidget(t2);
            		ll.complete();
            		
            		$$$('UserInfoAdd_UserOfficeLocationWrapperm').addWidget(ll);
            	});
            	$$$('UserInfoAdd_UserOfficeLocationWrapperm').complete(); 
            	
            	orgGuestRepo.selectedGuest = result.dto;
            	
            	},
	        error: function(error) {        	
	        	
	        }
		
           });	
	
		
		this.cc = '1';
    	this.aa = 1;
    	this.bb = 0;
		//this.org_path_array = [];
		//this.init_org_path();
		
		$('div#adminGuestModificationLayout').on('click', function(e){
			//프로필 사진  > 더보기 메뉴
			if($('div#adminGuestModificationLayout').css('display') == 'inline-block'){
			
				adminProfile_list_add(e, "ImageButton_Admin_Profile_Setting_add");		
			}
		});
		$.ajax({
			url : _workspace.url + "Admin/CompanyInfo", 
            type: 'GET',
            dataType: 'json',
            crossDomain: true,
            contentType: 'application/json; charset=utf-8',
            xhrFields: {
                withCredentials: true
              },
            data: JSON.stringify({
              "dto": {
                "ADMIN_ID": userManager.getLoginUserId()
              }
            }),
            success: function(result) {
	        },
	        error: function(error) {        	
	        	
	        }
			});	
		
		
		
		
		//이벤트 선언
//		Top.Dom.selectById('UserInfoAdd_UserIDContentm').setProperties({'on-keyup':'add_id_check'});
//		Top.Dom.selectById('UserInfoAdd_UserPasswordContent').setProperties({'on-keyup':'add_pw_check'});
//		Top.Dom.selectById("UserInfoAdd_addIcon").setProperties({'on-click':'addPathIcon'})
//		$('div#UserInfoAdd_UserOccupationHeader #UserInfoAdd_addIcon').css({"cursor":"pointer"})
//		Top.Dom.selectById("Button_id_checkm").setProperties({"on-click":"idExist"})
		
		//소속/부서 채우기
		
//		$.ajax({
//			url : _workspace.url + "Admin/OrgPathGet?action=", 
//            type: 'POST',
//            dataType: 'json',
//            crossDomain: true,
//            contentType: 'application/json; charset=utf-8',
//            xhrFields: {
//                withCredentials: true
//              },
//            data: JSON.stringify({
//              "dto": {
//                "COMPANY_CODE": Top.Dom.selectById('guestGroupTreeView').getSelectedNode().id.split('_')[0],
//                "DEPARTMENT_CODE": Top.Dom.selectById('guestGroupTreeView').getSelectedNode().id.split('_')[1],
//                "ORG_URL": adminGetTeamObjByName(Top.Dom.selectById("guestGroupTreeView").getSelectedNode().text).ORG_URL,
//                "ORG_TYPE": adminGetTeamObjByName(Top.Dom.selectById("guestGroupTreeView").getSelectedNode().text).ORG_TYPE
//              }
//            }),
//            success: function(result) {
//            	Top.Dom.selectById("TextView_affiliation_path_0").setText(result.dto.ORG_NAME)
//            	$('#TextView_affiliation_path_0').attr('CODE', Top.Dom.selectById('guestGroupTreeView').getSelectedNode().id);
//	        },
//	        error: function(error) {        	
//	        	
//	        }
//			});	
		
		
		
		
	}, openMGuestDetailInfo : function(event, widget)  {
		var adminAddOneGuestDialog = Top.Dom.selectById("adminAddOneGuestDialog"); 
        adminAddOneGuestDialog.setProperties({ title : "게스트 정보 수정" });
        adminAddOneGuestDialog.setProperties({ dialogLayout : "adminAddOneGuestLayout"});
        adminAddOneGuestDialog.close(true);
        adminAddOneGuestDialog.open();
        
		
	}, closeGuestDialog : function(event, widget) {
		 var adminAddOneGuestDialog = Top.Dom.selectById("adminAddOneGuestDialog"); 
		 adminAddOneGuestDialog.close(true);
	}
	
	,
	
});





	


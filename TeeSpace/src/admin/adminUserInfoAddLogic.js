
Top.Controller.create('adminUserInfoAddLogic', {
	
	
	init : function(event, widget) {
		const data = COUNTRY_CODES.map(value => {
			return {
				text: `${value.code} ${value.name}`,
				value: value.code
			}
		});
		Top.Dom.selectById('UserInfoAdd_UserCompanyContent').setText(orgTreeRepo.orgTree[0].children[0].text);
		Top.Dom.selectById('SelectBox212').setProperties({"nodes":data , "value":"+82"});
		$('#UserInfoAdd_UserCompanyPhoneNumContent0').focusout(function(){
			var tmptext = Top.Dom.selectById('UserInfoAdd_UserCompanyPhoneNumContent0').getText()
			if(!pattern_num.test(Number(tmptext))){
				Top.Dom.selectById('Icon22').setProperties({"visible":"visible"})
			}
			else {
				Top.Dom.selectById('UserInfoAdd_UserCompanyPhoneNumContent0').setProperties({"border-color":"#dddddd "})
				Top.Dom.selectById('Icon22').setProperties({"visible":"hidden"})
			}
		})
		$('#UserInfoAdd_UserCellPhoneNumContent0').focusout(function(){
			var tmptext = Top.Dom.selectById('UserInfoAdd_UserCellPhoneNumContent0').getText()
			if(!pattern_num.test(Number(tmptext))){
				Top.Dom.selectById('Icon59').setProperties({"visible":"visible"})
			}
			else {
				Top.Dom.selectById('UserInfoAdd_UserCellPhoneNumContent0').setProperties({"border-color":"#dddddd "})
				Top.Dom.selectById('Icon59').setProperties({"visible":"hidden"})
			}
		})
		this.cc = '1';
    	this.aa = 1;
    	this.bb = 0;
    	this.ee = [];
    	this.mcn1 = {};
    	this.mcn2 = {};
    	this.mcn3 = {};
    	this.user_job_list = [];
		this.user_position_list = [];
		this.user_emptype_list = [];
		//생년월일,입사일 DatePicker 설정
		Top.Dom.selectById('adminBirthDatePicker').setProperties({"year-range":"-100:+0"})
		Top.Dom.selectById('adminJoinDatePicker').setProperties({"year-range":"-100:+0"})
		
//		var emp_type_selectbox = Top.Dom.selectById('UserInfoAdd_UserEmploymentTypeContent');
		var job_selectbox = Top.Dom.selectById('UserInfoAdd_UserJobContent');
		var position_selectbox = Top.Dom.selectById('UserInfoAdd_OrgJobContent');
		var emptype_selectbox = Top.Dom.selectById('UserInfoAdd_UserEmploymentTypeContent');
		var metaGetInput = '';
		if($('#top-dialog-root_adminUserInfoViewDialog .top-dialog-title').text() === '조직원 정보') {
			if(orgUserRepo.selectedUser.companyProfileList != null)
				metaGetInput = orgUserRepo.selectedUser.companyProfileList[0].USER_AFF.split('_')[0];
		}
		else
			metaGetInput = Top.Dom.selectById('TreeView1305').getSelectedNode().id.split('_')[0];
		
		Top.Ajax.execute({
			url : _workspace.url + "Admin/CompanyProfileMetaList",
			type : 'GET',
			dataType : "json",
			async : false,
			cache : false,
			contentType : "application/json",
			crossDomain : true,
			xhrFields : {
			    withCredentials: true
			},
			headers: {
				"ProObjectWebFileTransfer":"true"
			},
			success : function(data) {
				var tmparr = JSON.parse(data).dto.comProfileMetaList;
				for(var i = 0 ; i < tmparr.length; i++){
					if(tmparr[i].META_TYPE ==='ADM0001') {
						Top.Controller.get('adminUserInfoAddLogic').user_job_list.push(tmparr[i].META_VALUE);
						Top.Controller.get('adminUserInfoAddLogic').mcn1[tmparr[i].META_VALUE] = tmparr[i].META_CODE;
					}
					else if (tmparr[i].META_TYPE === 'ADM0002'){
						Top.Controller.get('adminUserInfoAddLogic').user_position_list.push(tmparr[i].META_VALUE);
						Top.Controller.get('adminUserInfoAddLogic').mcn2[tmparr[i].META_VALUE] = tmparr[i].META_CODE;
					}
					else if (tmparr[i].META_TYPE === 'ADM0003'){
						Top.Controller.get('adminUserInfoAddLogic').user_emptype_list.push(tmparr[i].META_VALUE);
						Top.Controller.get('adminUserInfoAddLogic').mcn3[tmparr[i].META_VALUE] = tmparr[i].META_CODE;
					}
				}//UserInfoAdd_UserEmploymentTypeContent
				job_selectbox.setProperties({"nodes": Top.Controller.get('adminUserInfoAddLogic').user_job_list});
				position_selectbox.setProperties({"nodes": Top.Controller.get('adminUserInfoAddLogic').user_position_list});
				emptype_selectbox.setProperties({"nodes":Top.Controller.get('adminUserInfoAddLogic').user_emptype_list});
			}
		})


		//this.org_path_array = [];
		//this.init_org_path();
    	if($('#top-dialog-root_adminUserInfoViewDialog .top-dialog-title').text() === '조직원 정보'){
    		Top.Controller.get('adminUserInfoAddLogic').init_contents(Top.Controller.get('adminUserInfoViewLogic').user_id) 
    		}  
		
		$('div#adminUserInfoAdd').on('click', function(e){
			//프로필 사진  > 더보기 메뉴
			if($('div#adminUserInfoAdd').css('display') == 'inline-block'){
			
				adminProfile_list_add(e, "ImageButton_Admin_Profile_Setting_add");		
			}
		});
		$.ajax({
			url : _workspace.url + "Admin/CompanyInfo", 
            type: 'GET',
            dataType: 'json',
            crossDomain: true,
            contentType: 'application/json; charset=utf-8',
            xhrFields: {
                withCredentials: true
              },
            data: JSON.stringify({
              "dto": {
                "ADMIN_ID": userManager.getLoginUserId()
              }
            }),
            success: function(result) {
//            	Top.Dom.selectById("UserInfoAdd_UserCompanyContent").setText(result.dto.COMPANY_NAME)
	        },
	        error: function(error) {        	
	        	
	        }
			});	
		
		//다이얼로그 강제로 x 버튼 누를 시 트리뷰 초기화
//		$('.top-dialog-close').click(function() { 
//          	Top.Controller.get('adminOrgUserLayoutLogic').clickNode(MouseEvent,Top.Dom.selectById('TreeView1305'))
//		});
		
		
		
		//이벤트 선언
		Top.Dom.selectById('UserInfoAdd_UserIDContent').setProperties({'on-keyup':'add_id_check'});
		Top.Dom.selectById('UserInfoAdd_UserPasswordContent').setProperties({'on-keyup':'add_pw_check'});
		Top.Dom.selectById("UserInfoAdd_addIcon").setProperties({'on-click':'addPathIcon'})
		$('div#UserInfoAdd_UserOccupationHeader #UserInfoAdd_addIcon').css({"cursor":"pointer"})
		// Top.Dom.selectById("UserInfoAdd_changeBtn").setProperties({"on-click":"changeBtn"})
		Top.Dom.selectById("TextView_affiliation_path_0").setProperties({"on-click":"changeBtn"})
		Top.Dom.selectById("Button_id_check").setProperties({"on-click":"idExist"})
		
		//소속/부서 채우기
		if($('#top-dialog-root_adminUserAddDialog .top-dialog-title').text() === '조직원 개별 추가'){ 
		$.ajax({
			url : _workspace.url + "Admin/OrgPathGet?action=", 
            type: 'POST',
            dataType: 'json',
            crossDomain: true,
            contentType: 'application/json; charset=utf-8',
            xhrFields: {
                withCredentials: true
              },
            data: JSON.stringify({
              "dto": {
                "COMPANY_CODE": Top.Dom.selectById("TreeView1305").getSelectedNode().id.split('_')[0],
                "DEPARTMENT_CODE": Top.Dom.selectById("TreeView1305").getSelectedNode().id.split('_')[1],
                "ORG_URL": adminGetTeamObjByName(Top.Dom.selectById("TreeView1305").getSelectedNode().text).ORG_URL,
                "ORG_TYPE": adminGetTeamObjByName(Top.Dom.selectById("TreeView1305").getSelectedNode().text).ORG_TYPE
              }
            }),
            success: function(result) {
          
            		
            		Top.Dom.selectById("TextView_affiliation_path_0").setText(result.dto.ORG_NAME)
            		$('#TextView_affiliation_path_0').attr('CODE', Top.Dom.selectById("TreeView1305").getSelectedNode().id);
            	
            	
            	
	        },
	        error: function(error) {        	
	        	
	        }
			});	 

		}
		
		
		
		
	},
	//상세-> 수정 클릭 시, user_id를 이용한 함수
	////////////////////////////////////////////////////
	
	init_contents : function(user_id){
		
		this.user_id = user_id;
//		this.employment_type_list = [];
		this.user_job_list = [];
		this.user_position_list = [];
		this.user_emptype_list = [];
		
		var inputDTO = {
				"dto": {
					"USER_ID": user_id
				}
		};		
		
		var job_selectbox = Top.Dom.selectById('UserInfoAdd_UserJobContent');
		var position_selectbox = Top.Dom.selectById('UserInfoAdd_OrgJobContent');
		var emptype_selectbox = Top.Dom.selectById('UserInfoAdd_UserEmploymentTypeContent');
		
/*		
		Top.Ajax.execute({
			url : _workspace.url + "Admin/OrgUser?action=",
			type : 'GET',
			dataType : "json",
			async : false,
			cache : false,
			data : JSON.stringify(inputDTO), // arg
			contentType : "application/json",
			crossDomain : true,
			xhrFields : {
			    withCredentials: true
			},
			headers: {
				"ProObjectWebFileTransfer":"true"
			},
			success : function(data) {
				Top.Controller.get('adminUserInfoAddLogic').ee = JSON.parse(data).dto.companyProfileList

				
				
				//안보일 레이아웃 제거
				Top.Dom.selectById("UserInfoAdd_UserIDContent").setProperties({"visible":"none"})
				Top.Dom.selectById("UserInfoAdd_UserIDInfoErrorIcon").setProperties({"visible":"none"})
				Top.Dom.selectById("Button_id_check").setProperties({"visible":"none"})
				Top.Dom.selectById("LinearLayout_id_add").setProperties({"visible":"hidden"})
				Top.Dom.selectById("TextView_affiliation_path_0").setProperties({"visible":"none"})
				Top.Dom.selectById("LinearLayoutPath_0").setProperties({"visible":"none"})
				Top.Dom.selectById("LinearLayout_companyinfo_0").setProperties({"visible":"none"})
				Top.Dom.selectById("UserInfoAdd_ModifyButton").setProperties({"visible":"none"})
				Top.Dom.selectById("UserInfoAdd_CancelButton").setProperties({"visible":"none"})
				Top.Dom.selectById("UserInfoModify_ModifyButton").setProperties({"visible":"visible"})
				Top.Dom.selectById("UserInfoModify_CancelButton").setProperties({"visible":"visible"})
				
				//이벤트선언
				Top.Dom.selectById("UserInfoModify_ModifyButton").setProperties({"on-click":"modifyOk"})
				Top.Dom.selectById("UserInfoModify_CancelButton").setProperties({"on-click":"cancelOk"})
				
				var outputdata;
				data = JSON.parse(data);
				if(data.dto == undefined) {
				    outputdata = {
				        result : "Success",
				        message : data.exception.name + ' ' + data.exception.message,
				        data : data
				    }
				} else {
				    outputdata = data.dto;
				    outputdata.files = data.files;
				}
				
				console.log(outputdata);
				
				
				
				var user_info = outputdata.userInfo;
				Top.Dom.selectById('UserInfoAdd_UserFirstNameContent').setText(user_info.USER_NAME);
				Top.Dom.selectById('UserInfoModify_UserIDContent').setText(user_info.USER_LOGIN_ID.split('@')[0]);
				if(user_info.USER_EMAIL){
					Top.Dom.selectById('UserInfoAdd_UserEmailIDContent').setText(user_info.USER_EMAIL.split('@')[0]);
					Top.Dom.selectById('UserInfoAdd_UserEmailDomainContent').setText(user_info.USER_EMAIL.split('@')[1]);
				}
				if(user_info.USER_PHONE){
					Top.Dom.selectById('UserInfoAdd_UserCellPhoneNumContent0').setText(user_info.USER_PHONE.split('-')[0]);
					Top.Dom.selectById('UserInfoAdd_UserCellPhoneNumContent1').setText(user_info.USER_PHONE.split('-')[1]);
					Top.Dom.selectById('UserInfoAdd_UserCellPhoneNumContent2').setText(user_info.USER_PHONE.split('-')[2]);
				}
				if(user_info.USER_BIRTH){
					Top.Dom.selectById('adminBirthDatePicker').setDate(user_info.USER_BIRTH);
				}else{
					Top.Dom.selectById('adminBirthDatePicker').setDate();
				}
				//프로필 가져오기
				   if(user_info.THUMB_PHOTO == null){
                    Top.Dom.selectById('ImageButton_Admin_Profile_Setting_add').setSrc(userManager.getUserDefaultPhotoURL(user_id));
		        	 }else{
		        		 Top.Dom.selectById("ImageButton_Admin_Profile_Setting_add").setSrc(user_info.THUMB_PHOTO)		
		        	 }
				
				
				   
				//companyProfileList 겸임인경우
				for(let count = 0; count< outputdata.companyProfileList.length; count++){				
//		            if(outputdata.companyProfileList[count].USER_AFF === orgUserRepo.orgUserList[Top.Dom.selectById("OrgUserTableView").getClickedIndex()[0]].org_name ){
		 				if(outputdata.companyProfileList[count].USER_COM_NUM){
		 					Top.Dom.selectById('UserInfoAdd_UserCompanyPhoneNumContent0').setText(outputdata.companyProfileList[count].USER_COM_NUM.split('-')[0]);
		 					Top.Dom.selectById('UserInfoAdd_UserCompanyPhoneNumContent1').setText(outputdata.companyProfileList[count].USER_COM_NUM.split('-')[1]);
		 					Top.Dom.selectById('UserInfoAdd_UserCompanyPhoneNumContent2').setText(outputdata.companyProfileList[count].USER_COM_NUM.split('-')[2]);
		 				}
		 				Top.Dom.selectById('UserInfoAdd_UserInChargeOfContent').setText(outputdata.companyProfileList[count].USER_TASK);
		 				Top.Dom.selectById('UserInfoAdd_UserOfficeLocationContent').setText(outputdata.companyProfileList[count].USER_LOCATION);
		 				if(outputdata.companyProfileList[count].USER_EMPLOYMENT_DATE){
		 					Top.Dom.selectById('adminJoinDatePicker').setDate(outputdata.companyProfileList[count].USER_EMPLOYMENT_DATE);
		 				}
		 				//html로 생성되야할 부분
		 				let html_3 = '<top-linearlayout id="LinearLayoutPathModify_'+count+'" layout-width="wrap_content" layout-height="30px" layout-horizontal-alignment="left" border-width="0px" orientation="horizontal" position="false" title="" layout-tab-disabled="false" vertical-alignment="top" class="linear-child-vertical" style="text-align: left;" actual-height="30">'+ '<div id="LinearLayoutPathModify_'+count+'" class="top-linearlayout-root" style="white-space: nowrap; height: 30px; border-width: 0px; border-style: solid; vertical-align: top; overflow: auto hidden; width: 100%; line-height: 30px;">'
						+'<top-textview id="TextView_affiliation_'+count+'" layout-width="auto" layout-height="auto" layout-vertical-alignment="middle" border-width="0px" text="소속 '+(count+1)+'" text-decoration="" title="" layout-tab-disabled="false" class="linear-child-horizontal" style="line-height: normal;" actual-width="38.1094">'
					+'<span id="TextView_affiliation_Modify_'+count+'" class="top-textview-root" style="width: auto; height: auto; border-width: 0px; border-style: solid; vertical-align: middle;"><a class="top-textview-url">소속 '+ (count+1) +'</a><div class="top-textview-aligner middle"></div></span>'
				+'</top-textview>'
				+'<top-textview id="TextView_affiliation_path_Modify_'+count+'" layout-width="auto" layout-height="auto" layout-vertical-alignment="middle" margin="0px 0px 0px 10px" border-width="0px" text="'+outputdata.companyProfileList[count].ORG_PATH+'" text-decoration="" title="" layout-tab-disabled="false" class="linear-child-horizontal" style="line-height: normal;" actual-width="16">'
				+	'<span id="TextView_affiliation_path_Modify_'+count+'" class="top-textview-root" style="width: auto; height: auto; margin: 0px 0px 0px 10px; border-width: 0px; border-style: solid; vertical-align: middle;"><a class="top-textview-url"></a><div class="top-textview-aligner middle"></div></span>'
				+'</top-textview>'
				+		'</div></top-linearlayout>';
	           	
	           	let html_4 ='<top-linearlayout id="LinearLayout_companyinfo_Modify_'+count+'" layout-width="match_parent" layout-height="30px" layout-horizontal-alignment="left" border-width="0px" horizontal-scroll="false" vertical-scroll="false" orientation="horizontal" position="false" title="" layout-tab-disabled="false" vertical-alignment="top" class="linear-child-vertical" style="text-align: left;" actual-height="30">' 
	           		+'<div id="LinearLayout_companyinfo_Modify_'+count+'" class="top-linearlayout-root" style="white-space: nowrap; height: 30px; border-width: 0px; border-style: solid; vertical-align: top; overflow: hidden; width: 100%; line-height: 30px;">'
					+'<top-button id="UserInfoModify_changeBtn_'+count+'" class="text-solid linear-child-horizontal" on-click = "changeBtn"  layout-width="70px" layout-height="27px" padding="0px 0px 0px 0px" tab-index="0" text="변경" text-decoration="" icon-position="left" title="" layout-tab-disabled="false" layout-vertical-alignment="top" style="line-height: normal;" actual-width="70">'
					+'<button id="UserInfoModify_changeBtn_'+count+'" class="top-button-root" type="button" style="width: 70px; height: 27px; padding: 0px; vertical-align: top;" tabindex="0"><label class="top-button-text">변경</label></button>'
				+'</top-button>'
				+'<top-textfield id="UserInfoModify_UserCompanyIDContent_'+count+'" layout-width="130px" layout-height="27px" margin="0px 0px 0px 10px" tab-index="0" text="" text-decoration="" hint="사번" prevent-key="false" password="false" required="false" icon-position="right" auto-complete-align="bl" title="" layout-tab-disabled="false" class="in_textfield click-placeholder linear-child-horizontal" layout-vertical-alignment="top" style="line-height: normal;" actual-width="140">'
					+'<div class="top-textfield-root" style="display: inline-block; width: 130px; height: 27px; margin: 0px 0px 0px 10px; vertical-align: top;">'
						+'<form autocomplete="off" onsubmit="return false;">'
							+'<input id="UserInfoModify_UserCompanyIDContent_'+count+'" class="top-textfield-text pl-0 pr-0" type="text" value="" placeholder="사번" tabindex="0">'
							+'<span class="top-textfield-icon" style="display: none; vertical-align: baseline;"></span>'
						+'</form>'
					+'</div>'
				+'</top-textfield>'
				+'<top-icon id="UserInfoModify_UseOrgnumErrorIcon_'+count+'" class="icon-work_noti_info linear-child-horizontal" layout-width="16px" layout-height="16px" margin="2px 4px 0px 4px" visible="hidden" border-width="0px" text-size="16px" text-color="#FF0000" title="" layout-tab-disabled="false" layout-vertical-alignment="top" style="line-height: normal;" actual-width="24"><span class="top-icon-root" id="UserInfoModify_UseOrgnumErrorIcon_'+count+'" style="visibility: hidden; width: 16px; height: 16px; margin: 2px 4px 0px; color: rgb(255, 0, 0); font-size: 16px; border-width: 0px; border-style: solid; vertical-align: top;"><i class="top-icon-content" style="line-height: 16px;">'
				+'</i></span></top-icon>'
				+'<top-selectbox id="UserInfoModify_UserEmploymentTypeContent_'+count+'" layout-width="100px" layout-height="27px" title ="고용 형태" layout-vertical-alignment="top" border-width="0px" horizontal-scroll="false" vertical-scroll="false" items="" separator-visible="true" virtual-scroll="false" item-clicktype="click" tab-index="0" title="" multi-select="false" searchable="false" layout-tab-disabled="false" class="linear-child-horizontal" style="line-height: normal;" actual-width="100">'
					+'<div id="UserInfoModify_UserEmploymentTypeContent_'+count+'" class="top-selectbox-root" tabindex="0" style="width: 100px; height: 27px; border-width: 0px; border-style: solid; vertical-align: top; overflow: hidden;"><top-widgetitem text="정규직"></top-widgetitem><top-widgetitem text="계약직"></top-widgetitem><top-widgetitem text="아르바이트"></top-widgetitem><top-widgetitem text="인턴"></top-widgetitem></div>'
			+'</top-selectbox>'
				+'<top-selectbox id="UserInfoModify_UserJobContent_'+count+'" layout-width="100px" layout-height="27px" title ="직위" layout-vertical-alignment="top" margin="0px 0px 0px 4px" border-width="0px" horizontal-scroll="false" vertical-scroll="false" items="" separator-visible="true" virtual-scroll="false" item-clicktype="click" tab-index="0" title="" multi-select="false" searchable="false" layout-tab-disabled="false" class="linear-child-horizontal" style="line-height: normal;" actual-width="104">'
					+'<div id="UserInfoModify_UserJobContent_'+count+'" class="top-selectbox-root" tabindex="0" style="width: 100px; height: 27px; margin: 0px 0px 0px 4px; border-width: 0px; border-style: solid; vertical-align: top; overflow: hidden;"></div>'
			+'</top-selectbox>'
				+'<top-selectbox id="UserInfoModify_OrgJobContent_'+count+'" layout-width="100px" layout-height="27px" title ="직책" layout-vertical-alignment="top" margin="0px 0px 0px 4px" border-width="0px" horizontal-scroll="false" vertical-scroll="false" items="" separator-visible="true" virtual-scroll="false" item-clicktype="click" tab-index="0" title="" multi-select="false" searchable="false" layout-tab-disabled="false" class="linear-child-horizontal" style="line-height: normal;" actual-width="104">'
					+'<div id="UserInfoModify_OrgJobContent_'+count+'" class="top-selectbox-root" tabindex="0" style="width: 100px; height: 27px; margin: 0px 0px 0px 4px; border-width: 0px; border-style: solid; vertical-align: top; overflow: hidden;"></div>'
			+'</top-selectbox></div></top-linearlayout>';

	           		
	           	///////////////////////////////////////////////
	           	Top.Dom.selectById("UserInfoAddPathLayout_left").append(html_3) 
	           	Top.Dom.selectById("UserInfoAddPathLayout_right").append(html_4)
	       		Top.Dom.selectById("UserInfoAddPathLayout_left").complete();
	           	Top.Dom.selectById("UserInfoAddPathLayout_right").complete();

	           	setTimeout(function() {
	           		var additional_job_selectbox = Top.Dom.selectById("UserInfoModify_UserJobContent_" + count);
	            	var additional_position_selectbox = Top.Dom.selectById("UserInfoModify_OrgJobContent_" + count);
	            	additional_job_selectbox.setProperties({"nodes": Top.Controller.get('adminUserInfoAddLogic').user_job_list});
	            	additional_position_selectbox.setProperties({"nodes": Top.Controller.get('adminUserInfoAddLogic').user_position_list}); 
	           	}, 0)
            		
		 				
		 				Top.Dom.selectById('UserInfoModify_UserCompanyIDContent_'+count).setText(outputdata.companyProfileList[count].USER_EMPLOYEE_NUM);	
		 				Top.Dom.selectById("UserInfoModify_OrgJobContent_"+count).select(outputdata.companyProfileList[count].USER_POSITION)
		 			    Top.Dom.selectById('UserInfoModify_UserEmploymentTypeContent_'+count).select(adminCodeToText(outputdata.companyProfileList[count].USER_EMPLOYMENT_TYPE));
		 				Top.Dom.selectById("UserInfoModify_UserJobContent_"+count).select(outputdata.companyProfileList[count].USER_JOB)
		 				Top.Dom.selectById('TextView_affiliation_path_Modify_'+count).setText(outputdata.companyProfileList[count].ORG_PATH);
		 				
		          //  }
				}
	
				
					//소속회사
					$.ajax({
						url : _workspace.url + "Admin/CompanyInfo", 
			            type: 'GET',
			            dataType: 'json',
			            crossDomain: true,
			            contentType: 'application/json; charset=utf-8',
			            xhrFields: {
			                withCredentials: true
			              },
			            data: JSON.stringify({
			              "dto": {
			                "ADMIN_ID": userManager.getLoginUserId()
			              }
			            }),
			            success: function(result) {
			            	Top.Dom.selectById('UserInfoAdd_UserCompanyContent').setText(result.dto.COMPANY_NAME)
				        },
				        error: function(error) {        	
				        	
				        }
						});	
					
			},
			error : function(data) {
				var outputdata;
	            outputdata = {
	                result : "Fail",
	                message : data.exception != undefined ? data.exception.message : "Error",
	                data : data
	            }		
	            console.info("fail");
	            console.info(outputdata);
			}
		});
*/		
		
		if(orgUserRepo.selectedUser.companyProfileList != null) {
			Top.Controller.get('adminUserInfoAddLogic').ee = orgUserRepo.selectedUser.companyProfileList
		}
				
		Top.Ajax.execute({
			url : _workspace.url + "Admin/CompanyProfileMetaList",
			type : 'GET',
			dataType : "json",
			async : false,
			cache : false,
			contentType : "application/json",
			crossDomain : true,
			xhrFields : {
			    withCredentials: true
			},
			headers: {
				"ProObjectWebFileTransfer":"true"
			},
			success : function(data) {
				var tmparr = JSON.parse(data).dto.comProfileMetaList;
				for(var i = 0 ; i < tmparr.length; i++){
					if(tmparr[i].META_TYPE ==='ADM0001') {
						Top.Controller.get('adminUserInfoAddLogic').user_job_list.push(tmparr[i].META_VALUE);
					}
					else if (tmparr[i].META_TYPE === 'ADM0002'){
						Top.Controller.get('adminUserInfoAddLogic').user_position_list.push(tmparr[i].META_VALUE);
					}
					else if (tmparr[i].META_TYPE === 'ADM0003'){
						Top.Controller.get('adminUserInfoAddLogic').user_emptype_list.push(tmparr[i].META_VALUE);
						
					}
				}//UserInfoAdd_UserEmploymentTypeContent
				job_selectbox.setProperties({"nodes": Top.Controller.get('adminUserInfoAddLogic').user_job_list});
				position_selectbox.setProperties({"nodes": Top.Controller.get('adminUserInfoAddLogic').user_position_list});
				emptype_selectbox.setProperties({"nodes":Top.Controller.get('adminUserInfoAddLogic').user_emptype_list});
			}
		})
		
		//안보일 레이아웃 제거
		Top.Dom.selectById("UserInfoAdd_UserIDContent").setProperties({"visible":"none"})
		Top.Dom.selectById("UserInfoAdd_UserIDInfoErrorIcon").setProperties({"visible":"none"})
		Top.Dom.selectById("Button_id_check").setProperties({"visible":"none"})
		Top.Dom.selectById("LinearLayout_id_add").setProperties({"visible":"hidden"})
		Top.Dom.selectById("TextView_affiliation_path_0").setProperties({"visible":"none"})
		Top.Dom.selectById("LinearLayoutPath_0").setProperties({"visible":"none"})
		Top.Dom.selectById("LinearLayout_companyinfo_0").setProperties({"visible":"none"})
		Top.Dom.selectById("UserInfoAdd_ModifyButton").setProperties({"visible":"none"})
		Top.Dom.selectById("UserInfoAdd_CancelButton").setProperties({"visible":"none"})
		Top.Dom.selectById("UserInfoModify_ModifyButton").setProperties({"visible":"visible"})
		Top.Dom.selectById("UserInfoModify_CancelButton").setProperties({"visible":"visible"})
		
		//이벤트선언
		Top.Dom.selectById("UserInfoModify_ModifyButton").setProperties({"on-click":"modifyOk"})
		Top.Dom.selectById("UserInfoModify_CancelButton").setProperties({"on-click":"cancelOk"})
		
		
		var user_info = orgUserRepo.selectedUser.userInfo;
		
		if(user_info != null) {
			Top.Dom.selectById('SelectBox212').select(user_info.NATIONAL_CODE);
			Top.Dom.selectById('UserInfoAdd_UserFirstNameContent').setText(user_info.USER_NAME);
			Top.Dom.selectById('UserInfoModify_UserIDContent').setText(user_info.USER_LOGIN_ID.split('@')[0]);
			if(user_info.USER_EMAIL){
				Top.Dom.selectById('UserInfoAdd_UserEmailIDContent').setText(user_info.USER_EMAIL.split('@')[0]);
				Top.Dom.selectById('UserInfoAdd_UserEmailDomainContent').setText(user_info.USER_EMAIL.split('@')[1]);
			}
			if(user_info.USER_PHONE){
				Top.Dom.selectById('UserInfoAdd_UserCellPhoneNumContent0').setText(user_info.USER_PHONE);
			}
			Top.Dom.selectById('UserInfoAdd_UserCompanyPhoneNumContent0').setText(orgUserRepo.selectedUser.companyProfileList[0].USER_COM_NUM);
			if(user_info.USER_BIRTH){
				Top.Dom.selectById('adminBirthDatePicker').setDate(user_info.USER_BIRTH.substring(0,4)+"-"+user_info.USER_BIRTH.substring(4,6)+"-"+user_info.USER_BIRTH.substring(6,8));
			}else{
				Top.Dom.selectById('adminBirthDatePicker').setDate();
			}
			//프로필 가져오기
        	Top.Dom.selectById("ImageButton_Admin_Profile_Setting_add").setSrc(userManager.getUserPhoto(Top.Controller.get('adminUserInfoViewLogic').user_id,'medium',user_info.THUMB_PHOTO));	 	
		} else {
			user_info = orgUserRepo.orgUserList[Top.Dom.selectById("OrgUserTableView").getClickedIndex()[0]];
			
			Top.Dom.selectById('UserInfoAdd_UserFirstNameContent').setText(user_info.user_name);
			if(user_info.email.includes("@")) {
				Top.Dom.selectById('UserInfoModify_UserIDContent').setText(user_info.email.split("@")[0]);
				Top.Dom.selectById('UserInfoAdd_UserEmailIDContent').setText(user_info.email.split('@')[0]);
				Top.Dom.selectById('UserInfoAdd_UserEmailDomainContent').setText(user_info.email.split('@')[1]);
			}
		}
		
		if(orgUserRepo.selectedUser.companyProfileList != null) {   
			//companyProfileList 겸임인경우
			for(let count = 0; count< orgUserRepo.selectedUser.companyProfileList.length; count++){			
				var boolorgking = (orgUserRepo.selectedUser.companyProfileList[count].USER_ROLE == "WKS0004" ? true : false)
	//            if(outputdata.companyProfileList[count].USER_AFF === orgUserRepo.orgUserList[Top.Dom.selectById("OrgUserTableView").getClickedIndex()[0]].org_name ){
	 				if(orgUserRepo.selectedUser.companyProfileList[count].USER_COM_NUM){
	 					Top.Dom.selectById('UserInfoAdd_UserCompanyPhoneNumContent0').setText(orgUserRepo.selectedUser.companyProfileList[count].USER_COM_NUM);
	 				}
	 				Top.Dom.selectById('UserInfoAdd_UserInChargeOfContent').setText(orgUserRepo.selectedUser.companyProfileList[count].USER_TASK);
	 				Top.Dom.selectById('UserInfoAdd_UserOfficeLocationContent').setText(orgUserRepo.selectedUser.companyProfileList[count].USER_LOCATION);
	 				if(orgUserRepo.selectedUser.companyProfileList[count].USER_EMPLOYMENT_DATE){
	 					Top.Dom.selectById('adminJoinDatePicker').setDate(orgUserRepo.selectedUser.companyProfileList[count].USER_EMPLOYMENT_DATE.substring(0,4)+"-"
	 							+orgUserRepo.selectedUser.companyProfileList[count].USER_EMPLOYMENT_DATE.substring(4,6)+"-"
	 							+orgUserRepo.selectedUser.companyProfileList[count].USER_EMPLOYMENT_DATE.substring(6,8));
	 				}
	 				//html로 생성되야할 부분
	 				let html_3 = '<top-linearlayout id="LinearLayoutPathModify_'+count+'" layout-width="wrap_content" layout-height="30px" layout-horizontal-alignment="left" border-width="0px" orientation="horizontal" position="false" title="" layout-tab-disabled="false" vertical-alignment="top" class="linear-child-vertical" style="text-align: left;" actual-height="30">'+ '<div id="LinearLayoutPathModify_'+count+'" class="top-linearlayout-root" style="white-space: nowrap; height: 30px; border-width: 0px; border-style: solid; vertical-align: top; overflow: auto hidden; width: 100%; line-height: 30px;">'
					+'<top-textview id="TextView_affiliation_'+count+'" layout-width="auto" layout-height="auto" layout-vertical-alignment="middle" border-width="0px" text="소속 '+(count+1)+'" text-decoration="" title="" layout-tab-disabled="false" class="linear-child-horizontal" style="line-height: normal;" actual-width="38.1094">'
				+'<span id="TextView_affiliation_Modify_'+count+'" class="top-textview-root" style="width: auto; height: auto; border-width: 0px; border-style: solid; vertical-align: middle;"><a class="top-textview-url">소속 '+ (count+1) +'</a><div class="top-textview-aligner middle"></div></span>'
			+'</top-textview>'
			+'<top-button id="TextView_affiliation_path_'+count+'" on-click = "changeBtn" background-color="rgba(255,255,255,1.0)" layout-width="400px" layout-height="auto" layout-horizontal-alignment="center" margin="0px 0px 0px 10px" padding="0px 0px 1px 0px" border-color="rgba(194,194,194,1.0) rgba(194,194,194,1.0) rgba(194,194,194,1.0) rgba(194,194,194,1.0)" border-width="1px 1px 1px 1px"'
			+'tab-index="0" text="'+orgUserRepo.selectedUser.companyProfileList[count].ORG_PATH+'" text-color="rgba(0,0,0,1.0)" text-decoration="" icon-position="left" title="" layout-tab-disabled="false" class="linear-child-vertical" style="text-align: center;" actual-height="20">'
			+'<button id="TextView_affiliation_path_'+count+'" class="top-button-root" type="button" style="width: 400px; height: auto; background-color: rgb(255, 255, 255); margin: 0px 0px 0px 10px; padding: 0px 0px 1px; color: rgb(0, 0, 0); border-width: 1px; border-style: solid; border-color: rgb(194, 194, 194); text-align: center;" tabindex="0"><label class="top-button-text">"'+orgUserRepo.selectedUser.companyProfileList[count].ORG_PATH+'"</label></button>'
			+'</top-button>'
			+		'</div></top-linearlayout>';
	       	
	       	let html_4 ='<top-linearlayout id="LinearLayout_companyinfo_Modify_'+count+'" layout-width="match_parent" layout-height="30px" layout-horizontal-alignment="left" border-width="0px" horizontal-scroll="false" vertical-scroll="false" orientation="horizontal" position="false" title="" layout-tab-disabled="false" vertical-alignment="top" class="linear-child-vertical" style="text-align: left;" actual-height="30">' 
	       		+'<div id="LinearLayout_companyinfo_Modify_'+count+'" class="top-linearlayout-root" style="white-space: nowrap; height: 30px; border-width: 0px; border-style: solid; vertical-align: top; overflow: hidden; width: 100%; line-height: 30px;">'				
			+'</top-button>'
			+'<top-textfield id="UserInfoModify_UserCompanyIDContent_'+count+'" layout-width="100px" layout-height="27px" margin="0px 0px 0px 10px" tab-index="0" text="" text-decoration="" hint="사번" prevent-key="false" password="false" required="false" icon-position="right" auto-complete-align="bl" title="" layout-tab-disabled="false" class="in_textfield click-placeholder linear-child-horizontal" layout-vertical-alignment="top" style="line-height: normal;" actual-width="140">'
				+'<div class="top-textfield-root" style="display: inline-block; width: 100px; height: 27px; margin: 0px 0px 0px 10px; vertical-align: top;">'
					+'<form autocomplete="off" onsubmit="return false;">'
						+'<input id="UserInfoModify_UserCompanyIDContent_'+count+'" class="top-textfield-text pl-0 pr-0" type="text" value="" placeholder="사번" tabindex="0">'
						+'<span class="top-textfield-icon" style="display: none; vertical-align: baseline;"></span>'
					+'</form>'
				+'</div>'
			+'</top-textfield>'
			+'<top-icon id="UserInfoModify_UseOrgnumErrorIcon_'+count+'" class="icon-work_noti_info linear-child-horizontal" layout-width="16px" layout-height="16px" margin="2px 4px 0px 4px" visible="hidden" border-width="0px" text-size="16px" text-color="#FF0000" title="" layout-tab-disabled="false" layout-vertical-alignment="top" style="line-height: normal;" actual-width="24"><span class="top-icon-root" id="UserInfoModify_UseOrgnumErrorIcon_'+count+'" style="visibility: hidden; width: 16px; height: 16px; margin: 2px 4px 0px; color: rgb(255, 0, 0); font-size: 16px; border-width: 0px; border-style: solid; vertical-align: top;"><i class="top-icon-content" style="line-height: 16px;">'
			+'</i></span></top-icon>'
			+'<top-selectbox id="UserInfoModify_UserEmploymentTypeContent_'+count+'" layout-width="100px" layout-height="27px" title ="고용 형태" layout-vertical-alignment="top" border-width="0px" horizontal-scroll="false" vertical-scroll="false" items="" separator-visible="true" virtual-scroll="false" item-clicktype="click" tab-index="0" title="" multi-select="false" searchable="false" layout-tab-disabled="false" class="linear-child-horizontal" style="line-height: normal;" actual-width="100">'
				+'<div id="UserInfoModify_UserEmploymentTypeContent_'+count+'" class="top-selectbox-root" tabindex="0" style="width: 100px; height: 27px; border-width: 0px; border-style: solid; vertical-align: top; overflow: hidden;"><top-widgetitem text="정규직"></top-widgetitem><top-widgetitem text="계약직"></top-widgetitem><top-widgetitem text="아르바이트"></top-widgetitem><top-widgetitem text="인턴"></top-widgetitem></div>'
		+'</top-selectbox>'
			+'<top-selectbox id="UserInfoModify_UserJobContent_'+count+'" layout-width="100px" layout-height="27px" title ="직위" layout-vertical-alignment="top" margin="0px 0px 0px 4px" border-width="0px" horizontal-scroll="false" vertical-scroll="false" items="" separator-visible="true" virtual-scroll="false" item-clicktype="click" tab-index="0" title="" multi-select="false" searchable="false" layout-tab-disabled="false" class="linear-child-horizontal" style="line-height: normal;" actual-width="104">'
				+'<div id="UserInfoModify_UserJobContent_'+count+'" class="top-selectbox-root" tabindex="0" style="width: 100px; height: 27px; margin: 0px 0px 0px 4px; border-width: 0px; border-style: solid; vertical-align: top; overflow: hidden;"></div>'
		+'</top-selectbox>'
			+'<top-selectbox id="UserInfoModify_OrgJobContent_'+count+'" layout-width="100px" layout-height="27px" title ="직책" layout-vertical-alignment="top" margin="0px 0px 0px 4px" border-width="0px" horizontal-scroll="false" vertical-scroll="false" items="" separator-visible="true" virtual-scroll="false" item-clicktype="click" tab-index="0" title="" multi-select="false" searchable="false" layout-tab-disabled="false" class="linear-child-horizontal" style="line-height: normal;" actual-width="104">'
				+'<div id="UserInfoModify_OrgJobContent_'+count+'" class="top-selectbox-root" tabindex="0" style="width: 100px; height: 27px; margin: 0px 0px 0px 4px; border-width: 0px; border-style: solid; vertical-align: top; overflow: hidden;"></div>'				
		+'</top-selectbox>'
		+`<top-checkbox id="orgking_`+count+`" layout-width="auto" layout-height="auto" layout-vertical-alignment="middle" tab-index="0" text-decoration="" check-position="left" title="" layout-tab-disabled="false" class="linear-child-horizontal" style="line-height: normal;" actual-width="NaN">
		<input class="top-checkbox-check" type="checkbox" size="width:300px;" tabindex="0" style="display: inline-block;">
		<label class="top-checkbox-text left" for="checkbox" style="width: auto; height: auto; vertical-align: middle; padding-right: 0px; padding-top: 7px;"><i class="top-checkbox-icon"></i><span></span></label>
		</top-checkbox>`+
		'</div></top-linearlayout>';
	 
	       		
	       	///////////////////////////////////////////////
	       	Top.Dom.selectById("UserInfoAddPathLayout_right").append(html_3) 
	       	Top.Dom.selectById("UserInfoAddPathLayout_right").append(html_4)
//	   		Top.Dom.selectById("UserInfoAddPathLayout_left").complete();
	       	Top.Dom.selectById("UserInfoAddPathLayout_right").complete();
			Top.Dom.selectById("orgking_"+count).setChecked(boolorgking);
	       	setTimeout(function() {
	       		var additional_job_selectbox = Top.Dom.selectById("UserInfoModify_UserJobContent_" + count);
	        	var additional_position_selectbox = Top.Dom.selectById("UserInfoModify_OrgJobContent_" + count);
	        	var additional_emptype_selectbox = Top.Dom.selectById("UserInfoModify_UserEmploymentTypeContent_" + count);
	        	additional_job_selectbox.setProperties({"nodes": Top.Controller.get('adminUserInfoAddLogic').user_job_list});
	        	additional_position_selectbox.setProperties({"nodes": Top.Controller.get('adminUserInfoAddLogic').user_position_list}); 
				additional_emptype_selectbox.setProperties({"nodes": Top.Controller.get('adminUserInfoAddLogic').user_emptype_list}); 
					
	       	}, 0)
	    		
	 		
	 				Top.Dom.selectById('UserInfoModify_UserCompanyIDContent_'+count).setText(orgUserRepo.selectedUser.companyProfileList[count].USER_EMPLOYEE_NUM);	
	 				Top.Dom.selectById("UserInfoModify_OrgJobContent_"+count).select(orgUserRepo.selectedUser.companyProfileList[count].USER_POSITION)
	 			    Top.Dom.selectById('UserInfoModify_UserEmploymentTypeContent_'+count).select(adminCodeToText(orgUserRepo.selectedUser.companyProfileList[count].USER_EMPLOYMENT_TYPE));
	 				Top.Dom.selectById("UserInfoModify_UserJobContent_"+count).select(orgUserRepo.selectedUser.companyProfileList[count].USER_JOB)
	 				Top.Dom.selectById('TextView_affiliation_path_'+count).setText(orgUserRepo.selectedUser.companyProfileList[count].ORG_PATH);
	
					 $("#TextView_affiliation_path_"+count + " label").off('click').on('click' ,function() {
						Top.Controller.get("adminUserInfoAddLogic").changeBtn("",this);
						 });	
	          //  }
	          //  }
			}
		}
		
		$.ajax({
			url : _workspace.url + "Admin/CompanyInfo", 
            type: 'GET',
            dataType: 'json',
            crossDomain: true,
            contentType: 'application/json; charset=utf-8',
            xhrFields: {
                withCredentials: true
              },
            data: JSON.stringify({
              "dto": {
                "ADMIN_ID": userManager.getLoginUserId()
              }
            }),
            success: function(result) {
//            	Top.Dom.selectById('UserInfoAdd_UserCompanyContent').setText(result.dto.COMPANY_NAME)
	        },
	        error: function(error) {        	
	        	
	        }
			});	
		
		
		
	},
	
	/************************************조직원 상세->수정 ok/cancel*********************************************************/
	cancelOk : function(event,widget){
		var main = Top.Dom.selectById('adminUserInfoMain');
		var ctrl = Top.Controller.get('adminUserInfoViewLogic');
		let user_id = this.user_id;
		main.onSrcLoad(function(){
			ctrl.init_contents(user_id); //USER_ID 키값 넘김
		});
		main.src('adminUserInfoView.html' + verCsp()); //첫 화면은 유저 상세 보기
	},
	
	modifyOk : function(event, widget){
		
		//프로필이미지
		var image;
		if(Top.Dom.selectById("ImageButton_Admin_Profile_Setting_add").getSrc() === userManager.getUserDefaultPhotoURL(Top.Controller.get("adminUserInfoViewLogic").user_id)){
			image = userManager.getUserDefaultPhotoURL(Top.Controller.get("adminUserInfoViewLogic").user_id)
    	}else{
    		image = Top.Dom.selectById("ImageButton_Admin_Profile_Setting_add").getSrc();
    	}
		var flag_id=0;
		var tmpnum1= Top.Dom.selectById('UserInfoAdd_UserCompanyPhoneNumContent0').getText();
		if(!pattern_num.test(Number(tmpnum1))){
			Top.Dom.selectById('Icon22').setProperties({"visible":"visible"})
			flag_id = 1;
			Top.Dom.selectById('UserInfoAdd_UserCompanyPhoneNumContent0').setProperties({"border-color":"red"})
		}
		else Top.Dom.selectById('Icon22').setProperties({"visible":"hidden"})
		var tmpnum2 = Top.Dom.selectById('UserInfoAdd_UserCellPhoneNumContent0').getText();
		if(!pattern_num.test(Number(tmpnum2))){
			Top.Dom.selectById('Icon59').setProperties({"visible":"visible"})
			flag_id = 1;
			Top.Dom.selectById('UserInfoAdd_UserCellPhoneNumContent0').setProperties({"border-color":"red"})
		}
		else Top.Dom.selectById('Icon59').setProperties({"visible":"hidden"})
		 //비밀번호
			var flag_pwd = 0; 
			if( Top.Dom.selectById("Icon_userAdd_pwd_error").getProperties("text-color")=="#07C25B"){
				Top.Dom.selectById("UserInfoAdd_UserPasswordInfoErrorIcon").setProperties({"visible":"none"});
				Top.Dom.selectById("UserInfoAdd_UserPasswordContent").setProperties({"border-color":"#dddddd"})
			}else{
				if(Top.Dom.selectById("UserInfoAdd_UserPasswordContent").getText() === undefined || Top.Dom.selectById("UserInfoAdd_UserPasswordContent").getText() === "" ){
					Top.Dom.selectById("UserInfoAdd_UserPasswordInfoErrorIcon").setProperties({"visible":"none"});
					Top.Dom.selectById("UserInfoAdd_UserPasswordContent").setProperties({"border-color":"#dddddd"})
					
				}else{
					Top.Dom.selectById("UserInfoAdd_UserPasswordInfoErrorIcon").setProperties({"visible":"visible"});
					Top.Dom.selectById("UserInfoAdd_UserPasswordContent").setProperties({"border-color":"red"})
					flag_pwd = 1;
					}
			}
			//한글 입력 방지
			if(pattern_kor.test(Top.Dom.selectById("UserInfoAdd_UserPasswordContent").getText())){
				Top.Dom.selectById("UserInfoAdd_UserPasswordContent").setProperties({"border-color":"red"})
				Top.Dom.selectById("UserInfoAdd_UserPasswordInfoErrorIcon").setProperties({"visible":"visible"})
				flag_pwd = 1;
			}
			
		//이름
		var flag_name = 0;
		if(Top.Dom.selectById("UserInfoAdd_UserFirstNameContent").getText() === ""){
			Top.Dom.selectById("UserInfoAdd_UserFirstNameContent").setProperties({"border-color":"red"})
			Top.Dom.selectById("UserInfoAdd_UserNameErrorIcon").setProperties({"visible":"visible"})
			flag_name = 1;
		}
		 if(flag_name === 0){
		        Top.Dom.selectById("UserInfoAdd_UserFirstNameContent").setProperties({"border-color":"#dddddd "})
		        Top.Dom.selectById("UserInfoAdd_UserNameErrorIcon").setProperties({"visible":"none"})
		 }
		//사번
		
		 for(var z = 0 ; z<$('#UserInfoAddPathLayout_left top-linearlayout').length - 1 ; z++ ){
			 var flag_orgnum  = 0;
			 if(Top.Dom.selectById("UserInfoModify_UserCompanyIDContent_"+z).getText() === ""){
					Top.Dom.selectById("UserInfoModify_UserCompanyIDContent_"+z).setProperties({"border-color":"red"})
					Top.Dom.selectById("UserInfoModify_UseOrgnumErrorIcon_"+z).setProperties({"visible":"visible"})
					flag_orgnum = 1;
				}
				 if(flag_orgnum === 0){
				        Top.Dom.selectById("UserInfoModify_UserCompanyIDContent_"+z).setProperties({"border-color":"#dddddd "})
				        Top.Dom.selectById("UserInfoModify_UseOrgnumErrorIcon_"+z).setProperties({"visible":"hidden"})
				 }
		 }
//		 var flag_orgnum = 0;
//		 if(Top.Dom.selectById("UserInfoAdd_UserCompanyIDContent").getText() === ""){
//				Top.Dom.selectById("UserInfoAdd_UserCompanyIDContent").setProperties({"border-color":"red"})
//				Top.Dom.selectById("UserInfoAdd_UseOrgnumErrorIcon").setProperties({"visible":"visible"})
//				flag_orgnum = 1;
//			}
//			 if(flag_orgnum === 0){
//			        Top.Dom.selectById("UserInfoAdd_UserCompanyIDContent").setProperties({"border-color":"#dddddd "})
//			        Top.Dom.selectById("UserInfoAdd_UseOrgnumErrorIcon").setProperties({"visible":"none"})
//			 }
		
		//이메일
		var flag_mail = 0;
		var flag_domain = 0;
		if(Top.Dom.selectById("UserInfoAdd_UserEmailIDContent").getText() === ""){
			Top.Dom.selectById("UserInfoAdd_UserEmailIDContent").setProperties({"border-color":"red"})
			Top.Dom.selectById("UserInfoAdd_UserMailErrorIcon").setProperties({"visible":"visible"})
			flag_mail = 1;
			
		}
		 if(Top.Dom.selectById("UserInfoAdd_UserEmailDomainContent").getText() === ""){
		    Top.Dom.selectById("UserInfoAdd_UserEmailDomainContent").setProperties({"border-color":"red"})
		 	Top.Dom.selectById("UserInfoAdd_UserMailErrorIcon").setProperties({"visible":"visible"})
		 	flag_domain = 1;
		 }
		 
		 if(flag_mail === 0){
		        Top.Dom.selectById("UserInfoAdd_UserEmailIDContent").setProperties({"border-color":"#dddddd "})
		        Top.Dom.selectById("UserInfoAdd_UserMailErrorIcon").setProperties({"visible":"none"})
		 }
		 if(flag_domain === 0){
		        Top.Dom.selectById("UserInfoAdd_UserEmailDomainContent").setProperties({"border-color":"#dddddd "})
		         Top.Dom.selectById("UserInfoAdd_UserMailErrorIcon").setProperties({"visible":"none"})
		 }
		 if(flag_mail || flag_domain){
			 Top.Dom.selectById("UserInfoAdd_UserMailErrorIcon").setProperties({"visible":"visible"})
		 } 
		
		
		//리턴 처리
		if( flag_pwd || flag_name || flag_orgnum ||flag_mail || flag_domain || flag_id){ return;}
		
		
		var birthDate;
		
		if(Top.Dom.selectById("adminBirthDatePicker").getDate() === ""){
			birthDate = null
		}else{
			birthDate = Top.Dom.selectById('adminBirthDatePicker').getDate().split('-')[0] + Top.Dom.selectById('adminBirthDatePicker').getDate().split('-')[1] + Top.Dom.selectById('adminBirthDatePicker').getDate().split('-')[2]
		}
		
		var joinDate;
		if(Top.Dom.selectById("adminJoinDatePicker").getDate() === ""){
			joinDate = null
		}else{
			joinDate = Top.Dom.selectById('adminJoinDatePicker').getDate().split('-')[0] + Top.Dom.selectById('adminJoinDatePicker').getDate().split('-')[1] + Top.Dom.selectById('adminJoinDatePicker').getDate().split('-')[2]
		}
				//note : org tree에서 바로 ID를 org_path_array에 obj로 집어넣는게 더 효율적이나, 생각하기 편한 방식으로 일단 구현함..
			//var comp_obj = adminGetTeamObjByName(this.org_path_array[this.org_path_array.length - 1]);
			
			var role_code;			
//			
//			if(Top.Dom.selectById('UserInfoAdd_UserCompanyPositionContent').getSelectedText() === "팀원"){
//				role_code = "WKS0005";
//			} else{
//				role_code = "WKS0004";
//			}	
			
			var userInfo = {
				 "USER_ID":this.user_id,
				 "USER_LOGIN_ID":Top.Dom.selectById('UserInfoAdd_UserIDContent').getText(),
		         "USER_PW":Top.Dom.selectById('UserInfoAdd_UserPasswordContent').getText(),
		         "USER_NAME":Top.Dom.selectById('UserInfoAdd_UserFirstNameContent').getText(),
		         "USER_EMAIL":Top.Dom.selectById('UserInfoAdd_UserEmailIDContent').getText() + '@' + Top.Dom.selectById('UserInfoAdd_UserEmailDomainContent').getText(),
		         "USER_BIRTH":birthDate,
		         "USER_PHONE":Top.Dom.selectById('UserInfoAdd_UserCellPhoneNumContent0').getText(),
		         "PROFILE_PHOTO":image,
		         "NATIONAL_CODE": Top.Dom.selectById('SelectBox212').getValue()
			}; 
			var companyProfileList= [];
			var transferPath;
			var locationText = Top.Dom.selectById('UserInfoAdd_UserOfficeLocationContent').getText();
			var taskText = Top.Dom.selectById('UserInfoAdd_UserInChargeOfContent').getText();
			
			if( locationText === null || locationText.length ===  0 ){
				locationText =  "—";
			}
			if(taskText === null || taskText.length ===  0 ){
				taskText =  "—";
			}
			
			
			
for(let j = 0; j<this.ee.length; j++ ){
				
				
				var z = $('#TextView_affiliation_path_'+j).attr('CODE');
				 
				companyProfileList.push( 
						{
							"USER_EMPLOYEE_NUM":Top.Dom.selectById('UserInfoModify_UserCompanyIDContent_'+j).getText(),
				            "USER_JOB":Top.Controller.get('adminUserInfoAddLogic').mcn1[Top.Dom.selectById('UserInfoModify_UserJobContent_'+j).getSelectedText()],
				            "USER_POSITION":Top.Controller.get('adminUserInfoAddLogic').mcn2[Top.Dom.selectById('UserInfoModify_OrgJobContent_'+j).getSelectedText()],
				            "USER_AFF": this.ee[j].USER_AFF,
				            
				            "USER_ROLE":Top.Dom.selectById('orgking_'+j).isChecked() ? "WKS0004" : "WKS0005",//Top.Dom.selectById('').getText(),
				            "USER_COM_NUM": Top.Dom.selectById('UserInfoAdd_UserCompanyPhoneNumContent0').getText() ,
				            "USER_EMPLOYMENT_TYPE":Top.Controller.get('adminUserInfoAddLogic').mcn3[Top.Dom.selectById('UserInfoModify_UserEmploymentTypeContent_'+j).getSelectedText()],//Top.Dom.selectById('').getText(),
				            "USER_TASK":taskText,
				            "USER_LOCATION":locationText,
				            "USER_EMPLOYMENT_DATE":joinDate,
				            "USER_TRANSFER_AFF" : this.ee[j].USER_AFF == z ? null : z
						
						})
			}

for(let len = this.ee.length; len<$('#UserInfoAddPathLayout_right top-linearlayout button').length;len++){
	companyProfileList.push( 
			{
				"USER_EMPLOYEE_NUM":Top.Dom.selectById('UserInfoModify_UserCompanyIDContent_'+len).getText(),
	            "USER_JOB":Top.Controller.get('adminUserInfoAddLogic').mcn1[Top.Dom.selectById('UserInfoModify_UserJobContent_'+len).getSelectedText()],
	            "USER_POSITION":Top.Controller.get('adminUserInfoAddLogic').mcn2[Top.Dom.selectById('UserInfoModify_OrgJobContent_'+len).getSelectedText()],
	            "USER_AFF": $('#TextView_affiliation_path_'+len).attr('CODE'),
	            "USER_ROLE":Top.Dom.selectById('orgking_'+len).isChecked() ? "WKS0004" : "WKS0005",//Top.Dom.selectById('').getText(),
	            "USER_COM_NUM": Top.Dom.selectById('UserInfoAdd_UserCompanyPhoneNumContent0').getText(),
	            "USER_EMPLOYMENT_TYPE":Top.Controller.get('adminUserInfoAddLogic').mcn3[Top.Dom.selectById('UserInfoModify_UserEmploymentTypeContent_'+len).getSelectedText()],//Top.Dom.selectById('').getText(),
	            "USER_TASK":taskText,
	            "USER_LOCATION":locationText,
	            "USER_EMPLOYMENT_DATE": joinDate
	          //  "USER_TRANSFER_AFF" : adminGetTeamObjByName(this.ee[j].ORG_PATH.split(' > ')[this.ee[j].ORG_PATH.split(' > ').length-1]).COMPANY_CODE + '_' +adminGetTeamObjByName(this.ee[j].ORG_PATH.split(' > ')[this.ee[j].ORG_PATH.split(' > ').length-1]).DEPARTMENT_CODE == z ? null : z
			
			})
			
}

			var inputDTO = {
				dto: {
			        "userInfo": userInfo,
			        "companyProfileList": companyProfileList
			    }
			};
			
			Top.Ajax.execute({
				url : _workspace.url + "Admin/OrgUser",
				type : 'PUT',
				dataType : "json",
				async : false,
				cache : false,
				data : JSON.stringify(inputDTO), // arg
				contentType : "application/json",
				crossDomain : true,
				xhrFields : {
				    withCredentials: true
				},
				headers: {
					"ProObjectWebFileTransfer":"true"
				},
				success : function(data) {
				
//					//테이블 업데이트			
//					var orgUserInfo = {
//							user_name: userInfo.USER_NAME,
//							org_name: companyProfileList[0].ORG_NAME,
//							job: companyProfileList[0].USER_POSITION,
//							com_num: companyProfileList[0].USER_COM_NUM,
//							email: userInfo.USER_EMAIL,
//							status: Top.Dom.selectById('UserInfoModify_UserEmploymentTypeContent').getSelectedText(),
//							USER_ID: userInfo.USER_ID,
//							affcode: companyProfileList[0].USER_AFF
//					};
//					orgUserRepo.orgUserList[Top.Dom.selectById("OrgUserTableView").getClickedIndex()[0]]  = orgUserInfo	
//					//소속이 바뀔 경우
//					
//					
//					Top.Dom.selectById("OrgUserTableView").update();
					
					
//					
//					if(orgUserRepo.orgUserList[Top.Dom.selectById("OrgUserTableView").getClickedIndex()[0]] === undefined){
//            			Top.Controller.get('adminOrgUserLayoutLogic').clickNode(MouseEvent,Top.Dom.selectById('TreeView1305'))
//	            Top.Dom.selectById('adminUserInfoViewDialog').close()
//	           
//	            	return;
//	         } 
			

					//확인 클릭 시 view화면으로 이동
	            	Top.Dom.selectById('adminUserInfoMain').onSrcLoad(function(){
		            	Top.Controller.get('adminUserInfoViewLogic').init_contents(userInfo.USER_ID); 
	               	});
	            	
	            	
	           
	                   Top.Dom.selectById('adminUserInfoMain').src('adminUserInfoView.html' + verCsp());
//	               	Top.Controller.get('adminOrgUserLayoutLogic').clickNode(MouseEvent,Top.Dom.selectById('TreeView1305'))
	                   
//	          orgUserRepo.orgUserList[Top.Dom.selectById("OrgUserTableView").getClickedIndex()[0]].org_name = orgUserRepo.selectedUser.companyProfileList[0].ORG_PATH.split(' > ')[orgUserRepo.selectedUser.companyProfileList[0].ORG_PATH.split(' > ').length -1]
//					Top.Dom.selectById('OrgUserTableView').update();
				},
				
				error : function(data){
					TeeToast.open({
			    		text: '조직원 정보 수정 실패',
			    		size: 'min'
			    	});
					return;					
				}
			});
		
		
		
		
		
		
	},
	
	
	
	
	
	
	
	
	idExist : function(event, widget){
		
		
		
		$.ajax({
	        url: _workspace.url+'Users/IDCheck?action=SO', //Service Object
	        type: 'POST',
	        dataType: 'json',
	        crossDomain: true,
	        contentType : "application/json",
	        xhrFields: {
	            withCredentials: true
	          },
	        data: JSON.stringify({
	          "dto": {
	        	"USER_LOGIN_ID": Top.Dom.selectById('UserInfoAdd_UserIDContent').getText() + '@'+ _mailDomain 
	          }
	        }),
	        success: function(result) {
	        	if(result.dto.USER_PW == "RST0001"){
	        		Top.Dom.selectById("UserInfoAdd_UserIDContent").setProperties({"border-color":"red"});
	        		Top.Dom.selectById("UserInfoAdd_UserIDInfoErrorIcon").setProperties({"visible":"visible","tooltip-top":"이미 사용 중인 아이디 입니다. 다른 아이디를 입력하세요."});
	        		
	        		//조상호 동일한 아이디 존재
	        		// 아이디 중복됨
	        	}else{
	    			Top.Dom.selectById("UserInfoAdd_UserIDContent").setProperties({"border-color":"#D3DBDF"});
	    			Top.Dom.selectById("UserInfoAdd_UserIDInfoErrorIcon").setProperties({"visible":"hidden"});
	    	
	        		//아이디 중복안됨
	        	}
	        },
	        error: function(error) {
	        }
		})
		
		
		
		
		
		
	},
	
	addPathIcon : function (event, widget){
		Top.Controller.get('adminUserInfoAddLogic').bb = 0;
		Top.Dom.selectById("adminUserTreeviewDialog").open()
		
	},
	changeBtn : function (event, widget){
		//기존 소속 selected 되어있게해야함
		Top.Controller.get('adminUserInfoAddLogic').bb = 1;
		this.cc = widget.id;
		
		Top.Dom.selectById("adminUserTreeviewDialog").open(event, widget)
		
		
	},
	
	add_id_check : function(event , widget){
	
		var idVal = Top.Dom.selectById("UserInfoAdd_UserIDContent").getText()
		if(idVal.length<4 || idVal.length>20){
			Top.Dom.selectById("Icon_userAdd_id_error").setProperties({"text-color":"#808080"});
			
		}
		else{			
			if( (pattern_kor.test(idVal)) || (pattern_spc.test(idVal))){
				Top.Dom.selectById("Icon_userAdd_id_error").setProperties({"text-color":"#808080"});	
			}
			else{
				
				Top.Dom.selectById("Icon_userAdd_id_error").setProperties({"text-color":"#07C25B"});
				
			}	
		}
	},
	
	
	add_pw_check : function(event, widget){
		$("input#UserInfoAdd_UserPasswordContent").css("style","ime-mode:active")
		var pwdtext = Top.Dom.selectById("UserInfoAdd_UserPasswordContent").getText()
		
		
		var pwdarry = new Array(2);
	if(pwdtext.length >= 9 && pwdtext.length <= 20){
		pwdarry[0]=1;
	}
	else {
					
	}
	if((pattern_num.test(pwdtext)) && (pattern_engs.test(pwdtext)) && (pattern_spc2.test(pwdtext))){

		pwdarry[1]=1;
	}
	else if((pattern_num.test(pwdtext)) && (pattern_engs.test(pwdtext)) && (pattern_engb.test(pwdtext))){
		pwdarry[1]=1;
	}
	else if((pattern_engs.test(pwdtext)) && (pattern_spc2.test(pwdtext)) && (pattern_engb.test(pwdtext))){
		pwdarry[1]=1;
	}
	else{
	}
	
	if( pwdarry[0]==1 && pwdarry[1]==1){
		Top.Dom.selectById("UserInfoAdd_UserPasswordInfoErrorIcon").setProperties({"visible":"none"});
		Top.Dom.selectById("UserInfoAdd_UserPasswordContent").setProperties({"border-color":"#dddddd"})
		Top.Dom.selectById("Icon_userAdd_pwd_error").setProperties({"text-color":"#07C25B"});
	}else{
		Top.Dom.selectById("UserInfoAdd_UserPasswordInfoErrorIcon").setProperties({"visible":"none"});
		Top.Dom.selectById("UserInfoAdd_UserPasswordContent").setProperties({"border-color":"#dddddd"})
		Top.Dom.selectById("Icon_userAdd_pwd_error").setProperties({"text-color":"#808080"});
	}
		
		
	},
	
	
	
	/************************************조직원 개별 추가 ok/cancel*********************************************************/
	click_cancel : function(event, widget) {
		
		
		var dialog =Top.Dom.selectById('adminUserAddDialog');
		dialog.close();

	}, 
	
	click_confirm : function(event, widget) {
		//프로필이미지
		
    	var image = Top.Dom.selectById("ImageButton_Admin_Profile_Setting_add").getSrc();
    	//아이디
		var flag_id = 0;
		var tmpnum1= Top.Dom.selectById('UserInfoAdd_UserCompanyPhoneNumContent0').getText();
		if(!pattern_num.test(Number(tmpnum1))){
			Top.Dom.selectById('Icon22').setProperties({"visible":"visible"})
			flag_id = 1;
			Top.Dom.selectById('UserInfoAdd_UserCompanyPhoneNumContent0').setProperties({"border-color":"red"})
		}
		else Top.Dom.selectById('Icon22').setProperties({"visible":"hidden"})
		var tmpnum2 = Top.Dom.selectById('UserInfoAdd_UserCellPhoneNumContent0').getText();
		if(!pattern_num.test(Number(tmpnum2)) || tmpnum2 == "" ){
			Top.Dom.selectById('Icon59').setProperties({"visible":"visible"})
			flag_id = 1;
			Top.Dom.selectById('UserInfoAdd_UserCellPhoneNumContent0').setProperties({"border-color":"red"})
		}
		if(Top.Dom.selectById('adminJoinDatePicker').getDate() == "") {
			$('#adminJoinDatePicker input').css({"border-color":"red"});
			flag_id = 1;
		}
		if(Top.Dom.selectById('adminBirthDatePicker').getDate() == "") {
			$('#adminBirthDatePicker input').css({"border-color":"red"});
			flag_id = 1;
		}
		else Top.Dom.selectById('Icon59').setProperties({"visible":"hidden"})
    	if( Top.Dom.selectById("Icon_userAdd_id_error").getProperties("text-color")=="#07C25B"){
			Top.Dom.selectById("UserInfoAdd_UserIDInfoErrorIcon").setProperties({"display":"hidden"});
			Top.Dom.selectById("UserInfoAdd_UserIDContent").setProperties({"border-color":"#dddddd"})
		}else{
			
				Top.Dom.selectById("UserInfoAdd_UserIDContent").setProperties({"border-color":"red"})
				Top.Dom.selectById("UserInfoAdd_UserIDInfoErrorIcon").setProperties({"visible":"visible"})
				flag_id = 1;
			
		}
		 if(flag_id === 0){
		        Top.Dom.selectById("UserInfoAdd_UserIDContent").setProperties({"border-color":"#dddddd "})
		        Top.Dom.selectById("UserInfoAdd_UserIDInfoErrorIcon").setProperties({"display":"hidden"})
		 }
    	
		//비밀번호
		var flag_pwd = 0; 
		if( Top.Dom.selectById("Icon_userAdd_pwd_error").getProperties("text-color")=="#07C25B"){
			Top.Dom.selectById("UserInfoAdd_UserPasswordInfoErrorIcon").setProperties({"visible":"none"});
			Top.Dom.selectById("UserInfoAdd_UserPasswordContent").setProperties({"border-color":"#dddddd"})
		}else{
		
//			if(Top.Dom.selectById("UserInfoAdd_UserPasswordContent").getText() === undefined || Top.Dom.selectById("UserInfoAdd_UserPasswordContent").getText() === ""){
			Top.Dom.selectById("UserInfoAdd_UserPasswordContent").setProperties({"border-color":"red"})
			Top.Dom.selectById("UserInfoAdd_UserPasswordInfoErrorIcon").setProperties({"visible":"visible"})
			flag_pwd = 1;
//			}
		}
		//한글 입력 방지
		if(pattern_kor.test(Top.Dom.selectById("UserInfoAdd_UserPasswordContent").getText())){
			Top.Dom.selectById("UserInfoAdd_UserPasswordContent").setProperties({"border-color":"red"})
			Top.Dom.selectById("UserInfoAdd_UserPasswordInfoErrorIcon").setProperties({"visible":"visible"})
			flag_pwd = 1;
		}
		
		 if(flag_pwd === 0){
		        Top.Dom.selectById("UserInfoAdd_UserPasswordContent").setProperties({"border-color":"#dddddd "})
		        Top.Dom.selectById("UserInfoAdd_UserPasswordInfoErrorIcon").setProperties({"visible":"none"})
		 }
		
		//이름
		var flag_name = 0;
		if(Top.Dom.selectById("UserInfoAdd_UserFirstNameContent").getText() === ""){
			Top.Dom.selectById("UserInfoAdd_UserFirstNameContent").setProperties({"border-color":"red"})
			Top.Dom.selectById("UserInfoAdd_UserNameErrorIcon").setProperties({"visible":"visible"})
			flag_name = 1;
		}
		 if(flag_name === 0){
		        Top.Dom.selectById("UserInfoAdd_UserFirstNameContent").setProperties({"border-color":"#dddddd "})
		        Top.Dom.selectById("UserInfoAdd_UserNameErrorIcon").setProperties({"visible":"none"})
		 }
		//사번
		 
		 var flag_orgnum = 0;
		 if(Top.Dom.selectById("UserInfoAdd_UserCompanyIDContent").getText() === ""){
				Top.Dom.selectById("UserInfoAdd_UserCompanyIDContent").setProperties({"border-color":"red"})
				Top.Dom.selectById("UserInfoAdd_UseOrgnumErrorIcon").setProperties({"visible":"visible"})
				flag_orgnum = 1;
			}
			 if(flag_orgnum === 0){
			        Top.Dom.selectById("UserInfoAdd_UserCompanyIDContent").setProperties({"border-color":"#dddddd "})
			        Top.Dom.selectById("UserInfoAdd_UseOrgnumErrorIcon").setProperties({"visible":"hidden"})
			 }
			 

		        //사번 반복
		        for(var q = 2; q<=$('#UserInfoAddPathLayout_right top-linearlayout button').length + 1 ;q++){
		            if(Top.Dom.selectById("UserInfoAdd_UserCompanyIDContent_"+q).getText() === ""){
					    Top.Dom.selectById("UserInfoAdd_UserCompanyIDContent_"+q).setProperties({"border-color":"red"})
						Top.Dom.selectById("UserInfoAdd_UseOrgnumErrorIcon_"+q).setProperties({"visible":"visible"})
						flag_orgnum = 1;
					}
					 if(flag_orgnum === 0){
					        Top.Dom.selectById("UserInfoAdd_UserCompanyIDContent_"+q).setProperties({"border-color":"#dddddd "})
					        Top.Dom.selectById("UserInfoAdd_UseOrgnumErrorIcon_"+q).setProperties({"visible":"hidden"})
					 }
		        	
		        }
		
		//이메일
		var flag_mail = 0;
		var flag_domain = 0;
		if(Top.Dom.selectById("UserInfoAdd_UserEmailIDContent").getText() === ""){
			Top.Dom.selectById("UserInfoAdd_UserEmailIDContent").setProperties({"border-color":"red"})
			Top.Dom.selectById("UserInfoAdd_UserMailErrorIcon").setProperties({"visible":"visible"})
			flag_mail = 1;
			
		}
		 if(Top.Dom.selectById("UserInfoAdd_UserEmailDomainContent").getText() === ""){
		    Top.Dom.selectById("UserInfoAdd_UserEmailDomainContent").setProperties({"border-color":"red"})
		 	Top.Dom.selectById("UserInfoAdd_UserMailErrorIcon").setProperties({"visible":"visible"})
		 	flag_domain = 1;
		 }
		 
		 if(flag_mail === 0){
		        Top.Dom.selectById("UserInfoAdd_UserEmailIDContent").setProperties({"border-color":"#dddddd "})
		        Top.Dom.selectById("UserInfoAdd_UserMailErrorIcon").setProperties({"visible":"none"})
		 }
		 if(flag_domain === 0){
		        Top.Dom.selectById("UserInfoAdd_UserEmailDomainContent").setProperties({"border-color":"#dddddd "})
		         Top.Dom.selectById("UserInfoAdd_UserMailErrorIcon").setProperties({"visible":"none"})
		 }
		 if(flag_mail || flag_domain){
			 Top.Dom.selectById("UserInfoAdd_UserMailErrorIcon").setProperties({"visible":"visible"})
		 } 
		
		
		//리턴 처리
		if(flag_id || flag_pwd || flag_name || flag_orgnum ||flag_mail || flag_domain){ return;}
		
		
		var birthDate;
		
		if(Top.Dom.selectById("adminBirthDatePicker").getDate() === ""){
			birthDate = null
		}else{
			birthDate = Top.Dom.selectById('adminBirthDatePicker').getDate().split('-')[0] + Top.Dom.selectById('adminBirthDatePicker').getDate().split('-')[1] + Top.Dom.selectById('adminBirthDatePicker').getDate().split('-')[2]
		}
		var joinDate;
		if(Top.Dom.selectById("adminJoinDatePicker").getDate() === ""){
			joinDate = null
		}else{
			joinDate = Top.Dom.selectById('adminJoinDatePicker').getDate().split('-')[0] + Top.Dom.selectById('adminJoinDatePicker').getDate().split('-')[1] + Top.Dom.selectById('adminJoinDatePicker').getDate().split('-')[2]
		}
				//note : org tree에서 바로 ID를 org_path_array에 obj로 집어넣는게 더 효율적이나, 생각하기 편한 방식으로 일단 구현함..
			//var comp_obj = adminGetTeamObjByName(this.org_path_array[this.org_path_array.length - 1]);
			
			var role_code;			
//			
//			if(Top.Dom.selectById('UserInfoAdd_UserCompanyPositionContent').getSelectedText() === "팀원"){
//				role_code = "WKS0005";
//			} else{
//				role_code = "WKS0004";
//			}	
			
			var userInfo = {
				 "USER_LOGIN_ID":Top.Dom.selectById('UserInfoAdd_UserIDContent').getText(),
		         "USER_PW":Top.Dom.selectById('UserInfoAdd_UserPasswordContent').getText(),
		         "USER_NAME":Top.Dom.selectById('UserInfoAdd_UserFirstNameContent').getText(),
		         "USER_EMAIL":Top.Dom.selectById('UserInfoAdd_UserEmailIDContent').getText() + '@' + Top.Dom.selectById('UserInfoAdd_UserEmailDomainContent').getText(),
		         "USER_BIRTH": birthDate,
		         "USER_PHONE":Top.Dom.selectById('UserInfoAdd_UserCellPhoneNumContent0').getText(),
		         "PROFILE_PHOTO":image,
		         "NATIONAL_CODE" : Top.Dom.selectById('SelectBox212').getValue()
			}; 
			var companyProfileList= [];
			companyProfileList.push(
					{
						"USER_EMPLOYEE_NUM":Top.Dom.selectById('UserInfoAdd_UserCompanyIDContent').getText(),
			            "USER_JOB":Top.Controller.get('adminUserInfoAddLogic').mcn1[Top.Dom.selectById('UserInfoAdd_UserJobContent').getSelectedText()],
			            "USER_POSITION":Top.Controller.get('adminUserInfoAddLogic').mcn2[Top.Dom.selectById('UserInfoAdd_OrgJobContent').getSelectedText()],
			            //"USER_AFF":adminGetTeamObjByName(Top.Dom.selectById('TextView_affiliation_path_0').getText().split(' > ')[Top.Dom.selectById('TextView_affiliation_path_0').getText().split(' > ').length-1]).COMPANY_CODE + '_' +adminGetTeamObjByName(Top.Dom.selectById('TextView_affiliation_path_0').getText().split(' > ')[Top.Dom.selectById('TextView_affiliation_path_0').getText().split(' > ').length-1]).DEPARTMENT_CODE,
			            "USER_AFF": $('#TextView_affiliation_path_0').attr('CODE'),
			            "USER_ROLE":Top.Dom.selectById('orgking').isChecked() ? "WKS0004" : "WKS0005",//Top.Dom.selectById('').getText(),
			            "USER_COM_NUM": Top.Dom.selectById('UserInfoAdd_UserCompanyPhoneNumContent0').getText() ,
			            "USER_EMPLOYMENT_TYPE":Top.Controller.get('adminUserInfoAddLogic').mcn3[adminTextToCode(Top.Dom.selectById('UserInfoAdd_UserEmploymentTypeContent').getSelectedText())],//Top.Dom.selectById('').getText(),
			            "USER_TASK":Top.Dom.selectById('UserInfoAdd_UserInChargeOfContent').getText(),
			            "USER_LOCATION":Top.Dom.selectById('UserInfoAdd_UserOfficeLocationContent').getText(),
			            "USER_EMPLOYMENT_DATE": joinDate
					
					})
			
			for(let j = 2; j<=this.aa; j++ ){
				companyProfileList.push(
						{
							"USER_EMPLOYEE_NUM":Top.Dom.selectById('UserInfoAdd_UserCompanyIDContent_'+j).getText(),
				            "USER_JOB":Top.Controller.get('adminUserInfoAddLogic').mcn1[Top.Dom.selectById('UserInfoAdd_UserJobContent_'+j).getSelectedText()],
				            "USER_POSITION":Top.Controller.get('adminUserInfoAddLogic').mcn2[Top.Dom.selectById('UserInfoAdd_OrgJobContent_'+j).getSelectedText()],
				            //"USER_AFF":adminGetTeamObjByName(Top.Dom.selectById('TextView_affiliation_path_'+j).getText().split(' > ')[Top.Dom.selectById('TextView_affiliation_path_'+j).getText().split(' > ').length-1]).COMPANY_CODE + '_' +adminGetTeamObjByName(Top.Dom.selectById('TextView_affiliation_path_'+j).getText().split(' > ')[Top.Dom.selectById('TextView_affiliation_path_'+j).getText().split(' > ').length-1]).DEPARTMENT_CODE,
				            "USER_AFF": $('#TextView_affiliation_path_'+j).attr('CODE'),
				            "USER_ROLE":Top.Dom.selectById('orgking_'+j).isChecked() ? "WKS0004" : "WKS0005",//Top.Dom.selectById('').getText(),
				            "USER_COM_NUM": Top.Dom.selectById('UserInfoAdd_UserCompanyPhoneNumContent0').getText() + '-'
				            + Top.Dom.selectById('UserInfoAdd_UserCompanyPhoneNumContent1').getText() + '-' + Top.Dom.selectById('UserInfoAdd_UserCompanyPhoneNumContent2').getText(),
				            "USER_EMPLOYMENT_TYPE":Top.Controller.get('adminUserInfoAddLogic').mcn3[adminTextToCode(Top.Dom.selectById('UserInfoAdd_UserEmploymentTypeContent_'+j).getSelectedText())],//Top.Dom.selectById('').getText(),
				            "USER_TASK":Top.Dom.selectById('UserInfoAdd_UserInChargeOfContent').getText(),
				            "USER_LOCATION":Top.Dom.selectById('UserInfoAdd_UserOfficeLocationContent').getText(),
				            "USER_EMPLOYMENT_DATE":joinDate
						
						})
			}
			

			var inputDTO = {
				dto: {
			        "userInfo": userInfo,
			        "companyProfileList": companyProfileList
			    }
			};
			
			Top.Ajax.execute({
				url : _workspace.url + "Admin/OrgUser",
				type : 'POST',
				dataType : "json",
				async : false,
				cache : false,
				data : JSON.stringify(inputDTO), // arg
				contentType : "application/json",
				crossDomain : true,
				xhrFields : {
				    withCredentials: true
				},
				headers: {
					"ProObjectWebFileTransfer":"true"
				},
				success : function(data) {
					Top.Dom.selectById('adminUserAddDialog').close();
					
					Top.Controller.get('adminOrgUserLayoutLogic').clickNode(MouseEvent,Top.Dom.selectById('TreeView1305'))
					
					
				},
				
				error : function(data){
					/*for(let errorLeng = 2;errorLeng<= $('#UserInfoAddPathLayout_right top-linearlayout button').length + 1 ; errorLeng++){
						if(Top.Dom.selectById("UserInfoAdd_OrgJobContent_"+errorLeng).getSelected() !== "팀원"){

							return;
						}
						
					}
					
					
					if(Top.Dom.selectById("UserInfoAdd_OrgJobContent").getSelected() !== "팀원"){

						return;
					}*/
						

					Top.Dom.selectById("UserInfoAdd_UserIDContent").setProperties({"border-color":"red"})
					Top.Dom.selectById("UserInfoAdd_UserIDInfoErrorIcon").setProperties({"visible":"visible","tooltip-top":"중복된 ID입니다."})
					return;

				}
			});
		
	},
	

	iconclick : function(event, widget) {
		let fieldid = widget.id;
		var icon = $("#" + widget.id).find("#" + widget.id);

		if(Top.Dom.selectById(fieldid).getProperties("password") == true){
			Top.Dom.selectById(fieldid).setProperties({"icon": "icon-ic_password_show", "password":"false"});
			// 보이기모드
			icon.attr('type','text');
		}else{
			Top.Dom.selectById(fieldid).setProperties({"icon": "icon-ic_password_hide", "password":"true"});
			//숨기기 모드
			icon.attr('type','password')
		}
	},
	//프로필 사진 저장
	choosefile_add : function(event, widget) {
		
			let fileChooser = Top.Device.FileChooser.create({
			    onBeforeLoad : function() {
			        let newFileList = [];
					let file = {};
					let tmpId = 0;
			        for(var i=0; i<this.file.length; i++) {		  
			            file = this.file[i];
			            
	    		        let loadFile = fileManager.onBeforeLoadFile(file); 
	    		        if(!loadFile.result) {
	    		            try {
	    						TeeToast.open({
	    				    		text: loadFile.message,
	    				    		size: 'min'
	    				    	});
	    		            } catch (e) {}
	    		
	    		        }
	    		        Top.Dom.selectById('Icon_admin_profile_error_add').setProperties({"visible":"none"})
	    		        let nomalizedFileFullName = Top.Util.Browser.isIE() ? file.name : file.name.normalize('NFC');        
	    		        let fileNameArray = nomalizedFileFullName.split('.');
	    		        let fileExt = fileNameArray.length == 1 ? "" : fileNameArray[fileNameArray.length - 1];        
	    		        if(fileExt == "jpeg" || fileExt == "png" || fileExt == "jpg" || fileExt == "JPEG" || fileExt == "PNG" || fileExt == "JPG") {
	    		    		
	    		        }else{
	    		        	try {
	    		        		Top.Dom.selectById("Icon_admin_profile_error_add").setProperties({"visible":"visible", "tooltip-top":"JPG, PNG 파일만 가능합니다."}); 
	    		        		return false;
	    						
	    		            } catch (e) {}
	    		
	    		        }
						
						newFileList.push({
	    		            file_id         : tmpId++,
	                        file_extension  : fileExt,
	                        fileFullName    : nomalizedFileFullName,
	                        file_status     : FileStatus.TRY_UPLOAD,
						});
	                }
			      
		        },
				onFileChoose : Top.Controller.get('adminUserInfoAddLogic').onFileSelected_add,
			    charset: "euc-kr",
			    multiple: true
		    });
			fileChooser.show();
			
		//}else{
			// 티 드라이브에서 열기
//			var config = {
//					buttonText1: '열기',
//					buttonText2: '취소',
//					buttonFn1: function(){},
//					buttonFn2: function(){},
//					onOpenFn: function(){},
//					onCloseFn: function(){},
//					onOverlayClickFn: function(){},
//			}
//			OPEN_TDRIVE(config);
		//}
		
	},	onFileSelected_add : function(fileChooser) {
		let src = fileChooser[0].src;
        Top.Dom.selectById("ImageButton_Admin_Profile_Setting_add").setSrc(src)
     
		//let _USER_ID =  userManager.getLoginUserId()
		let Fext = src.split('/')[1].split(";")[0];
		let srcSend = src.split(',')[1];
//		let inputDTO ={
//            	files : [{
//                    filename : 'profilePhoto.' + Fext,
//                    contents : srcSend,
//                }],
//		          dto: {
//		            USER_ID: user_id,
//		            PROFILE_PHOTO : src 
//		          }
//		     };

    }, delete_pic_add : function(event, widget){
    	
    	
		encodeImage(userManager.getUserDefaultPhotoURL(userManager.getLoginUserId()), deletePhoto_add(enImage))
        
    },

});





function adminProfile_list_add(e, clickID) {
	
	if(e.toElement != undefined){
		if(e.toElement.id == clickID){
			more_list_adminProfile_add =
				'<top-linearlayout id="Sidebar_drop_adminProfile_add" class="position-Sidebar_drop_adminProfile_add linear-child-vertical" layout-width="97px" layout-height="auto" layout-horizontal-alignment="center" padding="" margin="" horizontal-alignment="center" vertical-alignment="middle" visible="visible" border-color="rgba(205,205,207,1.0) rgba(205,205,207,1.0) rgba(205,205,207,1.0) rgba(205,205,207,1.0)" border-width="1px 1px 1px 1px" horizontal-scroll="false" vertical-scroll="false" orientation="vertical" position="false" title="" layout-tab-disabled="false">' +
				'<div id="Sidebar_drop_adminProfile_add" class="top-linearlayout-root">' +
					'<top-button id="Sidebar_drop_adminProfile_add_pc" class="noti_drop linear-child-vertical" layout-width="match_parent" layout-height="28px" padding="0px 0px 1px 10px" horizontal-alignment="left" border-color="rgba(205,205,207,1.0) rgba(205,205,207,1.0) rgba(205,205,207,1.0) rgba(205,205,207,1.0)" border-width="0px 0px 1px 0px" tab-index="0" text="PC" text-color="rgba(51,51,51,1.0)" text-decoration="" icon-position="left" title="" layout-tab-disabled="false" actual-height="29">' +
						'<button id="Sidebar_drop_adminProfile_add_pc" class="top-button-root" type="button" tabindex="0">' +
							'<label class="top-button-text">PC</label>' +
						'</button>' +
					'</top-button>' +
//					'<top-button id="Sidebar_drop2_tdrive" class="noti_drop linear-child-vertical" layout-width="match_parent" layout-height="28px" padding="0px 0px 1px 10px" horizontal-alignment="left" tab-index="0" text="T-Drive" text-color="rgba(51,51,51,1.0)" text-decoration="" icon-position="left" title="" layout-tab-disabled="false" actual-height="29">' +
//						'<button id="Sidebar_drop2_tdrive" class="top-button-root" type="button" tabindex="0">' +
//							'<label class="top-button-text">T-Drive</label>' +
//						'</button>' +
//					'</top-button>' +
					'<top-button id="Sidebar_drop_adminProfile_add_delete" class="noti_drop linear-child-vertical" layout-width="match_parent" layout-height="28px" padding="0px 0px 1px 10px" horizontal-alignment="left" border-color="rgba(205,205,207,1.0) rgba(205,205,207,1.0) rgba(205,205,207,1.0) rgba(205,205,207,1.0)" border-width="0px 0px 1px 0px" tab-index="0" text="삭제" text-color="rgba(51,51,51,1.0)" text-decoration="" icon-position="left" title="" layout-tab-disabled="false" actual-height="29">' +
						'<button id="Sidebar_drop_adminProfile_add_delete" class="top-button-root" type="button" tabindex="0">' +
							'<label class="top-button-text">삭제</label>' +
						'</button>' +
					'</top-button>' +
				'</div>' +
				'</top-linearlayout>';
			
			if($('top-linearlayout#Sidebar_drop_adminProfile_add').length == 0) {

				more_list_adminProfile_add = $(more_list_adminProfile_add);
				more_list_adminProfile_add.find("button#Sidebar_drop_adminProfile_add_pc").on('click', Top.Controller.get('adminUserInfoAddLogic').choosefile_add);	//pc
				//more_list_picture.find("button#Sidebar_drop2_tdrive").on('click', Top.Controller.get('userProfileLayoutLogic').choosefile);				//t-drive
				more_list_adminProfile_add.find("button#Sidebar_drop_adminProfile_add_delete").on('click', Top.Controller.get('adminUserInfoAddLogic').delete_pic_add);		//삭제

				$('div#UserInfoAdd_UserProfilePictureWrapper').append(more_list_adminProfile_add);
				//if(Top.Dom.selectById('sidebarLayout_tempo0_2').getProperties('visible') == 'visible'){
				$('.position-Sidebar_drop_adminProfile_add').css({'top':'57px','right':'25px'});
				//}	
			//else{
				//$('.position-Sidebar_drop2').css({'top':'56px','right':'59px'});
				//	}
				}
		}
		else{
				
				if($('top-linearlayout#Sidebar_drop_adminProfile_add').length != 0) {
			
				$('top-linearlayout#Sidebar_drop_adminProfile_add').remove();
			}
		};
	};
}

function encodeImage(src) {
    var canvas = document.createElement('canvas'),
        ctx = canvas.getContext('2d'),
        img = new Image();

    img.onload = function() {
        canvas.width = img.width;
        canvas.height = img.height;
        ctx.drawImage(img, 0, 0, img.width, img.height);
        //callback(canvas.toDataURL());
		console.log(canvas.toDataURL())
        enImage =  canvas.toDataURL();
		
    }
    img.src = src;
}
function deletePhoto_add(data){
	if($('#top-dialog-root_adminUserInfoViewDialog .top-dialog-title').text() === '조직원 정보'){
		Top.Dom.selectById('ImageButton_Admin_Profile_Setting_add').setSrc(userManager.getUserDefaultPhotoURL(Top.Controller.get('adminUserInfoViewLogic').user_id))
	}else if($('#top-dialog-root_adminUserAddDialog .top-dialog-title').text() === '조직원 개별 추가'){ 
		
		Top.Dom.selectById("ImageButton_Admin_Profile_Setting_add").setSrc(userManager.getUserDefaultPhotoURL(userManager.getLoginUserId()))	
	}
	
	

}

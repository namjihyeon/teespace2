
Top.Controller.create('adminUserTreeviewDialogLayoutLogic', {
	
	init : function(event, widget) {
//		orgUserRepo.orgUserList = []; 
		//트리뷰적용
		OrgManager.get(_mailDomain, 'ADM0021')
		.then(function(orgList) {
            	console.log('a',orgTreeRepo.orgTree);
            	Top.Dom.selectById("adminUserTreeView").setProperties({"nodes":orgTreeRepo.orgTree})
        		Top.Dom.selectById("adminUserTreeView").expandNode('all');
        		//drag&drop
        		//--------------------------------------	    		
		});
	
		//이벤트 선언
		if(Top.Controller.get('adminUserInfoAddLogic').bb == 0)
		Top.Dom.selectById("Button_Treeview_ok").setProperties({"on-click":"selectUserOk"})
		else 
		Top.Dom.selectById("Button_Treeview_ok").setProperties({"on-click":"changeUserOk"})
		
		Top.Dom.selectById('Button_Treeview_cancel').setProperties({'on-click':function(){Top.Dom.selectById('adminUserTreeviewDialog').close()}});
		
	},
	changeUserOk: function(event, widget){
		var dd = Top.Controller.get('adminUserInfoAddLogic').cc.split('_')[3];
		$.ajax({
			url : _workspace.url + "Admin/OrgPathGet?action=", 
            type: 'POST',
            dataType: 'json',
            crossDomain: true,
            contentType: 'application/json; charset=utf-8',
            xhrFields: {
                withCredentials: true
              },
            data: JSON.stringify({
              "dto": {
//                "COMPANY_CODE": adminGetTeamObjByName(Top.Dom.selectById("adminUserTreeView").getSelectedNode().text).COMPANY_CODE,
//                "DEPARTMENT_CODE": adminGetTeamObjByName(Top.Dom.selectById("adminUserTreeView").getSelectedNode().text).DEPARTMENT_CODE,
            	  "COMPANY_CODE": Top.Dom.selectById("adminUserTreeView").getSelectedNode().id.split('_')[0],
	                "DEPARTMENT_CODE": Top.Dom.selectById("adminUserTreeView").getSelectedNode().id.split('_')[1],
                "ORG_URL": adminGetTeamObjByName(Top.Dom.selectById("adminUserTreeView").getSelectedNode().text).ORG_URL,
                "ORG_TYPE": adminGetTeamObjByName(Top.Dom.selectById("adminUserTreeView").getSelectedNode().text).ORG_TYPE
              }
            }), 
            success: function(result) {
            	if(dd == null) $('#TextView_affiliation_path_0 label').text(result.dto.ORG_NAME);
    		 	else{
    		 		if($('#top-dialog-root_adminUserInfoViewDialog .top-dialog-title').text() === '조직원 정보'){
                        ///////////////////////////////////////////////
	            	       
	            	


    		 			$('#TextView_affiliation_path_'+dd+' label').text(result.dto.ORG_NAME)

    		 			  var abc = '#TextView_affiliation_path_'+dd;
	                        	$(abc).attr('CODE', Top.Dom.selectById("adminUserTreeView").getSelectedNode().id);
    		 		}else if($('#top-dialog-root_adminUserAddDialog .top-dialog-title').text() === '조직원 개별 추가'){ 



    		 		        Top.Dom.selectById('TextView_affiliation_path_'+ dd).setText(result.dto.ORG_NAME);

                                 var ab = '#TextView_affiliation_path_'+dd;
	                        	$(ab).attr('CODE', Top.Dom.selectById("adminUserTreeView").getSelectedNode().id);

    		 		        
                     
    		 		}
 
    		 	} 

    	

            	Top.Dom.selectById("adminUserTreeviewDialog").close()	
	        },
	        error: function(error) {        	
	        	
	        }
			});	
            
	},
	//확인 이벤트
	selectUserOk : function(event, widget) {
	     if($('#top-dialog-root_adminUserInfoViewDialog .top-dialog-title').text() === '조직원 정보'){

			$.ajax({
				url : _workspace.url + "Admin/OrgPathGet?action=", 
	            type: 'POST',
	            dataType: 'json',
	            crossDomain: true,
	            contentType: 'application/json; charset=utf-8',
	            xhrFields: {
	                withCredentials: true
	              },
	            data: JSON.stringify({
	              "dto": {
	            	"COMPANY_CODE": Top.Dom.selectById("adminUserTreeView").getSelectedNode().id.split('_')[0],
		            "DEPARTMENT_CODE": Top.Dom.selectById("adminUserTreeView").getSelectedNode().id.split('_')[1],
	                "ORG_URL": adminGetTeamObjByName(Top.Dom.selectById("adminUserTreeView").getSelectedNode().text).ORG_URL,
	                "ORG_TYPE": adminGetTeamObjByName(Top.Dom.selectById("adminUserTreeView").getSelectedNode().text).ORG_TYPE
	              }
	            }),
	            success: function(result) {
	     
	            	var countLayout = $('#UserInfoAddPathLayout_right top-linearlayout button').length + 1 -1 ;
	                var pathVal = new Array()
	                for(let i =0; i<result.dto.ORG_NAME.split(" > ").length ; i++){
	                	 pathVal[i] = result.dto.ORG_NAME.split(" > ")[i]
	                }
	            	//확인 눌렀을 시, 소속 입력되서  html 추가
	            	let html_1 = '<top-linearlayout id="LinearLayoutPathModify_'+countLayout+'" layout-width="wrap_content" layout-height="30px" layout-horizontal-alignment="left" border-width="0px" orientation="horizontal" position="false" title="" layout-tab-disabled="false" vertical-alignment="top" class="linear-child-vertical" style="text-align: left;" actual-height="30">'+ '<div id="LinearLayoutPathModify_'+countLayout+'" class="top-linearlayout-root" style="white-space: nowrap; height: 30px; border-width: 0px; border-style: solid; vertical-align: top; overflow: auto hidden; width: 100%; line-height: 30px;">'
						+'<top-textview id="TextView_affiliation_'+countLayout+'" layout-width="auto" layout-height="auto" layout-vertical-alignment="middle" border-width="0px" text="소속 '+(countLayout+1)+'" text-decoration="" title="" layout-tab-disabled="false" class="linear-child-horizontal" style="line-height: normal;" actual-width="38.1094">'
					+'<span id="TextView_affiliation_'+countLayout+'" class="top-textview-root" style="width: auto; height: auto; border-width: 0px; border-style: solid; vertical-align: middle;"><a class="top-textview-url">소속 '+(countLayout+1)+'</a><div class="top-textview-aligner middle"></div></span>'
				+'</top-textview>'
				+'<top-button id="TextView_affiliation_path_'+countLayout+'" on-click = "changeBtn" background-color="rgba(255,255,255,1.0)" layout-width="400px" layout-height="auto" layout-horizontal-alignment="center" margin="0px 0px 0px 10px" padding="0px 0px 1px 0px" border-color="rgba(194,194,194,1.0) rgba(194,194,194,1.0) rgba(194,194,194,1.0) rgba(194,194,194,1.0)" border-width="1px 1px 1px 1px"'
				+'tab-index="0" text="'+result.dto.ORG_NAME+'" text-color="rgba(0,0,0,1.0)" text-decoration="" icon-position="left" title="" layout-tab-disabled="false" class="linear-child-vertical" style="text-align: center;" actual-height="20">'
				+'<button id="TextView_affiliation_path_'+countLayout+'" class="top-button-root" type="button" style="width: 400px; height: auto; background-color: rgb(255, 255, 255); margin: 0px 0px 0px 10px; padding: 0px 0px 1px; color: rgb(0, 0, 0); border-width: 1px; border-style: solid; border-color: rgb(194, 194, 194); text-align: center;" tabindex="0"><label class="top-button-text">"'+result.dto.ORG_NAME+'"</label></button>'
				+'</top-button>'
				+'</div></top-linearlayout>';
	            	
	            	let html_2 ='<top-linearlayout id="LinearLayout_companyinfo_Modify_'+countLayout+'" layout-width="match_parent" layout-height="30px" layout-horizontal-alignment="left" border-width="0px" horizontal-scroll="false" vertical-scroll="false" orientation="horizontal" position="false" title="" layout-tab-disabled="false" vertical-alignment="top" class="linear-child-vertical" style="text-align: left;" actual-height="30">' 
	            		+'<div id="LinearLayout_companyinfo_Modify_'+countLayout+'" class="top-linearlayout-root" style="white-space: nowrap; height: 30px; border-width: 0px; border-style: solid; vertical-align: top; overflow: hidden; width: 100%; line-height: 30px;">'
//					+'<top-button id="UserInfoModify_changeBtn_'+countLayout+'" class="text-solid linear-child-horizontal" on-click = "changeBtn"  layout-width="70px" layout-height="27px" padding="0px 0px 0px 0px" tab-index="0" text="변경" text-decoration="" icon-position="left" title="" layout-tab-disabled="false" layout-vertical-alignment="top" style="line-height: normal;" actual-width="70">'
//					+'<button id="UserInfoModify_changeBtn_'+countLayout+'" class="top-button-root" type="button" style="width: 70px; height: 27px; padding: 0px; vertical-align: top;" tabindex="0"><label class="top-button-text">변경</label></button>'
//				+'</top-button>'
				+'<top-textfield id="UserInfoModify_UserCompanyIDContent_'+countLayout+'" layout-width="100px" layout-height="27px" margin="0px 0px 0px 10px" tab-index="0" text="" text-decoration="" hint="사번" prevent-key="false" password="false" required="false" icon-position="right" auto-complete-align="bl" title="" layout-tab-disabled="false" class="in_textfield click-placeholder linear-child-horizontal" layout-vertical-alignment="top" style="line-height: normal;" actual-width="140">'
					+'<div class="top-textfield-root" style="display: inline-block; width: 100px; height: 27px; margin: 0px 0px 0px 10px; vertical-align: top;">'
						+'<form autocomplete="off" onsubmit="return false;">'
							+'<input id="UserInfoModify_UserCompanyIDContent_'+countLayout+'" class="top-textfield-text pl-0 pr-0" type="text" value="" placeholder="사번" tabindex="0">'
							+'<span class="top-textfield-icon" style="display: none; vertical-align: baseline;"></span>'
						+'</form>'
					+'</div>'
				+'</top-textfield>'
				+'<top-icon id="UserInfoModify_UseOrgnumErrorIcon_'+countLayout+'" class="icon-work_noti_info linear-child-horizontal" layout-width="16px" layout-height="16px" margin="2px 4px 0px 4px" visible="hidden" border-width="0px" text-size="16px" text-color="#FF0000" title="" layout-tab-disabled="false" layout-vertical-alignment="top" style="line-height: normal;" actual-width="24"><span class="top-icon-root" id="UserInfoModify_UseOrgnumErrorIcon_'+countLayout+'" style="visibility: hidden; width: 16px; height: 16px; margin: 2px 4px 0px; color: rgb(255, 0, 0); font-size: 16px; border-width: 0px; border-style: solid; vertical-align: top;"><i class="top-icon-content" style="line-height: 16px;">'
				+'</i></span></top-icon>'
				+'<top-selectbox id="UserInfoModify_UserEmploymentTypeContent_'+countLayout+'" layout-width="100px" layout-height="27px" title ="고용 형태" layout-vertical-alignment="top" border-width="0px" horizontal-scroll="false" vertical-scroll="false" items="" separator-visible="true" virtual-scroll="false" item-clicktype="click" tab-index="0" title="" multi-select="false" searchable="false" layout-tab-disabled="false" class="linear-child-horizontal" style="line-height: normal;" actual-width="100">'
					+'<div id="UserInfoModify_UserEmploymentTypeContent_'+countLayout+'" class="top-selectbox-root" tabindex="0" style="width: 100px; height: 27px; border-width: 0px; border-style: solid; vertical-align: top; overflow: hidden;"></div>'
			+'</top-selectbox>'
				+'<top-selectbox id="UserInfoModify_UserJobContent_'+countLayout+'" layout-width="100px" layout-height="27px" title ="직위" layout-vertical-alignment="top" margin="0px 0px 0px 4px" border-width="0px" horizontal-scroll="false" vertical-scroll="false" items="" separator-visible="true" virtual-scroll="false" item-clicktype="click" tab-index="0" title="" multi-select="false" searchable="false" layout-tab-disabled="false" class="linear-child-horizontal" style="line-height: normal;" actual-width="104">'
					+'<div id="UserInfoModify_UserJobContent_'+countLayout+'" class="top-selectbox-root" tabindex="0" style="width: 100px; height: 27px; margin: 0px 0px 0px 4px; border-width: 0px; border-style: solid; vertical-align: top; overflow: hidden;"></div>'
			+'</top-selectbox>'
				+'<top-selectbox id="UserInfoModify_OrgJobContent_'+countLayout+'" layout-width="100px" layout-height="27px" title ="직책" layout-vertical-alignment="top" margin="0px 0px 0px 4px" border-width="0px" horizontal-scroll="false" vertical-scroll="false" items="" separator-visible="true" virtual-scroll="false" item-clicktype="click" tab-index="0" title="" multi-select="false" searchable="false" layout-tab-disabled="false" class="linear-child-horizontal" style="line-height: normal;" actual-width="104">'
					+'<div id="UserInfoModify_OrgJobContent_'+countLayout+'" class="top-selectbox-root" tabindex="0" style="width: 100px; height: 27px; margin: 0px 0px 0px 4px; border-width: 0px; border-style: solid; vertical-align: top; overflow: hidden;"></div>'
			+'</top-selectbox>'
			+`<top-checkbox id="orgking_`+countLayout+`" layout-width="auto" layout-height="auto" layout-vertical-alignment="middle" tab-index="0" text-decoration="" check-position="left" title="" layout-tab-disabled="false" class="linear-child-horizontal" style="line-height: normal;" actual-width="NaN">
			<input class="top-checkbox-check" type="checkbox" size="width:300px;" tabindex="0" style="display: inline-block;">
			<label class="top-checkbox-text left" for="checkbox" style="width: auto; height: auto; vertical-align: middle; padding-right: 0px; padding-top:7px;"><i class="top-checkbox-icon"></i><span></span></label>
			</top-checkbox>`
			+'</div></top-linearlayout>';

	            		
	            	///////////////////////////////////////////////
	            
	            	
	            	Top.Dom.selectById("UserInfoAddPathLayout_right").append(html_1)  
	            	Top.Dom.selectById("UserInfoAddPathLayout_right").append(html_2)
//	        		Top.Dom.selectById("UserInfoAddPathLayout_left").complete();
	            	Top.Dom.selectById("UserInfoAddPathLayout_right").complete();
	            	var abc = '#TextView_affiliation_path_'+countLayout;
	            	$(abc).attr('CODE', Top.Dom.selectById("adminUserTreeView").getSelectedNode().id);
	            	setTimeout(function() {
	    	       		var additional_job_selectbox = Top.Dom.selectById("UserInfoModify_UserJobContent_" + countLayout);
	    	        	var additional_position_selectbox = Top.Dom.selectById("UserInfoModify_OrgJobContent_" + countLayout);
	    	        	var additional_emptype_selectbox = Top.Dom.selectById("UserInfoModify_UserEmploymentTypeContent_" + countLayout);
	    	        	additional_job_selectbox.setProperties({"nodes": Top.Controller.get('adminUserInfoAddLogic').user_job_list});
	    	        	additional_position_selectbox.setProperties({"nodes": Top.Controller.get('adminUserInfoAddLogic').user_position_list}); 
	    	        	additional_emptype_selectbox.setProperties({"nodes": Top.Controller.get('adminUserInfoAddLogic').user_emptype_list}); 
	    	       	}, 0)	
	            	Top.Dom.selectById("adminUserTreeviewDialog").close()	
	            	
		        },
		        error: function(error) {        	
		        	
		        }
				});	
			
		
		     }


		     else if($('#top-dialog-root_adminUserAddDialog .top-dialog-title').text() === '조직원 개별 추가'){ 
	    		 		        

	            $.ajax({
				url : _workspace.url + "Admin/OrgPathGet?action=", 
	            type: 'POST',
	            dataType: 'json',
	            crossDomain: true,
	            contentType: 'application/json; charset=utf-8',
	            xhrFields: {
	                withCredentials: true
	              },
	            data: JSON.stringify({
	              "dto": {
	            	"COMPANY_CODE": Top.Dom.selectById("adminUserTreeView").getSelectedNode().id.split('_')[0],
		            "DEPARTMENT_CODE": Top.Dom.selectById("adminUserTreeView").getSelectedNode().id.split('_')[1],
	                "ORG_URL": adminGetTeamObjByName(Top.Dom.selectById("adminUserTreeView").getSelectedNode().text).ORG_URL,
	                "ORG_TYPE": adminGetTeamObjByName(Top.Dom.selectById("adminUserTreeView").getSelectedNode().text).ORG_TYPE
	              }
	            }),
	            success: function(result) {
	            	 Top.Controller.get('adminUserInfoAddLogic').aa =  Top.Controller.get('adminUserInfoAddLogic').aa +1;
	            	var countLayout = Top.Controller.get('adminUserInfoAddLogic').aa;
	                var pathVal = new Array()
	                for(let i =0; i<result.dto.ORG_NAME.split(" > ").length ; i++){
	                	 pathVal[i] = result.dto.ORG_NAME.split(" > ")[i]
	                }
	            	//확인 눌렀을 시, 소속 입력되서  html 추가
	            	let html_1 = '<top-linearlayout id="LinearLayoutPath_'+countLayout+'" layout-width="wrap_content" layout-height="50px" layout-horizontal-alignment="left" border-width="0px" orientation="vertical" position="false" title="" layout-tab-disabled="false" vertical-alignment="top" class="linear-child-vertical" style="text-align: left;" actual-height="30">'+ '<div id="LinearLayoutPath_'+countLayout+'" class="top-linearlayout-root" style="white-space: nowrap; height: 30px; border-width: 0px; border-style: solid; vertical-align: top; overflow: auto hidden; width: 100%; line-height: 30px;">'
						+'<top-textview id="TextView_affiliation_'+countLayout+'" layout-width="calc(100% - 0px)" layout-height="auto" layout-vertical-alignment="middle" border-width="0px" text="소속 '+countLayout+'" text-decoration="" title="" layout-tab-disabled="false" class="linear-child-horizontal" style="line-height: normal;" actual-width="38.1094">'
					+'<span id="TextView_affiliation_'+countLayout+'" class="top-textview-root" style="width: auto; height: auto; border-width: 0px; border-style: solid; vertical-align: middle;"><a class="top-textview-url">소속 '+countLayout+'</a><div class="top-textview-aligner middle"></div></span>'
				+'</top-textview>'
				+'<top-button id="TextView_affiliation_path_'+countLayout+'" on-click = "changeBtn" background-color="rgba(255,255,255,1.0)" layout-width="400px" layout-height="auto" layout-horizontal-alignment="center" margin="0px 0px 0px 10px" padding="0px 0px 1px 0px" border-color="rgba(194,194,194,1.0) rgba(194,194,194,1.0) rgba(194,194,194,1.0) rgba(194,194,194,1.0)" border-width="1px 1px 1px 1px"'
				+'tab-index="0" text="'+result.dto.ORG_NAME+'" text-color="rgba(0,0,0,1.0)" text-decoration="" icon-position="left" title="" layout-tab-disabled="false" class="linear-child-vertical" style="text-align: center;" actual-height="20">'
				+'<button id="TextView_affiliation_path_'+countLayout+'" class="top-button-root" type="button" style="width: 400px; height: auto; background-color: rgb(255, 255, 255); margin: 0px 0px 0px 10px; padding: 0px 0px 1px; color: rgb(0, 0, 0); border-width: 1px; border-style: solid; border-color: rgb(194, 194, 194); text-align: center;" tabindex="0"><label class="top-button-text">"'+result.dto.ORG_NAME+'"</label></button>'
				+'</top-button>'
				+'</div></top-linearlayout>';
	            	
	            	let html_2 ='<top-linearlayout id="LinearLayout_companyinfo_'+countLayout+'" layout-width="match_parent" layout-height="30px" layout-horizontal-alignment="left" border-width="0px" horizontal-scroll="false" vertical-scroll="false" orientation="horizontal" position="false" title="" layout-tab-disabled="false" vertical-alignment="top" class="linear-child-vertical" style="text-align: left;" actual-height="30">' 
	            		+'<div id="LinearLayout_companyinfo_'+countLayout+'" class="top-linearlayout-root" style="white-space: nowrap; height: 30px; border-width: 0px; border-style: solid; vertical-align: top; overflow: hidden; width: 100%; line-height: 30px;">'
//					+'<top-button id="UserInfoAdd_changeBtn_'+countLayout+'" class="text-solid linear-child-horizontal" on-click = "changeBtn"  layout-width="70px" layout-height="27px" padding="0px 0px 0px 0px" tab-index="0" text="변경" text-decoration="" icon-position="left" title="" layout-tab-disabled="false" layout-vertical-alignment="top" style="line-height: normal;" actual-width="70">'
//					+'<button id="UserInfoAdd_changeBtn_'+countLayout+'" class="top-button-root" type="button" style="width: 70px; height: 27px; padding: 0px; vertical-align: top;" tabindex="0"><label class="top-button-text">변경</label></button>'
//				+'</top-button>'
				+'<top-textfield id="UserInfoAdd_UserCompanyIDContent_'+countLayout+'" layout-width="100px" layout-height="27px" margin="0px 0px 0px 10px" tab-index="0" text="" text-decoration="" hint="사번" prevent-key="false" password="false" required="false" icon-position="right" auto-complete-align="bl" title="" layout-tab-disabled="false" class="in_textfield click-placeholder linear-child-horizontal" layout-vertical-alignment="top" style="line-height: normal;" actual-width="140">'
					+'<div class="top-textfield-root" style="display: inline-block; width: 100px; height: 27px; margin: 0px 0px 0px 10px; vertical-align: top;">'
						+'<form autocomplete="off" onsubmit="return false;">'
							+'<input id="UserInfoAdd_UserCompanyIDContent_'+countLayout+'" class="top-textfield-text pl-0 pr-0" type="text" value="" placeholder="사번" tabindex="0">'
							+'<span class="top-textfield-icon" style="display: none; vertical-align: baseline;"></span>'
						+'</form>'
					+'</div>'
				+'</top-textfield>'
				+'<top-icon id="UserInfoAdd_UseOrgnumErrorIcon_'+countLayout+'" class="icon-work_noti_info linear-child-horizontal" layout-width="16px" layout-height="16px" margin="2px 4px 0px 4px" visible="hidden" border-width="0px" text-size="16px" text-color="#FF0000" title="" layout-tab-disabled="false" layout-vertical-alignment="top" style="line-height: normal;" actual-width="24"><span class="top-icon-root" id="UserInfoAdd_UseOrgnumErrorIcon_'+countLayout+'" style="visibility: hidden; width: 16px; height: 16px; margin: 2px 4px 0px; color: rgb(255, 0, 0); font-size: 16px; border-width: 0px; border-style: solid; vertical-align: top;"><i class="top-icon-content" style="line-height: 16px;">'
				+'</i></span></top-icon>'
				+'<top-selectbox id="UserInfoAdd_UserEmploymentTypeContent_'+countLayout+'" layout-width="100px" layout-height="27px" title ="고용 형태" layout-vertical-alignment="top" border-width="0px" horizontal-scroll="false" vertical-scroll="false" items="" separator-visible="true" virtual-scroll="false" item-clicktype="click" tab-index="0" title="" multi-select="false" searchable="false" layout-tab-disabled="false" class="linear-child-horizontal" style="line-height: normal;" actual-width="100">'
					+'<div id="UserInfoAdd_UserEmploymentTypeContent_'+countLayout+'" class="top-selectbox-root" tabindex="0" style="width: 100px; height: 27px; border-width: 0px; border-style: solid; vertical-align: top; overflow: hidden;"></div>'
			+'</top-selectbox>'
				+'<top-selectbox id="UserInfoAdd_UserJobContent_'+countLayout+'" layout-width="100px" layout-height="27px" title ="직위" layout-vertical-alignment="top" margin="0px 0px 0px 4px" border-width="0px" horizontal-scroll="false" vertical-scroll="false" items="" separator-visible="true" virtual-scroll="false" item-clicktype="click" tab-index="0" title="" multi-select="false" searchable="false" layout-tab-disabled="false" class="linear-child-horizontal" style="line-height: normal;" actual-width="104">'
					+'<div id="UserInfoAdd_UserJobContent_'+countLayout+'" class="top-selectbox-root" tabindex="0" style="width: 100px; height: 27px; margin: 0px 0px 0px 4px; border-width: 0px; border-style: solid; vertical-align: top; overflow: hidden;"></div>'
			+'</top-selectbox>'
				+'<top-selectbox id="UserInfoAdd_OrgJobContent_'+countLayout+'" layout-width="100px" layout-height="27px" title ="직책" layout-vertical-alignment="top" margin="0px 0px 0px 4px" border-width="0px" horizontal-scroll="false" vertical-scroll="false" items="" separator-visible="true" virtual-scroll="false" item-clicktype="click" tab-index="0" title="" multi-select="false" searchable="false" layout-tab-disabled="false" class="linear-child-horizontal" style="line-height: normal;" actual-width="104">'
					+'<div id="UserInfoAdd_OrgJobContent_'+countLayout+'" class="top-selectbox-root" tabindex="0" style="width: 100px; height: 27px; margin: 0px 0px 0px 4px; border-width: 0px; border-style: solid; vertical-align: top; overflow: hidden;"></div>'
			+'</top-selectbox>'
			+`<top-checkbox id="orgking_`+countLayout+`" layout-width="auto" layout-height="auto" layout-vertical-alignment="middle" tab-index="0" text-decoration="" check-position="left" title="" layout-tab-disabled="false" class="linear-child-horizontal" style="line-height: normal;" actual-width="NaN">
			<input class="top-checkbox-check" type="checkbox" size="width:300px;" tabindex="0" style="display: inline-block;">
			<label class="top-checkbox-text left" for="checkbox" style="width: auto; height: auto; vertical-align: middle; padding-right: 0px; padding-top: 7px;"><i class="top-checkbox-icon"></i><span></span></label>
			</top-checkbox>`+
			+'</div></top-linearlayout>';

	            		
	            	///////////////////////////////////////////////
	            	
	            	
	            	Top.Dom.selectById("UserInfoAddPathLayout_right").append(html_1) 
	            	Top.Dom.selectById("UserInfoAddPathLayout_right").append(html_2)
//	        		Top.Dom.selectById("UserInfoAddPathLayout_left").complete();
	            	Top.Dom.selectById("UserInfoAddPathLayout_right").complete();
	            	var ab = '#TextView_affiliation_path_'+countLayout;
	            	$(ab).attr('CODE', Top.Dom.selectById("adminUserTreeView").getSelectedNode().id);
	            	setTimeout(function() {
	    	       		var additional_job_selectbox = Top.Dom.selectById("UserInfoAdd_UserJobContent_" + countLayout);
	    	        	var additional_position_selectbox = Top.Dom.selectById("UserInfoAdd_OrgJobContent_" + countLayout);
	    	        	var additional_emptype_selectbox = Top.Dom.selectById("UserInfoAdd_UserEmploymentTypeContent_" + countLayout);
	    	        	additional_job_selectbox.setProperties({"nodes": Top.Controller.get('adminUserInfoAddLogic').user_job_list});
	    	        	additional_position_selectbox.setProperties({"nodes": Top.Controller.get('adminUserInfoAddLogic').user_position_list}); 
	    	        	additional_emptype_selectbox.setProperties({"nodes": Top.Controller.get('adminUserInfoAddLogic').user_emptype_list}); 
	    	       	}, 0)	
	            	
	            	
	            	Top.Dom.selectById("adminUserTreeviewDialog").close()		            	
		        },
		        error: function(error) {        	
		        	
		        }
				});	












	    		 		        
	                     
	    		 		}
		}

	});

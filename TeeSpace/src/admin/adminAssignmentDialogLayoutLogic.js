
Top.Controller.create('adminAssignmentDialogLayoutLogic', {
	init : function(event, widget) {
		
		if(Top.Dom.selectById('TextField_org_cap')){
			$('#top-dialog-root_adminAssignmentDialog .top-dialog-title-layout .top-dialog-title').text("조직원 권한 양도")
		}
		else{
			$('#top-dialog-root_adminAssignmentDialog .top-dialog-title-layout .top-dialog-title').text("조직도 및 조직원")
		}
		
		//커밋테스트
		Top.Dom.selectById("TextField_search_member").setProperties({"icon":"icon-search"}) 
		$('#TextField_search_member .top-textfield-icon').bind('click',function(){
			Top.Controller.get("adminAssignmentDialogLayoutLogic").searchOrgName(Top.Dom.selectById('TextField_search_member').getText());
		})
		Top.Dom.selectById("Button_assign_ok").setProperties({"on-click":"ok_btn"})
		Top.Dom.selectById("Button_assign_cancel").setProperties({"on-click":"cancel_btn"})
		 var selected_id;
		
		
		//초기화
		orgUserRepo.orgUserList = [];
		//트리뷰적용
		var orgTree = Top.Dom.selectById("TreeView_admin_assign");
		createOrgTree(orgTree, "ADM0021");
		
		//검색창 x표시
		Top.Dom.selectById("TextField_search_member").setProperties({
    		'clear' : 'true',
    		'on-clear' : this.cancelSearch
    	});
			//다이얼로그 닫을 때 동작

    	if(Top.Dom.selectById('TextField_org_cap')){
    		//다이얼로그 닫을 때 동작
            $('.top-dialog-close').click(function() { 
            Top.Dom.selectById("TextView_show_cap").setProperties({"visible":"visible"})
            Top.Dom.selectById("Button_org_cap").setProperties({"visible":"none"})
            Top.Dom.selectById("TextField_org_cap").setProperties({"visible":"none"})
            Top.Dom.selectById("Button_org_cap_change").setProperties({"visible":"visible"}) 
	   });
    	}
	},
	
	//트리뷰 클릭 테이블 띄우기
	clickNode : function(event, widget) {
		var clickedNode = widget.getSelectedNode();
		var aff_code = clickedNode.id;
		
		var adminInputDTO = {
				"dto" : {
					"COMPANY_CODE" : aff_code.split("_")[0],
					"DEPARTMENT_CODE" : aff_code.split("_")[1]
				}
		};

		setOrgUserRepoIns = function(orgUserList){		
			orgUserRepo.reset();
			
			for(var i = 0; i < orgUserList.length; i++) {			
				var orgUserInfo = {
						user_id: orgUserList[i].USER_ID,
						user_name: orgUserList[i].USER_NAME,
						org_name: clickedNode.text,
						job: orgUserList[i].USER_JOB,
						
						com_num: orgUserList[i].USER_COM_NUM,
						email: orgUserList[i].USER_EMAIL,
						status: orgUserList[i].USER_EMPLOYMENT_TYPE				
				}; // 유저 정보 instance

				orgUserRepo.orgUserList.push(orgUserInfo); 
			}
		}
		
		let outputdata = null;
		Top.Ajax.execute({
			url : _workspace.url + "Admin/OrgUserList",
			type : 'GET',
			dataType : "json",
			async : false,
			cache : false,
			data : JSON.stringify(adminInputDTO), // arg
			contentType : "application/json",
			crossDomain : true,
			xhrFields : {
			    withCredentials: true
			},
			headers: {
				"ProObjectWebFileTransfer":"true"
			},
			success : function(data) {
				data = JSON.parse(data);
				if(data.dto == undefined) {
				    outputdata = {
				        result : "Success",
				        message : data.exception.name + ' ' + data.exception.message,
				        data : data
				    }
				} else { 
				    outputdata = data.dto;
				    setOrgUserRepoIns(outputdata.orgUserList);
				    
				 
				    
				    var table = Top.Dom.selectById("TableView_org_assignment"); 
				    table.update();
				   
				    Top.Dom.selectById("TextView_member_count").setText(String.format("{0}명", orgUserRepo.orgUserList.length));
				    orgUserRepo.setValue("ChooseList", orgUserRepo.orgUserList)
				    
				}
	            console.info(outputdata);
			},
			error : function(data) {
	            outputdata = {
	                result : "Fail",
	                message : data.exception != undefined ? data.exception.message : "Error",
	                data : data
	            }		
	            console.info(outputdata);
			}
		});	
	},
	

	//검색창 x아이콘 동작
	cancelSearch : function(event, widget){
		let List_1 = orgUserRepo.ChooseList;
		let List_2 = JSON.parse(JSON.stringify(List_1));	
	    orgUserRepo.orgUserList = List_2;
		Top.Dom.selectById("TableView_org_assignment").update();
				    
	    },
	 onKeyTyped : function(event, widget) {
	    	if(event.key == "Enter" ){
	    		this.searchOrgName(widget.getText());
	    	}
	    }, 
	    
	 searchOrgName : function(searchName){
		 	

	    	if(searchName.length == 0 ){
	    		TeeAlarm.open({title: '1글자 이상 입력하세요.', content: ''})
	    	}
	    	
	    	var searchNametoLowerCase = searchName.toLowerCase();
	    	
	    		
	    		let searchList = orgUserRepo.ChooseList;
	    		searchList = searchList.filter(function(e) {
	    			return e.user_name.toLowerCase().indexOf(searchNametoLowerCase) != -1;
	    		})	    	
		    	
	    	let resultList = JSON.parse(JSON.stringify(searchList));	
	    	orgUserRepo.orgUserList = resultList;
	    	
	    	Top.Dom.selectById("TableView_org_assignment").update()
	    	
	   },
	
	   ok_btn : function(event, widget){
		   
		   
		   var selectedMember;
		  
		   selectedMember = Top.Dom.selectById("TableView_org_assignment").getSelectedData().user_name
		   var selectedid = Top.Dom.selectById('TableView_org_assignment').getSelectedData().user_id
		   if(selectedMember === undefined) {
			   	  TeeAlarm.open({title: '조직원을 선택해주세요.', content: ''})
				  return;
			  }
		  selected_id =  orgUserRepo.orgUserList[Top.Dom.selectById("TableView_org_assignment").getSelectedIndex()].user_id
		 
		  if(Top.Dom.selectById('TextField_org_cap')){
			  Top.Dom.selectById("TextField_org_cap").setText(selectedMember);
			   Top.Dom.selectById("TextView_show_cap").setProperties({"visible":"none"})
			  
		  }else{
			  Top.Dom.selectById('subOrgAdminNameView').setText(selectedMember)
			  $('#subOrgAdminNameView').attr('uid',selectedid);
		  }
		  
		   Top.Dom.selectById("adminAssignmentDialog").close()
	
	   },

	   cancel_btn : function(event,widget){
		   if(Top.Dom.selectById('TextField_org_cap')){
			   Top.Dom.selectById("TextView_show_cap").setProperties({"visible":"visible"})
			   Top.Dom.selectById("Button_org_cap").setProperties({"visible":"none"})
			   Top.Dom.selectById("TextField_org_cap").setProperties({"visible":"none"})
			   Top.Dom.selectById("Button_org_cap_change").setProperties({"visible":"visible"})
			   
		   }else{
			   
		   }
		  
		   Top.Dom.selectById("adminAssignmentDialog").close()
	   }

});

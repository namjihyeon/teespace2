
Top.Controller.create('adminCompanyInfoManageLayoutLogic', {
	
	init : function(event, widget) {
		
		Top.Dom.selectById('company_LinearLayout_bottom').src('adminCompanyJobPosSettingLayout.html' + verCsp());
		admin.setLnbColor();
	}, 
	
	toggleSelectedButton : function(index) {
		var buttons = [
				'Button_default_info_setting',
				'Button_free_func_setting',
				'Button_job_pos_setting',
				'Button_emp_type_setting'
			];
		
		Top.Dom.selectById(buttons[index]).removeClass('companyinfo-tab');
		Top.Dom.selectById(buttons[index]).addClass('companyinfo-tabactive');
		
		for(let i = 0; i < buttons.length; i++) {
			if(i === index)
				continue;
			else {
				if(Top.Dom.selectById(buttons[i]).hasClass('companyinfo-tabactive')) {
					Top.Dom.selectById(buttons[i]).removeClass('companyinfo-tabactive');
					Top.Dom.selectById(buttons[i]).addClass('companyinfo-tab');
				}
			}
		}
	},
	
	default_setting_click : function(event, widget) {
		this.toggleSelectedButton(0);
		
		Top.Dom.selectById('company_LinearLayout_bottom').src('adminCompanyDefaultSettingLayout.html' + verCsp());
	}, 
	
	free_func_setting_click : function(event, widget) {
		this.toggleSelectedButton(1);
		
//		Top.Dom.selectById('company_LinearLayout_bottom').src('adminCompanyDefaultSettingLayout.html' + verCsp());
	}, 
	
	job_pos_setting_click : function(event, widget) {
		this.toggleSelectedButton(2);
		
		Top.Dom.selectById('company_LinearLayout_bottom').src('adminCompanyJobPosSettingLayout.html' + verCsp());
	}, 
	
	emp_type_setting_click : function(event, widget) {
		this.toggleSelectedButton(3);
		
		Top.Dom.selectById('company_LinearLayout_bottom').src('adminCompanyEmpTypeSettingLayout.html' + verCsp());
	}
	
	,
	
	



});

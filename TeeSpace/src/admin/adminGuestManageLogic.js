

Top.Controller.create('adminGuestManageLayoutLogic',  {	
	init: function(){	
		Top.Dom.selectById("guestGroupTreeView").setProperties({
            nodes : [],
            option : {
            	node: {
                    checkable    : false,
                    editable     : false,
                    deletable    : false,
                    draggable    : false,
                    checkInherit : true
                },
                view : {
                    expand : true,
                    menu : {
                    	addLowGuestGroup : this.addLowGuestGroup,
                    	editGroupName    : this.editGuestGroupName,
                    	deleteGroup      : this.deleteGuestGroup
                    }, 
                    language : {
//                    	addLowGuestGroup : "하위 조직 추가",
//                    	editGroupName    : "조직명 수정",
//                    	deleteGroup      : "조직 삭제"
                    		addLowGuestGroup : "<span class='icon-add'></span> 하위 그룹 추가",
                        	editGroupName    : "<span class='icon-info'></span> 조직명 수정",
                        	deleteGroup      : "<span class='icon-delete'></span> 조직 삭제"
                    }
                },
                event: { 
                    onNodeclick     : this.onTreeNodeClick,
                    onNodeedit      : null,
                    onNodehover     : null,
                    onNodecheck     : null,
                    onNodedblclick  : null,
                    onNodedrag      : null,
                    onNodedragstart : null,
                    onNodedragover  : null,
                    onNodedrop      : null,
                    onNodedelete    : null,
                    onExpand        : null,
                    onCollapse      : null,
                }
            }
        });
		
		createOrgTree(Top.Dom.selectById("guestGroupTreeView"), "ADM0022");
		 
		
		
		
		Top.Dom.selectById("nameSearchTextField").setProperties({
    		'clear' : 'true',
    		
    		
    		'on-clear' : this.cancelSearch
    	});
		
		guestRepo.guestList = [];
		guestRepo.guest_backup = [];
		
		admin.setLnbColor();
	},
	
	treeNodeClick : function(event, widget, nodeObj) {
	    Top.Dom.selectById("tvSelectedGuestGroup").setText(nodeObj.text);
	    
	    let companyCode = nodeObj.id.split("_")[0];
	    let departmentCode = nodeObj.id.split("_")[1];
	    Top.Controller.get("adminGuestManageLayoutLogic").updateGuestTable(companyCode, departmentCode);
        Top.Dom.selectById("tvNumOfGuest").setText(String.format("{0}명", guestRepo.guestList.length));
	},

    
    addLowGuestGroup : function(widget, nodeObj) {
        Top.Controller.get('adminAddLowGuestGroupLayoutLogic').parentOrgId = nodeObj.id;
        Top.Dom.selectById("adminAddLowGuestGroupDialog").open();
    },
    
	deleteGuestGroup : function(widget, nodeObj) {
	    let inputDTO = {
            dto : {
            	orgList : [{
            		ORG_URL			: _mailDomain,
                    ORG_TYPE        : "ADM0022",
                    COMPANY_CODE    : nodeObj.id.split("_")[0],
                    DEPARTMENT_CODE : nodeObj.id.split("_")[1]
            	}]
            }
	    }
	    
	    Top.Ajax.execute({
            url : _workspace.url + "Admin/Org",
            type : 'DELETE',
            dataType : "json",
            async : false,
            cache : false,
            data : JSON.stringify(inputDTO), // arg
            contentType : "application/json",
            crossDomain : true,
            xhrFields : {
                withCredentials: true
            },
            success : function(data) {
                Top.Dom.selectById("guestGroupTreeView").removeNodes(nodeObj.id);
            },
            error : function(data) {
            }
        });
	},
	
	updateGuestTable : function(companyCode, departmentCode) {
        var adminInputDTO = {
            "dto" : {
                "COMPANY_CODE"    : companyCode,
                "DEPARTMENT_CODE" : departmentCode
            }
        };
        
        Top.Ajax.execute({
            url : _workspace.url + "Admin/OrgGuestList",
            type : 'GET',
            dataType : "json",
            async : false,
            cache : false,
            data : JSON.stringify(adminInputDTO), // arg
            contentType : "application/json",
            crossDomain : true,
            xhrFields : {
                withCredentials: true
            },
            success : function(data) {
                data = JSON.parse(data);
                outputdata = data.dto;
                if(outputdata.orgGuestList == undefined) {
                    guestRepo.reset();
                    return;
                }
                
                for(var i=0; i<outputdata.orgGuestList.length; i++) {
                    outputdata.orgGuestList[i].COMPANY_CODE = companyCode;
                    outputdata.orgGuestList[i].DEPARTMENT_CODE = departmentCode;
                    if(outputdata.orgGuestList[i].ORG_NAME == null)
                    	outputdata.orgGuestList[i].ORG_NAME = '소속없음';
                }
                
                guestRepo.setValue("guestList", outputdata.orgGuestList)
                guestRepo.setValue("guest_backup", outputdata.orgGuestList)

            },
            error : function(data) {
            }
        });
	},
	
	editGuestGroupName : function(widget, nodeObj) {
		Top.Controller.get('adminEditGuestGroupNameLayoutLogic').orgId = nodeObj.id;
		Top.Dom.selectById("adminEditGuestGroupNameDialog").open();
	},
	
	addOne : function(event, widget) {
	    if(Top.Dom.selectById("guestGroupTreeView").getSelectedNode() == undefined) {
	    	TeeAlarm.open({title: '게스트 그룹을 선택해주세요.', content: ''})
	        return;
	    }
	    
	    var adminAddOneGuestLayoutLogic = Top.Controller.get("adminAddOneGuestLayoutLogic");
//	    adminAddOneGuestLayoutLogic.isDetailMode = false;

	    var adminAddOneGuestDialog = Top.Dom.selectById("adminAddOneGuestDialog"); 
        adminAddOneGuestDialog.setProperties({ title : "게스트 개별 추가" })
        adminAddOneGuestDialog.setProperties({ dialogLayout : "adminAddOneGuestLayout"});
		adminAddOneGuestDialog.open();
	}, 
	
	addMany : function(event, widget) {
		Top.Dom.selectById("adminAddManyGuestDialog").open();
	},
	
	openGuestDetailInfo : function(event, widget) {
	    var adminAddOneGuestLayoutLogic = Top.Controller.get("adminAddOneGuestLayoutLogic");
	    adminAddOneGuestLayoutLogic.isDetailMode = true;
	    adminAddOneGuestLayoutLogic.selectedGuestId = Top.Dom.selectById("guestGroupTableView").getSelectedData().USER_ID;
	    
	    var adminAddOneGuestDialog = Top.Dom.selectById("adminAddOneGuestDialog"); 
        adminAddOneGuestDialog.setProperties({ title : "게스트 정보" });
        adminAddOneGuestDialog.setProperties({ dialogLayout : "adminGuestModificationLayout"});
        adminAddOneGuestDialog.open();
	},
	
	deleteGuest : function(event, widget) {
	    let selectedUser = Top.Dom.selectById("guestGroupTableView").getSelectedData()
	    let inputDTO = {
            dto : {
                orgGuestList : [{
                    USER_ID         : selectedUser.USER_ID,
                    COMPANY_CODE    : selectedUser.COMPANY_CODE,
                    DEPARTMENT_CODE : selectedUser.DEPARTMENT_CODE
                }]
            }
        }
    
	    Top.Ajax.execute({
            url : _workspace.url + "Admin/OrgGuest",
            type : 'DELETE',
            dataType : "json",
            async : false,
            cache : false,
            data : JSON.stringify(inputDTO), // arg
            contentType : "application/json",
            crossDomain : true,
            xhrFields : {
                withCredentials: true
            },
            success : function(data) {
                let newGuestList = guestRepo.guestList.filter(function(e) { return e != selectedUser });
                guestRepo.setValue("guestList", newGuestList);
                Top.Dom.selectById("tvNumOfGuest").setText(String.format("{0}명", guestRepo.guestList.length));
            },
            error : function(data) {
            }
        });
    }, 
      
    cancelSearch : function(event, widget){
    	var guestTreeView = Top.Dom.selectById("guestGroupTreeView"); 
    	let companyCode = guestTreeView.getSelectedNode().id.split("_")[0];
	    let departmentCode = guestTreeView.getSelectedNode().id.split("_")[1];
	    Top.Controller.get("adminGuestManageLayoutLogic").updateGuestTable(companyCode, departmentCode);
    },
    
    onKeyTyped : function(event, widget) {
    	if(event.key == "Enter"){
    		this.searchByName(widget.getText());
    	}
    },
    
    searchByName : function(searchName){
    	if(searchName.length == 0 ){
    		TeeAlarm.open({title: '1글자 이상 입력하세요.', content: ''})
    	}
    	
    	var searchNametoLowerCase = searchName.toLowerCase();
    	
    	if(Top.Dom.selectById("guestGroupTreeView").getSelectedNode() == undefined){
    		TeeAlarm.open({title: '게스트 그룹을 선택해주세요.', content: ''})
    		return;
    	}
    	
    	
    	
    	else{	
    		let searchList = guestRepo.guest_backup;
    		searchList = searchList.filter(function(e) {
    			return e.USER_NAME.toLowerCase().indexOf(searchNametoLowerCase) != -1;
    		})	    	
	    	
    	let resultList = JSON.parse(JSON.stringify(searchList));	
        guestRepo.guestList = resultList;
    	guestRepo.update("guestList");

    	}   	
    }
});


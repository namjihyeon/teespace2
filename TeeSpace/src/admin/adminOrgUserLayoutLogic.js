//drag&drop-----------------------------------------------------------
function dragStart(event){
	console.info("123");
}
function dropend(event){
	var clickdata = Top.Dom.selectById('OrgUserTableView').getClickedData();
	var moveorg = event.target.id;
	var affcode = orgUserRepo.orgUserList[0].affcode;
	if(moveorg == affcode) return;
	Top.Ajax.execute({
        url: _workspace.url+"Admin/OrgUserTransfer?action=", //Service Object
        type: 'POST',
        dataType: 'json',
        crossDomain: true,
        contentType: 'application/json; charset=utf-8',
        xhrFields: {
            withCredentials: true
          },
        data: JSON.stringify({
          "dto": {
        	  "userTransferList": [
                  {
                      "USER_ID":clickdata.USER_ID,
                      "companyProfile" : {
                          "USER_AFF":affcode,
                          "USER_TRANSFER_AFF":moveorg 
                      }
                  }
              ]
          }
        }),
        success: function(result) {
        	if(result.dto.RESULT_CD === 'RST0002') return;
        	var len = orgUserRepo.orgUserList.length;
        	var tmp = [];
        	for(var i= 0 ; i < len; i++){
        		if(orgUserRepo.orgUserList[i].USER_ID != Top.Dom.selectById('OrgUserTableView').getClickedData().USER_ID){
        			tmp.push(orgUserRepo.orgUserList[i]);
        		}
        	}
        	orgUserRepo.orgUserList = tmp ;
			Top.Dom.selectById('OrgUserTableView').update();
			Top.Dom.selectById('OrgUserTableView').render();
//		    $(".body-row").attr("draggable",true);
//		    $(".body-row").attr("ondragstart","dragStart(event)");
        },
        error: function(error) {
        }
     });
}
function allowDrop(event){
	event.preventDefault();
}
//-------------------------------------------------------------------------
function createOrgTree(widget, orgType, callback) {

	orgTreeRepo.reset();

	OrgManager.get(_mailDomain, orgType)
		.then(function(orgList) {
			if(document.querySelector('div#TreeView1305') || document.querySelector('div#guestGroupTreeView') || document.querySelector('div#TreeView_admin_assign') || document.querySelector('div#orgTreeTableView')) {
        		setOrgRepoIns(orgList, 0);
        		if(widget == 'orgTreeTable') {
        			if (callback !== undefined) {
        				callback();
        				return;
        			}
        		}
            	if(widget == null)
            		return;
            	console.log('a',orgTreeRepo.orgTree);
        		widget.setProperties({"nodes":orgTreeRepo.orgTree})
        		widget.expandNode('all');
        		
        		
        		//drag&drop
//        		$('.top-treeview-item').attr("ondrop","dropend(event)");
//        		$('.top-treeview-item').attr("ondragover","allowDrop(event)");
        		//--------------------------------------	
        	}    		
		});
	
	
	 setOrgRepoIns = function(orgGroup, step){	
		var output = [];
		for(var i = 0; i < orgGroup.length; i++) {			
			var orgInfo = {
					COMPANY_CODE: orgGroup[i].COMPANY_CODE,
					DEPARTMENT_CODE: orgGroup[i].DEPARTMENT_CODE,
					ORG_LEVEL: orgGroup[i].ORG_LEVEL,
					ORG_NAME: orgGroup[i].ORG_NAME,
					ORG_URL: orgGroup[i].ORG_URL,
					PARENT_ORG_DEPARTMENT_CODE: orgGroup[i].PARENT_ORG_DEPARTMENT_CODE,
					ADMIN_NAME: orgGroup[i].ADMIN_NAME,
					IS_PUBLIC: orgGroup[i].IS_PUBLIC,
					ORG_TYPE: orgGroup[i].ORG_TYPE,
					ORG_COMMENT: orgGroup[i].ORG_COMMENT,
					USER_COUNT: orgGroup[i].USER_COUNT
			}; // 조직 정보 instance
			
			var orgTreeItem = {
					id: orgGroup[i].COMPANY_CODE + "_" + orgGroup[i].DEPARTMENT_CODE,
					text: orgGroup[i].ORG_NAME,
					admin_name : orgGroup[i].ADMIN_NAME,
					is_pb : orgGroup[i].IS_PUBLIC,
					c_code : orgGroup[i].COMPANY_CODE,
					d_code : orgGroup[i].DEPARTMENT_CODE,
					u_cnt : orgGroup[i].USER_COUNT,
					org_name : orgGroup[i].ORG_NAME,
					
					icon: 'icon-work_company',
					children: []
			} // 조직 tree instance
			
			var children_org = [];	
			if(orgGroup[i].CHILDREN_ORG != null) { // tree children에 매핑
				children_org = setOrgRepoIns(orgGroup[i].CHILDREN_ORG.orgList, step+1)
				orgTreeItem.children = children_org;
			}
			
			if(!(orgInfo.DEPARTMENT_CODE === 'N9A9'))
				output.push(orgTreeItem); 
			
			orgTreeRepo.orgInfoList.push(orgInfo); 
			
			
		}
		
		if(step == 0) {
			let rootItem = {
				id: 'All_All',
				text: 'All',
				icon: 'none',
				children: []
			};
			rootItem.children = rootItem.children.concat(output);
//			orgTreeRepo.orgTree = [];
			orgTreeRepo.orgTree.push(rootItem);
		}
		
		return output;
	 };
} 

Top.Controller.create('adminOrgUserLayoutLogic', {	
	init : function(event, widget) {				
		var orgTree = Top.Dom.selectById("TreeView1305");
		createOrgTree(orgTree, "ADM0021"); 
		
		Top.Dom.selectById('adminOrgUserLayout_searchField').setProperties({
			'clear' : 'true',
    		'on-clear' : this.cancel_icon,
    		'on-keydown': this.searchUser,
			'on-blur': this.searchUser
		});
		var option = {node:{draggable:true}};
		Top.Dom.selectById('TreeView1305').setProperties({ option: option });;
		
		orgUserRepo.orgUserList.length=0;
		//backup list for search algorithms
		this.cur_user_list = [];
		
		admin.setLnbColor();
		
	}, 
	cancel_icon : function(event, widget) {
		
		Top.Dom.selectById('adminOrgUserLayout_searchField').setText("");
		
		
		orgUserRepo.orgUserList = this.cur_user_list;

		Top.Dom.selectById('OrgUserTableView').update();
		Top.Dom.selectById('OrgUserTableView').render();
		
	},
	importUsers : function(event, widget) {
		Top.Dom.selectById('adminUserAddManyDialog').open();
	}, 
	
	clickUserDetail : function(event, widget){
		
		var dialog = Top.Dom.selectById('adminUserInfoViewDialog');
		var ctrl = Top.Controller.get('adminUserInfoViewLogic'); //can't get controller from dialog...
		var user_id = Top.Dom.selectById('OrgUserTableView').getClickedData().USER_ID;
		
		dialog.setProperties({onOpen:function(){
			var main = Top.Dom.selectById('adminUserInfoMain');
			main.onSrcLoad(function(){
				ctrl.init_contents(user_id); //USER_ID 키값 넘김
			});
			main.src('adminUserInfoView.html' + verCsp()); //첫 화면은 유저 상세 보기
			
		}});
		
		dialog.open();
		
	},
	
	clickConfirm : function(event, widget) {
		let outputdata = null;
		Top.Ajax.execute({
			url : _workspace.url + "Admin/OrgUsersImport?action=",
			type : 'POST',
			dataType : "json",
			async : false,
			cache : false,
			data : JSON.stringify(import_inputDTO), // arg
			contentType : "application/json",
			crossDomain : true,
			xhrFields : {
			    withCredentials: true
			},
			headers: {
				"ProObjectWebFileTransfer":"true"
			},
			success : function(data) {
				data = JSON.parse(data);
				if(data.dto == undefined) {
				    outputdata = {
				        result : "Success",
				        message : data.exception.name + ' ' + data.exception.message,
				        data : data
				    }
				} else {
				    outputdata = data.dto;
				    outputdata.files = data.files;
				}
				console.info("success");
	            console.info(outputdata);
				alert("조직원 추가를 성공했습니다.");
	    		workspaceManager.update();
			},
			error : function(data) {
	            outputdata = {
	                result : "Fail",
	                message : data.exception != undefined ? data.exception.message : "Error",
	                data : data
	            }		
	            console.info("fail");
	            console.info(outputdata);
			}
		});
		console.info(outputdata);
	}, 
	
	importOrgs : function(event, widget) {
		Top.Dom.selectById('adminLoadOrgTreeDialog').open();
	}, 
	
	clickCancel : function(event, widget) {
		let outputdata = null;
		Top.Ajax.execute({
			url : _workspace.url + "Admin/OrgChartImport?action=",
			type : 'POST',
			dataType : "json",
			async : false,
			cache : false,
			data : JSON.stringify(import_inputDTO), // arg
			contentType : "application/json",
			crossDomain : true,
			xhrFields : {
			    withCredentials: true
			},
			headers: {
				"ProObjectWebFileTransfer":"true"
			},
			success : function(data) {
				data = JSON.parse(data);
				if(data.dto == undefined) {
				    outputdata = {
				        result : "Success",
				        message : data.exception.name + ' ' + data.exception.message,
				        data : data
				    }
				} else {
				    outputdata = data.dto;
				    outputdata.files = data.files;
				}
				console.info("success");
	            console.info(outputdata);
				alert("조직 추가를 성공했습니다."); 
	    		workspaceManager.update();
			},
			error : function(data) {
	            outputdata = {
	                result : "Fail",
	                message : data.exception != undefined ? data.exception.message : "Error",
	                data : data
	            }		
	            console.info("fail");
	            console.info(outputdata);
			}
		});
		console.info(outputdata);
	}, 
	
	clickNode : function(event, widget) {
		var clickedNode = widget.getSelectedNode();
		var aff_code = clickedNode.id;
		
		var adminInputDTO = {
				"dto" : {
					"COMPANY_CODE" : aff_code.split("_")[0],
					"DEPARTMENT_CODE" : aff_code.split("_")[1]
				}
		};
		let ctrl = Top.Controller.get('adminOrgUserLayoutLogic');
		setOrgUserRepoIns = function(orgUserList){		
			orgUserRepo.reset();
			
			
			for(var i = 0; i < orgUserList.length; i++) {	
				if(orgUserList[i].ORG_NAME == null)
					orgUserList[i].ORG_NAME = '소속없음'
				
				var orgUserInfo = {
						user_name: orgUserList[i].USER_NAME,
						org_name: orgUserList[i].ORG_NAME,
						job: orgUserList[i].USER_POSITION,
						com_num: orgUserList[i].USER_COM_NUM,
						email: orgUserList[i].USER_EMAIL,
						status: adminCodeToText(orgUserList[i].USER_EMPLOYMENT_TYPE),
						USER_ID: orgUserList[i].USER_ID,
						affcode: orgUserList[i].USER_AFF,
						org_path: orgUserList[i].ORG_PATH
				}; // 유저 정보 instance

				orgUserRepo.orgUserList.push(orgUserInfo); 
				
				
			}
			
			//backup list for search algorithms
			ctrl.cur_user_list.length = 0; //reset backup list
			ctrl.cur_user_list = JSON.parse(JSON.stringify(orgUserRepo.orgUserList));
			Top.Dom.selectById('adminOrgUserLayout_searchField').setText("");
		}
		
		let outputdata = null;
		Top.Ajax.execute({
			url : _workspace.url + "Admin/OrgUserList",
			type : 'GET',
			dataType : "json",
			async : false,
			cache : false,
			data : JSON.stringify(adminInputDTO), // arg
			contentType : "application/json",
			crossDomain : true,
			xhrFields : {
			    withCredentials: true
			},
			headers: {
				"ProObjectWebFileTransfer":"true"
			},
			success : function(data) {
				data = JSON.parse(data);
				if(data.dto == undefined) {
				    outputdata = {
				        result : "Success",
				        message : data.exception.name + ' ' + data.exception.message,
				        data : data
				    }
				} else {
					if(data.dto.orgUserList){
						Top.Dom.selectById('adminOrgUserLayout_userCountText').setText( data.dto.orgUserList.length + '명'); //count users
						outputdata = data.dto;
						setOrgUserRepoIns(outputdata.orgUserList);
					} else{
						Top.Dom.selectById('adminOrgUserLayout_userCountText').setText(' 0명');
						setOrgUserRepoIns([]);
					}
				    
				    
				    var table = Top.Dom.selectById('OrgUserTableView');
					table.update();
					table.render();
				    //drag&drop-----------------------------------------------------------
//				    $(".body-row").attr("draggable",true);
//				    $(".body-row").attr("ondragstart","dragStart(event)");
				    //------------------------------------------------------------
				}
	            console.info(outputdata);
			},
			error : function(data) {
	            outputdata = {
	                result : "Fail",
	                message : data.exception != undefined ? data.exception.message : "Error",
	                data : data
	            }		
	            console.info(outputdata);
			}
		});	
	}, 
	
	dragTableRow : function(event, widget) {
		console.info(widget);
	}, 
	
	clickUserAdd : function(event, widget) {
		
		if(!Top.Dom.selectById("TreeView1305").getSelectedNode() || Top.Dom.selectById("TreeView1305").getSelectedNode().text === "All"){
			TeeAlarm.open({title: '그룹을 선택해주세요.', content: ''})
			return ;
		}
		
		Top.Dom.selectById('adminUserAddDialog').open();
		
	},
	
	clickUserDelete : function(event, widget){
		

		let outputdata;
		var user_id = Top.Dom.selectById('OrgUserTableView').getClickedData().USER_ID
		var company_obj = adminGetTeamObjByName(Top.Dom.selectById('OrgUserTableView').getClickedData().org_name);
		var inputDTO = {
				dto:{
					"orgUserList":[{
						"USER_ID":user_id,
						"COMPANY_CODE":company_obj.COMPANY_CODE,
						"DEPARTMENT_CODE":company_obj.DEPARTMENT_CODE,
						"ORG_URL":company_obj.ORG_URL,
						"ORG_TYPE":company_obj.ORG_TYPE
					}]
				}
		};
		
		
		let ctrl = Top.Controller.get('adminOrgUserLayoutLogic');

		Top.Dom.selectById('adminReleaseDialog').open();
//		Top.Ajax.execute({
//			url : _workspace.url + "Admin/OrgUser",
//			type : 'DELETE',
//			dataType : "json",
//			async : false,
//			cache : false,
//			data : JSON.stringify(inputDTO), // arg
//			contentType : "application/json",
//			crossDomain : true,
//			xhrFields : {
//			    withCredentials: true
//			},
//			headers: {
//				"ProObjectWebFileTransfer":"true"
//			},
//			success : function(data) {
//				data = JSON.parse(data);
//				if(data.dto.RESULT_CD === "RST0001") {
//
//					ctrl.clickNode('', Top.Dom.selectById('TreeView1305'));
//					//simulate node click for current one to update table after delete
//					return;
//				}
//				else if(data.dto.RESULT_CD  === "RST0001"){
//					 outputdata = {
//						        result : "Success",
//						        message : data.exception.name + ' ' + data.exception.message,
//						        data : data
//					 }
//					 console.log(outputdata);
//				}
//				else {
//					console.info("unknown code : " + data.dto.RESULT_CD)
//				}
//	            
//			},
//			error : function(data) {
//	            outputdata = {
//	                result : "Fail",
//	                message : data.exception != undefined ? data.exception.message : "Error",
//	                data : data
//	            }		
//	            console.info(outputdata);
//			}
//		});	
//		
	}, 
	
	searchUser : function(event, widget) {
		if((event.type === "keydown" && event.key === "Enter") || event.type === "click"){
			
			var searchtext = Top.Dom.selectById('adminOrgUserLayout_searchField').getText();
			if(searchtext !== ""){
				orgUserRepo.orgUserList = this.cur_user_list.filter(e => (!e.user_name)?false:e.user_name.includes(searchtext));
			} else{
				orgUserRepo.orgUserList = this.cur_user_list;
			} 
			
			Top.Dom.selectById('OrgUserTableView').update();
			Top.Dom.selectById('OrgUserTableView').render();
		}
		
	}, drag : function(event, widget, params) {
		console.info('startdrag');
		console.info(widget);
	}, drop : function(event, widget, params) {
		var tmpnum = 0 ;
		$("#"+event.target.id).removeClass("moveorg");
		if(params.node.length == undefined){
			var clickdata = params.node;
			var moveorg = event.target.id;
			var affcode = orgUserRepo.orgUserList[0].affcode;
			if(moveorg == affcode) return;
			Top.Ajax.execute({
		        url: _workspace.url+"Admin/OrgUserTransfer?action=", //Service Object
		        type: 'POST',
		        dataType: 'json',
		        crossDomain: true,
		        contentType: 'application/json; charset=utf-8',
		        xhrFields: {
		            withCredentials: true
		          },
		        data: JSON.stringify({
		          "dto": {
		        	  "userTransferList": [
		                  {
		                      "USER_ID":clickdata.USER_ID,
		                      "companyProfile" : {
		                          "USER_AFF":affcode,
		                          "USER_TRANSFER_AFF":moveorg 
		                      }
		                  }
		              ]
		          }
		        }),
		        success: function(result) {
		        	if(result.dto.RESULT_CD === 'RST0002') return;
		        	var len = orgUserRepo.orgUserList.length;
		        	var tmp = [];
		        	for(var i= 0 ; i < len; i++){
		        		if(orgUserRepo.orgUserList[i].USER_ID != Top.Dom.selectById('OrgUserTableView').getClickedData().USER_ID){
		        			tmp.push(orgUserRepo.orgUserList[i]);
		        		}
		        	}
		        	orgUserRepo.orgUserList = tmp ;
					Top.Dom.selectById('OrgUserTableView').update();
					Top.Dom.selectById('OrgUserTableView').render();
//				    $(".body-row").attr("draggable",true);
//				    $(".body-row").attr("ondragstart","dragStart(event)");
		        },
		        error: function(error) {
		        }
		     });
		}
		else{
		for(var i = 0 ; i < params.node.length; i++){
		var clickdata = params.node[i];
		var moveorg = event.target.id;
		var affcode = orgUserRepo.orgUserList[0].affcode;
		if(moveorg == affcode) return;
		Top.Ajax.execute({
	        url: _workspace.url+"Admin/OrgUserTransfer?action=", //Service Object
	        type: 'POST',
	        dataType: 'json',
	        crossDomain: true,
	        contentType: 'application/json; charset=utf-8',
	        xhrFields: {
	            withCredentials: true
	          },
	        data: JSON.stringify({
	          "dto": {
	        	  "userTransferList": [
	                  {
	                      "USER_ID":clickdata.USER_ID,
	                      "companyProfile" : {
	                          "USER_AFF":affcode,
	                          "USER_TRANSFER_AFF":moveorg 
	                      }
	                  }
	              ]
	          }
	        }),
	        success: function(result) {
	        	if(result.dto.RESULT_CD === 'RST0002') return;
	        	var len = orgUserRepo.orgUserList.length;
	        	var tmp = [];
	        	for(var i= 0 ; i < len; i++){
	        		if(orgUserRepo.orgUserList[i].USER_ID != params.node[tmpnum].USER_ID){
	        			tmp.push(orgUserRepo.orgUserList[i]);
	        		}
	        	}
	        	tmpnum++;
	        	orgUserRepo.orgUserList = tmp ;
	        	Top.Dom.selectById('OrgUserTableView').update();
	        	Top.Dom.selectById('OrgUserTableView').render();
//			    $(".body-row").attr("draggable",true);
//			    $(".body-row").attr("ondragstart","dragStart(event)");
	        },
	        error: function(error) {
	        }
	     });
		}}
	}, dragover : function(event, widget) {
//		console.info('over');
//		$("#"+event.target.id).css({"background-color":"#9A9A9A"});
		$("#"+event.target.id).addClass("moveorg");
	}, dragleave : function(event, widget) {
//		console.info('leave');
//		$("#"+event.target.id).css({"background-color":"#FFFFFF"});
		$("#"+event.target.id).removeClass("moveorg");
	}, importPhoto : function(event, widget) {
		
	}
	
});
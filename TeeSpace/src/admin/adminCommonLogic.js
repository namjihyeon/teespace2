const adminCodeTable = {
		
	'ADM0001' : '정규직',
	'ADM0002' : '아르바이트',
	'ADM0003' : '계약직',
	'ADM0004' : '인턴',
	
	'WKS0004' : '조직원',
	'WKS0005' : '조직장'
		
}


function adminCodeToText(code){
	
	return code;
	
}

function adminTextToCode(text){
	
	return text;
	
}

function adminGetTeamObjByName(text){
		
	return orgTreeRepo.orgInfoList.find(value => value.ORG_NAME === text);
}


function adminGetTeamObjByAffCode(text){
	
	return orgTreeRepo.orgInfoList.find(value => (value.COMPANY_CODE + "_" + value.DEPARTMENT_CODE) === text);
}

function getFormatFile(fileName) {
    var a = document.createElement("a");
    document.body.appendChild(a);
    a.href = String.format("{0}res/{1}", location.pathname, fileName);
    a.download = fileName;
    a.click();
    document.body.removeChild(a);
}

function selectFormatFile(callback) {
    let fileChooser = Top.Device.FileChooser.create({
        extFilter : ".xlsx",
        onFileChoose: function(fileChooser) {
            let format = XLSX.read(fileChooser.src, { type: "array" });

            let bytes = new Uint8Array(fileChooser.src);
            let binary = "";
            for (var i = 0; i < bytes.length; i++) {
                binary += String.fromCharCode(bytes[i]);
            }
            let fileContent = btoa(binary);

            if(typeof callback == "function") {
                callback(format, fileChooser.file[0].name, fileContent);
            }
        },
    });
    
    fileChooser.show();
}

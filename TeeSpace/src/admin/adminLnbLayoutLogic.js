Top.Controller.create('adminLnbLayoutLogic', {
	init: function(widget){
		this.inquiryApp();
	},

	inquiryApp: function(){
		var _this = this;

		wscTab.wscList.length=0;									//	탭 초기화
    	
		var dau = {};
		dau.id = "setupWorkspacePublic";
		dau.text = "사용 현황";
		dau.html= "<top-linearlayout class='' orientation='vertical'>";
		dau.html += "<div layout-height='27px' class=''" + " id='user_setup_manage'" + " onclick='" + 'setMntrContentToDAU(event,this)' + "'>"
	 +         "<top-textview class='adminLnbText' text='" + "1일 현황" + "'/>"								
	 +     "</div>";
		dau.html += "<div layout-height='27px' class=''" + " id='user_pass_change'" + " onclick='" + 'setMntrContentToUserAnalysis(event,this)' + "'>"
   	 +         "<top-textview class='adminLnbText' text='" + "멤버 분석" + "'/>"								
   	 +     "</div>";
		dau.html += "<div layout-height='27px' class=''" + " id='user_delete'" + " onclick='" + 'setMntrContentToCloudSpaceAnalysis(event,this)' + "'>"
   	 +         "<top-textview class='adminLnbText' text='" + "CloudSpace 통계" + "'/>"							
   	 +     "</div>";
		dau.html += "</top-linearlayout>";

		wscTab.wscList.push(dau);
    	Top.Dom.selectById('DAUAccordionLayout').setProperties({'items': wscTab.wscList});	//앱리스트 적용

    	//    	_this.setAppArcordion();																		//데이터 셋팅 후 이벤트 바인딩

	}, MoveToDAU : function(event, widget) {
		Top.Dom.selectById('adminTitleLayout').src('./mntrTitleLayout.html' + verCsp());
		// Top.Dom.selectById('adminTitleLayout').src('MonitoringTitleLayout.html' + verCsp());
		Top.Dom.selectById('adminContentsLayout').src('DAULayout.html' + verCsp());
	}, clickOM : function(event, widget) {
		Top.Dom.selectById('adminContentsLayout').src('adminOrgManageLayout.html' + verCsp());
		Top.Controller.get("adminMainPageLayoutLogic").turn = "OM";
		Top.Dom.selectById('adminTitleLayout').src('adminTitleLayout.html' + verCsp());
	}, clickGM : function(event, widget) {
		Top.Dom.selectById('adminContentsLayout').src('adminGuestManageLayout.html' + verCsp());
		Top.Controller.get("adminMainPageLayoutLogic").turn = "GM";
		Top.Dom.selectById('adminTitleLayout').src('adminTitleLayout.html' + verCsp());
	}, clickCM : function(event, widget) {
		Top.Dom.selectById('adminContentsLayout').src('adminCompanyInfoManageLayout.html' + verCsp());
		Top.Controller.get("adminMainPageLayoutLogic").turn = "CM";
		Top.Dom.selectById('adminTitleLayout').src('adminTitleLayout.html' + verCsp());
	},
	
	
/*	setAppArcordion:function(){
		
		$('.wscWorkLayout').mouseenter(enterAppBtn);						// 호버시 즐겨찾기 버튼이 나타난다.			----------변경 필요
		$('.wscWorkLayout').mouseleave(leaveAppBtn);						// 호버가 끝날시 즐겨찾기 버튼이 사라진다.		----------변경 필요
		$('.wscWorkLayout').click(clickAppBtn);								// 워크스페이스의 App 선택				----------변경 필요
		$('.appBtnMenu').click(starAppBtn);										// 즐겨찾기 버튼 클릭						----------변경 필요
		$('#workspaceLnbAppArcodion .wscBtnExpand').click(expandWorkspaceList);								// 펼치기 버튼
		$('#workspaceLnbAppArcodion .wscBtnReduce').click(reduceWorkspaceList);								// 펼치기 버튼

	},*/
});

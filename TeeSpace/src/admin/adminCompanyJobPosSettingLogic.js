Top.Controller.create('adminCompanyJobPosSettingLayoutLogic', {
	init : function(){
		Top.Ajax.execute({
			url : _workspace.url + "Admin/CompanyProfileMetaList",
			type : 'GET',
			dataType : "json",
			async : false,
			cache : false,
			contentType : "application/json",
			crossDomain : true,
			xhrFields : {
			    withCredentials: true
			},
			headers: {
				"ProObjectWebFileTransfer":"true"
			},
			success : function(data) {
				var tmparr = JSON.parse(data).dto.comProfileMetaList;
				var insertarr = []; 
				var posarr = [];
				if(tmparr != null){ 
				for(var i = 0 ; i < tmparr.length; i++){
					if(tmparr[i].META_TYPE ==='ADM0001') {
						insertarr.push({
							'job' : tmparr[i].META_VALUE,
							'order' : tmparr[i].META_ORDER,
							'type' : tmparr[i].META_TYPE,
							'm_vi' : "visible",
							'd_vi' : 'visible',
							'a_vi' : "none",
							"u_vi" : "visible",
							"b_vi" : "visibie",
							"ro" : "true"
						})
					}
					else if(tmparr[i].META_TYPE === "ADM0002"){
						posarr.push({
							'pos' : tmparr[i].META_VALUE,
							'order' : tmparr[i].META_ORDER,
							'type' : tmparr[i].META_TYPE,
							'm_vi' : "visible",
							'd_vi' : 'visible',
							'a_vi' : "none",
							"u_vi" : "visible",
							"b_vi" : "visibie",
							"ro" : "true"
						})
					}
				}}
				insertarr.push({
					'job':"",
					'order':"",
					'type':'ADM0001',
					'm_vi' : "none",
					'd_vi' : 'none',
					'a_vi' : "visible",
					"u_vi" : "none",
					"b_vi" : "none",
					"ro" : "false"
				})
				posarr.push({
					'pos':"",
					'order':"",
					'type':'ADM0002',
					'm_vi' : "none",
					'd_vi' : 'none',
					'a_vi' : "visible",
					"u_vi" : "none",
					"b_vi" : "none",
					"ro" : "false"
				})
				adminjobpos.job = [];
				adminjobpos.job = insertarr;
				Top.Dom.selectById('tablejob').update();
				Top.Dom.selectById('tablejob').render();
				adminjobpos.pos = [];
				adminjobpos.pos = posarr;
				Top.Dom.selectById('tablepos').update();
				Top.Dom.selectById('tablepos').render();
				
			},
			error : function(data) {
	           
			}
		});
	},
	p_click_modify : function(event, widget) {
		Top.Dom.selectById('TextField816_'+Top.Dom.selectById('tablepos').getClickedIndex()[0]+'_1').setProperties({"readonly":"false"})
		Top.Dom.selectById('Button989_'+Top.Dom.selectById('tablepos').getClickedIndex()[0]+'_2').setText("저장");
		Top.Dom.selectById('Button989_'+Top.Dom.selectById('tablepos').getClickedIndex()[0]+'_2').setProperties({"on-click":"p_click_save"});
	}, 
	p_click_save : function(){
		var idx = Top.Dom.selectById('tablepos').getClickedIndex()[0];
		var tmptext = Top.Dom.selectById('TextField816_'+Top.Dom.selectById('tablepos').getClickedIndex()[0]+'_1').getText();
		adminjobpos.pos[idx].pos = tmptext;
		var inputdata = {
				"dto":{
					"META_TYPE": "ADM0002",
			        "META_ORDER": idx+1,
			        "META_VALUE" : tmptext,
			        "ACTION": "NAME"
				}
		}
		Top.Ajax.execute({
			url : _workspace.url + "Admin/CompanyProfileMeta",
			type : 'PUT',
			dataType : "json",
			async : false,
			cache : false,
			contentType : "application/json",
			data : JSON.stringify(inputdata),
			crossDomain : true,
			xhrFields : {
			    withCredentials: true
			},
			headers: {
				"ProObjectWebFileTransfer":"true"
			},
			success : function(data) {
				
				Top.Dom.selectById('TextField816_'+Top.Dom.selectById('tablepos').getClickedIndex()[0]+'_1').setProperties({"readonly":"true"})
				Top.Dom.selectById('Button989_'+Top.Dom.selectById('tablepos').getClickedIndex()[0]+'_2').setText("수정");
				Top.Dom.selectById('Button989_'+Top.Dom.selectById('tablepos').getClickedIndex()[0]+'_2').setProperties({"on-click":"p_click_modify"});
			},
			error : function(data) {
	           
			}
		});
	},
	p_click_delete : function(event, widget) {
		var idx = Top.Dom.selectById('tablepos').getClickedIndex()[0];
		var tmpdata = Top.Dom.selectById('tablepos').getClickedData();
		var inputdata = {
				"dto" : {
					"META_TYPE" : tmpdata.type,
					"META_ORDER" : tmpdata.order
				}
		}
		Top.Ajax.execute({
			url : _workspace.url + "Admin/CompanyProfileMeta",
			type : "DELETE",
			dataType : "json",
			async : false,
			cache : false,
			contentType : "application/json",
			data : JSON.stringify(inputdata),
			crossDomain : true,
			xhrFields : {
				withCredentials : true
			},
			headers: {
				"ProObjectWebFileTransfer":"true"
			},
			success : function(data){
				var flag = 1;
				adminjobpos.pos.splice(idx,1);
				for(var i = idx; i < adminjobpos.pos.length; i++){
					if(adminjobpos.pos[i].order == "") continue;
					adminjobpos.pos[i].order--; 
				}
				Top.Dom.selectById('tablepos').update();
				Top.Dom.selectById('tablepos').render();
			}
		})
	}, p_click_down : function(event, widget) {
		var idx = Top.Dom.selectById('tablepos').getClickedIndex()[0];
		if((idx==adminjobpos.pos.length-2))return;
		var inputdata = {
				"dto":{
					"META_TYPE": "ADM0002",
			        "META_ORDER": idx+1,
			        "ACTION": "DOWN"
				}
		}
		Top.Ajax.execute({
			url : _workspace.url + "Admin/CompanyProfileMeta",
			type : 'PUT',
			dataType : "json",
			async : false,
			cache : false,
			contentType : "application/json",
			data : JSON.stringify(inputdata),
			crossDomain : true,
			xhrFields : {
			    withCredentials: true
			},
			headers: {
				"ProObjectWebFileTransfer":"true"
			},
			success : function(data) {
				
					var a = adminjobpos.pos[idx+1];
					adminjobpos.pos[idx+1] = adminjobpos.pos[idx];
					adminjobpos.pos[idx+1].order = idx+2;
					adminjobpos.pos[idx] = a;
					adminjobpos.pos[idx].order = idx+1;
					Top.Dom.selectById('tablepos').update();
					Top.Dom.selectById('tablepos').render();
			},
			error : function(data) {
	           
			}
		});
		
	}, p_click_up : function(event, widget) {
		var idx = Top.Dom.selectById('tablepos').getClickedIndex()[0];
		if(adminjobpos.pos[idx-1]==undefined)return;
		var inputdata = {
				"dto":{
					"META_TYPE": "ADM0002",
			        "META_ORDER": idx+1,
			        "ACTION": "UP"
				}
		}
		Top.Ajax.execute({
			url : _workspace.url + "Admin/CompanyProfileMeta",
			type : 'PUT',
			dataType : "json",
			async : false,
			cache : false,
			contentType : "application/json",
			data : JSON.stringify(inputdata),
			crossDomain : true,
			xhrFields : {
			    withCredentials: true
			},
			headers: {
				"ProObjectWebFileTransfer":"true"
			},
			success : function(data) {
				
				var a = adminjobpos.pos[idx-1];
				adminjobpos.pos[idx-1] = adminjobpos.pos[idx];
				adminjobpos.pos[idx-1].order = idx;
				adminjobpos.pos[idx] = a;
				adminjobpos.pos[idx].order = idx+1;
				Top.Dom.selectById('tablepos').update();
				Top.Dom.selectById('tablepos').render();
			},
			error : function(data) {
	           
			}
		});
		
	}, p_addnode : function(event, widget){
		var idx = adminjobpos.pos.length-1;
		var tmptext = Top.Dom.selectById('TextField816_'+Top.Dom.selectById('tablepos').getClickedIndex()[0]+'_1').getText()
		var inputdata = {
				"dto" : {
					"META_TYPE" : "ADM0002",
					"META_VALUE" : tmptext,
					"META_ORDER" : idx+1
				}
		}
		Top.Ajax.execute({
			url : _workspace.url + "Admin/CompanyProfileMeta",
			type : 'POST',
			dataType : "json",
			async : false,
			cache : false,
			contentType : "application/json",
			data : JSON.stringify(inputdata),
			crossDomain : true,
			xhrFields : {
			    withCredentials: true
			},
			headers: {
				"ProObjectWebFileTransfer":"true"
			},
			success : function(data) {
				adminjobpos.pos[idx] = ({
					'pos' : tmptext,
					'order' : idx+1,
					'type' : 'ADM0002',
					'm_vi' : "visible",
					'd_vi' : 'visible',
					'a_vi' : "none",
					"u_vi" : "visible",
					"b_vi" : "visibie",
					"ro" : "true"
				})
				adminjobpos.pos.push({
					'pos':"",
					'order':"",
					'type':'ADM0002',
					'm_vi' : "none",
					'd_vi' : 'none',
					'a_vi' : "visible",
					"u_vi" : "none",
					"b_vi" : "none",
					"ro" : "false"
				})
				Top.Dom.selectById('tablepos').update();
				Top.Dom.selectById('tablepos').render();
			},
			error : function(data) {
	           
			}
		});
	},
	
	
	
	
	
	
	
	
	click_modify : function(event, widget) {
		Top.Dom.selectById('jobf_'+Top.Dom.selectById('tablejob').getClickedIndex()[0]+'_1').setProperties({"readonly":"false"})
		Top.Dom.selectById('Button309_'+Top.Dom.selectById('tablejob').getClickedIndex()[0]+'_2').setText("저장");
		Top.Dom.selectById('Button309_'+Top.Dom.selectById('tablejob').getClickedIndex()[0]+'_2').setProperties({"on-click":"click_save"});
	}, 
	click_save : function(){
		var idx = Top.Dom.selectById('tablejob').getClickedIndex()[0];
		var tmptext = Top.Dom.selectById('jobf_'+Top.Dom.selectById('tablejob').getClickedIndex()[0]+'_1').getText();
		adminjobpos.job[idx].job = tmptext;
		var inputdata = {
				"dto":{
					"META_TYPE": "ADM0001",
			        "META_ORDER": idx+1,
			        "META_VALUE" : tmptext,
			        "ACTION": "NAME"
				}
		}
		Top.Ajax.execute({
			url : _workspace.url + "Admin/CompanyProfileMeta",
			type : 'PUT',
			dataType : "json",
			async : false,
			cache : false,
			contentType : "application/json",
			data : JSON.stringify(inputdata),
			crossDomain : true,
			xhrFields : {
			    withCredentials: true
			},
			headers: {
				"ProObjectWebFileTransfer":"true"
			},
			success : function(data) {
				
				Top.Dom.selectById('jobf_'+Top.Dom.selectById('tablejob').getClickedIndex()[0]+'_1').setProperties({"readonly":"true"})
				Top.Dom.selectById('Button309_'+Top.Dom.selectById('tablejob').getClickedIndex()[0]+'_2').setText("수정");
				Top.Dom.selectById('Button309_'+Top.Dom.selectById('tablejob').getClickedIndex()[0]+'_2').setProperties({"on-click":"click_modify"});
			},
			error : function(data) {
	           
			}
		});
	},
	click_delete : function(event, widget) {
		var idx = Top.Dom.selectById('tablejob').getClickedIndex()[0];
		var tmpdata = Top.Dom.selectById('tablejob').getClickedData();
		var inputdata = {
				"dto" : {
					"META_TYPE" : tmpdata.type,
					"META_ORDER" : tmpdata.order
				}
		}
		Top.Ajax.execute({
			url : _workspace.url + "Admin/CompanyProfileMeta",
			type : "DELETE",
			dataType : "json",
			async : false,
			cache : false,
			contentType : "application/json",
			data : JSON.stringify(inputdata),
			crossDomain : true,
			xhrFields : {
				withCredentials : true
			},
			headers: {
				"ProObjectWebFileTransfer":"true"
			},
			success : function(data){
				var flag = 1;
				adminjobpos.job.splice(idx,1);
				for(var i = idx; i < adminjobpos.job.length; i++){
					if(adminjobpos.job[i].order == "") continue;
					adminjobpos.job[i].order--; 
				}
				Top.Dom.selectById('tablejob').update();
				Top.Dom.selectById('tablejob').render();
			}
		})
	}, click_down : function(event, widget) {
		var idx = Top.Dom.selectById('tablejob').getClickedIndex()[0];
		if((idx==adminjobpos.job.length-2))return;
		var inputdata = {
				"dto":{
					"META_TYPE": "ADM0001",
			        "META_ORDER": idx+1,
			        "ACTION": "DOWN"
				}
		}
		Top.Ajax.execute({
			url : _workspace.url + "Admin/CompanyProfileMeta",
			type : 'PUT',
			dataType : "json",
			async : false,
			cache : false,
			contentType : "application/json",
			data : JSON.stringify(inputdata),
			crossDomain : true,
			xhrFields : {
			    withCredentials: true
			},
			headers: {
				"ProObjectWebFileTransfer":"true"
			},
			success : function(data) {
				
					var a = adminjobpos.job[idx+1];
					adminjobpos.job[idx+1] = adminjobpos.job[idx];
					adminjobpos.job[idx+1].order = idx+2;
					adminjobpos.job[idx] = a;
					adminjobpos.job[idx].order = idx+1;
					Top.Dom.selectById('tablejob').update();
					Top.Dom.selectById('tablejob').render();
			},
			error : function(data) {
	           
			}
		});
		
	}, click_up : function(event, widget) {
		var idx = Top.Dom.selectById('tablejob').getClickedIndex()[0];
		if(adminjobpos.job[idx-1]==undefined)return;
		var inputdata = {
				"dto":{
					"META_TYPE": "ADM0001",
			        "META_ORDER": idx+1,
			        "ACTION": "UP"
				}
		}
		Top.Ajax.execute({
			url : _workspace.url + "Admin/CompanyProfileMeta",
			type : 'PUT',
			dataType : "json",
			async : false,
			cache : false,
			contentType : "application/json",
			data : JSON.stringify(inputdata),
			crossDomain : true,
			xhrFields : {
			    withCredentials: true
			},
			headers: {
				"ProObjectWebFileTransfer":"true"
			},
			success : function(data) {
				
				var a = adminjobpos.job[idx-1];
				adminjobpos.job[idx-1] = adminjobpos.job[idx];
				adminjobpos.job[idx-1].order = idx;
				adminjobpos.job[idx] = a;
				adminjobpos.job[idx].order = idx+1;
				Top.Dom.selectById('tablejob').update();
				Top.Dom.selectById('tablejob').render();
			},
			error : function(data) {
	           
			}
		});
		
	}, addnode : function(event, widget){
		var idx = adminjobpos.job.length-1;
		var tmptext = Top.Dom.selectById('jobf_'+Top.Dom.selectById('tablejob').getClickedIndex()[0]+'_1').getText()
		var inputdata = {
				"dto" : {
					"META_TYPE" : "ADM0001",
					"META_VALUE" : tmptext,
					"META_ORDER" : idx+1
				}
		}
		Top.Ajax.execute({
			url : _workspace.url + "Admin/CompanyProfileMeta",
			type : 'POST',
			dataType : "json",
			async : false,
			cache : false,
			contentType : "application/json",
			data : JSON.stringify(inputdata),
			crossDomain : true,
			xhrFields : {
			    withCredentials: true
			},
			headers: {
				"ProObjectWebFileTransfer":"true"
			},
			success : function(data) {
				adminjobpos.job[idx] = ({
					'job' : tmptext,
					'order' : idx+1,
					'type' : 'ADM0001',
					'm_vi' : "visible",
					'd_vi' : 'visible',
					'a_vi' : "none",
					"u_vi" : "visible",
					"b_vi" : "visibie",
					"ro" : "true"
				})
				adminjobpos.job.push({
					'job':"",
					'order':"",
					'type':'ADM0001',
					'm_vi' : "none",
					'd_vi' : 'none',
					'a_vi' : "visible",
					"u_vi" : "none",
					"b_vi" : "none",
					"ro" : "false"
				})
				Top.Dom.selectById('tablejob').update();
				Top.Dom.selectById('tablejob').render();
			},
			error : function(data) {
	           
			}
		});
	}
});

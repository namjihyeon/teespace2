
Top.Controller.create('adminAddLowGuestGroupLayoutLogic', {
    parentOrgId : String.empty,
    
	onSuccess : function(event, widget) {
	    let parentOrgId = this.parentOrgId;
	    
	    let companyCode = parentOrgId.split('_')[0];
	    let departmentCode = Top.Dom.selectById("tfDepartmentCode").getText();
	    let orgName = Top.Dom.selectById("tfOrgName").getText();
	    let parentOrgDepartmentCode = parentOrgId.split('_')[1];
	    let isPublic = Top.Dom.selectById("rbIsPublic").isChecked() ? true : false;
	    let orgComment = Top.Dom.selectById("taOrgComment").getText();
	    let adminName = Top.Dom.selectById("tfAdminName").getText();
	    
	    let inputDTO = {
            dto : {
                orgList : [{
                	ORG_URL					   : _mailDomain,
                    COMPANY_CODE               : companyCode,
                    DEPARTMENT_CODE            : departmentCode,
                    ORG_NAME                   : orgName,
                    PARENT_ORG_DEPARTMENT_CODE : parentOrgDepartmentCode,
                    IS_PUBLIC                  : isPublic ? "ADM0011" : "ADM0012",
                    ADMIN_NAME                 : adminName,
                    ORG_TYPE                   : "ADM0022",
                    ORG_COMMENT                : orgComment
                }]
            }
	    }
	    
	    Top.Ajax.execute({
            url : _workspace.url + "Admin/Org",
            type : "POST",
            dataType : "json",
            async : false,
            cache : false,
            data : JSON.stringify(inputDTO), // arg
            contentType : "application/json",
            crossDomain : true,
            xhrFields : {
                withCredentials: true
            },
            success : function(data) {
                Top.Dom.selectById("guestGroupTreeView").addNodes(parentOrgId, {
                    id: String.format("{0}_{1}", companyCode, departmentCode),
                    text: orgName,
                    icon: 'icon-work_company',
                    children: []
                });
            },
            error : function(data) {
            },
            complete : function() {
                Top.Dom.selectById("adminAddLowGuestGroupDialog").close(true);
            }
        });
	    
	},
	
	onFail : function(event, widget) {
	    Top.Dom.selectById("adminAddLowGuestGroupDialog").close(true);
    }
});

Top.Controller.create('adminAddOneGuestLayoutLogic', {
	init : function(event, widget) {	
		const data = COUNTRY_CODES.map(value => {
			return {
				text: `${value .code} ${value.name}`,
				value: value.code
			}
		});
		Top.Dom.selectById('SelectBox212g').setProperties({"nodes":data , "value":"+82"})
		this.aa = 1;
		this.bb = 0;
		this.cc = '1';
		this.ee = [];
    	

		if(Top.Dom.selectById('adminAddOneGuestDialog').getProperties('title') === '게스트 정보 수정'){
			Top.Dom.selectById('Button_id_check').setProperties({visible:"none"})
			Top.Dom.selectById('TextView2162_id_add').setProperties({visible:"none"})
			Top.Dom.selectById('Icon_userAdd_id_error').setProperties({visible:"none"})
			Top.Dom.selectById('UserInfoAdd_LowerContainer').removeWidget(Top.Dom.selectById('LinearLayoutPath_01'))
			
			$.ajax({
				url : _workspace.url + "Admin/OrgGuest", 
	            type: 'GET',
	            dataType: 'json',
	            crossDomain: true,
	            contentType: 'application/json; charset=utf-8',
	            xhrFields: {
	                withCredentials: true
	              },
	            data: JSON.stringify({
	              "dto": {
	                "USER_ID": Top.Dom.selectById('guestGroupTableView').getClickedData().USER_ID
	              }
	            }),
	            success: function(result) {	            	
	            	Top.Dom.selectById('UserInfoAdd_UserFirstNameContent').setText(result.dto.userInfo.USER_NAME);
	            	Top.Dom.selectById('UserInfoAdd_UserFirstNameContent').setProperties({'readonly':'true'});
	            	
	            	Top.Dom.selectById('UserInfoAdd_UserComNameContent').setText(result.dto.guestComProfileList[0].COMPANY_NAME);
	            	Top.Dom.selectById('UserInfoAdd_UserComNameContent').setProperties({'readonly':'true'});
	            	
	            	Top.Dom.selectById('depart_text').setText(result.dto.guestComProfileList[0].DEPARTMENT_NAME);
	            	Top.Dom.selectById('depart_text').setProperties({'readonly':'false'});
	            	
	            	Top.Dom.selectById('job_text').setText(result.dto.guestComProfileList[0].USER_JOB);
	            	Top.Dom.selectById('job_text').setProperties({'readonly':'false'});
	            	
	            	Top.Dom.selectById('UserInfoAdd_UserIDContent').setText(result.dto.userInfo.USER_LOGIN_ID.split('@')[0]);
	            	Top.Dom.selectById('UserInfoAdd_UserIDContent').setProperties({'readonly':'true'});
	            	
	            	Top.Dom.selectById('UserInfoAdd_UserPasswordContent').setText("");
	            	Top.Dom.selectById('UserInfoAdd_UserPasswordContent').setProperties({'readonly':'false'});
	            	if(result.dto.guestComProfileList[0].USER_COM_NUM != null){
	            	Top.Dom.selectById('UserInfoAdd_UserCompanyPhoneNumContent0').setText(result.dto.guestComProfileList[0].USER_COM_NUM.split('-')[0]);
	            	Top.Dom.selectById('UserInfoAdd_UserCompanyPhoneNumContent0').setProperties({'readonly':'false'});
	            	
	            	Top.Dom.selectById('UserInfoAdd_UserCompanyPhoneNumContent1').setText(result.dto.guestComProfileList[0].USER_COM_NUM.split('-')[1]);
	            	Top.Dom.selectById('UserInfoAdd_UserCompanyPhoneNumContent1').setProperties({'readonly':'false'});
	            	
	            	Top.Dom.selectById('UserInfoAdd_UserCompanyPhoneNumContent2').setText(result.dto.guestComProfileList[0].USER_COM_NUM.split('-')[2]);
	            	Top.Dom.selectById('UserInfoAdd_UserCompanyPhoneNumContent2').setProperties({'readonly':'false'});
	            	}
	            	if(result.dto.guestComProfileList[0].USER_PHONE != null){
	            	Top.Dom.selectById('UserInfoAdd_UserCellPhoneNumContent0').setText(result.dto.userInfo.USER_PHONE.split('-')[0]);
	            	Top.Dom.selectById('UserInfoAdd_UserCellPhoneNumContent0').setProperties({'readonly':'false'});
	            	Top.Dom.selectById('UserInfoAdd_UserCellPhoneNumContent1').setText(result.dto.userInfo.USER_PHONE.split('-')[1]);
	            	Top.Dom.selectById('UserInfoAdd_UserCellPhoneNumContent1').setProperties({'readonly':'false'});
	            	Top.Dom.selectById('UserInfoAdd_UserCellPhoneNumContent2').setText(result.dto.userInfo.USER_PHONE.split('-')[2]);
	            	Top.Dom.selectById('UserInfoAdd_UserCellPhoneNumContent2').setProperties({'readonly':'false'});
	            	}
	            	if(result.dto.guestComProfileList[0].FAX_NUM != null){
	            	Top.Dom.selectById('UserInfoAdd_UserFAXNumContent0').setText(result.dto.guestComProfileList[0].FAX_NUM.split('-')[0]);
	            	Top.Dom.selectById('UserInfoAdd_UserFAXNumContent0').setProperties({'readonly':'false'});
	            	Top.Dom.selectById('UserInfoAdd_UserFAXNumContent1').setText(result.dto.guestComProfileList[0].FAX_NUM.split('-')[1]);
	            	Top.Dom.selectById('UserInfoAdd_UserFAXNumContent1').setProperties({'readonly':'false'});
	            	Top.Dom.selectById('UserInfoAdd_UserFAXNumContent2').setText(result.dto.guestComProfileList[0].FAX_NUM.split('-')[2]);
	            	Top.Dom.selectById('UserInfoAdd_UserFAXNumContent2').setProperties({'readonly':'false'});
	            	}
	            	
	            	Top.Dom.selectById('UserInfoAdd_UserOfficeLocationContent').setText(result.dto.userInfo.USER_ADDRESS);
	            	Top.Dom.selectById('UserInfoAdd_UserOfficeLocationContent').setProperties({'readonly':'false'});
	            	
	            	if(result.dto.userInfo.USER_EMAIL != null){
	            	Top.Dom.selectById('UserInfoAdd_UserEmailIDContent').setText(result.dto.userInfo.USER_EMAIL.split('@')[0]);
	            	Top.Dom.selectById('UserInfoAdd_UserEmailIDContent').setProperties({'readonly':'false'});
	            	Top.Dom.selectById('UserInfoAdd_UserEmailDomainContent').setText(result.dto.userInfo.USER_EMAIL.split('@')[1]);
	            	Top.Dom.selectById('UserInfoAdd_UserEmailDomainContent').setProperties({'readonly':'false'});
	            	}
	            	
	            	if(result.dto.userInfo.USER_BIRTH){
	    				Top.Dom.selectById('DatePicker237').setDate(result.dto.userInfo.USER_BIRTH);
	    			}else{
	    				Top.Dom.selectById('DatePicker237').setDate();
	    			}
	            	
	            	Top.Dom.selectById('Button_id_check').setProperties({'visible':'none'});
	            	Top.Dom.selectById('UserInfoAdd_UserOccupationStarTv').setProperties({'visible':'none'});
	            	Top.Dom.selectById('UserInfoAdd_UserPasswordStarTv').setProperties({'visible':'none'});
	            	
	            	

//	            	Top.Dom.selectById("TextView_affiliation_path_0").setText(result.dto.ORG_NAME)
//	            	$('#TextView_affiliation_path_0').attr('CODE', Top.Dom.selectById('guestGroupTreeView').getSelectedNode().id);

	            	/*
	            	result.dto.guestComProfileList.map(elem=>elem.ORG_PATH).forEach((elem,idx) =>{
	            		var ll = Top.Widget.create('top-linearlayout');
	            		ll.setProperties({
	            			id: 'orgLayout_'+idx,
	            			'layout-width': 'match_parent',
	            			'layout-height': 'wrap_content',
	            			'orientation':'horizontal',
	            			'border-width': '0 0 0 0',
	            		});
	            		var t1 = Top.Widget.create('top-textview');
	            		t1.setProperties({
	            			id: 'orgText_'+idx,
	            			'layout-width': '90px',
	            			'layout-height': 'wrap_content',	
	            			'className': 'admin-org-t1',
	            			'text':'소속 ' + (idx+1).toString(),
	            		});
	            		ll.addWidget(t1);
	            		var t2 = Top.Widget.create('top-textview');
	            		t2.setProperties({
	            			id: 'orgText_'+idx,
	            			'layout-width': 'match_parent',
	            			'layout-height': 'wrap_content',
	            			'className': 'admin-org-t2',
	            			'text': elem,
	            		});
	            		ll.addWidget(t2);
	            		ll.complete();
	            		
//	          		$$$('UserInfoAdd_UserOfficeLocationWrapperm').addWidget(ll);
	            		$$$('UserInfoAddPathLayout_left').addWidget(ll);
	            	});
//	            	$$$('UserInfoAdd_UserOfficeLocationWrapperm').complete();
	            	$$$('UserInfoAddPathLayout_left').complete();
	            	*/
	            	
	            	var boolorgking = [];
	            	
	        		if(result.dto.guestComProfileList != null) {   
	        			//companyProfileList 겸임인경우
	        			for(let count = 0; count< result.dto.guestComProfileList.length; count++){		
	        				Top.Controller.get('adminAddOneGuestLayoutLogic').aa = Top.Controller.get('adminAddOneGuestLayoutLogic').aa + 1;
	        				boolorgking[count] = (result.dto.guestComProfileList[count].USER_ROLE == "WKS0004" ? true : false);

	        	 				//html로 생성되야할 부분
	        	 				let html_3 = '<top-linearlayout id="LinearLayoutPath_0'+(count+1)+'" layout-width="wrap_content" layout-height="30px" layout-horizontal-alignment="left" border-width="0px" orientation="horizontal" position="false" title="" layout-tab-disabled="false" vertical-alignment="top" class="linear-child-vertical" style="text-align: left;" actual-height="30">'+ '<div id="LinearLayoutPath_0'+(count+1)+'" class="top-linearlayout-root" style="white-space: nowrap; height: 30px; border-width: 0px; border-style: solid; vertical-align: top; overflow: auto hidden; width: 100%; line-height: 30px;">'
	        					+'<top-textview id="TextView_affiliation_0'+(count+1)+'" layout-width="auto" layout-height="auto" layout-vertical-alignment="middle" border-width="0px" text="소속 '+(count+1)+'" text-decoration="" title="" layout-tab-disabled="false" class="linear-child-horizontal" style="line-height: normal;" actual-width="38.1094">'
	        				+'<span id="TextView_affiliation_0'+(count+1)+'" class="top-textview-root" style="width: auto; height: auto; border-width: 0px; border-style: solid; vertical-align: middle;"><a class="top-textview-url">소속 '+ (count+1) +'</a><div class="top-textview-aligner middle"></div></span>'
	        			+'</top-textview>'
	        			+'<top-button id="TextView_affiliation_path_0'+(count+1)+'" on-click = "changeBtn" background-color="rgba(255,255,255,1.0)" layout-width="300px" layout-height="auto" layout-horizontal-alignment="center" margin="0px 0px 0px 10px" padding="0px 0px 1px 0px" border-color="rgba(194,194,194,1.0) rgba(194,194,194,1.0) rgba(194,194,194,1.0) rgba(194,194,194,1.0)" border-width="1px 1px 1px 1px"'
	        			+'tab-index="0" text="'+result.dto.guestComProfileList[count].ORG_PATH+'" text-color="rgba(0,0,0,1.0)" text-decoration="" icon-position="left" title="" layout-tab-disabled="false" class="linear-child-vertical" style="text-align: center;" actual-height="20">'
	        			+'<button id="TextView_affiliation_path_0'+(count+1)+'" class="top-button-root" type="button" style="width: 400px; height: auto; background-color: rgb(255, 255, 255); margin: 0px 0px 0px 10px; padding: 0px 0px 1px; color: rgb(0, 0, 0); border-width: 1px; border-style: solid; border-color: rgb(194, 194, 194); text-align: center;" tabindex="0"><label class="top-button-text">"'+result.dto.guestComProfileList[count].ORG_PATH+'"</label></button>'
	        			+'</top-button>'
	        			+`<top-checkbox id="orgkingg`+(count+1)+`" layout-width="auto" layout-height="auto" layout-vertical-alignment="middle" tab-index="0" text-decoration="" check-position="left" title="" layout-tab-disabled="false" class="linear-child-horizontal" style="line-height: normal;" actual-width="NaN">
	        			<input class="top-checkbox-check" type="checkbox" size="width:300px;" tabindex="0" style="display: inline-block;">
	        			<label class="top-checkbox-text left" for="checkbox" style="width: auto; height: auto; vertical-align: middle; padding-right: 0px; padding-top: 7px;"><i class="top-checkbox-icon"></i><span></span></label>
	        			</top-checkbox>`+
	        			+		'</div></top-linearlayout>'
	        			;
	        			
	        	 		       	Top.Dom.selectById("UserInfoAdd_LowerContainer").append(html_3) 
	        	 		       	Top.Dom.selectById("UserInfoAdd_LowerContainer").complete();
	        	 		
	        	 		       	setTimeout(function() {
	        	 					Top.Dom.selectById("orgkingg"+(count+1)).setChecked(boolorgking[count]);

	        	 		       	}, 0)
	        	 		    		
	        	 		 				
	        			
	        			}
	        		}
	            	
	        		if(orgGuestRepo.selectedGuest.guestComProfileList != null) {
	        			Top.Controller.get('adminAddOneGuestLayoutLogic').ee = orgGuestRepo.selectedGuest.guestComProfileList
	        		}
	            	
		        },
		        error: function(error) {        	
		        	
		        }
			});	
		} else if(Top.Dom.selectById('adminAddOneGuestDialog').getProperties('title') === '게스트 개별 추가') {
			Top.Dom.selectById('Button_id_check').setProperties({"disabled":"false"})
			Top.Controller.get('adminAddOneGuestLayoutLogic').aa = Top.Controller.get('adminAddOneGuestLayoutLogic').aa + 1;
			
			$.ajax({
				url : _workspace.url + "Admin/OrgPathGet?action=", 
	            type: 'POST',
	            dataType: 'json',
	            crossDomain: true,
	            contentType: 'application/json; charset=utf-8',
	            xhrFields: {
	                withCredentials: true
	              },
	            data: JSON.stringify({
	              "dto": {
	                "COMPANY_CODE": Top.Dom.selectById("guestGroupTreeView").getSelectedNode().c_code,
	                "DEPARTMENT_CODE": Top.Dom.selectById("guestGroupTreeView").getSelectedNode().d_code,
	                "ORG_URL": adminGetTeamObjByName(Top.Dom.selectById("guestGroupTreeView").getSelectedNode().text).ORG_URL,
	                "ORG_TYPE": adminGetTeamObjByName(Top.Dom.selectById("guestGroupTreeView").getSelectedNode().text).ORG_TYPE
	              }
	            }),
	            success: function(result) {
	          
	            		
	            		Top.Dom.selectById("TextView_affiliation_path_01").setText(result.dto.ORG_NAME)
	            		$('#TextView_affiliation_path_01').attr('CODE', Top.Dom.selectById("guestGroupTreeView").getSelectedNode().id);
	            	
	            	
	            	
		        },
		        error: function(error) {        	
		        	
		        }
				});	 

				
		}
		
		
		//this.org_path_array = [];
		//this.init_org_path();
		
		$('div#adminAddOneGuestLayout').on('click', function(e){
			//프로필 사진  > 더보기 메뉴
			if($('div#adminAddOneGuestLayout').css('display') == 'inline-block'){
			
				adminProfile_list_add(e, "ImageButton_Admin_Profile_Setting_add");		
			}
		});
		$.ajax({
			url : _workspace.url + "Admin/CompanyInfo", 
            type: 'GET',
            dataType: 'json',
            crossDomain: true,
            contentType: 'application/json; charset=utf-8',
            xhrFields: {
                withCredentials: true
              },
            data: JSON.stringify({
              "dto": {
                "ADMIN_ID": userManager.getLoginUserId()
              }
            }),
            success: function(result) {
	        },
	        error: function(error) {        	
	        	
	        }
			});	
		
		
		
		
		//이벤트 선언
		Top.Dom.selectById('UserInfoAdd_UserIDContent').setProperties({'on-keyup':'add_id_check'});
		Top.Dom.selectById('UserInfoAdd_UserPasswordContent').setProperties({'on-keyup':'add_pw_check'});
		Top.Dom.selectById("UserInfoAdd_addIcon").setProperties({'on-click':'addPathIcon'})
		$('div#UserInfoAdd_UserOccupationHeader #UserInfoAdd_addIcon').css({"cursor":"pointer"})
		Top.Dom.selectById("Button_id_check").setProperties({"on-click":"idExist"})
		
		//소속/부서 채우기
		
//		$.ajax({
//			url : _workspace.url + "Admin/OrgPathGet?action=", 
//            type: 'POST',
//            dataType: 'json',
//            crossDomain: true,
//            contentType: 'application/json; charset=utf-8',
//            xhrFields: {
//                withCredentials: true
//              },
//            data: JSON.stringify({
//              "dto": {
//                "COMPANY_CODE": Top.Dom.selectById('guestGroupTreeView').getSelectedNode().id.split('_')[0],
//                "DEPARTMENT_CODE": Top.Dom.selectById('guestGroupTreeView').getSelectedNode().id.split('_')[1],
//                "ORG_URL": adminGetTeamObjByName(Top.Dom.selectById("guestGroupTreeView").getSelectedNode().text).ORG_URL,
//                "ORG_TYPE": adminGetTeamObjByName(Top.Dom.selectById("guestGroupTreeView").getSelectedNode().text).ORG_TYPE
//              }
//            }),
//            success: function(result) {
//            	Top.Dom.selectById("TextView_affiliation_path_0").setText(result.dto.ORG_NAME)
//            	$('#TextView_affiliation_path_0').attr('CODE', Top.Dom.selectById('guestGroupTreeView').getSelectedNode().id);
//	        },
//	        error: function(error) {        	
//	        	
//	        }
//			});	
		
		
		
		
	},

	
	
	idExist : function(event, widget){
		
		
		
		$.ajax({
	        url: _workspace.url+'Users/IDCheck?action=SO', //Service Object
	        type: 'POST',
	        dataType: 'json',
	        crossDomain: true,
	        contentType : "application/json",
	        xhrFields: {
	            withCredentials: true
	          },
	        data: JSON.stringify({
	          "dto": {
	        	"USER_LOGIN_ID": Top.Dom.selectById('UserInfoAdd_UserIDContent').getText() + '@'+ _mailDomain 
	          }
	        }),
	        success: function(result) {
	        	if(result.dto.USER_PW == "RST0001"){
	        		Top.Dom.selectById("UserInfoAdd_UserIDContent").setProperties({"border-color":"red"});
	        		Top.Dom.selectById("UserInfoAdd_UserIDInfoErrorIcon").setProperties({"visible":"visible","tooltip-top":"이미 사용 중인 아이디 입니다. 다른 아이디를 입력하세요."});
	        		
	        		//조상호 동일한 아이디 존재
	        		// 아이디 중복됨
	        	}else{
	    			Top.Dom.selectById("UserInfoAdd_UserIDContent").setProperties({"border-color":"#D3DBDF"});
	    			Top.Dom.selectById("UserInfoAdd_UserIDInfoErrorIcon").setProperties({"visible":"hidden"});
	    	
	        		//아이디 중복안됨
	        	}
	        },
	        error: function(error) {
	        }
		})
		
		
		
		
		
		
	},
	
	addPathIcon : function (event, widget){
		Top.Controller.get('adminAddOneGuestLayoutLogic').bb = 0;
		Top.Dom.selectById("adminGuestTreeviewDialog").open()
		
	},
	changeBtn : function (event, widget){
		//기존 소속 selected 되어있게해야함
		Top.Controller.get('adminAddOneGuestLayoutLogic').bb = 1;

		this.cc = widget.id;
		Top.Dom.selectById("adminGuestTreeviewDialog").open(event, widget)
		
		
	},
	
	add_id_check : function(event , widget){
	
		var idVal = Top.Dom.selectById("UserInfoAdd_UserIDContent").getText()
		if(idVal.length<4 || idVal.length>20){
			Top.Dom.selectById("Icon_userAdd_id_error").setProperties({"text-color":"#808080"});
			
		}
		else{			
			if( (pattern_kor.test(idVal)) || (pattern_spc.test(idVal))){
				Top.Dom.selectById("Icon_userAdd_id_error").setProperties({"text-color":"#808080"});	
			}
			else{
				
				Top.Dom.selectById("Icon_userAdd_id_error").setProperties({"text-color":"#07C25B"});
				
			}	
		}
	},
	
	
	add_pw_check : function(event, widget){
		$("input#UserInfoAdd_UserPasswordContent").css("style","ime-mode:active")
		var pwdtext = Top.Dom.selectById("UserInfoAdd_UserPasswordContent").getText()
		
		
		var pwdarry = new Array(2);
	if(pwdtext.length >= 9 && pwdtext.length <= 20){
		pwdarry[0]=1;
	}
	else {
					
	}
	if((pattern_num.test(pwdtext)) && (pattern_engs.test(pwdtext)) && (pattern_spc2.test(pwdtext))){

		pwdarry[1]=1;
	}
	else if((pattern_num.test(pwdtext)) && (pattern_engs.test(pwdtext)) && (pattern_engb.test(pwdtext))){
		pwdarry[1]=1;
	}
	else if((pattern_engs.test(pwdtext)) && (pattern_spc2.test(pwdtext)) && (pattern_engb.test(pwdtext))){
		pwdarry[1]=1;
	}
	else{
	}
	
	if( pwdarry[0]==1 && pwdarry[1]==1){
		Top.Dom.selectById("UserInfoAdd_UserPasswordInfoErrorIcon").setProperties({"visible":"none"});
		Top.Dom.selectById("UserInfoAdd_UserPasswordContent").setProperties({"border-color":"#dddddd"})
		Top.Dom.selectById("Icon_userAdd_pwd_error").setProperties({"text-color":"#07C25B"});
	}else{
		Top.Dom.selectById("UserInfoAdd_UserPasswordInfoErrorIcon").setProperties({"visible":"none"});
		Top.Dom.selectById("UserInfoAdd_UserPasswordContent").setProperties({"border-color":"#dddddd"})
		Top.Dom.selectById("Icon_userAdd_pwd_error").setProperties({"text-color":"#808080"});
	}
		
		
	},
	
	
	
	/************************************게스트 개별 추가 ok/cancel*********************************************************/
	click_cancel : function(event, widget) {		
		var adminAddOneGuestDialog = Top.Dom.selectById("adminAddOneGuestDialog"); 
   //     adminAddOneGuestDialog.setProperties({ title : "게스트 정보" });
  //      adminAddOneGuestDialog.setProperties({ dialogLayout : "adminGuestModificationLayout"});
        adminAddOneGuestDialog.close(true);
 //       adminAddOneGuestDialog.open();
	}, 
	/*click_modify : function(event, widget){
		var image = Top.Dom.selectById("ImageButton_Admin_Profile_Setting_add").getSrc();
    	//아이디

		//비밀번호

		//이름
		var flag_name = 0;
		if(Top.Dom.selectById("UserInfoAdd_UserFirstNameContent").getText() === ""){
			Top.Dom.selectById("UserInfoAdd_UserFirstNameContent").setProperties({"border-color":"red"})
			Top.Dom.selectById("UserInfoAdd_UserNameErrorIcon").setProperties({"visible":"visible"})
			flag_name = 1;
		}
		 if(flag_name === 0){
		        Top.Dom.selectById("UserInfoAdd_UserFirstNameContent").setProperties({"border-color":"#dddddd "})
		        Top.Dom.selectById("UserInfoAdd_UserNameErrorIcon").setProperties({"visible":"none"})
		 }

		//이메일
		var flag_mail = 0;
		var flag_domain = 0;
		if(Top.Dom.selectById("UserInfoAdd_UserEmailIDContent").getText() === ""){
			Top.Dom.selectById("UserInfoAdd_UserEmailIDContent").setProperties({"border-color":"red"})
			Top.Dom.selectById("UserInfoAdd_UserMailErrorIcon").setProperties({"visible":"visible"})
			flag_mail = 1;
			
		}
		 if(Top.Dom.selectById("UserInfoAdd_UserEmailDomainContent").getText() === ""){
		    Top.Dom.selectById("UserInfoAdd_UserEmailDomainContent").setProperties({"border-color":"red"})
		 	Top.Dom.selectById("UserInfoAdd_UserMailErrorIcon").setProperties({"visible":"visible"})
		 	flag_domain = 1;
		 }
		 
		 if(flag_mail === 0){
		        Top.Dom.selectById("UserInfoAdd_UserEmailIDContent").setProperties({"border-color":"#dddddd "})
		        Top.Dom.selectById("UserInfoAdd_UserMailErrorIcon").setProperties({"visible":"none"})
		 }
		 if(flag_domain === 0){
		        Top.Dom.selectById("UserInfoAdd_UserEmailDomainContent").setProperties({"border-color":"#dddddd "})
		         Top.Dom.selectById("UserInfoAdd_UserMailErrorIcon").setProperties({"visible":"none"})
		 }
		 if(flag_mail || flag_domain){
			 Top.Dom.selectById("UserInfoAdd_UserMailErrorIcon").setProperties({"visible":"visible"})
		 } 
		
		
		//리턴 처리
		if( flag_name ||flag_mail || flag_domain){ return;}
		
		
	
		
				//note : org tree에서 바로 ID를 org_path_array에 obj로 집어넣는게 더 효율적이나, 생각하기 편한 방식으로 일단 구현함..
			//var comp_obj = adminGetTeamObjByName(this.org_path_array[this.org_path_array.length - 1]);
			
			var role_code;			
//			
//			if(Top.Dom.selectById('UserInfoAdd_UserCompanyPositionContent').getSelectedText() === "팀원"){
//				role_code = "WKS0005";
//			} else{
//				role_code = "WKS0004";
//			}	
			
			var userInfo = {
				 "USER_ID" : Top.Dom.selectById('guestGroupTableView').getClickedData().USER_ID,
				 "USER_LOGIN_ID":Top.Dom.selectById('UserInfoAdd_UserIDContent').getText(),
		         "USER_PW":Top.Dom.selectById('UserInfoAdd_UserPasswordContent').getText(),
		         "USER_NAME":Top.Dom.selectById('UserInfoAdd_UserFirstNameContent').getText(),
		         "USER_EMAIL":Top.Dom.selectById('UserInfoAdd_UserEmailIDContent').getText() + '@' + Top.Dom.selectById('UserInfoAdd_UserEmailDomainContent').getText(),
		         "USER_BIRTH":Top.Dom.selectById('UserInfoAdd_UserBirthdayContent0').getText() + Top.Dom.selectById('UserInfoAdd_UserBirthdayContent1').getText()
		         + Top.Dom.selectById('UserInfoAdd_UserBirthdayContent2').getText(),
		         "USER_PHONE":Top.Dom.selectById('UserInfoAdd_UserCellPhoneNumContent0').getText() + '-'
		         + Top.Dom.selectById('UserInfoAdd_UserCellPhoneNumContent1').getText() + '-'
		         + Top.Dom.selectById('UserInfoAdd_UserCellPhoneNumContent2').getText(),
		         "USER_ADDRESS" :Top.Dom.selectById('UserInfoAdd_UserOfficeLocationContent').getText(),
		         "PROFILE_PHOTO":image
			}; 
			var guestComProfileList= [];
			guestComProfileList.push(
					{
						"USER_ROLE":"WKS0005",
						"COMPANY_NAME":Top.Dom.selectById('UserInfoAdd_UserComNameContent').getText(),
						"DEPARTMENT_NAME" : Top.Dom.selectById('depart_text').getText(),
			            "USER_JOB":Top.Dom.selectById('job_text').getText(),
			            "USER_AFF":adminGetTeamObjByName(Top.Dom.selectById('TextView_affiliation_path_0').getText().split(' > ')[Top.Dom.selectById('TextView_affiliation_path_0').getText().split(' > ').length-1]).COMPANY_CODE + '_' +adminGetTeamObjByName(Top.Dom.selectById('TextView_affiliation_path_0').getText().split(' > ')[Top.Dom.selectById('TextView_affiliation_path_0').getText().split(' > ').length-1]).DEPARTMENT_CODE,			            
			            "USER_COM_NUM": Top.Dom.selectById('UserInfoAdd_UserCompanyPhoneNumContent0').getText() + '-'
			            + Top.Dom.selectById('UserInfoAdd_UserCompanyPhoneNumContent1').getText() + '-' + Top.Dom.selectById('UserInfoAdd_UserCompanyPhoneNumContent2').getText(),
			            "FAX_NUM": Top.Dom.selectById('UserInfoAdd_UserFAXNumContent0').getText() + '-'
			            + Top.Dom.selectById('UserInfoAdd_UserFAXNumContent1').getText() + '-' + Top.Dom.selectById('UserInfoAdd_UserFAXNumContent2').getText(),
					})
			
			for(let j = 2; j<=this.aa; j++ ){
				guestComProfileList.push(
						{
							"USER_ROLE":"WKS0005",
							"COMPANY_NAME":Top.Dom.selectById('UserInfoAdd_UserComNameContent').getText(),
							"DEPARTMENT_NAME" : Top.Dom.selectById('depart_text').getText(),
				            "USER_JOB":Top.Dom.selectById('job_text').getText(),
				            "USER_AFF":adminGetTeamObjByName(Top.Dom.selectById('TextView_affiliation_path_'+j).getText().split(' > ')[Top.Dom.selectById('TextView_affiliation_path_'+j).getText().split(' > ').length-1]).COMPANY_CODE + '_' +adminGetTeamObjByName(Top.Dom.selectById('TextView_affiliation_path_'+j).getText().split(' > ')[Top.Dom.selectById('TextView_affiliation_path_'+j).getText().split(' > ').length-1]).DEPARTMENT_CODE,
				            "USER_COM_NUM": Top.Dom.selectById('UserInfoAdd_UserCompanyPhoneNumContent0').getText() + '-'
				            + Top.Dom.selectById('UserInfoAdd_UserCompanyPhoneNumContent1').getText() + '-' + Top.Dom.selectById('UserInfoAdd_UserCompanyPhoneNumContent2').getText(),
				            "FAX_NUM": Top.Dom.selectById('UserInfoAdd_UserFAXNumContent0').getText() + '-'
				            + Top.Dom.selectById('UserInfoAdd_UserFAXNumContent1').getText() + '-' + Top.Dom.selectById('UserInfoAdd_UserFAXNumContent2').getText(),
						})
			}
			
//			var companyProfileList = [{
//				"USER_EMPLOYEE_NUM":Top.Dom.selectById('UserInfoAdd_UserCompanyIDContent').getText(),
//	            "USER_JOB":"",
//	            "USER_POSITION":Top.Dom.selectById('UserInfoAdd_UserCompanyPositionContent').getSelectedText(),
//	            "USER_AFF":comp_obj.COMPANY_CODE + '_' + comp_obj.DEPARTMENT_CODE,
//	            "USER_ROLE":role_code,//Top.Dom.selectById('').getText(),
//	            "USER_COM_NUM": Top.Dom.selectById('UserInfoAdd_UserCompanyPhoneNumContent0').getText() + '-'
//	            + Top.Dom.selectById('UserInfoAdd_UserCompanyPhoneNumContent1').getText() + '-' + Top.Dom.selectById('UserInfoAdd_UserCompanyPhoneNumContent2').getText(),
//	            "USER_EMPLOYMENT_TYPE":adminTextToCode(Top.Dom.selectById('UserInfoAdd_UserEmploymentTypeContent').getSelectedText()),//Top.Dom.selectById('').getText(),
//	            "USER_TASK":Top.Dom.selectById('UserInfoAdd_UserInChargeOfContent').getText(),
//	            "USER_LOCATION":Top.Dom.selectById('UserInfoAdd_UserOfficeLocationContent').getText(),
//	            "USER_EMPLOYMENT_DATE":Top.Dom.selectById('UserInfoAdd_UserAdmittanceDateContent0').getText() + '-'
//	            + Top.Dom.selectById('UserInfoAdd_UserAdmittanceDateContent1').getText() + '-'
//	            + Top.Dom.selectById('UserInfoAdd_UserAdmittanceDateContent2').getText()
//			
//			}];
			var inputDTO = {
				dto: {
			        "userInfo": userInfo,
			        "guestComProfileList": guestComProfileList
			    }
			};
			
			Top.Ajax.execute({
				url : _workspace.url + "Admin/OrgGuest",
				type : 'PUT',
				dataType : "json",
				async : false,
				cache : false,
				data : JSON.stringify(inputDTO), // arg
				contentType : "application/json",
				crossDomain : true,
				xhrFields : {
				    withCredentials: true
				},
				headers: {
					"ProObjectWebFileTransfer":"true"
				},
				success : function(data) {
					Top.Dom.selectById('adminAddOneGuestDialog').close(true);
					Top.Controller.get('adminGuestManageLayoutLogic').treeNodeClick(MouseEvent, 'guestGroupTreeView', Top.Dom.selectById('guestGroupTreeView').getSelectedNode());
					
					
					
				},
				
				error : function(data){
					Top.Dom.selectById("UserInfoAdd_UserIDContent").setProperties({"border-color":"red"})
					Top.Dom.selectById("UserInfoAdd_UserIDInfoErrorIcon").setProperties({"visible":"visible","tooltip-top":"중복된 ID입니다."})
					return;
					
					
				}
			});
	},*/
	
	click_confirm : function(event, widget) {
		//프로필이미지
		
    	var image = Top.Dom.selectById("ImageButton_Admin_Profile_Setting_add").getSrc();
    	
    	if(Top.Dom.selectById('adminAddOneGuestDialog').getProperties('title') === '게스트 개별 추가') { // 개별 추가일 때만 아이디/비밀번호 검증
	    	//아이디
	    	var flag_id = 0;
	    	if( Top.Dom.selectById("Icon_userAdd_id_error").getProperties("text-color")=="#07C25B"){
				Top.Dom.selectById("UserInfoAdd_UserIDInfoErrorIcon").setProperties({"display":"hidden"});
				Top.Dom.selectById("UserInfoAdd_UserIDContent").setProperties({"border-color":"#dddddd"})
			}else{
				
					Top.Dom.selectById("UserInfoAdd_UserIDContent").setProperties({"border-color":"red"})
					Top.Dom.selectById("UserInfoAdd_UserIDInfoErrorIcon").setProperties({"visible":"visible"})
					flag_id = 1;
				
			}
			 if(flag_id === 0){
			        Top.Dom.selectById("UserInfoAdd_UserIDContent").setProperties({"border-color":"#dddddd "})
			        Top.Dom.selectById("UserInfoAdd_UserIDInfoErrorIcon").setProperties({"display":"hidden"})
			 }
	    	
			//비밀번호
			var flag_pwd = 0; 
			if( Top.Dom.selectById("Icon_userAdd_pwd_error").getProperties("text-color")=="#07C25B"){
				Top.Dom.selectById("UserInfoAdd_UserPasswordInfoErrorIcon").setProperties({"visible":"none"});
				Top.Dom.selectById("UserInfoAdd_UserPasswordContent").setProperties({"border-color":"#dddddd"})
			}else{
			
	//			if(Top.Dom.selectById("UserInfoAdd_UserPasswordContent").getText() === undefined || Top.Dom.selectById("UserInfoAdd_UserPasswordContent").getText() === ""){
				Top.Dom.selectById("UserInfoAdd_UserPasswordContent").setProperties({"border-color":"red"})
				Top.Dom.selectById("UserInfoAdd_UserPasswordInfoErrorIcon").setProperties({"visible":"visible"})
				flag_pwd = 1;
	//			}
			}
			
			 if(flag_pwd === 0){
			        Top.Dom.selectById("UserInfoAdd_UserPasswordContent").setProperties({"border-color":"#dddddd "})
			        Top.Dom.selectById("UserInfoAdd_UserPasswordInfoErrorIcon").setProperties({"visible":"none"})
			 }
    	}
		
		//이름
		var flag_name = 0;
		if(Top.Dom.selectById("UserInfoAdd_UserFirstNameContent").getText() === ""){
			Top.Dom.selectById("UserInfoAdd_UserFirstNameContent").setProperties({"border-color":"red"})
			Top.Dom.selectById("UserInfoAdd_UserNameErrorIcon").setProperties({"visible":"visible"})
			flag_name = 1;
		}
		 if(flag_name === 0){
		        Top.Dom.selectById("UserInfoAdd_UserFirstNameContent").setProperties({"border-color":"#dddddd "})
		        Top.Dom.selectById("UserInfoAdd_UserNameErrorIcon").setProperties({"visible":"none"})
		 }
        //소속 회사
		 var flag_name = 0;
			if(Top.Dom.selectById("UserInfoAdd_UserComNameContent").getText() === ""){
				Top.Dom.selectById("UserInfoAdd_UserComNameContent").setProperties({"border-color":"red"})
				flag_name = 1;
			}
			 if(flag_name === 0)
			        Top.Dom.selectById("UserInfoAdd_UserComNameContent").setProperties({"border-color":"#dddddd "})
			        
		//이메일
		var flag_mail = 0;
		var flag_domain = 0;
		if(Top.Dom.selectById("UserInfoAdd_UserEmailIDContent").getText() === ""){
			Top.Dom.selectById("UserInfoAdd_UserEmailIDContent").setProperties({"border-color":"red"})
			Top.Dom.selectById("UserInfoAdd_UserMailErrorIcon").setProperties({"visible":"visible"})
			flag_mail = 1;
			
		}
		 if(Top.Dom.selectById("UserInfoAdd_UserEmailDomainContent").getText() === ""){
		    Top.Dom.selectById("UserInfoAdd_UserEmailDomainContent").setProperties({"border-color":"red"})
		 	Top.Dom.selectById("UserInfoAdd_UserMailErrorIcon").setProperties({"visible":"visible"})
		 	flag_domain = 1;
		 }
		 
		 if(flag_mail === 0){
		        Top.Dom.selectById("UserInfoAdd_UserEmailIDContent").setProperties({"border-color":"#dddddd "})
		        Top.Dom.selectById("UserInfoAdd_UserMailErrorIcon").setProperties({"visible":"none"})
		 }
		 if(flag_domain === 0){
		        Top.Dom.selectById("UserInfoAdd_UserEmailDomainContent").setProperties({"border-color":"#dddddd "})
		         Top.Dom.selectById("UserInfoAdd_UserMailErrorIcon").setProperties({"visible":"none"})
		 }
		 if(flag_mail || flag_domain){
			 Top.Dom.selectById("UserInfoAdd_UserMailErrorIcon").setProperties({"visible":"visible"})
		 } 
		
		
		//리턴 처리
		if(flag_id || flag_pwd || flag_name ||flag_mail || flag_domain){ return;}
		
		
		var birthDate;
		
		if(Top.Dom.selectById("DatePicker237").getDate() === ""){
			birthDate = null
		}else{
			birthDate = Top.Dom.selectById('DatePicker237').getDate().split('-')[0] + Top.Dom.selectById('DatePicker237').getDate().split('-')[1] + Top.Dom.selectById('DatePicker237').getDate().split('-')[2]
		}
		
				//note : org tree에서 바로 ID를 org_path_array에 obj로 집어넣는게 더 효율적이나, 생각하기 편한 방식으로 일단 구현함..
			//var comp_obj = adminGetTeamObjByName(this.org_path_array[this.org_path_array.length - 1]);
			
			var role_code;			
//			
//			if(Top.Dom.selectById('UserInfoAdd_UserCompanyPositionContent').getSelectedText() === "팀원"){
//				role_code = "WKS0005";
//			} else{
//				role_code = "WKS0004";
//			}	
			
			var userId = null;
			if(Top.Dom.selectById('adminAddOneGuestDialog').getProperties('title') === '게스트 정보 수정')
				userId = Top.Dom.selectById('guestGroupTableView').getClickedData().USER_ID
				
			var userInfo = {
				 "USER_ID": userId,
				 "USER_LOGIN_ID":Top.Dom.selectById('UserInfoAdd_UserIDContent').getText(),
		         "USER_PW":Top.Dom.selectById('UserInfoAdd_UserPasswordContent').getText(),
		         "USER_NAME":Top.Dom.selectById('UserInfoAdd_UserFirstNameContent').getText(),
		         "USER_EMAIL":Top.Dom.selectById('UserInfoAdd_UserEmailIDContent').getText() + '@' + Top.Dom.selectById('UserInfoAdd_UserEmailDomainContent').getText(),
		         "USER_BIRTH":birthDate,
		         "USER_PHONE":Top.Dom.selectById('UserInfoAdd_UserCellPhoneNumContent0').getText() + '-'
		         + Top.Dom.selectById('UserInfoAdd_UserCellPhoneNumContent1').getText() + '-'
		         + Top.Dom.selectById('UserInfoAdd_UserCellPhoneNumContent2').getText(),
		         "USER_ADDRESS" :Top.Dom.selectById('UserInfoAdd_UserOfficeLocationContent').getText(),
		         "PROFILE_PHOTO":image
			}; 
			var guestComProfileList= [];
			
			for(let n = 0; n < Top.Controller.get('adminAddOneGuestLayoutLogic').ee.length; n++) {
				guestComProfileList.push(
						{
							"USER_ROLE":Top.Dom.selectById('orgkingg'+(n+1)).isChecked() ? "WKS0004" : "WKS0005",
							"COMPANY_NAME":Top.Dom.selectById('UserInfoAdd_UserComNameContent').getText(),
							"DEPARTMENT_NAME" : Top.Dom.selectById('depart_text').getText(),
				            "USER_JOB":Top.Dom.selectById('job_text').getText(),
				            "USER_AFF":Top.Controller.get('adminAddOneGuestLayoutLogic').ee[n].USER_AFF,
				            "USER_COM_NUM": Top.Dom.selectById('UserInfoAdd_UserCompanyPhoneNumContent0').getText() + '-'
				            + Top.Dom.selectById('UserInfoAdd_UserCompanyPhoneNumContent1').getText() + '-' + Top.Dom.selectById('UserInfoAdd_UserCompanyPhoneNumContent2').getText(),
				            "FAX_NUM": Top.Dom.selectById('UserInfoAdd_UserFAXNumContent0').getText() + '-'
				            + Top.Dom.selectById('UserInfoAdd_UserFAXNumContent1').getText() + '-' + Top.Dom.selectById('UserInfoAdd_UserFAXNumContent2').getText(),
						})
			}
			for(let j = Top.Controller.get('adminAddOneGuestLayoutLogic').ee.length + 1; j<this.aa; j++ ){
				guestComProfileList.push(
						{
							"USER_ROLE":Top.Dom.selectById('orgkingg'+j).isChecked() ? "WKS0004" : "WKS0005",
							"COMPANY_NAME":Top.Dom.selectById('UserInfoAdd_UserComNameContent').getText(),
							"DEPARTMENT_NAME" : Top.Dom.selectById('depart_text').getText(),
				            "USER_JOB":Top.Dom.selectById('job_text').getText(),
				            "USER_AFF":$('#TextView_affiliation_path_0'+j).attr('CODE'),
				            "USER_COM_NUM": Top.Dom.selectById('UserInfoAdd_UserCompanyPhoneNumContent0').getText() + '-'
				            + Top.Dom.selectById('UserInfoAdd_UserCompanyPhoneNumContent1').getText() + '-' + Top.Dom.selectById('UserInfoAdd_UserCompanyPhoneNumContent2').getText(),
				            "FAX_NUM": Top.Dom.selectById('UserInfoAdd_UserFAXNumContent0').getText() + '-'
				            + Top.Dom.selectById('UserInfoAdd_UserFAXNumContent1').getText() + '-' + Top.Dom.selectById('UserInfoAdd_UserFAXNumContent2').getText(),
						})
			}

			var inputDTO = {
				dto: {
			        "userInfo": userInfo,
			        "guestComProfileList": guestComProfileList
			    }
			};
			
			
			 if(Top.Dom.selectById('adminAddOneGuestDialog').getProperties('title') === '게스트 개별 추가') {
				Top.Ajax.execute({
					url : _workspace.url + "Admin/OrgGuest",
					type : 'POST',
					dataType : "json",
					async : false,
					cache : false,
					data : JSON.stringify(inputDTO), // arg
					contentType : "application/json",
					crossDomain : true,
					xhrFields : {
					    withCredentials: true
					},
					headers: {
						"ProObjectWebFileTransfer":"true"
					},
					success : function(data) {
						Top.Dom.selectById('adminAddOneGuestDialog').close(true);
	
						Top.Controller.get('adminGuestManageLayoutLogic').treeNodeClick(MouseEvent, 'guestGroupTreeView', Top.Dom.selectById('guestGroupTreeView').getSelectedNode());
						
						
					},
					
					error : function(data){
						Top.Dom.selectById("UserInfoAdd_UserIDContent").setProperties({"border-color":"red"})
						Top.Dom.selectById("UserInfoAdd_UserIDInfoErrorIcon").setProperties({"visible":"visible","tooltip-top":"중복된 ID입니다."})
						return;
						
						
					}
				});
			} else if(Top.Dom.selectById('adminAddOneGuestDialog').getProperties('title') === '게스트 정보 수정') {
				Top.Ajax.execute({
					url : _workspace.url + "Admin/OrgGuest",
					type : 'PUT',
					dataType : "json",
					async : false,
					cache : false,
					data : JSON.stringify(inputDTO), // arg
					contentType : "application/json",
					crossDomain : true,
					xhrFields : {
					    withCredentials: true
					},
					headers: {
						"ProObjectWebFileTransfer":"true"
					},
					success : function(data) {
						Top.Dom.selectById('adminAddOneGuestDialog').close();
	
						Top.Controller.get('adminGuestManageLayoutLogic').treeNodeClick(MouseEvent, 'guestGroupTreeView', Top.Dom.selectById('guestGroupTreeView').getSelectedNode());
						
						
					},
					
					error : function(data){
						console.info("Failed to update")
					}
				});
			}
		
	},
	

	iconclick : function(event, widget) {
		let fieldid = widget.id;
		var icon = $("#" + widget.id).find("#" + widget.id);

		if(Top.Dom.selectById(fieldid).getProperties("password") == true){
			Top.Dom.selectById(fieldid).setProperties({"icon": "icon-ic_password_show", "password":"false"});
			// 보이기모드
			icon.attr('type','text');
		}else{
			Top.Dom.selectById(fieldid).setProperties({"icon": "icon-ic_password_hide", "password":"true"});
			//숨기기 모드
			icon.attr('type','password')
		}
	},
	//프로필 사진 저장
	choosefile_add : function(event, widget) {
		
			let fileChooser = Top.Device.FileChooser.create({
			    onBeforeLoad : function() {
			        let newFileList = [];
					let file = {};
					let tmpId = 0;
			        for(var i=0; i<this.file.length; i++) {		  
			            file = this.file[i];
			            
	    		        let loadFile = fileManager.onBeforeLoadFile(file); 
	    		        if(!loadFile.result) {
	    		            try {
	    		            	TeeToast.open({
	    		            		text: loadFile.message,
	    		            		size: 'min'
	    		            	});
	    		            } catch (e) {}
	    		
	    		        }
	    		        
	    		        let nomalizedFileFullName = Top.Util.Browser.isIE() ? file.name : file.name.normalize('NFC');        
	    		        let fileNameArray = nomalizedFileFullName.split('.');
	    		        let fileExt = fileNameArray.length == 1 ? "" : fileNameArray[fileNameArray.length - 1];        
	    		        if(fileExt == "jpeg" || fileExt == "png" || fileExt == "jpg" || fileExt == "JPEG" || fileExt == "PNG" || fileExt == "JPG") {
	    		    		
	    		        }else{
	    		        	try {
	    		        		Top.Dom.selectById("Icon_admin_profile_error_add").setProperties({"visible":"visible", "tooltip-top":"JPG, PNG 파일만 가능합니다."}); 
	    		        		return false;
	    						
	    		            } catch (e) {}
	    		
	    		        }
						
						newFileList.push({
	    		            file_id         : tmpId++,
	                        file_extension  : fileExt,
	                        fileFullName    : nomalizedFileFullName,
	                        file_status     : FileStatus.TRY_UPLOAD,
						});
	                }
			      
		        },
				onFileChoose : Top.Controller.get('adminUserInfoAddLogic').onFileSelected_add,
			    charset: "euc-kr",
			    multiple: true
		    });
			fileChooser.show();
			
		//}else{
			// 티 드라이브에서 열기
//			var config = {
//					buttonText1: '열기',
//					buttonText2: '취소',
//					buttonFn1: function(){},
//					buttonFn2: function(){},
//					onOpenFn: function(){},
//					onCloseFn: function(){},
//					onOverlayClickFn: function(){},
//			}
//			OPEN_TDRIVE(config);
		//}
		
	},	onFileSelected_add : function(fileChooser) {
		let src = fileChooser[0].src;
        Top.Dom.selectById("ImageButton_Admin_Profile_Setting_add").setSrc(src)
     
		//let _USER_ID =  userManager.getLoginUserId()
		let Fext = src.split('/')[1].split(";")[0];
		let srcSend = src.split(',')[1];
//		let inputDTO ={
//            	files : [{
//                    filename : 'profilePhoto.' + Fext,
//                    contents : srcSend,
//                }],
//		          dto: {
//		            USER_ID: user_id,
//		            PROFILE_PHOTO : src 
//		          }
//		     };

    }, delete_pic_add : function(event, widget){
    	
    	
		encodeImage(userManager.getUserDefaultPhotoURL(userManager.getLoginUserId()), deletePhoto_add(enImage))
        
    }, saveGuestInfo : function(event, widget) {
		 
    	
    	
    	
	}, gValidCheck : function(event, widget) {
		
		var mobileNum = tds('UserInfoAdd_UserCellPhoneNumContent0').getText()
		if(mobileNum === null || mobileNum.length === 0){
			tds('Icon617_12').setProperties({"visible":"visible"})
			tds('Icon832_1_16').setProperties({"visible":"none"})
			tds('UserInfoAdd_UserCellPhoneNumContent0').addClass('alert')
		}else{
			if(pattern_engs.test(mobileNum) || pattern_engb.test(mobileNum)	|| pattern_spc2.test(mobileNum) ||  pattern_kor.test(mobileNum)	||  pattern_blank.test(mobileNum))
				{
					tds("UserInfoAdd_UserCellPhoneNumContent0").addClass("alert")
					tds("Icon832_1_16").setProperties({"visible":"visible"})
					tds('Icon617_12').setProperties({"visible":"none"})
					setTimeout(function(){
						Top.Dom.selectById("MobileValidPopover").open()
					}, 100)
					return false;
				}
			else{
				tds("UserInfoAdd_UserCellPhoneNumContent0").removeClass("alert")
				tds("Icon832_1_16").setProperties({"visible":"none"})
				tds('Icon617_12').setProperties({"visible":"none"})
			}
		
		
		
	}


	}, 
	gValidCheck2 : function(event, widget) {
		var mobileNum = tds('UserInfoAdd_UserCellPhoneNumContent1').getText()
		if(mobileNum === null || mobileNum.length === 0){
			tds('Icon617_12').setProperties({"visible":"visible"})
			tds('Icon832_1_16').setProperties({"visible":"none"})
			tds('UserInfoAdd_UserCellPhoneNumContent1').addClass('alert')
		}else{
			if(pattern_engs.test(mobileNum) || pattern_engb.test(mobileNum)	|| pattern_spc2.test(mobileNum) ||  pattern_kor.test(mobileNum)	||  pattern_blank.test(mobileNum))
				{
					tds("UserInfoAdd_UserCellPhoneNumContent1").addClass("alert")
					tds("Icon832_1_16").setProperties({"visible":"visible"})
					tds('Icon617_12').setProperties({"visible":"none"})
					setTimeout(function(){
						Top.Dom.selectById("MobileValidPopover").open()
					}, 100)
					return false;
				}
			else{
				tds("UserInfoAdd_UserCellPhoneNumContent1").removeClass("alert")
				tds("Icon832_1_16").setProperties({"visible":"none"})
				tds('Icon617_12').setProperties({"visible":"none"})
			}
		
		}
		
		
	}, 
	gValidCheck3 : function(event, widget) {
		var mobileNum = tds('UserInfoAdd_UserCellPhoneNumContent2').getText()
		if(mobileNum === null || mobileNum.length === 0){
			tds('Icon617_12').setProperties({"visible":"visible"})
			tds('Icon832_1_16').setProperties({"visible":"none"})
			tds('UserInfoAdd_UserCellPhoneNumContent2').addClass('alert')
		}else{
			if(pattern_engs.test(mobileNum) || pattern_engb.test(mobileNum)	|| pattern_spc2.test(mobileNum) ||  pattern_kor.test(mobileNum)	||  pattern_blank.test(mobileNum))
				{
					tds("UserInfoAdd_UserCellPhoneNumContent2").addClass("alert")
					tds("Icon832_1_16").setProperties({"visible":"visible"})
					tds('Icon617_12').setProperties({"visible":"none"})
					setTimeout(function(){
						Top.Dom.selectById("MobileValidPopover").open()
					}, 100)
					return false;
				}
			else{
				tds("UserInfoAdd_UserCellPhoneNumContent2").removeClass("alert")
				tds("Icon832_1_16").setProperties({"visible":"none"})
				tds('Icon617_12').setProperties({"visible":"none"})
			}
		
		}
	
	
	
	}, 
	gValidCheck4 : function(event, widget) {
		
		
		
		var mobileNum = tds('UserInfoAdd_UserCompanyPhoneNumContent0').getText()
		if(mobileNum === null || mobileNum.length === 0){
			tds('Icon617_22').setProperties({"visible":"visible"})
			tds('Icon832_1_26').setProperties({"visible":"none"})
			tds('UserInfoAdd_UserCompanyPhoneNumContent0').addClass('alert')
		}else{
			if(pattern_engs.test(mobileNum) || pattern_engb.test(mobileNum)	|| pattern_spc2.test(mobileNum) ||  pattern_kor.test(mobileNum)	||  pattern_blank.test(mobileNum))
				{
					tds("UserInfoAdd_UserCompanyPhoneNumContent0").addClass("alert")
					tds("Icon832_1_26").setProperties({"visible":"visible"})
					tds('Icon617_22').setProperties({"visible":"none"})
					setTimeout(function(){
						Top.Dom.selectById("MobileValidPopover").open()
					}, 100)
					return false;
				}
			else{
				tds("UserInfoAdd_UserCompanyPhoneNumContent0").removeClass("alert")
				tds("Icon832_1_26").setProperties({"visible":"none"})
				tds('Icon617_22').setProperties({"visible":"none"})
			}
		
	}},
	gVlaldCheck5 : function(event, widget) {
	
		var mobileNum = tds('UserInfoAdd_UserCompanyPhoneNumContent1').getText()
		if(mobileNum === null || mobileNum.length === 0){
			tds('Icon617_22').setProperties({"visible":"visible"})
			tds('Icon832_1_26').setProperties({"visible":"none"})
			tds('UserInfoAdd_UserCompanyPhoneNumContent1').addClass('alert')
		}else{
			if(pattern_engs.test(mobileNum) || pattern_engb.test(mobileNum)	|| pattern_spc2.test(mobileNum) ||  pattern_kor.test(mobileNum)	||  pattern_blank.test(mobileNum))
				{
					tds("UserInfoAdd_UserCompanyPhoneNumContent1").addClass("alert")
					tds("Icon832_1_26").setProperties({"visible":"visible"})
					tds('Icon617_22').setProperties({"visible":"none"})
					setTimeout(function(){
						Top.Dom.selectById("MobileValidPopover").open()
					}, 100)
					return false;
				}
			else{
				tds("UserInfoAdd_UserCompanyPhoneNumContent1").removeClass("alert")
				tds("Icon832_1_26").setProperties({"visible":"none"})
				tds('Icon617_22').setProperties({"visible":"none"})
			}
	
	
	}
	
	
	
	
	
	
	}, 
	gValidCheck6 : function(event, widget) {
		var mobileNum = tds('UserInfoAdd_UserCompanyPhoneNumContent2').getText()
		if(mobileNum === null || mobileNum.length === 0){
			tds('Icon617_22').setProperties({"visible":"visible"})
			tds('Icon832_1_26').setProperties({"visible":"none"})
			tds('UserInfoAdd_UserCompanyPhoneNumContent2').addClass('alert')
		}else{
			if(pattern_engs.test(mobileNum) || pattern_engb.test(mobileNum)	|| pattern_spc2.test(mobileNum) ||  pattern_kor.test(mobileNum)	||  pattern_blank.test(mobileNum))
				{
					tds("UserInfoAdd_UserCompanyPhoneNumContent2").addClass("alert")
					tds("Icon832_1_26").setProperties({"visible":"visible"})
					tds('Icon617_22').setProperties({"visible":"none"})
					setTimeout(function(){
						Top.Dom.selectById("MobileValidPopover").open()
					}, 100)
					return false;
				}
			else{
				tds("UserInfoAdd_UserCompanyPhoneNumContent2").removeClass("alert")
				tds("Icon832_1_26").setProperties({"visible":"none"})
				tds('Icon617_22').setProperties({"visible":"none"})
			}
	
		
		
		
	}
	}, 
	gValidCheck7 : function(event, widget) {
		var mobileNum = tds('UserInfoAdd_UserFAXNumContent0').getText()
		if(mobileNum === null || mobileNum.length === 0){
			tds('Icon617_32').setProperties({"visible":"visible"})
			tds('Icon832_1_36').setProperties({"visible":"none"})
			tds('UserInfoAdd_UserFAXNumContent0').addClass('alert')
		}else{
			if(pattern_engs.test(mobileNum) || pattern_engb.test(mobileNum)	|| pattern_spc2.test(mobileNum) ||  pattern_kor.test(mobileNum)	||  pattern_blank.test(mobileNum))
				{
					tds("UserInfoAdd_UserFAXNumContent0").addClass("alert")
					tds("Icon832_1_36").setProperties({"visible":"visible"})
					tds('Icon617_32').setProperties({"visible":"none"})
					setTimeout(function(){
						Top.Dom.selectById("MobileValidPopover").open()
					}, 100)
					return false;
				}
			else{
				tds("UserInfoAdd_UserFAXNumContent0").removeClass("alert")
				tds("Icon832_1_36").setProperties({"visible":"none"})
				tds('Icon617_32').setProperties({"visible":"none"})
			}
		
		
		
		
	}
	}, 
	gValidCheck8 : function(event, widget) {
		var mobileNum = tds('UserInfoAdd_UserFAXNumContent1').getText()
		if(mobileNum === null || mobileNum.length === 0){
			tds('Icon617_32').setProperties({"visible":"visible"})
			tds('Icon832_1_36').setProperties({"visible":"none"})
			tds('UserInfoAdd_UserFAXNumContent1').addClass('alert')
		}else{
			if(pattern_engs.test(mobileNum) || pattern_engb.test(mobileNum)	|| pattern_spc2.test(mobileNum) ||  pattern_kor.test(mobileNum)	||  pattern_blank.test(mobileNum))
				{
					tds("UserInfoAdd_UserFAXNumContent1").addClass("alert")
					tds("Icon832_1_36").setProperties({"visible":"visible"})
					tds('Icon617_32').setProperties({"visible":"none"})
					setTimeout(function(){
						Top.Dom.selectById("MobileValidPopover").open()
					}, 100)
					return false;
				}
			else{
				tds("UserInfoAdd_UserFAXNumContent1").removeClass("alert")
				tds("Icon832_1_36").setProperties({"visible":"none"})
				tds('Icon617_32').setProperties({"visible":"none"})
			}
		
		
		
	}
	}, 
	gValidCheck9 : function(event, widget) {
	
		var mobileNum = tds('UserInfoAdd_UserFAXNumContent2').getText()
		if(mobileNum === null || mobileNum.length === 0){
			tds('Icon617_32').setProperties({"visible":"visible"})
			tds('Icon832_1_36').setProperties({"visible":"none"})
			tds('UserInfoAdd_UserFAXNumContent2').addClass('alert')
		}else{
			if(pattern_engs.test(mobileNum) || pattern_engb.test(mobileNum)	|| pattern_spc2.test(mobileNum) ||  pattern_kor.test(mobileNum)	||  pattern_blank.test(mobileNum))
				{
					tds("UserInfoAdd_UserFAXNumContent2").addClass("alert")
					tds("Icon832_1_36").setProperties({"visible":"visible"})
					tds('Icon617_32').setProperties({"visible":"none"})
					setTimeout(function(){
						Top.Dom.selectById("MobileValidPopover").open()
					}, 100)
					return false;
				}
			else{
				tds("UserInfoAdd_UserFAXNumContent2").removeClass("alert")
				tds("Icon832_1_36").setProperties({"visible":"none"})
				tds('Icon617_32').setProperties({"visible":"none"})
			}
	
	
	
	
	}
	
	}

});





function adminProfile_list_add(e, clickID) {
	
	if(e.toElement != undefined){
		if(e.toElement.id == clickID){
			more_list_adminProfile_add =
				'<top-linearlayout id="Sidebar_drop_adminProfile_add" class="position-Sidebar_drop_adminProfile_add linear-child-vertical" layout-width="97px" layout-height="auto" layout-horizontal-alignment="center" padding="" margin="" horizontal-alignment="center" vertical-alignment="middle" visible="visible" border-color="rgba(205,205,207,1.0) rgba(205,205,207,1.0) rgba(205,205,207,1.0) rgba(205,205,207,1.0)" border-width="1px 1px 1px 1px" horizontal-scroll="false" vertical-scroll="false" orientation="vertical" position="false" title="" layout-tab-disabled="false">' +
				'<div id="Sidebar_drop_adminProfile_add" class="top-linearlayout-root">' +
					'<top-button id="Sidebar_drop_adminProfile_add_pc" class="noti_drop linear-child-vertical" layout-width="match_parent" layout-height="28px" padding="0px 0px 1px 10px" horizontal-alignment="left" border-color="rgba(205,205,207,1.0) rgba(205,205,207,1.0) rgba(205,205,207,1.0) rgba(205,205,207,1.0)" border-width="0px 0px 1px 0px" tab-index="0" text="PC" text-color="rgba(51,51,51,1.0)" text-decoration="" icon-position="left" title="" layout-tab-disabled="false" actual-height="29">' +
						'<button id="Sidebar_drop_adminProfile_add_pc" class="top-button-root" type="button" tabindex="0">' +
							'<label class="top-button-text">PC</label>' +
						'</button>' +
					'</top-button>' +
//					'<top-button id="Sidebar_drop2_tdrive" class="noti_drop linear-child-vertical" layout-width="match_parent" layout-height="28px" padding="0px 0px 1px 10px" horizontal-alignment="left" tab-index="0" text="T-Drive" text-color="rgba(51,51,51,1.0)" text-decoration="" icon-position="left" title="" layout-tab-disabled="false" actual-height="29">' +
//						'<button id="Sidebar_drop2_tdrive" class="top-button-root" type="button" tabindex="0">' +
//							'<label class="top-button-text">T-Drive</label>' +
//						'</button>' +
//					'</top-button>' +
					'<top-button id="Sidebar_drop_adminProfile_add_delete" class="noti_drop linear-child-vertical" layout-width="match_parent" layout-height="28px" padding="0px 0px 1px 10px" horizontal-alignment="left" border-color="rgba(205,205,207,1.0) rgba(205,205,207,1.0) rgba(205,205,207,1.0) rgba(205,205,207,1.0)" border-width="0px 0px 1px 0px" tab-index="0" text="삭제" text-color="rgba(51,51,51,1.0)" text-decoration="" icon-position="left" title="" layout-tab-disabled="false" actual-height="29">' +
						'<button id="Sidebar_drop_adminProfile_add_delete" class="top-button-root" type="button" tabindex="0">' +
							'<label class="top-button-text">삭제</label>' +
						'</button>' +
					'</top-button>' +
				'</div>' +
				'</top-linearlayout>';
			
			if($('top-linearlayout#Sidebar_drop_adminProfile_add').length == 0) {

				more_list_adminProfile_add = $(more_list_adminProfile_add);
				more_list_adminProfile_add.find("button#Sidebar_drop_adminProfile_add_pc").on('click', Top.Controller.get('adminUserInfoAddLogic').choosefile_add);	//pc
				//more_list_picture.find("button#Sidebar_drop2_tdrive").on('click', Top.Controller.get('userProfileLayoutLogic').choosefile);				//t-drive
				more_list_adminProfile_add.find("button#Sidebar_drop_adminProfile_add_delete").on('click', Top.Controller.get('adminUserInfoAddLogic').delete_pic_add);		//삭제

				$('div#UserInfoAdd_UserProfilePictureWrapper').append(more_list_adminProfile_add);
				//if(Top.Dom.selectById('sidebarLayout_tempo0_2').getProperties('visible') == 'visible'){
				$('.position-Sidebar_drop_adminProfile_add').css({'top':'57px','right':'25px'});
				//}	
			//else{
				//$('.position-Sidebar_drop2').css({'top':'56px','right':'59px'});
				//	}
				}
		}
		else{
				
				if($('top-linearlayout#Sidebar_drop_adminProfile_add').length != 0) {
			
				$('top-linearlayout#Sidebar_drop_adminProfile_add').remove();
			}
		};
	};
}

Top.Controller.create('adminEditGuestGroupNameLayoutLogic', {
	orgId : String.empty,
	
	onSuccess : function(event, widget) {		
		let newGuestGroupName = Top.Dom.selectById("editedGroupName").getText();
			
		let orgId = this.orgId;
		let comCode = orgId.split("_")[0];
		let departmentCode = orgId.split("_")[1];
			
		let inputDTO = {
		    dto : {
		        orgList : [{
		        	ORG_URL			:_mailDomain,
		            COMPANY_CODE    : comCode,
		            DEPARTMENT_CODE : departmentCode,
		            ORG_TYPE		: "ADM0022",
		            ORG_NAME        : newGuestGroupName
		        }]
		    }
	    }
			    
		Top.Ajax.execute({
	        url : _workspace.url + "Admin/Org",
	        type : 'PUT',
	        dataType : "json",
	        async : false,
	        cache : false,
	        data : JSON.stringify(inputDTO), // arg
	        contentType : "application/json",
	        crossDomain : true,
	        xhrFields : {
	            withCredentials: true
	        },
	        success : function(data) {
	        	Top.Dom.selectById("guestGroupTreeView").editNodeText(orgId, newGuestGroupName);
	        	Top.Dom.selectById("tvSelectedGuestGroup").setText(newGuestGroupName);
	        },
	        error : function(data) {
	        }
	    });
			
		Top.Dom.selectById("adminEditGuestGroupNameDialog").close(true);
	}, 
	
	onFail : function(event, widget) {
		Top.Dom.selectById("adminEditGuestGroupNameDialog").close(true);
	}
	
});

Top.Controller.create('adminAddManyGuestLayoutLogic', {
    selectedFileName : String.empty,
    selectedFileContents : String.empty,
    
    downloadFormatFile : function(event, widget) {
        getFormatFile("GuestImportFormat.xlsx");
    },

	onSelectFormatFileBtnClick : function(event, widget) {
	    selectFormatFile(function(excel, fileName, fileContents) {
	        let sheet = excel.SheetNames.length == 1 ? excel.Sheets[format.SheetNames[0]] : excel.Sheets["직원"];
	        
	        let range = XLSX.utils.decode_range(sheet["!ref"]);
	        let headers = [];
	        let header = String.empty;
	        for(var col=range.s.c; col<=range.e.c; col++) {
	            header = sheet[XLSX.utils.encode_cell({ r: 0, c: col })];
                if(header == undefined) {
                    continue;
                }
	            
	            headers.push(header.v);
	        }
	        
	        // TODO : i18n 적용할 것
	        let GuestImportFormatHeaderToModel = {
                "이름" : "USER_NAME",
                "소속 회사코드" : "COMPANY_CODE",
                "소속 부서코드" : "DEPARTMENT_CODE",
                "소속 회사코드 (T)" : undefined,
                "소속 부서코드 (T)" : undefined,
                "소속 회사 이름" : "COMPANY_NAME",
                "부서 이름" : "DEPARTMENT_NAME",
                "직위" : undefined,
                "직책" : "USER_JOB",
                "회사전화" : "USER_COM_NUM",
                "이동전화" : "USER_PHONE",
                "팩스번호" : "FAX_NUM",
                "회사 메일 계정" : "USER_EMAIL",
                "생년월일" : "USER_BIRTH",
                "주소" : "USER_ADDRESS",
                "사진파일명" : undefined,
                "조직명" : "ORG_NAME"
            };

	        for(var i=0; i<headers.length; i++) {
	            headers[i] = GuestImportFormatHeaderToModel[headers[i]];
	        }
	        
	        range = XLSX.utils.encode_range({ s : { r:1, c:0 }, e : range.e });
	        let models = XLSX.utils.sheet_to_json(sheet, { header: headers, range: range });
	        
	        for(var i=0; i<models.length; i++) {
	            if(!models[i].hasOwnProperty("USER_NAME")       ||
	               !models[i].hasOwnProperty("COMPANY_CODE")    ||
	               !models[i].hasOwnProperty("DEPARTMENT_CODE") ||
                   !models[i].hasOwnProperty("USER_JOB")        ||
                   !models[i].hasOwnProperty("USER_EMAIL")      ||
                   !models[i].hasOwnProperty("USER_BIRTH")) {
	            	TeeAlarm.open({title: '필수 정보가 누락되었습니다.', content: ''})
	                return;
	            }
	        }
	        
	        Top.Dom.selectById("tvFormatFileName").setText(fileName);
	        guestRepo.setValue("guestImportList", models);
	        
	        var adminAddManyGuestLayoutLogic = Top.Controller.get("adminAddManyGuestLayoutLogic");
	        adminAddManyGuestLayoutLogic.selectedFileName = fileName;
	        adminAddManyGuestLayoutLogic.selectedFileContents = fileContents;
	    });
	}, 
	
	onSuccess : function(event, widget) {
	    var adminAddManyGuestLayoutLogic = Top.Controller.get("adminAddManyGuestLayoutLogic");
	    
	    let inputDTO = {
            files : [{
                filename : adminAddManyGuestLayoutLogic.selectedFileName,
                contents : adminAddManyGuestLayoutLogic.selectedFileContents
            }]
	    }
	    
	    let response = { RESULT_CD : String.empty,  };
	    Top.Ajax.execute({
            url : _workspace.url + "Admin/OrgGuestsImport?action=",
            type : 'POST',
            dataType : "json",
            async : false,
            cache : false,
            data : JSON.stringify(inputDTO), // arg
            contentType : "application/json",
            crossDomain : true,
            xhrFields : {
                withCredentials: true
            },
            headers : {
                ProObjectWebFileTransfer : true
            },
            success : function(data) {
                response = JSON.parse(data).dto;
            },
            error : function(data) {
            }
        });

	    if(response.RESULT_CD == AdminResultCode.SUCCESS) {
	        Top.Dom.selectById("adminAddManyGuestDialog").close(true);
	    } else {
	    	TeeToast.open({
	    		text: '게스트 일괄 추가 실패',
	    		size: 'min'
	    	});
	    }
    },
    
    onFail : function(event, widget) {
        Top.Dom.selectById("adminAddManyGuestDialog").close(true);
    }
});







var org_fileName_;
var org_fileContents_;

function tabledragStart(event){
	console.log('drag start');
	if(!document.elementFromPoint(event.clientX, event.clientY).closest('tr').classList.contains('pressed_row')){
		togglePressedRow(event);
	}	
}
function tableDragEnd(event){
	console.log('drag end');
//	togglePressedRow(event);
//	//dragend event일 경우 그냥 전부 press 해재
	
	for(var i=0; i<orgTreeManageRepo.orgTreeManage.length; i++){
		orgTreeManageRepo.orgTreeManage[i].is_pressed = false;
	}
	
	for(var i=0; i< $('table.top-tableview tbody.body')[0].children.length; i++){
		if($(`tbody.body .row_${i}`).hasClass('pressed_row')){
			$(`tbody.body .row_${i}`).toggleClass('pressed_row');
		}
	
		if($(`tbody.body tr.row_${i}`).hasClass('drag_overed')){
			$(`tbody.body tr.row_${i}`).removeClass('drag_overed');
		}
	}
	
//	var table_idx = document.elementFromPoint(event.clientX, event.clientY).closest('tr').rowIndex;
	
//	if($(`tbody.body tr.row_${table_idx}`).hasClass('drag_overed')){
//		$(`tbody.body tr.row_${table_idx}`).removeClass('drag_overed');
//	}
}
function tableDrop(event){
	event.preventDefault();
	console.log('drop');
	//~조직을 ~조직 하위로 이동하시겠습니까?
	
	var startRowNum = Top.Dom.selectById("orgTreeTableView").getStartRowNum()
	var clickedData = Top.Dom.selectById('orgTreeTableView').getClickedData();
	var clicked_repo_idx = Top.Dom.selectById('orgTreeTableView').getClickedIndex()[0];
	var last_child_repo_idx;
	
	//옮기는 부서 및  자식들 정보 push(tmp)로 만들어야함.
	var COMPANY_CODE = clickedData.company_code;
	var DEPARTMENT_CODE = clickedData.department_code; 
	
	//target 부서 정보
	var target_repo_idx = event.currentTarget.rowIndex + startRowNum; //event.currentTarget.rowIndex -> table_idx
//	console.log(target_repo_idx);
//	return;
	var target_info = orgTreeManageRepo.orgTreeManage[target_repo_idx];//target event 왜 달라지지
	var target_company_code = target_info.company_code;
	var PARENT_ORG_DEPARTMENT_CODE = target_info.department_code;
	var ORG_LEVEL = parseInt(target_info.org_level)+1;
		
	//지가 지로 옮길 때
	if((target_company_code === COMPANY_CODE)&&(PARENT_ORG_DEPARTMENT_CODE === DEPARTMENT_CODE)){
//		alert('자기 하위로는 이동할 수 없습니다.');
		return;
	}
	
	//회사 다를 때 경고
	if(target_company_code !== COMPANY_CODE){
		alert('다른 회사 하위로는 조직을 이동할 수 없습니다.');
		return;
	}
	//자기 자신 아래로 이동하는 경우 경고////////////////////
	
	//idx타고 가면서 org_level조사하기. parent_org_level 보다 작거나 같은게 나오면 그자리가 last child //누구 child말하는거야 target? 아님 clicked? -> clicked
	for(var i=clicked_repo_idx+1; ;i++){
		var parent_org_level = parseInt(orgTreeManageRepo.orgTreeManage[clicked_repo_idx].org_level);
		
		if(i < orgTreeManageRepo.orgTreeManage.length){
			var tmp_org_level = parseInt(orgTreeManageRepo.orgTreeManage[i].org_level);
			
			if(tmp_org_level <= parent_org_level){
				last_child_repo_idx = i;
				break;
			}
			
		} else {
			last_child_repo_idx = orgTreeManageRepo.orgTreeManage.length;
			break;
		}        			
	}
	
	var moved_org = {};//clicked org만 PO콜 태워주면됨.
	moved_org.COMPANY_CODE = orgTreeManageRepo.orgTreeManage[clicked_repo_idx].company_code;
	moved_org.DEPARTMENT_CODE = orgTreeManageRepo.orgTreeManage[clicked_repo_idx].department_code;
	moved_org.PARENT_ORG_DEPARTMENT_CODE = PARENT_ORG_DEPARTMENT_CODE;
	moved_org.ORG_LEVEL = ORG_LEVEL;
	moved_org.ORG_TYPE = "ADM0021";
	moved_org.ORG_URL = _mailDomain;
	
	
	let data={
			dto: {
				orgList: [moved_org]
			}
	};
	
	
	Top.Ajax.execute({
		type : "PUT",
		url : _workspace.url+"Admin/Org",
		dataType : "json",
		async : false,
		cache : false,
		data : JSON.stringify(data),
		contentType: "application/json; charset=utf-8",
	    xhrFields: {withCredentials: true},
		crossDomain : true,
		success : function(ret, xhr, status) {
            try {
            	console.log('org move success');
            	OrgManager.List.update(data.dto.orgList[0]);

            	//REPO 정리
            	createOrgTree(null, "ADM0021"); //Repo 정리
            	
            	//target arrow 펼쳐진 경우, 안펼쳐진 경우 나누기
            	if( orgTreeManageRepo.orgTreeManage[target_repo_idx].icon_status === "icon-arrow_right" ){
            		//안펼쳐져 있는 경우, 옮기는거만 안보이게 하면 됨.
            		orgTreeManageRepo.orgTreeManage.splice(clicked_repo_idx, last_child_repo_idx - clicked_repo_idx);
            		
            	} else {
            		//repo에서 옮기기
                	var moved_orgs = orgTreeManageRepo.orgTreeManage.slice(clicked_repo_idx,last_child_repo_idx);
                	//org_level 보정
                	var moved_orgs_parent_org_level = parseInt(moved_orgs[0].org_level);
                	for(var i=0; i<moved_orgs.length; i++){            		
                		moved_orgs[i].org_level = parseInt(moved_orgs[i].org_level) - ( moved_orgs_parent_org_level - parseInt(orgTreeManageRepo.orgTreeManage[target_repo_idx].org_level)  -1 );
                		console.log(moved_orgs[i].org_level, moved_orgs[0].org_level, orgTreeManageRepo.orgTreeManage[target_repo_idx].org_level);
                	}
            		orgTreeManageRepo.orgTreeManage.splice(clicked_repo_idx, last_child_repo_idx - clicked_repo_idx);
            		
            		//idx보정, target 보다 위에서 조직을 뺀경우 딸린 조직 수만큼 idx 바뀜.
            		if(target_repo_idx>clicked_repo_idx){
            			target_repo_idx = target_repo_idx - (last_child_repo_idx - clicked_repo_idx);
            		}
            		console.log(moved_orgs);
            		var target_last_child_repo_idx = getLastChildRepoIndex(target_repo_idx);
            		for(var i=0; i<moved_orgs.length; i++){
            			orgTreeManageRepo.orgTreeManage.splice(target_last_child_repo_idx, 0, moved_orgs[moved_orgs.length-i-1]);
            		}
            		
            	}
            	
            	
            	//자식이 다 옮겨졌을 경우 화살표 보정            	
            	for(var i=0; i<orgTreeManageRepo.orgTreeManage.length; i++){
            		if(!isRowHasChildren(i)){
            			orgTreeManageRepo.orgTreeManage[i].icon_status = 'icon-arrow_right';
            		}
            	}
            	
            	
            	Top.Controller.get('adminOrgTreeManageLayoutLogic').setOrgTreeTableView();

            	
            } catch (error) {
               console.log('err');
               console.log(error);
            }
		},
		complete : function(ret, xhr, status) {
			
		},
		error : function(ret, xhr, status) {
			
		}
	});
	
	
	
	
	
	
	
	
	
	
	console.log(PARENT_ORG_DEPARTMENT_CODE, ORG_LEVEL);
//	console.log('to',event.currentTarget.classList[1]);
}
function getLastChildRepoIndex(repo_idx){//last_child_idx 는 현재 있는 lastChild가 아니라, 새로운 last_child가 추가될 곳의 idx를 말함. idx는 parent_idx
	var last_child_repo_idx;
	
	//idx타고 가면서 org_level조사하기. parent_org_level 보다 작거나 같은게 나오면 그자리가 last child
	for(var i=repo_idx+1; ;i++){
		var parent_org_level = parseInt(orgTreeManageRepo.orgTreeManage[repo_idx].org_level);
		
		if(i < orgTreeManageRepo.orgTreeManage.length){
			var tmp_org_level = parseInt(orgTreeManageRepo.orgTreeManage[i].org_level);
			
			if(tmp_org_level <= parent_org_level){
				last_child_repo_idx = i;
				break;
			}
			
		} else {
			last_child_repo_idx = orgTreeManageRepo.orgTreeManage.length;
			break;
		}        			
	}
	
	return last_child_repo_idx; 
}
function tableAllowDrop(event){
	event.preventDefault();
}
function tableDragEnter(event){
	var table_idx = document.elementFromPoint(event.clientX, event.clientY).closest('tr').rowIndex;
	console.log('enter idx',table_idx);
	if(!$(`tbody.body tr.row_${table_idx}`).hasClass('drag_overed')){
		$(`tbody.body tr.row_${table_idx}`).addClass('drag_overed');
		console.log($(`tbody.body tr.row_${table_idx}`).hasClass('drag_overed'));
	}
	
	if( $(`tbody.body tr.row_${table_idx+1}`).hasClass('drag_overed') ){
		$(`tbody.body tr.row_${table_idx+1}`).removeClass('drag_overed');
	}
	if( $(`tbody.body tr.row_${table_idx-1}`).hasClass('drag_overed') ){
		$(`tbody.body tr.row_${table_idx-1}`).removeClass('drag_overed');
	}
}
function tableDragLeave(event){
	
}
function togglePressedRow(event){
//	event.stopPropagation();
//	event.preventDefault();
//	console.log(isRowHasChildren(event.currentTarget.rowIndex));
//	return;
	console.log('start toggle');
	
	var startRowNum = Top.Dom.selectById("orgTreeTableView").getStartRowNum();
	var clicked_idx = event.currentTarget.rowIndex;
	var repo_idx = event.currentTarget.rowIndex + startRowNum;
	
	if(event.target.tagName === 'I'){
		clicked_idx = document.elementFromPoint(event.clientX, event.clientY).closest('tr').rowIndex;
		repo_idx = clicked_idx + startRowNum;
	}
//	if( (event.type === 'mousedown')&&($(`tbody.body .row_${event.currentTarget.rowIndex}`).hasClass('pressed_row')) || (event.type === 'mouseup')&&(!$(`tbody.body .row_${event.currentTarget.rowIndex}`).hasClass('pressed_row')) ){
//		return;
//	} //up도 예외처리
	
//	//dragend event일 경우 그냥 전부 press 해재 --> dragend event로 이동
//	if(event.type === 'dragend'){
//	for(var i=0; i< $('table.top-tableview tbody.body')[0].children.length; i++){
//		if($(`tbody.body .row_${i}`).hasClass('pressed_row')){
//			$(`tbody.body .row_${i}`).removeClass('pressed_row');
//		}		
//	}
//		return;
//	}
	
	
	var last_child_repo_idx;
	
	//idx타고 가면서 org_level조사하기. parent_org_level 보다 작거나 같은게 나오면 그자리가 last child
	for(var i=repo_idx+1; ;i++){
		var parent_org_level = parseInt(orgTreeManageRepo.orgTreeManage[repo_idx].org_level);
		
		if(i < orgTreeManageRepo.orgTreeManage.length){
			var tmp_org_level = parseInt(orgTreeManageRepo.orgTreeManage[i].org_level);
			
			if(tmp_org_level <= parent_org_level){
				last_child_repo_idx = i;
				break;
			}
			
		} else {
			last_child_repo_idx = orgTreeManageRepo.orgTreeManage.length;
			break;
		}        			
	}
	
	
	//레포 정리를 하고	
	//레포에 따라 테이블을 그림
	
	//레포 정리
	var is_parent_pressed_before;
	if(orgTreeManageRepo.orgTreeManage[repo_idx].is_pressed){ //누른 row의 원래 pressed status 검사
		is_parent_pressed_before = true;
	} else {
		is_parent_pressed_before = false;
	}
	
	var is_parent_parent_pressed_before;
	if(repo_idx === 0){
		is_parent_parent_pressed_before = false;
	} else {
		if(orgTreeManageRepo.orgTreeManage[repo_idx-1].is_pressed){ //누른 row의 원래 pressed status 검사
			is_parent_parent_pressed_before = true;
		} else {
			is_parent_parent_pressed_before = false;
		}
	}
	
	
	for (var i=0; i<orgTreeManageRepo.orgTreeManage.length; i++){
		
		if( (i>= repo_idx) && (i<last_child_repo_idx) ){ //토글 해당 row
			
			if(!is_parent_parent_pressed_before && is_parent_pressed_before){
				orgTreeManageRepo.orgTreeManage[i].is_pressed = false;				
			} else {
				orgTreeManageRepo.orgTreeManage[i].is_pressed = true;
			}			
			
		} else { //토글 미해당 row
			if(orgTreeManageRepo.orgTreeManage[i].is_pressed === true){
				orgTreeManageRepo.orgTreeManage[i].is_pressed = false;
			}			
		}		
	}
	//테이블 그리기
	
	for(var i=0; i<$('div#orgTreeTableView tbody.body')[0].childNodes.length; i++){
		var current_repo_idx = i+startRowNum;
		
		if(orgTreeManageRepo.orgTreeManage[current_repo_idx].is_pressed === true){
			$(`tbody.body .row_${i}`).addClass('pressed_row');
		} else {
			$(`tbody.body .row_${i}`).removeClass('pressed_row');
		}
//		
//		
//		var is_parent_pressed_before;
//		if($(`tbody.body .row_${clicked_idx}`).hasClass('pressed_row')){ //누른 row의 원래 pressed status 검사
//			is_parent_pressed_before = true;
//		} else {
//			is_parent_pressed_before = false;
//		}		
//		
//		if( (i>= clicked_idx) && (i<last_child_repo_idx - startRowNum) ){ //토글 해당 row
//			if(is_parent_pressed_before){
//				$(`tbody.body .row_${i}`).removeClass('pressed_row');
//				
//			} else {
//				$(`tbody.body .row_${i}`).addClass('pressed_row');
//			}
//			
//		} else { //토글 미해당 row
//			if($(`tbody.body .row_${i}`).hasClass('pressed_row')){
//				$(`tbody.body .row_${i}`).removeClass('pressed_row');
//			}			
//		}		
	}
	console.log('done toggle');
	event.stopPropagation();
	
}
function isRowHasChildren(repo_idx){
	if(repo_idx === orgTreeManageRepo.orgTreeManage.length-1 ){
		return false;
	}
	
	var current_org_level = orgTreeManageRepo.orgTreeManage[repo_idx].org_level;
	var next_org_level = orgTreeManageRepo.orgTreeManage[repo_idx+1].org_level;
	
	if (current_org_level >= next_org_level){
		return false;
	}else {
		return true;
	}	
}

Top.Controller.create('adminOrgTreeManageLayoutLogic', {
	
	init: function(event, widget){
		this.isedit = false;
		this.dragObj;
		this.dropObj;
		this.setRepoOrgTreeManage(event, widget);
		
		var totalOrgNum = orgTreeRepo.orgInfoList.length;
		Top.Dom.selectById('TextView299').setText(totalOrgNum+'개');
		

		Top.Dom.selectById('orgTreeTableView').setProperties({'on-update':function(event, widget){
			setTimeout(function(){
				var totalOrgNum = orgTreeRepo.orgInfoList.length;
				Top.Dom.selectById('TextView299').setText(totalOrgNum+'개');		
			}, 0)
		}});
		
		this.setOrgTreeTableView(event,widget);
		
	},
	
	setOrgTreeTableView: function(event, widget){
		var totalOrgNum = orgTreeRepo.orgInfoList.length;
		Top.Dom.selectById('TextView299').setText(totalOrgNum+'개');
		 
		var startRowNum = Top.Dom.selectById("orgTreeTableView").getStartRowNum();
		 
		
		
		orgTreeManageRepo.setValue('orgTreeManage', orgTreeManageRepo.orgTreeManage);
		
		setTimeout(function(){
//			$(".body-row").draggable({
//				helper: function() {
//					var selected = $(".body-row.pressed_row");
//					var container = $('<div/>').attr('id', 'draggingContainer');
//				    container.append(selected.clone());
//				    return container;
//				}
//			});

//			for (let i in orgTreeManageRepo.orgTreeManage){
//				if (orgTreeManageRepo.orgTreeManage[i].is_public === "ADM0011"){
//					$("input:radio[id=public_yes_button_"+i+"_2]").prop('checked',true);				
//					$("input:radio[id=public_no_button_"+i+"_2]").prop('checked',false);				
//				}else {
//					$("input:radio[id=public_yes_button_"+i+"_2]").prop('checked',false);				
//					$("input:radio[id=public_no_button_"+i+"_2]").prop('checked',true);				
//				}
//			}
			
//			$('.body-row').mousedown(function(event, widget) {
//				togglePressedRow(event);							
//			});
//			$('.body-row').mouseup(function(event, widget) {
//				togglePressedRow(event);	
//			});

		},0);
		
	},
	tableDrop : function(event,a,b,c,d){
		//~조직을 ~조직 하위로 이동하시겠습니까?
		const self = this;
		
		let data={
				dto: {
					orgList: [{
						"COMPANY_CODE" : this.dragObj.company_code,
						"DEPARTMENT_CODE": this.dragObj.department_code,
						"ORG_NAME" : this.dragObj.org_name,
						"PARENT_ORG_DEPARTMENT_CODE" : this.dropObj.department_code,
						"ORG_LEVEL" : this.dragObj.LEVEL-1,
						"ORG_URL" : _mailDomain,
						"ORG_TYPE" : "ADM0021"
					}]
				}
		};
		
		
		Top.Ajax.execute({
			type : "PUT",
			url : _workspace.url+"Admin/Org",
			dataType : "json",
			async : false,
			cache : false,
			data : JSON.stringify(data),
			contentType: "application/json; charset=utf-8",
		    xhrFields: {withCredentials: true},
			crossDomain : true,
			success : function(ret, xhr, status) {
	            console.log("success");
	            OrgManager.reload(_mailDomain, "ADM0021")
	            	.then(function() {
	            		createOrgTree("orgTreeTable", "ADM0021", function () {
	    	            	var tmp=[];
	    		    		var nodes = orgTreeRepo.orgTree[0].children; 
	    		    		for(var i=0; i < nodes.length; i++){
	    		    			self.recursiveTreetmp(tmp,nodes[i],1);
	    		    		}
	    		    		for(var i = 0 ; i < orgTreeManageRepo.orgTreeManage.length; i++)
	    		    		{
	    		    			orgTreeManageRepo.orgTreeManage[i].user_count = tmp[i].user_count;
	    		    		}
	    					Top.Dom.selectById('orgTreeTableView').update();
	    	            });
	            	});

//	    		createOrgTree(null, "ADM0021");
			},
			complete : function(ret, xhr, status) {
				
			},
			error : function(ret, xhr, status) {
				
			}
		});
		
//		console.log('to',event.currentTarget.classList[1]);
	},
	getChildElementOnRep: function(event, widget){
		
	},
	recursiveTree : function(tmp, node, level){
		var c_code = node.c_code;
		var d_code = node.d_code;
		var admin_name = node.admin_name;
		var is_pb = node.is_pb;
		var u_cnt = node.u_cnt;
		var org_name = node.text;
		var org_level = node.org_level;
		var checked1, checked2;
		if (is_pb === "ADM0011"){
			checked2 = false;
			checked1 = true;
		} else if (is_pb === "ADM0012") {
			checked2 = true;
			checked1 = false;
		}
		tmp.push({
			'admin_name' : admin_name,
			'is_public' : is_pb,
			'user_count': u_cnt,
			'org_name': org_name,
			'company_code': c_code,
			'department_code': d_code,
			'org_level': org_level, 
			'checked1' : checked1,
			'checked2' : checked2,
			'LEVEL' : level
		})
		if(node.children != null) { // tree children에 매핑
			for(var i = 0 ; i < node.children.length; i++) this.recursiveTree(tmp, node.children[i], level+1);
		}
	},
	recursiveTreetmp : function(tmp, node, level){
		var c_code = node.c_code;
		var d_code = node.d_code;
		var admin_name = node.admin_name;
		var is_pb = node.is_pb;
		var u_cnt = node.u_cnt;
		var org_name = node.text;
		var org_level = node.org_level;
		var checked1, checked2;
		if (is_pb === "ADM0011"){
			checked2 = false;
			checked1 = true;
		} else if (is_pb === "ADM0012") {
			checked2 = true;
			checked1 = false;
		}
		tmp.push({
			'admin_name' : admin_name,
			'is_public' : is_pb,
			'user_count': u_cnt,
			'org_name': org_name,
			'company_code': c_code,
			'department_code': d_code,
			'org_level': org_level, 
			'checked1' : checked1,
			'checked2' : checked2,
			'LEVEL' : level
		})
		if(node.children != null) { // tree children에 매핑
			for(var i = 0 ; i < node.children.length; i++) this.recursiveTree(tmp, node.children[i], level+1);
		}
	},
	
	setRepoOrgTreeManage: function(event, widget){
		
		orgTreeManageRepo.orgTreeManage = [];
		
		
		var tmp=[];
		var nodes = orgTreeRepo.orgTree[0].children; 
		for(var i=0; i < nodes.length; i++){
			this.recursiveTree(tmp,nodes[i],1);
		}
//		for(var i=0, checked1, checked2; i<initial_nodes.length; i++){
//			
//			var admin_name;
//			var is_public;
//			var user_count;
//			var org_name;
//			
//			company = initial_nodes[i].id.split('_')[0];
//			department = initial_nodes[i].id.split('_')[1];
//			var filtered_team = orgTreeRepo.orgInfoList.filter(function(element){return ( (element.DEPARTMENT_CODE == department) && (element.COMPANY_CODE) == company) })[0];
//			
//			admin_name = filtered_team.ADMIN_NAME;
//			is_public = filtered_team.IS_PUBLIC;
//			user_count = filtered_team.USER_COUNT;
//			org_name = filtered_team.ORG_NAME;
//			company_code = filtered_team.COMPANY_CODE;
//			department_code = filtered_team.DEPARTMENT_CODE;
//			org_level = filtered_team.ORG_LEVEL;
//			
//			if (filtered_team.IS_PUBLIC === "ADM0011"){
//				checked2 = false;
//				checked1 = true;
//			} else if (filtered_team.IS_PUBLIC === "ADM0012") {
//				checked2 = true;
//				checked1 = false;
//			}
//			
//			tmp.push({
//				'admin_name' : admin_name,
//				'is_public' : is_public,
//				'user_count': user_count,
//				'org_name': org_name,
//				'company_code': company_code,
//				'department_code': department_code,
//				'org_level': org_level, 
//				'icon_status': "icon-arrow_right",
//				'checked1' : checked1,
//				'checked2' : checked2,
//				'gid' : 'gid'+i,
//				'LEVEL' : 1
//
//			})
//			 
//			
//		}
		
		
		orgTreeManageRepo.orgTreeManage = tmp;
		Top.Dom.selectById("orgTreeTableView").setProperties({'items':orgTreeManageRepo.orgTreeManage});
		return tmp;
	},

	
	onOpenOrgMenu: function(event, widget){
		console.log('open menu');
		Top.Dom.selectById('orgTreeManageMenu').style({'z-index':'1000'});
		Top.Dom.selectById('orgTreeManageMenu').open(event);
		event.stopPropagation();		
	},

	onNameEditDone: function(event, widget){
		var idx = Top.Dom.selectById("orgTreeTableView").getSelectedIndex();
		
		Top.Dom.selectById('org_name_'+idx).setProperties({'visible':'visible'});
		Top.Dom.selectById('org_menu_button_'+idx).setProperties({'visible':'visible'});
		Top.Dom.selectById('org_name_edit_'+idx).setProperties({'visible':'none'});
		Top.Dom.selectById('org_name_check_button_'+idx).setProperties({'visible':'none'});
		
		
		var company_code = orgTreeManageRepo.orgTreeManage[idx].company_code;
		var department_code = orgTreeManageRepo.orgTreeManage[idx].department_code; 
		var org_type = "ADM0021";
		var org_level = orgTreeManageRepo.orgTreeManage[idx].org_level;
		var org_name = Top.Dom.selectById('org_name_edit_'+idx).getProperties('text');
		
		//Repo update
//		orgTreeManageRepo.orgTreeManage[idx].org_name = org_name;
//		orgTreeManageRepo.setValue('orgTreeManage', orgTreeManageRepo.orgTreeManage);
		
		
		let data={
				dto: {
					orgList: [
						{
							COMPANY_CODE: company_code,
							DEPARTMENT_CODE: department_code,
							ORG_TYPE: org_type,
							ORG_NAME: org_name,
							ORG_LEVEL: org_level,
							ORG_URL: _mailDomain
						}						
					]					
				}
		};
		
		
		Top.Ajax.execute({
			type : "PUT",
			url : _workspace.url+"Admin/Org",
			dataType : "json",
			async : false,
			cache : false,
			data : JSON.stringify(data),
			contentType: "application/json; charset=utf-8",
		    xhrFields: {withCredentials: true},
			crossDomain : true,
			success : function(ret, xhr, status) {
	            try {
	            	console.log('name edit success');
	            	OrgManager.List.update(data.dto.orgList[0]);
	            	
	            	createOrgTree(null, "ADM0021"); //Repo 정리
	            	orgTreeManageRepo.orgTreeManage[idx].org_name = org_name;
	            	Top.Dom.selectById('org_name_'+idx).setText(org_name); 
//	            	orgTreeManageRepo.setValue('orgTreeManage', orgTreeManageRepo.orgTreeManage);
	            	//setValue 해주면 오류뜸. 왜뜰까? 일단 setText로 수정된 이름 표기해주고, 다음번에 render되도록했음.
//	            	Top.Dom.selectById("orgTreeTableView").update();
//	            	Top.Dom.selectById("orgTreeTableView").render();
	            	
	            } catch (error) {
	               console.log('err');
	               console.log(error);
	            }
			},
			complete : function(ret, xhr, status) {
				
			},
			error : function(ret, xhr, status) {
				
			}
		});
		
		
		
		
		

		
		
	},
	
	changePublicState: function(event, widget){
			
		let idx = Top.Dom.selectById('orgTreeTableView').getClickedData();
		let company_code = idx.company_code;
		let department_code = idx.department_code; 
		let org_type = "ADM0021";
		let is_public = widget.id.split('_')[1];
		let org_level = idx.org_level;
		
		if (is_public === "yes" ){
			is_public = "ADM0011"; 
		} else {
			is_public = "ADM0012";			
		}
		
		let data={
				dto: {
					orgList: [
						{
							COMPANY_CODE: company_code,
							DEPARTMENT_CODE: department_code,
							ORG_TYPE: org_type,
							IS_PUBLIC: is_public,
							ORG_LEVEL: org_level,
							ORG_URL: _mailDomain
						}						
					]					
				}
		};
		
		Top.Ajax.execute({
			type : "PUT",
			url : _workspace.url+"Admin/Org",
			dataType : "json",
			async : false,
			cache : false,
			data : JSON.stringify(data),
			contentType: "application/json; charset=utf-8",
		    xhrFields: {withCredentials: true},
			crossDomain : true,
			success : function(ret, xhr, status) {
	            	console.log('is_public edit success');
	            	OrgManager.List.update(data.dto.orgList[0]);
	            	
	            	createOrgTree(null, "ADM0021"); //Repo 정리
	            	idx.is_public = is_public;	 
	            	
	            	if(idx.is_public === "ADM0011"){
	            		idx.checked1 = true;
	            		idx.checked2 = false;
	            	}else{
	            		idx.checked1 = false;
	            		idx.checked2 = true;
	            	}
	            	
	            	Top.Dom.selectById('orgTreeTableView').update()
	        
			},
			complete : function(ret, xhr, status) {
				
			},
			error : function(ret, xhr, status) {
				
			}
		});
		
		
		
	},
	
	openLoadOrgTreeDialog : function(event, widget) {
		orgTreeLoadRepo.reset();
		orgTreeLoadRepo.setValue('orgTreeLoad',orgTreeLoadRepo.orgTreeLoad);
		Top.Dom.selectById('adminLoadOrgTreeDialog').open();		
	}, dragStart : function(event, widget) {
		this.dragObj = event.target.dataObj;
		console.log('dragstart');
	}, tmp123 : function(event, widget) {
		this.dropObj = event.target.dataObj;
	}
	
	
	
	
	
	
	
});

function setOrgTreeManageRepo(repo){ //orgTreeRepo.orgTree[0].children
	var company;
	var department;
	
	for(var i=0; i<repo.length; i++){
		var admin_name;
		var is_public;
		var user_count;
		var text;
		var children;
		
		company = repo[i].id.split('_')[0];
		department = repo[i].id.split('_')[1];
		
		admin_name = orgTreeRepo.orgInfoList.filter(function(element){return ( (element.DEPARTMENT_CODE == "1") && (element.COMPANY_CODE) == "티맥스OSW") })[0].ADMIN_NAME;
		is_public = orgTreeRepo.orgInfoList.filter(function(element){return ( (element.DEPARTMENT_CODE == "1") && (element.COMPANY_CODE) == "티맥스OSW") })[0].IS_PUBLIC;
		user_count = orgTreeRepo.orgInfoList.filter(function(element){return ( (element.DEPARTMENT_CODE == "1") && (element.COMPANY_CODE) == "티맥스OSW") })[0].USER_COUNT;
		text = repo[i].text;
		
		
	}
	
	return;
};
Top.Controller.create('TeeSpaceLogic', {
	openAddSuborgMenu : function(event, widget) {
		Top.Controller.get('adminOrgTreeManageLayoutLogic').isedit = false;
		Top.Dom.selectById('adminAddSuborgDialog').open();
	},
	
	openEditOrgNameMenu : function(event, widget) {
		Top.Controller.get('adminOrgTreeManageLayoutLogic').isedit = true;
		Top.Dom.selectById('adminAddSuborgDialog').open();
		
	},
	
	openDeleteOrgMenu : function(event, widget) {
		
		Top.Dom.selectById('adminDeleteSubOrgDialog').open();
		
	}, 
	
	openShowOrgDetailMenu : function(event, widget) {
		repo_idx = Top.Dom.selectById("orgTreeTableView").getSelectedIndex();
//		repo_idx = clicked_idx + Top.Dom.selectById("orgTreeTableView").getStartRowNum();
		Top.Dom.selectById('adminOrgDetailDialog').open();
		
		setTimeout(function(){
			
			Top.Dom.selectById('TextView449_5').setProperties( {'text': orgTreeManageRepo.orgTreeManage[repo_idx].org_name} )//조직명
			Top.Dom.selectById('TextView449_6').setProperties( {'text': orgTreeManageRepo.orgTreeManage[repo_idx].admin_name} )//조직장
			Top.Dom.selectById('TextView449_7').setProperties( {'text': orgTreeManageRepo.orgTreeManage[repo_idx].user_count} )//멤버 수
			Top.Dom.selectById('TextView449_8').setProperties( {'text': orgTreeManageRepo.orgTreeManage[repo_idx].is_public === "ADM0011" ? '공개' : '비공개'} )//조직 공개
			Top.Dom.selectById('TextView449_9').setProperties( {'text': orgTreeRepo.orgInfoList.filter(function(e){
				return (e.COMPANY_CODE === orgTreeManageRepo.orgTreeManage[repo_idx].company_code) && (e.DEPARTMENT_CODE === orgTreeManageRepo.orgTreeManage[repo_idx].department_code);
			})[0].ORG_COMMENT} )//설명
		},100);
		
	
		
	}
	
	
	
});
Top.Controller.create('adminAddSuborgDialogLayoutLogic', {
	init : function(event, widget) {
		Top.Dom.selectById('subOrgAdminNameButton').setProperties({'on-click':'subOrgAdminName_change_btn'});
		if(Top.Controller.get('adminOrgTreeManageLayoutLogic').isedit){
			$('#top-dialog-root_adminAddSuborgDialog .top-dialog-title').text("조직 정보 수정");
			Top.Dom.selectById('Button519').setProperties({'on-click' : 'editorginfo'});
			Top.Dom.selectById('subOrgNameField').setText(Top.Dom.selectById('orgTreeTableView').getClickedData().org_name);
			Top.Dom.selectById('subOrgAdminNameView').setText(Top.Dom.selectById('orgTreeTableView').getClickedData().admin_name);
			Top.Dom.selectById('subOrgDepartmentCodeField').setText(Top.Dom.selectById('orgTreeTableView').getClickedData().department_code);
//			Top.Dom.selectById('subOrgDepartmentCodeField').setProperties({"readonly":"true" , "background-color" :"rgb(241, 241, 241)"});
			Top.Dom.selectById('subOrgCommentField').setText("");
		}
		
	},
	editorginfo : function(event, widget){
		var inputdata ={
			"dto": {
			        "orgList": [
			            {
			                "COMPANY_CODE":Top.Dom.selectById('orgTreeTableView').getClickedData().company_code,
			                "DEPARTMENT_CODE":Top.Dom.selectById('orgTreeTableView').getClickedData().department_code,
			                "ORG_TYPE": "ADM0021",
			                "ADMIN_NAME":$('#subOrgAdminNameView').attr('uid')==undefined ? null : $('#subOrgAdminNameView').attr('uid'),
			                "ORG_NAME" : Top.Dom.selectById('subOrgNameField').getText(),
			                "ORG_COMMENT" : Top.Dom.selectById('subOrgCommentField').getText(),
			                "TRANSFER_DEPARTMENT_CODE": Top.Dom.selectById('subOrgDepartmentCodeField').getText()
			            }
			        ]
			    }
			};
		Top.Ajax.execute({
			type : "PUT",
			url : _workspace.url+"Admin/Org",
			dataType : "json",
			async : false,
			cache : false,
			data : JSON.stringify(inputdata),
			contentType: "application/json; charset=utf-8",
		    xhrFields: {withCredentials: true},
			crossDomain : true,
			success : function(ret, xhr, status) {
				OrgManager.List.update(inputdata.dto.orgList[0]);
				
				var repo_idx = Top.Dom.selectById("orgTreeTableView").getSelectedIndex();
				
				Top.Dom.selectById('orgTreeTableView').getClickedData().admin_name = Top.Dom.selectById('subOrgAdminNameView').getText();
				Top.Dom.selectById('orgTreeTableView').getClickedData().org_name = Top.Dom.selectById('subOrgNameField').getText(); 
				orgTreeManageRepo.orgTreeManage[repo_idx].department_code = Top.Dom.selectById('subOrgDepartmentCodeField').getText();
				Top.Dom.selectById('orgTreeTableView').update();
				Top.Dom.selectById('adminAddSuborgDialog').close();
			},
		    error : function(ret,xhr,status){
		    	
		    }});
		
	},
	subOrgAdminName_change_btn : function(event,widget){
		Top.Dom.selectById("adminAssignmentDialog").open()
		
	},
	
	
	createSubOrg : function(event, widget) {
		var repo_idx = Top.Dom.selectById("orgTreeTableView").getSelectedIndex();
		
		var org_name = Top.Dom.selectById('subOrgNameField').getText();
		var admin_name = Top.Dom.selectById('subOrgAdminNameView').getText();
		var admin_uuid = $('#subOrgAdminNameView').attr('uid');
		var department_code = Top.Dom.selectById('subOrgDepartmentCodeField').getText();
		var company_code = orgTreeManageRepo.orgTreeManage[repo_idx].company_code;
		var parent_org_department_code = orgTreeManageRepo.orgTreeManage[repo_idx].department_code;
		var is_public = Top.Dom.selectById('subOrgIsPublicTrueButton').isChecked() ? "ADM0011" : "ADM0012";
//		var is_public = "ADM0011";
		var org_comment = Top.Dom.selectById('subOrgCommentField').getText();
		var org_type = "ADM0021";
		
		let data={
				dto: {
					orgList: [
						{
							COMPANY_CODE: company_code,
							DEPARTMENT_CODE: department_code,
							ORG_NAME: org_name,
							ADMIN_NAME: admin_uuid,
							PARENT_ORG_DEPARTMENT_CODE: parent_org_department_code,
							ORG_TYPE: org_type,
							IS_PUBLIC: is_public,
							ORG_COMMENT: org_comment,
							ORG_URL : _mailDomain
							
						}						
					]					
				}
		};
		
		_this = this;
		Top.Ajax.execute({
			type : "POST",
			url : _workspace.url+"Admin/Org",
			dataType : "json",
			async : false,
			cache : false,
			data : JSON.stringify(data),
			contentType: "application/json; charset=utf-8",
		    xhrFields: {withCredentials: true},
			crossDomain : true,
			success : function(ret, xhr, status) {
	            try {
	            	console.log('subOrg create success');
	            	var added_subOrg_repo_idx;
	            	added_subOrg_obj = {};
	            	added_subOrg_obj.admin_name = admin_name;
	            	added_subOrg_obj.company_code = company_code;
	            	added_subOrg_obj.department_code = department_code;
	            	added_subOrg_obj.icon_status = icon_status = "icon-arrow_right";
	            	added_subOrg_obj.is_public = is_public;
	            	added_subOrg_obj.org_name = org_name;
	            	added_subOrg_obj.user_count = 1;
	            	added_subOrg_obj.LEVEL = orgTreeManageRepo.orgTreeManage[repo_idx].LEVEL + 1;
	            		
	            	orgTreeManageRepo.orgTreeManage.splice(repo_idx+1, 0, added_subOrg_obj);
	            	orgTreeManageRepo.orgTreeManage[repo_idx+1].is_pressed = true;
	            	Top.Controller.get('adminOrgTreeManageLayoutLogic').setOrgTreeTableView(event,widget);
	            	
	            	OrgManager.reload(_mailDomain, "ADM0021")
	            	.then(function() {
	            		createOrgTree("orgTreeTable", "ADM0021", function () {
	    	            	var tmp=[];
	    		    		var nodes = orgTreeRepo.orgTree[0].children; 
	    		    		for(var i=0; i < nodes.length; i++){
	    		    			Top.Controller.get('adminOrgTreeManageLayoutLogic').recursiveTreetmp(tmp,nodes[i],1);
	    		    		}
	    		    		for(var i = 0 ; i < orgTreeManageRepo.orgTreeManage.length; i++)
	    		    		{
	    		    			orgTreeManageRepo.orgTreeManage[i].user_count = tmp[i].user_count;
	    		    		}
	    					Top.Dom.selectById('orgTreeTableView').update();
	    	            });
	            	});

	            	
	            
	            	
	            	
	            } catch (error) {
	               
	               console.log('err');
	            }
			},
			complete : function(ret, xhr, status) {
				
			},
			error : function(ret, xhr, status) {
				alert('중복되는 조직 코드입니다.');
			}
		});
		
		
		Top.Dom.selectById('adminAddSuborgDialog').close();
	},
	
	cancelCreateSubOrg : function(event, widget) {
		Top.Dom.selectById('adminAddSuborgDialog').close();
	}
});
Top.Controller.create('adminDeleteSubOrgDialogLayoutLogic', {
deleteSubOrgConfirm : function(event, widget) {
		
		var repo_idx = Top.Dom.selectById("orgTreeTableView").getSelectedIndex();
		
		var company_code = orgTreeManageRepo.orgTreeManage[repo_idx].company_code;
		var department_code = orgTreeManageRepo.orgTreeManage[repo_idx].department_code;
		var org_type = "ADM0021";
		
		let data={
				dto: {
					orgList: [
						{
							COMPANY_CODE: company_code,
							DEPARTMENT_CODE: department_code,
							ORG_TYPE: org_type,		
							ORG_URL: _mailDomain
						}						
					]					
				}
		};
		
		_this = this;
		Top.Ajax.execute({
			type : "DELETE",
			url : _workspace.url+"Admin/Org",
			dataType : "json",
			async : false,
			cache : false,
			data : JSON.stringify(data),
			contentType: "application/json; charset=utf-8",
		    xhrFields: {withCredentials: true},
			crossDomain : true,
			success : function(ret, xhr, status) {
	            try {
	            	console.log('subOrg delete success');
	            	OrgManager.List.remove(data.dto.orgList[0]);

	            	var tmplevel = orgTreeManageRepo.orgTreeManage[repo_idx].LEVEL;
	            	Top.Dom.selectById('orgTreeTableView').removeRows(repo_idx);
	            	while(true){
	            		if(orgTreeManageRepo.orgTreeManage[repo_idx]==undefined) break;
	            		if(tmplevel < orgTreeManageRepo.orgTreeManage[repo_idx].LEVEL){
	            			Top.Dom.selectById('orgTreeTableView').removeRows(repo_idx);
	            		}
	            		else break;
	            	}
	            	
	            } catch (error) {
	               console.log(error);
	            }
			},
			complete : function(ret, xhr, status) {
				
			},
			error : function(ret, xhr, status) {
				
			}
		});
		
		Top.Dom.selectById('adminDeleteSubOrgDialog').close();
	},
	
	deleteSubOrgCancel : function(event, widget) {
		Top.Dom.selectById('adminDeleteSubOrgDialog').close();
	}
	
});
Top.Controller.create('adminLoadOrgTreeDialogLayoutLogic', {
	orgImportFormatDonwload : function(event, widget) {
		getFormatFile('OrgImportFormat.xlsx');		
	},
	init : function(event, widget){
		orgTreeLoadRepo.orgTreeLoad = [];
		Top.Dom.selectById('LoadOrgTableView').update();
	},
	orgImportFormatUpload : function(event, widget) {
		var format_;		
		var tableRow = [];
		
		selectFormatFile(function(format, name, content) {
			
			var sheetJson = XLSX.utils.sheet_to_json(format.Sheets['조직도']);
			var noChildrenElement = [];
			
			for(var row=0; row<sheetJson.length; row++){
				
				var element = sheetJson[row];
				if (element['상위 조직 부서코드'] == null){
					continue;
				}
				
				var childElement = sheetJson.filter(function(_element){
					return ( (_element['상위 조직 부서코드'] == element['조직 부서코드']) && (_element['조직 회사코드'] == element['조직 회사코드']) );
				})
				
				if (childElement.length == 0){
					noChildrenElement.push(element);
				}				
			}		
			
			for(var row=0; row<noChildrenElement.length; row++){
				var element = noChildrenElement[row];
				var org_level = parseInt(noChildrenElement[row]['조직 레벨']);
				
				var tmp = {};
				for(level=0; level<org_level+1; level++){
					tmp['depth_'+ (org_level + 1 - level)] = element['조직 이름'];
					
					if(element['상위 조직 부서코드'] == null){
						break;
					}
					
					var UpperElement = sheetJson.filter(function(_element){
						return ( (element['조직 회사코드'] == _element['조직 회사코드']) && (element['상위 조직 부서코드'] == _element['조직 부서코드']) );
					})
					element = UpperElement[0];
				}
				
				orgTreeLoadRepo.orgTreeLoad.push(tmp);				
			}
			
			orgTreeLoadRepo.setValue('orgTreeLoad',orgTreeLoadRepo.orgTreeLoad);
			
			Top.Dom.selectById('Button1034').setText(name);
			
			org_fileName_ = name;
			org_fileContents_ = content;
			
			
		}); 
		
		
		
		
	},
	
	orgChartImportCancel : function(event, widget) {
		
		Top.Dom.selectById('adminLoadOrgTreeDialog').close();
		
	},
	
	orgChartImportConfirm : function(event, widget) {
		
		let data={
				files: [
					{
						filename: org_fileName_,
						contents: org_fileContents_,
						ORG_URL: _mailDomain
					}						
				]					
		};
		
		Top.Ajax.execute({
			type : "POST",
			url : _workspace.url+"Admin/OrgChartImport?action=",
			dataType : "json",
			async : false,
			cache : false,
			data : JSON.stringify(data),
			contentType: "application/json; charset=utf-8",
		    xhrFields: {withCredentials: true},
			crossDomain : true,
			headers: {
				"ProObjectWebFileTransfer":"true"					
			},
			success : function(ret, xhr, status) {
	            try {
	            	console.log('org chart import success');
	            	if(Top.Dom.selectById('OUMMainLayout') == null)  Top.Controller.get('adminOrgManageLayoutLogic').load_org_chart();
	            	else Top.Controller.get('adminOrgManageLayoutLogic').load_org_user();
	            	
	            } catch (error) {
	               console.log('err');
	            }
			},
			complete : function(ret, xhr, status) {
				
			},
			error : function(ret, xhr, status) {
				
			}
		});
	
		Top.Dom.selectById('adminLoadOrgTreeDialog').close();
		
	}
	
	
	
});
Top.Controller.create('adminOrgDetailLayoutLogic', {
	confirmOrgDetail : function(event, widget) {
		Top.Dom.selectById('adminOrgDetailDialog').close();
	}
});









Top.Controller.create('adminUserInfoViewLogic', {
	init : function(event, widget) {
	
		this.user_id; // user id
	
	},
	
	init_contents : function(user_id){
		
		this.user_id = user_id;
		
		
		var inputDTO = {
				"dto": {
					"USER_ID": user_id
				}
		};
		
		
		Top.Ajax.execute({
			url : _workspace.url + "Admin/OrgUser?action=",
			type : 'GET',
			dataType : "json",
			async : false,
			cache : false,
			data : JSON.stringify(inputDTO), // arg
			contentType : "application/json",
			crossDomain : true,
			xhrFields : {
			    withCredentials: true
			},
			headers: {
				"ProObjectWebFileTransfer":"true"
			},
			success : function(data) {
				
				var outputdata;
				data = JSON.parse(data);
				if(data.dto == undefined) {
				    outputdata = {
				        result : "Success",
				        message : data.exception.name + ' ' + data.exception.message,
				        data : data
				    }
				} else {
				    outputdata = data.dto;
				    outputdata.files = data.files;
				}
				
				console.log(outputdata);
				
				//userinfo
				var user_info = outputdata.userInfo;
				
				if(user_info != null) {
					Top.Dom.selectById('UserInfoView_UserName').setText(user_info.USER_NAME);
					Top.Dom.selectById('UserInfoView_UserIDContent').setText(user_info.USER_LOGIN_ID);
					Top.Dom.selectById('UserInfoView_UserEmailContent').setText(user_info.USER_EMAIL);
					Top.Dom.selectById('UserInfoView_UserCellPhoneNumContent').setText(user_info.USER_PHONE);
					Top.Dom.selectById('UserInfoView_UserBirthdayContent').setText(user_info.USER_BIRTH);
				
		        
					  Top.Dom.selectById('ImageButton_Admin_Profile').setSrc(userManager.getUserPhoto(Top.Controller.get('adminUserInfoViewLogic').user_id,'medium',user_info.THUMB_PHOTO));
		          	 
				} else {
					user_info = orgUserRepo.orgUserList[Top.Dom.selectById("OrgUserTableView").getClickedIndex()[0]];
					
					Top.Dom.selectById('UserInfoView_UserName').setText(user_info.user_name);
					if(user_info.email.includes("@"))
						Top.Dom.selectById('UserInfoView_UserIDContent').setText(user_info.email.split("@")[0]);
					Top.Dom.selectById('UserInfoView_UserEmailContent').setText(user_info.email);
				}
	          	 
	          	 Top.Dom.selectById("OrgUserTableView").update();
	          	 
	          	 
	          	 
				//companyinfo
	          	var company_info =[];
	          	
	          	if(outputdata.companyProfileList != null) {
					for(let i = 0; i<outputdata.companyProfileList.length;i++){
						company_info[i] = outputdata.companyProfileList[i]
						 if(outputdata.companyProfileList[i].ORG_PATH.split(' > ')[outputdata.companyProfileList[i].ORG_PATH.split(' > ').length -1] === orgUserRepo.orgUserList[Top.Dom.selectById("OrgUserTableView").getClickedIndex()[0]].org_name ){
							Top.Dom.selectById('UserInfoView_UserCompanyID').setText(company_info[i].USER_EMPLOYEE_NUM);
							Top.Dom.selectById('UserInfoView_UserTeamName').setText(company_info[i].ORG_PATH.split(' > ')[outputdata.companyProfileList[i].ORG_PATH.split(' > ').length -1]);
							Top.Dom.selectById('UserInfoView_UserEmploymentType').setText(adminCodeToText(company_info[i].USER_EMPLOYMENT_TYPE));
							Top.Dom.selectById('UserInfoView_UserCompanyPhoneNumContent').setText(company_info[i].USER_COM_NUM);
							Top.Dom.selectById('UserInfoView_UserInChargeOfContent').setText(company_info[i].USER_TASK);
							Top.Dom.selectById('UserInfoView_UserOfficeLocationContent').setText(company_info[i].USER_LOCATION);
							Top.Dom.selectById('UserInfoView_UserAdmittanceDateContent').setText(company_info[i].USER_EMPLOYMENT_DATE); 
						 }
					
					
					
						//부서 소속은 따로
						let html_1 =	'<top-textview id="UserInfoView_UserOccupationGeneral'+i+'" layout-width="auto" layout-height="auto" border-width="0px" text="'+ company_info[i].USER_JOB +' / '+ company_info[i].USER_POSITION +' " text-decoration="" title="" layout-tab-disabled="false" class="linear-child-vertical" style="text-align: left" actual-height="15"></top-textview>';
						let html_2 = '<top-textview id="UserInfoView_UserOccupationPath'+i+'" layout-width="auto" layout-height="auto" border-width="0px" text="'+company_info[i].ORG_PATH+'" text-decoration="" title="" layout-tab-disabled="false" class="linear-child-vertical" style="margin-bottom: 1%; text-align: left margin-bottom: 1%;" actual-height="15"></top-textview>';
						Top.Dom.selectById("UserInfoView_UserOccupationContentWrapper").append(html_1+html_2);
					}
	          	} else {
	          		Top.Dom.selectById('UserInfoView_UserTeamName').setText(orgUserRepo.orgUserList[Top.Dom.selectById("OrgUserTableView").getClickedIndex()[0]].org_name);
	          	}
				//view창 끄기
				if(Top.Dom.selectById('UserInfoView_UserTeamName').getText() === ""){
					Top.Dom.selectById('adminUserInfoViewDialog').close()
					Top.Controller.get('adminOrgUserLayoutLogic').clickNode(MouseEvent,Top.Dom.selectById('TreeView1305'))
					
		           return;
				}
				
				orgUserRepo.selectedUser = outputdata;
				
				//소속회사
				$.ajax({
					url : _workspace.url + "Admin/CompanyInfo", 
		            type: 'GET',
		            dataType: 'json',
		            crossDomain: true,
		            contentType: 'application/json; charset=utf-8',
		            xhrFields: {
		                withCredentials: true
		              },
		            data: JSON.stringify({
		              "dto": {
		                "ADMIN_ID": userManager.getLoginUserId()
		              }
		            }),
		            success: function(result) {
//		            	Top.Dom.selectById('UserInfoView_UserCompanyContent').setText(result.dto.COMPANY_NAME)
			        },
			        error: function(error) {        	
			        	
			        }
					});	
				//근무처 담당업무 화면 동적으로
				if($('#UserInfoView_UserInChargeOfWrapper').height() < 50){
					Top.Dom.selectById("UserInfoView_UserInChargeOfWrapper").setProperties({"layout-height":"50px"})
					Top.Dom.selectById('UserInfoView_UpperContainer').setProperties({"layout-height":"80%"})
				}else{
					Top.Dom.selectById("UserInfoView_UserInChargeOfWrapper").setProperties({"layout-height":"auto"})
					Top.Dom.selectById('UserInfoView_UpperContainer').setProperties({"layout-height":"96%"})
				}
				if($('#UserInfoView_UserOfficeLocationWrapper').height() < 50){
					Top.Dom.selectById("UserInfoView_UserOfficeLocationWrapper").setProperties({"layout-height":"50px"})
					Top.Dom.selectById('UserInfoView_UpperContainer').setProperties({"layout-height":"80%"})
				}else{
					Top.Dom.selectById("UserInfoView_UserOfficeLocationWrapper").setProperties({"layout-height":"auto"})
					Top.Dom.selectById('UserInfoView_UpperContainer').setProperties({"layout-height":"96%"})
				}
			

			
			},
			error : function(data) {
				var outputdata;
	            outputdata = {
	                result : "Fail",
	                message : data.exception != undefined ? data.exception.message : "Error",
	                data : data
	            }		
	            console.info("fail");
	            console.info(outputdata);
			}
		});
		
	
		
		
	}, 
	
	click_cancel : function(event, widget) {
		
		var dialog = Top.Dom.selectById('adminUserInfoViewDialog');
		dialog.close(true);
		
	}, 
	
	click_modify : function(event, widget) {
		
// 		var main = Top.Dom.selectById('adminUserInfoMain');
// 		var ctrl = Top.Controller.get('adminUserInfoModifyLogic');
 		let user_id = this.user_id;
//		
//		Top.Dom.selectById('adminUserInfoMain').onSrcLoad(function(){
//			Top.Controller.get('adminUserInfoAddLogic').init_contents(user_id); 
//		});
// 		main.src('adminUserInfoModify.html' + verCsp());
		
// 		adminUserInfoAdd
        Top.Dom.selectById("adminUserInfoMain").src('adminUserInfoAdd.html'  + verCsp())

//        var flag_edit = 1;
	}
	
	
});


Top.Controller.create('adminGuestTreeViewDialogLayoutLogic', {
	
	init : function(event, widget) {
		orgUserRepo.orgUserList = [];
		//트리뷰적용
		OrgManager.get(_mailDomain, 'ADM0021')
		.then(function(orgList) {
            	console.log('a',orgTreeRepo.orgTree);
            	Top.Dom.selectById("adminUserTreeView").setProperties({"nodes":orgTreeRepo.orgTree})
        		Top.Dom.selectById("adminUserTreeView").expandNode('all');
        		//drag&drop
        		//--------------------------------------	    		
		});
	
		//이벤트 선언
		if(Top.Controller.get('adminAddOneGuestLayoutLogic').bb == 0)
			Top.Dom.selectById("Button_Treeview_ok").setProperties({"on-click":"selectUserOk"})
		else
			Top.Dom.selectById("Button_Treeview_ok").setProperties({"on-click":"changeUserOk"})
			
		Top.Dom.selectById('Button_Treeview_cancel').setProperties({'on-click':function(){Top.Dom.selectById('adminGuestTreeviewDialog').close()}});
		
	},
	
	changeUserOk: function(event, widget){
		// dd 가져오는거
		var dd = Top.Controller.get('adminAddOneGuestLayoutLogic').cc.split('_')[3];
		$.ajax({
			url : _workspace.url + "Admin/OrgPathGet?action=", 
            type: 'POST',
            dataType: 'json',
            crossDomain: true,
            contentType: 'application/json; charset=utf-8',
            xhrFields: {
                withCredentials: true
              },
            data: JSON.stringify({
              "dto": {
//                "COMPANY_CODE": adminGetTeamObjByName(Top.Dom.selectById("adminUserTreeView").getSelectedNode().text).COMPANY_CODE,
//                "DEPARTMENT_CODE": adminGetTeamObjByName(Top.Dom.selectById("adminUserTreeView").getSelectedNode().text).DEPARTMENT_CODE,
            	  "COMPANY_CODE": Top.Dom.selectById("adminUserTreeView").getSelectedNode().id.split('_')[0],
	                "DEPARTMENT_CODE": Top.Dom.selectById("adminUserTreeView").getSelectedNode().id.split('_')[1],
                "ORG_URL": adminGetTeamObjByName(Top.Dom.selectById("adminUserTreeView").getSelectedNode().text).ORG_URL,
                "ORG_TYPE": adminGetTeamObjByName(Top.Dom.selectById("adminUserTreeView").getSelectedNode().text).ORG_TYPE
              }
            }), 
            success: function(result) {
            	// 게스트에서 쓰는 id로 바꿔야
            	if(dd == null) Top.Dom.selectById('TextView_affiliation_path_01').setText(result.dto.ORG_NAME);
    		 	else{
    		 		if($('#top-dialog-root_adminAddOneGuestDialog .top-dialog-title').text() === '게스트 정보'){
                        ///////////////////////////////////////////////
	            	       
	            	


    		 			Top.Dom.selectById('TextView_affiliation_path_Modify_'+dd).setText(result.dto.ORG_NAME)

    		 			  var abc = '#TextView_affiliation_path_Modify_'+dd;
	                        	$(abc).attr('CODE', Top.Dom.selectById("adminUserTreeView").getSelectedNode().id);
    		 		}else if($('#top-dialog-root_adminAddOneGuestDialog .top-dialog-title').text() === '게스트 개별 추가'){ 



    		 		        Top.Dom.selectById('TextView_affiliation_path_'+ dd).setText(result.dto.ORG_NAME);

                                 var ab = '#TextView_affiliation_path_'+dd;
	                        	$(ab).attr('CODE', Top.Dom.selectById("adminUserTreeView").getSelectedNode().id);

    		 		        
                     
    		 		}
 
    		 	} 

    	

            	Top.Dom.selectById("adminGuestTreeviewDialog").close()	
	        },
	        error: function(error) {        	
	        	
	        }
			});	
            
	},
	
	
	//확인 이벤트
	selectUserOk : function(event, widget) {
	

		$.ajax({
			url : _workspace.url + "Admin/OrgPathGet?action=", 
            type: 'POST',
            dataType: 'json',
            crossDomain: true,
            contentType: 'application/json; charset=utf-8',
            xhrFields: {
                withCredentials: true
              },
            data: JSON.stringify({
              "dto": {
                "COMPANY_CODE": Top.Dom.selectById("adminUserTreeView").getSelectedNode().id.split('_')[0],
                "DEPARTMENT_CODE": Top.Dom.selectById("adminUserTreeView").getSelectedNode().id.split('_')[1],
                "ORG_URL": _mailDomain,
                "ORG_TYPE": "ADM0022"
              }
            }),
            success: function(result) {
            	var countLayout = Top.Controller.get('adminAddOneGuestLayoutLogic').aa;
                var pathVal = new Array()
                for(let i =0; i<result.dto.ORG_NAME.split(" > ").length ; i++){
                	 pathVal[i] = result.dto.ORG_NAME.split(" > ")[i]
                }
            	//확인 눌렀을 시, 소속 입력되서  html 추가
            	let html_1 = '<top-linearlayout id="LinearLayoutPath_0'+countLayout+'" layout-width="100%" layout-height="50px" layout-horizontal-alignment="left" border-width="0px" orientation="horizontal" position="false" title="" layout-tab-disabled="false" vertical-alignment="top" class="linear-child-vertical" style="text-align: left;" actual-height="30">'+ '<div id="LinearLayoutPath_0'+countLayout+'" class="top-linearlayout-root" style="white-space: nowrap; height: 30px; border-width: 0px; border-style: solid; vertical-align: top; overflow: auto hidden; width: 100%; line-height: 30px;">'
				+'<top-textview id="TextView_affiliation_0'+countLayout+'" layout-width="auto" layout-height="auto" layout-vertical-alignment="middle" border-width="0px" text="소속 '+countLayout+'" text-decoration="" title="" layout-tab-disabled="false" class="linear-child-horizontal" style="line-height: normal;" actual-width="38.1094">'
				+'<span id="TextView_affiliation_0'+countLayout+'" class="top-textview-root" style="width: auto; height: auto; border-width: 0px; border-style: solid; vertical-align: middle;"><a class="top-textview-url">소속 '+countLayout+'</a><div class="top-textview-aligner middle"></div></span>'
			+'</top-textview>'
			+'<top-button layout-vertical-alignment="CENTER" id="TextView_affiliation_path_0'+countLayout+'" on-click = "changeBtn" background-color="rgba(255,255,255,1.0)" layout-width="300px" layout-height="auto" layout-horizontal-alignment="center" margin="0px 0px 0px 10px" padding="0px 0px 1px 0px" border-color="rgba(194,194,194,1.0) rgba(194,194,194,1.0) rgba(194,194,194,1.0) rgba(194,194,194,1.0)" border-width="1px 1px 1px 1px"'
			+'tab-index="0" text="'+result.dto.ORG_NAME+'" text-color="rgba(0,0,0,1.0)" text-decoration="" icon-position="left" title="" layout-tab-disabled="false" class="linear-child-vertical" style="text-align: center;" actual-height="20">'
			+'<button id="TextView_affiliation_path_0'+countLayout+'" class="top-button-root" type="button" style="width: 400px; height: auto; background-color: rgb(255, 255, 255); margin: 0px 0px 0px 10px; padding: 0px 0px 1px; color: rgb(0, 0, 0); border-width: 1px; border-style: solid; border-color: rgb(194, 194, 194); text-align: center;" tabindex="0"><label class="top-button-text">"'+result.dto.ORG_NAME+'"</label></button>'
			+'</top-button>'
			+`<top-checkbox id="orgkingg`+countLayout+`" layout-width="auto" layout-height="auto" layout-vertical-alignment="middle" tab-index="0" text-decoration="" check-position="left" title="" layout-tab-disabled="false" class="linear-child-horizontal" style="line-height: normal;" actual-width="NaN">
			<input class="top-checkbox-check" type="checkbox" size="width:300px;" tabindex="0" style="display: inline-block;">
			<label class="top-checkbox-text left" for="checkbox" style="width: auto; height: auto; vertical-align: middle; padding-right: 0px; padding-top: 7px;"><i class="top-checkbox-icon"></i><span></span></label>
			</top-checkbox>`+
			+'</div></top-linearlayout>'
			;
            	            

            		
            	///////////////////////////////////////////////
            	Top.Dom.selectById("UserInfoAdd_LowerContainer").append(html_1) 
        		Top.Dom.selectById("UserInfoAdd_LowerContainer").complete();
                var ab = '#TextView_affiliation_path_0'+countLayout;
            	$(ab).attr('CODE', Top.Dom.selectById("adminUserTreeView").getSelectedNode().id);
            	
            	Top.Controller.get('adminAddOneGuestLayoutLogic').aa = Top.Controller.get('adminAddOneGuestLayoutLogic').aa + 1;
            	
            	Top.Dom.selectById("adminGuestTreeviewDialog").close()	
            	
	        },
	        error: function(error) {        	
	        	
	        }
			});	
	}
});





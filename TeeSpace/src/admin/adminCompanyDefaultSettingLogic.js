
Top.Controller.create('adminCompanyDefaultSettingLogic', {
	
	init : function(event, widget) {
		//기본 정보 설정에서 요소들 안보이게
		//textfield 안보이게
		Top.Dom.selectById("TextField_org_name").setProperties({"visible":"none"})
		Top.Dom.selectById("TextField_org_cap").setProperties({"visible":"none"})
		Top.Dom.selectById("TextField_org_mail").setProperties({"visible":"none"})
		Top.Dom.selectById("TextField_org_url").setProperties({"visible":"none"})
		Top.Dom.selectById("TextField_org_address").setProperties({"visible":"none"})
		Top.Dom.selectById("TextField_org_phone").setProperties({"visible":"none"})
		//저장하기 버튼 안보이게
		Top.Dom.selectById("Button_org_name").setProperties({"visible":"none"})
		Top.Dom.selectById("Button_org_cap").setProperties({"visible":"none"})
		Top.Dom.selectById("Button_org_mail").setProperties({"visible":"none"})
		Top.Dom.selectById("Button_org_url").setProperties({"visible":"none"})
		Top.Dom.selectById("Button_org_address").setProperties({"visible":"none"})
		Top.Dom.selectById("Button_org_phone").setProperties({"visible":"none"})
		//error icon 안보이게
		Top.Dom.selectById("Icon_admin_mail_error").setProperties({"visible":"none"})
		Top.Dom.selectById("Icon_admin_org_phone_error").setProperties({"visible":"none"})
		
		
		//클릭 버튼 이벤트 선언
		Top.Dom.selectById('Button_org_name_change').setProperties({'on-click':'org_name_change_btn'});
		Top.Dom.selectById('Button_org_cap_change').setProperties({'on-click':'org_cap_change_btn'});//권한양도
		Top.Dom.selectById('Button_org_mail_change').setProperties({'on-click':'org_mail_change_btn'});
		Top.Dom.selectById('Button_org_url_change').setProperties({'on-click':'org_url_change_btn'});
		Top.Dom.selectById('Button_org_address_change').setProperties({'on-click':'org_address_change_btn'});
		Top.Dom.selectById('Button_org_phone_change').setProperties({'on-click':'org_phone_change_btn'});
		
		//저장하기 버튼 이벤트 선언
		Top.Dom.selectById('Button_org_name').setProperties({'on-click':'org_name_btn'});
		Top.Dom.selectById('Button_org_cap').setProperties({'on-click':'org_cap_btn'});
		Top.Dom.selectById('Button_org_mail').setProperties({'on-click':'org_mail_btn'});
		Top.Dom.selectById('Button_org_url').setProperties({'on-click':'org_url_btn'});
		Top.Dom.selectById('Button_org_address').setProperties({'on-click':'org_address_btn'});
		Top.Dom.selectById('Button_org_phone').setProperties({'on-click':'org_phone_btn'});
		
		
		Top.Dom.selectById('Button_org_url_change').setProperties({'disabled':true})
		
		
		var URL_CHECK;
		
		Top.Ajax.execute({
			url : _workspace.url + "Admin/CompanyInfoGet?action=", 
            type: 'POST',
            dataType: 'json',
            crossDomain: true,
            contentType: 'application/json; charset=utf-8',
            xhrFields: {
                withCredentials: true
              },
            data: JSON.stringify({
              "dto": {
                "ADMIN_ID": userManager.getLoginUserId()
              }
            }),
            success: function(result) {
            	if(result.dto === undefined) {
//            		createCompanyInfo();
            	} else {            		
	            	Top.Dom.selectById("TextView_show_name").setText(result.dto.COMPANY_NAME)
	            	Top.Dom.selectById("TextView_show_cap").setText(result.dto.CEO_ID)
	            	Top.Dom.selectById("TextView_show_mail").setText(result.dto.COMPANY_EMAIL)
	            	Top.Dom.selectById("TextView_show_url").setText(result.dto.COMPANY_DOMAIN)
	            	Top.Dom.selectById("TextView_show_address").setText(result.dto.COMPANY_ADDRESS)
	            	Top.Dom.selectById("TextView_show_phone").setText(result.dto.COMPANY_NUM)
	            	URL_CHECK = result.dto.DOMAIN_UPDATE_CNT
	            	if(URL_CHECK === "1"){
	            		Top.Dom.selectById("Button_org_url_change").setProperties({"disabled":"true"})
	            		Top.Dom.selectById("Button_org_url_change").setProperties({"background-color":"#9A9A9A"})
	
	            	}
            	}
	        },
	        error: function(error) {        	
	        	
	        }
		});	

		admin.setLnbColor();
	},
	
	//조직명
	org_name_change_btn : function(event, widget) {
	
		Top.Dom.selectById("TextView_show_name").setProperties({"visible":"none"})
		Top.Dom.selectById("Button_org_name").setProperties({"visible":"visible"})
		Top.Dom.selectById("TextField_org_name").setProperties({"visible":"visible"})
		Top.Dom.selectById("Button_org_name_change").setProperties({"visible":"none"})
		Top.Dom.selectById("TextField_org_name").setText(Top.Dom.selectById("TextView_show_name").getText())
	},
	
	//조직명
	org_cap_change_btn : function(event, widget) {
	
		Top.Dom.selectById("TextView_show_cap").setProperties({"visible":"visible"})
		Top.Dom.selectById("Button_org_cap").setProperties({"visible":"visible"})
		Top.Dom.selectById("TextField_org_cap").setProperties({"visible":"visible"})
		Top.Dom.selectById("Button_org_cap_change").setProperties({"visible":"none"})
		
		Top.Dom.selectById("adminAssignmentDialog").open()
	},
	org_mail_change_btn : function(event, widget) {

		Top.Dom.selectById("TextView_show_mail").setProperties({"visible":"none"})
		Top.Dom.selectById("Button_org_mail").setProperties({"visible":"visible"})
		Top.Dom.selectById("TextField_org_mail").setProperties({"visible":"visible"})
		Top.Dom.selectById("Button_org_mail_change").setProperties({"visible":"none"})
		Top.Dom.selectById("TextField_org_mail").setText(Top.Dom.selectById("TextView_show_mail").getText())
	},
	org_url_change_btn : function(event, widget) {
		
		Top.Dom.selectById("TextView_show_url").setProperties({"visible":"none"})
		Top.Dom.selectById("Button_org_url").setProperties({"visible":"visible"})
		Top.Dom.selectById("TextField_org_url").setProperties({"visible":"visible"})
		Top.Dom.selectById("Button_org_url_change").setProperties({"visible":"none"})
		Top.Dom.selectById("TextField_org_url").setText(Top.Dom.selectById("TextView_show_url").getText())
	},	
	org_address_change_btn : function(event, widget) {
		
		Top.Dom.selectById("TextView_show_address").setProperties({"visible":"none"})
		Top.Dom.selectById("Button_org_address").setProperties({"visible":"visible"})
		Top.Dom.selectById("TextField_org_address").setProperties({"visible":"visible"})
		Top.Dom.selectById("Button_org_address_change").setProperties({"visible":"none"})
		Top.Dom.selectById("TextField_org_address").setText(Top.Dom.selectById("TextView_show_address").getText())
	},	
	org_phone_change_btn : function(event, widget) {
	
		Top.Dom.selectById("TextView_show_phone").setProperties({"visible":"none"})
		Top.Dom.selectById("Button_org_phone").setProperties({"visible":"visible"})
		Top.Dom.selectById("TextField_org_phone").setProperties({"visible":"visible"})
		Top.Dom.selectById("Button_org_phone_change").setProperties({"visible":"none"})
		Top.Dom.selectById("TextField_org_phone").setText(Top.Dom.selectById("TextView_show_phone").getText())
	},	
		
	org_name_btn : function(event, widget){
		
		$.ajax({
			url : _workspace.url + "Admin/CompanyInfo", 
            type: 'PUT',
            dataType: 'json',
            crossDomain: true,
            contentType: 'application/json; charset=utf-8',
            xhrFields: {
                withCredentials: true
              },
            data: JSON.stringify({
              "dto": {
                "ADMIN_ID": userManager.getLoginUserId(),
                "COMPANY_NAME" : Top.Dom.selectById("TextField_org_name").getText()
              }
            }),
            success: function(result) {
            if(result.dto.RESULT_CD  === "RST0001"){
            	Top.Dom.selectById("TextView_show_name").setText(Top.Dom.selectById("TextField_org_name").getText())
            	Top.Dom.selectById("TextField_org_name").setProperties({"visible":"none"})
            	Top.Dom.selectById("TextView_show_name").setProperties({"visible":"visible"})
            	Top.Dom.selectById("Button_org_name").setProperties({"visible":"none"})
            	Top.Dom.selectById("Button_org_name_change").setProperties({"visible":"visible"})
            }
            	
	        },
	        error: function(error) {        	
	        	
	        }
			});	
	},
	
	org_cap_btn : function(event, widget){
		
		$.ajax({
			url : _workspace.url + "Admin/CompanyInfo", 
            type: 'PUT',
            dataType: 'json',
            crossDomain: true,
            contentType: 'application/json; charset=utf-8',
            xhrFields: {
                withCredentials: true
              },
            data: JSON.stringify({
              "dto": {
                "ADMIN_ID": userManager.getLoginUserId(),
                "CEO_ID" : selected_id
              } 
            }),
            success: function(result) {
            if(result.dto.RESULT_CD  === "RST0001"){
            	Top.Dom.selectById("TextView_show_cap").setText(Top.Dom.selectById("TextField_org_cap").getText())
            	Top.Dom.selectById("TextField_org_cap").setProperties({"visible":"none"})
            	Top.Dom.selectById("TextView_show_cap").setProperties({"visible":"visible"})
            	Top.Dom.selectById("Button_org_cap").setProperties({"visible":"none"})
            	Top.Dom.selectById("Button_org_cap_change").setProperties({"visible":"visible"})
            }
            	
	        },
	        error: function(error) {        	
	        	
	        }
			});	
	},
		
	org_mail_btn : function(event, widget){
		let mail = Top.Dom.selectById("TextField_org_mail").getText();
		$.ajax({
	        url: _workspace.url+"Users/EmailDuplication", //Service Object
	        type: 'GET',
	        dataType: 'json',
	        crossDomain: true,
	        contentType : "application/json",
	        xhrFields: {
	
	            withCredentials: true
	
	          },
	          data: JSON.stringify({
		          "dto": {
		            "USER_EMAIL" : mail
		          }
		        }),
	        success: function(result) {
	        	if(result.dto.USER_ID == "해당 이메일은 존재합니다."){
	        		
	        		Top.Dom.selectById("TextField_org_mail").setProperties({"border-color":"red"})
	        		Top.Dom.selectById("Icon_admin_mail_error").setProperties({"visible":"visible", "tooltip-top":"이미 존재하는 이메일 주소입니다. 다른 이메일 주소로 확인해 주세요."})
	        		return false;
	        	}else if(result.dto.USER_ID == "해당 이메일은 등록되지 않았습니다."){
	        		Top.Dom.selectById("TextField_org_mail").setProperties({"border-color":"#ACB0BC"})
	        		Top.Dom.selectById("Icon_admin_mail_error").setProperties({"visible":"none"})
	        		
	        		
	        		$.ajax({
	        			url : _workspace.url + "Admin/CompanyInfo", 
	                    type: 'PUT',
	                    dataType: 'json',
	                    crossDomain: true,
	                    contentType: 'application/json; charset=utf-8',
	                    xhrFields: {
	                        withCredentials: true
	                      },
	                    data: JSON.stringify({
	                      "dto": {
	                        "ADMIN_ID": userManager.getLoginUserId(),
	                        "COMPANY_EMAIL" : Top.Dom.selectById("TextField_org_mail").getText()
	                      }
	                    }),
	                    success: function(result) {
	                    if(result.dto.RESULT_CD  === "RST0001"){
	                    	if(Top.Dom.selectById("TextField_org_mail").getText() === ""){
	                    		Top.Dom.selectById("TextView_show_mail").setText("—")
	                    	}else{
	                    		Top.Dom.selectById("TextView_show_mail").setText(Top.Dom.selectById("TextField_org_mail").getText())
	                    	}
	                    	
	                    	Top.Dom.selectById("TextField_org_mail").setProperties({"visible":"none"})
	                    	Top.Dom.selectById("TextView_show_mail").setProperties({"visible":"visible"})
	                    	Top.Dom.selectById("Button_org_mail").setProperties({"visible":"none"})
	                    	Top.Dom.selectById("Button_org_mail_change").setProperties({"visible":"visible"})
	                    }
	                    	
	        	        },
	        	        error: function(error) {        	
	        	        	
	        	        }
	        			});	

	        	}	        	
	        },
	        error: function(error) {
	        }
      });
		
		
		//$('#TextView_show_url .top-textview-root').css({"font-weight":"bold"})
		$('#TextView_show_url .top-textview-url').css({"font-weight":"bold"})

	
	},
	org_url_btn : function(event, widget){
		
		$.ajax({
			url : _workspace.url + "Admin/CompanyInfo", 
            type: 'PUT',
            dataType: 'json',
            crossDomain: true,
            contentType: 'application/json; charset=utf-8',
            xhrFields: {
                withCredentials: true
              },
            data: JSON.stringify({
              "dto": {
                "ADMIN_ID": userManager.getLoginUserId(),
                "COMPANY_DOMAIN" : Top.Dom.selectById("TextField_org_url").getText(),
                "DOMAIN_UPDATE_CNT": URL_CHECK 
              }
            }),
            success: function(result) {
            if(result.dto.RESULT_CD  === "RST0001"){
            	if(Top.Dom.selectById("TextField_org_url").getText() === ""){
            		Top.Dom.selectById("TextView_show_url").setText("—")
            	}else{
            		Top.Dom.selectById("TextView_show_url").setText(Top.Dom.selectById("TextField_org_url").getText())
            	}
            	
            	Top.Dom.selectById("TextField_org_url").setProperties({"visible":"none"})
            	Top.Dom.selectById("TextView_show_url").setProperties({"visible":"visible"})
            	Top.Dom.selectById("Button_org_url").setProperties({"visible":"none"})
            	Top.Dom.selectById("Button_org_url_change").setProperties({"visible":"visible"})
            }
            
            if(result.dto.RESULT_CD === "RST0002"){
            	Top.Dom.selectById("TextField_org_url").setProperties({"visible":"none"})
            	Top.Dom.selectById("TextView_show_url").setProperties({"visible":"visible"})
            	Top.Dom.selectById("Button_org_url").setProperties({"visible":"none"})
            	Top.Dom.selectById("Button_org_url_change").setProperties({"visible":"visible"})
            	Top.Dom.selectById("Button_org_url_change").setProperties({"disabled":"true"})
            	
            }
            
	        },
	        error: function(error) {        	
	        	
	        }
			});	
	},
	org_address_btn : function(event, widget){
		
		$.ajax({
			url : _workspace.url + "Admin/CompanyInfo", 
            type: 'PUT',
            dataType: 'json',
            crossDomain: true,
            contentType: 'application/json; charset=utf-8',
            xhrFields: {
                withCredentials: true
              },
            data: JSON.stringify({
              "dto": {
                "ADMIN_ID": userManager.getLoginUserId(),
                "COMPANY_ADDRESS" : Top.Dom.selectById("TextField_org_address").getText()
              }
            }),
            success: function(result) {
            if(result.dto.RESULT_CD  === "RST0001"){
            	if(Top.Dom.selectById("TextField_org_address").getText() === ""){
            		Top.Dom.selectById("TextView_show_address").setText("—")
            	}else{
            		Top.Dom.selectById("TextView_show_address").setText(Top.Dom.selectById("TextField_org_address").getText())
            	}
         
            	Top.Dom.selectById("TextField_org_address").setProperties({"visible":"none"})
            	Top.Dom.selectById("TextView_show_address").setProperties({"visible":"visible"})
            	Top.Dom.selectById("Button_org_address").setProperties({"visible":"none"})
            	Top.Dom.selectById("Button_org_address_change").setProperties({"visible":"visible"})
            }
            	
	        },
	        error: function(error) {        	
	        	
	        }
			});	
	},
	org_phone_btn : function(event, widget){
		
		
		 if(!pattern_num.test(Number(Top.Dom.selectById("TextField_org_phone").getText()))){
			 Top.Dom.selectById("Icon_admin_org_phone_error").setProperties({"visible":"visible"})
			 Top.Dom.selectById("TextField_org_phone").setProperties({"border-color":"#FF0000"})
			 return;
		 } 	
		 Top.Dom.selectById("Icon_admin_org_phone_error").setProperties({"visible":"none"})
		
		$.ajax({
			url : _workspace.url + "Admin/CompanyInfo", 
            type: 'PUT',
            dataType: 'json',
            crossDomain: true,
            contentType: 'application/json; charset=utf-8',
            xhrFields: {
                withCredentials: true
              },
            data: JSON.stringify({
              "dto": {
                "ADMIN_ID": userManager.getLoginUserId(),
                "COMPANY_NUM" : Top.Dom.selectById("TextField_org_phone").getText()
              }
            }),
            success: function(result) {
            if(result.dto.RESULT_CD  === "RST0001"){
            	if(Top.Dom.selectById("TextField_org_phone").getText() === ""){
            		Top.Dom.selectById("TextView_show_phone").setText("—")
            	}else{
            		Top.Dom.selectById("TextView_show_phone").setText(Top.Dom.selectById("TextField_org_phone").getText())
            	}
            
            	Top.Dom.selectById("TextField_org_phone").setProperties({"visible":"none"})
            	Top.Dom.selectById("TextView_show_phone").setProperties({"visible":"visible"})
            	Top.Dom.selectById("Button_org_phone").setProperties({"visible":"none"})
            	Top.Dom.selectById("Button_org_phone_change").setProperties({"visible":"visible"})
            }
            	
	        },
	        error: function(error) {        	
	        	
	        }
			});	
	}
	

});

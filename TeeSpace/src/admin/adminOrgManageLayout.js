
Top.Controller.create('adminOrgManageLayoutLogic', {
	init : function(event, widget){
		
		Top.Dom.selectById('adminOrgManageChartTab').setProperties({'backgroundColor':'#C5C5C8', 'textColor':'#000000'});
		Top.Dom.selectById('adminOrgManageUsersTab').setProperties({'backgroundColor':'#00A4C3', 'textColor':'#FFFFFF'});
		this.load_org_user();
		
	},
	
	user_tab_click : function(event, widget) {
		Top.Dom.selectById('adminOrgManageChartTab').setProperties({'backgroundColor':'#C5C5C8', 'textColor':'#000000'});
		widget.setProperties({'backgroundColor':'#00A4C3', 'textColor':'#FFFFFF'});

		this.load_org_user();
	}, 
	
	chart_tab_click : function(event, widget) {
		Top.Dom.selectById('adminOrgManageUsersTab').setProperties({'backgroundColor':'#C5C5C8', 'textColor':'#000000'});
		widget.setProperties({'backgroundColor':'#00A4C3', 'textColor':'#FFFFFF'});
		this.load_org_chart();
	},
	
	
	load_org_user : function(){
//		Top.Dom.selectById('adminOrgManageChartTab').setProperties({"textStyle":""});
//		Top.Dom.selectById('adminOrgManageUsersTab').setProperties({"textStyle":"bold"});
		Top.Dom.selectById('adminOrgManageMainContent').src('adminOrgUserLayout.html' + verCsp());
	},
	
	load_org_chart : function(){
		if(orgTreeRepo.orgInfoList.length == 0 || orgTreeRepo.orgInfoList == null)
			return; 
		
//		Top.Dom.selectById('adminOrgManageUsersTab').setProperties({"textStyle":""});
//		Top.Dom.selectById('adminOrgManageChartTab').setProperties({"textStyle":"bold"});
		Top.Dom.selectById('adminOrgManageMainContent').src('adminOrgTreeManageLayout.html' + verCsp());
	}
	
});

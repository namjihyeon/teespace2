
Top.Controller.create('AdminReleaseDialogLayoutLogic', {
	init : function(event, widget){
		this.data = Top.Dom.selectById('OrgUserTableView').getClickedData();
		Top.Dom.selectById('text_name').setText(this.data.user_name);
		Top.Dom.selectById('text_org').setText(this.data.org_name+'에서 ');
		Top.Dom.selectById('text_position').setText(this.data.job+'을(를) ');
	},
	onRelease : function(event, widget) {
		let outputdata;
		var user_id = this.data.USER_ID
		var company_obj = adminGetTeamObjByAffCode(this.data.affcode);
		
		var inputDTO = {
				dto:{
					"orgUserList":[{
						"USER_ID":user_id,
						"COMPANY_CODE":company_obj.COMPANY_CODE,
						"DEPARTMENT_CODE":company_obj.DEPARTMENT_CODE,
						"ORG_URL":company_obj.ORG_URL,
						"ORG_TYPE":company_obj.ORG_TYPE
					}]
				}
		};
		
		Top.Ajax.execute({
		url : _workspace.url + "Admin/OrgUser",
		type : 'DELETE',
		dataType : "json",
		async : false,
		cache : false,
		data : JSON.stringify(inputDTO), // arg
		contentType : "application/json",
		crossDomain : true,
		xhrFields : {
		    withCredentials: true
		},
		headers: {
			"ProObjectWebFileTransfer":"true"
		},
		success : function(data) {
			data = JSON.parse(data);
			if(data.dto.RESULT_CD === "RST0001") {
				Top.Dom.selectById('adminReleaseDialog').close();
//				let ctrl = Top.Controller.get('adminOrgUserLayoutLogic').clickNode('', Top.Dom.selectById('TreeView1305'));
				var table = Top.Dom.selectById('OrgUserTableView');
				orgUserRepo.orgUserList.splice(table.getClickedIndex()[0], 1)
				table.update();
				//simulate node click for current one to update table after delete
				return;
			}
			else if(data.dto.RESULT_CD  === "RST0001"){
				 outputdata = {
					        result : "Success",
					        message : data.exception.name + ' ' + data.exception.message,
					        data : data
				 }
				 console.log(outputdata);
			}
			else {
				console.info("unknown code : " + data.dto.RESULT_CD)
			}
            
		},
		error : function(data) {
            outputdata = {
                result : "Fail",
                message : data.exception != undefined ? data.exception.message : "Error",
                data : data
            }		
            console.info(outputdata);
		}
	});	
	}, onCancel : function(event, widget) {

		Top.Dom.selectById('adminReleaseDialog').close();
	}
	
});

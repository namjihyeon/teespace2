let excel_col_mapping = {"이름":"name", "조직명":"group", "사번":"company_idNum", "직군":"job", "직책":"position", "회사전화":"phoneNum", "이동전화":"mobileNum", "이메일":"mail"};

function importExcel_ajax(inputDTO){

	let outputdata = null;
	Top.Ajax.execute({
		url : _workspace.url + "Admin/ExcelImportV2?action=",
		type : 'POST',
		dataType : "json",
		async : false,
		cache : false,
		data : JSON.stringify(inputDTO), // arg
		contentType : "application/json",
		crossDomain : true,
		xhrFields : {
		    withCredentials: true
		},
		headers: {
			"ProObjectWebFileTransfer":"true"
		},
		success : function(data) {
			data = JSON.parse(data);
			if(data.dto == undefined) {
			    outputdata = {
			        result : "Success",
			        message : data.exception.name + ' ' + data.exception.message,
			        data : data
			    }
			} else {
			    outputdata = data.dto;
			    outputdata.files = data.files;
			}
			console.info("success");
            console.info(outputdata);
			alert("조직도 추가를 성공했습니다.");
    		workspaceManager.update();
		},
		error : function(data) {
            outputdata = {
                result : "Fail",
                message : data.exception != undefined ? data.exception.message : "Error",
                data : data
            }		
            console.info("fail");
            console.info(outputdata);
		}
	});
	return outputdata;
}

function adminTableUpdate(functionNum) {
	Top.Dom.selectById("UserTableView").update();
	var pagination = Top.Dom.selectById("UserTableView").getElement(".auto-pagination").item(0);
	var emptyLayout = Top.Dom.selectById("EmptyLayout");
	var deleteAllButton = Top.Dom.selectById("DeleteAllButton");
	var saveButton = Top.Dom.selectById("SaveButton");
	var messageTv = Top.Dom.selectById("MessageTextView");
	
	if(adminRepo.userList.length == 0) {
		pagination.hidden = true;
		emptyLayout.setProperties({"display":"visible"});
		if(functionNum == 0)
			messageTv.setText("추가된 정보가 없습니다.\n [조직도 추가] 버튼을 클릭하여, 조직도를 추가하세요.");
		else if(functionNum == 1)
			messageTv.setText("검색 결과가 없습니다.");
		deleteAllButton.setDisabled("true");
		saveButton.setDisabled("true");
	} else {
		pagination.hidden = false;
		emptyLayout.setProperties({"display":"none"});
		deleteAllButton.setDisabled("false");
		saveButton.setDisabled("false");
	}
}

Top.Controller.create('adminUserGroupManageLayoutLogic', {
	init:function(widget){
		Top.Dom.selectById("UserTableView").onRender(function() {
			Top.Dom.selectById("table-length-menu").select("20");
			adminTableUpdate(0);
			//
		})		
    },
    removeExcel : function(event, widget) {
		Top.Dom.selectById('adminExcelRemoveDialog').open();
	}, searchUser : function(event, widget) {
		var searchSelectBox = Top.Dom.selectById("SearchSelectBox");
		var searchTextField = Top.Dom.selectById("SearchTextField");
		
		var searchCol = searchSelectBox.getSelectedText();
		searchCol = excel_col_mapping[searchCol];
		var key = searchTextField.getText();
	
		adminBackupRepo.userList_backup = adminRepo.userList;
		var result = new Array();

		for(var i = 0; i < adminRepo.userList.length; i++) {
			if(adminRepo.userList[i][searchCol] == key) {
				var temp = {};
				temp = adminRepo.userList[i];
				result.push(temp);
			}
		}
		
		adminRepo.userList = result;
		adminTableUpdate(1);

	}, searchEnter : function(event, widget) {
		if(event.code === "Enter" && event.key === "Enter"){
			this.searchUser();
		}
	}, removeSearchResult : function(event, widget) {
		var searchTextField = Top.Dom.selectById("SearchTextField");
		searchTextField.setText("");
		
		adminRepo.userList = adminBackupRepo.userList_backup;
		adminTableUpdate(0);
	}, importExcel : function(event, widget) {
		importExcel_ajax(import_inputDTO); 
	}, resetPassword : function(event, widget) {
		Top.Dom.selectById('adminContentsLayout').src("adminPwResetLayout.html" + verCsp());
//		Top.Dom.selectById('Empty')
	}, addExcel : function(event, widget) {
		Top.Dom.selectById('adminExcelAddDialog').open();
	}
	
	
});






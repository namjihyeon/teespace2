
Top.Controller.create('adminCompanyEmpTypeSettingLayoutLogic', {
	init : function(){
		Top.Ajax.execute({
			url : _workspace.url + "Admin/CompanyProfileMetaList",
			type : 'GET',
			dataType : "json",
			async : false,
			cache : false,
			contentType : "application/json",
			crossDomain : true,
			xhrFields : {
			    withCredentials: true
			},
			headers: {
				"ProObjectWebFileTransfer":"true"
			},
			success : function(data) {
				var tmparr = JSON.parse(data).dto.comProfileMetaList;
				var insertarr = []; 
				var posarr = [];
				if(tmparr != null){
				for(var i = 0 ; i < tmparr.length; i++){
					if(tmparr[i].META_TYPE ==='ADM0003') {
						insertarr.push({
							'job' : tmparr[i].META_VALUE,
							'order' : tmparr[i].META_ORDER,
							'type' : tmparr[i].META_TYPE,
							'm_vi' : "visible",
							'd_vi' : 'visible',
							'a_vi' : "none",
							"u_vi" : "visible",
							"b_vi" : "visibie",
							"ro" : "true"
						})
					}
				}}
				insertarr.push({
					'job':"",
					'order':"",
					'type':'ADM0003',
					'm_vi' : "none",
					'd_vi' : 'none',
					'a_vi' : "visible",
					"u_vi" : "none",
					"b_vi" : "none",
					"ro" : "false"
				})
				adminjobpos.job = [];
				adminjobpos.job = insertarr;
				Top.Dom.selectById('tableemp').update();
				Top.Dom.selectById('tableemp').render();
				
			},
			error : function(data) {
	           
			}
		});
	},
	
	
	
	
	
	
	
	click_modify : function(event, widget) {
		Top.Dom.selectById('jobf_1_'+Top.Dom.selectById('tableemp').getClickedIndex()[0]+'_1').setProperties({"readonly":"false"})
		Top.Dom.selectById('Button309_1_'+Top.Dom.selectById('tableemp').getClickedIndex()[0]+'_2').setText("저장");
		Top.Dom.selectById('Button309_1_'+Top.Dom.selectById('tableemp').getClickedIndex()[0]+'_2').setProperties({"on-click":"click_save"});
	}, 
	click_save : function(){
		var idx = Top.Dom.selectById('tableemp').getClickedIndex()[0];
		var tmptext = Top.Dom.selectById('jobf_1_'+Top.Dom.selectById('tableemp').getClickedIndex()[0]+'_1').getText();
		adminjobpos.job[idx].job = tmptext;
		var inputdata = {
				"dto":{
					"META_TYPE": "ADM0003",
			        "META_ORDER": idx+1,
			        "META_VALUE" : tmptext,
			        "ACTION": "NAME"
				}
		}
		Top.Ajax.execute({
			url : _workspace.url + "Admin/CompanyProfileMeta",
			type : 'PUT',
			dataType : "json",
			async : false,
			cache : false,
			contentType : "application/json",
			data : JSON.stringify(inputdata),
			crossDomain : true,
			xhrFields : {
			    withCredentials: true
			},
			headers: {
				"ProObjectWebFileTransfer":"true"
			},
			success : function(data) {
				
				Top.Dom.selectById('jobf_1_'+Top.Dom.selectById('tableemp').getClickedIndex()[0]+'_1').setProperties({"readonly":"true"})
				Top.Dom.selectById('Button309_1_'+Top.Dom.selectById('tableemp').getClickedIndex()[0]+'_2').setText("수정");
				Top.Dom.selectById('Button309_1_'+Top.Dom.selectById('tableemp').getClickedIndex()[0]+'_2').setProperties({"on-click":"click_modify"});
			},
			error : function(data) {
	           
			}
		});
	},
	click_delete : function(event, widget) {
		var idx = Top.Dom.selectById('tableemp').getClickedIndex()[0];
		var tmpdata = Top.Dom.selectById('tableemp').getClickedData();
		var inputdata = {
				"dto" : {
					"META_TYPE" : tmpdata.type,
					"META_ORDER" : tmpdata.order
				}
		}
		Top.Ajax.execute({
			url : _workspace.url + "Admin/CompanyProfileMeta",
			type : "DELETE",
			dataType : "json",
			async : false,
			cache : false,
			contentType : "application/json",
			data : JSON.stringify(inputdata),
			crossDomain : true,
			xhrFields : {
				withCredentials : true
			},
			headers: {
				"ProObjectWebFileTransfer":"true"
			},
			success : function(data){
				var flag = 1;
				adminjobpos.job.splice(idx,1);
				for(var i = idx; i < adminjobpos.job.length; i++){
					if(adminjobpos.job[i].order == "") continue;
					adminjobpos.job[i].order--; 
				}
				Top.Dom.selectById('tableemp').update();
				Top.Dom.selectById('tableemp').render();
			}
		})
	}, click_down : function(event, widget) {
		var idx = Top.Dom.selectById('tableemp').getClickedIndex()[0];
		if((idx==adminjobpos.job.length-2))return;
		var inputdata = {
				"dto":{
					"META_TYPE": "ADM0003",
			        "META_ORDER": idx+1,
			        "ACTION": "DOWN"
				}
		}
		Top.Ajax.execute({
			url : _workspace.url + "Admin/CompanyProfileMeta",
			type : 'PUT',
			dataType : "json",
			async : false,
			cache : false,
			contentType : "application/json",
			data : JSON.stringify(inputdata),
			crossDomain : true,
			xhrFields : {
			    withCredentials: true
			},
			headers: {
				"ProObjectWebFileTransfer":"true"
			},
			success : function(data) {
				
					var a = adminjobpos.job[idx+1];
					adminjobpos.job[idx+1] = adminjobpos.job[idx];
					adminjobpos.job[idx+1].order = idx+2;
					adminjobpos.job[idx] = a;
					adminjobpos.job[idx].order = idx+1;
					Top.Dom.selectById('tableemp').update();
					Top.Dom.selectById('tableemp').render();
			},
			error : function(data) {
	           
			}
		});
		
	}, click_up : function(event, widget) {
		var idx = Top.Dom.selectById('tableemp').getClickedIndex()[0];
		if(adminjobpos.job[idx-1]==undefined)return;
		var inputdata = {
				"dto":{
					"META_TYPE": "ADM0003",
			        "META_ORDER": idx+1,
			        "ACTION": "UP"
				}
		}
		Top.Ajax.execute({
			url : _workspace.url + "Admin/CompanyProfileMeta",
			type : 'PUT',
			dataType : "json",
			async : false,
			cache : false,
			contentType : "application/json",
			data : JSON.stringify(inputdata),
			crossDomain : true,
			xhrFields : {
			    withCredentials: true
			},
			headers: {
				"ProObjectWebFileTransfer":"true"
			},
			success : function(data) {
				
				var a = adminjobpos.job[idx-1];
				adminjobpos.job[idx-1] = adminjobpos.job[idx];
				adminjobpos.job[idx-1].order = idx;
				adminjobpos.job[idx] = a;
				adminjobpos.job[idx].order = idx+1;
				Top.Dom.selectById('tableemp').update();
				Top.Dom.selectById('tableemp').render();
			},
			error : function(data) {
	           
			}
		});
		
	}, addnode : function(event, widget){
		var idx = adminjobpos.job.length-1;
		var tmptext = Top.Dom.selectById('jobf_1_'+Top.Dom.selectById('tableemp').getClickedIndex()[0]+'_1').getText()
		var inputdata = {
				"dto" : {
					"META_TYPE" : "ADM0003",
					"META_VALUE" : tmptext,
					"META_ORDER" : idx+1
				}
		}
		Top.Ajax.execute({
			url : _workspace.url + "Admin/CompanyProfileMeta",
			type : 'POST',
			dataType : "json",
			async : false,
			cache : false,
			contentType : "application/json",
			data : JSON.stringify(inputdata),
			crossDomain : true,
			xhrFields : {
			    withCredentials: true
			},
			headers: {
				"ProObjectWebFileTransfer":"true"
			},
			success : function(data) {
				adminjobpos.job[idx] = ({
					'job' : tmptext,
					'order' : idx+1,
					'type' : 'ADM0003',
					'm_vi' : "visible",
					'd_vi' : 'visible',
					'a_vi' : "none",
					"u_vi" : "visible",
					"b_vi" : "visibie",
					"ro" : "true"
				})
				adminjobpos.job.push({
					'job':"",
					'order':"",
					'type':'ADM0003',
					'm_vi' : "none",
					'd_vi' : 'none',
					'a_vi' : "visible",
					"u_vi" : "none",
					"b_vi" : "none",
					"ro" : "false"
				})
				Top.Dom.selectById('tableemp').update();
				Top.Dom.selectById('tableemp').render();
			},
			error : function(data) {
	           
			}
		});
	}
});

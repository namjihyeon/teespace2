Top.Controller.create('calendarRepeatedEventDeleteDialogLayoutLogic', {
  init: function(event, widget) {},
  onCancelClick: function(event, widget) {
    tds('calendarRepeatedEventDeleteDialog').close(true);
  },
  onApplyClick: function(event, widget) {
    let radioBtnArr = [Top.Dom.selectById('RadioButton695'), Top.Dom.selectById('RadioButton695_1'), Top.Dom.selectById('RadioButton695_2')];
    let applyOption = 0;

    for (var i = 0; i < radioBtnArr.length; ++i) {
      if (radioBtnArr[i].getChecked() == true) {
        applyOption = (i + 1) % 3;
        break;
      }
    }

    if (applyOption == 0) {
      // 전 일정 삭제
      calendarAPI.deleteEvent(calendarEventRepo.event.taskID, calendarEventRepo.event.startDate, 0, () => {
        // tds('cal-content').src('calendarMainLayout.html');
      });
    } else if (applyOption == 1) {
      // 해당 일정만 삭제
      calendarAPI.deleteEvent(calendarEventRepo.event.taskID, calendarEventRepo.event.startDate, 1, () => {
        // tds('cal-content').src('calendarMainLayout.html');
      });
    } else if (applyOption == 2) {
      // 해당 일정과 이후 일정 삭제
      calendarAPI.deleteEvent(calendarEventRepo.event.taskID, calendarEventRepo.event.startDate, 2, () => {
        // tds('cal-content').src('calendarMainLayout.html');
      });
    }

    // console.log(radioBtnArr);
    // console.log(applyOption);
    tds('calendarRepeatedEventDeleteDialog').close(true);

    // Render calendar layout
    const _this = Top.Controller.get('calendarMainLayoutLogic');
    _this._getEventList();
  }
});

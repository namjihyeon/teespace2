Top.Controller.create('calendarFileImportExportLayoutLogic', {
  init: function (event, widget) {},

  onBackClick: function (event, widget) {
    tds('cal-content').src('calendarMainLayout.html');
  },
  /*_onMultipleFileSelected(fileChoosers) {
    spt.startLoader();

    fileChoosers.forEach(fc => {
      Top.Controller.get('calendarFileImportExportLayoutLogic')._onFileSelected(fc);
    });

    spt.stopLoader(false);
  },*/
  _onFileSelected(fileChooser) {
    const _this = Top.Controller.get('calendarFileImportExportLayoutLogic');
    const targetFileName = fileChooser.path.split('\\')[2];
    let file = {};
    for (let i = 0; i < fileChooser.file.length; i++) {
      if (fileChooser.file[i].name === targetFileName) {
        file = fileChooser.file[i];
        break;
      }
    }

    const nomalizedFileFullName = Top.Util.Browser.isIE() ? file.name : file.name.normalize('NFC');
    const fileName = nomalizedFileFullName.substring(0, nomalizedFileFullName.lastIndexOf('.'));
    const fileNameSliceIndex = nomalizedFileFullName.lastIndexOf('.');
    const fileExtension = fileNameSliceIndex === -1 ? null : nomalizedFileFullName.slice(fileNameSliceIndex + 1, nomalizedFileFullName.length);

    const fileInfo = {
      id: calendarEventRepo.event.fileList.length,
      fileID: 'LOCAL',
      fileName,
      fileExtension,
      listIndex: calendarEventRepo.event.fileList.length,
      fileStatus: 'NORMAL',
      fileSize: file.size,
      fileChannel: TEESPACE_CHANNELS.TSCHEDULE.CHANNEL_TYPE,
      ///// PO 업로드콜 나오면 삭제 예정
      fileSrc: fileChooser.src,
      fileChooser: fileChooser
    };
    _this._setFileAttachItemLayout(fileInfo);
    calendarEventRepo.event.fileList.push(fileInfo);
  },
  _onTDriveFileSelected(file) {
    const fileInfo = {
      id: calendarEventRepo.event.fileList.length,
      fileID: file.file_id,
      fileName: file.file_name,
      fileExtension: file.file_extension,
      listIndex: calendarEventRepo.event.fileList.length,
      fileStatus: 'NORMAL',
      fileSize: file.file_size,
      fileChannel: TEESPACE_CHANNELS.TSCHEDULE.CHANNEL_TYPE,
      ///// PO 업로드콜 나오면 삭제 예정
      fileSrc: null, //fileChooser.src,
      fileChooser: null //fileChooser
    };
    this._setFileAttachItemLayout(fileInfo);
    calendarEventRepo.event.fileList.push(fileInfo);
  },
  _setFileAttachItemLayout({ id, fileExtension: ext, fileName: name, fileSize: size }) {
    const _this = this;
    const content = document.getElementById('cal-calfile-attach-content');
    tds('cal-calfile-attach-content').setText(`${name}.${ext}`);
    tds('cal-calfile-attach-content').setProperties({ textColor: '#000000' });
  },
  _fileValidityCheck(file) {
    // files : 추가한 파일들
    // curTotalSize : 직전까지 추가된 파일들의 총합 size (현재 추가한 파일은 제외)

    const isExistSameFile = calendarEventRepo.event.fileList.some((f) => f.fileName === file.name);
    if (isExistSameFile) {
      TeeAlarm.open({ title: '이미 추가한 파일입니다.', content: '파일 첨부를 계속하려면 기존 파일을 제거하세요.' });
      return false;
    }

    let curTotalSize = calendarEventRepo.event.fileList.reduce((prev, cur) => prev + cur.fileSize, 0);
    curTotalSize += file.size;
    if (curTotalSize >= MAX_UPLOAD_FILE_SIZE_MB * 1024 * 1024) {
      TeeAlarm.open({ title: '저장 공간이 부족하여 파일을 업로드 할 수 없습니다.', content: '파일 첨부를 계속하려면 내 저장 공간을 정리하세요.' });
      return false;
    }

    return true;
    /* 
      // 확장자 검사 로직
      const nomalizedName = Top.Util.Browser.isIE() ? file.name : file.name.normalize('NFC');
      const fileNameSliceIndex = nomalizedName.lastIndexOf('.');
      const fileExt = fileNameSliceIndex === -1 ? null : nomalizedName.slice(fileNameSliceIndex + 1, nomalizedName.length);
      if (!fileExt) {
        ALERT('확장자가 없는 파일은 업로드할 수 없습니다.');
        return false;
      } */
  },
  onCalfileAttachFromTDriveClick: function (event, widget) {
    const _this = Top.Controller.get('calendarFileImportExportLayoutLogic');
    OPEN_TDRIVE({
      buttonFn1: undefined,
      buttonFn2: undefined,
      channel_id: workspaceManager.getChannelList(workspaceManager.getWorkspaceId(getRoomIdByUrl()), TEESPACE_CHANNELS.TSCHEDULE.CHANNEL_TYPE),
      preventMultiSelect: false,
      filteringFileExtensionList: [],
      selectedFile: {},
      successCallback: function (file) {
        // 여러 개 파일을 선택하면 successCallback이 파일 수만큼 온다
        // 파일 업로드를 위한 파일 메타 수정 후 talkData.fileChooserList에 넣어준다
        console.log(file);
        file = file.storageFileInfo;
        if (_this._fileValidityCheck({ name: `${file.file_name}.${file.file_extension}`, size: file.file_size })) {
          _this._onTDriveFileSelected(file);
        }
      },
      cancelCallback: function () {
        // notiFeedback('파일 업로드에 실패하였습니다.');
      }
    });
  },
  onCalfileAttachFromLocalClick: function (event, widget) {
    const _this = Top.Controller.get('calendarFileImportExportLayoutLogic');
    Top.Device.FileChooser.create({
      onBeforeLoad(file) {
        return _this._fileValidityCheck(file);
      },
      onFileChoose: _this._onFileSelected,
      charset: 'euc-kr',
      multiple: false
    }).show();
  },
  onImportFileClick: function (event, widget) {
    const _this = Top.Controller.get('calendarFileImportExportLayoutLogic');

    const successCallback = () => {
      const fileID = calendarEventRepo.event.fileList[0].fileID;
      calendarAPI.getFileImport(fileID);
      tds('cal-content').src('calendarMainLayout.html');
      TeeToast.open({ text: '캘린더에 일정이 추가되었습니다.' });
    };
    const errorCallback = () => {
      console.error('edit error');
    };
    _this._sendFilesToStorage(calendarEventRepo.event.fileList, successCallback, errorCallback);
  },
  _sendFilesToStorage(files, completeCallback) {
    if (!files.length) {
      completeCallback(); //보낼 파일이 없는 경우.. 바로 callback 호출
      return;
    }

    const workspaceId = workspaceManager.getWorkspaceId(getRoomIdByUrl());
    const channelId = workspaceManager.getChannelList(workspaceManager.getWorkspaceId(getRoomIdByUrl()), TEESPACE_CHANNELS.TSCHEDULE.CHANNEL_TYPE);
    const userId = userManager.getLoginUserId();
    const promises = [];

    files.forEach((file) => {
      if (file.fileID === 'LOCAL') {
        // 로컬 파일 업로드
        const inDto = {
          dto: {
            workspace_id: workspaceId,
            channel_id: channelId,
            storageFileInfo: {
              user_id: userId,
              file_last_update_user_id: userId,
              file_id: '',
              file_name: file.fileName,
              file_extension: file.fileExtension,
              file_created_at: '',
              file_updated_at: '',
              file_size: file.fileSize,
              user_context_1: '',
              user_context_2: '',
              user_context_3: ''
            }
          }
        };
        promises.push(
          new Promise((resolve, reject) => {
            storageManager.UploadFile(
              file.fileChooser.file[0],
              inDto,
              'Calendar',
              'Calendar?action=TempStorages',
              (data) => {
                // success callback
                file.fileID = data.dto.storageFileInfoList[0].file_id;
                resolve();
              },
              (data) => {
                // error callback
                console.error(data);
                reject();
              }
            );
          })
        );
      } else {
        // TeeDrive 파일 업로드
        promises.push(
          new Promise((resolve, reject) => {
            storageManager.CopyFile(
              'Shallow',
              file.fileID,
              channelId,
              file.fileName,
              file.fileExtension,
              '',
              '',
              '',
              (data) => {
                // success callback
                file.fileID = data.dto.storageFileInfoList[0].file_id;
                resolve();
              },
              (data) => {
                // error callback
                console.error(data);
                reject();
              }
            );
          })
        );
      }
    });
    // NOTE: Promise를 사용하지않으면, storageManager API response 받기전에 completeCallback이 불림.
    // storageManager API response를 완전히 받은 후에 completeCallback이 불리도록 순서를 보장하기 위해 promise를 사용.
    Promise.all(promises).then(() => {
      completeCallback(); //파일 모두 업로드가 끝나면 callback 실행
    });
  },
  onExportFileClick: function (event, widget) {
    const ws_id = workspaceManager.getWorkspaceId(getRoomIdByUrl());
    const calendar_id = workspaceManager.getChannelList(workspaceManager.getWorkspaceId(getRoomIdByUrl()), TEESPACE_CHANNELS.TSCHEDULE.CHANNEL_TYPE);
    const user_id = JSON.parse(sessionStorage.getItem('userInfo')).userId;
    const a = document.createElement('a');
    document.body.appendChild(a);
    a.style = 'display: none';
    a.href = _workspace.url + 'Calendar/Calendar?action=Export' + '&calendarID=' + calendar_id + '&workspaceID=' + ws_id + '&userID=' + user_id;
    a.download = 'download';
    a.click();
    document.body.removeChild(a);
  }
});

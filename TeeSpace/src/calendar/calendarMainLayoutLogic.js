Top.Controller.create('calendarMainLayoutLogic', {
  init() {
    this.isDateClick = false;
	this.isInit = false;

    this._initialize();
    this._initializeHeader();
    this._buttonRoute();
    this._getEventList();
    this._setFilterButton();

    // this._setFilterButton(); TOP filter button
	
	// check mismatch
    const workspaceID = workspaceManager.getWorkspaceId(getRoomIdByUrl());
    if (calendarCommonData.dateRange[workspaceID] !== null) {
        if (calendarCommonData.dateRange[workspaceID] !== calendarCommonData.getTopCalendarWidget().getRange() ) {
            this._getEventList();
        }
    }
	
  },
  _initialize() {
    // header의 back button visible 처리
    //tds('cal-header-icon').setVisible('visible');
    //tds('cal-header-back').setVisible('none');

    // 앱바 숨김처리
    document.getElementById('calendarLayoutBar').style = 'display:none';
    // tds('calendarMainLayout').setDragSelection(false);
    // 일정 더보기 팝업 뜰 수 있도록 설정
    calendarCommonData.getTopCalendarWidget().setProperties({
      'on-extra-click': () => true
    });

    // TOP calendar widget에 button text 세팅
    document.getElementsByClassName('top-calendar-today-schedule-btn')[0].innerText = '새 일정';
    document.getElementsByClassName('top-calendar-filter')[0].style = 'display: none';
  },
  _initializeHeader() {
    const headerLayout = Top.Widget.create('top-linearlayout');
    headerLayout.setProperties({ id: 'calendarHeaderLayout', class: 'cal-header' });
    headerLayout.template.children[0].id = 'calendarHeader';
    document.querySelector('.top-calendar-header-right').appendChild(headerLayout.template);
    tds('calendarHeaderLayout').src('calendarIconLayout.html');
  },
  _addDndItems() {
    // 각 일정 아이템에 dnd 기능 허용
    const scheduleObjects = document.getElementsByClassName('top-calendar-schedule');
    for (scheduleObject of scheduleObjects) {
      scheduleObject.classList.add('dnd__item');
      console.debug(scheduleObject.classList);
    }
    dnd.update();
  },
  _buttonRoute() {
    $('.top-calendar-type-month').attr('id', 'top-calendar-type-month');
    $('.top-calendar-type-week').attr('id', 'top-calendar-type-week');
    $('.top-calendar-type-day').attr('id', 'top-calendar-type-day');

    const workspaceID = workspaceManager.getWorkspaceId(getRoomIdByUrl());
	if (calendarCommonData.dateRange[workspaceID]) {
        calendarCommonData.getTopCalendarWidget().changeDayCalendarType( this.calculateMidDate( calendarCommonData.dateRange[workspaceID][0],calendarCommonData.dateRange[workspaceID][1] ));
		$('#top-calendar-type-month').click();
    }
    const sessionKey = `calendarType_${workspaceID}`;
    const calendarType = sessionStorage.getItem(sessionKey);

    if (calendarType) {
    } else {
      sessionStorage.setItem(sessionKey, 'month');
    }

    $('#top-calendar-type-month').bind('click', function () {
      sessionStorage.setItem(sessionKey, 'month');
    });
    $('#top-calendar-type-week').bind('click', function () {
      sessionStorage.setItem(sessionKey, 'week');
    });
    $('#top-calendar-type-day').bind('click', function () {
      sessionStorage.setItem(sessionKey, 'day');
    });

    switch (sessionStorage.getItem(sessionKey)) {
      case 'day':
		$('#top-calendar-type-day').click();
        break;
      case 'week':
		$('#top-calendar-type-week').click();
        break;
	  case 'month':
		$('#top-calendar-type-month').click();
        break;
    }
	
	this.isInit = true;
  },
  _getEventList() {
	const _this = Top.Controller.get('calendarMainLayoutLogic');
	if (_this.isInit) {
		calendarAPI.getEventList(_this._processEventData);
	}
  },
  _processEventData(data) {
    // task
    const taskList = data.taskList.map((task) => {
      let et = task.endTime;
      if (et === '2400') {
        et = '2359';
      }
      return {
        // startDate, endDate를 조합하는 이유는 반복일정들끼리 task id가 같기 때문..
        taskID: `${task.taskID},${task.startDate},${task.endDate}`,
        title: task.title,
        color: task.color,
        taskType: task.taskType,
        allday: task.allDay,
        startDate: task.startDate,
        endDate: task.endDate,
        startTime: task.startTime,
        endTime: et,
        importance: task.importance,
        userList: task.userList,
        categoryList: task.labelList,
        ownUserID: task.OWN_USER_ID
      };
    });
    // holiday
    data.holidayInfo.dateinfo = data.holidayInfo.dateinfo ? data.holidayInfo.dateinfo : [];
    const holidayList = data.holidayInfo.dateinfo.map((h) => {
      const date = h.DATE_DAY;
      let year,
        month,
        day = null;
      if (date.length === 8) {
        year = date.substring(0, 4);
        month = date.substring(4, 6);
        day = date.substring(6, 8);
      } else {
        year = calendarCommonData.getTopCalendarWidget().getRange()[h.DATE_DAY.substr(0, 2) === '12' ? 0 : 1].substr(0, 4);
        month = date.substring(0, 2);
        day = date.substring(2, 4);
      }
      return {
        date: `${year}-${month}-${day}`,
        text: h.NAME,
        holiday: Boolean(h.IS_RED === 'Y')
      };
    });
    // NOTE: setSchedule => setHoliday 의 호출 순서를 반드시 지켜야함
    calendarCommonData.getTopCalendarWidget().setSchedules(taskList);
    calendarCommonData.holidayVisibility && /*data.holiday_yn === 'y' &&*/ calendarCommonData.getTopCalendarWidget().setHoliday(holidayList);
    const workspaceID = workspaceManager.getWorkspaceId(getRoomIdByUrl());
    const calendarType = sessionStorage.getItem(`calendarType_${workspaceID}`);
    // if (calendarType == 'month') {
    const _this = Top.Controller.get('calendarMainLayoutLogic');
    _this._addDndItems();

    // 임시코드
    let extra_number_idx = $('.top-calendar-timeline-extra-number').css('z-index');
    let dialog_idx = $('.top-calendar-extra-dialog-root').css('z-index');
    if (extra_number_idx >= dialog_idx) {
      $('.top-calendar-timeline-extra-number').css('z-index', dialog_idx - 1);
    }
  },
  _setFilterButton() {
    const calendarFilter = `
    <div id="cal-filter-file" class="cal__filter--submenu"
      style="margin-bottom: 8px;">전체 일정 파일 가져오기/내보내기</div>
    <div id="cal-filter-url" class="cal__filter--submenu"
      style="margin-bottom: 8px;">전체 일정 URL 가져오기/내보내기</div>
    <div id="cal-filter-share" class="cal__filter--submenu"
      style="margin-bottom: 8px;">TeeSpace 사용자에게 전체 일정 공유</div>
    <div id="cal-filter-select" class="cal__filter--submenu">캘린더 선택</div>
    `;

    $('.top-calendar-filter-popup').append(calendarFilter);

    const filterFile = document.getElementById('cal-filter-file');
    filterFile.addEventListener('click', () => {
      tds('cal-content').src('calendarFileImportExportLayout.html');
    });
    const filterUrl = document.getElementById('cal-filter-url');
    filterUrl.addEventListener('click', () => {
      tds('cal-content').src('calendarUrlImportExportLayout.html');
    });
    const filterShare = document.getElementById('cal-filter-share');
    filterShare.addEventListener('click', () => {
      tds('cal-content').src('calendarShareLayout.html');
    });
    const filterSelect = document.getElementById('cal-filter-select');
    filterSelect.addEventListener('click', () => {
      tds('cal-content').src('calendarViewMoreLayout.html');
    });

    const isCollapse = Top.App.current().path.includes('sub=');
    filterSelect.style = isCollapse ? 'display;' : 'display: none;';
    filterShare.style = isCollapse ? 'margin-bottom: 8px;' : 'margin-bottom: 0px;';
  },
  onAddEventClick: function (event, widget) {
    // 일정 생성 버튼 클릭
    const _this = Top.Controller.get('calendarMainLayoutLogic');
    _this.isDateClick = false;
    calendarCommonData.isEventEditMode = false;
    calendarEventRepo.setValue('event.startTime', undefined);
    tds('cal-content').src('calendarEventCreateLayout.html');

    // 새일정 클릭 시 TeeCalendar에서 문구 변경
    // TODO: 레이아웃에서 변경되면 아래 코드 삭제
    $('#calendarLayoutText .top-textview-url').text('새 일정');
	
	    
    // 월주일 유지관련
    const workspaceID = workspaceManager.getWorkspaceId(getRoomIdByUrl());
    calendarCommonData.dateRange[workspaceID] = calendarCommonData.getTopCalendarWidget().getRange();
	
	
  },
  onUpdateDatesClick: function (event, widget) {
    const _this = Top.Controller.get('calendarMainLayoutLogic');
    _this._getEventList();
  },
  onEventClick: function (event, widget, info) {
    const [taskID, startDate] = info.id.split(',');
    // let startDate = undefined;
    // for (var i = 0; i < $(`#calendar .top-calendar-date`)[info.drawIndex].childNodes[1].childNodes.length; i++) {
    //   if ($(`#calendar .top-calendar-date`)[info.drawIndex].childNodes[1].childNodes[i].dataset.id.includes(taskID)) {
    //     startDate = $(`#calendar .top-calendar-date`)[info.drawIndex].childNodes[1].childNodes[i].dataset.startDate;
    //     break;
    //   }
    // }
    calendarAPI.getEventDetail(taskID, startDate, (data) => {
      console.log(data);
      const _this = Top.Controller.get('calendarEventViewDialogLayoutLogic');
      _this._processEventData(data);
      tds('calendarEventViewDialog').open();
    });
  },
  onDateClick: function (event, widget, dataInfo) {
    const _this = Top.Controller.get('calendarMainLayoutLogic');
    _this.isDateClick = true;
    calendarEventRepo.setValue('event.startDate', dataInfo.date);
    calendarEventRepo.setValue('event.endDate', dataInfo.date);
    calendarEventRepo.setValue('event.startTime', dataInfo.startTime === undefined ? '0000' : dataInfo.startTime);
    calendarEventRepo.setValue('event.drawIndex', dataInfo.drawIndex);
    calendarCommonData.isEventEditMode = false;
    tds('calendarEventCreateDialog').open();
   },
  calculateMidDate: function( _from, _to) {
    let a = new Date(_from);
    const b = new Date(_to);
    const delta = (b-a) / (60*60*24*1000);
    
    if (_from == _to) {
        // day
        return _from;
    } else if (delta < 10) {
        // week
        return _from;
    } else {
        // month > add around 15 days from the beginning
        a.setDate(a.getDate() + 15);
        let newDate = a.toISOString().split('T')[0];
        
        return newDate.split("-")[0]+"-"+newDate.split("-")[1]+"-01";
    }
  }
  /* TOP filter button */
  /* _setFilterButton() {
    // document.getElementsByClassName('top-calendar-filter')[0].style.visibility = 'hidden';
    document.getElementsByClassName('top-calendar-filter-popup')[0].style.display = 'none';

    const filterEle = document.getElementsByClassName('top-calendar-filter')[0];
    filterEle.addEventListener('click', () => {
      tds('cal-content').src('calendarViewMoreLayout.html');
    });

    this._setFilterData();
  },
  _setFilterData() {
    const workspaceUserList = userManager.getWorkspaceUser(workspaceManager.getWorkspaceId(getRoomIdByUrl()));
    calendarCommonData.getTopCalendarWidget().setMemberList(
      workspaceUserList.map(u => ({
        id: u.USER_ID,
        text: u.USER_NICK
      }))
    );

    calendarCommonData.getTopCalendarWidget().setFilterHoliday(true);
    calendarCommonData.getTopCalendarWidget().setFilterEvent(true);
    calendarCommonData.getTopCalendarWidget().setFilterWork(false);
  },
  onFilterClick(event, widget, items) {
    console.log(items);
    const filterInfo = [];
    items.checkedItems.forEach(item => {
      if (item === 'filter-event') {
        // 이벤트
        filterInfo.push({
          FILTER_INFO: 'EVENT',
          FILTER_TYPE: 'task_type',
          USER_ID: userManager.getLoginUserId()
        });
      } else {
        // 유저
        filterInfo.push({
          FILTER_INFO: item,
          FILTER_TYPE: 'user',
          USER_ID: userManager.getLoginUserId()
        });
      }
    });
    if (calendarCommonData.holidayVisibility) {
      filterInfo.push({
        FILTER_INFO: 'holiday/KR',
        FILTER_TYPE: 'holiday_type',
        USER_ID: userManager.getLoginUserId()
      });
    }

    const _this = Top.Controller.get('calendarMainLayoutLogic');
    calendarAPI.getEventListByFilter(filterInfo, _this._processEventData);
  },
  onHolidayFilterClick: function(event, widget, items) {
    calendarCommonData.holidayVisibility = items.checked;
    const _this = Top.Controller.get('calendarMainLayoutLogic');
    _this._getEventList();
  },
  setUpFilter: function(event, widget, items) {
    if (FilterTypeRepo.type.length == items.checkedItems.length) {
      return;
    }
    FilterTypeRepo.type = [];

    var newFilterList = [];

    for (var a = 0; a < items.checkedItems.length; a++) {
      if (items.checkedItems[a].substr(0, 4) == 'user') {
        newFilterList.push({
          FILTER_INFO: items.checkedItems[a].split('/')[1],
          FILTER_TYPE: 'user',
          USER_ID: JSON.parse(sessionStorage.getItem('userInfo')).userId
        });
        FilterTypeRepo.type.push('user/' + items.checkedItems[a].split('/')[1]);
      } else if (items.checkedItems[a].substr(0, 8) == 'category') {
        newFilterList.push({
          FILTER_INFO: items.checkedItems[a].split('/')[1],
          FILTER_TYPE: 'category',
          USER_ID: JSON.parse(sessionStorage.getItem('userInfo')).userId
        });
        FilterTypeRepo.type.push('category/' + items.checkedItems[a].split('/')[1]);
      } else if (items.checkedItems[a] == 'filter-event') {
        newFilterList.push({
          FILTER_INFO: 'EVENT',
          FILTER_TYPE: 'task_type',
          USER_ID: JSON.parse(sessionStorage.getItem('userInfo')).userId
        });
        FilterTypeRepo.type.push('task_type/EVENT');
      } else if (items.checkedItems[a] == 'filter-work') {
        newFilterList.push({
          FILTER_INFO: 'WORK',
          FILTER_TYPE: 'task_type',
          USER_ID: JSON.parse(sessionStorage.getItem('userInfo')).userId
        });
        FilterTypeRepo.type.push('task_type/WORK');
      }
    }
    if (Top.Controller.get('TCalendarMonthMainLogic').isHolidayCheck) {
      newFilterList.push({
        FILTER_INFO: 'holiday/KR',
        FILTER_TYPE: 'holiday_type',
        USER_ID: JSON.parse(sessionStorage.getItem('userInfo')).userId
      });
    }

    let data = {
      header: {
        DATA_TYPE: 'J',
        service: 'CMS.Calendar.CalendarFilterUpdate'
      },
      dto: {
        calendar_id: TSchedule_getChannel(CLOUDSPACE_CHANNELS.TSCHEDULE.CHANNEL_TYPE),
        filter_list: newFilterList,
        USER_ID: JSON.parse(sessionStorage.getItem('userInfo')).userId,
        start_date: Top.Dom.selectById('MainCalendarWidget').getRange()[0],
        end_date: Top.Dom.selectById('MainCalendarWidget').getRange()[1]
      }
    };

    Top.Ajax.execute({
      type: 'POST',
      url: _workspace.url + 'Calendar/Calendar?action=FilterUpdate',
      dataType: 'json',
      data: JSON.stringify(data),
      contentType: 'application/json; charset=utf-8',
      xhrFields: { withCredentials: true },
      crossDomain: true,
      success: function(ret, xhr, status) {
        TScheduleTaskListRepo.TaskList = [];
        for (i = 0; i < ret.dto.taskListInfo.taskList.length; i++) {
          if (ret.dto.taskListInfo.taskList[i].taskType == 'EVENT') {
            var a =
              ret.dto.taskListInfo.taskList[i].taskID +
              ',' +
              ret.dto.taskListInfo.taskList[i].startDate +
              ',' +
              ret.dto.taskListInfo.taskList[i].endDate;

            TScheduleTaskListRepo.TaskList.push({
              taskID: a,
              title: ret.dto.taskListInfo.taskList[i].title,
              color: ret.dto.taskListInfo.taskList[i].color,
              taskType: ret.dto.taskListInfo.taskList[i].taskType,
              allday: ret.dto.taskListInfo.taskList[i].allDay,
              startDate: ret.dto.taskListInfo.taskList[i].startDate,
              endDate: ret.dto.taskListInfo.taskList[i].endDate,
              startTime: ret.dto.taskListInfo.taskList[i].startTime,
              endTime: ret.dto.taskListInfo.taskList[i].endTime,
              importance: ret.dto.taskListInfo.taskList[i].importance,
              //'userList': ret.dto.taskListInfo.taskList[i].userList,
              //'categoryList' : ret.dto.taskListInfo.taskList.labelList,
              ownUserID: ret.dto.taskListInfo.taskList[i].OWN_USER_ID
            });
          } else {
            TScheduleTaskListRepo.TaskList.push({
              taskID: ret.dto.taskListInfo.taskList[i].taskID,
              title: ret.dto.taskListInfo.taskList[i].title,
              color: ret.dto.taskListInfo.taskList[i].color,
              taskType: ret.dto.taskListInfo.taskList[i].taskType,
              allday: ret.dto.taskListInfo.taskList[i].allDay,
              startDate: ret.dto.taskListInfo.taskList[i].startDate,
              endDate: ret.dto.taskListInfo.taskList[i].endDate,
              startTime: ret.dto.taskListInfo.taskList[i].startTime,
              endTime: ret.dto.taskListInfo.taskList[i].endTime,
              importance: ret.dto.taskListInfo.taskList[i].importance,
              //'userList': ret.dto.taskListInfo.taskList[i].userList,
              //'categoryList' : ret.dto.taskListInfo.taskList.labelList,
              ownUserID: ret.dto.taskListInfo.taskList[i].OWN_USER_ID
            });
          }
        }

        Top.Dom.selectById('MainCalendarWidget').setSchedules(TScheduleTaskListRepo.TaskList);
        if (Top.Controller.get('TCalendarMonthMainLogic').isHolidayCheck) {
          Top.Dom.selectById('MainCalendarWidget').setHoliday(Top.Controller.get('TCalendarMonthMainLogic').holidayInfo);
        }
      },
      complete: function(ret, xhr, status) {},
      error: function(ret, xhr, status) {}
    });

  
		// FilterTypeRepo.type = items.checkedItems;
		// if (Top.Controller.get("TCalendarMonthMainLogic").isHolidayCheck){
		// 	FilterTypeRepo.type.push("holiday/KR");
		// }
		// this.applyFilter();
	
  } */
});

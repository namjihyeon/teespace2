Top.Controller.create('calendarEventCreateLayoutLogic', {
  init: function (event, widget) {
    this._initialize();
    calendarCommonData.isEventEditMode ? this._setEventDataForEdit() : this._initializeEventData();
    this._setPage();
  },
  _initialize() {
    // header의 back button visible 처리

    // tds('cal-header-icon').setVisible(tds('calendarEventCreateDialog').isOpen() || tds('calendarEventEditDialog').isOpen() ? 'visible' : 'none');
    // tds('cal-header-back').setVisible(tds('calendarEventCreateDialog').isOpen() || tds('calendarEventEditDialog').isOpen() ? 'none' : 'visible');

    const isExpand = appManager.isExpanded();
    tds('cal-icon--collapse').setVisible(isExpand ? 'visible' : 'none');
    tds('cal-icon--expand').setVisible(isExpand ? 'none' : 'visible');

    // 앱바 visble
    document.getElementById('calendarLayoutBar').style =
      tds('calendarEventCreateDialog').isOpen() || tds('calendarEventEditDialog').isOpen() ? 'display:none' : 'display:';

    // FIXME: 다시 보기
    // 새 일정 창에서calendarEventCreate_TailLayout이 아래로 내려가는 오류 수정
    tds('calendarEventCreateDialog').isOpen() || tds('calendarEventEditDialog').isOpen()
      ? $('#calendarEventCreateLayout.top-linearlayout-root').css('height', 'calc(100% - 1rem)')
      : $('#calendarEventCreateLayout.top-linearlayout-root').css('height', 'calc(100% - 3.875rem)');
  },
  /* initialize for create */
  _initializeEventData() {
    this._initialzeMemberListLayout();

    const mainCtrl = Top.Controller.get('calendarMainLayoutLogic');

    let currentTime;
    let allDay = false;

    const workspaceID = workspaceManager.getWorkspaceId(getRoomIdByUrl());
    const sessionKey = `calendarType_${workspaceID}`;

    switch (sessionStorage.getItem(sessionKey)) {
      case 'month':
        currentTime = this._currentTime();
        break;
      case 'week':
      case 'day':
        if (typeof calendarEventRepo.event.drawIndex != 'undefined') {
          allDay = true;
          currentTime = { startTime: '0000', endTime: '2400' };
        } else if (typeof calendarEventRepo.event.startTime == 'undefined') {
          currentTime = this._currentTime();
        } else {
          currentTime = this._customTime();
        }
        break;
    }

    // Calendar에서 알림 생성해두면 취소하고 다른 날짜에 생성하려고 할 때
    // 이전에 알림 추가해둔것이 그대로 있음 > 지우고 업데이트하도록 수정
    calendarAlarmRepo.items.splice(0, calendarAlarmRepo.items.length);
    calendarAlarmRepo.update('items');

    calendarEventRepo.setValue('event', {
      title: '',
      importance: false,
      startDate: mainCtrl.isDateClick ? calendarEventRepo.event.startDate : this._getTodayDate(),
      startTime: currentTime.startTime,
      endDate: mainCtrl.isDateClick ? calendarEventRepo.event.endDate : this._getTodayDate(),
      endTime: currentTime.endTime,
      color: '#FFD21C',
      location: '',
      description: '',
      allDay: allDay,
      repeatFlag: 0,
      repeatCycle: 1,
      repeatEndDate: '',
      userList: [],
      alarmList: [],
      fileList: [],
      labelList: [],
      colorIndex: 1
    });
  },
  _initialzeMemberListLayout() {
    // 기본값으로 룸멤버를 추가해주기 때문에 (append) 초기화 시키고 추가
    calendarMemberChipRepo.items.splice(0, calendarMemberChipRepo.items.length);

    const workspaceId = workspaceManager.getWorkspaceId(getRoomIdByUrl());

    spaceAPI
      .getSpaceMemberListBrief(workspaceId)
      .then((result) => {
        let inputArr = result.data.dto.UserProfileList.map((ele) => {
          return {
            id: ele.USER_ID,
            thumbPhoto: ele.THUMB_PHOTO,
            icon: 'icon-none',
            text: ele.USER_NAME
          };
        });
        this._setMemberListLayout(inputArr);
      })
      .catch((err) => {
        console.log(err);
        Top.Controller.get('calendarEventCreateLayoutLogic')._closeCreatePage();
      });
  },
  _currentTime() {
    const dateObj = new Date();
    let startHour = (endHour = dateObj.getHours());
    let startMin = (endMin = dateObj.getMinutes());

    function _getTimeStr(time) {
      // e.g. 1 => "01" / 0 => "00" / 21 => "21"
      return time.toString().length === 1 ? `0${time}` : time.toString();
    }

    if (startMin <= 30 && startMin > 0) {
      // 1~30분
      startMin = 30;
    } else {
      // 31분 ~ 00분(정각)
      startMin = 0;
      startHour += 1;
      endHour += 1;
    }

    if (startMin === 30) {
      // 시작시간이 hh:30 인 경우 마감시간을 hh+1:00 으로 설정하기 위함
      endHour += 1;
      endMin = 0;
    } else {
      endMin = 30;
    }
    return {
      startTime: _getTimeStr(startHour) + _getTimeStr(startMin),
      endTime: _getTimeStr(endHour) + _getTimeStr(endMin)
    };
  },
  _customTime() {
    let startTime = calendarEventRepo.event.startTime;

    let startHour = (endHour = parseInt(startTime.slice(0, 2)));
    let startMin = parseInt(startTime.slice(2, 4));

    function _getTimeStr(time) {
      // e.g. 1 => "01" / 0 => "00" / 21 => "21"
      return time.toString().length === 1 ? `0${time}` : time.toString();
    }

    if (startMin === 0) {
      endMin = 30;
    } else {
      endMin = 0;
      endHour += 1;
    }

    return {
      startTime: startTime,
      endTime: _getTimeStr(endHour) + _getTimeStr(endMin)
    };
  },
  _getTodayDate() {
    const today = new Date();
    let dd = today.getDate();
    let mm = today.getMonth() + 1; //As January is 0.
    let yyyy = today.getFullYear();

    if (dd < 10) dd = `0${dd}`;
    if (mm < 10) mm = `0${mm}`;
    return `${yyyy}-${mm}-${dd}`;
  },
  /* initialize for edit*/
  _setEventDataForEdit() {
    console.log(calendarEventRepo.event);
    this._oldStartDate = calendarEventRepo.event.startDate;
    this._oldEndDate = calendarEventRepo.event.endDate;
    this._setAlarmLayoutForEdit();
    this._setFileLayoutForEdit();

    calendarCommonData.isRepeatedEvent = calendarEventRepo.event.repeatFlag > 0;

    calendarMemberChipRepo.setValue('items', calendarEventRepo.event.userList);

    $('.top-chip-text').css('display', 'none');
    calendarMemberChipRepo.items.forEach((user, index) => {
      let p = userManager.getUserPhoto(user.id, 'small', user.thumbPhoto);
      this._setMemberProfile(index, p, user.id);
    });
  },
  _setAlarmLayoutForEdit() {
    calendarAlarmRepo.setValue('items', calendarEventRepo.event.alarmList);
    calendarAlarmRepo.update('items');

    if (calendarAlarmRepo.items.length === 5) tds('cal-alarm-add').setProperties({ disabled: true });
  },
  _setFileLayoutForEdit() {
    calendarEventRepo.event.fileList.forEach((file, index) => {
      file.id = index;
      this._setFileAttachItemLayout(file);
    });
  },
  /* set data to page */
  _setPage() {
    tds('cal-create-start-date').setDate(calendarEventRepo.event.startDate);
    tds('cal-create-end-date').setDate(calendarEventRepo.event.endDate);
    if (calendarEventRepo.event.repeatEndDate) {
      Top.Dom.selectById('cal-create-repeat-cycle').setProperties({ selectedIndex: 6 });
    } else {
      Top.Dom.selectById('cal-create-repeat-cycle').setProperties({ selectedIndex: calendarEventRepo.event.repeatFlag });
    }

    this._setRepeatDisplayTextView();

    this._checkLastDay();
    this._setWidget();
  },
  /* create or edit save */
  onEditClick() {
    const _this = Top.Controller.get('calendarEventCreateLayoutLogic');

    if (
      (calendarCommonData.isChangeRepeatFlag || calendarCommonData.isRepeatedEvent) &&
      calendarCommonData.isEventEditMode &&
      !calendarCommonData.isSelectedApplyOption
    ) {
      // 반복되는 모든 일정을 수정할지 물어보는 팝업 오픈
      tds('calendarEditRepeatedOptionDialog').open();
      return;
    }

    // flag 값 초기화
    calendarCommonData.isChangeRepeatFlag = false;
    calendarCommonData.isSelectedApplyOption = false;

    _this._setRequestData('edit');
  },
  onCreateClick() {
    const _this = Top.Controller.get('calendarEventCreateLayoutLogic');
    _this._setRequestData('create');
  },
  _setRequestData(page) {
    calendarEventRepo.setValue(
      'event.userList',
      calendarMemberChipRepo.items.map((user) => ({
        userID: user.id,
        text: user.text
      }))
    );
    if (!calendarEventRepo.event.title) calendarEventRepo.setValue('event.title', '(제목 없음)');
    if (calendarEventRepo.event.allDay) {
      calendarEventRepo.setValue('event.startTime', '0000');
      calendarEventRepo.setValue('event.endTime', '2400');
      //calendarEventRepo.setValue('event.color', '#FFD21C'); //색 임시방편
    }

    if (calendarEventRepo.event.userList.length === 0) {
      TeeToast.open({
        text: `선택된 멤버가 없습니다. 최소 1명 추가해주세요`,
        size: 'min'
      });
      return;
    }

    // 23:59 처리
    if (calendarEventRepo.event.endTime === '2359') {
      calendarEventRepo.setValue('event.endTime', '2400');
    }
    calendarEventRepo.setValue('event.alarmList', calendarAlarmRepo.items);

    const successCallback = () => {
      page === 'edit' ? (() => this._edit())() : (() => this._create())();
    };
    const errorCallback = () => {
      console.error('edit error');
    };
    this._sendFilesToStorage(calendarEventRepo.event.fileList, successCallback, errorCallback);
  },
  _edit() {
    // local file 첨부할때 fileChooser안보내도 되게 되면, 아래 로직을 _setRequestData에 옮기는게 나을듯
    if (calendarEventRepo.event.fileList.length) {
      calendarEventRepo.setValue(
        'event.fileList',
        calendarEventRepo.event.fileList.map((file) => Object.assign(file, { fileSrc: null, fileChooser: null }))
      );
    }

    calendarAPI.updateEvent(
      calendarEventRepo.event,
      {
        taskID: calendarEventRepo.event.taskID,
        oldStartDate: this._oldStartDate,
        oldEndDate: this._oldEndDate,
        repeatUpdateOption: calendarCommonData.selectedRepeatedOption
      },
      () => {
        // success callback
        Top.Controller.get('calendarEventCreateLayoutLogic')._closeCreatePage();
      }
    );
  },
  _create() {
    // local file 첨부할때 fileChooser안보내도 되게 되면, 아래 로직을 _setRequestData에 옮기는게 나을듯
    if (calendarEventRepo.event.fileList.length) {
      calendarEventRepo.setValue(
        'event.fileList',
        calendarEventRepo.event.fileList.map((file) => Object.assign(file, { fileSrc: null, fileChooser: null }))
      );
    }
    calendarAPI.createEvent(calendarEventRepo.event, () => {
      // success callback
      Top.Controller.get('calendarEventCreateLayoutLogic')._closeCreatePage();
    });
  },
  /* create or edit cancel */
  onCancelClick() {
    // edit, create에서 같이 씀.
    const _this = Top.Controller.get('calendarEventCreateLayoutLogic');
    // 앱바 none
    document.getElementById('calendarLayoutBar').style = 'display:none';
    _this._closeCreatePage();
  },
  _closeCreatePage() {
    const _this = Top.Controller.get('calendarMainLayoutLogic');
    if (tds('calendarEventCreateDialog').isOpen()) {
      tds('calendarEventCreateDialog').close(true);
      _this._getEventList();
    } else if (tds('calendarEventEditDialog').isOpen()) {
      tds('calendarEventEditDialog').close(true);
      _this._getEventList();
    } else {
      //      const _this = Top.Controller.get('calendarMainLayoutLogic');
      //      _this._getEventList();
      tds('cal-content').src('calendarMainLayout.html');
	  
	  
	  
	  // 월주일 유지관련
		const workspaceID = workspaceManager.getWorkspaceId(getRoomIdByUrl());
		calendarCommonData.dateRange[workspaceID] = calendarCommonData.getTopCalendarWidget().getRange();
		
    } // tds('cal-content').src('calendarMainLayout.html');
  },
  /* add member */
  _setMemberListLayout(users) {
    calendarMemberChipRepo.setValue('items', [...calendarMemberChipRepo.items, ...users]);
    $('.top-chip-text').css('display', 'none'); // chip에 바인딩된 데이터 업데이트할 때마다 숨겨줘야함.
    // chip에 바인딩된 데이터 업데이트 시 chip 새로 그리기 때문에 다시 set해야함.
    calendarMemberChipRepo.items.forEach((user, index) => {
      const p = userManager.getUserPhoto(user.id, 'small', user.thumbPhoto);
      this._setMemberProfile(index, p, user.id);
    });
  },
  _setMemberProfile(index, thumbPhoto, userID) {
    const imgTag = document.createElement('img');
    imgTag.src = thumbPhoto;
    imgTag.style = 'width: 20px;height:20px;'; // FIXME: css는 class로 할 것
    if (index > 0) document.querySelectorAll('#cal-member-chips .top-chip-content')[index].insertAdjacentElement('afterbegin', imgTag);
    else if (index === 0 && userID != userManager.getLoginUserId()) {
      // 첫번째 사용자인데 본인이 아닌 경우
      document.querySelectorAll('#cal-member-chips .top-chip-content')[index].insertAdjacentElement('afterbegin', imgTag);
    } else if (index === 0 && !document.querySelectorAll('#cal-member-chips .cal__member--me').length && userID === userManager.getLoginUserId()) {
      // 본인인 경우 TOP chip위젯에 class를 사용해서 클릭 불가 & chip 제거 불가 기능을 넣음.
      document.querySelectorAll('#cal-member-chips .top-chip-content')[index].insertAdjacentElement('afterbegin', imgTag);
      document.querySelectorAll('#cal-member-chips .top-chip-box')[0].classList.add('cal__member--me');
      document.querySelectorAll('#cal-member-chips .cal__member--me .top-chip-close ')[0].style = 'display:none;';
      const closeTag = document.createElement('i');
      closeTag.classList.add('cal__member-icon--me');
      document.querySelectorAll('#cal-member-chips .cal__member--me .top-chip-content')[0].insertAdjacentElement('beforeend', closeTag);
    }
  },
  onAddMemberClick() {
    const _this = Top.Controller.get('calendarEventCreateLayoutLogic');
    const alreadyInvitedList = calendarMemberChipRepo.items.map((user) => ({
      FRIEND_ID: user.id,
      THUMB_PHOTO: user.thumbPhoto,
      USER_NAME: user.text
    }));
    InviteMemberDialog.open({
      title: '멤버 추가',
      confirmedButtonText: '초대',
      alreadyInvitedList: alreadyInvitedList,
      confirmedButtonText: '추가',
      onConfirmedButtonClicked: function (users) {
        // console.log(users);
        const items = users.map((user) => ({
          text: user.USER_NAME,
          icon: 'icon-none',
          id: user.USER_ID,
          thumbPhoto: user.THUMB_PHOTO
        }));

        _this._setMemberListLayout(items);
        TeeToast.open({
          text: `선택된 멤버 ${users.length}명이 추가되었습니다.`,
          size: 'min'
        });
      },
      hideTeeMeetingCheckbox: true
    });
  },
  /* add alarm */
  onAlarmAddClick() {
    calendarAlarmRepo.items.unshift({ alarmType: '1', alarmTime: '0' });
    calendarAlarmRepo.update('items');

    if (calendarAlarmRepo.items.length === 5) tds('cal-alarm-add').setProperties({ disabled: true });
  },
  onAlarmRemoveClick: function (event, widget) {
    if (calendarAlarmRepo.items.length === 5) tds('cal-alarm-add').setProperties({ disabled: false });

    const index = widget.id.split('_')[1];
    calendarAlarmRepo.items.splice(index, 1);
    calendarAlarmRepo.update('items');
  },
  onAlarmTypeChange: function (event, widget) {
    const index = widget.id.split('_')[1];
    calendarAlarmRepo.items[index].alarmType = widget.getValue();
    calendarAlarmRepo.update('items');
  },
  onAlarmTimeChange: function (event, widget) {
    const index = widget.id.split('_')[1];
    calendarAlarmRepo.items[index].alarmTime = widget.getValue();
    calendarAlarmRepo.update('items');
  },
  /* file attach */
  _onMultipleFileSelected(fileChoosers) {
    spt.startLoader();

    fileChoosers.forEach((fc) => {
      Top.Controller.get('calendarEventCreateLayoutLogic')._onFileSelected(fc);
    });

    spt.stopLoader(false);
  },
  _onFileSelected(fileChooser) {
    const targetFileName = fileChooser.path.split('\\')[2];
    let file = {};
    for (let i = 0; i < fileChooser.file.length; i++) {
      if (fileChooser.file[i].name === targetFileName) {
        file = fileChooser.file[i];
        break;
      }
    }

    const nomalizedFileFullName = Top.Util.Browser.isIE() ? file.name : file.name.normalize('NFC');
    const fileName = nomalizedFileFullName.substring(0, nomalizedFileFullName.lastIndexOf('.'));
    const fileNameSliceIndex = nomalizedFileFullName.lastIndexOf('.');
    const fileExtension = fileNameSliceIndex === -1 ? null : nomalizedFileFullName.slice(fileNameSliceIndex + 1, nomalizedFileFullName.length);

    const fileInfo = {
      id: calendarEventRepo.event.fileList.length,
      fileID: 'LOCAL',
      fileName,
      fileExtension,
      listIndex: calendarEventRepo.event.fileList.length,
      fileStatus: 'NORMAL',
      fileSize: file.size,
      fileChannel: TEESPACE_CHANNELS.TSCHEDULE.CHANNEL_TYPE,
      ///// PO 업로드콜 나오면 삭제 예정
      // fileSrc: fileChooser.src,
      fileChooser: fileChooser
    };
    if (this._fileValidityCheck(file)) {
      this._setFileAttachItemLayout(fileInfo);
      calendarEventRepo.event.fileList.push(fileInfo);
    }
  },
  _onTDriveFileSelected(file) {
    const fileInfo = {
      id: calendarEventRepo.event.fileList.length,
      fileID: file.file_id,
      fileName: file.file_name,
      fileExtension: file.file_extension,
      listIndex: calendarEventRepo.event.fileList.length,
      fileStatus: 'NORMAL',
      fileSize: file.file_size,
      fileChannel: TEESPACE_CHANNELS.TSCHEDULE.CHANNEL_TYPE,
      ///// PO 업로드콜 나오면 삭제 예정
      // fileSrc: null, //fileChooser.src,
      fileChooser: null //fileChooser
    };
    this._setFileAttachItemLayout(fileInfo);
    calendarEventRepo.event.fileList.push(fileInfo);
  },
  _setFileAttachItemLayout({ id, fileExtension: ext, fileName: name, fileSize: size }) {
    const _this = this;
    tds('cal-file-attach-content').setProperties({ visible: 'visible' });
    tds('cal-file-attach-content').append(this._getFileAttachItemTemplate({ id, ext, name, size }));
    tds(`file-remove-${id}`).addEventListener('click', (e) => {
      TeeAlarm.open({
        title: `선택한 1개 파일을 삭제하시겠습니까?`,
        content: '삭제 후에는 복구할 수 없습니다.',
        buttons: [
          {
            text: '삭제',
            onClicked() {
              TeeAlarm.close();
              _this.onRemoveFileClick(e.path[1].id.slice(e.path[1].id.lastIndexOf('-') + 1, e.path[1].id.length));
            }
          }
        ],
        cancelButtonText: '취소' // default 취소
      });
    });
  },
  _getFileAttachItemTemplate({ id, ext, name, size }) {
    return `<top-linearLayout
    id="file-item-${id}"
    class="cal__file-item"
    orientation="horizontal">
        <top-imageView
        id="ImageView94"
        layout-horizontal-alignment="CENTER"
        src="${fileExtension.getInfo(ext).iconImage}">
        </top-imageView>
        <top-linearLayout
        id="Widget87ff13459af436168ba03e0ec7f2db49"
        border-width="0"
        class="cal__file-item-content"
        orientation="vertical">
            <top-textview
            id="file-name"
            class="cal__file-item-name"
            layout-horizontal-alignment="LEFT"
            layout-width="wrap_content"
            text="${name}">
            </top-textview>
            <top-textview
            id="file-size"
            class="cal__file-item-size"
            layout-horizontal-alignment="LEFT"
            layout-width="wrap_content"
            text="${size}">
            </top-textview>
        </top-linearLayout>
        <top-linearLayout
        id="Widget4527c5f29d0a3d9fbdcdb97e3f507da4"
        class="cal__file-item-remove"
        border-width="0"
        layout-height="match_parent"
        orientation="horizontal">
            <top-icon
            id="file-remove-${id}"
            class="icon-close_window">
            </top-icon>
        </top-linearLayout>
    </top-linearLayout>`;
  },
  _fileValidityCheck(file) {
    // files : 추가한 파일들
    // curTotalSize : 직전까지 추가된 파일들의 총합 size (현재 추가한 파일은 제외)

    const isExistSameFile = calendarEventRepo.event.fileList.some((f) => `${f.fileName}.${f.fileExtension}` === file.name);
    if (isExistSameFile) {
      TeeAlarm.open({ title: '이미 추가한 파일입니다.', content: '파일 첨부를 계속하려면 기존 파일을 제거하세요.' });
      return false;
    }

    if (file.name.substring(0, file.name.lastIndexOf('.')).length > 70) {
      TeeAlarm.open({ title: '파일명이 70자를 초과하면 업로드 할 수 없습니다.' });
      return false;
    }
    let curTotalSize = calendarEventRepo.event.fileList.reduce((prev, cur) => prev + cur.fileSize, 0);
    curTotalSize += file.size;
    if (curTotalSize >= 20 * 1024 * 1024) {
      //20MB
      TeeAlarm.open({ title: '파일 첨부는 최대 20MB까지 가능합니다.' });
      return false;
    }

    return true;
    /* 
      // 확장자 검사 로직
      const nomalizedName = Top.Util.Browser.isIE() ? file.name : file.name.normalize('NFC');
      const fileNameSliceIndex = nomalizedName.lastIndexOf('.');
      const fileExt = fileNameSliceIndex === -1 ? null : nomalizedName.slice(fileNameSliceIndex + 1, nomalizedName.length);
      if (!fileExt) {
        ALERT('확장자가 없는 파일은 업로드할 수 없습니다.');
        return false;
      } */
  },
  onRemoveFileClick(id) {
    let removeIndex = null;
    for (let i = 0; i < calendarEventRepo.event.fileList.length; i++) {
      if (calendarEventRepo.event.fileList[i].id === Number(id)) {
        removeIndex = i;
        break;
      }
    }
    calendarEventRepo.event.fileList.splice(removeIndex, 1);
    calendarEventRepo.update('event');
    tds('cal-file-attach-content').removeWidget(document.querySelector(`#file-item-${id}`));
  },
  _sendFilesToStorage(files, completeCallback) {
    if (!files.length) {
      completeCallback(); //보낼 파일이 없는 경우.. 바로 callback 호출
      return;
    }

    const workspaceId = workspaceManager.getWorkspaceId(getRoomIdByUrl());
    const channelId = workspaceManager.getChannelList(workspaceManager.getWorkspaceId(getRoomIdByUrl()), TEESPACE_CHANNELS.TSCHEDULE.CHANNEL_TYPE);
    const userId = userManager.getLoginUserId();
    const promises = [];

    files.forEach((file) => {
      if (file.fileID === 'LOCAL') {
        // 로컬 파일 업로드
        const inDto = {
          dto: {
            workspace_id: workspaceId,
            channel_id: channelId,
            storageFileInfo: {
              user_id: userId,
              file_last_update_user_id: userId,
              file_id: '',
              file_name: file.fileName,
              file_extension: file.fileExtension,
              file_created_at: '',
              file_updated_at: '',
              file_size: file.fileSize,
              user_context_1: '',
              user_context_2: '',
              user_context_3: ''
            }
          }
        };
        promises.push(
          new Promise((resolve, reject) => {
            storageManager.UploadFile(
              file.fileChooser.file[0],
              inDto,
              'Calendar',
              'Calendar?action=TempStorages',
              (data) => {
                // success callback
                file.fileID = data.dto.storageFileInfoList[0].file_id;
                resolve();
              },
              (data) => {
                // error callback
                console.error(data);
                reject();
              }
            );
          })
        );
      } else {
        // TeeDrive 파일 업로드
        promises.push(
          new Promise((resolve, reject) => {
            storageManager.CopyFile(
              'Shallow',
              file.fileID,
              channelId,
              file.fileName,
              file.fileExtension,
              '',
              '',
              '',
              (data) => {
                // success callback
                file.fileID = data.dto.storageFileInfoList[0].file_id;
                resolve();
              },
              (data) => {
                // error callback
                console.error(data);
                reject();
              }
            );
          })
        );
      }
    });
    // NOTE: Promise를 사용하지않으면, storageManager API response 받기전에 completeCallback이 불림.
    // storageManager API response를 완전히 받은 후에 completeCallback이 불리도록 순서를 보장하기 위해 promise를 사용.
    Promise.all(promises).then(() => {
      completeCallback(); //파일 모두 업로드가 끝나면 callback 실행
    });
  },
  /* **************************옛 날 코 드************************* */
  /* ************************************************************** */
  updateStartDateEvent: function (event, widget) {
    calendarEventRepo.setValue('event.startDate', widget.getDate());

    const _this = Top.Controller.get('calendarEventCreateLayoutLogic');
    _this._checkLastDay();
    _this._isValidEventStartDateAndTime();
  },
  updateStartTimeEvent: function (event, widget) {
    calendarEventRepo.setValue('event.startTime', widget.getValue());

    const _this = Top.Controller.get('calendarEventCreateLayoutLogic');
    _this._isValidEventStartDateAndTime();
  },
  updateEndDateEvent: function (event, widget) {
    calendarEventRepo.setValue('event.endDate', widget.getDate());

    const _this = Top.Controller.get('calendarEventCreateLayoutLogic');
    _this._setWidget();
  },
  updateEndTimeEvent: function (event, widget) {
    calendarEventRepo.setValue('event.endTime', widget.getValue());

    const _this = Top.Controller.get('calendarEventCreateLayoutLogic');
    _this._setWidget();
  },
  _isValidEventStartDateAndTime() {
    if (!this._isValidDate('date')) {
      calendarEventRepo.setValue('event.endDate', calendarEventRepo.event.startDate);
      tds('cal-create-end-date').setDate(calendarEventRepo.event.endDate);

      calendarEventRepo.setValue('event.endTime', this._getTimeAfterXmin(calendarEventRepo.event.endDate, calendarEventRepo.event.startTime));
      tds('cal-create-end-time').select(calendarEventRepo.event.endTime);
    } else {
      if (!this._isValidDate('time')) {
        calendarEventRepo.setValue('event.endTime', this._getTimeAfterXmin(calendarEventRepo.event.endDate, calendarEventRepo.event.startTime));
        tds('cal-create-end-time').select(calendarEventRepo.event.endTime);
      } else {
        this._setWidget(); //event for end date in case it gets corrected
      }
    }
  },
  _getTimeAfterXmin: function (date, time, x = 30) {
    let dateArr = date.split('-');
    let dateObj = new Date(dateArr[0], dateArr[1], dateArr[2], time.slice(0, 2), time.slice(2, 4), 0);

    dateObj.setMinutes(dateObj.getMinutes() + x); // 30분 후

    let hours = dateObj.getHours().toString();
    hours = hours.length === 1 ? `0${hours}` : hours;
    let minutes = dateObj.getMinutes().toString();
    minutes = minutes.length === 1 ? `0${minutes}` : minutes;

    const result = hours + minutes;
    return result === '0000' ? '2400' : result;
  },
  _setWidget() {
    const isValidDate = this._isValidDate('time');

    if (isValidDate) {
      // 정상
      tds('cal-create-end-date').setProperties({ 'border-color': '#FFFFFF' });
      tds('cal-create-end-time').setProperties({ 'border-color': '#FFFFFF' });
      tds('TScheduleAddDetailTab1UserException_Icon').setProperties({ visible: 'none' });

      tds('StartDateErrorPopover').close();
    } else {
      // 비정상
      tds('cal-create-end-date').setProperties({ 'border-color': '#FF0000' });
      tds('cal-create-end-time').setProperties({ 'border-color': '#FF0000' });
      tds('TScheduleAddDetailTab1UserException_Icon').setProperties({ visible: 'visible' });

      tds('StartDateErrorPopover').adjustPosition();
      tds('StartDateErrorPopover').open();
    }
    tds('TScheduleAddDetailTab1SaveCancel_Save').setProperties({ disabled: !isValidDate });
  },
  _checkLastDay() {
    const today = new Date(calendarEventRepo.event.startDate);
    const lastDay = new Date(today.getYear(), today.getMonth() + 1, 0).getDate();
    const lastDayLayout = tds('TScheduleAddDetailTab1RepeatCycle_Exception_LastDay');
    const fixedDateBtn = tds('cal-repeat-fixed-date');
    const lastDateBtn = tds('cal-repeat-last-date');

    if (lastDayLayout.getVisible() == 'visible') {
      lastDayLayout.setVisible('none');
    }

    if (today.getDate() == lastDay) {
      const cycleFlag = calendarEventRepo.event.repeatFlag;
      const cycle = calendarEventRepo.event.repeatCycle;

      if (cycleFlag == 4) {
        fixedDateBtn.setText('매월 ' + lastDay + '일');
        lastDateBtn.setText('매월 말일');
        lastDayLayout.setVisible(true);
      } else if (cycleFlag == 5) {
        if (today.getMonth() === 1 && today.getDate() === 29) {
          fixedDateBtn.setText('4년마다 02.29');
          lastDateBtn.setText('매년 2월 말일');
          lastDayLayout.setVisible(true);
        }
      }
      if (cycle < 0) {
        lastDateBtn.setProperties({ checked: true });
      }
    }
  },
  updateAllDayEvent: function (event, widget) {
    const _this = Top.Controller.get('calendarEventCreateLayoutLogic');
    _this._setWidget();
  },
  _isValidDate(type) {
    // data or time
    const startTime = type === 'date' ? '0000' : calendarEventRepo.event.startTime;
    const endTime = type === 'date' ? '0000' : calendarEventRepo.event.endTime;
    if (startTime === undefined || endTime === undefined) return;
    const startDate = new Date(`${calendarEventRepo.event.startDate}T${startTime.substr(0, 2)}:${startTime.substr(2, 2)}:00`);
    const endDate = new Date(`${calendarEventRepo.event.endDate}T${endTime.substr(0, 2)}:${endTime.substr(2, 2)}:00`);
    return endDate.getTime() > startDate.getTime();
  },
  updateRepeatEvent: function (event, widget) {
    const _this = Top.Controller.get('calendarEventCreateLayoutLogic');
    var selectInfo = widget.getSelected();

    if (calendarEventRepo.event.repeatFlag !== 0) {
      calendarCommonData.isChangeRepeatFlag = true;
    }

    if (selectInfo.repeat_flag == 6) {
      _this.openSetIterationDialog();
    } else {
      calendarEventRepo.event.repeatFlag = selectInfo.repeat_flag;
      calendarEventRepo.event.repeatCycle = selectInfo.repeat_cycle;
      calendarEventRepo.event.repeatEndDate = null;

      _this._setRepeatDisplayTextView();
      _this._checkLastDay();
    }
  },
  _setRepeatDisplayTextView: function () {
    var cycle = Math.abs(calendarEventRepo.event.repeatCycle);
    var cycleFlag = calendarEventRepo.event.repeatFlag;
    var cycleType = ['주중', '일', '주', '개월', '년'];
    var text = '';

    if (cycleFlag > 0) {
      var flagIndex = cycleFlag - 1;

      if (flagIndex == 0) {
        text += cycleType[flagIndex];
      } else {
        text += cycle + cycleType[flagIndex] + '마다';
      }
      text += ' 반복';

      var endDate = calendarEventRepo.event.repeatEndDate;

      if (endDate) {
        text += ' / ' + endDate + ' 종료';
      }
    }
    tds('TScheduleAddDetailTab1RepeatCycleDisplay_TextView').setText(text);
  },
  openSetIterationDialog: function () {
    this.dialog = Top.Controller.get('calendarSetEventIterationLogic');
    this.dialog.params = {
      repeatFlag: calendarEventRepo.event.repeatFlag,
      repeatCycle: Math.abs(calendarEventRepo.event.repeatCycle),
      endDate: calendarEventRepo.event.repeatEndDate,
      onConfirm: this.onConfirmIterationDialogEvent.bind(this)
    };

    Top.Dom.selectById('CalendarSetEventIterationDialog').open();
  },
  onConfirmIterationDialogEvent: function (flag, cycle, date) {
    calendarEventRepo.event.repeatFlag = flag;
    calendarEventRepo.event.repeatCycle = cycle;
    calendarEventRepo.event.repeatEndDate = date;
    calendarEventRepo.update('event');

    this._setRepeatDisplayTextView();
    this._checkLastDay();
  },
  onRepeatDateChangeClick(event, widget) {
    const fixedDateBtn = tds('cal-repeat-fixed-date');
    const lastDateBtn = tds('cal-repeat-last-date');

    // default는 날짜 지정 -> 아무것도 누르지 않으면 자동으로 날짜 지정
    // 각 조건에 맞는 로직을 탄 뒤에의 결과는 날짜 지정인 경우 1, 말일인 경우 -1
    if (fixedDateBtn.getChecked()) {
      calendarEventRepo.event.repeatCycle *= -1;
      console.log(calendarEventRepo.event.repeatCycle);
    } else if (lastDateBtn.getChecked()) {
      calendarEventRepo.event.repeatCycle = Math.abs(calendarEventRepo.event.repeatCycle);
      console.log(calendarEventRepo.event.repeatCycle);
    }
  },
  onColorClick: function (event, widget) {
    if (calendarEventRepo.event.colorIndex) tds(`TScheduleAddDetailTab1Color_${calendarEventRepo.event.colorIndex}`).clear();
    const check = Top.Widget.create('top-icon');
    const checkIndex = widget.id.split('_')[1];
    check.setProperties({
      id: 'check-icon',
      class: 'icon-check',
      margin: '4px 0px 0px 4px',
      'text-color': '#FFFFFF',
      'text-size': '14px'
    });
    widget.addWidget(check);
    widget.complete();
    calendarEventRepo.event.colorIndex = checkIndex;
    calendarEventRepo.event.color = calendarEventColorRepo.color[checkIndex - 1].eventColor;
  }
});

/* file attch menu logic */
Top.Controller.create('TeeSpaceLogic', {
  onFileAttachFromTDriveClick: function (event, widget) {
    const _this = Top.Controller.get('calendarEventCreateLayoutLogic');
    OPEN_TDRIVE({
      buttonFn1: undefined,
      buttonFn2: undefined,
      channel_id: workspaceManager.getChannelList(workspaceManager.getWorkspaceId(getRoomIdByUrl()), TEESPACE_CHANNELS.TSCHEDULE.CHANNEL_TYPE),
      preventMultiSelect: false,
      filteringFileExtensionList: [],
      selectedFile: {},
      successCallback: function (file) {
        // 여러 개 파일을 선택하면 successCallback이 파일 수만큼 온다
        // 파일 업로드를 위한 파일 메타 수정 후 talkData.fileChooserList에 넣어준다
        console.log(file);
        file = file.storageFileInfo;
        if (_this._fileValidityCheck({ name: `${file.file_name}.${file.file_extension}`, size: file.file_size })) {
          _this._onTDriveFileSelected(file);
        }
      },
      cancelCallback: function () {
        // notiFeedback('파일 업로드에 실패하였습니다.');
      }
    });
  },
  onFileAttachFromLocalClick: function (event, widget) {
    const _this = Top.Controller.get('calendarEventCreateLayoutLogic');
    Top.Device.FileChooser.create({
      onBeforeLoad(file) {
        return true;
      },
      onFileChoose: _this._onMultipleFileSelected,
      charset: 'euc-kr',
      multiple: true
    }).show();
  }
});

Top.Controller.create('calendarEditRepeatedOptionDialogLayoutLogic', {
  init() {calendarCommonData.selectedRepeatedOption = 0},
  onApplyOptionChange(event, widget) {
    calendarCommonData.selectedRepeatedOption = widget.getCheckedButton().getValue();
  },
  onApplyClick: function(event, widget) {
    calendarCommonData.isSelectedApplyOption = true;
    if (calendarCommonData.isEventEditMode) {
      const _this = Top.Controller.get('calendarEventCreateLayoutLogic');
      //_this._setEventDataForEdit();
      calendarCommonData.isEventEditMode = !calendarCommonData.isEventEditMode;

      calendarCommonData.isChangeRepeatFlag = false;
      calendarCommonData.isSelectedApplyOption = false;

      _this._setRequestData('edit');

    } else if (calendarCommonData.isRepeatedScheduleUpdateMode) {
      const oldScheduleData = calendar.getScheduleRepo();
      const taskID = oldScheduleData.taskID;
      const oldStartDate = oldScheduleData.startDate;
      const oldEndDate = oldScheduleData.endDate;
      const parameters  = { taskID, oldStartDate, oldEndDate, repeatUpdateOption: calendarCommonData.selectedRepeatedOption };
      console.debug(parameters);
      calendarAPI.updateEvent(
        calendarEventRepo.event,
        parameters,
        () => { console.log("calendar dnd repeated schedule update succeeded"); }
      );
      calendarCommonData.isRepeatedScheduleUpdateMode = !calendarCommonData.isRepeatedScheduleUpdateMode;
    }
    tds('calendarEditRepeatedOptionDialog').close(true);
  },
  onCancelClick: function(event, widget) {
    tds('calendarEditRepeatedOptionDialog').close(true);
  }
});

Top.Controller.create('calendarLayoutLogic', {
  init() {
    this._initializeLayout();
    calendarEventRepo.setValue('event', { userList: [], alarmList: [], fileList: [], labelList: [] });
  },
  _initializeLayout() {
    tds('cal-content').src('calendarMainLayout.html');

    const isExpand = appManager.isExpanded();
    tds('cal-icon--collapse').setVisible(isExpand ? 'visible' : 'none');
    tds('cal-icon--expand').setVisible(isExpand ? 'none' : 'visible');
  },
  /* header event handler */
  onExpandClick: function (event, widget) {
    tds('cal-icon--collapse').setVisible('visible');
    tds('cal-icon--expand').setVisible('none');
    onExpandButtonClick('schedule');
  },
  onCollapseClick: function (event, widget) {
    tds('cal-icon--collapse').setVisible('none');
    tds('cal-icon--expand').setVisible('visible');
    onFoldButtonClick('schedule');
  },
  onCloseClick: function (event, widget) {
    onCloseButtonClick('schedule');
	
	const workspaceID = workspaceManager.getWorkspaceId(getRoomIdByUrl());
    calendarCommonData.dateRange[workspaceID] = null;
    
  },
  onBackClick() {
    tds('cal-content').src('calendarMainLayout.html');
  }
});

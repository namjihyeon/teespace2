Top.Controller.create('calendarUrlImportExportLayoutLogic', {
  init: function (event, widget) {
    this._initialize();
  },
  _initialize() {
    const calendar_id = workspaceManager.getChannelList(workspaceManager.getWorkspaceId(getRoomIdByUrl()), TEESPACE_CHANNELS.TSCHEDULE.CHANNEL_TYPE);
    const exportUrl = `${_workspace.url}Calendar/${calendar_id}.ics`;
    tds('cal-urlexport-textfield').setText(exportUrl);
  },
  onBackClick: function (event, widget) {
    tds('cal-content').src('calendarMainLayout.html');
  },
  onImportUrlClick: function (event, widget) {
    const importUrl = tds('cal-urlimport-textfield').getText();
    calendarAPI.getUrlImport(importUrl);
    tds('cal-content').src('calendarMainLayout.html');
    TeeToast.open({ text: '캘린더에 일정이 추가되었습니다.' });
  }
});

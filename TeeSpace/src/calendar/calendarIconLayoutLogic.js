Top.Controller.create('calendarIconLayoutLogic', {
  init: function (event, widget) {
    this._initializeLayout();
    calendarEventRepo.setValue('event', { userList: [], alarmList: [], fileList: [], labelList: [] });
  },
  _initializeLayout() {
    const isExpand = appManager.isExpanded();
    tds('cal-iconlayout--collapse').setVisible(isExpand ? 'visible' : 'none');
    tds('cal-iconlayout--expand').setVisible(isExpand ? 'none' : 'visible');
  },
  onExpandClick: function (event, widget) {
    tds('cal-iconlayout--collapse').setVisible('visible');
    tds('cal-iconlayout--expand').setVisible('none');
    onExpandButtonClick('schedule');
  },
  onCollapseClick: function (event, widget) {
    tds('cal-iconlayout--collapse').setVisible('none');
    tds('cal-iconlayout--expand').setVisible('visible');
    onFoldButtonClick('schedule');
  },
  onCloseClick: function (event, widget) {
    onCloseButtonClick('schedule');
	
	const workspaceID = workspaceManager.getWorkspaceId(getRoomIdByUrl());
    calendarCommonData.dateRange[workspaceID] = null;
  }
});

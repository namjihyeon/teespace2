Top.Controller.create('calendarViewMoreLayoutLogic', {
  init: function (event, widget) {
    this._initialize(this._setMyCalendarLayout())
      .then(() => this._setFilterInfo())
      .then(() => this._expressCalendarFilter());
  },
  _initialize: function (f1) {
    return new Promise(function (resolve, reject) {
      // header의 back button visible 처리
      // tds('cal-header-icon').setVisible('none');
      tds('cal-header-back').setVisible('visible');
      document.getElementById('calendarLayoutBar').style = 'display:';
      f1;
      resolve();
    });
  },
  _setMyCalendarLayout: function () {
    const room_id = workspaceManager.getWorkspaceId(getRoomIdByUrl());
    const user_id = userManager.getLoginUserId();
    // user_id와 room_id를 이용해 조회하여 나온 값은 해당 user가 속해있는 모든 messanger list 정보를 반환함
    const whole_room_list = talkServer2.getMessengerRooms(user_id, room_id).ttalkMessengersList;
    // 전체 room_list 중 현재 room_id 값을 가진 값에서 WS_NAME을 가져오면 됨
    for (var i = 0; i < whole_room_list.length; i++) {
      if (whole_room_list[i].WS_ID === room_id) {
        const calendarName = whole_room_list[i].WS_NAME;
        tds('cal-mycalendar-check').setText(calendarName);
        break;
      }
    }
  },
  _setFilterInfo: function () {
    return new Promise(function (resolve, reject) {
      calendarAPI.getFilterInfo((data) => {
        const calendarFilterInfo = data.filterInfo.map((h) => {
          return { calendar_id: h.calendar_id, filter_tf: h.filter_tf };
        });
        // 공휴일 check
        calendarFilterRepo.setValue('holiday', calendarFilterInfo[0].filter_tf);
        // 추후 음력이 추가된다면 여기에 추가
        // 내 캘린더 check
        calendarFilterRepo.setValue('mine', calendarFilterInfo[1].filter_tf);
        var others_name = new Array();
        var others_flag = new Array();
        // 추후 음력이 추가된다면 i = 3 부터 시작
        for (var i = 2; i < calendarFilterInfo.length; i++) {
          // 외부 캘린더 check
          others_name[i - 2] = calendarFilterInfo[i].calendar_id;
          others_flag[i - 2] = calendarFilterInfo[i].filter_tf;
        }
        calendarOtherCalendarRepo.setValue('items', others_name);
        calendarFilterRepo.setValue('others', others_flag);
        console.log(calendarOtherCalendarRepo);
        console.log(calendarFilterRepo);
        resolve();
      });
    });
  },
  _expressCalendarFilter: function () {
    tds('cal-holiday-check').setChecked(calendarFilterRepo.holiday);
    tds('cal-mycalendar-check').setChecked(calendarFilterRepo.mine);
    for (var j = 0; j < calendarOtherCalendarRepo.items.length; j++) {
      tds(`cal-othercalendar-check_${j}`).setText(calendarOtherCalendarRepo.items[j]);
      tds(`cal-othercalendar-check_${j}`).setChecked(calendarFilterRepo.others[j]);
    }
    console.log('setting finished');
  },
  onHolidayCheckBoxChange: function (event, widget) {
    calendarCommonData.holidayVisibility = widget.getChecked();
    // check되면 create, uncheck되면 delete
    if (calendarCommonData.holidayVisibility) {
      calendarAPI.createCheckedCalendar('holiday_visible', 'mine');
    } else {
      calendarAPI.deleteCheckedCalendar('holiday_visible', 'mine');
    }
  },
  onMyCalendarCheckBoxChange: function (event, widget) {
    const calendar_id = workspaceManager.getChannelList(workspaceManager.getWorkspaceId(getRoomIdByUrl()), TEESPACE_CHANNELS.TSCHEDULE.CHANNEL_TYPE);
    // check되면 create, uncheck되면 delete
    if (widget.getChecked()) {
      calendarAPI.createCheckedCalendar(calendar_id, 'mine');
    } else {
      calendarAPI.deleteCheckedCalendar(calendar_id, 'mine');
    }
  },
  onOtherCalendarCheckBoxChange: function (event, widget) {
    const calendar_id = workspaceManager.getChannelList(workspaceManager.getWorkspaceId(getRoomIdByUrl()), TEESPACE_CHANNELS.TSCHEDULE.CHANNEL_TYPE);
    if (widget.getChecked()) {
      calendarAPI.createCheckedCalendar(calendar_id, widget.getText());
    } else {
      calendarAPI.deleteCheckedCalendar(calendar_id, widget.getText());
    }
  }
});

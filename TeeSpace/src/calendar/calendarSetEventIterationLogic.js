Top.Controller.create('calendarSetEventIterationLogic', {
  init: function(event, widget) {
    /*
     * params
     *  repeatFlag;
     *  repeatCycle;
     *  endDate;
     *  onConfirm(flag, cycle, date, text);
     */
    this.params;

    this.root = Top.Controller.get('TScheduleRootLogic');

    this.dialog = Top.Dom.selectById('CalendarSetEventIterationDialog');
    this.cycleTF = Top.Dom.selectById('TScheduleSetEventIterationRepeatCycle_TextField');
    this.cycleSB = Top.Dom.selectById('TScheduleSetEventIterationRepeatCycle_SelectBox');

    this.endCB = Top.Dom.selectById('TScheduleSetEventIterationRepeatEnd_CheckBox');
    this.endDP = Top.Dom.selectById('TScheduleSetEventIterationRepeatEnd_DatePicker');
    this.alertIcon = Top.Dom.selectById('TScheduleSetEventIterationRepeatEnd_AlertIcon');
    this.confirmBtn = Top.Dom.selectById('TScheduleSetEventIteraionSaveCancel_Save');
    this.popover = Top.Dom.selectById('RepeatEndDateErrorPopover');

    /*
     * cycle type : [반복 안함], [주중 반복]
     * selectBox Index = DEFAULT
     */

    var cycleTypeIdx = this.params.repeatFlag - 2;
    if (cycleTypeIdx >= 0) {
      this.cycleTF.setProperties({ text: this.params.repeatCycle });
      this.cycleSB.setProperties({ selectedIndex: cycleTypeIdx });
    } else {
      this.cycleSB.setProperties({ selectedIndex: 1 });
      this.cycleTF.setProperties({ text: 1 });
    }

    /* endDate value
     * checkbox - check setting
     * datepicker - disable, date setting
     */

    var endDate = this.params.endDate;
    if (!endDate) {
      this.endCB.setProperties({ checked: false });
      this.endDP.setProperties({ disabled: true });
      this.endDP.setProperties({ date: this.initDatePickerValue() });
    } else {
      this.endCB.setProperties({ checked: true });
      this.endDP.setProperties({ disabled: false });
      this.endDP.setProperties({ date: endDate });
    }
  },
  cancelEventIteration: function(event, widget) {
    this.dialog.close();
  },
  checkEndEvent: function(event, widget) {
    if (widget.isChecked()) {
      this.endDP.setProperties({ disabled: false });
    } else {
      this.endDP.setProperties({ disabled: true });
    }
  },
  setEventIterationDirect: function(event, widget) {
    var cycle = this.cycleTF.getText();
    var endDate;

    if (this.endCB.isChecked()) {
      endDate = this.endDP.getDate();
    }

    this.params.onConfirm(parseInt(this.cycleSB.getClickedIndex() + 2), cycle, endDate);
    this.dialog.close();
  },
  processFocusLostTextField: function(event, widget) {
    var text = this.filterText(this.cycleTF.getText());

    if (text == false) {
      text = 1;
    }
    this.cycleTF.setText(text);
  },
  processKeyEventTextField: function(event, widget) {
    var text = this.filterText(this.cycleTF.getText());

    this.cycleTF.setText(text);
  },
  onSelectDatePicker: function(event, widget) {
    const pickDate = this.endDP.getDate();
    const startDate = calendarEventRepo.event.startDate;

    if (Top.Controller.get('calendarSetEventIterationLogic')._dateDiff(pickDate, startDate) > 0) {
      this.alertIcon.setProperties({ visible: 'visible' });
      this.confirmBtn.setProperties({ disabled: true });

      this.endDP.setProperties({ 'border-color': '#FF0000' });

      this.popover.open();
    } else {
      this.alertIcon.setProperties({ visible: 'none' });
      this.confirmBtn.setProperties({ disabled: false });

      this.endDP.setProperties({ 'border-color': '#FFFFFF' });

      this.popover.close();
    }
  },
  _dateDiff(_date1, _date2) {
    //_date1: yyyy-mm-dd, _date2: yyyy-mm-dd, output: -number/+number
    var diffDate_1 = _date1 instanceof Date ? _date1 : new Date(_date1);
    var diffDate_2 = _date2 instanceof Date ? _date2 : new Date(_date2);

    diffDate_1 = new Date(diffDate_1.getFullYear(), diffDate_1.getMonth(), diffDate_1.getDate());
    diffDate_2 = new Date(diffDate_2.getFullYear(), diffDate_2.getMonth(), diffDate_2.getDate());

    //var diff = Math.abs(diffDate_2.getTime() - diffDate_1.getTime());
    var diff = diffDate_2.getTime() - diffDate_1.getTime();
    diff = Math.ceil(diff / (1000 * 3600 * 24));

    return diff;
  },
  // Local Function
  filterText: function(text) {
    if (text != null) {
      text = text.replace(/[^0-9]/g, '');
      if (parseInt(text.charAt(0)) == 0) {
        text = '';
      }
    }
    return text;
  },
  initDatePickerValue: function() {
    let startDate = new Date(calendarEventRepo.event.startDate);
    let date = startDate.getDate();
    let month = startDate.getMonth() + 1;
    let year = startDate.getFullYear();

    if (date < 10) {
      date = '0' + date;
    }

    if (month < 10) {
      month = '0' + month;
    }

    return parseInt(year) + 1 + '-' + month + '-' + date;
  }
});

Top.Controller.create('calendarEventViewDialogLayoutLogic', {
  init: function (event, widget) {
    $('#cal-detail-attachment-list.top-linearlayout-root')[0].style.display = 'none';
    this._setLayoutVisible(calendarEventRepo.event);
    this._setFileListLayout(calendarEventRepo.event);
  },
  /* process data 
  => calendarMainLayoutLogic의 onEventClick에서 불림 */
  _processEventData(data) {
    calendarEventRepo.setValue(
      'event',
      Object.assign(data, {
        timeInfoStr: this._getTimeInfoStr(data), // date + time
        cycleInfoStr: this._getCycleInfoStr(data), //  cycle
        alarmInfoStr: this._getAlarmInfoStr(data), // alarm
        title: data.title ? data.title : '(제목 없음)',
        importanceVisibility: data.importance ? 'visible' : 'none',
        userList: this._getUserList(data.userList),
        taskType: data.taskType
      })
    );
    setTimeout(() => {
      calendarEventRepo.setValue('event.membersInfoStr', this._getMembersInfoStr());
    }, 0);
  },
  _getTimeInfoStr({ allDay, startTime, endTime, startDate, endDate }) {
    // TODO: 제발 epochtime
    // date
    const sDate = startDate.replace(/-/gi, '.'); // 2020-03-01 -> 2020.03.01
    const eDate = endDate.replace(/-/gi, '.');

    if (allDay) {
      if (startDate === endDate) return `${sDate} 종일`;
      else return `${sDate} 종일 ~ ${eDate} 종일`;
    }

    // time
    function _getProcessedTime(time) {
      // let timezone = '오전';
      let hour = time.slice(time[0] === '0' ? 1 : 0, 2);
      // if (hour > 12) {
      //   timezone = '오후';
      //   hour -= 12;
      // }
      const min = time.slice(2, 4);
      return {
        // timezone,
        hour,
        min
      };
    }

    if (endTime === '2400') {
      endTime = '2359';
    }
    const sTimeInfo = _getProcessedTime(startTime);
    const eTimeInfo = _getProcessedTime(endTime);

    // result
    let timeInfo = sDate;
    // timeInfo += ` ${sTimeInfo.timezone} ${sTimeInfo.hour}:${sTimeInfo.min}`;
    timeInfo += ` ${sTimeInfo.hour}:${sTimeInfo.min}`;
    timeInfo += ` ~ ${eDate}`;
    // timeInfo += ` ${eTimeInfo.timezone} ${eTimeInfo.hour}:${eTimeInfo.min}`;
    timeInfo += ` ${eTimeInfo.hour}:${eTimeInfo.min}`;
    return timeInfo;
  },
  _getCycleInfoStr({ repeatCycle, repeatFlag, repeatEndDate }) {
    if (!repeatCycle) return ''; // 반복없으면 세팅 안함.
    repeatCycle = '매'; // 기획 문서 반영 : 날짜 지정 또는 말일 지정에 관계 없이 매월로 반복 설정 정보 표시
    const cycleIndex = repeatFlag - 1;
    const cycleType = ['주중', '일', '주', `${repeatCycle !== '매' ? '개월' : '월'}`, '년'];
    let cycleInfo = `${cycleIndex > 0 ? repeatCycle : ''}`; // 주중 or 매일,주,월,년 or N일,주,월,년
    cycleInfo += `${cycleType[cycleIndex]}`; // 주중/일/주/월/년
    cycleInfo += `${repeatCycle === '매' || cycleIndex === 0 ? '' : ' 마다'}`; // 주중/매일,주,월,년 or N일,주,월,년 마다
    cycleInfo += repeatEndDate ? ` / ${repeatEndDate} 종료` : '';
    return cycleInfo;
  },
  _getAlarmInfoStr({ alarmList }) {
    // TODO: time에다가 h,m 이런거 넣지말자.
    const timeType = new Map();
    timeType.set('m', '분').set('h', '시간').set('d', '일');

    const alarmListLength = alarmList.length;
    if (alarmListLength === 0) return '';
    let { alarmTime: timeStr, alarmType } = alarmList[0];
    const timeStrLength = timeStr.length;
    if (alarmType === '0') alarmType = '알림&메일';
    else if (alarmType === '1') alarmType = '알림';
    else alarmType = '메일';
    let str = timeStr.slice(0, timeStrLength - 1);
    if (str == 0) str = `일정 당시 ${alarmType}`;
    else str += `${timeType.get(timeStr[timeStrLength - 1])} 전 ${alarmType}`;

    if (alarmListLength > 1) str += ` 외 ${alarmListLength - 1}개`;

    return str;
  },
  _getUserList(users) {
    let original_users = users;

    const workspaceId = workspaceManager.getWorkspaceId(getRoomIdByUrl());
    const me = userManager.getLoginUserInfo();

    spaceAPI
      .getSpaceMemberListBrief(workspaceId)
      .then((result) => {
        let inputArr = result.data.dto.UserProfileList.map((ele) => {
          return {
            id: ele.USER_ID,
            thumbPhoto: ele.THUMB_PHOTO,
            icon: 'icon-none',
            text: ele.USER_NAME
          };
        });

        users.forEach((user, index) => {
          const userID = user.userID;

          // 스페이스룸에서 가져온 인원과 일치하면 thumbPhoto등 넣음
          const members = inputArr.filter((user) => user.id == userID);

          if (members.length === 1) {
            user.id = userID;
            user.thumbPhoto = members[0].thumbPhoto;
            user.text = members[0].text;
            user.icon = 'icon-none';
          } else if (members.length === 0) {
            user.id = userID;

            // 추가 조회
            let u = userManager.getUserInfo(userID);
            user.thumbPhoto = u.THUMB_PHOTO;
            user.text = u.USER_NAME;
            user.icon = 'icon-none';
          }
        });

        let my_user_index = -1;
        users.forEach((user, index) => {
          const userID = user.userID;
          // 본인인지 찾는 코드
          if (userID === me.userId) {
            my_user_index = index;
          }
        });

        if (my_user_index > -1) {
          // 만약 찾았다면 스왑
          let temp = users[my_user_index];
          users[my_user_index] = users[0];
          users[0] = temp;

          let users_0 = users.slice(0, 1);
          let users_except_me = users.slice(1, users.length);

          users_except_me.sort((a, b) => {
            if (a.text < b.text) {
              return -1;
            }
            if (a.text > b.text) {
              return 1;
            }
            return 0;
          });

          users_0.push(...users_except_me);
          users = users_0;
        } else {
          // 사용자 이름으로 정렬
          users.sort((a, b) => {
            if (a.text < b.text) {
              return -1;
            }
            if (a.text > b.text) {
              return 1;
            }
            return 0;
          });
        }

        calendarEventRepo.event.userList = users;
        calendarEventRepo.setValue('event.membersInfoStr', this._getMembersInfoStr());

        this._setLayoutVisible(calendarEventRepo.event);
      })
      .catch((err) => {
        console.log(err);

        original_users.map((user) => ({
          text: userManager.getUserNick(user.userID) ? userManager.getUserNick(user.userID) : userManager.getUserName(user.userID),
          icon: 'icon-none',
          thumbPhoto: userManager.getUserPhoto(user.userID),
          id: user.userID
        }));

        calendarEventRepo.event.userList = original_users;
        calendarEventRepo.setValue('event.membersInfoStr', this._getMembersInfoStr());

        this._setLayoutVisible(calendarEventRepo.event);
      });
  },
  _getMembersInfoStr() {
    const userListLength = calendarEventRepo.event.userList.length;
    let str = '';
    for (let i = 0; i < userListLength; i++) {
      // 최대 7명까지만 이름 표시
      if (i === 7) break;
      if (i !== 0) str += ', ';
      str += calendarEventRepo.event.userList[i].text;
    }
    if (userListLength > 7) str += `외 ${userListLength - 7}명`;
    return str;
  },
  /* set layout visiblity */
  _setLayoutVisible({ membersInfoStr, location, alarmInfoStr, description, fileList, taskType }) {
    const layoutIdList = new Map([
      ['member', membersInfoStr],
      ['location', location],
      ['alarm', alarmInfoStr],
      ['attachment', !!fileList.length],
      ['attachment-list', !!fileList.length],
      ['desc', description]
    ]);
    for (let key of layoutIdList.keys()) {
      tds(`cal-detail-${key}`).setVisible(layoutIdList.get(key) ? 'visible' : 'none');
      if (taskType === 'url') {
        tds(`cal-detail-edit`).setVisible('none');
        tds(`cal-detail-delete`).setVisible('none');
      } else {
        tds(`cal-detail-edit`).setVisible('visible');
        tds(`cal-detail-delete`).setVisible('visible');
      }
    }
  },
  /* append file item layout */
  _setFileListLayout({ fileList }) {
    if (!fileList.length) return;
    const _this = this;
    tds('cal-file-attch-cnt').setText(fileList.length);
    fileList.forEach(({ fileID: id, fileExtension: ext, fileName: name, fileSize: size }) => {
      tds('cal-detail-attachment-list').append(this._getFileItemTemplate({ id, ext, name, size }));
      const fileIconContainer = document.querySelector(`#file-item-${id} #fileIconContainer`);
      const fileIcon = document.querySelector(`#file-item-${id} #ImageView94`);
      const fileInfo = { fileID: id, fileExtension: ext, fileName: name };

      fileIconContainer.addEventListener(
        'mouseenter',
        function () {
          _this._HoveredDownloadBtn(this, fileIcon, fileInfo);
        },
        false
      );
      fileIconContainer.addEventListener(
        'mouseleave',
        function () {
          _this._revertFileItem(this, fileIcon);
        },
        false
      );
      // tds(`file-item-${id}`).addEventListener('click', (e) => {
      //   _this.onFildDownloadClick(id);
      // });
    });
  },
  _getFileItemTemplate({ id, ext, name, size }) {
    return `<top-linearLayout
    id="file-item-${id}"
    class="cal__file-item--detail"
    style="cursor:pointer"
    orientation="horizontal">
        <top-linearLayout
        id="fileIconContainer"
        layout-width="wrap_content"
        layout-height="wrap_content"
        class="cal__file-image-content">
            <top-imageView
            id="ImageView94"
            layout-horizontal-alignment="CENTER"
            src="${fileExtension.getInfo(ext).iconImage}">
            </top-imageView>
        </top-linearLayout>
        <top-linearLayout
        id="Widget87ff13459af436168ba03e0ec7f2db49"
        border-width="0"
        class="cal__file-item-content--detail"
        orientation="vertical">
            <top-textview
            id="file-name"
            class="cal__file-item-name"
            layout-horizontal-alignment="LEFT"
            layout-width="wrap_content"
            text="${name}">
            </top-textview>
            <top-textview
            id="file-size"
            class="cal__file-item-size"
            layout-horizontal-alignment="LEFT"
            layout-width="wrap_content"
            text="${size}">
            </top-textview>
        </top-linearLayout>
    </top-linearLayout>`;
  },
  _HoveredDownloadBtn(fileIconContainer, fileIcon, fileInfo) {
    fileIcon.style.display = 'none';

    let downloadBtn = fileIconContainer.querySelector('.cal__file-download');
    if (!downloadBtn) {
      downloadBtn = document.createElement('div');
      downloadBtn.classList.add('icon-work_download');
      downloadBtn.classList.add('cal__file-download');
      fileIconContainer.insertBefore(downloadBtn, fileIconContainer.firstElementChild);
    }
    downloadBtn.addEventListener('click', (e) => {
      const calendarFileSaveMenu = tds('calendarFileSaveMenu');
      calendarFileSaveMenu.setProperties({
        fileInfo: fileInfo
      });
      this._openContextManu(e.clientX, e.clientY);
    });
  },
  _openContextManu(X, Y) {
    let calendarFileSaveMenu = tds('calendarFileSaveMenu');
    calendarFileSaveMenu.open({
      clientY: Y,
      clientX: X
    });
    event.stopPropagation();
  },
  _revertFileItem(fileIconContainer, fileIcon) {
    fileIcon.style.display = '';

    let downloadBtn = fileIconContainer.querySelector('.cal__file-download');
    downloadBtn.remove();
  },
  onFildDownloadClick(id) {
    storageManager.DownloadFile(
      id,
      workspaceManager.getChannelList(workspaceManager.getWorkspaceId(getRoomIdByUrl()), TEESPACE_CHANNELS.TSCHEDULE.CHANNEL_TYPE),
      workspaceManager.getWorkspaceId(getRoomIdByUrl())
    );
  },
  /* edit, delete event handler */
  onEditClick() {
    calendarCommonData.isEventEditMode = true;
    tds('calendarEventViewDialog').close(true);
    tds('calendarEventEditDialog').open();
  },
  onDeleteEventClick() {
    tds('calendarEventViewDialog').close(true);
    TeeAlarm.open({
      title: '선택한 1개의 일정을 삭제하시겠습니까?',
      content: '삭제 후에는 복구할 수 없습니다.',
      buttons: [
        {
          text: '삭제',
          onClicked() {
            TeeAlarm.close();
            if (calendarEventRepo.event.cycleInfoStr == '') {
              calendarAPI.deleteEvent(calendarEventRepo.event.taskID, calendarEventRepo.event.startDate, 0, () => {
                const _this = Top.Controller.get('calendarMainLayoutLogic');
                calendarAPI.getEventList(_this._processEventData);
              });
            } else {
              tds('calendarRepeatedEventDeleteDialog').open();
            }
          }
        }
      ],
      cancelButtonText: '취소' // default 취소
    });
  },
  onOpenClick: function (event, widget) {
    tds('cal-detail-attach-close').setVisible('visible');
    tds('cal-detail-attach-open').setVisible('none');
    $('#cal-detail-attachment-list.top-linearlayout-root')[0].style.display = '';
  },
  onCloseClick: function (event, widget) {
    tds('cal-detail-attach-open').setVisible('visible');
    tds('cal-detail-attach-close').setVisible('none');
    $('#cal-detail-attachment-list.top-linearlayout-root')[0].style.display = 'none';
  }
});

Top.Controller.create('TeeSpaceLogic', {
  onFileSaveFromTDriveClick: function (event, widget) {
    const fileInfo = tds('calendarFileSaveMenu').getProperties('fileInfo');
    SAVE_TO_TDRIVE({
      fileId: fileInfo.fileID, // 저장하고자 하는 파일 id,
      fileName: fileInfo.fileName, // 기존 파일 이름,
      fileExt: fileInfo.fileExtension, // 저장하고자 하는 파일 확장자,
      successCallback: function (fileMeta) {
        console.log('성공'); //resolve(true);
        //pass global variable to office workarea
        // g_officeFileInfo = {
        //   fileId: fileMeta.storageFileInfo.file_id,
        //   fileName: fileMeta.file_name,
        //   fileExtension: fileMeta.file_extension,
        //   fileNameWithExtension: fileMeta.file_name + '.' + fileMeta.file_extension,
        //   whichChannel: 2 //T-Drive
        // };
        //call initWorkArea to reconnect to new file
        // officePeer.clearAllPeerConnection();
        // var ctrl = Top.Controller.get('officeWorkAreaLogic');
        // ctrl.initWorkArea(Top.Controller.get('officeMainLogic').app_type);
        // 각 앱에서 필요한 것
      },
      cancelCallback: function (fileMeta) {
        //resolve(false);
        //do nothing
        // 각 앱에서 필요한 것
      }
    });
  },
  onFileSaveFromLocalClick: function (event, widget) {
    const fileInfo = tds('calendarFileSaveMenu').getProperties('fileInfo');
    storageManager.DownloadFile(
      fileInfo.fileID,
      workspaceManager.getChannelList(workspaceManager.getWorkspaceId(getRoomIdByUrl()), TEESPACE_CHANNELS.TSCHEDULE.CHANNEL_TYPE),
      workspaceManager.getWorkspaceId(getRoomIdByUrl())
    );
  }
});

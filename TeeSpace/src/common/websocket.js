var websocket = null;
var default_websocket_conn_retry_interval = 5000; // ms

function ConnectWebSocket(loginKey) {
	let _url;
	
	if(window.location.protocol === "https:")
    	_url = "wss://" + mqttInfo.ssl_host;
	else
		_url = "ws://" + mqttInfo.host;

	//_url = _url + "/mqtt?USER_ID=" + loginKey + "&action=";
	_url = _url + "/mqtt?USER_ID=" + loginKey + "&action=&CONNECTION_ID=" + conid;
	
	websocket = new  WebSocket(_url);

	websocket.onopen = function(event) {	
		console.log('웹소켓이 연결되었습니다.  url = ' + _url);
		 $.ajax({
	            url: _workspace.url + "Users/LoginTime",	
	            type: 'PUT',
	            async: true,
	            dataType: 'json',
	            contentType: 'application/json; charset=utf-8',
	            data: JSON.stringify({
	                "dto": {
	                   USER_ID: userManager.getLoginUserId(),
	                   CONNECTION_ID  : conid
	                }
	            }),
	            success: function (ret) {					// workspaceId 값이 있지만 값이 없으면 []로 리턴
	                console.log('로그인 상태 변경 성공');
	            },
	            error: function (error) {
	            	console.log('로그인 상태 변경 실패');
	            }
	        });
	}
	websocket.onclose = function(event) {
		console.log('웹소켓이 끊어졌습니다.');
		websocket = null;
		setTimeout(initCmsWebSocket, default_websocket_conn_retry_interval);
	}
	websocket.onmessage = onmessage;
	websocket.onerror = function(error) {
		console.log('WebSocket Error = ' + error);
	}	
}

function onmessage(message) {
	if( !message || !message.data || message.data.length == 0 ) {
		console.error("[WWMS]의 데이터가 없습니다.");
		return;
	}

    var msg = JSON.parse(message.data);
    
    if( !msg.CH_TYPE ) {
    	console.error("[WWMS]에 CH_TYPE이 없습니다.");
    	return;
    }
    console.warn("[WWMS] message arrived");
    if( !!msg.SPACE_ID && msg.SPACE_ID.equals("MYSPACE"))
    	msg.SPACE_ID = workspaceManager.getMySpaceId();
    
    try {
    	TEESPACE_WEBSOCKET.executeWebSocketHandler(msg.CH_TYPE)(msg);
    } catch (err) {
        console.warn( err );
    }
}

function WebSocketClose() {
	if(websocket == null || websocket.readyState == websocket.CLOSED || websocket.readyState == websocket.CLOSING)
		return;
	websocket.onclose = function () {
		console.log('웹소켓이 끊어졌습니다.');
		websocket = null;
	}; // disable onclose handler first
    websocket.close();	
}


const TEESPACE_WEBSOCKET = ( function() {
	var wsHandler = {}, channelList = [];
	
	function _addWebSocketHandler(channelType, handler) {
		if(!channelType || typeof channelType !== "string") {
			console.log("잘못된 채널 타입 입니다. : " + channelType);
			return;
		}
		if( channelList.indexOf(channelType)  > -1 ) {
			console.log("이미 등록된 채널 타입 입니다. : " + channelType);
			return;			
		}
		if(!handler || typeof handler !== "function") {
			console.log("잘못된 웹소켓 핸들러 입니다. : " + channelType);
			return;
		}
		
		try {
			channelList.push(channelType);
			wsHandler[channelType] = handler;
		} catch(e) {
			
		}
	}
	
	function _removeWebSocketHandler(channelType) {
		try {
			let index = channelList.indexOf(channelType);
			if( index < 0 ) {
				console.log("channelType의 웹소켓 핸들러가 등록되어있지 않습니다.");
				return;
			}
			
			channelList.splice(index, 1);
			delete wsHandler[channelType];
		} catch(e) {
			
		}
	}


	return { 
		addWebSocketHandler: function( channelType, handler ) {
	        _addWebSocketHandler(channelType.toUpperCase(), handler);
	    },
	    removeWebSocketHandler: function( channelType ) {
	    	_removeWebSocketHandler(channelType.toUpperCase());
	    },
	    executeWebSocketHandler: function( channelType ) {
	    	return !!wsHandler[channelType.toUpperCase()] ? wsHandler[channelType.toUpperCase()] : function(msg) { console.log(channelType + " 핸들러가 등록되지 않았습니다."); };
	    },
	    getWebSocketHandlerChannelList : function() {
	    	return channelList;
	    }
	}
}) ();


window.name = "TeeSpace";
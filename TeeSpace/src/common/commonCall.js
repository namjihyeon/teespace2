/***  공통 함수  ***/

const rootUrl = window.location.pathname + "#!/";

//selectById
function tds(obj) {
    return Top.Dom.selectById(obj);
}

function getQueryStringByKey(key, url=window.location.href) {
    key = key.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + key + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return "";
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function getSubAppByUrl(url=window.location.href) {
    return getQueryStringByKey("sub", url);
}

function getLnbByUrl(url=window.location.href){
	const reg = /#!\/(.*?)\/.*/; // /#!\/rooms\/(.*?)\/.*/
    const result = reg.exec(url);            
    if (result && result.length > 1) return result[1];
    else return null;
} 

function getRoomIdByUrl(url=window.location.href) {	
	const reg = /#!\/\S\/(.*?)\/.*/; // /#!\/rooms\/(.*?)\/.*/
    const result = reg.exec(url);
    if (result && result.length > 1) return result[1];
    else return null;
} 

function getMainAppByUrl(url=window.location.hash) {		//window.location.href
//	const reg = /#!\/\S\/.*?\/(.*)/; // /#!\/rooms\/.*?\/(.*)/
//    const result = reg.exec(url);		
//    if (result && result.length > 1) return result[1].split("/")[0];
//    else return null;
	let result = url.split("#!/");
	result = url.split("?")[0].split("/")[3];
	if(result && result.length>1) return result;
	else return null;
}

function getMainAppFullUrl(url = location.hash){
	const reg = /#!\/\S\/!{0,1}\w{0,50}\/(.*)*/; // /#!\/rooms\/.*?\/(.*)/
	let result = url.split("?")[0];
	result = reg.exec(result);
	if(result && result.length>1) return result[1];
	else return null;
}

function getSubAppFullUrl(url = location.hash){
	let result = url;
	if(result.indexOf("?sub=")) return result.split("?sub=")[1].split("?")[0];
	else null;
}

function isNewWindow() {
    return window.location.href.indexOf("mini=true") !== -1;
}

//타임 스탬프
function getTimeStamp() {
    return new Date().getTime().toString();
}

//로컬 개발 환경
function isLocal(){
	return window.location.origin.match("localhost") || window.location.origin.match("192.168") || window.location.origin.match("127.0.0.1")? true: false;
}

//i18n
let i18n = (function(type) {
    let lang;
    let i18nConfig = {
        name: "ko_KR", //ip에 따라 default값 변경
        path: "external/config/i18n/",
        language: type,
        callback: ""
    };

    return {
        setLanguage: function(lang) {
            i18nConfig.language = lang;
        },

        load: function(app, func) {
            i18nConfig.name = app;
            i18nConfig.callback = func;
            Top.i18n.load(i18nConfig);
        }
    };
})();

function getFileSizeText(fileSize) {
    if (fileSize == undefined || typeof fileSize != "number") {
        return "0B";
    }

    let prefix = ["", "K", "M", "G", "T", "P", "E", "Z", "Y"];
    let idx = 0;
    while (fileSize >= 1024 && idx < prefix.length - 1) {
        fileSize = fileSize / 1024.0;
        idx++;
    }

    return String.format("{0}{1}B", fileSize.toFixed(1), prefix[idx]);
}

function getFileIconClass(fileExtension) {
    try {
        fileExtension = fileExtension.toLowerCase();

        if (MimeTypes.hasOwnProperty(fileExtension)) {
            let mimeTypeHeader = MimeTypes[fileExtension].split("/")[0];
            switch (mimeTypeHeader) {
                case "image":
                    return "icon-work_img_file";
                case "video":
                    return "icon-work_video_file";
            }
        }

        switch (fileExtension) {
            case "pdf":
                return "icon-work_pdf";
            case "doc":
            case "docx":
            case "toc":
                return "icon-work_toword";
            case "ppt":
            case "pptx":
            case "tpt":
                return "icon-work_topoint";
            case "xls":
            case "xlsx":
            case "tls":
            case "csv":
                return "icon-work_tocell";
            case "zip":
            case "tar":
            case "rar":
            case "tgz":
            case "war":
            case "alz":
            case "ace":
            case "arc":
            case "arj":
            case "b64":
            case "bh":
            case "bhx":
            case "bin":
            case "bz2":
            case "cab":
            case "ear":
            case "enc":
            case "gz":
            case "ha":
            case "hqx":
            case "ice":
            case "img":
            case "jar":
            case "lha":
            case "lzh":
            case "mim":
            case "pak":
            case "uue":
            case "xxe":
            case "zoo":
                return "icon-work_zip";
            case "txt":
                return "icon-work_txt";
            default:
                return "icon-work_document";
        }
    } catch (error) {
        return "icon-file";
    }
}

function getFileIconTooltip(fileExtension) {
    try {
        fileExtension = fileExtension.toLowerCase();

        if (MimeTypes.hasOwnProperty(fileExtension)) {
            let mimeTypeHeader = MimeTypes[fileExtension].split("/")[0];
            switch (mimeTypeHeader) {
                case "image":
                    return "이미지";
                case "video":
                    return "동영상";
            }
        }

        switch (fileExtension) {
            case "pdf":
                return "PDF";
            case "doc":
            case "docx":
            case "toc":
                return "ToWord";
            case "hwp":
                return "한글";
            case "ppt":
            case "pptx":
            case "tpt":
                return "ToPoint";
            case "xls":
            case "xlsx":
            case "tls":
            case "csv":
                return "ToCell";
            case "zip":
            case "tar":
            case "rar":
            case "tgz":
            case "war":
            case "alz":
            case "ace":
            case "arc":
            case "arj":
            case "b64":
            case "bh":
            case "bhx":
            case "bin":
            case "bz2":
            case "cab":
            case "ear":
            case "enc":
            case "gz":
            case "ha":
            case "hqx":
            case "ice":
            case "img":
            case "jar":
            case "lha":
            case "lzh":
            case "mim":
            case "pak":
            case "uue":
            case "xxe":
            case "zoo":
                return "압축 파일";
            default:
                return "기타";
        }
    } catch (error) {
        return "기타";
    }
}

function getFileDateFormat(time) {
    let curDate = new Date();
    let date = new Date(time);
    
    let str = String.empty;
    if(curDate.getFullYear() != date.getFullYear()) {
        str += String.format("{0}.", date.getFullYear())
    }
    
    if(curDate.getMonth() != date.getMonth() || curDate.getDate() != date.getDate()) {
        str += String.format("{0}.{1} ", ('0' + (date.getMonth()+1)).slice(-2), ('0' + date.getDate()).slice(-2))
    }
    
    let halfDayFormat = getHalfDayFormat(time);
    str += String.format("{0} {1}", (halfDayFormat.meridiem == 0) ? Top.i18n.t("value.m000147") : Top.i18n.t("value.m000148"), halfDayFormat.time);
    
    return str;
}

function getHalfDayFormat(time) {
    let meridiem = "";
    let date = new Date(time);
    let hour = date.getHours();

    // 1. Get meridiem
    meridiem =
        hour >= 12 ? 1 : 0;

    // 2. 시간을 0~23 시 -> 1~24시
    if (hour === 0) hour = 24;

    // 3. 1~24시 -> 오전/오후 1~12시
    if (hour > 12) hour -= 12;
    return {
        "meridiem": meridiem,
        "time": ("0" + hour).slice(-2) + ":" + ("0" + date.getMinutes()).slice(-2)
    }
}

(function(exports){
	const historyList = function(){

		let result;
		let lastUrl;
		let beforeUrl; 	//before lastUrl
		
		const historyAPI = {
				insert: function (main, sub) {
					 let spcId= "m"===getLnbByUrl()? "mail" : workspaceManager.getWorkspaceId(getRoomIdByUrl());					//메일 탭   , "mail"===getMainAppByUrl()?
					 spcId = "f"===getLnbByUrl()? "user" : spcId;																		//프렌즈 목록 탭
					 return axios.put( _workspace.url + "SpaceRoom/UserRoutingHistory", {
		        		 dto: {
		                    USER_ID			:	userManager.getLoginUserInfo().USER_LOGIN_ID,
		                    DEVICE_TYPE		:	"PC",					
		                    SPACE_ID		:	spcId,		//space아이디 아닌 경우
		                    LAST_URL		:	location.hash, 				
		                    APP_INFO		:	main + "/" + (sub? sub:"")	//appManager 기준,  
		                }
					 });
				},
				
				renew :  function(svcUrl, input, spc){
			        $.ajax({
			            url: svcUrl,
			            type: 'get',
			            async: false,
			            dataType: 'json',
			            contentType: 'application/json; charset=utf-8',
			            data: JSON.stringify({
			                "dto": spc? Object.assign(spc, input) : input
			            }),
			            success: function (ret) {
			            	result = ret? ret.dto : null
			            },
			            error: function (error) {}
			        });
				}
		};
		
		//갱신
		function renew(type="all", spcId){
			let svcUrl = _workspace.url + "SpaceRoom/UserRoutingHistory";
			let input =  {
					USER_ID 		:	userManager.getLoginUserInfo().USER_LOGIN_ID,
					DEVICE_TYPE		: 	"PC"
			};
			
			if("all" === type){ svcUrl += "List"; }
			else if("space"===type){ var spc = {SPACE_ID : workspaceManager.getWorkspaceId(spcId)}; }		//SPACE_ID : getRoomIdByUrl()}
			else return false;
			historyAPI.renew(svcUrl, input, spc);
		};
		
		//조회
		function get(type="all", spcId){
			renew(type.toLowerCase(), workspaceManager.getWorkspaceId(spcId));
			return result;
		};
				
		//추가
		function insert(){
			let subUrl = getSubAppByUrl();
			let main = appManager.getMainApp();
			let sub = appManager.getSubApp();	
			historyAPI.
				insert(main, sub).
					then((result) => {
						beforeUrl = lastUrl? lastUrl : null;
						lastUrl = location.hash;
					});
		}

		return {
			get,
			insert,
			getLast : () => lastUrl,
			getBefore : () => beforeUrl? beforeUrl : lastUrl
		}
	};
	
	exports.historyList = historyList();
})(this);

function noti_system(msg){
//	if( !msg.NOTI_TARGET || !msg.NOTI_TYPE )
//		return; 
	if(msg.USER_ID === userManager.getLoginUserId())
		return;
	
	let data;
//	if(msg.NOTI_TARGET){
		switch(msg.NOTI_TYPE) {
		case "space_invite" :							//강제 퇴장
			workspaceManager.update();							//유저그룹 목록 갱신
//			if(data[0]===routeManager.getWorkspaceId()){		//퇴장당한 그룹에 접속중인 경우 - 마이스페이스로 이동
//				Top.App.routeTo("/space/"+workspaceManager.getMySpaceId()+"/0/"+routeManager.getApp());
//			}
			spaceAPI.drawLnbSpace(msg.SPACE_ID)
			renderGnb()			
			NotificationManager.notify(msg, msg.NOTI_TYPE);
			break;
		case  "cs_exit" :													/* 수정 필요 !!!  */
			workspaceManager.update();	
			spaceAPI.drawLnbSpace(msg.SPACE_ID)			
//			let name =  getRoomIdByUrl()===workspaceManager.getMySpaceUrl() || getRoomIdByUrl()===workspaceManager.getMySpaceId()? userManager.getLoginUserName() + ' (나)' : (spaceInfo? spaceInfo.SPACE_NAME : ""); 
//			tds('AppGnbCurrentText').setText(name);
			renderGnb()
			break;
		case  "cs_ban" :
			workspaceManager.update();	
			spaceAPI.removeLnbSpace(msg.SPACE_ID)
			break;
		case  "cs_admin" :
			break;
		case  "cs_viceadmin" :
			break;
		case  "online" :
            FriendListModel.update(msg.USER_ID, {USER_STATUS: 'online'});
            //b2b
			$('span#fmStatusIcon_'+msg.USER_ID).css({'color':'#16AC66'})
            $('span#BtobspaceotherProfileStatusIcon_'+msg.USER_ID).css({'color':'#16AC66'})
            //b2c
            $('span#spaceotherProfileStatusIcon_'+msg.USER_ID).css({'color':'#16AC66'})
            $('span#fmStatusIcon_b2c_'+msg.USER_ID).css({'color':'#16AC66'})
            if ($('.mem-list #'+msg.USER_ID).length > 0) {
                $('.mem-list #'+msg.USER_ID).parent().children()[1].style.color = "#16AC66"
            }
			break;	
		case  "offline" :
            FriendListModel.update(msg.USER_ID, {USER_STATUS: 'offline'});
            //b2b
			$('span#fmStatusIcon_'+msg.USER_ID).css({'color':'#CCCCCC'})
            $('span#BtobspaceotherProfileStatusIcon_'+msg.USER_ID).css({'color':'#CCCCCC'})
            //b2c
            $('span#spaceotherProfileStatusIcon_'+msg.USER_ID).css({'color':'#CCCCCC'})
            $('span#fmStatusIcon_b2c_'+msg.USER_ID).css({'color':'#CCCCCC'})
            if ($('.mem-list #'+msg.USER_ID).length > 0) {
                $('.mem-list #'+msg.USER_ID).parent().children()[1].style.color = "#CCCCCC"
            }
			break;
		case "updatePro" :
			userManager.updateUserInfo(msg.USER_ID)
			FriendListModel.update(msg.USER_ID, {THUMB_PHOTO: msg.NOTI_MSG, lastUpdated: new Date()});
            InviteMemberDialog.updateUser({USER_ID: msg.USER_ID, THUMB_PHOTO: msg.NOTI_MSG});
            AddFriendsDialog.updateUser({USER_ID: msg.USER_ID, THUMB_PHOTO: msg.NOTI_MSG});
            workspaceManager.update();
            workspaceManager.getWorkspacePhoto(getRoomIdByUrl()).then((res)=>{
                tds('AppGnbCurrentImage').setSrc(res);
            });
		    if(!msg.NOTI_MSG){
		    	  //empty page
		    	  $('#BtobspaceotherProfileImageView.'+msg.USER_ID).children().attr('src',userManager.getUserDefaultPhotoURL(msg.USER_ID))
		    	  $('#spaceotherProfileImageView.'+msg.USER_ID).children().attr('src',userManager.getUserDefaultPhotoURL(msg.USER_ID))
		    	   //friend mangage popup
                  $('img#fmImageView.'+msg.USER_ID).attr('src',userManager.getUserDefaultPhotoURL(msg.USER_ID))
                  $('img#fmImageView_b2c.'+msg.USER_ID).attr('src',userManager.getUserDefaultPhotoURL(msg.USER_ID))
                $('.SPACE'+msg.USER_ID).attr('src',userManager.getUserDefaultPhotoURL(msg.USER_ID)) 
                $('#'+msg.USER_ID).attr('src',userManager.getUserDefaultPhotoURL(msg.USER_ID))
			}else{
				  $('#BtobspaceotherProfileImageView.'+msg.USER_ID).children().attr('src',userManager.getUserPhoto(msg.USER_ID,'medium', msg.NOTI_MSG))
				  $('#spaceotherProfileImageView.'+msg.USER_ID).children().attr('src',userManager.getUserPhoto(msg.USER_ID,'medium', msg.NOTI_MSG))
                  $('img#fmImageView.'+msg.USER_ID).attr('src',userManager.getUserPhoto(msg.USER_ID,'medium', msg.NOTI_MSG))
                $('img#fmImageView_b2c.'+msg.USER_ID).attr('src',userManager.getUserPhoto(msg.USER_ID,'medium', msg.NOTI_MSG))
                $('.SPACE'+msg.USER_ID).attr('src',userManager.getUserPhoto(msg.USER_ID,'medium', msg.NOTI_MSG)) 
                $('#'+msg.USER_ID).attr('src',userManager.getUserPhoto(msg.USER_ID,'medium', msg.NOTI_MSG))
            }
              
              ///talk.js의 profill 사진 업데이트 함수 호출
              talkRenderer.component.changeUserPhoto(
                  msg.USER_ID, 
                  userManager.getUserPhoto(
                      msg.USER_ID, 
                      "small", 
                      msg.NOTI_MSG
                    )
                )
			break;
		default:
			break;
		}
	
//	NotificationManager.notifySystem(msg, msg.NOTI_TYPE);
	
//}
}
TEESPACE_WEBSOCKET.addWebSocketHandler("SYSTEM", noti_system);  // 시스템 노티 핸들러 등록

const tsFocus = (function(){
	const get = () => document.hasFocus() && document.visibilityState === "visible"
	return{
		get
	}
})();
// 브라우저 포커스 잃을 시 이벤트
window.addEventListener('blur', function() {
});



// 브라우저 포커스 시 이벤트 등록
/*
window.addEventListener('focus', function() {
	let roomUrl = getRoomIdByUrl();
	if("s"===getLnbByUrl() && "talk" === getMainAppByUrl()) {
		
		let startMsg, endMsg, i, msgId;
        let msgList = talkData.messageList;
        let msgListLength = msgList.length;
        if(msgListLength ==0) return;
        for(i=msgListLength-1;i>=0;i--){
            msgId = msgList[i]['MSG_ID'];
            if(!msgId) continue;
            endMsg = msgId; break;
        }
		talkServer2.ttalk_readMessage(talkData.talkId, endMsg, userManager.getLoginUserId()); 
		updateCommonUnread(roomUrl);
	}
});*/

function updateCommonUnread(roomUrl = getRoomIdByUrl(), userId){
	if(userId && userId !== userManager.getLoginUserId()) return false;
	let roomId = workspaceManager.getWorkspaceId(roomUrl);
	NotificationManager.removeTitleBadge(workspaceManager.getWorkspaceId(getRoomIdByUrl()));  // 타이틀 뱃지 삭제 호출

	//unread
	spaceAPI.setUnreadMsg(roomId, true);
}
Number.prototype.pad = function (width) {
	width = typeof width !== 'undefined' ? width : 0;
	return (function (n) {
		return String(n).length < width ? new Array(width - String(n).length + 1).join('0') + String(n) : String(n);
	})(this.valueOf());
};

String.prototype.rnd = function (len) {
	if (typeof len === 'undefined') {
		return String.prototype.genUUID();
	} else if (typeof len !== 'number') {
		return;
	}
	var characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
	var result = '';
	for (var i = 0; i < len; i++) {
		result += characters.charAt(Math.floor(Math.random() * characters.length));
	}
	return result;
};
Number.prototype.rnd = function () {
	if (arguments.length === 2) {
		return Math.floor(Math.random() * (arguments[1] - arguments[0])) + arguments[0];
	} else if (arguments.length === 1) {
		return Math.floor(Math.random() * arguments[0]);
	} else {
		return Math.random();
	}
};

Array.prototype.rnd = function () {
	if (arguments.length === 0) {
		return this[Math.floor(Math.random() * this.length)];
	} else if (arguments.length > 0 && arguments[0]) {
		let list = [];
		for (var i = 0; i < arguments[0]; i++) {
			list.push(this[Math.floor(Math.random() * this.length)]);
		}
		return list;
	} else {
		return undefined;
	}
};

String.prototype.genUUID = function (prefix = '') {
	// method 1
	return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, (c) =>
		(c ^ (crypto.getRandomValues(new Uint8Array(1))[0] & (15 >> (c / 4)))).toString(16)
	);
	// method 2
	//	return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);return v.toString(16);});
	// method 3
	//	let result = [];
	//	let characters = '0123456789abcdef';
	//	let charactersLength = characters.length;
	//	let hyphenLocation = [8, 13, 18, 23];
	//	let uuidLength = 36;
	//	let k = 0;
	//	for (let i = 0; i < uuidLength; i++) {
	//		if (hyphenLocation.includes(i)) {
	//			result.push('-');
	//		} else {
	//			if (prefix[k]) {
	//				result.push(prefix[k]);
	//				k++;
	//			} else {
	//				result.push(characters.charAt(Math.floor(Math.random() * charactersLength)));
	//			}
	//		}
	//	}
	//
	//	return result.join('');
};

Number.prototype.toReadableSize = function (precision, spacer) {
	let n = parseInt(this);
	if (isNaN(n)) return '';
	precision = typeof precision === 'number' ? precision : 2;
	spacer = typeof spacer === 'string' ? spacer : '';
	const units = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
	var i = 0;
	while (i < units.length - 1 && n >= 1024) {
		n = n / 1024;
		i++;
	}
	// Byte has no decimal point.
	// Return of toFixed() is String.
	n = i === 0 ? n.toString() : n.toFixed(precision);
	return n + spacer + units[i];
};
String.prototype.toReadableSize = function (precision, spacer) {
	return parseInt(this).toReadable(precision, spacer);
};
String.prototype.toReadableDate = function (option) {
	// 	2019-07-12 11:22:33 -> 2019.07.12 오전 11:22

	const [date, time] = this.split(' ');
	const [year, month, day] = date.split('-');
	const [hour, minute, second] = time.split(':');
	var dateArr = [];
	var now = new Date();
	if (option === 'full') {
		return `${year}.${month}.${day} ${hour}:${minute}`;
	} else {
		if (Number(year) !== now.getFullYear()) {
			dateArr.push(year);
			dateArr.push(month);
			dateArr.push(day);
		} else if (!(Number(month) === now.getMonth() + 1 && Number(day) === now.getDate())) {
			dateArr.push(month);
			dateArr.push(day);
		} else {
			// nothing
		}

		return [dateArr.join('.'), `${hour < 12 ? '오전' : '오후'} ${hour < 13 ? hour : hour - 12}:${minute}`].join(
			' '
		);
	}
};

String.prototype.splitIntoNameAndExtension = function (fileNameWithExtension) {
	let target = typeof fileNameWithExtension === 'string' ? fileNameWithExtension : this;
	let chunk = target.split('.');
	let name = chunk.splice(0, chunk.length - 2 > 0 ? chunk.length - 1 : 1).join('.');
	let extension = chunk.join('');
	//	console.info(name, extension)
	return [name, extension];
};

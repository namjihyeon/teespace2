const storageManager = {
		UploadFile : function(file, inDto, SvcGroup, SvcName, successCallback, errorCallback) {
			if(file.size > 209715200){
				TeeAlarm.open({title: '용량 초과', content: '업로드 할 파일은 200MB를 넘을 수 없습니다.'});
				return;
			}
			Top.Ajax.execute({
				url : _workspace.url + SvcGroup + "/" + SvcName,
				type : "POST",
				dataType : "json",
				async : true,
				cache : false,
				data : JSON.stringify(inDto),
				contentType: "application/json; charset=utf-8",
				success : function(ret, xhr, status) {
					if( !ret.dto || ( !ret.dto.file_id || !ret.dto.ch_id) )
						{
							console.error("ret.dto.file_id or ret.dto.ch_id is missing ");
							return;
						}
					
					var workspaceID;
					
					if("None".equals(ret.dto.ch_id)){
						workspaceID = "Support";
					}else {
//						workspaceID = getRoomIdByUrl();
//						if(!workspaceID)
							workspaceID = workspaceManager.getWorkspaceId(getRoomIdByUrl());
					}
				
					
					Top.Ajax.execute({
					       url : _workspace.url + "Storage/StorageFile?action=Create" + "&fileID=" + ret.dto.file_id
					       + "&workspaceID=" + workspaceID + "&channelID=" + ret.dto.ch_id
					       + "&userID=" + userManager.getLoginUserId(),
					       type : "POST",
					       async : true,
					       cache : false,
					       data : file,
					       contentType : false,
					       crossDomain : true,
					       xhrFields : {
					           withCredentials: true
					       },
					       success : function(data) {
					    	   let msg = JSON.parse(data);
					    	   if( "Success".equals(msg.dto.resultMsg) ) {
					    		   if(typeof successCallback == "function") {
					    			   successCallback(msg);
					    		   }
					           }
					    	   else {
					    		   if(typeof errorCallback == "function") {
						               errorCallback(msg);
						           }
					    	   }
					       },
					       error : function(data) {
					           if(typeof errorCallback == "function") {
					               errorCallback(data);
					           }
					       }
					   })
				},
				complete : function(ret, xhr, status) {
				},
				error : function(data) {
			           if(typeof errorCallback == "function") {
			               errorCallback(data);
			           }	
				}
			})
},

//DownloadFile: 실제 파일 다운로드 
		DownloadFile : function(fileID, chID, wsID, successCallback, errorCallback) {
			
			var userDevice = "";
			
		    const varUA = navigator.userAgent.toLowerCase();
		    if ( varUA.indexOf("iphone") > -1||varUA.indexOf("ipad") > -1||varUA.indexOf("ipod") > -1 )
		        userDevice = "ios";
		    
		    var workspaceID;	
			if("None".equals(chID)){
				workspaceID = "Support";
			}else {
//				workspaceID = getRoomIdByUrl();
				if(wsID == null || wsID == undefined)
					workspaceID = workspaceManager.getWorkspaceId(getRoomIdByUrl());
				else
					workspaceID = wsID;
			}		
			
			var a = document.createElement("a");
		    document.body.appendChild(a);
		 
		    //fileMetaCheck 없이 바로 download
		    
		    a.style = "display: none";
		    a.href = _workspace.url + "Storage/StorageFile?action=Download" + "&fileID=" + fileID + "&workspaceID=" + workspaceID + "&channelID=" + chID + "&userID=" + userManager.getLoginUserId();
		    a.download = "download"//_workspace.url + "Storage/StorageFile?action=Download" + "&fileID=" + fileID + "&workspaceID=" + workspaceID + "&channelID=" + chID + "&userID=" + userManager.getLoginUserId();
		    a.click();
		    document.body.removeChild(a);
		    
//		    Top.Ajax.execute({
//				type : "GET",
//				url : _workspace.url + "Storage/StorageMeta" + "?fileID=" + fileID + "&workspaceID=" + workspaceID + "&channelID=" + chID + "&userID=" + userManager.getLoginUserId(),
//				dataType : "json",
//				contentType: "application/json; charset=utf-8",
//			    xhrFields: {withCredentials: true},
//				crossDomain : true,				
//				success : function(data) {
//					
//					var downloadUrl = _workspace.url + "Storage/StorageFile?action=Download" + "&fileID=" + fileID + "&workspaceID=" + workspaceID + "&channelID=" + chID + "&userID=" + userManager.getLoginUserId();
//
//					if( "Success".equals(data.dto.resultMsg) ) {
//						if("ios".equals(userDevice)){
//							LocalService.acall({
//								service:'tos.service.WebBrowser.loadUrl',
//								data: downloadUrl
//							});
//							
//						}
//						else {
//							a.href = downloadUrl;
//							//a.download = "download"
//							a.click();
//							document.body.removeChild(a);
//						}
//						if(typeof successCallback == "function") {
//							successCallback(data);
//						}
//					}
//					else {
//						if(typeof errorCallback == "function") {
//							errorCallback(data);
//						}
//					}
//				},
//				error : function(data) {
//					if(typeof errorCallback == "function") {
//						errorCallback(data);
//					}
//				}
//			});   
		    
		},

		DownloadFiles : function(fileIDList, chID, wsID, successCallback, errorCallback, isZipFile) {

			var userDevice = "";

			const varUA = navigator.userAgent.toLowerCase();
			if ( varUA.indexOf("iphone") > -1||varUA.indexOf("ipad") > -1||varUA.indexOf("ipod") > -1 )
				userDevice = "ios";

			var workspaceID;	
			if("None".equals(chID)){
				workspaceID = "Support";
			}else {
				if(wsID == null || wsID == undefined)
					workspaceID = workspaceManager.getWorkspaceId(getRoomIdByUrl());
				else
					workspaceID = wsID;
			}		

			var a = document.createElement("a");
			
			var fileCnt = fileIDList.length;

			if(fileCnt >= 10 || isZipFile == true){
				var fileListString = "";

				for(var i=0; i < fileCnt ; i++){
					tmpFileID = fileIDList[i];
					fileListString = fileListString.concat(tmpFileID, ',');
				}
				fileListString = fileListString.slice(0, -1);

				var downloadUrl = _workspace.url + "Storage/StorageFileMulti?action=Get" + "&fileID=" + fileListString + "&workspaceID=" + workspaceID + "&channelID=" + chID + "&userID=" + userManager.getLoginUserId();
				
				if("ios".equals(userDevice)){
					LocalService.acall({
						service:'tos.service.WebBrowser.loadUrl',
						data: downloadUrl
					});
				}
				else {
					a.href = downloadUrl;
					a.download = "download";
					a.click();
					//window.open(downloadUrl);
					document.body.removeChild(a);
				}
			}

			else {
				for(var i=0; i<fileCnt; i++){
					this.DownloadFile(fileIDList[i], chID, wsID, successCallback, errorCallback);
				}
			}
		},
				
//GetFileUrl: 파일 다운로드 URI Get
		GetFileUrl : function(fileID, chID, wsID) {
			var workspaceID;
			
			if("None".equals(chID)){
				workspaceID = "Support";
			}else {
				if(wsID == null || wsID == undefined)
					workspaceID = workspaceManager.getWorkspaceId(getRoomIdByUrl());
				else
					workspaceID = wsID;
			}
			
			var storageUrl = _workspace.url + "Storage/StorageFile?action=Download" + "&fileID=" + fileID + "&workspaceID=" + workspaceID + "&channelID=" + chID + "&userID=" + userManager.getLoginUserId();
			return storageUrl;
		},

//getDownloadFileInfo: 파일 blob 다운로드
		getDownloadFileInfo : function(targetFileId, chID, wsID, successCallback, errorCallback) {	 
		    var _targetFileId = targetFileId;
		    
		    var workspaceID;
			
			if("None".equals(chID)){
				workspaceID = "Support";
			}else {
//				workspaceID = getRoomIdByUrl();
//				if(!workspaceID)
				if(wsID == null || wsID == undefined)
					workspaceID = workspaceManager.getWorkspaceId(getRoomIdByUrl());
				else
					workspaceID = wsID;
			}
			
		    var _successCallback = function(response) {
	            if(successCallback == undefined || (typeof successCallback != "function")) {
	                return;
	            }
	            
		        if(response.resultMsg != "Success") {
		            //response.fileMetaList.push({ "file_id" : _targetFileId })
		            errorCallback(response);
		            return;
		        }
		        
		        let fileExt = Top.Util.getFileExtension(response.files[0].filename);
		        let data = atob(response.files[0].contents);
		        let result = "";
		        if((fileExt.toLowerCase() == "txt" || fileExt.toLowerCase() == "csv" ? false : true)) {
		            let bytes = new Uint8Array(data.length);
		            for(var i=0 ; i<data.length ; i++) {
		                bytes[i] = data.charCodeAt(i);
		            }
		            
		            result = bytes;
		        } else {
		            result = decodeURIComponent(data);
		        }
		        
		        switch(fileExt.toLowerCase()) {
		            case "csv":    case "txt":
		                result = "\ufeff" + result; //한글이 들어가는 경우 깨지기 때문에 한글이 깨지지 않기 위해 헤더를 붙임.
		                break;
		        }
		        
		        let contentType = response.resultDetail;
		        let blob = new Blob([result], { type: contentType }); //mime타입 넣어야함.
		        let returnVal = {
		            result : "Ok",
		            url: window.URL.createObjectURL(blob),
		            fileName : response.files[0].filename,
		            blobData : blob
		        }
		        
	            successCallback(returnVal);	            
		    };
		    
		    var _errorCallback = function(data) {
		        if(errorCallback != undefined && (typeof errorCallback == "function")) {
		            errorCallback(data);
	            }
		    };
		    
		    let parameter = String.format("?file_id={0}", _targetFileId);
	        
	        Top.Ajax.execute({
				url : _workspace.url + "Storage/StorageFile?fileID=" + targetFileId + "&workspaceID=" + workspaceID + "&channelID=" + chID + "&userID=" + userManager.getLoginUserId(),
				type : "GET",
				async : true,
				cache : false,
				headers: {
	    			"ProObjectWebFileTransfer":"true"
				},
				contentType: "application/json; charset=utf-8",
				success : function(data) { 		    
	                if(successCallback != undefined && (typeof successCallback == "function")) {
	                 
	                        if(typeof data == "string") {
	                            data = JSON.parse(data);
	                        }
	                        
	                        if(data.exception != undefined) {
	                            if(data.exception.name == "DownloadFileLimitExceedException") {
	                                outputdata = {
	                                    result : "FAIL",
	                                    message : data.exception.message
	                                };
	                            }
	                            _errorCallback(outputdata);
	                            return;
	                        }
	                        
	                        outputdata = data.dto;
	                        outputdata.files = data.files;
	                        
	                    if(outputdata.resultMsg != "Success") {
	                        _errorCallback(outputdata);
	                    } else {
	                        _successCallback(outputdata);                        
	                    }
	                }
	    	    },
	    		error : function(data) {               
	                if(errorCallback != undefined && (typeof errorCallback == "function")) {
	                    
	                    _errorCallback(data); 
	                }
	    	    }
	        })
	        
		},
		
//CopyFile : 파일 복사		
		CopyFile : function(type, targetFileID, chID, targetNewName, targetNewExt, userContext1, userContext2, userContext3, successCallback, errorCallback,  wsID) {
			if(!chID){
				console.log("채널 아이디가 필요합니다.");
				return;
			}
			
			if(!targetNewName || !targetNewExt){
				console.log("파일명 또는 파일 확장자가 필요합니다.");
				return;
			}
			
			
			if(type != "Deep" && type != "Shallow" ){
				console.log("Deep이거나 Shallow여야 합니다");
				return;
			}
			
			var workspaceID;
			
			if("None".equals(chID)){
				workspaceID = "Support";
			}else {
//				workspaceID = getRoomIdByUrl();
//				if(!workspaceID)
				if(wsID == null || wsID == undefined)
					workspaceID = workspaceManager.getWorkspaceId(getRoomIdByUrl());
				else
					workspaceID = wsID;
			}
			
			var inputDTO = {
					"dto": {
				        "workspace_id": workspaceID,
				        "channel_id": chID,
				        "storageFileInfo": {
				            "user_id": userManager.getLoginUserId(),
				            "file_last_update_id": userManager.getLoginUserId(),
				            "file_id": targetFileID,
				            "file_name": targetNewName,
				            "file_extension": targetNewExt,
				            "user_context_1": userContext1,
				            "user_context_2": userContext2,
				            "user_context_3": userContext3
				        }
				    }
			}
			   
		    Top.Ajax.execute({
		    	url : _workspace.url + "Storage/StorageFile?action=Copy&Type=" + type,
		    	type : "POST",
		    	async : "true",
		    	dataType : "json",
		    	cache : "false",
		    	data : JSON.stringify(inputDTO),
		    	contentType: "application/json; charset=utf-8",
		    	crossDomain : true,
		    	xhrFields : {
	    		    withCredentials: true
	    		},
	    		
	    		success : function(data) {
	    			
			    	   if( "Success".equals(data.dto.resultMsg) ) {
			    		   if(typeof successCallback == "function") {
			    			   successCallback(data);
			    		   }

			           }
			    	   else {
			    		   if(typeof errorCallback == "function") {
				               errorCallback(data);
				           }
			    	   }
			       },
			    
			    complete : function(data) {
			    	
			    },   
			       
			    error : function(data) {
			           if(typeof errorCallback == "function") {
			               errorCallback(data);
			           }
			    }
				
		    })
			
		}
		
		
//	test : function() {
//
//		var fileChooser = Top.Device.FileChooser.create({
//			onBeforeLoad : function() {
//		
//				// 앱 별로 필요한 전처리
//			
//				for(var i=0; i<this.file.length; i++) {
//					storageManager.UploadFile(this.file[0], null,"Storage", "StorageMeta",function() {}, function() {})
//				}
//			
//				return false;
//			}
//		});
//	
//		fileChooser.show();
//
//	}

}
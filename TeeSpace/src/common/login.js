var browsercode = "";
var pattern_num = /[0-9]/;
var pattern_engs = /[a-z]/;
var pattern_engb = /[A-Z]/;
var pattern_spc = /[~!@#$%^&*()+|<>?:{}.,\\=\\]/;
var pattern_spc2 = /[~!@#$%^&*()_+|<>?:{}.,\\=\\-]/;
var pattern_kor = /[ㄱ-ㅎ|ㅏ-ㅣ|가-힣]/; 
var pattern_blank = /[\s]/;
var jobs = ["전문직(의사,변호사,회계사,종교인 등)","교직(교사,교수,유치원 교사,학원 강사 등)","사무직(회사원,은행원 등)","서비스직(유통 판매원, 상담원 등)","생산/기능직(경비원, 자동자 정비사 등)","농업/축산/수산업", "자영업","학생(초등학생~대학원생,재수생 등)","주부", "무직", "기타"];
var area0 = ["서울특별시","인천광역시","대전광역시","광주광역시","대구광역시","울산광역시","부산광역시","세종특별자치시","경기도","강원도","충청북도","충청남도","전라북도","전라남도","경상북도","경상남도","제주특별자치도"];
var area1 = ["강남구","강동구","강북구","강서구","관악구","광진구","구로구","금천구","노원구","도봉구","동대문구","동작구","마포구","서대문구","서초구","성동구","성북구","송파구","양천구","영등포구","용산구","은평구","종로구","중구","중랑구"];
 var area2 = ["계양구","남구","남동구","동구","부평구","서구","연수구","중구","강화군","옹진군"];
 var area3 = ["대덕구","동구","서구","유성구","중구"];
 var area4 = ["광산구","남구","동구",     "북구","서구"];
 var area5 = ["남구","달서구","동구","북구","서구","수성구","중구","달성군"];
 var area6 = ["남구","동구","북구","중구","울주군"];
 var area7 = ["강서구","금정구","남구","동구","동래구","부산진구","북구","사상구","사하구","서구","수영구","연제구","영도구","중구","해운대구","기장군"];
 var area8 = []
 var area9 = ["고양시","과천시","광명시","광주시","구리시","군포시","김포시","남양주시","동두천시","부천시","성남시","수원시","시흥시","안산시","안성시","안양시","양주시","오산시","용인시","의왕시","의정부시","이천시","파주시","평택시","포천시","하남시","화성시","가평군","양평군","여주군","연천군"];
 var area10 = ["강릉시","동해시","삼척시","속초시","원주시","춘천시","태백시","고성군","양구군","양양군","영월군","인제군","정선군","철원군","평창군","홍천군","화천군","횡성군"];
 var area11 = ["제천시","청주시","충주시","괴산군","단양군","보은군","영동군","옥천군","음성군","증평군","진천군","청원군"];
 var area12 = ["계룡시","공주시","논산시","보령시","서산시","아산시","천안시","금산군","당진군","부여군","서천군","연기군","예산군","청양군","태안군","홍성군"];
 var area13 = ["군산시","김제시","남원시","익산시","전주시","정읍시","고창군","무주군","부안군","순창군","완주군","임실군","장수군","진안군"];
 var area14 = ["광양시","나주시","목포시","순천시","여수시","강진군","고흥군","곡성군","구례군","담양군","무안군","보성군","신안군","영광군","영암군","완도군","장성군","장흥군","진도군","함평군","해남군","화순군"];
 var area15 = ["경산시","경주시","구미시","김천시","문경시","상주시","안동시","영주시","영천시","포항시","고령군","군위군","봉화군","성주군","영덕군","영양군","예천군","울릉군","울진군","의성군","청도군","청송군","칠곡군"];
 var area16 = ["거제시","김해시","마산시","밀양시","사천시","양산시","진주시","진해시","창원시","통영시","거창군","고성군","남해군","산청군","의령군","창녕군","하동군","함안군","함양군","합천군"];
 var area17 = ["서귀포시","제주시","남제주군","북제주군"];
var conid;
var signupDomains = ["직접입력","naver.com","daum.net","nate.com","outlook.kr","gmail.com","yahoo.com"]


let ivtCode;
let tutoFlag= true;

 function setCookie(name, value, expires, path, domain, secure){
	 var time = new Date();
	 expires = expires ? time.setDate(time.getDate() + expires) : '';
	 path = path ? '; path='+path : '';
	 domain = domain ? '; domain=' + domain : '';
	 secure = secure ? '; secure' : '';
	 document.cookie=name+'='+escape(value)+(expires?'; expires='+time.toGMTString():'')+path+domain+secure;
}
 
function deleteCookie(cookieName){
	setCookie(cookieName, '', '-1','','','');
}

function getCookie(name){
	name = new RegExp(name + '=([^;]*)');
	return name.test(document.cookie) ? unescape(RegExp.$1) : '';
}

function checkMemGrade(memGrade) {	
	if(memGrade == "admin"){
		Top.Controller.get("loginLayoutLogic").mem_grade = "COM0012"
	}else if(memGrade == "operator"){
		Top.Controller.get("loginLayoutLogic").mem_grade = "COM0013"
	}else if(memGrade == "superadmin"){
		Top.Controller.get("loginLayoutLogic").mem_grade = "COM0014"
	}else{
		Top.Controller.get("loginLayoutLogic").mem_grade = "COM0011"
	}
		
}


function SHA256(s){
    
    var chrsz   = 8;
    var hexcase = 0;
  
    function safe_add (x, y) {
        var lsw = (x & 0xFFFF) + (y & 0xFFFF);
        var msw = (x >> 16) + (y >> 16) + (lsw >> 16);
        return (msw << 16) | (lsw & 0xFFFF);
    }
  
    function S (X, n) { return ( X >>> n ) | (X << (32 - n)); }
    function R (X, n) { return ( X >>> n ); }
    function Ch(x, y, z) { return ((x & y) ^ ((~x) & z)); }
    function Maj(x, y, z) { return ((x & y) ^ (x & z) ^ (y & z)); }
    function Sigma0256(x) { return (S(x, 2) ^ S(x, 13) ^ S(x, 22)); }
    function Sigma1256(x) { return (S(x, 6) ^ S(x, 11) ^ S(x, 25)); }
    function Gamma0256(x) { return (S(x, 7) ^ S(x, 18) ^ R(x, 3)); }
    function Gamma1256(x) { return (S(x, 17) ^ S(x, 19) ^ R(x, 10)); }
  
    function core_sha256 (m, l) {
         
        var K = new Array(0x428A2F98, 0x71374491, 0xB5C0FBCF, 0xE9B5DBA5, 0x3956C25B, 0x59F111F1,
            0x923F82A4, 0xAB1C5ED5, 0xD807AA98, 0x12835B01, 0x243185BE, 0x550C7DC3,
            0x72BE5D74, 0x80DEB1FE, 0x9BDC06A7, 0xC19BF174, 0xE49B69C1, 0xEFBE4786,
            0xFC19DC6, 0x240CA1CC, 0x2DE92C6F, 0x4A7484AA, 0x5CB0A9DC, 0x76F988DA,
            0x983E5152, 0xA831C66D, 0xB00327C8, 0xBF597FC7, 0xC6E00BF3, 0xD5A79147,
            0x6CA6351, 0x14292967, 0x27B70A85, 0x2E1B2138, 0x4D2C6DFC, 0x53380D13,
            0x650A7354, 0x766A0ABB, 0x81C2C92E, 0x92722C85, 0xA2BFE8A1, 0xA81A664B,
            0xC24B8B70, 0xC76C51A3, 0xD192E819, 0xD6990624, 0xF40E3585, 0x106AA070,
            0x19A4C116, 0x1E376C08, 0x2748774C, 0x34B0BCB5, 0x391C0CB3, 0x4ED8AA4A,
            0x5B9CCA4F, 0x682E6FF3, 0x748F82EE, 0x78A5636F, 0x84C87814, 0x8CC70208,
            0x90BEFFFA, 0xA4506CEB, 0xBEF9A3F7, 0xC67178F2);

        var HASH = new Array(0x6A09E667, 0xBB67AE85, 0x3C6EF372, 0xA54FF53A, 0x510E527F, 
                   0x9B05688C, 0x1F83D9AB, 0x5BE0CD19);

        var W = new Array(64);
        var a, b, c, d, e, f, g, h, i, j;
        var T1, T2;
  
        m[l >> 5] |= 0x80 << (24 - l % 32);
        m[((l + 64 >> 9) << 4) + 15] = l;
  
        for ( var i = 0; i<m.length; i+=16 ) {
            a = HASH[0];
            b = HASH[1];
            c = HASH[2];
            d = HASH[3];
            e = HASH[4];
            f = HASH[5];
            g = HASH[6];
            h = HASH[7];
  
            for ( var j = 0; j<64; j++) {
                if (j < 16) W[j] = m[j + i];
                else W[j] = safe_add(safe_add(safe_add(Gamma1256(W[j - 2]), W[j - 7]), Gamma0256(W[j - 15])), W[j - 16]);
  
                T1 = safe_add(safe_add(safe_add(safe_add(h, Sigma1256(e)), Ch(e, f, g)), K[j]), W[j]);
                T2 = safe_add(Sigma0256(a), Maj(a, b, c));
  
                h = g;
                g = f;
                f = e;
                e = safe_add(d, T1);
                d = c;
                c = b;
                b = a;
                a = safe_add(T1, T2);
            }
  
            HASH[0] = safe_add(a, HASH[0]);
            HASH[1] = safe_add(b, HASH[1]);
            HASH[2] = safe_add(c, HASH[2]);
            HASH[3] = safe_add(d, HASH[3]);
            HASH[4] = safe_add(e, HASH[4]);
            HASH[5] = safe_add(f, HASH[5]);
            HASH[6] = safe_add(g, HASH[6]);
            HASH[7] = safe_add(h, HASH[7]);
        }
        return HASH;
    }
  
    function str2binb (str) {
        var bin = Array();
        var mask = (1 << chrsz) - 1;
        for(var i = 0; i < str.length * chrsz; i += chrsz) {
            bin[i>>5] |= (str.charCodeAt(i / chrsz) & mask) << (24 - i%32);
        }
        return bin;
    }
  
    function Utf8Encode(string) {
        string = string.replace(/\r\n/g,"\n");
        var utftext = "";
  
        for (var n = 0; n < string.length; n++) {
  
            var c = string.charCodeAt(n);
  
            if (c < 128) {
                utftext += String.fromCharCode(c);
            }
            else if((c > 127) && (c < 2048)) {
                utftext += String.fromCharCode((c >> 6) | 192);
                utftext += String.fromCharCode((c & 63) | 128);
            }
            else {
                utftext += String.fromCharCode((c >> 12) | 224);
                utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                utftext += String.fromCharCode((c & 63) | 128);
            }
  
        }
  
        return utftext;
    }
  
    function binb2hex (binarray) {
        var hex_tab = hexcase ? "0123456789ABCDEF" : "0123456789abcdef";
        var str = "";
        for(var i = 0; i < binarray.length * 4; i++) {
            str += hex_tab.charAt((binarray[i>>2] >> ((3 - i%4)*8+4)) & 0xF) +
            hex_tab.charAt((binarray[i>>2] >> ((3 - i%4)*8  )) & 0xF);
        }
        return str;
    }
  
    s = Utf8Encode(s);
    return binb2hex(core_sha256(str2binb(s), s.length * chrsz));
  
}

function showLoadingBar2() { 
	var maskHeight = $(document).height(); 
	var maskWidth = window.document.body.clientWidth; 
	var mask = "<div id='mask' style='position:absolute; z-index:9000; background-color:#FFFFFF; display:none; left:0; top:0;'></div>"; 
	var loadingImg = ''; 
	loadingImg += "<div id='loadingImg'; style='display:flex; justify-content : center; margin-top : 5rem; z-index:100000;' >"; 
	loadingImg += "<img src='res/user/splash_teeki_taka.gif'; style='width: 25rem; height: 25rem;' />"; 
	loadingImg += "</div>"; 
	var loadingMsg = '';
	loadingMsg += "<div id='loadingMsg'; style='position:absolute; left:45.5%; top:55%; display:none; z-index:100000;'>"; 
	loadingMsg += "<top-textview id='testview131' layout-width='140px' layout-height='27px' border-width='0px' text='로딩 중...' style='line-height:normal; font-size:18px; color: #000000; text-align:center;'>"; 
	loadingMsg += "<span id = 'testview131>"
	loadingMsg += "<a class='top-textview-url'></a>";
	loadingMsg += "</span>";
	loadingMsg += "</top-textview>";
	loadingMsg += "</div>";
	$('body').append(mask);
	$('#mask').append(loadingImg); 
	$('#mask').append(loadingMsg);
	$('#mask').css({ 'width' : maskWidth , 'height': maskHeight}); 
	$('#mask').show(); 
	$('#loadingImg').show(); 
	$('#loadingMsg').show(); 
}

function hideLoadingBar2() { $('#mask, #loadingImg, #loadingMsg').hide(); $('#mask, #loadingImg, #loadingMsg').remove(); }

var openWindow = function(uri, name, options, closeCallback) {
    var win = window.open(uri, name, options);
    var interval = window.setInterval(function() {
        try {
            if (win == null || win.closed) {
                window.clearInterval(interval);
                closeCallback(win);
            }
        }
        catch (e) {
        }
    }, 1000);
    return win;
};

function checkMail(mail){
	let exptext = /^[A-Za-z0-9_\.\-]+@[A-Za-z0-9\-]+\.[A-Za-z0-9\-]+/;
	if(exptext.test(mail)==false){
		return false
	}else{
		return true
	}
	
}

function setBrowsercode(){
	let agent = navigator.userAgent.toLowerCase();
	
	if(agent.indexOf("chrome")!=-1){
		browsercode="BRO0001";  //  크롬
	}else if((navigator.appName == 'Netscape' && navigator.userAgent.search('Trident') != -1) || (agent.indexOf("msie") != -1)) {
		browsercode="BRO0002"   // 익플
	}else if(agent.indexOf("firefox")!=-1){
		browsercode="BRO0003" //파이어폭스
	}else if(agent.indexOf("safari")!=-1){
		browsercode="BRO0004" //사파리
	}else{
		browsercode="BRO0009" //기타등등 (이 잇긴한가)
	}
	return browsercode;
}


function saveCshid(valu){
	let enval = CryptoJS.AES.encrypt(valu, 'be080ffe-88a0-49ee-8b5c-d1a701ebba6c')
    setCookie('cshid',enval,7); 

}

function checkAge(age){
	if(!pattern_num.test(age))return false;
	if(age.length != 6)return false;
	let year = Number(age.substr(0,2))
	let mon = Number(age.substr(2,2))
	let day = Number(age.substr(4,2))
	if(mon<1 || mon>12) {
		return false;
	}
	if(day<1 || day>31) {
		return false;
	}
	 if ((mon==4 || mon==6 || mon==9 || mon==11) && day==31) {
         return false;
    }
//    if (mon == 2) {
//         var isleap = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
//         if (day>29 || (day==29 && !isleap)) {
//              return false;
//         }
//    }
    return true;
}

function is_hangul(ch) {
	  c = ch.charCodeAt(0);
	  if( 0x1100<=c && c<=0x11FF ) return true;
	  if( 0x3130<=c && c<=0x318F ) return true;
	  if( 0xAC00<=c && c<=0xD7A3 ) return true;
	  return false;
}

function is_num(ch) {
	  if(pattern_num.test(ch))return true;
	  else return false;
}
function is_eng(ch) {
	  if(pattern_engb.test(ch))return true;
	  else if(pattern_engs.test(ch)) return true;
	  else return false;
}
function is_henValid(chs){
	let io =  chs.length;
	for(var i=0; i<io;i++){
		if(is_hangul(chs[i]) || is_num(chs[i]) || is_eng(chs[i])){
			// 한글이거나 숫자거나 영어
		}else{
			return false;
		}
	}
	return true;
}


/**************************************************************로그인 로직******************************************************/
Top.Controller.create('loginLayoutLogic', {
	
	init: function(widget){
			if(typeof _validBtoc ==='function' && !_validBtoc()){
				tds('LoginSearchBorderLinearLayout2').setProperties({"display":"none"})
				tds('signupButton').setProperties({"display":"none"})
			}
			//로그인 방지 새로고침 이벤트
			$('span#refreshLoginIcon').click(function(){
				Top.Controller.get("loginLayoutLogic").refreshClick();
			})
			if(localStorage.getItem('loginCount')){
				console.log('로그인 오류 5회이상')
				tds('protectLoginLayout').setProperties({"visible":"visible"})
				tds('LoginBottomLinearLayout').setProperties({"line-height":"7.7rem"})
    			Top.Ajax.execute({
    				url : _workspace.url + "Users/SimpleCaptcha",
    				type : 'POST',
    				dataType : "json",
    				async : false,
    				cache : false,
    				data : null, // arg
    				contentType : "application/json",
    				crossDomain : true,
    				xhrFields : {
    					withCredentials: true
    				},
    				headers: {
    					"ProObjectWebFileTransfer":"true"
    				},
    				success : function(data) {
    					data = JSON.parse(data);
    					Top.Controller.get('signupBaseLogic').answer=data.dto.DESCRIPTION;
    					var bytes = new Uint8Array(data.dto.PROFILE_PHOTO);
    					var binary = "";
    					for (var i = 0; i < bytes.length; i++) {
    						binary += String.fromCharCode(bytes[i]);
    					}
    					var base64String = btoa(binary);
    					
    					tds("protectLoginImageView").setSrc("data:image/png;base64," + base64String);
    					
    				},
    				error : function(data) {
    					outputdata = {
    							result : "Fail",
    							message : data.exception != undefined ? data.exception.message : "Error",
    									data : data
    				}		
    					console.info("fail");
    					console.info(outputdata);
    				}
    			});
				
			}
			if(isMobile()){
				$('html').css({"font-size": 16 })
			}
			//기업 별 이미지
//			  $.ajax({
//	              url: _workspace.url+"Admin/CompanyImageGet?action=", //Service Object
//	              type: 'POST',
//	              dataType: 'json',
//	              crossDomain: true,
//	              contentType : "application/json",
//	              xhrFields: {
//	                  withCredentials: true
//	                },
//	                data: JSON.stringify({
//	                  "dto": {
//	                	 "COMPANY_EMAIL_DOMAIN" : _mailDomain 
//	                  }
//	                }),
//	              success: function(result) {
//	            	 if(!result.dto.COMPANY_IMAGE){
//	            		
//	            		 tds('LoginBtobImageView').setProperties({"visible":"hidden"})
//	            	 }else{
//	 	              	Top.Dom.selectById("LoginBtobImageView").setSrc(result.dto.COMPANY_IMAGE) 
//	            	 }
//	              },
//	              error: function(error) {
//	                alert("error-ExtUserInvite")
//	              }
//	            });


		
		
		//tds('loginButton').setProperties({'on-click':function(){ Top.App.routeTo(`/s/1/talk`); }});
		if(_validBtoc()){
			tds('signupButton').setProperties({'on-click':function(){tds('RegisterDialog').open()}})
			tds('searchIdButton').setProperties({'on-click':function(){tds('IdSearchDialog').open()}})
			tds('searchPwdButton').setProperties({'on-click':function(){tds('PwChangeDialog').open()}})
		}else{
			tds('signupButton').setProperties({'on-click':function(){tds('RegisterDialog').open()}})
			tds('searchIdButton').setProperties({'on-click':function(){tds('BtobIdSearchDialog').open()}})
			tds('searchPwdButton').setProperties({'on-click':function(){tds('BtobPwChangeDialog').open()}})
		}
		//tds('loginButton').focus();
		
		this.inviter = "";
		let key;
		if(ivtCode){
			Top.Ajax.execute({
		        url: _workspace.url + "Users/AEScode?action=Get",
		        type: 'POST',
		        dataType: "json",
		        cache: false,
		        data: JSON.stringify({
		            "dto": {
		                "KEY": "Decrypt",
		                "VALUE" : ivtCode.trim()
		            }
		        }),
		        contentType: "application/json; charset=utf-8",
		        crossDomain: true,
		        xhrFields: {
		            withCredentials: true
		        },
		        headers: {
		            "ProObjectWebFileTransfer": "true"
		        },
		        success: function (result) {
		            Top.Controller.get("loginLayoutLogic").inviter = result.dto.VALUE
		        }
		    });
		}
	
		this.mem_grade = "";
		let agent = navigator.userAgent.toLowerCase();
		browsercode = setBrowsercode()		
		var idfield = Top.Dom.selectById("LoginIdTextField");
		let pwfield = Top.Dom.selectById("LoginPwdTextField");
		idfield.setText("");
		pwfield.setText("");
		var checkbox = Top.Dom.selectById("SaveIdCheckBox");
			
		
		//최초 접속시 모바일 튜토리얼
		setTimeout(function(){																// 타이밍 문제 X, 앱 최초 실행시에만 Top.LocalService 존재
			if(!!Top.LocalService && tutoFlag){
	            Top.LocalService.acall({service:'tos.service.PackageManager.checkFirstRun'}).then((data) => {
	                    if(!!data){
	                            Top.Dom.selectById('mobileTutorialDialog').open();
	                            tutoflag = false;
	                            //Top.Controller.get('MainCMS_loginLayoutLogic').useYn = 'USE0001';
	                    }
	            });
			}
		}, 1000);
		
//		pwfield.setProperties({'on-blur':function(){
//			if(isMobile() && !navigator.userAgent.match(/iPhone/)) {
//				Top.Dom.selectById('LinearLayout3').setVisible('visible');
//			}
//		}})
//		Top.Ajax.execute({
//			url : _workspace.url + "Admin/AuthMasterToken",
//			type : 'GET',
//			dataType : "json",
//			cache : false,
//			data : null, // arg
//			contentType : "application/json",
//			crossDomain : true,
//			async : false,
//			xhrFields : {
//			    withCredentials: true
//			},
//			headers: {
//				"ProObjectWebFileTransfer":"true"
//			},
//			success : function(result) {
//				setCookie("loginid","auth_master",1);
				let userInputId = getCookie("cshid");
				if(userInputId){
					let list = CryptoJS.AES.decrypt(userInputId, 'be080ffe-88a0-49ee-8b5c-d1a701ebba6c').toString(CryptoJS.enc.Utf8).split('앤');
					Top.Dom.selectById("LoginIdTextField").setText(list[0]);
				//	Top.Dom.selectById("LoginPwdTextField").setText(list[1]);
					 Top.Dom.selectById("SaveIdCheckBox").setChecked(true); 
				    Top.Dom.selectById("LoginIdTextField").focus(); 
				}else{
					Top.Dom.selectById("SaveIdCheckBox").setChecked(false)
				}				
	},
	
	
	loginbutt : function(event, widget) {
	if(localStorage.getItem('loginCount') >= 5){
		
		if(tds('protectLoginTextField').getText() !== Top.Controller.get('signupBaseLogic').answer){
			tds('protectErrorIcon').setProperties({"visible":"visible"})
			tds('protectLoginTextField').addClass('alert')
			tds('protectPopover').open()
			return false;
		} else if(tds('protectLoginTextField').getText() === Top.Controller.get('signupBaseLogic').answer){				
			}
	}	
		
	showLoadingBar2();
	if(!_mailDomain){
		Top.Ajax.execute({
	        url: _workspace.url + "Users/SysContext",
	        type: 'GET',
	        dataType: "json",
	        cache: false,
	        data: JSON.stringify({
	            "dto": {
	                "KEY": "MAIL_URL"
	            }
	        }),
	        contentType: "application/json; charset=utf-8",
	        crossDomain: true,
	        xhrFields: {
	            withCredentials: true
	        },
	        headers: {
	            "ProObjectWebFileTransfer": "true"
	        },
	        success: function (result) {
	            _mailDomain = result.dto.VALUE
	        },
	        error: function(error){
	        	setTimeout(function(){
        			hideLoadingBar2();
        		},150)
	        	return;
	        }
	    });
	}
	var y;
	if (Top.Dom.selectById("LoginMaintainCheckBox").isChecked() == true){
    	y = 'y'
	}else{
		y = 'n'
	}
	var idp = Top.Dom.selectById("LoginIdTextField").getText();
	var pwp = Top.Dom.selectById("LoginPwdTextField").getText();
	var _this = this;
	let mailpt = idp.split('@')[1]
	if(mailpt == _mailDomain){
		//idp = idp.split('@')[0];
	}
	let ivt;
	if(Top.Controller.get("loginLayoutLogic").inviter){
		ivt = "invite"
	}else{
		ivt = "no"
	}
	//location.origin
	let b2bFlag;
///통합// b2bFlag = Top.Dom.selectById('TextField435').getText().split("@")[1]? true : false;
	
	let loginUrl = b2bFlag? "https://oqa.tmaxcloudspace.com/CMS/": _workspace.url;
	$.ajax({
        url: loginUrl +'Users/User?action=Auth',       //User?action=Auth', //Service Object
        type: 'POST',
        dataType: 'json',
        crossDomain: true,
        contentType : "application/json",
        xhrFields: {
            withCredentials: true
          },
        data: JSON.stringify({
          "dto": {
            "USER_LOGIN_ID": idp.trim(),
            "USER_PW": SHA256(idp.trim()+pwp.trim()),
            "REGI_DATE" : y,
            //"MODI_DATE": deviceId, // 디바이스 아이디  sessionStorage.getItem("DEVICEID")
            "MEM_GRADE" :"", // 디바이스 타입
            "RECOMMEND_MEM" : Top.Controller.get("loginLayoutLogic").inviter,
            "OAUTH_UUID" : ivt
          }
        }),
        success: function(result) {
///통합//	        if(b2bFlag){ 
///통합//	        	location.replace("https://oqa.tmaxcloudspace.com/#!/login?DEVICEID="+deviceId);
///통합//	        	return false;
///통합//	        }
        	
        	if( idp == "" || idp == null || idp == undefined || ( idp != null && typeof idp == "object" && !Object.keys(idp).length ) ){ 
        		setTimeout(function(){
        			hideLoadingBar2();
        		},150)
    			
    			Top.Dom.selectById("LoginIdErrorIcon").setProperties({"visible":"visible"});
    			Top.Dom.selectById("InputErrorIcon").setProperties({"visible":"none"});
    			Top.Dom.selectById("PasswordErrorIcon").setProperties({"visible":"none"});
    			
    			Top.Dom.selectById("LoginIdTextField").addClass("alert");
    			Top.Dom.selectById("LoginIdTextField").focus();
    			Top.Dom.selectById("LoginIdErrorPopover").open();
    			return false 
	    		}
    		if( pwp == "" || pwp == null || pwp == undefined || ( pwp != null && typeof pwp == "object" && !Object.keys(pwp).length ) ){ 
    			setTimeout(function(){
        			hideLoadingBar2();
        		},150)
    			Top.Dom.selectById("PasswordErrorIcon").setProperties({"visible":"visible"});
    			Top.Dom.selectById("InputErrorIcon").setProperties({"visible":"none"});
    			Top.Dom.selectById("LoginIdErrorIcon").setProperties({"visible":"none"});
    			
    			Top.Dom.selectById("LoginPwdTextField").addClass("alert");
    			Top.Dom.selectById("LoginPwdTextField").focus();
    			Top.Dom.selectById("PasswordErrorPopover").open();
    			
    			$('.top-textfield-icon').addClass("login-alert")

    			return false
    		}
        	if(result.dto.RESULT_CD != "RST0001" ){
        		// admin, operator 용
        		setTimeout(function(){
        			hideLoadingBar2();
        		},150)
        			Top.Dom.selectById("LoginIdTextField").addClass("alert");
        		
        			Top.Dom.selectById("LoginPwdTextField").addClass("alert");
        			Top.Dom.selectById("LoginPwdTextField").focus();
        			Top.Dom.selectById("LoginIdErrorIcon").setProperties({"visible":"none"});
        			Top.Dom.selectById("PasswordErrorIcon").setProperties({"visible":"none"});
        			Top.Dom.selectById("InputErrorIcon").setProperties({"visible":"visible"});
        			Top.Dom.selectById("InputErrorPopover").open();
        			 
        			//로그인 시도 5회 이상 할 시
                    if(result.dto.ACCESS_CNT >= 5){
                    	console.log(result.dto.ACCESS_CNT)
                    	tds('protectLoginLayout').setProperties({"visible":"visible"})
                    	tds('LoginBottomLinearLayout').setProperties({"line-height":"7.7rem"})
                    	localStorage.setItem("loginCount",result.dto.ACCESS_CNT)
                    	
                    			Top.Ajax.execute({
                    				url : _workspace.url + "Users/SimpleCaptcha",
                    				type : 'POST',
                    				dataType : "json",
                    				async : false,
                    				cache : false,
                    				data : null, // arg
                    				contentType : "application/json",
                    				crossDomain : true,
                    				xhrFields : {
                    					withCredentials: true
                    				},
                    				headers: {
                    					"ProObjectWebFileTransfer":"true"
                    				},
                    				success : function(data) {
                    					data = JSON.parse(data);
                    					Top.Controller.get('signupBaseLogic').answer=data.dto.DESCRIPTION;
                    					var bytes = new Uint8Array(data.dto.PROFILE_PHOTO);
                    					var binary = "";
                    					for (var i = 0; i < bytes.length; i++) {
                    						binary += String.fromCharCode(bytes[i]);
                    					}
                    					var base64String = btoa(binary);
                    					
                    					Top.Dom.selectById("protectLoginImageView").setSrc("data:image/png;base64," + base64String);
                    					
                    				},
                    				error : function(data) {
                    					outputdata = {
                    							result : "Fail",
                    							message : data.exception != undefined ? data.exception.message : "Error",
                    									data : data
                    				}		
                    					console.info("fail");
                    					console.info(outputdata);
                    				}
                    			});
                    }
        			
        			
    	        	return false
        	

        	}
        	else{
        		conid = result.dto.CONNECTION_ID;
        		setCookie("loginid",idp.trim(),1);
        		//local storage 카운트 제거
        		localStorage.removeItem("loginCount")
        		// 로그인 성공
            	if(isLocal()){	
                	//localhost에서 cookie없이 로그인 가능하도록
                		userManager.localLogin(result.dto.USER_ID, function setLocalInfo(){
                			setLoginInfo("login");
        				 });
                		return false;
                }else{
                	setLoginInfo("login");
                }
        		
        		
        		_this.useYn = result.dto.USE_YN;

	        	if (Top.Dom.selectById("SaveIdCheckBox").isChecked() == true){
	        		let coval = idp.trim()
	        		saveCshid(coval);
	        	}else{
	        		deleteCookie("cshid");
	        	}
	        	if (Top.Dom.selectById("LoginMaintainCheckBox").isChecked() == true){
	        	}else{
//	        		deleteCookie("loginkey");
	        	}
	        	
	        	//외부 사용자 초대를 통해 들어온 경우
	        	if(Top.Controller.get('loginLayoutLogic').isExternal == true)
	              $.ajax({
                    url: _workspace.url+"WorkSpace/WorkspaceExtUser?action=Invite", //Service Object
                    type: 'POST',
                    dataType: 'json',
                    crossDomain: true,
                    contentType : "application/json",
                    xhrFields: {
                        withCredentials: true
                      },
                      data: JSON.stringify({
                        "dto": {
                          "wsUserList":[
                            {
                              "USER_ID": result.dto.USER_ID, 
                              "WS_ID" : Top.Controller.get('loginLayoutLogic').wsId
                            }
                          ]
                        }
                      }),
                    success: function(result) {
                    	Top.Controller.get('loginLayoutLogic').isExternal = false
                    	Top.Controller.get('loginLayoutLogic').wsId = ""
                    },
                    error: function(error) {
                      alert("error-ExtUserInvite")
                    }
                  });
	        	//////////////////////////
	        	$.ajax({
					url: _workspace.url+'Users/UserAlarmSet', //Service Object
					type: 'GET',
					data: JSON.stringify({
						"dto" :{
							"USER_ID" : result.dto.USER_ID
						  }
					}),
					dataType: 'json',
					contentType: 'application/json; charset=utf-8',
					xhrFields: {
	
						withCredentials: true
	
					  },
					crossDomain: true,
					contentType: 'application/json; charset=UTF-8',
					success: function(result2) {
						setUserAlarmConfig(result2.dto.dtos); // NotificationManager.updateConfig()가 함수 안에 내장되어있음.
					}
				})
	        	checkMemGrade(result.dto.MEM_GRADE);
	        	//getWorkspaceInfo();

        	}
        	

        },
        error: function(error) {
        	
        	if( idp == "" || idp == null || idp == undefined || ( idp != null && typeof idp == "object" && !Object.keys(idp).length ) ){ 
        		setTimeout(function(){
        			hideLoadingBar2();
        		},150)
			Top.Dom.selectById("LoginIdErrorIcon").setProperties({"visible":"visible"});
			Top.Dom.selectById("InputErrorIcon").setProperties({"visible":"none"});
			Top.Dom.selectById("PasswordErrorIcon").setProperties({"visible":"none"});
	
			Top.Dom.selectById("LoginIdTextField").addClass("alert");
			Top.Dom.selectById("LoginIdTextField").focus();
			Top.Dom.selectById("LoginIdErrorPopover").open();
			return false 
    		}
    		if( pwp == "" || pwp == null || pwp == undefined || ( pwp != null && typeof pwp == "object" && !Object.keys(pwp).length ) ){ 
    			setTimeout(function(){
        			hideLoadingBar2();
        		},150)
    			Top.Dom.selectById("PasswordErrorIcon").setProperties({"visible":"visible"});
    			Top.Dom.selectById("InputErrorIcon").setProperties({"visible":"none"});
    			Top.Dom.selectById("LoginIdErrorIcon").setProperties({"visible":"none"});
    			
    			Top.Dom.selectById("LoginPwdTextField").addClass("alert");
    			Top.Dom.selectById("LoginPwdTextField").focus();
    			Top.Dom.selectById("PasswordErrorPopover").open();
    			$('.top-textfield-icon').addClass("login-alert")
    			return false
    		}

			Top.Dom.selectById("LoginIdTextField").addClass("alert");
		
			Top.Dom.selectById("LoginPwdTextField").addClass("alert");
			Top.Dom.selectById("LoginPwdTextField").focus();
			Top.Dom.selectById("LoginIdErrorIcon").setProperties({"visible":"none"});
			Top.Dom.selectById("PasswordErrorIcon").setProperties({"visible":"none"});
			Top.Dom.selectById("InputErrorIcon").setProperties({"visible":"visible"});
			Top.Dom.selectById("InputErrorPopover").open();
			setTimeout(function(){
    			hideLoadingBar2();
    		},150)
        	return false

        	}
	});

	  
	 	  
	}, 

loginenter : function(event, widget) {
	if(browsercode == "BRO0001"){
		if(event.code ==="Enter" || event.key ===  'Enter'){
			this.loginbutt();
		}
	}
	else if(browsercode == "BRO0002"){
		key=(event)?event.keyCode:event.keyCode;
		if(key==13){
			this.loginbutt();
		}	
	}else{
		if(event.code ==="Enter"){
			this.loginbutt();
		}
	}
	
},
showpw: function(event, widget){
	let pw = Top.Dom.selectById(widget.id)
	if(pw.getProperties("password")){
		pw.setProperties({"password": "false", "icon": "icon-ic_password_show"})
	}else{
		pw.setProperties({"password": "true", "icon": "icon-ic_password_hide"})
	}
}, pwdTextFieldFocusout : function(event, widget) {
	tds('LoginPwdTextField').removeClass('alert')
	tds('PasswordErrorIcon').setProperties({"visible":"none"})
	$('.top-textfield-icon').removeClass("login-alert")
}, protectKeyup : function(event, widget) {
	tds('protectLoginTextField').removeClass('alert')
	tds('protectErrorIcon').setProperties({"visible":"none"})
}, refreshClick : function(event, widget) {
	Top.Ajax.execute({
		url : _workspace.url + "Users/SimpleCaptcha",
		type : 'POST',
		dataType : "json",
		async : false,
		cache : false,
		data : null, // arg
		contentType : "application/json",
		crossDomain : true,
		xhrFields : {
			withCredentials: true
		},
		headers: {
			"ProObjectWebFileTransfer":"true"
		},
		success : function(data) {
			data = JSON.parse(data);
			Top.Controller.get('signupBaseLogic').answer=data.dto.DESCRIPTION;
			var bytes = new Uint8Array(data.dto.PROFILE_PHOTO);
			var binary = "";
			for (var i = 0; i < bytes.length; i++) {
				binary += String.fromCharCode(bytes[i]);
			}
			var base64String = btoa(binary);
			
			Top.Dom.selectById("protectLoginImageView").setSrc("data:image/png;base64," + base64String);
			
		},
		error : function(data) {
			outputdata = {
					result : "Fail",
					message : data.exception != undefined ? data.exception.message : "Error",
							data : data
		}		
			console.info("fail");
			console.info(outputdata);
		}
	});
}



});
/***************************************************************************************************************************************************************************************************/

let timerFlag;
let LOGOUT = true;

// 자동로그인
function autoLogin(){
	let hash = location.hash;
	$.ajax({
        url: _workspace.url+'Users/Auto?action=Login',  
        type: 'POST',
        dataType: 'json',
        crossDomain: true,
        contentType : "application/json",
        async:false,
        data: JSON.stringify({ "dto": {
        	"MODI_DATE": deviceId
        }}),
        success: function(ret) {
			let result = ret.dto;
        	if("RST0001"===result.RESULT_CD ){
        		userManager.getLoginUserInfo();
	        	checkMemGrade(result.MEM_GRADE);
				conid = result.CONNECTION_ID;
				$.ajax({
					url: _workspace.url+'Users/UserAlarmSet', //Service Object
					type: 'GET',
					data: JSON.stringify({
						"dto" :{
							"USER_ID" : ret.dto.USER_ID
						  }
					}),
					dataType: 'json',
					contentType: 'application/json; charset=utf-8',
					xhrFields: {
	
						withCredentials: true
	
					  },
					crossDomain: true,
					contentType: 'application/json; charset=UTF-8',
					success: function(result2) {
						setUserAlarmConfig(result2.dto.dtos); // NotificationManager.updateConfig()가 함수 안에 내장되어있음.
					}
				})
	        	timerFlag = "y"===result.USER_PW? true: false;
        		setLoginInfo();
        		
        	}else{
        		autoLoginFail(hash);
        	}

        },
        error: function(error) {
        	console.log("autologin fail : ", error);
    		autoLoginFail(hash);							
        }
	});
}

function setLoginInfo (type){
	
			//임시방 삭제
			if(!isNewWindow()) spaceAPI.updateTempManager().then(ret => {spaceAPI.truncateTempManager()});
	
			//타이머
			let connFlag = getCookie("BCID");
			if(!connFlag || connFlag.length<1 ) LogoutTimer.start(false);
			
			//로그인 flag
			LOGOUT=false;
				
			//웹소켓
			initCmsWebSocket();
			
			//알림 탭 초기화
			NotificationManager.initTeeSpaceTabList();

			//언리드, 서비스 변경
//			 let spaceList = talkServer2.getMessengerRooms(userManager.getLoginUserId());
			
			//미니톡
			let miniTalk = getQueryStringByKey("mini")==="true"
			if(miniTalk){
			    spaceAPI.updateTempManagerSync();					//temp space- minitalk
				return;
			}
			if(officeManager.popupCheck())return;
			
			// 외부 메일 타이머
			clearInterval(Top.Controller.get('mailWriteLogic').tempInt);
			mail.getExtMail30Min();	
			
			//권한 체크
			let userInfo = userManager.getLoginUserInfo();
			
			if(['admin', 'superadmin'].includes(userInfo.userGrade)){
				admin.replaceLocation(userInfo.userGrade);
			} else{
				// 마지막 접속 url
				let mySpace = workspaceManager.getMySpaceUrl();
				let lnbChck = ("f" === getLnbByUrl());														
				let hst = historyList.get("all")? historyList.get("all").UserRoutigHistoryList[0] : null;
				let initUrl = sessionStorage.initUrl? sessionStorage.initUrl : null;
				sessionStorage.removeItem("initUrl");
				
				if(hst){		//임시
					var main = hst.APP_INFO.split("/")[0];
					var sub = ""===hst.APP_INFO.split("/")[1]? null : hst.APP_INFO.split("/")[1];
		        	var mainUrl = getMainAppByUrl(hst.LAST_URL);
		        	var subUrl = getSubAppByUrl(hst.LAST_URL);
		        	var ratioType = appManager.getType(mainUrl, subUrl)? appManager.getType(mainUrl, subUrl) : {eventType: 'close'};
					var expandable = ("expand" === ratioType.eventType);
					
				}
				
				//사용자 입력 url
				if(initUrl && "#!/login" === initUrl){
					window.location.replace(`${rootUrl}f/${userInfo.USER_LOGIN_ID}`);	//  replace시 init이 불려지지 않는 오류 - top 문의 필요 (splitlayout)
//					Top.App.routeTo(`/f/${userInfo.USER_LOGIN_ID}`, {eventType:'close'});
				}
				else if(initUrl && ""!==initUrl){
					let initMain = getMainAppByUrl(initUrl);
					let initSub = getSubAppByUrl(initUrl);
					let mainEntry = appManager.getMains();
					let subEntry = appManager.getApps();
					let ratioType = appManager.getType(initMain, initSub);
					
					if(mainEntry.includes(initMain)){
						appManager.setMainApp(initMain);
						appManager.setSubApp(initSub);
					}
					else if(subEntry.includes(initSub)){														
						appManager.setSubApp(initMain);
					}
					// 일반 로그인 - route / 자동 로그인 - 기존 url 
					if("login" === type) Top.App.routeTo(initUrl.slice(2), ratioType.ratio);
					
/*					event.preventDefault();
					event.stopPropagation();
					window.location.replace(rootUrl + initUrl.slice(3));//Top.App.routeTo(initUrl.slice(2)); */
//					return false;
				}
				
				//로그인
				else if("login"===type) {
					if(hst) {
						appManager.setMainApp(main);
						appManager.setSubApp(sub);
						appManager.updateExpandState(expandable);
						Top.App.routeTo(hst.LAST_URL.slice(2), {eventType: ratioType.eventType});
//						return false;
					}
					else{
						Top.App.routeTo(`/f/${userInfo.USER_LOGIN_ID}`, {eventType:'close'});
					} 
				}
				//자동 로그인
				else {
					if(hst) {
							appManager.setMainApp(main);
							appManager.setSubApp(sub);
							appManager.updateExpandState(expandable);
							location.replace(hst.LAST_URL);
//							return false;
					}
					else window.location.replace(rootUrl + `s/${mySpace}/talk`, {eventType:'close'});
				}
				 
			}
}
 
// 자동로그인 실패 (fail)
function autoLoginFail(hash){
	switch(hash){ 
	  	case 'searchid' : ""; 		break;
	  	case 'changepw' : ""; 		break;
	  	case 'register' : ""; 		break;
	  	default: window.location.replace(rootUrl+"login");
	} 
}


var LogoutTimer = function() {
	let flag = true;						//false = 동작
    var S = {
              timer : null, 
              limit : 1000 *  60 * 60 * 12, 	//12시간 후 로그아웃   60 * 60 * 12
              fnc   : function() { 
            	  try {
            		  var msg = '장시간 사용하지 않아 로그아웃되었습니다.';
            		  notiFeedback(msg);
            	  } catch (e) {}
            	  //S.end();
            	  userManager.logout()
              },
              start : function(inputFlag) {
            	  flag = inputFlag;
            	  S.timer = window.setTimeout(S.fnc, S.limit);
              },
              reset : function() {
            	  window.clearTimeout(S.timer);
            	  S.start(false);
              },
              end: function(){
            	  window.clearTimeout(S.timer);
            	  flag = true;
              }
    };
    document.onmousemove = function() { if(!flag) {S.reset();} };  return S; 
}();



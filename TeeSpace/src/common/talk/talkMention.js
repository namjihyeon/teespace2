
const talkMention = (function () {
    let _isMentionIconClicked = false;
    let selectedIndex = -1;
    let selectedData = null;
    let maxLength = 0;
//TODO : Mention renderer로 이동
    function _create(targetSelector) {
        $("<div id='talk__mention-popup'/>")
            .append(
                $("<div id='talk__mention-popup__list-container'/>")
                    .append(
                        $("<ul id='talk__mention-popup__list'/>")
                            .on("click", "li",
                                function (event) {
                                    let target = $(event.currentTarget);
                                    selectedIndex = talkUtil.parseInt(target.attr("data-index"));
                                    selectedData = {
                                        "user-id": target.attr("data-user-id"),
                                        "user-name": target.attr("data-user-name")
                                    };

                                    if (!_isMentionIconClicked) {
                                        talkFooter.removeOneText();
                                    }
                                    _onUserClick(selectedData);
                                }
                            )
                    )
            )
            .append(
             $("<div id='talk__mention-popup__search-container__wrapper'>").append(
                $("<div id='talk__mention-popup__search-container'/>")
                    .append(
                        $("<span id='talk__mention-popup__search-icon'>").addClass("icon-work_search")
                    ).append(
                        $("<input type='text' id='talk__mention-popup__search-area'>")
                            .on("input", function () {
                                let name = $(this).val();
                                _searchUser(name)
                            })
                            .focus(function () {
                                $("#talk__mention-popup__search-container").css("border", "0.063rem solid #6C56E5");
                            })
                            .blur(function(){
                                $("#talk__mention-popup__search-container").css("border", "0.063rem solid #CDCDCF");
                            })
                    	).append(
                    		$("<span id='talk__mention-popup__search-cancel' class='icon-work_cancel'>").on("click",function(){
                    			//input창에 치고 있던 내용 clear하는 함수
                    			//_clearVariables();
                    			document.getElementById('talk__mention-popup__search-area').value=''	
                    		})
                    	)
                	)
            )	
            .appendTo(targetSelector);   
    }

    function _update(roomId, callback) {
    	spaceAPI.getSpaceMemberListBrief(talkData.workspaceId).then(function (response) {
            let userType = "USR0001"
            userType = (talkData.myInfo.userType && talkData.myInfo.userType === "USR0002") ? "USR0002" : "USR0001";

            let container = $("#talk__mention-popup__list");
            let frag = $(document.createDocumentFragment());
            let isMyInfo = false;
            
            userList = response.data.dto.UserProfileList;
            let _length = userList.length;
            maxLength = _length;
            for (let i = 0; i < _length; i++) {
                let _userInfo = userList[i];
                if (_userInfo["USER_ID"] === talkData.myInfo.userId) {
                	isMyInfo = true;
                } else {
                	isMyInfo = false;
                }
                
//                let _imageSrc = (_userInfo.PROFILE_PHOTO) ? _userInfo.PROFILE_PHOTO : "./res/bi_favicon.ico";
                let _imageSrc = userManager.getUserPhoto(_userInfo["USER_ID"],"small",_userInfo["THUMB_PHOTO"]);
                //let _imageSrc = _userInfo["PROFILE_PHOTO"] || userManager.getUserDefaultPhotoURL(_userInfo["USER_ID"]);
                //B2C, B2B 이제는 무조건 USER_NAME으로 잘 주기로 협의
                let _infoString = _userInfo.USER_NAME;
                
                // (나) 표시해주기
                if (isMyInfo) {
                	_infoString += " (나)"
                }
                
//                if (userType !== "USR0002") {
//                    let _tempArr = [];
//                    if (_userInfo.USER_POSITION)
//                        _tempArr.push(_userInfo.USER_POSITION);
///*                    if (_userInfo.USER_JOB)
//                        _tempArr.push(_userInfo.USER_JOB);
// */                  
//                    if (_userInfo.USER_JOB){
//                    _tempArr.push(_userInfo.USER_JOB);
//                    _infoString += (" (" + _tempArr.join("&nbsp;·&nbsp;") + ")");
//                    }
//                }

                let _errorEvent = 'onerror="talkRenderer.component.profileImgError(event)"';
                
                let item = $("<li class='mention-popup__list-item'/>")
                    .attr({ "data-index": i, "data-user-id": _userInfo.USER_ID, "data-user-name": _infoString })
                    .append("<div class='mention-popup__photo-wrapper'><img class='mention-popup__user-photo' src='" + _imageSrc + "' "+ _errorEvent + "/></div>")
                    .append("<span class='mention-popup__user-info'>" + _infoString + "</span>")
                    .append("<span class='mention-popup__status'/>");

                // if (i === 0)
                //     item.addClass("mention-popup__list-item--active");
                
                // Mention창에서 내 info는 가장 위로
                if (isMyInfo) {
                	item.css("border-bottom", "0.063rem solid #CDCDCF")
                		.prependTo(frag);
                } else {
                	item.appendTo(frag);
                }                
            }

            container.html(frag);

            if (callback && typeof callback === "function")
                callback();
        });
    }

    function _onUserClick(userInfo) {

        let mention = $("<span/>")
            .addClass("mention")
            .attr({
                "data-type": "mention",
                "data-user-id": userInfo["user-id"],
                "data-user-name": userInfo["user-name"],
                "contenteditable": "true"
            })
            .text("@" + userInfo["user-name"]);

        talkFooter.appendHTML(mention[0]);
        talkFooter.appendText("\u00A0");
    }

    function _closeEvent() {
        let whiteList = [".icon-commat", "#talk__mention-popup__search-container"];
        for (let i = 0; i < whiteList.length; i++) {
            if ($(event.target).closest(whiteList[i]).length) {
                return;
            }
        }
        talkMention.close();       
    }

    function _keydownEvent(event) {
        switch (event.keyCode) {
            case 38:    // 방향키 위
                event.preventDefault();
                if (selectedIndex > 0) {
                    let idx = selectedIndex;
                    while (!$(".mention-popup__list-item").eq(--idx).is(":visible")) {
                        if (idx < 0)
                            return;
                    }
                    // idx--;
                    _selectIndex(idx);
                    _updateScroll(event.keyCode, idx);
                }
                break;
            case 40:    // 방향키 아래
                event.preventDefault();
                if (selectedIndex !== maxLength - 1) {
                    let idx = selectedIndex;
                    while (!$(".mention-popup__list-item").eq(++idx).is(":visible")) {
                        if (idx === maxLength)
                            return;
                    }
                    // idx++;
                    _selectIndex(idx);
                    _updateScroll(event.keyCode, idx);
                }
                break;
            case 13:    // 엔터
                event.preventDefault();
                if (selectedIndex >= 0 && selectedData["user-id"] && selectedData["user-name"]) {
                    // @ 입력해서 멘션 기능 사용한 경우
                    if (!_isMentionIconClicked) {
                        // window.getSelection().anchorNode.childNodes[window.getSelection().anchorOffset - 2
                        talkFooter.removeOneText();
                        // $("#text-area").focus();
                        // // 앵커 맨 뒤로

                        // let selection = window.getSelection();
                        // let range = new Range();
                        // range.setStart(selection.anchorNode, selection.anchorNode.length - 1);
                        // range.setEnd(selection.anchorNode, selection.anchorNode.length);

                        // selection.removeAllRanges();
                        // selection.addRange(range)
                        // selection.deleteFromDocument();
                    }
                    _onUserClick(_getSelectedData());
                    _close();


                }
                // console.warn("data", _getSelectedData());
                break;

            case 27: // ESC
                _close();
                break;

            case 8: // backspace
                // console.warn("mentionpopup backspace")
                break;

            default:
                break;

        }
    }

    function _isExist() {
        return !!$("#talk__mention-popup").length;
    }

    function _updateScroll(keycode, idx) {
        let _listItemHeight = $('.mention-popup__list-item').height();
        let _minIdx = $("#talk__mention-popup__list-container").scrollTop() / _listItemHeight
        let _maxIdx = talkUtil.parseInt($("#talk__mention-popup__list-container").css("max-height").replace("px", "")) / _listItemHeight - 1 + _minIdx;
        let _container = $("#talk__mention-popup__list-container");
        let _scrollTop = _container.scrollTop();

        switch (keycode) {
            case 38: // up arrow
                if (idx < _minIdx) {
                    _container.scrollTop(_scrollTop - _listItemHeight);
                }
                break;
            case 40: // down arrow
                if (idx > _maxIdx) {
                    _container.scrollTop(_scrollTop + _listItemHeight);
                }
                break;
            default:
                _container.scrollTop(0);
                break;
        }
    }
    function _selectIndex(idx) {
        let items = $(".mention-popup__list-item");
        let activeClassName = "mention-popup__list-item--active";
        let activedItem = items.eq(selectedIndex);
        if (activedItem.hasClass(activeClassName))
            activedItem.removeClass(activeClassName);

        selectedIndex = idx;
        selectedData = {
            "user-id": (idx < 0) ? null : items.eq(selectedIndex).attr("data-user-id"),
            "user-name": (idx < 0) ? null : items.eq(selectedIndex).attr("data-user-name")
        };
        items.eq(selectedIndex).addClass(activeClassName);
    }

    function _getSelectedData() {
        return selectedData;
    }

    function _clearVariables() {
        selectedIndex = -1;
        selectedData = null;
        maxLength = 0;
        $(document).off("click", _closeEvent);
        $(document).off("keydown", _keydownEvent);
        $("#talk__mention-popup__search-area").val("");
    }

    function _close() {
        if (_isExist()) {
            _clearVariables();
            $("#talk__mention-popup__list-container").scrollTop(0);
            $("#talk__mention-popup").hide();
//            $("#mention-popup").remove();
            talkFooter.appendText("");
            
            document.querySelector('#talk-footer__mention-button').classList.remove("talk-img__filter");
        }
    }

    function _searchUser(userName) {
        let firstVisibleIdx = -1;
        let isFinded = false;
        for (let i = 0; i < maxLength; i++) {
            let _userItem = $(".mention-popup__list-item").eq(i);
            let _userName = _userItem.text();
            if (_userName.indexOf(userName) !== -1) {
                if (!isFinded) {
                    firstVisibleIdx = i;
                    isFinded = true;
                }
                _userItem.show();
            }
            else {
                _userItem.hide();
            }
        }

        _selectIndex(firstVisibleIdx);
        // let _visibleItems = $(".mention-popup__list-item:visible");
        // let _length = _visibleItems.length;
        // if (_length) {
        //     _selectIndex(parseInt(_visibleItems.eq(0).attr("data-index")));
        // }
    }

    function _open(css, isMentionIconClicked) {
        if (!_isVisible()) {
            _isMentionIconClicked = isMentionIconClicked;
            let appendTarget = "body"
            if (!_isExist()) {
                _create(appendTarget);
            }

            _update(talkData.selectedRoomId, function () {
                // let _visibleItems = $(".mention-popup__list-item:visible");
                // let _length = _visibleItems.length;
                // if (_length) {
                //     _selectIndex(_visibleItems.eq(0).attr("data-index"));
                // }

                $("#talk__mention-popup").css({ "top": "", "bottom": "", "left": "", "right": "" }).css(css).show();
                $(document).on("click", _closeEvent);
                $(document).on("keydown", _keydownEvent);
                _searchUser("");
                $("#talk__mention-popup__search-area").focus();
            });
            document.querySelector('#talk-footer__mention-button').classList.add("talk-img__filter");
        }
    }

    function _isVisible() {
        return $("#talk__mention-popup").is(":visible");
    }

    return {
        open: function (css, isMentionIconClicked) {
            // _open(top, left);
            _open(css, isMentionIconClicked);
        },

        close: function () {
            _close();
        },

        getSelectedData: function () {
            return _getSelectedData();
        },

        isOpened: function () {
            return _isVisible();
        },

        searchUser: function (userName) {
            _searchUser(userName);
        }
    }
})();
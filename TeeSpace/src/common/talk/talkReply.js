const talkReply = (function() {
	// create Message하는데 필요한 createFormat 함수는 talkServiceCall.js 파일에 있음
	
	
    //TODO: 답장해야하는 타겟의 내용 자체를 VIEW에 넣어야함.
    mobx.observe(talkData.selectedReply, function(change) {
        let selectedReply = $(talkData.selectedReply.value);
        let replyView = document.querySelector('#talk-footer__reply-view');
        if (replyView) {
        	talkUtil.removeChildrenNodes(replyView);
        };
        
        if (replyView && talkData.selectedReply.value !== null) {
            //        	replyView.text(talkData.selectedReply.value["MSG_BODY"]);
            talkData.selectedEmoticonTag.set(null);
            // 이모티콘 버튼 활성화 표시 풀기
            document.querySelector("#talk-footer__emoticon-button").classList.remove("talk-img__filter");
            
            let footerInputContainer = document.querySelector('#talk-footer__input-wrapper');
            
            if (!$('#talk-footer__input-wrapper').find("img.talk__reply-arrow").length) {
            	let arrow = document.createElement('img');
            	arrow.setAttribute('src', "./res/talk/reply.svg");
            	arrow.classList.add("talk__reply-arrow");
            	footerInputContainer.insertBefore(arrow, footerInputContainer.firstElementChild);
            }
           
            
            talkFooter.setCaret();

            let originalMsgSenderName = talkUtil.getUserNick(talkData.selectedReply.value["USER_ID"]);
            
            let displayOriginalMsg = talkMessageConverter.serverMsgToHtmlMsg(false,talkData.selectedReply.value["MSG_BODY"]);
            
            let replyContent = document.createElement("div");
            replyContent.setAttribute("id", 'talk-footer__reply-content');
            replyContent.classList.add("talk-footer__originalMsg");
            replyContent.classList.add("talk__text-truncated");

            let cancelBtn = document.createElement("div");
            cancelBtn.setAttribute("id", "talk-footer__reply-cancel-button");
            cancelBtn.classList.add("icon-work_cancel")
            cancelBtn.addEventListener("click", function() {
            	talkData.selectedReply.set(null);
            }, false);
            
			let replyTo = document.createElement("div");
			replyTo.classList.add("talk-footer__reply-to")
            replyTo.textContent = originalMsgSenderName+"에 답장";
            
            let originalMsgContainer = document.createElement("div");
            originalMsgContainer.style.display = "inline-flex";
            originalMsgContainer.style.color = "#000000";
            let template = `<div>${displayOriginalMsg}</div>`;
            originalMsgContainer.insertAdjacentHTML("beforeend", template);
                 
            replyContent.appendChild(cancelBtn);
            replyContent.appendChild(replyTo);
            replyContent.appendChild(originalMsgContainer);
            
            replyView.appendChild(replyContent);
            replyView.style.display = "flex";
            talkFooter.updateButtonStatus(true);
            
        } else {
            replyView.style.display = "none";
            talkFooter.updateButtonStatus(talkFooter.isTextAreaEmpty());
            let replyArrow = document.querySelector("#talk-footer__input-wrapper .talk__reply-arrow");
            if (replyArrow) {
            	replyArrow.remove();
            }
        }
    });

	return {
		
		hasAttachedFile: function(targetAttachList) {
			var _length = targetAttachList.length;
			if (_length) {
				if (targetAttachList[0]["ATTACHMENT_TYPE"].includes("file")) {
					// 첨부 파일이 있고 그 시작은 0번 idx부터다
					return [true, 0];
				}
				else if (_length>1 && targetAttachList[1]["ATTACHMENT_TYPE"].includes("file")) {
					// 첨부 파일이 있고 답장 때문에 그 시작이 1번 idx다
					return [true, 1];
				}
			}
			
			return [false, null];
		},
		
        replyMsgToServerMsg: function(replyObject) {    
            // attachment-body가 html tag임	    	
	        let obj = JSON.stringify({
	            "data-type": "reply",
	            "origin-msg-id": replyObject.get().MSG_ID,
	            "origin-msg-owner": replyObject.get().USER_ID,
	            "origin-msg-body": replyObject.get().MSG_BODY
	        });
	        inputText = "<" + obj + ">";

	        return inputText;
	    },
	    
        // 답장 attachment 보여줄 때 사용할 함수
	    serverMsgToReplyMsg: function(rawContent) {
            return JSON.parse(rawContent.slice(1, rawContent.length - 1));
        },
        
        // 기존에는 data-type="reply"에 html tag를 그대로 붙였는데 server Msg 형태로 저장하는 것
        //으로 바꿈, 따라서 기존과의 호환성 생각해서 html 형태로 바꾸는 로직 추가
        originalMsgtoHtmlMsg: function(inputText) {
            arr = inputText.match(/<span(.+?)<\/span>/g);
            arr1=  inputText.match(/<a(.+?)<\/a>/g);
            arr2 = inputText.match(/<div(.+?)<\/div>/g);
            if (arr || arr1 || arr2) {
                return inputText;
            } else {
                return talkMessageConverter.serverMsgToHtmlMsg(false,inputText);
            }
	    },
	    
	    // reply-view에 보여줄 데이터를 담는 object 만들기
	    createReplyMsgData: function(event) {
	    	var msgId = parseInt($(event.currentTarget)
				            .closest("talk-message-layout")
				            .attr("data-message-id"));
			var targetMessage = talkData.messageList.find(function(elem) {
		                            return elem["MSG_ID"] === msgId;
		                        });
			var userId = targetMessage["USER_ID"]
            var originalMsgBody = targetMessage["MSG_BODY"] || "";
			
			if (targetMessage["ATTACHMENT_LIST"]) {
				var _length = targetMessage["ATTACHMENT_LIST"].length;
				
				// available type에는 file, fileList가 있다
				// (~.jpg)msgBody
				// (~.jpg 외 3개 파일)
				var attachedFile = talkReply.hasAttachedFile(targetMessage["ATTACHMENT_LIST"]); // return은 [true or false, 시작 idx]
				if (attachedFile[0]) {
					var fileMeta = talkContent.talk_parseFile(targetMessage["ATTACHMENT_LIST"][attachedFile[1]]["ATTACHMENT_BODY"]);
					_prefix = "("+fileMeta["data-file-name"] + "." + fileMeta["data-file-extension"]
					// 답장 빼고 attachment 개수
					_length = parseInt(_length - attachedFile[1]);
					if (_length > 1) {
						_prefix += " 외 " + (_length-1) + "개 파일"
					}
					originalMsgBody = _prefix + ") " + originalMsgBody;
				}
			}

			
		    var replyMsgData = {
		        MSG_ID: msgId,
		        USER_ID: userId,
		        MSG_BODY: originalMsgBody // 이모티콘도 보이도록 수정
		    };
		    

            talkData.selectedReply.set(replyMsgData);
		    
		    return replyMsgData;
	    }
	}
})()

const talkHeader = (function () {

    let container = null;
    let moveScrollTimer = null;
    let moveTime = 200;

    function onAlarmIconClick(event) {
        let roomId = talkData.selectedRoomId.get();
        let targetRoom = talkData.roomList.get(roomId);

        if (!targetRoom)
            return;

        let inputData = {
            "dto": {
                ROOM_ID: roomId,
                USER_ID: talkData.myInfo.userId,
                ROOM_NOTIFICATION: (targetRoom["ROOM_NOTIFICATION"] === "true" ? "false" : "true")
            }
        };

        talkServer2.ttalk_updateRoomUser(inputData)
            .then(function (res) {
                targetRoom["ROOM_NOTIFICATION"] = res.data.dto["ROOM_NOTIFICATION"];
            })
    }

    function onFavoriteIconClick(event) {
        let targetRoom = talkData.roomList.get(talkData.selectedRoomId.get());
        if (!targetRoom)
            return;

        let inputData = {
            "dto": {
                ROOM_ID: targetRoom["ROOM_ID"],
                USER_ID: talkData.myInfo.userId,
                IS_FAVORITE: (targetRoom["IS_FAVORITE"] === "true" ? "false" : "true")
            }
        };

        talkServer2.ttalk_updateRoomUser(inputData)
            .then(function (res) {
                targetRoom["IS_FAVORITE"] = res.data.dto["IS_FAVORITE"];
            })
    }

    // 이제 안 써서 삭제 필요하면 ver1.코드보기
    function onSettingClick(event) {
    }

    function moveScroll(distance) {
        let container = $("#talk-header__room-list");
        container.animate({ scrollLeft: container.scrollLeft() + distance }, moveTime);
    }

    function startMoveScroll(distance) {
        if (moveScrollTimer)
            clearInterval(moveScrollTimer);

        moveScrollTimer = setInterval(function () {
            moveScroll(distance);
        }, moveTime)
    }

    function stopMoveScroll() {
        if (moveScrollTimer)
            clearInterval(moveScrollTimer);
    }

    return {
        // 		render : function(){
        // // 			let _class = (talkData.isMySpace) ? "--my-space" : "--user-group";
        // // .addClass("header__menu-icon"+_class)

        // 		},
        init: function () {
            let titleContainer = $("#talk-title")
                .append($('<div id="header__room-info"/>'))
                .append($('<div id="header__right-container"/>'))

            if (talkUtil.isSimple()) {

                let header = $("#talk-header").css({
                    "height": "2.45rem",
                    "background-color": "#F5F5F5"
                })
                
                $('#talk-title').css({
                	"margin" : "0 0.625rem",
                	"padding" : "0",
                	"border-bottom" : "0.063rem solid #D3D3D5"
                })
//                $("#talk-title-empty").css("display", "none");

// 미니채팅방 룸 리스트            
//                if (talkUtil.isMySpace()) {
//                    let roomContainer = $("<div id='talk-header__room-list-container'/>")
//                        .append(
//                            $("<div id='talk-header__prev-room-button'/>")
//                                .addClass("icon-work_previous")
//                                .on("click", function () {
//                                    moveScroll(-50);
//                                })
//                                .on("mousedown", function () {
//                                    startMoveScroll(-50);
//                                })
//                                .on("mouseup", function () {
//                                    stopMoveScroll();calc(100% - 218px)
//                                })
//                        )
//                        .append($("<div id='talk-header__room-list'/>"))
//                        .append(
//                            $("<div id='talk-header__next-room-button'/>")
//                                .addClass("icon-work_next")
//                                .on("click", function () {
//                                    moveScroll(50);
//                                })
//                                .on("mousedown", function () {
//                                    startMoveScroll(50);
//                                })
//                                .on("mouseup", function () {
//                                    stopMoveScroll();
//                                })
//                        );
//
//                    header.after(roomContainer);
//                }
            }

            $("<div/>")
                .css({
                    "display": "flex",
                    "align-items": "center"
                })
//                .append(
//                    $("<span id='header__menu-icon'/>")
//                        .addClass("icon-hamburger_menu2")
//                        .on("click", function () {
//                            $("#talk-sidebar").show();
//                            $("#talk-main").css({ "display": "none" });
                            //Top.App.routeTo("/talk-test?show=menu");
//                        })
//                )
//                .append(
//                    $('<span/>')
//                        .css({
//                            "display": (talkData.isSimple) ? "inline-block" : "none",
//                            "font-size": "0.938rem",
//                            "color": "#717376"
//                        })
//                        .addClass("icon-work_comment")
//                        .html("&nbsp")
//                )
                .append(
                    $("<span id='header__talk-title'/>")
                        .css({
                        	"display" : (talkData.isSimple || talkUtil.isMobile()) ? "block" : "none",
                			"font-size" : talkUtil.isMobile() ? "1rem" : "0.875rem",
                			"color" : "#424344",
	                        "margin-right": "0.25rem"                			
                        })
                        .text("T-Talk")
                        .i18n("MSG_TT_TITLE_BAR_TEXT_TTALK_APP_NAME")
//                            "font-size": talkData.isSimple ? "0.875rem" : "1.063rem",
//                    		"color" : talkData.isSimple ? "#424344" : "#242424"
//                            "border-right": (talkData.isSimple || !talkData.isMySpace) ? "0rem" : "0.063rem solid #D8D8D8",
//                            "padding-right": (talkData.isSimple || !talkData.isMySpace) ? "0rem" : "1.125rem",
//                            "margin-right": (talkData.isSimple || !talkData.isMySpace) ? "0.25rem" : "1rem",
//                        })
                        // .attr("data-i18n", "MSG_TT_TITLE_BAR_TEXT_TTALK_APP_NAME")
                        // .text(Top.i18n.t("MSG_TT_TITLE_BAR_TEXT_TTALK_APP_NAME"))
                )
                // 미니채팅방에서 새 채팅방 만들기 기능 삭제
//                .append(
//                    $('<span/>')
//                        .css({
//                            "display": (talkData.isSimple) ? "inline-block" : "none",
//                            "font-size": "0.938rem",
//                            "color": "#717376",
//                            "cursor": "pointer",
//                            "margin-right": (talkData.isSimple || !talkData.isMySpace) ? "0.375rem" : "0rem",
//                        })
//                        .addClass("icon-add")
//                        .on("click", function () {
//                            Top.Dom.selectById("talk__create-room-dialog").open();
//                        })
//                )
                // 즐겨찾기 기능 삭제
//                .append(
//                    $("<span id='header__talk-icon'/>")
//                        .addClass("icon-work_star")
//                        .css({
//                            "display": (talkUtil.isMobile() || !talkData.roomList.size) ? "none" : "inline-block",
//                        })
//                        .on("click", onFavoriteIconClick)
//                )
                .appendTo($('#header__room-info'));

             // 채팅방 이름 필요 없어졌음
//            $("<div id='header__name-text'/>")
//                .addClass('talk__text-truncated talk-tooltip')
//                .css({ "font-size": talkData.isSimple ? "0.875rem" : "1.125rem" })
//                .appendTo($('#header__room-info'));
            
//            $('<div/>')
//                .addClass("talk-title-right-items")
//                .css({
//                    "white-space": "nowrap"
//                })
////                .append(
////                    $("<span id='header__num-member'/>")
////                )
//                .append(
//                    $("<span id='header__alarm-on-icon'/>")
//                        .addClass("icon-work_noti_on")
//                        .on("click", onAlarmIconClick)
//                        .hide()
//                )
//                .append(
//                    $("<span id='header__alarm-off-icon'/>")
//                        .addClass("icon-work_noti_off")
//                        .on("click", onAlarmIconClick)
//                        .hide()
//                )
//                .appendTo($('#header__room-info'));


            $('#header__right-container')
                //            	.addClass("talk-title-right-items")
            	.css({
            		"display" : (talkData.isSimple) ? "none" : "flex"
            	})
                .append(
                    $("<span id='header__message-search-icon'/>")
                        .addClass("icon-work_search")
                        .on("click", function (event) {

                            $(event.target).css("display", "none");
                            $("#header__message-search").css("display", "flex");
                            $("#header__message-search__input").val("").focus();

                        })
                )
                .append(
                    $("<div id='header__message-search'/>")
                        .append(
                            $("<div id='header__message-search-container'/>")
                                .append(
                                    $("<span id='header__message-search__icon'/>").addClass("icon-work_search")
                                )
                                .append(
                                    $("<input id='header__message-search__input' type='text'/>")
                                        .focus(function () {
                                            $("#header__message-search-container").css("border", "0.063rem solid #00A4C3");
                                        })
                                        .blur(function () {
                                            $("#header__message-search-container").css("border", "0.063rem solid #CDCDCF");
                                        })
                                )
                        )
                        .append(
                            $("<span id='header__message-calendar__icon'/>").addClass("icon-prozone_date").on("click", function (event) {
                                $(event.target).css("color", "#00A4C3");
                                talkDatePicker.open("#talk-content-main", function () {
                                    // console.log(talkDatePicker.getSelected());

                                    // talkDatePicker.close();
                                });
                            })
                        )
                        .append(
                            $("<span id='header__search-cancel__icon'/>").addClass("icon-work_cancel").on("click", function (event) {
                                $("#header__message-search-icon").css("display", "inline-block");
                                $("#header__message-search").css("display", "none");
                            })
                        )
                )
                // 오른쪽에 있는 메뉴들
                .append(
                    $("<span id='header__room-member-icon'/>")
                        .addClass("icon-work_person")
                        .on("click", function (event) {
                            if (!$('#included-user-container').length) {
                                $("<div id='included-user-container' />")
                                    .html("<ul></ul>")
                                    .appendTo("#talk")
                            }
                            Top.Dom.selectById("talk__user-list-dialog").open()
                        })
                )
                .append( $('<span id="header__num-member"/>').css("font-size", talkData.isSimple ? "0.875rem" : "1.125rem"))
                .append(
                    $("<span id='header__room-file-icon'/>")
                        .addClass("icon-work_box")
                        .on("click", function () {
                            if ($('#talk__file-list').length > 0 && $('#talk__file-list').attr('data-room-id') === talkData.selectedRoomId.get()) {
                                $('#talk-main').css({ "display": "none" });
                                $('#talk__file-list').show();
                                // 계속 display : none 으로 되어있어서 추가
                                //                                    $('#talk__file-list-header span:nth-child(1)').css('display', 'inline')
                            } else {
                                $('#talk__file-list').remove()
                                // 파일리스트함 만들기
                                talk.createFileList()
                                $('#talk-main').css({ "display": "none" });
                                $('#talk__file-list').show();
                            }
                        })
                )

        }
    }
})();

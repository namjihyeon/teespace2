const talkSidebar = (function () {
    let container = null;
    let favoriteRoomContainer = null;
    let normalRoomContainer = null;

//    mobx.observe(talkData.roomList, function (change) {
//        // 미니채팅이면 탈출
//        if (talkUtil.isSimple()) {
//            return;
//        }
//
//        switch (change.type) {
//            case "add":
//                _render(favoriteRoomContainer, change.newValue);
//                _render(normalRoomContainer, change.newValue);
//                break;
//            case "update":
//                break;
//            case "delete":
//                _delete(change.oldValue);
//                break;
//            default:
//                break;
//        }
//    });
//
//    mobx.autorun(function (change) {
//        let roomId = talkData.selectedRoomId.get();
//        $(".menu-item.active").removeClass("active");
//        $(".menu-item[data-room-id='" + roomId + "']").addClass("active");
//    });

    function _delete(roomInfo) {
        $("#room-container .menu-item[data-room-id='" + roomInfo["ROOM_ID"] + "']").remove();
    }
    function _render(container, roomInfo) {

        let isFavoriteContainer = (container === favoriteRoomContainer) ? "true" : "false";
        let selectedRoomId = talkData.selectedRoomId.get();

        // item
        let menuItem = $("<div/>").addClass("menu-item").attr({ "data-room-id": roomInfo["ROOM_ID"] }).css('justify-content', 'normal');
        if (selectedRoomId === roomInfo["ROOM_ID"]) {
            menuItem.addClass("active");
        }

        // favorite icon
        let favoriteSpan = $("<span/>")
            .addClass("menu-item__favorite-icon")
            .addClass("icon-work_star")
            .appendTo(menuItem);


        // menuItem.append(favoriteSpan);
        mobx.autorun(function (change) {
            // console.warn("call ", change);
            favoriteSpan
                .css({
                    "color": (roomInfo["IS_FAVORITE"] === "true") ? "#ffca00" : "#999999"
                });

            (isFavoriteContainer === roomInfo["IS_FAVORITE"] && roomInfo["IS_SEARCH_VISIBLE"]) ? menuItem.show() : menuItem.hide();

            let isExpanded = container.hasClass("expanded");
            if (isExpanded) {
                container.css({ "max-height": (($(".menu-item:visible", container).length + 1) * 36) *0.0625+"rem" })
            }
        });

        // name 
        let nameText = $("<span/>")
            .addClass("menu-item__name-text")
            .addClass("talk-tooltip")
            .appendTo(menuItem);

        // 인원 수 표기
        let numText = $("<span/>")
            .css({
                "display": "inline-block",
                "font-size": "0.813rem",
                "color": "#00A4C3",
                "margin-right": "0.313rem"
            })
            .appendTo(menuItem);

        mobx.autorun(function (change) {
            nameText.text(roomInfo["ROOM_NICK"] ? roomInfo["ROOM_NICK"] : roomInfo["ROOM_NAME"]);
            numText.text(roomInfo["USER_NUM"]);
        });

        let rightItems = $('<div/>')
            .css({
                "display": "flex",
                "margin-left": "auto",
                "height": "100%",
                "align-items": "center"
            })
            .appendTo(menuItem)


        // alarm 
        let alarmIcon = $("<span/>")
            .addClass("menu-item__alarm-icon")
            .appendTo(rightItems);

        mobx.autorun(function (change) {
            // console.warn("call ", change);
            if (roomInfo["ROOM_NOTIFICATION"] === "true") {
                alarmIcon.removeClass("icon-work_noti_off").addClass("icon-work_noti_on").css({ "visibility": "hidden" });
            }
            else {
                alarmIcon.removeClass("icon-work_noti_on").addClass("icon-work_noti_off").css({ "visibility": "visible" });
            }
        });

        // unread Message
        //        if unread 있으면
        let unreadMsg = $("<span/>")
            //        	.addClass("menu-item__unreadMsg")
            //        	.text('4')
            .appendTo(rightItems);

        // setting 
        let settingIcon = $("<span/>")
            .addClass("menu-item__setting-icon")
            .addClass("icon-work_vertical")
            .appendTo(rightItems);

        // mobx.autorun(function (change) {
        //     settingIcon
        //         // .css({
        //         //     "display": "flex",
        //         //     "position": "absolute",
        //         //     "right": "0",
        //         //     "cursor": "pointer",
        //         //     "width": "0.625rem",
        //         //     "height": "0.625rem",
        //         //     "border-radius": "50%",
        //         //     "background-color": "orange",
        //         //     "margin-right": "0.313rem"
        //         // });
        // });
        container.append(menuItem);
    }

//    function onFavoriteIconClick(event) {
//        event.stopPropagation();
//
//        let roomId = $(this).closest(".menu-item").attr("data-room-id");
//        let targetRoom = talkData.roomList.get(roomId);
//        if (!targetRoom)
//            return;

        //        let inputData = {
        //            "dto": {
        //                "room_notification": "",
        //                "room_nick": "",
        //                "is_favorite": (targetRoom.is_favorite === "true") ? "false" : "true"
        //            }
        //        };
        //
        //        talkServer.updateUserConfig(roomId, inputData)
        //            .then(function (res) {
        //                targetRoom.is_favorite = res.data.dto.is_favorite;
        //            });

        //        let is_favorite = (targetRoom["IS_FAVORITE"] === "true") ? "0" : "1"
        //    	console.log(is_favorite)
//        let inputData = {
//            "dto": {
//                ROOM_ID: roomId,
//                USER_ID: talkData.myInfo.userId,
//                IS_FAVORITE: (targetRoom["IS_FAVORITE"] === "true" ? "false" : "true")
//            }
//        }
//        //            console.log('inputData', inputData)
//
//        talkServer2.ttalk_updateRoomUser(inputData)
//            .then(function (res) {
//                //        		console.log(res.data["dto"])
//                //                targetRoom.is_favorite = talkServer2.addLowerCaseKey1depth(res.data)["dto"].is_favorite;
//                targetRoom["IS_FAVORITE"] = res.data.dto["IS_FAVORITE"];
//            });
//    }

    function onSettingClick(event) {
        event.stopPropagation();

        $(event.currentTarget).closest(".menu-item").addClass("menu-item--hover");

        let roomId = $(this).closest(".menu-item").attr("data-room-id");
        let targetRoom = talkData.roomList.get(roomId);
        if (!targetRoom)
            return;

        // $(".menu-item").addClass("hover");
        // $(".menu-item[data-room-id='" + roomId + "']").addClass("hover");

        let isAlarmOn = (targetRoom["ROOM_NOTIFICATION"] === "true");
        if (isAlarmOn) {
            text = "알람 끄기";
        }
        else {
            text = "알람 켜기";
        }

        //        let isDefaultRoom = (targetRoom.is_default_room === "true");
        let renderData = [
            {
                "text": "이름 변경",
                "onclick": function () {
                    $("#talk__change-name-dialog").attr({ "data-room-id": targetRoom["ROOM_ID"] });
                    Top.Dom.selectById("talk__change-name-dialog").open();

                }
            },
            {
                "text": text,
                "onclick": function () {
                    onAlarmClick(targetRoom);
                }
            },
            {
                "text": "나가기",
                "onclick": function () {
                    $("#talk__exit-room-dialog").attr({ "data-room-id": targetRoom["ROOM_ID"] });
                    Top.Dom.selectById("talk__exit-room-dialog").open();
                }
            }
        ]

        //        if (!isDefaultRoom) {
        //            renderData.push({
        //                "text": "나가기",
        //                "onclick": function () {
        //                    console.log("나가기")
        //                }
        //            });
        //        }
        // console.log($(event.currentTarget).offset());
        // $(event.currentTarget).offset()

        let margin = 15;
        let talkLayoutOffset = $("#talk").offset();
        talkContextMenu.render(renderData,
            "#talk",
            {
                "top": (event.clientY + margin - talkLayoutOffset.top) +"rem",
                "left": talkUtil.isMobile() ? (($(window).width() - margin - 70 - talkLayoutOffset.left))+"rem" : (event.clientX + margin - talkLayoutOffset.left)+"rem"
            },
            // [".menu-item__setting-icon"],
            function () {
                $(".menu-item--hover", $("#talk-sidebar")).removeClass("menu-item--hover");
            });
    }


    function onAlarmClick(targetRoom) {

        // 첨부 파일 리스트를 보고 있는 경우 talk-main을 다시 띄우고 이동한다
        //        if ($('#talk-main').css('display') === 'none' ) {
        //        	$('#talk-main').css('display', 'inline-block')
        //    		$('#talk__file-list').css('display', 'none')
        //        }

        let roomId = targetRoom["ROOM_ID"];
        let isAlarmOn = (targetRoom["ROOM_NOTIFICATION"] === "true");

        let inputData = {
            "dto": {
                ROOM_ID: roomId,
                USER_ID: talkData.myInfo.userId,
                ROOM_NOTIFICATION: (targetRoom["ROOM_NOTIFICATION"] === "true" ? "false" : "true")
            }
        }

        talkServer2.ttalk_updateRoomUser(inputData)
            .then(function (res) {
                //        		console.log(res.data)
                targetRoom["ROOM_NOTIFICATION"] = res.data.dto["ROOM_NOTIFICATION"];
                //                targetRoom.room_notification = talkServer2.addLowerCaseKey1depth(res.data)["dto"].room_notification;
            });
    }

    function onRoomClick(event) {

        // 첨부 파일 리스트를 보고 있는 경우 talk-main을 다시 띄우고 이동한다
        if ($('#talk-main').css('display') === 'none') {
            $('#talk-main').css('display', 'flex')
            $('#talk__file-list').css('display', 'none')
        }


        let roomId = $(this).closest(".menu-item").attr("data-room-id");
        _selectRoom(roomId);


        if (talkUtil.isMobile()) {
            // Top.App.routeTo("/workspace/" + talkData.workspaceId + "/messenger?show=content");

            $("#talk-sidebar").hide();
            $("#talk-main").css({ "display": "flex" });
            talkContent.gotoLastMessage();
        }
        //Top.App.routeTo("/talk-test?show=content");
    }

    function _selectRoom(roomId) {
        talkData.selectedRoomId.set(roomId);
        $('div#talk').attr('data-talk-id', talkData.talkId)
        
        if (talkUtil.isMySpace()) {
            sessionStorage.setItem("lastTalkRoomId", roomId);
        }

        // mobx.set(data.selectedRoom, roomInfo);
    }

//    function searchRoom(searchString) {
//
//        talkData.roomList.forEach(function (value, key, mapObj) {
//
//            value["IS_SEARCH_VISIBLE"] = ((value["ROOM_NICK"] ? value["ROOM_NICK"] : value["ROOM_NAME"]).indexOf(searchString) !== -1) ? true : false;
//
//        })
//    }
    //  <span id='sidebar__talk-icon' class='icon-work_comment'></span>\
    return {
        init: function () {

            let sidebar = $("<div id='talk-sidebar'>\
                                <div id='sidebar__logo-container'>\
                                    <span>T-Talk</span>\
                                </div>\
                                <div id='sidebar__room-search-container'>\
                                    <input id='sidebar__room-search-input' type='text' placeholder='채팅방 검색'>\
                                    <span id='sidebar__room-search-icon' class='icon-search'></span>\
                                </div>\
                                <div id='room-container'>\
                                    <div id='sidebar__favorite-room-container'>\
                                        <div id='sidebar__favorite-title' class='sidebar__menu--inactive'>\
                                            <span class='sidebar__title-icon icon-work_close'></span>\
                                            <span>즐겨찾기</span>\
                                        </div>\
                                        <div id='sidebar__favorite-rooms' class='expanded'>\
                                        </div>\
                                    </div>\
                                    <div id='sidebar__normal-room-container'>\
                                        <div id='sidebar__normal-title' class='sidebar__menu--inactive'>\
                                            <div>\
                                                <span class='sidebar__title-icon icon-work_close'></span>\
                                                <span>채팅방</span>\
                                            </div>\
                                                <button id='sidebar__btn-create-room'>+</button>\
                                        </div>\
                                        <div id='sidebar__normal-rooms' class='expanded'>\
                                        </div>\
                                    </div>\
                                </div>\
                            </div>").hide();

            let talk = $('#talk')
                .prepend(sidebar);

            setTimeout(function () {
                container = $("#talk-sidebar", talk);
            }, 1)

            $("#sidebar__room-search-input").on("keyup", function () {
                let roomName = $(this).val();
                searchRoom(roomName);
            })
            $("#sidebar__favorite-title").on("click", function () {
                $(".sidebar__title-icon", $(this)).toggleClass("icon-work_close");
                $(".sidebar__title-icon", $(this)).toggleClass("icon-work_open");

                if (favoriteRoomContainer.hasClass("expanded")) {
                    favoriteRoomContainer.removeClass("expanded");
                    favoriteRoomContainer.css({ "max-height": "0rem" })
                    // favoriteRoomContainer.css({ "height": "0rem" })
                }
                else {
                    favoriteRoomContainer.addClass("expanded");
                    // favoriteRoomContainer.css({ "height": "6.25rem" })
                    favoriteRoomContainer.css({ "max-height": ($(".menu-item:visible", favoriteRoomContainer).length * 36) *0.0625+"rem" })
                }
            })

            $("#sidebar__normal-title").on("click", function () {
                $(".sidebar__title-icon", $(this)).toggleClass("icon-work_close");
                $(".sidebar__title-icon", $(this)).toggleClass("icon-work_open");

                if (normalRoomContainer.hasClass("expanded")) {
                    normalRoomContainer.removeClass("expanded");
                    normalRoomContainer.css({ "max-height": "0rem" })
                }
                else {
                    normalRoomContainer.addClass("expanded");
                    normalRoomContainer.css({ "max-height": ($(".menu-item:visible", normalRoomContainer).length * 36) *0.0625+"rem" })
                    // ($(".menu-item:visible", normalContainer).length * 36) *0.0625+"rem"}
                }
                // $("#sidebar__normal-rooms").toggleClass("expanded");
            })


            favoriteRoomContainer = $("#sidebar__favorite-rooms")
                .on("click", ".menu-item", onRoomClick)
                .on("click", ".menu-item__favorite-icon", onFavoriteIconClick)
                // .on("click", ".menu-item__alarm-icon", onAlarmIconClick)
                .on("click", ".menu-item__setting-icon", onSettingClick)

            normalRoomContainer = $("#sidebar__normal-rooms")
                .on("click", ".menu-item", onRoomClick)
                .on("click", ".menu-item__favorite-icon", onFavoriteIconClick)
                // .on("click", ".menu-item__alarm-icon", onAlarmIconClick)
                .on("click", ".menu-item__setting-icon", onSettingClick)

            let createRoomBtn = $("#sidebar__btn-create-room")

            createRoomBtn.on("click", function (event) {
                event.stopPropagation();
                talkData.includedUserList.clear();
                talkData.selectedUserList.clear();
                Top.Dom.selectById("talk__create-room-dialog").open();
            })
        },


        selectRoom: function (selectedRoomId) {
            _selectRoom(selectedRoomId);
        }
    }
})();
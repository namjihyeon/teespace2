const talkOpenAPI = (function () {
	
	return {
		getSelectedRoomId: function () {
//			 axios.post(talkData.serverURL +
//            "Messenger/ttalk/" + (talkData.isMySpace ? "mySpaceTtalk" : talkData.talkId) + "/ttalkrooms?action=List&user-id=" + talkData.myInfo.userId)

			// talkSidebar.render 부분에서 가져옴
			talkServer2.ttalk_getRoomList()
				.then(function (res) {
                    if (res.data.dto.TtalkRoomList) {
                        let roomList = res.data.dto.TtalkRoomList;
                        let roomCount = roomList.length;
                        
                        // roomCount가 1일 때 상정                        
                        for (let i = 0; i < roomCount; i++) {
                            talkData.selectedRoomId.set(roomList[i]["ROOM_ID"]);
                        }
                    }
				});
		},
        
        		// 다른 앱에서 메시지 보내기
		openTalkFileUpload: function (workspaceId, channelId, fileMetaList) {
			if(workspaceId == 'talk-content-main') workspaceId = talkData.workspaceId;
			talkData.fileMetaList.clear();
			talkData.fileMetaList.replace(fileMetaList.slice(0,fileMetaList.length));
			talkFileUpload.setLocalUpload(false);
			let uploadCancel = true;
			
			// 파일 전송은 한번에 30개까지
			if (talkData.fileMetaList.length > 30) {
				talkData.fileMetaList.replace(talkData.fileMetaList.slice(0,30))
	        }
			
			if (document.querySelector('top-dialog#talk__file-upload-dialog')) {
				document.querySelector('top-dialog#talk__file-upload-dialog').remove()
			}    	
			
			talkRenderer.dialog.createTopDialog("talk__file-upload-dialog", "27.5rem", "2.688rem", "채팅방 파일 첨부", "talk__dialog-layout", ()=> {
				
				Top.Dom.selectById("talk__file-upload-dialog").setProperties({
					"on-close" : function() {						
						if(fileMetaList[0].file_id){
							return;
						} else {
							if(uploadCancel) {
								let fileId;
								
								if(fileMetaList.length == 1) {
									fileId = fileMetaList[0].storageFileInfo.file_id;
									talkServer2.deleteStorageFile(talkFileUpload.deleteFileMetaFormat(fileId));
								} else {
									let fileIdArr = [];
									for(let i=0; i<fileMetaList.length; i++){
										fileId = fileMetaList[i].storageFileInfo.file_id;
										fileIdArr.push(fileId);
									}
									
									let inputDTO = {
										"dto" : {
											"workspace_id" : talkData.workspaceId,
											"channel_id" : talkData.talkId,
											"file_id" : fileIdArr
										}
									};
								
								talkServer2.deleteStorageMultiFile(inputDTO);
								}
							}
						}
					}
				});
				
				talkRenderer.dialog.renderFileUploadDialog(true, () => {					
					
					uploadCancel = false;
					if(talkFooter.getDriveAttachStatus() != true) {					
						fileMetaList["workspace_id"] = workspaceId;
						fileMetaList["channel_id"] = channelId;
					
						storageManager.CopyFile("Shallow", 
							fileMetaList[0]["file_id"], 
							channelId, 
							fileMetaList[0]["file_name"], 
							fileMetaList[0]["file_extension"],
							fileMetaList[0]["user_context_1"], 
							fileMetaList[0]["user_context_2"], 
							fileMetaList[0]["user_context_3"], 
							
							(res) => {
								if(res.dto.resultMsg === "Success"){
									const fileId = res.dto.storageFileInfoList[0]["file_id"];
									const fileName = res.dto.storageFileInfoList[0]["file_name"];
									const fileExtension = res.dto.storageFileInfoList[0]["file_extension"];
									const fileDate = res.dto.storageFileInfoList[0]["file_created_at"];
	
									let driveDTO = talkFileMeta.driveUploadDTO(workspaceId, fileId, fileName, fileExtension, fileDate);
									let driveServiceUrl = _workspace.fileUrl + "DriveFile";
									talkFileUpload.teeDriveAjaxCall(driveServiceUrl, "POST", driveDTO);
	
									// 내가 보고 있는 화면인지 체크하자.
									//////
									////
									
									for (let i=0;i< talkData.fileMetaList.length;i++) {
										const targetMessage = talkFileUpload.createTempFileMessage(i);
										// temp에 추가
										// 내가 보고 있는 방에 파일을 올리면 temp메시지를 그리지만
										// 내가 보고 있지 않은 방에 파일을 올리면 temp메시지를 그리면 안된다.
										
										const targetTalkChannelId = workspaceManager.getChannelList( workspaceId, "CHN0001" );
										if(targetTalkChannelId === talkData.talkId){
											talkData.tempMessageList.push(targetMessage);
										}
										targetMessage["ATTACHMENT_LIST"] = talkFileUpload.getAttachmentList(i);
										//우선 급한대로 다른 코드에 영향을 줄 수 있을 것 같아서 talkFIleUpload.getAttah이거를 고치지는 못했다. 향후 이걸 고쳐야함.
										//이 이슈는 dnd로 파일을 업로드한 후 원래 첨부함에서 파일을 지우면 이것도 같이 지워지는 현상이 있음.
										targetMessage["ATTACHMENT_LIST"][0].ATTACHMENT_BODY = targetMessage["ATTACHMENT_LIST"][0].ATTACHMENT_BODY.replace(fileMetaList[0]["file_id"], fileId);
										talkContent.sendMessage(targetMessage, i, workspaceId);
									}
	
									Top.Dom.selectById("talk__file-upload-dialog").close();
									talkFooter.updateButtonStatus(true);
									//DDBB
									// 	document.querySelector('#talk-content').click();
									// 	document.querySelectorAll('.lnbSpaceList.dnd__container1.selected')[0].click();
								 }
							}, 
	
							(a) => {
								console.log("***** fail : ", a)
							},
							workspaceId
						);
					} else {						
						for(let i=0; i<talkData.fileMetaList.length; i++){
							if(talkData.fileMetaList[i] == ""){
								continue;
							} else {							
								const fileId = talkData.fileMetaList[i].storageFileInfo["file_id"];
								const fileName = talkData.fileMetaList[i].storageFileInfo["file_name"];
								const fileExtension = talkData.fileMetaList[i].storageFileInfo["file_extension"];
								const fileDate = talkData.fileMetaList[i].storageFileInfo["file_created_at"];
	
								let driveDTO = talkFileMeta.driveUploadDTO(workspaceId, fileId, fileName, fileExtension, fileDate);
								let driveServiceUrl = _workspace.fileUrl + "DriveFile";
								talkFileUpload.teeDriveAjaxCall(driveServiceUrl, "POST", driveDTO);
															
								const targetMessage = talkFileUpload.createTempFileMessage(i);
//								if(channelId == talkData.talkId) talkData.tempMessageList.push(targetMessage);
								const targetTalkChannelId = workspaceManager.getChannelList( workspaceId, "CHN0001" );
								if(targetTalkChannelId === talkData.talkId){
									talkData.tempMessageList.push(targetMessage);
								}
								targetMessage["ATTACHMENT_LIST"] = talkFileUpload.getAttachmentList(i);
								talkContent.sendMessage(targetMessage, i, workspaceId);
							}
						}
						
						talkFooter.setDriveAttach([], false);
						Top.Dom.selectById("talk__file-upload-dialog").close();
						talkFooter.updateButtonStatus(true);
						//DDBB
						// 	document.querySelector('#talk-content').click();
						// 	document.querySelectorAll('.lnbSpaceList.dnd__container1.selected')[0].click();
					 }
					
				});
  		
				talkRenderer.dialog.addFileMessage();
				talkRenderer.dialog.addFileChip(talkData.fileMetaList, true)
				let count = talkData.fileMetaList.length;
				document.querySelector('#selectedItemCount').textContent = count;
			});
			
			Top.Dom.selectById("talk__file-upload-dialog").open();
	    	
			// storage 복사 서비스
			const shallowUpload = (storageFileInfo) => {
					return axios.get(talkData.serverURL + "Storage/" +
							"StorageFile?action=Copy&Type=Shallow",storageFileInfo);		
			};

			// // 클릭 이벤트를 여기서 달아준다
			// $("div#top-dialog-root_send-to-talk-dialog")
			// 	.on("click", " #talk__action-btn", function() {
					
			// 		for (let i=0;i< talkData.fileMetaList.length;i++) {
			// 			targetMessage = talkFileUpload.createTempFileMessage(i);
			// 			targetMessage["ATTACHMENT_LIST"] = talkFileUpload.getAttachmentList(i);
			// 			// 사실 storage에 업로드하지 않아도 되므로 i, parameter로 넣어줄 필요 없음
			// 			talkContent.sendMessage(targetMessage, i);
			// 		}

			// 		Top.Dom.selectById("send-to-talk-dialog").close(true);
			// 		talkData.fileChooserList.clear();
			// 		talkData.fileMetaList.clear();
			// 	});

		},
		
		updateLinkMeta: function () {
			
		}
    }
})()
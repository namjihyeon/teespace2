const talkUtil = (function() {
    return {
    	getIncludeUserResource: function(messageList){
    	    let userList = [];
    		let userMap = new Map();
    		if(messageList!==null){
    			for(let i = 0; i <messageList.length; i++){
    				if(messageList[i]["USER_ID"]){
    					userMap.set(messageList[i]["USER_ID"],messageList[i]["USER_ID"])
    				}
    				if(messageList[i]["MSG_TYPE"]==="userLeave"||messageList[i]["MSG_TYPE"]==="userEnter"){
    					let temp = messageList[i]["MSG_BODY"].split(",");
    					if(temp!==null&&temp.length>0){
    						for(let j=0; j<temp.length ; j++){
    							userMap.set(temp[j],temp[j])
    						}
    					}
    				}
    			}
    			for(let item of userMap.keys()){
    			        userList.push(item);
    			}
    			return userList;
    		}
    	},
        downloadValidFile: function(fileId) {
            talkServer2
                .validFileCheck(fileId)
                .then(function(res) {
                    if (res.data.dto.resultMsg === "Success") {
                        storageManager.DownloadFile(fileId, talkData.talkId, talkData.workspaceId);
                    } else {
                    	TeeToast.open({text: "손상되었거나 삭제된 파일입니다."});
                    }
                })
                .catch(function(err) {
                	TeeToast.open({text: "손상되었거나 삭제된 파일입니다."});
                });
        },
        
        // 안에 있는 노드 지우기
        removeChildrenNodes: function(elem) {
        	while (elem.lastChild) {
        		elem.removeChild(elem.lastChild);
        	}
        },

        getByteLength: function(s) {
            let b, i, c;
            for (b = i = 0; (c = s.charCodeAt(i++)); b += c >> 11 ? 3 : c >> 7 ? 2 : 1);
            return b;
        },

        sliceByteLength: function(s, maxByte) {
            let b, i, c;
            for (b = i = 0; (c = s.charCodeAt(i)); i++) {
                b += c >> 11 ? 3 : c >> 7 ? 2 : 1;
                if (b >= maxByte) {
                    break;
                }
            }
            return s.substring(0, i);
        },


        downloadFile: function(fileId) {
            let _this = $(this);

            var successCallback = function(response) {
                // // console.warn(result)
                if (response.result != "OK") {
                    return;
                }
                if (Top.Util.Browser.isIE()) {
                    window.navigator.msSaveBlob(response.blobData, response.fileName);
                } else {
                    let a = document.createElement("a");
                    a.href = response.url;
                    a.download = response.fileName;
                    a.click();
                }

                URL.revokeObjectURL(response.url);
            };
            var errorCallback = function(response) {
                // console.warn("download err")
            };

            // 수정 요망

            fileManager.getDownloadFileInfo(fileId, successCallback, errorCallback);
        },
        // img tag용
        getFormatedFullTime: function(time) {
            let date = new Date(parseInt(time));
            try {
                let month = String(date.getMonth() + 1).padStart(2, "0");
                let _date = String(date.getDate()).padStart(2, "0");
                let hour = date.getHours() < 13 ? date.getHours() : date.getHours() - 12;
                let minutes = String(date.getMinutes()).padStart(2, "0");
                //            console.log('img tag용 input', time) ->1574648546344
                if (date.getHours() >= 12) {
                    date = date.getFullYear() + "." + month + "." + _date + " 오후 " + hour + ":" + minutes;
                } else {
                    date = date.getFullYear() + "." + month + "." + _date + " 오전 " + hour + ":" + minutes;
                }

                return date;
            } catch (e) {
                return date;
            }
        },

        // 파일첨부함용-storage 2019/11/25 11:22:26으로 주는 것에 맞춘 버전
        getFormatedStorageTime: function(time) {
            // 혹시 time format이 바뀔 것 대비
            // time이 string임, "2019/11/25 11:22:26" 형태
            try {
                let today = new Date();
                let date = new Date(time);

                // 시간 얻기, 시 : 분
                let temp = time.split(" ")[1].split(":");
                let displayTime;
                let hour = date.getHours() < 13 ? date.getHours() : date.getHours() - 12;
                let minutes = String(date.getMinutes()).padStart(2, "0");

                if (date.getHours() >= 12) {
                    displayTime = " 오후 " + hour + ":" + minutes;
                } else {
                    displayTime = " 오전 " + hour + ":" + minutes;
                }

                let _date = time.split(" ")[0].split("/");
                // 날짜가 같으면 시:분만 출력, 그렇지 않으면 달.일 시:분 출력
                if (today.getFullYear() === date.getFullYear()) {
                    if (today.getMonth() === date.getMonth() && today.getDate() === date.getDate()) {
                        return displayTime;
                    } else {
                        return _date[1].padStart(2, "0") + "." + _date[2].padStart(2, "0") + "." + displayTime;
                    }
                } else {
                    return (
                        _date[0] + "." + _date[1].padStart(2, "0") + "." + _date[2].padStart(2, "0") + "." + displayTime
                    );
                }
            } catch (e) {
                return time;
            }
        },

        getCustomFormatDate: function(time) {
            let week = [
                //   Top.i18n.t("talk.sunday"),
                //   Top.i18n.t("talk.monday"),
                //   Top.i18n.t("talk.tuesday"),
                //   Top.i18n.t("talk.wednesday"),
                //   Top.i18n.t("talk.thursday"),
                //   Top.i18n.t("talk.friday"),
                //   Top.i18n.t("talk.saturday")
                "일요일",
                "월요일",
                "화요일",
                "수요일",
                "목요일",
                "금요일",
                "토요일"
            ];

            let curTime = new Date();
            let curYear = curTime.getFullYear();
            let curMonth = curTime.getMonth() + 1;
            let curDate = curTime.getDate();

            let _time = new Date(time * 1);
            let year = _time.getFullYear();
            let month = _time.getMonth() + 1;
            let date = _time.getDate();
            let dayLabel = _time.getDay();

            if (curYear === year && curMonth === month && curDate === date)
                return /* Top.i18n.t("talk.today"); */ "오늘";
            else return year + "." + month + "." + date + " " + week[dayLabel];
        },

        // 위 함수는 이미 다른 곳에서 사용중이어서 변경이 어려우므로
        convertTimestampToCustumFormat: function(time) {
            let week = [
                //   Top.i18n.t("talk.sunday"),
                //   Top.i18n.t("talk.monday"),
                //   Top.i18n.t("talk.tuesday"),
                //   Top.i18n.t("talk.wednesday"),
                //   Top.i18n.t("talk.thursday"),
                //   Top.i18n.t("talk.friday"),
                //   Top.i18n.t("talk.saturday")
                "일요일",
                "월요일",
                "화요일",
                "수요일",
                "목요일",
                "금요일",
                "토요일"
            ];

            let curTime = new Date();
            let curYear = curTime.getFullYear();
            let curMonth = curTime.getMonth() + 1;
            let curDate = curTime.getDate();

            let _time = new Date(time);
            let year = _time.getFullYear();
            let month = _time.getMonth() + 1;
            let date = _time.getDate();
            let dayLabel = _time.getDay();

            if (curYear === year && curMonth === month && curDate === date)
                return /* Top.i18n.t("talk.today"); */ "오늘";
            else return year + "." + month + "." + date + " " + week[dayLabel];
        },
        
        //다른 모듈에서 이미 우리 halfday를 쓰고 있음        
        convertTimestampToTimeStr: function(timestamp){
        	let date = new Date(timestamp);
            let present = new Date();
        	let hour = date.getHours();
            let ans;

            // 1. Get meridiem
            // TODO : i18n
            // 모바일이면 am, pm 사용
            let ante = "오전"; 
            let post = "오후";
            if (talkUtil.isMobile()) {
            	ante = "am";
            	post = "pm";
            }
            
            meridiem = hour >= 12 ? "오후" : "오전";

            // 2. 시간을 0~23 시 -> 1~24시
            if (hour === 0) hour = 24;

            // 3. 1~24시 -> 오전/오후 1~12시
            if (hour > 12) hour -= 12;

            ans = meridiem + " " + ("0" + hour).slice(-2) + ":" + ("0" + date.getMinutes()).slice(-2);
            
            if((date.getDate()!==present.getDate())||(date.getMonth()!==present.getMonth()) || (date.getYear()!==present.getYear()))
            {
            	   let targetDay = new Date(timestamp);
            	   let year = targetDay.getFullYear();
                   let month = targetDay.getMonth()+1;
                   if(month<10){
                   		month = "0" + month;
                   }
                   let date = targetDay.getDate();
                   if(date<10){
                	   date = "0" + date;
                  }
                   ans = month+"-"+date+" "+ans;
            }
            return ans;
        },
        
        getHalfDayFormat: function(time) {
            let meridiem = "";
            let date = new Date(time * 1);
            let hour = date.getHours();

            // 1. Get meridiem
            // TODO : i18n
            // 모바일이면 am, pm 사용
            let ante = "오전"; 
            let post = "오후";
            if (talkUtil.isMobile()) {
            	ante = "am";
            	post = "pm";
            }
            
            meridiem = hour >= 12 ? "오후" : "오전";

            // 2. 시간을 0~23 시 -> 1~24시
            if (hour === 0) hour = 24;

            // 3. 1~24시 -> 오전/오후 1~12시
            if (hour > 12) hour -= 12;

            return meridiem + " " + ("0" + hour).slice(-2) + ":" + ("0" + date.getMinutes()).slice(-2);
        },

        generateNumber: function() {
            let _min = 10000; 
            let _max = 99999; 
            return Math.floor(Math.random() * (_max - _min + 1)) + _min; 
        },
        
        generateGUID: function() {
            function s4() {
                return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
            }
            return s4() + s4() + "-" + s4() + "-" + s4() + "-" + s4() + "-" + s4() + s4() + s4();
        },

        parseInt: function(value) {
            return parseInt(value, 10) || 0;
        },

        //해당 함수가 실패시 return 을 false로 주기 때문에 직접 출력할 수 있는 알 수 없을을 띄워주기 위해 한번 감싸두었다.
        getUserNick: function(userId) {
        	let result = userManager.getUserName(userId);
        	if(result){
        		return result;
        	}
        	else{
        		return "알 수 없음";
        	}
        },

        isMySpace: function() {
            return talkData.isMySpace;
        },

        isSimple: function() {
            return talkData.isSimple;
        },

        isMobile: function() {
            return window.matchMedia("(max-width:1024px)").matches;
        },

        getFileFormatSize: function(x) {
        	if(x !== "0"){
        		var s = ["bytes", "KB", "MB", "GB", "TB", "PB"];
                var e = Math.floor(Math.log(x) / Math.log(1024));
                var result = (x / Math.pow(1024, e)).toFixed(2);
                if (result.endsWith(0)) {
                    result = result.substring(0, result.length - 1);
                }
                return result + s[e];
        	} else {
        		return "0 byte"
        	}
            
        },

        createLinkAttachment: function(parentMsgId, serverMsgFormat) {
            let isIncludeLink = function(serverMsgFormat) {
                let regex = /<{"data-type":"url"(?:.*?)"data-url":"(.*?)"}>/g;
                return regex.test(serverMsgFormat);
            };

            let getLinkURL = function(serverMsgFormat) {
                let urlList = [];
                let regex = /<{"data-type":"url"(?:.*?)"data-url":"(.*?)"}>/g;

                let result = null;
                while ((result = regex.exec(serverMsgFormat))) {
                    urlList.push(result[1]);
                }
                return urlList;
            };

            if (isIncludeLink(serverMsgFormat)) {
                let urlList = getLinkURL(serverMsgFormat);
                if (urlList && urlList.length) {
                    // attachment 추가
                    let attachmentList = urlList.map(function(elem) {
                        return {
                            MSG_ID: parentMsgId,
                            ATTACHMENT_TYPE: "og",
                            ATTACHMENT_BODY: elem
                        };
                    });

                    talkServer2.ttalk_createAttachment(parentMsgId, attachmentList);
                }
            }
        },

        // loadJsFiles : function(path, fileNames){
        //     fileNames.forEach(function(fileName){
        //         this.loadJsFiles(path, fileName)
        //     });
        // },

        // loadJsFile: function(path, fileName, callback) {
        //     let script = document.createElement("script");
        //     script.src = url;
        //     script.onload = function() {
        //         callback();
        //     };
        //     document.head.appendChild(script);

        //     // let fileList = fileNameList.map(function(fileName) {
        //     //     return path + fileName;
        //     // });
        //     // Top.App.loadScript(fileList, function() {
        //     //     console.log("aa");
        //     // });
        // }

        loadJsFiles: function(path, loadFiles) {
            let promises = [];
            let fileNameList = Object.keys(loadFiles);

            for(let i=0; i<fileNameList.length; i++){
                let fileName = fileNameList[i];
                if(!loadFiles[fileName]){
                    promises.push(new Promise(function(resolve, reject) {
                        var script = document.createElement("script");
                        script.onload = resolve;
                        script.onerror = reject;
                        script.src = path + fileName;
                        document.head.appendChild(script);
                        loadFiles[fileName] = true;
                    }));
                }
            }

            return Promise.all(promises);
        }
    };
})();

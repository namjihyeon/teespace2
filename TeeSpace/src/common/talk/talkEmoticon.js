const talkEmoticon = (function () {
    let activatedTabIndex = null;
    let config = {
        "path": "res/emoticon/",
        "0": {
            "type": null,
            "count": 0,
            "unit": -20,
            "size": 0,
            "files": []
        },

        "1": {
            "type": "small",
            "count": 48,
            "unit": -20,
            "size": 5000,
            "files": [{ "name": "emoticon_01", "extension": "png" }],
        },
        
        "2": {
            "type": "big",
            "count": 9,
            "unit": 0,
            "size": 100,
            "files": [
            	{ "name": "emoticon_05_01", "extension": "gif" },
            	{ "name": "emoticon_05_02", "extension": "gif" },
            	{ "name": "emoticon_05_03", "extension": "gif" },
            	{ "name": "emoticon_05_04", "extension": "gif" },
            	{ "name": "emoticon_05_05", "extension": "gif" },
            	{ "name": "emoticon_05_06", "extension": "gif" },
            	{ "name": "emoticon_05_07", "extension": "gif" },
            	{ "name": "emoticon_05_08", "extension": "gif" },
            	{ "name": "emoticon_05_09", "extension": "gif" }
            ],
        },

        "3": {
            "type": "big",
            "count": 9,
            "unit": 0,
            "size": 100,
            "files": [
                { "name": "emoticon_02_03", "extension": "gif" },
                { "name": "emoticon_02_04", "extension": "gif" },
                { "name": "emoticon_02_05", "extension": "gif" },
                { "name": "emoticon_02_06", "extension": "gif" },
                { "name": "emoticon_02_07", "extension": "gif" },
                { "name": "emoticon_02_08", "extension": "gif" },
                { "name": "emoticon_02_09", "extension": "gif" },
                { "name": "emoticon_02_10", "extension": "gif" },
                { "name": "emoticon_02_11", "extension": "gif" }
            ],
        },

        "4": {
            "type": "big",
            "count": 5,
            "unit": -60,
            "size": 500,
            "files": [{ "name": "emoticon_03", "extension": "png" }]
        },

        "5": {
            "type": "big",
            "count": 5,
            "unit": -60,
            "size": 500,
            "files": [{ "name": "emoticon_04", "extension": "png" }]
        }
    };

    function _create(targetSelector) {
        let popup = $("<div id='talk__emoticon-popup'/>")
            .append($("<div id='talk__emoticon-popup__tab'/>"))
            .append($("<div id='talk__emoticon-popup__content'/>"))
            .appendTo(targetSelector)
            .hide();


        // layout
        let tabClassList = [
            "icon-work_filter",
            "icon-work_smile",
            "icon-cloud_good",
            "icon-work_emoji_1",
            "icon-work_emoji_2",
            "icon-work_emoji_3",
            // "icon-work_settings"
        ]
        let tabClassLength = tabClassList.length;
        let tabFrag = $(document.createDocumentFragment())
        let pageFrag = $(document.createDocumentFragment());

        // TODO : 0 ~ length
        for (let i = 1; i < tabClassLength; i++) {
            // for (let i = 0; i < tabClassLength; i++) {
        	let _tab = document.createElement("div")
        	_tab.classList.add("emoticon-popup__tab-item");
        	_tab.setAttribute("data-tab-index", i);
        	tabFrag[0].appendChild(_tab)
        	
        	let _tabIcon = document.createElement("span")
        	_tabIcon.classList.add("emoticon-popup__tab-item-icon")
        	_tabIcon.classList.add(tabClassList[i])
        	_tab.appendChild(_tabIcon)
            $("<div/>").addClass("emoticon-popup__content-page").attr({ "data-page-index": i }).append(_getPage(i)).appendTo(pageFrag);
        }
        tabFrag.appendTo($("#talk__emoticon-popup__tab", popup));
        pageFrag.appendTo($("#talk__emoticon-popup__content", popup));


        // event
        $("#talk__emoticon-popup__tab").on("click", ".emoticon-popup__tab-item", function (event) {
            let tabIndex = $(this).attr("data-tab-index");
            talkEmoticon.selectTab(tabIndex);
        });
    }

    function _getPage(pageIndex) {
        let emoticonContainer = $("<div/>").addClass("emoticon-popup__emoticon-container");
        let frag = $(document.createDocumentFragment());
        let unit = config[pageIndex].unit;
        let count = config[pageIndex].count;
        let offset = 0;

        // 모바일 창 크기에 맞추어 이모티콘 4개씩 배열해야해
        // 패딩 30 + bg 각 이모티콘당 +6 * 이모티콘 개수(4) + 마진 각 이모티콘당 4*4
        let containerWidth = $('#talk-footer').width() - 70
        let setWidthHeight = config[pageIndex].type === "big" ? ((talkUtil.isMobile()) ? String(containerWidth/4)*0.0625+"rem" : "3.75rem") : "";
        let setBgWidthHeight = config[pageIndex].type === "big" ? ((talkUtil.isMobile()) ? String(containerWidth/4 + 6)*0.0625+"rem" : "4.125rem") : "";
        
        switch (pageIndex) {
            // 일반 이모티콘
            case 1:
            case 4:
            case 5:
                for (let i = 0; i < count; i++) {
                	let _item = $("<span/>").addClass("emoticon-popup__emoticon-item--" + config[pageIndex].type).css({
                    	"width" : setWidthHeight,
            			"height" : setWidthHeight,
                        'background': 'url(' + config.path + config[pageIndex].files[0]["name"] + "." + config[pageIndex].files[0]["extension"] + ') ' + offset + 'rem 0rem / ' + config[pageIndex].size + '% no-repeat content-box',
                    });
                	
                	let _itemBackground = $("<span/>")
	                    .addClass("emoticon-popup__emoticon-item__background--" + config[pageIndex].type)
	                    .attr({
	                        "data-file-name": config[pageIndex].files[0]["name"],
	                        "data-file-extension": config[pageIndex].files[0]["extension"],
	                        "data-coordinate": offset *0.0625+"rem 0rem",
	                        // "data-size": (config[pageIndex].type === "small") ? "1.25rem" : "3.75rem",
	                        "data-size": config[pageIndex].type,
	                        "data-scale": config[pageIndex].size,
	                        "data-index": i,
	                    })
	                    .css({
	                    	"width" : setBgWidthHeight,
	            			"height" : setBgWidthHeight
	                    })
	                    .appendTo(frag);
                    
                	_item.appendTo(_itemBackground);
                	
                    if (pageIndex !== 1) {
                    	// "rem" 문자 제거
                    	offset -= Number(setWidthHeight.substr(0,setWidthHeight.length-3))
                    } 
                    // 음수이고 절대값이 1.25rem씩 커지는게 이모티콘이 잘리지 않고 온전히 나오는 것 같다
                    else{
                    	offset -= 1.25;
                    }
                }
                break;

            // gif 이모티콘
            case 2:
            case 3:
                for (let i = 0; i < count; i++) {
                	let _item = $("<span/>").addClass("emoticon-popup__emoticon-item--" + config[pageIndex].type).css({
                    	"width" : setWidthHeight,
            			"height" : setWidthHeight,
                        'background': 'url(' + config.path + config[pageIndex].files[i]["name"] + "." + config[pageIndex].files[i]["extension"] + ')  0rem 0rem / ' + config[pageIndex].size + '% no-repeat content-box',
                    });
                	
                	let _itemBackground =  $("<span/>")
	                    .addClass("emoticon-popup__emoticon-item__background--" + config[pageIndex].type)
	                    .attr({
	                        "data-file-name": config[pageIndex].files[i]["name"],
	                        "data-file-extension": config[pageIndex].files[i]["extension"],
	                        "data-coordinate": "0rem 0rem",
	                        // "data-size": (config[pageIndex].type === "small") ? "1.25rem" : "3.75rem",
	                        "data-size": config[pageIndex].type,
	                        "data-scale": config[pageIndex].size,
	                        "data-index": "0"
	                    })
	                    .appendTo(frag);
                    
                	_item.appendTo(_itemBackground);  
                }
                break;

            // 세팅 페이지
            case 6:
                return $("<span/>").text("세팅 : 기획 필요");

            // 뭔 페이지??
            case 0:
                return $("<span/>").text("최근사용 : 기획 필요");
            default:
                break;
        }


        return emoticonContainer.append(frag).on("click", "span[data-file-name]", function (event) {

            let _this = $(this);
            let target = $(_this[0].firstElementChild);
            let isBig = (_this.attr("data-size") === "big");

            let imageIndex = _this.attr("data-index");
            
            let img = $("<img/>")
                .attr({
                    "src": "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7",
                    "contenteditable": "true",
                    "data-type": "emoticon",
                    "data-file-name": _this.attr("data-file-name"),
                    "data-file-extension": _this.attr("data-file-extension"),
                    "data-coordinate": _this.attr("data-coordinate"),
                    "data-scale": _this.attr("data-scale"),
                    "data-size": _this.attr("data-size"),
                    "data-index": _this.attr("data-index"),
                })
                .css({
                    "display": "inline-block",
                    "vertical-align": "bottom",
                    "width": (isBig) ? "5rem" : "1.25rem",
                    "height": (isBig) ? "5rem" : "1.25rem",
                    "background": target.css("background"),
                    "background-position-x": (isBig) ? (imageIndex * 0.0625 * -80 +"rem") : (imageIndex * 0.0625 * -20 +"rem"),
            		"margin-left" : "auto"
                });

            // let isFocusing = (talkUtil.isMobile()) ? false: true;
            if (isBig) {
                talkData.selectedEmoticonTag.set(img[0].outerHTML);
                talkFooter.appendText("");
            }
            else {
                talkFooter.appendHTML(img[0]);
                talkEmoticon.close();
            }
        });
    }

    function _isExist() {
        // $("#emoticon-popup").remove();
        return !!$("#talk__emoticon-popup").length;
    }

    function _closeEvent() {
        if (!$(event.target).closest("#talk__emoticon-popup").length && !$(event.target).closest("#footer__emoticon-button").length){
            talkEmoticon.close();
        }
    }

    function _selectTab(index) {

        if (activatedTabIndex !== null) {
            $(".emoticon-popup__tab-item[data-tab-index='" + activatedTabIndex + "']").removeClass("emoticon-popup__tab-item--active")
            $(".emoticon-popup__content-page[data-page-index='" + activatedTabIndex + "']").hide();
        }
        activatedTabIndex = index;
        $(".emoticon-popup__tab-item[data-tab-index='" + activatedTabIndex + "']").addClass("emoticon-popup__tab-item--active");
        $(".emoticon-popup__content-page[data-page-index='" + activatedTabIndex + "']").show();
    }
    
    return {
        open: function (appendTarget, css) {
            let replyView= document.querySelector('#talk-footer__reply-view');
        	if (replyView && replyView.style.display === "flex") {
                replyView.style.display = "none";
        	}
            if (!_isExist()) {
                _create(appendTarget);
            }
            else{
                let popup = $("#talk__emoticon-popup").detach();
                $(appendTarget).append(popup);
            }
            $("#talk__emoticon-popup").css({ "top": "", "bottom": "", "left": "", "right": "" }).css(css).show();
            
            document.querySelector('#talk-footer__emoticon-button').classList.add("talk-img__filter");
            // this.selectTab(0);
            this.selectTab(1);
            
            setTimeout(function() {
    			$(document).on("click", _closeEvent)
            },100 );
        },

        close: function () {
            if (_isExist()) {
                this.selectTab(1);
                activatedTabIndex = null;
                $(document).off("click", _closeEvent);
                $("#talk__emoticon-popup").hide();
                //                $("#talk__emoticon-popup").remove();

                if (talkUtil.isMobile()) {
                    $("#talk-content-main").css("height", ($("#talk-content-main").height() + 190) *0.0625+"rem");
                }
                document.querySelector('#talk-footer__emoticon-button').classList.remove("talk-img__filter");
            }
        },

        selectTab: function (index) {
            _selectTab(index);
        },

        getPath: function () {
            return config.path;
        }
    }
})();


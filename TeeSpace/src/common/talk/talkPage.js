const talkPage = (function() {
    // global JavaScript variables
    let list = new Array();
    let pageList = new Array();
    let currentPage = 1;
    // default : 20
    let perPage = 20;
    let numPage = 1; // calculates the total number of pages
    let roomId = "";
    let fileMetaList = [];
    let fileCount = 0;

    function check() {
        document.getElementById("next").disabled = currentPage == numPage ? true : false;
        document.getElementById("previous").disabled = currentPage == 1 ? true : false;
        document.getElementById("first").disabled = currentPage == 1 ? true : false;
        document.getElementById("last").disabled = currentPage == numPage ? true : false;
    }

    function loadTable() {
        if (fileCount === 0) {
            $("#talk__file-list-table tbody")
                .empty()
                .append(
                    $("<tr/>").append(
                        $('<td colspan="6"/>')
                            .css("text-align", "center")
                            .append(
                                $("<span/>")
                                    .text("파일이 없습니다.")
                                    .css({ "text-align": "center", height: "2.688rem" })
                            )
                    )
                );
        } else {
            let begin = (currentPage - 1) * perPage;
            let end = begin + perPage;
            pageList = fileMetaList.slice(begin, end);
            drawList();
        }

        // button disable
        check();
    }

    function drawList() {
        $("#talk__file-list-table tbody").empty();

        for (let i = 0; i < pageList.length; i++) {
            let fileMeta = pageList[i];

            //            fileMeta = {
            //                    "file_id" : "flksdjg;ilejr;gio",
            //                    "file_name" : "파일이름파일이름파일이름파일이름파일이름파일이름파일이름파일이름파일이름파일이름파일이름파일이름daksjdglajgasg",
            //                    "file_extension" : '이미지',
            //                    "fileFullName" : "파일이름.jpg", - result에 없음
            //                    "file_size" : "10KB",
            //                    "file_updated_at" : "2019.05.12 오전 11:00",
            //                    "file_owner" : "홍길동"
            //            }

            //	        let isOdd = "";
            //	        if (i % 2){
            //	            isOdd = "talk__border_bottom";
            //	        }

            let iconClass = getFileIconClass(fileMeta["file_extension"]);
            let iconColor = "#595B5D";
            switch (iconClass) {
                case "icon-work_folder":
                    iconColor = "#FFC412";
                    break;
                case "icon-work_video_file":
                    iconColor = "purple";
                    break;
                case "icon-work_img_file":
                    iconColor = "#00A4C3";
                    break;
                case "icon-work_pdf":
                    iconColor = "#4E4F84";
                    break;
                case "icon-work_toword":
                    iconColor = "#439BED ";
                    break;
                case "icon-work_topoint":
                    iconColor = "red";
                    break;
                case "icon-work_tocell":
                    iconColor = "#35BC68";
                    break;
            }
            
        
            let fileDate = fileMeta["file_updated_at"] ? talkUtil.getFormatedStorageTime(fileMeta["file_updated_at"]) : talkUtil.getFormatedStorageTime(fileMeta["file_created_at"]);

            $("#talk__file-list-table tbody").append(
                $("<tr/>")
                    .attr({ "data-file-id": fileMeta["file_id"], "data-attachment-id": fileMeta["attachment_id"] })
                    .append(
                        $("<td/>")
                            .css({
                                "text-align": "center"
                            })
                            .append(
                                $('<input type="checkbox" />')
                                    .addClass("talk__input_check")
                                    .on("click", function() {
                                        // chanaged value가 들어온다
                                        if ($(this).prop("checked")) {
                                            $("#talk__file-download").attr("disabled", false);
                                            $("#talk__file-delete").attr("disabled", false);
                                        } else {
                                        	$('#check_all').prop("checked", false)
                                            $("input.talk__input_check:checkbox").each(function() {
                                                if ($(this).prop("checked")) {
                                                    $("#talk__file-download").attr("disabled", false);
                                                    $("#talk__file-delete").attr("disabled", false);
                                                    return false;
                                                }
                                                // true인게 없는 경우 모두 disabled 처리
                                                $("#talk__file-download").attr("disabled", true);
                                                $("#talk__file-delete").attr("disabled", true);
                                            });
                                        }
                                    })
                            )
                    )
                    .append(
                        $("<td/>")
                            .css({
                                "font-size": "1.25rem",
                                "text-align": "center",
                                color: iconColor
                            })
                            .addClass(iconClass)
                    )
                    .append(
                        $("<td/>")
                            .addClass("talk__file-item-name")
                            .append(
                        		$('<div/>')
                        			.addClass("talk-tooltip")
                        			.css({"display":"flex", "align-items" : "center"})
                        			.append($('<div/>').text(fileMeta["file_name"] + "." + fileMeta["file_extension"]))
                        			.append(
		                        		$('<img/>').attr("src", "./res/ic_t-drive_upload.svg").css("margin-left","0.5rem")
		                        		.click(function(){
		                        			//[TODO]meta를 넘길 때 file id 만 넘겨도 되는가? 주의
		                        			SEND_TO_TDRIVE({
		                        				fileMeta : {
		                                              file_id: $(this).closest('tr').attr('data-file-id')
//		                                              file_name: null,
//		                                              file_extension: null,
//		                                              fileFullName: null,
//		                                              file_size: null,
//		                                              file_updated_at: null,
//		                                              file_owner: null      
		                        				},
		                        				successCallback : function() {
		                        					notiFeedback('T-Drive로 보내기에 성공했습니다');
		                        				},
		                        				cancelCallback : function() {
//		                        					notiFeedback('실패했습니다');
		                        				}
		                        			})
		                        		})
		                        		.css("cursor","pointer")
		                            )
                            )
//                            .html(fileMeta["file_name"] + "." + fileMeta["file_extension"])                            
                            .css({
                                //	                                "padding-right" : "3%",
                                padding: "0 0.938rem"
                                //                                    "cursor" : "pointer"
                            })
                    )
                    .append(
                        $("<td/>")
                            .addClass("talk-tooltip")
                            .css({
                                padding: "0 0.938rem"
                            })
                            .html(fileMeta["file_size"] ? getFileSizeText(parseInt(fileMeta["file_size"])) : "&nbsp")
                    )
                    .append(
                        $("<td/>")
                            .addClass("talk-tooltip")
                            .css({
                                padding: "0 0.938rem"
                            })
                            .text(fileDate)
                        )
                    .append(
                            $("<td/>")
                                .addClass("talk-tooltip")
                                .css({
                                    padding: "0 0.938rem"
                                })
                                .text(talkUtil.getUserNick(fileMeta["file_last_update_user_id"]))
                        )
                )
                    
        }

        document.getElementById(String(currentPage)).style.color = "rgb(0, 164, 195)";
    }

    function firstPage() {
        if (fileCount == 0) {
            return;
        }
        document.getElementById(String(currentPage)).style.color = "#8C8E92";
        currentPage = 1;
        loadTable();
        document.getElementById("1").style.color = "rgb(0, 164, 195)";
    }

    function lastPage() {
        if (fileCount == 0) {
            return;
        }
        document.getElementById(String(currentPage)).style.color = "#8C8E92";
        currentPage = numPage;
        loadTable();
        document.getElementById(String(numPage)).style.color = "rgb(0, 164, 195)";
    }

    return {
        getRoomId: function() {
            return roomId;
        },

        getPerPage: function() {
            return perPage;
        },

        getCurrentPage: function() {
            return currentPage;
        },

        setCurrentPage: function(num) {
            currentPage = num;
            for (let i=1; i<=numPage; i++) {
            	if (i === num) {
            		document.getElementById(String(i)).style.color = "rgb(0, 164, 195)";
            	} else {
            		document.getElementById(String(i)).style.color = "rgb(140, 142, 146)";
            	}
            }
            
            loadTable();
        },

        //		getFiles : function () {
        //			setFileManagerChannelId(talkData.selectedRoomId.get())
        //			let result = fileManager.getFileList()
        //			fileMetaList = result.fileMetaList
        //			fileCount = fileMetaList.length
        //		},

        //		getAttachmentFiles : function () {
        //			let result = talkServer2.ttalk_getAttachmentList(talkData.selectedRoomId.get());
        //			fileMetaList=[];
        //			for(let i =0; i<result.length; i++){
        //				let temp = talkContent.talk_parseFile(result[i]["ATTACHMENT_BODY"]);
        //				if (!temp) {
        //					continue
        //				}
        //				let fileMeta =
        //				{
        //                    file_id : temp["data-file-id"],
        //                    file_name : temp["data-file-name"],
        //                    file_extension : temp["data-file-extension"],
        //                    fileFullName : temp["data-file-name"],
        //                    file_size : temp["data-size"],
        //                    file_updated_at : temp["data-updated-at"],
        //                    file_owner : userManager.getUserInfo(temp["data-user-id"])["USER_NAME"],
        //                    attachment_id : result[i]["ATTACHMENT_ID"]
        //			    }
        //
        //				fileMetaList.push(fileMeta);
        //
        //			}
        //
        //			fileCount = fileMetaList.length;
        //			fileMetaList.sort(function(a,b) {
        //				return (parseInt(b["file_updated_at"]) - parseInt(a["file_updated_at"]))
        //			})
        //			return fileMetaList;
        //
        //		},

        getAttachmentFiles: function() {
            let result = [];
            talkServer2.getStorageFiles(talkData.selectedRoomId.get()).then(function(res) {
            	fileMetaList = res.data.dto["storageFileInfoList"];
                return fileMetaList;
            });
        },
        // TODO : T-Drive에서 내보낼 수 있는 파일인지 판별할 때 사용
        getTDriveFileList : function() {
        	let fileList = TDrive.getFileList()["fileMetaList"];
        },
        
        // storage로부터 받아오기
//        getResult: function(res) {
//            result = res.data.dto["storageFileInfoList"];
//            fileMetaList = [];
//            fileCount = 0;
//            if (result) {
//                fileCount = result.length;
//                for (let i = 0; i < fileCount; i++) {
//                    let temp = result[i];
//                	console.log(temp)
//                    let fileMeta = {
//                        file_id: temp["file_id"],
//                        file_name: temp["file_name"],
//                        file_extension: temp["file_extension"],
//                        fileFullName: temp["data-file-name"],
//                        file_size: temp["file_size"],
//                        file_updated_at: temp["file_created_at"] || temp["file_updated_at"],
//                        file_owner: talkUtil.getUserNick(temp["file_last_update_user_id"])
//                    };
//
//                    fileMetaList.push(fileMeta);
//                }
//            }
//            return fileMetaList;
//        },

        getPage: function() {
            talkServer2.getStorageFiles(talkData.selectedRoomId.get()).then(function(res) {
//                talkPage.getResult(res);            	
            	fileMetaList = res.data.dto["storageFileInfoList"];
            	if (!fileMetaList || !fileMetaList.length) {
            		fileCount = 0;
            		// attachment 개수가 0개일 때
                    talkPage.getNumPages();
            	}
            	else {
            		fileCount = fileMetaList.length;
                    if (talkData.selectedRoomId.get() === talkPage.getRoomId()) {
                        curPage = talkPage.getCurrentPage();
                        perPage = talkPage.getPerPage();
                        //            talkPage.getFiles()
                        talkPage.getNumPages();
                        $("#talk__file-num").val(perPage);
                        talkPage.changeItemNum(Number(perPage));
                        if (numPage < curPage) {
                        	talkPage.setCurrentPage(numPage);
                        } else {
                        	talkPage.setCurrentPage(curPage);
                        }
                    } else {
                        //		        	talkPage.getAttachmentFiles() -> 이미 앞에서 새로 받아옴
                        talkPage.getNumPages();
                    }
            	}
            	
            });
        },

        getFileMetaList: function() {
            return fileMetaList;
        },

        reverseFileMetaList: function() {
            fileMetaList.reverse();
        },

        getFileCount: function() {
            return fileCount;
        },

        //		getFilesNCount : function() {
        //			setFileManagerChannelId(talkData.selectedRoomId.get())
        //			let result = fileManager.getFileList()
        //			return result.fileMetaList.length
        //		},

        getFileCount: function() {
            return fileCount;
        },

        // 페이지 당 파일 개수 바꾸는 함수
        changeItemNum: function(num) {
            perPage = num;
        },

        loadTable: function() {
            loadTable();
        },

        getNumPages: function() {
            //roomId 설정
            roomId = talkData.selectedRoomId.get();

            // 페이지 수 만들고 페이지네이션
            numPage = (!fileMetaList) ? Number(0) : Math.ceil(fileMetaList.length / perPage);
            if (numPage === 0) {
                numPage = 1;
            }
            // 첫 번째 페이지로 가기
            $("#talk__pagination")
                .empty()
                .append(
                    $("<span/>")
                        .attr("id", "first")
                        .css({
                            "font-size": "0.688rem",
                            "margin-right": "1.081rem",
                            color: "#8C8E92"
                        })
                        .addClass("icon-arrow_thin_first")
                        .on("click", firstPage)
                )
                // 이전 페이지로 가기
                .append(
                    $("<span/>")
                        .attr("id", "previous")
                        .css({
                            "font-size": "0.688rem",
                            "margin-right": "1.081rem",
                            color: "#8C8E92"
                        })
                        .addClass("icon-arrows_left_thin")
                        .on("click", function() {
                            if (fileCount == 0) {
                                return;
                            }
                            if (currentPage > 1) {
                                document.getElementById(String(currentPage)).style.color = "#8C8E92";
                                currentPage -= 1;
                                loadTable();
                                document.getElementById(String(currentPage)).style.color = "rgb(0, 164, 195)";
                            }
                        })
                );

            // page 버튼 생성
            for (i = 0; i < numPage; i++) {
                $("#talk__pagination").append(
                    $("<span/>")
                        .attr("id", i + 1)
                        .css({
                            "font-size": "0.813rem",
                            "margin-right": "1.081rem",
                            color: "#8C8E92"
                        })
                        .text(i + 1)
                        .on("click", function(event) {
                            if (fileCount == 0) {
                                return;
                            }
                            document.getElementById(String(currentPage)).style.color = "#8C8E92";
                            currentPage = $(this).attr("id");
                            loadTable();
                            document.getElementById(String(currentPage)).style.color = "rgb(0, 164, 195)";
                        })
                );
            }

            // 다음 페이지 가기
            $("#talk__pagination").append(
                $("<span/>")
                    .attr("id", "next")
                    .css({
                        "font-size": "0.688rem",
                        "margin-right": "1.081rem",
                        color: "#8C8E92"
                    })
                    .addClass("icon-arrows_right_thin")
                    .on("click", function() {
                        if (currentPage < numPage) {
                            if (fileCount == 0) {
                                return;
                            }
                            document.getElementById(String(currentPage)).style.color = "#8C8E92";
                            currentPage += 1;
                            document.getElementById(String(currentPage)).style.color = "rgb(0, 164, 195)";
                            loadTable();
                        }
                    })
            );
            // 마지막 페이지
            $("#talk__pagination").append(
                $("<span/>")
                    .attr("id", "last")
                    .css({
                        "font-size": "0.688rem",
                        "margin-right": "1.081rem",
                        color: "#8C8E92"
                    })
                    .addClass("icon-arrow_thin_last")
                    .on("click", lastPage)
            );

            // 첫 페이지 그리기
            currentPage = 1;
            loadTable();
            document.getElementById("1").style.color = "rgb(0, 164, 195)";
        }
    };
})();

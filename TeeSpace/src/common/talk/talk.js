const talk = (function() {

    // i18n 쉽게 하기 위해.
    $.fn.i18n = function(i18nKey) {
        return $(this)
            .attr("data-i18n", i18nKey)
            .text(Top.i18n.t(i18nKey));
    };

    function _updateLanguage() {
        let _talk = $("#talk");
        if (_talk.length) {
            let targetList = $("[data-i18n]", _talk);
            for (let i = 0; i < targetList.length; i++) {
                let targetItem = targetList.eq(i);
                targetItem.text(Top.i18n.t(targetItem.attr("data-i18n")));
            }
        }
    }
    
    // dragNdrop 관련 함수
    function _onDragEnter(e,data) {
    	// 기본 modal 사용 안할경우 구현.
    }    
    
    function _onDragLeave(e,data) {
    	// 기본 modal 사용 안할경우 구현.
    }
    
    function _onDragStart(e, data) {
        // background template
        const element = document.createElement("div");
        Object.assign(element.style, {
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            width: "fit-content",
            height: "6.25rem",
            backgroundColor: "pink"
        });
        
        // TODO : storage Manager에서 파일 정보 가져와서 기획대로 보여주기?
        let fileId = e.target.getAttribute('data-file-id');
        
        // TODO : valid file일 때만 수행, storageManager 사용가능하면 if, else문 주석 풀기
        // TODO : invalid일 때 처리(else 안에서)
//        if (talkUtil.downloadValidFile(fileId)) {
        	
        	let fileName, fileExtension, fileFullName;
        	
        	// 모아보기 아닐 때
        	if (e.target.classList.contains("file-info")) {
        		fileName = document.querySelector('[data-file-id="'+fileId+'"] .file-message__file-name').textContent;
                fileExtension = document.querySelector('[data-file-id="'+fileId+'"] .file-message__file-extension').textContent;
                fileFullName = fileName + '.' + fileExtension
        	}
        	// 모아보기일 때
        	else {
        		// 이미지일 때
        		if (e.target.children[0].children[0]) {
        			fileFullName = e.target.children[0].children[0].textContent;
        		}
        		// 이미지 파일 아닐 때
        		else {
        			fileName = e.target.children[1].children[0].textContent;
                    fileExtension = e.target.children[1].children[1].textContent;
                    fileFullName = fileName + '.' + fileExtension
        		}
        	}
            let template = `
            	<div class="talk-drag__file-wrapper">
    			    <div class="talk-drag__file-info">
    			        <div class="talk-drag__file-name">${fileFullName}</div>
    			    </div>
    			</div>
            `;
            element.insertAdjacentHTML('afterbegin', template);

        // background set
        dnd.setBackground(element);
        
        // default modal
        dnd.useDefaultEffect(true);
        
        // data set
        dnd.setData({ "1": 1, "2": 2, "3": 3 });
    }
    
    function _onDragOver(e, data) {
    	// 기본 modal 사용 안할경우 구현.
    }
    
    function _onDrop(e,dropData) {
        const target = e.target.closest('.dnd__container');
        if(dropData && dropData.data && dropData.data.name && dropData.data.file_extension)
            dropData.data.name = dropData.data.name + "." + dropData.data.file_extension;
        const {fromApp, data, files} = dropData;
        let workspaceId = target.id;
        if(workspaceId === 'talk-main') workspaceId = talkData.workspaceId;
        talkFooter.setDriveAttachStatus(false);
        
        // 내 로컬에서 drag & drop하는 경우
        talkFileUpload.setLocalUpload(true);
    	if (!fromApp && e.dataTransfer["files"].length) {
    		if (document.querySelector('#top-dialog-root_talk__file-upload-dialog')) {
    			return;
    		} else if(e.dataTransfer["files"].length > 30){
    			Top.Dom.selectById("driveSendFailDialog").open();
    			return;
    		} else {
            	talkData.fileChooserList.clear();
    		}
    		let fileChooserList = e.dataTransfer["files"];
            let fileCount = fileChooserList.length;
            
            if (fileCount) {
        		let temp=[];
        		
                for (let i = 0; i < fileCount; i++) {
                    if (talkFileUpload.isFileNameLong(fileChooserList[i]["name"])) {
                    	TeeToast.open({text: '파일명 70자 초과 파일은 업로드할 수 없습니다.'});
                    } else {
                    	talkData.fileChooserList.push(fileChooserList[i]);
                    	temp.push(fileChooserList[i]) // addFileChip할 것
                    }
                    
                	if (talkData.fileChooserList.length >= 30) {
                    	TeeToast.open({text: '파일 전송은 한번에 30개까지 가능합니다.'});
                		break;
                	}
                }
                
                if (temp.length) {
        			talkFileUpload.onOpenFileUploadDialog(temp);
                }
            }
            
            return;
    	} 
    	
        switch(fromApp){
            // fropApp 에 따른 처리 (타앱 -> drive)
            // 타 앱에서 이미 업로드된 파일이므로, file id 만 알면 복사 가능할 듯.
            case "drive" : {
                // create 파일 업로드하는 message
                talkFileUpload.setLocalUpload(false);
                let tmp_arr = [dropData.data];
                if(tmp_arr.length)
                    talkOpenAPI.openTalkFileUpload(workspaceId,
                         workspaceManager.getChannelList(workspaceId, "CHN0006"), 
                         tmp_arr);
                    }
                break;

            case "note" :
                break;

            case "mail" :
                break;

            case "meeting" :
                break;

            case "office" :
                break;

            case "schedule" : 
                break;

            case "plus" : {
                talkFileUpload.setLocalUpload(false);
                let tmp_arr = [dropData.data];
                if(tmp_arr.length)
                    talkOpenAPI.openTalkFileUpload(talkData.workspaceId,
                         workspaceManager.getChannelList(talkData.workspaceId, "CHN0006"), 
                         tmp_arr);
            }
                break;
            default :
                break;
        }
    
    }
    
    function _onDropDialog(e,dropData) {
        const target = e.target.closest('.dnd__container');
        const {fromApp, data, files} = dropData;
        let workspaceId = target.id;
        if(workspaceId === "selected-item-container") workspaceId = talkData.workspaceId;
            
        talkFileUpload.setLocalUpload(true);
    	if (!fromApp && e.dataTransfer["files"].length) {

    		let fileChooserList = e.dataTransfer["files"];
            let fileCount = fileChooserList.length;            
            if (fileCount) {
        		let temp=[];
        		
                for (let i = 0; i < fileCount; i++) {
                    if (talkFileUpload.isFileNameLong(fileChooserList[i]["name"])) {
                    	TeeToast.open({text: '파일명 70자 초과 파일은 업로드할 수 없습니다.'});
                    } else {
                    	talkData.fileChooserList.push(fileChooserList[i]);
                    	temp.push(fileChooserList[i])
                    }
                    
                	if (talkData.fileChooserList.length >= 30) {
                    	TeeToast.open({text: '파일 전송은 한번에 30개까지 가능합니다.'});
                		break;
                	}
                }
                
                if (temp.length) {
            		talkRenderer.dialog.addFileChip(temp);
                	let count = talkData.fileChooserList.length;
                	document.querySelector('#selectedItemCount').textContent = count;
                }
            }           
            return;
    	}    
    }
    
    ///////////////////////////chk
    function get_position_of_mousePointer ( event ) { 
        event = event || window.event; 
   
        var x = 0; // 마우스 포인터의 좌측 위치 
        var y = 0; // 마우스 포인터의 위쪽 위치 
   
        if ( event.pageX ) { // pageX & pageY를 사용할 수 있는 브라우저일 경우 
               x = event.pageX; 
               y = event.pageY; 
        } 
        else { // 그외 브라우저용 
               x = event.clientX + document.body.scrollLeft + document.documentElement.scrollLeft; 
               y = event.clientY + document.body.scrollTop + document.documentElement.scrollTop; 
           } 
   
        return { positionX : x, positionY : y }; 
   } 
    /////////////////////////////////////

    function talk__setProfilePopup(userId, isMention = false) {
        try {
            if (!isNewWindow()) {
                let curPosX = get_position_of_mousePointer(event)["positionX"];
                let curPosY = get_position_of_mousePointer(event)["positionY"];
                if (isMention) {
                    const mentionName = $(event.target.closest('.mention'));
                    var remVal = screen.width / 1366 * 16; // 프로필 창 width 확인
                    if (event.target.parentNode.parentNode.classList[1] === 'my-message') { // 내가 보낸 메시지
                        curPosX = mentionName.offset().left - (17.5 * remVal); 
                        curPosY = mentionName.offset().top - (29.35 * remVal);
                    }
                    else { // 남이 보낸 메시지
                        curPosX = mentionName.offset().left + mentionName.width();
                        curPosY = mentionName.offset().top;
                    }
                }
                else {
                    let profileName;
                	if($(event.target.closest('.talk-tooltip')).length){
                		profileName = $(event.target.closest('.talk-tooltip'));
                	} else {
                		profileName = $(event.target.closest('.message__photo'));
                	}
                	
                    curPosX = profileName.offset().left + profileName.width();
                    curPosY = profileName.offset().top + profileName.height();
                }
                _UserId = userId;
                _friendManageProfilePopupWithPosition(_UserId, curPosX, curPosY);
                if (userId) {
                    talkRenderer.component.changeUserPhoto(userId, userManager.getUserPhoto(userId, "small", talkData.userInfoList.get(userId)["THUMB_PHOTO"]));
                }
            }
        } catch (e) {
            console.log('아직 프로필 팝업창 없음')
        }
    }

    // 처음 render 관련 함수
    function _render(target) {

        Top.Loader.start();
        
        //TODO : 임시코드 향후 system이 userid인거는 이상함 type만 그걸로
        talkData.userInfoList.set("system", { USER_NAME: "SYSTEM" });
        talkData.userInfoList.delete(talkData.myInfo.userId);
        
        talkData.userInfoList.set(talkData.myInfo.userId, userManager.getUserInfo(userManager.getLoginUserId()));

        const oldTalk = document.querySelector('div#talk');
        if (oldTalk) {
        	oldTalk.remove();
        }
        
        const template = `
			        	<div id='talk' data-talk-id=${talkData.talkId}>
			                <div id='talk-main'>
        						<div id='talk-header'>
        							<div class="top-flowlayout-root" id="AppGnbTitleLayout" style="width: 100% !important; height: auto; border-width: 0px 0px 1px 0px; margin: 0.63rem 0; border-style: solid; padding-bottom: 0.63rem; text-align: left; vertical-align: middle; overflow: hidden;">
										<top-imageview id="AppGnbCurrentImage" background-color="rgba(221,221,221,1.0)" layout-width="2.13rem" layout-height="2.13rem" margin="0px 0px 0px 0.63rem" border-radius="30px" title="" layout-tab-disabled="false" class="flow-child" layout-vertical-alignment="top">
											<img id="AppGnbCurrentImage" class="top-imageview-root" style="width: 2.13rem; height: 2.13rem; background-color: rgb(221, 221, 221); margin: 0px 0px 0px 0.63rem; border-radius: 30px; vertical-align: top; visibility: visible;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJAAAACQCAYAAADnRuK4AAAgAElEQVR4Xu1dB3wU1dY/M9trkk2vtJAAoRelKRFpIkgNSlGxgR1QERDQgCKCL+pTwPLehzwVEaI+rNjAiDwUpEgNNQRIIT3b+8z3O3fZkEDKbrI7u5vs8eVlSWbm3nvmn3NPu+dQEADUr18/QaokVR4iDgkTCoWRIYKQOIFIECekhZF8mh9OUZSKpVgpUGAGBqqtdmuFnbWXWs3WYr1ZX2iwGMoAoCLvYp5ux7kdFgBgA2DZATFFyk9nKZwxeEZst8RudypFylk0j+5IAy0CABFFUUIA4Lkxb4YF1sqyrBlYMDEsU2awGLbkleb99/2d7+cBgDkIKDe4ed2l/gIg/oMjH4xqJ283RC6VTxQJREN5NC+poWXRFA34BRT+jwKKIv8PLP7Hkv8nMobB/ximQe4gmMw28z6tXru93Fie886Ody4BgLX57Gx7d/oUQMmQLBoxYkS3bvHdsgQ8wW31sR+BIRQIQcATgIAWAJ/Hd/st2ew2sDE2sNqtYLFaCLDqIxtjO1tYVfj4R399tLeoqMjg9kBt8AafAChjUIaqf1L/x2QS2b08mtcZAGgn7xEgQp4DMDyaRyQNShhPkp2xA34RUNmsYLGjWnSN7Iz9stFs/Ca/JH/t+l3rUSoFdaYGXoBn30zjb5m6q89dsYNSBz0ZKg19vrYeg0BB6SIRSpolYVoKLgSTyWoCs81MtjyyBTqIMZgNW49dPvbSpt2bzuO/WzpWa7ufEwClpaXJZ/eavUoqks6kKTrcyUSUNE7QEJ3Gx4T6E0olk8UEJpupZjYsy2pMNtOvOYdz5n59/OsSH0/Tr4b3KoAyIIOXOil1QmxI7DqapmNx5QgU3J4QOPjdXwmlktFiJNsbfkZiWVZfpatauufwnvd2nNuB1lubJ68BaFDaINX0AdO/EdCCgU4dR8QXgVwsd1hQAUIolQwWA/lyoAhYlmIrjl04NvTdX989HSDL8No0vQEg+pUpr9ytUqrepSgqBGfu3KqEfHThBCahJYcgstgsDncBsAatXvvqjpIda3JycmyBuaqWz9qjABrVc5RsbM+xn4qEortwamg9yYQysl21FkIAaUwa4m9Cstlth3Yf3T0m+3A2ervbHHkMQE+MfiKtS0yXrQKeoBsLLMWn+aAQK3xiVXn7LTIsA1qTlkgjJIZhCi9WXnxg7ddrf2lrJr9HADT/jvlDUmJSvsaYFDJULBCDXCT3uP/G28Bw9/l6sx6MVmONNCqpLnkg88vMj9qSud9SAFErp668N0IR8R5FURL0GiskCkBdBz+3BULvtsaoAZRKwIJFZ9FtWLh54XMA4DDdWjm15C3TL015aU60MvoNBA9aVih1RAKMebYtQhDhlobmPgssozVq39y2ZduSg3Cw1cfVmgsgasXUFbOjlFEbESoInlBpKAk9tGWq0lcRRySSwWL497OfPDu3tW9nzQLQiqkrMiIVkf9ByYPOQFSW2zp4iDLNMoB6EYZFWGBtWpN21aJPF61ozYq12wCaP35+35SIlD3ObSsoeerKXPQQaQyamgBtmbbsgRezX9zUWiWzWwB6ctSTHbrFdzuA1hZKnFBJKNB04HiVuXqJKInURjX6iEj442LFxbFrvl6zm6vxuRzHZQCN7zdeOrL7yD8EPEFPdBDitoWhiSDVzwHUhdQGdY11dqzwWPKGnzZcbm38chVA9Fuz3vqvWCgej05CpUQZBI8LSEAQVRuqiZ/Iarceffo/T/dvbRmPLgFoZcbK2ZGKyA9rnIRieZvx87iAk0YvwYi+zqwj1+jN+g3PbX7uqdZkmTUJoCGpQ+JmDp55EgOjGJ5ApdnTGYItfUn+fj9KIfQVoaPxTNGZO9/88U0MebQKahRA6enp/MntJv8p4An6IWhQaW5OTnKr4FQLFoFbWLWxmijVGDfL+SunS/aJbIdYCnBqFEDLJi2bHB8Wn42+QvQyt6aoOtfvDQOvaJld3crWPrf5uUVcz8Eb4zUIoJ7RPWVz75h7FjMJMZ8nREpSe4LUAg7oTDpn8FX7/aHvu3575NvCFjzOL25tEECvz3j9dblYjkFBCJGEkABpkFrGATtrh2p9NTHtzVbzz/M/nn9HoAdd6wVQRp+MyPTe6bmYAI++HjTbg+QZDhjMBtBb9ORhlyouDVz91ep9nnmyb55SL4BW37M6M1Qa+hIGScNkYZzkMGN+356LufBb/kmQCkSQqAyHzhGx0CksGhQiz2Q0MiwLxbpqOFteDOcrr0CBphIiZAp4oE86GZMLIgq1oZoEXc1W8/fzP54/LpBjZTcAKKNfRsjwXsMr8NwWl9LHytjh5V8/hxK9Q9F0Eo+ioXN4DAxKTIGUiDiQCIQg5glcciXYGQYMVjNUmfRwuPgC7Cs4BxUG7Q04ub1jD5jWfRAX+CFjYBIa6kPoD9p7fG/yx/s/vsDZ4B4e6AYArblnzSKlVPkajoM+H66O3tgYO7zy25dQrK1qcIkivgBUEjnEykOhR0wS9IhKukE6oZQp0FTA38X5cLq8CCqNOlCbDGDHhK8GaFSnnjAlDQ+PcEOYN1RlqCIeaovNsnPeR/NGBqoUqgOgNEgTPjb7saM8mpfqC8vrr4Jz8HPeMSjXa0BvbfrYFUqnjqoo6BndDsKlCgKYYyWXCGiaIrw3VCKD1Ig4mNFjKAh43OYyYQIaSftgWeOBggPJG3/eWNTUnP3x93UA9PjIx3v3SOxxEP0+vrK8LHYbmG1WyKssgd8vnYZTZQWA21tThAtx5QB7qFgGQ5JSoW9cR1BJZCDmCzjR8a6fP1pilbpKckCoUlP5yNLPl/67qTX64+9rA4j65/3/zBHyhLeSVA1pqE8Yez2TDBYznKkshv9dPAXnKq+AwVq3EIIrTI2ShUDf2A4wIKETxCtULulPrjy3pddgLjWex7cz9jNPbnqyWyCa9DUAmpM+J6JPxz7kbJOYLybJ8b4i1GNQJ7qirYLf8nNhf+E5QMnUUkLw3NK+C/SL6wRSgRB4NO3ToDBuYbiVIZ0rO9c165usUy1dI9f31wAoc0rmtOiQ6K04gTBpmE9iXqi7/HrhBFxWl0OxthqqTQ5/SWOEMbo4hQoipHI4U1EMRhcklIDHh0ipEuIUodA3rgMBlC8IlelKfSUZ2mgxrnvmk2cwUh9QVAOgrJlZm6Ui6QwMliKAuCY0ubP2fgPnKxsvfsGneaAUSSBMIoPeMR0IACKk16RlblkhHCw8D3nVpcT60luwsl3D2hEyYO6AkdAntgPXSybjYXwM42R2xn763b/e7XnixAn392ifzNwxqBNAwnWz153FsnJSoRRkIhnnU7La7fDSrq1QUY8FhWfMkkLDYUBcMnSJjIcQsRQUQnGjuozZbgOt2QilOjUcLMqDv4rOE+W8PkIf0+w+6ZyvGQd0BlmxhMzvJ39P2bJvS0CVjyEAunfIvUmDUwdfxM++sr7QJ7L1+F7YnZ9LdJMYeSjEKsKImd01Mp74f1pC+HyUbrnlhZBfVUr8Tbhlhkrk8NTNYyBeSQ7Vck44rwp9BfEJ5ZfnDwu03GkCoFX3rJqjkqrex88YusDEMV+QU3nGsdFPg0DyBuE4aEbjd9ShBD48z1Y7tHE1Y/EJb6zZW88kAHr7vrf3CfiCm3CrCJeH+42Z661F+9tzneY8wzBXntj0REIgmfMUZh1O7TC1gEfxovFYslIcjLxzDTCsO4QHEpF2HdkVmn0wu25AkOsJuTEeNWfEnJDeib3zKYoKxephEoFnIt9uzKHNX1o7W/FCyYVua79bmxsoTKEeHfNo+56xPTFpXuIL/w86CHUWEyhIzURu41G1X5LG7CjTgi4Cdw8NkGM7rBWsFAOWcCFY4sRgiZMAEyrCwgEuY4FVG8GsN/xqoO0fXjx+cv/xnTtLQgF0/lwBjVowesFNKfEpe1FvjVREurxYT1xYbtDCu3/9BNVGPciFEpjZayikhJNanJwRmvbfnTkEfxacJQAa1r4bjEvt1+j4pJor2MEUQoMhUQzWSBEwSiGwEv41x0jLV8CyDFNlt9sv2622owZd9efnD5/cuyUzEz2PflNumFo6YemkhPCELzF5DBVoLumr3L/g+7OHa4bEOaRGxMKA+GRIVsVAtNw7edgobdCUR5Me84SqjHU93suGTYHEkBt5YQU7GGQs6LspwNpegaZiXXZhy4VaXzSPBh5fABRNAYVhk6u/w5tISwb8wk4eDAOM3QZ2ux1/ce3nV8vo1QzCsnaLxbK/qqh47ZVjJ3//aPVqBJMrMWSvvVbqlWmvPBouD38XTXc04bmkzUf3wO78kw0OGS0LgYGJKXBzQjJgFL2mNwbeQfpj1E8Ojjr6ZeBnfFFmu5WA5Y/LZ0k2IprwDdGzg8eR5DUkNPdNAgY0PRVgiZcCK60rZRAUfKEQBCIh8AQIFoyvOepD4hzdJSewgGHAbrOD1WIGq9lCQFaLiHSy2WwHruTnZ7719oYDcNA3tYioNfesyVRKlS/5IoSRV1UC7/z5A8kabIow5RT1E/yOWYlCHp/oTOjDEfL5wKd4BCTo0SZfmDJqs5Fno0ca42pN/akiJPvEtocH+w0HHk2BXs6CtocCrInyOroMD8cTCkAgEjlA0wygNLXe639vt9nAajKB1WIFxkaKNjgvYW1Wy+Gq0vJXT/y0c8c3H3zAaY8P6h8z/7FeJpI9jpmHmMLBNaE3eMvR/8GZiiIwNRBq8PacEDihEikM79AdhrXvCiYpC5qRscDIBXV0GqFYDCKZFBBAviRsx2AxGMFsMNQBEsuyprKCwofXTJv+JQBcK7XvxclSWbOytkmF0gw8toNhDF8Qpm6U6NQkZ/n3i7kuSSRPzbNDWBSM7NQTOofHgkAphKo+CrC2U9QAh+bxQCiRgFAsAvzsT4RAsprNYDWawGa9GuejKMZmthxSl5YsfjVj+i5v60gIoP9KhdKJXCbQN/YS0KzH1NTjpZehUFNJkuDVZgNgtL6lhFtfmFgGkTIlUdJ7xLQjMTeGtUNlshAMAyJqhkCwiOUyQKkTCIQAMmq0NYo4ztlsMn++/6tvH9n+1lvV3loDbmHZMpFsqi8lUEOLQ9DYWDvY7AxcUpdDfnUp+V6q0xC9Bv1H9SXLi3gCUIolECKSkiBpu9AI6BQWQ7YpjLFhSohTb9FL7FB5SzgwKsexHlSCCXBEIvI5kIi4FywWMGp1wKBF56hhXVRdWvb0qklTcVtrSg10e7lU1sys96Qi6Vxf6UBuz/i6GwjISHXUawFY2gWlFpvOVbbjg36Qw/dFrCmRECRyud9tVW7ziGXBqNeTrc3ZsdGk1//77z1/LsjOzGz6xIEbA6IVtlIpVS73hRXmxjw9eqkZrFB+ewTYIsQO1wC2ZAgNIeZ4ayKUQrpqNbHakFjGnn9y9x+3b1yyBHvFeoSoVXevmqeSqd7yhR/IIytw8yE6sQ2q0iMcYQYA4r+RKBSBL3Ua4ANua7ilWYxGB4hYVlteUPjIa9Omb/PElkYtn7T8nriwuC1YLDNcxq0n2s133+LLq+VWUI+Nr/HpoEmOW1ZbIIvJRJRsp/9IW1GxMHPcxDdaGhahnrnzmcGdozv/jmfBuI6FcfXiUD+qigXQDY4EVkCT0AJKnUCxsDzFJ7TUDGo1MHZs6wkWo0azfufq159vSbCWenz048nd47sfp4AS+SIa7ynmNPQccnAvkgVdegyJXaFlJQ1RgqCV6Tuu8hGVan11NditVyvq67Tvbl+8fN7BZoZCqIdGP6TqG9f3Ak3RSizdi512WhNVRQNobo0i4KH5fJCHKMn3tkwYVzNotMQJiS3PTHrDxqUjRj/aHJ2IykjLEKYPSL9M03QUggdB1FpII7VB9R2xZNvC8ANaWv7mTfYVr0krT7WaBGoRONry8qczx09a5+58SLj4nfvfOc7n8dNooCFc0ToUaYPQDmUTY4nCjNuWQhUWBM/16GABdNXVxPkILNirSkqmvjJp6nZ3QEQAtHr66oWhktC1+FklUwV84xTM27kyJpyY6ggeWQj6ePy3Q7Q7L8zT1xJfUVUVUayBZTV5x48OXj/nyROujkMANHfk3M69E3ufwc++Ohfm6oSbug7tiyu3hYA12pHbjQpzW7O2muLR9b9H8GgrK0nOEcuylTu3ZHfY8c47Glee48x4Eq1/YP1lmqIj8VQqnk4NVCpvT4P+5kjiYW5Lfp6Wvi/0ExnUDsxYTabtizd8MBWys5usq+MEEPXmrDe/FAvFE31RWKqli3fer5XaoPKuePJP9DDLQrnPb/LUWnzxHKNOh0n9ZOjyouKHV0+Z9n9NzaMm53LllJWzIkMiPw5UPcjK2uDKmAhgwkQktqUIVwWV5qbe/nW/R8tMV4U+IpJbZD1z5FDa+4/OO9vYY2oANH/0/NjU+FRSZs1XBRbcXG+dy0uTeWDsH9FqA6Mt4Y079zqU6mqSDmIxmX9YctuIOxsLd9SpULb+gfVHaYrujhXK0CvNRa6vO4tr6FqjyA6lkxxJ8AKxiFhdQWo+B8xGI4mbIZUWFt21Zurd3zT0tDrHBp4f9/ztHaI6/II5wtjagKsKrc1fquPERfFAGVg7KIL+npYwsta9aI1p0bS32TEFpGTfzznJDeUR1QFQdHS0bPkdy3N5NC/R12XuXOWFVmCBysmJxOqSKBUgkgSPZrvKu8auw1Mg2soqoFiWrbhSghmN9Xqpbzi49Pr019+US+TzA0GZxkBpwVgVORWKIQr0NnOVhlp4rgAKzl6G5N6dITz2Wi61J15e7WdYzVYoOHsJqkqroMtN3UAq587FYtBowGI0oX+o+tvNWxNzNmy4IZvxBgA9OfLJuLTENOztSWOhBSy44K+kkzFQMS6WSB8uHYaF5wtg2xufgu1qRFsVHQ4duneExJQkUMWEQ0hEKPAFzQvYGvVGqC6tgtLLJZB/8gJcOpUPJoPjhE5cx3iYtmA6cVFwQZj+oat0FH43qNWLl48Zt+b6ces9Opk1I2uDVCx9zJ+VaaL7DJKRI8YofZQR3MXwfvn0Rzicg+W065LjlKoApHIJdOqdAr1u6Q0RcU3XG0Ag5p/MgyO7/4aivEKwWaw14Kw9gkQmgfuWPwRKFXcleNAiw1gZY7dd3P/F112z33zTkdp4leoF0INDHozrn9I/l6Iopb+a9Aa+Dcomx5FgqVguB7GMO9FedL4AvliXDSZ9HV7WKxRQGqHkiO0QR75HJkSB2WiGK/lFUJRXBMUXCuFKfjH5WVOU3Ksz3PnwBBByJIFwPpg3hAo1sCxTmp9/15rp937XJIDwgjfufePfEoHkIfzsj4lmJX3EYEp1mOsofbhO09CpdfD3roOQe+Ak6DU6IjGwUEJTxOPzyJl3Vwivxa0wul0M9LqlL3QZ0NWV2zx6DUp6PUohqxV9QxcWDk1Pru0XavD0/8TeE0NH9RlVQFGUzF8OHTo5YwMGisZHAivjk0Ap6j++IgSDplINlcUVcPnsZbhw/DxUFJfXPnLs1tRwC0xKTYL23TpCdFIM0amkCu6ka32TxSPUmJiPVJR7Ni3rwQdrKmI0Wj5i1d2r7lNJVZso3MskSr/pWoiJYlXj44nyjCELX59VJycfdAbQVmmh6HwhnDl4Ci6dIUVv3SKpQgbJPZMhuU8KoGIuD5VzpjA3NlFMg9WWOyrJ6rWatS+OurOm32ujAML6idM6TCukKCoKFUTMFcIaPr6mov4SsCYrCXAQQL4ii8kCuftPwOkDuVBVWgkGnZEowC0l5LVIKgKJXAoJnZMgbWB3iOsUDzwfns3XqzWkOojdaj35/K3De2OsDNfZZAGbOaPmdOmd0PsgBZTUH44/Y2WwwgxMkKdBJJWQ0xVck16tg58+2QHnjjQaZ6yZlkQhhYi4CIhKiIaEzolg0Bqg8NxlKCssg4riippjyI2tQxYih5tGD4S+t/UHLFzFNWH+tL5ajfWvbDu3bMN8oQKXAIQX1W5C52urTKdgoeLOGMI/eVioT06T/rrtFzjwy/5G3yFKkeTeKdDntv6QkJxAXvr1sUVUui1mC5z44xgc2nWASLHGCHWh+5Y9BIow7v9oMLiKnmkMc+gqqha/NO4u4hNqUgLhRbiVTWk3ZT+fx++DTMCsRV/FycpShWDo4wj0KiMd0Xeu6fjeo7Bj07d1hkUJkZCcCPHJCcRqQmkjFLvn8FNXqKH00hUozi8m3ufivKKas+04WGhkKMxa8gBI5L4J12DWIpr1jJ25snDoMIxesy5zn3Ry7pV+mKbpeNSDUKnmGkQYuihMV4I9RkoKIch9mDC2a+vPkHfsPPE+pw3qQXQUPN3rSULpdOqvk3By33EwG8wwdMIw6NQTrWjfUK2EM+bYnwfCNy1YUO0ygHDKSyYsuSUpPGk3fkYvNSrVXJKVsULxxBhSp1CqVJDCT0HijgO1Qhts0elTA7JmP3LQLQDhVFdMXTEzShG1ESgQYkEGTPvgyjIzgBnK7k4i3mcMnPrafOfu1fnPSOqyMuIwrbxSet+qSVM+dhtAqDf9Y+Y/VkpF0hfwxBUeRsSAa8M1Uz23eHUYA9WjYx15P5iy6uEtw3Mzbb1PwiNA6Kow6vTrlo0c81RzAITcodfOWLtCIVYsw39gIj62yPS2JCrtKgBjL0euM5epG60XDu6vzJniYTOb/1iUPmJIcwFE1KA3Zr2RJRaKn6CA4mOBKjwW7c1WUZdHhgATLnY4EFVhzarD7D7LHHfgX93pQ6eAsbOQ0i8VRCKRizZsc0e88T7cOqrKquD8kTOQ1KU9sfSw0giXZNLpwaTXo++qaOHQ9A4tHZ16bcZrz4aIQ14nYomiiU7kLRBdnBZF9B+uj+xgpHzL6x9DWUEpeVfK8BDo0r8r9B7Wl+T+eJsYGwNnj5yBk38eIzlCGLhF98Xt00dBn/TG2zJ4em5YqAoLM+Df1GsPPxrWUgDhg6iV01ZmRMgiNmLglZSLE8pAIvS8hXTxnmjCD7FMRgphckXFF4rgk9Wb6h0utW8X6DqwO8n7QUefSOKofNYSwmLiRp0RdNVaOHPoNJzcdwz06hsbEKf0TYUJj05pyVBu34vFGLA8DNLfP/zSzhMAIg97fvzzAztEdPgNrTP8N/qI0OHoSUefE0AShRxEUu4i1PjyPliyHmxXaw1ez3USu5KIAEMWMqUcopOiQRUTQRx/GBAVScQglopBILp2Pp8UCzdawGQwglFvAm2FmmxPCNaqkkoSnEUQOYtk1vemB44dArdMHOY2CFpyA27lqEgjXc492c9jAMIHzrt9XnSnhE7fCHiCAfhv1ItQGmEMzRPkBBCX6avOeV/MvQA5n+8iqabNJbQaEUR4Fp3kDzXSr6OxMTChrEP3TjD87pEEoFwSHjrEkAZSRVHRcI8C6OpC+Fkzs7KkQumjTmmEudUSkQR4VMsqvTsBhHV+sE8F14TddHL3nQD0QruSQeiN+cV3SoDR994BqljfhHHIaY0KR8xOW14+yRsAwmdT88bOuyklKuULDH3gD1DBJkASut/QrUYKXNWBfBVEdc7DYjTDxVP55Ovy6UtQXlTmDayQZ4rEIojtGAdJqe2hXdf2JMmMa8ur9uIwqKopryA/0lRU3ustAJEBxvcbL70l9ZYlcpF8HkVRJISMFppYKCa+IwyHuENOCeRrAF0/Z1R2MScILSRM0cDtCTMVkdmow5CeYA1sVwgG/OPCaD3N5wGmsWLyPEoaVJITU9s1+4SHO7x19VpOAeSc1OR+k2OHpQ3bJOQLRzl/RhRPvgiwnIyrDkhfb2GuMBmBg0dzzAYT2eYImKw2sFntV4FlIxKEjy2jBHzg4RefB0KRAERSh7KN3z1pfLgyb1evqbOFlZXf7VUJdN2kqEUTFnWPVcYuFQlEGbirkb2OBNWE5Astt8akki+VaFcZ3Nqvqw2gisKiMVwCqIa3s9Nnx/RN6vsGn8efiM1+azMdgYS6kjNVpPZf4jUzXkGyEYPEPQdqm/FFp87d5BMAOZeNLceTQpPSQ+WhL/JoXt/a7EDgoNWGEgndAag7Fc9KJJdw7Ujk/jX574hWi4Uc80HK3fNnR58CqBabqAXjFsQlhCTcJ+QLp/NoXjs8CHI9Gw2PdCc/wuZv0mAJF5+grHYpvHfnPxfiLwCqI3wy+mUoO8V0Sg4PDZ8kFUrvpyk6AS/QZ3QCKlRCFE+FittkNp+8LT8cFAOpGFBlGKZ84ZBh8f4IoBvYNjt9tjheHh8uffimDeJo1V1otchVKr+1VPzwvXtsSlh4CgtQ2ayWQ4tuvX1AQADIufql27+4WxUd9Rn6TBBAwYQyj+HC5QeRwuRmCxbj/M8LI0bPDigA3f/yy516jkg/iydlSeX5Nt7zwuW37sELNeXlJJZXXVr22MsTJr8XUABKHTJE8cjaV/MpmlZhHUSshxgk7jjg9EJTAGzhhbxbs2bcvyegAAQZGbw1Tz12kC8Q9BJKxCBV+q6oAnevzX9GqlVkgTn+1+GYD59+uiywAIR9PXb9+L5QIp2DifUhkd4rLec/r81/ZuJMqGdZ1vTc4FsxIcv1g4X+soxlX305JiwqcgfOxx8qc/gLX7w9D1K5taKSBIcNeu2a5SPGLsYxA04CZWRmqgaOGl4KFMXDqmRYnSxI3ucAlrnDDtBYqezE7pxuGxctPx2QAELQv5bzy26BSDSUJxA4TmcEyescwAJTqAPZ7baLzw+9LQUASKe6gJNAOOklWzePjUhK+paY81jeLnjA0KsAwlwmTCLDbcxs0K974fYxTzkHDEgAzVi8OKzv+DvOoTkfDKx6FTvk4bXjXyWXL928dtrMmto2AQkgXNRrOT//KBCJR5HOPEEp5D0U1WqLyTJM+YdvrYs/kZ1Ntq+A3cJw4vM+2nhzUufOf+A2HGxx4D38kMJSmETPsmxVacnTL0+o2/IgYCUQHq1e+9vOP3hC4QC+QADyoDLtFRThKVQ8jcowTOn+z7djofE6Zb5saH8AAAZNSURBVNQCGUCwOPuzUZEJ8T+gFEIAIZCC5DkO1E6gNxmM7y29fdRj1z89oAHUb84cwYwH7i3H4+pYcAFB5K/J6J57rdw9yaTTgcnRApM98uv3kR+9sNpxnqcWBTSAcB2Lt24eEdmu3Y+YpC8LUfrkwCF3r5S7kUht6ApHJ2eDRpO5fPSdK+obPeABhMBZ89vOPXyhcBBpuoJ1o31QeJO7V8vBSCwL2upqsFuseKZNm7vvQPv/e+aZekvItgYAwQuffdY3vF08aZ8TbPXdcoA5a0Ljk/TV6rkv3jHug4ae2ioAhIt7ddfPG0US8QPolUZdiOvmKy1/bf7xhNqdm20225FFt9yGp2WYVg+gO556Sjn8nqm5NE3HYaYiKYEX3MrcQyULoNc4WhogFZ7J6/vG/fcfbuwhrUYC4SKXbf9ialh0VDZ+xhLAWAo4SK5zANtbGrRadBqCQavPWj5qzEK0wNoMgNAftOrnHzaI5bJHcdG+qCPk+uvyrytrt7e0Wa1Hti1YOODgwYNNdo5pVRIIX8n4zEzpLSPST/B4vPak3B720wg6GBtFK+l/gVYXFr0CVn/xaG6vd+bOPe8KxFsdgHDRM155pV3f4bcew2yP4BGgxmGASjP2wMAe8djCqSjvwvismfehX80lapUAwpUvyf5sXERC/Df4GU+yYl8NrlqCu8R5P7nIoNaQdA0kvVq98sUx4zKb0ntqT73VAggX+eK3Xz2mDFe9RQEIsSQe6kRBy+zq68cui3o9HhDErELWqDf8a+mI0RjratBkrw/zrRpAqFS//OOOF6VKOf5VOepLh4QGaB6mZ0WWs2A4PtVmte/e9smnIw5+8EGTSvP1s2jtAML10it/+v5lmULxPFbY4wuFxLxvq45G0t9VqyMpGkTpsZi/y968bVJzwIP3twUAkXWu+uWn1WKZBEFEEcU6DHtueLa/l2dlhOefVtvawm3LZrP9tvXjLaOaC562BCCy1he/2z47JCz8X4C1q2gasGA5tg1vC4R+HqNWS0x1JEyO/+nl1xbk5OQ4ftBMaisSqIY9y77cNiYsJmYrXC1ghcFXTMxvtco1ezUpXqNx8sBq0GhXLB899lV3rK2G8NXmAISMeGzDho6deqftpSiaNN/A82WYS9Ta9CLUdzAlFaPrGJ5AJ2Fp3oXxa2fen+MJ8LS1LazOH1HGggWSfhPGbhKIxNOIps3jkVQQoUgU+P4ilgWsZYjZhNiaAMlutR4qyD076e25cy81c7eq97Y2KYFqcYJe+vnWu1Xxcf9BK58Aic8DLB0TqO00MZNQr1aTZDAnGbTaf25fsnyhK7Etd8HV1gFE+PX4+vWJSd1S3haIxRPJDyiKFPLEiH6gxNEwAR5Nc7PRRNJQidSxWY+XXCx6MGvWrAOe2rKuB1gQQNc4Qr3w5bYRYVHRH/F4dDR71cWBfiOJXE4kk98p2iwAw9jJVuX06+ByMA1Vr9MsfWn0uA2II3elijvXBwF0Hbcwmt+jV/fJqqjIdyiadrQjpBytCbAimkAs9vlZfFSOUTHG/B3UcWr6cFAUo6+qevXC4WNvf/jCC97rAFOLZ0EANfDnlpGZKew16ObpIrlkGU3zOta0ZkAwiURki0M9iSvLDbclbDOAHQMx+OncptAhaGeYMovZvO7kb//75+YVK2rsdXckSXOvDQKoCc6lp6fzb3rkkb6R7eLepnn8m2tfjs5I9GbjNofBWk/rS0SvMZvBZjaTdAsGO/7U6vrDMszl6tKyp/d89e0POZs2OULqHFMQQG4wfOGHH8Yo4iIniyTSOTw+P42iKH5dQFE1UommeQRcFO3QnUh6NvnuYLlz2yGtoPA/hiHtoVg7A3bGDozNRqqh1iWKsdttl6xG4yZdRemW1ffcf9ZbyrGrbAkCyFVOXfcmRy94KGzwiAmjpSHKZ2ge6fNBINK8x9V/F25PKHOAZUtMev3r544e/PzDZ5cVeVsxdmcNHl2wOwO3pmtRX2rfvn2iJESWIpDKe/PFwoF8Hq8/zeNFYQaAq2vF9gF2m+0oY7PtNekNf5t16lPaKv3FDU88oXP1GVxfFwSQ9zhOpaWlCfrNnKmIiIgIF4QpIiRicRRFU2Es8HgMa1Xb7WyJSasuV5drKn7Zvl2dn5Nj9vWW5C47/h96szB70cRS8wAAAABJRU5ErkJggg==">
										</top-imageview>
										<top-textview id="AppGnbCurrentText" layout-width="auto" layout-height="auto" margin="0.3rem 0px 0px 0.63rem" border-width="0px" text="김동재, 한상욱ffff" text-size="0.875rem" text-decoration="" title="" layout-tab-disabled="false" class="flow-child" layout-vertical-alignment="top">
											<span id="AppGnbCurrentText" class="top-textview-root" style="width: auto; height: auto; margin: 0.3rem 0px 0px 0.63rem; font-size: 0.875rem; border-width: 0px; border-style: solid; vertical-align: top; visibility: visible;"><a class="top-textview-url">김동재, 한상욱ffff</a><div class="top-textview-aligner middle"></div></span>
										</top-textview>
									<span id="talk__Content-search-button" class="icon-work_search" style="display: flex; right: 0.5rem !important;"></span></div>
        						</div>
                                <div id='talk-content-main'>
                                    <div id='talk-content'></div>                               
                                    <div id='talk-content-empty'></div>                     
			                    </div>
			                    <div id='talk-footer-wrapper'>
				                    <div id='talk-footer'>
				                        <div id='talk-footer__emoticon-view'></div>
				                        <div id='talk-footer__reply-view'></div>
				                    </div>
			                    </div>
			                </div>
			            </div>
			        `;
        // target 비우고 talk 넣기
    	talkUtil.removeChildrenNodes(target);
    	target.insertAdjacentHTML('beforeend',template);
        
    	const _talk = target.querySelector('#talk');
    	if(_talk){
    	_talk.removeEventListener('click', readHandler);
    	_talk.addEventListener('click', readHandler);
    	}

        const curURL = window.location.href;
        //미니채팅방이라는 소리
        if ( curURL.indexOf('talk?mini-talk=true') !== -1) {
        	//미니 채팅방일때 처리
        	document.querySelector("html").style["font-size"]="1rem";
        	target.querySelector('#talk-header').style.display = 'block';
        	renderGnbMini(target);
    	} else {
    		target.querySelector('#talk-header').style.display = 'none';
        }
        
        // read 처리할 때 현재 창이 focus되어 있는지 확인하기 위해 필요
        window.onfocus = function() {
            talkData.isTSFocused = true;
        };

        window.onblur = function() {
            talkData.isTSFocused = false;
        };
        
        setTimeout(function() {
            talkContent.init();
        }, 1);

        setTimeout(function() {
          talkFooter.init();
        }, 1);

        setTimeout(function() {
            talkTooltip.activateTooltip("#talk", null);
        }, 1);
    }
    
    function readHandler() {       
        if (talkData.messageList.length) {
            let i, msgListLength = talkData.messageList.length;
            let msgId, msgList = talkData.messageList;
         for(i=msgListLength-1;i>=0;i--){
             msgId = msgList[i]['MSG_ID'];
             if(!msgId) continue;
             endMsg = msgId; break;
         }
    		updateCommonUnread(getRoomIdByUrl());
         
            talkServer2.ttalk_readMessage(workspaceManager.getChannelList(workspaceManager.getWorkspaceId(getRoomIdByUrl()),"CHN0001"), msgId, userManager.getLoginUserId())
            .then(function(){

            })
            .catch((rej)=>{
                console.log(rej);
            })
        }  
    }

    
    function _initMobx() {  
    	// TODO : 괜찮은지 check
        /**
         * @observe         data.messageList 변경 시 호출
         * @description     메세지 렌더링
         */
        mobx.observe(talkData.messageList, change => {
        	// when get first 20 messages => change.added=1개 메시지, index=1, type=splice, indexCount=1
            switch (change.type) {
                case "splice":
                    let index = change.index;
                    let addedCount = change.addedCount; // 처음이다.
                    
                    // 처음 들어왔을 때
                    // if (change.addedCount === talkData.messageList.length) {
                    //     talkRenderer.component.renderMessage(change.added, false, true);
                    // }
                    // // talkData.messageList의 prepend되면 index = 0이다
                    // else 
                    if (index === 0) {
                        talkRenderer.component.renderMessage(change.added, true, true);
                    } 
                    else {
                        talkRenderer.component.renderMessage(change.added, false, true);
                    }                   	
                    
                    break;

                default:
                    break;
            }
        });
        
        // 내부 메시지 전송
        mobx.observe(talkData.tempMessageList, function(change) {
            switch (change.type) {
                case "splice":
                    var index = change.index;
                    var addedCount = change.addedCount;
                    var removedCount = change.removedCount; // 처음이다.

                    if (addedCount > 0) {
                        talkRenderer.component.renderMessage(change.added, false, false);
                    }

                    if (removedCount > 0) talkContent.remove(change.removed);
                    break;

                default:
                    break;
            }
        }); 
    	
    	// emoticon 관련 mobx
        mobx.autorun(function() {
            let emoticonTag = $(talkData.selectedEmoticonTag.get());
            let emoticonView = $("#talk-footer__emoticon-view").empty();
            if (emoticonView.length && emoticonTag.length) {
                emoticonView
                    .append(emoticonTag)
                    .append(
                        $("<div id='talk-footer__emoticon-cancel-button'/>")
                            .addClass(talkData.isSimple ? "talk-footer__emoticon-cancel-button--simple" : "")
                            .addClass("icon-work_cancel")
                            .on("click", function() {
                                talkData.selectedEmoticonTag.set(null);
                                document.querySelector('#talk-footer__emoticon-button').classList.remove("talk-img__filter");
                            })
                    )
                    .css("display", "flex");

                // textArea.focus();
                talkFooter.updateButtonStatus(false);
            } else {
                emoticonView.css("display", "none");
                talkFooter.updateButtonStatus(talkFooter.isTextAreaEmpty());
            }
        }); 
    }
    function _initData() {
        //get first 20 Messages
        talkContent.getFirstMessageListFromServer();

        //처음에 방 진입할 때 footer에 포커스 주기
        //TODO: 이 코드가 여기 있는것이 적합한가?
        let footerInput = document.querySelector('#talk-footer__input');
        if(footerInput) footerInput.focus();

    }

    function _init() {
        let container = document.querySelector('div#talk-content');
    	
    	// TODO : javascript 함수로 바꾸기
    	if (container) {
            container.addEventListener("scroll", talkContent.infiniteScroll, false);
            $(container).off("scroll", talkContent.updateTimeScreen).on("scroll", talkContent.updateTimeScreen);
            // renderer의 renderMessage에서 message 각각에 eventhandler를 달던 방식-> init할 때, container에만 등록하도록 수정 2020-05-05
            container.addEventListener('mouseover', (e) => {
            	let targetMessage = e.target.closest('talk-message-layout');
            	if(targetMessage){
            		let targetMessageId = targetMessage.getAttribute('data-message-id');
            		talkContent.mouseoverEventHandler(targetMessageId)
            	}
            },false)            
            container.addEventListener('mouseout', (e) => {
            	let targetMessage = e.target.closest('talk-message-layout');
            	if(targetMessage){
            		let targetMessageId = targetMessage.getAttribute('data-message-id');
            		talkContent.mouseoutEventHandler(targetMessageId)
            	}
            },false)
    	}
    	
    	// 초기화하는 부분
        talkData.selectedRoomId.set(null);
        talkData.selectedReply.set(null);
        talkData.selectedEmoticonTag.set(null);
        talkData.messageList.clear();
        
        // mobx 한 번만 달기
        if (talkData.isInit) {
        	_initMobx();
        	talkData.isInit = false;
        }
        
        let textArea = document.querySelector('#talk-footer__input');
    	let remainedMsg = JSON.parse(localStorage.getItem("teetalk_"+talkData.talkId));
    	if (remainedMsg && remainedMsg["userId"] === userManager.getLoginUserId() && remainedMsg["msg"]) {
    		textArea.innerHTML = remainedMsg["msg"];
    		let emptyLocal = talkFooter.isTextAreaEmpty();
            talkFooter.updateButtonStatus(emptyLocal);
    	}
        _initData();
        container = null; textArea=null;
        // talkSidebar에서 하던 로직을 이곳으로 옮김
//        const res = talkServer2.ttalk_getRoomList()
//	        .then(function (res) {
//		        if (res.data.dto.TtalkRoomList) {
//		            const roomList = res.data.dto.TtalkRoomList;
//		                    
//		            if (!roomList.length){
//		            	const emptyContent = document.querySelector('#talk-content-empty');
//		             	emptyContent.style.display = "flex";
//		               	emptyContent.innerHTML = "생성된 방이 없습니다."
//		            } else {
//		                	// roomList로는 해당 방 1개만 온다.
//		                 	// ROOM_ID, ROOM_NAME, ROOM_NOTIFICATION, MESSENGER_ID, USER_ID, IS_DEFAULT_ROOM 값이 있다
//		             	talkData.selectedRoomId.set(roomList[0]["ROOM_ID"])
//		             	
//				    	let textArea = document.querySelector('#talk-footer__input');
//				    	
//				    	let remainedMsg = JSON.parse(localStorage.getItem("teetalk_"+talkData.talkId));
//				    	if (remainedMsg && remainedMsg["userId"] === talkData.myInfo.userId && remainedMsg["msg"]) {
//				    		textArea.innerHTML = remainedMsg["msg"];
//				    		let emptyLocal = talkFooter.isTextAreaEmpty();
//				            talkFooter.updateButtonStatus(emptyLocal);
//				    	}
//		             	//console.log("i get room id");
//		                      // 서버에서 기본 정보 가져오자. 시점 문제로 setTimeout 걸어 놓음
//		                 _initData();
//		             }
//		          }
//	        })
            
        //미니채팅방 badge 없애기
        // let el = $("#talk_ICON").find(".top-alarmbadge-text");
        // if ($("#talk").length && !talkData.isSimple && el.hasClass("notibadge")) {
        //     // 미니채팅방 아니면서 notibadge 있으면
        //     el.removeClass("notibadge");
        // }

    }

    function updateTimer(userId) {
        let targetUser = talkData.typingUserList.get(userId);

        if (!targetUser) {
            talkData.typingUserList.set(userId, {
                USER_ID: userId,
                USER_NAME: talkData.userInfoList.get(userId)["USER_NAME"],
                TIMER: null
            });
            targetUser = talkData.typingUserList.get(userId);
        }

        if (targetUser["TIMER"]) clearTimeout(targetUser["TIMER"]);

        targetUser["TIMER"] = setTimeout(function() {
            talkData.typingUserList.delete(targetUser["USER_ID"]);
        }, 2000);
    }

    function isSameRoom(chId) {
        return chId === workspaceManager.getChannelList(workspaceManager.getWorkspaceId(getRoomIdByUrl()),"CHN0001");
    }

    function isMyMessage(message) {
        return message["USER_ID"] === talkData.myInfo["userId"];
    }

    // function _toggleNotiBadge() {
    //     // 같은 space/room에 있을 때임
    //     let el = $("#talk_ICON").find(".top-alarmbadge-text");

    //     if ($("#talk").length) {
    //         if (talkData.isSimple) {
    //             if ($("#talk-footer__input").is(":focus")) {
    //                 el.removeClass("notibadge");
    //             } else {
    //                 el.addClass("notibadge");
    //             }
    //         } else if (el.hasClass("notibadge")) {
    //             // talk app에 있는 경우
    //             el.removeClass("notibadge");
    //         }
    //     } else {
    //         el.addClass("notibadge");
    //     }
    // }

    function _handleMessage(mqttMsg) {
        //임시 방어 코드 모든 이벤트 다 notiDto에 넣어서 하면 안되는 상황. (모바일 푸시를 주지 않아도 되는 상황에도 주게 됨. 따라서 별도 정책이 나올 때까지 임시 코드)
        if (mqttMsg.NOTI_ETC) {
            message = JSON.parse(mqttMsg.NOTI_ETC);
            if (mqttMsg.NOTI_TARGET !== null) {
                let splitArr = mqttMsg.NOTI_TARGET.split("/");
                mqttMsg.WS_ID = splitArr[0];
                mqttMsg.CH_ID = splitArr[1];
                mqttMsg.MSG_ID = splitArr[3];
                if (mqttMsg.CH_ID === "mySpaceTtalk") {
                    mqttMsg.WS_ID = workspaceManager.getMySpaceId();
                }
            }
        } else {
            message = mqttMsg;
        }
        
        const messageTalkChannelId = message["CH_ID"];
        const messageSenderId = message["USER_ID"];

        switch (message["MSG_TYPE"]) {
            case "typing": {
                if (isSameRoom(messageTalkChannelId) && !isMyMessage(message)) {
                    let userInfo = userManager.getUserInfo(messageSenderId);
                    updateTimer(userInfo["USER_ID"]);
                }
                break;
            }
            case "file":
            case "create": {
            	// [My Message가 아닐 때만 처리하는 if문 삭제했습니다]
                //같은 유저 그룹, 즉 같은 스페이스 혹은 룸에서 보내진 메시지인 경우
                //메시지 내용에 mention이 있는 경우 처리
                mqttMsg["mentionContain"] = talkMessageConverter.checkMessageContainUserMention(
                    message["MSG_BODY"],
                    talkData.myInfo.userId
                );
                mqttMsg["realTalkMsg"] = talkContent.convertToMsgBodyText(JSON.parse(mqttMsg.NOTI_ETC));

            	//데스크탑 알림을 위한 부분 이모티콘과 일반 텍스트만 구분해서 여기서 처리하고 파일의 경우는 실제 메시지 생성과 파일 업로드 성공 시점이 다르므로 update부분에서 처리한다.
            	mqttMsg.NOTI_MSG = mqttMsg["realTalkMsg"];
                if(message["MSG_BODY"].includes('"data-type":"emoticon"')){
            		NotificationManager.notify(mqttMsg, "ttalk_emoticon");
            	}
            	else{
            		NotificationManager.notify(mqttMsg, "ttalk_text");	
                }
            }
            // break없이 다음 것도 함께 탄다
            case "userEnter":
            case "userLeave": {
                talkContent.receiveMessage(message);
                
            	// mqttMsg["CH_ID"] = mqttMsg["SPACE_ID"]
                //LNB SpaceList 갱싱 Code
	            if(workspaceManager.getWorkspace(mqttMsg.SPACE_ID).MEM_JOB === 'N'){
	        		jQuery.ajax({
	        			type: "PUT",
	        			url: _workspace.url + "SpaceRoom/SpaceRoomMember",
	        			dataType : "json",
	        			data : JSON.stringify({
	        				"dto":
	        				{
	        					"WS_ID":mqttMsg.SPACE_ID,
	        					"USER_ID":userManager.getLoginUserId(),
	        					"MEM_JOB":"Y"
	        				}
	        			}),
	       		     	contentType: "application/json",
	       		     	async: false,
	       		     	success: function (result) {
	        				if(result.dto.RESULT_CD === "RST0001") {
                                spaceAPI.drawLnbSpace( mqttMsg.SPACE_ID );
                            }
                            workspaceManager.update()
	       		     	}
	        		})	

	            }
                break;
            }
            case "delete": {
                let targetMessage = talkData.messageList.find(function(elem) {
                    return elem["MSG_ID"] === message["MSG_ID"];
                });

                if (targetMessage) {
                    targetMessage.MSG_TYPE = "delete";
                    if(targetMessage.ATTACHMENT_LIST.length){
    					if(targetMessage.ATTACHMENT_LIST[0].ATTACHMENT_TYPE === "file"){
    						message["FILE_DELETE"] = true;
    					}					
    				}               
                    message["USER_ID"] = targetMessage["USER_ID"];                    
                }				
                break;
            }

            case "read":
            case "readList":{
                if (!isSameRoom(message["CH_ID"])) break;
                talkContent.updateUnreadCount();

                break; 	
            }
            case "roomCreate":
                break;
            case "roomUpdate":
                break;
            case "roomDelete":
                break;
            case "reactionCreate": {
                let localMessage = talkData.messageList.find(function(elem) {
                    return elem["MSG_ID"] === message["MSG_ID"];
                });
                if (localMessage) {
                    localMessage["REACTION_USER_COUNT"] = talkUtil.parseInt(localMessage["REACTION_USER_COUNT"]) + 1;
                }
                break;
            }
            case "reactionDelete": {
                let localMessage = talkData.messageList.find(function(elem) {
                    return elem["MSG_ID"] === message["MSG_ID"];
                });
                if (localMessage) {
                    localMessage["REACTION_USER_COUNT"] = talkUtil.parseInt(localMessage["REACTION_USER_COUNT"]) - 1;
                }
                break;
            }

            case "messageUpdate": {
            	talkServer2.ttalk_getMessage(mqttMsg["CH_ID"], message["MSG_ID"])
                	.then(function(res) {
	                	let targetMessage = res.data.dto;
	                	
	                    talkContent.updateMessage(
                    		targetMessage["MSG_ID"],
	                        "ATTACHMENT_LIST",
	                        targetMessage["ATTACHMENT_LIST"]
	                    );
	                    
	                    // messageUpdate는 renderLnb를 여기서 해준다 왜?? 아래에서 하는데????
                        // 확인 필요합니다.
	                    // talkRenderer.component.renderLnb(targetMessage, false, true);
	                    // let lastTextMessage = talkContent.convertToMsgBodyText(targetMessage);
                        // let roomId = mqttMsg['SPACE_ID'];
                        // let msgId = targetMessage['MSG_ID'];
                        // let active =  isActive(messageTalkChannelId);
                        // spaceAPI.setRoomInfo(roomId, msgId, messageSenderId, "messageUpdate", lastTextMessage, active);
                        // lastTextMessage =null; roomId=null; msgId =null; active =null;
                    });
                   
                break;
            }
        }
        
        let msgType = message['MSG_TYPE'];
        let lastMessageText = talkContent.convertToMsgBodyText(message);
        let roomId = mqttMsg['SPACE_ID'];
        let msgId = message['MSG_ID'];
        let active =  isActive(messageTalkChannelId);
        spaceAPI.setRoomInfo(roomId, msgId, messageSenderId, msgType, lastMessageText, active);
        //for garbage clean ( 필요한가. )
        msgType = null; lastMessageText =null; roomId =null; msgId =null; active =null;
    }
    
    const isActive = channelId =>{
        return tsFocus.get() && !(document.hidden) && document.querySelector('#talk').contains(document.activeElement)  && talk.isSameRoom(channelId);
    };

    return {
        render: function(targetSelector, selectedRoomId, isTest) {
            // TODO : 공통 변수 language 로 대체
                    
            const target = document.querySelector(targetSelector);
            if (!target) return;

            target.style.display = "none";
            _render(target);

            setTimeout(function() {
            	_init();
            }, 1);

            target.style.display = "flex";
        },

        handleMessage: function(message) {
            _handleMessage(message);
        },

        // toggleNotiBadge: function() {
        //     _toggleNotiBadge();
        // },

        updateLanguage: function() {
            _updateLanguage();
        },
        isSameRoom (chId) {
        	return isSameRoom(chId)
        },
        isMyMessage: function(message) {
        	return isMyMessage(message);
        },
        
        // dragNdrop 관련 함수
        onDragEnter: function(e,data) {
        	_onDragEnter(e,data);
        },
        onDragLeave: function(e,data) {
        	_onDragLeave(e,data)
        },
        onDragStart: function(e, data) {
        	_onDragStart(e, data)
        },
        onDragOver: function(e, data) {
        	_onDragOver(e, data)
        },
        onDrop: function(e,data) {
        	_onDrop(e,data)
        },
        onDropDialog: function(e,data){
            _onDropDialog(e,data)
        },        
        talk__setProfilePopup : function(userId, isMention){
        	talk__setProfilePopup(userId, isMention)
        },
        updateUserNick: (userId, userNick) => {
            let _talk = document.querySelectorAll('.content__complete-message-container')[0];
            let _talkChild = _talk.childNodes;
            let i, _talkProf;
            for(i=_talkChild.length-1;i>=0;i--){
                _talkProf = _talkChild[i].getElementsByClassName('message__content-header__name');
                //console.log(_talkProf);
                if(_talkProf){
                    if(_talkProf[0].getAttribute('data-user-id') === userId)
                        _talkProf[0].textContent = userNick;
                }
            }
            _talk = null; _talkProf = null; _talkChild = null;
            //console.log(`TEST UserId= ${userId}, UserNick = ${userNick}`);
        },
        isActive : isActive
    };
})();

TEESPACE_WEBSOCKET.addWebSocketHandler("CHN0001", talk.handleMessage);

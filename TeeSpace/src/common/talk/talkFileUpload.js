// 파일 관련 함수 여기에 정리!
const talkFileMeta = {
        // drive meta 쓰기
        driveUploadDTO : function(workspaceId, fileId, fileName, fileExtension, date) {
            let loginUserId = talkData.myInfo.userId;
            fileName = `${fileName}_${date}_${talkUtil.generateNumber()}`;
            
            return {
                dto : {
                	user_id        : loginUserId,
                	workspace_id   : workspaceId,
                	ch_id          : workspaceManager.getChannelList(workspaceId, "CHN0006"),
                	file_parent_id : workspaceManager.getChannelList(workspaceId, "CHN0006"),
                	app_number     : "1",
            		file_id		   : fileId,
                	storageFileInfo: {
                    	file_name      : fileName,
                    	file_extension: fileExtension,
                    	file_size: 0
                	}
                }
            }
        }
}

const talkFileUpload = (function () {
	let localUpload = true;	

	function _getLocalUpload() {
		return localUpload;
	}
	
	function _setLocalUpload(isLocal) {
		localUpload = isLocal;
	}
	
    function isFileNameLong(name) {
        return name.split(".")[0].length > 70;
    }
    
    function onOpenFileUploadDialog(fileList) {
    	if (document.querySelector('top-dialog#talk__file-upload-dialog')) {
    		document.querySelector('top-dialog#talk__file-upload-dialog').remove()
    	}    	
    	
    	talkRenderer.dialog.createTopDialog("talk__file-upload-dialog", "27.5rem", "2.688rem", "채팅방 파일 첨부", "talk__dialog-layout", ()=> {
    		talkRenderer.dialog.renderFileUploadDialog();
    		talkRenderer.dialog.addFileMessage();
    		talkRenderer.dialog.addFileChip(fileList)
        	let count = talkData.fileChooserList.length;
        	document.querySelector('#selectedItemCount').textContent = count;
        	document.querySelector('#top-dialog-root_talk__file-upload-dialog .top-dialog-content').style.maxHeight = window.innerHeight - 53 + "px";
        });
    	
    	Top.Dom.selectById("talk__file-upload-dialog").open();
    }
    
    // 처음 dialog 열 때만 isOpen = true
	function openFileChooser(isOpen) {
		if (isOpen) {
			talkData.fileChooserList.clear();
		}
		
    	if(talkData.fileChooserList.length >= 30) {
            Top.Dom.selectById("driveSendFailDialog").open();
    		//TeeToast.open({text: '파일 전송은 한번에 30개까지 가능합니다.'});
    	 	return;
    	}
		
		let _fileChooser = Top.Device.FileChooser.create({
            multiple: false,
            onBeforeLoad: function() {
        		let temp=[];
        		
                for (let i = 0; i < this.file.length; i++) {                	
                	if (isFileNameLong(this.file[i]["name"])) {
                    	TeeToast.open({text: '파일명 70자 초과 파일은 업로드할 수 없습니다.'});
                    } else {
                    	talkData.fileChooserList.push(this.file[i]);
                    	temp.push(this.file[i]) // addFileChip할 것
                    }
                }
                
                if (temp.length) {
                	// dialog 없을 때, 즉 처음 열 때는 _onOpenFileUploadDialog로 열어준다
            		if (!document.querySelector('#top-dialog-root_talk__file-upload-dialog')) {
            			// dialog open시 그려준다
						onOpenFileUploadDialog(temp);
						if(temp.length > 30){
							TeeToast.open({text: '파일 전송은 한번에 30개까지 가능합니다.'});
						}
            		} else {
                    	let count = talkData.fileChooserList.length;
                    	document.querySelector('#selectedItemCount').textContent = count;
						// 그리기
						talkRenderer.dialog.addFileChip(temp)						
						if(count > 30){
							$('#talk__action-btn').attr('disabled', true);
							TeeToast.open({text: '파일 전송은 한번에 30개까지 가능합니다.'});
						}
            		}
                }
                return true;
            }
        });
		
		_fileChooser.show();
	}
	
    function deleteFileChip(idx, node) {
    	// 채팅방에서 첨부할 때
    	if(localUpload){    	
	    	if (talkData.fileChooserList.length) {
	    		talkData.fileChooserList.remove(talkData.fileChooserList[idx]);
	    		// chip을 지운다
	            node.remove();
	            document.querySelector('#selectedItemCount').textContent = talkData.fileChooserList.length;
	    	}
	    	
	        // 선택된 파일이 0개가 된 경우 업로드 버튼 비활성화
	        if (!talkData.fileChooserList.length) {
	        	$('#talk__action-btn').attr('disabled', true)
	        	TeeToast.open({text: '선택된 파일이 없습니다.'});
	        } else if(talkData.fileChooserList.length > 0 && talkData.fileChooserList.length < 31){
				$('#talk__action-btn').attr('disabled', false);
			} else if(talkData.fileChooserList.length > 30){
				$('#talk__action-btn').attr('disabled', true);
			}
    	} else {
    		if(talkData.fileMetaList.length){
	    		if(talkData.fileMetaList[idx].storageFileInfo){
	    			let fileId = talkData.fileMetaList[idx].storageFileInfo.file_id;
	    			talkServer2.deleteStorageFile(talkFileUpload.deleteFileMetaFormat(fileId));
	    		} 
	    		    		
	    		talkData.fileMetaList.splice(idx, 1, "");
	    		//talkData.fileMetaList.remove(talkData.fileMetaList[idx]);
		        node.remove();
		        
		        let num = 0;
		        for(let i=0; i<talkData.fileMetaList.length; i++){
		        	if(talkData.fileMetaList[i] != ""){
		        		num++;
		        	}
		        }
		        document.querySelector('#selectedItemCount').textContent = num;
		    	
		        if(num == 0) {
		            $('#talk__action-btn').attr('disabled', true)
		        	TeeToast.open({text: '선택된 파일이 없습니다.'});
		        }
    		}
    	}
    }
	
//	return Object.freeze({
//	openFileChooser: openFileChooser,
//    deleteFileChip, deleteFileChip,
//    isFileNameLong:isFileNameLong,
//    onOpenFileUploadDialog:onOpenFileUploadDialog,
	//})
    return {    	
    	getLocalUpload: function() {
    		_getLocalUpload();
    	},
    	
    	setLocalUpload: function(isLocal) {
    		_setLocalUpload(isLocal);
    	},    	
    	
    	isFileNameLong: function(name) {
    		return isFileNameLong(name);
    	},
    	
    	onOpenFileUploadDialog: function(temp) {
    		onOpenFileUploadDialog(temp)
    	},
    	
    	openFileChooser: function(isOpen) {
    		openFileChooser(isOpen);
    	},
    	
    	deleteFileChip : function(idx, node) {
    		deleteFileChip(idx, node);
    	},
    	// T-drive에서 첨부하기 등 이미 storage에 업로드된 파일 메타
    	// storageManager에 보낼 metaList
        createFileMeta: function(msgId, idx) {
        	let inputList = [];
        	// 파일 묶어보내기인 경우
        	let isBundled = $('#multipleFiles').is(":checked");
			let fileMeta = talkData.fileChooserList[idx];
				
            let fileName = Top.Util.Browser.isIE() ? fileMeta.name : fileMeta.name.normalize('NFC')
            let fileNameArray = fileName.split('.');
            let file_extension = fileNameArray.length == 1 ? "" : fileNameArray[fileNameArray.length - 1];   
            if (file_extension !== ""){
                fileName = fileName.slice(0,fileName.indexOf(file_extension)-1)
            }
            
            const _date = new Date();
			fileName += `_${_date.getFullYear()}${_date.getMonth()+1}${_date.getDate()}_${talkUtil.generateNumber()}`;
            
            let attachInfo = {
                        "workspace_id": talkData.workspaceId,
                        "channel_id": talkData.talkId,
                        "storageFileInfo": {
                        "user_id": talkData.myInfo.userId,
                        "file_last_update_user_id": talkData.myInfo.userId,
                        "file_id": "",
                        "file_name": fileName,
                        "file_extension": file_extension,
                        "file_created_at": "",
                        "file_updated_at": "",
                        "file_size": fileMeta["size"],
                        "user_context_1": msgId,
                        "user_context_2": "",
                        "user_context_3": ""
                }
            }

    		let inputData = {
    				"dto" : {
	                    "MSG_ID" : msgId,
	                    "ATTACHMENT_TYPE": (isBundled) ? "fileList" :  "file",
	                    "ATTACHMENT_BODY": "",
	                    "STATUS":"create",
	                    "ATTACHMENT_FILE" : attachInfo,
	                    "ch_id" : talkData.talkId
	                    //"ROOM_ID" : talkData.selectedRoomId
    				}
                }
        	return inputData;
        },
        
        deleteFileMetaFormat: function (fileId){
        	let inputData = {
    				"dto" : {
    					"workspace_id": talkData.workspaceId,
            	        "channel_id": talkData.talkId,
            	        "storageFileInfo": {
                	        "user_id": "",
                	        "file_last_update_user_id": "",
                	        "file_id": fileId,
                	        "file_name": "",
                	        "file_extension": "",
                	        "file_created_at": "",
                	        "file_updated_at": "",
                	        "file_size": 0,
                	        "user_context_1": "",
                	        "user_context_2": "",
                	        "user_context_3": ""
            	        }
    				}
                }
        	return inputData;
        },
        
        createFileMetaList: function(msgId, idx=false) {
            let inputList = [];
            // 파일 묶어보내기인 경우
            let isBundled = $('#multipleFiles').is(":checked");
            let inputData;
            
            // 묶어보내기이면 파일 여러개당 메세지 하나
            if (isBundled) {
                for(let i=0; i<talkData.fileChooserList.length; i++) {
                    inputData = talkFileUpload.createFileMeta(msgId, i);
                    inputList.push(inputData)
                }
            }
            
            // 묶어보내기가 아니면 파일 하나당 메세지 하나
            else {
                inputData = talkFileUpload.createFileMeta(msgId, idx);
                inputList.push(inputData)
            }
            
            return inputList
        },
        
        // 로컬에서 첨부(묶어보기일 때와 아닐 때 분기탄다)
        createTempFileMessage: function(idx=false) {
            // targetMessage 만들고 tempMessageList에 push
            let isBundled = $('#multipleFiles').is(":checked"); 
			
			// 파일 메시지 바디에 텍스트 입력시 추가
            let msgBody = document.querySelector('#talk-dialog__text-area').innerHTML;
            document.querySelector('#talk-dialog__text-area').innerHTML="";
            
            let targetMessage = talkServer2.createMessageFormat(talkData.selectedRoomId.get(), msgBody, "create")
                        
            targetMessage["MSG_STATE"] = "sending";
            // 맞는치 추후 check
            targetMessage["IS_HEAD"] = false;       
            targetMessage["MSG_BODY"] = talkMessageConverter.htmlMsgToServerMsg(targetMessage["MSG_BODY"]);
                        
            // noti message
            if (isBundled) {
            	if (talkData.fileChooserList.length) {
                	targetMessage["MSG_BODY_TEXT"] = '(파일 ' + talkData.fileChooserList.length + "개) " + msgBody;
                } else if (talkData.fileMetaList.length){
                	targetMessage["MSG_BODY_TEXT"] = '(파일 ' + talkData.fileMetaList.length + "개) " + msgBody;
                }
            } 
            // 묶어보내기가 아닐 때
            else if (idx>=0){
            	targetMessage["MSG_BODY_TEXT"] = '(파일 1개) ' + msgBody;
            }            
            
            // retry 버튼을 위한 복사
            if (isBundled) {
            	if (talkData.fileChooserList.length) {
            		targetMessage["TEMP_FILE"] = Array.prototype.slice.call(talkData.fileChooserList);
            	} else if (talkData.fileMetaList.length) {
            		targetMessage["TEMP_FILE"] = Array.prototype.slice.call(talkData.fileMetaList);
            	}
                
            } else {
                // deep copy
            	if (talkData.fileChooserList.length) {
            		targetMessage["TEMP_FILE"] = $.extend(true, {}, talkData.fileChooserList[idx]);
            	} else if (talkData.fileMetaList.length) {
            		targetMessage["TEMP_FILE"] = $.extend(true, {}, talkData.fileMetaList[idx]);
            	}
                
            }
			
			// temp에 추가
            // 다른 앱에서 보내는데 미니채팅방이 있는 경우
			//0410 내가 보고 있는 방에 파일을 올리는 지 아닌지를 확인 후에 temp메시지를 그려야 해서
			//temp메시지 그리는 부분을 이 함수 바깥으로 뺐다
			// 예전에는 createTempFile을 하면 자동적으로 그리지만 이제는 createTempFile을 하고 
			// 그릴지 말지 확인후에 그릴 것이다. 
			// if ($('#talk').length) {
            // 	talkData.tempMessageList.push(targetMessage)
            // }
            return targetMessage;
        },
        
        // TeeDrive에서 채팅방으로 보낼 때 attachment_list 만들어주는 함수
        getAttachmentList : function(idx) {
        	let isBundled = $('#multipleFiles').is(":checked"); 
        	let inputList = [];
        	let inputData;
        	
        	let selectedRoomId = talkData.talkId;//talkData.selectedRoomId.get();
        	if (!selectedRoomId) {
				// create Message하기 위해서는 roomId가 필요하다(talkData.talkId를 가져온 후 roomId를 얻는다)
				talkData.talkId = workspaceManager.getChannelList(talkData.workspaceId, "CHN0001");
				talkOpenAPI.getSelectedRoomId();
        	}
        	
        	if (isBundled) {
        		for (let i=0; i<talkData.fileMetaList.length; i++) {
    				inputData = {
				        "ATTACHMENT_TYPE":"fileList",
				        "ATTACHMENT_BODY": talkMessageConverter.storageMetaFormatToAttachmentFormat(talkData.fileMetaList[i]),
				        "STATUS":"create",
				        "ATTACHMENT_FILE" : null,
				        "ch_id" : talkData.talkId
				        //"ROOM_ID" : talkData.selectedRoomId.get()
			        };
    				inputList.push(inputData)
    			}
        	} 
        	// fileMetaList는 fileMetaList가 아니라 개별 fileMeta다
        	else {
        		inputData = {
			        "ATTACHMENT_TYPE":"file",
			        "ATTACHMENT_BODY": talkMessageConverter.storageMetaFormatToAttachmentFormat(talkData.fileMetaList[idx]),
			        "STATUS":"create",
			        "ATTACHMENT_FILE" : null,
			        "ch_id" : talkData.talkId
			        //"ROOM_ID" : talkData.selectedRoomId.get()
		        };
        		inputList.push(inputData);
        	}
        	return inputList;
        },
        
        // 로컬에서 첨부하기 메뉴에서 '업로드'버튼 클릭시 제일 처음 실행된다.
        uploadLocalFiles: function () {
            let fileCount = talkData.fileChooserList.length;
			if (!fileCount) return;

			let targetMessage;
			let isBundled = $('#multipleFiles').is(":checked");
			let totalFileSize =0;
			let totalFileCount = talkData.fileChooserList.length;
			
			for(let i=0;i<totalFileCount;i++) 
								totalFileSize += talkData.fileChooserList[i]['size'];
			
			if(totalFileSize > 209715200){
                TeeAlarm.open({title: '용량 초과', content: '업로드 할 파일은 200MB를 넘을 수 없습니다.'});									
                Top.Loader.stop(); return;			
			}
			 
            if (isBundled) {
                // 묶어보내기인 경우
				targetMessage = talkFileUpload.createTempFileMessage();
				talkData.tempMessageList.push(targetMessage);
                talkContent.sendMessage(targetMessage, false,talkData.workspaceId);                    
            } else {
                // 묶어보내기 아닌 경우
                for (let i=0;i< fileCount;i++) {
                    targetMessage = talkFileUpload.createTempFileMessage(i);
					talkData.tempMessageList.push(targetMessage);
					talkContent.sendMessage(targetMessage, i,talkData.workspaceId);
                }
            }
                       
        },
        
        teeDriveAjaxCall : function(serviceUrl, type, inputDTO) { 
            let outputData = null;
            try {
                Top.Ajax.execute({
                    url :  serviceUrl,
                    type : type,
                    dataType : "json",
                    async : false,
                    cache : false,
                    data : inputDTO,
                    contentType : "application/json",
                    crossDomain : true,
                    xhrFields : {
                        withCredentials: true
                    },
                    success : function(data) {
                    },
                    error : function(data) {
                    }
                });
            } catch(ex) {
                StartLoader(false);
            }
                
            return outputData;
        },
        
        // storage에 업로드하는 함수
        uploadFiles : function (msgId, idx=false) {    
            let inputList = talkFileUpload.createFileMetaList(msgId, idx);
            Top.Loader.start();
            
            
            // 묶어보내기면 여러 개 보내고, 아니면 하나만 보낸다.
            let isBundled = $('#multipleFiles').is(":checked"); 
            let totalFileCount = 1;
            let file = talkData.fileChooserList[idx];
            
            if (isBundled) {
                totalFileCount = talkData.fileChooserList.length;
            }
            
            let successCount = 0;
            let errorCount = 0;
            
            for (let i = 0; i < totalFileCount; i++) {
                if (isBundled) {
                    file = talkData.fileChooserList[i]
                }
                storageManager.UploadFile(file,
            		inputList[i], "Messenger", "ttalkattachment",
//            		"", "Messenger", "ttalkattachment",
                    function (res) {  
    	                if (res.dto["resultMsg"] !== "Success") {
    	            	    talkContent.updateMessage(msgId, 'MSG_STATE', "fail");
                            errorCount += 1;
    	                }
    	                else {
    	                	// 드라이브에 업로드
    	                	let fileInfo = res.dto.storageFileInfoList[0];
    	                	let fileId = fileInfo["file_id"];
    	                	let fileName = fileInfo["file_name"];
    	                	let fileExtension = fileInfo["file_extension"];
    	                	let fileDate = fileInfo["file_created_at"].split(' ')[0].replace(/\//g,'');    	                	
    	                	
    	                	let driveDTO = talkFileMeta.driveUploadDTO(talkData.workspaceId, fileId, fileName, fileExtension, fileDate);
    	                	let driveServiceUrl = _workspace.fileUrl+ "DriveFile";
    	                	talkFileUpload.teeDriveAjaxCall(driveServiceUrl, "POST", driveDTO)
    	                	drive.talkRenderView();
    	                	 
    	                    talkContent.updateMessage(msgId, 'MSG_STATE', "success");
                            successCount += 1;                            
                            
                            if (!isBundled) {
                                Top.Loader.stop();
                                talkServer2.sendWebsocketMessage(msgId);
                                
                            }
    	                }
    	                // 전체 Callback을 받은 경우에 호출 되도록 변경한다.
                        if( isBundled && totalFileCount === (errorCount+successCount) ){
                            talkFileUpload.completeUpload(isBundled, i, msgId, successCount, errorCount);
                        }
                        
                        if (!isBundled && idx === talkData.fileChooserList.length-1) {
                            talkFileUpload.completeUpload(isBundled, i, msgId, successCount, errorCount)
                        }
    	            }, function (err) {   
                        talkContent.updateMessage(msgId, 'MSG_STATE', "fail");
                        errorCount += 1;
                        // 전체 Callback을 받은 경우에 호출 되도록 변경한다.
                        if( isBundled && totalFileCount === (errorCount+successCount) ){
                            talkFileUpload.completeUpload(i, msgId, successCount, errorCount)
                        }
                        
                        if (!isBundled && idx === talkData.fileChooserList.length-1) {
                	        Top.Dom.selectById('talk__file-upload-dialog').close(true);
                	        // 두 번 넣어야 멈춰지는 경우가 있어
                        	for (let j=0;j<talkData.fileChooserList.length;j++) {
                        		Top.Loader.stop();
                        	}
                            talkData.fileChooserList.clear();
                            talkContent.updateMessage(msgId, 'MSG_STATE', "fail");
						}
						
						document.querySelector('#talk-content').click();
						document.querySelectorAll('.lnbSpaceList.dnd__container1.selected')[0].click();
					   
                    });
			}
			
        },        
        
        completeUpload : function (isBundled, i, msgId, successCount, errorCount) {
            // 마지막 caller가 아닌 length와 success+errocount로 call하게 한다.
            // 이부분은 앞 에서 먼저 체크 해서 부른다.
            // Loader의 경우 추후 각 File Uploader에서 부르도록 변경한다.
        	for (let j=0;j<talkData.fileChooserList.length;j++) {
        		Top.Loader.stop();
        	}
			Top.Dom.selectById('talk__file-upload-dialog').close(true);
			talkFooter.updateButtonStatus(true);			
	        // document.querySelector('#talk-content').click();
	        // document.querySelectorAll('.lnbSpaceList.dnd__container1.selected')[0].click();
            
	        talkData.fileChooserList.clear();
            
            if (isBundled) {
                if (errorCount) {
                	TeeToast.open({text: `${successCount}개의 파일 전송에 성공하고 ${errorCount}개 파일 전송에 실패했습니다.`});
                }
                else {
                	TeeToast.open({text: `${successCount}개 파일 전송에 성공했습니다.`});    
                }
                if (successCount) {
                    // 하나라도 성공하면 일단 success
                    talkContent.updateMessage(msgId, 'MSG_STATE', "success");
                }
                talkServer2.sendWebsocketMessage(msgId);
            }                
        },
        
        // file render 부분
        
        // 파일 묶어보내기할 때 한 줄에 파일 몇 개씩 들어가야 하는지
        alignFiles : function(count) {
        	// count === 1인 경우는 이미 걸렀음
        	var rowItems, row;
        	
        	if (!(count%3)) {
        		// 3의 배수, 3*x
        		// 줄 개수
        		row = parseInt(count/3);
        		rowItems = new Array(row);
        		
        		for (var i=0;i<row;i++) {
        			rowItems[i] = 3;
        		}
        	} else if (count%3 === 2) {
        		// 3*x + 2
        		row = parseInt(count/3)+1;
        		rowItems = new Array(row);
        		
        		for (var i=0;i<row-1;i++) {
        			rowItems[i] = 3;        			
        		}
        		rowItems[row-1] = 2;
        	} else {
        		// 3*x + 4
        		row = parseInt((count-4)/3)+2;
        		rowItems = new Array(row);
        		
        		for (var i=0;i<row-2;i++) {
        			rowItems[i] = 3
        		}
        		rowItems[row-2] = 2;
        		rowItems[row-1] = 2;
        	}
        	return rowItems;
        },
        
        renderFile : function(targetMessage, targetAttachment, messageLayout) {
        	const msgId = targetMessage["MSG_ID"];
        	let attachmentContainer = document.querySelector('[data-message-id="'+msgId+'"] .message__attachment-container');
        	
        	let attachmentLayout = document.createElement("div");
        	attachmentLayout.classList.add("message__attachment-layout");
        	attachmentLayout.setAttribute("data-attachment-id", targetAttachment["ATTACHMENT_ID"]);
        	
	        let attachmentMsg = talkMessageConverter.convertFileTagToHTML(false, targetAttachment["ATTACHMENT_BODY"]);
	
	        let attachmentBody  = document.createElement("div");
	        attachmentBody.classList.add("message__attachment-body")
	        attachmentBody.insertAdjacentHTML('beforeend', attachmentMsg);

	        attachmentLayout.appendChild(attachmentBody)
	        attachmentContainer.appendChild(attachmentLayout)

			let fileInfoContainer = document.querySelector('[data-message-id="'+msgId+'"] .file-info');
			
			// 손상된 파일인 경우 render image error 처리시 이벤트 지운다 
			fileInfoContainer.addEventListener("mouseenter", function() {
				talkRenderer.component.renderHoveredFileInfo(this)
			}, false)
			
			fileInfoContainer.addEventListener("mouseleave", function() {
				talkRenderer.component.revertToFileInfo(this)
			}, false)
        	
			let imageContainer = attachmentBody.querySelector('.file-message__image-container');
			if (imageContainer) {
				imageContainer.addEventListener("click", function() {
					let srcUrl = this.querySelector('img').getAttribute('src');
					let fileName = this.querySelector('img').getAttribute('data-file-name');
					let fileExt = this.querySelector('img').getAttribute('data-file-extension');					
					
					let fileInfo = {
						file_name : fileName,
						file_extension : fileExt
					}
                    if(srcUrl === "./res/talk/deleted_face.svg"){
                    	drivePreview.open(fileInfo, "image", srcUrl, true);
                    } else {
                    	drivePreview.open(fileInfo, "image", srcUrl);
                    }
				}, false)
			}
	        
			
			// style먹이기
			let contentContainer = document.querySelector('[data-message-id="'+msgId+'"] .message__content-body__items');
			let msgBodyContainer = document.querySelector('[data-message-id="'+msgId+'"] .message__content-body__content');
			let attachContainer = document.querySelector('[data-message-id="'+msgId+'"] .message__attachment-container');
			
			// 파일 첨부된 메시지가 답장인 경우 화살표 없애기
			if (talkContent.isReply(msgId)) {
				if (!targetMessage["MSG_BODY"]) {
					msgBodyContainer.style.display = "none"
				}	
				if (talkData.myInfo.userId !== targetMessage["USER_ID"]) {
					contentContainer.querySelector('.message__reply-container').style.padding = "0.63rem 0.63rem 0 0.63rem";			
				}	
			}
			contentContainer.style.maxWidth = "15rem";
			attachContainer.style.maxWidth = "15rem";
			
			// msg body가 없는 경우 아니면 create일 때?
			if (contentContainer.classList.contains("other-message")) {
				contentContainer.style.padding = "0rem";
				if (targetMessage["MSG_BODY"]) {
					// reply도 보기
					msgBodyContainer.style.padding = "0.5rem";
					attachContainer.classList.add("talk-message__file-meta-with-msg");
				} else {
					attachContainer.classList.add("talk-message__file-meta-without-msg");
				}
			}
	        // 마지막 메세지 5개 image load event 추가
	        talkContent.loadImageEvent(targetMessage, messageLayout, ".message__image-file");	
        },
        
        renderBundleLayout : function(targetMessage, targetAttachment, messageLayout) {
        	var fileCount = targetMessage["ATTACHMENT_LIST"].length;
        	if (fileCount === 1) {
        		// 파일 한 개만 묶어보내기 한 경우 개별 파일 render 함수로 동작 바꾼다
        		talkFileUpload.renderFile(targetMessage, targetAttachment, messageLayout); 
        		return
        	}
        	// 한 줄에 몇 개 들어가는지 array
        	var rowItemCount = talkFileUpload.alignFiles(fileCount);
        	// 몇 줄인지
        	let rowCount = rowItemCount.length;
            let _width, _max_width = '';
            
            //TODO : 일단 주석처리, 모아보기만 일단 510px로 제공
            if (talkUtil.isMobile()) {
                _width = "16.5rem"
//                _max_width = "calc(100% - 3.75rem)"
//            } else if (talkData.isSimple) {
//                _width = "205px" // 미니채팅방은 메시지 크기 제한 없음
            } else {
                _width = "18.75rem"
            }
            
        	
        	// attachment-container css 변경
	        var _container = $(".message__attachment-container", messageLayout);
	        
            $('.message__content-body__items', messageLayout).css({
                "width" : _width,
                "max-width" : _max_width,
                "box-sizing" : "content-box" // padding 제외하고 _width 너비 갖도록 한다
            })
            
	        // simple일 때 모바일 비슷해야
	        var flag = (talkData.isSimple) ? "--simple" : "";
	        
	        // 일단 Desktop 510px, mobile은 264px 너비로 맞춤
	        _container.css({
	        	"flex-direction": "row",
	        	"align-items" : "center",
	        	'justify-content' : 'center',
	        	"flex-wrap" : "wrap", // 개행
                "width":_width,
                "max_width" : _max_width
	        });
	        
	        if (_container.parent().width() < _container.width()) {
	        	_container.css({
	        		"width" : "100%"
	        	})
	        }
	        
	        // 계층 구조는 attachment-container 아래에 fileList-item
	        for (var i=0;i<rowCount;i++) {
	        	// 한 줄에 몇 개 있는지
        		var itemCount = rowItemCount[i];
        		var className = (itemCount === 2) ? "message__fileList-item2 dnd__item" : "message__fileList-item3 dnd__item";
        		
        		
	        	for (var j =0; j <itemCount;j++) {
	        		var fileItem = $("<div/>")
			            .addClass(className+flag)
			            .appendTo(_container)     		
	        		
	        	}
	        }
	        
	        // Bundle 중 첫 번째 file render하기
	        talkFileUpload.renderBundledFileItem(targetMessage, targetAttachment, messageLayout, 0)
        },
        
        renderBundledFileItem : function(targetMessage, targetAttachment, messageLayout, i) {
            let arr = targetAttachment["ATTACHMENT_BODY"].match(/<(.*?)>/g);
            if (!arr) return

            arr = arr.filter(function(elem) {
                if (elem.indexOf('"data-type":"fileList"') !== -1 || elem.indexOf("'data-type':'fileList'") !== -1) return elem;
            });
            
            let _obj;
            let msgId = targetMessage["MSG_ID"];
            
            // 어차피 한 개이겠지만
            for (let j=0;j<arr.length;j++) {
            	
            	_obj = talkContent.talk_parseFile(arr[j]);
            	
            	let fileId = _obj["data-file-id"]; 
            	
            	let extension = _obj["data-file-extension"] ? "."+_obj["data-file-extension"] : "";
            	let fileSize = _obj["data-size"] ? getFileSizeText(parseInt(_obj["data-size"])) : "";
            	
            	let fileItemContainer = document.querySelector('[data-message-id="'+msgId + '"] .message__attachment-container').children[i]
            	fileItemContainer.setAttribute("data-attachment-id", targetAttachment["ATTACHMENT_ID"]) 
            	fileItemContainer.setAttribute("data-file-id", fileId) 
            	fileItemContainer.setAttribute("onclick", `talkMessageConverter.openContextMenu(event, "${fileId}")`)
            	
            	let srcUrl;
            	let _fileIconClass = fileExtension.getInfo(_obj["data-file-extension"])["iconImage"]
            	
            	// 이미지일 경우 바로 보여준다
            	switch (_fileIconClass){
            		case "./res/drive/file_icons/drive_image.svg": 
            			let messageOverlay = document.createElement('div')
            			messageOverlay.classList.add('message__overlay')
            			
            			let fileName = document.createElement('p')
            			fileName.textContent = _obj["data-file-name"]+"." + _obj["data-file-extension"];
            			let downloadImg = document.createElement('div')
            			downloadImg.classList.add("message__image-icon")
            			downloadImg.classList.add("icon-work_download")
            			messageOverlay.appendChild(fileName);
            			messageOverlay.appendChild(downloadImg);
            			fileItemContainer.appendChild(messageOverlay)
            			
        				srcUrl = storageManager.GetFileUrl(fileId, talkData.talkId, talkData.workspaceId);
            			
            			let img = document.createElement('img');
            			img.setAttribute('src', srcUrl);
            			img.setAttribute('onerror', "talkRenderer.component.renderImageError(this, true)")
            			fileItemContainer.appendChild(img)
            			
            			// 미리보기
//				        fileItemContainer.addEventListener("click", function() {
//				        	let srcUrl = this.querySelector('img').getAttribute('src');
//				        	drivePreview.open("image", srcUrl);
//				        }, false)
				        
    		            break;
                		
            		default:   
            			let template = `
            				<img src="${_fileIconClass}" style="width:2.7rem"/>
            				<div>
            					<span class="talk-tooltip">${_obj["data-file-name"]}</span>
            					<span style="white-space:nowrap">${extension}</span>
            				</div>
            				<span style="font-size:0.813rem">${fileSize}</span>
            			`
        				fileItemContainer.insertAdjacentHTML("beforeend", template);
        				fileItemContainer.classList.add("message__bundle-item");         
//        				fileItemContainer.setAttribute("onclick", `talkMessageConverter.openContextMenu(event, "${fileId}")`)
            			break;            			
            	}  
            };
            
        	
			// 마지막 메세지 5개 image load event 추가
	        talkContent.loadImageEvent(targetMessage, messageLayout, "img");
        },
    	
    }
})()
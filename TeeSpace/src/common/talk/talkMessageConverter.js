const talkMessageConverter = (function() {
    let debugMode = false;

    function htmlMsgToServerMsg(inputHtml) {
        if (debugMode) {
            console.warn("============ INPUT-> SERVER =============");
            console.warn("INPUT Message = ", inputHtml);
        }
        inputHtml = convertHtmlParagraphToTag(inputHtml);
        if (debugMode) {
            console.warn("remove paragraph = ", inputHtml);
        }

        inputHtml = convertHtmlEmoticonToTag(inputHtml);
        if (debugMode) {
            console.warn("remove emoticon = ", inputHtml);
        }

        inputHtml = convertHtmlUrlToTag(inputHtml);
        if (debugMode) {
            console.warn("remove Url = ", inputHtml);
        }

        inputHtml = convertHtmlMailToTag(inputHtml);
        if (debugMode) {
            console.warn("remove Mail = ", inputHtml);
        }

        // Mention에 띄어 쓰기가 있기에 먼저 처리한다.
        inputHtml = convertHtmlMentionToTag(inputHtml);
        if (debugMode) {
            console.warn("remove Mention = ", inputHtml);
            console.warn("====================================\n");
        }
        return inputHtml;
    }

    function serverMsgToHtmlMsg( serverMsg, noHtml) {
        if (debugMode) {
            console.warn("============ SERVER -> VIEW =============");
            console.warn("RECEIVE Message = ", serverMsg);
        }

        serverMsg = _convertEmoticonTagToHtml(serverMsg, noHtml);
        if (debugMode) {
            console.warn("Emoticon Convert = ", serverMsg);
        }

        serverMsg = convertFileTagToHTML( serverMsg, noHtml);
        if (debugMode) {
            console.warn("file Convert = ", serverMsg);
        }

        serverMsg = convertURLTagToHtml(serverMsg, noHtml);
        if (debugMode) {
            console.warn("Url Convert = ", serverMsg);
        }

        serverMsg = convertMailTagToHtml(serverMsg, noHtml);
        if (debugMode) {
            console.warn("Url Convert = ", serverMsg);
        }

        serverMsg = convertMentionTagToHtml(serverMsg, noHtml);
        if (debugMode) {
            console.warn("Mention Convert = ", serverMsg);
        }
        if (!noHtml) serverMsg = convertNewLineTagToHtml(serverMsg);
        if (debugMode) {
            console.warn("NewLine Convert = ", serverMsg);
            console.warn("====================================\n");
        }

        return serverMsg;
    }
    function storageMetaFormatToAttachmentFormat(storageFormatMeta){
    	let isBundled = $('#multipleFiles').length ? $('#multipleFiles').is(":checked") : false; 

//      2020/4/17 시간 보여주는 기획 없어서 주석처리 - 나중에 기획 변경 있다면 다시 추가
//    	let tempTime;
//    	
//    	if(storageFormatMeta.file_updated_at){
//    		tempTime = storageFormatMeta.file_updated_at.split(" ");
//    	} else {
//    		tempTime = storageFormatMeta.storageFileInfo.file_updated_at.split(" ");
//    	}
//    	//[TODO]storage 쪽에서 고정되어 있는 시간을 주고 있다. 이럴 경우 시간대가 다른 지역에서 접속하였을 때 다르게 고려해주는 채팅 앱과 상이하게 시간을 보여줄 수 있으므로 변환 필요 향후 수정 요망
//    	let linuxTime = new Date(tempTime[0].replace("/","-").replace("/","-")+"T"+tempTime[1]);
    	let attachmentFormat;
    	
    	if (!isBundled) {
    		if(!storageFormatMeta.storageFileInfo) {
    			attachmentFormat= "<{'data-type':'file','data-user-id':'" + storageFormatMeta.user_id + "','data-file-id':'" + storageFormatMeta.file_id + "'," +
    								"'data-file-name':'" + storageFormatMeta.file_name + "','data-file-extension':'" + storageFormatMeta.file_extension + "','data-size':'" + 
    								storageFormatMeta.file_size + "','data-updated-at':'" + "123" + "'}>"; //linuxTime.getTime()
    		} else {
    			attachmentFormat= "<{'data-type':'file','data-user-id':'" + storageFormatMeta.storageFileInfo.user_id + "','data-file-id':'" + storageFormatMeta.storageFileInfo.file_id + "'," +
									"'data-file-name':'" + storageFormatMeta.storageFileInfo.file_name + "','data-file-extension':'" + storageFormatMeta.storageFileInfo.file_extension + "','data-size':'" + 
									storageFormatMeta.storageFileInfo.file_size + "','data-updated-at':'" + "" + "'}>"; //linuxTime.getTime()
    		}
    	} else {
    		attachmentFormat= "<{'data-type':'fileList','data-user-id':'"+storageFormatMeta.user_id+"','data-file-id':'"+storageFormatMeta.file_id+"','data-file-name':'"+storageFormatMeta.file_name+"','data-file-extension':'"+storageFormatMeta.file_extension+"','data-size':'"+storageFormatMeta.file_size+"','data-updated-at':'"+linuxTime.getTime()+"'}>";
    	}
    	
    	return attachmentFormat;
    }
    function convertHtmlParagraphToTag(inputText) {
        inputText = inputText.replace(/<p><br><\/p>/g, "\n");
        // 첫 줄을 줄바꿈해서 입력하는 경우
        if (inputText.startsWith('<p>')) {
        	inputText = inputText.slice(3,inputText.length);
        } 
        
        if (inputText.endsWith('</p>')) {
        	inputText = inputText.slice(0,-4);
        }
        inputText = inputText.replace(/<\/p>\n/g, '\n');
    	inputText = inputText.replace(/<\/p><p>/g, "\n");
        inputText = inputText.replace(/<p>/g, "\n");
        inputText = inputText.replace(/<\/p>/g, "\n");
        inputText = inputText.replace(/<br>/g, "\n");
        // <p> --> \n, br은 잘못들어 간거라서 제거
        //		기존 코드
        //      inputText = inputText.replace(/<p>(.*?)<\/p>/g, "\n$1")

        //        let tag = inputText.match(/(<p>|<\/p>)/g)
        //        if (tag) {
        //            for (let i = 0; i < tag.length; i++) {
        //                if (tag[i] === "<p>") {
        //    				inputText = inputText.replace(/<p>/g, "\n")
        //                }
        // </p>이면
        //                else {
        //                    inputText = inputText.replace(/<\/p>/g, "")
        //                }
        //            }
        //        }

        return inputText;
        // return inputText.replace(/\<\/p\>\<p\>/gi, "\n").replace(/\<p\>|\<\/p\>/gi, "").replace(/\<br\>/g, "");
    }

    function convertNewLineTagToHtml(inputText) {
        let lineBreak = inputText.match(/\n/g);
        if (lineBreak) {
            inputText = "<p style='margin:0'>" + inputText + "</p>";
            inputText = inputText.replace(/\n/g, "</p><p style='margin:0'>");
            inputText = inputText.replace(/<p style='margin:0'><\/p>/g, "<p style='margin:0'><br></p>");
            inputText = inputText.replace(/<p style='margin:0'> <\/p>/g, "<p style='margin:0'><br></p>");
            //    		inputText = inputText.replace(/\n/g, "</p><p style='margin:0'>")
        }
        //        inputText = inputText.replace(/\n/g, "<br>");

        // 할게 없는거 같다.
        return inputText;
    }

    // ?포함한 문자열을 정규표현식으로 찾을 때
    function _createUrlTag(elem, inputText) {
        let temp = elem;
        if (temp.includes("?")) {
            temp = temp.replace(/\?/g, "\\?");
        }

        let _RegExp = new RegExp(temp, "g");
        let check = _RegExp.exec(inputText);
        if (inputText[check.index - 1] === "@") {
            return false;
        }

        let obj = JSON.stringify({
            "data-type": "url",
            "data-url": check[0]
        });

        inputText = inputText.replace(_RegExp, "<" + obj + ">");

        return inputText;
    }

    // naver.com --> <naver.com>
    function convertHtmlUrlToTag(inputText) {
        let newLineRegExp = new RegExp("\n", "g");
        let spaceDelimiter = new RegExp("[\\s]", "g");

        let stringTypeUrlRegExp = new RegExp(
            "((?:https?:\\/\\/)+(?:(?:[a-zA-Z0-9\\-]+)\\.)+(?:[a-zA-Z0-9\\-]+)(?:[a-zA-z0-9\\!\\/\\?\=\\%\\&\\.\\-\\:\\#\\;])+)", "g"
            // "(http|http(s)?:\\/\\/)?(www+\\.)?(\\w+\\.)+(net|co\\.kr|com|in|org|ac\\.kr)+([/?%&=][^\\s]*)?", "g"
        );
        let numberTypeUrlRegExp = new RegExp(
            "(http|http(s)?:\\/\\/)?([0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3})(:[0-9]{1,4})?(\\/)?(www+\\.)?([\\w-]+)?(\\.net|\\.co\\.kr|\\.com|\\.in|.org)?([/?%&=][^\\s]*)?",
            "g"
        );
        let eachLines = inputText.split(newLineRegExp);
        for (let lineCount = 0; lineCount < eachLines.length; lineCount++) {
            let oneLine = eachLines[lineCount];
            let eachWords = oneLine.split(spaceDelimiter);
            for (let spaceCount = 0; spaceCount < eachWords.length; spaceCount++) {
                let oneWord = eachWords[spaceCount];

                let result; 
                    result = oneWord.match(stringTypeUrlRegExp);
                    if (result) {
                        let temp = result[0];
                        result[0] = result[0].replace(/&nbsp;/g, "");
                        result[0] = result[0].replace(/&amp;/g, "&");

                        let target = JSON.stringify({ "data-type": "url", "data-url": result[0] });
                        eachWords[spaceCount] = oneWord.replace(temp, "<" + target + ">");
//                        console.warn("STRING URL")
                    } else {
                        result = oneWord.match(numberTypeUrlRegExp);
                        if (result) {
                            let temp = result[0];
                            result[0] = result[0].replace(/&amp;/g, "&");
                            let target = JSON.stringify({ "data-type": "url", "data-url": result[0] });
                            eachWords[spaceCount] = oneWord.replace(temp, "<" + target + ">");
//                            console.warn("IP URL")
                        }
                    // }
                }
            }
            //            eachLines[lineCount] = eachWords.join("&nbsp;");
            eachLines[lineCount] = eachWords.join(" ");
        }
        return eachLines.join("\n");        
    }
    
    function convertHtmlMailToTag(inputText) {
        let arr = inputText.match(/[\w+\-.]+@[a-z\d\-.]+\.[a-z]+/g);
        if (!arr) {
            return inputText;
        }
        
        for(let i=0; i<arr.length; i++){
        	let obj = JSON.stringify({
                "data-type": "mail",
                "data-url": "mailto:tmp" + i
            });
        	inputText = inputText.replace(arr[i], "<" + obj + ">");
        }
    
	    for(let i=0; i<arr.length; i++){
	    	inputText = inputText.replace("mailto:tmp" + i, "mailto:" + arr[i]);	
	    }
                
        return inputText;
    }
    
    function convertHtmlEmoticonToTag(inputText) {
        let arr = inputText.match(/<img(.*?)>/g);
        if (!arr) return inputText;

        arr = arr.filter(function(elem) {
            if (elem.indexOf('data-type="emoticon"') !== -1) return elem;
        });

        arr.forEach(function(elem) {
            let _elem = $(elem);

            let _width = _elem.css("width").substring(0, _elem.css("width").length-3)*1;
            
            // 큰 이모티콘은 크기를 더 키워준다
            if (_width === 5) {
            	_width = "6"
            }
            
            let obj = JSON.stringify({
                "data-type": _elem.attr("data-type"),
                "data-file-name": _elem.attr("data-file-name"),
                "data-file-extension": _elem.attr("data-file-extension"),
                "data-scale": _elem.attr("data-scale"),
                "data-size": _elem.attr("data-size"),
                "data-index": _elem.attr("data-index"),
                "data-width": _width
            });

            inputText = inputText.replace(elem, "<" + obj + ">");
        });
        return inputText;
    }

    function checkMessageContainUserMention(messageBody, userId) {
        if (!messageBody) {
            return false;
        }
        let arr = messageBody.match(/<(.*?)>/g);
        if (!arr) {
            return false;
        }

        arr = arr.filter(function(elem) {
            if (elem.indexOf('"data-type":"mention"') !== -1) return elem;
        });

        let exist = false;
        for (let index = 0; index < arr.length; index++) {
            let targetElement = arr[index];
            targetElement = targetElement.replace(/[\<\>]/g, "");

            let targetJson = JSON.parse(targetElement);
            if (userId && userId === targetJson["data-user-id"]) {
                exist = true;
                break;
            }
        }

        return exist;
    }

    function _convertEmoticonTagToHtml(inputText, noHtml) {
        let arr = inputText.match(/<(.*?)>/g);
        if (!arr) return inputText;

        arr = arr.filter(function(elem) {
            if (elem.indexOf('"data-type":"emoticon"') !== -1) return elem;
        });

        let path = talkEmoticon.getPath();
        arr.forEach(function(elem) {
            let _elemenet = elem.replace(/[\<\>]/g, "");
            let _obj = JSON.parse(_elemenet);
            
            let _width = _obj["data-width"];            

            switch (Number(_width)) {
	            case 6: // 큰 이모티콘
	            case 2.5: // 작은 이모티콘 한 개(크게 보일 때)
	            case 1.25: // 작은 이모티콘
	            	break;
            	// rem으로 바꾸기 전
	            default:
	            	_width = _width * 0.0625;
	            	break;
            }
            
            let element = $("<span/>").css({
                "display": "inline-block",
                "vertical-align": "bottom",
                "width": _width +"rem",
                "height": _width +"rem",
                "background-image": "url(" + path + _obj["data-file-name"] + "." + _obj["data-file-extension"] + ")",
                "background-position-x": _obj["data-index"] * _width * -1 +"rem",
                "background-position-y": "0rem",
                "background-size": _obj["data-scale"] + "%",
                "background-repeat-x": "no-repeat",
                "background-repeat-y": "no-repeat",
                "background-attachment": "initial",
                "background-origin": "content-box",
                "background-clip": "content-box",
//                transform: "translateY(-3px)"
            })[0].outerHTML;

            if (noHtml) {
                element = "[Emoticon]";
            }
            inputText = inputText.replace(elem, element);
        });
        return inputText;
    }
    
    function convertFileTagToHTML( inputText, noHtml) {
        let arr = inputText.match(/<(.*?)>/g);
        if (!arr) return inputText;

        arr = arr.filter(function(elem) {
            if (elem.indexOf('"data-type":"file"') !== -1 || elem.indexOf("'data-type':'file'") !== -1 || elem.indexOf("'data-type':'fileList'") !== -1) return elem;
        });

        arr.forEach(function(elem) {
        	_obj = talkContent.talk_parseFile(elem);

            let element = null;
            
            let _fileIconClass = fileExtension.getInfo(_obj["data-file-extension"])["iconImage"];
            
            switch (_fileIconClass) {
            	// 이미지만 다르게
                case "./res/drive/file_icons/drive_image.svg":
                    element = talkRenderer.component.renderImageFileTag( _obj, _fileIconClass);
                    break;
                default:
                    element = talkRenderer.component.renderOtherFileTag(_obj, _fileIconClass, false);
                	break;
            }

            if (noHtml) {
                // 확장자.toLowerCase()삭제
                element = "[File: " + _obj["data-file-name"] + "." + _obj["data-file-extension"] + "]";
            }
            inputText = inputText.replace(elem, element);
        });
        return inputText;
    }

    function _unfoldImage(e) {
    	// 이미지 미리보기 접고 펴는 화살표 클릭해서 들어온 경우
        let event = e || window.event;
        // _eventTarget은 arrow
        let _eventTarget = event.target;
        
        // file-info, 파일 이름 클릭해서 함수 들어온 경우라면 target 바꿔주어야 한다
        if (event.target.classList.contains("file-info")) {
        	_eventTarget = e.target.nextElementSibling;
        }
        else if (!event.target.classList.contains("talk-file-message__arrow")) {
        	_eventTarget = $(e.target).closest('div.file-info')[0].nextElementSibling;
        }
        
        let target = $(_eventTarget.parentNode);
        
        let targetChild = $(target.children()[0]);
        
        if (_eventTarget.classList.contains("icon-arrow_up")) {
        	_eventTarget.classList.remove("icon-arrow_up")
        	_eventTarget.classList.add("icon-arrow_down")
            $(_eventTarget.parentNode.nextElementSibling).fadeOut("fast")
        } 
        else if (_eventTarget.classList.contains("icon-arrow_down")) {
	    	_eventTarget.classList.remove("icon-arrow_down")
	    	_eventTarget.classList.add("icon-arrow_up")
	        $(_eventTarget.parentNode.nextElementSibling).fadeIn("fast")
	        
	        // 이미지 펼칠 때 talk-content 창 넘어가는 경우 스크롤 내리기
	        let containerBottom = $('#talk-content').offset()["top"] + $('#talk-content').height();
	        let expectedImgBottom = target.offset()["top"] + target.height() + target.next().height();
	        if (containerBottom < expectedImgBottom) {
	        	document.getElementById('talk-content').scrollTop += expectedImgBottom-containerBottom+50;
	        }            
        }
    }
    
    function _openContextMenu(event, fileId) {
        event.stopPropagation();
        
        let renderData = [
            {
                "text": "TeeDrive에 저장",
                "onclick": function () {
                	talkServer2
                    .validFileCheck(fileId)
                    .then(function(res) {
                        if (res.data.dto.resultMsg === "Success") {
                        	SEND_TO_TDRIVE({
                        	    fileId   : fileId,
                        	    fileName : res.data.dto.storageFileInfoList[0].file_name,
                        	    fileExt  : res.data.dto.storageFileInfoList[0].file_extension,
                			})
                        } else {
                        	TeeToast.open({text: "손상되었거나 삭제된 파일입니다."});
                        }
                    })
                    .catch(function(err) {
                    	TeeToast.open({text: "손상되었거나 삭제된 파일입니다."});
                    });
                }
            },
            {
//                "text": (talkUtil.isMobile()) ? "내 기기에 저장" : "내 로컬에 다운로드",
            	"text": "내 PC에 저장",
                "onclick": function () {
                	talkUtil.downloadValidFile(fileId);
                }
            },
        ];
//        let margin = 45;
        let marginLeft = $('#talk').offset().left;
        let marginTop = $('#talk').offset().top;
                
        let extra = -45;
        if (talkData.isSimple) {
        	extra = -68; // 끝이 안 짤리도록
        }
        let left = (event.clientX - marginLeft+extra);
        let top = (event.clientY - marginTop+15);
        
        let _maxLeft = $('#talk').width() - $('.context-menu').width();
        let _minLeft = 0;
        
        if (left > _maxLeft) left = _maxLeft - 15;
         else if(left < _minLeft) left = _minLeft + 15;

        talkContextMenu.render(renderData,
            "#talk",
            { "top": top + "px", "left":  left +"px"}
        );
    }
    
    function convertURLTagToHtml(inputText, noHtml) {
        // // http 가 없는 값을 단일 link 로 변경한다.
        // let onlyLinkRegExp = new RegExp("<(?!@)(?!http)+(.*?)>", "g");
        // inputText = inputText.replace(onlyLinkRegExp, "<a href='http://$1' target='_blank'>$1</a>");

        // // http, https 가 있는 부분 정리
        // let includeHttpLinkRegExp = new RegExp("<(https?.*?)>", "g");
        // inputText = inputText.replace(includeHttpLinkRegExp, "<a href='$1' target='_blank'>$1</a>");
        // return inputText;

        let arr = inputText.match(/<(.*?)>/g);
        if (!arr) return inputText;

        arr = arr.filter(function(elem) {
            if (elem.indexOf('"data-type":"url"') !== -1) return elem;
        });

        arr.forEach(function(elem) {
            // ex. <{"data-type":"url","data-url":"naver.com"}>
            // <,> 떼기
            let _element = elem.replace(/[\<\>]/g, "");
            let _obj = null;
            try {
                _obj = JSON.parse(_element);
            } catch (e) {
                inputText = inputText.replace(elem, "");
                return;
            }
            let _url = _obj["data-url"];
            if (_obj["data-url"].indexOf("http://") === -1 && _obj["data-url"].indexOf("https://") === -1) {
                _url = "http://" + _obj["data-url"];
            }
            let element = $("<a/>")
                .attr({ href: _url, target: "_blank" })
                .text(_obj["data-url"])[0].outerHTML;
            if (noHtml) {
                element = "[Link: " + _obj["data-url"] + "]";
            }
            inputText = inputText.replace(elem, element);
            //inputText = inputText.replace(/(\s)*(<a)+(.+)(<\/a>)+/g,"&nbsp$2$3$4");
            //0409 //chk
            inputText = inputText.replace(/(\s!\n)*(<a)+(.+)(<\/a>)+/g,"&nbsp$2$3$4");
            inputText = inputText.replace(/(\n)+(<a)+(.+)(<\/a>)+/g,"$1$2$3$4");
        });

        return inputText;
    }

    function convertMailTagToHtml(inputText, noHtml) {
        let arr = inputText.match(/<(.*?)>/g);
        if (!arr) return inputText;

        arr = arr.filter(function(elem) {
            if (elem.indexOf('"data-type":"mail"') !== -1) return elem;
        });

        arr.forEach(function(elem) {
            // ex. <{"data-type":"url","data-url":"naver.com"}>
            // <,> 떼기
            let _element = elem.replace(/[\<\>]/g, "");
            let _obj = null;
            try {
                _obj = JSON.parse(_element);
            } catch (e) {
                inputText = inputText.replace(elem, "");
                return;
            }
            let _url = _obj["data-url"];
            if (_obj["data-url"].indexOf("http://") === -1 && _obj["data-url"].indexOf("https://") === -1) {
                _url = "http://" + _obj["data-url"];
            }
            // _url : mailto:gildong@tmax.tmail.com 의 형태
            let element = $("<a/>")
                .attr({ href: _url.slice(7), target: "_blank" })
                .text(_obj["data-url"].slice(7))[0].outerHTML;

            if (noHtml) {
                element = "[Link: " + _obj["data-url"] + "]";
            }
            inputText = inputText.replace(elem, element);
        });

        return inputText;
    }

    function convertHtmlMentionToTag(inputText) {
        // let mentionRegExp = new RegExp("<span [a-z0-9\"= ]*?class=\"mention\"[a-z0-9\"= ]*user-id=\"(.*?)\"[a-z0-9\"= ]*?>(@.*?)</span>", "g");
        // inputText = inputText.replace(mentionRegExp, "<@$1>");
        // return inputText;

        let arr = inputText.match(/<span(.*?)<\/span>/g);
        if (!arr) return inputText;

        arr = arr.filter(function(elem) {
            if (elem.indexOf('data-type="mention"') !== -1) return elem;
        });

        arr.forEach(function(elem) {
            let _elem = $.parseHTML(elem)[0];

            let obj = JSON.stringify({
                "data-type": _elem.getAttribute("data-type"),
                "data-user-id": _elem.getAttribute("data-user-id"),
                "data-user-name": _elem.getAttribute("data-user-name")
            });

            inputText = inputText.replace(elem, "<" + obj + ">");
        });
        return inputText;
    }

    function convertMentionTagToHtml(inputText, noHtml) {
        let arr = inputText.match(/<(.*?)>/g);
        if (!arr) return inputText;

        arr = arr.filter(function(elem) {
            if (elem.indexOf('"data-type":"mention"') !== -1) return elem;
        });

        arr.forEach(function(elem) {
            let _elemenet = elem.replace(/[\<\>]/g, "");
            let _obj = JSON.parse(_elemenet);

            let element = $("<span/>")
                .addClass("mention")
                .attr({ onclick: "talk.talk__setProfilePopup('" + _obj["data-user-id"] + "', true )" })
                .text("@" + _obj["data-user-name"])[0].outerHTML;
            //            let element = $("<span/>").addClass("mention").attr({ "onclick": "talkProfilePopup.render(\'" + _obj["data-user-id"] + "\')" }).text("@" + _obj["data-user-name"])[0].outerHTML;
            if (noHtml) {
                element = "@" + _obj["data-user-name"];
            }
            inputText = inputText.replace(elem, element);
        });

        return inputText;
    }

    return {
        unfoldImage: function(e) {
            return _unfoldImage(e);
        },
        htmlMsgToServerMsg: function(htmlContents) {
            return htmlMsgToServerMsg(htmlContents);
        },
        serverMsgToHtmlMsg: function( rawContents) {
            return serverMsgToHtmlMsg( rawContents);
        },
        enableDebug: function() {
            debugMode = true;
        },
        temporaryRender: function(htmlContents) {
            let container = $(".message-container:last");
            // 없으면 container 만들기

            let renderOption = {
                id: "m-" + "1312412",
                properties: {
                    "msg-state": "process",
                    "msg-body": htmlContents,
                    "msg-owner": "my",
                    "msg-sender": _myInfo.userId,
                    "msg-time": new Date().getTime(),
                    "msg-unix-time": new Date().getTime(),
                    "unread-count": "0",
                    "is-like-activate": "0",
                    "like-count": "0"
                }
            };

            renderOption.properties["msg-type"] = "simple";

            let msgView = Top.Widget.create("message-layout", renderOption);
            container.append(msgView.template);

            let chatLayout = $("div#chatContentLayout");
            chatLayout.eq(0).scrollTop(chatLayout[0].scrollHeight);
        },
        removeHtmlTagForNotification: function(inputText) {
            let outputMessage = serverMsgToHtmlMsg(null, inputText, true);

            // remove All HtmlTag
            outputMessage = outputMessage.replace(/(<([^>]+)>)/gi, "");

            outputMessage = outputMessage.replace(/&nbsp;/gi, " ");
            return outputMessage;
        },
        convertFileTagToHTML: function(isPrepend, inputText, noHtml) {
        	return convertFileTagToHTML(isPrepend, inputText, noHtml)
        },
        convertEmoticonTagToHtml : function(inputText, noHtml) {
        	return _convertEmoticonTagToHtml(inputText, noHtml)
        },
        convertHtmlUrlToTag : function(inputHtml) {
        	return convertHtmlUrlToTag(inputHtml);
        },
        convertHtmlMailToTag : function(inputHtml) {
        	return convertHtmlMailToTag(inputHtml);
        },
        convertMailTagToHtml: function(inputText, noHtml) {
        	return convertMailTagToHtml(inputText, noHtml);
        },
        storageMetaFormatToAttachmentFormat : function (storageFormatMeta){
        	return storageMetaFormatToAttachmentFormat(storageFormatMeta);
        },
        openContextMenu: function (event, fileId) {
            _openContextMenu(event, fileId)
        },
        checkMessageContainUserMention: checkMessageContainUserMention
    };
})();

function moveScroll(tag, isPrepend) {
    if (isPrepend !== null && !isPrepend) {
        let content = $("#talk-content");
        let scrollTop = content.scrollTop();
        scrollTop += $(tag).height() + 16;
        content.scrollTop(scrollTop);
    }
}

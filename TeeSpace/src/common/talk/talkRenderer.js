const talkRenderer = (function() {
	//////

    let _renderUnreadCount = function(unreadInfoList){
        let listLength = unreadInfoList.length;
        let i, j;
        let msgContainer = document.querySelector('div.content__complete-message-container');
        let containerChildren = msgContainer.childNodes;
      let containerLength = containerChildren.length;
      let msgInfoList = talkData.messageList; 
        let curMsgId, domMsgId;
        let unreadCountMap = {};
        for(i=0; i<listLength; i++){
        	if((unreadInfoList[i]['MSG_ID']!=undefined) && (unreadInfoList[i]['UNREAD_USER_COUNT']!=undefined)){
        		unreadCountMap[unreadInfoList[i]['MSG_ID']] = unreadInfoList[i]['UNREAD_USER_COUNT'];
        	}
        }
        for(i=0,j=0;i<containerLength;i++,j++){
         domMsgId = containerChildren[i].getAttribute('data-message-id');
         if((unreadCountMap[domMsgId] !==null) && (unreadCountMap[domMsgId] !==undefined) && (unreadCountMap[domMsgId]>=0)){
        	const targetList=containerChildren[i].getElementsByClassName('message__unread-count');
        	if( targetList.length )        		
        		targetList[0].textContent = (unreadCountMap[domMsgId] <=0 ? '' : unreadCountMap[domMsgId])
         }                                                                                             
      }                                                
    }
	//스페이스에서 대화내용 찾기
	
	
	//TODO:버튼을 눌렀을 때 이전의 정보가 남아있지 않도록
	//init함수를 만들어야 한다. 나중에 언젠가 
	//searchButton은 항상 visible하게 기획변경되었다. 
	//혹시 모르니 우선 searchbuttonisvisible = true항상 넣어주고
	//좀 굳어지면 로직에서 빼야겠다. 
	function stateOfSearchSection(SearchButtonisvisible, SearchContainerisvisible){
		const curSearchButton = document.querySelector("#talk__Content-search-button");
		const curSearchContainer = document.querySelector("#talk__Search-total-wrapper");
		if(SearchButtonisvisible=== true && SearchContainerisvisible === false){
			
			//검색버튼이 아닌 검색창을 띄울 때에는 검색창 관련 정보를 모두 초기화하고 띄워야 한다. 
			_hideArrowButtons();
			_hidecurIdxOfMsg();
			document.getElementById('talk__Content-search-input').value='';
			talkData.searchedMsgList = null;
			talkData.IdxOfsearchedMsg =0;
			talkData.curSearchKeyword='';
		
			curSearchButton.style["display"]="flex";
			curSearchContainer.style["display"]="none";
			}
		else if(SearchButtonisvisible=== true && SearchContainerisvisible === true){
			curSearchButton.style["display"]="flex";
			curSearchContainer.style["display"]="flex";
			
			const searchInput = curSearchContainer.querySelector("#talk__Content-search-input");
			if(searchInput){
				searchInput.focus();
			}
		}
	}
	
	
    function renderContent() {
        // TODO : timeline 필요없는거 확실하면 삭제
        //		 <div id='talk-timeline-container'>
        //	         <div id='talk-timeline'></div>
        //	         <div id='talk-timeline-empty'></div>
        //	     </div>
    	
    	// search input 창 추가한 곳
        const template = `
		                <div id="talk__Search-total-wrapper" style="display:none;">	
							<div id = "talk__Content-search-category">
								<div id = "talk__Search-category-name">전체</div>
								<div id = "talk__Search-category-selector" class="icon-arrow_down "></div> 
							</div>
						<div id="talk__Content-search-container-wrapper">
		               		<div id="talk__Content-search-container">
		               		<span id="talk__Content-small-search-icon" class="icon-work_search"></span>
		               		<input type="text" id="talk__Content-search-input" placeholder="대화 내용 검색"></input>
		               		
		               		<div id="talk__Search-MsgCounter-wrapper">
		               			<div id="talk__Search-curIdxOfMsg" style="display:none;"></div>
		               			<div id="talk__Search-numOfTotals" style="display:none;"></div>
		               		</div>
		               		<img id="talk_Content-search-arrowUp" src="./res/talk/arrow_up_line.svg" style="display:none;">
		               		<img id="talk_Content-search-arrowDown" src="./res/talk/arrow_down_line.svg" style="display:none;">
		               		
		               		
		               		<span id="talk__Search-close" class="icon-work_cancel"></span>
		               		</div>
		               	</div></div>
		               	
		               	
		                    <div class="content__complete-message-container"></div>
		                    <div class="content__temp-message-container"></div>
		                    <span id="goto-last-button" class="icon-arrow_down talk__hidden"></span>
		                </div>`;
        
        
        
        let content = document.querySelector("#talk-content");
        content.innerHTML = template;
        /*<span id="talk__Content-search-button" class="icon-work_search"></span>*/
/*		//미니채팅일 때 검색창은 기존 기획대로 임시로 ...

		const curURL = window.location.href;
        //미니채팅방이라는 소리
        if ( curURL.indexOf('talk?mini-talk=true') != -1) {
        	//미니 채팅방일때 처리
			let miniSearchButton = document.createElement('span');
			miniSearchButton.setAttribute('class','icon-work_search');
			miniSearchButton.setAttribute('id','talk__Content-search-button');
			let msgContainer = document.querySelector('#talk-content .content__complete-message-container');
            document.querySelector('#talk-content').insertBefore(miniSearchButton, msgContainer);
        }*/
		
		//document.createElement를 이용하여
        //search창 카테고리 팝업창을 그려볼것입니다. 
        //바로 이렇게요~
        content = document.getElementById("talk-content");
        
        const categoryContainer = document.createElement('div');
        categoryContainer.setAttribute("id","talk__Search-categoryList-container");
        categoryContainer.style["display"]="none";
        content.appendChild(categoryContainer);
           
        const categoryList = document.createElement('ul');
        categoryList.setAttribute("id","talk__Search-category-list");
        
        categoryContainer.appendChild(categoryList);
        //TODO: for문으로 바꿀 수 있지 않을까?
        const categoryItemAll = document.createElement('li');
        categoryItemAll.setAttribute("id","talk__Search-category-itemAll");
        categoryItemAll.innerText="전체";
        
        const categoryItemFile = document.createElement('li');
        categoryItemFile.setAttribute("id","talk__Search-category-itemFile");
        categoryItemFile.innerText="파일";

        const categoryItemLink = document.createElement('li');
        categoryItemLink.setAttribute("id","talk__Search-category-itemLink");
        categoryItemLink.innerText="링크";
        
        categoryList.appendChild(categoryItemAll);
        categoryList.appendChild(categoryItemFile);
        categoryList.appendChild(categoryItemLink);
        
        //TODO: 다른 곳을 클릭해도 categorylist를 끄자
        //기획 확인후에,,
        /*setTimeout(()=> {
        	$(document).one("click", {content:content}, function(event) {
        		_hideCategoryList();
        	})
        }, 10)*/
        
        //TODO: 클래스이름으로 QUERYSELECTOR해서 가져오면 더 간단하게 되지 않을까?
        //LI 하나마다 이벤트 먹임 ㅈㅈ
        categoryItemAll.addEventListener('click',()=>{
        	//categoryItemAll안에는 "전체" 라는 글자가 있다.
        	const chosenItemName = categoryItemAll.innerText;
        	const curCategoryName = document.querySelector('#talk__Search-category-name');
        	
        	//현재 카테고리 이름을 클릭한 아이템의 이름으로 바꿔준다. 
        	curCategoryName.innerText = chosenItemName;
        	talkData.curCategoryName =  chosenItemName;
        	
        	//검색창 초기화 과정
        	talkData.searchedMsgList=null;
        	talkData.IdxOfsearchedMsg= 0;
        	_hideArrowButtons();
        	_hidecurIdxOfMsg();
        	document.querySelector("#talk__Content-search-input").value ="";
        	talkData.curSearchKeyword='';
        	
        	//필요한 토글하는 과정
        	//카테고리 리스트를 클릭했으면 리스트를 안보이게 하고 
        	_toggleCategoryList();
        	//위화살표를 아래화살표 아이콘으로 바꾸고
        	_toggleCategorySelectorIcon();
        	//배경화면도 바꿔준다.
        	_toggleCategoryBackground();
        	
        	//현재 가지고 있는 메시지 중에서 
        	//필터링꺼서 보여주기
        	talkSearch.msgFilteredAll();
            
        })
        function hasScroll() {
			const container = $("#talk-content");
			if(container&&container[0])
			{
				return container && container[0].scrollHeight > container[0].clientHeight;
			}
			else
			{
				return null;
			}
		}
		categoryItemFile.addEventListener('click',async()=>{
        	//categoryItemFile안에는 "파일" 라는 글자가 있다.
        	const chosenItemName = categoryItemFile.innerText;
        	const curCategoryName = document.querySelector('#talk__Search-category-name');
        	
        	//현재 카테고리 이름을 클릭한 아이템의 이름으로 바꿔준다. 
        	curCategoryName.innerText = chosenItemName;
        	talkData.curCategoryName = chosenItemName;
        	
        	
        	//검색창 초기화 과정
            talkData.searchedMsgList=null;
        	talkData.IdxOfsearchedMsg= 0;
        	_hideArrowButtons();
        	_hidecurIdxOfMsg();
        	document.querySelector("#talk__Content-search-input").value ="";
        	talkData.curSearchKeyword='';
        	
        	
        	_toggleCategoryList();
        	_toggleCategorySelectorIcon();
        	_toggleCategoryBackground();
        	
        	//현재 가지고 있는 메시지 중에서 
        	//file로 필터링해서 보여주기
			talkSearch.msgFilteredFile();
			
			while(!hasScroll()){
				if (talkData.messageList.length) firstMessageId = talkData.messageList[0]["MSG_ID"];
				const res = await talkServer2.ttalk_getMessageList(firstMessageId,20);
				talkContent.addMessages(res.data.dto["MSG_LIST"], true);
				talkContent.updateUnreadCount();
			}
        })
		
			categoryItemLink.addEventListener('click', async ()=>{
				//categoryItemLink안에는 "링크" 라는 글자가 있다.
				const chosenItemName = categoryItemLink.innerText;
				const curCategoryName = document.querySelector('#talk__Search-category-name');
			
				//현재 카테고리 이름을 클릭한 아이템의 이름으로 바꿔준다. 
				curCategoryName.innerText = chosenItemName;
				talkData.curCategoryName =  chosenItemName;
				
				_toggleCategoryList();
				_toggleCategorySelectorIcon();
				_toggleCategoryBackground();
				
				//검색창 초기화 과정
				talkData.searchedMsgList=null;
				talkData.IdxOfsearchedMsg= 0;
				document.querySelector("#talk__Content-search-input").value ="";
				_hideArrowButtons();
				_hidecurIdxOfMsg();
				talkData.curSearchKeyword='';
				
				//현재 가지고 있는 메시지 중에서 
				//link로 필터링해서 보여주기
				talkSearch.msgFilteredLink();
	
				while(!hasScroll()){
					if (talkData.messageList.length) firstMessageId = talkData.messageList[0]["MSG_ID"];
					const res = await talkServer2.ttalk_getMessageList(firstMessageId,20);
					talkContent.addMessages(res.data.dto["MSG_LIST"], true);
					talkContent.updateUnreadCount();
				}
				
			})
	
		 ////////////////////////버튼에 이벤트 핸들러 다는 곳////////////////////////
       //document.querySelector('#talk__Search-category-name').innerText="전체";
       const searchInput = document.querySelector('#talk__Content-search-input');
	   searchInput.addEventListener('keydown', talkSearch.keydownEvent);
       
       //위 화살표 눌렀을 때 해당하는 이전 메시지 찾아가는 핸들러 달기
       const arrowUpButton = document.querySelector("#talk_Content-search-arrowUp");
       arrowUpButton.addEventListener('click',()=>{
       	//arrowup버튼 클릭시 할 함수
    	 //시나리오상 talkData.searchedMsgList 서버에서 긁어온 값을 가지고 있다. 
		let IdxOfNxtUpMsg = talkData.IdxOfSearchedMsg+1; 
		if(parseInt(IdxOfNxtUpMsg) >= parseInt(talkData.searchedMsgList.length)){
			   TeeToast.open({text:"가장 마지막 단어입니다"});
			   event.stopPropagation();
		}
       	else talkSearch.searchKeywords(IdxOfNxtUpMsg);
       })
       
       const arrowDownButton = document.querySelector("#talk_Content-search-arrowDown");
       arrowDownButton.addEventListener('click',()=>{
       	//arrowdown버튼 클릭시 할 함수
    	//시나리오상 talkData.searchedMsgList 서버에서 긁어온 값을 가지고 있다. 
		//    talkData.IdxOfSearchedMsg = talkData.IdxOfSearchedMsg - 1 ;
		let IdxOfNxtDownMsg = talkData.IdxOfSearchedMsg-1; 
		   if(parseInt(IdxOfNxtDownMsg) <0){
			   TeeToast.open({text:"가장 최신 단어입니다"});
			   event.stopPropagation();
		   }
		   else talkSearch.searchKeywords(IdxOfNxtDownMsg);
	   })
	   
       
       //검색버튼 누르고 난 후에 생기는 검색버튼에 핸들러 달기 
       const smallSearchButton = document.querySelector('#talk__Content-small-search-icon');
       smallSearchButton.addEventListener('click', () =>{
       	//TODO: 대화내용 검색하는 함수
       	//버튼이 눌렸을 당시에 SearchInput창에 입력된 단어를 키워드로 하려고 한다. 
			const keyword = document.getElementById('talk__Content-search-input').value;
			event.stopPropagation();
			//검색버튼으로 누르면 처음부터 다시 검색하게 하려고 한다. 
			//그래서 talkData의 search관련된 정보들을 초기화한다. 
			//TODO: 나중에 따로 init 함수로 만들어야겠다. 
			
			
			//하이라이팅을 검색할 때마다 없애주는 과정
			//지금 검색된 하이라이팅된 메시지를 초기화하는 과정
			//TODO: 따로 함수 파야함.
			if(talkData.searchedMsgList!=null ){
				let curMsg = talkData.searchedMsgList[talkData.IdxOfSearchedMsg];
				let msgId =curMsg["MSG_ID"];
				//현재 메시지가 파일이라면
				let content = document.querySelector("[data-message-id='" + msgId + "'] " +
				".file-message_title");
				if(content !=null){
					let fileTitle = content.innerText;
                	let fileTitleArray = fileTitle.split('\n').join('').split('.');
                	let fileTitleArrayLength = fileTitleArray.length;
                	let fileExtension = "." + fileTitleArray[fileTitleArrayLength-1];
                	fileTitleArray.splice(fileTitleArrayLength-1,1);
                	let FileName=fileTitleArray.join('');
                
                	var span=content.getElementsByTagName('span'); // get the span
                	let spanCount = span.length;
                	var pa=span[0].parentNode;
                	while(span[0] != undefined) pa.removeChild(span[0]);
                	let spanFileName = document.createElement('span');
                	spanFileName.setAttribute('class','file-message__file-name talk-tooltip');
                	spanFileName.innerText=FileName;
                	content.appendChild(spanFileName);
                	let spanFileExt = document.createElement('span');
                	spanFileExt.setAttribute('class','file-message__file-extension');
                	spanFileExt.innerText=fileExtension; 
					content.appendChild(spanFileExt);
				}

				content = content = document.querySelector("[data-message-id='" + msgId + "'] " +
				".message__content-body__content");
				//파일이 아닌 다른 url, 메시지라면
				if(content !=null){
					let contentChild = content.childNodes;
					let originString=new Array();
					let idx;
					for(idx=0;idx<contentChild.length;idx++){
						if(contentChild[idx]["nodeName"] != 'A' && contentChild[idx]["nodeName"] != 'DIV') continue;
						originString[idx] = contentChild[idx].innerText;
						var span=contentChild[idx].getElementsByTagName('span'); // get the span
						let spanCount = span.length;
						while(span[0] != undefined)   contentChild[idx].removeChild(span[0]);
						contentChild[idx].innerText = originString[idx];
					}
				}
			}
			
			talkData.searchedMsgList=null;
			talkData.IdxOfSearchedMsg=0;
			talkData.curSearchKeyword=keyword;
			//keywords를 넘겨주면 찾는다. content부분에서 수행하므로 talkContent.js에 로직을 넣는다.
			talkSearch.searchKeywords(0);
       })
       
       
       //search button 핸들러 달기
       const searchButton = document.querySelector('#talk__Content-search-button');
      searchButton.addEventListener('click', () => {
      	const SearchButtonShow = true;
      	const SearchContainerShow = true;
      	stateOfSearchSection(SearchButtonShow,SearchContainerShow);
      })
       
       const closeButton = document.querySelector('#talk__Search-close');
       closeButton.addEventListener('click', () => {
       	const SearchButtonShow = true;
       	const SearchContainerHide = false;
       	_hideCategoryList();
       	stateOfSearchSection(SearchButtonShow,SearchContainerHide);
       })
       
    //    const categorySelector = document.querySelector('#talk__Search-category-selector');
    //    categorySelector.addEventListener('click',()=>{
    // 	   //TODO: 여러개 토글하는 함수가 있는 데 이것을 하나로 합칠까 말까 고민중 
    // 	   //난중에,,
    // 	   _toggleCategoryList();
    // 	   _toggleCategorySelectorIcon();
    // 	   _toggleCategoryBackground();
	//    })  
	   //selector 화살표 버튼 뿐만 아니라 카테고리 영역 전체를 눌렀을 때 리스트를 펼치도록
	   const categorySelector = document.querySelector('#talk__Content-search-category');
       categorySelector.addEventListener('click',()=>{
       	   _toggleCategoryList();
    	   _toggleCategorySelectorIcon();
    	   _toggleCategoryBackground();
       })
    }

    //////////////////////////////////////////////////////////////////////////////////////////////
    //대화창 내에서 검색한 메시지 리스트가 있을 때 네비게이션을 위한 위화살표 버튼과 아래화살표 버튼을 그려야 한다.
    //그리는 내용이므로 talkRenderer에 선언하였고
    //이것이 그 과정 ㅋ
    function _showArrowButtons(){
    	document.querySelector("#talk_Content-search-arrowUp").style["display"] = "";
    	document.querySelector("#talk_Content-search-arrowDown").style["display"] = "";
    }
    
    //검색한 메시지 리스트가 없고, 현재 이전 검색한 메시지 리스트는 있어서 화살표 버튼이 있으면
    //그 버튼들을 지워줘야 할 때 처리하는 함수
    //이것이 그 과정 ㅋ
    function _hideArrowButtons(){
    	document.querySelector("#talk_Content-search-arrowUp").style["display"] = "none";
    	document.querySelector("#talk_Content-search-arrowDown").style["display"] = "none"; 
    }
    
    //카테고리 리스트를 토글하는 과정
    //현재 있으면 없애주고 현재 없으면 생기게 해주고
    function _toggleCategoryList(){
    	const isVisibleCategoryContainer= 
 		   document.querySelector('#talk__Search-categoryList-container');
 	   if(isVisibleCategoryContainer.style["display"] == "none")
 		   isVisibleCategoryContainer.style["display"]="flex";
 	   else
 		   isVisibleCategoryContainer.style["display"]="none";
    }
    
    //카테고리 셀렉하는 섹션의 위화살표 아래화살표를 토글하는 과정
    function _toggleCategorySelectorIcon(){
    	const curSelectorClasses = document.querySelector('#talk__Search-category-selector').classList;
    	const curSelectorIcon = curSelectorClasses[1];
    	if(curSelectorIcon == 'icon-arrow_down'){
    		curSelectorClasses.remove('icon-arrow_down');
    		curSelectorClasses.add('icon-arrow_up');
    	} else{
    		curSelectorClasses.add('icon-arrow_down');
    		curSelectorClasses.remove('icon-arrow_up');
    	}
    }
    
    //카테고리 셀렉하는 섹션의 색깔을 토글하는 과정
    function _toggleCategoryBackground(){
    	const searchCategory = document.querySelector("#talk__Content-search-category");
    	const searchContainer = document.querySelector("#talk__Content-search-container");
    	if(searchCategory.style["background"] != "rgb(220, 221, 255)") {
    		searchCategory.style["background"] = "rgb(220, 221, 255)";
    		searchCategory.style["border-right"] = "0.0625rem solid #6C56E5";
    		searchContainer.style["border-left"] =  "none";
    	}else {
    		searchCategory.style["background"] = "white";
    		searchCategory.style["border-right"] = "none";
    		searchContainer.style["border-left"] =  "0.0625rem solid #C6CED6"
    	}
    }
    
    //현재 찾은 메시지가 몇번째 메시지인지 나타내는 과정
    function _showcurIdxOfMsg(){
    	document.querySelector("#talk__Search-numOfTotals").style["display"] ="inline-block";
    	document.querySelector("#talk__Search-curIdxOfMsg").style["display"] ="inline-block";
    }
    //현재 찾은 메시지가 몇번째 메시지인지를 지우는 과정
    function _hidecurIdxOfMsg(){
    	document.querySelector("#talk__Search-numOfTotals").style["display"] ="none";
    	document.querySelector("#talk__Search-curIdxOfMsg").style["display"] ="none";
    }
    
    function _hideCategoryList(){
    	const isVisibleCategoryContainer= 
  		   document.querySelector('#talk__Search-categoryList-container');
    	isVisibleCategoryContainer.style["display"]="none";
    	
    	const curSelectorClasses = document.querySelector('#talk__Search-category-selector').classList;
    	const curSelectorIcon = curSelectorClasses[1];
    	
    	if(curSelectorIcon === 'icon-arrow_up') curSelectorClasses.remove('icon-arrow_up');
    	curSelectorClasses.add('icon-arrow_down');
    	
    	const searchCategory = document.querySelector("#talk__Content-search-category");
    	const searchContainer = document.querySelector("#talk__Content-search-container");
    	searchCategory.style["background"] = "white";
		searchCategory.style["border-right"] = "none";
		searchContainer.style["border-left"] =  "0.0625rem solid #C6CED6";
    }
    
    function _showIdxOfMsg(IdxOfSearchedMsg,totalKeywords){
    	document.querySelector("#talk__Search-curIdxOfMsg").innerText=(IdxOfSearchedMsg+1);
    	   document.querySelector("#talk__Search-numOfTotals").innerText="/"+(totalKeywords);
	}
	
//////////////////////////////여기까지 서치 부분///////////////////////////////////////////////////////
    ///////////////TODO: 나중에 RENDER CONTENT에서도  서치부분 떼어낼 것이다/////////////////////////////////
    function renderFooter() {
    	// TODO : test용, 나중에 필요한 곳으로 옮기면 삭제, <div id='talk-footer__test-button' class="icon-work_send" style="cursor:pointer"></div>
        const template = `
        		<div id='talk-footer__buttons'>
		            <div id='talk-footer__file-button'>
		            	<img src="./res/talk/attach1.svg" />
		            </div>
		            <div id='talk-footer__emoticon-button'>
		            	<img src="./res/talk/emoticon.svg" />
		            </div>
		            <div id='talk-footer__mention-button'>
		            	<img src="./res/talk/mention.svg" />
		            </div>
		            <div id='talk-footer__schedule-button' style="display:none">
		            	<img src="./res/talk/schedule.svg" />
		            </div>
		            <div id='talk-footer__message-counter'><span id="talk-footer__message-counting">0</span><span id="talk-footer__message-limit">&nbsp;/ 5000자</span></div>
		        </div>
		        <div id='talk-footer__input-wrapper'>
		            <div id='talk-footer__input' contenteditable='true' data-placeholder='메시지를 입력하세요. (Ctrl + Enter로 줄 바꿈이 가능합니다.)'></div>
		            <button id='talk-footer__send-button' disabled='disabled'><img class='talk-footer__send-image talk-footer__btn-default-color' src="./res/talk/rocket.png" /><span>보내기</span></button>
		        </div>`;
        
		
				
        let wrapper = document.querySelector("#talk-footer");
		wrapper.insertAdjacentHTML("beforeend", template);

        // TODO : 나중에 필요한 곳으로 옮기기
//        $("#talk-footer__test-button").on("click", () => {
//            let newTalkWindow = window.open(
//                window.location.href + "?new-window=true",
//                "_blank",
//                "width=400, height=662"
//            );
//            newTalkWindow.document.body.innerHTML = "HTML";
//            newTalkWindow.document.createAttribute("isDiffTalk");
//            //            newTalkWindow.$("#talk-footer__test-button").css("display","none");
//        });
    }
    
    // talkData.messageList 혹은 tempMessageList에 변화 발생시 mobx타고 온다
    function renderMessage(messageList, isPrepend, isCompleteContainer) {
        let msgContainer;
        
        if (isCompleteContainer) {
            msgContainer = document.querySelector(".content__complete-message-container");
        } else {
            msgContainer = document.querySelector(".content__temp-message-container");
        }
        
		const documentFragment = document.createDocumentFragment();
		messageList.forEach( ( message ) => {
			// create Element안에서 autorun을 넣어야 한다.
			const messageLayout = createMessageLayout( message );
			if ( messageLayout ) {
				documentFragment.appendChild( messageLayout );
				if ( talkContent.isChatMessage( message.MSG_TYPE ) ) {
					mobx.autorun( function ( aa ) {
						updateMessageElement( message, messageLayout );
					} );
				}
			}
		} );
		
		if ( isPrepend ) {
			msgContainer.insertBefore( documentFragment, msgContainer.children[0] );
		} else {
			let myMessage = false;
			let isScrollBottom = checkScrollBottom();
			if ( messageList.length === 1 ) {
				myMessage = talk.isMyMessage( messageList[0] );
			}
			
			msgContainer.appendChild( documentFragment );
			setTimeout( function () {
				if ( isScrollBottom || myMessage ) {
					talkContent.gotoLastMessage();
				}
			}, 1 )
		}
    }
    
    function checkScrollBottom(){
    	// 소수점 확인을 위해 비교를 1단위로 넣었다.
    	const content = document.getElementById('talk-content');
    	return content.scrollHeight - (content.scrollTop + content.clientHeight)  < 1;
    }
    
    function createMessageLayout(targetMessage) {
        let layout = null;

        switch (targetMessage["MSG_TYPE"]) {
            case "newMessage":
                layout = createNewMsgSeparator();
                break;

            case "userEnter":
            	layout = createUserInOutMessage(targetMessage, targetMessage["MSG_TYPE"]);
            	break;
            case "userLeave":
            	layout = createUserInOutMessage(targetMessage, targetMessage["MSG_TYPE"]);
            	break;
            case "roomCreate":
                layout = createSystemElement(targetMessage, false);
                break;

            case "create":
            case "file":
                layout = createMessageElement(targetMessage, false);
                break;

            case "delete":
                layout = createMessageElement(targetMessage, true);
                break;

            case "timeLine":
                layout = createSystemElement(targetMessage, true);
                break;
        }

        return layout;
    }

    function createNewMsgSeparator() {
        layout = document.createElement("talk-system-message-layout");
        layout.classList.add("talk-new-message");
        layout.textContent = "여기까지 읽었습니다.";
        return layout;
    }

    function createSystemElement(targetMessage, isTimeLine) {
        let layout, _text;
        if (isTimeLine) {
            layout = document.createElement("talk-system-message-layout");
            layout.classList.add("talk-timeline");
            _date = document.createElement("div");
            _date.textContent = talkUtil.convertTimestampToCustumFormat(targetMessage["MSG_TIME"]);
            layout.appendChild(_date);
        } else {
            layout = document.createElement("talk-system-message-layout");
            layout.classList.add("talk-system-message__read-mark");
            layout.textContent = targetMessage["MSG_BODY"];
        }

        return layout;
    }

    function createUserInOutMessage(targetMessage, type) {
    	// 초대 : ~님이 ~님, ~님을 초대하였습니다.
    	// ~님이 나갔습니다.
    	let layout = document.createElement("talk-system-message-layout");
    	layout.classList.add("talk-system-message__userInOut");
    	
    	let _user = talkUtil.getUserNick(targetMessage["USER_ID"]);
    	
    	switch (type) {
    		case "userEnter":
				// user 초대 메시지 버그 대비 try, catch문 삽입
				try {
					let invitedUserList = targetMessage["MSG_BODY"];
					if (invitedUserList.includes(',')) {
						invitedUserList = invitedUserList.split(',');
					} else {
						invitedUserList = [invitedUserList];
					}

					let systemMessage = `${_user}님이 <font class="talk-message__font-bold">${talkUtil.getUserNick(invitedUserList[0])}</font>님`
    			
					if (invitedUserList.length>1) {
						for (let i=1;i<invitedUserList.length;i++) {
							let invitedUser = talkUtil.getUserNick(invitedUserList[i]);
							systemMessage += `, <font class="talk-message__font-bold">${invitedUser}</font>님`;
						};
					}
					
					systemMessage += `을 초대하였습니다.`;
					
					layout.insertAdjacentHTML('beforeend',`<div class="talk-system-message__inout-text">${systemMessage}</div>`);
					
				} catch(e) {
					return null;
				}
				break;
    			
    		case "userLeave":
    			layout.insertAdjacentHTML('beforeend',`<div class="talk-system-message__inout-text">${_user}님이 나갔습니다.</div>`);
    			break;
    	}
    	return layout;
    }
    
    function createMessageElement(targetMessage, isDeleted) {
        let isMyMessage = false;
        const userId = targetMessage["USER_ID"];
        const msgId = targetMessage["MSG_ID"];
        const tempId = targetMessage["TEMP_ID"];
		const hasText = talkContent.hasText(targetMessage["MSG_BODY"]);
		const onlyaEmoticon = talkContent.hasOnlyaEmoticon(targetMessage["MSG_BODY"]);
        let isTransparent = false;

        //if (!hasText) {
        // 하나의 이모티콘만 있는지 check
        
        if (onlyaEmoticon) {
            let elem = targetMessage["MSG_BODY"].replace(/[\<\>]/g, "");
            let obj = JSON.parse(elem);
            switch (String(obj["data-width"])) {
	            case "40": // px일 때, 작은 이모티콘 한 개
	            case "80": // px일 때, 큰 이모티콘
	            case "6": // rem일 때, 큰 이모티콘
	            case "2.5": // rem일 때, 작은 이모티콘 한 개
	            	isTransparent = true;
               }
        }
        //}

        if (userId === talkData.myInfo.userId) {
            isMyMessage = true;
        } else {
            _marginRight = "9.875rem";
        }

        let container = document.createElement("talk-message-layout");
        // attribute 설정을 한번에 하지 못하는 것 같다, one at a time!
        // success message에 temp_id 넣지 않기
        if (msgId) {
        	container.setAttribute("data-message-id", targetMessage["MSG_ID"]);
        } else if (tempId){
        	container.setAttribute("data-temp-id", targetMessage["TEMP_ID"]);
        }        
        container.setAttribute("data-user-id", targetMessage["USER_ID"]);

        let bodyClass = isMyMessage ? "my-message" : "other-message";
        if (isTransparent && (!targetMessage["ATTACHMENT_LIST"] || !targetMessage["ATTACHMENT_LIST"].length)) {
        	bodyClass = " illust-message"
        }
        const isDisplayed = isDeleted ? "none" : "flex";

        // photo필요한지
        let _photo = "";
        const userInfo = userManager.getUserInfo(targetMessage["USER_ID"]);
        
        //_photo = userInfo["PROFILE_PHOTO"] || userInfo["THUMB_PHOTO"] || userManager.getUserDefaultPhotoURL(userId);
        _photo = userManager.getUserPhoto(userInfo["USER_ID"],"talk",userInfo["THUMB_PHOTO"]);
        
        if (!isMyMessage) {
            // ver.1과 다르게 talk-message-layout에 누구 메시지인지 표시
            container.classList.add("othersMessage");
        } else {
            container.classList.add("myMessage");
        }

        // 일단 넣고 후에 display:none 시키기
        // ver.1과 다르게 photo container를 header 안에 넣게됨. 따라서 message__content-container 태그 삭제
        // <div class="message__content-container" style="width:${contentContainerWidth}"></div>        
        let _errorEvent = "talkRenderer.component.profileImgError(event)";
        
        const template = `
					<div class="message__content-header">
						<div class="message__photo-container"><img class="message__photo" src="${_photo}" onerror="${_errorEvent}" /></div>
						<div class="message__content-header__name talk-tooltip" data-user-id="${userId}"></div>				
						<div class="message__content-header__time">${talkUtil.convertTimestampToTimeStr(targetMessage["MSG_TIME"])}</div>
					</div>
					<div class="message__content-body-wrapper">
						<div class="message__content-body">
							<div class="message__content-body__items ${bodyClass}">
								<div class="message__reply-container"></div>
								<div class="message__content-body__content"></div>
								<div class="message__attachment-container"></div>
							</div>
							<div class="message__content-body__state" style="display:${isDisplayed};">
								<div class="message__content-body__state--success">
									<div class="message__unread-count"></div>
								</div>
								<div class="message__content-body__state--sending">
									<img src="./res/talk/Tee_loading.gif" style="width:1.25rem;height:1.25rem;"/>
								</div>
								<div class="message__content-body__state--fail">
									<div class='fail-message__cancel-button'><span class='icon-work_cancel'></span></div>										
									<div class='fail-message__retry-button'><span class='icon-refresh'></span></div>
								</div>
							</div>
						</div>
					</div>
					`;

        container.insertAdjacentHTML("afterbegin", template);

        // 삭제되지 않았다면 subMenu 부착해주기
        if (!isDeleted) {
            let messageMenu = document.createElement("div");
            messageMenu.classList.add("message__sub-menu-container");

            // 이모티콘, 사진 담는 메세지에 답장 가능
            let replyMenu = document.createElement("div");
            replyMenu.classList.add("message__sub-menu__item");
            replyMenu.setAttribute("data-menu", "reply");
            let _replyImg = document.createElement("img");
            _replyImg.setAttribute("src", "./res/talk/reply.svg");
            let _replyText = document.createElement("span");
            _replyText.textContent = "답장";
            replyMenu.appendChild(_replyImg);
            replyMenu.appendChild(_replyText);
            messageMenu.appendChild(replyMenu);
            replyMenu.addEventListener("click", event => {
            	event.preventDefault();
            	event.stopPropagation();
                let replyMsgData = talkReply.createReplyMsgData(event);
            });

            if (isMyMessage) {
                let deleteMenu = document.createElement("div");
                deleteMenu.classList.add("message__sub-menu__item");
                deleteMenu.setAttribute("data-menu", "delete");
                let _deleteImg = document.createElement("div");
                _deleteImg.classList.add("icon-delete");
                let _deleteText = document.createElement("span");
                _deleteText.textContent = "삭제";
                deleteMenu.appendChild(_deleteImg);
                deleteMenu.appendChild(_deleteText);
                messageMenu.appendChild(deleteMenu);
                deleteMenu.addEventListener("click", () => {
                    talkContent.deleteMessage(msgId);
                });
            }

            if (hasText || talkContent.isOnlyLink(targetMessage["MSG_BODY"])) {
                let copyMenu = document.createElement("div");
                copyMenu.classList.add("message__sub-menu__item");
                copyMenu.setAttribute("data-menu", "copy");
                let _copyImg = document.createElement("div");
                _copyImg.classList.add("icon-copy");
                let _copyText = document.createElement("span");
                _copyText.textContent = "복사";
                copyMenu.appendChild(_copyImg);
                copyMenu.appendChild(_copyText);
                messageMenu.appendChild(copyMenu);
                copyMenu.addEventListener("click", event => {
                    talkContent.onCopyClick(msgId);
                });
            }

            // TODO : T-Mail로 보내기
            //			 const mailMenu = document.createElement("div")
            //			 mailMenu.classList.add("message__sub-menu__item")
            //			 mailMenu.setAttribute("data-menu", "mail")
            //			 let _mailImg = document.createElement('div')
            //			 _mailImg.classList.add('icon-work_email')
            //			 let _mailText = document.createElement('span')
            //			 _mailText.textContent = "삭제"
            //			 mailMenu.appendChild(_mailImg)
            //			 mailMenu.appendChild(_mailText)
            //			 messageMenu.appendChild(mailMenu)
            //			 mailMenu.addEventListener("click", (event) => {
            //			 	talkContent.mailMessage()
            //			 })

            // .message__content-body
            container.lastElementChild.appendChild(messageMenu);            
        }

        return container;
    }

    // Head일 때만 들어옴(is_head, true)
    function changeHead(msgId, msgType, userId) {
        let messageLayoutHeader = document.querySelector(
            "[data-message-id='" + msgId + "'] .message__content-header"
        );
        if (messageLayoutHeader) {
            messageLayoutHeader.style.display = "flex";
            // TODO : css GUI 문서 확인 및 css 파일로 보낼 수 있는지 확인

            // todo : 삭제해도 되는지 확인
            //			if (msgType !== "newMessage") {
            //				document.querySelector("[data-message-id='"+msgId+"']").style.marginTop = "0.375rem";
            //			}
            if (userId) {
                // 서버 가서 받아오기 (동기 처리)
                //                    let userInfo = userManager.getUserInfo(targetMessage["USER_ID"]);
                document.querySelector(
                    "[data-message-id='" + msgId + "'] .message__content-header__name"
                ).textContent = talkUtil.getUserNick(userId);
            }
        }
    }

    // 기존 updateDeleteMsg 대신 만듦
    // getMessagePostProcess에서 msg_body를 "삭제된 메시지"로 바꿔주던 부분 삭제하고 여기에서 모두 처리
    function renderDeleteMsg(messageLayout, message) {
    	
    	
    	// 메세지 박스 형태가 있는지 확인 -> 이모티콘은 없음
    	
    	let msgBody = messageLayout.getElementsByClassName('message__content-body__items')[0];
    	let contentBody = messageLayout.getElementsByClassName('message__content-body__content')[0];

        if (msgBody.classList.contains("illust-message")) {
            msgBody.classList.remove("illust-message");
        }

        talkUtil.removeChildrenNodes(contentBody);
        
        let _deleteMsg = document.createElement("span");
        _deleteMsg.style.marginLeft = "0.5rem";
        
        _deleteMsg.textContent = "삭제된 메시지입니다.";
        contentBody.appendChild(_deleteMsg);

        contentBody.classList.add("icon-chart_error");
        contentBody.classList.add("talk__deleted-message");
                
        messageLayout.getElementsByClassName("message__content-body__state")[0].remove();
           
        let subMenu =   messageLayout.getElementsByClassName("message__sub-menu-container")[0];
        if (subMenu) {
        	subMenu.remove()
        }
        
        // attachment 부분
        let hasAttachment = message["ATTACHMENT_LIST"] && message["ATTACHMENT_LIST"].length;
        
        if (hasAttachment) {
        	for (let i = 0; i < message["ATTACHMENT_LIST"].length; i++) { 
        		let targetAttachment = message["ATTACHMENT_LIST"][i];
        		
        		switch (targetAttachment["ATTACHMENT_TYPE"]) {
        			// 파일 형식에 맞게 delete 이미지 파일 넣어줘야함
	        		case "file":
	        		case "fileList": {
	        			let arr = targetAttachment["ATTACHMENT_BODY"].match(/<(.*?)>/g);
	        			if (!arr) return
	        			let _obj = talkContent.talk_parseFile(arr[0]);
	        			
	        			let _fileIconClass = fileExtension.getInfo(_obj["data-file-extension"])["iconImage"];
	        			
	        			let _fileExtSrc = './res/talk/toword_delete.png';
	        			switch (_fileIconClass) {
		        			case "./res/drive/file_icons/drive_image.svg":
		        				_fileExtSrc = './res/talk/image_delete.png'
		        				break;
		        			case "./res/drive/file_icons/drive_tocell.svg":
		        				_fileExtSrc = './res/talk/tocell_delete.png'
		        				break;
		        			case "./res/drive/file_icons/drive_topoint.svg":
		        				_fileExtSrc = './res/talk/topoint_delete.png'
		        				break;
		        			case "./res/drive/file_icons/txt.svg":
		        				_fileExtSrc = './res/talk/text_delete.png'
		        				break;
		        			case "./res/drive/file_icons/drive_zip.svg":
		        				_fileExtSrc = './res/talk/zip_delete.png'
		        				break;
	        			}
				
						let attachmentContainer = messageLayout.getElementsByClassName('message__attachment-container')[0];
	        			
	        			talkUtil.removeChildrenNodes(attachmentContainer);
	                	attachmentContainer.style.width = "";

                        let messageUserId = message.USER_ID;
                        let messageUserName = userManager.getUserName(messageUserId);     
                        let messageUserNick = userManager.getUserNick(messageUserId);
                        let messageUser;
                        
                        if(messageUserNick !== ""){
                            messageUser = messageUserNick;
                        } else {
                            messageUser = messageUserName;
                        }
	                	
	                	let _deletedFileTemplate = `
        					<div class="file-message__deleted">
        						<img class="file-message__icon" src="${_fileExtSrc}"/>
        						<div class="file-message_text file-message__deletedMsg-text">${messageUser} 님에 의해 삭제된 파일입니다.</div>
        				    </div>
	        		    	`; 
	                		
                		// 파일은 삭제되었고 msg_body는 없으면 '삭제된 메시지입니다' 메시지 삭제
                		if (!message["MSG_BODY"]) {
                			talkUtil.removeChildrenNodes(contentBody);
                	        contentBody.classList.remove("icon-chart_error");
                	        contentBody.classList.remove("talk__deleted-message");
                		}
	                	
                		contentBody.insertAdjacentHTML('beforeend', _deletedFileTemplate);
	        			break;
	        		}
	        		case "reply" : {
	        			let reply =  messageLayout.getElementsByClassName("message__reply-container")[0];
	        	        if (reply) {
	        	        	reply.remove();
	        	        	contentBody.classList.remove("message__reply-content");
	        	        }
	        			break;
	        		}
	        		case "og": {
	        	        let ogContainer =  messageLayout.getElementsByClassName("attachment__og-container")[0];
	        	        if (ogContainer) {
	        	        	messageLayout.removeChild(ogContainer);
	        	        }
	        			break;
	        		}
        		}
            }
        }
        
        contentBody.style.display = "";
        msgBody.style.maxWidth = "";
        msgBody.style.minWidth = "";
        // attachment 첨부시 width 설정해준 경우 있어
        msgBody.style.width = "";
	}
	function updateMessageElementAutoRun(targetMessage, targetElement) {
		// temp일 때 안 그려도 되는건 msgId 존재 확인
		// const msgId = targetMessage["MSG_ID"];
		// // TEMP 메시지일 때
		// const tempId = targetMessage["TEMP_ID"];
		// let messageLayout, contentContainer;
		// if (msgId) {
		//
		// 	messageLayout = document.querySelector('[data-message-id="'+ msgId + '"]');
		// 	contentContainer = document.querySelector("[data-message-id='" + msgId + "'] .message__content-body__content");
		// 	console.log(" CONTAINER = ", contentContainer);
		// } else {
		// 	messageLayout = document.querySelector('[data-temp-id="'+ tempId + '"]')
		// 	contentContainer = document.querySelector("[data-temp-id='" + tempId + "'] .message__content-body__content");
		// 	console.log("TEMP  CONTAINER = ", contentContainer);
		// }
		// if (!messageLayout) return;
		//
		// // 빈 메세지 안보이게 하기 : msg_body는 없고, attachment는 뒤늦게 들어온 경우 때문에 이곳에 위치
		// if (targetMessage["MSG_TYPE"] === "create" && !targetMessage["MSG_BODY"]) {
		// 	if (!targetMessage["ATTACHMENT_LIST"].length) {
		// 		messageLayout.style.display = "none";
		// 	} else {
		// 		messageLayout.style.display = "flex";
		// 	}
		// }
		// //필터링하기 위해 messagelayout에 클래스 및 display 속성 추가,변화
		// talkSearch.actingForFiltering(targetMessage, messageLayout);
		//
		// // updateMessageElement 안에 autorun 걸기
		// let isHead = targetMessage["IS_HEAD"];
		// let msgType = targetMessage.MSG_TYPE;
		// const userId = targetMessage["USER_ID"];
		// let msgState = targetMessage["MSG_STATE"];
		// let msgBody = targetMessage["MSG_BODY"];
		// let unreadCount = targetMessage["UNREAD_USER_COUNT"];
		// const isMyMessage = userId === talkData.myInfo.userId;
		// let hasAttachment = targetMessage["ATTACHMENT_LIST"] && targetMessage["ATTACHMENT_LIST"].length;
		//
		// // ["IS_HEAD"] 바뀔때, 동작
		// // temp는 head 안 넣음
		// if (msgId && isHead) {
		// 	changeHead(msgId, msgType, userId);
		// }
		//
		// // ["MSG_STATE"] 바뀔때, 동작
		// // 복잡해져서 ver.1 코드 사용(temp냐 아니냐 구분해야해서 복잡)
		// switch (targetMessage["MSG_STATE"]) {
		// 	case "sending":
		// 		$(".message__content-body__state--fail", $(messageLayout)).hide();
		// 		$(".message__content-body__state--success", $(messageLayout)).hide();
		// 		$(".message__content-body__state--sending", $(messageLayout)).show();
		// 		var newTimerId = setTimeout(function() {
		// 			targetMessage["MSG_STATE"] = "fail";
		// 		}, talkData.responseTimeLimit);
		// 		$(messageLayout).attr("data-timer-id", newTimerId);
		// 		break;
		//
		// 	case "fail":
		// 		$(".message__content-body__state--success", $(messageLayout)).hide();
		// 		$(".message__content-body__state--sending", $(messageLayout)).hide();
		// 		$(".message__content-body__state--fail", $(messageLayout)).show();
		// 		$('.message__sub-menu-container', $(messageLayout)).css("display","none");
		// 		break;
		//
		// 	default:
		// 		$(".message__content-body__state--sending", $(messageLayout)).hide();
		// 		$(".message__content-body__state--fail", $(messageLayout)).hide();
		// 		$(".message__content-body__state--success", $(messageLayout)).show();
		// 		let timerId = $(messageLayout).attr("data-timer-id");
		// 		if (timerId) {
		// 			clearTimeout(timerId * 1);
		// 			$(messageLayout).removeAttr("data-timer-id");
		// 		}
		// 		break;
		// }
		//
		// // ["MSG_TYPE"] 바뀔때, 동작 + attachment가 있는 경우 attachment 그리기
		// if (msgType === "delete") {
		// 	console.log("이제 지우면돼????");
		// 	// renderDeleteMsg(msgId, isMyMessage);
		// 	contentContainer.innerHTML="삭제된 메시지입니다.";
		// 	// 모바일에서 1000byte이상은 말줄임표 처리하기
		// } else {
		// 	// Message_Body 그리기, temp일 때도 그린다.
		// 	if (msgBody) {
		// 		let originalText = talkMessageConverter.serverMsgToHtmlMsg(isPrepend, msgBody);
		// 		contentContainer.innerHTML = originalText;
		// 	}
		// 	// temp message 아닐 때 state, unRead
		// 	if (msgId) {
		// 		// attachment가 있는 경우 attachment 그리기
		// 		// temp message 아닐 때
		// 		if (hasAttachment) {
		// 			// attachment가 있는 경우 내 메세지든, 다른 사람의 메세지든 왼쪽 정렬
		// 			// TODO : 기존에 attachment_list의 length가 없을 때 동작하는 코드.... 맞는 코드? 그래서 바꿈
		// 			contentContainer.style.flexDirection = "row";
		//
		// 			for (let i = 0; i < targetMessage["ATTACHMENT_LIST"].length; i++) {
		// 				talkContent.renderAttachment(targetMessage, targetMessage["ATTACHMENT_LIST"][i], messageLayout, i);
		// 			}
		// 		};
		// 	}
		// }
		//
		// if (msgId) {
		// 	let stateContainer = document.querySelector("[data-message-id='" + msgId + "'] .message__content-body__state");
		// 	stateContainer.style.display = "flex";
		//
		// 	const unReadCountContainer = document.querySelector(
		// 		"[data-message-id='" + msgId + "'] .message__unread-count"
		// 	);
		// 	if (String(unreadCount) === "0") {
		// 		unReadCountContainer.style.display = "none";
		// 	}
		// 	else if (unreadCount) {
		// 		let temp = unReadCountContainer.textContent;
		// 		if (!temp || temp > unreadCount) {
		// 			unReadCountContainer.textContent = unreadCount;
		// 		}
		// 	}
		// }
	}
	
	
	function updateMessageElement( targetMessage, messageLayout ) {
    	if (!messageLayout) return;
    	// temp일 때 안 그려도 되는건 msgId 존재 확인	
        const msgId = targetMessage["MSG_ID"];
		let msgState = targetMessage["MSG_STATE"];
        if( msgState ==="sending" ){
        	// TEMP render를 분리 하자. !!!!!
			// 굳이 이걸 여기다 넣으면 안된다.
			// 깜박임을 최소화 하기 위에 넣는다.
			messageLayout.style.display = "none";
			setTimeout(function() {
				messageLayout.style.display = "flex";
				talkContent.gotoLastMessage();
			}, 500);
			
		}
        
        
        let contentContainer = messageLayout.getElementsByClassName('message__content-body__content')[0];
        
		// 빈 메세지 안보이게 하기 : msg_body는 없고, attachment는 뒤늦게 들어온 경우 때문에 이곳에 위치
        if (targetMessage["MSG_TYPE"] === "create" && !targetMessage["MSG_BODY"]) {
            if (!targetMessage["ATTACHMENT_LIST"].length) {
                messageLayout.style.display = "none";
            } else {
                // messageLayout.style.display = "flex";
            }
		}      
		//필터링하기 위해 messagelayout에 클래스 및 display 속성 추가,변화
		talkSearch.actingForFiltering(targetMessage, messageLayout);  
		
       // updateMessageElement 안에 autorun 걸기
        let isHead = targetMessage["IS_HEAD"];
        let msgType = targetMessage.MSG_TYPE;
        const userId = targetMessage["USER_ID"];
        let msgBody = targetMessage["MSG_BODY"];
        let unreadCount = targetMessage["UNREAD_USER_COUNT"];
        let hasAttachment = targetMessage["ATTACHMENT_LIST"] && targetMessage["ATTACHMENT_LIST"].length;

        // ["IS_HEAD"] 바뀔때, 동작
        // temp는 head 안 넣음
        if (msgId && isHead) {
        	changeHead(msgId, msgType, userId);
        }        

        // ["MSG_STATE"] 바뀔때, 동작
        // 복잡해져서 ver.1 코드 사용(temp냐 아니냐 구분해야해서 복잡)
        switch (targetMessage["MSG_STATE"]) {
            case "sending":
                $(".message__content-body__state--fail", $(messageLayout)).hide();
                $(".message__content-body__state--success", $(messageLayout)).hide();
                $(".message__content-body__state--sending", $(messageLayout)).show();
                var newTimerId = setTimeout(function() {
                    targetMessage["MSG_STATE"] = "fail";
                }, talkData.responseTimeLimit);
                $(messageLayout).attr("data-timer-id", newTimerId);
                break;

            case "fail":
                $(".message__content-body__state--success", $(messageLayout)).hide();
                $(".message__content-body__state--sending", $(messageLayout)).hide();
                $(".message__content-body__state--fail", $(messageLayout)).show();
            	$('.message__sub-menu-container', $(messageLayout)).css("display","none");
                break;

            default:
                $(".message__content-body__state--sending", $(messageLayout)).hide();
                $(".message__content-body__state--fail", $(messageLayout)).hide();
                $(".message__content-body__state--success", $(messageLayout)).show();
                let timerId = $(messageLayout).attr("data-timer-id");
                if (timerId) {
                    clearTimeout(timerId * 1);
                    $(messageLayout).removeAttr("data-timer-id");
                }
                break;
		}
		
        // ["MSG_TYPE"] 바뀔때, 동작 + attachment가 있는 경우 attachment 그리기
        if (msgType === "delete") {
        	// console.log("이제 지우면돼????");
			renderDeleteMsg(messageLayout, targetMessage);
			
			// contentContainer.innerHTML="삭제된 메시지입니다.";
            // 모바일에서 1000byte이상은 말줄임표 처리하기
        } else {
            // Message_Body 그리기, temp일 때도 그린다.
            if (msgBody) {
				contentContainer.innerHTML = talkMessageConverter.serverMsgToHtmlMsg( msgBody);
            }
            // temp message 아닐 때 state, unRead
            if (msgId) {
            	// attachment가 있는 경우 attachment 그리기
                // temp message 아닐 때
            	if (hasAttachment) {
                    // attachment가 있는 경우 내 메세지든, 다른 사람의 메세지든 왼쪽 정렬
                    // TODO : 기존에 attachment_list의 length가 없을 때 동작하는 코드.... 맞는 코드? 그래서 바꿈
                    contentContainer.style.flexDirection = "row";

                    for (let i = 0; i < targetMessage["ATTACHMENT_LIST"].length; i++) {                    	
                        talkContent.renderAttachment(targetMessage, targetMessage["ATTACHMENT_LIST"][i], messageLayout, i);
                    }
            	};
            }
        }
        
        if (msgId) {
			let stateContainer = messageLayout.getElementsByClassName('message__content-body__state')[0];
        	// Delete Message일경우 remove를 했다. 추후에 변경이 필요하다. 
        	// 위에서 querySelector를 사용하는 것은 붚필요하다.( layout에 직접 접근 하는게 맞다. )
        	if( stateContainer ){
        		stateContainer.style.display = "flex";
        		const unReadCountContainer =messageLayout.getElementsByClassName('message__unread-count')[0];
                if (String(unreadCount) === "0") {
                	unReadCountContainer.style.display = "none";
                }
                else if (unreadCount) {
                    let temp = unReadCountContainer.textContent;
                    if (!temp || temp > unreadCount) {
                        unReadCountContainer.textContent = unreadCount;
                    }
                }
        	}
        	
		}
    }
    
    // 임시 : image file만 미리보기 제공
    function renderOtherFileTag(info, fileIcon,isImage) {
        let _displyArrow = isImage ? "flex" : "none";
        
        // extension없는 경우 대비 + 있으면 앞에 '.' 붙이기
        let _fileExtension = info["data-file-extension"];
        if (_fileExtension) {
        	_fileExtension = '.'+_fileExtension;
        } else {
        	_fileExtension = '';
        }
        
        // TODO : 이미지 아닌 다른 파일도 미리보기가 가능해지면 onclick으로 다운로드하는 길을 지우자
        // 지금은 이미지인 파일은 fileName 클릭하면 미리보기 펼쳐지고 이미지 아닌 파일은 context menu열기!
        let fileId = info["data-file-id"];
    	let _onclick = `onclick="talkMessageConverter.openContextMenu(event,'${fileId}')"`
        
        let template = `
			<div class="file-message" style="max-width:15rem">
				<div class="file-info dnd__item" data-file-id="${info["data-file-id"]}">
					<img class="file-message__icon" src="${fileIcon}" />
					<div class="file-message_text">
						<div class="file-message_title" ${_onclick}>
							<span class="file-message__file-name talk-tooltip">${info["data-file-name"]}</span>
							<span class="file-message__file-extension">${_fileExtension}</span>
			            </div>
			            <div class="file-message__file-size talk-tooltip">${talkUtil.getFileFormatSize(info["data-size"])}</div>
					</div>
			    </div>
			    <div class="icon-arrow_up talk-file-message__arrow" style="display:${_displyArrow}"></div>
			</div>
        	` 
        return template;
    }
    
    function renderImageFileTag( info, _fileIconClass) {
        let title = talkRenderer.component.renderOtherFileTag(info, _fileIconClass, true);
        let fileId = info["data-file-id"];
        let fileName = info["data-file-name"];
        let fileExt = info["data-file-extension"];
        
		let srcUrl = '';
	  	srcUrl = storageManager.GetFileUrl(fileId, talkData.talkId, talkData.workspaceId);
        
//		let _clickEvent = `talkMessageConverter.openContextMenu(event,'${fileId}')`
		let _errorEvent = `talkRenderer.component.renderImageError(this, false)`
			
		// TODO : fileId가 왜 두 군데나 있는지 체크
		//  data-file-id="${fileId}", onclick="${_clickEvent}">, <div class="message__image-icon icon-work_download"></div> : 삭제 요청으로 삭제
        let template = `<div class="file-message__image-container">
							<div class="message__overlay">
								<p>${info["data-file-name"]}.${info["data-file-extension"]}</p>
							</div>
							<img class="talk__file_image message__image-file" src='${srcUrl}' data-file-id="${fileId}" data-file-name="${fileName}" data-file-extension="${fileExt}"onerror="${_errorEvent}">
						</div>`;
		let container = document.createElement('div')
		container.insertAdjacentHTML("beforeend", title)
		container.insertAdjacentHTML("beforeend", template)
        
		// TODO : 줄바꿈 필요없게 깔끔하게 바꾸기
        return container.innerHTML.replace(/\n/g,"");
    }
    

    function renderImageError(imageSelector, isBundled) {
        let template = `<div class="talk__deleted-file">
					        <img src="./res/talk/deleted_face.svg" class="talk__deletedFileIcon">
					        <div class="talk__deleted-file-text">손상되었거나 삭제된 파일입니다.</div>
					    </div>`
        	
    	// 묶어보내기 아닌 경우 target: file-message__image-container
    	// 묶어보내기인 경우 : message__fileList-item3(또는 2)
		let _target = imageSelector.parentNode
		
		// 일단 target을 비우고 deleted file tag를 넣는다
		talkUtil.removeChildrenNodes(_target)
		
        // 그냥 보내기일 때
    	if (!isBundled) {
    		// file icon에 error(!)표시 달아주기
    		let _errorMark = document.createElement("span");
    		_errorMark.classList.add("icon-chart_error"); // top-class
    		_errorMark.classList.add("talk__deletedFileMsg"); // 위치 설정하는 클래스
    		
    		let fileInfo = _target.previousElementSibling.querySelector('.file-info');
    		let fileIcon = fileInfo.querySelector('.file-message__icon');
			fileInfo.insertBefore(_errorMark, fileIcon);
			
			_target.classList.add("talk-message__deleted-container");

			// 다운로드 메뉴 뜨는 이벤트 없애기
			let eventRemoved = fileInfo.cloneNode(true);
			fileInfo.parentNode.replaceChild(eventRemoved, fileInfo);
    	} 
        // 묶어보내기일 때 : onclick으로 openContextMenu함수 동작하는 이벤트 제거
    	else {
    		_target.removeAttribute("onclick");
    	}
    	
    	_target.insertAdjacentHTML('beforeend', template)
    }

    function renderHoveredFileInfo(fileInfoContainer) {
    	fileInfoContainer.style.backgroundColor = "#DCDDFF";
    	
    	let fileIcon = fileInfoContainer.querySelector(".file-message__icon");
    	fileIcon.style.display = "none";
    	
    	let downloadBtn = fileInfoContainer.querySelector(".talk__fileDownloadBtn");
    	if (!downloadBtn) {
    		downloadBtn = document.createElement("div");
        	downloadBtn.classList.add("icon-work_download");
        	downloadBtn.classList.add("talk__fileDownloadBtn");
        	fileInfoContainer.insertBefore(downloadBtn, fileInfoContainer.firstElementChild);
    	}
    	
    	let fileId = fileInfoContainer.getAttribute("data-file-id");
    	let _click = (function(fileId, fileInfoContainer) {
    		let handler = function(event) {
    			talkMessageConverter.openContextMenu(event, fileId);
    		}
    		return handler;
    	})(fileId, fileInfoContainer);
    	
    	downloadBtn.addEventListener("click", _click, false);
    }
    
    function revertToFileInfo(fileInfoContainer) {
    	// contextMenu 있을 때는 작동 X
    	let _contextMenu = document.querySelector('#talk .context-menu');
    	if (_contextMenu && _contextMenu.style.display !== "none") {
    		return;
    	}
    	fileInfoContainer.style.backgroundColor = "#FFFFFF";
    		
		// fileIcon 살려줘야 해
		let fileIcon = fileInfoContainer.querySelector(".file-message__icon");
    	fileIcon.style.display = "";
    	
    	let downloadBtn = fileInfoContainer.querySelector(".talk__fileDownloadBtn");
    	downloadBtn.remove();
    }
    
    function getUnreadCount(workspaceId){
    	targetDiv = $("#"+workspaceId)
    	return Number(targetDiv.children("div.unreadBadge").children("div.unreadCount").text());
    }
    
    function setUnreadCount(workspaceId, unreadCount){
    	targetDiv = $("#"+workspaceId)
    	if(targetDiv){
    		if(unreadCount>0){
        		targetDiv.children("div.unreadBadge").css("visibility","visible");
    		}
        	else{
        		targetDiv.children("div.unreadBadge").css("visibility","hidden");
        	}
        	targetDiv.children("div.unreadBadge").children("div.unreadCount").text(unreadCount);	
    	}
	}
	
    // function _setCommunicationBarUnreadCount(){
	// 	let total = 0;
	// 	let unreadCountList = $('.unreadCount')
    // 	for(let i = 0; i<unreadCountList.length; i++){
    // 		total+=Number(unreadCountList[i].textContent)
    // 	}
    	
    		
    // }
    // isLastMessage = false -> 마지막 메시지인지 체크해야하는 것
    // handleMessage로 들어오는 것만 lnb unreadCount 갱신
    function renderLnb(message, isLastMessage, isHandleMessage) {
    	let msgId = message["MSG_ID"];
    	let targetWs, targetWsId;
    	
		for (let i=0;i<talkData.messengerList.length;i++) {
			if (talkData.messengerList[i]["CH_ID"] === message["CH_ID"]) {
				targetWs = talkData.messengerList[i];
				targetWsId = targetWs["WS_ID"];
				
				// msg_type === delete, messageUpdate인 경우 마지막 메시지인지 체크하기
				if (isLastMessage) {
					talkData.messengerList[i]["LAST_MESSAGE"] = message;
				}
				else if (targetWs["LAST_MESSAGE"]["MSG_ID"] === msgId) {	
					talkData.messengerList[i]["LAST_MESSAGE"] = message;
				} 
				else {
					return;
				}
				break;
			}
			
		}
		// // document.visibilityState -> 한 브라우저 내 선택된 탭 체크
		let focusFlag= tsFocus.get();
		// //gnb - talkCount (같은방 / 포커스된 경우 제외)
    	// if(workspaceManager.getWorkspace(targetWsId).WS_TYPE !== 'WKS0001'){
        // 	if (message["MSG_TYPE"]==="create" && isHandleMessage && !(focusFlag && talk.isSameRoom(message["CH_ID"]))) {
        // 		setTalkCount(parseInt(getTalkCount()) + 1);
        // 	}
       	// }
		
		let lnb = document.querySelector('div#ourSpaceLayout');
		let lnbRoom = document.querySelector(`div#${CSS.escape(targetWsId)}`);
		
		// 마이스페이스 아닐 때만
        if(lnbRoom){
        	if(workspaceManager.getWorkspace(targetWsId).WS_TYPE !== 'WKS0001'){
				lnb.insertBefore(lnbRoom, lnb.firstElementChild);

				// message update일 때는 count 올리면 안되지만 바로 메시지 생성가능하면 고려하지 않아도 되므로
				// [TODO] 파일을 올릴 때 dialog 때문에 window focus를 뺏겨서 읽지 않음 표시로 되는 문제와 파일 올릴 때 두번 보내져서 생기는 문제가 있음 파일을 보내는 로직을 변경할지, 두번 오도록 그대로 두고 예외처리를 할지는 결정해야하는데 전자가 나아보임.
            	if (message["USER_ID"]!==userManager.getLoginUserId() && message["MSG_TYPE"] !== "delete" && isHandleMessage && !( tsFocus.get() && document.querySelector('#talk').contains(document.activeElement)  && talk.isSameRoom(message["CH_ID"]))) {
            		lnb.querySelector('.unreadBadge').style.visibility = 'visible';
                	lnb.querySelector('.unreadCount').textContent = Number(lnb.querySelector('.unreadCount').textContent)+1;
            	}
        	}        	
			let _target = lnbRoom.querySelector('.msgBody');
    		_target.innerHTML = talkContent.convertToMsgBodyText(message);
        }
    }
    
    function renderReply(targetMessage, targetAttachment) {
    	const attachmentId = targetAttachment["ATTACHMENT_ID"];
    	const attachmentBody = targetAttachment["ATTACHMENT_BODY"];
    	const msgId = targetMessage["MSG_ID"];

    	// 필요한 정보 가져오기
    	let replyInfo = talkReply.serverMsgToReplyMsg(attachmentBody);
    	let originalMsgId = replyInfo["origin-msg-id"]
    	let originalMsgBody = talkReply.originalMsgtoHtmlMsg(replyInfo["origin-msg-body"]); 
    	let originalMsgSenderName = talkUtil.getUserNick(replyInfo["origin-msg-owner"]);

    	// layout만들기
    	let replyContainer = document.querySelector('[data-message-id="'+msgId+'"] .message__reply-container');
    	talkUtil.removeChildrenNodes(replyContainer);
    	replyContainer.setAttribute("data-attachment-id", attachmentId);

    	let replyTo = document.createElement('div');
    	replyTo.classList.add("message__replyto")
    	replyTo.textContent = originalMsgSenderName+"에 답장";

    	let originalMsgContainer = document.createElement('div');
    	originalMsgContainer.classList.add("message__original-msg-container");
    	if (talkContent.hasOnlyaEmoticon(replyInfo["origin-msg-body"])) {
    		originalMsgContainer.style.textAlign = "center";
    	}
    	originalMsgContainer.insertAdjacentHTML('beforeend',originalMsgBody)

    	replyContainer.appendChild(replyTo);
    	replyContainer.appendChild(originalMsgContainer);

    	// 클릭 이벤트
    	replyContainer.setAttribute("onclick", 'talkContent.gotoMessage(event,"' + originalMsgId + '")')

    	// 클래스 달기
    	if (targetMessage["USER_ID"] === talkData.myInfo.userId) {
    	  replyContainer.classList.add("toMyMessage")
    	} else {
    	  replyContainer.classList.add("toOthers")
    	}
    	
    	// 답장 내용에 화살표 붙이기
    	let _arrow = document.createElement("img");
    	_arrow.setAttribute("src", "./res/talk/reply.svg");
    	_arrow.classList.add("talk__reply-arrow");
    	
    	let msgBodyContainer = document.querySelector("[data-message-id='"+msgId+"'] .message__content-body__content");
    	let newMsgBody = document.createElement('div');
    	newMsgBody.insertAdjacentHTML('beforeend',msgBodyContainer.innerHTML);
    	
		talkUtil.removeChildrenNodes(msgBodyContainer);
		msgBodyContainer.appendChild(_arrow);
    	msgBodyContainer.appendChild(newMsgBody);
    	
    	// 화살표 바로 옆에 msgBody 오게 하기
    	msgBodyContainer.classList.add("message__reply-content")
    }
    
    function renderOg(targetMessage,targetAttachment) {
    	// 필요한 변수 정리
    	let msgId = targetAttachment["MSG_ID"];
    	let messageLayout = document.querySelector("[data-message-id='"+msgId+"']");
    	let attachBody = targetAttachment["ATTACHMENT_BODY"];
    	let JSONObj = attachBody.substring(1, attachBody.length - 1);
    	JSONObj = JSONObj.replace(/\n/gi, '\\n');
    	JSONObj = JSONObj.replace(/\r/gi, '\\r');
    	let jsObj = JSON.parse(JSONObj);
    	let imgSrc = jsObj["data-image"];
    	let domainName = jsObj["data-title"];
    	let description = jsObj["data-description"];
    	let _url = jsObj["data-url"];

    	// tag 생성
    	// 전체 container
    	let ogContainer = document.createElement("div")
    	ogContainer.classList.add("attachment__og-container")
    	ogContainer.setAttribute("data-attachment-id", targetAttachment["ATTACHMENT_ID"])
    	messageLayout.appendChild(ogContainer)

    	// img container
    	let imgContainer;
    	if (imgSrc || domainName) {
    	    imgContainer = document.createElement("img");
    	    imgContainer.classList.add("attachment__og-image");
    	    imgContainer.setAttribute("src", imgSrc);
    	    imgContainer.setAttribute("onerror", "talkContent.replaceOgImg(this,'" + domainName + "')");
    	    ogContainer.appendChild(imgContainer);
    	}

    	// contents container
    	let ogContent = document.createElement("div");
    	ogContent.classList.add('attachment__og-content-container')
    	ogContainer.appendChild(ogContent);

    	let nameContainer;
    	if (domainName) {
    	    nameContainer = document.createElement("span");
    	    nameContainer.classList.add("attachment__og-title")
    	    nameContainer.textContent = domainName;
    	    ogContent.appendChild(nameContainer);
    	}

    	let descContainer;
    	if (description) {
    	    descContainer = document.createElement("span");
    	    descContainer.classList.add("attachment__og-description")
    	    descContainer.textContent = description;
    	    ogContent.appendChild(descContainer);
    	}

    	if (_url) {
    	    ogContainer.addEventListener("click", () => {
    	        window.open(_url);
    	    }, false)
    	}

    	// 마지막 메세지 5개 image load event 추가
    	// TODO : 작동 check
    	// 필요없으면 targetMessage 인자로 넘겨주지 말기
    	talkContent.loadImageEvent(targetMessage, messageLayout, '.attachment__og-image')

    	// style
    	// TODO : 모바일은 나중에 모바일 git에서 class 먹이기
    	if (talkUtil.isMobile() || talkUtil.isSimple()) {
//    	    ogContainer.style.flexDirection = "column";
//    	    ogContainer.style.alignItems = "center";
//    	    imgContainer.style.marginLeft = "auto";
//    	    imgContainer.style.marginRight = "auto";
    	    document.querySelector('[data-message-id="' + msgId + '"] .message__content-body__items').style.maxWidth = "11rem";
    	}
    	    
	}
	
    function renderStartMeeting(targetAttachment, isMyMessage) {
    	let msgId = targetAttachment["MSG_ID"];
    	let attachmentId = targetAttachment["ATTACHMENT_ID"];
    	// TODO : 참여중인지 아닌지 알아야
    	let _attending = false;
    	let _buttonMsg = "TeeMeeting 참여";
//    	if (_attending) {
//    		_buttonMsg = "참여 중";
//    	}
    	
    	// TODO : 서비스콜 주세요. 종료된 미팅인지 알아야합니다~
    	let roomExist = true;
    	if (!roomExist) {
    		_buttonMsg = "종료된 미팅입니다.";
    	};
    	
		let _display = "flex";
    	if (isMyMessage) {
    		_display = "none";
    	}
    	let _src = './res/talk/TeeMeeting.png';
    		
		let startMeetingTemplate = `
			<div class="talk__meeting-start-wrapper">
				<div class="talk__meeting-msg">
					<img src="${_src}" />
					<span class="talk__meeting-start-msg">TeeMeeting을 시작합니다.</span>
				</div>
				<div class="talk__meeting-button-wrapper" style="display:${_display}">
					<div class="talk__meeting-button" onclick='document.getElementById("gnbAppMeetingIcon").click()'>${_buttonMsg}</div>
				</div>
			</div>
		`
		let attachmentContainer = document.querySelector(
	            "[data-message-id='" + msgId + "'] .message__attachment-container"
	        );
		attachmentContainer.setAttribute("data-attachment-id", attachmentId)
		attachmentContainer.insertAdjacentHTML('beforeend', startMeetingTemplate);
    }
    
    function renderEndMeeting(targetAttachment) {
    	let msgId = targetAttachment["MSG_ID"];
    	let attachmentId = targetAttachment["ATTACHMENT_ID"];
    	let _src = './res/talk/TeeMeeting.png';
    	
    	let endMeetingTemplate = `
				<div class="talk__meeting-msg">
					<img src="${_src}" />
					<span class="talk__meeting-end-msg">TeeMeeting을 종료합니다.</span>
				</div>`
		
		let attachmentContainer = document.querySelector(
	            "[data-message-id='" + msgId + "'] .message__attachment-container"
	        );		
		attachmentContainer.setAttribute("data-attachment-id", attachmentId)
		attachmentContainer.insertAdjacentHTML('beforeend', endMeetingTemplate);
    }
    
    function renderAttachment() {}

    
    function createTopDialog(id, width, height, title, layout, onOpen) {
        if (!Top.Dom.select(id).length) {
            let commonDialog = Top.Widget.create("top-dialog", {
                id: id,
                class: "popup_02 move-x-button",
                layoutWidth: width,
                layoutHeight: height,
                title: title,
                isConfirmation: "false",
                dialogLayout: layout,
                movable: "true",
                closedOnclickoutside: "false",
                onOpen: onOpen
            });

            if (document.body)
                document.body.appendChild(commonDialog.template);            
        }
    }
    
    function renderFileUploadDialog(isExternalUpload, onSendClick) {
    	let count = talkData.fileChooserList.length;
    	
    	let template = 
		`
		<div id="talk_noti_top">다음 파일을 전송합니다.</div>
		<div id="talk__message-input-title" class="talk-dialog__text">메시지</div>
		<div id="talk-dialog__text-area" class="talk__input" contenteditable="true" data-placeholder="메시지를 입력하세요. (Ctrl + Enter로 줄 바꿈이 가능합니다)"></div>
		<div class="talk__selected-title">
    		<div>
    			<span class="talk-dialog__text">선택된 파일</span>
    			<span id="selectedItemCount" class="talk-dialog__text" style='display:${isExternalUpload ? "inline-block" : "inline-block"}'>${count}</span>
    			<div class="icon-chart_error file-upload__error-msg">
    				<span>파일 전송은 한번에 30개까지 가능합니다.</span>
    			</div>
			</div>
			<div class="talk__common-button--white talk-dialog__add-file-btn" style='display:${isExternalUpload ? "none" : "flex"}'>+</div>
		</div>
		<div id="selected-item-container" class="talk__selected-container"></div>
		<input type="checkbox" id="multipleFiles" name="multipleFiles" value="파일 묶어보내기" margin="0.313rem">
		<label for="multipleFiles" class="icon-check" style='display:${isExternalUpload ? "none" : "flex"}'></label>
		<label style='display:${isExternalUpload ? "none" : "flex"}'>파일 묶어보내기</label>
		<div class="talk-dialog__button-container">
    		<button id="talk__action-btn" class="talk__common-button--purple">첨부</button>
    		<button id="talk-dialog__cancel-btn" class="talk__common-button--white">취소</button>
		</div>
		`	
		document.querySelector('div#talk__dialog-layout').insertAdjacentHTML("beforeend", template);

    	if(!talkFooter.getDriveAttachStatus()){
	    	let talkFileAttach = document.querySelector(".talk__selected-container");
	    	talkFileAttach.classList.add("dnd__container");
	    	talkFileAttach.setAttribute("data-dnd-app", "talk-file-dialog");    	
    	}
    	
    	// 04-28 파일 업로드 다이얼로그 띄울 때 미니 톡이면 사이즈 확장시켜줌. 크기에 대한 기획 없어서 일단 임시로 박음
        if( window.location.href.indexOf('talk?mini=true') != -1 ) { // 미니 채팅룸일 때
            window.resizeTo(500,1000);
            document.querySelector('div#talk__dialog-layout').classList.add('minitalk_local_upload_dialog')
        }
    	
    	document.querySelector('.talk-dialog__add-file-btn').addEventListener("click", () => {
    		talkFileUpload.openFileChooser(false);
    	}, false);
    	// TODO : talk-dialog__action-btn이라고 이름 바꾸고 싶다
    	document.querySelector('#talk__action-btn').addEventListener("click", () => {
    		isExternalUpload ?  onSendClick() : talkFileUpload.uploadLocalFiles();
    		document.querySelector('#talk-footer__input').textContent = "";
    	}, false);
    	document.querySelector('#talk-dialog__cancel-btn').addEventListener("click", () => {
    		Top.Dom.selectById("talk__file-upload-dialog").close(true);
    		talkData.fileChooserList.clear();
			talkData.fileMetaList.clear();
		}, false);		

		/////////////////////
		//////////////ctrl+enter로 줄바꿈///////////
		/////////////enter키로 파일 전송///////////
		document.querySelector('#talk-dialog__text-area').addEventListener('keydown',function(event){
			event.stopPropagation();	
			if(event.ctrlKey && event.keyCode == 13){	
				event.preventDefault();
				document.execCommand("insertParagraph", false, "p");
				const input = document.getElementById('talk-dialog__text-area');
				if(input) input.scrollTop = input.scrollHeight;
			}else if(event.keyCode == 13){
			 	event.preventDefault();
			 	isExternalUpload ?  onSendClick() : talkFileUpload.uploadLocalFiles();
			 	document.querySelector('#talk-footer__input').textContent = "";
			 }
		})

		document.querySelector('#talk-dialog__text-area').addEventListener('paste',(event)=>{
			event.stopPropagation();
        	event.preventDefault();
			let clipboardData = event.clipboardData || window.clipboardData;
			let pastedText = null;
			if(pastedText = clipboardData.getData("Text")) document.execCommand("insertText", false, pastedText);
		})

		// $(document).one('keydown',function(event){
		// 	if(event.keyCode == 13){ 
		// 		event.stopPropagation();
		// 		isExternalUpload ?  onSendClick() : talkFileUpload.uploadLocalFiles();
		// 	}
		//  })

		 
		 ///////////////
	}
	
    function addFileMessage(){
    	let msgText = document.querySelector('#talk-footer__input').textContent;
    	let container = document.querySelector('#talk-dialog__text-area')
    	container.textContent = msgText;
    	
    }
    function addFileChip(fileList, isExternalUpload) {
		let container = document.querySelector('#selected-item-container');		
		let _actionBtn = document.querySelector('#talk__action-btn');
		if(fileList.length > 30){
			_actionBtn.disabled = true;
		} else {
			_actionBtn.disabled = false;
		}
		
		for (let i=0; i< fileList.length; i++ ){
			let fileName, fileNameArray, file_extension;
			
			if(fileList[i]["name"]) {
				fileName = Top.Util.Browser.isIE() ? fileList[i]["name"] : fileList[i].name.normalize('NFC')
			} else {
				fileName = fileList[i].storageFileInfo.file_name + "." + fileList[i].storageFileInfo.file_extension;
			}					
		    fileNameArray = fileName.split('.');
	        file_extension = fileNameArray.length == 1 ? "" : fileNameArray[fileNameArray.length - 1];   
	        if (file_extension !== ""){
    			fileName = fileName.slice(0,fileName.indexOf(file_extension)-1)
    			// 폴더일 때 extension 앞에 . 없어야 해서 이 때 추가
    			file_extension = '.' + file_extension
    		}
	        
	        let talkChip =
	        	`
	        	<div class="talk-chip talk-dialog__chip" data-file-name=${fileName}>
	        		<span class="talk-dialog__chip-text">${fileName}${file_extension}</span>
	        		<span style='display:${isExternalUpload? "inline-block" : "inline-block"}'>&times;</span>
	        	</div>
	        	`
        	let temp = new DOMParser().parseFromString(talkChip, 'text/html');
	        talkChip = temp.body.children[0]
	        container.appendChild(talkChip)
	        
	        if(talkFooter.getDriveAttachArr().length || talkData.fileMetaList.length){        		        	
		        talkChip.addEventListener("click", function(){ talkFileUpload.deleteFileChip(i, event.currentTarget) }, false);		        
	        } else {
	        	let _click = (function(file) {
		        	let handler = function(event) {
		        		let targetIdx = talkData.fileChooserList.findIndex((elem) => {
			        		return elem === fileList[i];
			        	});
			        	
			        	talkFileUpload.deleteFileChip(targetIdx, event.currentTarget);
		        	}
		        	return handler;
		        	
		        })(fileList[i]);	        		        	
		        talkChip.addEventListener("click", _click, false);
	        }
		}        
    }
    
    function renderSimpleDialog(type) {
    	let _warnContainer = document.createElement("div");
    	_warnContainer.classList.add("icon-chart_error")
    	_warnContainer.classList.add("talk-dialog__icon")
    	
		let _title = document.querySelector(".top-dialog-title-layout")
		talkUtil.removeChildrenNodes(_title);
    	
    	let _msgContainer = document.createElement("div");
    	_msgContainer.classList.add("talk-dialog__question-text");
    		
		let _btn, _rootContainer;
    	if (type === "delete") {
    		_rootContainer = document.querySelector("#top-dialog-root_talk__delete-message-dialog");
    		
    		_msgContainer.textContent = "다음 메시지를 삭제하시겠습니까?"
			_btn = talkRenderer.dialog.renderDialogBtn("deleteMessage");
    	}
    	else if (type === "resend") {
    		_rootContainer = document.querySelector("#top-dialog-root_talk__resend-message-dialog");
    		_msgContainer.textContent = "재전송 하시겠습니까?"
			_btn = talkRenderer.dialog.renderDialogBtn("resendMessage");
    	}
    	
    	let dialogLayout = document.querySelector("div#talk__dialog-layout")
    	dialogLayout.appendChild(_warnContainer);
		dialogLayout.appendChild(_msgContainer);
    	dialogLayout.appendChild(_btn);
    	
    	let _talk = document.querySelector('#talk-main');
    	
    	_talk.appendChild(_rootContainer)
    }
    
    function renderResendDialog() {
    	talkRenderer.dialog.renderSimpleDialog("resend");
    }
    
    function renderDeleteDialog() {
    	talkRenderer.dialog.renderSimpleDialog("delete");
    }
    
    // TODO : talkDailog에 있는 createDialogBtn 대체하고 test
    function renderDialogBtn(type) {   	
        // type : 만들기인지, 초대인지
        let command = ""
    	let dialogName;
    	switch (type) {
	    	case "fileUpload":
	    		command = "업로드"
    			dialogName = "talk__file-upload-dialog"
				actionBtn.addEventListener("click", () => {
					talkFileUpload.uploadLocalFiles();
				})
    			break;
	    	case "sendToMessenger":
	    		command = "보내기"
    			dialogName = "send-to-talk-dialog"
    			break;
	    	case "deleteFile":
	    		command = '삭제'
	    		dialogName = "talk__delete-file-dialog";
    			break;
	    	case "deleteMessage":
	    		command = '삭제'
	    		dialogName = "talk__delete-message-dialog"
    			break;
	    	case "resendMessage":
	    		command = '확인';
    			dialogName = "talk__resend-message-dialog";
    		default:
    			break;
    	}
    	
    	
        // 만들기, 취소 버튼
    	let buttonContainer = document.createElement("div")
    	buttonContainer.classList.add("talk-dialog__button-container")
    	
    	let actionBtn = document.createElement("button")
    	actionBtn.classList.add("talk__common-button--purple")
    	actionBtn.setAttribute("id", "talk-dialog__action-btn")
    	    	
    	let cancelBtn = document.createElement("button")
    	cancelBtn.classList.add("talk__common-button--white")
    	cancelBtn.setAttribute("id", "talk-dialog__cancel-btn")
    	
		buttonContainer.appendChild(actionBtn);
    	buttonContainer.appendChild(cancelBtn);
    	
    	actionBtn.textContent = command;
    	cancelBtn.textContent = "취소"
    	
		cancelBtn.addEventListener("click", () => {
        	Top.Dom.selectById(dialogName).close(true);
        	if (document.querySelector('#top-dialog-root_talk__file-upload-dialog')) {
        		talkData.fileChooserList.clear();
    			talkData.fileMetaList.clear();
        	}
		})
		
        return buttonContainer;
    }
    
    function changeUserPhoto(userId, newSrc) {
    	let messageList = document.querySelectorAll('talk-message-layout[data-user-id="'+userId+'"]');
    	let oldSrc;
    	for (let i=0;i<messageList.length;i++) {
    		if (i === 0) {
    			oldSrc = messageList[i].querySelector('.message__photo').getAttribute('src');
    			if (newSrc === oldSrc) return
    		}
    		
    		messageList[i].querySelector('.message__photo').setAttribute('src', newSrc);
    	}
    }
    
    function profileImgError(event) {
    	let profileImgContainer = event.target;
    	let wrapper = profileImgContainer.parentNode;
    	profileImgContainer.style.display="none";
    }
    
    return Object.freeze({
        structure: {
            //            render: render,
            //            renderHeader: renderHeader,
            renderContent: renderContent,
            renderFooter: renderFooter
        },
        //
        component: {
            //            renderTitle: renderTitle,
        	profileImgError:profileImgError,
            renderMessage: renderMessage,
            createMessageLayout: createMessageLayout,
            createNewMsgSeparator: createNewMsgSeparator,
            createSystemElement: createSystemElement,
            createUserInOutMessage: createUserInOutMessage,
            createMessageElement: createMessageElement,
            updateMessageElement: updateMessageElement,
            changeHead: changeHead,
            changeUserPhoto:changeUserPhoto,
            renderDeleteMsg: renderDeleteMsg,
            renderAttachment: renderAttachment,
            renderImageFileTag : renderImageFileTag,
            renderImageError: renderImageError,
            renderOtherFileTag : renderOtherFileTag,
            renderHoveredFileInfo: renderHoveredFileInfo,
            revertToFileInfo:revertToFileInfo,
            renderReply:renderReply,
            renderOg:renderOg,
            renderStartMeeting:renderStartMeeting,
            renderEndMeeting:renderEndMeeting,
            setUnreadCount:setUnreadCount,
            renderLnb:renderLnb,
            //Search부분에서 그리는 함수
            showArrowButtons: _showArrowButtons,
            hideArrowButtons: _hideArrowButtons,
            showcurIdxOfMsg : _showcurIdxOfMsg,
            hidecurIdxOfMsg : _hidecurIdxOfMsg,
			showIdxOfMsg: _showIdxOfMsg,
			//setCommunicationBarUnreadCount:_setCommunicationBarUnreadCount,
		   // toggleCategoryList : _toggleCategoryList,
		   renderUnreadCount: _renderUnreadCount
        },
        dialog : {
        	createTopDialog:createTopDialog,
        	renderFileUploadDialog: renderFileUploadDialog,
        	addFileMessage:addFileMessage,
        	addFileChip:addFileChip,
        	renderSimpleDialog:renderSimpleDialog,
        	renderDialogBtn: renderDialogBtn,
        	renderResendDialog : renderResendDialog,
        	renderDeleteDialog : renderDeleteDialog
        }
        
    });
})();

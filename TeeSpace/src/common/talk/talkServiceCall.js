axios.defaults.headers.common['Content-Type'] = 'application/json';
axios.defaults.timeout = talkData.responseTimeLimit;

const talkServer2 = (function () {
    return {
        ttalk_unreadList : function(talkId, startMsg, endMsg) {
        	if(talkId && startMsg && endMsg) return axios.put(talkData.serverURL + "Messenger/messengers/"+talkId+"/TtalkUnread?action=List&start="+startMsg+"&end="+endMsg)
            return new Promise((resolve,reject)=>{
                reject('ttalk_unreadList is error!');
            })
        },

    	
    	// 타이핑 중인 유저 알림 서비스 : 특정 방에 누군가 타이핑을 하고 있는 지를 서로의 client에게 알려주기 위한 서비스
    	sendTyping : function(workspaceId, messengerId, userId){
    		return axios.post(_workspace.url + "Messenger/workspaces/"+workspaceId+"/messengers/"+messengerId+"/ttalkrooms?action=Typing&user-id="+userId);
        	
    	},
    	//메시지 검색 서비스 : 특정 방에 특정 키워드의 내용을 가진 메시지를 반환한다.
    	searchMessage: function(talkId, keyword, type){
    		let messageList;
    		 jQuery.ajax({
                 type: "POST",
                 url: talkData.serverURL + "Messenger/ttalkmessages?action=Search&talk-id="+ talkId +"&keyword=" + keyword + "&msg-type=" + type ,                 
                 contentType: "application/json",
                 async: false,
                 success: function (data) {
                     messageList = data.dto.MSG_LIST;
                     for(let i=0; i<messageList.length; i++){  
                         if(messageList[i].MSG_BODY!==null&&messageList[i].MSG_BODY.includes("<")){
                        	 messageList[i].MSG_BODY = talkContent.convertToMsgBodyText(messageList[i].MSG_BODY);
                         }
                      }
                 }
             });           
             return messageList;
    	},
    	//메신저 조회 서비스 : 내가 포함되어 있는 모든 메신저를 조회한다.
    	getMessengerRooms: function (userId, isTemp="false", workspaceId) {
    		let urlStr;
    		if(workspaceId){
    			urlStr = _workspace.url + "Messenger/ttalkmessengers?action=List&user-id=" + userId + "&ws-id=" + workspaceId + "&istemp=" + isTemp;
    		}
    		else{
    			urlStr = _workspace.url + "Messenger/ttalkmessengers?action=List&user-id=" + userId + "&istemp=" + isTemp;
    		}
            let userInfo = {};

            jQuery.ajax({
                type: "GET",
                url: urlStr,
                contentType: "application/json",
                async: false,
                success: function (data) {
                    messengerInfo = data.dto;
                }
            });
            
            //이부분 다 수정해야함. 우선 msg_body_text로 변환할때 기존에는 msg_body에 있는 것 만을 했지만 이제는 attachment에 있는 정보로도 convert해야함
            if(messengerInfo.ttalkMessengersList!=null){
                 let targetMessengerInfo = null;
                 // let inputText = null;
                 // let msgType, userId = null;
                 for(let i=0; i<messengerInfo.ttalkMessengersList.length; i++){ 
            	 //     //TODO : 라스트 메시지가 널일때는 아무것도 안할꺼야 
                	 targetMessengerInfo = messengerInfo.ttalkMessengersList[i];
                	 messengerInfo.ttalkMessengersList[i].MSG_BODY = talkContent.convertToMsgBodyText(targetMessengerInfo["LAST_MESSAGE"]);
                 }
             	 talkData.messengerList = messengerInfo.ttalkMessengersList;
            }
            return messengerInfo;
        },
        //유저 조회 서비스 : 유저 정보를 조회한다.
        getUserInfo: function (userId) {
            let userInfo = {};

            jQuery.ajax({
                type: "GET",
                url: _workspace.url + "Users/BothProfile?{%22dto%22:{%22USER_ID%22:%22" + userId + "%22}}",
                contentType: "application/json",
                async: false,
                success: function (data) {
                    userInfo = data.dto;
                }
            });
            return userInfo;
        },
        //유저의 프로필 정보를 다중으로 받아오는 로직
        getMultipleUsersInfo: function(userIdList) {
            let _userList = [];
            for (let i=0;i<userIdList.length;i++) {
                _userList.push({"USER_ID" : userIdList[i]})
            };
            jQuery.ajax({
                url: _workspace.url + "Users/BothProfileList?action=Get",
                //Service Object
                type: 'POST',
                crossDomain: true,
                contentType: 'application/json',
                data: JSON.stringify({
                  "dto": {
                    "userList" : _userList
                  }
                }),                
                async: false,
                success: function (data) {
                    userInfoList = data.dto;
                }
            })
            return userInfoList;
        },

        //파일 유효성 체크 서비스 : 파일의 유효성을 체크한다.
    	validFileCheck: function(fileId){
    		return axios.get(talkData.serverURL + "Storage/StorageMeta?fileID="+fileId+"&channelID="+talkData.talkId+"&userID="+talkData.myInfo.userId+"&workspaceID="+talkData.workspaceId);
    	},
    	
    	//단순 웹소켓 전송 서비스 : DB에 가지 않고 WEBSOCKET만을 사용할 때 사용한다.
    	sendWebsocketMessage: function(targetMessage){
    		let message = {
    				"MSG_TYPE":"messageUpdate",
    				"MSG_ID" : targetMessage,
    				"ROOM_ID" : talkData.selectedRoomId.get(),
    				"USER_ID" : talkData.myInfo.userId
    				
    		}
    		let inputData = {
    	            		  "dto":{
    	            		    "USER_ID" : talkData.myInfo.userId,
    	            		    "CH_ID" : talkData.talkId,
    	            		    "CH_TYPE" : "CHN0001",
    	            		    "NOTI_TARGET" : talkData.workspaceId+"/"+talkData.talkId+"/"+talkData.selectedRoomId.get()+"/"+targetMessage,
    	            		    "NOTI_TO" : talkData.talkId,
    	            		    "NOTI_TYPE" : "MESSAGE_CREATE",
    	            		    "NOTI_ETC" : JSON.stringify(message)
    	            	    }
    	            	
    		};
    		return axios.post(talkData.serverURL + "Messenger/ttalkmessages?action=Event",inputData);
    	},
    	
    	//파일 다운로드 서비스 : 파일을 다운로드한다.
    	getFileDownload: function(fileID){
    		return axios.get(talkData.serverURL + "Storage/" +
                    "StorageFile?action=Download&fileID="+fileID+"&workspaceID="+talkData.workspaceId+"&channelID="+talkData.talkId+"&userID="+talkData.myInfo.userId);
    	},
    	
    	getStorageFiles : function() {
    		return axios.get(talkData.serverURL + "Storage/" +
                    "StorageMetaList?workspaceID="+talkData.workspaceId+"&channelID="+talkData.talkId);
    	},
    	
    	deleteStorageFile : function(inputData) {
    		return axios.post(talkData.serverURL + "Storage/StorageFile?action=Delete", inputData);
    	},
    	
    	deleteStorageMultiFile : function(inputData) {
    		return axios.post(talkData.serverURL + "Storage/StorageFileMulti?action=Delete", inputData);
    	},

        //메시지 단일 읽음처리 서비스 : 특정 방의 마지막 읽은 메시지를 갱신한다.
        ttalk_readMessage : function(talkId, msgId, userId) {
            if(talkId && msgId && userId && (talkData.lastReadMsgId!==msgId)) {
            	talkData.lastReadMsgId = msgId;
            	return axios.put(talkData.serverURL + "Messenger/messengers/"+talkId+"/ttalkmessages?action=Read&msg-id="+msgId+"&user-id="+userId)
            }
            return new Promise((resolve,reject)=>{
//                reject("read try");
            })
        },
        
        //부가 메시지 생성 서비스 : 특정 메시지에 대한 ATTACHMENT를 생성한다.
        ttalk_createAttachment: function(msgId, TtalkAttachmentList){
        	let inputData = {
        			"dto":{
        				MSG_ID : msgId,
        				TtalkAttachmentsList : TtalkAttachmentList
        			}
        	}
    	    return axios.post(talkData.serverURL +
                  "Messenger/ttalk/" + talkData.talkId +"/ttalkattachments?user-id="+talkData.myInfo.userId,inputData)
        },
    
        //방 리스트 조회 서비스 : 특정 유저의 방을 조회한다.
        ttalk_getRoomList: function () {
            return axios.post(talkData.serverURL +
                "Messenger/ttalk/" + talkData.talkId + "/ttalkrooms?action=List&user-id=" + talkData.myInfo.userId)
        },
      //스페이스 멤버 조회 : 해당 스페이스에 포함된 유저의 정보를 조회한다.
        getSpaceMemberInfo: function (userId, workspaceId) {
            return axios.get(talkData.serverURL + "SpaceRoom/SpaceRoomMemberList?{%22dto%22:{%22USER_ID%22:%22" + userId + "%22,%22WS_ID%22:%22" + workspaceId + "%22}}");
        },
        // 메세지 생성
        createMessage: function (messageDTO, workspaceId) {
        	if(talkData.messageList.length === 0){   	
        		spaceAPI.spaceInit()        
            }
            let talkId = workspaceManager.getChannelList(workspaceId,"CHN0001");
            // let inputData = this.createMessageFormat(ROOM_ID, MSG_BODY, MSG_TYPE);
            return axios.post(talkData.serverURL +
                "Messenger/workspaces/"+workspaceId+
                "/rooms/"+workspaceId+
                "/ttalk/" + talkId + "/ttalkmessages", { "dto": messageDTO })
        },
        //테스트 용 미팅 초대용
        meetingTest: function (workspaceId, msgBody, userId, meetingRoomId, isStart){
        	return axios.post(talkData.serverURL +
                    "Messenger/workspaces/" + workspaceId + "/ttalkmessages",
                    { 
        				"dto": 
        				{
        					USER_ID: userId,
            				MSG_BODY: msgBody,
            				MSG_TYPE: "create",
            				MSG_BODY_TEXT : (isStart?"TeeMeeting을 시작합니다":"TeeMeeting을 종료합니다"),
            				ATTACHMENT_LIST : [{
            					   ATTACHMENT_TYPE: (isStart?"startMeeting":"endMeeting"),
            					   ATTACHMENT_BODY : "<{'data-type':"+(isStart?"'startMeeting'":"'endMeeting'")+", 'data-user-id':"+userId+", 'data-meetingRoom-id':"+meetingRoomId+"}>"
            					}]
        				} 
        			})
        },
        
        createMessageFormat: function (ROOM_ID, MSG_BODY, MSG_TYPE) {
            let dto = {
                USER_ID: talkData.myInfo.userId,
                ROOM_ID: null,
                MSG_BODY: MSG_BODY,
                TEMP_ID: talkUtil.generateGUID(),
                MSG_TYPE: MSG_TYPE,
                MSG_TIME: new Date(),
                ATTACHMENT_LIST: []
            }

            if (talkData.selectedReply.value !== null) {
                let attachmentInfo = {
                    ATTACHMENT_TYPE: "reply",
                    ATTACHMENT_BODY: talkReply.replyMsgToServerMsg(talkData.selectedReply),
                    ch_id : talkData.talkId
                };
                dto["ATTACHMENT_LIST"].push(attachmentInfo)
            }

            return dto;
        },        

        //메시지 삭제
        deleteMessage: function (roomId, msgId) {
            let inputData = {
                "dto": {
                    "MSG_TYPE": "delete"
                }
            }
            return axios.put(talkData.serverURL + "Messenger/workspaces/"+talkData.workspaceId+"/ttalk/"+talkData.talkId+"/ttalkrooms/" + roomId + "/ttalkmessages/" + msgId, inputData)

        },

        //메시지 단일 조회
        ttalk_getMessage: function(talkId, msgId){
       	 return axios.get(talkData.serverURL +"Messenger/ttalk/" + talkId + "/ttalkmessages/"+msgId)
       },
        
        // 메세지 리스트 조회
        ttalk_getMessageList: function (lastMsg, count) {
        	if(lastMsg===0){
        		lastMsg = "none";
        	}
            return axios.get(talkData.serverURL +
                "Messenger/ttalk/" + talkData.talkId + "/ttalkrooms/" + talkData.selectedRoomId.get() + "/ttalkmessages?action=List&count="
                + count + "&last-msg=" + lastMsg + "&user-id=" + talkData.myInfo.userId)
        }
    }
})();

const talkTooltip = (function () {

    function isEllipsisActive(element) {
        return element.offsetWidth < element.scrollWidth;
    }

    function _isExist() {
        return !!$("body > .tooltip").length;
    };

    function _init() {
        if (!_isExist()) {
            $('<span/>', {
                'class': 'tooltip'
            }).appendTo('body');
        }
    }

    function _setText(str) {
        _init();
        $('body > .tooltip').text(str);
    }

    function _show() {
        _init();

        let marginRight = 20;
        let margin = 5;
        let top = event.clientY + margin;
        let left = event.clientX + marginRight;
        // if(event.clientX + margin + _this.width() > $(document).width()){
        //     left = event.clientX - (event.clientX + margin + _this.width() - $(document).width());
        //     // left = margin + _this.width() - $(document).width();

        // }
        let tooltip = $("body > .tooltip").css({
            'visibility': 'visible',
            'top': top + 'px',
            'left': left + 'px',
            'z-index' : '1001'
        });
        
        if (event.clientX + marginRight + tooltip.width() > $(document).width()) {
            tooltip.css({
                'left': event.clientX - (event.clientX + marginRight + tooltip.width() - $(document).width()) + 'px'
            });
        }

    }

    function _hide() {
        _init();
        $("body > .tooltip").css('visibility', 'hidden');
    }

    function _setEvent(selector, context, text) {
        _init();
        let elem = $(selector, (context) ? context : null)[0];

        elem.onmouseenter = function () {
            _setText(text);
            _show();
        };

        elem.onmouseleave = function () {
            _hide();
        }

        elem.onmousemove = function () {
            _show();
        }
    }

    function _activateTooltip(selector) {
        let target = $(selector)
        if (target.length) {
            $(target).off("mouseenter").on("mouseenter", ".talk-tooltip", function (event) {
                if (isEllipsisActive($(event.target)[0])) {
                    let text = $(event.target).attr("data-tooltip");
                    if (!text)
                        text = $(event.target).text();

                    _setText(text);
                    _show();
                }
                else {
                    _setText("");
                    _hide();
                }
            });

            $(target).off("mouseleave").on("mouseleave", ".talk-tooltip", function (event) {
                _hide();
            });

            $(target).off("mousemove").on("mousemove", ".talk-tooltip", function (event) {
                if (isEllipsisActive($(event.target)[0])) {
                    _show();
                }
                else{
                    _hide();
                }
            });
        }
    }

    return {
        // setText : function(str){
        //     _setText(str);
        // },

        // setEvent : function(selector, context, text){
        //     _setEvent(selector, context, text);
        // },

        // show: function () {
        //     _show();
        // },

        // hide: function () {
        //     _hide();
        // },

        activateTooltip: function (selector) {
            _activateTooltip(selector);
        }
    }
})();
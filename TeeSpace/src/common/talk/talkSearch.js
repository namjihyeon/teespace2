const talkSearch = (function(){
	function isIncludeLink(targetMessage) {
        let regex = /<{"data-type":"url"(?:.*?)"data-url":"(.*?)"}>/g;
        let targetMsgBody= mobx.toJS(targetMessage["MSG_BODY"]);
        if(targetMsgBody == null || targetMsgBody == undefined
        		|| targetMsgBody == "") return false;
        return regex.test(targetMessage["MSG_BODY"]);
    }
    function isIncludeFile(targetMessage){
    	 let AttachmentList= mobx.toJS(targetMessage["ATTACHMENT_LIST"]);
         if(AttachmentList == null || AttachmentList == undefined) return false;
    	 const AttachmentListLength = AttachmentList.length;
    	if(AttachmentListLength==0) return false;
        //attachment가 있는 메시질 경우
        let i;
        for(i=0;i<AttachmentListLength;i++){
            let AttachmentType = AttachmentList[i]["ATTACHMENT_TYPE"];
            switch(AttachmentType){
            case "file":
            return true;
            case "fileList":
            return true; 
            default:
             break;
            }
           }
    	return false;
    }
	function _actingForFiltering(targetMessage,messageLayout){
		//////search category를 위해 추가한 부분 /////////
   	    //search category를 위한 css class이름 추가하는 과정
        //태그에 클래스 이름에 category이름을 넣어 (class="containsFile"), (class="containsURL")을 넣어서
        //태그만 봐도 file 메시지인지 url 메시지인지 확인 할 것이다.
        //TODO: 함수 정리
        //url도 없고 파일도 없는 메시지 체크
        let isPlain=true;
        //url인지 확인
        if(isIncludeLink(targetMessage)) {
        	if(messageLayout.getElementsByClassName('message__sub-menu-container').length!=0)
                messageLayout.classList.add("containsURL");
            if(talkData.curCategoryName == "파일") messageLayout.style["display"] ="none";
            isPlain=false;
        }
        //file인지 확인
        if(isIncludeFile(targetMessage)){
        	if(messageLayout.getElementsByClassName('message__sub-menu-container').length!=0)
                messageLayout.classList.add("containsFile");
        	if(talkData.curCategoryName == "링크") messageLayout.style["display"] ="none";
        	isPlain=false;
        }
        //위에 if문 두개중 하나도 안 걸렸으면 url도 없고 파일도 없는 pure 메시지
        if(isPlain){
        	if(talkData.curCategoryName == "파일" ||talkData.curCategoryName == "링크") 
          		messageLayout.style["display"] ="none";
        }
       ///////////////////////////////////////////
	}

    function _getMessagePostProcessForSearch(messageList, msgIdOfKeyword){
		// _getMessagePostProcess함수를 오마주 
	 	// _getMessagePostProcess함수와 전반적으로 같다.
		// isprepend가 true인 경우 밖에 없다. 
		// 하나씩 그리면서 내가 찾는 메시지 아이디와 같은지 확인한다.
		const messageCount = messageList.length;
		if (messageCount == 0 ) return false;
		let i,index,isSearched = false;
		// for (i = 0; i < messageCount; i++) {
		// 	messageList[i]["IS_HEAD"] = true;
		// 	messageList[i]["MSG_STATE"] = "success";
		// }
		for (index = messageCount - 1; index >= 0; index--) {
		   	// header를 넣을지 체크한 후 talkData.messageList에 최종적으로 넣는다
			//isprepend=true 로 항상 해준다. 메시지를 더 긁어오면서 검색할때는 위로 add하는 경우밖에 없다.
			// talkContent.addMessages(true, messageList[index]);
		 	//키워드가 같고 메시지 타입이 카테고리와 같은 경우
			// 내가 넣은 messageList[index] 메시지가 찾으려는 메시지의 id와 같다면
			// 플래그 켜주고 마저 넣기
			if(messageList[index]["MSG_ID"] === msgIdOfKeyword) {
				isSearched=true;break;
			}
		}
		talkContent.addMessages(true, messageList);
		return isSearched;
	}
	 //메시지 바디에서 하이라이팅을 해야할 지, 파일 타이틀에서 해야할지 결정하고
	 //TODO : 파일 묶음일때는 어디를 하이라이팅 해야할 까?

	 function _highlightKeyword(msgId){
		 if(talkData.curCategoryName == "전체"){
			 _highlightFile(msgId);
			 _highlightURL(msgId);
			 _highlightMsg(msgId);
		 }
		 else if(talkData.curCategoryName == "파일"){
			 _highlightFile(msgId);
		 }
		 else if(talkData.curCategoryName == "링크"){
			 _highlightURL(msgId);
		 }
	 }
	 
	 function _highlightFile(msgId){
		 let content =null;
		 content = document.querySelector("[data-message-id='" + msgId + "'] " +
		 ".file-message_title");
		 if(content == null || content == undefined) return;
		 //let contentInnerHTML = content.innerHTML;
		 let contentFirstChild = content.firstElementChild;
		 let contentSecondChild = content.lastElementChild;
		 let joinedContentTxt = contentFirstChild.innerText + contentSecondChild.innerText;
		 content.removeChild(contentFirstChild);
		 content.removeChild(contentSecondChild);
		 content.innerText = joinedContentTxt;
		 
		 //TODO: css 클래스를 이용해 정리
		 //파일이름을 표현하는 기획 추가될 예정 --> 추가되면 TODO,,
		 let originString = content.innerText;
		 let noHighlightSecs = originString.split(talkData.curSearchKeyword);
		 let i;
		 content.innerHTML=`<span style="white-space:nowrap;overflow:hidden;font-size:0.688rem">${noHighlightSecs[0]}</span>`;
		 for(i=1;i<noHighlightSecs.length;i++){
			 let tmpElemInfo = document.createElement('span');
			 let tmp = document.createElement('span');
			 tmpElemInfo.style["background"]='yellow';
			 tmpElemInfo.style["white-space"] = "nowrap";
			 tmpElemInfo.style["height"]="fit-content";
			 tmpElemInfo.style["font-size"]="0.688rem";
			 tmp.style["font-size"]="0.688rem";
			 tmp.style["overflow"]="hidden";
			 tmp.style["text-overflow"]="ellipsis";
			 tmp.style["white-space"] = "nowrap";
			 tmp.style["height"]="fit-content";
			 tmpElemInfo.innerText=talkData.curSearchKeyword;
			 tmp.innerText=noHighlightSecs[i];
			 content.appendChild(tmpElemInfo);
			 content.appendChild(tmp);
		 }
		 setTimeout(()=> {
			 $(document).one("click", {content:content}, function(event) {
				 // TODO:SPAN 벗겨내기
				 let fileTitle = content.innerText;
				 let fileTitleArray = fileTitle.split('\n').join('').split('.');
				 let fileTitleArrayLength = fileTitleArray.length;
				 let fileExtension = "." + fileTitleArray[fileTitleArrayLength-1];
				 fileTitleArray.splice(fileTitleArrayLength-1,1);
				 let FileName=fileTitleArray.join('');
				 
				 var span=content.getElementsByTagName('span'); // get the span
				 let spanCount = span.length;
				 var pa=span[0].parentNode;
				 while(span[0] != undefined) pa.removeChild(span[0]);
				 let spanFileName = document.createElement('span');
				 spanFileName.setAttribute('class','file-message__file-name talk-tooltip');
				 spanFileName.innerText=FileName;
				 content.appendChild(spanFileName);
				 let spanFileExt = document.createElement('span');
				 spanFileExt.setAttribute('class','file-message__file-extension');
				 spanFileExt.innerText=fileExtension; 
				 content.appendChild(spanFileExt);
			 })
		 }, 10)
	 }

	 function _highlightURL(msgId){
		 let content =null;
		 let contentChild=null;
		 let originString=new Array();
		 content = document.querySelector("[data-message-id='" + msgId + "'] " +
		 ".message__content-body__content");
		 // TODO: 코드 정리해야합니다.
		 // TODO: 원래 메시지에 DIV가 있는 지 없는 지 체크하는 변수가 ㅇ ㅣㅆ어야한다.
		 if(content == null || content == undefined) return;
		 contentChild = content.getElementsByTagName('a');
		 if(contentChild.length==0) return;
		 const contentChildLength = contentChild.length;
		 let idx;
		 for(idx=0;idx<contentChildLength;idx++){
			 originString[idx] = contentChild[idx].innerText;
			 let noHighlightSecs = originString[idx].split(talkData.curSearchKeyword);
			 console.log(noHighlightSecs);
			 let i;
			 let noYellowSpan = document.createElement('span');
			 noYellowSpan.innerText = noHighlightSecs[0];
			 contentChild[idx].innerHTML='';
			 contentChild[idx].appendChild(noYellowSpan);
			 for(i=1;i<noHighlightSecs.length;i++){
				let yellowSpan = document.createElement('span');
				 noYellowSpan = document.createElement('span');
				 yellowSpan.style["background"]='yellow';
				 yellowSpan.innerText=talkData.curSearchKeyword;
				 noYellowSpan.innerText=noHighlightSecs[i];
				 contentChild[idx].appendChild(yellowSpan);
				 contentChild[idx].appendChild(noYellowSpan);
			 }
		 }
		 setTimeout(()=> {
			 $(document).one("click", {contentChild:contentChild}, function(event) {
				 // TODO:SPAN 벗겨내기
				 let idx;
				 for(idx=0;idx<contentChild.length;idx++){
					 if(contentChild[idx]["nodeName"] != 'A') continue;
					 var span=contentChild[idx].getElementsByTagName('span'); // get the span
					 let spanCount = span.length;
					 while(span[0] != undefined)   contentChild[idx].removeChild(span[0]);
					 contentChild[idx].innerText = originString[idx];
				 }
			 })
		 }, 10)
	 }
 
	 function _highlightMsg(msgId){
		 let content =null;
		 let contentChild=null;
		 let originString=new Array();
		 content = document.querySelector("[data-message-id='" + msgId + "'] " +
		 ".message__content-body__content");
		 // TODO: 코드 정리해야합니다.
		 // TODO: 원래 메시지에 DIV가 있는 지 없는 지 체크하는 변수가 ㅇ ㅣㅆ어야한다.
		 if(content == null || content == undefined) return;
		 contentChild = content.childNodes;
		 if(contentChild.length==0) return;
		 let idx;
		 for(idx=0;idx<contentChild.length;idx++){
			 if(contentChild[idx]["nodeName"] != '#text' && contentChild[idx]["nodeName"] != 'DIV' 
			 && contentChild[idx]["nodeName"] != 'P' && contentChild[idx]["nodeName"] != 'SPAN' ) continue;
			 ///wrap the div tag
			 if(contentChild[idx]["nodeName"] == '#text'){
				 let divElem = document.createElement('div');
				 divElem.innerText = contentChild[idx].nodeValue;
				 divElem.style["display"]="inline-block";
				 contentChild[idx].parentNode.insertBefore(divElem,contentChild[idx]);
				 contentChild[idx+1].remove();
			 }
			 originString[idx] = contentChild[idx].innerText;
			 let noHighlightSecs = originString[idx].split(talkData.curSearchKeyword);
			 let i;
			 let noYellowSpan = document.createElement('span');
			 noYellowSpan.innerText = noHighlightSecs[0];
			 contentChild[idx].innerHTML='';
			 contentChild[idx].appendChild(noYellowSpan);
			 for(i=1;i<noHighlightSecs.length;i++){
				 let yellowSpan = document.createElement('span');
				 noYellowSpan = document.createElement('span');
				 yellowSpan.style["background"]='yellow';
				 yellowSpan.innerText=talkData.curSearchKeyword;
				 noYellowSpan.innerText=noHighlightSecs[i];
				 contentChild[idx].appendChild(yellowSpan);
				 contentChild[idx].appendChild(noYellowSpan);
			 }
		 }
		 setTimeout(()=> {
			 $(document).one("click", {contentChild:contentChild}, function(event) {
				 let idx;
				 for(idx=0;idx<contentChild.length;idx++){
					 if(contentChild[idx]["nodeName"] != 'DIV' && contentChild[idx]["nodeName"] != 'P'
					 &&contentChild[idx]["nodeName"] != 'SPAN') continue;
					 var span=contentChild[idx].getElementsByTagName('span'); // get the span
					 let spanCount = span.length;
					 while(span[0] != undefined) 
						 contentChild[idx].removeChild(span[0]);
					 contentChild[idx].innerText = originString[idx];
					 //div없애기
					 /*pa=contentChild[idx].parentNode;
					pa.insertBefore(contentChild[idx].firstChild, contentChild[idx]);
											 content.remove();*/
				 }
			 })
		 }, 10)
	 }
	 /////////////////////////////////
	async function _getMoreMessagesForSearch(msgIdOfKeyword, isFlag){
		let firstMessageId = 'none';
	 //    // 검색어가 서치가 되었는지 아닌지를 확인하는 플래그
		 let isSearched = isFlag;
		
		//계속 찾아야한다면 서버에서 메시지 긁어온다.
		// 그 시간을 ttalk_getMessagList의 두번째 인자로 넘겨주면
		// 그 시간보다 이전 메시지 20개를 긁어온다.
		while(isSearched != true){
		 try{
		 // 가지고 있는 메시지 리스트에서 위에서 첫번째 메시지의 시간을 가져온다.
		//  if (talkData.messageList.length) {
			//  firstMessageId= talkData.messageList[0]["MSG_ID"];
			if (talkData.messageList.length) {
				for(let i=0;i<talkData.messageList.length;i++){
					if(talkData.messageList[i]["MSG_ID"]){ 
						firstMessageId =talkData.messageList[i]["MSG_ID"];
						break;
					}
				}
		  }
		const res = await talkServer2.ttalk_getMessageList(firstMessageId,20);
		 
		 // 20개 혹은 그보다 적게(해당문자열을 발견했을 경우)를 다 post하고 나서 메시지를 더 불러와야하는
		 // 지 해야하는 지 그만 해야하는 지 판단,             
		 isSearched =
					   _getMessagePostProcessForSearch(res.data.dto["MSG_LIST"],msgIdOfKeyword);
		 
		 talkContent.updateUnreadCount();		 
		  //내가 받아와서 그린 메시지가 이게 끝일때
		  // 서버에서 더 받아올 게 없을 때, 함수 종료	 
		 if (!res.data.dto["HAS_MORE"]) {
			 let container = $('#talk-content')[0];
			 $(container).scrollTop(1);
			 container.removeEventListener('scroll', talkContent.infiniteScroll);
			 break;
		 }
 
		 } catch(e){
			// let container = $('#talk-content')[0];
			// $(container).scrollTop(1);
			console.log("getMoreMessageForSearch Error!");
			break;
			 }
		 }
		 return isSearched;
	  }
	
	async function _searchKeywords(index){
		   //console.log(keywords);
		//서버에서 받은 검색 문자열에 해당하는 메시지 리스트에서 몇번째를 찾을 것인가를 나타내는 인덱스 변수
		let IdxOfSearchedMsg = index;
		let searchedMsgList =  talkData.searchedMsgList;
		let keyword = talkData.curSearchKeyword;
		//암것도 안하고 엔터치면 검색 안할꺼야
		//1글자 이상 입력하라고 경고창 띄움
		if(keyword === ''){
			TeeToast.open({text:"최소 한 글자 이상 입력하세요. "});
			//TODO: 검색창 초기화 함수를 따로 만들면 좋겠다는 생각이 든다.
			talkData.searchedMsg = null;
			talkData. IdxOfsearchedMsg=0;
			talkRenderer.component.hideArrowButtons();
			talkRenderer.component.hidecurIdxOfMsg();
			return;
		}
		
		//서버에서 searchedMsgList 긁어온다. 유형별로
		//&& keywords !== null
		if(searchedMsgList===null){
			try{
				switch(talkData.curCategoryName){
				case "파일":
					searchedMsgList = talkServer2.searchMessage(talkData.talkId, keyword, "file");
					break;
				case "링크":
					searchedMsgList = talkServer2.searchMessage(talkData.talkId, keyword, "url");
					break;
				case "전체":
					searchedMsgList = talkServer2.searchMessage(talkData.talkId, keyword, "all");
					break;
				}
			searchedMsgListLength=searchedMsgList.length;
		 }catch(e){
			 console.log("searchMessage Error!");
			 return;
		 }
		} 
		//해당 문자열과 같은 문자열이 전체에서 없을 때
		//즉 서버에서 받아온 검색어와 일치하는 메시지가 없을 때
		   if(searchedMsgListLength == 0){
			   //화살표 지워줘야지~ 찾은 메시지 리스트가 없으니 네비게이션할 필요도 없쥬?
			   talkRenderer.component.hideArrowButtons();
			   talkRenderer.component.hidecurIdxOfMsg();
			   //TODO:없을 때 어떻게 해줄지에 대한 추후 정책 필요
			   TeeToast.open({text: "'"+talkData.curSearchKeyword+"'" + " 검색 결과가 없습니다."});
			   return;
		   }
		   
		//위에서 체크해줘서
		//서버에서 받아온 검색어와 일치하는 메시지가 없지 않을 때이므로
		// 있을 때 arrow up / down button 보이게 한다. 
		let i, msgId=null;
		let curMsgList = talkData.messageList;
		let curMsgListLength= talkData.messageList.length;
	  
		//현재의 talkData.messagList에 있는지 없는지 확인하기 위한 플래그
		//이 플래그로 더 긁어올것인지 아니면 찾아서 gotomessage함수 호출할 것인지 판단할 것이다. 
		let isInCurMsgList = false;
		let isSearched = false;
		
		//TODO : 다 안 뒤져도 될 것 같은?? 최적화 좀 더 가능할 것 같은 느낌
		//최근 메시지부터 뒤질꺼기때문에 idx뒤에서부터 본다.
		for(i=curMsgListLength-1;i>=0;i--){
			   msgId= curMsgList[i]["MSG_ID"];
			if(msgId === searchedMsgList[IdxOfSearchedMsg]["MSG_ID"]){
				//TODO: talkreply참고,, 그 쪽지로 타고 가야한다. ---> gotomessage 함수 이용하면 된다.
				isInCurMsgList=true; break;
			   }
		}
		if(isInCurMsgList!==true) 
			 isSearched = await _getMoreMessagesForSearch(searchedMsgList[IdxOfSearchedMsg]["MSG_ID"], false); // 메시지를 더
		 // 불러와서 찾는다. 
		 
		if(isInCurMsgList == true || isSearched == true){
			
		try{
		 let totalKeywords = searchedMsgList.length; 
		 //메시지IDX와 네비게이션화살표 출력
		 talkRenderer.component.showArrowButtons();
		 talkRenderer.component.showIdxOfMsg(IdxOfSearchedMsg,totalKeywords);   
		 talkRenderer.component.showcurIdxOfMsg();
		//스크롤을 그 메시지에 가져다 놓는다. 
		setTimeout(
			talkContent.scrollToMsg(searchedMsgList[IdxOfSearchedMsg]["MSG_ID"])
			,0);
		
		_highlightKeyword(searchedMsgList[IdxOfSearchedMsg]["MSG_ID"]);
		
		//현재 이 스코프에서 가지고 있는 정보와 talkData에서 가지고 있는 정보가 다를 수 있으므로 최신화
		//다음 호출되었을 때 오류 발생을 방지하기 위해
		talkData.searchedMsgList = searchedMsgList;
		talkData.IdxOfSearchedMsg = IdxOfSearchedMsg;
		talkData.curSearchKeyword = keyword;
	 }catch(e){console.log("searchedMsgList is null !!")};   
	 }
		else{
		 talkContent.scrollToMsg(talkData.searchedMsgList[IdxOfSearchedMsg-1]["MSG_ID"]);
		}
	}
	 //////////////////////////////////////////////////////////////////////
	
	//TODO: msgFilteredAll/File/Link 함수 합치거나 정리하기
	//filtered 된것과 filtered해체(현재  msgfilteredall함수) 함수로 나눌것?
	function _msgFilteredAll(){
		let idx,i, curMsgClassList;
        //현재 앞단에서 가지고 있는 메시지 리스트
        const curMsgList = mobx.toJS(talkData.messageList);
        const curMsgListLength = curMsgList.length;
        
        //내가 갖고 있는 메시지를 다 돌면서
        //MSG_TYPE인 timeLine인 것은 타임라인이므로 건들지 않고
        //모든 메시지의 display를 다시 보이게 해준다. ""도 좋고 "flex"도 좋고 
		let messageLayout=null;
		let targetMessage=null;
		for(idx=0;idx<curMsgListLength;idx++){
				if(curMsgList[idx]["MSG_TYPE"] != "create") continue;
				messageLayout = document.querySelector("[data-message-id='" + curMsgList[idx]["MSG_ID"] + "']")
				targetMessage = curMsgList[idx];
				if (!targetMessage["MSG_BODY"] && !targetMessage["ATTACHMENT_LIST"].length) 
					messageLayout.style.display = "none";
				else messageLayout.style.display = "";
        }
        
        //지워진 타임라인을 다시 생성하는 과정
        //timeLine 클래스를 가지고 있는 것을 다 모아가지고 와서
        const timeLineMsgList = document.querySelectorAll(".content__complete-message-container .timeLine");
        const timeLineMsgListLength = timeLineMsgList.length;
        
        //다뒤지면서 display를 보여주게 한다. 
        for(i=0;i<timeLineMsgListLength;i++){
			timeLineMsgList[i].style["display"]="";
		}
        
	}
	function _msgFilteredFile(){
		let idx,i, hasFile, numOfFile=0;
    	//앞단에서 가지고 있는 메시지 리스트
        const curMsgList = mobx.toJS(talkData.messageList);
        const curMsgListLength = curMsgList.length;
        //containsURL의 클래스를 가진 태그들을 싹다 모은다.
        //querySelectorAll로 그러면 배열로 반환한다
            
        //TODO: 이 과정을 더 빠르게 할 수 있을 것 같긴하다.
        //fileMsgList를 정렬하고 curMsgList와 같이 찾는 식으로 하면? 
        //우선은 나이브하게 ㄱㄱ  
        const FileMsgList = document.querySelectorAll('.content__complete-message-container .containsFile');
        const FileMsgListLength = FileMsgList.length;
        
        let numFilesInOneDay=0; //하루에 file이 몇개인지 세기
        for(idx=curMsgListLength-1;idx>=0;idx--){
        	
        	//내가 가지고 있는 메시지들을 다 돌면서 
        	//LinkMsgList에 있는 메시지 아이디와 같은 것이 있는 지 확인한ㄷ,. 
        	hasFile=false; //디폴트 값은 없다. 
            //TODO: file이 하나도 없던 날의 타임라인은 출력하면 안된다. 
            //반면 file이 있던 날의 타임라인은 출력한다. 
            //현재는 모든 타임라인을  출력한다.
            if(curMsgList[idx]["MSG_TYPE"] == "timeLine") {
            	//하루에 file이 하나도 없다면? timeline 출력 ㄴㄴ
            	if(numFilesInOneDay >0) {
            		numFilesInOneDay=0; continue;
            	}
            	//TODO: 여기서 display none을 해주는데 안먹는다....
            	/*else curMsgList[idx]["style"]="display:none;";*/
            	/*else document.querySelector("[CREATE_TIME='"+curMsgList[idx]["CREATE_TIME"]+"']")
            														.style["display"]="none";*/
            }
            //내가 현재 보고 있는 메시지가
            //타임라인이 아니라 일반 메시지라면,,
            else if(curMsgList[idx]["MSG_TYPE"] == "create"){
            	for(i=0;i<FileMsgListLength;i++){
            		//LinkMsgList의 구조에 "dataset"-->"messageId"에/
            		//file을 가지고 있는 메시지의 아이디가 들어있clo다.
            		if(FileMsgList[i]["dataset"]["messageId"] == curMsgList[idx]["MSG_ID"]) {
            			hasFile=true;numFilesInOneDay++;break;
            		}
				}
				let messageLayout = document.querySelector("[data-message-id='" + curMsgList[idx]["MSG_ID"] + "']");
            	if(hasFile==false)  
				messageLayout.style["display"]="none";
            	else{
					// if(curMsgList[idx]["ATTACHMENT_LIST"]==undefined) messageLayout.style["display"]="none";
					// else
					messageLayout.style["display"]="";
				}
			}
        }
	}
	function _msgFilteredLink(){
		let idx,i, curMsgClassList,hasURL, numOfURL=0;
    	//앞단에서 가지고 있는 메시지 리스트
        /*const curMsgList = mobx.toJS(talkData.messageList);*/
    	const curMsgList = mobx.toJS(talkData.messageList);
        const curMsgListLength = curMsgList.length;
        //containsURL의 클래스를 가진 태그들을 싹다 모은다.
        //querySelectorAll로 그러면 배열로 반환한다
            
        //TODO: 이 과정을 더 빠르게 할 수 있을 것 같긴하다.
        //LinkMsgList를 정렬하고 curMsgList와 같이 찾는 식으로 하면? 
        //우선은 나이브하게 ㄱㄱ  
        const LinkMsgList = document.querySelectorAll('.content__complete-message-container .containsURL');
        const LinkMsgListLength = LinkMsgList.length;
        
        
        let numURLsInOneDay=0; //하루에 url이 몇개인지 세기
        for(idx=curMsgListLength-1;idx>=0;idx--){   	
        	//내가 가지고 있는 메시지들을 다 돌면서 
        	//LinkMsgList에 있는 메시지 아이디와 같은 것이 있는 지 확인한ㄷ,. 
        	hasURL=false; //디폴트 값은 없다. 
            //TODO: URL이 하나도 없던 날의 타임라인은 출력하면 안된다. 
            //반면 URL이 있던 날의 타임라인은 출력한다. 
            //현재는 모든 타임라인을  출력한다.
            if(curMsgList[idx]["MSG_TYPE"] == "timeLine") {
            	//하루에 url이 하나도 없다면? timeline 출력 ㄴㄴ
            	if(numURLsInOneDay >0) {
            		numURLsInOneDay=0; continue;
            	}
            	//TODO: 여기서 display none을 해주는데 안먹는다....
            	/*else curMsgList[idx]["style"]="display:none;";*/
            	/*else document.querySelector("[CREATE_TIME='"+curMsgList[idx]["CREATE_TIME"]+"']")
				.style["display"]="none";*/
            }
            
			else if(curMsgList[idx]["MSG_TYPE"] == "create"){
            	for(i=0;i<LinkMsgListLength;i++){
            		//LinkMsgList의 구조에 "dataset"-->"messageId"에/
            		//URL을 가지고 있는 메시지의 아이디가 들어있다.
            		if(LinkMsgList[i]["dataset"]["messageId"] == curMsgList[idx]["MSG_ID"]) {
            			hasURL=true;numURLsInOneDay++;break;
            		}
            	}
            	if(hasURL==false)  
            		document.querySelector("[data-message-id='" + curMsgList[idx]["MSG_ID"] + "']").style["display"]="none";
            	else
            		document.querySelector("[data-message-id='" + curMsgList[idx]["MSG_ID"] + "']").style["display"]="";
            }
        }
	}

	////////////////////////////////////////////////////////////////////////////
	//검색창에서 엔터를 눌렀을 때 핸들링하는 과정
	function _keydownEvent(e){
		//빈칸일때 아무것도 안하게 해놨는데 그게 아니라 에러창 띄워야 한다.
		//그래서 searchKeywords함수 타게 놔둔다.
		// && document.getElementById('talk__Content-search-input').value!==''
		if(e.key === 'Enter'){
			
			//엔터를 누를 시에
			//이전에 검색한 적이 있다면
			//현재 하이라이팅 중인 메시지를 하이라이팅을 없애는 초기화 작업을 해야한다. 
			if(talkData.searchedMsgList!=null ){
				let curMsg = talkData.searchedMsgList[talkData.IdxOfSearchedMsg];
				let msgId =curMsg["MSG_ID"];
				//현재 메시지가 파일이라면
				let content = document.querySelector("[data-message-id='" + msgId + "'] " +
				".file-message_title");
				if(content !=null){
					let fileTitle = content.innerText;
                	let fileTitleArray = fileTitle.split('\n').join('').split('.');
                	let fileTitleArrayLength = fileTitleArray.length;
                	let fileExtension = "." + fileTitleArray[fileTitleArrayLength-1];
                	fileTitleArray.splice(fileTitleArrayLength-1,1);
                	let FileName=fileTitleArray.join('');
                
                	var span=content.getElementsByTagName('span'); // get the span
                	let spanCount = span.length;
                	var pa=span[0].parentNode;
                	while(span[0] != undefined) pa.removeChild(span[0]);
                	let spanFileName = document.createElement('span');
                	spanFileName.setAttribute('class','file-message__file-name talk-tooltip');
                	spanFileName.innerText=FileName;
                	content.appendChild(spanFileName);
                	let spanFileExt = document.createElement('span');
                	spanFileExt.setAttribute('class','file-message__file-extension');
                	spanFileExt.innerText=fileExtension; 
					content.appendChild(spanFileExt);
				}

				content = content = document.querySelector("[data-message-id='" + msgId + "'] " +
				".message__content-body__content");
				//파일이 아닌 다른 url, 메시지라면
				if(content !=null){
					let contentChild = content.childNodes;
					let originString=new Array();
					let idx;
					for(idx=0;idx<contentChild.length;idx++){
						if(contentChild[idx]["nodeName"] != 'A' && contentChild[idx]["nodeName"] != 'DIV') continue;
						originString[idx] = contentChild[idx].innerText;
						var span=contentChild[idx].getElementsByTagName('span'); // get the span
						let spanCount = span.length;
						while(span[0] != undefined)   contentChild[idx].removeChild(span[0]);
						contentChild[idx].innerText = originString[idx];
					}
				}
			}
			
			//엔터버튼으로 누르면 처음부터 다시 검색하게 하려고 한다. 
			//그래서 talkData의 search관련된 정보들을 초기화한다. 
			//TODO: 나중에 따로 함수로 만들어야겠다. 
			talkData.searchedMsgList=null;
			talkData.curSearchKeyword='';
			talkData.IdxOfSearchedMsg=0;
			const keyword = document.getElementById('talk__Content-search-input').value;
			talkData.curSearchKeyword=keyword;
			//keywords를 넘겨주면 찾는다. content부분에서 수행하므로 talkContent.js에 로직을 넣는다. 
			talkSearch.searchKeywords(0);
			return;
		}
	}


	return {
		msgFilteredAll : function(){
			_msgFilteredAll();
		},
		msgFilteredFile:function(){
			_msgFilteredFile();
		},
		msgFilteredLink:function(){
			_msgFilteredLink();
		},
		keydownEvent: function(e){
			_keydownEvent(e);
		},
		searchKeywords: function(keywords, index){
        	_searchKeywords(keywords, index);
		},
		actingForFiltering: function(targetMessage, messageLayout){
			_actingForFiltering(targetMessage,messageLayout);
		}
	}
})();
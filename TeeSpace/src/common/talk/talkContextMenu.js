// mobx.observable(
const talkContextMenu = (function () {
    // 출력될 텍스트, 클릭시 이벤트
    // {"text" : "알람 끄기", "onclick" : talk.onAlarmOff }
    // {"text" : "알람 켜기", "onclick" : talk.onAlarmOn }
    // {"text" : "테마 변경", "onclick" : talk.changeTheme }
    // let _exceptCloseElement = [];
    let _afterClose = null;
    let _beforeOpen = null;
    
    let renderData = [
        { "text": "알람 끄기", "onclick": function () { } }, // console.warn("알람 끄기") } },
        { "text": "알람 켜기", "onclick": function () { } }, // console.warn("알람 켜기") } },
        { "text": "테마 변경", "onclick": function () { } } // console.warn("테마 변경") } }
    ];

    function _render(target, pos) {
        if (!renderData.length)
            return;

        if (_isOpen())
            _close();

        if (_beforeOpen && typeof _beforeOpen === "function")
            _beforeOpen();
        
        let contextMenu = $(".context-menu", $(target));
        
        if (!contextMenu.length) {
            contextMenu = $("<div/>").addClass("context-menu").appendTo($(target));
        }
        
        let length = renderData.length;
        let frag = $(document.createDocumentFragment());

        for (let i = 0; i < length; i++) {
            let item = $("<div/>")
                // .attr({ "data-group": "alarm" })
                .text(renderData[i].text);


            if (renderData[i]["disabled"]) {
                item.addClass("context-menu__item--disabled");
            }
            else {
                item.addClass("context-menu__item").on("click", function () {
                    renderData[i].onclick();
                    _close();
                });
            }

            item.appendTo(frag);
            // if (renderData[i]["group"] && $("[data-group='" + renderData[i]["group"] + "']", frag).length)
            //     item.hide();
            // item.appendTo(frag);
        }

        contextMenu.css({ "position" : "absolute", "top": "", "bottom": "", "left": "", "right": "" });

        // 방향 판별
        contextMenu
            .css(pos)
            .append(frag)
            .show();

        contextMenu.after(
            $("<div/>")
                .addClass("context-menu__background")
                .on("click", function () {
                    _close();
                })
        );
        // $(document).off("click", _closeFunction).on("click", _closeFunction);
    }

    // function _closeFunction() {
    //     let length = _exceptCloseElement.length;
    //     for (let i = 0; i < length; i++) {
    //         if (!$(event.target).closest(_exceptCloseElement[i]).length) {
    //             _close();
    //             if (_afterClose && typeof _afterClose === "function")
    //                 _afterClose();
    //             break;
    //         }
    //     }
    // }

    function _isOpen() {
        return $(".context-menu").is(":visible");
    }

    function _close() {
        $(".context-menu").empty();
        $(".context-menu").hide();
        $(".context-menu__background").remove();
        
        if (_afterClose && typeof _afterClose === "function")
            _afterClose();
        	
    	_afterClose = null;
    	_beforeOpen = null;
    }

    return {
        setData: function (data) {
            if (data)
                renderData = data;
        },
        // target relative 여야 함
        // render: function (data, target, pos, exceptCloseElement, afterClose) {
        render: function (data, target, pos, afterClose, beforeOpen) {
            // _exceptCloseElement = exceptCloseElement;

            _afterClose = afterClose;
            _beforeOpen = beforeOpen;

            if (data)
                this.setData(data);

            _render(target, pos);
        }
    }
})();

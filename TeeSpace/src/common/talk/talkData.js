// mobx.observable(
const talkData = {
    isTSFocused : false,

    selectedRoomId: mobx.observable.box(),
    messageList: mobx.observable.array(),
    tempMessageList: mobx.observable.array(),
    userInfoList: mobx.observable.map(),
    selectedEmoticonTag: mobx.observable.box(),
    selectedReply: mobx.observable.box(),
    responseTimeLimit: 20000,
    messengerList: mobx.observable.array(),
    lastReadMsgId : "",
    
    // room create, 유저 초대 팝업용
    includedUserList: mobx.observable.map(),
    selectedUserList: mobx.observable.map(),

    // 채팅방 파일 업로드 팝업
    successFileList: mobx.observable.array(),
    fileChooserList: mobx.observable.array(),
    
    // 다른 앱에서 채팅방으로 파일 보내기
    fileMetaList: mobx.observable.array(),

    // 관찰 필요없는 값들 [라우팅에서 init하도록 수정]
    serverURL: "",

    // 타이핑 중인 사람
    typingUserList: mobx.observable.map(),

    // 프로필 바뀌면
    updatedUserId: mobx.observable.box(),

    // 제비뽑기 참가 유저 리스트
    gameUserList : mobx.observable.array(),

    workspaceId: '',
    talkId: '',
    myInfo: {},
    isSimple: false,
    isText : false,
    // initMobx 여러번 타지 않도록 넣어줌
    isInit : true,
    roomInfo: '', 
    
    
    //대화창 검색을 위한 정보 저장하는 곳
    ///////////////////////////////////////
    //해당검색에 대한 문자열 리스트 
    searchedMsgList: null,
    
    //내가 지금 보고있는 메시지가 몇번째인지
    IdxOfsearchedMsg: 0,
    
    //현재 카테고리가 어떤 것인지
    //전체인지 링크인지 파일인지를  저장하는 변수
    curCategoryName: "전체",
    curSearchKeyword: '',
    ///////////////////////////////////////
}

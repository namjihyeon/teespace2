function generateQueryStr(obj) {
  let str = '';
  for (let key in obj) {
    str += `${key}=${obj[key]}&`;
  }
  str = str.substr(0, str.length - 1); // 마지막 &를 제거
  return str;
}
const calendarCommonData = {
  holidayVisibility: true,
  mycalendarVisibility: true,
  getTopCalendarWidget() {
    return tds('calendar') ? tds('calendar') : null;
  },
  isChangeRepeatFlag: false,
  isRepeatedEvent: false,
  isEventEditMode: false,
  isEditData: false,
  isSelectedApplyOption: false,
  isRepeatedScheduleUpdateMode: false,
  selectedRepeatedOption: 0,
  dateRange: {}
};

const calendarAPI = {
  url: `${_workspace.url}Calendar`,
  getEventList(successCallback) {
    const queryObj = {
      workspaceID: workspaceManager.getWorkspaceId(getRoomIdByUrl()),
      calendarID: workspaceManager.getChannelList(workspaceManager.getWorkspaceId(getRoomIdByUrl()), TEESPACE_CHANNELS.TSCHEDULE.CHANNEL_TYPE),
      userID: userManager.getLoginUserId(),
      startDate: calendarCommonData.getTopCalendarWidget().getRange()[0],
      endDate: calendarCommonData.getTopCalendarWidget().getRange()[1]
    };
    Top.Ajax.execute({
      cache: false,
      type: 'GET',
      url: `${this.url}/TaskList?${generateQueryStr(queryObj)}`,
      dataType: 'json',
      crossDomain: true,
      contentType: 'application/json; charset=utf-8',
      xhrFields: { withCredentials: true },
      success: function (response, xhr, status) {
        typeof successCallback === 'function' && successCallback(response.dto);
      },
      error: function (ret, xhr, status) {},
      complete: function (ret, xhr, status) {
        const workspaceID = workspaceManager.getWorkspaceId(getRoomIdByUrl());
        const calendarType = sessionStorage.getItem(`calendarType_${workspaceID}`);
        if (calendarType === 'week' || calendarType === 'day') {
          // week or day인 경우 현재 시간에 맞게 스크롤 위치 조정
          const currentHour = new Date().getHours();
          document.getElementsByClassName('top-calendar-body-bottom')[0].scroll({
            behavior: 'smooth',
            left: 0,
            top: 44 * (currentHour - 8)
          });
        }
      }
    });
  },
  getEventDetail(taskID, startDate, successCallback) {
    const queryObj = {
      workspaceID: workspaceManager.getWorkspaceId(getRoomIdByUrl()),
      calendarID: workspaceManager.getChannelList(workspaceManager.getWorkspaceId(getRoomIdByUrl()), TEESPACE_CHANNELS.TSCHEDULE.CHANNEL_TYPE),
      userID: userManager.getLoginUserId(),
      taskID: taskID,
      startDate: startDate
    };
    Top.Ajax.execute({
      cache: false,
      type: 'GET',
      url: `${this.url}/Event?${generateQueryStr(queryObj)}`,
      dataType: 'json',
      crossDomain: true,
      contentType: 'application/json; charset=utf-8',
      xhrFields: {
        withCredentials: true
      },
      success: function (response, xhr, status) {
        typeof successCallback === 'function' && successCallback(response.dto.eventInfo);
      },
      complete: function (ret, xhr, status) {},
      error: function (ret, xhr, status) {}
    });
  },
  getEventListByFilter(filterInfo, successCallback) {
    const data = {
      header: {
        DATA_TYPE: 'J',
        service: 'CMS.Calendar.CalendarFilterUpdate'
      },
      dto: {
        calendar_id: workspaceManager.getChannelList(workspaceManager.getWorkspaceId(getRoomIdByUrl()), TEESPACE_CHANNELS.TSCHEDULE.CHANNEL_TYPE),
        filter_list: filterInfo,
        USER_ID: userManager.getLoginUserId(),
        startDate: calendarCommonData.getTopCalendarWidget().getRange()[0],
        endDate: calendarCommonData.getTopCalendarWidget().getRange()[1]
      }
    };
    Top.Ajax.execute({
      cache: false,
      type: 'POST',
      url: `${this.url}/Calendar?action=FilterUpdate`,
      dataType: 'json',
      data: JSON.stringify(data),
      crossDomain: true,
      contentType: 'application/json; charset=utf-8',
      xhrFields: {
        withCredentials: true
      },
      success: function (response, xhr, status) {
        typeof successCallback === 'function' && successCallback(response.dto.eventInfo);
      },
      complete: function (ret, xhr, status) {},
      error: function (ret, xhr, status) {}
    });
  },
  createEvent(eventInfo, successCallback) {
    spaceAPI.spaceInit();
    const data = {
      header: {
        DATA_TYPE: 'J',
        service: 'CMS.Calendar.EventCreate'
      },
      dto: {
        workspaceID: workspaceManager.getWorkspaceId(getRoomIdByUrl()),
        calendarID: workspaceManager.getChannelList(workspaceManager.getWorkspaceId(getRoomIdByUrl()), TEESPACE_CHANNELS.TSCHEDULE.CHANNEL_TYPE),
        userID: userManager.getLoginUserId(),
        eventInfo
      }
    };
    Top.Ajax.execute({
      cache: false,
      type: 'POST',
      url: `${this.url}/Event`,
      dataType: 'json',
      data: JSON.stringify(data),
      crossDomain: true,
      contentType: 'application/json; charset=utf-8',
      xhrFields: {
        withCredentials: true
      },
      success: function (response, xhr, status) {
        typeof successCallback === 'function' && successCallback();
      },
      complete: function (ret, xhr, status) {
        // notiFeedback('이벤트를 생성하였습니다.');
      },
      error: function (ret, xhr, status) {}
    });
  },
  updateEvent(eventInfo, params, successCallback) {
    console.debug('updateEvent');
    const data = {
      header: {
        DATA_TYPE: 'J',
        service: 'CMS.Calendar.EventUpdate'
      },
      dto: {
        workspaceID: workspaceManager.getWorkspaceId(getRoomIdByUrl()),
        calendarID: workspaceManager.getChannelList(workspaceManager.getWorkspaceId(getRoomIdByUrl()), TEESPACE_CHANNELS.TSCHEDULE.CHANNEL_TYPE),
        userID: userManager.getLoginUserId(),
        eventInfo,
        taskID: params.taskID,
        oldStartDate: params.oldStartDate,
        oldEndDate: params.oldEndDate,
        repeatUpdateOption: params.repeatUpdateOption
      }
    };
    console.debug('updateEvent data', data);
    Top.Ajax.execute({
      cache: false,
      type: 'PUT',
      url: `${this.url}/Event`,
      dataType: 'json',
      data: JSON.stringify(data),
      crossDomain: true,
      contentType: 'application/json; charset=utf-8',
      xhrFields: {
        withCredentials: true
      },
      success: function (response, xhr, status) {
        typeof successCallback === 'function' && successCallback();
      },
      complete: function (ret, xhr, status) {
        // notiFeedback('이벤트를 수정하였습니다.');
      },
      error: function (ret, xhr, status) {}
    });
  },
  deleteEvent(taskID, startDate, repeatDeleteOption, successCallback) {
    const data = {
      dto: {
        workspaceID: workspaceManager.getWorkspaceId(getRoomIdByUrl()),
        calendarID: workspaceManager.getChannelList(workspaceManager.getWorkspaceId(getRoomIdByUrl()), TEESPACE_CHANNELS.TSCHEDULE.CHANNEL_TYPE),
        userID: userManager.getLoginUserId(),
        taskID: taskID,
        startDate: startDate,
        repeatDeleteOption: repeatDeleteOption
      }
    };
    Top.Ajax.execute({
      cache: false,
      type: 'DELETE',
      url: `${this.url}/Event`,
      dataType: 'json',
      data: JSON.stringify(data),
      crossDomain: true,
      contentType: 'application/json; charset=utf-8',
      xhrFields: {
        withCredentials: true
      },
      success: function (response, xhr, status) {
        typeof successCallback === 'function' && successCallback(response.dto.eventInfo);
      },
      complete: function (ret, xhr, status) {},
      error: function (ret, xhr, status) {}
    });
  },
  getFileImport(fileID, successCallback) {
    const data = {
      dto: {
        workspaceID: workspaceManager.getWorkspaceId(getRoomIdByUrl()),
        calendarID: workspaceManager.getChannelList(workspaceManager.getWorkspaceId(getRoomIdByUrl()), TEESPACE_CHANNELS.TSCHEDULE.CHANNEL_TYPE),
        userID: userManager.getLoginUserId(),
        file: {
          fileID
        }
      }
    };
    Top.Ajax.execute({
      cache: false,
      type: 'GET',
      url: `${this.url}/File?action=Import`,
      dataType: 'json',
      data: JSON.stringify(data),
      crossDomain: true,
      contentType: 'application/json; charset=utf-8',
      xhrFields: {
        withCredentials: true
      },
      success: function (response, xhr, status) {
        typeof successCallback === 'function' && successCallback(response.dto.eventInfo);
        console.log('file import');
      },
      complete: function (ret, xhr, status) {},
      error: function (ret, xhr, status) {}
    });
  },
  getUrlImport(importUrl, successCallback) {
    const data = {
      dto: {
        workspaceID: workspaceManager.getWorkspaceId(getRoomIdByUrl()),
        calendarID: workspaceManager.getChannelList(workspaceManager.getWorkspaceId(getRoomIdByUrl()), TEESPACE_CHANNELS.TSCHEDULE.CHANNEL_TYPE),
        userID: userManager.getLoginUserId(),
        url: importUrl
      }
    };
    Top.Ajax.execute({
      cache: false,
      type: 'GET',
      url: `${this.url}/SOUrlImportScheduler`,
      dataType: 'json',
      data: JSON.stringify(data),
      crossDomain: true,
      contentType: 'application/json; charset=utf-8',
      xhrFields: {
        withCredentials: true
      },
      success: function (response, xhr, status) {
        typeof successCallback === 'function' && successCallback(response.dto.eventInfo);
        console.log('file import');
      },
      complete: function (ret, xhr, status) {},
      error: function (ret, xhr, status) {}
    });
  },
  createCheckedCalendar(calendar_id, shared_cal_id) {
    const data = {
      dto: {
        room_id: workspaceManager.getWorkspaceId(getRoomIdByUrl()),
        user_id: userManager.getLoginUserId(),
        calendar_id: calendar_id,
        shared_cal_id: shared_cal_id
      }
    };
    Top.Ajax.execute({
      cache: false,
      type: 'POST',
      url: `${this.url}/SOCheck`,
      dataType: 'json',
      data: JSON.stringify(data),
      crossDomain: true,
      contentType: 'application/json; charset=utf-8',
      xhrFields: {
        withCredentials: true
      },
      success: function (response, xhr, status) {
        //  console.log('createCheckedCalendar_success');
      },
      complete: function (ret, xhr, status) {},
      error: function (ret, xhr, status) {}
    });
  },
  deleteCheckedCalendar(calendar_id, shared_cal_id) {
    const data = {
      dto: {
        room_id: workspaceManager.getWorkspaceId(getRoomIdByUrl()),
        user_id: userManager.getLoginUserId(),
        calendar_id: calendar_id,
        shared_cal_id: shared_cal_id
      }
    };
    Top.Ajax.execute({
      cache: false,
      type: 'DELETE',
      url: `${this.url}/SOCheckedCalendar`,
      dataType: 'json',
      data: JSON.stringify(data),
      crossDomain: true,
      contentType: 'application/json; charset=utf-8',
      xhrFields: {
        withCredentials: true
      },
      success: function (response, xhr, status) {
        // console.log('deleteCheckedCalendar_success');
      },
      complete: function (ret, xhr, status) {},
      error: function (ret, xhr, status) {}
    });
  },
  getFilterInfo(successCallback) {
    const data = {
      dto: {
        room_id: workspaceManager.getWorkspaceId(getRoomIdByUrl()),
        user_id: userManager.getLoginUserId(),
        calendar_id: workspaceManager.getChannelList(workspaceManager.getWorkspaceId(getRoomIdByUrl()), TEESPACE_CHANNELS.TSCHEDULE.CHANNEL_TYPE)
      }
    };
    Top.Ajax.execute({
      cache: false,
      type: 'GET',
      url: `${this.url}/SOFilterInfo`,
      dataType: 'json',
      data: JSON.stringify(data),
      crossDomain: true,
      contentType: 'application/json; charset=utf-8',
      xhrFields: { withCredentials: true },
      success: function (response, xhr, status) {
        typeof successCallback === 'function' && successCallback(response.dto);
      },
      error: function (ret, xhr, status) {},
      complete: function (ret, xhr, status) {}
    });
  },
  handleMessage: function (mqttMsg) {
    let _type = mqttMsg.NOTI_TYPE;

    switch (_type) {
      case 'tschedule_new':
        if ($('#calendarLayout').length) {
          const _this = Top.Controller.get('calendarMainLayoutLogic');
          calendarAPI.getEventList(_this._processEventData);
        } else {
        }
        break;
      case 'tschedule_delete':
        if ($('#calendarLayout').length) {
          const _this = Top.Controller.get('calendarMainLayoutLogic');
          calendarAPI.getEventList(_this._processEventData);
        } else {
        }
        break;
      case 'tschedule_update':
        if ($('#calendarLayout').length) {
          const _this = Top.Controller.get('calendarMainLayoutLogic');
          calendarAPI.getEventList(_this._processEventData);
        } else {
        }
        break;
      default:
        NotificationManager.notify(mqttMsg, _type);
    }
  }
};

const calendar = (function () {
  const dndAreaClassList = ['top-calendar-date', 'top-calendar-schedules-bottom-date-time'];
  let _scheduleRepo = {};

  function _render(renderTarget, appWrapper, tsVersion) {
    tds(renderTarget).src(`${appWrapper}.html${tsVersion}`);
    const targetSelector = `div#${renderTarget}`;
    const targetElement = document.querySelector(targetSelector);
    _enableDnd(targetElement);
    /*
    // const targetElement = document.getElementsByClassName("top-calendar-dates")[0];
    targetElement.classList.add('dnd__container');
    // dnd__container에서 추출되는 앱 이름 (dataset.dndApp) 정보 추가
    // 커스텀 데이터 속성의 이름은 dash-style에서 camelCase로 변환
    // 접두사 data-는 삭제되고 소문자 앞에 오는 '-' 삭제 및 해당 소문자 capitalize
    // 해당 커스텀 데이터 속성의 이름은 DOMStringMap의 key로 변환
    targetElement.setAttribute('data-dnd-app', 'schedule');
    */
  }

  function _enableDnd(targetElement) {
    console.debug('_enableDnd');
    console.debug(targetElement);
    targetElement.classList.add('dnd__container');
    // dnd__container에서 추출되는 앱 이름 (dataset.dndApp) 정보 추가
    // 커스텀 데이터 속성의 이름은 dash-style에서 camelCase로 변환
    // 접두사 data-는 삭제되고 소문자 앞에 오는 '-' 삭제 및 해당 소문자 capitalize
    // 해당 커스텀 데이터 속성의 이름은 DOMStringMap의 key로 변환
    targetElement.setAttribute('data-dnd-app', 'schedule');
  }

  function _isDndArea(event, dndAreaClassNameList = ['top-calendar-date', 'top-calendar-schedules-bottom-date-time']) {
    // dnd가 허용되는 공간인지 확인
    for (i = 0; i < event.path.length; i++) {
      for (dndAreaClassName of dndAreaClassNameList) {
        if (event.path[i].classList && event.path[i].classList[0] === dndAreaClassName) {
          console.log(`event.path[${i}].classList[0]===${dndAreaClassName}`);
          return true;
        }
      }
    }
  }

  function _onDragStart(e, data) {
    // drag target 복사 (drag 이미지)
    console.debug('calendar.js _onDragStart');
    const targetSchedule = e.target;
    console.debug(targetSchedule);
    dnd.setBackground(targetSchedule.cloneNode(true));
    dnd.useDefaultEffect(true);

    // 일정 데이터 dnd 객체에 저장
    const scheduleFullId = targetSchedule.dataset.id;
    const scheduleId = scheduleFullId.split(',')[0];
    const startDate = scheduleFullId.split(',')[1];
    console.debug(scheduleId, startDate);
    calendarAPI.getEventDetail(scheduleId, startDate, (data) => {
      dnd.setData(data);
      _scheduleRepo = data;
      console.debug('calendar.js _onDragStart getEventDetail data', data);
    });
  }

  function _onDragEnter(e, data) {
    // 기본 modal 사용 안할경우 구현.
  }

  function _onDragOver(e, data) {
    // 기본 modal 사용 안할경우 구현.
  }

  function _onDragLeave(e, data) {
    // 기본 modal 사용 안할경우 구현.
  }

  function _convertTimeIndexToTime(timeIndex = 0) {
    // timeIndex 7 => `03:30` timeIndex 40 => `20:00`
    let HH = '00';
    let MM = '00';
    hourRaw = Math.floor(timeIndex / 2);
    if (hourRaw < 10) {
      HH = '0' + hourRaw.toString();
    } else {
      HH = hourRaw.toString();
    }
    minuteRaw = timeIndex % 2;
    if (minuteRaw) {
      MM = '30';
    } else {
      MM = '00';
    }
    return `${HH}:${MM}`;
  }

  const monthNames = {
    Jan: '01',
    Feb: '02',
    Mar: '03',
    Apr: '04',
    May: '05',
    Jun: '06',
    Jul: '07',
    Aug: '08',
    Sep: '09',
    Oct: '10',
    Nov: '11',
    Dec: '12'
  };

  function _calculateNewEnd(oldStart, oldEnd, newStart) {
    // input format: "YYYY-MM-DD HH:MM"
    oldStartEpoch = Date.parse(oldStart);
    oldEndEpoch = Date.parse(oldEnd);
    newStartEpoch = Date.parse(newStart);
    const newEndEpoch = newStartEpoch + oldEndEpoch - oldStartEpoch;
    const newEndRaw = new Date(newEndEpoch);
    // new Date() format(not a String): Mon Apr 13 2020 16:36:54 GMT+0900 (대한민국 표준시)
    const newEndDateString = newEndRaw.toDateString();
    // toDateString() format: "Mon Apr 13 2020"
    const [dayOfWeek, month, DD, YYYY] = newEndDateString.split(' ');
    const MM = monthNames[month];
    const newEndTimeString = newEndRaw.toTimeString();
    // toTimeString() format: "16:36:54 GMT+0900 (대한민국 표준시)"
    const [Hours, Minutes, rest] = newEndTimeString.split(':');
    // output format: "YYYY-MM-DD HHMM"
    newEnd = `${YYYY}-${MM}-${DD} ${Hours}:${Minutes}`;
    console.debug('_calculateNewEnd newEnd:', newEnd);
    return newEnd;
  }

  function _deleteColon(timeString) {
    return timeString.split(':').join('');
  }

  function _onDrop(e, data) {
    console.debug('calendar.js _onDrop');
    // 기존 일정 데이터 dnd 객체로부터 획특
    const oldScheduleData = dnd.getData();
    console.debug('_onDrop oldScheduleData', oldScheduleData);
    // oldScheduleData.start/endDate: 'YYYY-MM-DD'
    const oldStartDate = oldScheduleData.startDate;
    const oldEndDate = oldScheduleData.endDate;
    // oldScheduleData.start/endTime: 'HHMM'
    // oldStart/EndTime: 'HH:MM'
    const oldStartTime = oldScheduleData.startTime.slice(0, 2) + ':' + oldScheduleData.startTime.slice(2, 4);
    const oldEndTime = oldScheduleData.endTime.slice(0, 2) + ':' + oldScheduleData.endTime.slice(2, 4);
    console.debug('onDrop oldData ', oldStartDate, oldStartTime, ' ~ ', oldEndDate, oldEndTime);
    // 드롭 위치 타임라인 영역인지 확인
    let isTimeLineRegion = false;
    for (i = 0; i < e.path.length; i++) {
      if (e.path[i].classList && e.path[i].classList.contains('top-calendar-schedules-bottom-date-time')) {
        isTimeLineRegion = true;
        break;
      }
    }
    // from 종일 영역 to 타임라인 영역 이동 금지
    if ((oldScheduleData.allDay || oldStartDate !== oldEndDate) && isTimeLineRegion) {
      TeeToast.open({
        text: `종일 영역에서 타임라인 영역으로의 드래그는 불가합니다.`,
        size: `min`
      });
      return;
    }
    // 드롭 위치 종일 영역인지 확인
    let isAllDayRegion = false;
    for (i = 0; i < e.path.length; i++) {
      if (e.path[i].classList && e.path[i].classList.contains('top-calendar-schedules-top')) {
        isAllDayRegion = true;
        break;
      }
    }
    // from 타임라인 영역 to 종일 영역 이동 금지
    if (!oldScheduleData.allDay && oldStartDate === oldEndDate && isAllDayRegion) {
      TeeToast.open({
        text: `타임라인 영역에서 종일 영역으로의 드래그는 불가합니다.`,
        size: `min`
      });
      return;
    }
    let newStartDate = oldStartDate;
    let newStartTime = oldStartTime;
    let newEndDate = oldEndDate;
    let newEndTime = oldEndTime;
    // 새로운 일정 데이터 드롭 이벤트로부터 획득 및 계산
    const elementListAtMouseDrop = document.elementsFromPoint(e.x, e.y);
    // 날짜 정보 갱신
    for (i = 0; i < elementListAtMouseDrop.length; i++) {
      if (elementListAtMouseDrop[i].dataset.fullDate) {
        newStartDate = elementListAtMouseDrop[i].dataset.fullDate;
        break;
      }
    }
    // 시간 정보 갱신
    for (i = 0; i < elementListAtMouseDrop.length; i++) {
      if (elementListAtMouseDrop[i].dataset.timeIndex) {
        newStartTime = _convertTimeIndexToTime(elementListAtMouseDrop[i].dataset.timeIndex);
        break;
      }
    }

    console.debug('_onDrop newStart: ', newStartDate, newStartTime);
    const oldStart = oldStartDate + ' ' + oldStartTime;
    const oldEnd = oldEndDate + ' ' + oldEndTime;
    const newStart = newStartDate + ' ' + newStartTime;
    const newEnd = _calculateNewEnd(oldStart, oldEnd, newStart);
    // output format: `YYYY-MM-DD HHMM`
    newEndDate = newEnd.split(' ')[0];
    newEndTime = newEnd.split(' ')[1];

    if (newEndTime == '00:00') {
      newEndTime = '24:00';

      let t = new Date(newEndDate);
      t.setDate(t.getDate() - 1);

      newEndDate = t.toISOString().split('T')[0];
    }

    console.debug('_onDrop newEnd:', newEnd);
    const newStartTimeDisplay = _deleteColon(newStartTime);
    const newEndTimeDisplay = _deleteColon(newEndTime);
    calendarEventRepo.setValue('event', oldScheduleData);
    calendarEventRepo.setValue('event.startDate', newStartDate);
    calendarEventRepo.setValue('event.startTime', newStartTimeDisplay);
    calendarEventRepo.setValue('event.endDate', newEndDate);
    calendarEventRepo.setValue('event.endTime', newEndTimeDisplay);
    console.debug('calendar.js _onDrop calendarEventRepo', calendarEventRepo);
    // 일정 업데이트 서비스 호출
    const isRepeated = calendarEventRepo.event.repeatFlag !== 0;
    console.debug('isRepeated', isRepeated);
    if (isRepeated) {
      calendarCommonData.isRepeatedScheduleUpdateMode = true;
      tds('calendarEditRepeatedOptionDialog').open();
    } else {
      calendarCommonData.isRepeatedScheduleUpdateMode = false;
      // repeatUpdateOption	반복 이벤트  수정 옵션. 0: 전체 수정, 1: 그 일정만 수정, 2: 그 일정 이후 수정
      const taskID = oldScheduleData.taskID;
      calendarEventRepo.repeatUpdateOption = 0;
      const parameters = {
        taskID,
        oldStartDate,
        oldEndDate,
        repeatUpdateOption: 0
      };
      console.debug(parameters);
      calendarAPI.updateEvent(calendarEventRepo.event, parameters, () => {
        console.log('calendar dnd schedule update succeeded');
      });
    }
  }

  return {
    setScheduleRepo(schedule) {
      _scheduleRepo = schedule;
    },
    getScheduleRepo() {
      return _scheduleRepo;
    },
    render: function (renderTarget, appWrapper, tsVersion) {
      _render(renderTarget, appWrapper, tsVersion);
    },
    onDragStart: function (e, data) {
      _onDragStart(e, data);
    },
    onDragEnter: function (e, data) {
      _onDragEnter(e, data);
    },
    onDragOver: function (e, data) {
      _onDragOver(e, data);
    },
    onDragLeave: function (e, data) {
      _onDragLeave(e, data);
    },
    onDrop: function (e, data) {
      _isDndArea(e, dndAreaClassList) ? _onDrop(e, data) : console.log('should not drop here');
    },
    redraw() {
      calendarCommonData.getTopCalendarWidget().setSchedules(calendarCommonData.getTopCalendarWidget().getSchedules());
    }
  };
})();

TEESPACE_WEBSOCKET.addWebSocketHandler('CHN0005', calendarAPI.handleMessage);

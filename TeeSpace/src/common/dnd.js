const dnd = (function() {
    const imagePath = "./res/file_move_illust.svg";

    let trackingElement = null;
    let contentNode = null;
    // let contentTitleNode = null;
    // let contentTextNode = null;
    let modal = null;

    // enter, leave 판별 위함
    let currentContainer = null;

    // 전송할 데이터
    let transferData = null;

    // 기본 효과 적용 유무
    let useDefault = true;

    // 드래그 시작된 앱
    let fromApp = null;

    const init = function(handlers) {
        _initModal();
        _initTemplate();
        _initItems(handlers);
        _initContainers(handlers);
    };

    const _initModal = function() {
        const modalId = "dnd__modal";
        modal = document.getElementById(modalId);
        if (!modal) {
            modal = document.createElement("div");
            modal.setAttribute("id", modalId);
            Object.assign(modal.style, {
                display: "none"
            });
            document.body.appendChild(modal);
        }
    };

    const _showModal = function(target) {
        if (modal) {
            const targetRect = target.getBoundingClientRect();
            Object.assign(modal.style, {
                display: "flex",
                width: target.scrollWidth + "px",
                height: target.scrollHeight + "px",
                top: targetRect.top + "px",
                left: targetRect.left + "px"
            });
        }
    };

    const _hideModal = function() {
        if (modal) {
            modal.style.display = "none";
        }
    };

    const _initTemplate = function() {
        if (!trackingElement) {
            const template = document.createElement("div");
            template.setAttribute("id", "dnd__tracking");
            template.innerHTML = `<img id='dnd__tracking__image' src='${imagePath}'/>
                                <div id='dnd__tracking__content'>
    
                                </div>`;

            // <span id='dnd__tracking__content-title'></span>
            // <span id='dnd__tracking__content-text'></span>
            document.body.appendChild(template);
        }

        trackingElement = document.getElementById("dnd__tracking");
        contentNode = document.getElementById("dnd__tracking__content");
        // contentTitleNode = document.getElementById("dnd__tracking__content-title");
        // contentTextNode = document.getElementById("dnd__tracking__content-text");
    };

    const _addDraggableAttribute = function() {
        const items = document.getElementsByClassName("dnd__item");
        if (items.length) {
            const length = items.length;
            for (let i = 0; i < length; i++) {
                if (!items[i].getAttribute("draggable")) {
                    items[i].setAttribute("draggable", true);
                }
            }
        }
    };

    const _initItems = function(handlers) {
        _addDraggableAttribute();

        // dragstart 이벤트 (위임)
        document.addEventListener("dragstart", function(e) {
            _onDragStart(e, handlers);
        });
    };

    const _initContainers = function(handlers) {
        // drop 이벤트
        document.addEventListener("drop", function(e) {
            _onDrop(e, handlers);
        });

        // dragover 이벤트
        document.addEventListener("dragover", function(e) {
            _onDragOver(e, handlers);
        });

        // dragenter 이벤트
        document.addEventListener("dragenter", function(e) {
            _onDragEnter(e, handlers);
        });

        // dragleave 이벤트
        document.addEventListener("dragleave", function(e) {
            _onDragLeave(e, handlers);
        });
    };

    const _onDragStart = function(e, handlers) {
        // 순서 변경한다.
        // Text일 경우 eles로 무시된다.
        if (_hasClass(e.target, "dnd__item") && trackingElement) {
            e.dataTransfer.setDragImage(trackingElement, "auto", "auto");

            if (handlers["dragstart"]) {
                const container = e.target.closest(".dnd__container");

                const appName = container ? container.dataset.dndApp : null;
                fromApp = appName;
                transferData = {};
                handlers["dragstart"](e, appName, { fromApp: fromApp, data: getData() });
            }
        } else {
            dnd.useDefaultEffect(false);
        }
    };

    const _onDrop = function(e, handlers) {
        // 파일 열리는거 방지
        e.preventDefault();

        const container = e.target.closest(".dnd__container");
        if (container) {
            _hideModal();

            if (handlers["drop"]) {
                const appName = container.dataset.dndApp;
                handlers["drop"](e, appName, { fromApp: fromApp, data: getData(), files: e.dataTransfer.files });
            }

            transferData = null;
            fromApp = null;
        }
    };

    const _onDragOver = function(e, handlers) {
        const container = e.target.closest(".dnd__container");
        if (container) {
            e.preventDefault();

            if (handlers["dragover"]) {
                handlers["dragover"](e, container.dataset.dndApp, { fromApp: fromApp, data: getData() });
            }
        }
    };

    const _onDragEnter = function(e, handlers) {
        // count++;
        currentContainer = e.target.closest(".dnd__container");

        if (handlers["dragenter"] && currentContainer) {
            const appName = currentContainer.dataset.dndApp;
            handlers["dragenter"](e, appName, { fromApp: fromApp, data: getData() });
        }

        if (useDefault && currentContainer) {
            _showModal(currentContainer);
        }
    };

    const _onDragLeave = function(e, handlers) {
        const fromTarget = e.fromElement ? e.fromElement.closest(".dnd__container") : null;
        const targetContainer = e.target.closest(".dnd__container");

        if (handlers["dragleave"] && targetContainer && fromTarget) {
            const appName = targetContainer.dataset.dndApp;
            handlers["dragleave"](e, appName, { fromApp: fromApp, data: getData() });
        }

        if (useDefault && !fromTarget) {
            _hideModal();
        }
    };

    const _hasClass = function(elem, classStr) {
        // 아래 코드면 직관적일텐데 ㅠㅠ
        // return elem?.classList?.contains(classStr)??false;
        return elem ? ( elem.classList ? elem.classList.contains(classStr) : false ) : false;
    };

    // const setText = function(textObj) {
    //     if (textObj) {
    //         if (textObj["title"] && contentTitleNode) {
    //             contentTitleNode.innerText = textObj["title"];
    //         }

    //         if (textObj["text"] && contentTextNode) {
    //             contentTextNode.innerText = textObj["text"];
    //         }
    //     }
    // };

    const setData = function(data) {
        Object.assign(transferData, data);
    };

    const getData = function() {
        return JSON.parse(JSON.stringify(transferData));
    };

    const update = function() {
        _addDraggableAttribute();
    };

    const useDefaultEffect = function(value) {
        useDefault = value;
    };

    const setBackground = function(element) {
        if (contentNode) {
            while (contentNode.hasChildNodes()) {
                contentNode.removeChild(contentNode.firstChild);
            }

            if (typeof html === "string") contentNode.innerHTML(element);
            else contentNode.appendChild(element);
        }
    };

    return {
        init: init,
        // setText: setText,
        setData: setData,
        getData: getData,
        useDefaultEffect: useDefaultEffect,
        setBackground: setBackground,
        update: update
    };
})();

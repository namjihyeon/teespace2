const noteJsList = {
	            "noteEditorLogic.js":false,
	            "noteListView.js":false,
	            "noteLnbListLogic.js":false,
	            "noteLnbLogic.js":false,
	            "noteOption.js":false,
	            "noteRouteLogic.js":false,
	            "noteTableLogic.js":false,
	            "noteTagLogic.js":false,
	            "TnoteController.js":false,
	            "TnoteData.js":false,
	            "TnoteFile.js":false,
	            "TnoteMobile.js":false,
	            "TnoteRender.js":false,
	            "TnoteSearch.js":false,
	            "TnoteServiceCall.js":false,
	            "TnoteTableView.js":false,
	            "TnoteLnbMenu.js":false
};

let noteChannel_Id = null;

const noteEventLogic = {
        "noteEventLogic.js":false,
};

// 2020-01-21 다른곳에서도 customInit 함수를 쓰기 때문에 노트와 관련없이 바로 불리게 해둠
//talkUtil.loadJsFiles("src/TeeNote/",noteEventLogic) 이제 index.html에서 부르는 듯 함


let NoteApp = (function(){
	    let noteChannelId = null;
	    let workspaceId= null;
	    let noteRenderLayout = null;
	    let channelName = "Note";

	    let renderer = null;
	    let controller = null;
	    let serviceCaller = null;
		
	    function start( targetWorkspaceId, targetNoteChannelId, targetLayout ) {

	        // 내부에서만 사용하게 바꾸기 위해 작업 한다.
	        workspaceId=targetWorkspaceId;
	        noteChannelId = targetNoteChannelId;
	        noteRenderLayout = targetLayout;
			noteChannel_Id = targetNoteChannelId;
			// setting isExpanded인지
			noteLnbMenu.myIsExpanded();

	        // 그림과 관계 없는
	        initApp();
	        // 그림과 관계
	        initModule();
			// 다른 곳에서 사용하고 있을 곳을 위해 유지 ( 추후 삭제 필요 )
			
			// (SJ) talkTooltip 사용
			talkTooltip.activateTooltip("#note", null);
	    }
	    function initModule( callback ) {
	        serviceCaller.initServiceCaller( workspaceId, noteChannelId );
	        controller.initController( workspaceId, noteChannelId );
	        renderer.initRender( noteRenderLayout, controller )
	    }

	    function initApp() {
//	        setFileManagerChannelId( noteChannelId );
	        renderer = TNoteRenderer;
	        controller = TNoteController;
	        serviceCaller = TNoteServiceCaller;
//	        loadLocalizationData( "ko" );
	    }

	    function loadLocalizationData( locale ) {
	        // Target Language 를 얻어오는게 맞는거 같다.
	        let language = "ko";
	        if ( locale ) {
	            language = locale;
	        }
	        if ( !Top.i18n.map.hasOwnProperty( channelName ) ) {
	            let i18nConfig = {
	                "name": "note",
	                "path": "external/config/i18n/",
	                "language": language,
	                "callback" : function(){}
	            };
	            Top.i18n.load( i18nConfig );
	        }
	    }


	    function showTotalNoteBookMenu() {
	    	if(!isMobile()) controller.selectTotalNoteBookMenu();
	    }

	function controllerSet(){
		_ctrEvent = Top.Controller.get('TopApplicationLogic');
		_ctrAllNote = Top.Controller.get('allNoteLayoutLogic');
		_ctrLnbList = Top.Controller.get('noteLnbListLayoutLogic');
		_ctrTag = Top.Controller.get('tagLayoutLogic');
		_ctrTrashCan = Top.Controller.get('trashCanLayoutLogic');
		_ctrLnb = Top.Controller.get('noteLnbLayoutLogic');
		_ctrTable = Top.Controller.get('noteTableLayoutLogic');
		_ctrEditor = Top.Controller.get('noteEditorLayoutLogic');
	}
	
    return {
        startTNote: start,
        getChannelId: function () {
            return noteChannelId;
        }
        // render:render,
    }
	
})()

function isMobile() {
    return window.matchMedia("(max-device-width:1024px)").matches;
}
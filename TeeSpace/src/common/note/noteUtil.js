let dragTableData =[];
let UTIL = (function(){
	/*
	 *  Note function
	 */
	function closeDialog(id){
		let el = Top.Dom.selectById(id);
		el.close();
	}

	function openDialog(id){
		let el = Top.Dom.selectById(id);
		el.open();
	}
	function setProperty(id, attribute, value){
		let el = Top.Dom.selectById(id);
		if(attribute && value != null){
			el.setProperties({attribute , value});
		}else if(attribute != null && value == null){
			el.setProperties(attribute);
		}
	}
	function getProperty(id,attribute){
		let el = Top.Dom.selectById(id);
		el.getProperties(attribute);
	}
	function ID(id){
		return Top.Dom.selectById(id);
	}
	
	return {
		closeDialog:closeDialog,
		openDialog:openDialog,
		setProperty:setProperty,
		getProperty:getProperty,
		ID:ID
	}
}());
let DynamicAction = ( function(){
    let handler = {
        get: function( target, property ){
            return property in target ? "RESULE = "+target[property] : null;
        },
		set: function ( target, property, value ){
            let beforeValue =target[property];

            if( value === beforeValue ) {
//				console.warn( " You Set SameValue :),  OldValue=",beforeValue);
			}else {

//                console.warn( " You Set Value :) OldValue=",beforeValue," New Value =", value  );
                target[ property ] = value;
            }
            if( !!!beforeValue ) {
                beforeValue = null;
            }
			return beforeValue;
		}
    };

	return{
		observe:{
			makeObject : function( target ){
				return new Proxy( target, handler);
			},
		},
		dynamic:{
			caller : function ( callMethod ){

			}
		},


    //     mobx.autorun(function () {
    //     if (targetMessage["USER_ID"]) {
    //         // 서버 가서 받아오기 (동기 처리)
    //         let userInfo = talkUtil.getUserInfoSync(targetMessage["USER_ID"]);
    //         $(".message__content-header__name[data-user-id='" + targetMessage["USER_ID"] + "']").text(userInfo["USER_NAME"]);
    //     }
    // });

	}

})();
let TNoteUtil = (function(){
    let debug = true;

    function changeTwoDigit( digit ) {
        return ('0' +digit).slice(-2);
    }
    function setFocusEnd(){
    	let target = $('#note div[contenteditable=true]')[0]; 
    	target.focus();
    	let lastRange = document.createRange();
    	lastRange.selectNode(target.lastElementChild);
    	let selObj = window.getSelection();
    	let range = selObj.getRangeAt(0);
    	selObj.removeAllRanges();
    	selObj.addRange(lastRange);
    	selObj.collapseToEnd();
    	target.focus();
    	target.scrollTop = target.scrollHeight;
	}
	
	// input tag에 커서 끝으로 보내기
	function setCaretToTheEnd(input) {
		if (input.setSelectionRange) {
			input.focus();
			let len = input.value.length;
			input.selectionStart = input.value.length;
		}
	}

	return{
		log: function( ...param ) {
			if ( debug ) {
				console.warn( ...param );
			}
		},
		// 만약 이걸 1회만 수행하고 싶다면, noteDate 가 하루가 지나는 것을 고려해야한다.
		getNoteDateFormat: function( linuxTimeFormat ){
			let linuxDate = linuxTimeFormat.split(' ')[0];
			let linuxTime = linuxTimeFormat.split(' ')[1];
			let linuxYear = linuxDate.split('.')[0]*1;
			let linuxMonth = linuxDate.split('.')[1]*1;
			let linuxDay = linuxDate.split('.')[2]*1;
			let linuxHour = linuxTime.split(':')[0]*1;
			let linuxMinute = linuxTime.split(':')[1]*1; 
			
			let meridiem = linuxHour < 12 ? "오전" : "오후";
            let today_newDate = new Date();
            
            if(linuxHour > 12) linuxHour = linuxHour - 12
			// 같은 날
			if( today_newDate.getFullYear() === linuxYear && today_newDate.getMonth()+1 === linuxMonth && today_newDate.getDate() === linuxDay ) {
                return meridiem + ' ' + changeTwoDigit(linuxHour.toString()) + ':' + changeTwoDigit(linuxMinute.toString());
            // 같은 해
            } else if(today_newDate.getFullYear() === linuxYear){
            	return changeTwoDigit(linuxMonth.toString()) + '.' + changeTwoDigit(linuxDay.toString()) + ' ' + meridiem + ' ' + changeTwoDigit(linuxHour.toString()) + ':' + changeTwoDigit(linuxMinute.toString())
            // 다른 해	
            } else {
                return linuxYear + '.' + changeTwoDigit(linuxMonth.toString()) + '.' + changeTwoDigit(linuxDay.toString()) + ' ' + meridiem + ' ' + changeTwoDigit(linuxHour.toString()) + ':' + changeTwoDigit(linuxMinute.toString())
            }
		},
		notiFeedBack : function( msg ){
            notiFeedback( msg );
            $( '.top-notification-icon' ).remove();
            $( '.top-notification-title' ).remove();
            $( '.top-notification-close' ).click( function ( ) {
                closeFeedBack();
            } );
		},
		checkSameAndRemove : function ( targetList , parameter ){
			if( !targetList || targetList.length <= 1){
				return targetList;
			}
            let resultList  = [];
            for ( let index = 0; index < targetList.length; index++ ) {
                if ( ( index > 0 && targetList[ index - 1 ][ parameter ] !== targetList[ index ][ parameter ] ) || index === 0 ) {
                    resultList.push( targetList[ index ] );
                }
            }
            return resultList;
        },
        escapeHtml : function(text){
        	return text.replace(/[\"&<>]/g, function (a) { 
        		return { '"': '&quot;', '&': '&amp;', '<': '&lt;', '>': '&gt;' }[a];
    		});
        },
		setFocusEnd:setFocusEnd,
		setCaretToTheEnd : setCaretToTheEnd,
		// (SJ)안에 있는 노드 지우기(jquery empty 대신 사용하려고 만들었어요)
		removeChildrenNodes: function(elem) {
			while (elem.lastChild) {
				elem.removeChild(elem.lastChild);
			}
		},

		// TODO : B2B는 사용자 이름, B2C는 사용자 별명/아이디
		getUserName : function(data) {
			let userName;
			if (data["USER_ID"]) {
				userName = userManager.getUserName(data["USER_ID"]);
				if (!userName) return "(탈퇴한 멤버)"
				return userName;
			} 
			// 전 버전에는 USER_ID를 입력하지 않았다
			else if (data["user_name"]) {
				return data["user_name"]
			}
			else {
				return "";
			}
		},

		// viewport 안에 있는지 확인하기
		isInViewport : function(node, targetSelector) {
			try {
				if (!node) return;
				let bounding = node.getBoundingClientRect();
				let target = window;
				if (targetSelector) {
					target = document.querySelector(targetSelector);
					let _targetPos = target.getBoundingClientRect();
					let _targetTop = _targetPos.top;
					let _targetLeft = _targetPos.left;
					let _targetBottom = _targetPos.bottom;
					let _targetRight = _targetPos.right;
	
					let condition1 = (_targetTop <= bounding.top) && ( bounding.top <= _targetTop+_targetPos.height);
					let condition2 = (_targetLeft <= bounding.left) && (bounding.left <= _targetLeft+_targetPos.width);
					let condition3 = (_targetBottom >= bounding.bottom) && (bounding.bottom >= _targetBottom-_targetPos.height);
					let condition4 = (_targetRight >= bounding.right) && (bounding.right >= _targetRight-_targetPos.width);
	
					return (condition1 && condition2) || (condition3 && condition4);
	
				} else {
					let condition1 = (0 <= bounding.top) && ( bounding.top <= 0 (window.innerWidth || document.documentElement.clientWidth));
					let condition2 = (0 <= bounding.left) && ( bounding.left <= (window.innerHeight || document.documentElement.clientHeight));
					let condition3 = ((window.innerHeight || document.documentElement.clientHeight)>= bounding.bottom) && (bounding.bottom >= 0);
					let condition4 = ((window.innerWidth || document.documentElement.clientWidth) >= bounding.right) && (bounding.right >= 0);
					
					return (condition1 && condition2) || (condition3 && condition4);
				}
			} catch(e) {
				return false;
			}
			
		}        
	}

})();

let NoteDragDrop = (function(e, data){
	let enterPnode = null;
	let enterPId = null;
	let leavePnode = null;
	let leavePId = null;
	let dropPnode = null;
	let dropPId = null;
	
	let onDragStart = function(e, data){
		if(e.target.getAttribute("chapter-id") === "undefined") {
			dnd.useDefaultEffect(false);
			return;
		}else{
			dnd.useDefaultEffect(true);
			dnd.setData({ app: "note", type: e.target.getAttribute("type"), note_id: e.target.getAttribute("content-id"), note_title: e.target.innerText, parent_notebook : e.target.closest('.dnd__container').getAttribute("chapter-id"), targetList: TNoteData.getMultiplePageData().length !== 0 ? TNoteData.getMultiplePageData() : [] });
			TNoteData.setParentNameData();
		}
	}
	let onDragEnter = function(e, data){
		enterPnode = e.target.closest('.dnd__container');
		enterPId = enterPnode.getAttribute("chapter-id");
		
		if(data.fromApp === "note"){
			if(enterPnode.getAttribute("type") === "app"){
				$(enterPnode).children('#emptyDiv').empty();
				dnd.useDefaultEffect(false);
				$('#dnd__modal').css("display","none");
			}else{
				dnd.useDefaultEffect(true);

				if(dnd.getData().type === "container" && dnd.getData().note_id === null){
					if(enterPId === null) {
						dnd.useDefaultEffect(false);
						$('#dnd__modal').css("display","none");
					}
					enterPnode.setAttribute("style","border-top :2px solid #1ea8df"); 
				}else if(data.data.parent_notebook !== enterPId){
					let insertNode = emptyNode();
					let existEmptyDiv = $(enterPnode).children('#emptyDiv')[0]
					if(existEmptyDiv !== undefined){
						if(existEmptyDiv.children.length === 0) existEmptyDiv.append(insertNode);
						else return;
					}				
				}
			}
		} else {
			dnd.useDefaultEffect(false);
		}
	}
	let onDragLeave = function(e, data){
		leavePnode = e.target.closest('.dnd__container');
		leavePId = leavePnode.getAttribute("chapter-id");
		if(data.fromApp === "note"){
			if(leavePnode.getAttribute("type") === "app"){
				$(leavePnode).children('#emptyDiv').empty();				
				dnd.useDefaultEffect(false);
				$('#dnd__modal').css("display","none");
			}else{
				dnd.useDefaultEffect(true);
				if(enterPId !== leavePId){
					$(leavePnode).children('#emptyDiv').empty();
	                leavePnode.removeAttribute("style");
				}
			}
		}
	}
	let onDragOver = function(e, data){
		e.preventDefault();
	}
	let onDragEnd = function(e,data){
		let existDiv = document.querySelectorAll('#emptyDiv li#emptyNode');
		if(existDiv.length !== 0){
			for(let count=0; count < existDiv.length; count++) existDiv[count].remove();
		}
		$('#dnd__modal').css("display","none");
		deleteBorderColor();
	}
	let onDrop = function(e, data){
		
		dropPnode = e.target.closest('.dnd__container');
		dropPId = dropPnode.getAttribute("chapter-id");
		containerId = dropPnode.getAttribute("id");
		/*
		 * TODO : 각 챕터 하위에 노트들의 is_Edit 여부 확인하는 함수 필요
		 */
		if(data.fromApp === "note"){
			if(dropPnode.getAttribute("type") === "app"){
				$(dropPnode).children('#emptyDiv').empty();				
				dnd.useDefaultEffect(false);
				$('#dnd__modal').css("display","none");
			}else{
				if(dnd.getData().type === "container" && dnd.getData().note_id === null){
	                dropPnode.removeAttribute("style");
	                let enterNodeID = dnd.getData().parent_notebook; 
	                let tempNode = $('[chapter-id=' + enterNodeID + ']')[0];
	                if(enterPId === null) dropPnode.parentElement.previousElementSibling.appendChild(tempNode)
	                else dropPnode.before(tempNode);               
	                TNoteData.makeSnapShotData();
	            }else if(dropPId !== data.data.parent_notebook){
					if(containerId === null){
						/*
						 * 노트 일 경우 추후 챕터 일때도 분기 필요
						 */
						let single = TNoteData.getCurPageData()
						let multiple = TNoteData.getMultiplePageData()
						// 다중 선택일 경우
						if(single.length === undefined && multiple.length !==0){
							let isEditCheck=false;
							for(let i=0; i< multiple.length; i++){
								let tempData = TNoteServiceCaller.noteInfoList(multiple[i].id);
								let noteData = tempData[0];
								if(noteData.is_edit) isEditCheck=true;
							}
							if(!isEditCheck){
								for(let count=0; count < multiple.length; count++) TNoteServiceCaller.noteParentUpdate(multiple[count].id,dropPId)
								TNoteController.initChapterList();
								TNoteData.setMultiplePageData([]);
								TeeToast.open({text: '선택한 ' + multiple.length + '개의 페이지를 ' + TNoteData.getParentNameByParentId(dropPId) + '(으)로 이동하였습니다.'});
							} else {
								TeeToast.open({text: '수정 중인 페이지가 포함되어 있습니다.'});
							}
						}else{ // 단일 선택일 경우
							let tempData = TNoteServiceCaller.noteInfoList(data.data.note_id);
							let noteData = tempData[0];
							if(noteData.is_edit){ // 수정 중일 경우
								TeeToast.open({text: '선택한 페이지는 수정 중 입니다.'});
							} else {
								TNoteServiceCaller.noteParentUpdate(data.data.note_id,dropPId, function(){
									TNoteController.initChapterList();
									TeeToast.open({text: '선택한 1개의 페이지를 ' + TNoteData.getParentNameByParentId(dropPId) + '(으)로 이동하였습니다.'});
								})
							}
												
						}
					}else if(containerId === "note" || containerId === "tag" )return;
				}
				let existDiv = document.querySelectorAll('#emptyDiv li#emptyNode');
				if(existDiv.length !== 0){
					for(let count=0; count < existDiv.length; count++) existDiv[count].remove();
				}
				deleteBorderColor();
			}            
		}
	}
	let deleteBorderColor = function(){
		let borderContainerList = document.querySelectorAll('.note-lnb-ul');
		for(let i=0; i< borderContainerList.length; i++){
			borderContainerList[i].style.borderTop = ""
		}
	}
	let emptyNode = function(){
		let emptyLI = document.createElement("li");
		emptyLI.setAttribute('id','emptyNode');
		emptyLI.setAttribute("type","note");
		emptyLI.setAttribute('class',"note-lnb-li dnd__item");
		emptyLI.setAttribute('style',"border-bottom: 1px solid #dadada; margin: 0px 15px 0px 30px; width: calc(100% - 45px)")
		return emptyLI;
	}
	return {
		onDragStart:onDragStart,
		onDragEnter:onDragEnter,
		onDragLeave:onDragLeave,
		onDragOver:onDragOver,
		onDragEnd:onDragEnd,
		onDrop:onDrop
	}
}())

window.addEventListener('beforeunload', function(e){
	if(document.querySelector('#noteEditorLayout div[contenteditable=true]')){
		Top.Controller.get('noteSaveLayoutLogic').saveOkBtn();
	}
})
$(document).keydown(function(e){
	let isExistTagFocus = document.querySelectorAll('#note .note-tag-chip-container-focus');
	  if(isExistTagFocus.length !== 0){
		  if(e.keyCode == '37') {
			    if(isExistTagFocus.length !== 0){
			    	isExistTagFocus[0].classList.remove("note-tag-chip-container-focus");
			    	if(isExistTagFocus[0].previousElementSibling.classList.contains("note-tag-chip-container")) {
			    		isExistTagFocus[0].previousElementSibling.classList.add("note-tag-chip-container-focus");
			    		isExistTagFocus[0].previousElementSibling.scrollIntoView(false);
			    	}else if(isExistTagFocus[0].previousElementSibling.classList.contains("note-tag-add-textfield")) isExistTagFocus[0].classList.add("note-tag-chip-container-focus")
			    }else {
			    	e.preventDefault(); e.stopPropagation(); return false; 
		    	}
		  }
		  else if(e.keyCode == '39') {
				if(isExistTagFocus.length !== 0){
					isExistTagFocus[0].classList.remove("note-tag-chip-container-focus");
					if(isExistTagFocus[0].nextElementSibling !== null) {
						if(isExistTagFocus[0].nextElementSibling.classList.contains("note-tag-chip-container")) {
							isExistTagFocus[0].nextElementSibling.classList.add("note-tag-chip-container-focus");
							isExistTagFocus[0].nextElementSibling.scrollIntoView(false);
						}
					}else if(isExistTagFocus[0].nextElementSibling === null) isExistTagFocus[0].classList.add("note-tag-chip-container-focus")
				}else {
					e.preventDefault(); e.stopPropagation(); return false; 
				}
		  }else if(e.keyCode == '27'){
				if(isExistTagFocus.length !== 0){
				   for(let i=0; i< isExistTagFocus.length; i++) isExistTagFocus[i].classList.remove("note-tag-chip-container-focus");
				}
		  }
	  }
});
window.onload =  function(){
	var _noteContextMenu = !function(){
		var id = '__noteContextMenu';
		{
			var noteContextMenuDiv = document.createElement('div');
			noteContextMenuDiv.classList.add('note-contextmenu-container');
			noteContextMenuDiv.id = id;
			noteContextMenuDiv.style.display = 'none';
			document.body.appendChild(noteContextMenuDiv);
		}
		var __noteTemp = document.getElementById(id)
		document.addEventListener('click', function(e){ __noteTemp.style.display = 'none' })
	}()
	
	var _tagMoreCover = !function(){
		var id = '__noteTagMoreCover';
		{
			var tagMoreDiv = document.createElement('div');
			tagMoreDiv.classList.add('note-tag-more-cover');
			tagMoreDiv.id = id;
			tagMoreDiv.style.display = 'none';
			// (SJ) 처음 만들 때 위치 설정함
			tagMoreDiv.style.right = '0.75rem';
            tagMoreDiv.style.bottom = '3.19rem';
			document.body.appendChild(tagMoreDiv);

			// talk-tooltip activate(#note안에 없어서 따로)
			talkTooltip.activateTooltip(".note-tag-more-cover", null);
		}
		var __noteTemp2 = document.getElementById(id);
		document.addEventListener('click', function(e){
			if(e.path.includes(document.getElementById('__noteTagMoreCover'))){
				return;
			}  else {
				__noteTemp2.style.display = 'none';
			}
		})
	}()
	
	var _fileAttachContextMenu = !function(){
		var id = '__fileAttachContextMenu';
		{
			var fileAttachDiv = document.createElement('div');
			fileAttachDiv.classList.add('note-file-contextmenu-container');
			fileAttachDiv.id = id;
			fileAttachDiv.style.display = 'none';
			document.body.appendChild(fileAttachDiv);
		}
		var __noteTemp3 = document.getElementById(id);
		document.addEventListener('click', function(e){
			if(e.path.includes(document.getElementById('__fileAttachContextMenu'))){
				return;
			}  else {
				__noteTemp3.style.display = 'none'
			}
		})
	}()
	$(document).off('click').on('click', function(e){
		let isExistFocus = document.querySelectorAll('#note .note-tag-chip-container-focus');
		if(e.target.classList.contains('note-tag-chip-content') || e.target.classList.contains('note-tag-chip-text') ){
			return;
		}else{
			if(isExistFocus.length !== 0) {
				for(let i=0; i< isExistFocus.length; i++) {
					$(isExistFocus[i]).removeClass('note-tag-chip-container-focus')
					isExistFocus[i].classList.remove('note-tag-chip-container-focus');
				}
			}
		}
	})
}
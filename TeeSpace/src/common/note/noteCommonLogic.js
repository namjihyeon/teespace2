urlInfo = Top.Data.create({
    ip: "192.168.158.12",
    port: "8080",
    App: "/CMS",
    SG: "/Note"
});

var urlVar = "http://" + urlInfo.ip + ":" + urlInfo.port + urlInfo.App + urlInfo.SG + "/";
var _allNoteData = []
var _allTagData = []
var _allNoteBookData = []
var _allNoteBooksData = []
//공유용 ws리스트 선언
var _noteShareWSList = []
var listTag = []
var _noteTagData = []

NOTE_EVENT_TYPE ={
    CREATE: "CREATE",
    DELETE: "DELETE",
    REMOVE: "REMOVE",
    UPDATE: "UPDATE",
    EDIT_START: "EDIT",
    EDIT_DONE: "EDITDONE",
    RENAME: "RENAME",
    CHAPTER_RENAME:"CHAPTERRENAME",
    CHAPTER_CREATE:"CHAPTERCREATE",
    CHAPTER_DELETE:"CHAPTERDELETE",
    NONEDIT: "NONEDIT",
    MOVE: "MOVE",
    RESTORE: "RESTORE",
    PERM_REQ: "PERMISSIONREQ",
    PERM_RES: "PERMISSIONRES"
};

Note = (function(){
	
	/*
	 *  Note Render Logic
	 */
//	let _noteFrameCount = 0;
//	let requestAnimationFrameHandle = null;
//	let renderDOM = function(id, path, callback){
//		var ele = Top.Dom.selectById(id);
//        if(!ele) return;
//
//        var prevPath = $("#"+id).attr("src");
//        ele.template.src = undefined;
//        if(prevPath != path){
//            ele.src(path, function() {
//            	var query = document.querySelector("div #" + ele.id);
//            	if(query){
//                    $("#"+id).attr("src",path);
//                    if($("#"+id).attr("src") != undefined){
//                    	if (typeof callback === "function"){
//                        	callback();
//                        }
//                    }
//            	}else {
//        	        requestAnimationFrameHandle = window.requestAnimationFrame(function () {
//        	            _noteFrameCount++;
//        	            if (frameCount < 120)
//        	            	renderDOM(id, path ,callback);
//        	            else {
//        	                window.cancelAnimationFrame(requestAnimationFrameHandle);
//        	                requestAnimationFrameHandle = null;
//        	                _noteFrameCount = 0;
//        	            }
//        	        });
//            	}
//            });
//        }else{
//            if (typeof callback === "function")
//            	callback();
//        }
//	}
	/*
	 *  Note i18n Logic
	 */
//    let i18nLoad = function(type){
//        let i18nConfig = {
//                "name" : "note",
//                "path" : "external/config/i18n/",
//                "language" : type,
//        }
//        Top.i18n.load(i18nConfig);
//    }
	/*
	 * 노트북 서비스 콜
	 */
	let noteBookCreate = function(notebookName, pNode, callback){
		var inputDTO = {
			    "dto": {
			       "id" : "",
			       "note_channel_id" : noteChannel_Id,
			       "text" : notebookName,
			       "children" : [],
		           "type" : type,
			       "parent_notebook" : pNode,
			       "user_name": JSON.parse(sessionStorage.getItem('userInfo')).userName,
			       "note_channel_id" : noteChannel_Id
			    }
			 };
        	Top.Ajax.execute({
		        url: _workspace.url + 'Note/notebooks', 
		        type: 'POST',
		        dataType: 'json',
		        data: JSON.stringify(inputDTO),
		        contentType: 'application/json; charset=UTF-8',
		        async:false,
		        success: function(data) {
		        	data = JSON.parse(data)
		        	if(typeof callback === "function"){
		        		callback()
		        	}
		        }
			})
	}
	let noteBookList = function (noteChannel_Id, callback){
		Top.Ajax.execute({
	        url: _workspace.url + 'Note/notebooks?action=List&note_channel_id=' + noteChannel_Id, /* + "&t=" + new Date().getTime().toString(), */ 
	        type: 'GET',
	        dataType: 'json',
	        contentType: 'application/json; charset=UTF-8',
	        async:false,
	        success: function(data) {
	        	data = JSON.parse(data)
	        	_allNoteBooksData = data.dto.notbookList
	        	
	        	if(typeof callback === "function"){
	        		callback(data)	        	
	        		bookList(noteChannel_Id)
	        	}
	        }
		})
	}
	let bookList = function (noteChannel_Id, callback){
    	Top.Ajax.execute({
			url: _workspace.url + 'Note/notebook?action=List&note_channel_id=' + noteChannel_Id + '&type=notebook'+ "&t=" + new Date().getTime().toString(),  
	        type: 'GET',
	        dataType: 'json',
	        contentType: 'application/json; charset=UTF-8',
	        async:false,
	        success: function(data) {
	        	data = JSON.parse(data)
	        	_allNoteBookData = data.dto.notbookList
	        	if(typeof callback === "function"){
	        		callback(data.dto.notbookList)
	        	}
	        }
		})
	}
	let stackList = function (noteChannel_Id, callback){
    	Top.Ajax.execute({
			url: _workspace.url + 'Note/notebook?action=List&note_channel_id=' + noteChannel_Id + '&type=stack'+ "&t=" + new Date().getTime().toString(),  
	        type: 'GET',
	        dataType: 'json',
	        contentType: 'application/json; charset=UTF-8',
	        async:false,
	        success: function(data) {
	        	data = JSON.parse(data)
	        	if(typeof callback === "function"){
	        		callback(data.dto.notbookList)
	        	}
	        }
		})
	}	
	let noteBookDelete = function(query, type, callback){
    	Top.Ajax.execute({
	        url: _workspace.url + 'Note/notebook?action=Delete&id=' + query.id, 
	        type: 'DELETE',
	        dataType: 'json',
	        contentType: 'application/json; charset=UTF-8',
	        async:false,
	        success: function(data) {
	        	data = JSON.parse(data)
	        	
	        	if(typeof callback === "function"){
	        		callback()
	        	}
	        }
	    })
	}
	let noteBookParentUpdate = function(id){
		var notebookUpdateDTO = {
				"dto": {
					"id" : id,
					"parent_notebook" : "",
					"note_channel_id" : noteChannel_Id,
				}
		};
		Top.Ajax.execute({
			url: _workspace.url + 'Note/notebooks?action=Update',
			type: 'PUT',
			dataType: 'json',
			data: JSON.stringify(notebookUpdateDTO),
			contentType: 'application/json; charset=UTF-8',
			async:false,
			success: function(data) {
				data = JSON.parse(data)
			}
		})
	}

	let noteBookUpdate = function (id, param, p_Id, renderFlag){
		var notebookUpdateDTO = {
			    "dto": {
			       "id" : id,
			       "text": param,
			       "parent_notebook": p_Id,
			       "user_name": JSON.parse(sessionStorage.getItem('userInfo')).userName,
			       "note_channel_id" : noteChannel_Id
			    }
		};
    	Top.Ajax.execute({
	        url: _workspace.url + 'Note/notebooks?action=Update&id=' + id, 
	        type: 'PUT',
	        dataType: 'json',
	        data: JSON.stringify(notebookUpdateDTO),
	        contentType: 'application/json; charset=UTF-8',
	        async:false,
	        success: function(data) {
	        	data = JSON.parse(data)
	        	if(renderFlag === true){
//	        		Note.noteBookList(noteChannel_Id, function(data){
//		        		_ctrLnb._setAllNoteTreeSet(data)
//		        		_ctrLnb.setNoteTreeNodes(data.dto)
//		        	})
	                let treeData = TNoteServiceCaller.noteBookList();
	                let allnote = treeData.find( function ( notebook ) {
	                    return ( notebook[ NOTEBOOK_DTO_KEY.TYPE ] === "allNote" );
	                });
	                treeData.splice( treeData.indexOf( allnote ), 1 );
	                UTIL.ID('noteTree').addNodes('root', treeData);
	        	}
	        }
		})
	}
	/*
	 *  노트 서비스 콜
	 */
    let noteCreate = function(noteName, parent, callback){
        p_Id = parent;
        var noteinput = {
                  "dto": {
                     "WS_ID": ws_id,
                     "note_channel_id": noteChannel_Id,
                     "CH_TYPE": "CHN0003",
                     "USER_ID": JSON.parse(sessionStorage.getItem('userInfo')).userId,
                     "parent_notebook" : p_Id,
                     "note_title": noteName,
                     "user_name": JSON.parse(sessionStorage.getItem('userInfo')).userName,
                     "is_edit" : JSON.parse(sessionStorage.getItem('userInfo')).userId
                  }
         };
         Top.Ajax.execute({
            url: _workspace.url + 'Note/note',
            type: 'POST',
            dataType: 'json',
            data: JSON.stringify(noteinput),
            contentType: 'application/json; charset=UTF-8',
            async:false,
            success: function(data) {
                data = JSON.parse(data)
                if(typeof callback === "function"){
                    callback(noteinput)
                }
            }
        })
    }

    function handleWebSocketMessage(message) {
        if (message.NOTI_ETC === null) {
            console.warn(" NOTE_ETC is empty");
            return;
        }
        
        let eventCase = message.NOTI_ETC.split(',');
        let tokenArray = eventCase[1].split(':');
        let loginUser = userManager.getLoginUserId();
        switch (eventCase[0]) {
            case NOTE_EVENT_TYPE.CREATE: {
            	
            	let noteId = eventCase[1];
            	let user_id = eventCase[2];
            	
            	if (user_id !== userManager.getLoginUserId()) {
            		TNoteController.initChapterList(); // 임시
            	}
//            	
//            	document.querySelector('div[content-id="'+tokenArray[2]+'"]')
            	
            	
            		// NotificationManager.notify(message,"/workspace/" + message.SPACE_ID + "/note",CS_NotificationEventType.TNOTE.NEW_NOTE);
                break;
            }
            case NOTE_EVENT_TYPE.CHAPTER_CREATE: {
            	let chapterId = tokenArray[0];
            	let editingUserId = tokenArray[1];
            	if (editingUserId !== userManager.getLoginUserId()) {
            		TNoteController.initChapterList(); // 임시
            	}
            	break;
            }
            case NOTE_EVENT_TYPE.CHAPTER_DELETE: {
            	let chapterId = tokenArray[0];
            	let editingUserId = tokenArray[1];
            	
            	if (editingUserId !== userManager.getLoginUserId()) {
//            		TNoteController.initChapterList(); // 임시
            		
            		if( document.getElementById('note-editor-container').style.display === "flex" && 
        				document.querySelector('li[content-id="'+document.getElementById('note-editor-container').getAttribute('rendered-page-id') +'"]').parentNode.getAttribute('chapter-id')===chapterId){
            			
            			let childrenNumberOfDeletedChapter = document.querySelector('li[content-id="'+document.getElementById('note-editor-container').getAttribute('rendered-page-id') +'"]').parentNode.childElementCount - 3;
            			if(document.querySelectorAll('#note-chapter-cover li').length === childrenNumberOfDeletedChapter){
            				document.getElementById('editorDIV').style.display = 'none';
	                        document.getElementById('note-empty-div').style.display = 'block';
	                        document.querySelector('div[chapter-id="'+chapterId+'"]').parentNode.remove()
	                		TNoteData.makeSnapShotData()
            			} else {
            				document.querySelector('div[chapter-id="'+chapterId+'"]').parentNode.remove()
                    		TNoteData.makeSnapShotData()
            				let firstPageId = document.querySelector('li[content-id].note-lnb-li').getAttribute('content-id');
                			document.querySelector('li[content-id].note-lnb-li').classList.add('selected')
                			TNoteController.selectPageMenu(firstPageId);
            			}
            		} else {
            			document.querySelector('div[chapter-id="'+chapterId+'"]').parentNode.remove()
                		TNoteData.makeSnapShotData()
            		}
            	}
            	break;
            }
            
            case NOTE_EVENT_TYPE.CHAPTER_RENAME: {
            	// CHAPTERRENAME, CHAPTER_ID, USER_ID, CHAPTER_TITLE
            	let chapterId = tokenArray[0];
                let editingUserId = tokenArray[1];
                let changedName = tokenArray[2];
            	if (editingUserId !== userManager.getLoginUserId()) {
                	document.querySelector('div[chapter-id="'+ chapterId +'"]').childNodes[1].innerText = changedName;
                }
            	
            	break;
            }
            case NOTE_EVENT_TYPE.RENAME: {
            	// RENAME, NOTE_ID, USER_ID, "NOTE_TITLE"
                // "RENAME,0002F00C5DE8907105284671EE56BF4C:894002a7-056a-407c-850e-e3887fdadb93:바꿨지롱"
            	let noteId = tokenArray[0];
                let editingUserId = tokenArray[1];
                let changedName = tokenArray[2];
                
                if (editingUserId !== userManager.getLoginUserId()) {
                	document.querySelector('li[content-id="'+ noteId +'"]').childNodes[1].children[0].innerText = changedName;
                	if(document.getElementById('note-editor-container').style.display === "flex" && document.getElementById('note-editor-container').getAttribute('rendered-page-id') === noteId) {
            			TNoteController.selectPageMenu(noteId);
            		}
                }
                break;
            }
            case NOTE_EVENT_TYPE.MOVE: {
            	// MOVE, NOTE_ID, USER_ID, "P_ID"
                // "MOVE,0002F00C5DE8907105284671EE56BF4C:894002a7-056a-407c-850e-e3887fdadb93:928392398-B23K323U"
            	let noteId = tokenArray[0];
                let editingUserId = tokenArray[1];
                
                if (editingUserId !== userManager.getLoginUserId()) {
                	document.querySelector('div[chapter-id="'+tokenArray[2]+'"]').after(document.querySelector('li[content-id="'+ noteId +'"]'))
                }
                
            	break;
            }
            case NOTE_EVENT_TYPE.EDIT_START: {
                // console.warn("NOTIFICATION - NOTE   ", "EDIT_START ", message);
                // EDIT, NOTE_ID, USER_ID
                // "EDIT,0002F00C5DE8907105284671EE56BF4C,894002a7-056a-407c-850e-e3887fdadb93"
                let noteId = tokenArray[0];
                let editingUserId = tokenArray[1];
                let editingUserName = userManager.getUserInfo(editingUserId).USER_NAME;

                if (editingUserId !== userManager.getLoginUserId()
                    && document.getElementById('note-editor-container').getAttribute('rendered-page-id') === noteId) {
                    if (editingUserId.length > 0) {
                        // 수정 중인 경우 : EDIT_START
                    	TNoteController.setCannotEditMode(editingUserName);
                    }
                }
                break;
            }
            case NOTE_EVENT_TYPE.NONEDIT:
            case NOTE_EVENT_TYPE.EDIT_DONE: {
                // EDITDONE, NOTE_ID, USER_ID, NOTE_TITLE
                // "EDITDONE,0002F00C5DE8907105284671EE56BF4C:894002a7-056a-407c-850e-e3887fdadb93:NULL"
            		let noteId = tokenArray[0];
                    let editingUserId = tokenArray[1];
                    let changedName = tokenArray[2];
                    
                    if (editingUserId !== userManager.getLoginUserId()){
                		if(document.getElementById('note-editor-container').style.display === "flex" && document.getElementById('note-editor-container').getAttribute('rendered-page-id') === noteId) {
                			TNoteController.initChapterList(); // 임시코드. 일단은 이걸로  lnb와 에디터를 둘다 업데이트 해준다. 다만 lnb 불필요한 영역까지 다 다시 그리게 됨
//                			document.querySelector('li[content-id="'+ noteId +'"]').childNodes[1].children[0].innerText = changedName; // 서버에서 줄 경우 이걸로 lnb영역을 업데이트
//                			TNoteController.selectPageMenu(noteId); // 서버에서 줄 경우 이걸로 editor 영역을 업데이트 해줌
                		}
                    }
            		// NotificationManager.notify(message,"/workspace/" + message.WS_ID + "/note",CS_NotificationEventType.TNOTE.MODIFY);
                break;
            }
//            case NOTE_EVENT_TYPE.REMOVE: {
//            		// 노트 id 리스트
//                    let _noteIDList = eventCase[1];
//                    let _userID = eventCase[2];
//
//                    let _userName = userManager.getUserInfo(_userID).user_name;
//                    /*
//                     * 삭제 내역이 화면에 반영되어야 하는 유저의 화면 렌더 로직
//                     */
//                    // Error
//                    if (_userID !== JSON.parse(sessionStorage.getItem('userInfo')).userId) { //자기 자신은 제외시킨다.
//                        if ($("div#noteEditorLayout") && UTIL.ID('noteListView')) { // 노트북에서 노트 목록을 보고있던 사람
//                            let currentNoteId = _noteListView.getSelectedData().note_id;
//                            let noteIdArray = _noteIDList.split(':');
//                            _removeFromListView(currentNoteId, noteIdArray) // 리스트 뷰의 노트 목록을 조회하고 지워진 노트를 포함하고 있을 경우, 리스트뷰에서 제거해 준다.
//
//                        }
//                    }
//            		// NotificationManager.notify(message,"/workspace/" + message.WS_ID + "/note",CS_NotificationEventType.TNOTE.DELETE);
//                break;
//            }

            case NOTE_EVENT_TYPE.DELETE: {
            	let user_id = eventCase[2];
            	let noteId = eventCase[1];
            	
            	if (user_id !== userManager.getLoginUserId()){
            		document.querySelector('li[content-id="'+ noteId +'"]').remove();
            		if(document.getElementById('note-editor-container').style.display === "flex" && document.getElementById('note-editor-container').getAttribute('rendered-page-id') === noteId) {
            			if(!noteLnbMenu.isExpanded()){
                			noteLnbMenu.setLnbVisibility('flex')
                			noteLnbMenu.setEditorVisibility('none')
                		} else {
                			let firstPageId = document.querySelector('li[content-id].note-lnb-li').getAttribute('content-id');
                			document.querySelector('li[content-id].note-lnb-li').classList.add('selected')
                			TNoteController.selectPageMenu(firstPageId);
                		}
            		}
                }
            	
            		// NotificationManager.notify(message,"/workspace/" + message.WS_ID + "/note",CS_NotificationEventType.TNOTE.MOVE_TO_TRASH);
                break;
            }
//            case NOTE_EVENT_TYPE.PERM_REQ: {
//                // 누군가 수정권한 요청을 보냈을 때.
//                // PERMISSIONREQ, NOTE_ID, IS_FAVORITE, IS_EDIT
//                // NOTI_ETC: "PERMISSIONREQ,00039E62B7EADF00D4DFFDD141D67256:9c727cbc-654f-4e29-8dfc-ea4a456f6f40:f4ae2b66-bf58-4eb8-be90-d2289b178943"
//                let note_id = tokenArray[0];
//                let requester_id = tokenArray[1];
//                let editingUser = tokenArray[2];
//                let user_id = JSON.parse(sessionStorage.getItem('userInfo')).userId;
//                
//                if(UTIL.ID('noteEditorLayout') && UTIL.ID('noteListView') && _noteListView.getSelectedData().note_id === note_id){ // 해당 노트를 보고 있는 사람
//                       if(user_id === editingUser){ // 수정 요청을 받은 대상자. 팝업 띄우기
//                              Top.Controller.get('permissionRequestLayoutLogic').openRequestPopup(note_id, requester_id);
//                       } else { // 대상자를 제외한 나머지 사람들. 권한 요청을 제한하도록 하는 로직
//                             UTIL.ID('permRequestBtn').setDisabled(true);
//                       }
//                }
//                break;
//              }
//              case NOTE_EVENT_TYPE.PERM_RES: {
//	                // 수정권한에 대한 응답이 일어났을 때
//	                // PERMISSIONRES, NOTE_ID, IS_EDIT
//	                
//	                let note_id = tokenArray[0];
//	                let editingUser = tokenArray[1];
//	                let user_id = JSON.parse(sessionStorage.getItem('userInfo')).userId;
//	                
//	                if(UTIL.ID('noteEditorLayout') && UTIL.ID('noteListView') && _noteListView.getSelectedData().note_id === note_id) { // 해당 노트를 보고 있는 사람
//	                       if(UTIL.ID('permRequestBtn').getProperties("visible")==="visible"){ // 요청자 + 제 3자만 선별
//	                             if(editingUser === user_id){ // 요청자 입장, 수락된 경우. 그런데 요청자는 다른 DOM으로 가있을 수도 있다.  --> 우선 다른페이지로 이동 불가하게 하는 정책으로 1차 구현하기로 함
//	                                    Note.allnoteList(noteChannel_Id, function(data){
//	                                           let targetIdx;
//	                                           for(let i=0; i<data.dto.noteList.length; i++){
//	                                                  if(note_id === data.dto.noteList[i].note_id){
//	                                                         targetIdx = i;
//	                                                  }
//	                                           }
//	                                           data.dto.noteList = data.dto.noteList = _ctrLnbList.addTagNum(data.dto.noteList);
//	                                           temp2 = data.dto.noteList // 검색용
//	                                           _ctrLnbList.setListView(data.dto.noteList)
//	                                           _ctrLnb.fav_check_render(data.dto.noteList);
//	                                           setTimeout(function(){
//	                                                  _ctrLnb._setClickedNote(targetIdx)
//	                                                  _ctrLnbList.setEditMode();
//	                                           },1)
//	                                    })
//	                             } else { // 거절된 경우. 이 경우 피청자나 제 3자나 동일해 짐.
//	                                    UTIL.ID('permRequestBtn').setDisabled(false);
//	                             }
//	                       }
//	                }                
//	                break;
//              }
              case NOTE_EVENT_TYPE.RESTORE : {
            		//   NotificationManager.notify(message,"/workspace/" + message.WS_ID + "/note",CS_NotificationEventType.TNOTE.NOTE_RESTORE)
              	break;
              }
            default: {
                // console.warn("NOTE_NOTIFICATION :", message);
                console.warn(" Unknown Notification arraived - NoteEtc: ", message.NOTI_ETC);
                break;
            }
        }

    }
    let noteList = function (parent_notebook, callback){
    	Top.Ajax.execute({
            url: _workspace.url + 'Note/note?action=List&note_channel_id=' + noteChannel_Id + '&parent_notebook=' + parent_notebook + "&USER_ID=" + JSON.parse(sessionStorage.getItem('userInfo')).userId + "&t=" + new Date().getTime().toString(), 
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json; charset=UTF-8',
            async:false,
            success: function(data) {
            	data = JSON.parse(data)
                data = Note.removeErasedNote(data)
                
                if(typeof callback === "function"){
                	callback(data)
                }                
            }
        })
    }
	let noteDelete = function (note_list, callback){
		var deleteInfo = {
			"dto" : {
				"noteList" : note_list
			}
		}
    	Top.Ajax.execute({
	        url: _workspace.url + 'Note/note?action=Delete', 
	        type: 'POST',
	        dataType: 'json',
	        data : JSON.stringify(deleteInfo),
	        contentType: 'application/json; charset=UTF-8',
	        async:false,
	        success: function(data) {
	        	data = JSON.parse(data)
	        	if(typeof callback === "function"){
	        		callback()
	        	}
	        }
	    })
	}
	let noteTrash = function(note_list, callback){
		let removable = [];
		let occupied = [];
		for(let i=0; i<note_list.length; i++){
			noteInfoList(note_list[i].note_id, function(data){
				if(data.is_edit){
					occupied.push(note_list[i]);
				} else {
					removable.push(note_list[i]);
				}
			})
		}
		if(removable.length !== 0){
			let trashinput = {
					"dto": {
						"noteList" : removable
					}
				};
	    	Top.Ajax.execute({
		        url: _workspace.url + 'Note/noteTrash', 
		        type: 'POST',
		        dataType: 'json',
		        data: JSON.stringify(trashinput),
		        contentType: 'application/json; charset=UTF-8',
		        async:false,
		        success: function(data) {
		        	data = JSON.parse(data)
		        	if(typeof callback === "function"){
		        		callback()
		        	}
		        }
			 })
		}
		return occupied;
		
	}
	let noteTrashList = function (callback){
    	Top.Ajax.execute({
            
            url: _workspace.url + 'Note/noteTrash?action=List&note_channel_id=' + noteChannel_Id + '&WS_ID=' + ws_id + "&t=" + new Date().getTime().toString(), 
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json; charset=UTF-8',
            async:false,
            success: function(data) {
            	data = JSON.parse(data)
            	if(typeof callback === "function"){
	        		callback(data)
	        	}
            }
        })
    }
	let noteTrashDelete = function (note_list, callback){
		var _inputDTO = {
			"dto" : {
				"noteList" : note_list
			}
		}
    	Top.Ajax.execute({
	        url: _workspace.url + 'Note/noteTrash?action=Delete', 
	        type: 'POST',
	        dataType: 'json',
	        data: JSON.stringify(_inputDTO),
	        contentType: 'application/json; charset=UTF-8',
	        async:false,
	        success: function(data) {
	        	data = JSON.parse(data)	        	
	        	if(typeof callback === "function"){
	        		callback()
	        	}
	        }
	    })
	}
	let noteParentUpdate = function(note_id, p_id, renderFlag){
		let updateFlag;
		let returnData = undefined;
		noteInfoList(note_id, function(data){
			if(data.is_edit){ // 누군가 수정 중일 때
				updateFlag = false;
				returnData = data.note_title;
			} else {
				updateFlag = true;
				returnData = true;
			}
		})
		
		if(updateFlag === true){ // 노트의 parent update 수행
			let inputDTO = {
					"dto": {
						"WS_ID": routeManager.getWorkspaceId(),
//						"CH_TYPE": "CHN0003",
						"note_id": note_id,
						"parent_notebook": p_id,
						"user_name": JSON.parse(sessionStorage.getItem('userInfo')).userName,
						"USER_ID": JSON.parse(sessionStorage.getItem('userInfo')).userId,
						"TYPE": "default",
						"note_channel_id" : noteChannel_Id,
					}
				}
		    	Top.Ajax.execute({
			        url: _workspace.url + 'Note/note?action=Update',
			        type: 'PUT',
			        dataType: 'json',
			        data: JSON.stringify(inputDTO),
			        contentType: 'application/json; charset=UTF-8',
			        async:false,
			        success: function(data) {
			        	data = JSON.parse(data)
			        }
				})	
		}
		return returnData;
	}
	let noteEditUpdate = function(note_id, callback){ // 수정버튼을 눌렀을 때 수정 중으로 업데이트 하기 위한 서비스 콜
        let p_id = _noteListView.getSelectedData().parent_notebook;
		var inputDTO = {
			"dto": {
                "WS_ID": routeManager.getWorkspaceId(),
//                "CH_TYPE": "CHN0003",
		        "USER_ID": JSON.parse(sessionStorage.getItem('userInfo')).userId,
				"note_id": note_id,
				"user_name": JSON.parse(sessionStorage.getItem('userInfo')).userName,
				"is_edit": JSON.parse(sessionStorage.getItem('userInfo')).userId,
				"parent_notebook": p_id,
				"TYPE": "EDIT_START",
				"note_channel_id" : noteChannel_Id
			}
		}
		Top.Ajax.execute({
	        url: _workspace.url + 'Note/note?action=Update',
	        type: 'PUT',
	        dataType: 'json',
	        data: JSON.stringify(inputDTO),
	        contentType: 'application/json; charset=UTF-8',
	        async:false,
	        success: function(data) {
	        	data = JSON.parse(data)
	        	if(typeof callback === "function"){
	        		callback()
	        	}
	        }
		})
	}
	let noteNonEditUpdate = function(note_id, callback){ // 수정 중 나가기 했을때 업데이트 하기 위한 서비스 콜
		p_id = _noteListView.getSelectedData().parent_notebook
		var inputDTO = {
			"dto": {
                "WS_ID": ws_id,
//                "CH_TYPE": "CHN0003",
		        "USER_ID": JSON.parse(sessionStorage.getItem('userInfo')).userId,
		        "note_title": "",
				"note_id": note_id,
				"is_edit": "",
				"note_content": "",
				"parent_notebook":p_id,
				"TYPE": "default",
				"note_channel_id" : noteChannel_Id,
				// 0822
                "is_favorite":"none"
			}
		}
		Top.Ajax.execute({
	        url: _workspace.url + 'Note/note?action=Update', 
	        type: 'PUT',
	        dataType: 'json',
	        data: JSON.stringify(inputDTO),
	        contentType: 'application/json; charset=UTF-8',
	        async:false,
	        success: function(data) {
	        	data = JSON.parse(data)
	        	noteUpdateFrom = _ctrLnb.getLnbMenuType()
	        	if(typeof callback === "function"){
	        		callback()
	        	}
	        }
		})
	}
	let noteUpdate = function (note_id, param, renderFlag){ // 기존에 사용하던 저장 시 업데이트 치는 함수임.
    	var title;
		var inputDTO;
		if(param == undefined && noteUpdateFrom!="trashcan"){
			title = Top.Dom.selectById('contenteditorTitle').getText();
			let p_id = _noteListView.getSelectedData().parent_notebook
			var context;
			var spaceremove;
			var findImg = $('#sun_editorEdit').find('img');
			if(findImg != null){
//				for(var i=0; i< findImg.length; i++){
//					if(findImg[i].getAttribute('file_id')){
//						findImg[i].setAttribute("src", " ")
//					}
//				}
				spaceremove = $('#sun_editorEdit').html();
				spaceremove = [].filter.call( spaceremove, function( c ) {
	    			return c.charCodeAt( 0 ) !== 8203;
				}).join('');
				context = spaceremove
			}else{
				spaceremove = $('#sun_editorEdit').html();
				spaceremove = [].filter.call( spaceremove, function( c ) {
	    			return c.charCodeAt( 0 ) !== 8203;
				}).join('');
				context = spaceremove;
			}
			if(isNaN(context.charCodeAt()) == true){
				context = "<p></p>"
			}
			var noteUpdate = {
				"dto": {
			        "WS_ID": ws_id,
//			        "CH_TYPE": "CHN0003",s
			        "USER_ID": JSON.parse(sessionStorage.getItem('userInfo')).userId,
					"note_id": note_id,
					"note_title":title,
					"note_content":context,
					"user_name": JSON.parse(sessionStorage.getItem('userInfo')).userName,
					"is_edit": "",
					"parent_notebook": p_id,
					"TYPE": "EDIT_DONE",
					"note_channel_id" : noteChannel_Id,
					// 0822
                    "is_favorite":"none"
				}
			}
			inputDTO = noteUpdate;

		}
//		if(param != undefined){
//			title = param;
//			var contextUpdate = {
//				"dto": {
//                    "WS_ID": ws_id,
//                    "CH_TYPE": "CHN0003",
//			        "USER_ID": JSON.parse(sessionStorage.getItem('userInfo')).userId,
//					"note_id": note_id,
//					"note_title":title,
//					"user_name": JSON.parse(sessionStorage.getItem('userInfo')).userName,
//					"is_edit": ""
//				}
//			}
//			inputDTO = contextUpdate;
//		}
    	Top.Ajax.execute({
	        url: _workspace.url + 'Note/note?action=Update', 
	        type: 'PUT',
	        dataType: 'json',
	        data: JSON.stringify(inputDTO),
	        contentType: 'application/json; charset=UTF-8',
	        async:false,
            success: function(data) {
           	  data = JSON.parse(data)
          }
      })
	}
//	let noteFavoriteUpdate = function(note_id,fav_flag,p_id,callback){		
//		var inputDTO = {
//			"dto": {
//				"WS_ID" : ws_id,
//				"USER_ID": JSON.parse(sessionStorage.getItem('userInfo')).userId,
//				"note_id": note_id,
//				"is_favorite": fav_flag,
//				"parent_notebook" : p_id,
//				"TYPE": "default",
//				"note_channel_id" : noteChannel_Id
//			}
//		}
//		Top.Ajax.execute({
//	        url: _workspace.url + 'Note/note?action=Update', 
//	        type: 'PUT',
//	        dataType: 'json',
//	        data: JSON.stringify(inputDTO),
//	        contentType: 'application/json; charset=UTF-8',
//	        async:false,
//            success: function(data) {
//           	  data = JSON.parse(data)
//           	  if(typeof callback === "function"){
//           		  callback()
//           	  }
//            }
//		})
//	}
	let noteRestoreUpdate = function(note_id, p_id, renderFlag){		
		var inputDTO = {
			"dto": {
				"WS_ID" : ws_id,
				"USER_ID": JSON.parse(sessionStorage.getItem('userInfo')).userId,
				"note_id": note_id,
				"is_erased" : 0,
				"parent_notebook" : p_id,
				"TYPE": "default",
				"note_channel_id" : noteChannel_Id
			}
		}
		Top.Ajax.execute({
	        url: _workspace.url + 'Note/note?action=Update', 
	        type: 'PUT',
	        dataType: 'json',
	        data: JSON.stringify(inputDTO),
	        contentType: 'application/json; charset=UTF-8',
	        async:false,
            success: function(data) {
           	  data = JSON.parse(data)
          }
      })
	}
//	let noteTrashUpdate = function(note_id, p_id, renderFlag){
//		var inputDTO = {
//			"dto": {
//				"WS_ID" : ws_id,
//				"USER_ID": JSON.parse(sessionStorage.getItem('userInfo')).userId,
//				"note_id": note_id,
//				"is_erased" : 1,
//				"parent_notebook":p_id,
////				"is_favorite": JSON.parse(sessionStorage.getItem('userInfo')).userId,
//				"TYPE": "default",
//				"note_channel_id" : noteChannel_Id
//			}
//		}
//		Top.Ajax.execute({
//	        url: _workspace.url + 'Note/note?action=Update', 
//	        type: 'PUT',
//	        dataType: 'json',
//	        data: JSON.stringify(inputDTO),
//	        contentType: 'application/json; charset=UTF-8',
//	        async:false,
//            success: function(data) {
//           	  data = JSON.parse(data)           	  
//          }
//      })	
//	}
	let allnoteList = function(noteChannel_Id, callback){
		function parseValue(data, callback){
			parsedData = JSON.parse(data);
			if(typeof callback === "function"){
				callback(parsedData);
			}
		}
    	Top.Ajax.execute({
			
	        url: _workspace.url + 'Note/allnote?action=List&note_channel_id=' + noteChannel_Id + "&USER_ID=" + JSON.parse(sessionStorage.getItem('userInfo')).userId + "&t=" + new Date().getTime().toString(),  
	        type: 'GET',
	        dataType: 'json',
	        contentType: 'application/json; charset=UTF-8',
	        async:false,
	        success: function(data) {
	        	parseValue(data, function(parsedData){
	        		parsedData = Note.removeErasedNote(parsedData)
		            _allNoteData = parsedData.dto.noteList
		            
		            if(typeof callback === "function"){
		            	callback(parsedData)
		            }
	        	})
	        }
		})
	}
	/*
	 *  태그 서비스 콜
	 */
	let tagCreate = function(noteId, tagName, callback){
		var taginput = {
		    "dto": {
		       "tag_id" : "",
		       "text" : tagName,
		       "icon":"",
		       "note_id": noteId
		    }
		 };
		Top.Ajax.execute({
			
	        url: _workspace.url + 'Note/tag', 
	        type: 'POST',
	        dataType: 'json',
	        data: JSON.stringify(taginput),
	        contentType: 'application/json; charset=UTF-8',
	        async:false,
	        success: function(data) {
	        	data = JSON.parse(data)
	        	if(typeof callback === "function"){
	        		callback(data)
	        	}
	        }
		})
	}
	let tagList = function(noteId, callback){
    	Top.Ajax.execute({
			
	        url: _workspace.url + 'Note/tag?action=List&note_id=' + noteId + "&t=" + new Date().getTime().toString(),  
	        type: 'GET',
	        dataType: 'json',
	        contentType: 'application/json; charset=UTF-8',
	        async:false,
	        success: function(data) {
	        	data = JSON.parse(data)
	        		tag_item = [];
        			data.dto.tagList.forEach(function(tag){
		        		tag_item.push(tagProxy(tag));
		        	});
        			
        			noteTag.initEditorTag();
        			noteTag.renderTagChips(tag_item, document.querySelector('#note .tags-inner-container'));
        			callback()
//        			noteTag.updateTagMoreButton();
        			
        			
        			
//		        	let tagChip = Top.Widget.create('top-chip');	        	
//	        		tagChip.setProperties({		        		
//		        		"id" : "tag_chip",		        		
//		                "layout-width": "auto",
//		                "layout-height": "1.875rem",
//		                "margin": "0.4375rem 0rem 0.4375rem 0rem",
//		                "border-width": "0rem",
//		                "layout-tab-disabled": false,
//		                "class": "linear-child-vertical",
//		                "actual-height": "2.75rem",
//		                "style" : "text-align: center;",
//		                "readonly" : true
//		            });
//	        		Note.checkDOM("div#chipLayout", function(){
//	        			Top.Dom.selectById('chipLayout').addWidget(tagChip);
//		        		tagChip.setProperties({
//		        			"chip-items":tag_item,
//		        			"on-close": function(event, widget, data){
//			        			Top.Controller.get('noteEditorLayoutLogic').tagDel(data);
//			        		}
//		        		})
//		        		UTIL.setProperty('tag_chip', {'on-keydown': function(event, widget, data){
//		        			if(event.keyCode === 13) event.target.blur()
//		        		}})
//				        document.querySelectorAll('div#chipLayout div.top-chip-root form')[0].style.display = "inline-flex"
//			        	
//				        if(callback && typeof callback === "function") callback();
//	        		})
//	        	_noteTagData = data
	        }
		})
	}
    let tagDelete = function(data, callback){ 
		var deleteInfo = {
			"dto" : {
				"tag_id" : data.tag_id,
				"note_id" : data.note_id
			}
		}
    	Top.Ajax.execute({
	        url: _workspace.url + 'Note/tag?action=Delete&tag_id=' + data.tag_id, 
	        type: 'POST',
	        dataType: 'json',
	        data : JSON.stringify(deleteInfo),
	        contentType: 'application/json; charset=UTF-8',
	        async:false,
	        success: function(data) {
	        	data = JSON.parse(data)
	        	if(typeof callback === "function"){
	        		callback()
	        	}	        	
	        }
	    })
	}
    let alltagList = function(noteChannel_Id, callback){
    	Top.Ajax.execute({
			
	        url:_workspace.url + 'Note/alltag?action=List&note_channel_id=' + noteChannel_Id + "&t=" + new Date().getTime().toString(),  
	        type: 'GET',
	        dataType: 'json',
	        contentType: 'application/json; charset=UTF-8',
	        async:false,
	        success: function(data) {
	        	data = JSON.parse(data)
	        	for(var i=0; i<data.dto.tagList.length; i++){
	        		data.dto.tagList[i].id = data.dto.tagList[i].tag_id;
	        	}
	        	_allTagData = data.dto.tagList
	        	
	        	if(typeof callback === "function"){
	        		callback(data)
	        	}
            }
		})
	}
	let tagSortList = function(noteChannel_Id){
    	Top.Ajax.execute({
	        url: _workspace.url + 'Note/tagSort?action=List&note_channel_id=' + noteChannel_Id + "&t=" + new Date().getTime().toString(),  
	        type: 'GET',
	        dataType: 'json',
	        contentType: 'application/json; charset=UTF-8',
	        async:false,
	        success: function(data) {
	        	data = JSON.parse(data)
	        	data.dto.tag_index_list_dto.sort(function (a, b){
	        		return a.KEY.charCodeAt(0) - b.KEY.charCodeAt(0);
	        	})
	        	sorted_tag_item = data.dto.tag_index_list_dto;
	        }
		})
	}
	let tagNoteList = function(tag_id, callback){
    	Top.Ajax.execute({
			
            url: _workspace.url + 'Note/tagnote?action=List&tag_id=' + tag_id + "&USER_ID=" + JSON.parse(sessionStorage.getItem('userInfo')).userId + "&note_channel_id=" + noteChannel_Id,// + "&t=" + new Date().getTime().toString(),  
	        type: 'GET',
	        dataType: 'json',
	        contentType: 'application/json; charset=UTF-8',
	        async:false,
	        success: function(data) {
	        	data = JSON.parse(data)
	        	data = Note.removeErasedNote(data)
	        	if(typeof callback === "function"){
	        		callback(data)
	        	}
	        }
		})
	}
    
    let noteFavoriteCreate = function(note_id){
            var favoriteInfo = {
                dto: {
                   "note_channel_id" : noteChannel_Id,
                   "note_id" : note_id,
                   "USER_ID" : JSON.parse(sessionStorage.getItem('userInfo')).userId
                }
            }
            Top.Ajax.execute({
                url: _workspace.url + 'Note/notefavorite', 
                type: 'POST',
                dataType: 'json',
                data : JSON.stringify(favoriteInfo),
                contentType: 'application/json; charset=UTF-8',
                async:false,
                success: function(data) {
                    data = JSON.parse(data)
                    if(typeof callback === "function"){
                        callback()
                    }                
                }
            })
    }
    
    let noteFavoriteList = function(callback){
        Top.Ajax.execute({
            
            url: _workspace.url + 'Note/notefavorite?action=List&USER_ID=' + JSON.parse(sessionStorage.getItem('userInfo')).userId + '&note_channel_id=' + noteChannel_Id + "&t=" + new Date().getTime().toString(),
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json; charset=UTF-8',
            async:false,
            success: function(data) {
                data = JSON.parse(data)
                data = Note.removeErasedNote(data);
                if(typeof callback === "function"){
                    callback(data)
                }
            }
        })
    }
    
    let noteFavoriteDelete = function(note_id){
        var favorite_deleteInfo = {
                dto: {
                   "note_id" : note_id,
                   "USER_ID" : JSON.parse(sessionStorage.getItem('userInfo')).userId
                }
            }
            Top.Ajax.execute({
                url: _workspace.url + 'Note/notefavorite?action=Delete', 
                type: 'POST',
                dataType: 'json',
                data : JSON.stringify(favorite_deleteInfo),
                contentType: 'application/json; charset=UTF-8',
                async:false,
                success: function(data) {
                    data = JSON.parse(data)
                    if(typeof callback === "function"){
                        callback()
                    }                
                }
            })
    }
    
	let shareWSList = function(){
    	Top.Ajax.execute({
			
	        url: "http://192.168.158.11:8080/CMS/WorkSpace/" + 'Workspace?action=List&USER_ID=' + JSON.parse(sessionStorage.getItem('userInfo')).userId + '&TASK_TYPE=COM0007' + "&t=" + new Date().getTime().toString(),  
	        type: 'GET',
	        dataType: 'json',
	        contentType: 'application/json; charset=UTF-8',
	        async:false,
	        success: function(data) {
	        	data = JSON.parse(data)
	        	_noteShareWSList = data	        	
	        }
		})
	}
	let noteShareCreate = function(){
		var createShare = {
			"dto": {
				"noteList" : [
				
				],
				"ws_List" : [
					
				]
			}
		}
		
    	Top.Ajax.execute({
	        url: _workspace.url + 'Note/noteshare', 
	        type: 'POST',
	        dataType: 'json',
	        data: JSON.stringify(createShare),
	        contentType: 'application/json; charset=UTF-8',
	        async:false,
	        success: function(data) {
	        	data = JSON.parse(data)
	        }
		})
	}
	let noteShareUpdate = function(){
		var updateShare = {
			"dto": {
				"note_id": ""
			}
		}
		
    	Top.Ajax.execute({
	        url: _workspace.url + 'Note/noteshare?action=Update', 
	        type: 'PUT',
	        dataType: 'json',
	        data: JSON.stringify(updateShare),
	        contentType: 'application/json; charset=UTF-8',
	        async:false,
	        success: function(data) {
	        	data = JSON.parse(data)
	        }
		})
	}
	
	let noteEditOnClose = function(){
//		emptyFileQueue();
//		if(_noteListView.getSelectedData() != null && document.querySelectorAll('.noteEditor_overlay1').length != 0){
//			var oncloseNote_id = _noteListView.getSelectedData().note_id;
//			var p_id = _noteListView.getSelectedData().parent_notebook;
//			TNoteServiceCaller.noteNonEditUpdate(oncloseNote_id,p_id);
//			$('.noteEditor_overlay1').remove();
//	    	$('.noteEditor_overlay2').remove();
//	    	$('.noteEditor_overlay3').remove();
//	    	$('.noteEditor_overlay4').remove();
//		}
		// 기획 변경으로 저장하는 효과를 낸다.
		Top.Controller.get('noteSaveLayoutLogic').saveOkBtn();
	}
	////////////////////////////////////////////////////////////////////////서비스 콜이 아닌 common functions
//	/**
//	 * note에서 렌더 이후 시점 확정용 테스트 checkDOM 코드
//	 */
	let _noteFrameCount = 0;
	let requestAnimationFrameHandle = null;
	let checkDOM = function (selector, callback) {
	    if (document.querySelector(selector)) {
	        if (callback && typeof callback === "function"){
	            callback();
	        }
		_noteFrameCount =0;
	    } else {
	        requestAnimationFrameHandle = window.requestAnimationFrame(function () {
	            _noteFrameCount++;
	            if (_noteFrameCount < 120){
	                checkDOM(selector, callback);	                
	            }else {
	                window.cancelAnimationFrame(requestAnimationFrameHandle);
	                requestAnimationFrameHandle = null;
	                _noteFrameCount = 0;
	                
	            }	
	        });
	    }
	};

    function refreshAllNote(){
        Top.Dom.selectById('notebookRefreshBtn').trigger("click");
    }

	let removeErasedNote = function(data){
		let i=0;
		let erasedList = data.dto.noteList;
		if(erasedList.length != undefined){
	        while(i<data.dto.noteList.length){
	        	if(data.dto.noteList[i].is_erased==1){
	        		data.dto.noteList.splice(i,1)
	        	}
	        	else i++
	        }			
		}

        return data
	}

	function renderListView (noteList){
        _noteListView.setValue(noteList, 0, false);
    }
    function  _removeFromListView(_curNoteId, removedNoteIdArray){
        //DeepCopy
        let _curNoteList = JSON.parse(JSON.stringify(_noteListView.getData()));
        let currentNoteIdList = [];
        // find google.(javascript )
        for(let i=0;i<_curNoteList.length;i++){
            currentNoteIdList.push(_curNoteList[i].note_id)
        }
        for(let j=0; j< removedNoteIdArray.length; j++){
            let index = currentNoteIdList.indexOf(removedNoteIdArray[j]);
            if(index !== -1){// 존재할 경우
                _curNoteList.splice(index, 1);
                currentNoteIdList.splice(index, 1);
            }
        }
        renderListView( _curNoteList )
        /*
         * 제거 한 뒤, 내가 보고있던 노트가 삭제된 경우와 내가 보고있던 노트가 삭제 되지 않은 경우로 나눠서 처리해줘야 함.
         */
        if(currentNoteIdList.indexOf(_curNoteId) === -1){ // 내가 보고있던 노트가 삭제된 경우
            if(_curNoteList.length !== 0)
                _noteListView.selectDataByIndex(0);
            else{
                UTIL.ID('notebookRefreshBtn').trigger("click")
            }
        }
        else{
            let _curNoteIndex;
            let currentNoteListViewData = _noteListView.getData();
            for(let index=0; index< currentNoteListViewData.length; index++){
                if(currentNoteListViewData[index].note_id ===_curNoteId)
                    _curNoteIndex = index
            }
            _noteListView.selectDataByIndex(_curNoteIndex)
        }
    }
 
    //0819 test code 수정권한 요청관련
    let notePermissionAcceptUpdate = function (note_id, requesterId, callback){
          var title = Top.Dom.selectById('contenteditorTitle').getText();
          let p_id = _noteListView.getSelectedData().parent_notebook
          var context;
          var spaceremove;
          var findImg = $('#sun_editorEdit').find('img');
          if(findImg != null){
               for(var i=0; i< findImg.length; i++){
                      if(findImg[i].getAttribute('file_id')){
                     findImg[i].setAttribute("src", " ")
                 }
              }
             spaceremove = $('#sun_editorEdit').html();
             spaceremove = [].filter.call( spaceremove, function( c ) {
                 return c.charCodeAt( 0 ) !== 8203;
             }).join('');
             context = spaceremove
          }else{
             spaceremove = $('#sun_editorEdit').html();
             spaceremove = [].filter.call( spaceremove, function( c ) {
               return c.charCodeAt( 0 ) !== 8203;
             }).join('');
             context = spaceremove;
          }
          if(isNaN(context.charCodeAt()) == true){
                 context = "<p></p>"
          }
          let inputDTO = {
                 "dto": {
                      "WS_ID": ws_id,
                     "CH_TYPE": "CHN0003",
                     "USER_ID": JSON.parse(sessionStorage.getItem('userInfo')).userId,
                     "note_id": note_id,
                     "note_title":title,
                     "note_content":context,
                     "user_name": JSON.parse(sessionStorage.getItem('userInfo')).userName,
                     "is_edit": requesterId,
                     "parent_notebook": p_id,
                     "is_favorite": "none",
                     "TYPE": "PERM_ACCEPT",
                     "note_channel_id" : noteChannel_Id
                      }
          }
             Top.Ajax.execute({
                        url: _workspace.url + 'Note/note?action=Update',
                        type: 'PUT',
                        dataType: 'json',
                        data: JSON.stringify(inputDTO),
                        contentType: 'application/json; charset=UTF-8',
                        async:false,
                        success: function(data) {
                               data = JSON.parse(data)                        
                               if(typeof callback === "function"){
                                  callback();
                               }
                        }
             })
    }
    let notePermissionDenyUpdate = function(note_id, p_id, callback){

          var inputDTO = {
                 "dto": {
                        "WS_ID": ws_id,
                        "CH_TYPE": "CHN0003",
                        "USER_ID": JSON.parse(sessionStorage.getItem('userInfo')).userId,
                          "note_id": note_id,
                          "parent_notebook": p_id,
                          "is_edit":JSON.parse(sessionStorage.getItem('userInfo')).userId,
                          "is_favorite": "none",
                          "TYPE": "PERM_DENY",
                          "note_channel_id" : noteChannel_Id
                 }
          }
          Top.Ajax.execute({
               url: _workspace.url + 'Note/note?action=Update',
               type: 'PUT',
               dataType: 'json',
               data: JSON.stringify(inputDTO),
               contentType: 'application/json; charset=UTF-8',
               async:false,
               success: function(data) {
                     data = JSON.parse(data)
                     if(typeof callback === "function"){
                            callback()
                     }
               }
          })           
   }
  
    let permissionRequestUpdate = function(note_id, p_id, is_edit, callback){
              var inputDTO = {
                     "dto": {
                      "WS_ID": ws_id,
                      "CH_TYPE": "CHN0003",
                      "USER_ID": JSON.parse(sessionStorage.getItem('userInfo')).userId,
                      "note_id": note_id,
                      "parent_notebook": p_id,
                      "is_favorite": JSON.parse(sessionStorage.getItem('userInfo')).userId,
                      "is_edit": is_edit,
                      "TYPE": "PERM_REQ",
                      "note_channel_id" : noteChannel_Id
                     }
              }
              Top.Ajax.execute({
                      url: _workspace.url + 'Note/note?action=Update',
                      type: 'PUT',
                      dataType: 'json',
                      data: JSON.stringify(inputDTO),
                      contentType: 'application/json; charset=UTF-8',
                      async:false,
                      success: function(data) {
                            data = JSON.parse(data)
                            if(typeof callback === "function"){
                                   callback()
                            }
                      }
              })           
       }
    // 완전임시 개임시
    let getNoteData = function(note_id, callback){
       let targetIndex;
       Note.allnoteList(noteChannel_Id, function(data){
              for(let i=0; i<data.dto.noteList.length; i++){
                     if(data.dto.noteList[i].note_id === note_id)
                           targetIndex = i;
              }
              if(typeof callback === "function"){
                     callback(data.dto.noteList[targetIndex])
              }
       })     
    }
    let noteFileCreate = function(FileInput,note_id){
        var inputDTO = {
           "dto": {
                "WS_ID": ws_id,
                "CH_TYPE": "CHN0003",
                "USER_ID": JSON.parse(sessionStorage.getItem('userInfo')).userId,
                "file_id": FileInput.file_id,
                "note_id": note_id,
                "file_name": FileInput.file_name,
                "file_size": FileInput.file_size,
                "file_extension": FileInput.file_extension,
                "file_created_at": FileInput.file_created_at,
                "file_last_update_user_id" : FileInput.file_last_update_user_id,
//                "file_last_update_user_name": FileInput.file_last_update_user_name
           }
        }
        Top.Ajax.execute({
	        url: _workspace.url + 'Note/noteFile', 
	        type: 'POST',
	        dataType: 'json',
	        data: JSON.stringify(inputDTO),
	        contentType: 'application/json; charset=UTF-8',
	        async:false,
	        success: function(data) {
	        	data = JSON.parse(data)
	        	if(typeof callback === "function"){
	        		callback()
	        	}
	        }
		})
    }
    //11-02 file content download service
    let noteFileContentGet = function(file_id, ws_id, ch_id, user_id, callback){
    	Top.Ajax.execute({
			url: _workspace.url + 'Storage/StorageFile?fileID=' + file_id + '&workspaceID='+ ws_id + '&channelID=' + ch_id + '&userID=' + user_id,   
	        type: 'GET',
	        dataType: 'json',
	        contentType: 'application/json; charset=UTF-8',
	        async:false,
	        success: function(data) {
	        	data = JSON.parse(data);
	        	if(typeof callback === "function"){
	        		callback(data);
	        	}
	        }
		})
    }
    let noteFileMetaGet = function(file_id, ws_id, ch_id, user_id, callback){
    	Top.Ajax.execute({
			url: _workspace.url + 'Storage/StorageMeta?fileID=' + file_id + '&channelID=' + ch_id  +  '&userID=' + user_id +'&workspaceID='+ ws_id ,   
	        type: 'GET',
	        dataType: 'json',
	        contentType: 'application/json; charset=UTF-8',
	        async:false,
	        success: function(data) {
	        	data = JSON.parse(data);
	        	if(typeof callback === "function"){
	        		callback(data);
	        	}
	        }
		})
    }
	let noteFileList = function (noteChannel_Id, callback){
    	Top.Ajax.execute({
			url: _workspace.url + 'Note/noteFile?action=List&WS_ID=' + getWorkspaceId() + '&note_channel_id=' + noteChannel_Id + "&t=" + new Date().getTime().toString(),  
	        type: 'GET',
	        dataType: 'json',
	        contentType: 'application/json; charset=UTF-8',
	        async:false,
	        success: function(data) {
	        	data = JSON.parse(data)
	        	if(typeof callback === "function"){
	        		callback(data)
	        	}
	        }
		})
	}	
	let noteFileDelete = function (fileId, callback){
		var _inputDTO = {
			"dto" : {
				"workspace_id": routeManager.getWorkspaceId(),
		        "channel_id": noteChannel_Id,
		        "storageFileInfo": {
		        "user_id": "",
		        "file_last_update_user_id": "",
		        "file_id": fileId,
		        "file_name": "",
		        "file_extension": "",
		        "file_created_at": "",
		        "file_updated_at": "",
		        "user_context_1": "",
		        "user_context_2": "",
		        "user_context_3": ""
		        }
			}
		}
    	Top.Ajax.execute({
	        url: _workspace.url + 'Storage/StorageFile', 
	        type: 'DELETE',
	        dataType: 'json',
	        data: JSON.stringify(_inputDTO),
	        contentType: 'application/json; charset=UTF-8',
	        async:false,
	        success: function(data) {
	        	data = JSON.parse(data)
	        	if(typeof callback === "function"){
	        		callback()
	        	}
	        }
	    })
	}
	let noteAppDelete = function(noteChannel_Id){
		var _inputDTO = {
			"dto" : {
				"note_channel_id" : noteChannel_Id
			}
		}
		Top.Ajax.execute({
	        url: _workspace.url + 'Note/noteapp?action=Delete', 
	        type: 'POST',
	        dataType: 'json',
	        data: JSON.stringify(_inputDTO),
	        contentType: 'application/json; charset=UTF-8',
	        async:false,
	        success: function(data) {
	        	data = JSON.parse(data)
	        }
	    })
	}
	let addChildNumOnTable = function(tableList){
		for(let i=0;i<tableList.length;i++){
			if(tableList[i].type==="notebook"){
				Note.noteList(tableList[i].id, function(_data){
					if(_data.dto.noteList.length!== 0){
						tableList[i].table_title = tableList[i].text + " ("+ _data.dto.noteList.length+ ")"
					} else {
						tableList[i].table_title = tableList[i].text
					}
					
				})
			} else { // stack
				if(tableList[i].children.length !==0){
					tableList[i].table_title = tableList[i].text + " ("+tableList[i].children.length+ ")"
				}  else {
					tableList[i].table_title = tableList[i].text
				}
			}
		}
		return tableList
	}
	
	function noteSystemMessage (title, onOpenConfig) {
		let noteCommonDialog = UTIL.ID('noteSystemMessageDialog');
		noteCommonDialog.open();
		noteCommonDialog.setProperties({
	        class: "popup_02 move-x-button",
	        title: title,
	        onOpen: function(){
	        	UTIL.ID('noteSystemMessageOkButton').setProperties({ 
	        		'on-click' : function(){
	        			UTIL.ID('noteSystemMessageDialog').close(true); 
	        		}
        		});
	        	onOpenConfig();
	        }
		})
	}
	
	function emptyFileQueue () {
		if(noteFileArray.length !== 0){
			for(let i=0; i<noteFileArray.length; i++){
				Note.noteFileDelete(noteFileArray[i]);
			}
			noteFileArray = [];
		}
	}
	
	let noteInfoList = function(note_id, callback){

		Top.Ajax.execute({
	        url: _workspace.url + 'Note/noteinfo?action=List&note_id=' + note_id + '&note_channel_id=' + noteChannel_Id + "&t=" + new Date().getTime().toString(), 
	        type: 'GET',
	        dataType: 'json',
	        contentType: 'application/json; charset=UTF-8',
	        async:false,
	        success: function(data) {
	        	data = JSON.parse(data);
	            if(typeof callback === "function"){
	            	callback(data.dto.noteList[0])
	            }                
	        }
	    })
	}
	let moveToTrash = function( delNoteList, callbackRenderFunction ) {
		let deleteFailed;
		deleteFailed = noteTrash( delNoteList, callbackRenderFunction )
		setTimeout(function(){
			if(deleteFailed.length > 0){
				if(deleteFailed.length === 1){
					notiFeedback("다른 유저가 페이지를 수정 중입니다.");
				} else {
					notiFeedback("다른 유저가 "+ deleteFailed.length +"개의 페이지를 수정 중입니다.");
				}
			}
		},1)
	}
	

	
       return{
//            renderDOM:renderDOM,
    	  noteAppDelete:noteAppDelete,
//		  i18nLoad:i18nLoad,
		  noteBookCreate : noteBookCreate,
		  noteBookList : noteBookList,
		  bookList:bookList,
		  noteBookDelete:noteBookDelete,
		  noteBookUpdate:noteBookUpdate,
		  noteCreate:noteCreate,
		  handleWebSocketMessage:handleWebSocketMessage,
		  noteList:noteList,
		  noteUpdate:noteUpdate,
		  noteTrash:noteTrash,
		  noteTrashList:noteTrashList,
		  noteTrashDelete:noteTrashDelete,
		  noteDelete:noteDelete,
		  allnoteList:allnoteList,
		  tagCreate:tagCreate,
		  tagList:tagList,
		  tagDelete:tagDelete,
		  alltagList:alltagList,
		  tagSortList:tagSortList,
		  tagNoteList:tagNoteList,
		  noteFavoriteCreate:noteFavoriteCreate,
		  noteFavoriteList:noteFavoriteList,
		  noteFavoriteDelete:noteFavoriteDelete,         
		  shareWSList:shareWSList,
		  noteShareCreate:noteShareCreate,
		  noteShareUpdate:noteShareUpdate,
		  noteParentUpdate:noteParentUpdate,
		  checkDOM:checkDOM,
//		  noteTrashUpdate:noteTrashUpdate,
		  noteRestoreUpdate:noteRestoreUpdate,
		  noteEditUpdate:noteEditUpdate,
		  noteNonEditUpdate:noteNonEditUpdate,
		  noteEditOnClose:noteEditOnClose,
		  removeErasedNote:removeErasedNote,
		  noteBookParentUpdate:noteBookParentUpdate,
		  stackList:stackList,
//		  noteFavoriteUpdate:noteFavoriteUpdate,
//		  noteMoveUpdate:noteMoveUpdate,
		  permissionRequestUpdate:permissionRequestUpdate,
		  notePermissionDenyUpdate:notePermissionDenyUpdate,
		  notePermissionAcceptUpdate:notePermissionAcceptUpdate,
		  getNoteData:getNoteData,
		  noteFileCreate:noteFileCreate,
		  noteFileList:noteFileList,
		  noteFileDelete:noteFileDelete,
		  addChildNumOnTable:addChildNumOnTable,
		  noteFileContentGet:noteFileContentGet,
		  noteFileMetaGet:noteFileMetaGet,
		  noteSystemMessage:noteSystemMessage,
		  emptyFileQueue:emptyFileQueue,
		  noteInfoList:noteInfoList,
		  moveToTrash:moveToTrash
       }
}())

TEESPACE_WEBSOCKET.addWebSocketHandler("CHN0003", Note.handleWebSocketMessage);

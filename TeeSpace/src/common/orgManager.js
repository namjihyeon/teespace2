OrgManager = function () {
	function OrgManager() {}
	
	let orgList = {};
	let orgMetaList = {};
	let orgCallbackList = {};
	
	/**
	 * 초기 변수
	 */
	OrgManager.List = {};
	OrgManager.MetaList = {};
	OrgManager.Callback = {};
	
	/**
	 * 메소드 
	 */
	OrgManager.reload = function (url, type) {
		const self = this;
		return new Promise(function (resolve, reject) {
			return axios.post(_workspace.url + 'Admin/OrgTree?action=Get', {
	    		dto: {
	    			ORG_URL: url,
	    			ORG_TYPE: type
	    		}
	    	}).then(function (res) {
	    		let data = res.data.dto.orgList;
	    		orgList[type] = data;
	    		resolve(data);
	    	}).catch(reject);
		});
	}
	
	// 조직도 리스팅
	OrgManager.get = function (url, type) {
		const self = this;
		return new Promise(function (resolve, reject) {
			if (orgList.hasOwnProperty(type) === false || orgList[type].length === 0) {
				return axios.post(_workspace.url + 'Admin/OrgTree?action=Get', {
		    		dto: {
		    			ORG_URL: url,
		    			ORG_TYPE: type
		    		}
		    	}).then(function (res) {
		    		let data = res.data.dto.orgList;
		    		orgList[type] = data;
		    		resolve(data);
		    	}).catch(reject);
			} else {
				resolve(orgList[type]);
			}
		});
	}
	
	// 조직도 생성
	OrgManager.create = function (parentOrg, data) {
		return axios.post(_workspace.url + 'Admin/Org?action=Create', {
    		dto: {
    			orgList: [
    				data
    			]
    		}
    	}).then(function (res) {
    		create(parentOrg, data);
    		    		
    		return res;
    	});
	}
	
	// 조직도 업데이트
	OrgManager.update = function (data) {
		update(data);
		return false;
		return axios.post(_workspace.url + 'Admin/Org?action=Update', {
    		dto: {
    			orgList: [
    				data
    			]
    		}
    	}).then(function (res) {
    		update(data);
    		    		
    		return res;
    	});
	}
	
	// 조직도 삭제
	OrgManager.remove = function (data) {
		return axios.post(_workspace.url + 'Admin/Org?action=Delete', {
    		dto: {
    			orgList: [
    				data
    			]
    		}
    	}).then(function (res) {
    		remove(data);
    		    		    		
    		return res;
    	});
	}
	
	// 직위, 직책 등 가져오기
	OrgManager.metaGet = function (company_code) {
		const self = this;
		return new Promise(function (resolve, reject) {
			if (orgMetaList.hasOwnProperty(company_code) === false || orgMetaList[company_code] === null) {
				return axios.post(_workspace.url + 'Admin/OrgMetaList?action=Get', {
		    		dto: {
		    			USER_AFF: company_code
		    		}
		    	}).then(function (res) {
		    		let data = res.data.dto.comProfileMetaList;
		    		orgMetaList[company_code] = data;
		    		resolve(data);
		    	}).catch(reject);
			} else {
				resolve(orgMetaList[company_code]);
			}
		});
	}
	
	OrgManager.List.create = function (parentOrg, data) {
		let parent = findOrg(orgList[data.ORG_TYPE], parentOrg.COMPANY_CODE, parentOrg.DEPARTMENT_CODE);
		
		if (parent.CHILDREN_ORG === null) {
			parent.CHILDREN_ORG = {
				orgList: []
			}
		}
		
		const addedData = {
				COMPANY_CODE: data.COMPANY_CODE,
				DEPARTMENT_CODE: data.DEPARTMENT_CODE,
				ORG_NAME: data.ORG_NAME,
				ADMIN_NAME: data.ADMIN_NAME,
				PARENT_ORG_DEPARTMENT_CODE: data.PARENT_ORG_DEPARTMENT_CODE,
				ORG_TYPE: data.ORG_TYPE,
				IS_PUBLIC: data.IS_PUBLIC,
				ORG_COMMENT: data.ORG_COMMENT,
				CHILDREN_ORG: null,
				ORG_URL: parent.ORG_URL,
				ORG_LEVEL: parseInt(parent.ORG_LEVEL) + 1,
				USER_COUNT: '0'
		};
		
		parent.CHILDREN_ORG.orgList.splice(0, 0, addedData);
		
		// 소팅
		sortOrgByName(parent.CHILDREN_ORG.orgList);
	}
	
	OrgManager.List.update = function (data) {
		let org = findOrg(orgList[data.ORG_TYPE], data.COMPANY_CODE, data.DEPARTMENT_CODE);
		
		// null값 제거 
		let keys = Object.keys(data);
		for (let i = 0 ; i < keys.length ; i++) {
			if (data[keys[i]] === null) {
				delete data[keys[i]];
			}
		}

		// 조직도 이동이 필요한가?
		if (data.hasOwnProperty('PARENT_ORG_DEPARTMENT_CODE')) {
			// 삭제 
			OrgManager.List.remove({
				COMPANY_CODE: data.COMPANY_CODE,
				DEPARTMENT_CODE: data.DEPARTMENT_CODE,
				ORG_TYPE: data.ORG_TYPE
			});
			
			// 이동
			let movoToParent = findOrg(orgList[data.ORG_TYPE], data.COMPANY_CODE, data.PARENT_ORG_DEPARTMENT_CODE);
			if (movoToParent.CHILDREN_ORG === null) {
				movoToParent.CHILDREN_ORG = {
					orgList: []
				}
			}
			movoToParent.CHILDREN_ORG.orgList.splice(0, 0, org);
		}
		
		// 정보 업데이트
		keys = Object.keys(data);
		for (let i = 0 ; i < keys.length ; i++) {
			org[keys[i]] = data[keys[i]];
		}
		
		// 부서 코드가 변경된다면
		if (data.hasOwnProperty('TRANSFER_DEPARTMENT_CODE')) {
			org['DEPARTMENT_CODE'] = data['TRANSFER_DEPARTMENT_CODE']
		}
		
		if(data.ORG_LEVEL === '0')
			return;
		
		// 최종 부모 객체를 얻고 이름순 정렬
		let parent = findParentOrg(orgList[data.ORG_TYPE], data.COMPANY_CODE, data.DEPARTMENT_CODE);
		if(parent) sortOrgByName(parent.CHILDREN_ORG.orgList);
	}
	
	OrgManager.List.remove = function (data) {
		let parent = findParentOrg(orgList[data.ORG_TYPE], data.COMPANY_CODE, data.DEPARTMENT_CODE);
		
		parent.CHILDREN_ORG.orgList = parent.CHILDREN_ORG.orgList.filter(function (v) {
			return (v.COMPANY_CODE !== data.COMPANY_CODE) || (v.DEPARTMENT_CODE !== data.DEPARTMENT_CODE); 
		});
	}
	
	OrgManager.List.getParentOrg = function (org_type, company_code, department_code) {
		return findParentOrg(org_type, company_code, department_code);
	}
	
	OrgManager.Callback.addEventListener = function (name, callback) {
		orgCallbackList[name] = callback;
	}
	
	OrgManager.Callback.removeEventListener = function (name) {
		delete orgCallbackList[name];
	}
	
	OrgManager.Callback.invoke = function (name, data) {
		if (orgCallbackList.hasOwnProperty(name)) {
			orgCallbackList[name](data);
		}
	}
	
	function findOrg(object, company_code, department_code) {
	    if(object['COMPANY_CODE'] === company_code && object['DEPARTMENT_CODE'] === department_code) {
	        return object;
	    }

	    for (let i = 0 ; i < Object.keys(object).length ; i++) {
	        if (object[Object.keys(object)[i]] !== null && typeof object[Object.keys(object)[i]] === "object") {
	        	let org = findOrg(object[Object.keys(object)[i]], company_code, department_code);
	        	if (org !== null) return org;
	        }
	    }
	    
	    return null;
	}
	
	function findParentOrg(object, company_code, department_code) {
		if (Array.isArray(object) === false && typeof object === 'object' && object.hasOwnProperty('CHILDREN_ORG') && object.CHILDREN_ORG !== null) { 
			let filtered = object.CHILDREN_ORG.orgList.filter(function (v) {
				return v.COMPANY_CODE === company_code && v.DEPARTMENT_CODE === department_code; 
			});
			
			if (filtered.length > 0) {
				return object;
			}
		}

	    for (let i = 0 ; i < Object.keys(object).length ; i++) { 
	    	if (object[Object.keys(object)[i]] !== null && typeof object[Object.keys(object)[i]] === "object") {
	        	let org = findParentOrg(object[Object.keys(object)[i]], company_code, department_code);
	        	if (org !== null) {
	        		return org;
	        	}
	        }
	    }
	    
	    return null;
	}
	
	function sortOrgByName(orgList) {
		orgList.sort(function(a, b) {
		    return a.ORG_NAME < b.ORG_NAME ? -1 : a.ORG_NAME > b.ORG_NAME ? 1 : 0;
		});
	}
	
	return OrgManager;
}();

function noti_mail(data) {
	if(data.NOTI_ETC == '' || data.NOTI_ETC == null || data.NOTI_ETC == undefined) {
		
		if(data.NOTI_TYPE == 'outbound') {
			var status = data.NOTI_MSG.split('/')[0];
			var folderId = data.NOTI_MSG.split('/')[1];
			var mailId = data.NOTI_MSG.split('/')[2];
			if(mailData.curBox.FOLDER_ID == folderId && mailData.curBox.WIDGET_ID.split('Text')[1].substr(0,1) != '0'
				&& Top.Dom.selectById('mailTwoPanelLayout_Table') != null) {
				var mailList = mailDataRepo.mailReceivedBoxDataList;
				for(var i=0; i<mailList.length; i++) {
					if(mailList[i].MAIL_ID == mailId) {
						mailList[i].STATUS = status;
						Top.Dom.selectById('mailTwoPanelLayout_Table').render();
						return;
					}
				}
				
			}
		} else if(Top.Controller.get('mailMainLeftLogic') != undefined){
			if(JSON.parse(data.NOTI_MSG).STATE == undefined){
				console.log(JSON.parse(data.NOTI_MSG).STATE)
				Top.Controller.get('mailMainLeftLogic').checkExternalAccount(data,true);
			}
			else{
				console.log(JSON.parse(data.NOTI_MSG).STATE)
				if(JSON.parse(data.NOTI_MSG).REQUEST_TYPE == "make" ){
					if(Top.Controller.get("mailMainLeftLogic").pop3requestCount < 2){
						Top.Controller.get("mailMainLeftLogic").checkExternalAccount(data)
					}
					else{
						Top.Controller.get('mailMainLeftLogic').checkExternalAccount(data,true);
//						Top.Controller.get("mailThreePanelMainLogic").setMailCountPageLength()
					}
					Top.Controller.get("mailMainLeftLogic").pop3requestCount ++;
				}
				else{
					Top.Controller.get('mailMainLeftLogic').checkExternalAccount(data);
				}
				
			}
			
		}
			
		Top.Controller.get('mailMainLeftLogic').resetLnbMailCount();
		return;
	}
	if(workspaceManager.getMySpaceId() == data.NOTI_TARGET && getMainAppByUrl() == 'mail') {
		Top.Controller.get('mailMainLeftLogic').fromNoti = true;
		Top.Controller.get('mailMainLeftLogic').refreshCurrentList();
		Top.Controller.get('mailMainLeftLogic').resetCount();
	}else{
		Top.Controller.get('mailMainLeftLogic').resetLnbMailCount();
	}
	
	data.WS_ID = data.NOTI_TARGET;
	data.SPACE_ID = data.WS_ID;
//	data.NOTI_TITLE = 'T-Mail | ' + workspaceManager.getWorkspaceName(data.SPACE_ID) + ' | ' + talkUtil.getUserInfoSync(data.USER_ID)["USER_NAME"];
	data.NOTI_TITLE = data.NOTI_TITLE;	//제목 서비스 반영중
	data.NOTI_ETC = JSON.parse(data.NOTI_MSG).FOLDER_ID + '/' + JSON.parse(data.NOTI_MSG).MAIL_ID;
 
	var tmp = JSON.parse(data.NOTI_MSG).CONTENT;
	tmp = tmp.replace(/&nbsp;/gi, ' ');
	tmp = tmp.replace(/<(\/?)p>/gi,'');
	tmp = tmp.replace(/\s/gi,'');
	tmp = tmp.replace(/&nbsp;/gi,'');
	tmp = tmp.replace(/<head>(.*?)<(\/?)head>/gi,'');
	tmp = tmp.replace(/<style>(.*?)<(\/?)style>/gi,'');
	tmp = tmp.replace(/<(\/?)body>/gi,'');
	data.NOTI_MSG =tmp;
	console.log(data.NOTI_MSG);
	NotificationManager.notify( data, CS_Alarm_Config.TMAIL.ALL);//(data, null, data.NOTI_TYPE);
	
};

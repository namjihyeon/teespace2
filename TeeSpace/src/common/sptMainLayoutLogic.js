Top.Controller.create('sptMainLayoutLogic', {
	lnbButtonList: [
		{
			id: 'lnbButtonHome',
			parent: 'root',
			text: '홈',
			type: 'home',
			hash: '/spt/home',
		},
		{
			id: 'lnbButtonHelp',
			parent: 'root',
			text: '도움말',
			type: undefined,
			hash: undefined,
		},
		{
			id: 'lnbButtonManual',
			parent: 'lnbButtonHelp',
			text: '사용자 매뉴얼',
			type: 'manual',
			hash: '/spt/manual',
		},
		{
			id: 'lnbButtonFaq',
			parent: 'lnbButtonHelp',
			text: '자주하는 질문(FAQ)',
			type: 'faq',
			hash: '/spt/faq?mode=list&cate=all&subcate=all&view=ss30&page=1',
		},
		{
			id: 'lnbButtonQna',
			parent: 'lnbButtonHelp',
			text: 'Q&A',
			type: 'qna',
			hash: '/spt/qna?mode=list&cate=all&subcate=all&view=ss30&page=1',
		},
		{
			id: 'lnbButtonOneonone',
			parent: 'lnbButtonHelp',
			text: '1:1 문의',
			type: 'oneonone',
			hash: '/spt/oneonone?mode=list&cate=all&subcate=all&view=ss30&page=1',
		},
		{
			id: 'lnbButtonAnn',
			parent: 'root',
			text: '공지',
			type: undefined,
			hash: undefined,
		},
		{
			id: 'lnbButtonServiceAnn',
			parent: 'lnbButtonAnn',
			text: '서비스 공지',
			type: 'service_announcement',
			hash: '/spt/service_announcement?mode=list&cate=all&subcate=all&view=ss30&page=1',
		},
		{
			id: 'lnbButtonUpdateAnn',
			parent: 'lnbButtonAnn',
			text: '업데이트 공지',
			type: 'update_announcement',
			hash: '/spt/update_announcement?mode=list&cate=all&subcate=all&view=ss30&page=1',
		},
		//    {
		//        'id': 'lnbButton09',
		//        'parent': 'root',
		//        'text': '요금제 관리',
		//        'type': 'plan',
		//        'hash': '/spt/plan',
		//    }
	],
	init: function init(event, widget) {
		var that = this;
		that.setLogoImg();
		that.setProfileImg();
		// B2B -> QNA
		// B2C -> ONEONONE
		const lnbButtonList = [
			...that.lnbButtonList.filter(
				(elem) => !((isb2c() && elem.type === 'qna') || (!isb2c() && elem.type === 'oneonone'))
			),
		];
		// lnb 생성
		$('div#lnbLL').empty();
		Top.Controller.get('components').simpleLnb({
			id: 'supportLnb',
			list: lnbButtonList,
			onClick: that.onClickLnb,
			targetId: 'lnbLL',
			type: 1,
		});
	},
	onClickLnb: function (event, widget) {
		//		mobileLnbHide('div#mainLL', 'div#lnbLL'); // 모바일 lnb 숨기기
		var that = Top.Controller.get('sptMainLayoutLogic');
		Top.App.routeTo(widget.getValue());
	},
	setLogoImg: function () {
		$$$('logoImage').setProperties({
			'on-click': function (event, widget) {
				var spt = JSON.parse(sessionStorage.getItem('spt'));
				console.log(spt);
				if (!!spt && !!spt.prevPage) {
					var prevPage = spt.prevPage;
					if (!prevPage.includes('/spt')) {
						Top.App.routeTo(prevPage);
					} else if (prevPage.includes('/login')) {
						appManager.setSubApp(null);
						Top.App.routeTo('/f/' + userManager.getLoginUserInfo().USER_LOGIN_ID);
					} else {
						Top.App.routeTo(prevPage);
					}
				} else {
					appManager.setSubApp(null);
					Top.App.routeTo('/f/' + userManager.getLoginUserInfo().USER_LOGIN_ID);
				}
			},
		});
	},
	setProfileImg: function () {
		var userId = userManager.getLoginUserId();
		$$$('titleProfile').setSrc(
			userManager.getUserPhoto(userId, 'small', userManager.getUserInfo(userId).THUMB_PHOTO)
		);

		$(document)
			.off()
			.on('click', function (e) {
				spt_more_list(e, 'titleProfile', 'sptMainLayout');
				//			$('div#LinearLayout4114 .top-treeview-edit_btn').removeClass('selected');
				//			$('div#LinearLayout4114 .top-treeview-edit_nav').css({ display: 'none' });
				//			$('div#LinearLayout4114 .top-treeview-edit_btn').css({ visibility: 'hidden' });
			});
	},
	onClickLogout: function (event, widget) {
		userManager.logout();
	},
});

function spt_more_list(event, clickID, viewID) {
	var layoutId = 'sptMainLayout';
	var logicId = 'sptMainLayoutLogic';

	if (event.toElement !== undefined) {
		var option = {
			id: 'Sidebar_drop1asdf',
			button: {
				list: [
					{
						text: '로그아웃',
						id: 'Sidebar_drop1_Logout',
						onClick: Top.Controller.get(logicId).onClickLogout,
					},
					//					{
					//						text: '고객지원',
					//						id: 'Sidebar_drop1_Support',
					//						onClick: function() {},
					//					},
				],
			},
		};

		if (event.toElement.id === clickID) {
			more_list =
				'<top-linearlayout id="' +
				option.id +
				'" class="dropdown-list linear-child-vertical" orientation="vertical" layout-tab-disabled="false">' +
				'<div id="' +
				option.id +
				'" class="top-linearlayout-root">';
			option.button.list.forEach((elem) => {
				more_list +=
					'<top-button id="' +
					elem.id +
					'" class="noti_drop linear-child-vertical">' +
					'<button id="' +
					elem.id +
					'" class="top-button-root button" type="button" tabindex="0">' +
					'<label class="top-button-text">' +
					elem.text +
					'</label>' +
					'</button>' +
					'</top-button>';
			});
			more_list += '</div>' + '</top-linearlayout>';

			if ($('top-linearlayout#' + option.id).length === 0) {
				if (viewID != layoutId) {
					$('#' + viewID + ' .content_0').addClass('mobile-noti_split');
				}
				more_list = $(more_list);
				option.button.list.forEach((elem) => more_list.find('#' + elem.id).on('click', elem.onClick));
				//				more_list.find('#Sidebar_drop1_Logout').on('click', Top.Controller.get(logicId).onClickLogout);

				//계정 설정
				$('div#' + layoutId).append(more_list);
			} else {
				if (viewID !== layoutId) {
					$('#' + viewID + ' .content_0').removeClass('mobile-noti_split');
				}
				$('top-linearlayout#' + option.id).remove();
			}
		} else {
			if ($('top-linearlayout#' + option.id).length !== 0) {
				$('#' + viewID + ' .content_0').removeClass('mobile-noti_split');
				$('top-linearlayout#' + option.id).remove();
			}
		}
	}
}

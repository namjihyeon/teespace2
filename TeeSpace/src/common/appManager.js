const appManager = (function() {
    const apps = [null, "note", "drive", "toword", "tocell", "topoint",  "meeting", "schedule", "plus"];	 //config 파일로? 
    const mains = ["talk", "mail"];
    const types = {
	    			close	: { eventType: "close" , ratio: "1:0" },
	    			fold	: { eventType: "fold"  , ratio: "5:7" },			// sub activated	
	        		expand 	: { eventType: "expand", ratio: "0:1" }				// sub expanded
    }

    // 초기값 man : talk / sub : null
    let _main = "m"===getLnbByUrl()? "mail" : "talk";//mains.includes(getMainAppByUrl())? getMainAppByUrl() : "talk";	
    let _sub = apps.includes(getMainAppByUrl())? getMainAppByUrl() : ( getSubAppByUrl()?  getSubAppByUrl().split("?")[0] : null ); 	

    let _isExpanded = false;
    let _beforeExpand = {};

    const getMainApp = function() {
        return _main;
    };

    const setMainApp = function(appName) {
        if (mains.includes(appName)) _main = appName;
    };

    const getSubApp = function() {
        return _sub;
    };

    const setSubApp = function(appName) {
        if (apps.includes(appName)) _sub = appName;
    };

    const updateExpandState = function(value) {
        if (value.constructor.name === "Boolean") _isExpanded = value;
    };
    
    // 현재 url 기준으로 리턴하는 방식은?
    const isExpanded = function() {
//        return _isExpanded;
    	let main = getMainAppByUrl();
    	let sub = getSubAppByUrl();
    	let type = getType(main, sub);
    	if("expand" === type.eventType) return true;
    	else return false;
    };
    
    // 기준 - url
    const getType =  function(inMain, inSub){
    	if(!inSub){
    		if(!inMain) return types["close"];													// no input - friends
    		else if(mains.includes(inMain)) return types["close"];								// || mains.includes("mail")
    		else return types["expand"];														
    	}else{
    		return types["fold"];																
    	}
    };
 
    return {
    	getMains: () => mains,
    	getApps: () => apps, 
        getMainApp,
        getSubApp,
        setMainApp,
        setSubApp,
        updateExpandState,
        isExpanded,
        getType,
    };
})();


window.onhashchange = hashChanged;
function hashChanged(event){
//	let old = event.oldURL;
	let now = event.newURL;
	let hash = window.location.hash;
	let mainUrl = getMainAppByUrl(hash);
	let subUrl = getSubAppByUrl(hash);
	let mainEntry = appManager.getMains();
	let subEntry = appManager.getApps();

	
	if(mainEntry.includes(mainUrl)){
		appManager.setMainApp(mainUrl);
		appManager.setSubApp(subUrl);
	}
	else if(subEntry.includes(mainUrl)){															//appEntry에 null도 포함 - 예외처리 필요
		appManager.setSubApp(mainUrl);
	}
}

(function(exports){
	
	 let _beforeExpand = {};
	
	 //[공통] 확장된거 접는 버튼
	function onFoldButtonClick(appName, preRoute) {
	    // main app은 바뀌지 않는다. 
	    appManager.setSubApp(appName);
	    appManager.updateExpandState(false);
	
	    if (preRoute && typeof preRoute === "function") {
	        preRoute();
	    }
	
	    const lnbUrl = getLnbByUrl();
		const roomId = getRoomIdByUrl();
    	const target = "m" === lnbUrl? "mail" : roomId;
    	const targetUrl = _beforeExpand[target];
    	const time = getQueryStringByKey("q")? getQueryStringByKey("q") : getTimeStamp();
    	const sub = `?sub=${appManager.getSubApp()}&q=${time}`;
    	
    	if(targetUrl){
        	const main = getMainAppFullUrl(targetUrl);
        	Top.App.routeTo(`/${lnbUrl}/${roomId}/${main}${sub}`, { eventType: "fold" }); 
        	_beforeExpand[target] = null;
    	}
    	else{
    		Top.App.routeTo(`/${lnbUrl}/${roomId}/${appManager.getMainApp()}${sub}`, { eventType: "fold" });
    	}
	}
	
	// [공통] 확장 하기 버튼
	function onExpandButtonClick(appName, preRoute) {
	    // main app은 바뀌지 않는다.
	    appManager.setSubApp(appName);
	    appManager.updateExpandState(true);
	
	    if (preRoute && typeof preRoute === "function") {
	        preRoute();
	    }
	
    	const lnbUrl = getLnbByUrl();
    	const roomId = getRoomIdByUrl();
    	const target = "m" === lnbUrl? "mail" : roomId;
    	
    	_beforeExpand[target] = getSubAppByUrl(window.location.hash)? window.location.hash : _beforeExpand[target]; 
        
        const mySpace = workspaceManager.getMySpaceUrl();
        const time = getQueryStringByKey("q")? getQueryStringByKey("q") : getTimeStamp(); 
        const subApp = appManager.getSubApp();
    	
    	Top.App.routeTo(`/${lnbUrl}/${roomId}/${subApp}?q=${time}`, { eventType: "expand" });
	}
	
	// [공통] 닫기 버튼
	function onCloseButtonClick(appName, preRoute) {
	    appManager.setSubApp(null);
	    appManager.updateExpandState(false);
	
	    if (preRoute && typeof preRoute === "function") {
	        preRoute();
	    }
	
	    const mains = appManager.getMains();
	    const lnbUrl = getLnbByUrl();
		const roomId = getRoomIdByUrl();
		const time = getQueryStringByKey("q")? getQueryStringByKey("q") : getTimeStamp();
    	const expandState = !mains.includes(getMainAppByUrl());
    	//확장 O
    	if(expandState){
    		const target = "m" === lnbUrl? "mail" : roomId;
    		const targetUrl = _beforeExpand[target];
    		if(targetUrl) {
    			const main = getMainAppFullUrl(targetUrl);
    			Top.App.routeTo(`/${lnbUrl}/${roomId}/${main}?q=${time}`, { eventType: "close" });
    			_beforeExpand[target] = null;
    		}
    		else{
    			Top.App.routeTo(`/${lnbUrl}/${roomId}/${appManager.getMainApp()}?q=${time}`, { eventType: "close" });
    		}
    	}
    	//확장 X
    	else{     	
    		const main = getMainAppFullUrl();
			Top.App.routeTo(`/${lnbUrl}/${roomId}/${main}?q=${time}`, { eventType: "close" });
    	}
	}
	
	this.onFoldButtonClick = onFoldButtonClick;
	this.onExpandButtonClick = onExpandButtonClick;
	this.onCloseButtonClick = onCloseButtonClick; 
})(this);

//window.onpopstate = popstated;
//function popstated(event){
////	let old = event.oldURL;
//	let now = event.newURL;
//	let mainUrl = getMainAppByUrl(now);
//	let subUrl = getSubAppByUrl(now);
//	let mainEntry = appManager.getMains();
//	let subEntry = appManager.getApps();
//
//	
//	if(mainEntry.includes(mainUrl)){
//		appManager.setMainApp(mainUrl);
//		appManager.setSubApp(subUrl);
//	}
//	else if(subEntry.includes(subUrl)){															//appEntry에 null도 포함 - 예외처리 필요
//		appManager.setSubApp(mainUrl);
//	}
//}

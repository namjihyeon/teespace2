

function renderSubApp(data) {
    let ratio = "";
    let main = getMainAppByUrl();
    let sub = getSubAppByUrl();
    let type = appManager.getType(main, sub);

    let oldSpace = getRoomIdByUrl(historyList.getLast());
    let newSpace = getRoomIdByUrl(location.hash);
    let changed = oldSpace !== newSpace; // 최초 로드시 false

    if (changed && !meetingManager.data.disconnected) {
        meetingServiceCall.getUserNum().then((res) => {
            //console.log(res);
            meetingManager.stopLocalVideo();
            meetingManager.exitMeeting();
            if (res.data.dto.participantsNum === 1) {
                meetingServiceCall.DeleteRoom().then((res) => {
                    talkServer2.meetingTest(workspaceManager.getWorkspaceId(getRoomIdByUrl()), "", userManager.getLoginUserId(), meetingManager.data.myRoomId, false);
                    //console.log(res);
                });
            }
        });
    };
    if (changed && Meeting2Manager.isConnected()) {
        Meeting2Manager.stop();
    }
    //    if(sub) appManager.setSubApp(sub)
    /*  getType 이용 */

    //    if (data) {
    //        switch (data["eventType"]) {
    //            case "fold":
    //                ratio = "2:1";
    //                break;
    //            case "expand":
    //                ` = "0:1";
    //                break;
    //            case "close":
    //                ratio = "1:0";
    //                break;
    //        }
    //    } else {

    //        if (appManager.getSubApp()) {
    //            ratio = "2:1";
    //        } else {
    //            ratio = "1:0";
    //        }
    /* 0:1 인 경우 고려 필요? data를 항상 넘겨주나? */
    //    }

    /* 임시 - mousedown remove x */
    //    ['close', 'fold', 'expand'].forEach( function(type){ tds("AppSplitMainLayout").removeClass(type) });
    //    tds("AppSplitMainLayout").addClass(type.eventType);
    if ("fold" === type.eventType) tds('AppSplitMainLayout').addClass('limit');
    else tds('AppSplitMainLayout').removeClass('limit');
    tds("AppSplitMainLayout").setProperties({ ratio: type.ratio });

    if (appManager.getSubApp()) {
        const renderTarget = "AppSplitRightLayout";

        switch (appManager.getSubApp()) {
            case "mail": {
                // TODO : appWrapper (앱의 최상위 layout id) 바꿔주세요.
                const appWrapper = "mailLayout";
                const isRendered = !!document.querySelector(`div#${renderTarget} div#${appWrapper}`);

                if (!isRendered) {
                    // TODO : sub app rendering 로직 넣어주세요.
                    // ex) office.render(renderTarget);
                }
                break;
            }
            case "toword": {
                // TODO : appWrapper (앱의 최상위 layout id) 바꿔주세요.
                const appWrapper = "officeMainLayout";
                const isRendered = !!document.querySelector(`div#${renderTarget} div#${appWrapper}`);

                officeManager.renderOffice(renderTarget, appWrapper, appManager.getSubApp(), isRendered);
                break;
            }
            case "tocell": {
                // TODO : appWrapper (앱의 최상위 layout id) 바꿔주세요.
                const appWrapper = "officeMainLayout";
                const isRendered = !!document.querySelector(`div#${renderTarget} div#${appWrapper}`);

                officeManager.renderOffice(renderTarget, appWrapper, appManager.getSubApp(), isRendered);
                break;
            }
            case "topoint": {
                // TODO : appWrapper (앱의 최상위 layout id) 바꿔주세요.
                const appWrapper = "officeMainLayout";
                const isRendered = !!document.querySelector(`div#${renderTarget} div#${appWrapper}`);

                officeManager.renderOffice(renderTarget, appWrapper, appManager.getSubApp(), isRendered);
                break;
            }
            case "schedule": {
                // TODO : appWrapper (앱의 최상위 layout id) 바꿔주세요.
                // 서브앱인데 스케줄로 바꼈을시
                const appWrapper = "calendarLayout";
                const tsVersion = ``;

                let lastUrl = historyList.getLast();

                if (typeof lastUrl == 'undefined') { // 첫 로그인 시
                    $(`div#${renderTarget} div#${appWrapper}`).empty();
                    calendar.render(renderTarget, appWrapper, tsVersion);
                    break;
                }

                if (changed) { // 룸 변경 시
                    $(`div#${renderTarget} div#${appWrapper}`).empty();
                    calendar.render(renderTarget, appWrapper, tsVersion);
                    break;
                }
                let lastUrlTokens = lastUrl.split('/');
                let lastApp = lastUrlTokens[lastUrlTokens.length - 1];

                if (lastUrlTokens.includes('mail') && (lastApp.indexOf('sub=schedule') > 0)) {
                    lastApp = sub;
                }
				if (lastApp.indexOf('?') > 0) {
                    lastApp = lastApp.split('?')[0];
                }
                if (lastApp.indexOf('&') > 0) {
                    lastApp = lastApp.split('&')[0];
                }

                const isExpanded = appManager.isExpanded();
                const isRendered = !!document.querySelector(`div#${renderTarget} div#${appWrapper}`);
                appManager.updateExpandState(true);

                if (!isRendered) {
                    $(`div#${renderTarget} div#${appWrapper}`).empty();
                    calendar.render(renderTarget, appWrapper, tsVersion);
                } else {
                    if (!isExpanded) {
                        if (data === undefined) { // 첫 렌더나 새로고침 시
                            $(`div#${renderTarget} div#${appWrapper}`).empty();
                            calendar.render(renderTarget, appWrapper, tsVersion);
                        } else if (lastApp != sub) {
                            $(`div#${renderTarget} div#${appWrapper}`).empty();
                            calendar.render(renderTarget, appWrapper, tsVersion);
                        } else if (data.eventType === "fold" && !changed) { // 펼쳤다 닫았을 시
                            // 필터 캘린더 선택 visible
                            document.getElementById('cal-filter-select').style = 'display;';
                            document.getElementById('cal-filter-share').style = 'margin-bottom: 8px;';

                            // 확장/축소 버튼 변경되도록
                            tds('calendarIconLayout')
                                ? (tds('cal-iconlayout--collapse').setVisible('none'), tds('cal-iconlayout--expand').setVisible('visible'))
                                : (tds('cal-icon--collapse').setVisible('none'), tds('cal-icon--expand').setVisible('visible'));
                            calendar.redraw();
                            break;
                        }
                    } else {
                        // 필터 캘린더 선택 none
                        document.getElementById('cal-filter-select').style = 'display: none;';
                        document.getElementById('cal-filter-share').style = 'margin-bottom: 0px;';
                        tds('calendarIconLayout')
                            ? (tds('cal-iconlayout--collapse').setVisible('visible'), tds('cal-iconlayout--expand').setVisible('none'))
                            : (tds('cal-icon--collapse').setVisible('visible'), tds('cal-icon--expand').setVisible('none'));
                        calendar.redraw();
                    }
                }
                break;
            }
            case "talk": {
                // TODO : appWrapper (앱의 최상위 layout id) 바꿔주세요.
                const appWrapper = "talk";
                const isRendered = !!document.querySelector(`div#${renderTarget} div#${appWrapper}`);

                if (!isRendered) {
                    // TODO : sub app rendering 로직 넣어주세요.
                    // ex) talk.render(renderTarget);
                }
                break;
            }
            case "drive": {
                const workspaceId = workspaceManager.getWorkspaceId(getRoomIdByUrl(), "CHN0006");
                const channelId = workspaceManager.getChannelList(getRoomIdByUrl(), "CHN0006");
                const isRendered = !!document.querySelector(`div#${renderTarget} .drive[data-drive-id='${channelId}'][data-is-drive='true']`);

                if (!isRendered) {
                    $(`div#${renderTarget}`).empty();
                    drive.render(workspaceId, channelId, `div#${renderTarget}`, true);
                }
                break;
            }
            case "plus": {
                const workspaceId = workspaceManager.getWorkspaceId(getRoomIdByUrl(), "CHN0006");
                const channelId = workspaceManager.getChannelList(getRoomIdByUrl(), "CHN0006");
                const isRendered = !!document.querySelector(`div#${renderTarget} .drive[data-drive-id='${channelId}'][data-is-drive='false']`);

                if (!isRendered) {
                    $(`div#${renderTarget}`).empty();
                    drive.render(workspaceId, channelId, `div#${renderTarget}`, false);
                }
                break;
            }
            case "note": {
                const appWrapper = "note";
                const noteWS_ID = workspaceManager.getWorkspaceId(getRoomIdByUrl(), "CHN0003");
                const noteChannelId = workspaceManager.getChannelList(getRoomIdByUrl(), "CHN0003");
                const isExpanded = appManager.isExpanded();
                const isRendered = !!document.querySelector(`div#${renderTarget} #note[note-channel-id='${noteChannelId}']`);

                if (!isRendered) {
                    $(`div#${renderTarget}`).empty();
                    NoteApp.startTNote(noteWS_ID, noteChannelId, document.querySelector(`div#${renderTarget}`));
                } else {
                    if (!isExpanded) {
                        if (data === undefined) { // 첫 렌더나 새로고침 시
                            $(`div#${renderTarget}`).empty();
                            NoteApp.startTNote(noteWS_ID, noteChannelId, document.querySelector(`div#${renderTarget}`));
                        }
                        else if ( // 펼쳤다 닫았을 시
                            data !== undefined &&
                            data.eventType === "fold" &&
                            !changed
                        ) {
                            noteLnbMenu.hotfixFunction()
                            return;
                        }

                        else if (changed) { // 스페이스 변경 시
                            $(`div#${renderTarget}`).empty();
                            NoteApp.startTNote(noteWS_ID, noteChannelId, document.querySelector(`div#${renderTarget}`));
                        }
                    }
                }
                break;
            }
            case "meeting": {
                // TODO : appWrapper (앱의 최상위 layout id) 바꿔주세요.
                // const appWrapper = "meetingMainLayout";
                const appWrapper = "meetingMain";
                const isRendered = !!document.querySelector(`div#${renderTarget} div#${appWrapper}`);

                // if (!isRendered || meetingManager.data.disconnected) {
                //     // TODO : sub app rendering 로직 넣어주세요.
                //     // ex) meeting.render(renderTarget);
                //     tds(renderTarget).src("meetingMainLayout.html");

                // }
                if (!isRendered || !Meeting2Manager.isConnected()) {
                    // TODO : sub app rendering 로직 넣어주세요.
                    // ex) meeting.render(renderTarget);
                    $(`div#${renderTarget}`).empty();
                    const meetingChannelId = workspaceManager.getWorkspaceId(getRoomIdByUrl(), "CHN0007");
                    Meeting2Manager.render();

                    if (getSubAppByUrl() === 'meeting')
                        Meeting2Manager.start(meetingChannelId, MEETING_VIEW_TYPE.SIDE_BAR);
                    else if (getMainAppByUrl() === 'meeting')
                        Meeting2Manager.start(meetingChannelId, MEETING_VIEW_TYPE.EXPENDED);
                    //Meeting2Manager.start( meetingChannelId, MEETING_VIEW_TYPE.EXPENDED);

                } else {
                    if (getSubAppByUrl() === 'meeting')
                        Meeting2Manager.changeView(MEETING_VIEW_TYPE.SIDE_BAR);
                    else if (getMainAppByUrl() === 'meeting')
                        Meeting2Manager.changeView(MEETING_VIEW_TYPE.EXPENDED);
                }
                break;
            }
            default:
                break;
        }
    }
}

/**
 * router =>  /room/{roomId}/MainApp/{MainAppRenderOption}?sub=SubㄴApp&subOption={option}
 * */
Top.App.onLoad(function () {

    // rem 기준 (가로 길이/ 1366)
    $("html").css("fontSize", screen.width / 13.66 + "%");
    // default css
    $("body").addClass("ts-default");
    // drag and drop init
    dndRounter.init();

    //initUrl
    sessionStorage.setItem("initUrl", window.location.hash);

    function getRenderTarget(data) {
        let renderTarget = "AppSplitLeftLayout";
        if (data && data["eventType"] && data["eventType"] === "expand") renderTarget = "AppSplitRightLayout";

        return renderTarget;
    }
    // 라우팅
    let router = {
        "/admin": {
            activate: function (path, data, next) {
                if (!['admin', 'superadmin'].includes(userManager.getLoginUserInfo().userGrade)) {
                    userManager.logout();
                }
                if (next) {
                    next();
                } else {
                    admin.routeLayout();
                }

            },
            route: {
                "/:queryString": {
                    activate: function (path, data, next) {
                        admin.routeLayout(path);
                    }
                }
            }
        },

        "/spt": {
            activate: function (path, data, next) {
                if (!checkUserGrade()) return false;
                if (next) {
                    next();
                } else {
                    spt.routeLayout();
                }
            },
            route: {
                "/:queryString": {
                    activate: function (path, data, next) {
                        spt.routeLayout(path);
                    }
                }
            }
        },

        "/login": {
            activate: function (path, data, next) {
                if (!LOGOUT && userManager.getLoginUserInfo()) {
                    checkUserGrade("login");
                    return false;
                }
                let page = tds("MainTeeSpacePage");
                page.src("loginLayout.html" + verCsp());
                LOGOUT = true;
            }
        },

        "/privacy-policy": {
            activate: function (path, data, next) {
                let page = tds("MainTeeSpacePage");
                page.src("external/template/privacyPolicy.html");
                return false;
            }
        },

        "/term-and-conditions": {
            activate: function (path, data, next) {
                let page = tds("MainTeeSpacePage");
                page.src("external/template/termAndConditions.html");
                return false;
            }
        },

        /**
         * B2C 환경에서는 root 경로가 서비스 페이지
         */
        "/": {
            activate: function (path, data, next) {
                if (_validBtoc()) {
                    let page = tds("MainTeeSpacePage");
                    page.src("external/template/serviceMain.html", onServicePageLoaded);
                    return false;
                }
            }
        },

        "/:root": {
            activate: function (path, data, next) {
                if (!checkUserGrade()) return false;
                switch (true) {
                    case path.root === "f" || path.root === "s" || path.root === "m": {
                        // 친구, 스페이스 ,메일
                        if (isNewWindow()) {
                            if (next) next();
                        } else {
                            let page = tds("MainTeeSpacePage");
                            if (!tds("AppLayout")) {
                                page.src("AppLayout.html" + verCsp(), function () {
                                    if (next) next();
                                });
                            } else {
                                if (next) next();
                            }
                        }
                        break;
                    }
                    default: {
                    	window.location.replace("/503.html");
                    	return false;
                    }
                }
            },
            route: {
                "/:id": {
                    activate: function (path, data, next) {

                        let lnbType = Top.curRoute.params.root;
                        let url = Top.curRoute.params.id;
                        let id = workspaceManager.getWorkspaceId(url);
                        let lastUrl = getRoomIdByUrl(historyList.getLast());
                        if ("s" === lnbType && !checkSpace(url)) return false;
                        if (isNewWindow()) {
                            if (next) next();
                        } else {
                            if (!tds("AppSplitLayout")) {
                                tds("AppContainLayout").src("AppSplitLayout.html", function () {
                                    if (next) { next(); }
                                    renderGnb();
                                    if ("s" === lnbType) userManager.getWorkspaceUser(id);
                                });
                            } else {
                                if (next) next();
                                if (spaceAPI.isTempSpace(lastUrl) && id != workspaceManager.getWorkspaceId(lastUrl)) {
                                    let TempId = workspaceManager.getWorkspaceId(lastUrl);
                                    spaceAPI.removeLnbSpace(TempId);
                                    spaceAPI.truncateTempManager(TempId);
                                }

                                if("f"===lnbType) _spaceProfilePopup(url);
                                if("s"===lnbType) {
//                                	updateCommonUnread(url);
                                	userManager.getWorkspaceUser(id);
                                }
                                renderLnb(lnbType);
                                renderGnb();
                                spaceAPI.setSpace(url);
                            }
                        }

                        hideLoadingBar2();
                        if (appManager.getSubApp() !== "meeting")
                            Meeting2Manager.stop();
                    },
                    route: {
                        "/talk": {
                            activate: function (path, data, next) {
                                if ("s" !== Top.curRoute.params.root) {
                                    invalidUrl();
                                    return false;
                                }
                                //data set
                                talkData.serverURL = _workspace.url;
                                talkData.workspaceId = workspaceManager.getWorkspaceId(getRoomIdByUrl(), "CHN0001");
                                talkData.myInfo = userManager.getLoginUserInfo();
                                talkData.talkId = workspaceManager.getChannelList(
                                    workspaceManager.getWorkspaceId(getRoomIdByUrl()),
                                    "CHN0001"
                                );

                                const appWrapper = "talk";
                                let renderTarget = `div#${getRenderTarget(data)}`;
                                let isRendered = false;
                                // 새창일 경우.
                                if (isNewWindow()) {
                                    let space = spaceAPI.getSpaceProfile(talkData.workspaceId);
                                    let name = getRoomIdByUrl() === workspaceManager.getMySpaceId() || getRoomIdByUrl() === workspaceManager.getMySpaceUrl() ? userManager.getLoginUserName() + ' (나)' : (space ? space.SPACE_NAME : "");
                                    renderTarget = "top-page#MainTeeSpacePage";
                                    document.querySelector("body").style.minWidth = "unset";
                                    document.title = name;
                                }
                                // 새창 아닌 경우.
                                else {
                                    isRendered = !!document.querySelector(
                                        `${renderTarget} div#${appWrapper}[data-talk-id='${talkData.talkId}']`
                                    );
                                    setTimeout(function () {
                                        renderSubApp(data);
                                    }, 0);
                                }
                                if (!isRendered) {
                                    talk.render(renderTarget, null, false);
                                }
                                
                                let startMsg, endMsg, i, msgId;
                                let msgList = talkData.messageList;
                                let msgListLength = msgList.length;
                                if(msgListLength ==0) return;
                                for(i=msgListLength-1;i>=0;i--){
                                    msgId = msgList[i]['MSG_ID'];
                                    if(!msgId) continue;
                                    endMsg = msgId; break;
                                }
                        		talkServer2.ttalk_readMessage(talkData.talkId, endMsg, userManager.getLoginUserId()); 
                                
                            }
                        },

                        "/note": {
                            activate: function (path, data, next) {
                                // TODO : appWrapper (앱의 최상위 layout id) 바꿔주세요.
                                const appWrapper = "note";
                                //                                const renderTarget = getRenderTarget(data);
                                const renderTarget = 'AppSplitRightLayout';
                                const noteWS_ID = workspaceManager.getWorkspaceId(getRoomIdByUrl(), "CHN0003");
                                const noteChannelId = workspaceManager.getChannelList(getRoomIdByUrl(), "CHN0003");
                                const isExpanded = appManager.isExpanded();
                                const isRendered = !!document.querySelector(`div#${renderTarget} #note[note-channel-id='${noteChannelId}']`);
                                appManager.updateExpandState(true);
                                if (!isRendered) {
                                    //                                  tds("AppSplitMainLayout").setProperties({ ratio: "0:1" });
                                    $(`div#${renderTarget}`).empty();
                                    NoteApp.startTNote(noteWS_ID, noteChannelId, document.querySelector(`div#${renderTarget}`));
                                } else {
                                    if (!isExpanded) {
                                        if (data === undefined) { // 첫 렌더나 새로고침 시
                                            $(`div#${renderTarget}`).empty();
                                            NoteApp.startTNote(noteWS_ID, noteChannelId, document.querySelector(`div#${renderTarget}`));
                                        }
                                        else if ( // 펼쳤다 닫았을 시
                                            data !== undefined &&
                                            data.eventType === "fold" &&
                                            !changed
                                        )
                                            return;
                                        else if (changed) { // 스페이스 변경 시
                                            $(`div#${renderTarget}`).empty();
                                            NoteApp.startTNote(noteWS_ID, noteChannelId, document.querySelector(`div#${renderTarget}`));
                                        }
                                    }
                                    //                                    else{ // 펼칠 시
                                    ////                                        tds("AppSplitMainLayout").setProperties({ ratio: "0:1" });                                    
                                    //                                    }
                                }
                                setTimeout(function () {
                                    renderSubApp(data);
                                }, 0);
                            }
                        }, //note

                        "/mail": {
                            activate: function (path, data, next) {
                                if (("m" !== Top.curRoute.params.root) || (Top.curRoute.params.id !== workspaceManager.getMySpaceUrl())) {
                                    checkSpace();//invalidUrl();
                                    return false;
                                }
                                //data set
                                talkData.serverURL = _workspace.url;
                                talkData.workspaceId = workspaceManager.getWorkspaceId(getRoomIdByUrl(), "CHN0001");
                                talkData.myInfo = userManager.getLoginUserInfo();
                                talkData.talkId = workspaceManager.getChannelList(
                                    workspaceManager.getWorkspaceId(getRoomIdByUrl()),
                                    "CHN0001"
                                );

                                // TODO : appWrapper (앱의 최상위 layout id) 바꿔주세요.
                                const appWrapper = "mailMainRight";
                                const renderTarget = getRenderTarget(data);

                                const isRendered = !!document.querySelector(`div#${renderTarget} div#${appWrapper}`);

                                if (!isRendered) {
                                    Top.Dom.selectById('AppSplitLeftLayout').src('mailMainRight.html');
                                    // TODO : 여기에 mail rendering 로직 넣어주세요.
                                    // ex) mail.render(renderTarget);
                                }
                                setTimeout(function () {
                                    renderSubApp(data);
                                }, 0);

                                mailData.routeInfo = [];
                                if (!tds('mailMainLeft')) {
                                    Top.Dom.selectById('AppLnbLayout').src('mailMainLeft.html' + verCsp());
                                	mailData.routing=false;}
                                if (next != undefined) {
                                    if (!next) {
                                        //                                      Top.Dom.selectById('AppLnbLayout').src('mailMainLeft.html' + verCsp());
                                        //                                      Top.Dom.selectById('mailMainLayout_Center_All').src('mailTwoPanelLayout.html');
                                        mailData.routing = false;
                                    }
                                    else
                                        next();
                                }
                            },
                            route: {
                                "/setting": {
                                    activate: function (path, data, next) {
                                        if (Top.Dom.selectById("mailMainLeft") == null) {
                                            mailData.routing = 'setting';
                                            //                                             Top.Dom.selectById("contentsLayout").src("./mailMainLayout.html" + verCsp());
                                            Top.Dom.selectById('AppLnbLayout').src('mailMainLeft.html' + verCsp());
                                        } else {
                                            if (historyList.getLast() == undefined)
                                                Top.Controller.get('mailMainLeftLogic').activateRouting('setting');
                                            else {
                                                let bef = historyList.getLast().substr(2, historyList.getLast().length).split('?')[0];
                                                let now = Top.curRoute.path.split('?')[0];
                                                if (bef != now || getQueryStringByKey("q", historyList.getLast().substr(2, historyList.getLast().length)) !=
                                                    getQueryStringByKey("q", Top.curRoute.path))
                                                    Top.Controller.get('mailMainLeftLogic').activateRouting('setting');
                                            }

                                        }
                                    }
                                },
                                "/address": {
                                    activate: function (path, data, next) {
                                        if (Top.Dom.selectById("mailMainLeft") == null) {
                                            mailData.routing = 'address';
                                            //                                         Top.Dom.selectById("contentsLayout").src("./mailMainLayout.html" + verCsp());
                                            Top.Dom.selectById('AppLnbLayout').src('mailMainLeft.html' + verCsp());
                                        } else {
                                            if (historyList.getLast() == undefined)
                                                Top.Controller.get('mailMainLeftLogic').activateRouting('address');
                                            else {
                                                let bef = historyList.getLast().substr(2, historyList.getLast().length).split('?')[0];
                                                let now = Top.curRoute.path.split('?')[0];
                                                if (bef != now || getQueryStringByKey("q", historyList.getLast().substr(2, historyList.getLast().length)) !=
                                                    getQueryStringByKey("q", Top.curRoute.path))
                                                    Top.Controller.get('mailMainLeftLogic').activateRouting('address');
                                            }

                                        }
                                    }
                                },
                                "/file": {
                                    activate: function (path, data, next) {
                                        if (Top.Dom.selectById("mailMainLeft") == null) {
                                            mailData.routing = 'file';
                                            //                                         Top.Dom.selectById("contentsLayout").src("./mailMainLayout.html" + verCsp());
                                            Top.Dom.selectById('AppLnbLayout').src('mailMainLeft.html' + verCsp());
                                        } else {
                                            if (historyList.getLast() == undefined)
                                                Top.Controller.get('mailMainLeftLogic').activateRouting('file');
                                            else {
                                                let bef = historyList.getLast().substr(2, historyList.getLast().length).split('?')[0];
                                                let now = Top.curRoute.path.split('?')[0];
                                                if (bef != now || getQueryStringByKey("q", historyList.getLast().substr(2, historyList.getLast().length)) !=
                                                    getQueryStringByKey("q", Top.curRoute.path))
                                                    Top.Controller.get('mailMainLeftLogic').activateRouting('file');
                                            }
                                        }
                                    }
                                },
                                "/new": {
                                    activate: function (path, data, next) {
                                        if (!next) {
                                            if (Top.Dom.selectById("mailMainLeft") == null) {
                                                mailData.routing = 'new';
                                                //                                                 Top.Dom.selectById("contentsLayout").src("./mailMainLayout.html" + verCsp());
                                                Top.Dom.selectById('AppLnbLayout').src('mailMainLeft.html' + verCsp());
                                            } else {
                                                if (historyList.getLast() == undefined)
                                                    Top.Controller.get('mailMainLeftLogic').activateRouting('new');
                                                else {
                                                    let bef = historyList.getLast().substr(2, historyList.getLast().length).split('?')[0];
                                                    let now = Top.curRoute.path.split('?')[0];
                                                    if (bef != now || getQueryStringByKey("q", historyList.getLast().substr(2, historyList.getLast().length)) !=
                                                        getQueryStringByKey("q", Top.curRoute.path))
                                                        Top.Controller.get('mailMainLeftLogic').activateRouting('new');
                                                }
                                            }
                                        } else next();

                                    },
                                    route: {
                                        "/complete": {
                                            activate: function (path, data, next) {
                                                if (Top.Dom.selectById("mailMainLeft") == null) {
                                                    //                               						Top.Dom.selectById("contentsLayout").src("./mailMainLayout.html" + verCsp());
                                                    Top.Dom.selectById('AppLnbLayout').src('mailMainLeft.html' + verCsp());
                                                } else {
                                                    if (historyList.getLast() == undefined)
                                                        Top.Controller.get('mailMainLeftLogic').activateRouting('sendcomplete');
                                                    else {
                                                        let bef = historyList.getLast().substr(2, historyList.getLast().length).split('?')[0];
                                                        let now = Top.curRoute.path.split('?')[0];
                                                        if (bef != now || getQueryStringByKey("q", historyList.getLast().substr(2, historyList.getLast().length)) !=
                                                            getQueryStringByKey("q", Top.curRoute.path))
                                                            Top.Controller.get('mailMainLeftLogic').activateRouting('sendcomplete');
                                                    }
                                                }
                                            }
                                        }
                                    }
                                },
                                "/newtome": {
                                    activate: function (path, data, next) {
                                        if (!next) {
                                            if (Top.Dom.selectById("mailMainLeft") == null) {
                                                mailData.routing = 'newtome';
                                                //                                                 Top.Dom.selectById("contentsLayout").src("./mailMainLayout.html" + verCsp());
                                                Top.Dom.selectById('AppLnbLayout').src('mailMainLeft.html' + verCsp());
                                            } else {
                                                if (historyList.getLast() == undefined)
                                                    Top.Controller.get('mailMainLeftLogic').activateRouting('newtome');
                                                else {
                                                    let bef = historyList.getLast().substr(2, historyList.getLast().length).split('?')[0];
                                                    let now = Top.curRoute.path.split('?')[0];
                                                    if (bef != now || getQueryStringByKey("q", historyList.getLast().substr(2, historyList.getLast().length)) !=
                                                        getQueryStringByKey("q", Top.curRoute.path))
                                                        Top.Controller.get('mailMainLeftLogic').activateRouting('newtome');
                                                }
                                            }
                                        } else next();

                                    },
                                    route: {
                                        "/complete": {
                                            activate: function (path, data, next) {
                                                if (Top.Dom.selectById("mailMainLeft") == null) {
                                                    //                               						Top.Dom.selectById("contentsLayout").src("./mailMainLayout.html" + verCsp());
                                                    Top.Dom.selectById('AppLnbLayout').src('mailMainLeft.html' + verCsp());
                                                } else {
                                                    if (historyList.getLast() == undefined)
                                                        Top.Controller.get('mailMainLeftLogic').activateRouting('savecomplete');
                                                    else {
                                                        let bef = historyList.getLast().substr(2, historyList.getLast().length).split('?')[0];
                                                        let now = Top.curRoute.path.split('?')[0];
                                                        if (bef != now || getQueryStringByKey("q", historyList.getLast().substr(2, historyList.getLast().length)) !=
                                                            getQueryStringByKey("q", Top.curRoute.path))
                                                            Top.Controller.get('mailMainLeftLogic').activateRouting('savecomplete');
                                                    }
                                                }
                                            }
                                        }
                                    }
                                },
                                "/:folderId": {
                                    activate: function (path, data, next) {
                                        mailData.routeInfo.folderId = path.folderId.split('?')[0];
                                        if (!next) {
                                            Top.Controller.get('mailReadButtonLogic').goToThreePanel = false;
                                            if (Top.Dom.selectById("mailMainLeft") == null) {
                                                mailData.routing = 'folder';
                                                //                                                 Top.Dom.selectById("contentsLayout").src("./mailMainLayout.html" + verCsp());
                                                Top.Dom.selectById('AppLnbLayout').src('mailMainLeft.html' + verCsp());
                                            } else {
                                                if (historyList.getLast() == undefined)
                                                    Top.Controller.get('mailMainLeftLogic').activateRouting('folder');
                                                else {
                                                    let bef = historyList.getLast().substr(2, historyList.getLast().length).split('?')[0];
                                                    let now = Top.curRoute.path.split('?')[0];
                                                    if (bef != now || getQueryStringByKey("q", historyList.getLast().substr(2, historyList.getLast().length)) !=
                                                        getQueryStringByKey("q", Top.curRoute.path))
                                                        Top.Controller.get('mailMainLeftLogic').activateRouting('folder');
                                                }
                                            }
                                        } else next();

                                    },
                                    route: {
                                        "/:mailId": {
                                            activate: function (path, data, next) {
                                                mailData.routeInfo.mailId = path.mailId.split('?')[0];
                                                if (!next) {
                                                    if (Top.Dom.selectById("mailMainLeft") == null) {
                                                        mailData.routing = 'mail';
                                                        //                                                        Top.Dom.selectById("contentsLayout").src("./mailMainLayout.html" + verCsp());
                                                        Top.Dom.selectById('AppLnbLayout').src('mailMainLeft.html' + verCsp());
                                                    } else {
                                                        if (historyList.getLast() == undefined)
                                                            Top.Controller.get('mailMainLeftLogic').activateRouting('mail');
                                                        else {
                                                            let bef = historyList.getLast().substr(2, historyList.getLast().length).split('?')[0];
                                                            let now = Top.curRoute.path.split('?')[0];
                                                            if (bef != now || getQueryStringByKey("q", historyList.getLast().substr(2, historyList.getLast().length)) !=
                                                                getQueryStringByKey("q", Top.curRoute.path))
                                                                Top.Controller.get('mailMainLeftLogic').activateRouting('mail');
                                                        }

                                                    }
                                                } else next();

                                            },
                                            route: {
                                                "/:write": {
                                                    activate: function (path, data, next) {
                                                        if (Top.Dom.selectById("mailMainLeft") == null) {
                                                            mailData.routing = path.write.split('?')[0];
                                                            //                                                            Top.Dom.selectById("contentsLayout").src("./mailMainLayout.html" + verCsp());
                                                            Top.Dom.selectById('AppLnbLayout').src('mailMainLeft.html' + verCsp());
                                                        } else {
                                                            if (historyList.getLast() == undefined)
                                                                Top.Controller.get('mailMainLeftLogic').activateRouting(path.write.split("?")[0]);
                                                            else {
                                                                let bef = historyList.getLast().substr(2, historyList.getLast().length).split('?')[0];
                                                                let now = Top.curRoute.path.split('?')[0];
                                                                if (bef != now || getQueryStringByKey("q", historyList.getLast().substr(2, historyList.getLast().length)) !=
                                                                    getQueryStringByKey("q", Top.curRoute.path))
                                                                    Top.Controller.get('mailMainLeftLogic').activateRouting(path.write.split("?")[0]);
                                                            } 

                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }, //mail

                        "/toword": {
                            activate: function (path, data, next) {
                                // TODO : appWrapper (앱의 최상위 layout id) 바꿔주세요.
                                officeManager.routeToOffice("toword", data, getRenderTarget(data));
                            }
                        }, //toWord

                        "/topoint": {
                            activate: function (path, data, next) {
                                // TODO : appWrapper (앱의 최상위 layout id) 바꿔주세요.
                                officeManager.routeToOffice("topoint", data, getRenderTarget(data));
                            }
                        }, //toOffice

                        "/tocell": {
                            activate: function (path, data, next) {
                                // TODO : appWrapper (앱의 최상위 layout id) 바꿔주세요.
                                officeManager.routeToOffice("tocell", data, getRenderTarget(data));
                            }
                        }, //toCell

                        "/schedule": {
                            activate: function (path, data, next) {
                                // TODO : appWrapper (앱의 최상위 layout id) 바꿔주세요.
                                const appWrapper = "calendarLayout";
                                const renderTarget = getRenderTarget(data);

                                let oldSpace = getRoomIdByUrl(historyList.getLast());
                                let newSpace = getRoomIdByUrl(location.hash);
                                let changed = oldSpace !== newSpace; // 최초 로드시 false

                                if (changed) {
                                    calendar.render(renderTarget, appWrapper, tsVersion);
                                }
                                //tds(renderTarget).src(`${appWrapper}.html${tsVersion}`);
                                //                                
                                //                                const isRendered = !!document.querySelector(`div#${renderTarget} div#${appWrapper}`);
                                //                                
                                //                                if (!isRendered) {
                                //                                    // TODO : 여기에 schedule rendering 로직 넣어주세요.
                                //                                    // ex) schedule.render(renderTarget);
                                //                                    tds(renderTarget).src(`${appWrapper}.html${tsVersion}`);
                                //                                }
                                setTimeout(function () {
                                    renderSubApp(data);
                                }, 0);
                            }
                        }, //schedule

                        "/drive": {
                            activate: function (path, data, next) {
                                const workspaceId = workspaceManager.getWorkspaceId(getRoomIdByUrl(), "CHN0006");
                                const channelId = workspaceManager.getChannelList(getRoomIdByUrl(), "CHN0006");

                                const renderTarget = getRenderTarget(data);
                                const isRendered = !!document.querySelector(
                                    `div#${renderTarget} .drive[data-drive-id='${channelId}'][data-is-drive='true']`
                                );

                                if (!isRendered) {
                                    $(`div#${renderTarget}`).empty();
                                    //  drive.render(workspaceId, channelId, `div#${renderTarget}`, true);
                                }
                                setTimeout(function () {
                                    renderSubApp(data);
                                }, 0);
                            }
                        }, //drive
                        "/plus": {
                            activate: function (path, data, next) {
                                const workspaceId = workspaceManager.getWorkspaceId(getRoomIdByUrl(), "CHN0006");
                                const channelId = workspaceManager.getChannelList(getRoomIdByUrl(), "CHN0006");

                                const renderTarget = getRenderTarget(data);
                                const isRendered = !!document.querySelector(
                                    `div#${renderTarget} .drive[data-drive-id='${channelId}'][data-is-drive='false']`
                                );

                                if (!isRendered) {
                                    $(`div#${renderTarget}`).empty();
                                    drive.render(workspaceId, channelId, `div#${renderTarget}`, false);
                                }
                                setTimeout(function () {
                                    renderSubApp(data);
                                }, 0);
                            }
                        },

                        "/meeting": {
                            activate: function (path, data, next) {
                                // TODO : appWrapper (앱의 최상위 layout id) 바꿔주세요.
                                const appWrapper = "meetingMain";
                                const renderTarget = getRenderTarget(data);

                                const isRendered = !!document.querySelector(`div#${renderTarget} div#${appWrapper}`);

                                if (!isRendered) {
                                    // TODO : 여기에 meeting rendering 로직 넣어주세요.
                                    // ex) meeting.render(renderTarget);
                                }
                                setTimeout(function () {
                                    renderSubApp(data);
                                }, 0);
                            }
                        }, //meeting
                        "/:default": {
                            activate: function (path, data, next) {
                                /* default 처리 - top logic */
                            }
                        } //default 
                    }
                } //roomId
            }
        } //room
    }; //router

    /**
     * router preprocess
     * */

    //  if (!isLocal()) {
    for (let key in router) {
        let before = router[key].activate;

        router[key].activate = function (i, v, h) {
            // Before - check user
            if (
                !isLocal() &&
                !LOGOUT &&
                !!userManager.getLoginUserInfo() &&
                getCookie("loginid") !== userManager.getLoginUserInfo().USER_LOGIN_ID
            ) {
                userManager.update();
                workspaceManager.update();
                WebSocketClose();
            }

            before(i, v, h);

            //after - history (f/s/m) 
            if ("f/s/m".match(getLnbByUrl()) && !window.location.href.match("mini")) {
                setTimeout(                                                                     //순서 : top hashchange(불변) - appManager hashchange - ( history insert / renderSubApp ) 
                    function () {
                        historyList.insert();
                        gnbSubState(appManager.getSubApp());                                //subApp state
                    }, 0
                );
            }
        };
    }

    Top.App.addRoute(router);
    setInviter();
    const isRoot = function () {
        return location.hash === '#!/' || location.hash === '/' || location.hash === '';
    };

    /**
     * B2C, 로컬 개발 환경에서는 root 경로가 서비스 페이지
     */
    if (_validBtoc() && isRoot()) {
        location.replace(`${rootUrl}`);
    } else {
        if (!isLocal()) autoLogin();
        else userManager.localLogin();

        if ("/" === location.hash || "" === location.hash) {
            location.replace(rootUrl + "login");
        }
    }


    //setDeviceId();

    //    initCmsWebSocket();
});
function initCmsWebSocket() {
    if (!websocket || websocket.readyState != websocket.OPEN) {
        let loginKey = userManager.getLoginUserId();
        console.info("[INFO] webSocket connect trying - ID=" + loginKey);
        ConnectWebSocket(loginKey);
    } else {
        console.info("[INFO] webSocket already connected");
    }

    // if (!cmsWebSocket) {
    //     cmsWebSocket = new WebSocket("ws://" + _workspace.ip + ":" + _workspace.port + "/CMS/CMSws")
    //     cmsWebSocket.onopen = function (event) {
    //         console.log('WebSocket is connected.');
    //     }
    // }

    // setWebsocketMessageEvent(cmsWebSocket);
}
let deviceId;
//function setDeviceId() {
//    var params = {}; //dev
//    window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(str, key, value) {
//        params[key] = value;
//    });
//   if (params.DEVICEID) {
//       sessionStorage.setItem("DEVICEID", params.DEVICEID);
//       deviceId = params.DEVICEID;
//   } else {
//   }
//}
function setInviter() {
    if (location.href.indexOf("INVITER_ID") > -1) {
        ivtCode = location.href.split("INVITER_ID=")[1];
    } else {
        ivtCode = "";
    }
}

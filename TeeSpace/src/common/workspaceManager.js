const USER_GROUP_TYPE={
    MY_SPACE: "WKS0001",
	PRIVATE : "WKS0002",
	PUBLIC : "WKS0003"
};

const workspaceManager = (function(){
	let workspaceMap={																	// 가입된 워크스페이스 목록  (json)
		all:{WS_NAME:"전체", WS_ID:"all"} 
	};
	let roomMap={};																		// 가입된 룸 목록 (json)
	let channelMap = {};
	let wsInfo;																			// 가입된 워크스페이스 목록 (배열)
	let updateFlag = true;
    let mySpaceUrl = null;
    let wsIdMap = {};																	// id to url
    let roomIdMap = {};																	// id to url

    let updateHideFlag = true;															// 숨긴 목록 조회  Flag
    let wsHideMap = {};																	// 숨긴 스페이스 포함 목록	
    let tempChannel;
    let _tempId;
    
	function getMySpaceUrl() {
        if( mySpaceUrl ){
            return mySpaceUrl;
        }
        checkAndUpdate(null, null, true);
        // sync로 하더라도 return 이후라 Null이 Return 될수 있다.
        return mySpaceUrl;
    }

	let checkAndUpdate = function(workspaceId, channelType, list) {
		// 워크스페이스 아이디가 없으면 종료
		// list가 "true"면 리스트 조회, 아니면 해당 워크스페이스 조회
		// 여기서  채널 타입이 "ALL" 이면 채널목록 리턴, 채널 타입이 있으면 해당 채널 리턴, 없으면 워크스페이스 리턴
		let userId = userManager.getLoginUserId();
		if(!!!userId) {
			return; 
		}
		if(!updateFlag){		//0916test
			return;
		}
//		Top.Loader.start('large');
		$.ajax({
	        url: _workspace.url+ 'SpaceRoom/SpaceRoomList?USER_ID='+ userId + "&TASK_TYPE=SHOW",  //'WorkSpace/Workspace?action=List&USER_ID=' + userId + "&TASK_TYPE=" + type, 
	        type: 'GET',
	        async: false,
	        dataType: 'json',
	        contentType: 'application/json; charset=utf-8',
	        crossDomain: true,
	        success: function(ret) {
	        	Top.Loader.stop();
	        	let result = ret.dto.SpaceRoomList;
	        	updateFlag = false;
	        	
	        	wsInfo = result;
		        for(let i=0; i<result.length; i++){
	        		workspaceMap[result[i].SPACE_URL] = result[i];
	        		wsIdMap[result[i].WS_ID] = result[i].SPACE_URL;
                       if( result[i]["WS_TYPE"] === USER_GROUP_TYPE.MY_SPACE ){
                           mySpaceUrl = result[i]["SPACE_URL"];
                       }
                       if(result[i]["RoomList"]){
                           result[i]["RoomList"].forEach(function(room){
                           	roomMap[room.ROOM_URL]=room;
                           	roomIdMap[room.WS_ID] = room.ROOM_URL;
                           });
                       }
	        		channelMap[result[i].SPACE_URL] = workspaceMap[result[i].SPACE_URL].WsChList;
 	        	}
		        
		        let spaceList = talkServer2.getMessengerRooms(userManager.getLoginUserId()).ttalkMessengersList
		        let spaceListMap = {};
		        let wsInfoMap = {};
		        
		        spaceList.forEach(function(ele){
		        	spaceListMap[ele.WS_ID] = ele
		        })
		        wsInfo.forEach(function(ele){
		        	ele.WS_NAME = spaceListMap[ele.WS_ID]? spaceListMap[ele.WS_ID].WS_NAME : ''
		        	ele.UID_LIST = spaceListMap[ele.WS_ID]? spaceListMap[ele.WS_ID].UID_LIST : ''
		        	ele.THUMB_LIST = spaceListMap[ele.WS_ID]? spaceListMap[ele.WS_ID].THUMB_LIST : ''
		        	ele.WS_ALARM_YN = spaceListMap[ele.WS_ID]? spaceListMap[ele.WS_ID].WS_ALARM_YN : ''
	        		ele.RANK = spaceListMap[ele.WS_ID]? spaceListMap[ele.WS_ID].RANK : ''
		        })
		        
		        
		        
		        
		        
		        
	        },
	        error: function(error) {
	        	console.warn(error);
	        	Top.Loader.stop();
	        }
	    });
	};
	
	let _getHideList = function(workspaceId) {
		// 워크스페이스 아이디가 없으면 종료
		let userId = userManager.getLoginUserId();
		if(!!!userId) {
			return; 
		}
		if(!updateHideFlag){		//0916test
			return wsHideMap;
		}
		Top.Loader.start('large');
		$.ajax({
	        url: _workspace.url+ 'SpaceRoom/SpaceRoomList?USER_ID='+ userId +'&TASK_TYPE=SHOW',   //'WorkSpace/Workspace?action=List&USER_ID=' + userId + "&TASK_TYPE=" + type, 
	        type: 'GET',
	        async: false,
	        dataType: 'json',
	        contentType: 'application/json; charset=utf-8',
	        crossDomain: true,
	        success: function(ret) {
	        	Top.Loader.stop();
	        	let result = ret.dto.SpaceRoomList;
	        	updateHideFlag = false;
	        	
		        for(let i=0; i<result.length; i++){ 
	        		wsHideMap[result[i].SPACE_URL] = result[i]; 
 	        	}
		        return wsHideMap;
	        },
	        error: function(error) {
	        	console.warn(error);
	        	Top.Loader.stop();
	        }
	    });
	};
	
	let _getMailId = function(){
		return userManager.getLoginUserInfo().email;
	};
	
	let _clear = function(){
	};
	
	let _update = function(){
		updateFlag=true;																// true = 서비스 재 조회
		workspaceMap={all:{WS_NAME:"전체", WS_ID:"all"}};																		
		channelMap = {};
		wsInfo = null;
		roomMap={};
		wsIdMap ={};
		roomIdMap ={};
		
		updateHideFlag = true;															// 숨긴 목록 조회
		wsHideMap = {};
	};
	
	let _converWs = function(workspaceId){												//id(36자리) to url(20자리)
		return wsIdMap[workspaceId]? wsIdMap[workspaceId] : workspaceId;
	};
	let _convertRoom = function(roomUrl){												//id(36자리) to room sequence
		return roomIdMap[roomUrl]? roomIdMap[roomUrl] : roomUrl;
	};
	
	let _getWorkspacePhoto = function(wsId){
		let wsUrl = workspaceManager.getWorkspaceUrl(wsId);
		if(!wsUrl) 
			return new Promise(function(resolve, reject){
				resolve(false);
			}); 														//아이디값이 없는 경우
		checkAndUpdate(wsUrl, null, null);
		
		if(workspaceMap[wsUrl] && workspaceMap[wsUrl].PHOTO){
			return new Promise(function(resolve, reject){
				resolve(workspaceMap[wsUrl].PHOTO);
			});
		}
		
		let photo = spaceAPI.getSpaceProfile(wsId).THUMB_DIV;
		
		const SIZE = 160;
		const canvas = document.createElement("canvas");
        canvas.width = SIZE;
        canvas.height = SIZE;

        const ctx = canvas.getContext("2d");
        
        const LoadImage = (src) => {
        	let imageObj = new Image();
        	imageObj.src = src;
        	
        	return new Promise((resolve, reject) => {
        		imageObj.onload = function() {
        			resolve(this);
  		        };
        	});
        };
        
        const Circle = (ctx, x, y, r) => {
        	ctx.beginPath();
        	ctx.arc(x, y, r, 0, Math.PI*2, true); 
        	ctx.closePath();
        	ctx.clip();
        };

        const DrawImage = (ctx, img, x, y, w, h) => {
        	ctx.save();

            Circle(ctx, w/2 + x, h/2 + y, w/2);
            ctx.drawImage(img, x, y, w, h);

        	ctx.restore();
        };
        
        return new Promise((resolve, reject)  => {
        	const el = htmlToElement(photo);
    		const imgs = el.querySelectorAll('img');

    		const srcs = [];
    		
    		for(let img of imgs) {
    			srcs.push(LoadImage(img.getAttribute('src')));
    		}
    		
    		Promise.all(srcs).then(img => {
    			const length = img.length;
    			
    			switch(length) {
    			case 1:
                    DrawImage(ctx, img[0], 0, 0, SIZE, SIZE);
    				break;
    			case 2:
    		    	DrawImage(ctx, img[0], 0, 0, 115, 115);
    			    DrawImage(ctx, img[1], 43, 43, 115, 115);
    				break;
    			case 3:
    			    DrawImage(ctx, img[0], 33.5, 0, 93, 93);
    			    DrawImage(ctx, img[1], 0, 70, 91, 91);
    			    DrawImage(ctx, img[2], 68, 68, 91, 91);
    				break;
    			case 4:
    			    DrawImage(ctx, img[0], 0, 0, 90, 90);
    			    DrawImage(ctx, img[1], 70, 0, 90, 90);
    			    DrawImage(ctx, img[2], 0, 70, 90, 90);
    			    DrawImage(ctx, img[3], 70, 70, 90, 90);
    				break;
    			}
    			
//    			document.querySelector('img#gnbAppProfileIcon').setAttribute('src', canvas.toDataURL("image/png"));
        		let result = canvas.toDataURL("image/png");
        		if(workspaceMap[wsUrl]) workspaceMap[wsUrl].PHOTO = result; 	//임시방 예외처리
    			resolve(result); 
    		});
        });
	}
	
	return {
		
		//워크스페이스
		getWorkspaceDetail: function (wsId, userId) {
			return $.ajax({
				url: _workspace.url+"SpaceRoom/SpRm",
	            type: 'GET',
	            data: { WS_ID : wsId, USER_ID: userId },
	            dataType: 'json',
	            contentType: 'application/json; charset=utf-8',
	        }).then(function (res) {
	        	return res.dto;
	        }).catch(function (res) {
	        	
	        });
		},
		//숨긴 스페이스 제외
		getWorkspaceList : function(type){
			checkAndUpdate(null, null, true);
			return wsInfo;			
		},
		//숨긴 스페이스 목록 포함
		getTotalList : function(type){
			_getHideList()
			return wsHideMap;			
		},
        getMySpaceUrl: function(){
        	checkAndUpdate(null, null, null);
            return getMySpaceUrl();
        },
        getMySpaceId: function(){
        	checkAndUpdate(null, null, null);
            return workspaceMap[getMySpaceUrl()]? workspaceMap[getMySpaceUrl()]["WS_ID"]: undefined; 
        },
		getWorkspace : function(workspaceId){
			if(!workspaceId) return false; 											//아이디값이 없는 경우
			checkAndUpdate(workspaceId, null, null);
			workspaceId = _converWs(workspaceId);
			if(workspaceMap[workspaceId]) return workspaceMap[workspaceId];
		},
		getWorkspaceName : function(workspaceId){
			if(!workspaceId) return false; 											//아이디값이 없는 경우
			checkAndUpdate(workspaceId, null, null);
			workspaceId = _converWs(workspaceId);	
			if(workspaceMap[workspaceId]) return workspaceMap[workspaceId]["WS_NAME"];
		},
		getWorkspacePhoto : _getWorkspacePhoto,
		getWorkspaceId : function( workspaceId ){
			if(!workspaceId) return false; 											//아이디값이 없는 경우
			checkAndUpdate(workspaceId, null, null);

			//temp;;
			if(!workspaceManager.getWorkspace(workspaceId)){
					spaceAPI.getTempManager().forEach(ele => {
						if(ele.WS_ID == workspaceId || ele.SPACE_URL == workspaceId) {
							_tempId = ele.WS_ID
							return;
						}
					})
					
					return _tempId
			} 	
			
			workspaceId = _converWs(workspaceId); 
			if(workspaceMap[workspaceId]) return workspaceMap[workspaceId]["WS_ID"];
		},
		
		getWorkspaceUrl: function(workspaceId){
			if(!workspaceId) return false;
			checkAndUpdate(workspaceId, null, null);
			
			//temp;;
			if(!workspaceManager.getWorkspace(workspaceId)){
				spaceAPI.getTempManager().forEach(ele => {
					if(ele.WS_ID == workspaceId || ele.SPACE_URL == workspaceId) {
						_tempId = ele.SPACE_URL
						return;
					}
				})
				
				return _tempId
		} 	
			
			let converted = _converWs(workspaceId)
			return converted;
		},
		
		getRoomList : function( workspaceId ){
			if(!workspaceId) return false; 											//아이디값이 없는 경우
			checkAndUpdate(workspaceId, null, null);
			workspaceId = _converWs(workspaceId);	
			if(workspaceMap[workspaceId]) return workspaceMap[workspaceId]["RoomList"];
		},
		
		getRoom : function(roomUrl){ 
			if(!roomUrl) return false; 												//아이디값이 없는 경우
			checkAndUpdate(null, null, null);
			roomUrl = _convertRoom(roomUrl);				
			if(roomMap[roomUrl]) return roomMap[roomUrl];
		},
		getRoomId : function(roomUrl){
			if(!roomUrl) return false; 												//아이디값이 없는 경우
			checkAndUpdate(null, null, null);
			roomUrl = _convertRoom(roomUrl);				
			if(roomMap[roomUrl]) return roomMap[roomUrl]["WS_ID"];
		},
		getRoomName : function(roomUrl){
			if(!roomUrl) return false; 												//아이디값이 없는 경우
			checkAndUpdate(null, null, null);
			roomUrl = _convertRoom(roomUrl);				
			if(roomMap[roomUrl]) return roomMap[roomUrl]["WS_NAME"];
		},
		getRoomPhoto : function(roomUrl){
			if(!roomUrl) return false; 												//아이디값이 없는 경우
			checkAndUpdate(null, null, null);
			roomUrl = _convertRoom(roomUrl);				
			if(roomMap[roomUrl]) return roomMap[roomUrl]["WS_PHOTO"];
		},
		
		getRoomUrl: function(roomUrl){
			if(!roomUrl) return 0;
			if(""===roomUrl) return 0;
			checkAndUpdate(null, null, null);
			let converted = _convertRoom(roomUrl)
			return converted;
		},
		
		
		isMySpace : function( workspaceId ){
            if(!workspaceId) return false; 											//아이디값이 없는 경우
            checkAndUpdate(workspaceId, null, null);
            if( !workspaceMap[workspaceId] )
            	return false;
            else{
            	return workspaceMap[workspaceId]["WS_TYPE"] === USER_GROUP_TYPE.MY_SPACE;
			}
		},
		
		//채널
		getChannelList : function(workspaceId, channelType){
			if(!workspaceId) return false;					//아이디값이 없는 경우
			
			var type = channelType;
			let workspaceUrl = workspaceManager.getWorkspaceUrl(workspaceId);
			workspaceId = workspaceManager.getWorkspaceId(workspaceId)
//			workspaceId = _converWs(workspaceId);
			if('undefined'=== typeof(type)) type = 'ALL';	//채널타입이 없는 경우 리스트 조회
			checkAndUpdate(workspaceId, type, null);
			if("ALL" === type){
				return channelMap[workspaceUrl];
			}
			else{
				if(!channelMap[workspaceUrl]){
					//temp;;					
					jQuery.ajax({
				        type: "GET",
				        url: _workspace.url + "SpaceRoom/SpaceRoomChList",
						dataType : "json",
						data : JSON.stringify({
							"dto" :{
								"WS_ID" : workspaceId,
							}
						}),
				        contentType: "application/json",
				        async: false,
				        success: function (result) {
				        	tempChannel = result.dto.WsChList;
				        	for(let k=0; k<tempChannel.length; k++){
			        			if(channelType === tempChannel[k].CH_TYPE){
			        				tempChannel = tempChannel[k].CH_ID;
			        				return;
			        			}
				        	}
				        }
				    });
					return tempChannel;
				}
        		for(let j=0; j<channelMap[workspaceUrl].length; j++){
        			if(channelType === channelMap[workspaceUrl][j].CH_TYPE){
        				return channelMap[workspaceUrl][j].CH_ID;
        			}
        		}
			}
		
		},
		
		getRoomChannelList : function(roomUrl, channelType){
			if(!roomUrl) return false;					//아이디값이 없는 경우
			
			var type = channelType;
			roomUrl = _convertRoom(roomUrl);
			if('undefined'=== typeof(type)) type = 'ALL';	//채널타입이 없는 경우 리스트 조회
			checkAndUpdate(null, null, null);
			if("ALL" === type){
				if(roomMap[roomUrl]) return roomMap[roomUrl]["WsChList"];
				else return false;
			}
			else{
				if(!roomMap[roomUrl]) return false;
        		for(let j=0; j<roomMap[roomUrl]["WsChList"].length; j++){
        			if(channelType === roomMap[roomUrl]["WsChList"][j].CH_TYPE){
        				return roomMap[roomUrl]["WsChList"][j].CH_ID;
        			}
        		}
			}
		
		},
        getWorkspaceAndChannelId : function( workspaceUrl, roomUrl, channelType){
			let info={
				"workspaceId" :"",
				"channelId":""
			};
			let workspaceId = workspaceManager.getWorkspaceId(workspaceUrl);
            let roomId= workspaceManager.getRoomId(roomUrl);

            if( roomUrl=== "0" || roomId === null ){
            	info.workspaceId=workspaceId;
            	info.channelId = workspaceManager.getChannelList(info.workspaceId, channelType);
			}else{
                info.workspaceId=roomId;
                info.channelId = workspaceManager.getRoomChannelList(info.workspaceId, channelType);
			}
            return info;
        },
		//최신화 - 유저 그룹 변동사항 있을시
		update: function(){
			_update();
		},
		
		
		//로그인 아이디
		getMailId : function(){
			return _getMailId();
		},
		
		getNoti : function(NOTI_ID, USER_ID, CH_ID, WS_ID, NOTI_FROM, NOTI_READ, NOTI_TIME, CH_TYPE, SENDER_NAME) {
			
			Top.Ajax.execute({
				url : "http://192.168.158.12:8080/CMS/WorkSpace/Noti?action=Get",
				type : 'POST',
				dataType : "json",
				data : JSON.stringify(NOTI_ID, USER_ID, CH_ID, WS_ID, NOTI_FROM, NOTI_READ, NOTI_TIME, CH_TYPE, SENDER_NAME), //값을 json 문자열로 변환
				contentType : "application/json; charset=utf-8",
				crossDomain : true,
				xhrFields : {
				    withCredentials: true
				},
				success : function(data) {
					
				},
				error : function(data) {
					console.info("실패");
				}
			});
		},
		
		createNoti : function(USER_ID, CH_ID, CH_TYPE, WS_ID, NOTI_MSG) {
			Top.Ajax.execute({
				url : _workspace.url + "WorkSpace/Noti",
				type : 'POST',
				dataType : "json",
				data : JSON.stringify({
						"dto" :{
							"USER_ID" : USER_ID,
							"SENDER_NAME" : SENDER_NAME,
							"CH_ID" : CH_ID,
							"CH_TYPE" : CH_TYPE,
							"WS_ID" : WS_ID,
							"WS_NAME" : WS_NAME,
							"NOTI_MSG" : NOTI_MSG
						}
				}),
				contentType : "application/json; charset=utf-8",
				crossDomain : true,
				xhrFields : {
				    withCredentials: true
				},
				success : function(data) {
					
				},
				error : function(data) {
					console.info("실패");
				}
			});
		},
		

        
		getSpaceDefaultColor: function (WS_ID) {
        	  var index = WS_ID[0].match(/([0-1]+)?([2-3]+)?([4-5]+)?([6-7]+)?(8+)?(9+)?(a+)?(b+)?(c+)?(d+)?(e+)?(f+)?/).slice(1).findIndex(function (v) {
        	    return typeof v !== 'undefined';
        	  });
        	  var Space_Hex = ['#FFE783', '#FFBF75', '#FFB18E', '#FF9F9F', '#FFA8C1', '#FFAAE8', '#D4A9FF', '#A7B6F7', '#86D2FF', '#6FF0E2', '#94F7A6', '#C0F57F'];
        	  return Space_Hex[index];
        },
        
        
        getRoomDefaultColor: function (ROOM_ID) {
        		  var index = ROOM_ID[0].match(/([0-1]+)?([2-3]+)?([4-5]+)?([6-7]+)?(8+)?(9+)?(a+)?(b+)?(c+)?(d+)?(e+)?(f+)?/).slice(1).findIndex(function (v) {
        		    return typeof v !== 'undefined';
        		  });
        		  var Room_Hex = ['#FFDC4E', '#FFAD4F', '#FF8B57', '#FF6969', '#FF799F', '#F97CD7', '#BE7FFF', '#7E94EF', '#4EB7F6', '#33DAC7', '#57E972', '#9CE245'];
        		  return Room_Hex[index];
		},
		updatePhoto: function(wsUrl){
			workspaceMap[wsUrl].PHOTO = null;
		}
	}
})();



/***  스페이스 공통 함수  ***/

function checkChannel(wsUrl, roomUrl, chnIn){
	let app = chnIn? chnIn:routeManager.getApp();
	let channelList = !roomUrl? workspaceManager.getChannelList(wsUrl):workspaceManager.getRoomChannelList(roomUrl);
	 
	for(var i=0; i<channelList.length; i ++){
		if(app===getRoutingApp([channelList[i].CH_TYPE])){
			return true;
		}
	}
	return false;											//채널이 없거나 일치하는 채널이 없는 경우 드라이브로 이동
}
const commonLoader = (function() {
    let loader = null;

    const init = () => {
        const loaderImg = document.createElement("img");
        loaderImg.setAttribute("src", "./res/loading-13.gif");

        loader = document.createElement("div");
        loader.classList.add("common__loader");
        loader.appendChild(loaderImg);

        document.body.appendChild(loader);
    };

    const start = selector => {
        if (!loader) {
            init();
        }

        let target = "body";
        if (selector) {
            target = document.querySelector(selector);
        }

        if (target && loader) {
            const targetRect = target.getBoundingClientRect();
            loader.style.display = "flex";
            loader.style.top = targetRect.top + "px";
            loader.style.left = targetRect.left + "px";
            loader.style.width = targetRect.width + "px";
            loader.style.height = targetRect.height + "px";
        }
    };

    const stop = () => {
        if (loader) {
            loader.style.display = "none";
        }
    };

    return {
        start,
        stop
    };
})();

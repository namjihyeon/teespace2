const dndRounter = (function() {
    // drag and drop 기능
    const userHandlers = {
        dragenter: function(e, appName, data) {
            switch (appName) {
	            case "room":
	            	console.log("room dragENTER");
                    if(data.fromApp === "note"){
            			let existDiv = document.querySelectorAll('#emptyDiv li#emptyNode');
            			if(existDiv.length !== 0){
            				for(let count=0; count < existDiv.length; count++) existDiv[count].remove();
            			}
 	            	}
	            	break;            
                case "drive":
                    drive.onDragEnter(e, data);
                    break;

                case "schedule":
                    console.debug("[schedule DRAG ENTER]", data);
                    calendar.onDragEnter(e, data);
                    break;

                case "talk":
                     talk.onDragEnter(e, data);
                    if(data.fromApp === "note"){
            			let existDiv = document.querySelectorAll('#emptyDiv li#emptyNode');
            			if(existDiv.length !== 0){
            				for(let count=0; count < existDiv.length; count++) existDiv[count].remove();
            			}
 	            	}
                    break;

                case "note":
                    NoteDragDrop.onDragEnter(e, data);
                    break;

                case "office":
                    console.log("[office DRAG ENTER]", data);
                    break;

                case "meeting":
                    console.log("[meeting DRAG ENTER]", data);
                    break;

                case "mail":
                	mail.onDragEnter(e, data);
//                    console.log("[mail DRAG ENTER]", data);
                    break;
                    
                case "talk-file-dialog":                	
                	break;

                default:
                    console.log("[unknown DRAG ENTER]", data);
            }
        },
        dragleave: function(e, appName, data) {
            switch (appName) {
	            case "room":
	            	console.log("room dragleave");
	            	break;
                case "drive":
                    drive.onDragLeave(e, data);
                    break;

                case "schedule":
                    console.debug("[schedule DRAG LEAVE]", data);
                    calendar.onDragLeave(e, data);
                    break;

                case "talk":
                      talk.onDragLeave(e, data);
                    break;

                case "note":
                    NoteDragDrop.onDragLeave(e, data);
                    break;

                case "office":
                    console.log("[office DRAG LEAVE]", data);
                    break;

                case "meeting":
                    console.log("[meeting DRAG LEAVE]", data);
                    break;

                case "mail":
                	mail.onDragLeave(e, data);
//                    console.log("[mail DRAG LEAVE]", data);
                    break;

                default:
                    console.log("[unknown DRAG LEAVE]", data);
            }
        },
        dragstart: function(e, appName, data) {
            switch (appName) {
	            case "room":
	            	console.log("room drag start");
	            	break;
                case "drive":
                    drive.onDragStart(e, data);
                    break;

                case "schedule": {
                    // 필수
                    dnd.useDefaultEffect(true);
                    calendar.onDragStart(e, data);
                    break;
                }

                case "talk": {
                    talk.onDragStart(e, data);
                    break;
                }

                case "note": {
                    // 필수
                    dnd.useDefaultEffect(false);
                    NoteDragDrop.onDragStart(e, data);
                    // 필수
                    dnd.setBackground(
                        $("<div />")
                            .css({
                                display: "flex",
                                "justify-content": "center",
                                "align-items": "center",
                                width: "auto",
                                height: "auto",
                                background: "skyblue",
                                "margin-left": "15px"
                            })
                            .text(dnd.getData().note_title)[0]
                    );
                    // setData 필수
                    break;
                }
                case "office": {
                    // 필수
                    dnd.useDefaultEffect(true);
                    dnd.setData({ "1": 1, "2": 2, "3": 3 });
                    break;
                }

                case "meeting": {
                    // 필수
                    dnd.useDefaultEffect(true);
                    dnd.setData({ "1": 1, "2": 2, "3": 3 });
                    break;
                }

                case "mail": {
                    // 필수
                	dnd.useDefaultEffect(true);
                    dnd.setBackground(e.target.cloneNode(true));
                    mail.onDragStart(e, data);
                    dnd.setData({ "1": 1, "2": 2, "3": 3, "mailData" : data.mailData,"mailContainerType": data.mailContainerType});
                    break;
                }

                case "plus" : {
                    drive.onDragStart(e, data);
                    break;
                }
                default:
                    console.log("[unknown DRAG START]", data);
            }
        },
        dragover: function(e, appName, data) {
            switch (appName) {
	            case "room":
	            	console.log("room dragover");
	            	break;
                case "drive":
                    drive.onDragOver(e, data);
                    break;

                case "schedule":
                    calendar.onDragOver(e, data);
                    break;

                case "talk":
                     talk.onDragOver(e, data);
                    break;

                case "note":
                    NoteDragDrop.onDragOver(e, data);
                    break;

                case "office":
                    console.log("[office DRAG OVER]", data);
                    break;

                case "meeting":
                    console.log("[meeting DRAG OVER]", data);
                    break;

                case "mail":
                	  mail.onDragOver(e, data);
//                    console.log("[mail DRAG OVER]", data);
                    break;
                    
                case "talk-file-dialog":                	
                	break;

                default:
                    console.log("[unknown DRAG OVER]", data);
            }
        },
        drop: function(e, appName, data) {
            switch (appName) {
	            case "room":
	            	let fromApp = data.fromApp;
	            	//roomOnDrop(e,data);
	            	talk.onDrop(e, data);
	            	if(fromApp === "note"){
		            	let targetID = $(event.target).closest(".lnbSpaceList").attr("id");
		            	let targetData = data.data;
	            		// 페이지 일 경우
	            		if(targetID){
	            			if(targetID !== workspaceManager.getWorkspaceId(getRoomIdByUrl())){
			            		if(targetData.targetList.length === 0) {
			            			let shareTargetList = [];
			            			let shareTargetObj = {};
			            			shareTargetObj.note_id = targetData.note_id;
			            			shareTargetList.push(shareTargetObj)
			            			TNoteController.shareNoteCreate(targetID, shareTargetList);
			            		}
			            		else if(targetData.targetList.length !== 0) TNoteController.shareNoteCreate(targetID, targetData.targetList);	            				
	            			}
	            		}
	            	}
	            	break;
                case "drive":
                    drive.onDrop(e, data);
                    break;

                case "schedule":
                    console.debug("[schedule DROP]", data);
                    calendar.onDrop(e, data);
                    break;

                case "talk":
                     talk.onDrop(e, data);
                    break;

                case "talk-file-dialog":
                	talk.onDropDialog(e, data);
                    
                case "note":
                    NoteDragDrop.onDrop(e, data);
                    break;

                case "office":
                    console.log("[office DROP]", data);
                    break;

                case "meeting":
                    console.log("[meeting DROP]", data);
                    break;

                case "mail":
                	mail.onDrop(e, data);
                    console.log("[mail DROP]", data);
                    break;

                default:
                    console.log("[unknown DROP]", data);
            }
        }
    };

    return {
        init: function() {
            dnd.init(userHandlers);
        }
    };
})();

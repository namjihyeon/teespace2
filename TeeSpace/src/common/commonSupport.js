// i18n
if (!Top.i18n.map.hasOwnProperty('support')) {
	var i18nConfig = {
		name: 'support',
		path: 'external/config/i18n/',
		language: 'ko',
	};
	Top.i18n.load(i18nConfig);
}

function parseUrl(url) {
	// url에 들어갈 정보
	// url --> faq?mode=list&cate=ALL
	// empty --> #!/spt/faq?mode=list&cate=ALL
	var state = {};
	var qString = '';
	if (url) {
		console.info('before parse: ' + url);
		state['module'] = window.location.hash.split('#!/')[1].split('/')[0];
		state['type'] = url.split('?')[0];
		qString = url.split('?')[1];
	} else {
		var url = window.location.hash.split('#!/').join('');
		var hash = url.split('/');
		state['module'] = hash[0];

		if (hash.length > 1) {
			state['type'] = hash[1].split('?')[0];
			qString = hash[1].split('?')[1];
		}
	}

	if (typeof qString !== 'undefined' && qString.length > 1) {
		qString.split('&').forEach(function (elem) {
			state[elem.split('=')[0]] = elem.split('=')[1];
		});
	}

	return state;
}
//************************************************************
//************************************************************
var spt = (function (arg) {
	return {
		onChangeDisabling: false, // 에디터에 내용에 따라서 저장버튼 활성/비활성화하는 옵션

		setBoardTitlebar: function (title, desc, button) {
			if (typeof title === 'string') {
				// board title
				$$$('titleText').setProperties({
					visible: 'visible',
					text: title,
				});
			}
			if (typeof desc === 'string') {
				// board description
				$$$('descText').setProperties({
					visible: 'visible',
					text: desc,
				});
			} else {
				$('top-linearlayout#descLL').remove();
			}
			if (button && button.text && button.onClick) {
				// button next to the board title
				$$$('titleButton').setProperties({
					visible: 'visible',
					text: button.text,
					'on-click': button.onClick,
				});
			} else {
				$('top-button#titleButton').remove();
			}
		},
		CONST: {
			CATE_ALL: 'all', // 게시글 목록 보기에서 모든 카테고리를 보여줄때 카테고리 코드
			SUBCATE_ALL: 'all', // 모든 서브카테고리를 포함하는 카테고리 코드
			CHANNEL_ID: 'None',
			TITLE_MAX_LENGTH: 250, // 게시글 제목 최대 길이

			TOTAL_FILE_SIZE_LIMIT: 20 * 1024 * 1024, // 파일사이즈 합계 제한(bytes)
			SINGLE_FILE_SIZE_LIMIT: 20 * 1024 * 1024, // 개별 파일 사이즈 제한(bytes)
		},
		encodeFileChooserList: function (fileChooserList) {
			let fileDataList = []; // encoded data
			for (var i = 0; i < fileChooserList.length; i++) {
				let fc = fileChooserList[i];
				let fileSrc = fc.src;
				if (fc.file.length > 1) {
					// fileChooser로 복수개 파일 선택시 각 fileChooser의 file에 각 n개의 file이 다 들어있음
					var fileNameWithExtension = fc.file[i].name;
				} else {
					// 단수개 파일 선택시 fileChooser의 file이 1개만 들어있음
					var fileNameWithExtension = fc.file[0].name;
				}

				let fileExt = fileNameWithExtension.splitIntoNameAndExtension()[1];

				if (typeof fileSrc === 'string') {
					fileSrc = isBase64Encoded(fileExt)
						? fileSrc.split(',')[1] || ''
						: btoa(encodeURIComponent(fileSrc));
				} else {
					let uint8Array = new Uint8Array(fileSrc); // 0~127
					let binary = '';
					for (let j = 0; j < uint8Array.length; j++) {
						binary += String.fromCharCode(uint8Array[j]);
					}
					fileSrc = btoa(binary);
				}
				fileDataList.push({
					filename: fileNameWithExtension,
					contents: fileSrc,
				});
			}
			return fileDataList;
		},

		startLoader: function () {
			// TOP loader starts

			Top.Loader.start('large');
			//		        var loaderMaxTimeout = officeConfig.loaderMaxTimeout;
			var loaderMaxTimeout = 5000;
			setTimeout(function () {
				Top.Loader.stop();
			}, loaderMaxTimeout);
		},
		stopLoader: function (flag) {
			// TOP loader stops
			if (flag) {
				Top.Loader.stop();
			} else {
				setTimeout(function () {
					Top.Loader.stop();
				}, 1);
			}
		},
		isAdmin: function (userId) {
			if (userId) {
				const { MEM_GRADE } = userManager.getUserInfo(userId);
				return ['superadmin', 'admin'].includes(MEM_GRADE);
			}
			return userManager.getLoginUserInfo().userGrade !== null;
		},

		fillTextCorrespondingToValue: function (list) {
			return list.map((elem) => {
				elem.text = spt.cvt(null, elem.value);
				return elem;
			});
		},
		setLnbColor: function () {
			const u = parseUrl();
			var module = u.module;
			var type = u.type;

			if (module === 'op') {
				var lnbButtonList = Top.Controller.get('opMainLayoutLogic').lnbButtonList;
			} else if (module === 'spt') {
				var lnbButtonList = Top.Controller.get('sptMainLayoutLogic').lnbButtonList;
			} else {
				return;
			}
			$('.cmp-lnb-button button.selected').removeClass('selected');
			$('.cmp-lnb-button button#' + lnbButtonList.filter((elem) => elem.type === type)[0].id).addClass(
				'selected'
			);
		},
		dateConverter: function (d) {
			if (typeof d === 'string') {
				// YYYY-MM-DD hh:mm:ss -> YYYY-MM-DD
				d = d.split(' ')[0].split('-').join('.');
				return d;
			} else if (typeof d === 'object') {
				// new Date -> YYYY-MM-DD
				var DD = d.getDate();
				var MM = d.getMonth() + 1; //January is 0!

				var YYYY = d.getFullYear();
				DD = DD.pad(2);
				MM = MM.pad(2);
				d = [YYYY, MM, DD].join('-');
				return d;
			} else {
			}
		},
		daysAgo: function (nDaysAgo, from) {
			// spt.daysAgo(3) -> "2020-01-25"
			// spt.daysAgo(3, '1999-01-04') -> "1999-01-01"
			if (from) {
				return spt.dateConverter(new Date(new Date(from) - nDaysAgo * 24 * 60 * 60 * 1000));
			} else {
				return spt.dateConverter(new Date(new Date() - nDaysAgo * 24 * 60 * 60 * 1000));
			}
		},
		isNew: function (date, today = new Date()) {
			// YYYY-MM-DD hh:mm:ss
			if (typeof date === 'string') {
				date = Date.parse(date);
			}
			if (typeof today === 'string') {
				today = Date.parse(today);
			}
			// 72시간 미만은 new
			return (today - date) / 1000 / 3600 < 72;
		},
		valOfKey: function (tarKey, key, val, list) {
			var val;
			list = list.filter(function (elem) {
				return elem[key] === val;
			});
			if (typeof list === 'Object') var tarVal = list[tarKey];
			else if (list.length > 0) var tarVal = list[0][tarKey];
			else var tarVal = undefined;

			return tarVal;
		},
		setTextPassword: function (id) {
			document.querySelector('input#' + id).setAttribute('type', 'password');
			document.querySelector('input#' + id).setAttribute('name', 'password');
			return;
		},
		//		setDisableTextSymbol: function(id) {
		//			// 어길시 테두리, 콜백실행
		//			//			$$$('id').setProperties({
		//			//				'border'
		//			//			})
		//		},
		//		setDisableLongText: function(id, limit) {
		//			// 넘어가면 타자가 안쳐짐
		//		},
		//		_throttle: function _throttle(callback, period) {
		//			//			var timer;
		//			//			document.querySelector('#input').addEventListener('input', function (e) {
		//			//			  if (!timer) {
		//			//			    timer = setTimeout(function() {
		//			//			      timer = null;
		//			//			      console.log('여기에 ajax 요청', e.target.value);
		//			//			    }, 200);
		//			//			  }
		//			//			});
		//			if (!timer) {
		//				timer = setTimeout(function() {
		//					timer = null;
		//					callback();
		//				});
		//			}
		//
		//			if (timer) {
		//				clearTimeout(timer);
		//			}
		//
		//			timer = setTimeout(callback, period);
		//		},
		//		_debounce: function _debounce(callback) {
		//			//			var timer;
		//			//			document.querySelector('#input').addEventListener('input', function(e) {
		//			//			  if (timer) {
		//			//			    clearTimeout(timer);
		//			//			  }
		//			//			  timer = setTimeout(function() {
		//			//			    console.log('여기에 ajax 요청', e.target.value);
		//			//			  }, 200);
		//			//			});
		//			if (!timer) {
		//				timer = null;
		//				setTimeout(callback, period);
		//			}
		//		},
		//		test: {
		//			list: function list(targetId) {
		//				var page = $$$(targetId);
		//				page.src('commonListLayout.html' + verCsp());
		//			},
		//			read: function read(targetId) {
		//				//				let page = Top.Dom.selectById('MainTopApplicationPage');
		//				var page = $$$(targetId);
		//				page.src('commonReadLayout.html' + verCsp());
		//			},
		//			write: function write(targetId) {
		//				//				let page = Top.Dom.selectById('MainTopApplicationPage');
		//				var page = $$$(targetId);
		//				page.src('commonWriteLayout.html' + verCsp());
		//			},
		//			modify: function modify(targetId) {
		//				//				let page = Top.Dom.selectById('MainTopApplicationPage');
		//				var page = $$$(targetId);
		//				page.src('commonWriteLayout.html' + verCsp());
		//			},
		//		},
		routeLayout: function route(path) {
			// 모바일에서 lnb 메뉴만 표시하기 위해 메인 페이지만 라우팅할때
			if (!path) {
				if (parseUrl().module === 'spt') {
					Top.Dom.selectById('MainTeeSpacePage').src('sptMainLayout.html' + verCsp());
				} else if (parseUrl().module === 'op') {
					Top.Dom.selectById('MainTeeSpacePage').src('opMainLayout.html' + verCsp());
				} else {
					//  do nothing
				}
				return;
			}

			function contentsRouting() {
				const u = parseUrl();
				var module = u.module;
				var type = u.type;
				var mode = u.mode;
				var html = ''; // layout html file name
				var callback;

				if (mode === 'list') {
					html = 'commonListLayout.html';
					callback = function () {
						try {
							Top.Controller.get('commonListLayoutLogic')[module + '_' + type]();
						} catch (e) {}
					};
				} else if (mode === 'read') {
					html = 'commonReadLayout.html';
					callback = function () {
						try {
							Top.Controller.get('commonReadLayoutLogic')[module + '_' + type]();
						} catch (e) {}
					};
				} else if (mode === 'write') {
					html = 'commonWriteLayout.html';
					callback = function () {
						try {
							Top.Controller.get('commonWriteLayoutLogic')[module + '_' + type]();
						} catch (e) {}
					};
				} else if (module === 'spt' && type === 'home') {
					html = 'sptHomeLayout.html';
				} else if (module === 'spt' && type === 'manual') {
					html = 'sptManualLayout.html';
				} else if (module === 'spt' && type === 'plan') {
					html = 'sptPlanLayout.html';
				} else {
					return;
				}

				$$$('contentsLL').src(html + verCsp(), callback);
			}

			if ($$$('contentsLL') === null || typeof $$$('contentsLL') === 'undefined') {
				if (parseUrl().module === 'spt') {
					$$$('MainTeeSpacePage').src('sptMainLayout.html' + verCsp(), contentsRouting);
				} else if (parseUrl().module === 'op') {
					$$$('MainTeeSpacePage').src('opMainLayout.html' + verCsp(), contentsRouting);
				} else {
					return;
				}
			} else {
				contentsRouting();
			}
			// 쿼리스트링이 올바를때
			//            new Promise(function (res, rej) {
			//                if ($$$('contentsLL') === null || typeof $$$('contentsLL') === 'undefined') {
			//                    if (parseUrl().module === 'spt') {
			//                        $$$('MainTopApplicationPage').src('sptMainLayout.html' + verCsp(),res());
			//                    } else if (parseUrl().module === 'op') {
			//                        $$$('MainTopApplicationPage').src('opMainLayout.html' + verCsp(),res());
			//                    } else {
			//                        rej();
			//                    }
			//                } else {
			//                    rej();
			//                }
			//            })
			//                .then(function () {
			//                    const { module, type, mode } = parseUrl();
			//                    let html = ''; // layout html file name
			//                    if (mode === 'list') {
			//                        html = 'commonListLayout.html';
			//                    } else if (mode === 'read') {
			//                        html = 'commonListLayout.html';
			//                    } else if (mode === 'write') {
			//                        html = 'commonWriteLayout.html';
			//                    } else if (module === 'spt' && type === 'home') {
			//                        html = 'sptHomeLayout.html';
			//                    } else if (module === 'spt' && type === 'manual') {
			//                        html = 'sptManualLayout.html';
			//                    } else if (module === 'spt' && type === 'plan') {
			//                        html = 'sptPlanLayout.html';
			//                    } else {
			//                        return;
			//                    }
			//                    $$$('contentsLL').src(html + verCsp());
			//                })
			//                .catch(function () {
			//                    // do nothing
			//                });
		},
		routeState: function routeState(state, returnValueType) {
			// state: object containing query string information
			//            var { module, type, mode, cate, subcate, page, postId, sw, st } = parseUrl();
			//            var module = (state.module) ? state.module : module;
			//            var type = state.type ? state.type : type;
			//            var mode = state.mode ? state.mode : mode;
			var keys = Object.keys(state);
			var hash = '';
			if (keys.includes('module')) hash += '/' + state.module;
			if (keys.includes('type')) hash += '/' + state.type;
			if (keys.includes('mode')) hash += '?mode=' + state.mode;
			if (keys.includes('cate')) hash += '&cate=' + state.cate;
			if (keys.includes('subcate')) hash += '&subcate=' + state.subcate;
			if (keys.includes('view')) hash += '&view=' + state.view;
			if (keys.includes('page')) hash += '&page=' + state.page;
			if (keys.includes('postId')) hash += '&postId=' + state.postId;
			// if (keys.includes('sType')) hash += '&st=' + state.st;
			// if (keys.includes('sWord')) hash += '&sw=' + state.sw;

			if (returnValueType === 'String') {
				return hash;
			} else {
				Top.App.routeTo(hash);
			}
		},
		isBase64Encoded: function (str) {
			//		var re = /^([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)?$/;
			//			return re.exec(str);
			var base64Regex = /^(?:[A-Z0-9+\/]{4})*(?:[A-Z0-9+\/]{2}==|[A-Z0-9+\/]{3}=|[A-Z0-9+\/]{4})$/i;
			var isValid = base64Regex.test(str);
			return isValid;
		},

		openHome: function () {
			sessionStorage.setItem('spt', JSON.stringify({ prevPage: Top.curRoute.path }));
			// history.pushState(
			var media = window.matchMedia('screen and (max-device-width: 1024px)');
			if (media.matches) {
				Top.App.routeTo('/spt');
			} else {
				Top.App.routeTo('/spt/home');
			}
		},
		openSvcAnnList: function (postId) {
			sessionStorage.setItem('spt', JSON.stringify({ prevPage: Top.curRoute.path }));
			if (postId) {
				var hash = '/spt/service_announcement?mode=read&postId=' + postId;
			} else {
				var hash = '/spt/service_announcement?mode=list&cate=all&subcate=all&view=ss30&page=1';
			}
			Top.App.routeTo(hash);
		},
		openUpdAnnList: function (postId) {
			sessionStorage.setItem('spt', JSON.stringify({ prevPage: Top.curRoute.path }));
			if (postId) {
				var hash = '/spt/update_announcement?mode=read&postId=' + postId;
			} else {
				var hash = '/spt/update_announcement?mode=list&cate=all&subcate=all&view=ss30&page=1';
			}
			Top.App.routeTo(hash);
		},
		ajax: function (
			serviceGroup,
			serviceName,
			inputData,
			successCallback = function () {},
			errorCallback = function () {},
			completeCallback = function () {},
			contentType = 'application/json; charset=utf-8'
		) {
			$.ajax({
				url: _workspace.url + serviceGroup + '/' + serviceName + '?action=',
				type: 'POST',
				dataType: 'json',
				data: JSON.stringify(inputData),
				// contentType: ,
				contentType: contentType, // 'multipart/form-data'
				xhrFields: { withCredentials: true },
				headers: { ProObjectWebFileTransfer: 'true' },
				crossDomain: true,
				async: true,
				success: function (data, xhrStatus, xhr) {
					successCallback(data, xhrStatus, xhr);
				},
				error: function (xhr, xhrStatus, err) {
					errorCallback(xhr, xhrStatus, err);
				},
				complete: function (xhr, xhrStatus) {
					//					  console.info(xhr, xhrStatus)
					completeCallback(xhr, xhrStatus);
				},
			});
		},
		getProperIcon: function (ext) {
			if (['docx', 'toc'].includes(ext)) return 'icon-work_toword';
			else if (['xlsx', 'tls'].includes(ext)) return 'icon-work_tocell';
			else if (['pptx', 'tpt'].includes(ext)) return 'icon-work_topoint';
			else if (['jpg', 'jpeg', 'png', 'gif', 'bmp'].includes(ext)) return 'icon-work_img_file';
			else return 'icon-work_document';
		},

		exportExel: function (tableId) {
			/*
            export	tableId를 갖는 table을 선택한 booktype 형식의 파일로 export 한다.	
            tableId (string/array) - 단일 tableview 표현 시 string으로 입력, 여러 테이블을 하나의 파일 내 sheet로 구분하여 생성할 때는 Array로 id 입력,
            booktype (string) - xlsx(default)와 xls, txt, csv를 지원한다.,
            pageId (string) - 생략 가능하며 생략 시 현재 pageId로 설정한다.,
            tableData (array) - 생략 가능하며 입력 시 table에 보이는 것이 아닌 data 기반으로 export 한다.,
            option (object) - 생략 가능하며 type(single/list) 선택을 통해, 단일/다중 테이블 export, 숨김 column data 표현 및 특정 header data만 표현 옵션 기능을 설정한다.
            */
			var fileExtension = 'xlsx';
			var pageId = null;
			var data = tableId.getItems();
			var option = {
				filename: (function (d) {
					return (
						'List_' +
						d.getFullYear().pad(4) +
						(d.getMonth() + 1).pad(2) +
						d.getDate().pad(2) +
						'_' +
						d.getHours().pad(2) +
						d.getMinutes().pad(2) +
						d.getSeconds().pad(2)
					);
				})(new Date()),
			};
			Top.Excel.export(tableviewId, fileExtension, pageId, data, option);
		},
		cvt: function (name, code) {
			var mapper = {
				전체: 'all',
				회원: 'ss1',
				앱: 'ss2',
				요금: 'ss3',
				장애: 'ss4',
				기타: 'ss5',
				TeeTalk: 'ss10',
				TeeNote: 'ss11',
				TeeDrive: 'ss12',
				TeeOffice: 'ss13',
				TeeCalendar: 'ss14',
				Admin: 'ss15',
				TeeMeeting: 'ss16',
				TeeMail: 'ss17',
				Operator: 'ss18',
				'고객 지원': 'ss19',
				'작업 현황': 'ss20',
				TmaxOS: 'ss21',
				ToOffice: 'ss22',
				TeeSpace: 'ss23',
				New: 'ss24',
				업데이트: 'ss25',
				TeePhone: 'ss26',
				TeeStudy: 'ss27',

				분류: 'ss1001',
				제목: 'ss1002',
				등록일: 'ss1003',
				작성자: 'ss1004',
				'전체 글 보기': 'ss1005',
				'내 글만 보기': 'ss1006',
				//				'답변완료': 'ss1007',
				//				'답변대기': 'ss1008',
				답변완료: 'CLOSED',
				답변대기: 'OPEN',
			};

			if ((typeof code === 'undefined' || !code) && name) {
				return mapper[name];
			}

			if ((typeof name === 'undefined' || !name) && code) {
				var mapperList = Object.keys(mapper);

				for (var i = 0; i < mapperList.length; i++) {
					if (mapper[mapperList[i]] === code) {
						return mapperList[i];
					}
				}
			}
		},
		extractTextFromHtml: function (html) {
			// extract text contents from html string
			return new DOMParser().parseFromString(html, 'text/html').documentElement.textContent;
		},
	};
})(window);

const userManager = (function () {
    const faces = [
        'M30.7878178,19.3952227 C31.2424082,20.2416105 30.9425017,21.3060245 30.1179578,21.7726579 C26.5338713,23.8009972 21.8271028,24.7971686 16.0019311,24.8039803 C10.174758,24.8107254 5.46577304,23.8175157 1.87985455,21.7811053 C1.05597728,21.3132329 0.757589547,20.2483693 1.21338689,19.4026657 C1.66918424,18.5569622 2.70656548,18.2506696 3.53044274,18.718542 C6.55101981,20.4339001 10.7052683,21.3101059 15.9980666,21.3039481 C21.2928664,21.2977879 25.4493313,20.4180863 28.4717404,18.7076167 C29.2962842,18.2409833 30.3332274,18.548835 30.7878178,19.3952227 Z M10.5,8 C11.8807119,8 13,9.11928813 13,10.5 C13,11.8807119 11.8807119,13 10.5,13 C9.11928813,13 8,11.8807119 8,10.5 C8,9.11928813 9.11928813,8 10.5,8 Z M21.8857907,8 C23.2665026,8 24.3857907,9.11928813 24.3857907,10.5 C24.3857907,11.8807119 23.2665026,13 21.8857907,13 C20.5050788,13 19.3857907,11.8807119 19.3857907,10.5 C19.3857907,9.11928813 20.5050788,8 21.8857907,8 Z',
        'M30.7878178,19.3952227 C31.2424082,20.2416105 30.9425017,21.3060245 30.1179578,21.7726579 C26.5338713,23.8009972 21.8271028,24.7971686 16.0019311,24.8039803 C10.174758,24.8107254 5.46577304,23.8175157 1.87985455,21.7811053 C1.05597728,21.3132329 0.757589547,20.2483693 1.21338689,19.4026657 C1.66918424,18.5569622 2.70656548,18.2506696 3.53044274,18.718542 C6.55101981,20.4339001 10.7052683,21.3101059 15.9980666,21.3039481 C21.2928664,21.2977879 25.4493313,20.4180863 28.4717404,18.7076167 C29.2962842,18.2409833 30.3332274,18.548835 30.7878178,19.3952227 Z M28.0911889,7.02060176 C28.2984598,7.77224319 27.8953114,8.54808278 27.1813511,8.8203604 L27.0439144,8.86538249 L21.6424599,10.353976 L27.0439144,11.8442949 C27.8425334,12.0645202 28.3114142,12.8904566 28.0911889,13.6890757 C27.8839181,14.4407171 27.1401003,14.9002721 26.3874957,14.7681333 L26.2464082,14.7363502 L15.9994599,11.910976 L5.75359179,14.7363502 L5.6125043,14.7681333 C4.85989966,14.9002721 4.11608187,14.4407171 3.90881105,13.6890757 C3.70154023,12.9374342 4.10468861,12.1615946 4.81864889,11.889317 L4.95608563,11.8442949 L10.3564599,10.353976 L4.95608563,8.86538249 C4.15746661,8.64515725 3.68858581,7.81922078 3.90881105,7.02060176 C4.11608187,6.26896033 4.85989966,5.80940535 5.6125043,5.94154411 L5.75359179,5.97332718 L15.9994599,8.79797602 L26.2464082,5.97332718 L26.3874957,5.94154411 C27.1401003,5.80940535 27.8839181,6.26896033 28.0911889,7.02060176 Z',
        'M28.4717404,18.7076167 C29.2962842,18.2409833 30.3332274,18.548835 30.7878178,19.3952227 C31.2424082,20.2416105 30.9425017,21.3060245 30.1179578,21.7726579 C26.5338713,23.8009972 21.8271028,24.7971686 16.0019311,24.8039803 C10.174758,24.8107254 5.46577304,23.8175157 1.87985455,21.7811053 C1.05597728,21.3132329 0.757589547,20.2483693 1.21338689,19.4026657 C1.66918424,18.5569622 2.70656548,18.2506696 3.53044274,18.718542 C6.55101981,20.4339001 10.7052683,21.3101059 15.9980666,21.3039481 C21.2928664,21.2977879 25.4493313,20.4180863 28.4717404,18.7076167 Z M26.4074549,7.26248556 C26.7828963,7.86804625 26.6427024,8.65681905 26.0886792,9.09630429 L25.9745219,9.17735812 L23.913,10.4550309 L26.4471974,10.4550309 C27.1596312,10.4550309 27.7561416,10.9898133 27.8377298,11.6922607 L27.8464653,11.8319937 C27.8471286,12.5675335 27.3123462,13.1640439 26.6098989,13.2456321 L26.4701658,13.254367 L19,13.2550309 C17.6632556,13.2550309 17.0942753,11.5636658 18.1305375,10.7568128 L18.2447091,10.6767148 L24.479879,6.81032291 C25.1370256,6.40289879 26.0000308,6.6053389 26.4074549,7.26248556 Z M 10.5, 10.5 m -2.5, 0 a 2.5,2.5 0 1,0 5,0 a 2.5,2.5 0 1,0 -5,0',
        'M30.7878178,19.3952227 C31.2424082,20.2416105 30.9425017,21.3060245 30.1179578,21.7726579 C26.5338713,23.8009972 21.8271028,24.7971686 16.0019311,24.8039803 C10.174758,24.8107254 5.46577304,23.8175157 1.87985455,21.7811053 C1.05597728,21.3132329 0.757589547,20.2483693 1.21338689,19.4026657 C1.66918424,18.5569622 2.70656548,18.2506696 3.53044274,18.718542 C6.55101981,20.4339001 10.7052683,21.3101059 15.9980666,21.3039481 C21.2928664,21.2977879 25.4493313,20.4180863 28.4717404,18.7076167 C29.2962842,18.2409833 30.3332274,18.548835 30.7878178,19.3952227 Z M20.2352442,10.2495293 L20.3576854,10.3264968 C20.9455681,10.7362307 21.6455155,10.9429034 22.5,10.9429034 C23.2595418,10.9429034 23.8969802,10.7796065 24.4421449,10.455549 L24.6423146,10.3264968 C25.321956,9.85281024 26.2569131,10.0197697 26.7305997,10.6994111 C27.2042863,11.3790526 27.0373269,12.3140097 26.3576854,12.7876963 C25.2509705,13.5590383 23.950918,13.9429034 22.5,13.9429034 C21.049082,13.9429034 19.7490295,13.5590383 18.6423146,12.7876963 C17.9626731,12.3140097 17.7957137,11.3790526 18.2694003,10.6994111 C18.7152229,10.0597486 19.5696451,9.87422651 20.2352442,10.2495293 Z M14.2940025,10.7984414 C14.7129961,11.5130993 14.473313,12.4321054 13.7586551,12.851099 C12.5302786,13.5712786 11.2716808,13.9429034 10,13.9429034 C8.72831917,13.9429034 7.46972135,13.5712786 6.24134487,12.851099 C5.52668704,12.4321054 5.28700387,11.5130993 5.70599753,10.7984414 C6.12499118,10.0837836 7.04399729,9.84410043 7.75865513,10.2630941 C8.54478435,10.7239904 9.28618653,10.9429034 10,10.9429034 C10.7138135,10.9429034 11.4552156,10.7239904 12.2413449,10.2630941 C12.9560027,9.84410043 13.8750088,10.0837836 14.2940025,10.7984414 Z',
        'M30.7878178,19.3952227 C31.2424082,20.2416105 30.9425017,21.3060245 30.1179578,21.7726579 C29.6740899,22.0238557 29.213003,22.2592228 28.7347052,22.4788405 L29.4590387,25.1888677 C30.1737477,27.8561982 28.5908353,30.597883 25.9235047,31.312592 C23.3282642,32.0079846 20.6626343,30.5282732 19.8621302,27.9909279 L19.7997804,27.7770581 L18.9766559,24.7080149 C18.0198125,24.7708698 17.0282298,24.8027802 16.0019311,24.8039803 C10.174758,24.8107254 5.46577304,23.8175157 1.87985455,21.7811053 C1.05597728,21.3132329 0.757589547,20.2483693 1.21338689,19.4026657 C1.66918424,18.5569622 2.70656548,18.2506696 3.53044274,18.718542 C6.55101981,20.4339001 10.7052683,21.3101059 15.9980666,21.3039481 C21.2928664,21.2977879 25.4493313,20.4180863 28.4717404,18.7076167 C29.2962842,18.2409833 30.3332274,18.548835 30.7878178,19.3952227 Z M25.9104848,23.5344397 C24.6935421,23.8977632 23.3898468,24.1823884 21.999491,24.3892367 L22.6975579,27.000601 C22.9568023,27.9681145 23.8827752,28.5789329 24.8486969,28.4711848 L25.0014514,28.4481477 L25.1470476,28.4148146 C26.1654829,28.1419257 26.7886881,27.1302795 26.5945943,26.110921 L26.5612612,25.9653248 L25.9104848,23.5344397 Z M10.5,8 C11.8807119,8 13,9.11928813 13,10.5 C13,11.8807119 11.8807119,13 10.5,13 C9.11928813,13 8,11.8807119 8,10.5 C8,9.11928813 9.11928813,8 10.5,8 Z M21.8857907,8 C23.2665026,8 24.3857907,9.11928813 24.3857907,10.5 C24.3857907,11.8807119 23.2665026,13 21.8857907,13 C20.5050788,13 19.3857907,11.8807119 19.3857907,10.5 C19.3857907,9.11928813 20.5050788,8 21.8857907,8 Z',
    ];
    let workspaceUser = {};		//워크스페이스별 유저 리스트
    let userInfo = {};			//유저별 정보
    let t = {};
    let loginUserInfo;
    let __loginFlag = false;	// true=존재, false=업데이트

    //워크스페이스별 유저 목록 조회
    let _updateWorkspaceUser = function (workspaceId) {
    	
        if(!workspaceId) return false;						// workspaceId가 없으면 false로 종료
        if(workspaceUser[workspaceId]) return;
        Top.Loader.start('large');							// 없는 경우 ajax로 워크스페이스별 유저 정보 조회 및저장
        $.ajax({
            url: _workspace.url + "SpaceRoom/SpaceRoomMemberListBrief?action=Get",	 
            type: 'post',
            async: false,
            dataType: 'json',
            data: JSON.stringify({
                dto: {
                	WS_ID 	: workspaceId,
                	USER_ID	: userManager.getLoginUserId()
                }
            }),
            contentType: 'application/json; charset=utf-8',
            success: function (ret) {						// workspaceId 값이 있지만 값이 없으면 []로 리턴
                Top.Loader.stop();
                if ('object' === typeof (ret.dto.UserProfileList)) {
                    workspaceUser[workspaceId] = ret.dto.UserProfileList;
                }
                let memberList = ret.dto.UserProfileList;
                for(let i=0; i<memberList.length; i++){
                	userInfo[memberList[i].USER_ID] = memberList[i];
                }
            },
            error: function (error) {
                Top.Loader.stop();
            }
        });
    };

    //스페이스/룸별 유저 목록 조회
    let _getWorkspaceUser = function (workspaceId) {
        if (!workspaceId) return false;						// workspaceId가 없으면 종료
        if (!workspaceUser[workspaceId]) return false;		// workspaceId가 있지만 정보가 없는 경우
        return workspaceUser[workspaceId];
    };
    
    //스페이스/룸별 유저 목록 최신화 
    let _renewWorkspaceUser = function (workspaceId) {
        if (!workspaceId) workspaceUser = {};				// spaceId가 없으면 전체 초기화
        else if (!workspaceUser[workspaceId]) return false;	// spaceId가 있지만 해당 스페이스 정보가 없는 경우
        else {
        	workspaceUser[workspaceId] = null;				// 해당 스페이스만 초기화
        	_updateWorkspaceUser(workspaceId);
        }
    };    
 
    //유저별 정보 업데이트
    let _updateUserInfo = function (userId) {
        if (!userId) return false;							// id가 나의 정보조회
        if(userInfo[userId]) return;
        //		if(!userInfo[userId]){						// 해당하는 정보가 이미 있는지 확인		
        Top.Loader.start('large');					  	    // 없는 경우 유저 정보 조회 및저장
        $.ajax({
            url: _workspace.url + "Users/BothProfile",
            type: 'get',
            async: false,
            data: JSON.stringify({
                "dto": {
                    "USER_ID": userId

                }
            }),
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            xhrFields: {
                withCredentials: true
            },
            success: function (ret) {
                Top.Loader.stop();
                if ('object' === typeof (ret.dto)) {
                    userInfo[userId] = ret.dto;
                    t[userId] = getTimeStamp();						//talk에서 업데이트 칠 때, 타임스탬프값 이용해주기위해 선언
                }
            },
            error: function (error) {
                Top.Loader.stop();
                return null;
            }
        });
    };  
    
    //스페이스/룸별 유저 목록 최신화 
    let _renewUserInfo = function (userId) {
        if (!userId) userInfo = {};					// userId가 없으면 전체 초기화
        else if (!userInfo[userId]) return false;	// userId가 있지만 해당 유저가 없는 경우
        else userInfo[userId] = null;				// 해당 유저만 초기화
    };   
   
    //나의 정보 업데이트
    let _updateMyInfo = function(){																	//업데이트 or 최초 로그인시에만 수행
    	if(__loginFlag) return loginUserInfo;
		$.ajax({
	        url: _workspace.url+'Users/BothProfile',///'Users/Profile',
	        type: 'GET', 
	        dataType: 'json',
	        data: JSON.stringify({ "dto": {
	        	"WS_ID": deviceId
	        }}),
	        async: false,
	        contentType : "application/json",
	        success: function(ret){
	        	loginUserInfo = {
					userId 			: ret.dto.USER_ID,
					userName 		: ret.dto.USER_NAME,
					userNick		: ret.dto.USER_NICK,
					userGrade	    : ret.dto.MEM_GRADE,					//profilePhoto	: ret.dto.PROFILE_PHOTO,
					userType		: ret.dto.USER_TYPE,
					domain			: ret.dto.USER_DOMAIN,
					regi_path		: ret.dto.REGI_PATH,
                    email			: ret.dto.USER_EMAIL,
                    orgName         : ret.dto.ORG_NAME,
                    userPosition    : ret.dto.USER_POSITION,
                    userJob         : ret.dto.USER_JOB,
					thumbPhoto		: ret.dto.THUMB_PHOTO,
                    USER_LOGIN_ID	: ret.dto.USER_LOGIN_ID,
                    backPhoto       : ret.dto.BACKGROUND_THUMB
                };
	        	sessionStorage.setItem('userInfo',JSON.stringify(loginUserInfo));					//삭제 예정
	        	if("No Valid Token"===loginUserInfo.userId) {
	        		loginUserInfo =null;
	        		return loginUserInfo; 
	        	}
	        	__loginFlag = true; 
	        	return loginUserInfo;
	        },
	        error: function(error) {
                hideLoadingBar2();
        		autoLoginFail(location.hash);
	        }
		});
    }

    //유저별 정보 조회
    let _getUserInfo = function (userId) {
        if (!userId) return false;																	// userId가 없으면 종료
        if (!userInfo) return false;																// userId가 있지만 정보가 없는 경우
        return userInfo[userId];
    };

    let _getLoginUserId = function () {
        _updateMyInfo();
        if (loginUserInfo) return loginUserInfo.userId;
    };
    let _getLoginUserName = function () {
        _updateMyInfo();
        if (loginUserInfo) return loginUserInfo.userName;
    };
    
    let getUserDefaultPhotoURL = function (userId) {
    	const fields = userId.split('-');

        const faceIdx = fields[0][0].match(/([0-3]+)?([4-6]+)?([7-9]+)?([a-c]+)?([d-f]+)?/).slice(1).findIndex(v => typeof v !== 'undefined');
        const colorSetIdx = fields[1][0].match(/([0-3]+)?([4-7]+)?([8-b]+)?([c-f]+)?/).slice(1).findIndex(v => typeof v !== 'undefined');
        const colorIdx = fields[fields.length-1][0].match(/([0-1]+)?([2-3]+)?([4]+)?([5]+)?([6-7]+)?([8-9]+)?([a]+)?([b]+)?([c-d]+)?([e]+)?([f]+)?/).slice(1).findIndex(v => typeof v !== 'undefined');
    	return `res/face/${faceIdx}${colorSetIdx}${colorIdx}.png`;
    };

    function getWorkspaceInfo(){
        let input = "&USER_ID=" + userManager.getLoginUserId()+"&TASK_TYPE=" + type;
        $.ajax({
            url: _workspace.url+'WorkSpace/Workspace?action=List' + input, //Service Object
            type: 'GET',
            data: JSON.stringify({}),
            dataType: 'json',
            contentType: 'application/json; charset=UTF-8',
            xhrFields: {    withCredentials: true },
            crossDomain: true,
            success: function(result) {
                Top.Controller.get('createWorkspaceLayoutLogic').treeviewUpdate();
                Top.App.routeTo("/landing");
            },
            error: function(error) {
                hideLoadingBar();
                console.log("getWorkspaceInfo error")
            }
        })
    }


    function __update() {								// 접속중인 유저 정보 업데이트
    	sessionStorage.removeItem("userInfo");			// 접속중 유저 정보  == loginUserInfo
    	loginUserInfo = null;
    	__loginFlag = false;
        __userFlag = false;
    }
    
    function __clear(){									// 전체(접속중 유저, 스페이스별 유저, 유저별 정보) 초기화   /  쿠키 초기화는 로그인 로직에서 수행  - clear에서 X 
    	
    	workspaceUser={};								// 워크스페이스별 유저 리스트
    	userInfo = {};									// 유저별 정보
    	__update();
    }

    return {
        //워크스페이스의유저 리스트 조회
        getWorkspaceUser: function (workspaceId) {
            if (!workspaceId) return false;				// id가 없으면 종료
            _updateWorkspaceUser(workspaceId);			// 존재하는지 체크 후 업데이트
            return _getWorkspaceUser(workspaceId);		// 워크스페이스별 유저 조회후 리턴
        },
        
        
        //해당 스페이서 유저 목록 최신화
        updateWorkspaceUser: function(workspaceId){
        	_renewWorkspaceUser(workspaceId);			//_update = 조회 , _renew = 서비스 재조회 요청
        },

        //유저 정보 조회
        getUserInfo: function (userId) {
            if (!userId) return false;					// id가 없으면 종료
            _updateUserInfo(userId);					// 존재하는지 체크 후 업데이트
            return _getUserInfo(userId)? _getUserInfo(userId): false;      // 유저정보 조회후 리턴
        },
        
        //해당 유저 정보 최신화
        updateUserInfo: function(userId){
        	_renewUserInfo(userId);						//_update = 조회 , _renew = 서비스 재조회 요청
        },


        //유저 이름 조회
        getUserName: function (userId) {
            var userInfo = userManager.getUserInfo(userId);
            if (userInfo && "string" === typeof (userInfo["USER_NAME"])) return userInfo["USER_NAME"];
            else return false;
        },

        //유저 닉네임 조회
        getUserNick: function (userId) {
            var userInfo = userManager.getUserInfo(userId);
            if (userInfo && "string" === typeof (userInfo["USER_NICK"])) return userInfo["USER_NICK"];
            else return false;
        },
        
        //유저 사진 조회
        getUserPhoto: function (userId = userManager.getLoginUserId(), size = "medium", exe = null) {
    /*        let userInfo = userManager.getUserInfo(userId);
            let result
            if (userInfo){
            	result = "string" === typeof (userInfo["THUMB_PHOTO"])? userPhotoGet(userId, "medium",userInfo.thumbPhoto) : getUserDefaultPhotoURL(userId);
            	return result;
            }
            else return false; */
            if(isLocal()){	
                if(size === "back") return getDefaultBackground(userId);
                else return getUserDefaultPhotoURL(userId);
            }
            if(size === "back"){
                if(exe)  return "/photo/profile/back/"+userId + "."+exe+ "?"+ new Date().getTime();
                else return getDefaultBackground(userId);
            }else if (size === "small"){
                if(exe) return "/photo/profile/small/"+userId + "."+exe+ "?"+ new Date().getTime();
                else return getUserDefaultPhotoURL(userId);
            }else if (size === "talk"){
            	if(exe) return "/photo/profile/small/"+userId + "."+exe+"?"+ t[userId];
                else return getUserDefaultPhotoURL(userId);
            } 
            else{
                if(exe) return "/photo/profile/medium/"+userId + "."+exe+ "?"+ new Date().getTime();
                else return getUserDefaultPhotoURL(userId);
            }
        },
        getUserDefaultPhotoURL,
        getLoginUserInfo: function () {
        	_updateMyInfo();
            return loginUserInfo; 
        },
        getLoginUserId: function () {
            return _getLoginUserId();
        },
        getLoginUserName: function () {
            return _getLoginUserName();
        },
        getLoginUserPhoto: function(size="medium"){
        	let id = _getLoginUserId();
        	let userInfo = userManager.getLoginUserInfo();
        	let type = userInfo? userInfo.thumbPhoto : null;
        	let photo = userManager.getUserPhoto(id, size, type);
        	return photo? photo : getUserDefaultPhotoURL(id);
        },
        skipUserLogin: function (userIndex) {
            return skipUserLogin(userIndex);
        },
        isBtoc: function () {
            var userType = _updateMyInfo().userType;
            if (userType && "USR0002" == userType) return true;			//btoc
            else return false;											//btob
        },
       update: function(){
    	   __update();
       },
		
        clear:function(){
        	__clear();
        },
        logout: function(){
        		Top.Ajax.execute({
        		      url : _workspace.url + "Users/Logout",
        		      type : 'DELETE',
        		      dataType : "json",
        		      async: false,
        		      contentType: 'application/json',
        		      data: JSON.stringify({
        		          "dto": {
        		        	"USER_ID": userManager.getLoginUserId(), 
        		        	"DEVICE_ID" :deviceId
        		          }
        		        }),
        		      crossDomain : true,
        		      xhrFields : {
        		          withCredentials: true
        		      },
        		      success : async function(result) {
        		    	  WebSocketClose();
        		    	  NotificationManager.clearTeeSpaceTabList();
        		    	  await spaceAPI.truncateTempManager(workspaceManager.getWorkspaceId(getRoomIdByUrl()));
        		  		  sessionStorage.clear();
        		          LOGOUT = true;
        		          userManager.clear();
        		          workspaceManager.update();
        		          appManager.setSubApp(null);
        		          deleteCookie("BCID");
        		          deleteCookie("loginkey");
        		          deleteCookie("token");
        		          deleteCookie("loginid"); 
        		          LogoutTimer.end();
        		          clearInterval(mailData.interval30);
        		          NotificationManager.clear();
        		          Top.App.routeTo("/login");
        		      }
        	      });
        },
        testLogin: function (index) {
            testLogin(index);
        },
        localLogin: function(inputId, callback){
            if(sessionStorage.userInfo){
                __loginFlag = true;
                loginUserInfo = JSON.parse(sessionStorage.userInfo);
                checkUserGrade();
                setLoginInfo();
                return false;
            }else{
                $.ajax({
                    url: _workspace.url+'Users/BothProfile',///'Users/Profile',
                    type: 'GET', 
                    dataType: 'json',
                    async: false,
                    data: JSON.stringify({
                        "dto": {
                            "USER_ID": inputId
                        }
                    }),
                    contentType : "application/json",
                    success: function(ret){
                        loginUserInfo = {
                            userId 			: ret.dto.USER_ID,
                            userName 		: ret.dto.USER_NAME,
                            userGrade	    : ret.dto.MEM_GRADE,    				//	profilePhoto	: ret.dto.PROFILE_PHOTO,
                            userType		: ret.dto.USER_TYPE,
                            domain			: ret.dto.USER_DOMAIN,
                            regi_path		: ret.dto.REGI_PATH,
                            email			: ret.dto.USER_EMAIL,
                            orgName         : ret.dto.ORG_NAME,
                            userPosition    : ret.dto.USER_POSITION,
                            userJob         : ret.dto.USER_JOB,
                            thumbPhoto		: ret.dto.THUMB_PHOTO,
                            USER_LOGIN_ID	: ret.dto.USER_LOGIN_ID,
                            backPhoto       : ret.dto.BACKGROUND_THUMB
                        };
                        __loginFlag = true; 
                        sessionStorage.setItem('userInfo',JSON.stringify(loginUserInfo));					//삭제 예정
                        if(callback && typeof callback ==='function') callback();
                    },
                    error: function(error) {
                        console.log("local login error"); 
                        window.location.replace(rootUrl + "login"); 
                    }
                });
            }
        }
	}
})();


/* 유저 등급  체크 */
function checkUserGrade(from) {
	//로그인X
	if(!userManager.getLoginUserInfo()){
		window.location.replace(rootUrl + "login");
		return false;
	}
	//로그인O
    switch (userManager.getLoginUserInfo().userGrade) {
        case "admin":
//            window.location.replace(rootUrl + "admin/company_org"); 
            return false;
        default:{
        	 if("login" === from) {
        		 window.location.replace(`${rootUrl}f/${userManager.getLoginUserInfo().USER_LOGIN_ID}`); 
        	 }
        	 else if ("" === location.hash) {
        		 window.location.replace(`${rootUrl}s/${workspaceManager.getMySpaceUrl()}/talk`);		//"" === location.hash || "#!/login"===location.hash
        		 return false;
        	 }
        	 else {}
        }
    }
    return true;
}

/* b2c 여부*/
function isb2c() {
//	return false;
//	return true;
	return userManager.isBtoc();
}

/**** setUserAlarmConfig  ****/

let NotificationBlockEventConfig = 'alset';

function getwsOpt(wsid, chn){
	let wsopt = JSON.parse(sessionStorage.getItem(NotificationBlockEventConfig));
	temp = [];
	if(wsopt.length==0){
		return [];
	}
	for (var i=0;i<wsopt.length;i++){
		if(wsopt[i].WS_ID == wsid && wsopt[i].CH_TYPE == chn){
			temp.push(wsopt[i]);
		}
	}
	return temp;
}
function setUserAlarmConfig( data ){
	if( !data ){
		sessionStorage.removeItem(NotificationBlockEventConfig);
	}else{
		sessionStorage.setItem(NotificationBlockEventConfig,JSON.stringify(data));
    }
	NotificationManager.updateConfig();
}
function getUserAlarmConfig(){

    let configString= sessionStorage.getItem(NotificationBlockEventConfig);
    if( !configString ){
        return [];
    }
    return JSON.parse(configString);
}

function search(value){
	let keyy = Object.keys(codelist)
	for(var i=0; i<keyy.length;i++){
		if(codelist[keyy[i]] == value){
			return keyy[i];
		}
	}
}

function getUserGroupoff(){
    	let wsopt = JSON.parse(sessionStorage.getItem(NotificationBlockEventConfig));
    	temp = [];
    	if(wsopt.length==0){return [];}
    	for(var i=0;i<wsopt.length;i++){
    		if(wsopt[i].CH_ALARM_CODE == "UserGroup"){
    			temp.push(wsopt[i].WS_ID);
    		}
    	}
    	return temp;
}

function getPushUserGroupoff(){
	let wsopt = JSON.parse(sessionStorage.getItem(NotificationBlockEventConfig));
	temp = [];
	if(wsopt.length==0){return [];}
	for(var i=0;i<wsopt.length;i++){
		if(wsopt[i].CH_ALARM_CODE == "pushUserGroup"){
			temp.push(wsopt[i].WS_ID);
		}
	}
	return temp;
}

function getSysGroupoff(){
	let wsopt = JSON.parse(sessionStorage.getItem(NotificationBlockEventConfig));
	temp = [];
	if(wsopt.length==0){return [];}
	for(var i=0;i<wsopt.length;i++){
		if(wsopt[i].WS_ID == "System"){
			temp.push(wsopt[i].CH_ALARM_CODE);
		}
	}
	return temp;
}
function userAlarmGet(userid, wsid, chtype){
	var res;
	$.ajax({
        url: _workspace.url+'Users/AlarmSet', //Service Object
        type: 'GET',
        data: JSON.stringify({
        	"dto" :{
        		"USER_ID" : userid,
        		"WS_ID" : wsid,
        		"CH_TYPE" : chtype
        	  }
        }),
        dataType: 'json',
        async: false,
        contentType: 'application/json; charset=utf-8',
        xhrFields: {

            withCredentials: true

          },
        crossDomain: true,
        contentType: 'application/json; charset=UTF-8',
        success: function(result) {
        	res = result.dto.dtos;
        },
        error: function(error) {
        	return "Error"
        }
    })		
    return res;
}
/**** setUserAlarmConfig ****/


// 프로필 사진 불러오기
function userPhotoGet (userId = userManager.getLoginUserId(), size = 'medium', exe = null){
    if(size === "back"){
        return "/photo/profile/back/"+userId + "."+exe + "?"+ new Date();
    }else if (size === "small"){
        if(exe) return "/photo/profile/small/"+userId + "."+exe + "?"+ new Date();
        else return getUserDefaultPhotoURL(userId);
    }else if (size === "talk"){
        if(exe) return "/photo/profile/small/"+userId + "."+exe;
        else return getUserDefaultPhotoURL(userId);
    }else{
        if(exe) return "/photo/profile/medium/"+userId + "."+exe + "?"+ new Date();
        else return getUserDefaultPhotoURL(userId);
    }
}
 //

function getDefaultBackground (tmp){
	const idx = tmp[0].match(/([0-1]+)?([2-3]+)?([4-5]+)?([6]+)?([7]+)?([8]+)?([9]+)?([a]+)?([b]+)?([c]+)?([d]+)?([e-f]+)?/).slice(1).findIndex(v => typeof v !== 'undefined') + 1;
	return `res/profile/profile_BG0${idx}.jpg`;
}

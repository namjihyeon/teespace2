function ajax_call(serv_name, action, parameter, type, inputdata){	
    if(type.toUpperCase() == "GET") {
        parameter += "&rn=" + Date.now();
    }
    
	let outputdata = null;
	try {
    	Top.Ajax.execute({
    		url : _workspace.fileUrl + serv_name + action + parameter,
    		type : type,
    		dataType : "json",
    		async : false,
    		cache : false,
    		data : inputdata,
            contentType : "application/json",
            crossDomain : true,
            xhrFields : {
                withCredentials: true
            },
    		success : function(data) {
    		    try {
    		        if(typeof data == "string") {
    		            data = JSON.parse(data);
    		        }
                    
        			outputdata = data.dto;
        			if(outputdata.result != undefined) {
        			    outputdata.result = outputdata.result.toUpperCase();
        			}
    		    } catch(error) {
    		        outputdata = {
    	                result : "FAIL",
    	                message : "UnknownError.(CORS?)... JSON.stringify(error) : " + JSON.stringify(error),
    	                data : error
    		        }
    		    }
    		},
    		error : function(data) {
    			outputdata = {
    		        result : "FAIL",
    		        message : data.exception != undefined ? data.exception.message : "Error",
    		        data : data
    			}	
    			if(data.exception != undefined) {
    			    console.log(data.exception.message);
    			}
    		}
    	});
	} catch(ex) {
	    StartLoader(false);
	    console.log("Service URL : " + _workspace.fileUrl + serv_name + action + parameter);
	    ALERT("CORS");
	}
	
	return outputdata;
}

var tableSortingInfo = {
    columnIndex : 4,
    order : "desc",
    
    columnIndex_recycleBin : 5,
    order_recycleBin : "desc"
}    
function sortTable(columnIndex, order, tableId) {
    var fileContentsTableView = Top.Dom.selectById(tableId);
    var columnIdx = columnIndex;
    var nextState = order;
    switch(tableId) {
        case "fileContentsTableView":
            fileRepo.curFolderFileList.sort(function (a,b) {
                let returnVal;
                if(!a.is_file && b.is_file) {
                    returnVal = -1;
                } else if(a.is_file && !b.is_file) {
                    returnVal = 1;
                } else {
                    switch(columnIdx) {
                        case 1:
                            returnVal = (a.file_extension.toLowerCase() < b.file_extension.toLowerCase()) ? -1 : (a.file_extension.toLowerCase() > b.file_extension.toLowerCase()) ? 1 : 0;
                            break;
                        case 2:
                            returnVal = (a.fileFullName < b.fileFullName) ? -1 : (a.fileFullName > b.fileFullName) ? 1 : 0;
                            break;
                        case 3:
                            returnVal = (a.file_size < b.file_size) ? -1 : (a.file_size > b.file_size) ? 1 : 0;
                            break;
                        case 4:
                            returnVal = (a.file_updated_at < b.file_updated_at) ? -1 : (a.file_updated_at > b.file_updated_at) ? 1 : 0;
                            break;
                        case 5:
                            returnVal = (a.file_last_update_user_name < b.file_last_update_user_name) ? -1 : (a.file_last_update_user_name > b.file_last_update_user_name) ? 1 : 0;
                            break;
                    }                
                    
                    if(nextState == "desc") {
                        returnVal = -returnVal;
                    }
                }
                
                return returnVal;
            });

            fileRepo.update("curFolderFileList");
            
            tableSortingInfo.columnIndex = columnIdx;
            tableSortingInfo.order = order;
            break;
        case "fileRecycleBinTableView":
            fileRepo.deletedFileList.sort(function (a,b) {
                let returnVal;
                if(!a.is_file && b.is_file) {
                    returnVal = -1;
                } else if(a.is_file && !b.is_file) {
                    returnVal = 1;
                } else {
                    switch(columnIdx) {
                        case 1:
                            returnVal = (a.file_extension.toLowerCase() < b.file_extension.toLowerCase()) ? -1 : (a.file_extension.toLowerCase() > b.file_extension.toLowerCase()) ? 1 : 0;
                            break;
                        case 2:
                            returnVal = (a.fileFullName < b.fileFullName) ? -1 : (a.fileFullName > b.fileFullName) ? 1 : 0;
                            break;
                        case 3:
                            returnVal = (a.file_parent_name < b.file_parent_name) ? -1 : (a.file_parent_name > b.file_parent_name) ? 1 : 0;
                            break;
                        case 4:
                            returnVal = (a.file_size < b.file_size) ? -1 : (a.file_size > b.file_size) ? 1 : 0;
                            break;
                        case 5:
                            if(a.file_removed_at.length != b.file_removed_at.length) {
                                returnVal = a.file_removed_at.length < b.file_removed_at.length ? 1 : -1; 
                            } else {
                                returnVal = (a.file_removed_at < b.file_removed_at) ? -1 : (a.file_removed_at > b.file_removed_at) ? 1 : 0;
                            }
                            break;
                    }                
                    
                    if(nextState == "desc") {
                        returnVal = -returnVal;
                    }
                }
                
                return returnVal;
            });
            
            fileRepo.update("deletedFileList");
            
            tableSortingInfo.columnIndex_recycleBin = columnIdx;
            tableSortingInfo.order_recycleBin = order;
            break;
    }
    
    fileContentsTableView.render();
    
    setTimeout(function() {        
        let curPageNum = -1;
        try {
            curPageNum = fileContentsTableView.getPageInfo().page;
        } catch(ex) {
            console.log(ex);
            checkDOM("#" + tableId, function() {
                curPageNum = fileContentsTableView.getPageInfo().page;
            });
        } 
        
        let maxPageNum = fileContentsTableView.getPageInfo().pages;
        fileContentsTableView.goToPage(curPageNum > maxPageNum ? maxPageNum : curPageNum);
        
        let headers = [];
        try {
            headers = fileContentsTableView.template.getHeaders();
        } catch(ex) {
            console.log(ex);
            checkDOM("#" + tableId, function() {
                headers = fileContentsTableView.template.getHeaders();
            });
        }
        headers.pop();
        for(var i=0; i<headers.length; i++) {
            if(columnIndex == i) {
                fileContentsTableView.template.__setHeaderSortState(headers[i], nextState);
            } else {
                headers[i].removeClass("sorting_asc");
                headers[i].removeClass("sorting_desc");
            }
        }        
        
        StartLoader(false);
    }, 1);
}

function invalidateUI(callback) {
    StartLoader();
    setTimeout(function() {
        try {
            var fileTreeView = Top.Dom.selectById("fileTreeView"); 
            let selectedNode = fileTreeView.getSelectedNode();
            
            if(selectedNode.id == "recycleBin" && fileRepo.deletedFileList.length > 0) {
                Top.Dom.selectById("fileRecycleBinTableView").checkAll(false);
            } else if(selectedNode.id != "recycleBin" && fileRepo.curFolderFileList.length > 0) {
                Top.Dom.selectById("fileContentsTableView").checkAll(false);
            }
            
            fileTreeView.deselectNode(selectedNode.id);
            Top.Controller.get("fileLnbLayoutLogic").initTreeView();
    
            if(fileTreeView.getNode(selectedNode.id) == undefined) {
                fileTreeView.selectNode("root");
            } else {
                fileTreeView.selectNode(selectedNode.id);
            }
        } catch(error) {
            // console.log("onRefreshFileList : " + error);
        } finally {
            StartLoader(false);
            if(callback != undefined && typeof callback == "function") {
                setTimeout(function() { callback() }, 1);
            }
        }
    }, 1);
}

function getContentTypeByFileExtension(fileExt) {
    if(fileExt == undefined || fileExt == "") {
        return "";
    }
    
    if(MimeTypes[fileExt.toLowerCase()] == undefined) {
        return "text/html";
    }
    
    return MimeTypes[fileExt.toLowerCase()];
}

function StartLoader(isStart) {
    if(isStart == undefined || isStart) {
        Top.Loader.start("large");
    } else {
        //setTimeout(function() { Top.Loader.stop(); }, 1);
        Top.Loader.stop();
    }
}

function S(stringKey, p0, p1, p2, p3) {
    return String.format(Top.i18n.t(stringKey), p0, p1, p2, p3);
}
function errCodeToString(errCode) {
    switch(errCode) {
        case "ERR0100": return S("MSG_TD_CONTENTS_CONTEXT_MENU_FOLDER_MOVE_ERROR_SAME_FOLDER");
        case "ERR0101": return S("MSG_TD_CONTENTS_CONTEXT_MENU_FOLDER_MOVE_ERROR_UNDER_FOLDER");
    }
}


function getFileChannelId(workspaceId) {
    if(String.isNullOrEmpty(workspaceId)) {
        workspaceId = routeManager.getWorkspaceId();
    }
    
    return workspaceManager.getChannelList(workspaceId, "CHN0006")
}
const TDrive = (function() {  
    var _fileUploadTmpId = 0;
    
    DTO = (function() {
        getUploadFileDTO = function(fileName, fileExt, fileSize, fileParentId, tmpId) {
            if(String.isNullOrEmpty(fileName) || String.isNullOrEmpty(fileExt) || String.isNullOrEmpty(fileParentId)) {
                return undefined;
            }
            if(tmpId == undefined || tmpId < 0) {
                return undefined;
            }
            if((file.size >= MAX_UPLOAD_FILE_SIZE_MB * 1024 * 1024) || file.size <= 0) {
                return undefined;
            }

            let loginUserId = userManager.getLoginUserId();
            let loginUserName = userManager.getLoginUserName();
            return {
                dto : {
                    user_id         : loginUserId,
                    workspace_id    : getWorkspaceId(),
                    channel_id      : getChannelId(),
                    file_parent_id  : fileParentId,
                    storageFileInfo : {
                        user_id                  : loginUserId,
                        file_last_update_user_id : loginUserId,
                        file_name                : fileName,
                        file_extension           : fileExt,
                        file_size                : fileSize,
                        user_context_1           : tmpId,
                        user_context_2           : false.toString()
                    },
                                    
                    file_user_name             : loginUserName,
                    file_last_update_user_name : loginUserName,                
                }
            } 
        };
        getUploadFolderDTO = function(folderName, fileParentId) {
            if(String.isNullOrEmpty(folderName) || String.isNullOrEmpty(fileParentId)) {
                return undefined;
            }
            
            return {
                dto : {
                    user_id          : userManager.getLoginUserId(),
                    workspace_id     : getWorkspaceId(),
                    channel_id       : getChannelId(),
                    folder_name      : folderName,
                    folder_parent_id : fileParentId,
                }
            }
        };
        getCopyFileDTO = function(fileMeta, newFileName, parentFolderId) { // "Shallow"
            let loginUserId = userManager.getLoginUserId();
            
            return {
                dto : {
                    file_parent_id  : parentFolderId,
                    workspace_id    : getWorkspaceId(),
                    channel_id      : getChannelId(),
                    user_id         : loginUserId,
                    storageFileInfo : {
                        file_id                  : fileMeta.file_id,
                        user_id                  : loginUserId,
                        file_last_update_user_id : loginUserId,
                        file_name                : newFileName,
                        file_extension           : fileMeta.file_extension
                    },
                }
            }
        };
        
        getExportCopyFileDTO = function(fileMeta, newFileName, parentFolderId, dstChannel, context_1, context_2, context_3){
            let loginUserId = userManager.getLoginUserId();
            
            return {
                dto : {
                    file_parent_id  : parentFolderId,
                    workspace_id    : getWorkspaceId(),
                    channel_id      : dstChannel,
                    user_id         : loginUserId,
                    storageFileInfo : {
                        file_id                  : fileMeta.file_id,
                        user_id                  : loginUserId,
                        file_last_update_user_id : loginUserId,
                        file_name                : newFileName,
                        file_extension           : fileMeta.file_extension,
                        user_context_1           : context_1,
                        user_context_2           : context_2,
                        user_context_3           : context_3
                    },
                }
            }
        };
        
        getRestoreFileDTO = function(fileMetas) {
            if(!Array.isArray(fileMetas)) {
                return undefined;
            }
            
            let file_ids = [];
            for(var i=0; i<fileMetas.length; i++) {
                file_ids.push({ file_id : fileMetas[i].file_id })
            }
            
            return {
                dto : {
                    user_id      : userManager.getLoginUserId(),
                    workspace_id : getWorkspaceId(),
                    channel_id   : getChannelId(),
                    file_ids     : file_ids,
                }
            }
        };
        
        getMoveFileDTO = function(fileMetas, dstFolderId) {
            if(!Array.isArray(fileMetas) || String.isNullOrEmpty(dstFolderId)) {
                return undefined
            }
            
            let file_ids = [];
            for(var i=0; i<fileMetas.length; i++) {
                file_ids.push({ 
                    file_id : fileMetas[i].file_id, 
                    is_file : fileMetas[i].is_file 
                })
            }
            
            return {
                dto : {
                    user_id      : userManager.getLoginUserId(),
                    workspace_id : getWorkspaceId(),
                    channel_id   : getChannelId(),
                    file_ids     : file_ids,
                    dstFolderId  : dstFolderId,
                }
            }
        };
        getRenameFileDTO = function(targetFileId, newFileName, fileExtension) {
            if(String.isNullOrEmpty(newFileName)) {
                return undefined;
            }
            
            return {
                dto : {
                    user_id         : userManager.getLoginUserId(),
                    workspace_id    : getWorkspaceId(),
                    channel_id      : getChannelId(),
                    storageFileInfo : {
                    	user_id					 : userManager.getLoginUserId(),
                        file_id                  : targetFileId,
                        file_name                : newFileName,
                        file_extension           : fileExtension,
                        file_last_update_user_id : userManager.getLoginUserId(),
                    }
                }
            }
        };
        getRenameFolderDTO = function(targetFileId, newFileName) {
            if(String.isNullOrEmpty(newFileName)) {
                return undefined;
            }
            
            return {
                dto : {
                    user_id      : userManager.getLoginUserId(),
                    workspace_id : getWorkspaceId(),
                    channel_id   : getChannelId(),
                    file_id      : targetFileId,
                    file_name    : newFileName,
                }
            }
        };
        
        getRemoveFileDTO = function(fileIDs) {
            if(!Array.isArray(fileIDs)) {
                return undefined;
            }
            
            let file_ids = [];
            for(var i=0; i<fileIDs.length; i++) {
                file_ids.push({ file_id : fileIDs[i] })
            }
            
            return {
                dto : {
                    user_id                  : userManager.getLoginUserId(),
                    workspace_id             : getWorkspaceId(),
                    channel_id               : getChannelId(),
                    file_ids                 : file_ids,
                    file_last_update_user_id : userManager.getLoginUserId(),
                }
            }
        };  
        getDeleteFileDTO = function(fileIDs) {
            if(!Array.isArray(fileIDs)) {
                return undefined;
            }
            
            let file_ids = [];
            for(var i=0; i<fileIDs.length; i++) {
                file_ids.push({ file_id : fileIDs[i] })
            }
            
            return {
                dto : {
                    user_id      : userManager.getLoginUserId(),
                    workspace_id : getWorkspaceId(),
                    channel_id   : getChannelId(),
                    file_ids     : file_ids,
                }
            }
        }
        
        getStorageUpdateDTO = function(fileId, userContext1, userContext2) {
            if(String.isNullOrEmpty(fileId)) {
                return undefined;
            }
            
            return {
                dto : {
                    workspace_id    : getWorkspaceId(),
                    channel_id      : getChannelId(),
                    user_id         : userManager.getLoginUserId(),
                    storageFileInfo : {
                        file_id        : fileId,
                        user_id        : userManager.getLoginUserId(),
                        user_context_1 : userContext1,
                        user_context_2 : userContext2
                    }
                }
            }
        }
        
        return Object.freeze({
            getUploadFileDTO      : getUploadFileDTO,
            getUploadFolderDTO    : getUploadFolderDTO,
            getCopyFileDTO        : getCopyFileDTO,
            getExportCopyFileDTO  : getExportCopyFileDTO,
            getRestoreFileDTO     : getRestoreFileDTO,

            getMoveFileDTO        : getMoveFileDTO,
            getRenameFileDTO      : getRenameFileDTO,
            getRenameFolderDTO    : getRenameFolderDTO, 
            
            getRemoveFileDTO      : getRemoveFileDTO,
            getDeleteFileDTO      : getDeleteFileDTO,
            
            getStorageUpdateDTO : getStorageUpdateDTO
        });
    })();
    
    _ajaxCall = function(serviceUrl, type, inputDTO) {    
        if(type.toUpperCase() == "GET") {
            serviceUrl += "&rn=" + Date.now();
        }
        
        let outputData = null;
        try {
            Top.Ajax.execute({
                url : String.format("{0}{1}", _workspace.fileUrl, serviceUrl),
                type : type,
                dataType : "json",
                async : false,
                cache : false,
                data : inputDTO,
                contentType : "application/json",
                crossDomain : true,
                xhrFields : {
                    withCredentials: true
                },
                success : function(data) {
                    try {
                        if(typeof data == "string") {
                            data = JSON.parse(data);
                        }
                        
                        outputData = data.dto;
                        if(outputData.result != undefined) {
                            outputData.result = outputData.result.toUpperCase();
                        }
                    } catch(error) {
                        outputData = {
                            result : "FAIL",
                            message : "UnknownError.(CORS?)... JSON.stringify(error) : " + JSON.stringify(error),
                            data : error
                        }
                    }
                },
                error : function(data) {
                    outputData = {
                        result : "FAIL",
                        message : data.exception != undefined ? data.exception.message : "Error",
                        data : data
                    }   
                    if(data.exception != undefined) {
                        console.log(data.exception.message);
                    }
                }
            });
        } catch(ex) {
            StartLoader(false);
        }
        
        return outputData;
    }
    
    _onBeforeLoadFiles = function(files) {
        let newFileList = [];
        let file = {};
        let checkFileSize = {};
        let nomFileFullName = "";
        let fileNameArray = [];
        let fileExt = "";
        for(var i=0; i<files.length; i++) {
            file = files[i];
            
            nomFileFullName = Top.Util.Browser.isIE() ? file.name : file.name.normalize('NFC');
            fileNameArray = nomFileFullName.split('.');
            fileExt = fileNameArray.length == 1 ? "" : fileNameArray[fileNameArray.length - 1];        
            if(fileExt == "") {
                return {
                    result : false,
                    message : S("file.CommonLogic.FILE_EXTENSION_NOT_EXIST") 
                }
            }
            
            let fileName = nomFileFullName.substring(0, nomFileFullName.lastIndexOf(fileExt) - 1);
            if(fileName.length > MAX_FILE_NAME_LENGTH) {
                return {
                    result : false,
                    message : S("MSG_TD_CONTENTS_UPLOADING_HOVER_ERROR") // 70자 제한
                }
            }       
            
            newFileList.push({
                file_id         : _fileUploadTmpId++,
                iconClassName   : getFileIconClass(fileExt),
                file_extension  : fileExt,
                fileFullName    : nomFileFullName,
                file_status     : FileStatus.TRY_UPLOAD,
                is_file         : true
            });
        }

        return {
            result : true,
            newFileList : newFileList
        }
    };    
    
    _convertFolderDtoToModel = function(folderDto) {
        let valid = folderDto.hasOwnProperty("folder_id")
                 && folderDto.hasOwnProperty("folder_channel_id")
                 && folderDto.hasOwnProperty("folder_name")
                 && folderDto.hasOwnProperty("folder_parent_id");
         
        if(!valid) {
            return undefined;
        }
        
        let model = {
            file_id                    : folderDto.folder_id,
            file_channel_id            : folderDto.folder_channel_id,
            file_name                  : folderDto.folder_name,
            file_extension             : String.empty,
            file_size                  : 0,
            file_owner                 : EM_DASH,
            file_created_at            : EM_DASH,
            file_last_update_user_name : EM_DASH,
            file_parent_id             : folderDto.folder_parent_id,
            file_status                : FileStatus.NORMAL,
            is_file                    : false,
            file_updated_at            : EM_DASH,
        }
        model.fileFullName = model.file_name
        model.fileUpdatedAtMeridiem = EM_DASH
        model.fileSizeText = EM_DASH;
        model.iconClassName = "icon-work_folder";
        model.iconTooltip = "폴더";
        
        return model;
    };    
    _convertFileDtoToModel = function(fileDto) {
        let valid = fileDto.hasOwnProperty("file_id")
                 && fileDto.hasOwnProperty("file_name")
                 && fileDto.hasOwnProperty("file_extension")
                 && fileDto.hasOwnProperty("file_size")
                 && fileDto.hasOwnProperty("file_user_name")
                 && fileDto.hasOwnProperty("file_created_at")
                 && fileDto.hasOwnProperty("file_last_update_user_name")
                 && fileDto.hasOwnProperty("file_parent_id")
                 && fileDto.hasOwnProperty("file_updated_at"); 
                 
        if(!valid) {
            return undefined;
        }
                 
        let model = {
            file_id                    : fileDto.file_id,
            file_channel_id            : String.empty,
            file_name                  : fileDto.file_name,
            file_extension             : fileDto.file_extension,
            file_size                  : fileDto.file_size,
            file_owner                 : fileDto.file_user_name,
            file_created_at            : fileDto.file_created_at,
            file_last_update_user_name : fileDto.file_last_update_user_name,
            file_parent_id             : fileDto.file_parent_id,
            file_status                : FileStatus.NORMAL,
            is_file                    : true,
            file_updated_at            : fileDto.file_updated_at,
        }
        model.fileFullName = String.format("{0}.{1}", model.file_name, model.file_extension);
        model.fileUpdatedAtMeridiem = getFileDateFormat(model.file_updated_at);
        model.fileSizeText = getFileSizeText(model.file_size);
        model.iconClassName = getFileIconClass(model.file_extension);
        model.iconTooltip = getFileIconTooltip(model.file_extension);
        
        return model;
    };
    
    getWorkspaceId = function() {
        let roomId = routeManager.getRoomId();
        return (String.isNullOrEmpty(roomId) || roomId == 0) ? routeManager.getWorkspaceId() : roomId;
    };
    getChannelId = function() {
        let channelId = String.empty;
        
        let roomId = routeManager.getRoomId();
        if(!String.isNullOrEmpty(roomId)) {
            return workspaceManager.getRoomChannelList(roomId, CLOUDSPACE_CHANNELS.TDRIVE.CHANNEL_TYPE);
        }
        
        let workspaceId = routeManager.getWorkspaceId();
        return workspaceManager.getChannelList(workspaceId, CLOUDSPACE_CHANNELS.TDRIVE.CHANNEL_TYPE)
    };
    getCurrentFolderId = function() {
        return Top.Dom.selectById("fileTreeView").getSelectedNode().id;
    };
    
    uploadFiles = function(files) {
        let beforeCheck = _onBeforeLoadFiles(files);
        if(!beforeCheck.result) {
            ALERT(beforeCheck.message);
            return;
        }
        
        Top.Controller.get("fileUploadStatusLayoutLogic").initUploadStatus(beforeCheck.newFileList);
        
        var inputDTO = {};
        let file = {};
        let nomFileFullName = String.empty;
        let fileNameArray = [];
        let fileName = String.empty;
        let fileExt = String.empty;
        let fileParentId = TDrive.getCurrentFolderId();
        if(fileParentId == "recycleBin") {
            fileParentId = "root";
        }
        let tmpId = -1;
        let _successCallback = function(res) {
            let tmpId = res.dto.storageFileInfoList[0].user_context_1;
            let realFileId = res.dto.storageFileInfoList[0].file_id;
            Top.Controller.get("fileUploadStatusLayoutLogic").uploadSuccess(tmpId, realFileId);
            
            let storageFileUpdateUrl = String.format("{0}Storage/StorageMeta", _workspace.url);
            let storageFileUpdateDto = DTO.getStorageUpdateDTO(realFileId, tmpId, true.toString());
            Top.Ajax.put(storageFileUpdateUrl, storageFileUpdateDto, undefined, function() { invalidateUI(); });
        };    
        let _errorCallback = function(res) {
            let tmpId = res.dto.storageFileInfoList[0].user_context_1;
            Top.Controller.get("fileUploadStatusLayoutLogic").uploadFail(tmpId);
        };
        for(var i=0; i<beforeCheck.newFileList.length; i++) {
            file = files[i];
            nomFileFullName = Top.Util.Browser.isIE() ? file.name : file.name.normalize('NFC');
            fileNameArray = nomFileFullName.split('.');
            fileExt = fileNameArray[fileNameArray.length - 1];  
            fileName = String.isNullOrEmpty(fileExt) ? nomFileFullName : nomFileFullName.substring(0, nomFileFullName.lastIndexOf(fileExt) - 1)
            tmpId = beforeCheck.newFileList[i].file_id;
            inputDTO = DTO.getUploadFileDTO(fileName, fileExt, file.size, fileParentId, tmpId)
            storageManager.UploadFile(file, inputDTO, "File", "SODriveFileMeta", _successCallback, _errorCallback)
        }
    };
    
    makeFolder = function(folderName, fileParentId) {
        return _ajaxCall("SODriveFolderMeta", "POST", DTO.getUploadFolderDTO(folderName, fileParentId));
    };
    
    copyFile = function(targetFile, newFileName, parentFolderId) {
        let inputDTO = DTO.getCopyFileDTO(targetFile, newFileName, parentFolderId);
        return _ajaxCall("SODriveCopy?action=", "POST", inputDTO); 
    };
    
    exportCopyFile = function(targetFile, newFileName, parentFolderId, dstChannel, user_context_1, user_context_2, user_context_3) {
        let inputDTO = DTO.getExportCopyFileDTO(targetFile, newFileName, parentFolderId, dstChannel, user_context_1, user_context_2, user_context_3);
        return _ajaxCall("SODriveCopy?action=", "POST", inputDTO);
    };
    
    restoreFile = function(targetFiles) {
        let inputDTO = DTO.getRestoreFileDTO(targetFiles);
        let response = _ajaxCall("SODrive?action=Restore", "POST", inputDTO);
        
        if(response.result == "OK") {
            invalidateUI();
        } else {
            ALERT("복원 실패 : " + response.message);
        }
    };
    
    getFolderList = function() {
        let serviceUrl = String.format("SODriveFolderMeta?action=List&channelID={0}", getChannelId());
        return _ajaxCall(serviceUrl, "GET")
    };
    
    getFileList = function() {
        let serviceUrl = String.format("SODriveFileMeta?action=List&workspaceID={0}&channelID={1}", getWorkspaceId(), getChannelId());
        return _ajaxCall(serviceUrl, "GET")
    };
    
    getAllFileFolderList = function() {
        let allFileArray = [];
        
        let response = getFolderList();
        if(response.hasOwnProperty("folderMetaList") && response.folderMetaList != undefined) {            
            let folderList = response.folderMetaList;
            
            for(var i=0; i<folderList.length; i++) {
                var folderModel = _convertFolderDtoToModel(folderList[i]);
                if(folderModel != undefined) {
                    allFileArray.push(folderModel);
                }
            }
        } 
        
        response = getFileList();
        if(response.hasOwnProperty("fileMetaList") && response.fileMetaList != undefined) {
            let fileList = response.fileMetaList;
            for(var i=0; i<fileList.length; i++) {
                if(fileList[i].user_context_2 == false.toString()) {
                    continue;
                }
                
                var fileModel = _convertFileDtoToModel(fileList[i]);
                if(fileModel != undefined) {
                    allFileArray.push(fileModel);
                }
            }
        } 
        
        return allFileArray;
    };
    
    getRecycleBinList = function() {
        let serviceUrl = String.format("SODriveDeletedMeta?action=List&workspaceID={0}&channelID={1}", getWorkspaceId(), getChannelId());
        let response = _ajaxCall(serviceUrl, "GET");
        
        let allMetaArray = [];
        let newData = {};
        let removedMeta = {};
        if(response.hasOwnProperty("deletedFileMetaList") && response.deletedFileMetaList != undefined) {
            for(var i=0; i<response.deletedFileMetaList.length; i++) {
                removedMeta = response.deletedFileMetaList[i];
                newData = {
                    file_id               : removedMeta.file_id,
                    file_name             : removedMeta.file_name,
                    file_extension        : removedMeta.file_extension,
                    file_size             : removedMeta.file_size,
                    file_parent_id        : removedMeta.file_parent_id,
                    file_status           : FileStatus.REMOVED,
                    file_parent_name      : removedMeta.file_full_parent_name == "root" ? S("MSG_TD_LNB_LIST_STORAGE") : removedMeta.file_parent_name,
                    file_full_parent_name : removedMeta.file_full_parent_name == "root" ? S("MSG_TD_LNB_LIST_STORAGE") : removedMeta.file_full_parent_name,
                    is_file               : true,
                    file_removed_by       : removedMeta.file_deleted_by,
                    file_removed_at       : removedMeta.file_deleted_at,
                };
                newData.fileFullName = String.format("{0}.{1}", newData.file_name, newData.file_extension);
                newData.fileRemovedAtMeridiem = getFileDateFormat(newData.file_removed_at);
                newData.fileSizeText = getFileSizeText(newData.file_size);
                newData.iconClassName = getFileIconClass(newData.file_extension);
                newData.iconTooltip = getFileIconTooltip(newData.file_extension);
                
                allMetaArray.push(newData);
            }
        }

        if(response.hasOwnProperty("deletedFolderMetaList") && response.deletedFolderMetaList != undefined) {
            for(var i=0; i<response.deletedFolderMetaList.length; i++) {
                removedMeta = response.deletedFolderMetaList[i];
                newData = {
                    file_id               : removedMeta.file_id,
                    file_name             : removedMeta.file_name,
                    file_size             : removedMeta.file_size,
                    file_parent_id        : removedMeta.file_parent_id,
                    file_status           : FileStatus.REMOVED,
                    file_parent_name      : removedMeta.file_full_parent_name == "root" ? S("MSG_TD_LNB_LIST_STORAGE") : removedMeta.file_parent_name,
                    file_full_parent_name : removedMeta.file_full_parent_name == "root" ? S("MSG_TD_LNB_LIST_STORAGE") : removedMeta.file_full_parent_name,
                    is_file               : false,
                    file_removed_by       : removedMeta.file_deleted_by,
                    file_removed_at       : removedMeta.file_deleted_at
                };
                newData.fileFullName = newData.file_name;
                newData.fileRemovedAtMeridiem = getFileDateFormat(newData.file_removed_at);
                newData.fileSizeText = EM_DASH
                newData.iconClassName = Icons.FOLDER;
                newData.iconTooltip = getFileIconTooltip(newData.file_extension);
                
                allMetaArray.push(newData);
            }
        }
        
        return allMetaArray;
    };
    
    moveFile = function(targetFiles, dstFolderId) {
        let inputDTO = DTO.getMoveFileDTO(targetFiles, dstFolderId);
        let response = _ajaxCall("SODrive?action=Move", "PUT", inputDTO);
        
        if(response.result == "OK") {
            invalidateUI();
        } else {
            ALERT("이동 실패 : " + response.message);
        }
    };
    
    renameFile = function(targetFileId, newFileName, fileExtension) {
        let inputDTO = DTO.getRenameFileDTO(targetFileId, newFileName, fileExtension);
        return _ajaxCall("SODriveFileMeta", "PUT", inputDTO);
    };
    
    renameFolder = function(targetFileId, newFolderName) {
        let inputDTO = DTO.getRenameFolderDTO(targetFileId, newFolderName);
        return _ajaxCall("SODriveFolderMeta", "PUT", inputDTO);
    };
    
    removeFile = function(targetFiles) {
        if(!Array.isArray(targetFiles)) {
            targetFiles = [targetFiles];
        }

        let isFile = false;
        let isFolder = false;
        let targetFileIDs = [];
        for(var i=0; i<targetFiles.length; i++) {
            targetFileIDs.push(targetFiles[i].file_id);

            if(targetFiles[i].is_file) {
                isFile = true;
            } else {
                isFolder = true;
            }
        }
        
        let inputDTO = DTO.getRemoveFileDTO(targetFileIDs);              
        let response = _ajaxCall("SODriveMeta?action=Remove", "POST", inputDTO);
        if(response.result != "OK") {
            ALERT("삭제 실패 : " + response.message);
            return;
        } 
        invalidateUI(function() {
            let notiMessage = "";
        	//let item = "";
            if(isFile && isFolder){ // item = "항목"
                notiMessage = S("MSG_TD_CONTENTS_CONTEXT_MENU_FILE_DELETE_MESSAGE_ALL", targetFiles.length);	  
            } else if (isFolder){ // item = "폴더"
            	notiMessage = S("MSG_TD_CONTENTS_CONTEXT_MENU_FILE_DELETE_MESSAGE_FOLDER", targetFiles.length);
            } else{ // item = "파일"
            	notiMessage = S("MSG_TD_CONTENTS_CONTEXT_MENU_FILE_DELETE_MESSAGE_FILE", targetFiles.length);
            }
            
            notiFeedback(notiMessage);
        });
    };
    
    deleteFile = function(targetFiles) {
        let targetFileIDs = [];
        for(var i=0; i<targetFiles.length; i++) {
            targetFileIDs.push(targetFiles[i].file_id);
        }
        
        let inputDTO = DTO.getDeleteFileDTO(targetFileIDs);
        let response = _ajaxCall("SODriveFileMeta", "DELETE", inputDTO); 
            
        if(response.result == "OK") {
            invalidateUI();
        } else {
            ALERT("영구 삭제 실패 : " + response.message);
        }
    };

    isIOS = function() { // IOS 버전 13 이상에서의 문제 위해 뒷 부분 임시로 추가 -> 추후 수정 필요
        return (/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream) || (navigator.platform === 'MacIntel' && navigator.maxTouchPoints > 1);
    }

    isSafari = function() {
        return Top.Util.Browser.isSafari();
    }
    
    isOfficeFile = function(file_extension) {
        if(String.isNullOrEmpty(file_extension)) {
            return false;
        }
        
        file_extension = file_extension.toLowerCase();
        
        return file_extension == "ppt" || file_extension == "pptx" || file_extension == "tpt" ||
               file_extension == "doc" || file_extension == "docx" || file_extension == "toc" ||
               file_extension == "xls" || file_extension == "xlsx" || file_extension == "tls" ||
               file_extension == "csv";
    }
    
    return Object.freeze({
        getWorkspaceId        : getWorkspaceId,
        getChannelId          : getChannelId,
        getCurrentFolderId    : getCurrentFolderId,
        
        uploadFiles           : uploadFiles,
        makeFolder            : makeFolder,
        copyFile              : copyFile,
        exportCopyFile        : exportCopyFile,
        restoreFile           : restoreFile,
        
        getFolderList         : getFolderList,
        getFileList           : getFileList,
        getAllFileFolderList  : getAllFileFolderList,
        getRecycleBinList     : getRecycleBinList,

        moveFile              : moveFile,
        renameFile            : renameFile,
        renameFolder          : renameFolder,
        
        removeFile            : removeFile,
        deleteFile            : deleteFile,

        isIOS                 : isIOS,
        isSafari              : isSafari,
        isOfficeFile          : isOfficeFile
        
    });
})();


let mailLoad = false;

function mailRead__showLoadingBar() { 
	var maskHeight = $(document).height(); 
	var maskWidth = window.document.body.clientWidth; 
    var mask = "<div id='mask' style='position:absolute; z-index:9000; background-color:#FFFFFF; display:none; left:0; top:0;'></div>";
    var loadingImg = ''; 
    var media = window.matchMedia("(max-device-width: 1024px)");
    if( media.matches ){
       loadingImg += "<div id='loadingImg'; style='position:absolute; left:50%; top:50%; transform: translate(-50%, -50%) display:none; z-index:100000;' >";
    }
    else{
       loadingImg += "<div id='loadingImg'; style='position:absolute; left:50%; top:50%; transform: translate(-50%, -50%); display:none; z-index:100000;' >";
    }
	loadingImg += "<img src='./../../../../../res/bi_loading.gif'; style='width: 144px' />"; 
	loadingImg += "</div>"; 
//	var loadingMsg = '';
//	loadingMsg += "<div id='loadingMsg'; style='position:absolute; left:45.5%; top:55%; display:none; z-index:100000;'>"; 
//	loadingMsg += "<top-textview id='testview131' layout-width='200px' layout-height='50px' border-width='0px' text='로딩 중...' style='line-height:normal; font-size:18px; color: #000000; text-align:center;'>"; 
//	loadingMsg += "<span id = 'testview131>"
//	loadingMsg += "<a class='top-textview-url'>Loading...</a>";
//	loadingMsg += "</span>";
//	loadingMsg += "</top-textview>";
//	loadingMsg += "</div>";
    $('body').append(mask);
    
    
    $('#mask').append(loadingImg); 
//	$('#mask').append(loadingMsg);
    $('#mask').css('top',parseInt($('#mailReadLayout').offset().top));
    $('#mask').css('width',parseInt($('#mailReadLayout').width()));
    $('#mask').css('left',parseInt($('#mailReadLayout').offset().left));
    $('#mask').css('height',parseInt(700));
//	$('#mask').css({ 'width' : maskWidth , 'height': maskHeight}); 
    $('#mask').show(); 
    $('#loadingImg').show(); 
//	$('#loadingMsg').show(); 
}

function mailRead__hideLoadingBar() { $('#mask, #loadingImg, #loadingMsg').hide(); $('#mask, #loadingImg, #loadingMsg').remove(); }


let mail = (function () {
	return {
		changeTime : function(time) {
			
			var year,month,date,hour,minute,second, ampm;
			year = time.substr(0,4);
			month = time.substr(5,2);
			date = time.substr(8,2);
			hour = time.substr(11,2);
			minute = time.substr(14,2);
			
			
			if(hour < 12) {
			
				ampm = Top.i18n.get().value.m000147;
				
			} else {
				
				ampm = Top.i18n.get().value.m000148;
				if(hour != '12')  {
					hour = hour - 12;
					if(hour < 10 )
						hour = '0' + hour;
				}
				
			}
			var today = new Date();
			
			if(year == today.getFullYear()) {
				
				if(parseInt(month) == today.getMonth() + 1 && parseInt(date) == today.getDate()) {
					return ampm + ' ' + hour + ':' + minute;
				} else {
					return month + '.' + date + ' ' + ampm + ' ' + hour + ':' + minute;
				}
				
				
			} else {
				return year + '.' + month + '.' + date + ' ' + ampm + ' ' + hour + ':' + minute;
			}
			
		}, changeSize : function(size) {
			
			if      (size>=1073741824) {size=(size/1073741824).toFixed(2)+' GB';}
	        else if (size>=1048576)    {size=(size/1048576).toFixed(2)+' MB';}
	        else if (size>=1024)       {size=(size/1024).toFixed(2)+' KB';}
	        else if (size>1)           {size=size+' bytes';}
	        else if (size==1)          {size=size+' byte';}
	        else                        {size='0 byte';}
			
			return size;
		
		}, after30Days : function(time, separator) {
			
			if(time) {
				var year, month, date;
				year = time.substr(0,4);
				month = time.substr(5,2);
				date = time.substr(8,2);
				
				var today = new Date(year*1, (month-1)*1, date*1);
			
			} else
				var today = new Date();
			
			today.setDate(today.getDate() + 29);
			
			var mm = today.getMonth()+1; 
			if (mm.toString().length < 2)
				mm ='0'+ mm.toString();
			
			var dd = today.getDate(); 
			if (dd.toString().length < 2)
				dd ='0'+ dd.toString();
			
			return today.getFullYear() + separator + mm + separator + dd;
			
		}, tempTime : function() {
			
			var today = new Date();
			var time;
			var hour = today.getHours();
			
			if(hour == 12)
				time = '오후 ';
			else if (hour > 12){
				time = '오후 '; hour =  hour - 12;
			}
			else
				time = '오전 ';
			
			if (String(hour).length < 2)
				time = time + '0' + hour;
			else
				time = time + hour;
			
			var min = today.getMinutes();
			if(String(min).length < 2)
				time = time + ':0' + min;
			else
				time = time + ':' + min;
			
			var sec = today.getSeconds(); 
			if (String(sec).length < 2)
				time = time + ":0" + sec;
			else
				time = time + ":" + sec;
			
			
			return time;
		
		}, arrangeData : function(data) {
			
			if(data.REC_MEM_INFO != null) {
				
				if(mailData.curBox.WIDGET_ID.split('Text')[1].substr(0,1) != '0') {
					data.STATUS_ICON = 'none';
					if(data.OUTBOUND_STATUS == 'SND1001')
						data.STATUS = '전송중';
					else if(data.OUTBOUND_STATUS == 'SND1002')
						data.STATUS = '전송완료';
					else if(data.OUTBOUND_STATUS == 'SND1003')
						data.STATUS = '전송실패';
					
				} else if(data.REC_MEM_INFO.length == 1) {
					if(data.REC_MEM_INFO[0].FIRST_SAW_TIME == null) {
						
						data.STATUS = Top.i18n.get().value.m000149;
						data.STATUS_ICON = 'none';
					} else {
						
						data.STATUS_ICON = 'visible';
						data.STATUS = mail.changeTime(data.REC_MEM_INFO[0].FIRST_SAW_TIME);
						
					}
				} else {
					data.STATUS_ICON = 'none';
					var count = 0;
					var readCount = 0;
					for(var j=0; j<data.REC_MEM_INFO.length; j++) {
						count++;
						if(data.REC_MEM_INFO[j].FIRST_SAW_TIME != null)
							readCount++;
					}
					data.STATUS = count+'명 중 '+readCount+'명 읽음';
					
				}
				
				if(data.REC_MEM_INFO.length == 0)
					data.RECEIVER_NAME = Top.i18n.get().value.m000155;
				else if(data.REC_MEM_INFO[0].TO_WS_NAME == null && data.REC_MEM_INFO[0].TO_USER_NAME_IN_WS == null)
					data.RECEIVER_NAME = data.REC_MEM_INFO[0].TO_ADDR;
				else if(data.REC_MEM_INFO[0].TO_USER_NAME_IN_WS == null)
					data.RECEIVER_NAME = data.REC_MEM_INFO[0].TO_WS_NAME;
				else
					data.RECEIVER_NAME = data.REC_MEM_INFO[0].TO_USER_NAME_IN_WS;
			} else {
				data.STATUS_ICON = 'none';
			}
			
			
			if(data.HAS_ATTACHMENT_YN_CODE == 'COM0001')
				data.HAS_ATTACHMENT_YN_CODE = 'visible';
			else
				data.HAS_ATTACHMENT_YN_CODE = 'none';
			
			if(data.IMPORTANCE_YN_CODE == 'PRI0001')
				data.IMPORTANCE_YN_CODE = '#6C56E5'; //#FFC412 -> #6C56E5
			else
				data.IMPORTANCE_YN_CODE = '#C5C5C8';
			
			if(data.FOLDER_ID == mailData.curBox.FOLDER_ID)
				data.DISPLAY_NAME = '';
			else
				data.DISPLAY_NAME = '['+data.DISPLAY_NAME+']';
						
			if(data.SENDER_WS_NAME == Top.i18n.get().value.m000146 || data.SENDER_WS_NAME == 'MySpace')
				data.SENDER_NAME = data.SENDER_USER_NAME_IN_WS;
			else if(data.SENDER_WS_NAME != null)
				data.SENDER_NAME = data.SENDER_WS_NAME;
			else if(data.SENDER_NAME == null && data.SENDER_WS_NAME == null)
				data.SENDER_NAME = data.SENDER_ADDR;
			
			if(mailData.curBox.FOLDER_TYPE == 'MFL0003' || mailData.curBox.PARENT_FOLDER_TYPE == 'MFL0003')
				data.SENDER_NAME = data.RECEIVER_NAME;
			
			if(data.SEND_IMPORTANCE_CODE == 'IMP0001')
				data.SEND_IMPORTANCE_CODE = 'visible';
			else
				data.SEND_IMPORTANCE_CODE = 'hidden';
			
			data.RECEIVED_TIME = mail.changeTime(data.RECEIVED_TIME);
			
			if(data.SEEN_YN_CODE == 'REA0001') {
				data.TEXT_COLOR_SENDER = '#6A6C71';
				if(mailData.panelType == '3')
					data.TEXT_COLOR = '#6A6C71';
				else
					data.TEXT_COLOR = '#000000';
				data.TEXT_COLOR = '#6A6C71';
				data.TEXT_STYLE = '';
			} else {
				data.TEXT_COLOR_SENDER = '#000000';
				data.TEXT_COLOR = '#000000';
				data.TEXT_STYLE = 'bold';
			}
			
			return data;
			
		}, isWriting : function() {
			return mail.isWritingNew() || mail.isWritingToMe();
		}, isWritingNew : function() {
			return Top.Dom.selectById('mailWriteLayout') != null;
		}, isWritingToMe : function() {
			return Top.Dom.selectById('mailWriteToMeLayout') != null;
		}, getExtMail30Min : function(){
			
			var data={
					dto: {
						"USER_ID"	: userManager.getLoginUserId()
					}
			};
			mail.getAccountIdBySeq(data);
			mailData.interval30 = setInterval(function() {
				mail.getAccountIdBySeq(data);
			}, 1800000);
			
		},getAccountIdBySeq : function(data) {
			
			Top.Ajax.execute({
				type : "GET",
				url : _workspace.url + "Mail/Mail?action=GetAccountIdBySeq",
				data	 : JSON.stringify(data),
				dataType : "json",
				crossDomain: true,
				contentType: "application/json",
			    xhrFields: {withCredentials: true},
				success : function(ret,xhr,status) {
					mail.requestPOP3(ret.dto.ACCOUNT_ID);
				}
			});
		},
		
		requestPOP3 : function(list) {
			
			
			

    
			
			
			if(list.length > 1) {
				Top.Controller.get("mailMainLeftLogic").pop3requestCount = 0;
				for(var i=1; i<list.length; i++) {
					var data={
							dto: {
								"CMFLG"		: "Pop3Request",
								"USER_ID"	: userManager.getLoginUserId(),
								"ACCOUNT_ID": list[i].ACCOUNT_ID,
								"REQUEST_TYPE":"make"
							}
					};
					
					Top.Ajax.execute({
						
						type : "POST",
						url : _workspace.url + "Mail/Mail?action=POP3Request",
						data	 : JSON.stringify(data),
						crossDomain: true,
						contentType: "application/json",
					    xhrFields: {withCredentials: true},
						success : function(ret,xhr,status) {

						}
					});
				}
				
			}
			
		}, isMobile : function() {
			
			var media = window.matchMedia("screen and (max-device-width: 1024px)");
			return media.matches
			
		},  isReading : function() {
			return Top.Dom.selectById('mailReadLayout') != null;
		}, getEMailFromChip : function(chipText) {
			if(chipText.indexOf('<') != -1 && chipText.indexOf('>') != -1)
				return chipText.split('<')[1].split('>')[0];
			else
				return chipText;
		}, newMail : function() {
			//if(mailData.writeMode == 'full' || mail.isMobile())
			let sub = appManager.getSubApp();
			if(sub) Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/new' + "?sub=" + sub+'&q=' + new Date().getTime(), {eventType:'fold'});
			else Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/new?q=' + new Date().getTime(), {eventType:'close'});
			//else {
				//Top.Dom.selectById('mailWriteDialog').setProperties({'layout-top':'','layout-left':'','layout-bottom': '15px', 'layout-right':'70px'});
				//Top.Dom.selectById('mailWriteDialog').open();			
			//}
		}, newMailToMe : function() {
			//if(mailData.writeMode == 'full' || mail.isMobile())
			let sub = appManager.getSubApp();
			if(sub) Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/newtome' + "?sub=" + sub, {eventType:'fold'});
			else Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/newtome?' + new Date().getTime(), {eventType:'close'});
			//else {
				//Top.Dom.selectById('mailWriteToMeDialog').setProperties({'layout-top':'','layout-left':'','layout-bottom': '15px', 'layout-right':'70px'});
				//Top.Dom.selectById('mailWriteToMeDialog').open();			
			//}
		}, isExternal : function() {
			return mailData.curBox.WIDGET_ID.split('Text')[1].substr(0,1) != '0';
		}, noti_mail : function(msg){
			noti_mail(msg);  
		}, onDragEnter : function(e, data){
//			console.log(e);
//			console.log(data);
		},	onDragLeave : function (e, data){
//			console.log(e);
//			console.log(data);
		}, onDragStart : function (e, data){
			console.log(e);
			if(mailData.panelType == "2"){
				console.log(Top.Dom.selectById('mailTwoPanelLayout_Table').getItems()[e.toElement.rowIndex]);
				data.mailData = Top.Dom.selectById('mailTwoPanelLayout_Table').getItems()[e.toElement.rowIndex];
			}else{
				console.log(Top.Dom.selectById('mailThreePanelLayout_Table').getItems()[e.toElement.rowIndex]);
				data.mailData = Top.Dom.selectById('mailThreePanelLayout_Table').getItems()[e.toElement.rowIndex];
			}
			
			data.mailContainerType = $(e.toElement).attr('mail-containter-type');
			console.log(data);
		}, onDragOver : function (e, data){
//			console.log(e);
//			console.log(data);
		}, onDrop : function (e, data){
			console.log(e);
			//1. 메일리스트 폴더이동
			const target = e.target.closest('.dnd__container');
			var id = target.id;
			var containerType;
			containerType = $('#'+id+'').attr('mail-container-type');
			
			if(data.fromApp == "mail" && containerType == "folderList")
				this.dnd__moveMail(e, data);
				
			//2. 파일첨부
			if(!data.fromApp && e.dataTransfer["files"].length)
				this.dnd__attachFile(e, data);	
		},dnd__attachFile(e, data){
			var fileChooser= [{'file': e.dataTransfer["files"]}];
			Top.Controller.get('mailWriteLogic').attachTable(fileChooser);
		},dnd__moveMail(e, data){
			//folderID찾기
			var targetFolderName;
			if($('[folderInfo="'+e.toElement.id+'"]').length > 0){
				if($('[folderInfo="'+e.toElement.id+'"]')[0].textContent == undefined || $('[folderInfo="'+e.toElement.id+'"]')[0].textContent == ""){
					console.log($('[folderInfo="'+e.toElement.id+'"]')[0].innerText);
					targetFolderName = $('[folderInfo="'+e.toElement.id+'"]')[0].innerText;
				}
				else{
					console.log($('[folderInfo="'+e.toElement.id+'"]')[0].textContent);
					targetFolderName = $('[folderInfo="'+e.toElement.id+'"]')[0].textContent;
				}
			}else{
				if(e.toElement.innerText == ""){
				    if(e.toElement.parentElement.innerText == ""){
				        if(e.toElement.parentElement.parentElement.innerText == ""){
				            if(e.toElement.parentElement.parentElement.parentElement.innerText == ""){
				            	if(e.toElement.parentElement.parentElement.parentElement.parentElement.innerText == ""){
				            		if(e.toElement.parentElement.parentElement.parentElement.parentElement.parentElement.innerText == ""){
				            			
				            		}else
				            			targetFolderName = e.toElement.parentElement.parentElement.parentElement.parentElement.parentElement.innerText;
				            	}else
				            		targetFolderName = e.toElement.parentElement.parentElement.parentElement.parentElement.innerText;
				            }else
				            	targetFolderName = e.toElement.parentElement.parentElement.parentElement.innerText;
				        }else
				        	targetFolderName = e.toElement.parentElement.parentElement.innerText;
				    }else
				    	targetFolderName = e.toElement.parentElement.innerText;
				}else
					targetFolderName = e.toElement.innerText;
			}
			var acc_id = mailData.curBox.ACCOUNT_ID;
			
			var list = mailData.accountList;
			var folder_id;
			for(var i=0; i<list.length; i++){
			    if(list[i].ACCOUNT_ID == acc_id){
			        for(var j=0; j<list[i].FolderList.length; j++){
			            if(list[i].FolderList[j].DISPLAY_NAME == targetFolderName)
			              folder_id = list[i].FolderList[j].FOLDER_ID;
			        }
			    }
			}
			if(mailData.curBox.FOLDER_ID == folder_id)
				return;
			
			var input = {"MAIL_ID":"","ACCOUNT_ID":"","FOLDER_ID":"","CMFLG":""};
			if(data != null && data.data.mailData != null)
				input.MAIL_ID = [data.data.mailData.MAIL_ID];
			input.ACCOUNT_ID = mailData.curBox.ACCOUNT_ID;
			input.FOLDER_ID = folder_id;
			input.CMFLG = "FolderIdUpdate";
			input.DISPLAY_NAME = targetFolderName;
			if(mailData.panelType == "2"){
				Top.Controller.get('mailTwoPanelLogic').dragAndDrop__MoveFolder(input);
			}
			else{
				Top.Controller.get('mailThreePanelMainLogic').dragAndDrop__MoveFolder(input);
			}
		}

	}
})();

const mailData = {
		mailWrite_popup_flg : true,
		
		routing : false,
		routeInfo : [],
		
		panelType : '3',
		writeMode : 'full',
		
		curPage : 1,
		pageLength : 20,
		
		clickedMail : '',
		
		selectedBox : [],
		curBox : [],
		widgetList : [],

		boxId : [],
		countWidget : [],
		
		accountList : [],
		
		exitPath : '',
		
		interval30 : '',
};

function SendMail (sender, accountId, receiverArr, subject, content){
	
	var receiverAddr=[];
	for (var i = 0; i < receiverArr.length; i++){
		receiverAddr.push({ "TO_ADDR" : receiverArr[i]})
	}
	
	
	var data = {
			"dto":{
				"MAIL_FLAG" : "SND0002",
				"MailSendTo" : receiverAddr,
		        "MailSendCc" : [{ "TO_ADDR" : ""}],
		        "MailSendBcc" : [{ "TO_ADDR" : ""}],
		        "DOMailAttachment":[{
		            "ATTACHMENT_ID":"",
		            "MAIL_ID": "",
		            "STORAGE_PATH" : "", 
		            "USER_ID" : "",
		            "WS_ID" : "",
		            "EXTENSION" : "",
		            "FILE_NAME" : "",
		            "TDRIVE_CH_ID" : "", 
		            "FILE_ID" : "",
		            "FILE_SIZE" : ""
		        }],
		        "FOLDER_ID":"",
		        "SUBJECT" : subject,
		        "SENDER_NAME" : "",
		        "HAS_ATTACHMENT_YN_CODE" : "COM0002",
		        "SEEN_YN_CODE" : "",
		        "IMPORTANCE_YN_CODE" : "",
		        "PINNED_YN_CODE" : "",
		        "FILE_NAME" : "",
		        "WRITING_MAIL_ID" : "",
		        "SYNC_STATE" : "",
		        "SOURCE_MAIL_ID" : "",
		        "FILE_SIZE" : "",
		        "DISPOSITION_NOTI" : "",
		        "MESSAGE_ID" : "",
		        "X_TNEF" : "",
		        "SPAM_YN_CODE" : "",
		        "SENDER_ADDR" : sender,
		        "CONTENT" : content,
		        "CMFLG" : "SendMail",
		        "USER_ID" : userManager.getLoginUserId(),
		        "WS_ID" : workspaceManager.getMySpaceId(),
		        "MESSAGE_HEADER" : "",
		        "EXTRA_INFOS" : "",
		        "FIRST_SAW_TIME" : "", 
		        "SEND_IMPORTANCE_CODE" : "IMP0002",
		        "MAIL_ID" : "",
		        "ACCOUNT_ID" : accountId
		        
	    }
	}
	Top.Ajax.execute({
		
		type : "POST",
		url :_workspace.url + "Mail/Mail?action=Send",
		dataType : "json",
		data	 : JSON.stringify(data),
		contentType: "application/json; charset=utf-8",
		xhrFields: {withCredentials: true},
		crossDomain : true,
		success : function(ret,xhr,status) {
		},
		error : function(ret, xhr,status) {
		}
		
	});		

	
};


const MailDragDrop = {
    dragCounter : 0,
    
    onDragEnter : function(event) {
        MailDragDrop.dragCounter++;
        if(event.dataTransfer.types[0] != 'Files') return;
        var mailDropZoneOverlay = Top.Dom.selectById("mailDropZoneOverlay");
        if(mailDropZoneOverlay.getVisible() != "visible") {
            mailDropZoneOverlay.setVisible("visible");
        }
    },
    
    onDragLeave : function(event) {
        MailDragDrop.dragCounter--;
        if(MailDragDrop.dragCounter > 0) {
            return;
        }
        if(event.dataTransfer.types[0] != 'Files') return;
        var mailDropZoneOverlay = Top.Dom.selectById("mailDropZoneOverlay");
        if(mailDropZoneOverlay.getVisible() == "visible") {
        	mailDropZoneOverlay.setVisible("none");
        }
    },
    
    onDrop : function(event) {
        event.stopPropagation();
        event.preventDefault();
        MailDragDrop.dragCounter = 0;
        if(event.dataTransfer.types[0] != 'Files') return;
        var mailDropZoneOverlay = Top.Dom.selectById("mailDropZoneOverlay");
        if(mailDropZoneOverlay.getVisible() == "visible") {
        	mailDropZoneOverlay.setVisible("none");
        }
        
        event.dataTransfer.file = event.dataTransfer.files;
        var ctrl = (Top.Dom.selectById('mailWriteToMeLayout') == null) ? Top.Controller.get('mailWriteLogic') : Top.Controller.get('mailWriteToMeLogic')
        ctrl.attachTable(event.dataTransfer);
    }
}

TEESPACE_WEBSOCKET.addWebSocketHandler("CHN0002", mail.noti_mail);
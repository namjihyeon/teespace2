(function (exports) {
    const TeeSearch = function (root) {
        if (!root) return false;
        
        const setActive = function () {
            root.classList.add('tee-search-active');
        };

        const setInactive = function () {
            root.classList.remove('tee-search-active');
        };

        const inputEl = root.querySelector('input[type=text]');

        if (inputEl) {
            inputEl.addEventListener('focus', function (e) {
                setActive();
            });

            inputEl.addEventListener('focusout', function (e) {
                if (e.target.value.length === 0) {
                    setInactive();
                }
            });
        }
    };

    exports.InitializeTeeSearch  = TeeSearch;
})(this);

Node.prototype.find = function(selector) {
    if (/(^\s*|,\s*)>/.test(selector)) {
        if (!this.id) {
            this.id = 'ID_' + new Date().getTime();
            var removeId = true;
        }
        selector = selector.replace(/(^\s*|,\s*)>/g, '$1#' + this.id + ' >');
        var result = document.querySelectorAll(selector);
        if (removeId) {
            this.id = null;
        }
        return result;
    } else {
        return this.querySelectorAll(selector);
    }
};

(function (arr) {
    arr.forEach(function (item) {
      if (item.hasOwnProperty('append')) {
        return;
      }
      Object.defineProperty(item, 'append', {
        configurable: true,
        enumerable: true,
        writable: true,
        value: function append() {
          var argArr = Array.prototype.slice.call(arguments),
            docFrag = document.createDocumentFragment();
          
          argArr.forEach(function (argItem) {
            var isNode = argItem instanceof Node;
            docFrag.appendChild(isNode ? argItem : document.createTextNode(String(argItem)));
          });
          
          this.appendChild(docFrag);
        }
      });
    });
})([Element.prototype, Document.prototype, DocumentFragment.prototype]);

(function (arr) {
    arr.forEach(function (item) {
      if (item.hasOwnProperty('prepend')) {
        return;
      }
      Object.defineProperty(item, 'prepend', {
        configurable: true,
        enumerable: true,
        writable: true,
        value: function prepend() {
          var argArr = Array.prototype.slice.call(arguments),
            docFrag = document.createDocumentFragment();
          
          argArr.forEach(function (argItem) {
            var isNode = argItem instanceof Node;
            docFrag.appendChild(isNode ? argItem : document.createTextNode(String(argItem)));
          });
          
          this.insertBefore(docFrag, this.firstChild);
        }
      });
    });
})([Element.prototype, Document.prototype, DocumentFragment.prototype]);

(function (arr) {
    arr.forEach(function (item) {
        if (item.hasOwnProperty('remove')) {
            return;
        }
        Object.defineProperty(item, 'remove', {
            configurable: true,
            enumerable: true,
            writable: true,
            value: function remove() {
                if (this.parentNode !== null)
                this.parentNode.removeChild(this);
            }
        });
    });
})([Element.prototype, CharacterData.prototype, DocumentType.prototype]);

if (!String.prototype.trim) {
    String.prototype.trim = function () {
        return this.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '');
    };
}

(function (exports) {
	let htmlToElement = function (html) {
	    var template = document.createElement('template');
	    html = html.trim();
        template.innerHTML = html;
	    return template.content.firstChild;
    }
    
    let remToPx = function (rem) {
        return screen.width / 13.66 / 100 * rem * 16;
    }

    exports.htmlToElement = htmlToElement;
    exports.remToPx = remToPx;
})(this);

(function (exports) {
    const template = htmlToElement(`
        <div class="tee-tooltip">
        </div>
    `);
    
    function open(text, opts = {}) {
        template.textContent = text;
        template.style.left = opts.x + 'px';
        template.style.top = opts.y + 'px';
        template.style.display = 'block';
    }

    function close() {
        template.style.display = 'none';
    }

    template.addEventListener('mouseout', function (e) {
        e.preventDefault();
        close();
    });

    window.addEventListener('load', function (e) {
        document.body.appendChild(template);
    });

    exports.TeeTooltip = {
        open,
        close,
    };
})(this);

(function (exports) {

    const OrgAPI = {
        CurrentUserInfo: function (currentUserId) {
            return axios.post(_workspace.url + 'SpaceRoom/OrgUserDept?action=Get', {
                dto: {
                    USER_ID: currentUserId, 
                }
            });
        },
    };

    const template = htmlToElement(`
        <div class="org-tree-container">
            <div class="tree-container open" data-id="all">
                <div class="tree-item">
                    <top-icon class="icon-arrow_filled_down tree-handle"></top-icon>
                    <span class="name">All</span>
                </div>
                <div class="tree-children">
                </div>
            </div>
        </div>
    `);

    const overlay = htmlToElement(`<div class="org-tree-overlay"></div>`);

    let onItemClicked = null,
        onClosed      = null;
    let _config = {
        defaultExpand: true,
    };

    function drawChildren(parentNode, org) {
        const children = htmlToElement(`
            <div class="tree-container">
                <div class="tree-item">
                    <top-icon class="icon-arrow_filled_right tree-handle"></top-icon>
                    <span class="name"></span>
                </div>
                <div class="tree-children">
                </div>
            </div>
        `);

        children.querySelector('span.name').textContent = org.ORG_NAME;
        children.querySelector('.tree-handle').style.marginLeft = 0.75 * (Number(org.ORG_LEVEL) + 1) + 'rem';
        children.setAttribute('data-id', org.COMPANY_CODE + '.' + org.DEPARTMENT_CODE);
        children.data = {
            companyCode: org.COMPANY_CODE,
            departmentCode: org.DEPARTMENT_CODE,
            orgName: org.ORG_NAME,
        };

        children.classList.add('close');

        if (org.CHILDREN_ORG !== null) {
            org.CHILDREN_ORG.orgList.forEach(childOrg => {
                if (childOrg.IS_PUBLIC === 'ADM0011') {
                    drawChildren(children, childOrg);
                }
            });
        } else {
            children.querySelector('div.tree-item').classList.add('last');
        }

        parentNode.querySelector('div.tree-children').appendChild(children);
    }

    function expandAll() {
        template.find('div.tree-container.close').forEach(node => {
            node.classList.remove('close');
            node.classList.add('open');

            const handle = node.find(' > .tree-item top-icon');
            if (handle.length > 0) {
                handle[0].classList.remove('icon-arrow_filled_right');
                handle[0].classList.add('icon-arrow_filled_down');
            }
        });
    }

    function expandTo(id) {
        const node = template.querySelector('div[data-id="'+id+'"]');
        let parent = node;

        const item = node.find('> .tree-item')[0];

        while(parent && parent.getAttribute('data-id') !== 'all') {
            parent = parent.parentNode.closest('div.tree-container'); 
            parent.classList.remove('close');
            parent.classList.add('open');
            const handle = parent.find('> .tree-item .tree-handle');
            if (handle.length > 0) {
                handle[0].classList.remove('icon-arrow_filled_right');
                handle[0].classList.add('icon-arrow_filled_down');
            }
        }

        item.classList.add('selected');

        const top = item.getBoundingClientRect().top - template.getBoundingClientRect().height;
        template.scrollTop = top;
    }

    function getOrgNameTree(node) {
        let nameTree = [];

        let parent = node;

        if (parent.getAttribute('data-id') === 'all') {
            return ['All'];
        }

        while(parent && parent.getAttribute('data-id') !== 'all') {
            nameTree.push(parent.data.orgName);
            parent = parent.parentNode.closest('div.tree-container'); 
        }

        return nameTree.reverse();
    }

    function close() {
        document.querySelector('body').removeChild(overlay);
        document.querySelector('body').removeChild(template);
    }

    function eventDelegator(e) {
        const target = e.target;
        
        if (target && target.classList.contains('tree-handle')) {
            const container = target.parentNode.parentNode;

            if (container.classList.contains('close')) {
                container.classList.remove('close');
                container.classList.add('open');
                target.classList.remove('icon-arrow_filled_right');
                target.classList.add('icon-arrow_filled_down');
            } else {
                container.classList.remove('open');
                container.classList.add('close');
                target.classList.add('icon-arrow_filled_right');
                target.classList.remove('icon-arrow_filled_down');
            }
        } else if (target && (target.classList.contains('tree-item') || target.classList.contains('name'))) {
            const node = target.classList.contains('name') ? target.parentNode.parentNode : target.parentNode;
            const orgNameTree = getOrgNameTree(node);
            if (onItemClicked) {
                onItemClicked(node.data, orgNameTree);
            }

            close();
        }
    };

    function onOverlayClicked(e) {
        if (onClosed) {
            onClosed(null);
        }

        close();
    }

    function initialize() {
        const self = this;
        template.find(' .tree-container > div.tree-children')[0].innerHTML = '';
        template.removeEventListener('click', eventDelegator);

        overlay.removeEventListener('click', onOverlayClicked);
        overlay.addEventListener('click', onOverlayClicked);

        template.querySelector('.tree-container').data = {
            companyCode: 'ALL',
            departmentCode: '',
            orgName: 'All',
        };

        const domain = JSON.parse(sessionStorage.getItem('userInfo')).USER_LOGIN_ID.split('@')[1];

        Promise.all([
            OrgAPI.CurrentUserInfo(userManager.getLoginUserId()),
            OrgManager.get(domain, 'ADM0021')
        ])
        .then(res => {
            res[1].forEach(org => {
                if (org.IS_PUBLIC === 'ADM0011') {
                    drawChildren(template, org);
                }
            });

            if (!_config.selectedId) {
                expandTo(res[0].data.dto.COMPANY_CODE + '.' + res[0].data.dto.DEPARTMENT_CODE);
            } else {
                expandTo(_config.selectedId);
            }
        })
        .catch(err => {})
        .finally(() => {
            template.addEventListener('click', eventDelegator);
        });
    }

    exports.OrgTreeSelectView = {
        open: function (
            rect = {x: '1rem', y: '1rem', width: '23.63rem', height: '18.44rem'},
            opts = {
            onItemClicked: null,
            onClosed: null,
            selectedId: null,
        }) {
            onItemClicked = opts.onItemClicked;
            onClosed = opts.onClosed;
            _config = opts;

            initialize();
            template.style.left = rect.x;
            template.style.top = rect.y;
            template.style.width = rect.width;
            template.style.height = rect.height;

            document.body.appendChild(overlay);
            document.body.appendChild(template);
        },
        close: close,
        expandAll: expandAll,
        expandTo: expandTo,
    };

})(this);

(function (exports) {
    let overlayTemplate = null;
    let template = null;
    let isOpend = false;

    function initialize() {
        overlayTemplate = htmlToElement(`
            <div class="top-dialog-overlay" style="z-index:65000" />
        `);

        template = htmlToElement(`
            <div class="tee-custom-dialog" style="z-index:65001" tabindex="-1">
                <top-icon class=""></top-icon>
                <h3></h3>
                <p></p>
                <div class="button-container">
                    
                </div>
            </div>
        `);
    }

	function open(_opts = {}, callback) {
        if (isOpend) return; 

        let opts = {
            icon: 'icon-chart_error', 
            title: '', 
            content: '',
            buttons: [],
            cancelButtonText: '취소',
            onCancelButtonClicked: null,
        };

        Object.assign(opts, _opts);

        isOpend = true;
        initialize();

		document.querySelector('body').appendChild(overlayTemplate);
		
		template.querySelector('top-icon').className = opts.icon;
		
        template.querySelector('h3').innerHTML = opts.title;
        
        if (opts.content === undefined) {
            template.querySelector('p').textContent = '';
        } else {
            template.querySelector('p').innerHTML = opts.content;
        }
		
        document.querySelector('body').appendChild(template);
        
        template.querySelector('.button-container').innerHTML = '';
        if (opts.buttons.length > 0) {
            opts.buttons.forEach(v => {
                const button = htmlToElement(`<button class="solid">${v.text}</button>`);
                button.addEventListener('click', v.onClicked);
                template.querySelector('.button-container').appendChild(button);    
            });   
        }

        const closeButton = htmlToElement(`<button class="outlined">${opts.cancelButtonText}</button>`);
        template.querySelector('.button-container').appendChild(closeButton);

        if (opts.onCancelButtonClicked && typeof opts.onCancelButtonClicked === 'function') {
            closeButton.addEventListener('click', opts.onCancelButtonClicked);
        } else {
            closeButton.addEventListener('click', function () {
                close();
            });
        }

        setTimeout(() => {
            template.focus();
        }, 0);
		
		if(typeof callback === 'function') callback();
	}
	
	function close() {
        isOpend = false;
		document.querySelector('body').removeChild(overlayTemplate);
		document.querySelector('body').removeChild(template);
	}
	
	let TeeAlarm = {
		open: open,
		close: close,
	}
	
	exports.TeeAlarm = TeeAlarm;
	
})(this);

(function (exports) {	
    let timeoutId = null;

	const template = htmlToElement(`
        <div class="tee-toast-popup" style="z-index:65005">
            <div class="tee-toast-icon">
                <top-icon class="icon-check"></top-icon>
            </div>
			<span class="tee-toast-text"></span>
            <div class="cases"></div>
            <top-icon class="icon-close close"></top-icon>
		</div>
    `);
    //<top-icon class="icon-chart_error"></top-icon>
	
	template.querySelector('.close').addEventListener('click', function () {
		close();
	});
	
	function open(opts = {text: ''}, timedout = 2500) {	
        const defaultOpts = {size: 'normal', cases: []};
        Object.assign(defaultOpts, opts);
        Object.assign(opts, defaultOpts);

        const self = this;

        template.className = `tee-toast-popup ${opts.size}`;
        
        template.querySelector('.tee-toast-text').textContent = opts.text;
        template.querySelector('.cases').innerHTML = '';
        
        if (opts.cases.length > 0) {
            const caseContainer = template.querySelector('.cases');
            opts.cases.forEach( (data, i) => {
                const caseTemplate = htmlToElement(`
                    <a class="case">${data.text}</a>
                `);

                if (data.onClicked) {
                    caseTemplate.addEventListener('click', data.onClicked);
                }

                caseContainer.appendChild(caseTemplate);

                if (i < opts.cases.length - 1) {
                    const divider = htmlToElement(`<div class="divider"></div>`);
                    caseContainer.appendChild(divider);
                }
            });
        }
        
        document.querySelector('body').appendChild(template);
        
        if (timeoutId) {
            window.clearTimeout(timeoutId);
        }

        timeoutId = setTimeout(() => {
            self.close();
            timedout = null;
        }, timedout);
	}
	
	function close() {
        if (timeoutId) {
            window.clearTimeout(timeoutId);
            timedout = null;
        }

		document.querySelector('body').removeChild(template);
	}
	
	let TeeToast = {
		open: open,
		close: close,
	}
	
	exports.TeeToast = TeeToast;
	
})(this);

(function (exports) {
	/**
	 * ContextMenu
	 */
	let ContextMenu = function (config = {x: 0, y: 0, menuList: [], mode: 'normal'}) {
        const self = this;

        this.model = {
            selectedIdx: 0,
            listElms: [],
        };

		let container = document.createElement('div');
		let listContainer = document.createElement('ul');
		
		listContainer.style.listStyleType = 'none';
		listContainer.style.margin = '0';
        listContainer.style.padding = '0';
		
		container.appendChild(listContainer);
		container.style.position = 'fixed';
		container.style.left = config.x;
		container.style.top = config.y;
		container.style.minWidth = '118px';
		container.style.background = '#fff';
		container.style.padding = '0.1rem 0rem 0.1rem 0rem';
		container.style.border = '1px solid #cdcdcf';
		container.style.display = 'none';
        container.style.fontSize = '0.81rem';
        container.style.zIndex = '10000';
        container.style.borderRadius = '4px';
		
		container.addEventListener('mouseleave', function (e) {
			container.style.display = 'none';
		});
		
		return {
			containerElem: container,
			data: null,
			show: function () {
				container.style.display = 'block';
			},
			show: function (x, y) {
				container.style.left = x + 'px';
				container.style.top = y + 'px';
				container.style.display = 'block';
			},
			hide: function () {
				container.style.display = 'none';
			},
			addItem: function (name, onClick) {
				let li = document.createElement('li');
                listContainer.append(li);
                li.style.width = '5.56rem';
                li.style.height = '1.8rem';
                li.style.cursor = 'pointer';

                if (config.mode === 'select') {
                    let icon = htmlToElement('<top-icon class="icon-work_check check-icon"></top-icon>');
                    let nameSpan = htmlToElement(`<span>${name}</span>`);

                    icon.style.fontSize = '0.7rem';
                    icon.style.color = '#75757f';
                    icon.style.margin = '0 0.31rem 0 0.56rem';
                    icon.style.visibility = 'hidden';

                    li.appendChild(icon);
                    li.appendChild(nameSpan);

                    li.style.display = 'flex';
                    li.style.alignItems = 'center';
                    li.style.padding = '0';
                    li.style.width = '6.88rem';

                    li.idx = self.model.listElms.length;
                    if (li.idx === 0) {
                        icon.style.visibility = 'visible';
                    }

                    self.model.listElms.push(li);
                } else {
                    li.textContent = name;
                    li.style.padding = '0rem 0rem 0rem 0.69rem';
                    li.style.display = 'flex';
                    li.style.flexDirection = 'column';
                    li.style.justifyContent =  'center';
                    li.style.cursor = 'pointer';
                }
            		
				li.addEventListener('mouseenter', function (e) {
					e.target.style.background = '#DCDDFF';
					e.target.style.borderRadius = '18px 18px 18px 18px / 18px 18px 18px 18px'
				}); 
				
				li.addEventListener('mouseleave', function (e) {
					e.target.style.background = 'transparent';
				});
                
                if (config.mode === 'select') {
                    li.addEventListener('click', function (e) {
                        const node = e.target.closest('li');
                        if (node) {
                            self.model.selectedIdx = node.idx;

                            self.model.listElms.forEach( (el, i) => {
                                if (self.model.selectedIdx !== i) {
                                    el.querySelector('.check-icon').style.visibility = 'hidden';
                                } else {
                                    el.querySelector('.check-icon').style.visibility = 'visible';
                                }
                            });
                            onClick(e);
                        }
                    }, false);
                } else {
                    li.addEventListener('click', onClick, false);
                }
			},
			updateItem: function (index, name) {
				let lis = listContainer.querySelectorAll('li');
				if (index < lis.length) {
					lis[index].textContent = name;
				}
            },
            updateItemData: function (index, data) {
                let lis = listContainer.querySelectorAll('li');
				if (index < lis.length) {
					lis[index].data = data;
				}
            },
            getItemData: function (index) {
                let lis = listContainer.querySelectorAll('li');
				if (index < lis.length) {
					return lis[index].data;
				}
            },
			removeItem: function (index) {
				let lis = listContainer.querySelectorAll('li');
				if (index < lis.length) {
					listContainer.removeChild(lis[index]);
				}
			}
		}
	};
	
	exports.ContextMenu = ContextMenu;
})(this);

/**
 * TokenField
 */
(function( factory){

    if(typeof module == 'object' && typeof module.exports == 'object' )
        module.exports = factory();
    else if( typeof window == 'object')
        window.TagsInput = factory();
    else
        console.error('To use this library you need to either use browser or node.js [require()]');

})(function(){
    "use strict"

    var initialized = false

    // Plugin Constructor
    var TagsInput = function(opts){
        this.options = Object.assign(TagsInput.defaults , opts);
        this.init();
    }

    // Initialize the plugin
    TagsInput.prototype.init = function(opts){
        this.options = opts ? Object.assign(this.options, opts) : this.options;

        if(initialized)
            this.destroy();
            
        if(!(this.orignal_input = this.options.selector) ){
            console.error("tags-input couldn't find an element with the specified ID");
            return this;
        }

        this.arr = [];
        this.wrapper = document.createElement('div');
        this.input = document.createElement('input');
        
        if (this.options.placeholder) {
        	this.input.setAttribute('placeholder', this.options.placeholder);
        }
        
        init(this);
        initEvents(this);

        initialized =  true;
        return this;
    }

    // Add Tags
    TagsInput.prototype.addTag = function(string){

        if(this.anyErrors(string))
            return ;

        this.arr.push(string);
        var tagInput = this;

        var tag = document.createElement('span');
        tag.className = this.options.tagClass;
        tag.innerText = string;

        var errorIcon = document.createElement('i');
        errorIcon.classList.add('icon-work_notice');
        errorIcon.style.color = '#ff6060';
        errorIcon.style.fontSize = '13px';
        errorIcon.style.display = 'inline-block';
        errorIcon.style.margin = '0 2px 3px';
        
        var closeIcon = document.createElement('a');
        closeIcon.innerHTML = '&times;';
        
        // delete the tag when icon is clicked
        closeIcon.addEventListener('click' , function(e){
            e.preventDefault();
            var tag = this.parentNode;

            for(var i =0 ;i < tagInput.wrapper.childNodes.length ; i++){
                if(tagInput.wrapper.childNodes[i] == tag)
                    tagInput.deleteTag(tag , i);
            }
        })

        if (this.options.emailValidator) {
        	let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        	if (!re.test(string)) {
        		tag.appendChild(errorIcon);
        	}
        }
        
        tag.appendChild(closeIcon);
        this.wrapper.insertBefore(tag , this.input);
        this.orignal_input.value = this.arr.join(',');

        if (this.options.onAddedTag) {
        	this.options.onAddedTag();
        }
        
        return this;
    }

    // Delete Tags
    TagsInput.prototype.deleteTag = function(tag , i){
        tag.remove();
        this.arr.splice( i , 1);
        this.orignal_input.value =  this.arr.join(',');
        
        if (this.options.onDeletedTag) {
        	this.options.onDeletedTag();
        }
        
        return this;
    }

    // Make sure input string have no error with the plugin
    TagsInput.prototype.anyErrors = function(string){
        if( this.options.max != null && this.arr.length >= this.options.max ){
            console.log('max tags limit reached');
            return true;
        }
        
        if(!this.options.duplicate && this.arr.indexOf(string) != -1 ){
            console.log('duplicate found " '+string+' " ')
            return true;
        }

        return false;
    }

    // Add tags programmatically 
    TagsInput.prototype.addData = function(array){
        var plugin = this;
        
        array.forEach(function(string){
            plugin.addTag(string);
        })
        return this;
    }

    // Get the Input String
    TagsInput.prototype.getInputString = function(){
        return this.arr.join(',');
    }
    
    TagsInput.prototype.getInputArray = function(){
        return this.arr;
    }


    // destroy the plugin
    TagsInput.prototype.destroy = function(){
    	if (this.orignal_input) {
    		this.orignal_input.removeAttribute('hidden');

            delete this.orignal_input;
            var self = this;
            
            Object.keys(this).forEach(function(key){
                if(self[key] instanceof HTMLElement)
                    self[key].remove();
                
                if(key != 'options')
                    delete self[key];
            });
    	}
        initialized = false;
    }

    // Private function to initialize the tag input plugin
    function init(tags){
        tags.wrapper.append(tags.input);
        tags.wrapper.classList.add(tags.options.wrapperClass);
        tags.orignal_input.setAttribute('hidden' , 'true');
        tags.orignal_input.parentNode.insertBefore(tags.wrapper , tags.orignal_input);
    }

    // initialize the Events
    function initEvents(tags){
        tags.wrapper.addEventListener('click' ,function(){
            tags.input.focus();           
        });
        

        tags.input.addEventListener('keydown' , function(e){

            var str = tags.input.value.trim(); 

            if( !!(~[9 , 13 , 188].indexOf( e.keyCode ))  )
            {
                tags.input.value = "";
                if(str != "")
                    tags.addTag(str);
            }

        });
    }


    // Set All the Default Values
    TagsInput.defaults = {
        selector : '',
        wrapperClass : 'tags-input-wrapper',
        tagClass : 'tag',
        max : null,
        duplicate: false
    }

    return TagsInput;
});

(function (exports) {
    'use strict'

    // Default configuration.
    const defaultConfig = {
    width: '100%',
    height: '100%'
    }

    // Check for valid number.
    const isNumber = input => Number(input) === Number(input)

    // Add a class to an element.
    const addClass = 'classList' in document.documentElement
    ? (element, className) => {
        element.classList.add(className)
    }
    : (element, className) => {
        const oldClass = element.getAttribute('class') || ''
        element.setAttribute('class', `${oldClass} ${className}`)
    }

    /**
     * Creates a HyperList instance that virtually scrolls very large amounts of
     * data effortlessly.
     */
    class HyperList {
        static create (element, userProvidedConfig) {
            return new HyperList(element, userProvidedConfig)
        }

        /**
         * Merge given css style on an element
         * @param {DOMElement} element
         * @param {Object} style
         */
        static mergeStyle (element, style) {
            for (let i in style) {
            if (element.style[i] !== style[i]) {
                element.style[i] = style[i]
            }
            }
        }

        static getMaxBrowserHeight () {
            // Create two elements, the wrapper is `1px` tall and is transparent and
            // positioned at the top of the page. Inside that is an element that gets
            // set to 1 billion pixels. Then reads the max height the browser can
            // calculate.
            const wrapper = document.createElement('div')
            const fixture = document.createElement('div')

            // As said above, these values get set to put the fixture elements into the
            // right visual state.
            HyperList.mergeStyle(wrapper, {position: 'absolute', height: '1px', opacity: 0})
            HyperList.mergeStyle(fixture, {height: '1e7px'})

            // Add the fixture into the wrapper element.
            wrapper.appendChild(fixture)

            // Apply to the page, the values won't kick in unless this is attached.
            document.body.appendChild(wrapper)

            // Get the maximum element height in pixels.
            const maxElementHeight = fixture.offsetHeight

            // Remove the element immediately after reading the value.
            document.body.removeChild(wrapper)

            return maxElementHeight
        }

        constructor (element, userProvidedConfig) {
            this._config = {}
            this._lastRepaint = null
            this._maxElementHeight = HyperList.getMaxBrowserHeight()

            this.refresh(element, userProvidedConfig)

            const config = this._config

            // Create internal render loop.
            const render = () => {
            const scrollTop = this._getScrollPosition()
            const lastRepaint = this._lastRepaint

            this._renderAnimationFrame = window.requestAnimationFrame(render)

            if (scrollTop === lastRepaint) {
                return
            }

            const diff = lastRepaint ? scrollTop - lastRepaint : 0
            if (!lastRepaint || diff < 0 || diff > this._averageHeight) {
                let rendered = this._renderChunk()

                this._lastRepaint = scrollTop

                if (rendered !== false && typeof config.afterRender === 'function') {
                config.afterRender()
                }
            }
            }

            render()
        }

        destroy () {
            window.cancelAnimationFrame(this._renderAnimationFrame)
        }

        refresh (element, userProvidedConfig) {
            Object.assign(this._config, defaultConfig, userProvidedConfig)

            if (!element || element.nodeType !== 1) {
            throw new Error('HyperList requires a valid DOM Node container')
            }

            this._element = element

            const config = this._config

            const scroller = this._scroller || config.scroller ||
            document.createElement(config.scrollerTagName || 'tr')

            // Default configuration option `useFragment` to `true`.
            if (typeof config.useFragment !== 'boolean') {
            this._config.useFragment = true
            }

            if (!config.generate) {
            throw new Error('Missing required `generate` function')
            }

            if (!isNumber(config.total)) {
            throw new Error('Invalid required `total` value, expected number')
            }

            if (!Array.isArray(config.itemHeight) && !isNumber(config.itemHeight)) {
            throw new Error(`
                Invalid required \`itemHeight\` value, expected number or array
            `.trim())
            } else if (isNumber(config.itemHeight)) {
            this._itemHeights = Array(config.total).fill(config.itemHeight)
            } else {
            this._itemHeights = config.itemHeight
            }

            // Width and height should be coerced to string representations. Either in
            // `%` or `px`.
            Object.keys(defaultConfig).filter(prop => prop in config).forEach(prop => {
            const value = config[prop]
            const isValueNumber = isNumber(value)

            if (value && typeof value !== 'string' && typeof value !== 'number') {
                let msg = `Invalid optional \`${prop}\`, expected string or number`
                throw new Error(msg)
            } else if (isValueNumber) {
                config[prop] = `${value}px`
            }
            })

            const isHoriz = Boolean(config.horizontal)
            const value = config[isHoriz ? 'width' : 'height']

            if (value) {
            const isValueNumber = isNumber(value)
            const isValuePercent = isValueNumber ? false : value.slice(-1) === '%'
            // Compute the containerHeight as number
            const numberValue = isValueNumber ? value : parseInt(value.replace(/px|%/, ''), 10)
            const innerSize = window[isHoriz ? 'innerWidth' : 'innerHeight']

            if (isValuePercent) {
                this._containerSize = (innerSize * numberValue) / 100
            } else {
                this._containerSize = isNumber(value) ? value : numberValue
            }
            }

            const scrollContainer = config.scrollContainer
            const scrollerHeight = config.itemHeight * config.total
            const maxElementHeight = this._maxElementHeight

            if (scrollerHeight > maxElementHeight) {
            console.warn([
                'HyperList: The maximum element height', maxElementHeight + 'px has',
                'been exceeded; please reduce your item height.'
            ].join(' '))
            }

            // Decorate the container element with styles that will match
            // the user supplied configuration.
            const elementStyle = {
                width: `${config.width}`,
                height: scrollContainer ? `${scrollerHeight}px` : `${config.height}`,
                overflow: scrollContainer ? 'none' : 'auto',
                position: 'relative'
            }

            if (config.width !== null) {
                elementStyle.width = `${config.width}`;
            }

            HyperList.mergeStyle(element, elementStyle)

            if (scrollContainer) {
            HyperList.mergeStyle(config.scrollContainer, {overflow: 'auto'})
            }

            const scrollerStyle = {
            opacity: '0',
            position: 'absolute',
            [isHoriz ? 'height' : 'width']: '1px',
            [isHoriz ? 'width' : 'height']: `${scrollerHeight}px`
            }

            HyperList.mergeStyle(scroller, scrollerStyle)

            // Only append the scroller element once.
            if (!this._scroller) {
            element.appendChild(scroller)
            }

            const padding = this._computeScrollPadding()
            this._scrollPaddingBottom = padding.bottom
            this._scrollPaddingTop = padding.top

            // Set the scroller instance.
            this._scroller = scroller
            this._scrollHeight = this._computeScrollHeight()

            // Reuse the item positions if refreshed, otherwise set to empty array.
            if (!this._itemPositions || this._itemPositions.length === 0 || this._itemPositions[0] === undefined) {
                this._itemPositions = Array(config.total).fill(0)
            } else {
                this._itemPositions = this._itemPositions;
            }

            // Each index in the array should represent the position in the DOM.
            this._computePositions(0)

            // Render after refreshing. Force render if we're calling refresh manually.
            this._renderChunk(this._lastRepaint !== null)

            if (typeof config.afterRender === 'function') {
            config.afterRender()
            }
        }

        _getRow (i) {
            const config = this._config
            let item = config.generate(i)
            let height = item.height

            if (height !== undefined && isNumber(height)) {
            item = item.element

            // The height isn't the same as predicted, compute positions again
            if (height !== this._itemHeights[i]) {
                this._itemHeights[i] = height
                this._computePositions(i)
                this._scrollHeight = this._computeScrollHeight(i)
            }
            } else {
            height = this._itemHeights[i]
            }

            if (!item || item.nodeType !== 1) {
            throw new Error(`Generator did not return a DOM Node for index: ${i}`)
            }

            addClass(item, config.rowClassName || 'vrow')

            const top = this._itemPositions[i] + this._scrollPaddingTop
            const paddingTop = config.paddingTop ? remToPx(config.paddingTop) : 0;

            HyperList.mergeStyle(item, {
                position: 'absolute',
                [config.horizontal ? 'left' : 'top']: `${top + paddingTop}px`
            })

            return item
        }

        _getScrollPosition () {
            const config = this._config

            if (typeof config.overrideScrollPosition === 'function') {
            return config.overrideScrollPosition()
            }

            return this._element[config.horizontal ? 'scrollLeft' : 'scrollTop']
        }

        _renderChunk (force) {
            const config = this._config
            const element = this._element
            const scrollTop = this._getScrollPosition()
            const total = config.total

            let from = config.reverse ? this._getReverseFrom(scrollTop) : this._getFrom(scrollTop) - 1

            if (from < 0 || from - this._screenItemsLen < 0) {
            from = 0
            }

            if (!force && this._lastFrom === from) {
            return false
            }

            this._lastFrom = from

            let to = from + this._cachedItemsLen

            if (to > total || to + this._cachedItemsLen > total) {
            to = total
            }

            // Append all the new rows in a document fragment that we will later append
            // to the parent node
            const fragment = config.useFragment ? document.createDocumentFragment() : [
            // Sometimes you'll pass fake elements to this tool and Fragments require
            // real elements.
            ]

            // The element that forces the container to scroll.
            const scroller = this._scroller

            // Keep the scroller in the list of children.
            fragment[config.useFragment ? 'appendChild' : 'push'](scroller)

            for (let i = from; i < to; i++) {
            let row = this._getRow(i)

            fragment[config.useFragment ? 'appendChild' : 'push'](row)
            }

            if (config.applyPatch) {
            return config.applyPatch(element, fragment)
            }

            element.innerHTML = ''
            element.appendChild(fragment)
        }

        _computePositions (from = 1) {
            const config = this._config
            const total = config.total
            const reverse = config.reverse

            if (from < 1 && !reverse) {
            from = 1
            }

            for (let i = from; i < total; i++) {
            if (reverse) {
                if (i === 0) {
                this._itemPositions[0] = this._scrollHeight - this._itemHeights[0]
                } else {
                this._itemPositions[i] = this._itemPositions[i - 1] - this._itemHeights[i]
                }
            } else {
                this._itemPositions[i] = this._itemHeights[i - 1] + this._itemPositions[i - 1]
            }
            }
        }

        _computeScrollHeight () {
            const config = this._config
            const isHoriz = Boolean(config.horizontal)
            const total = config.total
            const scrollHeight = this._itemHeights.reduce((a, b) => a + b, 0) + this._scrollPaddingBottom + this._scrollPaddingTop

            HyperList.mergeStyle(this._scroller, {
            opacity: 0,
            position: 'absolute',
            top: '0px',
            [isHoriz ? 'height' : 'width']: '1px',
            [isHoriz ? 'width' : 'height']: `${scrollHeight}px`
            })

            // Calculate the height median
            const sortedItemHeights = this._itemHeights.slice(0).sort((a, b) => a - b)
            const middle = Math.floor(total / 2)
            const averageHeight = total % 2 === 0 ? (sortedItemHeights[middle] + sortedItemHeights[middle - 1]) / 2 : sortedItemHeights[middle]

            const clientProp = isHoriz ? 'clientWidth' : 'clientHeight'
            const element = config.scrollContainer ? config.scrollContainer : this._element
            const containerHeight = element[clientProp] ? element[clientProp] : this._containerSize
            this._screenItemsLen = Math.ceil(containerHeight / averageHeight)
            this._containerSize = containerHeight

            // Cache 3 times the number of items that fit in the container viewport.
            this._cachedItemsLen = Math.max(this._cachedItemsLen || 0, this._screenItemsLen * 3)
            this._averageHeight = averageHeight

            if (config.reverse) {
            window.requestAnimationFrame(() => {
                if (isHoriz) {
                this._element.scrollLeft = scrollHeight
                } else {
                this._element.scrollTop = scrollHeight
                }
            })
            }

            return scrollHeight
        }

        _computeScrollPadding () {
            const config = this._config
            const isHoriz = Boolean(config.horizontal)
            const isReverse = config.reverse
            const styles = window.getComputedStyle(this._element)

            const padding = location => {
            const cssValue = styles.getPropertyValue(`padding-${location}`)
            return parseInt(cssValue, 10) || 0
            }

            if (isHoriz && isReverse) {
            return {
                bottom: padding('left'),
                top: padding('right')
            }
            } else if (isHoriz) {
            return {
                bottom: padding('right'),
                top: padding('left')
            }
            } else if (isReverse) {
            return {
                bottom: padding('top'),
                top: padding('bottom')
            }
            } else {
            return {
                bottom: padding('bottom'),
                top: padding('top')
            }
            }
        }

        _getFrom (scrollTop) {
            let i = 0

            while (this._itemPositions[i] < scrollTop) {
            i++
            }

            return i
        }

        _getReverseFrom (scrollTop) {
            let i = this._config.total - 1

            while (i > 0 && this._itemPositions[i] < scrollTop + this._containerSize) {
            i--
            }

            return i
        }
    }

exports.HyperList = HyperList;

})(this);
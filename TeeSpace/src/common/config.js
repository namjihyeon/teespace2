/**
 * 설정 파일 로드
 * */
let tsVersion;
let _workspace;
let _allowSameUserWebSocket = true;
let mqttInfo = {};
let _webOffice;
let _mailDomain = ""; //임시 도메인 - 시스템 콘텍스트

// config file
(function server() {

	$.ajaxSetup({
        async: false
    });
    $.getJSON("./external/config/server.json", function(data) {
        _workspace = data.workspace;
        tsVersion = data.version? data.version : getTimeStamp();

        if (!_workspace["url"]) {
            if (_workspace["ip"] === "" && _workspace["port"] === "") {
                _workspace["url"] = "/CMS/";
            } else {
                _workspace["url"] = "http://" + _workspace.ip + ":" + _workspace.port + "/CMS/";
            }
        }
        if (!_workspace["fileUrl"]) {
            _workspace["fileUrl"] = _workspace.url + "File/";
        }
        if (!_workspace["messengerURL"]) {
            _workspace["messengerURL"] = _workspace.url + "Messenger/";
        }
        if (!_workspace["conferenceURL"] || _workspace["conferenceURL"] !== _workspace.url + "Conference/") {
            _workspace["conferenceURL"] = _workspace.url + "Conference/";
        }


        sessionStorage.setItem("_workspace", JSON.stringify(_workspace));
        mqttInfo.host = data.mqttInfo.host;
        mqttInfo.port = data.mqttInfo.port;
        mqttInfo.ssl_host = data.mqttInfo.ssl_host;
        mqttInfo.ssl_port = data.mqttInfo.ssl_port;
        _webOffice = data.webOffice;

        if (data.allowSameUserWebSocket) {
            if (!!data.allowSameUserWebSocket.enable) {
                if (
                    data.allowSameUserWebSocket.enable === false ||
                    "false".equals(data.allowSameUserWebSocket.enable)
                ) {
                    _allowSameUserWebSocket = false;
                }
            }
        }
        console.log(" Option : allowSameUser's WebSocket set is " + _allowSameUserWebSocket);
    });
    $.ajaxSetup({
        async: true
    });
})();

//mail domain
let getMailDomain = (function() {
    Top.Ajax.execute({
        url: _workspace.url + "Users/SysContext",
        type: "GET",
        dataType: "json",
        cache: false,
        data: JSON.stringify({
            dto: {
                KEY: "MAIL_URL"
            }
        }),
        contentType: "application/json; charset=utf-8",
        crossDomain: true,
        xhrFields: {
            withCredentials: true
        },
        headers: {
            ProObjectWebFileTransfer: "true"
        },
        success: function(result) {
            _mailDomain = result.dto.VALUE;
        }
    });
})();

// TeeSpace 버전
function verCsp(){
	return "?" + tsVersion;
}

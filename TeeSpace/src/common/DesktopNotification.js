let CloudSpaceDesktopNotification = (function () {

    // 참고 자료
    // https://developers.google.com/web/fundamentals/push-notifications/how-push-works
    let debug = false;
    let checked = false;
    let enabled = checkAndRequestPermission();
    let previousTag = null;
    let currentTag = null;
    let pupup = null;
    let popupTimeout = null;
    
    function notificationAvailable() {
        if (!("Notification" in window)) {
            console.warn('Desktop notifications not available in your browser.');
            return false;
            // alert("This browser does not support system notifications");
            // This is not how you would really do things if they aren't supported. :)
        }
        return true;
    }

    function checkPermission() {
        if (!notificationAvailable()) {
            return false;
        } else if (Notification.permission === 'granted') {
            return true;
        } else if (Notification.permission === 'denied') {
            return false;
        } else {
            // noinspection JSIgnoredPromiseFromCall
            Notification.requestPermission(function (status) {
                enabled = (status === 'granted');
                console.log("Desktop notifications is permitted");
            });
            return false;
        }
        // Check 중일 경우 해당 Notification은 버리는 것으로 한다.
    }

    function checkAndRequestPermission() {
        if (!checked) {
            checked = true;
            return checkPermission();
        }
        return enabled;
    }
    
    // function closePreviousNotification(previousNotification) {
    // 	if (previousNotification === null) {
    // 		return;
    // 	} else {
    // 		previousNotification.close();
    // 	}
    // }
    
    // function updateNotification(currentNotification) {
    // 	previousNotification = currentNotification;
    // }

    function sendNotification(sender, message, url, photo, spaceId) {
    	if(message == null || message == undefined){
    		console.log("sendNotification : message is NULL or undefined");
    		return;
    	}
    	if(url == null || url == undefined){
    		console.log("sendNotification : url is NULL or undefined");
    		return;
    	}
    	
    	if (!!!enabled) {
            if (debug) {
                console.log("Desktop notification not allowed");
            }
            return;
        }

        let notificationTitle = sender;

        if (message !== null && message.length > 0) {
        	if (photo == undefined)
        		photo = cloudSpaceIconImg;
            let options = {
                body: message,
                icon: photo,
                requireInteraction: true,
                data: {
//                	urlToOpen: "/TeeSpace/#!" + url, // 로컬
                	urlToOpen: "/#!" + url,
                    spc: workspaceManager.getWorkspace(spaceId).SPACE_URL // ex: WNB83M4XAY
                },
                tag: Date.now()
            };
            
            // closePreviousNotification(previousNotification);

            //데스크탑 알림 요청
            // let notification = new Notification(notificationTitle, options);
            // updateNotification(notification);
            currentTag = options.tag;
//            console.log("previousTag: ", previousTag, "\ncurrentTag: ", currentTag);
            
            if ( !previousTag || typeof(serviceWorkerRegistration.getNotifications({tag : previousTag})) == "undefined" ) {
         	   serviceWorkerRegistration.showNotification(notificationTitle, options);
            }
            else {
            	serviceWorkerRegistration.getNotifications({tag : previousTag}).then(function(notification) {
            		if (notification.length) {
         			    notification[0].close();
         		    }
            		serviceWorkerRegistration.showNotification(notificationTitle, options);
            	})    
            }
            previousTag = currentTag;
            
            serviceWorkerRegistration.getNotifications({tag : previousTag}).then(function(notification) {
            	popup = notification[0];
            	if (popupTimeout == null) {
            		// do nothing
            	}
            	else {
            		clearTimeout(popupTimeout);
            	}
        		popupTimeout = setTimeout(function() { popup.close(); }, 10000);
        	})
            
//             if (debug) {
//                 console.warn(" NOTIFICATION ", notification);
//             }
//             notification.onclick = function () {
//                 // Browser가 활성화 되어 있는 여부를 체크 하여 추가 하라.
            	
//             	try {            		
//             		window.open(document.documentURI, "TeeSpace").focus();
//             		/*히스토리 제거 전 임시 라우팅*/
//             		let targetUrl = "#!" + url;
//             		let targetMain = getMainAppByUrl(targetUrl);
//             		let targetSub = getSubAppByUrl(targetUrl);
            		
//             		routeUrl(targetUrl, targetMain, targetSub);
            		
// //            		Top.App.routeTo(url);		// "#!"+ url
// 				} catch (e) {
// 					console.error(e);
// 				}
//             };
//             setTimeout(function () { notification.close(); }, 11000);  // 10초후 알림 닫기
        }
    }

    return {
        notify: function (sender, message, url, photo, spaceId) {
            if (debug) {
                console.warn(" Notify Called ", message);
            }
            sendNotification(sender, message, url, photo, spaceId);
        },
        enableDebug: function () {
            debug = true;
        }
    }

})();
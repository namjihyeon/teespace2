//"use strict";
const $$$ = (id) => Top.Dom.selectById(id);
const $ctrl = (id) => Top.Controller.get(id);

Top.Controller.create('components', {
	radioButtonGroup: function ({
		targetId,
		list,
		idPrefix = 'radio',
		onClick,
		initiallyClickedIndex,
		initiallyClickedValue,
		css,
		className,
	}) {
		if (!targetId || list.length === 0) return;

		if (initiallyClickedValue) {
			initiallyClickedIndex = list.map((elem) => elem.value).indexOf(initiallyClickedValue);
		} else if (!initiallyClickedValue && (typeof initiallyClickedIndex !== 'number' || initiallyClickedIndex < 0)) {
			initiallyClickedIndex = 0;
		}

		var classList = className ? className.split(' ') : [];
		var groupId = ''.rnd(32);
		for (var i = 0; i < list.length; i++) {
			var r = Top.Widget.create('top-radiobutton');
			r.setProperties({
				id: idPrefix + i,
				text: list[i].text,
				value: list[i].value,
				css: css,
				'on-click': onClick,
				'group-id': groupId,
			});
			r.addClass('cmradio');
			classList.forEach(function (elem) {
				r.addClass(elem);
			});
			$$$(targetId).addWidget(r);
		}
		$$$(targetId).complete();

		// 이걸로하면getchecked()가 안먹힘. setChecked() 사용해야함.
		//        $('top-radiobutton#' + idPrefix + initiallyClickedIndex.toString() + ' label.top-radiobutton-text').addClass('checked');
		//        console.info(idPrefix + initiallyClickedIndex.toString())
		//        debugger;
		$$$(idPrefix + initiallyClickedIndex.toString()).setChecked(true);
	},
	buttonTab: function ({ targetId, type = 1, idPrefix = 'cmpTab', initiallyClickedIndex, onClick, list, css }) {
		var className = 'cmp-tab' + type;
		var onClickCustom = function (event, widget) {
			// css
			$('.' + className + ' button').removeClass('selected');
			$('button#' + widget.id).addClass('selected'); // action
			onClick(event, widget);
		};
		// Make button elements and attach them to the target layout
		for (var i = 0; i < list.length; i++) {
			// essential
			var b = Top.Widget.create('top-button');
			b.setProperties({
				className: className,
				id: idPrefix + String(i),
				text: list[i].text,
				'on-click': onClickCustom,
				child: list[i].child,
				value: list[i].value,
			});
			// additional
			if (css) {
				var keys = Object.keys(css);
				for (var k = 0; k < keys.length; k++) {
					$('top-button>button').css(keys[k], css[keys[k]]);
				}
			}
			$$$(targetId).addWidget(b);
		}
		$$$(targetId).complete(); // default clicked button

		if (typeof initiallyClickedIndex !== 'undefined') {
			//    	$('button#' + idPrefix + String(initiallyClickedIndex)).addClass('selected');
			$('button#' + idPrefix + String(initiallyClickedIndex)).trigger('click');
		}
	},
	simpleGnb: function ({ id, targetId, list, onClick, type = 1, customData = undefined }) {
		// Create lnb layout
		var ll = Top.Widget.create('top-linearlayout');
		ll.setProperties({
			id: id,
			orientation: 'horizontal',
			'layout-width': 'wrap_content',
			'layout-height': 'match_parent',
			'border-width': '0px',
		});
		ll.addClass('cmp-lnb' + type);

		var onClickCustom = function onClickCustom(event, widget) {
			$('.cmp-gnb-button div.selected').removeClass('selected');
			$('div#' + widget.id).addClass('selected');
			onClick(event, widget);
		};
		$$$(targetId).addClass('cmp-gnb' + type);
		for (var i = 0; i < list.length; i++) {
			var btn = Top.Widget.create('top-linearlayout');
			btn.setProperties({
				id: list[i].id,
				'layout-width': '108px',
				'layout-height': '62px',
				orientation: 'vertical',
				'border-width': '0px 0px 0px 0px',
				value: list[i].hash,
				'on-click': onClickCustom,
			});
			btn.addClass('cmp-gnb-button');

			if (customData) {
				var tmpCustomData = {};
				Object.keys(customData).forEach((elem) => {
					tmpCustomData[elem] = customData[elem][i];
				});
				btn.setProperties({
					customData: tmpCustomData,
				});
			}

			var wr = Top.Widget.create('top-linearlayout');
			wr.setProperties({
				//                id: 'adminGnbWrapper' + i,
				'layout-width': 'match_parent',
				'layout-height': 'match_parent',
				orientation: 'vertical',
				'border-width': '0px 0px 0px 0px',
			});
			wr.addClass('cmp-gnb-button-inner-wrapper');
			var img = Top.Widget.create('top-imageview');
			img.setProperties({
				src: list[i].src,
				'layout-width': '20px',
				'layout-height': '20px',
			});
			wr.addWidget(img);
			wr.complete();
			var txt = Top.Widget.create('top-textview', {
				text: list[i].text,
				className: 'cm-text',
				'layout-width': 'match_parent',
			});
			wr.addWidget(txt);
			wr.complete();
			btn.addWidget(wr);
			btn.complete();
			$$$(targetId).addWidget(btn);
			$$$(targetId).complete();
		}
		$$$(targetId).complete();
		//        if (targetId) {
		//             // top 이슈(addWidget후 이벤트 바인딩풀림)
		//
		//            list.forEach(function (elem) {
		//                $$$(elem.id).setProperties({
		//                    'on-click': $$$(elem.id).template.onClick
		//                });
		//            }); // top 이슈(addWidget후 아이콘 사라짐)
		//
		//            list.forEach(function (elem) {
		//                if (elem.parent === 'root') {
		//                    $$$(elem.id).setProperties({
		//                        'icon': 'icon-work_open'
		//                    });
		//                }
		//            });
		//        }
		return $$$(targetId);
	},
	simpleLnb: function ({ id, targetId, list, onClick, type = 1, customData }) {
		if (list.length === 0) return;

		// Add 'child' property which contains child information
		for (var i = 0; i < list.length; i++) {
			list[i].child = [];
			for (var j = 0; j < list.length; j++) {
				if (i !== j && list[i].id === list[j].parent) {
					// [i]가 [j]의 부모일때
					list[i].child.push(list[j].id);
				}
			}
		}

		// Add click event for end nodes
		var onClickCustom = function (event, widget) {
			//            console.info(widget.id); // style
			$('.cmp-lnb-button button.selected').removeClass('selected');
			$('button#' + widget.id).addClass('selected'); // action
			onClick(event, widget);
		};
		// Add toggle click event for parent nodes
		var onClickToggle = function (event, widget) {
			//            console.info(widget.id); // style
			$('top-button#' + widget.id + ' span.top-button-icon').toggleClass('icon-work_open');
			$('top-button#' + widget.id + ' span.top-button-icon').toggleClass('icon-work_next');
			list.find(function (elem) {
				return elem.id === widget.id;
			}).child.forEach(function (childId) {
				$('top-button#' + childId).toggleClass('disp-none');
			});
		};

		// Create lnb layout
		var ll = Top.Widget.create('top-linearlayout');
		ll.setProperties({
			id: id,
			orientation: 'vertical',
			'layout-height': 'wrap_content',
			'border-width': '0px',
			'layout-width': 'match_parent',
		});
		ll.addClass('cmp-lnb' + type);

		var subllCnt = 0;
		var subll = null;
		for (var i = 0; i < list.length; i++) {
			var btn = Top.Widget.create('top-button', {
				id: list[i].id,
				text: list[i].text,
				'layout-width': 'match_parent',
				value: list[i].hash,
			});
			btn.addClass('cmp-lnb-button');

			if (customData) {
				var tmpCustomData = {};
				Object.keys(customData).forEach((elem) => {
					tmpCustomData[elem] = customData[elem][i];
				});
				btn.setProperties({
					customData: tmpCustomData,
				});
			}

			// 최상위 노드면 좌측 아이콘 생성(자식 유무와 관계없이, 위젯 정렬 목적)
			if (list[i].parent === 'root') {
				btn.setProperties({ icon: 'icon-work_open', 'icon-position': 'left' });

				// 기존에 만든 서브레이어가 있으면 레이아웃에 붙이기
				btn.addClass('root-child');

				subll !== null && ll.addWidget(subll);

				// 서브레이어 생성
				subllCnt++;
				subll = Top.Widget.create('top-linearlayout');
				subll.setProperties({
					id: 'cmpLnbSub' + subllCnt,
					className: 'cmp-lnb-sub',
					orientation: 'vertical',
					'layout-height': 'wrap_content',
					'layout-width': 'match_parent',
					'border-width': '0px',
				});
				subll.addClass('cmp-lnb-sub');
			} else {
				btn.addClass('child');
			}

			if (list[i].child.length > 0) {
				// 자식이 있는 노드
				btn.addClass('parent');
				btn.setProperties({ 'on-click': onClickToggle });
			} else {
				// 자식이 없는 노드
				btn.setProperties({ 'on-click': onClickCustom });
			}

			subll.addWidget(btn);
		}
		subll !== null && ll.addWidget(subll); // 기존에 만든 서브레이어가 있으면 레이아웃에 붙이기

		ll.complete(); // 레이아웃 완성

		if (targetId) {
			// attach lnb to target if target id is given
			$$$(targetId).addWidget(ll);
			// top 이슈(addWidget후 이벤트 바인딩풀림)
			list.forEach((elem) => {
				$$$(elem.id).setProperties({ 'on-click': $$$(elem.id).template.onClick });
			});
			// addWidget후 아이콘 사라지는 top 이슈 때문에 다시 아이콘 바인딩
			list.forEach((elem) => {
				elem.parent === 'root' &&
					$$$(elem.id).setProperties({ icon: 'icon-work_open', 'icon-position': 'left' });
			});
		}
		return ll;
	},

	simpleAttachment: function ({
		targetId,
		eachFileWrapperHeight = '30px', // 높이
		eachFileWrapperWidth = '50%', // 가로
		dataRepositoryName,
		dataInstanceName, // 필수 프로퍼티 fileId, fileNameWithExtension, fileSize
		onClickDownload,
		onClickDownloadAll,
		onClickDelist,
	}) {
		var files = window[dataRepositoryName][dataInstanceName];
		if (!files || files.length < 1) return;
		var titleLL = Top.Widget.create('top-linearlayout');
		titleLL.setProperties({
			id: 'attTitleLL',
			'layout-height': eachFileWrapperHeight,
			'layout-width': 'wrap_content',
			orientation: 'horizontal',
			'border-width': '0px',
		});
		var expandButton = Top.Widget.create('top-button');
		expandButton.setProperties({
			id: 'attTitleExpand',
			icon: 'icon-accordion_unselect', // 'icon-accordion_select'
			className: 'cm-icon-button',
			'layout-height': 'match_parent',
			'layout-width': 'wrap_content',
			'on-click': function (event, widget) {
				console.info(event);
				$('top-linearlayout.attach-file-wrapper').toggleClass('disp-none');
				$('#attTitleExpand>button>span').toggleClass('icon-accordion_select icon-accordion_unselect');
			},
		});
		//        var clipButton = Top.Widget.create('top-button');
		//        clipButton.setProperties({
		//        	id: 'attTitleClip',
		//            icon: 'icon-attach',
		//            className: 'cm-icon-button',
		//            'layout-height': 'match_parent',
		//			'layout-width': 'wrap_content',
		//		    'layout-height': 'match_parent',
		//			'layout-width': 'wrap_content',
		//        });

		var clipIcon = Top.Widget.create('top-icon');
		clipIcon.setProperties({
			id: 'attTitleClip',
			className: 'cm-icon icon-attach',
			'layout-height': 'match_parent',
			'layout-width': 'wrap_content',
		});
		var labelText = Top.Widget.create('top-textview');
		labelText.setProperties({
			id: 'attTitleLabel',
			text: Top.i18n.map.support.NORMAL_FILES,
			className: 'cm-text',
			'layout-height': 'match_parent',
			'layout-width': 'wrap_content',
		});
		var numText = Top.Widget.create('top-textview');
		numText.setProperties({
			id: 'attTitleNum',
			text: Top.i18n.map.support.N_FILES.replace('{d}', files.length),
			className: 'cm-text',
			'layout-height': 'match_parent',
			'layout-width': 'wrap_content',
		});
		var sizeText = Top.Widget.create('top-textview');

		sizeText.setProperties({
			id: 'attTitleSize',
			text: `(${files.reduce((acc, cur) => (acc = acc + cur.fileSize), 0).toReadableSize()})`,
			className: 'cm-text',
			'layout-height': 'match_parent',
			'layout-width': 'wrap_content',
		});
		var saveText = Top.Widget.create('top-textview');
		saveText.setProperties({
			id: 'attTitleSave',
			className: 'cm-text',
			text: Top.i18n.map.support.SAVE_ALL,
			'layout-height': 'match_parent',
			'layout-width': 'wrap_content',
			'on-click': onClickDownloadAll,
		});
		// var saveButton = Top.Widget.create('top-button');
		// saveButton.setProperties({
		// 	id: 'attTitleSave',
		// 	className: 'cmbutton',
		// 	text: Top.i18n.map.support.SAVE_ALL,
		// 	'layout-height': 'match_parent',
		// 	'layout-width': 'wrap_content',
		// 	'on-click': Top.Controller.get('commonReadLayoutLogic').onClickDownloadAll,
		// });

		titleLL.addWidget(expandButton);
		//        titleLL.addWidget(clipButton);
		titleLL.addWidget(clipIcon);
		titleLL.addWidget(labelText);
		titleLL.addWidget(numText);
		titleLL.addWidget(sizeText);
		titleLL.addWidget(saveText);
		titleLL.complete();
		$$$(targetId).addWidget(titleLL);

		// 개별 파일들
		for (var i = 0; i < files.length; i++) {
			// 모든 위젯에 fileId 바인딩
			var fileLL = Top.Widget.create('top-linearlayout');
			fileLL.setProperties({
				id: 'attWrapper' + i,
				className: 'attach-file-wrapper',
				'layout-height': eachFileWrapperHeight,
				'layout-width': eachFileWrapperWidth,
				orientation: 'horizontal',
				'border-width': '0px',
				value: files[i].fileId,
			});
			var fileLLSub = Top.Widget.create('top-linearlayout');
			fileLLSub.setProperties({
				id: 'attWrapperSub' + i,
				className: 'attach-file-wrapper-sub',
				'layout-height': eachFileWrapperHeight,
				'layout-width': 'wrap_content',
				orientation: 'horizontal',
				'border-width': '0px',
				'on-click': onClickDownload,
				value: files[i].fileId,
			});
			var downloadButton = Top.Widget.create('top-button');
			downloadButton.setProperties({
				id: 'attDownloadButton_' + i,
				icon: 'icon-work_download',
				className: 'cm-icon-button download',
				'layout-height': 'match_parent',
				'layout-width': 'wrap_content',
				//                'on-click': onClickDownload,
				value: files[i].fileId,
			});
			var tt1 = Top.Widget.create('top-textview');
			tt1.setProperties({
				id: 'attNameText_' + i,
				className: 'file-name',
				text: files[i].fileNameWithExtension,
				'layout-height': 'match_parent',
				'layout-width': 'wrap_content',
				//                'on-click': onClickDownload,
				value: files[i].fileId,
			});
			var tt2 = Top.Widget.create('top-textview');
			tt2.setProperties({
				id: 'attSizeText_' + i,
				className: 'file-size',
				text: files[i].fileSize.toReadableSize(),
				'layout-height': 'match_parent',
				'layout-width': 'wrap_content',
				value: files[i].fileId,
			});
			var delistButton = Top.Widget.create('top-button');
			delistButton.setProperties({
				id: 'attDelistButton_' + i,
				icon: 'icon-work_cancel',
				className: 'cm-icon-button delist',
				'layout-height': 'match_parent',
				'layout-width': 'wrap_content',
				'on-click': onClickDelist,
				value: files[i].fileId,
			});

			//          fileLL.addWidget(downloadButton);
			//          fileLL.addWidget(tt1);
			//          fileLL.addWidget(tt2);
			//          fileLL.addWidget(delistButton);
			//          fileLL.complete();
			//          $$$(targetId).addWidget(fileLL);

			fileLLSub.addWidget(downloadButton);
			fileLLSub.addWidget(tt1);
			fileLLSub.addWidget(tt2);
			fileLLSub.complete();
			fileLL.addWidget(fileLLSub);
			fileLL.addWidget(delistButton);
			fileLL.complete();
			$$$(targetId).addWidget(fileLL);
		}
		$$$(targetId).complete();

		// 다운로드 버튼, 파일 이름 간 css 효과 연동(에러있음. closure 때문에 맨 마지막 위젯만 자꾸 선택됨)

		//        $('.attach-file-wrapper').ready(function() {
		//        	for(var i=0; i<files.length; i++){
		//        		var buttonId = 'attDownloadButton_' + i;
		//        		var nameId = 'attNameText_'+i;
		//
		//        		$('.attach-file-wrapper top-button#' + buttonId + '.cm-icon-button.download button').hover(function() {
		//        			console.info("int")
		//        			$('.read .attach-file-wrapper top-button#' + buttonId + '.download button').addClass('hover');
		//        			$('.read .attach-file-wrapper top-textview#' + nameId + '.file-name span').addClass('hover');
		//        		}, function() {
		//        			console.info("out")
		//        			$('.read .attach-file-wrapper top-button#' + buttonId + '.download button').removeClass('hover');
		//        			$('.read .attach-file-wrapper top-textview#' + nameId + '.file-name span').removeClass('hover');
		//        		});
		//
		//        		$('.attach-file-wrapper top-textview#' + nameId + '.file-name span').hover(function() {
		//        			console.info("int")
		//        			$('.read .attach-file-wrapper top-button#' + buttonId + '.download button').addClass('hover');
		//        			$('.read .attach-file-wrapper top-textview#' + nameId + '.file-name span').addClass('hover');
		//        		}, function() {
		//        			console.info("out")
		//        			$('.read .attach-file-wrapper top-button#' + buttonId + '.download button').removeClass('hover');
		//        			$('.read .attach-file-wrapper top-textview#' + nameId + '.file-name span').removeClass('hover');
		//        		});
		//        	}
		//        });
	},

	testAttach: function (nFiles = 10) {
		$('div#attachmentLL').empty();
		$$$('attachmentLL').setProperties({ visible: 'visible' });
		let list = [...Array(nFiles).keys()].map((elem) => {
			let fileId = ''.rnd(40);
			// let fileExtension = 'jpg';
			let fileExtension = ['jpg', 'hwp', 'ppt', 'doc', 'zip', 'asdf'].rnd();

			let tmp = {
				fileId: fileId,
				fileExtension: fileExtension,
				fileNameWithExtension: `${fileId}.${fileExtension}`,
				readableFileSize: Number.prototype.rnd(100).toReadableSize(),
			};
			return tmp;
		});
		this.attachmentChip({
			targetId: 'attachmentLL',
			list: list,
		});
	},
	attachmentChip: function ({ targetId, list, detachCallback, downloadCallback, renderCallback }) {
		let target = document.querySelector(`div#${targetId}`);
		Object.assign(target.style, {
			display: 'flex',
			'flex-direction': 'row',
			'flex-wrap': 'wrap',
			'justify-content': 'flex-start',
			'align-content': 'flex-start',
			'align-items': 'flex-start',
		});
		while (target.firstChild) target.removeChild(target.firstChild);

		let html = '';
		for (var i = 0; i < list.length; i++) {
			let { fileId, fileExtension, fileNameWithExtension, readableFileSize, fileImage, tmpFileId } = list[i];
			// console.info(fileExtension);
			const _driveImagePath = './res/support/TeeDrive_Active.svg';
			const _fileImagePath = ((ext) => {
				if (['jpg', 'png', 'gif'].includes(ext)) return './res/support/file_icons/drive_image.svg';
				if (['xls', 'xlsx', 'tls'].includes(ext)) return './res/support/file_icons/drive_tocell.svg';
				if (['hwp'].includes(ext)) return './res/support/file_icons/drive_tohangul.svg';
				if (['ppt', 'pptx', 'tpt'].includes(ext)) return './res/support/file_icons/drive_topoint.svg';
				if (['doc', 'docx', 'toc'].includes(ext)) return './res/support/file_icons/drive_toword.svg';
				if (['zip', 'tar', 'gz', '7z', 'egg', 'alz'].includes(ext))
					return './res/support/file_icons/drive_zip.svg';
				return './res/support/file_icons/drive_file.svg';
			})(fileExtension);
			let isUploaded, fileIdString, uploadedClassName;
			if (fileId) {
				isUploaded = 1;
				fileIdString = fileId;
				uploadedClassName = 'uploaded';
			} else {
				isUploaded = 0;
				fileIdString = tmpFileId;
				uploadedClassName = 'added';
			}
			html = `
    	    	<div id="attach_${fileIdString}" class="spt-attach" data-file-id="${fileIdString}">
    		    	<div class="spt-attach-img" data-file-id="${fileIdString}">
    		    		<img class="file-image" src="${_fileImagePath}" title="${fileExtension.toLowerCase()}" alt="${fileExtension.toLowerCase()}" data-file-id="${fileIdString}">
    		    		<img class="${uploadedClassName}" src="${_driveImagePath}" alt="drive" data-file-id="${fileIdString}"/>
    	    		</div>
    	    		<div class="spt-attach-txt">
	    	    		<div class="file-name" title="${fileNameWithExtension}">${fileNameWithExtension}</div>
	    	    		<div class="file-size" title="${readableFileSize}">${readableFileSize}</div>
    	    		</div>
    	    		<div class="spt-attach-btn">
    	    			<span class="close-btn" data-file-id="${fileIdString}">
    	    			&times;
    	    			</span>
    	    		</div>
    			</div>
				`;
			target.appendChild(htmlToElement(html));
		}
		renderCallback();

		document.querySelectorAll('div.spt-attach-img').forEach((elem) =>
			elem.addEventListener('click', function (event) {
				// console.info(event.target.dataset.fileId);
				downloadCallback(event, null, event.target.dataset.fileId);
			})
		);
		// document.querySelectorAll('div.spt-attach-img').forEach((elem) =>
		// 	elem.addEventListener('click', function (event) {
		// 		// console.info(event.target.dataset.fileId);
		// 		downloadCallback(event, event.target, event.target.dataset.fileId);
		// 	})
		// );
		document.querySelectorAll('span.close-btn').forEach((elem) =>
			elem.addEventListener('click', function (event) {
				// console.info(event.target.dataset.fileId);
				detachCallback(event, null, event.target.dataset.fileId);
			})
		);
	},
	searchModule: function ({
		targetId,
		id = 'searchLayout',
		boxType,
		boxDefaultType,
		boxTitle,
		boxChange,
		fieldText,
		fieldHint,
		buttonText,
		buttonClick,
		buttonIcon,
	}) {
		var ll = Top.Widget.create('top-linearlayout');
		ll.setProperties({
			id: id,
			className: 'search',
		});
		$$$(targetId).addWidget(ll);
		var sb = this.selectBox({
			targetId: ll,
			id: 'searchType',
			className: 'selectbox',
			item: boxType,
			title: boxTitle,
			defaultItem: boxDefaultType,
			'on-change': boxChange,
		});
		var tf = Top.Widget.create('top-textfield');
		tf.setProperties({
			id: 'searchField',
			className: 'textfield',
			text: fieldText,
			hint: fieldHint,
		});
		var bt = Top.Widget.create('top-button');
		bt.setProperties({
			id: 'searchButton',
			text: buttonText,
			className: 'button',
			icon: buttonIcon,
			'on-click': buttonClick,
		});
		ll.addWidget(sb);
		ll.addWidget(tf);
		ll.addWidget(bt);
		ll.complete();
	},

	postMetaTable: function ({ targetId, nameList, valueList, nr = 4, nc = 2 }) {
		//        // **************************************************
		//        if (!arg || !arg.targetId) return;
		//        var nameList = arg.nameList;
		//        var valueList = arg.valueList;
		//        var targetId = arg.targetId;
		//        var nr = 4;
		//        var nc = 2;
		//        // **************************************************
		//
		//        for (var ir = 0; ir < nr; ir++) {
		//            var tmpRow = Top.Widget.create('top-linearlayout',{
		//            	id: 'metaRow_' + ir,
		//            });
		//            tmpRow.addClass('meta-row');
		//
		//            for (var ic = 0; ic < nc; ic++) {
		//            	var tmpSub = Top.Widget.create('top-linearlayout');
		//            	tmpSub.setProperties({
		//            		   id: 'metaSub_' + ir + '_' + ic,
		//            		   width: String(100/nc) + '%',
		//            	});
		//
		//                var tmpName = Top.Widget.create('top-textview', {
		//                    id: 'metaName_' + ir + '_' + ic,
		//                    text: nameList[nc * ir + ic],
		//                    'width': '200px',
		//                    'min-width': '200px',
		//                    'max-width': '300px',
		//                });
		//                tmpName.addClass('meta-name');
		//
		//                var tmpValue = Top.Widget.create('top-textview', {
		//                    id: 'metaValue_' + ir + '_' + ic,
		//                    text: valueList[nc * ir + ic],
		//                    width: 'match_parent',
		//                });
		//                tmpValue.addClass('meta-value')
		//
		//                tmpSub.addWidget(tmpName);
		//                tmpSub.addWidget(tmpValue);
		//                tmpRow.addWidget(tmpSub);
		//            }
		//            $$$(targetId).addWidget(tmpRow);
		//        }
		//
	},
});

// const $cmp = (componentName, arg) => Top.Controller.get('components')[componentName](arg);
const $cmp = (() => Top.Controller.get('components'))();

Top.Controller.create("AppMainLogic", {
    init: function(widget) {
    	
    	// user photo
    	Top.Dom.selectById('gnbAppProfileIcon').setSrc(userManager.getLoginUserPhoto());
        
    	// selected subapp
        gnbSubState(appManager.getSubApp());
        
    	// 우측 -app
		$(".gnb-app_icon").on("click", clickApp);
//		$("span#gnbAppPlusIcon").on("click", clickApp);
        $('img#gnbAppProfileIcon').on("click", openDropdown);
        Top.Dom.selectById('AppGnbFreindButton').setProperties({'on-click':'getFriendList'});
        Top.Dom.selectById('AppGnbRoomButton').setProperties({'on-click':'getSpaceList'});
        Top.Dom.selectById('AppGnbCurrentIcon').setProperties({'on-click': 'createSpace'});
        
        // minitalk, search, invite
        $('div#AppGnbTitleLayout').append(`<span id="talk__Content-search-button" class="icon-work_search" style="display: flex";></span>`);
        $('div#AppGnbCurrentImage').on("click", () => { 
        	if(spaceAPI.isTempSpace(getRoomIdByUrl())) return false;
         	if(workspaceManager.getMySpaceUrl() === getRoomIdByUrl()) return false;
        	showmemClickid = workspaceManager.getWorkspaceId(getRoomIdByUrl());
        	tds('ShowMemDialog').open();
        });
        $('div#AppGnbTitleLayout').append(`<span id="gnbTalkIcon" class="gnb-icon_talk" style="display: flex";></span>`);
        $('div#AppGnbTitleLayout>#gnbTalkIcon').on("click", ()=> openMiniTalk(getRoomIdByUrl()));
        
        
        inquiryUnreadMail().then((result)=>{
        	let unreadCnt = 0;
        	accountList = result.data.dto.AccountList;
            accountList.forEach((account) =>{
            	if("USE0003" === account.WS_USE_CODE) return true;			//임시 - 메일서버 메일 제외
            	for(let i=0; i<account.FolderList.length; i++){
            		let folder = account.FolderList[i];
                	if("MFL0001" === folder.FOLDER_TYPE) unreadCnt += parseInt(folder.MAIL_COUNT);
            	}
            	setMailCount(unreadCnt);
            });
        });
        
        /*inquiryMessengerRooms().then((result) =>{
        	let unreadCnt = 0;
        	if(result && result.ttalkMessengersList){
        		result.ttalkMessengersList.forEach((ele)=>{
        			if(ele.UNREAD_USER_COUNT) unreadCnt += parseInt(ele.UNREAD_USER_COUNT);
        		});
        		setTalkCount(unreadCnt);
        	}
        });*/
        
        // unread talk, mail
        $('div#AppGnbRoomButton').append(`<span id="gnbTalkUnread" class="gnb-unread"></span>`);
        $('div#gnbAppMailIcon').append(`<span id="gnbMailUnread" class="gnb-unread"></span>`);
		spaceAPI.initRoomList();
		
		//top split layout init 되지 않는 오류 (router.js)
		if(!tds('AppSplitLayout')) tds('AppContainLayout').src('AppSplitLayout.html' + verCsp()); 
    },
    
    //getSpaceList와 통합 필요
    getFriendList: function(event, widget) {
    	clickFriend();
//    	let url = location.hash.slice(5);
//    	let sub = appManager.getSubApp();
//    	let main = appManager.getMainApp();
//    	let option = appManager.getType(main, sub);
//
//        Top.App.routeTo(`/f/${url}`, {eventType: option.eventType});
    },
    
    //getFriendList와 통합 필요    
	getSpaceList : function(event, widget) {
		clickSpace();
//    	let url = location.hash.slice(5);
//    	let sub = appManager.getSubApp();
//    	let main = appManager.getMainApp();
//    	let option = appManager.getType(main, sub);
//    	
//        Top.App.routeTo(`/s/${url}`, {eventType: option.eventType});
	},
	
	createSpace: function(){
		let spcId = workspaceManager.getWorkspaceId(getRoomIdByUrl());
			
		spaceAPI
			.getSpaceMemberListBrief(spcId)
			.then(result => {
				let inputArr = result.data.dto.UserProfileList.map((ele) => {
					return {
						FRIEND_ID	:	ele.USER_ID,
						THUMB_PHOTO	:	ele.THUMB_PHOTO,
						USER_NAME	:	ele.USER_NAME,
					};
				});
				InviteMemberDialog.open({
					wsId: workspaceManager.getWorkspaceId(spcId), 
					alreadyInvitedList: inputArr
				});
			})
			.catch(err => {
				InviteMemberDialog.open({
					wsId: workspaceManager.getWorkspaceId(spcId), 
					alreadyInvitedList: []
				});
			});
	}
	
});


function getOrgUserInfo() {
		
	this.user_id = userManager.getLoginUserInfo().userId;
	
	var inputDTO = {
			"dto": {
				"USER_ID": userManager.getLoginUserInfo().userId
			}
	};
	
	let empNum = [];

	Top.Ajax.execute({
		url : _workspace.url + "Admin/OrgUser?action=",
		type : 'GET',
		dataType : "json",
		async : false,
		cache : false,
		data : JSON.stringify(inputDTO), // arg
		contentType : "application/json",
		crossDomain : true,
		xhrFields : {
			withCredentials: true
		},
		headers: {
			"ProObjectWebFileTransfer":"true"
		},
		success : function(data) {
			data = JSON.parse(data);

     	    data.dto.companyProfileList.forEach((ele) => {
					empNum.push(ele.USER_EMPLOYEE_NUM);
			});
		},
		error : function(data) {
			var outputdata;
			outputdata = {
				result : "Fail",
				message : data.exception != undefined ? data.exception.message : "Error",
				data : data
			}		
			console.info("fail");
			console.info(outputdata);
		}
	});
	
	return empNum;
}

function clickApp() {
    let clicked = this.id.toLowerCase();
    clicked = clicked.split("app")[1].slice(0, -4);
	let temp = getQueryStringByKey("q")? getQueryStringByKey("q") : getTimeStamp(); 

    if ("mail" === clicked) {
    	
    	if("https://csdev2.tmaxcloudspace.com" === window.location.origin){
    		tds('AppContainLayout').src('webviewLayout.html', function(){
    			let targetUrl = "https://222.122.51.20";
        		tds('mainWebview').setProperties({'url': targetUrl});
        		setTimeout(()=>{
        			let iframe = $('#mainWebview>iframe')[0];
        			iframe.contentWindow.postMessage(getCookie('token'), targetUrl);
        			iframe.contentWindow.postMessage(getOrgUserInfo(), targetUrl);
        		},1000);
    		});
    		return false;
    	}
    	
    	/* 룸 클릭과 같은 함수로 변경 필요 */
    	let url = window.location.hash.slice(2);
    	let sub = appManager.getSubApp();
    	let main = getMainAppByUrl();
    	let type = appManager.getType("mail", sub);
    	let routeSub = sub?  `?sub=${sub}&q=${getTimeStamp()}` : `?q=${getTimeStamp()}`
    	
    	// lnb만 교체된 경우 - 레이아웃 교체
//    	if("mail"===appManager.getMainApp() && !tds('mailMainLeft')){
//    		clickMail();
//    		return false;
//    	}
    	let mailTarget = tds('mailMainLeft')? "mail" : ("mail"===main? getMainAppFullUrl() : "mail");
    		//"m"===getLnbByUrl()? getMainAppFullUrl() : "mail";
    	
    	appManager.setMainApp("mail");
    	if("mail" === main){	//getRoomIdByUrl() === workspaceManager.getMySpaceUrl()
    		Top.App.routeTo(`/m/${workspaceManager.getMySpaceUrl()}/${mailTarget}${routeSub}`, { eventType: type.eventType });    		
    	}else{
    		appManager.setSubApp(null);
        	Top.App.routeTo(`/m/${workspaceManager.getMySpaceUrl()}/mail?q=${getTimeStamp()}`, { eventType: 'close' }); 
    	}
    }
    else {
    	if(clicked == appManager.getSubApp() ){
    		onCloseButtonClick();
    		return false;
    	}
    	
        appManager.setSubApp(clicked);
//      appManager.setMainApp(mainUrl); 
        
        const mySpace = workspaceManager.getMySpaceUrl();
        let mainApp = appManager.getMainApp();
        const subApp = appManager.getSubApp();
        const mainUrl = getMainAppByUrl();
        const subUrl = getSubAppByUrl();
        const lnbUrl = getLnbByUrl();
        const roomId = ["f","m"].includes(lnbUrl)? mySpace : getRoomIdByUrl();	// 친구, 메일탭 - 마이스페이스
        let inputLnb;
        
        if("f"===lnbUrl){														// 친구 탭에서 서브앱을 키는 경우 (스페이스/ 토크/ 서브앱) -- 추후 정리
        	inputLnb = "s";
        	mainApp = "talk";
        	appManager.setMainApp("talk");
        }
        else{
        	inputLnb = lnbUrl;
		}
		
		if(subApp == "meeting" && subUrl != "meeting" && mainUrl != "meeting") {
			TeeAlarm.open({
				title: 'TeeMeeting을 시작하시겠습니까?',
				content: '미팅을 시작하면 멤버들에게 참여 알림이 전송됩니다.',
				icon: 'icon-info',
				buttons: [
					// {
					// 	text: '미팅 시작',
					// 	onClicked: (e) => {
					// 		appManager.setSubApp("meeting2");
					// 		if("talk" === mainUrl || "mail" === mainUrl || (roomId === mySpace && "f"===lnbUrl )){
					// 			let mainTarget = "f" ===lnbUrl? "talk" : getMainAppFullUrl();
					// 			Top.App.routeTo(`/${inputLnb}/${roomId}/${mainTarget}?sub=meeting2`, { eventType: "fold" });          //${mainApp}
					// 		}
					// 		else{
					// 			Top.App.routeTo(`/${inputLnb}/${roomId}/meeting2`, { eventType: "expand" });
					// 		}
					// 		TeeAlarm.close();
					// 	}
					// }
					{
						text: '미팅 시작',
						onClicked: (e) => {
							if("talk" === mainUrl || "mail" === mainUrl || (roomId === mySpace && "f"===lnbUrl )){
								let mainTarget = "f" ===lnbUrl? "talk" : getMainAppFullUrl();
								Top.App.routeTo(`/${inputLnb}/${roomId}/${mainTarget}?sub=${subApp}&q=${getTimeStamp()}`, { eventType: "fold" });          //${mainApp}
							}
							else{
								Top.App.routeTo(`/${inputLnb}/${roomId}/${subApp}?q=${getTimeStamp()}`, { eventType: "expand" });
							}
							TeeAlarm.close();
						}
					}
				],
				cancelButtonText: '취소',
				onCancelButtonClicked: (e) => { 
					appManager.setSubApp(getSubAppByUrl());
					TeeAlarm.close();
				}, 
			});
			return;
		}
		
        if("talk" === mainUrl || "mail" === mainUrl || (roomId === mySpace && "f"===lnbUrl )){
			let mainTarget = "f" ===lnbUrl? "talk" : getMainAppFullUrl();
			Top.App.routeTo(`/${inputLnb}/${roomId}/${mainTarget}?sub=${subApp}&q=${temp}`, { eventType: "fold" });          //${mainApp}
        }
        else{
        	onExpandButtonClick();
        	//Top.App.routeTo(`/${inputLnb}/${roomId}/${subApp}?q=${temp}`, { eventType: "expand" });
        }
    }
} 

//lnb - 스페이스 리스트
function clickSpace(){
	if(!tds('spaceListLayout')){
		Top.Dom.selectById("AppLnbLayout").src("spaceListLayout.html" + verCsp());		
	}

	$('top-linearlayout.button-gnb').removeClass('active');
	$('top-linearlayout#AppGnbRoomButton').addClass('active');
}
//lnb - 프렌즈 리스트
function clickFriend(){
	if (spaceAPI.getTempManager()[0]) {
		var WS_ID = spaceAPI.getTempManager()[0].WS_ID;
		var cur_url = getRoomIdByUrl();
		if (workspaceManager.getWorkspaceId(getRoomIdByUrl()) == WS_ID) {
			let PREV_WS_ID = $('div.lnbSpaceList#' + WS_ID).prev().attr('id')
			let NEXT_WS_ID = $('div.lnbSpaceList#' + WS_ID).next().attr('id')
			if (NEXT_WS_ID) {
				var targetSpace = workspaceManager.getWorkspaceUrl(NEXT_WS_ID);
				routeUrl(`/s/${targetSpace}/talk`, "talk", null);
			}
			else if (PREV_WS_ID) {
				var targetSpace = workspaceManager.getWorkspaceUrl(PREV_WS_ID);
				routeUrl(`/s/${targetSpace}/talk`, "talk", null);
			}
		}
	}
	if(!tds('friendListLayout')){
		Top.Dom.selectById("AppLnbLayout").src("friendListLayout.html"+ verCsp());
	}

	$('top-linearlayout.button-gnb').removeClass('active');
	$('top-linearlayout#AppGnbFreindButton').addClass('active');
}
//lnb - 메일
function clickMail(){
	if(!tds('mailMainLeft')){
		//		Top.Dom.selectById('AppLnbLayout').src('mailMainLeft.html' + verCsp());
		//		routeTo
			}
		
	$('top-linearlayout.button-gnb').removeClass('active');
	$('top-linearlayout#gnbAppMailIcon').addClass('active');
}

 
function renderLnb(lnbType){
//	$('.button-gnb').removeClass('active');
	switch(lnbType){
		case "f":{
			clickFriend();
			break;
		}
		case "s":{
			clickSpace();
			break;
		}
		case "m":{
			clickMail();
			break;
		}
	}
}
function renewGnbImage(){
	workspaceManager.update()
	let space = workspaceManager.getWorkspaceId(getRoomIdByUrl());
	let spaceInfo = spaceAPI.getSpaceProfile(space);
	let photo = spaceInfo? spaceInfo.THUMB_DIV : "";
	$('div#AppGnbCurrentImage .lnbSpaceList').remove();
	$('div#AppGnbCurrentImage').append('<div class="lnbSpaceList"></div>');		//임시
	$('div#AppGnbCurrentImage .lnbSpaceList').append(photo); 
}

function renderGnb(){
	let space = workspaceManager.getWorkspaceId(getRoomIdByUrl());
	let spaceInfo = spaceAPI.getSpaceProfile(space);
	let lnb = getLnbByUrl();
	let name =  getRoomIdByUrl()===workspaceManager.getMySpaceUrl() || getRoomIdByUrl()===workspaceManager.getMySpaceId()? userManager.getLoginUserName() + ' (나)' : (spaceInfo? spaceInfo.SPACE_NAME : ""); 
	let photo = spaceInfo? spaceInfo.THUMB_DIV : "";
	
	tds('AppGnbCurrentText').setText(name);
	


	document.querySelector('span#gnbTalkIcon').style.display="none";
	document.querySelector('span#talk__Content-search-button').style.display="none";
	tds('AppGnbCurrentIcon').setVisible('none');
	if("f"===lnb) {
		tds('AppGnbCurrentImage').setVisible('none');
		tds('AppGnbCurrentText').setVisible('none');
	}else{
		tds('AppGnbCurrentImage').setVisible('visible');
		tds('AppGnbCurrentText').setVisible('visible');
//		workspaceManager.getWorkspacePhoto(space).then((res)=>{
//			tds('AppGnbCurrentImage').setSrc(res);
//		});
		$('div#AppGnbCurrentImage .lnbSpaceList').remove();
		$('div#AppGnbCurrentImage').append('<div class="lnbSpaceList"></div>');		//임시
		$('div#AppGnbCurrentImage .lnbSpaceList').append(photo); 
		 
		
		if("s"===lnb){
			document.querySelector('span#talk__Content-search-button').style.display= appManager.isExpanded()? "none" : "flex";
			tds('AppGnbCurrentIcon').setVisible( workspaceManager.getMySpaceUrl() === getRoomIdByUrl()? 'none' : 'visible');
			document.querySelector('span#gnbTalkIcon').style.display="flex"; 	//if(workspaceManager.getMySpaceUrl() !== getRoomIdByUrl()) 
		} 
	}
}


function renderGnbMini(){
	let space = workspaceManager.getWorkspaceId(getRoomIdByUrl());
	let spaceInfo = spaceAPI.getSpaceProfile(space);
	let lnb = getLnbByUrl();
	let name =  getRoomIdByUrl()===workspaceManager.getMySpaceUrl() || getRoomIdByUrl()===workspaceManager.getMySpaceId()? userManager.getLoginUserName() + ' (나)' : (spaceInfo? spaceInfo.SPACE_NAME : ""); 
	let photo = spaceInfo? spaceInfo.THUMB_DIV : "";
	
	document.querySelector('span#AppGnbCurrentText a').textContent = name;

	document.querySelector('span#talk__Content-search-button').style.display="none";

//	workspaceManager.getWorkspacePhoto(space).then((res)=>{
//		document.querySelector('img#AppGnbCurrentImage').src = res;
//	});
	$('div#AppGnbCurrentImage .lnbSpaceList').remove();
	$('div#AppGnbCurrentImage').append('<div class="lnbSpaceList"></div>');		//임시
	$('div#AppGnbCurrentImage .lnbSpaceList').append(photo); 
	
//	document.querySelector('span#talk__Content-search-button').style.display="flex";
	
}


function gnbSubState(sub){
    $(`.gnb-app_icon>div`).removeClass('active');
//    if("plus"===sub) return false;
    if(sub){
        let subState = sub[0].toUpperCase() + sub.slice(1);
        $(`div#gnbApp${subState}Icon`).addClass('active');
    }
}


(function (exports){
	let _mailCnt;
//	let _talkCnt;
	let _talkCnt = mobx.observable.box();
	_talkCnt.set(0);
	
    mobx.observe(_talkCnt, change=>{ 
    	let total = change.newValue;

    	if(total>0){
    		$('#gnbTalkUnread').text(total>99? '99+': total); 
    		$('#gnbTalkUnread').css('visibility', 'visible');
    	}
    	else{
    		$('#gnbTalkUnread').css('visibility', 'hidden');
    	}
    });

	
	
	const _getMessengerRooms = function(){
		let serviceUrl = `${_workspace.url}Messenger/ttalkmessengers?action=List&user-id=${userManager.getLoginUserId()}`
		return axios.get(serviceUrl).then((ret) => {
			let messengerInfo = ret.data.dto;
			return messengerInfo;
		});
	}
	
	const _inquiryUnreadMail = function(){
		let serviceUrl = `${_workspace.url}Mail/Mail?action=BasicInfo&USER_ID=${userManager.getLoginUserId()}&WS_USE_CODE=USE0001`
		return axios.get(serviceUrl);
	}
	
	//Todo : new, prototype으로 토크와 동일한 부모로?
	const getUnReadMail = function(){
		if(!_mailCnt){	//init으로 뺀다.
			_inquiryUnreadMail().then((result) => {
				let accountList = result.data.dto;
			});
		}
		return _mailCnt;
	}
	const _setMailCount = function(input){
		_mailCnt = input;		 // 받은편지함 갯수, 즐겨찾기는?
		_renderMailCount();
	}
	const _setTalkCount = function(input){
//		_talkCnt = input;		 // 토크 갯수?
//		_renderTalkCount();
		_talkCnt.set(Number(input));
	}

	const _updateUnreadTotalCount = () => {

		//Talk 부분
        let lnb = document.querySelector('div#ourSpaceLayout');
        let nodeList=lnb.querySelectorAll('.unreadCount');
        let totalCount=0;
        for( let index=0; index<nodeList.length; index++){
        	if( nodeList[index] ){
                if( nodeList[index].textContent ){
                	totalCount +=parseInt(nodeList[index].textContent);
				}
			}
        }
        _setTalkCount(totalCount);
	};
	const _renderMailCount = function(){
		if(!_mailCnt || _mailCnt < 1 || $('#gnbMailUnread').length === 0){
			$('#gnbMailUnread').css('visibility', 'hidden');
		}
		else{
			$('#gnbMailUnread').text(_mailCnt>99? '99+': _mailCnt);
			$('#gnbMailUnread').css('visibility', 'visible');
		} 
	}
	/*
	const _renderTalkCount = function(){

		talkRenderer.component.setCommunicationBarUnreadCount();
	} */
	
	exports.inquiryUnreadMail = _inquiryUnreadMail;
	exports.setMailCount = _setMailCount;
	exports.getUnReadMail = getUnReadMail;
	exports.inquiryMessengerRooms = _getMessengerRooms;
	exports.updateUnreadTotalCount=_updateUnreadTotalCount;
	exports.setTalkCount = _setTalkCount;
	exports.getTalkCount = () => _talkCnt.get();
	 
})(this);

const makeSpan = (id, cls) => htmlToElement(`<span id='${id}' class='${cls}'></span>`);
const makeDiv = (id, cls) => htmlToElement(`<div id='${id}' class='${cls}'></div>`);	
const makeImg = (id, cls, src) => htmlToElement(`<img id='${id}' class='${cls}' src='${src}'></img>`);	
const makeButton = (id, cls, txt) => htmlToElement(`<button id='${id}' class='${cls}'>${txt}</button>`);
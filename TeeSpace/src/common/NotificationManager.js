let TEESPACE_CHANNELS = {
    TTALK:{
        CHANNEL_TYPE: "CHN0001",
        ROUTING_KEY: "talk",
    },
    TMAIL:{
        CHANNEL_TYPE: "CHN0002",
        ROUTING_KEY: "mail"
    },
    TNOTE:{
        CHANNEL_TYPE: "CHN0003",
        ROUTING_KEY: "note"
    },
    OFFICE:{
        CHANNEL_TYPE: "CHN0004",
        ROUTING_KEY: "office"
    },
    TSCHEDULE:{
        CHANNEL_TYPE: "CHN0005",
        ROUTING_KEY: "schedule"
    },
    TDRIVE:{
        CHANNEL_TYPE: "CHN0006",
        ROUTING_KEY: "drive"
    },
    CONFERENCE:{
        CHANNEL_TYPE: "CHN0007",
        ROUTING_KEY: "meeting"
    }
}

// 실제 Config를 통해 결정되는 설정들
let CS_Alarm_Config={
	SYSTEM:{
		ALL		        :"space_all",
		DESKTOP_NOTIFICATION :"desktop_all",
		SYSTEM_INVITE   :"space_invite"		
	},
    TTALK:{
        ALL             :"ttalk_all",
        SHOW_CONTENTS   :"ttalk_showContents",
    },
    TMAIL: {
        ALL             : "tmail_all",
    },
    TSCHEDULE: {
        ALL             : "tschedule_all",
    },
    CONFERENCE: {
        ALL             : "cloudmeeting_all",
        MEETING_START   : "cloudmeeting_meetingstart",
        MEETING_END     : "cloudmeeting_meetingend",
    }
};

let TEESPACE_CHANNEL_TYPE = {
	CHN0001      :  {
		NAME            : "TTALK",
		ttalk_text      : "새 메시지를 보냈습니다.",
		ttalk_emoticon  : "이모티콘을 보냈습니다.",
		ttalk_image     : "사진을 보냈습니다.",
		ttalk_video     : "동영상을 보냈습니다.",
		ttalk_file      : "파일을 보냈습니다.",
		ttalk_mention   : "님을 언급했습니다.",
		ttalk_reply		: "답장을 보냈습니다.",
		ROUTING_KEY: "talk"
	},
	CHN0007    :  {
		NAME                      : "CONFERENCE",
		cloudmeeting_meetingstart : "TeeMeeting을 시작합니다.",
		cloudmeeting_meetingend   : "TeeMeeting을 종료합니다.",
		ROUTING_KEY: "meeting"
	},
	CHN0005    :  {
		NAME          : "TSCHEDULE",
        tschedule_0   : "일정을 시작합니다.",
		tschedule_1   : "일정에 초대했습니다.",	
		tschedule_5m  : "일정 시작 5분 전입니다.",
		tschedule_10m : "일정 시작 10분 전입니다.",
		tschedule_15m : "일정 시작 15분 전입니다.",
		tschedule_30m : "일정 시작 30분 전입니다.",
		tschedule_1h  : "일정 시작 1시간 전입니다.",
		tschedule_2h  : "일정 시작 2시간 전입니다.",
		tschedule_1d  : "일정 시작 1일 전입니다.",
		tschedule_2d  : "일정 시작 2일 전입니다.",
		ROUTING_KEY: "schedule"
	},
	CHN0002    :  {
		NAME        : "TMAIL",
		ROUTING_KEY: "mail"
	},
	SYSTEM     :  {
		NAME         : "SYSTEM",
		space_invite : "Room에 초대했습니다.",
		ROUTING_KEY: "talk" // 스페이스 생성 시 톡 화면으로 이동
	}
}

const NotificationManager = ( function(){
	let recentNotifications = [];
    let blockNotificationData = [];
    let badgeSpaceId = null;      // 마지막으로 뱃지를 생성한 스페이스
    let defaultTitle = "티키타카 - TeeSpace";
    document.title = defaultTitle;
    const titleBadgeIcon = " 🔔"; // 파비콘 미정
    const zeroWidthSpace = '\u200b'; // 타이틀 공백일 때, 일반 공백은 데스크탑노티에서 다 무시하기 때문에 zero width space 문자를 이용해 제목을 빈칸으로 채움
    
    // 알림에 대한 broadcastChannel, 이벤트 핸들러 등록
    const broadcastChannel = (function () {
    	const notificationChannel = new BroadcastChannel("teeSpace_notification");
    	
    	notificationChannel.addEventListener("message", function(event) {
    		// broadcastChannelMessageCount로 broadcastChannel를 통해 전달된 메세지의 횟수를 센다.
    		Notification(event.data.webSocketMessage, event.data.eventType, event.data.broadcastChannelMessageCount + 1); 
    	})

    	return notificationChannel;
    })();
    
    const isMiniTalk = function() {
    	return opener !== null ? true : false;
    }
    
    // 알림을 처리할 teeSpace Tab을 지정.
    const teeSpaceTab = (function () {
    	if (isMiniTalk())  // 미니톡이라면
    		return;
    	
    	if (localStorage.getItem("teeSpaceTabs") === null) {
    		const teeSpaceTabs = { tabs: [ conid ], 
								   masterTabRefreshed: false };
    		localStorage.setItem("teeSpaceTabs", JSON.stringify(teeSpaceTabs));
    	} else {
    		const entries = performance.getEntriesByType("navigation");
    		const teeSpaceTabs = JSON.parse(localStorage.getItem("teeSpaceTabs"));
    		let newTeeSpaceTabs = null;
    		
    		if (teeSpaceTabs.tabs.includes(conid) === false) {
    			newTeeSpaceTabs = { tabs: teeSpaceTabs.tabs.filter(function(tab) { return tab !== null }), 
    							    masterTabRefreshed: teeSpaceTabs.masterTabRefreshed };
    			
    			// 현재 탭이 마스터 탭이었고, 새로고침 되었다면
    			if (teeSpaceTabs.masterTabRefreshed === true && entries.map( nav => nav.type )[0] === "reload") 
    				newTeeSpaceTabs.tabs.unshift(conid); // teeSpaceTabs의 첫 번째 요소에 추가한다.
    			else
    				newTeeSpaceTabs.tabs.push(conid); // 새로 추가된 탭은 리스트의 마지막에 추가한다.
    			
				newTeeSpaceTabs.masterTabRefreshed = false;
        		localStorage.setItem("teeSpaceTabs", JSON.stringify(newTeeSpaceTabs));
    		}
    	}
    	
    	// winow가 unload될 때 teeSpaceTabs에서 자신을 삭제한다.
    	window.addEventListener('beforeunload', function() {
    		const teeSpaceTabs = JSON.parse(localStorage.getItem("teeSpaceTabs"));
			const newTeeSpaceTabs = { tabs: teeSpaceTabs.tabs.filter(function(tab){ return (tab !== conid) && (tab != null) }),
								      masterTabRefreshed: isMasterTab ? true : false }; // 새로고침될 때 자신이 masterTab인지 확인하는 flag.

    		if (newTeeSpaceTabs.tabs.length === 0)
    			localStorage.removeItem("teeSpaceTabs");
    		else { 
    			if (newTeeSpaceTabs.masterTabRefreshed === true) {
    				navigator.serviceWorker.controller.postMessage({ // masterTabRefreshed flag를 내린다.
    				  type: 'CLEAR_MASTER_TAB_REFRESHED',
    				});
    			}
    		
    			localStorage.setItem("teeSpaceTabs", JSON.stringify(newTeeSpaceTabs));
    		}
    	});
    	
    	// serviceWorker로 부터 CLEAR_MASTER_TAB_REFRESHED 메시지가 오면 flag를 내린다.
    	navigator.serviceWorker.addEventListener('message', event => {
    		if (event.data && event.data.type === "CLEAR_MASTER_TAB_REFRESHED") {
    			const teeSpaceTabs = JSON.parse(localStorage.getItem("teeSpaceTabs"));
    			const newTeeSpaceTabs = { tabs: teeSpaceTabs.tabs,
    								      masterTabRefreshed: false };
    			localStorage.setItem("teeSpaceTabs", JSON.stringify(newTeeSpaceTabs));
    		}
    	});
    });
    
    // 리스트의 첫 번째 요소가 masterTab이 된다.
	const isMasterTab = function() {
		if (localStorage.getItem("teeSpaceTabs") !== null) {
			const teeSpaceTabs = JSON.parse(localStorage.getItem("teeSpaceTabs"));
			return teeSpaceTabs.tabs[0] === conid ? true : false;
		}
	}
	
	// 로그아웃할 때 teeSpaceTabs에서 자신을 삭제한다.
	const clearTeeSpaceTabList = function() {
		if (localStorage.getItem("teeSpaceTabs") !== null) {
			const teeSpaceTabs = JSON.parse(localStorage.getItem("teeSpaceTabs"));
			const newTeeSpaceTabs = { tabs: teeSpaceTabs.tabs.filter(function(tab) { return (tab !== conid) && (tab != null) }),
									  masterTabRefreshed: teeSpaceTabs.masterTabRefreshed };
			
			if (newTeeSpaceTabs.tabs.length === 0)
    			localStorage.removeItem("teeSpaceTabs");
    		else
    			localStorage.setItem("teeSpaceTabs", JSON.stringify(newTeeSpaceTabs));
		}
	}
	
	const clear = function() {
		badgeSpaceId = null;
    	blockNotificationData = [];
    	document.title= defaultTitle;
	}
    
    // 최근 온 알림 중에 같은 알림이 있는지 체크, 브로드캐스트 채널로 넘어 온 메시지 중에 같은 알림이 있을 수 있다.
    function checkRecentNotifications(notiId) {
		if (recentNotifications.includes(notiId)) 
			return true;
		else { 
			recentNotifications.push(notiId);			
			if (recentNotifications.length > 5)
				recentNotifications.shift();
			return false;
		}
    }
    
    // 마스터 탭인지 확인하고 브로드 캐스트 채널 메시지 처리를 한다.
    const checkBroadcastMessage = function(webSocketMessage, eventType, broadcastChannelMessageCount) {
    	if (isMasterTab() === false) {
    		if (broadcastChannelMessageCount < 1) // broadcastChannelMessage가무한 루프 도는 것을 방지, broadcastMessage는 한 번만 전송되어야 한다.
    			broadcastChannel.postMessage({webSocketMessage, eventType, broadcastChannelMessageCount}); // 자신이 마스터 탭이 아니면 다른 탭으로 메시지 전송.
    		return true;
    	} else {
    		return false;
    	}
    }
    
    function Notification(webSocketMessage, eventType, broadcastChannelMessageCount = 0){
    	let spaceId = webSocketMessage.SPACE_ID;
        let channelType = webSocketMessage.CH_TYPE;
        let senderId = webSocketMessage.USER_ID;
        let main_chId = null, sub_chId = null;
        
        if ( checkBroadcastMessage(webSocketMessage, eventType, broadcastChannelMessageCount) ) 
        	return;
        
        if ( checkRecentNotifications(webSocketMessage.NOTI_ID) )
        	return;
        
        if( !TEESPACE_CHANNEL_TYPE[channelType] )  // 혹시 알림을 띄울 채널이 아닌 곳에서 호출했을 경우 예외 처리
        	return;
        
        updateNotificationConfiguration();       // 세션 스토리지에 있는 alset을 blockNotificationData에 가져옴
        
        if( channelType != TEESPACE_CHANNELS.TSCHEDULE.CHANNEL_TYPE && senderId === userManager.getLoginUserId() ) // 내가 보낸 알림 검사
            return;
        
        createTitleBadge(spaceId, channelType);    // 타이틀 뱃지 생성, 톡 알림만 뱃지 생성
        
        if( !checkNotificationEnabled( spaceId, channelType ) )
            return;

        if( isMobile() == false ) {
        	let senderName = userManager.getUserName(senderId);
        	let title = getNotiTitle(channelType, webSocketMessage.NOTI_TITLE);
            let message = getNotiMessage(channelType, eventType, webSocketMessage.NOTI_MSG, JSON.parse(webSocketMessage.NOTI_ETC), webSocketMessage.mentionContain);
            let targetUrl = getRoutingUrl(channelType, spaceId, webSocketMessage.NOTI_ETC);  // 메일 알림의 경우 NOTI_ETC에 '{folderId}/{mailId}'를 넣어서 줌
            
            workspaceManager.getWorkspacePhoto(spaceId).then((photo) => {
            	window.parent.postMessage({
          		  eventName: 'POST_DESKTOP_NOTI',
          		  params: {
          			  "senderName" : senderName,
          			  "title" : title,
          			  "message" : message,
          			  "photo" : photo,
          			  "targetUrl" : targetUrl 
          		  }
            	}, "*");
            	CloudSpaceDesktopNotification.notify(senderName, title + "\n" + message, targetUrl, photo, spaceId);
            });
        }
    }
    
    function getNotiTitle( channelType, noti_title) {
    	return ( channelType === TEESPACE_CHANNELS.TMAIL.CHANNEL_TYPE ||
    			channelType === TEESPACE_CHANNELS.TSCHEDULE.CHANNEL_TYPE) ? noti_title : zeroWidthSpace;
    }
    
//    const getMessageByteLength = function(string, byte, index, character){
//    	for(byte = index = 0; character = string.charCodeAt(index++); byte += character >> 7 ? 2 : 1);
//    	return byte;
//    }
//    
//    const sliceMessage = function(message) {
//    	let byte;
//    	for(byte = index = 0; string.charCodeAt(index) !== null; index++) {
//    		character = string.charCodeAt(index);
//    		byte += character >> 7 ? 2 : 1;
//			
//    		if (byte > 34)
//    			return message.subStr(0, index) + "...";
//    	}
//    	return message;
//    }
    
    const makeMessage = function(message) {
    	const MAX_BYTE = 26;
    	let byte, character, index;
    	
    	for(byte = index = 0; message.charCodeAt(index); index++) {
    		character = message.charCodeAt(index);
    		byte += character >> 7 ? 2 : 1;
			
    		if (byte > MAX_BYTE)
    			return message.substring(0, index) + "...";
    	}
    	return message;
    }
    
    function getNotiMessage(channelType, eventType, noti_msg, notiEtc, mention) {
    	// 나중에 i18n 작업 해야됨
    	let msg="";
    	
    	// 멘션 메시지일 때
    	if (channelType === TEESPACE_CHANNELS.TTALK.CHANNEL_TYPE && mention === true)
    		eventType = "ttalk_mention";
    	// 답장일 때
    	else if (channelType === TEESPACE_CHANNELS.TTALK.CHANNEL_TYPE && notiEtc.ATTACHMENT_LIST.length !== 0 && notiEtc.ATTACHMENT_LIST[0].ATTACHMENT_TYPE === "reply")
    		eventType = "ttalk_reply";
    	
    	if(channelType == TEESPACE_CHANNELS.TTALK.CHANNEL_TYPE && !includeNotifyConfiguration("all", CS_Alarm_Config.TTALK.SHOW_CONTENTS) ) {
    		// TTalk에서 메시지 내용 미리보기가 설정되어 있는 경우
    		if(eventType == "ttalk_text")  // 메시지 내용 표시
    			msg = noti_msg;
    		else if(eventType == "ttalk_emotion")  // 메시지 내용 표시
    			msg = noti_msg;
    		else if(eventType == "ttalk_mention") // 메시지 내용 표시
    			msg = noti_msg;
    		else if(eventType == "ttalk_reply")
    			msg = "⤷" + noti_msg;
    		else  // 메시지 제외
    			msg = TEESPACE_CHANNEL_TYPE[channelType][eventType];
    	}
    	else if(channelType ==  TEESPACE_CHANNELS.TMAIL.CHANNEL_TYPE)
    		// 메일의 경우 편지 내용
    		msg = noti_msg;
    	else // 그 외 나머지
    		msg = mention ? userManager.getUserName(userManager.getLoginUserId()) + TEESPACE_CHANNEL_TYPE[channelType][eventType] : 
    						TEESPACE_CHANNEL_TYPE[channelType][eventType];
    	
    	return makeMessage(msg);
    }
    
    function getRoutingUrl(channelType, spaceId, mailUrl) {
    	if( !channelType || !spaceId )
    		return;
    	
    	let url = null;
    	
    	if(channelType === "CHN0002") {
    		url = "/m/" + workspaceManager.getMySpaceUrl() + "/mail/" + mailUrl + "?desktop=";
    		// /m/{myspaceUrl}/mail/{folderId}/{mailId} 
    	}
    	else {
    		url = "/s/" + workspaceManager.getWorkspaceUrl(spaceId) + "/talk";
    		if(channelType !== "CHN0001") 
    			url += "?sub=" + TEESPACE_CHANNEL_TYPE[channelType].ROUTING_KEY + "&desktop=";
    		else
    			url += "?desktop=";
    	}
    	    	
    	return url;
    }
    function getIconUrl(spaceId, channelType) {
    	return ""; // 채널별로 icon 찾아서 주소 리턴좀
    }
    
    function createTitleBadge(spaceId, channelType) {
    	if( channelType != TEESPACE_CHANNELS.TTALK.CHANNEL_TYPE  // 토크 메시지가 아니면 배지 표지 안함
//    		|| !document.hidden    // 활성화 상태일 때 만 배지 표시 안함
    		|| document.hasFocus() ) // 포커스 활성화 시 배지 표시 안함    
    		return;
    	
    	badgeSpaceId = spaceId;
    	
        let defaultTitle = document.title;
        if( !defaultTitle.includes(titleBadgeIcon) )
        	document.title = defaultTitle.concat(titleBadgeIcon);
        ///// 파비콘이 아직 정의되지 않아 임시 아이콘 삽입 /////
    }
    
    function _removeTitleBadge(spaceId) {
    	if( !!badgeSpaceId && badgeSpaceId === spaceId ) {     // 마지막으로 뱃지를 생성한 알림을 읽었을 때
    		document.title = document.title.replace(titleBadgeIcon, "");
    		badgeSpaceId = null;
    	}
    }
    function checkNotificationEnabled(spaceId, channelType) {
    	// 데스크탑 알림을 띄울지 검사
    	// 노티를 띄울 때 true 반환
    	
    	return checkDesktopNotificationEnabled()     // (1) 데스크탑 알림 수신 설정이 되어 있는가?
    			&& checkNotificationPolicy(spaceId, channelType)               // (2) 데스크탑 알림 정책에 따라 표시해야 할 상황인가?
    			&& checkSpaceNotificationEnabled( spaceId )      // (3) 특정 스페이스의 알림 수신이 설정되어 있는가?
    			&& checkChannelTypeNotificationEnabled( channelType );  // (4) 특정 채널(앱)의 알림 수신이 설정되어 있는가?
    }
    function checkDesktopNotificationEnabled( ) {
    	// 데스크탑 노티 여부 검사
    	// 노티를 띄울 때 true 반환
        return  !includeNotifyConfiguration("all", CS_Alarm_Config.SYSTEM.DESKTOP_NOTIFICATION);
    }
    function checkNotificationPolicy(spaceId, channelType) {
        if( channelType === TEESPACE_CHANNELS.TMAIL.CHANNEL_TYPE  // 메일(CHN0006) 알림 항상 띄움 
//              || document.hidden       // 탭 비활성화 시 알림 항상 띄움
    		|| document.hasFocus() == false )    // 포커스 비활성화 시 알림 항상 띄움
        	return true;
        
       	let currentSpaceUrl = workspaceManager.getWorkspaceId(getRoomIdByUrl());
    	let mainApp = appManager.getMainApp();
    	
    	if( spaceId !== currentSpaceUrl     // 알림이 온 space가 현재 보고있는 id가 아닐 경우
    		|| mainApp !== TEESPACE_CHANNELS.TTALK.ROUTING_KEY ) // 현재 보고 있는 space에 온 알림이라도 메인앱이 talk이 아닌경우
    		return true;
    	return false;
    }
    function checkSpaceNotificationEnabled( spaceId ) {
    	// 특정 스페이스의 모든 알림을 껏는지 검사
    	// 당연히 eventType 은 'space_all'
    	// 노티를 띄울 때 true 반환
    	if( !spaceId ) {
    		console.log("checkSpaceNotificationEnabled : spaceId is NULL or undefined");
    		return;
    	}
    		
        return  !includeNotifyConfiguration(spaceId, CS_Alarm_Config.SYSTEM.ALL);
    }
    function checkChannelTypeNotificationEnabled( channelType ) {
    	// 전체 스페이스 대상으로 특정 채널(ttalk, mail, schedule 등등)의 알림 수신에 대해 검사
    	// System으로 들어오는 채널은 space_invite 이벤트임
    	// SpaceId는 당연히 'all'
    	// 노티를 띄울 때 true 반환
    	if( !channelType ){
    		console.log("checkChannelTypeNotificationEnabled : channelType is NULL or undefined");
    		return;
    	}
    	
    	let channelName = TEESPACE_CHANNEL_TYPE[channelType].NAME;
    	let eventType = channelName === "SYSTEM" ? CS_Alarm_Config[channelName].SYSTEM_INVITE : CS_Alarm_Config[channelName].ALL;
        // 해당 Event가 없다는 것은 알림을 사용하겠다는 의미가 된다.
        return !includeNotifyConfiguration("all", eventType);
    }

    function includeNotifyConfiguration( spaceId, eventType ){
    	// 특정 스페이스에서 특정 이벤트에 대한 알림 수신 거부 여부를 검사.
    	// spaceNotiConf.configEvent 에 특정 이벤트가 들어가 있다면 수신 거부 상태이라는 뜻.
    	// 수신 거부 일 때  true 반환
    	if( !spaceId ){
    		console.log("checkNotificationEnabled : spaceId is NULL or undefined");
    		return;
    	}
    	if( !eventType ) {
    		console.log("checkNotificationEnabled : eventType is NULL or undefined");
    		return;
    	}
    	
    	let spaceNotiConf = blockNotificationData[spaceId];
        if( !spaceNotiConf )
            return false;
        
        return spaceNotiConf.configEvent.some(function(event) {return event == eventType});        	        
    }

    function updateNotificationConfiguration(){

        blockNotificationData = [];
        let configurationArray = getUserAlarmConfig();

        if( configurationArray.length ===0 ){
            return blockNotificationData;
        }
        // console.warn(" configurationArray  ", configurationArray);

        configurationArray.forEach( function( config) {
            if( !blockNotificationData[config.WS_ID] ){
                blockNotificationData[config.WS_ID]={
                    configEvent:[]
                };
            }
            blockNotificationData[config.WS_ID].configEvent.push(config.CH_ALARM_CODE);
        });
        return blockNotificationData;
    }
    return{

        notify: function( webSocketMessage, routeParameter , eventType ){
            Notification(webSocketMessage, routeParameter, eventType);
        },
        updateConfig : function(){
            // 이 메소드는 alset의 key값으로 알림 설정이 변경 될때마다 호출 된다.
            return updateNotificationConfiguration();
        },
        checkConfig: function ( spaceId, eventType ){
        	// spaceId에 eventType 
            return !includeNotifyConfiguration(spaceId, blockEventName);
        },
        removeTitleBadge : function (spaceId) {
        	_removeTitleBadge(spaceId);
		},
		
		clear,

        broadcastChannel,
        
        initTeeSpaceTabList: function() {
        	teeSpaceTab();
        },
        
        clearTeeSpaceTabList: function() {
        	clearTeeSpaceTabList();
        },
    }

})();



const admin = (function() {
	return {
		replaceLocation(userGrade){
			if(userGrade === 'admin'){
				return location.replace(rootUrl+"admin/company_org");	
			}else if(userGrade === 'superadmin'){
				return location.replace(rootUrl+"admin/manual?mode=list&cate=all&subcate=all&view=ss30&page=1");
			}else{
				return userManager.logout();
			}
		},
		setTitlebar: function(title, description, button) {
//			if (title) {
//				$$$('adminTitleText').setProperties({ visible: 'visible', text: 'title' });
//			} else {
//				$('top-textview#adminTitleText').remove();
//			}
//			if (description) {
//				$$$('adminDescText').setProperties({ visible: 'visible', text: 'description' });
//			} else {
//				$('top-textview#adminDescText').remove();
//			}
//			if (!title && !description) {
//				$('top-linearlayout#adminTitlebarLayout').remove();
//			}
		},
		setGnbColor: function() {
			// 임시 방편
			var type = parseUrl().type;
			$('.cmp-gnb-button').removeClass('selected');
			if(type.startsWith('company_')){
				$('#adminGnbButtonCompany.cmp-gnb-button').addClass('selected');
			}else if(type.startsWith('service_')){
				$('#adminGnbButtonService.cmp-gnb-button').addClass('selected');
			}else if(type.startsWith('stat_')){
				$('#adminGnbButtonStat.cmp-gnb-button').addClass('selected');
			}
			
			//			var buttonList = Top.Dom.select('.cmp-gnb-button');
			//			for(var i=0;i<buttonList.length;i++){
			//				if(buttonList[i].getValue().split('/admin/')[1].split('?')[0] === parseUrl().type){
			//					$('div#' + buttonList[i].id).addClass('selected');
			//					break;
			//				}
			//			}
		},
		setLnbColor: function() {
			// 임시 방편
			var type = parseUrl().type;
			$('.cmp-lnb-button>button').removeClass('selected');
			
//			var list = Top.Dom.select('.cmp-lnb-button');
//			for(var i=0;i<list.length; i++){
//				list[i].getProperties('customData').buttonId
//			}
			
			if(type === 'company_org'){
				$('#lnbButtonCompanyOrg.cmp-lnb-button>button').addClass('selected');
			}else if(type === 'company_guest'){
				$('#lnbButtonCompanyGuest.cmp-lnb-button>button').addClass('selected');
			}else if(type === 'company_info'){
				$('#lnbButtonCompanyInfo.cmp-lnb-button>button').addClass('selected');	
			}else if(type === 'manual'){
				$('#lnbButtonManual.cmp-lnb-button>button').addClass('selected');	
			}else if(type === 'faq'){
				$('#lnbButtonFaq.cmp-lnb-button>button').addClass('selected');	
			}else if(type === 'oneonone'){
				$('#lnbButtonOneonone.cmp-lnb-button>button').addClass('selected');	
			}else if(type === 'qna'){
				$('#lnbButtonQna.cmp-lnb-button>button').addClass('selected');				
			}else if(type === 'service_announcement'){
				$('#lnbButtonServiceAnnouncement.cmp-lnb-button>button').addClass('selected');
			}else if(type === 'update_announcement'){
				$('#lnbButtonUpdateAnnouncement.cmp-lnb-button>button').addClass('selected');
			}else if(type === 'plan'){
				$('#lnbButtonPlan.cmp-lnb-button>button').addClass('selected');
			}else if(type === 'stat_daily'){
				$('#lnbButtonStatDaily.cmp-lnb-button>button').addClass('selected');
			}else if(type === 'stat_member'){
				$('#lnbButtonStatMember.cmp-lnb-button>button').addClass('selected');
			}else if(type === 'stat_cloudspace'){
				$('#lnbButtonStatCloudspace.cmp-lnb-button>button').addClass('selected');	
			}
			
			//			// adminLnb 안에 있는 cmp-lnb-button 모든 원소 찾고
			//			// 그 원소안에서 value에서 type을 찾고, 그 중에 현재 url type과 동일한 버튼 찾고
			//			// 그 버튼에 add class
			//			var buttonList = Top.Dom.select('#adminLnb .cmp-lnb-button');
			//			for (var i = 0; i < buttonList.length; i++) {
			//				if (parseUrl(buttonList[i].getValue()) === parseUrl().type) {
			//					$('button#' + buttonList[i].id).addClass('selected');
			//					break;
			//				}
			//			}
		},
		route: function(path) {
			var hash = '';
			Top.App.routeTo(hash);
			return;
		},

		routeLayout: function(path) {
			// 모바일에서 lnb 메뉴만 표시하기 위해 메인 페이지만 라우팅할때
			if (!path && (parseUrl().module === 'admin')) {
				Top.Dom.selectById('MainTeeSpacePage').src('adminMainLayout.html' + verCsp());
				return;
			}
			var contentsRouting = function(type) {
				// adminContentsLayout 레이아웃 라우팅
				const u = parseUrl();
				var module = u.module;
				var type = u.type;
				var mode = u.mode;
				var html = ''; // layout html file name
				var callback;
				// **************************************************
				if (['list', 'read', 'write'].includes(mode)) {
					// 원래 operator에 있던 게시판들
					if (mode === 'list') {
						$$$('adminContentsLayout').src('commonListLayout.html' + verCsp(), function() {
							Top.Controller.get('commonListLayoutLogic')[module + '_' + type]();
						});
					} else if (mode === 'read') {
						$$$('adminContentsLayout').src('commonReadLayout.html' + verCsp(), function() {
							Top.Controller.get('commonReadLayoutLogic')[module + '_' + type]();
						});
					} else if (mode === 'write') {
						$$$('adminContentsLayout').src('commonWriteLayout.html' + verCsp(), function() {
							Top.Controller.get('commonWriteLayoutLogic')[module + '_' + type]();
						});
					}
				} else if (type === 'auth') {
					$$$('adminContentsLayout').src('adminAuthLayout.html' + verCsp());
				} else if (type === 'company_org') {
					$$$('adminContentsLayout').src('adminOrgManageLayout.html' + verCsp());
					admin.setTitlebar('조직 관리', '조직 및 구성원을 추가하거나 정보를 수정할 수 있습니다.');
				} else if (type === 'company_guest') {
					$$$('adminContentsLayout').src('adminGuestManageLayout.html' + verCsp());
					admin.setTitlebar('게스트 관리', '외부의 게스트를 추가하거나 정보를 수정할 수 있습니다.');
				} else if (type === 'company_info') {
					$$$('adminContentsLayout').src('adminCompanyInfoManageLayout.html' + verCsp());
					admin.setTitlebar('회사 정보 관리', '회사 정보를 추가하거나 수정할 수 있습니다.');
				} else if (type === 'security') {
					//                	$$$('adminContentsLayout').src('라우팅할 파일.html' + verCsp()); // 해당 페이지 담당자가 작성할 것
				} else if (type === 'log') {
					//                	$$$('adminContentsLayout').src('라우팅할 파일.html' + verCsp()); // 해당 페이지 담당자가 작성할 것
				} else if (type === 'stat_daily') {
					if(!window.mntrFirstLoaded) {
						setTimeout(function() {
							window.mntrFirstLoaded = true;
							Top.App.routeTo('/admin/stat_daily&' + new Date().getTime());
							Top.App.routeTo('/admin/stat_daily');
						}, 100);
					}
					Top.Controller.get('mntrMainLayoutLogic').changeType("DAU");
					$$$('adminContentsLayout').src('mntrMainLayout.html' + verCsp());
					admin.setTitlebar('1일 현황', '');
				} else if (type === 'stat_member') {
					Top.Controller.get('mntrMainLayoutLogic').changeType("memberAnalysis");
					$$$('adminContentsLayout').src('mntrMainLayout.html' + verCsp());	
					admin.setTitlebar('멤버 분석', '');
				} else if (type === 'stat_cloudspace') {
					Top.Controller.get('mntrMainLayoutLogic').changeType("cloudSpaceAnalysis");
					$$$('adminContentsLayout').src('mntrMainLayout.html' + verCsp());	
					admin.setTitlebar('CloudSpace 통계', '');
				} else {
					return;
				}
			};

			var afterAdminPageRouting = function(path) {
				// 현재 gnb에 해당하는 lnb 그리기 + 현재 gnb에 해당하는 contents 라우팅
				let cmp = Top.Controller.get('components');
				let main = Top.Controller.get('adminMainLayoutLogic');
				var u = parseUrl();
				var type = u.type;
				var mode = u.mode;

				// url의 type에 해당하는 gnb 버튼, lnb id, lnb 버튼 찾기
				// url -> type -> gnb -> lnb 순서로 찾아나감
				var breakFlag = false;
				for (var i = 0; i < main.data._gnb.list.length; i++) {
					var tmpGnb = main.data._gnb.list[i];
					var lnbId = tmpGnb.lnb;
					var tmpLnb = main.data[lnbId];
					if (!tmpLnb.list || tmpLnb.list.length === 0) {
						break;
					}
					for (var k = 0; k < tmpLnb.list.length; k++) {
						if (tmpLnb.list[k].type === type) {
							breakFlag = true;
							break;
						}
					}
					if (breakFlag) {
						break;
					}
				}

				var currentGnbButton = tmpGnb;
				console.info('currentGnbButton: ', currentGnbButton);

				var lnbId = currentGnbButton.lnb;
				console.info('lnbId: ', lnbId);

				$('top-linearlayout#adminLnb') && $('top-linearlayout#adminLnb').remove(); // 기존 lnb 삭제
				cmp.simpleLnb({
					id: 'adminLnb',
					list: main.data[lnbId].list,
					onClick: function(event, widget) {
						console.info(widget.getValue());
						Top.App.routeTo(widget.getValue());
					},
					targetId: 'adminLeftLayout',
					type: 2,
					customData: {type: main.data[lnbId].list.map(elem => elem.type)}
				});
				contentsRouting(type);
			};

            if ($$$('adminMainLayout')) {
                // admin에서 admin으로 페이지 이동한 경우 이미 adminMainLayout은 라우팅된 상태
                afterAdminPageRouting(path);
            } else {
                // admin이 아닌 페이지에서 admin으로 페이지 이동한 경우 adminMainLayout.html부터 라우팅해야함
                $$$('MainTeeSpacePage').src('adminMainLayout.html' + verCsp(), function () {
                    afterAdminPageRouting(path);
                });
            }
        },
    }
})();



/* 
 * monitoring section
 */



// 20-01-21 임시
window.mntrFirstLoaded = false;
self.addEventListener('activate', event => {
  event.waitUntil(clients.claim());
});

self.addEventListener('push', function (event) {
    console.log('notification received');
});

self.addEventListener('message', function (event) {
	console.log('service worker message received');
	if (event.data && event.data.type === "CLEAR_MASTER_TAB_REFRESHED") {
		setTimeout(function() {
			const clientId = event.source.id;
			clients.matchAll().then(function(clients) {
				clients[0].postMessage({type: "CLEAR_MASTER_TAB_REFRESHED"});
			});
		}, 3000);
	}
});

self.addEventListener('notificationclick', function (event) {
    console.log('notification was clicked');
    const urlToOpen = new URL(event.notification.data.urlToOpen, self.location.origin).href;
    const miniTalkSpc = event.notification.data.spc;
    
    const promiseChain = clients.matchAll({
        type: 'window',
        includeUncontrolled: true
    })
    .then((windowClients) => {
    	let matchingClient = null;
        if (windowClients.length) {
        	for (let i = 0; i < windowClients.length; i++) {
        		if (windowClients[i].url.indexOf(miniTalkSpc) !== -1) {
        			windowClients[i].focus();
        			return;
        		}
        	}
        	matchingClient = windowClients[windowClients.length - 1];
        	if (matchingClient) {
        		matchingClient.navigate(urlToOpen);    			
        		matchingClient.focus();
        	}
        } else {
            return clients.openWindow(urlToOpen);
        }
    });
    event.waitUntil(promiseChain);
});
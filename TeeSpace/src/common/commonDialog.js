(function(exports){
	
	/* overlay */
	const overlay = function(id){
		let overlay = makeDiv(`${id}Overlay`, `dialog-overlay`); 
		overlay.addEventListener('click', function(){
			if($('#top-dialog-root_pwdConfirmDialog .top-dialog-title').html() === '비밀번호 확인'){}
			else if($('div#profileDialog div#gpf').length != 0){}
			else{
				$(`#${id}`).remove();						
				overlay.remove();
			}
			
		});
	    document.querySelector('body').appendChild(overlay);
	};
	
	
	/* profile(dropdown) */
	const dropdown = function(){
		let userInfo = userManager.getLoginUserInfo();
		let userPhoto = userManager.getLoginUserPhoto();
		let backPhoto = userManager.getLoginUserPhoto(userInfo.userId, "back", userInfo.backPhoto);
		var userNick = '';
		if(userManager.isBtoc()){
			//userNick = userManager.getUserInfo(userManager.getLoginUserId()).USER_NICK != null ? userManager.getUserInfo(userManager.getLoginUserId()).USER_NICK : userManager.getUserInfo(userManager.getLoginUserId()).USER_LOGIN_ID;
			userNick = userManager.getLoginUserInfo().userNick != null ? userManager.getLoginUserInfo().userNick : userManager.getLoginUserInfo().USER_LOGIN_ID;
		}else{
			userNick = userInfo.userName;
		}
		
	
		//상단
		let upperWrapper = htmlToElement(			
								`<div class="prf-wrapper" style="background:#0B1D41">
									<img class="prf-user_image" src="${userPhoto}"></img>
									<div class= "prf-stat">
										<i class="icon-circle" style="color:#16AC66;font-size:7px;"></i><span class="prf-text_1" id="profileUserNick">${userNick}</span>
									</div>
									<span class="prf-text_2">${userInfo.USER_LOGIN_ID}</span>
									<span class="prf-edit" id="profileEdit">프로필 편집</span>
									<div style="display:inline-block; border-right:1px solid #cacdff; height:0.81rem; vertical-align:middle"></div>
									<span class="prf-edit" id="profilePwdEdit">비밀번호 변경</span>
									<span class="prf-logout">로그아웃</span>
								</div>`
							);
		
		//하단
		let btnWrapper = htmlToElement(
							`<div class="prf-div"> 
								<span class="prf-button" id="profileAddAccount">계정 추가</span>
								<span class="prf-button">언어 설정: 한국어</span>
								<div class="prf-bottom" > 
									<button class="prf-spt text" id="profileSetting">설정</button><span>·</span><button class="prf-spt text" id="profileSupport">고객지원</button>
								</div>
								<button class="prf-spt text" style="display: none;">튜토리얼</button>
							</div>`
						  );
		
		//다이얼로그
		let profileDialog = makeDiv("profileDialog", "prf-dialog");		
	    profileDialog.appendChild(upperWrapper);
	    profileDialog.appendChild(btnWrapper);
	       
	    
	  //상태 툴팁 이벤트 설정
	    profileDialog.querySelector('div.prf-stat i').addEventListener('mouseover', function (e) {
			if($('div.prf-stat i').css('color') === "rgb(22, 172, 102)"){
				TeeTooltip.open('온라인', {
					x: e.clientX, 
					y: e.clientY,
				});
				}else{
					TeeTooltip.open('오프라인', {
						x: e.clientX, 
						y: e.clientY,
					});
				}
			});
	    profileDialog.querySelector('div.prf-stat i').addEventListener('mouseout', function (e) {
				TeeTooltip.close();	
		});
	    //이벤트 바인딩
	    
	    profileDialog.querySelector('.prf-logout').addEventListener('click', function (){
			userManager.logout();
			$('.prf-dialog').remove();
			$("#profileDialogOverlay").remove();
	    });
	    profileDialog.querySelector('#profileEdit').addEventListener('click', function (){
 	    	//프로필 편집 이벤트
    	$.ajax({
            url: _workspace.url + "Users/BothProfile",
            //Service Object
            type: 'GET',
            dataType: 'json',
            crossDomain: true,
            contentType: 'application/json',
            xhrFields: {
              withCredentials: true
            },
            data: JSON.stringify({
              "dto": {
                "USER_ID": userManager.getLoginUserId()
              }
            //  ttt:  new Date().getTime()
            }),
            success: function success(result) {
            	var profilePhoto;
            	var backPhoto;
            	var nCode;
            	if(!result.dto.THUMB_PHOTO){
            		profilePhoto = userManager.getUserDefaultPhotoURL(userManager.getLoginUserId());
            	//}else{profilePhoto = result.dto.THUMB_PHOTO}
            	}else{profilePhoto = userManager.getUserPhoto(userManager.getLoginUserId(),'medium', result.dto.THUMB_PHOTO)}
            	if(!result.dto.BACKGROUND_THUMB){
            		backPhoto = getDefaultBackground(userManager.getLoginUserId())
            	}else{
            		//backPhoto =  result.dto.BACKGROUND_THUMB
            		backPhoto = userManager.getUserPhoto(userManager.getLoginUserId(),'back', result.dto.BACKGROUND_THUMB)
            	}
            	if(!result.dto.NATIONAL_CODE){
            		nCode  = "+82"
            	}else{
            		nCode = result.dto.NATIONAL_CODE
            	}
            	//var poj;
            	//var poj = result.dto.USER_JOB != null ? '·'+result.dto.USER_JOB : (result.dto.USER_POSITION != null ? '·'+result.dto.USER_POSITION : "");
            	  /////
          		var NewPoj = result.dto.USER_JOB != null ? result.dto.USER_JOB : (result.dto.USER_POSITION != null ? result.dto.USER_POSITION : "");
		    	var poj = result.dto.USER_JOB != null ? '·'+result.dto.USER_JOB : (result.dto.USER_POSITION != null ? '·'+result.dto.USER_POSITION : "");
		    	var NewJop="";
		    	var endJop = "";
		    	for(let jop= 1; jop <  NewPoj.split(', ').length-1 ; jop++){
		    		NewJop += result.dto.COMPANY_CODE+"·"+result.dto.ORG_NAME.split(', ')[jop]+"·"+poj.split(', ')[jop]+",<br>" 
		    	    endJop =  result.dto.COMPANY_CODE+"·"+result.dto.ORG_NAME.split(', ')[NewPoj.split(', ').length-1]+"·"+poj.split(', ')[NewPoj.split(', ').length-1] 
		    	}
				var inputOffice;    
		    	
				//소속없을 경우 분기 처리
			    if(NewPoj === "" || poj === ""){
			    	inputOffice = "—";
			    }else{
			     	if(NewPoj.split(', ').length === 1){//only one
			    		inputOffice = result.dto.COMPANY_CODE+"·"+result.dto.ORG_NAME+poj
			    	}else if(NewPoj.split(', ').length === 2){//two job
			    		inputOffice = result.dto.COMPANY_CODE+"·"+result.dto.ORG_NAME.split(', ')[0]+poj.split(', ')[0]+",<br>&emsp;&nbsp;&nbsp;"+result.dto.COMPANY_CODE+"·"+result.dto.ORG_NAME.split(', ')[1]+"·"+poj.split(', ')[1]
			    	}else if(NewPoj.split(', ').length === 0) {//null
			    		inputOffice = result.dto.COMPANY_CODE+"·"+result.dto.ORG_NAME+poj
			    	}else{//three job
			    		inputOffice = result.dto.COMPANY_CODE+"·"+result.dto.ORG_NAME.split(', ')[0]+poj.split(', ')[0]+",<br>&emsp;&nbsp;&nbsp;"+NewJop +endJop
			    	}
			    } 	
		    	
		   
          	
            
		
            	result.dto.USER_COM_NUM  = result.dto.USER_COM_NUM != null ? result.dto.USER_COM_NUM : "";
            	result.dto.USER_PHONE  = result.dto.USER_PHONE != null ? result.dto.USER_PHONE : "";
                let upperProfileWrapper = htmlToElement(    
                        `<div id = "gpf">
                            <div class = "top-gpf" style = "background-image : url(${backPhoto}); background-repeat : no-repeat; background-size : cover; background-position: center;">
                                <div class = "top-dim" style = "background-color: rgba(0,0,0,0.6);">
                                	<div class ="back-div">
                                    	<button id = "backgroundBtn" class ="gpf_background"><img id = "backgroundBtn2" src="res/profile/photo_white.svg"></button>
                                	</div>
                                	<div class = "photo-div">
                                		<button id = "imageBtn" class ="gpf_photo"><img id = "imageBtn2" src="${profilePhoto}"></button>
                                    	<button id = "cameraImage" class ="gpf_camera"><img src="res/profile/camera.svg"></button>
                                    	<img  class="gpf_icon_b2b" src="res/profile/Profile_IDcard.svg"></img>                                
                                	</div>
                                	<div class = "beChanged-div">
                                		<div class = "name-div">
                                    		<i class="icon-circle" style="color:#16AC66;font-size:0.44rem;"></i><span class="gpf_nameView">${result.dto.USER_NAME}</span>
                                		</div>
                                		<div class = "id-div">
                                    		<span class = "gpf_id">(${result.dto.USER_LOGIN_ID})</span>
                                		</div>
                                		<div class = "office-div">
                                    		<img class="gpf_office_icon" src="res/profile/office_white.svg"></img>
                                    		<span class = "gpf_office_text">${inputOffice}</span>
                                   
                                		</div>
                                 		<div class = "comNum-div">
                                    		<img class="gpf_comNum_icon" src="res/profile/call_white.svg"></img>
                                    		<span class = "gpf_comNum_nc">${nCode}</span><input class = "gpf_comNum_text" value ="${result.dto.USER_COM_NUM}"></input>
											<button id = "phoneValidIcon" class = "icon-chart_error" style = "visibility : hidden">
                                		</div>
                                		<div class = "phone-div">
                                    		<img class="gpf_phone_icon" src="res/profile/phone_white.svg"></img>
                                    		<span class = "gpf_phone_nc">${nCode}</span><input class = "gpf_phone_text" value ="${result.dto.USER_PHONE}"></input>
											<button id = "mobileValidIcon" class = "icon-chart_error" style = "visibility : hidden">
                                		</div>
                                		<div class = "mail-div"> 
                                    		<img class="gpf_mail_icon" src="res/profile/mail_white.svg"></img>
                                    		<span class = "gpf_mail_text">${result.dto.USER_EMAIL}</span>
                                		</div>
                                	</div>
                                </div>
                            </div>
                            <div class ="bot-gpf">
                                <button id = "saveBtn" class = "solid">저장</button>
                                <button id = "canceBtn" class = "outlined">취소</button>
                            </div>
                        </div>
                        `
                    );
           
                
                upperProfileWrapper.querySelector('div.comNum-div button').addEventListener('click', function (e) {
                	const upperPopover1 = htmlToElement(`
                			<div class ="phone_popover1">
                				<span class = "phone_popover2">숫자만 입력해 주세요.</span>
                			</div>
                	`)
                	
                	if($('span.phone_popover2').length === 1){
                		$('div.phone_popover1').remove()
                	}else{
                		upperProfileWrapper.querySelector('div.comNum-div button').after(upperPopover1)
                	}
                	
                });
                upperProfileWrapper.querySelector('div.phone-div button').addEventListener('click', function (e) {
                	const upperPopover2 = htmlToElement(`
                			<div class ="mobile_popover1">
                				<span class = "mobile_popover2">숫자만 입력해 주세요.</span>
                			</div>
                	`)
                	
                	if($('span.mobile_popover2').length === 1){
                		$('div.mobile_popover1').remove()
                	}else{
                		upperProfileWrapper.querySelector('div.phone-div button').after(upperPopover2)
                	}
                	
                });
              //상태 툴팁 이벤트 설정
                upperProfileWrapper.querySelector('div.name-div i').addEventListener('mouseover', function (e) {
        			if($('div.name-div i').css('color') === "rgb(22, 172, 102)"){
        				TeeTooltip.open('온라인', {
        					x: e.clientX, 
        					y: e.clientY,
        				});
        				}else{
        					TeeTooltip.open('오프라인', {
        						x: e.clientX, 
        						y: e.clientY,
        					});
        				}
        			});
                upperProfileWrapper.querySelector('div.name-div i').addEventListener('mouseout', function (e) {
        				TeeTooltip.close();	
        		});
                
                
                
                upperWrapper.replaceWith(upperProfileWrapper);
                btnWrapper.remove();
                
                
                //////////////////////////////////////////b2c 개발///////////////////////////////////////////
                if(userManager.isBtoc()){
                	
                	var userNick = result.dto.USER_NICK != null ? result.dto.USER_NICK : result.dto.USER_LOGIN_ID;
                	
                	let B2CProfileWrapper = htmlToElement(
                      	 	`<div class = "change-div">
                      	 		<div class = "b2c-nick-div">
                      	 			<input class = "b2c_nick_edit"  maxlength="20" value ="${userNick}"><span id = "nick_length"></span></input>
                      	 			<button id = "nickNullIcon" class = "icon-chart_error" style = "visibility : hidden">
                      	 		</div>
                      	 		<div class = "b2c-id-div">
                      	 			<span class = "b2c_id">(${result.dto.USER_LOGIN_ID})</span>
                      	 		</div>
                      	 		<div class = "b2c-mobile-div">
                      	 			<img class="b2c_mobile_icon" src="res/profile/phone_white.svg"></img>
                      	 			<span class = "b2c_national_code">${nCode}</span>
                                    <span class = "b2c_mobile_text">${result.dto.USER_PHONE}</span>
									<button class = "b2c_mobile_button">변경</button>
                      	 		</div>
                      	 		<div class = "b2c-mail-div">
                					<img class="b2c_mail_icon" src="res/profile/mail_white.svg"></img>
                                	<span class = "b2c_mail_text">${result.dto.USER_EMAIL}</span>
                      	 		</div>
                      	 	</div>`
                      );   
                	//닉네임 없을 경우 팝오버
                	B2CProfileWrapper.querySelector('div.b2c-nick-div button').addEventListener('click', function (e) {
                       	const upperPopover3 = htmlToElement(`
                       			<div class ="nick_popover">
                       				<span class = "nick_popover">별명을 입력해 주세요.</span>
                       			</div>
                       	`)
                       	
                       	if($('span.nick_popover').length === 1){
                       		$('div.nick_popover').remove()
                       		$('div.b2c-nick-div').css({'margin-left':'2rem'})
                       	}else{
                       		B2CProfileWrapper.querySelector('div.b2c-nick-div button').after(upperPopover3)
                       		$('div.b2c-nick-div').css({'margin-left':'8rem'})
                       		$('div.nick_popover').css({'left':'-5.5rem'})
                       	}
                       	
                       });
                	
                	B2CProfileWrapper.querySelector('div.b2c-nick-div input.b2c_nick_edit').addEventListener('blur', function (e) {
                		if(!$('input.b2c_nick_edit').val() || $('input.b2c_nick_edit').val().length === 0 ){
                			$('button#nickNullIcon').css({"visibility":"visible"})
                    		$('button#nickNullIcon').click()

                		}else{
                			$('button#nickNullIcon').css({"visibility":"hidden"})
                			$('div.b2c-nick-div').css({'margin-left':'2rem'})
                			$('div.nick_popover').remove()
                		}
                	});
                	//별명 글자수 세는 함수
                	 $(function () {
		    		      $('.b2c_nick_edit').keyup(function (e) {
		    		        var nickLength = $(this).val();
//		    		   
		    		       
		    		        $('#nick_length').html(nickLength.length + '/20');
		    		        $('#nick_length').css({
		    		          "display": "inline",
		    		          "font-size": "0.69rem",
		    		          "position":"relative",
		    		          "right":"2rem",
		    		          "color": "#FFFFFF",
		    		          "opacity": "0.75"
		    		        });
		    		      });
		    		      $('.b2c_nick_edit').keyup();
		    		    });
                	 
                	 //b2c 모바일 변경 버튼 이벤트 
                	 B2CProfileWrapper.querySelector('button.b2c_mobile_button').addEventListener('click', function (e) {
                		 tds('pwdConfirmDialog').open()
                 	 });
                 	 
                	 
                	 
                	 
                	 
                	 
                	 $('.top-gpf').height('20rem')
                	 $('.top-dim').height('20rem')
                	 $('div.bot-gpf').addClass('bot-gpf_b2c')
                	 $('.gpf_icon_b2b').remove()
                	$('.beChanged-div').replaceWith(B2CProfileWrapper);	
                	 //프로필 편집 시 별명 란에 포커스
                	$('input.b2c_nick_edit').focus();
                	
                }
              
                //////////////////////////////////////////b2c 종료/////////////////////////////////////////// 
                
                
        	    //배경화면 변경시, 후버 설정
        		//$('#backgroundBtn2 i').attr('id',"da")
        		$('button#backgroundBtn').hover(function(){
        			$('button#backgroundBtn').css({'background-color' : 'rgba(90,95,255,0.8)'})
        			},function(){
        				$('button#backgroundBtn').css({'background-color' : 'rgba(0,0,0,1)'})
        			})
        		//프로필화면 변경시, 후버 설정
        		$('button.gpf_camera img').attr('id',"b2bHtmlCamera")
        		$('button.gpf_camera').hover(function(){
        			$('button.gpf_camera').css({'background-color' : 'rgba(90,95,255,0.8)'})
        			},function(){
        				$('button.gpf_camera').css({'background-color' : 'rgba(0,0,0,1)'})
        			})
        			
        	    
                
                
                
                
                
                
                
                
                if(NewPoj.split(', ').length === 1){//only one
		    	}else if(NewPoj.split(', ').length === 2){//two job
		    	}else if(NewPoj.split(', ').length === 0) {//null
		    	}else{//three job
		    		$('div.office-div').css({'height':'auto'})
		    		$('span.gpf_office_text').css({'max-height':'2.2rem'})
		    		$('span.gpf_office_text').attr({'title':inputOffice.replace(/<br>/g,"").replace(/&emsp;&nbsp;&nbsp;/g,"")})
		    	}
                
              
                $('div.top-gpf').on('click', function (e) {
      		        btobPhotoDrop(e, "b2bHtmlCamera");
      		        btobBackgroundDrop(e,"backgroundBtn");
      		    });
              //백그라운드 이미지 이벤트
                function btobBackgroundDrop(e, clickID) {
              	  if (e.toElement != undefined) {
              	    if (e.toElement.id == clickID  || e.toElement.id === "backgroundBtn2") {
              	    	BtobBackgroundList = '<top-linearlayout id="pf_b2b_background" class="position-pf_b2b_background linear-child-vertical" layout-width="120px" layout-height="auto" layout-horizontal-alignment="center" padding="" margin="" horizontal-alignment="center" vertical-alignment="middle" visible="visible" border-color="rgba(205,205,207,1.0) rgba(205,205,207,1.0) rgba(205,205,207,1.0) rgba(205,205,207,1.0)" border-width="1px 1px 1px 1px" horizontal-scroll="false" vertical-scroll="false" orientation="vertical" position="false" title="" layout-tab-disabled="false">' + '<div id="pf_b2b_background" class="top-linearlayout-root">' + '<top-button id="pf_b2b_background_pc" class="noti_drop linear-child-vertical" layout-width="match_parent" layout-height="28px" padding="0px 0px 1px 10px" horizontal-alignment="left" border-color="rgba(205,205,207,1.0) rgba(205,205,207,1.0) rgba(205,205,207,1.0) rgba(205,205,207,1.0)" border-width="0px 0px 1px 0px" tab-index="0" text="배경 변경" text-color="rgba(51,51,51,1.0)" text-decoration="" icon-position="left" title="" layout-tab-disabled="false" actual-height="29">' + '<button id="pf_b2b_background_pc" class="top-button-root" type="button" tabindex="0">' + '<label class="top-button-text">배경 변경</label>' + '</button>' + '</top-button>' + 
              	      '<top-button id="pf_b2b_background_delete" class="noti_drop linear-child-vertical" layout-width="match_parent" layout-height="28px" padding="0px 0px 1px 10px" horizontal-alignment="left" border-color="rgba(205,205,207,1.0) rgba(205,205,207,1.0) rgba(205,205,207,1.0) rgba(205,205,207,1.0)" border-width="0px 0px 1px 0px" tab-index="0" text="기본 이미지로 변경" text-color="rgba(51,51,51,1.0)" text-decoration="" icon-position="left" title="" layout-tab-disabled="false" actual-height="29">' + '<button id="pf_b2b_background_delete" class="top-button-root" type="button" tabindex="0">' + '<label class="top-button-text">기본 이미지로 변경</label>' + '</button>' + '</top-button>' + '</div>' + '</top-linearlayout>';

              	      if ($('top-linearlayout#pf_b2b_background').length == 0) {
              	    	BtobBackgroundList = $(BtobBackgroundList);
              	    	BtobBackgroundList.find("button#pf_b2b_background_pc").on('click', Top.Controller.get('spaceBtobMyProfileEditLayoutLogic').pf_back_choosefileBtob); //pc
              	    	BtobBackgroundList.find("button#pf_b2b_background_delete").on('click', Top.Controller.get('spaceBtobMyProfileEditLayoutLogic').pf_back_delete_backgroundBtob); //삭제

              	        $('div.back-div').append(BtobBackgroundList);

              	        $('.position-pf_b2b_background').css({
              	          'top': '1.5rem',
              	          'left': '1.5rem'
              	        });
              	      }
              	    } else {
              	      if ($('top-linearlayout#pf_b2b_background').length != 0) {
              	        $('top-linearlayout#pf_b2b_background').remove();
              	      }
              	    }

              	    ;
              	  }

              	  ;
              	}
                
                //프로필 이미지 이벤트
                function btobPhotoDrop(e, clickID) {
              	  if (e.toElement != undefined) {
              	    if (e.toElement.id == clickID || e.toElement.id === "cameraImage" ) {
              	    	BtobImageList = '<top-linearlayout id="pf_b2b_image" class="position-pf_b2b_image linear-child-vertical" layout-width="120px" layout-height="auto" layout-horizontal-alignment="center" padding="" margin="" horizontal-alignment="center" vertical-alignment="middle" visible="visible" border-color="rgba(205,205,207,1.0) rgba(205,205,207,1.0) rgba(205,205,207,1.0) rgba(205,205,207,1.0)" border-width="1px 1px 1px 1px" horizontal-scroll="false" vertical-scroll="false" orientation="vertical" position="false" title="" layout-tab-disabled="false">' + '<div id="pf_b2b_image" class="top-linearlayout-root">' + '<top-button id="pf_b2b_image_pc" class="noti_drop linear-child-vertical" layout-width="match_parent" layout-height="28px" padding="0px 0px 1px 10px" horizontal-alignment="left" border-color="rgba(205,205,207,1.0) rgba(205,205,207,1.0) rgba(205,205,207,1.0) rgba(205,205,207,1.0)" border-width="0px 0px 1px 0px" tab-index="0" text="PC" text-color="rgba(51,51,51,1.0)" text-decoration="" icon-position="left" title="" layout-tab-disabled="false" actual-height="29">' + '<button id="pf_b2b_image_pc" class="top-button-root" type="button" tabindex="0">' + '<label class="top-button-text">프로필 사진 변경</label>' + '</button>' + '</top-button>' + 
              	      '<top-button id="pf_b2b_image_delete" class="noti_drop linear-child-vertical" layout-width="match_parent" layout-height="28px" padding="0px 0px 1px 10px" horizontal-alignment="left" border-color="rgba(205,205,207,1.0) rgba(205,205,207,1.0) rgba(205,205,207,1.0) rgba(205,205,207,1.0)" border-width="0px 0px 1px 0px" tab-index="0" text="기본 이미지로 변경" text-color="rgba(51,51,51,1.0)" text-decoration="" icon-position="left" title="" layout-tab-disabled="false" actual-height="29">' + '<button id="pf_b2b_image_delete" class="top-button-root" type="button" tabindex="0">' + '<label class="top-button-text">기본 이미지로 변경</label>' + '</button>' + '</top-button>' + '</div>' + '</top-linearlayout>';

              	      if ($('top-linearlayout#pf_b2b_image').length == 0) {
              	    	BtobImageList = $(BtobImageList);
              	    	BtobImageList.find("button#pf_b2b_image_pc").on('click', Top.Controller.get('spaceBtobMyProfileEditLayoutLogic').pf_image_choosefileBtob); //pc
              	    	BtobImageList.find("button#pf_b2b_image_delete").on('click', Top.Controller.get('spaceBtobMyProfileEditLayoutLogic').pf_image_delete_picBtob); //삭제

              	        $('div.photo-div').append(BtobImageList);

              	        $('.position-pf_b2b_image').css({
              	          'top': '8.8rem',
              	          'right': '1.5rem'
              	        });
              	      }
              	    } else {
              	      if ($('top-linearlayout#pf_b2b_image').length != 0) {
              	        $('top-linearlayout#pf_b2b_image').remove();
              	      }
              	    }

              	    ;
              	  }

              	  ;
              	}
            //저장 이벤트
            document.querySelector('.bot-gpf #saveBtn').addEventListener('click', function(){
            	///////////////////////////////////////////////b2b일떄
            	if(!userManager.isBtoc()){
            		
            		
                	var cflag = 0;
                	var pflag = 0;
                	
                	if(!pattern_num.test(Number(($('input.gpf_comNum_text').val())))){
                		cflag = 1;
                	}           	
                	if(!pattern_num.test(Number(($('input.gpf_phone_text').val()))) ||$('input.gpf_phone_text').val().trim() === "" ){
                		pflag = 1;
                	} 
                	
                	
                	//배경화면 이미지
                	if($("div.top-gpf").css('background-image').indexOf(getDefaultBackground(userManager.getLoginUserId())) > -1  ){
                		var backgroundImage = null; 		
                    	
                	}else {backgroundImage = $("div.top-gpf").css('background-image').split(`"`)[1]}
                	//프로필 이미지 
                	if( $('img#imageBtn2').attr("src").indexOf(userManager.getUserDefaultPhotoURL(userManager.getLoginUserId())) === 0){
                		var profileImage = null;
                	}else{
                		profileImage = $('img#imageBtn2').attr("src");
                	}
                	
                	if(cflag){
                		$('button#phoneValidIcon').css({"visibility":"visible"})
                		$('button#phoneValidIcon').click()
                	}else{
                		$('button#phoneValidIcon').css({"visibility":"none"})
                	}
                	if(pflag){
                		$('button#mobileValidIcon').css({"visibility":"visible"})
                		$('button#mobileValidIcon').click()
                	}else{
                		$('button#mobileValidIcon').css({"visibility":"none"})
                	}
                	
                	
                	if(cflag || pflag){return false}
                	
                	
                	//b2b put
                	 $.ajax({
           	          url: _workspace.url+"Users/BothProfile?action=Put", //
           	          type: 'POST',
           	          dataType: 'json', 
           	          crossDomain: true,
           	          contentType: 'application/json',
           	          xhrFields: {

           	              withCredentials: true

           	            },
           	            data: JSON.stringify({
           			          "dto": {
           			            "USER_ID": userManager.getLoginUserId(),
           			            "USER_PHONE" : $('input.gpf_phone_text').val(),
           			            "USER_COM_NUM" : $('input.gpf_comNum_text').val(),
           			            "PROFILE_PHOTO" : profileImage,
           			            "NATIONAL_CODE" : $('.gpf_comNum_nc').text(),
           			            "BACKGROUND_PHOTO" : backgroundImage,
           			            "USER_EMAIL" : $('span.gpf_mail_text').text(),
           			            "USER_NICK" : $('.gpf_nameView').text()
           			          }
           			        }),
           	          success: function(result) {

           	        	  //업데이트 쳐주는
           	        	  userManager.update();      	        	  
           	        	//실시간으로 갱신되게끔
        				  if($('ul.me-list .me img').length != 0){$('ul.me-list .me img').attr('src',$('img#imageBtn2').attr("src"))}//LNB Friend List
           	        	  if($('button#createSpaceButton').length !=0)spaceAPI.drawLnbSpace(workspaceManager.getMySpaceId())	  //LNB Space List 
        				  //if($('span#AppGnbCurrentText').text().indexOf('(나)') > -1 ){ $('img#AppGnbCurrentImage').attr('src',$('img#imageBtn2').attr("src"))} //GNB Space Name
        				  renewGnbImage()//GNB Space Name
        				  if($('img#gnbAppProfileIcon').length != 0){$('img#gnbAppProfileIcon').attr('src',$('img#imageBtn2').attr("src"))}  //GNB profile menu
        				  if($('img#BtobspaceProfileImageView').length != 0) {											  // Contents Empty Page
        					  $('img#BtobspaceProfileImageView').attr('src',$('img#imageBtn2').attr("src"))
        					  backgroundImage != null ? backgroundImage = "url("+backgroundImage+")": backgroundImage = "url("+getDefaultBackground(userManager.getLoginUserId())+")"
        					  $('div#spaceBtobMyProfileLayout').css({'background-image':backgroundImage})
        				  }
           	        	  upperProfileWrapper.remove();
           	        	  profileDialog.appendChild(upperWrapper);
           	        	  profileDialog.appendChild(btnWrapper);
           	        	  $('img.prf-user_image').attr('src',$('img#gnbAppProfileIcon').attr("src"))
           	        	  
           	        	  //토스트팝업
  						TeeToast.open({text: '변경 사항이 저장되었습니다.'});
           	        	  
           	          },
           	          error: function(error) { 
           	          }
           	        });		
            		
            		
            	}else{
            		////////////////////////////////////////////////////////b2c일때
            		var nflag = 0;
            		if(!$('input.b2c_nick_edit').val() || $('input.b2c_nick_edit').val().length === 0 ){
            			nflag = 1;
            		}
            		
            		if(nflag){
                		$('button#nickNullIcon').css({"visibility":"visible"})
                		
                		return false;
                	}else{
                		$('button#nickNullIcon').css({"visibility":"hidden"})
            			$('div.b2c-nick-div').css({'margin-left':'2rem'})
            			$('div.nick_popover').remove()
                	}
            		
                	//배경화면 이미지
                	if($("div.top-gpf").css('background-image').indexOf(getDefaultBackground(userManager.getLoginUserId())) > -1  ){
                		var backgroundImage = null; 		
                    	
                	}else {backgroundImage = $("div.top-gpf").css('background-image').split(`"`)[1]}
                	//프로필 이미지 
                	if( $('img#imageBtn2').attr("src").indexOf(userManager.getUserDefaultPhotoURL(userManager.getLoginUserId())) === 0){
                		var profileImage = null;
                	}else{
                		profileImage = $('img#imageBtn2').attr("src");
                	}
            		
            		
            		 $.ajax({
              	          url: _workspace.url+"Users/BothProfile?action=Put", //
              	          type: 'POST',
              	          dataType: 'json', 
              	          crossDomain: true,
              	          contentType: 'application/json',
              	          xhrFields: {

              	              withCredentials: true

              	            },
              	            data: JSON.stringify({
              			          "dto": {
              			            "USER_ID": userManager.getLoginUserId(),
              			            "USER_PHONE" : $('.b2c_mobile_text').text(),
              			            "USER_COM_NUM" : null,
              			            "PROFILE_PHOTO" : profileImage,
              			            "NATIONAL_CODE" : $('.b2c_national_code').text(),
              			            "BACKGROUND_PHOTO" : backgroundImage,
              			            "USER_EMAIL" : $('.b2c_mail_text').text(),
              			            "USER_NICK" : $('.b2c_nick_edit').val()
              			          }
              			        }),
              	          success: function(result) {

              	        	  //업데이트 쳐주는
              	        	  userManager.update();      	        	  
              	        	//실시간으로 갱신되게끔
           				 
              	       
              	          if($('ul.me-list .me img').length != 0){$('ul.me-list .me img').attr('src',$('img#imageBtn2').attr("src"))}//LNB Friend List
           				  if($('button#createSpaceButton').length !=0)spaceAPI.drawLnbSpace(workspaceManager.getMySpaceId())	  //LNB Space List 
           				 // if($('span#AppGnbCurrentText').text().indexOf('(나)') > -1 ){ $('img#AppGnbCurrentImage').attr('src',$('img#imageBtn2').attr("src"))} //GNB Space Name
           				 renewGnbImage()
           				  if($('img#gnbAppProfileIcon').length != 0){$('img#gnbAppProfileIcon').attr('src',$('img#imageBtn2').attr("src"))}  //GNB profile menu
           				  if($('img#spaceProfileImageView').length != 0) {											  // Contents Empty Page
           					  $('img#spaceProfileImageView').attr('src',$('img#imageBtn2').attr("src"))
           					  backgroundImage != null ? backgroundImage = "url("+backgroundImage+")": backgroundImage = "url("+getDefaultBackground(userManager.getLoginUserId())+")"
           					  $('div#spaceMyProfileLayout').css({'background-image':backgroundImage})
           				  }
              	        	  upperProfileWrapper.remove();
              	        	  profileDialog.appendChild(upperWrapper);
              	        	  //별명 바로 반영
              	        	  profileDialog.querySelector('.prf-text_1').innerText =userManager.getLoginUserInfo().userNick;
              	        	  profileDialog.appendChild(btnWrapper);
              	        	  $('img.prf-user_image').attr('src',$('img#gnbAppProfileIcon').attr("src"))
              	        	  //토스트팝업
              	        	  TeeToast.open({text: '변경 사항이 저장되었습니다.'});
              	          },
              	          error: function(error) { 
              	          }
              	        });		
            		
            		
            		
            	}
            
            	   
        	});   
                
                
                
            //취소 이벤트
    	    document.querySelector('.bot-gpf #canceBtn').addEventListener('click', function(){
            	    upperProfileWrapper.remove();
        	   	    profileDialog.appendChild(upperWrapper);
        	 	    profileDialog.appendChild(btnWrapper);
        	});

    	    
    	    if($('div.office-div').height() < 50 ){
    	    	$('div.office-div').css({"height":"auto"})
    	    }else{
    	    	$('div.office-div').css({"height":"2.2rem"})
    	    }
    	    
    	    
    	    
    	    
    	    
    	    
    	    
    	    
            },
            error: function error(_error2) {}
          
    		});	 
    	
	    
	    
	    });	 		
	    
	    profileDialog.querySelector('#profilePwdEdit').addEventListener('click', function (){
	    	tds('userSettingDialog').open();
            Top.Controller.get('userSettingDialogLayoutLogic').pwFlag = 1;
	    	$('.prf-dialog').remove();
			$("#profileDialogOverlay").remove();			
	    });
	    profileDialog.querySelector('#profileSetting').addEventListener('click', function (){
	    	tds('userSettingDialog').open();
            Top.Controller.get('userSettingDialogLayoutLogic').pwFlag = 0;
	    	$('.prf-dialog').remove();
			$("#profileDialogOverlay").remove();
	    });
	    profileDialog.querySelector('#profileSupport').addEventListener('click',function(){
	    	
	    	Top.App.routeTo("/spt/home");
	    	$('.prf-dialog').remove();
	    	$("#profileDialogOverlay").remove();
	    });

	    //overlay
	    openOverlay('profileDialog');
		document.querySelector('body').appendChild(profileDialog);
	};

	let talkMap = {};
	
	/* minitalk */
	const miniTalk = function(spc=workspaceManager.getMySpaceUrl()){
		let pHeight = 600;
		let pWidth = 400;
		let xPos = window.screenLeft + (window.innerWidth/2) - (pWidth/2);
		let yPos= window.screenTop + (window.innerHeight/2) - (pHeight/2);
		let targetUrl = `${rootUrl}s/${spc}/talk?mini=true`;
		
		//중복 실행 방지 -- 새로고침시 문제의 소지O (todo: 세션 스토리지X - window 객체 자체가  sessionStorage 소유)
		//          --  새로고침시 기존 미니톡과 새로 켜지는 미니톡을 연결시킬 방법 필요
		if(talkMap[spc]){
			talkMap[spc].location = targetUrl;
			talkMap[spc].focus();
			return false;
		} 
		
		//오픈
		let newTalkWindow = window.open(															//주소, 이름, 옵션 
                targetUrl,												
                `name(${spc})`,																		//spc,
                `menubar=0, width=${pWidth}, height=${pHeight}, left=${xPos}, top=${yPos}, channelmode=yes`			//,location=0(주소,opera), resizable=0(ie), toolbar=0(ie,ff), status=0
          );

		newTalkWindow.document.createAttribute("isDiffTalk");
        newTalkWindow.onbeforeunload = ()=> {talkMap[spc] = null;}
        talkMap[spc] = newTalkWindow;
        return newTalkWindow; 
	}
	
	/* menu */
	const comMenu = function(){
		function initMenu(){
			let template = htmlToElement(
								`<div class="com-menu">
									<ul>
									</ul>
								</div>`
							);
			return{
				open: function(){
					$('body').prepend(template);
				},
				
				set: function(x='50%', y='50%', cls=''){
					template.style.left = x;
					template.style.top = y;
					template.classList.add(cls);
				},
				
				add: function(input=[]){
					let contents = ``;
					for (let i=0; i<input.length; i++){
						let item = `<li>${input[i]}</li>`;	
						contents += item;
					}
					template.appendChild(htmlToElement(contents));
				}
			} 
		}
		return initMenu();
	}
	
	const checkSpace = function(spcId){

		let data = workspaceManager.getWorkspace(spcId);
		let temp = spaceAPI.isTempSpace(spcId);
		if(data || temp) return true;

		TeeAlarm.open(
				{
					type 	:	'error',
					title	:	'접근 오류',
					content	:	'접근할 수 없는 URL 주소 입니다.',
					cancelButtonText	:	'확인',
					onCancelButtonClicked	: (e) =>{
						let type = tds('AppSplitLayout'); //getRoomIdByUrl();
						if(type){	//최초 접속 이후 - 뒤로가기
							history.back();
						}
						else{		//최초 접속 시 - 프로필
							Top.App.routeTo(`/f/${userManager.getLoginUserInfo().USER_LOGIN_ID}`, {eventType: 'close'});
						    //['close', 'fold', 'expand'].forEach( function(type){ tds("AppSplitMainLayout").removeClass(type) });
						    //tds("AppSplitMainLayout").addClass('close');
						}
						TeeAlarm.close();
					}
				}
		); 
		return false;
	}
	
	const invalidUrl = function(){
//		TeeAlarm.open(
//				{
//					type 	:	'error',
//					title	:	'접근 오류',
//					content	:	'유효하지 않은 URL 주소 입니다.',
//					cancelButtonText		:	'확인',
//					onCancelButtonClicked	:	(e) =>{
							let target = location.hash;
							let main = getMainAppByUrl();
							let rev = { mail : { before : "/s/", after: "/m/" }, 
										talk : { before : "/m/", after: "/s/" }};
							target = target.replace(rev[main].before, rev[main].after);
							if("talk" === main) window.location.replace(rootUrl + target.slice(3));
							else if("mail" === main){
								let roomUrl = getRoomIdByUrl(target);
								let myRoom = workspaceManager.getMySpaceUrl();
								if(roomUrl === myRoom){
									window.location.replace(rootUrl + target.slice(3));
								}else{
									target = target.replace(roomUrl, myRoom);
									window.location.replace(rootUrl + target.slice(3));
								}
							}
//						TeeAlarm.close();
//					}
//				}
//		);
	}
	
	/**/
	exports.openOverlay = overlay;
	exports.openDropdown = dropdown;
	exports.openMiniTalk = miniTalk;
	exports.getMiniTalkList = () => talkMap,
	exports.comMenu = comMenu;
	exports.checkSpace = checkSpace;
	exports.invalidUrl = invalidUrl;
})(this);

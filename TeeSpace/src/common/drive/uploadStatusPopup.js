const uploadStatusPopup = (() => {
    let _rootElement = null;
    const _appendTarget = "body";

    const _createDialog = () => {
        const element = document.createElement("div");
        element.setAttribute("id", "upload__dialog");

        const template = `<div id="upload__dialog__header">
                              <div style="display:flex;align-items:center;">
                                  <span id="upload__progress-count">0</span>
                                  <span id="upload__progress-text">개 항목 업로드 중</span>
                                  <div class="upload__vertical-line"></div>
                                  <span id="upload__success-count">0</span>
                                  <span id="upload__success-text">개 성공</span>
                              </div>

                              <div style="display:flex;align-items:center;font-size:0.75rem;color:#ffffff;">
                                  <span class="icon-work_close upload__expand-button"></span>
                                  <span class="icon-work_open upload__collapse-button"></span>
                                  <span class="icon-work_cancel upload__close-button"></span>
                              </div>
                          </div>
                          <div id="upload__dialog__content">
                          </div>`;

        element.insertAdjacentHTML("afterbegin", template);
        element.querySelector(".upload__expand-button").addEventListener("click", expand);
        element.querySelector(".upload__collapse-button").addEventListener("click", collapse);
        element.querySelector(".upload__close-button").addEventListener("click", close);
        return element;
    };

    const _createItemElement = (id, extension, name) => {
        const element = document.createElement("div");
        element.classList.add("upload__item");
        element.setAttribute("data-item-id", id);
//  ${getFileIconClass(extension)}
        const template = `<div class='upload__file__icon'>
                            <img src='${fileExtension.getInfo(extension)["iconImage"]}'/>
                          </div>
                          <span class='upload__file__name' style="overflow-x:hidden;text-overflow:ellipsis;flex-wrap:nowrap;flex:auto";>${name}</span>
                          <div class='upload__status__icon'></div>`;

        element.insertAdjacentHTML("afterbegin", template);
        //element.querySelector(".upload__file__name").addEventListener("click", _fileNameClick);
        return element;
    };
    
    const _fileNameClick = () => {
    	console.log("TEST");
    }    
    
    const open = () => {
        if (!_rootElement) {
            _rootElement = _createDialog();
            const appendTarget = document.querySelector(_appendTarget);
            if (appendTarget) {
                appendTarget.appendChild(_rootElement);
            }
        }

        if (_rootElement) {
            _rootElement.style.display = "flex";
        }
    };

    const collapse = () => {
        if (_rootElement) {
            _rootElement.querySelector("#upload__dialog__content").style.display = "none";
            _rootElement.querySelector(".upload__expand-button").style.display = "flex";
            _rootElement.querySelector(".upload__collapse-button").style.display = "none";
        }
    };

    const expand = () => {
        if (_rootElement) {
            _rootElement.querySelector("#upload__dialog__content").style.display = "flex";
            _rootElement.querySelector(".upload__expand-button").style.display = "none";
            _rootElement.querySelector(".upload__collapse-button").style.display = "flex";
        }
    };

    const close = () => {
        if (_rootElement) {
            _rootElement.style.display = "none";
            
            const contents = _rootElement.querySelector("#upload__dialog__content");
            while (contents.hasChildNodes()) {
            	contents.removeChild(contents.firstChild);
            }
        }        
    };

    const insert = (id, extension, name) => {
        if (_rootElement) {
            const itemElement = _createItemElement(id, extension, name + "." + extension);
            _rootElement.querySelector("#upload__dialog__content").appendChild(itemElement);

            update(id, FileStatus.TRY_UPLOAD);
        }
    };

    const remove = id => {
        if (_rootElement) {
            const targetElement = _rootElement.querySelector(`[data-item-id='${id}']`);
            if (targetElement) {
                targetElement.remove();
            }
        }
    };

    const update = (id, status) => {
        // status : uploading, success, fail
        if (_rootElement) {
            const targetElement = _rootElement.querySelector(`[data-item-id='${id}']`);
            if (targetElement) {
                const iconContainer = targetElement.querySelector(".upload__status__icon");
                while (iconContainer.hasChildNodes()) {
                    iconContainer.removeChild(iconContainer.firstChild);
                }
                switch (status) {
                    case 11: // uploading
                        iconContainer.classList.remove("icon-chart_error");
                        iconContainer.classList.remove("icon-circle_check");

                        const template =
                            '<div class="new-file-loader" style="display: inherit; width:100%; height:100%; float: right;"></div>';
                        iconContainer.insertAdjacentHTML("afterbegin", template);
                        break;
                    case 12: // success
                        iconContainer.classList.remove("icon-chart_error");
                        iconContainer.classList.add("icon-check");
                        break;
                    case 13: // fail
                        iconContainer.classList.remove("icon-check");
                        iconContainer.classList.add("icon-chart_error");
                        iconContainer.title = "파일명이 70자를 넘는 경우 업로드할 수 없습니다.";
                        break;
                }
            }
        }
    };

    const updateCount = (type, count) => {
        if (_rootElement) {
            // type : progress, success
            if (type !== "progress" && type !== "success") return;

            if (_rootElement) {
                _rootElement.querySelector(`#upload__${type}-count`).innerText = count;
            }
        }
    };
    
    const updateUploadText = (text) => {
    	if(_rootElement) {
    		_rootElement.querySelector(`#upload__progress-text`).innerText = text;
    	}    	
    }
    
    const lengthCheck = () => {
    	if(_rootElement) {
    		return _rootElement.querySelectorAll('.upload__item').length;
    	}
    	return null;
    }
    

    return {
        // 연다
        open,

        // 접는다
        collapse,

        // 펼친다.
        expand,

        // 닫는다
        close,

        // row 추가
        insert,

        // row 삭제
        remove,

        // row 갱신
        update,

        //  count 갱신
        updateCount,
        
        // text 갱신
        updateUploadText,
        
        // 존재하는 항목 수 확인
        lengthCheck,
    };
})();

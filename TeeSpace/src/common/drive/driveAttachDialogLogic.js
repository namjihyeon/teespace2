const driveAttachFileDialog_DEFAULT_CONFIG = { 
    buttonFn1                  : undefined, 
    successCallback            : undefined,
    buttonFn2                  : undefined, 
    cancelCallback             : undefined,
    channel_id                 : undefined,
    
    user_context_1             : undefined,
    user_context_2             : undefined,
    user_context_3             : undefined,
    	
    preventMultiSelect         : false,
    filteringFileExtensionList : [],
    
    selectedFile               : {},
    sendFile                   : [],
    fileId                     : undefined,
    fileName                   : undefined,
    fileExt                    : undefined,
    
    success                    : undefined,
    cancel                     : undefined,
}

function OPEN_TDRIVE(config, isSaveToTeeDriveDialog, isSendToTeeDriveDialog) {
    var driveAttachFileLayoutLogic = Top.Controller.get('driveAttachFileLayoutLogic');
    driveAttachFileLayoutLogic.config = config == undefined ? driveAttachFileDialog_DEFAULT_CONFIG : config;
    driveAttachFileLayoutLogic.isSaveToTeeDriveDialog = isSaveToTeeDriveDialog == undefined ? false : isSaveToTeeDriveDialog;    
    driveAttachFileLayoutLogic.isSendToTeeDriveDialog = isSendToTeeDriveDialog == undefined ? false : isSendToTeeDriveDialog;
	
	var driveAttachFileDialog = Top.Dom.selectById('driveAttachFileDialog');
	
	if(isSaveToTeeDriveDialog && !isSendToTeeDriveDialog){
			driveAttachFileDialog.template.title = "다른 이름으로 TeeDrive에 저장"; //S("MSG_TD_CONTENTS_CONTEXT_MENU_FILE_OTHER_SAVE_POPUP_TITLE")
	} else if(!isSaveToTeeDriveDialog && isSendToTeeDriveDialog){
			driveAttachFileDialog.template.title =  "TeeDrive에 저장"; //S("MSG_TT_MESSAGE_BUBBLE_CONTEXT_MENU_DNLD_TO_TD")
	} else {
			driveAttachFileDialog.template.title =  "TeeDrive에서 첨부"; // S("MSG_WCM_DROPDOWN_BUTTON_ATTACH_FROM_TD_POPUP_TITLE")
	}	
	driveAttachFileDialog.open();
	driveAttachFileDialog.adjustPosition();

	if (config != undefined && config.callback != undefined){
		return config.callback();
	}
}

function SAVE_TO_TDRIVE(config) {
    let isSaveToTeeDriveDialog = true;
    let isSendToTeeDriveDialog = false;
    OPEN_TDRIVE(config, isSaveToTeeDriveDialog, isSendToTeeDriveDialog);
}

function SEND_TO_TDRIVE(config) {
	driveAttach.attachFromTeeDrive = false;
    let isSaveToTeeDriveDialog = false;
    let isSendToTeeDriveDialog = true;
    OPEN_TDRIVE(config, isSaveToTeeDriveDialog, isSendToTeeDriveDialog);
}

function TDrive_SaveAs(parentFolderId, fileId, fileExt, newFileName, contextOne, contextTwo, contextThree, successCallback, cancelCallback) {
	let response = driveServiceCall.copyFile(parentFolderId, fileId, fileExt, newFileName, contextOne, contextTwo, contextThree);
    if(response.result == "OK") {
        if(typeof successCallback == "function") {
            response.file_name = newFileName;
            response.file_extension = fileExt;
            successCallback(response);
        } 
        Top.Dom.selectById("driveAttachFileDialog").close();
    } else {
        TeeAlarm.open({title: "", content: response.message}); 
    	let deepCopyErr = "7 StorageFileCopy err : NO_SUCH_OBJECT";			// 19-11-17 CS1-3 김성준 
    	if(response.resultMsg == deepCopyErr){
    		TeeAlarm.open({title: "", content: "저장되지 않은 파일입니다. 저장 후에 다시 시도해주세요."});
    	} else {
    		TeeAlarm.open({title: "", content: "다른 이름으로 저장에 실패했습니다."});
    	}
    	if(typeof cancelCallback == "function"){
        	response.file_name = newFileName;
            response.file_extension = fileExt;
            cancelCallback(response);
        }	            	
    }
}




Top.Controller.create('driveAttachFileLayoutLogic', {
    config : driveAttachFileDialog_DEFAULT_CONFIG,
    isSaveToTeeDriveDialog : false,
    isSendToTeeDriveDialog : false,
    
	init : function(widget) {
        
		Top.Dom.selectById("newNameTextField").setProperties({
    		'hint'       : "파일 이름을 입력해 주세요.",
			'max-length' : MAX_FILE_NAME_LENGTH
    	});
		
		if((this.config.successCallback == undefined) && (typeof this.config.buttonFn1 == "function")) {
            this.config.successCallback = this.config.buttonFn1;
        }
        if((this.config.cancelCallback == undefined) && (typeof this.config.buttonFn2 == "function")) {
            this.config.cancelCallback = this.config.buttonFn2;
        }
                       
        const renderTarget = "driveAttachFile_main";
        const workspaceId = workspaceManager.getWorkspaceId(getRoomIdByUrl(), "CHN0006");
        const channelId = workspaceManager.getChannelList(getRoomIdByUrl(), "CHN0006");
        const isRendered = !!document.querySelector(`div#${renderTarget} .drive[data-drive-id='${channelId}'][data-is-drive='true']`);
      
        if (!isRendered) {
            $(`div#${renderTarget}`).empty();
            driveAttach.render(workspaceId, channelId, `div#${renderTarget}`, true);
        }
        
        if(this.isSaveToTeeDriveDialog == false && this.isSendToTeeDriveDialog == true) {
        	if(Top.Dom.selectById("driveAttachFileLayout").hasClass("drive__attach")){
        		Top.Dom.selectById("driveAttachFileLayout").removeClass("drive__attach")
        	}        	
        	document.getElementsByClassName("drive__attachfile__sidebar")[0].classList.add("drive__save");
        	document.getElementsByClassName("drive__attachfile__content")[0].classList.add("drive__save");
        	document.getElementsByClassName("drive__attachfile__search__input")[0].classList.add("drive__save");
        	Top.Dom.selectById("driveAttachFileLayout").addClass("drive__send");
        	Top.Dom.selectById("successBtn").setText("저장");
        	Top.Dom.selectById("newNameTextCover").setVisible("none");
        } else if(this.isSaveToTeeDriveDialog == true && this.isSendToTeeDriveDialog == false) {        	
        	if(Top.Dom.selectById("driveAttachFileLayout").hasClass("drive__attach")){
        		Top.Dom.selectById("driveAttachFileLayout").removeClass("drive__attach");
        	} 
        	document.getElementsByClassName("drive__attachfile__sidebar")[0].classList.add("drive__save");
        	document.getElementsByClassName("drive__attachfile__content")[0].classList.add("drive__save");
        	document.getElementsByClassName("drive__attachfile__search__input")[0].classList.add("drive__save");
        	Top.Dom.selectById("driveAttachFileLayout").addClass("drive__saveAs");
        	Top.Dom.selectById("successBtn").setText("저장");
        	Top.Dom.selectById("successBtn").setDisabled(true);
        	Top.Dom.selectById("newNameTextCover").setVisible("visible");
        	Top.Dom.selectById("newNameTextField").setText(this.config.fileName);
        	Top.Dom.selectById("newNameTextField").getElement("input#newNameTextField")[0].select();
        	Top.Dom.selectById("driveAttachFileDialog").setProperties({'isOpen': false});
        } else {
        	document.getElementsByClassName("drive__attachfile__sidebar")[0].classList.remove("drive__save");
        	document.getElementsByClassName("drive__attachfile__content")[0].classList.remove("drive__save");
        	document.getElementsByClassName("drive__attachfile__search__input")[0].classList.remove("drive__save");
        	if(Top.Dom.selectById("driveAttachFileLayout").hasClass("drive__saveAs")){
        		Top.Dom.selectById("driveAttachFileLayout").removeClass("drive__saveAs")
        	}
        	if(driveAttach.getSelectedFileList().length == 0){
            	Top.Dom.selectById("successBtn").setDisabled(true);
            }
        	Top.Dom.selectById("driveAttachFileLayout").addClass("drive__attach");
        	Top.Dom.selectById("newNameTextCover").setVisible("none");
        }
	},
	
	onAdd : function(event, widget) {
		if(this.isSaveToTeeDriveDialog && !this.isSendToTeeDriveDialog) { // "다른 이름으로 저장" 
		    let contextOne = this.config.user_context_1;
		    let contextTwo = this.config.user_context_2;
		    let contextThree = this.config.user_context_3;
			
			let newFileName = Top.Dom.selectById("newNameTextField").getText();
            if(!this.validate(newFileName)) {
                return;
            }
            let parentFolderId = driveAttach.getCurrentFolderId();            
            let fileFolder = driveAttach.getFileList();
            let targetFile = [];            
            let targetExt = [];
            
			for(let i=0; i<fileFolder.length; i++) {
				if(fileFolder[i].is_folder == "false" && fileFolder[i].storageFileInfo.file_name == newFileName &&
					fileFolder[i].storageFileInfo.file_extension == this.config.fileExt && fileFolder[i].storageFileInfo.file_id != this.config.fileId) {					
					targetFile.push({fileId: fileFolder[i].storageFileInfo.file_id, fileName: fileFolder[i].storageFileInfo.file_name, 
									  fileExt: fileFolder[i].storageFileInfo.file_extension});
					break;
				}
			}
            
            let successCallback = Top.Controller.get("driveAttachFileLayoutLogic").config.successCallback;
            let cancelCallback = Top.Controller.get('driveAttachFileLayoutLogic').config.cancelCallback;
			
			if(targetFile.length != 0){
				const config_att = {
					isFrom         : "saveAsDialog",
					target         : targetFile,
					newName        : newFileName,
					parentFolder   : parentFolderId,
					fileId         : this.config.fileId,
					fileExt        : this.config.fileExt,
					success        : successCallback,
					cancel         : cancelCallback,				
					user_context_1 : contextOne,
					user_context_2 : contextTwo,
					user_context_3 : contextThree,
				}
				openDriveOverWriteDialog(config_att);
				
			} else {
				TDrive_SaveAs(parentFolderId, this.config.fileId, this.config.fileExt, newFileName, contextOne, contextTwo, contextThree, successCallback, cancelCallback);
			}
        }
		
		else if(!this.isSaveToTeeDriveDialog && this.isSendToTeeDriveDialog){ // "TeeDrive에 저장", config에 fileMeta 요청 		    	   	
			let parentFolderId = driveAttach.getCurrentFolderId();
			let fileFolder = driveAttach.getFileList();
			let fileMetas;
			let targetFile = [];
			let sendFileArr = [];
			
			if(Array.isArray(this.config)){
	    		fileMetas = this.config;
	    	} else {
	    		fileMetas = [this.config];
	    	}
	    	
	    	for(let i=0; i<fileFolder.length; i++) {
	    		for(let j=0; j<fileMetas.length; j++) {
	    			if(fileFolder[i].is_folder == "false" && fileFolder[i].storageFileInfo.file_name == fileMetas[j].fileName &&
	    				fileFolder[i].storageFileInfo.file_extension == fileMetas[j].fileExt) {					
	    				targetFile.push({fileName: fileFolder[i].storageFileInfo.file_name, fileExt: fileFolder[i].storageFileInfo.file_extension, 
	    								   parent: parentFolderId});
	    					
	    				sendFileArr.push({fileId: fileMetas[j].fileId, fileName: fileMetas[j].fileName, fileExt: fileMetas[j].fileExt, parentFolder: parentFolderId, 
	    									contextOne: fileMetas[j].user_context_1, contextTwo: fileMetas[j].user_context_2, contextThree: fileMetas[j].user_context_3,
	    									success: fileMetas[j].successCallback, cancel: fileMetas[j].cancelCallback});
	    			}
	    		}
	    	}
	    	
	    	if(targetFile.length != 0) {
	    		const config_att = {
	    			isFrom   : "sendDriveDialog",
	    		}
	    		drive.setSendTargetFile(targetFile);
	    		drive.setSendFileArr(sendFileArr);	    		
	    		openDriveOverWriteDialog(config_att);
	    		
	    	} else {
		    	for(var i=0; i<fileMetas.length; i++) {
		    		let response = driveServiceCall.copyFile(parentFolderId, fileMetas[i].fileId, fileMetas[i].fileExt, fileMetas[i].fileName,
		    										fileMetas[i].user_context_1, fileMetas[i].user_context_2, fileMetas[i].user_context_3);	    		
		    		if(response.result == "OK"){
		    			let successCallback = Top.Controller.get("driveAttachFileLayoutLogic").config.successCallback;	    			
		    			if(typeof successCallback == 'function'){
		    			 	successCallback(response);
		    			}	    			
		    		}
		    		else {
		    			let cancelCallback = Top.Controller.get("driveAttachFileLayoutLogic").config.cancelCallback;
		    			if(typeof cancelCallback == 'function'){
		    			 	cancelCallback(response);
		    			}
		    		}
		    	}
		    	Top.Dom.selectById("driveAttachFileDialog").close();
	    	}
	    }
		
        else { // "TeeDrive에서 첨부"
			let useChannel = this.config.channel_id;
			
		 	if(useChannel == undefined){ // ToOffice
		 		if(typeof this.config.successCallback == 'function') {
		 			let selectedFiles = driveAttach.getSelectedFileList();
			     	this.config.successCallback(selectedFiles);
			    }
		 		Top.Dom.selectById('driveAttachFileDialog').close();
		 		return;
			}
			
			let selectedFiles = driveAttach.getSelectedFileList();
		    let response = [];
		    let contextOne = this.config.user_context_1;
		    let contextTwo = this.config.user_context_2;
		    let contextThree = this.config.user_context_3;
			
			for(var i=0; i<selectedFiles.length; i++){
				let selectedFileId = "";
				let selectedFileExt = "";
				let selectedFileName = "";
				
				if(selectedFiles[i].is_folder == "true"){
					selectedFileId = selectedFiles[i].file_id;
					selectedFileExt = null;
					selectedFileName = selectedFiles[i].folder_name;					
				} else {
					selectedFileId = selectedFiles[i].storageFileInfo.file_id;
					selectedFileExt = selectedFiles[i].storageFileInfo.file_extension.toLowerCase();
					selectedFileName = selectedFiles[i].storageFileInfo.file_name;
				}
				
				response = driveServiceCall.exportCopyFile(selectedFileId, selectedFileExt, selectedFileName, useChannel, contextOne, contextTwo, contextThree);
				
				if(response.result == "OK"){
					let successCallback = Top.Controller.get("driveAttachFileLayoutLogic").config.successCallback;					
					if(typeof successCallback == 'function') {
					 	successCallback(response);
					}
				} else {
					let cancelCallback = Top.Controller.get("driveAttachFileLayoutLogic").config.cancelCallback;					
					if(typeof cancelCallback == 'function') {
						cancelCallback(response);
					}
				}
			}
			Top.Dom.selectById("driveAttachFileDialog").close();
	    }
	},
	
	onCancel : function(event, widget) {
		if(typeof this.config.buttonFn2 === 'function') {
			this.config.buttonFn2();
        } else if(typeof this.config.cancelCallback == 'function') {
            this.config.cancelCallback();
        }
		Top.Dom.selectById("driveAttachFileDialog").close();
	},
	
	newFileNameKeyReleased : function(event, widget) {
    	if (event.code == "Escape" || window.event.keyCode == 27) {
            this.onCancel();
            return;
        }
    	
    	let fileName = widget.getText();
    	if(fileName.length > MAX_FILE_NAME_LENGTH) {
    		let fileNameSub = fileName.substr(0, MAX_FILE_NAME_LENGTH);
    		widget.setText(fileNameSub);
    		fileName = fileNameSub;
    	}  
    	    	
        if (this.validate(fileName) && (event.code == "Enter" || event.code == "NumpadEnter" || window.event.keyCode == 13 )) {
            this.success();
        }
        
        if (fileName == this.config.fileName) {
        	Top.Dom.selectById("successBtn").setDisabled(true);
        } else {
        	Top.Dom.selectById("successBtn").setDisabled(false);
        }
	}, 
	
    validate: function (newFileName) {
        let valid = validateFileName(newFileName);

        let showRedBorder = valid.result == false && valid.message != String.empty;
        if (showRedBorder && !Top.Dom.selectById("newNameTextField").hasClass("error-textfield")) {
            Top.Dom.selectById("newNameTextField").addClass("error-textfield");
        } else if (!showRedBorder && Top.Dom.selectById("newNameTextField").hasClass("error-textfield")) {
            Top.Dom.selectById("newNameTextField").removeClass("error-textfield");
        }

        if(!valid.result && !Top.Dom.selectById("successBtn").hasClass("error-button")){
        	Top.Dom.selectById("successBtn").addClass("error-button")
        	Top.Dom.selectById("successBtn").setDisabled(true);
        } else if (valid.result && Top.Dom.selectById("successBtn").hasClass("error-button")){
        	Top.Dom.selectById("successBtn").removeClass("error-button")
        	Top.Dom.selectById("successBtn").setDisabled(false);
        }
        
        if(!valid.result) {
        	if(!Top.Dom.selectById("driveAttachFileDialog").getProperties('isOpen')) {
        		Top.Dom.selectById("driveAttachFileDialog").setProperties({'isOpen': true});
        		Top.Dom.selectById("driveSpecialCharacterErrorPopover").open();
        	}
        	
        	if(!Top.Dom.selectById("errorIcon").hasClass("error-popover")){
        		Top.Dom.selectById("errorIcon").addClass("error-popover");
        	}
        	        	
        } else {
        	if(Top.Dom.selectById("errorIcon").hasClass("error-popover")){
        		Top.Dom.selectById("errorIcon").removeClass("error-popover");
        	}
        	//Top.Dom.selectById("specialCharacterErrorPopover").close();
        }
        
        let showMetaphor = valid.message != String.empty ? "visible" : "none";
        Top.Dom.selectById("errorIcon").setVisible(showMetaphor);
            
        return valid.result;
    }
	
	
});

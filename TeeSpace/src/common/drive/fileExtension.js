const fileExtension = (() => {
    const _path = "./res/drive/file_icons/";
    const _iconImage = {
        file: _path + "drive_file.svg",
        folder: _path + "drive_folder.svg",
        image: _path + "drive_image.svg",
        tocell: _path + "drive_tocell.svg",
        tohangul: _path + "drive_tohangul.svg",
        topoint: _path + "drive_topoint.svg",
        toword: _path + "drive_toword.svg",
        txt: _path + "drive_txt.svg",
        pdf: _path + "drive_pdf.svg",
        audio: _path + "drive_audio.svg",
        movie: _path + "drive_movie.svg",
        zip: _path + "drive_zip.svg"
    };

    const getInfo = extension => {
        const _extension = extension.toLowerCase();

        const info = {
            type: null,
            iconImage: null,
            isPreview: false
        };

        switch (_extension) {
            // 이미지
            case "apng":
            case "bmp":
            case "gif":
            case "jpg":
            case "jpeg":
            case "jfif":
            case "png":
            case "svg":
            case "rle":
            case "die":
            case "tif":
            case "tiff":
            case "raw":
                info["type"] = "image";
                info["iconImage"] = _iconImage["image"];
                info["isPreview"] = true;
                break;

            // 동영상
            case "mkv":
            case "avi":
            case "mp4":
            case "mpg":
            case "flv":
            case "wmv":
            case "asf":
            case "asx":
            case "ogm":
            case "ogv":
            case "mov":
            case "dat":
            case "rm":
            case "mpe":
            case "mpeg":
                info["type"] = "video";
                info["iconImage"] = _iconImage["movie"];
                info["isPreview"] = true;
                break;

            // 오디오
            case "mp3":
            case "wav":
            case "ogg":
            case "flac":
            case "wma":
            case "aac":
                info["type"] = "audio";
                info["iconImage"] = _iconImage["audio"];
                info["isPreview"] = true;
                break;

            // 오피스 (파워포인트)
            case "ppt":
            case "pptx":
            case "tpt":
                info["type"] = "office";
                info["iconImage"] = _iconImage["topoint"];
                info["isPreview"] = false;
                break;

            // 오피스 (워드)
            case "doc":
            case "docx":
            case "toc":
                info["type"] = "office";
                info["iconImage"] = _iconImage["toword"];
                info["isPreview"] = false;
                break;

            // 오피스 (엑셀)
            case "xls":
            case "xlsx":
            case "tls":
            case "csv":
                info["type"] = "office";
                info["iconImage"] = _iconImage["tocell"];
                info["isPreview"] = false;
                break;

            // 오피스 (한글)
            case "hwp":
                info["type"] = "office";
                info["iconImage"] = _iconImage["tohangul"];
                info["isPreview"] = false;
                break;

            // 오피스 (기타)
            case "txt":
                info["type"] = "file";
                info["iconImage"] = _iconImage["txt"];
                info["isPreview"] = false;
                break;
            case "pdf":
                info["type"] = "file";
                info["iconImage"] = _iconImage["pdf"];
                info["isPreview"] = false;
                break;

            // 압축 파일
            case "zip":
            case "tar":
            case "rar":
            case "tgz":
            case "war":
            case "alz":
            case "ace":
            case "arc":
            case "arj":
            case "b64":
            case "bh":
            case "bhx":
            case "bin":
            case "bz2":
            case "cab":
            case "ear":
            case "enc":
            case "gz":
            case "ha":
            case "hqx":
            case "ice":
            case "img":
            case "jar":
            case "lha":
            case "lzh":
            case "mim":
            case "pak":
            case "uue":
            case "xxe":
            case "zoo":
                info["type"] = "file";
                info["iconImage"] = _iconImage["zip"];
                info["isPreview"] = false;
                break;

            // 폴더
            case "folder":
                info["type"] = "folder";
                info["iconImage"] = _iconImage["folder"];
                info["isPreview"] = false;
                break;

            //exe 파일
            case "exe":
                info["type"] = "exe";
                info["iconImage"] = _iconImage["file"];
                info["isPreview"] = false;
                break;

            // 파일
            default:
                info["type"] = "exe";
                info["iconImage"] = _iconImage["file"];
                info["isPreview"] = false;
                break;
        }

        return info;
    };

    return {
        getInfo
    };
})();

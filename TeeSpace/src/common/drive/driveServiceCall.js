let driveServiceCall =  {

    _ajaxCall : function(serviceUrl, type, inputDTO, callback)  {
        if (type.toUpperCase() == "GET") {
            serviceUrl += "&rn=" + Date.now();
        }

        let outputData = null;
        try {
            Top.Ajax.execute({
                url: String.format("{0}{1}", _workspace.fileUrl, serviceUrl),
                type: type,
                dataType: "json",
                async: false,
                cache: false,
                data: inputDTO,
                contentType: "application/json",
                crossDomain: true,
                xhrFields: {
                    withCredentials: true
                },
                success: function(data) {
                    try {
                        if (typeof data == "string") {
                            data = JSON.parse(data);
                        }

                        outputData = data.dto;
                        if (outputData.result != undefined) {
                            outputData.result = outputData.result.toUpperCase();
                        }

                        if (callback && typeof callback === "function") callback();
                    } catch (error) {
                        outputData = {
                            result: "FAIL",
                            message: "UnknownError.(CORS?)... JSON.stringify(error) : " + JSON.stringify(error),
                            data: error
                        };
                    }
                },
                error: function(data) {
                    outputData = {
                        result: "FAIL",
                        message: data.exception != undefined ? data.exception.message : "Error",
                        data: data
                    };
                    if (data.exception != undefined) {
                        console.log(data.exception.message);
                    }
                }
            });
        } catch (ex) {
            //StartLoader(false);
        }

        return outputData;
    },

    getMoveFileFolderDTO : (workspaceId, channelId, appNumber, files) => {
        if (!Array.isArray(fileMetas) || String.isNullOrEmpty(dstFolderId)) {
            return undefined;
        }

        let inputDTO = [];
        for (var i = 0; i < files.length; i++) {
            inputDTO.push({
                is_folder: files[i].is_folder,
                file_id: files[i].is_folder == "false" ? files[i].storageFileInfo.file_id : files[i].file_id
            });
        }

        return {
            dto: {
                user_id: userManager.getLoginUserId(),
                workspace_id: workspaceId,
                channel_id: channelId,
                app_number: appNumber,
                teeDriveDtoList: inputDTO
            }
        };
    },
    
    moveFileFolder : (workspaceId, channelId, appNumber, files) => {
        let inputDTO = driveServiceCall.getMoveFileFolderDTO(workspaceId, channelId, appNumber, files);
        return driveServiceCall._ajaxCall("DriveMoveChannel?action=", "POST", inputDTO);
    },

    getDeleteOneFileDTO : (fileId, parentId) => {
        return {
            dto: {
                user_id: userManager.getLoginUserId(),
                app_number: "6",
                teeDriveDtoList: [
                    {
                        workspace_id: workspaceManager.getWorkspaceId(getRoomIdByUrl(), "CHN0006"),
                        ch_id: workspaceManager.getChannelList(getRoomIdByUrl(), "CHN0006"),
                        file_id: fileId,
                        file_parent_id: parentId,
                        is_folder: "false"
                    }
                ]
            }
        };
    },
    
    deleteOneFile : (fileId, parentId) => {
        return driveServiceCall._ajaxCall("DriveFileDelete?action=", "POST", driveServiceCall.getDeleteOneFileDTO(fileId, parentId));
    },

    getCopyFileDTO : (fileParentId, fileId, fileExt, fileName, userContext1, userContext2, userContext3) => {
        let loginUserId = userManager.getLoginUserId();

        return {
            dto: {
                workspace_id: workspaceManager.getWorkspaceId(getRoomIdByUrl(), "CHN0006"),
                channel_id: workspaceManager.getChannelList(getRoomIdByUrl(), "CHN0006"),
                user_id: loginUserId,
                file_parent_id: fileParentId,
                storageFileInfo: {
                    file_id: fileId,
                    user_id: loginUserId,
                    file_last_update_user_id: loginUserId,
                    file_name: fileName,
                    file_extension: fileExt,
                    user_context_1: userContext1,
                    user_context_2: userContext2,
                    user_context_3: userContext3
                }
            }
        };
    },
    
    copyFile : (fileParentId, fileId, fileExt, fileName, userContext1, userContext2, userContext3) => {
        let inputDTO = driveServiceCall.getCopyFileDTO(
            fileParentId,
            fileId,
            fileExt,
            fileName,
            userContext1,
            userContext2,
            userContext3
        );
        return driveServiceCall._ajaxCall("SODriveCopy?action=", "POST", inputDTO);
    },

    exportCopyFile : (fileId, fileExt, fileName, dstChannel, userContext1, userContext2, userContext3) => {
        let inputDTO = driveServiceCall.getExportCopyFileDTO(
            fileId,
            fileExt,
            fileName,
            dstChannel,
            userContext1,
            userContext2,
            userContext3
        );
        return driveServiceCall._ajaxCall("SODriveCopy?action=", "POST", inputDTO);
    },

    getExportCopyFileDTO : function(
        fileId,
        fileExt,
        fileName,
        dstChannel,
        userContext1,
        userContext2,
        userContext3
    ) {
        let loginUserId = userManager.getLoginUserId();

        return {
            dto: {
                workspace_id: workspaceManager.getWorkspaceId(getRoomIdByUrl(), "CHN0006"),
                channel_id: dstChannel,
                user_id: loginUserId,
                file_parent_id: "",
                storageFileInfo: {
                    file_id: fileId,
                    user_id: loginUserId,
                    file_last_update_user_id: loginUserId,
                    file_name: fileName,
                    file_extension: fileExt,
                    user_context_1: userContext1,
                    user_context_2: userContext2,
                    user_context_3: userContext3
                }
            }
        };
    },

    removeDuplicateFileFolder : (dstFolderId, fileName, fileExt) => {
        return driveServiceCall._ajaxCall("CheckDuplicateName?action=", "POST", driveServiceCall.driveDuplicateNameDTO(dstFolderId, fileName, fileExt));
    },

    driveDuplicateNameDTO : (dstFolderId, fileName, fileExt) => {
        let loginUserId = userManager.getLoginUserId();
        let inputDTO = [];

        inputDTO.push({
            is_folder: "false",
            storageFileInfo: {
                file_name: fileName,
                file_extension: fileExt
            }
        });

        return {
            dto: {
                user_id: loginUserId,
                workspace_id: workspaceManager.getWorkspaceId(getRoomIdByUrl(), "CHN0006"),
                channel_id: workspaceManager.getChannelList(getRoomIdByUrl(), "CHN0006"),
                file_parent_id: dstFolderId,
                app_number: "6",
                teeDriveDtoList: inputDTO
            }
        };
    },

    _dragInFolder : (dstFolderElement, data) => {
        let inputDto;
        if (data.is_folder == "true") {
            // 폴더
            inputDto = {
                dto: {
                    file_parent_id: dstFolderElement.getAttribute("data-file-id"),
                    user_id: userManager.getLoginUserId(),
                    app_number: data.appNumber,
                    teeDriveDtoList: [
                        {
                            workspace_id: data.workspace_id,
                            ch_id: data.channel_id,
                            is_folder: "true",
                            folder_name: data.folder_name,
                            file_id: data.file_id
                        }
                    ]
                }
            };
        } else {
            // 파일
            inputDto = {
                dto: {
                    file_parent_id: dstFolderElement.getAttribute("data-file-id"),
                    user_id: userManager.getLoginUserId(),
                    app_number: data.appNumber,
                    teeDriveDtoList: [
                        {
                            workspace_id: data.workspace_id,
                            ch_id: data.channel_id,
                            is_folder: "false",
                            app_number: data.appNumber,
                            storageFileInfo: {
                                user_id: userManager.getLoginUserId(),
                                file_last_update_user_id: userManager.getLoginUserId(),
                                file_id: data.file_id
                            }
                        }
                    ]
                }
            };
        }

        let serviceUrl = `${_workspace.fileUrl}DriveMeta`;
        return axios.put(serviceUrl, inputDto);
    }
}
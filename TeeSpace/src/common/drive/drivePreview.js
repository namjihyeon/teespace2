const drivePreview = (() => {
    const _renderTarget = "body";
    let _rootElement = null;
    let _contentElement = null;
    let _isOpened = false;
    let _src = null;
    let _isDeleted = false;

    const open = (storageFileInfo, type, src, isDeleted = false) => {
        _setDeleted(isDeleted);
        if (!_rootElement) {
            _rootElement = _createPreviewElement(storageFileInfo);
            document.querySelector(_renderTarget).appendChild(_rootElement);
        }
        if (_rootElement && !_isOpened) {
            _rootElement.style.display = "flex";
            _isOpened = true;

            _updateFileInfo(storageFileInfo);
            _contentElement = _rootElement.querySelector(".drive__preview__content");
            if (_contentElement) {
                _setsrc(src);
                const mediaElement = _createMediaElement(type, src);
                _contentElement.appendChild(mediaElement);
            }
        }
    };

    const close = () => {
        if (_rootElement) {
            while (_contentElement.hasChildNodes()) {
                _contentElement.removeChild(_contentElement.firstChild);
            }

            _rootElement.style.display = "none";
            _isOpened = false;
        }
    };
    
    const _onDownButtonClick = (event) => {
        let drivePreviewDownloadContextMenu = Top.Dom.selectById("drivePreviewDownloadContextMenu");

        let offsetX = 0;
        let offsetY = 0;
        let boxWidth = 133;
        let boxHeight = 214;
        var posX = event.clientX;
        var posY = event.clientY;

        offsetY = 242;

        if (posX + boxWidth > window.innerWidth - 20) {
            posX -= boxWidth + offsetX - 10;
        }

        drivePreviewDownloadContextMenu.open({
            clientY: isMobile() ? posY- offsetY : posY - 20,
            clientX: isMobile() ? posX - 160 : posX + 10
        });

        drivePreviewDownloadContextMenu.getElement("li")[0].addEventListener("click", uploadToDrive);
        drivePreviewDownloadContextMenu.getElement("li")[1].addEventListener("click", download);
        //drivePreviewDownloadContextMenu.getElement("li")[0].hidden = true;
        event.stopPropagation();
    }

    const uploadToDrive = (file) => {
        SEND_TO_TDRIVE([{
            fileId: _getsrc().split("&")[1].substr(7),
            fileName: null,
            fileExt: null,
            user_context_1: null,
            user_context_2: null,
            user_context_3: null}]      
            // },
            // successCallback : function() {
            //     console.log('TeeDrive로 보내기에 성공했습니다');
            // },
            // cancelCallback : function() {
            //     console.log("TeeDrive로 보내기에 실패했습니다.")
        );
    }

    const download = () => {
        let wid = workspaceManager.getWorkspaceId(getRoomIdByUrl());
        let cid = workspaceManager.getChannelList(wid, "CHN0006");
        fileid= _getsrc().split("&")[1].substr(7);
        storageManager.DownloadFile(
            fileid,
            cid,
            wid,
            function() {},
            function() {},
        );
    }

    const _updateFileInfo = (storageFileInfo) => {
        if(_getDeleted()) {
            document.querySelector(".drive__preview__download-button").style.display = "none";
            document.querySelector("#drive__preview__icon").style.display = "none";
            document.querySelector("#drive__preview__filename").textContent = "삭제된 파일입니다";
        }else {
            const extensionInfo = fileExtension.getInfo(storageFileInfo.file_extension);
            const fileIcon = extensionInfo["iconImage"];
            document.getElementById("drive__preview__icon").setAttribute("src", fileIcon);
            document.getElementById("drive__preview__filename").textContent = storageFileInfo.file_name + "." + storageFileInfo.file_extension;
            document.querySelector(".drive__preview__download-button").style.display = "flex";
            document.querySelector("#drive__preview__icon").style.display = "flex";
        }
    };
    
    const _createPreviewElement = (storageFileInfo) => {
        const element = document.createElement("div");
        element.classList.add("drive__preview");

        const template = `<img id=drive__preview__icon ></img><div id="drive__preview__filename"></div>
            <span class='drive__preview__download-button icon-download'></span><span class='drive__preview__close-button icon-work_cancel'></span>
            <div class='drive__preview__content'></div>`;
        element.insertAdjacentHTML("afterbegin", template);

        if (!_getDeleted()) {
            const extensionInfo = fileExtension.getInfo(storageFileInfo.file_extension);
            const fileIcon = extensionInfo["iconImage"];
            element.querySelector("#drive__preview__icon").src = fileIcon;
            element.querySelector("#drive__preview__filename").textContent = storageFileInfo.file_name + "." + storageFileInfo.file_extension;
        } else {
            element.querySelector(".drive__preview__download-button").style.display = "none";
            element.querySelector("#drive__preview__icon").style.display = "none";
            element.querySelector("#drive__preview__filename").textContent = "손상된 파일입니다.";
        }

        element.querySelector(".drive__preview__close-button").addEventListener("click", close);
        element.querySelector(".drive__preview__download-button").addEventListener("click", download);
        return element;
    };

    const _createMediaElement = (type, src) => {
        let element = null;
        switch (type) {
            case "image":
                element = document.createElement("img");
                element.setAttribute("src", src);
                break;
            case "video":
                element = document.createElement("video");
                // element.setAttribute("src", src);
                element.setAttribute("autoplay", "autoplay");
                element.setAttribute("controls", "controls");

                element.insertAdjacentHTML("afterbegin", `<source src='${src}'></source> 지원하지 않는 동영상 파일 입니다.`);
                break;
            case "audio":
                element = document.createElement("audio");
                // element.setAttribute("src", src);
                element.setAttribute("autoplay", "autoplay");
                element.setAttribute("controls", "controls");

                element.insertAdjacentHTML("afterbegin", `<source src='${src}'></source> 지원하지 않는 오디오 파일 입니다.`);
                break;
            default:
                element = document.createElement("span");
                element.style.color = "#ffffff";
                element.textContent = "지원하지 않는 확장자 입니다.";
                break;
        }

        return element;
    };
    const _setsrc  = (src) => {
        this._src = src;
    }

    const _getsrc = () => {
        return this._src;
    }

    const _setDeleted = (isDeleted) => {
        this._isDeleted = isDeleted;
    }

    const _getDeleted = () => {
        return this._isDeleted; 
    }

    return {
        open,
        download,
        close
    };
})();

const drive = (() => {
    // 검색 필터 노드
    const _selectboxNodes = [
        {
            id: "all",
            text: "전체"
        },
        {
            id: "image",
            text: "사진"
        },
        {
            id: "video",
            text: "동영상"
        },
        {
            id: "office",
            text: "오피스"
        },
        {
            id: "etc",
            text: "기타"
        }
    ];
    const _sortSelectboxNodes = [
        {
            id: "time",
            text: "수정 날짜"
        },
        {
            id: "name",
            text: "이름"
        },
        {
            id: "size",
            text: "크기"
        }
    ];
    let _currentFilter = "all"; //all(default), image, video, office, file
    let _currentSortFilter = "time";

    /* ID */
    let _currentWorkspaceId = null; // 현재 workspace (room) id
    let _currentChannelId = null; // 현재 channel (drive) id
    let _currentFolderId = null; // 현재 folder (parent) id
    let _lastFile = null; // get해온 마지막 파일 (스크롤)
    let _lastQueryFileId = null;
    let _lastQueryTime = null;
    let _currentFolderCount = 0;
    let _currentFileCount = 0;
    let _lastFileTypeNum;
    let _sortOrder = 'desc';

    let _isScrollFilesRender = false;

    /* ELEMENT */
    let _rootElement = null; // drive root element (.drive)

    /* ARRAY */
    let _sidebarItems = []; // sidebar 그리기 위한 데이터
    let _fileList = []; // 서버에서 받아온 파일들
    let _selectedFileList = []; // 선택한 파일 쌓는 용도
    let _paths = []; // 진입한 폴더 쌓는 용도

    /* BOOLEAN */
    let _isDrive = false; // drive 또는 viewFiles
    let _isAllWorkspaceFiles = false;
    let _isGrid;
    
    if(localStorage.getItem("driveViewState") == null || localStorage.getItem("driveViewState") == "Grid") {
    	_isGrid = true;
    } else {
    	_isGrid = false;
    }
    
    let _isExpand = false; // 확장 여부

    /* TYPES */
    let _appNumber = null; // talk ("1") 또는 space ("6")

    /* UPLOAD */
    var _driveUploadTmpId = 0;
    let _numOfUploadFiles = 0;
    let _numOfUploadSuccess = 0;
    let _numOfUploadFail = 0;

    /* SEND */
    let _sendTargetFile = [];
    let _sendFileArr = [];

    const render = (workspaceId, channelId, targetSelector, isDrive) => {
        const targetElement = document.querySelector(targetSelector);
        if (targetElement) {
            _isDrive = isDrive;
            _currentWorkspaceId = workspaceId;
            _currentChannelId = channelId;
            let oldpaths = sessionStorage.getItem(getRoomIdByUrl());
            _paths = oldpaths ? JSON.parse(oldpaths) : [];
            _currentFolderId = _paths.length ? _paths[_paths.length-1].folder_id :_currentChannelId; 

            targetElement.appendChild(_createDriveTemplate());
            targetElement.classList.add("dnd__container");
            if (_isDrive) targetElement.setAttribute("data-dnd-app", "drive");
            else targetElement.setAttribute("data-dnd-app", "plus");

            _renderSidebarItems().then(() => {
                // 첫번째 클릭
                const clickEevent = new MouseEvent("click", {
                    view: window,
                    bubbles: true,
                    cancelable: false
                });

                const firstElement = document.querySelector(".drive__sidebar__item");
                if (firstElement) {
                    firstElement.dispatchEvent(clickEevent);
                }
            });
        }
    };
    
    const setDriveInfo = (workspaceId, channelId)=>{
        _currentWorkspaceId = workspaceId;
        _currentChannelId = channelId;
        _currentFolderId = _currentChannelId; 
    };

    const _renderSidebarItems = () => {
        return new Promise((resolve, reject) => {
            const frag = document.createDocumentFragment();
            // drive
            if (_isDrive) {
                // data set
                _sidebarItems = [
                    {
                        channelId: _currentChannelId,
                        appNumber: "6",
                        text: "룸 저장소"
                    },
                    {
                        channelId: _currentChannelId,
                        appNumber: "1",
                        text: "Talk 첨부함"
                    }
                ];
            } else {
                // 모아보기
                _sidebarItems = [
                    {
                        workspaceId: "all",
                        channelId: "all",
                        appNumber: "6",
                        text: "전체"
                    }
                ];
                //commonLoader.start(".drive");
                let roomList = getRoomListFromServer().ttalkMessengersList;
                if (roomList && roomList.length) {
                     let index = 2;
                     roomList.forEach((ele) => {
                        const tmp_workspaceId = ele["WS_ID"];
                        const tmp_channelId = workspaceManager.getChannelList(tmp_workspaceId, "CHN0006");
                        let tmp_ele =   {
                            workspaceId: tmp_workspaceId,
                            channelId: tmp_channelId,
                            appNumber: "6",
                            text:  ele["WS_NAME"]
                                             ? ele["WS_NAME"]
                                             : ele["User_name"]
                                             ? ele["User_name"]
                                             : userManager.getLoginUserName() + " (나)"
                        };
                        if (tmp_channelId) {
                            if(workspaceManager.getWorkspace(tmp_workspaceId).WS_TYPE == 'WKS0001')
                                _sidebarItems[1] = tmp_ele;
                            else
                                _sidebarItems[index++] = tmp_ele;
                        }
                     });
                }
            }
            // rendering
            const length = _sidebarItems.length;
            for (let i = 0; i < length; i++) {
                frag.appendChild(_createSidebarItemTemplate(_sidebarItems[i]));
            }
            _rootElement.querySelector(".drive__sidebar").appendChild(frag.cloneNode(true));

            setTimeout(() => {
                resolve();
            }, 1);
        });
    };

    const _createSidebarItemTemplate = item => {
        const itemElement = document.createElement("div");
        itemElement.setAttribute("data-channel-id", item["channelId"]);
        if (_isDrive) {
            itemElement.setAttribute("data-app-number", item["appNumber"]);
        } else {
            // 파일 모아보기 사이드
            itemElement.setAttribute("data-workspace-id", item["workspaceId"]);
            itemElement.setAttribute("data-app-number", item["appNumber"]);
        }
        itemElement.classList.add("drive__sidebar__item");

        const template =
            // `<span class='${item["icon"]}' style='font-size:1.563rem;margin-bottom:0.125rem;'>
            `<span style='font-size:1.563rem;margin-bottom:0.125rem;'><img class='drive__sidebar__image' src='./res/drive/talk_folder.svg' draggable='false'></span>
                          <span class='drive__sidebar__title talk-tooltip'>${item["text"]}</span>`;

        itemElement.insertAdjacentHTML("afterbegin", template);

        return itemElement;
    };

    const _createDriveTemplate = () => {
        _rootElement = document.createElement("div");
        _rootElement.classList.add("drive");
        _rootElement.setAttribute("data-drive-id", _currentChannelId);
        _rootElement.setAttribute("data-is-drive", _isDrive);

        const template = `<div style='display:flex;flex:auto;height:0;'>
                            <div class='drive__sidebar'></div>
                            <div class='drive__content'>
                                <div class='drive__content__row'>
                                    <div class='drive__header__column'>
                                        <div class='drive__bread-crumb'></div>
                                    </div>    
                                    <div class='drive__header__column'>
                                        <span class='drive__button--more icon-ellipsis_vertical_small'></span>
                                        <div class='drive__vertical-line'></div>
                                        <span class='drive__button--expand icon-work_expand'></span>
                                        <span class='drive__button--collapse icon-work_collapse'></span>
                                        <span class='drive__button--close icon-work_cancel'></span>
                                    </div>
                                </div>
                                <div class='drive__content__row'>
                                    <div class='drive__search'>
                                        <div class='drive__search__select' style='width: auto; margin-left: 0.125rem'>
                                        </div>
                                        <div class='drive__search__input' style='margin-left: 0.25rem; margin-right: 0.75rem'>
                                        	<span class='icon-search' style='padding-left: 0.5rem; font-size:0.8rem; color:#75757F;line-height:1.19rem;'></span>
        									<input type='text' placeholder='검색' style='padding-left: 0.3rem; border:0rem; width = 100%'/>
                                        </div>
                                    </div>
                                    <img class='drive__change-view ${
                                        _isGrid ? "drive__listlayout__button" : "drive__gridlayout__button"
                                    }'></span>
                                </div>
                                <div class='drive__content__row'>
                                    <div class='drive__select-all'>
                                        <input id='drive__select-all__check' type='checkbox' />
                                        <label for='drive__select-all__check' style='vertical-align: middle;'><span></span></label>
                                        <div class='drive__sort__vertical-line'></div>
                                    </div>
                                    <div>
                                    </div>
                                    <div class='drive__buttons'>
                                        <img class='drive__button--download'></span>                                        
                                        <img class='drive__button--delete'></span>
                                        <img class='drive__button--new-folder'></span>
                                        <img class='drive__button--upload'></span>
                                    </div>
                                </div>
                                <div class='drive__content__row'>
                                    <div class='drive__files'></div>
                                </div>
                            </div>
                        </div>`;

        // <span class='drive__button--share icon-shared'></span>

        _rootElement.insertAdjacentHTML("afterbegin", template);
        const selectbox = Top.Widget.create("top-selectbox", {
            id: "drive__search__selectbox",
            layoutHeight: "1.875rem",
            nodes: _selectboxNodes,
            selectedIndex: _selectboxNodes
                .findIndex(node => {
                    return node["id"] === _currentFilter;
                })
                .toString(),
            onChange: _onFilterChange
        }).template;
        selectbox.selectedIndex = 0;

        const sortSelectbox = Top.Widget.create("top-selectbox", {
            id: "drive__filter__selectbox",
            layoutHeight: "1.875rem",
            nodes: _sortSelectboxNodes,
            selectedIndex: _sortSelectboxNodes
                .findIndex(node => {
                    return node["id"] === _currentSortFilter;
                })
                .toString(),
            onChange: _onSortFilterChange
        }).template;
        sortSelectbox.selectedIndex = 0;

        const orderButton = document.createElement('img');
        orderButton.setAttribute('class', 'drive__button--order');
        orderButton.setAttribute('src', './res/drive/drive_back.svg');
        orderButton.setAttribute('style', 'vertical-align: middle;');

        _rootElement.querySelector(".drive__search__input input").addEventListener("keydown", _onSearchInput);
        _rootElement.querySelector(".drive__search__select").insertAdjacentElement("afterbegin", selectbox);
        _rootElement.querySelector(".drive__sidebar").addEventListener("click", _onSidebarItemClick);
        _rootElement.querySelector(".drive__change-view").addEventListener("click", _onViewChangeClick);
        _rootElement
            .querySelector(".drive__select-all input[type='checkbox']")
            .addEventListener("change", _onAllFileSelect);
            
        _rootElement.querySelector(".drive__select-all").insertAdjacentElement("beforeend", sortSelectbox);
        _rootElement.querySelector(".drive__select-all").insertAdjacentElement("beforeend", orderButton);
        _rootElement.querySelector(".drive__button--download").addEventListener("click", _onDownloadButtonClick);
        //_rootElement.querySelector(".drive__button--share").addEventListener("click", _onShareButtonClick);
        _rootElement.querySelector(".drive__button--delete").addEventListener("click", _onDeleteButtonClick);
        //_rootElement.querySelector(".drive__button--refresh").addEventListener("click", _onRefreshButtonClick);
        _rootElement.querySelector(".drive__button--new-folder").addEventListener("click", _onNewFolderButtonClick);
        _rootElement.querySelector(".drive__button--upload").addEventListener("click", _onUploadButtonClick);
        _rootElement.querySelector(".drive__button--order").addEventListener("click", _onSortOrderButtonClick);

        _rootElement.querySelector(".drive__button--download").title = "다운로드";
        _rootElement.querySelector(".drive__button--delete").title = "삭제";
        // _rootElement.querySelector(".drive__button--share").title = "전달하기";};

        const expandButton = _rootElement.querySelector(".drive__button--expand");
        const collapseButton = _rootElement.querySelector(".drive__button--collapse");
        expandButton.addEventListener("click", _onExpandButtonClick);
        collapseButton.addEventListener("click", _onCollapseButtonClick);
        _rootElement.querySelector(".drive__button--close").addEventListener("click", _onCloseButtonClick);

        _isExpand = !Top.App.current().path.includes("sub=");
        if (!_isExpand) {
            expandButton.style.display = "inline-flex";
            collapseButton.style.display = "none";
        } else {
            expandButton.style.display = "none";
            collapseButton.style.display = "inline-flex";
        }

        return _rootElement;
    };

    const _onFilterChange = (event, widget) => {
        _currentFilter = widget.getSelected().id;
        renderView();
    };

    const _onSortFilterChange = (event, widget) => {
        _currentSortFilter = widget.getSelected().id;
        renderView();
    };

    const _onSortOrderButtonClick = () => {
        if (_sortOrder === 'desc'){
            _sortOrder = 'asc';
            document.querySelector(".drive__button--order").style.transform = 'rotate(90deg)'
        }
        else if (_sortOrder === 'asc'){
            _sortOrder = 'desc'
            document.querySelector(".drive__button--order").style.transform = 'rotate(-90deg)'
        }
        renderView();
    };

    const _onSearchInput = event => {
        if (event.keyCode == 13) renderView();
    };

    const _onHeaderMoreButtonClick = event => {
        const isDisabled = !!event.target.getAttribute("disabled");
        if (isDisabled) return;
    };
    const _onExpandButtonClick = () => {
        if (_isDrive) {
            onExpandButtonClick("drive", function() {
                document.querySelector(".drive__button--expand").style.display = "none";
                document.querySelector(".drive__button--collapse").style.display = "inline-flex";
                _isExpand = true;
                _updateView(_isGrid, _isExpand);
                _updateBreadCrumb(_isExpand);
            });
        } else {
            onExpandButtonClick("plus", function() {
                document.querySelector(".drive__button--expand").style.display = "none";
                document.querySelector(".drive__button--collapse").style.display = "inline-flex";
                _isExpand = true;
                _updateView(_isGrid, _isExpand);
                _updateBreadCrumb(_isExpand);
            });
        }
    };
    const _onCollapseButtonClick = () => {
        if (_isDrive) {
            onFoldButtonClick("drive", function() {
                document.querySelector(".drive__button--expand").style.display = "inline-flex";
                document.querySelector(".drive__button--collapse").style.display = "none";
                _isExpand = false;
                _updateView(_isGrid, _isExpand);
                _updateBreadCrumb(_isExpand);
            });
        } else {
            onFoldButtonClick("plus", function() {
                document.querySelector(".drive__button--expand").style.display = "inline-flex";
                document.querySelector(".drive__button--collapse").style.display = "none";
                _isExpand = false;
                _updateView(_isGrid, _isExpand);
                _updateBreadCrumb(_isExpand);
            });
        }
    };
    const _onCloseButtonClick = () => {
        onCloseButtonClick("drive", function() {
            document.querySelector(".drive__button--expand").style.display = "inline-flex";
            document.querySelector(".drive__button--collapse").style.display = "none";
        });
    };

    const _onDownloadButtonClick = event => {
        const isDisabled = !!event.target.getAttribute("disabled");
        if (isDisabled) return;

        const isZipFile = _selectedFileList.length > 9;
        const idArr = [];
        let id = "";

        for (let i = 0; i < _selectedFileList.length; i++) {
            id = _selectedFileList[i]["storageFileInfo"]["file_id"];
            idArr.push(id);
            if (i == _selectedFileList.length - 1) {
                storageManager.DownloadFiles(
                    idArr,
                    _currentChannelId,
                    _currentWorkspaceId,
                    function() {},
                    function() {},
                    isZipFile
                );
            }
        }
    };

    const _onShareButtonClick = event => {
        const isDisabled = !!event.target.getAttribute("disabled");
        if (isDisabled) return;

        Top.Dom.selectById("drivePassFileDialog").open();
    };
    const _onDeleteButtonClick = event => {
        const isDisabled = !!event.target.getAttribute("disabled");
        if (isDisabled) return;

        Top.Dom.selectById("driveDeleteDialog").open();
        Top.Dom.selectById("driveDeleteDialog").setProperties({
            dialogFrom: "button"
        });
    };

    const _onRefreshButtonClick = event => {
        renderView();
    };

    const _onNewFolderButtonClick = event => {
        const isDisabled = !!event.target.getAttribute("disabled");
        if (isDisabled) return;

        Top.Dom.selectById("driveNewFolderDialog").open();
    };
    const _onUploadButtonClick = event => {
        const isDisabled = !!event.target.getAttribute("disabled");
        if (isDisabled) return;

        let fileChooser = Top.Device.FileChooser.create({
            onBeforeLoad: function() {
                _uploadFiles(this.file);
                return true;
            },
            multiple: true
        });

        fileChooser.show();
    };

    _onDriveBeforeLoadFiles = files => {
        let newFileList = [];
        let file = {};
        let checkFileSize = {};
        let nomFileFullName = "";
        let fileNameArray = [];
        let fileExt = "";
        let fileSize = 0;
        for (var i = 0; i < files.length; i++) {
            file = files[i];

            nomFileFullName = Top.Util.Browser.isIE() ? file.name : file.name.normalize("NFC");
            fileNameArray = nomFileFullName.split(".");
            fileExt = fileNameArray.length == 1 ? "" : fileNameArray[fileNameArray.length - 1];
            fileSize = file.size;

            if (fileExt == "") {
                return {
                    result: false,
                    message: "확장자가 없는 파일은 업로드 할 수 없습니다."
                };
            }

            if (fileSize > 200 * 1024 * 1024) {
                return {
                    result: false,
                    message: "200MB 이상의 파일은 업로드 할 수 없습니다."
                };
            }

            let fileName = nomFileFullName.substring(0, nomFileFullName.lastIndexOf(fileExt) - 1);

            newFileList.push({
                file_id: _driveUploadTmpId++,
                iconClassName: getFileIconClass(fileExt),
                file_extension: fileExt,
                fileFullName: nomFileFullName,
                file_status: FileStatus.TRY_UPLOAD,
                is_file: true
            });
        }

        return {
            result: true,
            newFileList: newFileList
        };
    };

    const _uploadFiles = files => {
        let beforeCheck = _onDriveBeforeLoadFiles(files);
        if (!beforeCheck.result) {
            TeeAlarm.open({ title: "업로드 실패", content: beforeCheck.message });
            return;
        }

        let inputDTO = {};
        let file = {};
        let nomFileFullName = String.empty;
        let fileNameArray = [];
        let fileName = String.empty;
        let fileExt = String.empty;
        let folderid = _currentFolderId;
        let filepath = _paths;

        uploadStatusPopup.open();
        uploadStatusPopup.expand();

        let isUploading = _numOfUploadFiles > _numOfUploadSuccess + _numOfUploadFail;
        if (isUploading) {
            _numOfUploadFiles += beforeCheck.newFileList.length;
        } else {
            _numOfUploadFiles = beforeCheck.newFileList.length;
            _numOfUploadSuccess = 0;
            _numOfUploadFail = 0;
        }

        uploadStatusPopup.updateCount("progress", _numOfUploadFiles);
        uploadStatusPopup.updateUploadText("개 항목 업로드 중");
        uploadStatusPopup.updateCount("success", _numOfUploadSuccess);

        let tmpId = -1;
        let _successCallback = function(res) {
            let tmpId = res.dto.storageFileInfoList[0].user_context_1;
            let realFileId = res.dto.storageFileInfoList[0].file_id;
            let storageFileUpdateUrl = String.format("{0}Storage/StorageMeta", _workspace.url);
            let storageFileUpdateDto = _driveStorageUpdateDTO(realFileId, tmpId, true.toString());
            let popupcontainer = document.querySelector(`[data-item-id='${tmpId}']`);
            let iconcontainer = popupcontainer.querySelector(".upload__status__icon");
            Top.Ajax.put(storageFileUpdateUrl, storageFileUpdateDto, undefined, function(res2) {
                let result = JSON.parse(res2);
                if (result.dto.resultMsg === "Success"){
                    let fileNameSpan = popupcontainer.querySelector(".upload__file__name");
                    let fileId = realFileId;
                    let channelId = _currentChannelId;
                    let workspaceId = _currentWorkspaceId;
                    fileNameSpan.addEventListener("click", () => {
                        const storageFileInfo = result.dto.storageFileInfoList[0];
                        if (storageFileInfo.extension !== null){
                            const extension = storageFileInfo.file_extension
                                ? storageFileInfo.file_extension.toLowerCase()
                                : "";
                            if (_isOfficeFile(extension)) {
                                const data = {
                                    fileId: fileId,
                                    fileName: storageFileInfo.file_name,
                                    fileExtension: extension
                                };
                                officeManager.callOffice(data);
                            } else {
                                const extensionInfo = fileExtension.getInfo(extension);
                                if (extensionInfo["isPreview"]) {
                                    if (extension === "pdf") {
                                        storageManager.getDownloadFileInfo(
                                            fileId,
                                            channelId,
                                            workspaceId,
                                            res => {
                                                window.open(res.url);
                                            },
                                            () => {}
                                        );
                                    } else {
                                        drivePreview.open(
                                            storageFileInfo,
                                            extensionInfo["type"],
                                            storageManager.GetFileUrl(fileId, channelId, workspaceId)
                                        );
                                    }
                                } else {
                                    storageManager.DownloadFiles(
                                        [fileId],
                                        channelId,
                                        workspaceId,
                                        () => {},
                                        () => {},
                                        false
                                    );
                                }
                            }
                        }
                        else{
                            //폴더 업로드 미구현
                        }
                    });
                    fileNameSpan.addEventListener("mouseenter", () => {
                        fileNameSpan.style.textDecoration = "underline";
                        fileNameSpan.style.cursor = "pointer";
                    });
                    fileNameSpan.addEventListener("mouseout", () => {
                        fileNameSpan.style.textDecoration = "none";
                    });
                    renderView();
                }
            });
            _numOfUploadSuccess++;
            uploadStatusPopup.updateCount("success", _numOfUploadSuccess);
            uploadStatusPopup.update(tmpId, FileStatus.UPLOAD_SUCCESS);
            iconcontainer.addEventListener("mouseenter", () => {
                iconcontainer.classList.remove("icon-check");
                iconcontainer.classList.add("icon-work_folder_file_location");
            });
            iconcontainer.addEventListener("mouseout", () => {
                iconcontainer.classList.remove("icon-work_folder_file_location");
                iconcontainer.classList.add("icon-check");
            });
            iconcontainer.addEventListener("click", () => {
                if(appManager.getSubApp() === "drive") {
                    _paths = filepath;
                    const length = _paths.length;
                    let idx = -1;
                    for (let i = 0; i < length; i++) {
                        if (_paths[i]["folder_id"] === folderid) {
                            idx = i;
                            break;
                        }
                    }

                    if (idx > -1) {
                        _paths = _paths.slice(0, idx + 1);
                        _currentFolderId = folderid;
                        if (_paths.length == 1) {
                            _currentChannelId = _currentFolderId;
                        }
                        renderView();
                    }
                }
            });

            if (_numOfUploadFiles == _numOfUploadSuccess + _numOfUploadFail) {
                uploadStatusPopup.updateUploadText("개 항목 업로드 완료");
            }

            setTimeout(function() {
                //if(appManager.getSubApp() == "drive") {
                    uploadStatusPopup.remove(tmpId);
                    if (uploadStatusPopup.lengthCheck() == 0) {
                        uploadStatusPopup.close();
                    }
                //}
            }, 5000);
        };

        let _errorCallback = function(res) {
            //let tmpId = res.dto.storageFileInfoList[0].user_context_1;
            let popupcontainer = document.querySelector(`[data-item-id='${tmpId}']`);
            let iconcontainer = popupcontainer.querySelector(".upload__status__icon");
            _numOfUploadFail++;
            uploadStatusPopup.update(tmpId, FileStatus.UPLOAD_FAIL);

            if (_numOfUploadFiles == _numOfUploadSuccess + _numOfUploadFail) {
                uploadStatusPopup.updateUploadText("개 항목 업로드 완료");
            }
            iconcontainer.addEventListener("mouseenter", () => {
                iconcontainer.classList.remove("icon-chart_error");
                iconcontainer.classList.add("icon-work_restartsvg");
                iconcontainer.title = "다시 시도";
            });
            iconcontainer.addEventListener("mouseout", () => {
                iconcontainer.classList.remove("icon-work_restartsvg");
                iconcontainer.classList.add("icon-chart_error");
            });
            iconcontainer.addEventListener("click", () => {
                uploadStatusPopup.remove(tmpId);
                _uploadFiles([file]);
            });
        };
        spaceAPI.spaceInit();
        for (var i = 0; i < beforeCheck.newFileList.length; i++) {
            successCount = 0;
            failCount = 0;
            file = files[i];
            if (file.size > 200 * 1024 * 1024) {
                // 200MB 이상 업로드 제한
                continue;
            }

            let fileFolder;
            if (Top.Dom.selectById("driveNewFolderDialog").getProperties("from") == "attachDialog") {
                fileFolder = driveAttach.getFileList();
            } else {
                fileFolder = drive.getFileList();
            }
        
            let targetFileList = [];
            for (let i = 0; i < fileFolder.length; i++) {
                if (fileFolder[i].is_folder === 'false' && 
                    fileFolder[i].storageFileInfo.file_name + '.' + fileFolder[i].storageFileInfo.file_extension === file.name) {
                    targetFileList.push(fileFolder[i].storageFileInfo.file_id);
                }
            }
            nomFileFullName = Top.Util.Browser.isIE() ? file.name : file.name.normalize("NFC");
            fileNameArray = nomFileFullName.split(".");
            fileExt = fileNameArray[fileNameArray.length - 1];
            fileName = String.isNullOrEmpty(fileExt)
                ? nomFileFullName
                : nomFileFullName.substring(0, nomFileFullName.lastIndexOf(fileExt) - 1);
     
            tmpId = beforeCheck.newFileList[i].file_id;
            if (fileName.length > 70) {
                uploadStatusPopup.insert(tmpId, fileExt, fileName);
                uploadStatusPopup.update(tmpId, FileStatus.UPLOAD_FAIL);
                _numOfUploadFail++;
                continue;
            }
            inputDTO = _driveUploadFileDTO(fileName, fileExt, file.size, tmpId);

            let uploadCallback = function () {
                storageManager.UploadFile(file, inputDTO, "File", "DriveFile", _successCallback, _errorCallback);
                uploadStatusPopup.insert(tmpId, fileExt, fileName);
            };

            if (targetFileList.length != 0) {
                const config = {
                    isFrom: "uploadDialog",
                    target: targetFileList,
                    newName: fileName,
                    overWriteCallback: uploadCallback
                };
                openDriveOverWriteDialog(config);
            } else {               
                uploadCallback();
            }
        }
    };

    const getRoomListFromServer = () => {
        //        //const myId = userManager.getLoginUserId();
        //        //return axios.get(`${_workspace.url}SpaceRoom/SpaceRoomList?USER_ID=${myId}`);
        //        return new Promise((resolve, reject) => {
        //            const roomList = talkServer2.getMessengerRooms(userManager.getLoginUserId());
        //            if (roomList)
        //                resolve(roomList);
        //            else
        //                reject();
        //        });
        return talkServer2.getMessengerRooms(userManager.getLoginUserId());
    };

    const getWorkspaceId = () => {
        return _currentWorkspaceId;
    };

    const getChannelId = () => {
        return _currentChannelId;
    };

    const getChannelList = roomList => {
        // 임시 코드.
        // 공통팀한테 만들어 달라고 하자.
        // user id 로 채널 리스트 얻어올수 있는 거.

        const channelList = [];
        if (roomList && roomList.constructor.name === "Array") {
            const length = roomList.length;
            for (let i = 0; i < length; i++) {
                const roomName = roomList[i]["WS_NAME"] ? roomList[i]["WS_NAME"] : "이름없음";

                if (roomList[i]["WsChList"] && roomList[i]["WsChList"].constructor.name === "Array") {
                    const chLength = roomList[i]["WsChList"].length;
                    for (let j = 0; j < chLength; j++) {
                        if (roomList[i]["WsChList"][j]["CH_TYPE"] === "CHN0006")
                            channelList.push({
                                name: roomName,
                                channel: roomList[i]["WsChList"][j]
                            });
                    }
                }
            }
        }

        return channelList;
    };

    const _getFileList = (order, folderId, appNumber, filter, searchStr, fileID, time, is_folder, lastFileTypeNum) => {
        // default. 50개씩 가져옴
        let serviceUrl = `${_workspace.fileUrl}DriveList?directoryID=${folderId}&userID=${userManager.getLoginUserId()}&type=${filter.currentFilter}&rownum=50`;
        serviceUrl += `&orderBy=${filter.currentSortFilter}`;
        order = order || "desc";
        serviceUrl += `&order=${order}`;
        if (searchStr) {
            serviceUrl += `&query=${searchStr.toLowerCase()}`;
        }
       if (fileID && time) {
           serviceUrl += `&fileID=${fileID}`;
           serviceUrl += `&time=${time}`;
       }
        if (appNumber) {
            serviceUrl += `&appNumber=${appNumber}`;
        }
        if (is_folder){
            let isFile = is_folder === "true" ? "folder" : "file";
            serviceUrl += `&lastFileType=${isFile}`;
        }
        if (lastFileTypeNum !== undefined) {
            serviceUrl += `&lastFileTypeNum=${lastFileTypeNum}`;
        }
        return axios.get(serviceUrl);
    };

    const _onSidebarItemClick = event => {
        const item = event.target.closest(".drive__sidebar__item");
        _isAllWorkspaceFiles = item.innerText && item.innerText == "전체" ? true : false;
        // 검색어 초기화
        document.querySelector(".drive__search__input input").value = "";
        document.querySelector(".drive__files").scrollTo(0,0);

        // 모아보기에만 sidebar에 workspace가 있음.
        if (!_isDrive) {
            _currentWorkspaceId = item.getAttribute("data-workspace-id");
        }
        _currentChannelId = item.getAttribute("data-channel-id");       

        let appNumber = null;
        appNumber = item.getAttribute("data-app-number");
        _appNumber = appNumber;

        let sideBarImageTag = document.getElementsByClassName("drive__sidebar__image");
        
        if (_isDrive) {
            if (_appNumber === "1") {  //talk 첨부함
                _paths = [];
                _currentFolderId = _currentChannelId;
                let space = workspaceManager.getWorkspaceId(getRoomIdByUrl());
                let spaceInfo = spaceManager.getSpaceProfile(space);
                let talkFolderName = 'Root';
                /*
                    getRoomIdByUrl() === workspaceManager.getMySpaceUrl() ||
                    getRoomIdByUrl() === workspaceManager.getMySpaceId()
                        ? userManager.getLoginUserName() + " (나)"
                        : spaceInfo
                        ? spaceInfo.SPACE_NAME
                        : "";
                */
                _paths.push({
                    folder_id: _currentFolderId,
                    folder_name: talkFolderName
                });

                if (_isDrive) {
                    sideBarImageTag[0].src = "./res/drive/space_folder.png";
                    sideBarImageTag[1].src = "./res/drive/talk_folder_active.svg";
                }
            } else if (_appNumber === "6") {  //룸 저장소
                let oldpaths = sessionStorage.getItem(getRoomIdByUrl());
                _paths = oldpaths ? JSON.parse(oldpaths) : [];
                _currentFolderId = _paths.length ? _paths[_paths.length-1].folder_id :_currentChannelId; 
                if (_paths.length === 0){
                    _paths.push({
                        folder_id: _currentChannelId,
                        folder_name: "Root"
                    });
                }

                if (_isDrive) {
                    sideBarImageTag[0].src = "./res/drive/space_folder_active.png";
                    sideBarImageTag[1].src = "./res/drive/talk_folder.svg";
                }
            }
        } else {
            // 파일 모아보기
            _paths = [];
            _currentFolderId = _currentChannelId;
            _paths.push({
                folder_id: _currentFolderId,
                folder_name: $(item)
                    .text()
                    .trim()
            });
        }

        const selectedItem = document.querySelector(".drive__sidebar__item--selected");
        if (selectedItem) {
            selectedItem.classList.remove("drive__sidebar__item--selected");
        }
        item.classList.add("drive__sidebar__item--selected");

        renderView();
    };

    const renderView = () => {
        const textElement = document.querySelector(".drive__search__input input");
        const searchStr = textElement ? textElement.value : "";
        const oldApp = getSubAppByUrl() || getMainAppByUrl();

        $(".drive__files").on("scroll", function() {
            let filesElement = _rootElement.querySelector(".drive__files");
            let scrollT = filesElement.scrollTop;
            let scrollH = filesElement.scrollHeight;
            let contentH = filesElement.offsetHeight;

            if (scrollT + contentH >= scrollH && !_isScrollFilesRender) {
                _isScrollFilesRender = true;
                _onScrollContainer();
            }
        });

        const filter = {
            currentFilter : _currentFilter,
            currentSortFilter : _currentSortFilter
        };
        _currentFolderCount = 0;
        _currentFileCount = 0;
        _getFileList(_sortOrder, _currentFolderId, _appNumber, filter, searchStr)
            .then(res => {
                const currentApp = getSubAppByUrl() || getMainAppByUrl();
                if (oldApp !== currentApp){ // 파일 리스트가 늦게 온 경우
                    return ;
                }

                _fileList = [];
                tempList = res.data.dto.teeDriveDtoList;
                for (let i = 0; i < tempList.length; i++) {
                    let tmpFile = tempList[i];
                    if (tmpFile.storageFileInfo.user_context_2 != false.toString()) {
                        _fileList.push(tempList[i]);
                        tmpFile.is_folder === "true" ? _currentFolderCount++ : _currentFileCount ++; 
                    }
                }

                if (_fileList.length) {
                    // 맨 마지막 폴더 또는 파일 저장
                    _lastFile = _fileList[_fileList.length - 1];

                    if (_lastFile.is_folder == "true") {
                        _lastQueryFileId = _lastFile.file_id;
                        _lastQueryTime = _lastFile.updated_time;
                        _lastFileTypeNum = _currentFolderCount;
                    } else {
                        _lastQueryFileId = _lastFile.storageFileInfo.file_id;
                        _lastQueryTime = _lastFile.storageFileInfo.file_updated_at;
                        _lastFileTypeNum = _currentFileCount;
                    }
                }
                // 버튼 초기화
                _updateButtons(false, false, false);

                _renderFiles(_fileList);
                _updateBreadCrumb(_isExpand);
            })
            .catch(err => {})
            .then(() => {
                if (_currentFolderId == "all") {
                    _currentChannelId = "all";
                    _currentWorkspaceId = "all";
                }
                dnd.update();
                talkTooltip.activateTooltip("body");
            });
    };

    const _onScrollContainer = () => {
       
        const textElement = document.querySelector(".drive__search__input input");
        const searchStr = textElement ? textElement.value : "";
        const filter = {
            currentFilter : _currentFilter,
            currentSortFilter : _currentSortFilter
        };
        _getFileList(_sortOrder, _currentFolderId, _appNumber, filter, searchStr, _lastQueryFileId, _lastQueryTime, _lastFile.is_folder, _lastFileTypeNum)
            .then(res => {
                newList = [];                
                tempList = res.data.dto.teeDriveDtoList;
                for (let i = 0; i < tempList.length; i++) {
                    tmpFile = tempList[i];
                    if (tmpFile.storageFileInfo.user_context_2 != false.toString()){
                        newList.push(tmpFile);
                        _fileList.push(tmpFile);
                        tmpFile.is_folder === "true" ? _currentFolderCount++ : _currentFileCount++; 
                    }
                }

                if (_fileList.length) {
                    _lastFile = _fileList[_fileList.length - 1];
                    if (_lastFile.is_folder == "true") {
                        _lastQueryFileId = _lastFile.file_id;
                        _lastQueryTime = _lastFile.updated_time;
                        _lastFileTypeNum = _currentFolderCount;
                    } else {
                        _lastQueryFileId = _lastFile.storageFileInfo.file_id;
                        _lastQueryTime = _lastFile.storageFileInfo.file_updated_at;
                        _lastFileTypeNum = _currentFileCount;
                    }
                    _renderAppendFiles(newList);
                }
            })
            .then(() => {
                _isScrollFilesRender = false;
            });
        //  }
    };

    const _updateBreadCrumb = (isExpand) => {
        const length = _paths.length;
        const breadCrumb = document.querySelector(".drive__bread-crumb");

        let template = "";
        if (breadCrumb) {
            while (breadCrumb.hasChildNodes()) {
                breadCrumb.removeChild(breadCrumb.firstChild);
            }
            if (isExpand === false && length > 3) {
                template += `<span class='drive__bread-crumb__item' data-path-type='item' data-folder-id='${_paths[0]["folder_id"]}'>${_paths[0]["folder_name"]}</span>`; 
                template += `<span class='drive__bread-crumb__divider'></span>`;
                template += `<span class='drive__bread-crumb__item' data-path-type='ellipsis'>...</span>`;
                template += `<span class='drive__bread-crumb__divider'></span>`;
                template += `<span class='drive__bread-crumb__item' data-path-type='item' data-folder-id='${_paths[length-1]["folder_id"]}'>${_paths[length-1]["folder_name"]}</span>`; 
            }
            else if (isExpand === true && length > 4){
                template += `<span class='drive__bread-crumb__item' data-path-type='item' data-folder-id='${_paths[0]["folder_id"]}'>${_paths[0]["folder_name"]}</span>`; 
                template += `<span class='drive__bread-crumb__divider'></span>`;
                template += `<span class='drive__bread-crumb__item' data-path-type='ellipsis'>...</span>`; 
                template += `<span class='drive__bread-crumb__divider'></span>`;
                template += `<span class='drive__bread-crumb__item' data-path-type='item' data-folder-id='${_paths[length-2]["folder_id"]}'>${_paths[length-2]["folder_name"]}</span>`; 
                template += `<span class='drive__bread-crumb__divider'></span>`;
                template += `<span class='drive__bread-crumb__item' data-path-type='item' data-folder-id='${_paths[length-1]["folder_id"]}'>${_paths[length-1]["folder_name"]}</span>`; 
            }
            else{
                for (let i = 0; i < length; i++) {
                    template += `<span class='drive__bread-crumb__item' data-path-type='item' data-folder-id='${_paths[i]["folder_id"]}'>${_paths[i]["folder_name"]}</span>`;
                    if (i < length - 1) {
                        template += `<span class='drive__bread-crumb__divider'></span>`;
                    }
                }
            }
            template += "</div>";
            breadCrumb.insertAdjacentHTML("afterbegin", template);
            breadCrumb.addEventListener("click", _onBreadCrumbClick);
        }
    };

    const _onBreadCrumbClick = event => {
        const target = event.target.closest(".drive__bread-crumb__item");

        if (target) {
            if (target.getAttribute("data-path-type") === "item") {
                const folderId = target.getAttribute("data-folder-id");
                _onBreadCrumbItemClick(folderId);
            } else if (target.getAttribute("data-path-type") === "ellipsis") {
                _onBreadCrumbPathClick();
            }
        }
    };

    const _onBreadCrumbItemClick = folderId => {
        const length = _paths.length;
        let idx = -1;
        for (let i = 0; i < length; i++) {
            if (_paths[i]["folder_id"] === folderId) {
                idx = i;
                break;
            }
        }

        // 마지막 클릭이면
        if (idx === _paths.length - 1) {
            return;
        }

        if (idx > -1) {
            _paths = _paths.slice(0, idx + 1);
            _currentFolderId = folderId;
            if (_paths.length == 1) {
                _currentChannelId = _currentFolderId;
            }
            renderView();
        }

        // 툴팁 제거
        if (document.getElementsByClassName("tooltip").length != 0) {
            document.getElementsByClassName("tooltip")[0].classList.remove("tooltip");
        }
    };

    const _onBreadCrumbPathClick = () => {
        const length = _paths.length;
        const renderData = [];
        if (length === 0) return;
        _paths.forEach((path, index) => {
            if (index > 0 && index < length - 1)
                renderData.push({
                    text: path["folder_name"],
                    value: path["folder_id"],
                    onclick: function() {
                        _onBreadCrumbItemClick(this.value);
                    }
                });
        });

        const css = {
            position: "fixed",
            left: event.clientX + "px",
            top: event.clientY + "px"
        };
        talkContextMenu.render(
            renderData,
            ".drive",
            css,
            function() {},
            function() {}
        );
        if (document.getElementsByClassName("context-menu__item")) {
            document.getElementsByClassName("context-menu__item")[0].classList.add("talk-tooltip");
        }
    };

    const _createPrevFolderTemplate = () => {
        const prevFolder = document.createElement("div");
        prevFolder.classList.add("file__item");
        prevFolder.classList.add("file__item__prev");
        prevFolder.classList.add("dnd__container");
        prevFolder.setAttribute("data-file-id", _paths[_paths.length - 2]["folder_id"]);
        prevFolder.setAttribute("data-is-folder", "true");
        prevFolder.setAttribute("data-filter-type", "always");
        prevFolder.setAttribute("data-dnd-app", "drive");
        
        const extensionInfo = fileExtension.getInfo("folder");
        const fileIcon = extensionInfo["iconImage"];

        template = `<div class='file__item__icon'>
                        <img src='${fileIcon}' draggable='false'/>
                    </div>
                    <span class='file__item__title'>...</span>
                    <span class='file__item__time'></span>
                    <span class='file__item__size'></span>`;

        prevFolder.insertAdjacentHTML("afterbegin", template);

        return prevFolder;
    };

    const _onViewChangeClick = event => {
        if (_isGrid == true) {
            _isGrid = false;
            localStorage.setItem("driveViewState", "List");
        } else {
            _isGrid = true;
            localStorage.setItem("driveViewState", "Grid");
        }

        const icon = event.target;
        if (_isGrid) {
            icon.classList.remove("drive__gridlayout__button");
            icon.classList.add("drive__listlayout__button");
        } else {
            icon.classList.remove("drive__listlayout__button");
            icon.classList.add("drive__gridlayout__button");
        }

        _updateView(_isGrid, _isExpand);
    };

    const _createFileTemplate = file => {
        const isFolder = file["is_folder"] === "true";

        const fileId = isFolder ? file["file_id"] : file["storageFileInfo"]["file_id"];
        const extension = isFolder ? null : file["storageFileInfo"]["file_extension"].toLowerCase();
        //let name = isFolder ? file["folder_name"] : file["storageFileInfo"]["file_name"] + "." + extension;

        const textElement = document.querySelector(".drive__search__input input");
        const searchStr = textElement ? textElement.value.toLowerCase() : "";

        let fileName = isFolder ? file["folder_name"] : file["storageFileInfo"]["file_name"];
        let beginIdx = fileName.toLowerCase().indexOf(searchStr);
        let colorStr = fileName.substr(beginIdx, searchStr.length);
        let colorName = fileName.replace(
            colorStr,
            String.format("<span class='drive-font-convert'>{0}</span>", colorStr)
        );
        let name = isFolder ? colorName : colorName + "." + extension;

        const lastUpdateUser = isFolder ? EM_DASH : file["storageFileInfo"]["file_last_update_user_id"];
        const updatedAt = isFolder
            ? getDriveFileDateFormat(file["updated_time"])
            : getDriveFileDateFormat(file["storageFileInfo"]["file_updated_at"]);
        const size = isFolder ? EM_DASH : getFileSizeText(file["storageFileInfo"]["file_size"]);

        const fileItem = document.createElement("div");

        const extensionInfo = fileExtension.getInfo(isFolder ? "folder" : extension);
        const fileType = extensionInfo["type"];
        const fileIcon = extensionInfo["iconImage"];

        fileItem.classList.add("file__item");
        fileItem.classList.add("dnd__item");
        if (isFolder == true) {
            fileItem.classList.add("dnd__container");
            fileItem.setAttribute("data-dnd-app", "drive");
        }
        // [모아보기-전체] 에서 받아온 파일은 각자 ch, ws id를 가지고 있음
        //if(file["workspace_id"] && file["ch_id"]) {
        if (_currentFolderId == "all") {
            _currentWorkspaceId = file["workspace_id"];
            _currentChannelId = file["ch_id"];
            fileItem.setAttribute("data-workspace-id", _currentWorkspaceId);
            fileItem.setAttribute("data-channel-id", _currentChannelId);
        }

        fileItem.setAttribute("data-file-id", fileId);
        fileItem.setAttribute("data-is-folder", isFolder);
        fileItem.setAttribute("data-filter-type", fileType);

        // 이미지 일 경우.
        let imageUrl = null;
        if (fileType === "image") {
            imageUrl = storageManager.GetFileUrl(fileId, _currentChannelId, _currentWorkspaceId);
            // if(_isAllWorkspaceFiles && _paths.length == 1) {
            //     imageUrl = storageManager.GetFileUrl(fileId, tmp_channelId, tmp_workspaceId);
            // }
            // else {
            //     imageUrl = storageManager.GetFileUrl(fileId, _currentChannelId, _currentWorkspaceId);
            // }
            fileItem.setAttribute("data-image-url", imageUrl);
        }

        const template = `<input id='checkbox-${fileId}' type='checkbox' />
                          <label for='checkbox-${fileId}'><span></span></label>
                          <div class='file__item__icon'>
                            <img src='${fileIcon}' draggable='false'/>
                          </div>
                          <div class='file__item__text-container'>
                            <span class='file__item__title talk-tooltip'>${name}</span>
                            <span class='file__item__more icon-ellipsis_vertical_small'></span>
                            <span class='file__item__time'>${updatedAt}</span>
                            <span class='file__item__last-update-user'>${lastUpdateUser}</span>
                            <span class='file__item__size'>${size}</span>
                          </div>`;

        fileItem.insertAdjacentHTML("afterbegin", template);

        return fileItem;
    };

    const _renderFiles = files => {
        const length = files.length;

        const textElement = document.querySelector(".drive__search__input input");
        const searchStr = textElement ? textElement.value : "";
        const container = document.querySelector(".drive__files");
        const allCheckBox = document.querySelector("#drive__select-all__check");

        if (container) {
            while (container.hasChildNodes()) {
                container.removeChild(container.firstChild);
            }

            const frag = document.createDocumentFragment();

            if (!_isRoot() && _paths.length > 1 && searchStr === "") {
                frag.appendChild(_createPrevFolderTemplate());
            }

            if (length) {
                if (allCheckBox) {
                    allCheckBox.removeAttribute("disabled");
                }
                for (let i = 0; i < length; i++) {
                    frag.appendChild(_createFileTemplate(files[i]));
                }
            } else {
                if (allCheckBox) {
                    allCheckBox.setAttribute("disabled", "");
                }

                if (_isRoot() && _appNumber == "1" && searchStr == "") {
                    // empty 레이아웃 출력
                    frag.appendChild(_createTalkEmptyFileElement());
                } else if (_isRoot() && _appNumber == "6" && searchStr == "") {
                    frag.appendChild(_createDriveEmptyFileElement());
                } else if (_isRoot && searchStr != "") {
                    frag.appendChild(_createSearchNoFileElement(searchStr));
                }
            }

            container.appendChild(frag.cloneNode(true));
            _updateView(_isGrid, _isExpand);

            container.addEventListener("click", _onFileClick);
            container.addEventListener("click", _onMoreButtonClick);
            container.addEventListener("change", _onFileSelect);
        }
    };

    const _renderAppendFiles = files => {
        const length = files.length;

        const container = document.querySelector(".drive__files");

        if (container) {
            const frag = document.createDocumentFragment();

            if (length) {
                for (let i = 0; i < length; i++) {
                    frag.appendChild(_createFileTemplate(files[i]));
                }
            }

            container.appendChild(frag.cloneNode(true));
            _updateView(_isGrid, _isExpand);
        }
    };

    const _createTalkEmptyFileElement = () => {
        const rootElement = document.createElement("div");
        rootElement.classList.add("drive__empty");

        const template = `<img src='./res/img_no_file.png' draggable='false'/>
                          <span style='font-size:0.94rem;font-weight:bold;color:#000000;margin-bottom:0.75rem;'>파일이 없습니다.</span>
                          <span style='font-size:0.75rem;color:#777777;margin-bottom:0.1rem'>채팅방에서 주고 받은 파일을 여기서 모아보고,</span>
                          <span style='font-size:0.75rem;color:#777777;'>간편하게 관리할 수 있습니다.</span>`;

        rootElement.insertAdjacentHTML("afterbegin", template);

        return rootElement;
    };

    const _createDriveEmptyFileElement = () => {
        const rootElement = document.createElement("div");
        rootElement.classList.add("drive__empty");

        const template = `<img src='./res/img_no_file.png' draggable='false'/>
                          <span style='font-size:0.94rem;font-weight:bold;color:#000000;margin-bottom:0.75rem;'>파일이 없습니다.</span>
                          <span style='font-size:0.75rem;color:#777777;margin-bottom:0.1rem'>룸 멤버들과 함께 파일을 공유할 수 있는</span>
                          <span style='font-size:0.75rem;color:#777777;'>공용 저장 공간입니다.</span>`;

        rootElement.insertAdjacentHTML("afterbegin", template);

        return rootElement;
    };

    const _createSearchNoFileElement = (searchStr) => {
        const rootElement = document.createElement("div");
        rootElement.classList.add("drive__search__empty");

        const template = `<span style='font-size:0.9375rem;color:#000000;margin-bottom:0.6875rem'>'${searchStr}'</span>
                            <span style='font-size:0.75rem;color:#777777;margin-bottom:1.3125rem'>검색 결과가 없습니다.</span>
        				    <img src='./res/note/img_no_results.png' draggable='false' style='opacity:0.5'/>`;

        rootElement.insertAdjacentHTML("afterbegin", template);

        return rootElement;
    };

    const _createErrorElement = () => {
        const rootElement = document.createElement("div");
        rootElement.classList.add("drive__error");

        const template = `<img src='./res/note/deleted_note.svg' draggable='false'/>
                          <span style='font-size:0.94rem;font-weight:bold;color:#000000;margin-bottom:0.75rem;'>서버 에러</span>`;

        rootElement.insertAdjacentHTML("afterbegin", template);

        return rootElement;
    };

    // root 폴더 인지 확인
    const _isRoot = () => {
        return _currentChannelId === _currentFolderId;
    };

    const _onMoreButtonClick = event => {
        if (event.target.classList.contains("file__item__more")) {
            const target = event.target.closest(".file__item");
            const fileId = target.getAttribute("data-file-id");

            let selectedFile = findFile(fileId);

            let offsetX = 0;
            let offsetY = 0;
            let boxWidth = 133;
            let boxHeight = 214;
            var posX = event.clientX;
            var posY = event.clientY;

            offsetY = 242;

            if (posX + boxWidth > window.innerWidth - 20) {
                posX -= boxWidth + offsetX - 10;
            }

            if (_appNumber == "6") {
                if (posY + boxHeight > window.innerHeight) {
                    posY -= offsetY - 105;
                }
            }

            if (selectedFile["is_folder"] === "false") {
                let driveFileContextMenu = Top.Dom.selectById("driveFileContextMenu");
                driveFileContextMenu.open({
                    clientY: isMobile() ? event.clientY - offsetY : posY - 20,
                    clientX: isMobile() ? event.clientX - 160 : posX + 10
                });

                if (_isOfficeFile(selectedFile.storageFileInfo.file_extension) == false) {
                    driveFileContextMenu.getElement("li")[0].hidden = true;
                }

                // 일단 숨김(추후 연계기능 추가)
                driveFileContextMenu.getElement("li")[2].hidden = true;
                driveFileContextMenu.getElement("li")[4].hidden = true;

                if (_appNumber == "1") {
                    // Talk에서는 이름 변경, 삭제 불가
                    driveFileContextMenu.getElement("li")[5].hidden = true;
                    driveFileContextMenu.getElement("li")[6].hidden = true;
                }

                driveFileContextMenu.setProperties({
                    fileInfo: selectedFile
                    //'on-close' : 'driveContextMenuRemoveId'
                });
            } else {
                driveFolderContextMenu = Top.Dom.selectById("driveFolderContextMenu");
                driveFolderContextMenu.open({
                    clientY: isMobile() ? event.clientY - offsetY : posY - 20,
                    clientX: isMobile() ? event.clientX - 160 : posX + 10
                });

                driveFolderContextMenu.setProperties({
                    fileInfo: selectedFile
                    //'on-close' : 'driveContextMenuRemoveId'
                });
            }

            event.stopPropagation();
        }
    };

    const findFile = fileId => {
        const length = _fileList.length;
        for (let i = 0; i < length; i++) {
            if (_fileList[i]["is_folder"] === "true") {
                if (_fileList[i]["file_id"] === fileId) {
                    return _fileList[i];
                }
            } else {
                if (_fileList[i]["storageFileInfo"]["file_id"] === fileId) {
                    return _fileList[i];
                }
            }
        }

        return null;
    };

    const _onFileClick = event => {
        //전체 모아보기 파일클릭시에 데이터 넘겨줌 (체크박스, 더보기 아이콘 등)
        //if (_isAllWorkspaceFiles && _paths.length == 1) {
        if (_currentFolderId == "all" && event.target.closest(".file__item")) {
            _currentWorkspaceId = event.target.closest(".file__item").getAttribute("data-workspace-id");
            _currentChannelId = event.target.closest(".file__item").getAttribute("data-channel-id");
        }
        // 아이콘, 이름에만 반응
        if (event.target.closest(".file__item__icon") || event.target.classList.contains("file__item__title")) {
            if (document.getElementsByClassName("tooltip").length != 0) {
                document.getElementsByClassName("tooltip")[0].classList.remove("tooltip"); //툴팁있으면, 제거
            }
            const fileItem = event.target.closest(".file__item");
            if (fileItem) {
                // ...뒤로가기 -> [모아보기 - 전체 루트 바로 위]
                if (_isAllWorkspaceFiles && !_currentWorkspaceId && !_currentChannelId && _paths.length == 2) {
                    _currentWorkspaceId = "all";
                    _currentChannelId = "all";
                    _currentFolderId = "all";
                }
                const fileId = fileItem.getAttribute("data-file-id");
                const isFolder = fileItem.getAttribute("data-is-folder") === "true";
                const fileInfo = findFile(fileId);

                if (!isFolder) {
                    const extension = fileInfo.storageFileInfo.file_extension
                        ? fileInfo.storageFileInfo.file_extension.toLowerCase()
                        : "";
                    if (_isOfficeFile(extension)) {
                        const data = {
                            fileId: fileId,
                            fileName: fileInfo.storageFileInfo.file_name,
                            fileExtension: extension
                        };
                        if (document.getElementsByClassName("tooltip").length != 0) {
                            document.getElementsByClassName("tooltip")[0].classList.remove("tooltip"); // 220736 툴팁있으면, 제거
                        }
                        officeManager.callOffice(data);
                    } else {
                        const extensionInfo = fileExtension.getInfo(extension);
                        if (extensionInfo["isPreview"]) {
                            if (extension === "pdf") {
                                storageManager.getDownloadFileInfo(
                                    fileId,
                                    _currentChannelId,
                                    _currentWorkspaceId,
                                    res => {
                                        window.open(res.url);
                                    },
                                    () => {}
                                );
                            } else {
                                drivePreview.open(
                                    fileInfo.storageFileInfo,
                                    extensionInfo["type"],
                                    storageManager.GetFileUrl(fileId, _currentChannelId, _currentWorkspaceId)
                                );
                            }
                        } else {
                            storageManager.DownloadFiles(
                                [fileId],
                                _currentChannelId,
                                _currentWorkspaceId,
                                () => {},
                                () => {},
                                false
                            );
                        }
                    }
                } else {
                    // 빼거나, 더하거나 해야됨.
                    if (_paths.length > 1 && _paths[_paths.length - 2]["folder_id"] === fileId) {
                        _paths.pop();
                    } else {
                        _paths.push({
                            folder_id: fileId,
                            folder_name: fileInfo["folder_name"]
                        });
                    }                    
                    try {
                        sessionStorage.setItem(getRoomIdByUrl(), JSON.stringify(_paths));
                    }
                    catch (e) {
                        console.log('local memory is full!');
                        console.log(e);
                    };
                    _currentFolderId = fileId;
                    renderView();
                }
            }
        }
    };

    const _isOfficeFile = extension => {
        if (String.isNullOrEmpty(extension)) {
            return false;
        }

        extension = extension.toLowerCase();

        return (
            extension == "ppt" ||
            extension == "pptx" ||
            extension == "tpt" ||
            extension == "doc" ||
            extension == "docx" ||
            extension == "toc" ||
            extension == "xls" ||
            extension == "xlsx" ||
            extension == "tls" ||
            extension == "csv"
        );
    };

    const _onFileSelect = event => {
        const checkbox = event.target;
        const fileItem = checkbox.closest(".file__item");

        if (fileItem) {
            if (checkbox.checked === true) {
                fileItem.classList.add("file__item--selected");
            } else {
                fileItem.classList.remove("file__item--selected");
            }

            //const checkboxList = document.querySelectorAll(".file__item input[type='checkbox']");
            const checkboxList = document.querySelectorAll(".drive__files input[type='checkbox']");
            const length = checkboxList.length;
            let checkedFolderCount = 0;
            let checkedFileCount = 0;
            _selectedFileList = [];
            for (let i = 0; i < length; i++) {
                const _fileItem = checkboxList[i].closest(".file__item");
                const _isFolder = _fileItem.getAttribute("data-is-folder");
                const _fileId = _fileItem.getAttribute("data-file-id");
                if (checkboxList[i].checked) {
                    _selectedFileList.push(findFile(_fileId));
                    checkedFileCount++;
                    if (_isFolder === "true") {
                        checkedFolderCount++;
                    }
                }
            }
            // depth 들어가면, ... 뒤로가기 폴더, dnd file item(안보이는 하나 추가됨) 제외하고 카운트
            let allCount = document.querySelectorAll(".drive__files")[0].childElementCount;
            if ($(".file__item__prev").length != 0) {
                allCount -= 1;
            }
            _updateButtons(allCount === _selectedFileList.length, !!checkedFileCount, !!checkedFolderCount);
        }
    };

    const _onAllFileSelect = event => {
        const isChecked = event.target.checked;
        const checkboxList = document.querySelectorAll(".drive__files input[type='checkbox']");
        if (checkboxList) {
            const length = checkboxList.length;
            for (let i = 0; i < length; i++) {
                if (checkboxList[i].checked !== isChecked) {
                    const event = new MouseEvent("click", {
                        bubbles: false,
                        cancelable: false,
                        view: window
                    });
                    checkboxList[i].dispatchEvent(event);
                }
            }
        }
    };

    const _updateButtons = (isAllChecked, isChecked, isIncludeFolder) => {
        const downloadButton = document.querySelector(".drive__button--download");
        //const shareButton = document.querySelector(".drive__button--share");
        const deleteButton = document.querySelector(".drive__button--delete");
        const newFolderButton = document.querySelector(".drive__button--new-folder");
        const uploadButton = document.querySelector(".drive__button--upload");

        if (isChecked) {
            downloadButton.style.display = "inline-flex";
            //shareButton.style.display = "inline-flex";
            deleteButton.style.display = "inline-flex";
        } else {
            downloadButton.style.display = "none";
            //shareButton.style.display = "none";
            deleteButton.style.display = "none";
        }

        if (isIncludeFolder) {
            downloadButton.setAttribute("disabled", true);
            _rootElement.querySelector(".drive__button--download").title = "";
            //shareButton.setAttribute("disabled", true);
        } else {
            downloadButton.removeAttribute("disabled");
            _rootElement.querySelector(".drive__button--download").title = "다운로드";
            //shareButton.removeAttribute("disabled");
        }

        if (_isDrive) {
            if (_appNumber === "1") {
                newFolderButton.style.display = "none";
                uploadButton.style.display = "none";
            } else {
                newFolderButton.style.display = "inline-flex";
                uploadButton.style.display = "inline-flex";
            }
        } else {
            newFolderButton.style.display = "none";
            uploadButton.style.display = "none";
        }

        document.querySelector("#drive__select-all__check").checked = isAllChecked;
    };

    const _updateView = (isGrid, isExpand) => {
        const fileTemplates = document.querySelectorAll(".file__item");
        const length = fileTemplates.length;
        for (let i = 0; i < length; i++) {
            const imageUrl = fileTemplates[i].getAttribute("data-image-url");
            if (imageUrl) {
                if (isGrid) {
                    fileTemplates[i].style.backgroundImage = `url('${imageUrl}')`;
                    fileTemplates[i].querySelector(".file__item__icon").style.opacity = "0";
                } else {
                    fileTemplates[i].style.backgroundImage = `none`;
                    fileTemplates[i].querySelector(".file__item__icon").style.opacity = "1";
                }
            } else {
                fileTemplates[i].style.backgroundImage = `none`;
                fileTemplates[i].querySelector(".file__item__icon").style.opacity = "1";
            }

            fileTemplates[i].classList.add(`file__item${isGrid ? "--grid" : "--list"}`);
            fileTemplates[i].classList.remove(`file__item${isGrid ? "--list" : "--grid"}`);

            fileTemplates[i].classList.add(`file__item${isExpand ? "--main" : "--sub"}`);
            fileTemplates[i].classList.remove(`file__item${isExpand ? "--sub" : "--main"}`);
        }
    };

    const getFileList = () => {
        return _fileList;
    };

    const renameFile = (targetFileId, targetFileSize, newFileName) => {
        let inputDTO = _driveFileRenameDTO(targetFileId, targetFileSize, newFileName);
        return _ajaxCall("DriveMeta", "PUT", inputDTO);
    };
    
    const renameFolder = (targetFileId, newFolderName) => {
        let inputDTO = _driveFolderRenameDTO(targetFileId, newFolderName);
        return _ajaxCall("DriveMeta", "PUT", inputDTO);
    };

    const _driveFolderRenameDTO = (targetFileId, newFolderName) => {
        if (String.isNullOrEmpty(newFolderName)) {
            return undefined;
        }

        return {
            dto: {
                file_parent_id: _currentFolderId,
                user_id: userManager.getLoginUserId(),
                app_number: _appNumber,
                teeDriveDtoList: [
                    {
                        workspace_id: _currentWorkspaceId,
                        ch_id: _currentChannelId,
                        file_parant_id: _currentFolderId,
                        is_folder: "true",
                        folder_name: newFolderName,
                        file_id: targetFileId
                    }
                ]
            }
        };
    };   

    const _driveFileRenameDTO = (targetFileId, targetFileSize, newFileName) => {
        if (String.isNullOrEmpty(newFileName)) {
            return undefined;
        }

        return {
            dto: {
                file_parent_id: _currentFolderId == "all" ? _currentChannelId : _currentFolderId,
                user_id: userManager.getLoginUserId(),
                app_number: _appNumber,
                teeDriveDtoList: [
                    {
                        workspace_id: _currentWorkspaceId,
                        ch_id: _currentChannelId,
                        is_folder: "false",
                        storageFileInfo: {
                            user_id: userManager.getLoginUserId(),
                            file_last_update_user_id: userManager.getLoginUserId(),
                            file_id: targetFileId,
                            file_name: newFileName,
                            file_size: targetFileSize
                        }
                    }
                ]
            }
        };
    };
    
    const _driveUploadFolderDTO = folderName => {
        if (String.isNullOrEmpty(folderName)) {
            return undefined;
        }

        return {
            dto: {
                workspace_id: _currentWorkspaceId,
                app_number: _appNumber,
                user_id: userManager.getLoginUserId(),
                ch_id: _currentChannelId,
                file_parent_id: _currentFolderId,
                //is_folder        : "true",
                folder_name: folderName
                //app_number       : 6,
                //storageFileInfo  : {},
            }
        };
    };

    const _driveUploadFileDTO = (fileName, fileExt, fileSize, tmpId) => {
        if (String.isNullOrEmpty(fileName) || String.isNullOrEmpty(fileExt)) {
            return undefined;
        }
        if (tmpId == undefined || tmpId < 0) {
            return undefined;
        }

        let loginUserId = userManager.getLoginUserId();

        return {
            dto: {
                user_id: loginUserId,
                workspace_id: _currentWorkspaceId,
                ch_id: _currentChannelId,
                file_parent_id: _currentFolderId,
                app_number: _appNumber,
                storageFileInfo: {
                    user_id: loginUserId,
                    file_last_update_user_id: loginUserId,
                    file_name: fileName,
                    file_extension: fileExt,
                    file_size: fileSize,
                    user_context_1: tmpId,
                    user_context_2: false.toString()
                }
            }
        };
    };

    const _driveStorageUpdateDTO = (fileId, userContext1, userContext2) => {
        if (String.isNullOrEmpty(fileId)) {
            return undefined;
        }

        return {
            dto: {
                workspace_id: _currentWorkspaceId,
                channel_id: _currentChannelId,
                user_id: userManager.getLoginUserId(),
                storageFileInfo: {
                    file_id: fileId,
                    //file_last_update_user_id : userManager.getLoginUserId(),
                    user_id: userManager.getLoginUserId(),
                    user_context_1: userContext1,
                    user_context_2: userContext2
                }
            }
        };
    };

    const driveDuplicateNameDTO = (dstFolderId, fileName, fileExt) => {
        let loginUserId = userManager.getLoginUserId();
        let inputDTO = [];

        inputDTO.push({
            is_folder: "false",
            storageFileInfo: {
                file_name: fileName,
                file_extension: fileExt
            }
        });

        return {
            dto: {
                user_id: loginUserId,
                workspace_id: workspaceManager.getWorkspaceId(getRoomIdByUrl(), "CHN0006"),
                channel_id: workspaceManager.getChannelList(getRoomIdByUrl(), "CHN0006"),
                file_parent_id: dstFolderId,
                app_number: "6",
                teeDriveDtoList: inputDTO
            }
        };
    };



    const getCopyFileDTO = function(fileParentId, fileId, fileExt, fileName, userContext1, userContext2, userContext3) {
        let loginUserId = userManager.getLoginUserId();

        return {
            dto: {
                workspace_id: workspaceManager.getWorkspaceId(getRoomIdByUrl(), "CHN0006"),
                channel_id: workspaceManager.getChannelList(getRoomIdByUrl(), "CHN0006"),
                user_id: loginUserId,
                file_parent_id: fileParentId,
                storageFileInfo: {
                    file_id: fileId,
                    user_id: loginUserId,
                    file_last_update_user_id: loginUserId,
                    file_name: fileName,
                    file_extension: fileExt,
                    user_context_1: userContext1,
                    user_context_2: userContext2,
                    user_context_3: userContext3
                }
            }
        };
    };

    const _ajaxCall = (serviceUrl, type, inputDTO, callback) => {
        if (type.toUpperCase() == "GET") {
            serviceUrl += "&rn=" + Date.now();
        }

        let outputData = null;
        try {
            Top.Ajax.execute({
                url: String.format("{0}{1}", _workspace.fileUrl, serviceUrl),
                type: type,
                dataType: "json",
                async: false,
                cache: false,
                data: inputDTO,
                contentType: "application/json",
                crossDomain: true,
                xhrFields: {
                    withCredentials: true
                },
                success: function(data) {
                    try {
                        if (typeof data == "string") {
                            data = JSON.parse(data);
                        }

                        outputData = data.dto;
                        if (outputData.result != undefined) {
                            outputData.result = outputData.result.toUpperCase();
                        }

                        if (callback && typeof callback === "function") callback();
                    } catch (error) {
                        outputData = {
                            result: "FAIL",
                            message: "UnknownError.(CORS?)... JSON.stringify(error) : " + JSON.stringify(error),
                            data: error
                        };
                    }
                },
                error: function(data) {
                    outputData = {
                        result: "FAIL",
                        message: data.exception != undefined ? data.exception.message : "Error",
                        data: data
                    };
                    if (data.exception != undefined) {
                        console.log(data.exception.message);
                    }
                }
            });
        } catch (ex) {
            //StartLoader(false);
        }

        return outputData;
    };

    const uploadFolder = (folderName) => {
        const inputData = {
            dto: {
                workspace_id: _currentWorkspaceId,
                app_number: _appNumber,
                user_id: userManager.getLoginUserId(),
                ch_id: _currentChannelId,
                file_parent_id: _currentFolderId,
                folder_name: folderName
            }
        };

        return axios.post(`${_workspace.fileUrl}DriveFolder`, inputData);
        // return _ajaxCall("DriveFolder", "POST", _driveUploadFolderDTO(folderName), successCallback);
    };





    const getAppNumber = () => {
        if (_isDrive) {
            return _appNumber;
        }

        return null;
    };

    const getSelectedFileList = () => {
        return _selectedFileList;
    };

    const deleteFiles = files => {
        let serviceUrl = _workspace.fileUrl + "DriveFile?action=Delete";

        let inputDTO = [];
        for (let i = 0; i < files.length; i++) {
            if (files[i]) {
                inputDTO.push({
                    workspace_id: workspaceManager.getWorkspaceId(getRoomIdByUrl(), "CHN0006"),
                    ch_id: workspaceManager.getChannelList(getRoomIdByUrl(), "CHN0006"),
                    file_id: files[i].is_folder == "false" ? files[i].storageFileInfo.file_id : files[i].file_id,
                    file_parent_id: _currentFolderId,
                    is_folder: files[i].is_folder
                });
            }
        }

        return axios.post(serviceUrl, {
            dto: {
                user_id: userManager.getLoginUserId(),
                app_number: _appNumber,
                teeDriveDtoList: inputDTO
            }
        });
    };


    const getPaths = () => {
        return _paths;
    };

    // 필수 구현
    const onDragStart = (e, data) => {
        // background template
        const element = e.target;
        dnd.setBackground(e.target.cloneNode(true));

        dnd.useDefaultEffect(true);

        // 추후 여러개 dnd 할 경우 - talk와 협의 필요
        // 복수 파일 드래그 고려..

        tmp_fileId = element.getAttribute("data-file-id");
        let fileInfo = findFile(tmp_fileId);

        if (fileInfo.is_folder == "true") {
            fileInfo.app = "drive";
            fileInfo.name = fileInfo.folder_name;
            fileInfo.appNumber = fileInfo.app_number;
            fileInfo.channel_id =
                _currentFolderId == "all" ? element.getAttribute("data-channel-id") : _currentChannelId;
            fileInfo.workspace_id =
                _currentFolderId == "all" ? element.getAttribute("data-workspace-id") : _currentWorkspaceId;
        } else {
            let tmpAppNumber = fileInfo.app_number;
            fileInfo = fileInfo.storageFileInfo;
            fileInfo.app = "drive"; // 어떤 앱에서 드래그
            fileInfo.appNumber = tmpAppNumber; // 파일의 앱넘버 (talk첨부함, 룸 저장소)
            fileInfo.name = fileInfo.file_name;
            fileInfo.channel_id =
                _currentFolderId == "all" ? element.getAttribute("data-channel-id") : _currentChannelId;
            fileInfo.workspace_id =
                _currentFolderId == "all" ? element.getAttribute("data-workspace-id") : _currentWorkspaceId;
        }
        // data set
        dnd.setData(fileInfo);
    };

    // 필수 구현
    const onDrop = (e, dropData) => {
        const { fromApp, data, files } = dropData;
        switch (fromApp) {
            // fropApp 에 따른 처리 (타앱 -> drive)
            // 타 앱에서 이미 업로드된 파일이므로, file id 만 알면 복사 가능할 듯.
            case "drive":
                const dstFolderElement = e.target.closest(".dnd__container");
                if (dstFolderElement && dstFolderElement.getAttribute("data-is-folder") == "true"
                	&& data.file_id != dstFolderElement.getAttribute("data-file-id")) {
                    driveServiceCall._dragInFolder(dstFolderElement, data)
                        .then(res => {
                            renderView();
                        })
                        .catch(err => {});
                }
                dnd.useDefaultEffect(true);
                break;

            case "talk":
                break;

            case "note":
                break;

            case "mail":
                break;

            case "meeting":
                break;

            case "office":
                break;

            case "schedule":
                break;

            // file 업로드에 대한 처리 (바탕화면 -> drive)
            default:
                if (files && files.length) {
                    _uploadFiles(files, _appNumber);
                }
                break;
        }
    };

    const onDragOver = (e, data) => {
        // 드래그 배경 이미지가 영역에 따라 변화해야 할 경우 구현.
    };

    const onDragEnter = (e, data) => {
        // 기본 modal 사용 안할경우 구현.
        let isFolderContainer = e.target.closest(".dnd__container").getAttribute("data-is-folder");
        if (data.fromApp == "drive" && isFolderContainer) {
            dnd.useDefaultEffect(true);
        }
    };

    const onDragLeave = (e, data) => {
        // 기본 modal 사용 안할경우 구현.
    };

    const S = (stringKey, p0, p1, p2, p3) => {
        return String.format(Top.i18n.t(stringKey), p0, p1, p2, p3);
    };

    const getDriveFileDateFormat = time => {
        let curDate = new Date();
        let date = new Date(time);
        let str = String.empty;

        if (
            curDate.getFullYear() == date.getFullYear() &&
            curDate.getMonth() == date.getMonth() &&
            curDate.getDate() == date.getDate()
        ) {
            str = "오늘";
        } else if (
            curDate.getFullYear() == date.getFullYear() &&
            curDate.getMonth() == date.getMonth() &&
            curDate.getDate() - 1 == date.getDate()
        ) {
            str = "어제";
        } else {
            str +=
                date
                    .getFullYear()
                    .toString()
                    .slice(2, 4) + ".";
            str += String.format("{0}.{1} ", ("0" + (date.getMonth() + 1)).slice(-2), ("0" + date.getDate()).slice(-2));
        }

        return str;
    };

    const getDriveDetailDateFormat = time => {
        let curDate = new Date();
        let date = new Date(time);

        let str = String.empty;
        str += String.format("{0}-", date.getFullYear());
        str += String.format("{0}-{1} ", ("0" + (date.getMonth() + 1)).slice(-2), ("0" + date.getDate()).slice(-2));

        let halfDayFormat = getHalfDayFormat(time);
        str += String.format(
            "{0} {1}",
            halfDayFormat.meridiem == 0 ? Top.i18n.t("value.m000147") : Top.i18n.t("value.m000148"),
            halfDayFormat.time
        );

        return str;
    };

    const handleMessage = wwms => {
        // console.log("drive:", JSON.parse(wwms.NOTI_ETC));
        if (wwms.NOTI_ETC && appManager.getSubApp() === "drive") {
            let message = JSON.parse(wwms.NOTI_ETC);
            for (ele in message.fileInfoList) {
                if (_currentFolderId === message.fileInfoList[ele].file_parent_id)
                    renderView();
            }
            // switch (message) {

            // }
        }
    };

    const talkRenderView = () => {
        if (_isRoot && _appNumber == "1") {
            const renderEevent = new MouseEvent("click", {
                view: window,
                bubbles: true,
                cancelable: false
            });

            const talkElement = document.querySelectorAll(".drive__sidebar__item")[1];
            if (talkElement) {
                talkElement.dispatchEvent(renderEevent);
            }
        }
    };

    const setSendTargetFile = input => {
        _sendTargetFile = input;
    };

    const getSendTargetFile = () => {
        return _sendTargetFile;
    };

    const setSendFileArr = input => {
        _sendFileArr = input;
    };

    const getSendFileArr = () => {
        return _sendFileArr;
    };

    return {
        render,
        setDriveInfo,
        getFileList,
        getSelectedFileList,
        uploadFolder,
        getAppNumber,
        renderView,
        deleteFiles,
        findFile,
        renameFile,
        renameFolder,
        getPaths,
        getChannelId,
        getWorkspaceId,
        onDragStart,
        onDrop,
        onDragOver,
        onDragEnter,
        onDragLeave,
        S,
        getDriveDetailDateFormat,
        handleMessage,
        talkRenderView,
        getCopyFileDTO,
        driveDuplicateNameDTO,
        setSendTargetFile,
        getSendTargetFile,
        setSendFileArr,
        getSendFileArr
    };
})();

TEESPACE_WEBSOCKET.addWebSocketHandler("CHN0006", drive.handleMessage);

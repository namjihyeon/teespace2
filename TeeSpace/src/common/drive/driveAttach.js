const driveAttach = (() => {
    /* FILTER */
    const _fileType = {
        // image
        apng: "image",
        bmp: "image",
        gif: "image",
        jpg: "image",
        jpeg: "image",
        jfif: "image",
        png: "image",
        svg: "image",

        // video
        mkv: "video",
        avi: "video",
        mp4: "video",
        mpg: "video",
        flv: "video",
        wmv: "video",
        asf: "video",
        asx: "video",
        ogm: "video",
        ogv: "video",
        mov: "video",

        // office
        ppt: "office",
        pptx: "office",
        tpt: "office",
        doc: "office",
        docx: "office",
        toc: "office",
        xls: "office",
        xlsx: "office",
        tls: "office",
        csv: "office"
    };

    // 검색 필터 노드
    const _selectboxNodes = [
        {
            id: "all",
            text: "전체"
        },
        {
            id: "image",
            text: "사진"
        },
        {
            id: "video",
            text: "동영상"
        },
        {
            id: "office",
            text: "오피스"
        },
        {
            id: "other",
            text: "기타"
        }
    ];

    let _currentFilter = "all"; //all(default), image, video, office, other

    /* ID */
    let _currentWorkspaceId_attachFile= null; // 현재 workspace (room) id
    let _currentChannelId_attachFile = null; // 현재 channel (drive) id
    let _currentFolderId_attachFile = null; // 현재 folder (parent) id

    /* ELEMENT */
    let _rootElement_attachFile = null; // drive root element (.drive)

    /* ARRAY */
    let _sidebarItems_attachFile = []; // sidebar 그리기 위한 데이터
    let _fileList_attachFile = []; // 서버에서 받아온 파일들
    let _selectedFileList_attachFile = []; // 선택한 파일 쌓는 용도
    let _paths_attachfile = []; // 진입한 폴더 쌓는 용도

    /* BOOLEAN */
    let _isDrive_attachFile = false; // drive 또는 viewFiles
    let _isGrid_attachFile = true; // grid 또는 list
    let _isExpand_attachFile = false; // 확장 여부

    /* TYPES */
    let _appNumber = null; // talk ("1") 또는 space ("6")    
    let attachFromTeeDrive = false;    

    const render = (workspaceId, channelId, targetSelector, isDrive) => {
        const targetElement = document.querySelector(targetSelector);
        if (targetElement) {
            _isDrive_attachFile = isDrive;
            _currentWorkspaceId_attachFile = workspaceId;
            _currentChannelId_attachFile = channelId;
            _currentFolderId_attachFile = _currentChannelId_attachFile;
            _isGrid_attachFile = localStorage.getItem("driveViewState") == null ? true : localStorage.getItem("driveViewState");
            
            targetElement.appendChild(_createDriveTemplate());
            _renderSidebarItems().then(() => {
                // 첫번째 클릭
                const clickEevent = new MouseEvent("click", {
                    view: window,
                    bubbles: true,
                    cancelable: false
                });

                const firstElement = document.querySelector(".drive__attachfile__sidebar__item");
                if (firstElement) {
                    firstElement.dispatchEvent(clickEevent);
                }
            });
        }  
    };

    const _createSidebarItemTemplate = item => {
        const itemElement = document.createElement("div");
        itemElement.setAttribute("data-channel-id", item["channelId"]);
        if (_isDrive_attachFile) {
            itemElement.setAttribute("data-app-number", item["appNumber"]);
        }
        itemElement.classList.add("drive__attachfile__sidebar__item");

        const template = // `<span class='${item["icon"]}'
							// style='font-size:1.563rem;margin-bottom:0.125rem;'>
        				 `<span style='font-size:1.563rem;margin-bottom:0.125rem;'><img class='drive__attachfile__sidebar__image'></span>
                          <span style='font-size:0.625rem;font-weight:bold;'>${item["text"]}</span>`;

        itemElement.insertAdjacentHTML("afterbegin", template);

        return itemElement;
    };

    const _renderSidebarItems = () => {
        return new Promise((resolve, reject) => {
            const frag = document.createDocumentFragment();

            // drive
            if (_isDrive_attachFile) {
                // data set
                _sidebarItems_attachFile = [
                    {
                        channelId: _currentChannelId_attachFile,
                        // icon: "icon-work_app_ic_ttalk",
                        appNumber: "6",
                        text: "룸 저장소"
                    },
                    {
                        channelId: _currentChannelId_attachFile,
                        // icon: "icon-work_tmaxcloudspace",
                        appNumber: "1",
                        text: "Talk 첨부함"
                    }
                ];

                // rendering
                const length = _sidebarItems_attachFile.length;
                for (let i = 0; i < length; i++) {
                    frag.appendChild(_createSidebarItemTemplate(_sidebarItems_attachFile[i]));
                }
                _rootElement_attachFile.querySelector(".drive__attachfile__sidebar").appendChild(frag.cloneNode(true));

                setTimeout(() => {
                    resolve();
                }, 1);
            }
            // view file
            else {
                // 서버에서 채널리스트 받아와서 그리자.
                getRoomListFromServer()
                    .then(res => {
                        const roomList = res.data.dto.SpaceRoomList;
                        if (roomList && roomList.length) {
                            const channels = getChannelList(roomList);

                            _sidebarItems_attachFile = [];

                            const length = channels.length;
                            for (let i = 0; i < length; i++) {
                                _sidebarItems_attachFile.push({
                                    channelId: channels[i].channel["CH_ID"],
                                    icon: "icon-work_tmaxcloudspace",
                                    text: channels[i]["name"]
                                });

                                frag.appendChild(_createSidebarItemTemplate(_sidebarItems_attachFile[i]));
                            }
                        }
                        _rootElement_attachFile.querySelector(".drive__attachfile__sidebar").appendChild(frag.cloneNode(true));
                    })
                    .catch(err => {
                        reject();
                    })
                    .then(() => {
                        resolve();
                    });
            }
        });
    };

    const _createDriveTemplate = () => {
        _rootElement_attachFile = document.createElement("div");
        _rootElement_attachFile.classList.add("drive");
        _rootElement_attachFile.setAttribute("data-drive-id", _currentChannelId_attachFile);
        _rootElement_attachFile.setAttribute("data-is-drive", _isDrive_attachFile);

		const template = `<div style='display:flex;height:100%;'>
                            <div class='drive__attachfile__sidebar'></div>
                            <div class='drive__attachfile__content'>
                                <div class='drive__attachfile__content__row'>
                                	<span class='drive__attachfile__space__image src='./res/drive/talk_folder.svg'></span>
                                	<span class='drive__attachfile__title__text'>룸 저장소</span>
                                	<div class='drive__attachfile__vertical-line-one'></div>
                                    <div class='drive__attachfile__bread-crumb'></div>
                                    <div class='drive__attachfile__search__input'>
                                        <span class='icon-search' style='padding-left: 0.5rem; font-size:0.8rem; color:#75757F; margin-top:0.3rem;'></span>
                                        <input type='text' placeholder='드라이브 검색' style='padding-left: 0.3rem; border:0rem; width : 100%;'/>
                                    </div>
									<span class='drive__attachfile__button--change-view ${
									_isGrid_attachFile ? "icon-work_format_list" : "icon-palette_gridlayout"
									}'></span>
								</div>								
                                <div class='drive__attachfile__content__row'>
									<span class='drive__attachfile__button--new-folder icon-work_folder' style='margin-left:1rem; font-size:1.25rem; cursor:pointer'></span>
									<span class='drive__attachfile__button-text' style='margin-left:0.38rem; font-size:0.81rem; color:#45474a;'>새 폴더</span>
								</div>
								<div class='drive__attachfile__content__row'>
                                    <div class='drive__attachfile__files'></div>
                                </div>
                            </div>
                        </div>`;
		
		_rootElement_attachFile.insertAdjacentHTML("afterbegin", template);
		_rootElement_attachFile.querySelector(".drive__attachfile__search__input input").addEventListener("keydown", _onSearchInput);
		_rootElement_attachFile.querySelector(".drive__attachfile__sidebar").addEventListener("click", _onSidebarItemClick);
		_rootElement_attachFile.querySelector(".drive__attachfile__button--change-view").addEventListener("click", _onViewChangeClick);
		_rootElement_attachFile.querySelector(".drive__attachfile__button--new-folder").addEventListener("click", _onNewFolderClick)

		return _rootElement_attachFile;
    };
        
    const _onFilterChange = (event, widget) => {
        _currentFilter = widget.getSelected().id;
        filterView();
    };

    const _onSearchInput = event => {
        if (event.keyCode == 13)
            renderView();    	
    	//filterView();
    };

    const filterView = () => {
        const textElement = document.querySelector(".drive__attachfile__search__input input");
        const searchStr = textElement ? textElement.value : "";
        const fileItems = document.querySelectorAll(".file__item__attachfile");

        const checkboxList = document.querySelectorAll(".file__item__attachfile input[type='checkbox']");
        let visibleCount = 0;
        
        if (fileItems && fileItems.length) {
            fileItems.forEach(fileItem => {
                const fileType = fileItem.getAttribute("data-filter-type");
                const isMatchString = fileItem.querySelector(".file__item__attachfile__title").innerText.includes(searchStr);
                if (_currentFilter === "all" && isMatchString) {
                    fileItem.style.display = "flex";
                    visibleCount++;
                } else {
                    if (fileType === "always" || (fileType === _currentFilter && isMatchString)) {
                        fileItem.style.display = "flex";
                        visibleCount++;
                    } else {
                        // 화면에 안보여질 파일은 selectedFileList에서 제거, selected 해제,
						// check 해제
                        const _fileId = fileItem.getAttribute("data-file-id");
                        const itemToFind = _selectedFileList_attachFile.find(function(item) {
                            let fileId = item.is_folder ? item.file_id : item.storageFileInfo.file_id;
                            return fileId === _fileId;
                        });
                        const idx = _selectedFileList_attachFile.indexOf(itemToFind);

                        _selectedFileList_attachFile.splice(idx, 1);

                        fileItem.classList.remove("file__item__attachfile--selected");
                        fileItem.querySelector(".file__item__attachfile input[type='checkbox']").checked = false;
                        fileItem.style.display = "none";
                    }
                }
            });
            
            const container = document.querySelector(".drive__attachfile__files");            
            let emptyElement = document.querySelector(".drive__attachfile__search__empty");
            
            if (!emptyElement && container && _isRoot()) {
                container.appendChild(_createSearchNoFileElement());                    
            }                        
            
            if(emptyElement) {
            	if(visibleCount == 0) {             	
            		emptyElement.style.display = "flex";
            	} else {
            		emptyElement.style.display = "none";
            	}    
            }
        }

        let checkedFolderCount = 0;
        let checkedFileCount = 0;
        let visibleFileCount = 0;
        for (let i = 0; i < checkboxList.length; i++) {
            const _fileItem = checkboxList[i].closest(".file__item__attachfile");
            const _isFolder = _fileItem.getAttribute("data-is-folder");
            const _fileId = _fileItem.getAttribute("data-file-id");
            if (_fileItem.style.display != "none") visibleFileCount++;
            if (checkboxList[i].checked) {
                checkedFileCount++;
                if (_isFolder === "true") {
                    checkedFolderCount++;
                }
            }
        }
        if (checkedFileCount)
            _updateButtons(checkedFileCount === visibleFileCount, !!checkedFileCount, !!checkedFolderCount);
        // 체크된 파일이 없으면 전체선택 체크박스 해제
        else _updateButtons(false, false, false);
    };

    // test code, to remove .duck
    const _onHeaderMoreButtonClick = event =>{
        const isDisabled = !!event.target.getAttribute("disabled");
        if (isDisabled) return;

        Top.Dom.selectById("driveAttachFileDialog").open();
    };
    const _onExpandButtonClick = () => {
        onExpandButtonClick("drive", function() {
            document.querySelector(".drive__button--expand").style.display = "none";
            document.querySelector(".drive__button--collapse").style.display = "inline-flex";
            _isExpand_attachFile = true;
            _updateView(_isGrid_attachFile, _isExpand_attachFile);
        });
    };
    const _onCollapseButtonClick = () => {
        onFoldButtonClick("drive", function() {
            document.querySelector(".drive__button--expand").style.display = "inline-flex";
            document.querySelector(".drive__button--collapse").style.display = "none";
            _isExpand_attachFile = false;
            _updateView(_isGrid_attachFile, _isExpand_attachFile);
        });
    };
    const _onCloseButtonClick = () => {
        onCloseButtonClick("drive", function() {
            document.querySelector(".drive__button--expand").style.display = "inline-flex";
            document.querySelector(".drive__button--collapse").style.display = "none";
        });
    };

    const _onDownloadButtonClick = event => {
        const isDisabled = !!event.target.getAttribute("disabled");
        if (isDisabled) return;

        const isZipFile = _selectedFileList_attachFile.length > 9;
        const idArray = [];
        for (let i = 0; i < _selectedFileList.length; i++) {
            let id = null;
            if (_selectedFileList_attachFile[i]["is_folder"] === "true") {
                id = _selectedFileList_attachFile[i]["file_id"];
            } else {
                id = _selectedFileList_attachFile[i]["storageFileInfo"]["file_id"];
            }
            idArray.push(id);
        }

        storageManager.DownloadFiles(
            idArray,
            _currentChannelId_attachFile,
            function() {},
            function() {},
            isZipFile
        );
    };
    const _onShareButtonClick = event => {
        const isDisabled = !!event.target.getAttribute("disabled");
        if (isDisabled) return;

        Top.Dom.selectById("drivePassFileDialog").open();
    };
    const _onDeleteButtonClick = event => {
        const isDisabled = !!event.target.getAttribute("disabled");
        if (isDisabled) return;

        Top.Dom.selectById("driveDeleteDialog").open();
        Top.Dom.selectById("driveDeleteDialog").setProperties({
            dialogFrom: "button"
        });
    };
    const _onNewFolderButtonClick = event => {
        const isDisabled = !!event.target.getAttribute("disabled");
        if (isDisabled) return;

        Top.Dom.selectById("driveNewFolderDialog").open();
    };
    const _onUploadButtonClick = event => {
        const isDisabled = !!event.target.getAttribute("disabled");
        if (isDisabled) return;

        let fileChooser = Top.Device.FileChooser.create({
            onBeforeLoad: function() {
                _uploadFiles(this.file);
                return false;
            },
            multiple: true
        });

        fileChooser.show();
    };

    const _uploadFiles = files => {
        let beforeCheck = _onBeforeLoadFiles(files);
        if (!beforeCheck.result) {
            return;
        }

        uploadStatusPopup.open();
        uploadStatusPopup.expand();
        uploadStatusPopup.updateCount("progress", beforeCheck.newFileList.length);
        uploadStatusPopup.updateUploadText("개 항목 업로드 중");        
        uploadStatusPopup.updateCount("success", 0);
        
        let inputDTO = {};
        let file = {};
        let nomFileFullName = String.empty;
        let fileNameArray = [];
        let fileName = String.empty;
        let fileExt = String.empty;
        let successCount;
        let failCount;

        let tmpId = -1;
        let _successCallback = function(res) {
            let tmpId = res.dto.storageFileInfoList[0].user_context_1;
            let realFileId = res.dto.storageFileInfoList[0].file_id;
            let storageFileUpdateUrl = String.format("{0}Storage/StorageMeta", _workspace.url);
            let storageFileUpdateDto = _driveStorageUpdateDTO(realFileId, tmpId, true.toString());
            Top.Ajax.put(storageFileUpdateUrl, storageFileUpdateDto, undefined, function() { renderView(); });
            successCount++;
            uploadStatusPopup.updateCount("success", successCount);
            uploadStatusPopup.update(tmpId, FileStatus.UPLOAD_SUCCESS);

            if (beforeCheck.newFileList.length == successCount + failCount) {
                uploadStatusPopup.updateUploadText("개 항목 업로드 완료");
            }
        };

        let _errorCallback = function(res) {
            // let tmpId = res.dto.storageFileInfoList[0].user_context_1;
            failCount++;
            uploadStatusPopup.update(tmpId, FileStatus.UPLOAD_FAIL);

            if (beforeCheck.newFileList.length == successCount + failCount) {
                uploadStatusPopup.updateUploadText("개 항목 업로드 완료");
            }
        };

        for (var i = 0; i < beforeCheck.newFileList.length; i++) {
            successCount = 0;
            failCount = 0;
            file = files[i];
            nomFileFullName = Top.Util.Browser.isIE() ? file.name : file.name.normalize("NFC");
            fileNameArray = nomFileFullName.split(".");
            fileExt = fileNameArray[fileNameArray.length - 1];
            fileName = String.isNullOrEmpty(fileExt)
                ? nomFileFullName
                : nomFileFullName.substring(0, nomFileFullName.lastIndexOf(fileExt) - 1);
            tmpId = beforeCheck.newFileList[i].file_id;
            inputDTO = _driveUploadFileDTO(fileName, fileExt, file.size, tmpId);
            storageManager.UploadFile(file, inputDTO, "File", "DriveFile", _successCallback, _errorCallback);

            uploadStatusPopup.insert(tmpId, fileExt, fileName);
        }
    };

    const getRoomListFromServer = () => {
        const myId = userManager.getLoginUserId();
        return axios.get(`${_workspace.url}SpaceRoom/SpaceRoomList?USER_ID=${myId}`);
    };

    const getWorkspaceId = () => {
        return _currentWorkspaceId_attachFile;
    };

    const getChannelId = () => {
        return _currentChannelId_attachFile;
    };
    
    const getCurrentFolderId = () => {
    	return _currentFolderId_attachFile;
    };

    const getChannelList = roomList => {
        // 임시 코드.
        // 공통팀한테 만들어 달라고 하자.
        // user id 로 채널 리스트 얻어올수 있는 거.

        const channelList = [];
        if (roomList && roomList.constructor.name === "Array") {
            const length = roomList.length;
            for (let i = 0; i < length; i++) {
                const roomName = roomList[i]["WS_NAME"] ? roomList[i]["WS_NAME"] : "이름없음";

                if (roomList[i]["WsChList"] && roomList[i]["WsChList"].constructor.name === "Array") {
                    const chLength = roomList[i]["WsChList"].length;
                    for (let j = 0; j < chLength; j++) {
                        if (roomList[i]["WsChList"][j]["CH_TYPE"] === "CHN0006")
                            channelList.push({
                                name: roomName,
                                channel: roomList[i]["WsChList"][j]
                            });
                    }
                }
            }
        }

        return channelList;
    };

    const _getFileList = (folderId, appNumber, searchStr) => {
        // 선택사항 (필요할때 넣자.)
        // "&directoryID=" + driveCurrentFolderId +
        // "&time=0" +
        // "&order=ua" +
        // "&rownum=20";
        let serviceUrl = `${_workspace.fileUrl}DriveList?directoryID=${folderId}`;
        if (searchStr) {
            serviceUrl += `&query=${searchStr}`;
        }        
        if (appNumber) {
            serviceUrl += `&appNumber=${appNumber}`;
        }

        return axios.get(serviceUrl);
    };

    const _onSidebarItemClick = event => {
        const item = event.target.closest(".drive__attachfile__sidebar__item");

        _currentChannelId_attachFile = item.getAttribute("data-channel-id");
        _currentFolderId_attachFile = _currentChannelId_attachFile;

        let appNumber = null;
        if (_isDrive_attachFile) {
            appNumber = item.getAttribute("data-app-number");
            _appNumber = appNumber;
        }
        
        let sideBarImageTag = document.getElementsByClassName('drive__attachfile__sidebar__image');      
        _paths_attachfile = [];
        if (_appNumber === "1") {
            _paths_attachfile.push({
                folder_id: _currentChannelId_attachFile,
                folder_name: "Talk"
            });

            _rootElement_attachFile.classList.remove("dnd__container");
            _rootElement_attachFile.removeAttribute("data-dnd-app");
            sideBarImageTag[0].src = './res/drive/space_folder.png'
            sideBarImageTag[1].src = './res/drive/talk_folder_active.svg';    
            document.querySelector(".drive__attachfile__search__input input").placeholder = "Talk 첨부함 검색";
        } else if (_appNumber === "6") {
            _paths_attachfile.push({
                folder_id: _currentChannelId_attachFile,
                folder_name: "Root"
            });

            _rootElement_attachFile.classList.add("dnd__container");
            _rootElement_attachFile.setAttribute("data-dnd-app", "drive");
            sideBarImageTag[0].src = './res/drive/space_folder_active.png';
            sideBarImageTag[1].src = './res/drive/talk_folder.svg'
            document.querySelector(".drive__attachfile__search__input input").placeholder = "드라이브 검색";
                      
        }

        const selectedItem = document.querySelector(".drive__attachfile__sidebar__item--selected");
        if (selectedItem) {
            selectedItem.classList.remove("drive__attachfile__sidebar__item--selected");
        }
        item.classList.add("drive__attachfile__sidebar__item--selected");

        renderView();
    };

    const renderView = () => {
        const textElement = document.querySelector(".drive__attachfile__search__input input");
        const searchStr = textElement ? textElement.value : "";
    	
        _getFileList(_currentFolderId_attachFile, _appNumber, searchStr)
            .then(res => {
            	// _fileList_attachFile = res.data.dto.teeDriveDtoList;
            	_fileList_attachFile = [];
            	tempList = res.data.dto.teeDriveDtoList;
            	
            	for(let i=0; i<tempList.length; i++){
            		if(tempList[i].storageFileInfo.user_context_2 != false.toString())
            		_fileList_attachFile.push(tempList[i]);
            	}
            	
                _renderFiles(_fileList_attachFile);
                _updateBreadCrumb();
            })
            .catch(err => {
                console.log("DRIVE LIST : ERR", err);
            })
    };

    const _updateBreadCrumb = () => {
        const length = _paths_attachfile.length;
        const breadCrumb = document.querySelector(".drive__attachfile__bread-crumb");

        let template = "";
        if (breadCrumb) {
            while (breadCrumb.hasChildNodes()) {
                breadCrumb.removeChild(breadCrumb.firstChild);
            }

            for (let i = 0; i < length; i++) {
                // 양끝
                if (i === 0 || i === length - 1) {
                    template += `<span class='drive__attachfile__bread-crumb__item' data-path-type='item' data-folder-id='${_paths_attachfile[i]["folder_id"]}'>${_paths_attachfile[i]["folder_name"]}</span>`;
                    if (i < length - 1) {
                        template += `<span style='margin:0 0.3125rem;'>&gt;</span>`;
                    }
                }

                // 3개 이상인 경우
                if (length > 2 && i === 1) {
                    template += `<span class='drive__attachfile__bread-crumb__item' data-path-type='ellipsis'>...</span>`;
                    if (i < length - 1) {
                        template += `<span style='margin:0 0.3125rem;'>&gt;</span>`;
                    }
                }
            }
            template += "</div>";
            breadCrumb.insertAdjacentHTML("afterbegin", template);
            breadCrumb.addEventListener("click", _onBreadCrumbClick);
        }
    };

    const _onBreadCrumbClick = event => {
        const target = event.target.closest(".drive__attachfile__bread-crumb__item");
        if (target) {
            if (target.getAttribute("data-path-type") === "item") {
                const folderId = target.getAttribute("data-folder-id");
                _onBreadCrumbItemClick(folderId);
            } else if (target.getAttribute("data-path-type") === "ellipsis") {
                _onBreadCrumbPathClick();
            }
        }
    };

    const _onBreadCrumbItemClick = folderId => {
        const length = _paths_attachfile.length;
        let idx = -1;
        for (let i = 0; i < length; i++) {
            if (_paths_attachfile[i]["folder_id"] === folderId) {
                idx = i;
                break;
            }
        }

        // 마지막 클릭이면
        if (idx === _paths_attachfile.length - 1) {
            return;
        }

        if (idx > -1) {
            _paths_attachfile = _paths_attachfile.slice(0, idx + 1);
            _currentFolderId_attachFile = folderId;
            renderView();
        }
    };

    const _onBreadCrumbPathClick = () => {
        const length = _paths_attachfile.length;
        const renderData = [];
        if (length === 0) return;
        _paths_attachfile.forEach((path, index) => {
            if (index > 0 && index < length - 1)
                renderData.push({
                    text: path["folder_name"],
                    value: path["folder_id"],
                    onclick: function() {
                        _onBreadCrumbItemClick(this.value);
                    }
                });
        });

        const css = {
            position: "fixed",
            left: event.clientX + "px",
            top: event.clientY + "px"
        };
        talkContextMenu.render(
            renderData,
            ".drive",
            css,
            function() {},
            function() {}
        );
    };

    const _createPrevFolderTemplate = () => {
        const prevFolder = document.createElement("div");
        prevFolder.classList.add("file__item__attachfile");
        prevFolder.classList.add("file__item__attachfile__prev");
        prevFolder.setAttribute("data-file-id", _paths_attachfile[_paths_attachfile.length - 2]["folder_id"]);
        prevFolder.setAttribute("data-is-folder", "true");
        prevFolder.setAttribute("data-filter-type", "always");
        
        const extensionInfo = fileExtension.getInfo("folder");
        const fileIcon = extensionInfo["iconImage"];
        
        template = `<div class='file__item__attachfile__icon'>
        				<img src='${fileIcon}' draggable='false'/>
            		</div>
                    <span class='file__item__attachfile__title'>...</span>
                    <span class='file__item__attachfile__time'></span>
                    <span class='file__item__attachfile__size'></span>`;

        prevFolder.insertAdjacentHTML("afterbegin", template);

        return prevFolder;
    };

    const _onViewChangeClick = event => {
        _isGrid_attachFile= !_isGrid_attachFile;

        const icon = event.target;
        if (_isGrid_attachFile) {
            icon.classList.remove("icon-palette_gridlayout");
            icon.classList.add("icon-work_format_list");
        } else {
            icon.classList.remove("icon-work_format_list");
            icon.classList.add("icon-palette_gridlayout");
        }

        _updateView(_isGrid_attachFile, _isExpand_attachFile);
    };

    const _onNewFolderClick = () => {
    	Top.Dom.selectById("driveNewFolderDialog").open();
    	Top.Dom.selectById("driveNewFolderDialog").setProperties({
    		from : "attachDialog",
    	})
    };
    
    const _createFileTemplate = file => {
        const isFolder = file["is_folder"] === "true";
        const fileId = isFolder ? file["file_id"] : file["storageFileInfo"]["file_id"];
        const extension = isFolder ? null : file["storageFileInfo"]["file_extension"].toLowerCase();
        //const name = isFolder ? file["folder_name"] : file["storageFileInfo"]["file_name"] + "." + extension;
        
        const textElement = document.querySelector(".drive__attachfile__search__input input");
        const searchStr = textElement ? textElement.value : "";      
        
    	let fileName = isFolder ? file["folder_name"] : file["storageFileInfo"]["file_name"];
    	let beginIdx = fileName.toLowerCase().indexOf(searchStr);
    	let colorStr = fileName.substr(beginIdx, searchStr.length);
    	let colorName = fileName.replace(colorStr, String.format("<span class='drive-font-convert'>{0}</span>", colorStr));
    	let name = isFolder ? colorName : colorName + "." + extension;
        
        const lastUpdateUser = isFolder ? EM_DASH : file["storageFileInfo"]["file_last_update_user_id"];
        const createdAt = isFolder
            ? getDriveFileDateFormat(file["created_time"])
            : getDriveFileDateFormat(file["storageFileInfo"]["file_created_at"]);
        const size = isFolder ? EM_DASH : getFileSizeText(file["storageFileInfo"]["file_size"]);

        const fileItem = document.createElement("div");
        
        const extensionInfo = fileExtension.getInfo(isFolder ? "folder" : extension);
        const fileType = extensionInfo["type"];
        const fileIcon = extensionInfo["iconImage"];
        
        fileItem.classList.add("file__item__attachfile");
        fileItem.setAttribute("data-file-id", fileId);
        fileItem.setAttribute("data-is-folder", isFolder);
        fileItem.setAttribute("data-filter-type", fileType);

        // 이미지 일 경우.
        let imageUrl = null;
        if (_fileType[extension] === "image") {
            imageUrl = storageManager.GetFileUrl(fileId, _currentChannelId_attachFile);
            fileItem.setAttribute("data-image-url", imageUrl);
        }
        // onchange=onCheckChanged('${fileId}')
        // onclick=onFileFolderClick('${fileId}')
        // onclick=onFileFolderClick('${fileId}')

        const template = `<input id='checkbox-attachfile-${fileId}' type='checkbox' />
                          <label for='checkbox-attachfile-${fileId}'><span></span></label>
                          <div class='file__item__attachfile__icon'>
                            <img src='${fileIcon}' draggable='false'/>
                          </div>
                          <div class='file__item__attachfile__text-container'>
                            <span class='file__item__attachfile__title'>${name}</span>                            
                            <span class='file__item__attachfile__time'>${createdAt}</span>
                            <span class='file__item__attachfile__last-update-user'>${lastUpdateUser}</span>
                            <span class='file__item__attachfile__size'>${size}</span>
                          </div>`;
                          
        // <span class='file__item__attachfile__more
		// icon-ellipsis_vertical_small'></span>
        // 
        
        fileItem.insertAdjacentHTML("afterbegin", template);

        return fileItem;
    };

    const _renderFiles = files => {
        const length = files.length;
        
        const textElement = document.querySelector(".drive__attachfile__search__input input");
        const searchStr = textElement ? textElement.value : "";
        const container = document.querySelector(".drive__attachfile__files");
        const checkboxList = document.querySelectorAll(".drive__attachfile__files input[type='checkbox']");
        
        if (container) {
            while (container.hasChildNodes()) {
                container.removeChild(container.firstChild);
            }

            const frag = document.createDocumentFragment();

            if (!_isRoot() && _paths_attachfile.length > 1) {
                frag.appendChild(_createPrevFolderTemplate());
            }

            if (length) {
                for (let i = 0; i < length; i++) {
                    frag.appendChild(_createFileTemplate(files[i])); 
                }
            } else {
            	if (_isRoot() && _appNumber == "1" && searchStr == "") {
                    // empty 레이아웃 출력
                    frag.appendChild(_createTalkEmptyFileElement());
                } else if (_isRoot() && _appNumber == "6" && searchStr == "") {
                	frag.appendChild(_createDriveEmptyFileElement());
                } else if (_isRoot && searchStr != "") {
                    frag.appendChild(_createSearchNoFileElement());
                }
            }

            container.appendChild(frag.cloneNode(true));
            _updateView(_isGrid_attachFile, _isExpand_attachFile);

            container.addEventListener("click", _onFileClick);
            container.addEventListener("click", _onMoreButtonClick);
            container.addEventListener("change", _onFileSelect);
        }
    };

    const _createTalkEmptyFileElement = () => {
        const rootElement = document.createElement("div");
        rootElement.classList.add("drive__attachfile__empty");

        const template = `<img src='./res/img_no_file.png' />
                          <span style='font-size:0.94rem;font-weight:bold;color:#000000;margin-bottom:0.75rem;'>파일이 없습니다.</span>
                          <span style='font-size:0.75rem;color:#777777;margin-bottom:0.1rem'>채팅방에서 주고 받은 파일을 여기서 모아보고,</span>
                          <span style='font-size:0.75rem;color:#777777;'>간편하게 관리할 수 있습니다.</span>`;

        rootElement.insertAdjacentHTML("afterbegin", template);

        return rootElement;
    };
    
    const _createDriveEmptyFileElement = () => {
        const rootElement = document.createElement("div");
        rootElement.classList.add("drive__empty");

        const template = `<img src='./res/img_no_file.png' draggable='false'/>
                          <span style='font-size:0.94rem;font-weight:bold;color:#000000;margin-bottom:0.75rem;'>파일이 없습니다.</span>
                          <span style='font-size:0.75rem;color:#777777;margin-bottom:0.1rem'>스페이스 멤버들과 함께 파일을 공유할 수 있는</span>
                          <span style='font-size:0.75rem;color:#777777;'>공용 저장 공간입니다.</span>`;

        rootElement.insertAdjacentHTML("afterbegin", template);

        return rootElement;
    };
    
    const _createSearchNoFileElement = () => {
        const rootElement = document.createElement("div");
        rootElement.classList.add("drive__attachfile__search__empty");

        const template = `<span style='font-size:0.75rem;color:#777777;margin-bottom:0.5rem'>검색 결과가 존재하지 않습니다.</span>
        				  <img src='./res/note/img_no_results.png' draggable='false'/>`;

        rootElement.insertAdjacentHTML("afterbegin", template);        
        return rootElement;
    };
    
    // root 폴더 인지 확인
    const _isRoot = () => {
        return _currentChannelId_attachFile === _currentFolderId_attachFile;
    };

    const _onMoreButtonClick = event => {
        if (event.target.classList.contains("file__item__attachfile__more")) {
            const target = event.target.closest(".file__item__attachfile");
            const fileId = target.getAttribute("data-file-id");

            let selectedFile = findFile(fileId);

            let offsetX = 0;
            let offsetY = 0;
            let boxWidth = 133;
            let boxHeight = 214;
            var posX = event.clientX;
            var posY = event.clientY;

            offsetY = 242;
            
            if (posX + boxWidth > window.innerWidth - 20) {
            	posX -= boxWidth + offsetX - 10;
            }
            
            if(_appNumber == "6"){
            	if (posY + boxHeight > window.innerHeight) {
            		posY -= offsetY - 105;
            	}
            }
            
            if (selectedFile["is_folder"] === "false") {
                let driveFileContextMenu = Top.Dom.selectById("driveFileContextMenu");
                driveFileContextMenu.open({
                    clientY: isMobile() ? event.clientY - offsetY : posY - 20,
                    clientX: isMobile() ? event.clientX - 160 : posX + 10
                });

                if (_isOfficeFile(selectedFile.storageFileInfo.file_extension) == false) {
                    driveFileContextMenu.getElement("li")[0].hidden = true;
                }

                // 일단 숨김(추후 연계기능 추가)
                driveFileContextMenu.getElement("li")[2].hidden = true;
                driveFileContextMenu.getElement("li")[4].hidden = true;

                if (_appNumber == "1") { // Talk에서는 이름 변경, 삭제 불가
                    driveFileContextMenu.getElement("li")[5].hidden = true;
                    driveFileContextMenu.getElement("li")[6].hidden = true;
                }

                driveFileContextMenu.setProperties({
                    fileInfo: selectedFile
                    // 'on-close' : 'driveContextMenuRemoveId'
                });
            } else {
                driveFolderContextMenu = Top.Dom.selectById("driveFolderContextMenu");
                driveFolderContextMenu.open({
                    clientY: isMobile() ? event.clientY - offsetY : posY - 20,
                    clientX: isMobile() ? event.clientX - 160 : posX + 10
                });

                driveFolderContextMenu.setProperties({
                    fileInfo: selectedFile
                    // 'on-close' : 'driveContextMenuRemoveId'
                });
            }

            event.stopPropagation();
        }
    };

    const findFile = fileId => {
        const length = _fileList_attachFile.length;
        for (let i = 0; i < length; i++) {
            if (_fileList_attachFile[i]["is_folder"] === "true") {
                if (_fileList_attachFile[i]["file_id"] === fileId) {
                    return _fileList_attachFile[i];
                }
            } else {
                if (_fileList_attachFile[i]["storageFileInfo"]["file_id"] === fileId) {
                    return _fileList_attachFile[i];
                }
            }
        }

        return null;
    };

    const _onFileClick = event => {
    	const fileItem = event.target.closest(".file__item__attachfile");    	
    	if(fileItem) { 
    		const isFolder = fileItem.getAttribute("data-is-folder") === "true";
    		
	    	if(isFolder) {    	
		        if (
		            event.target.closest(".file__item__attachfile__icon") ||
		            // event.target.classList.contains("file__item__attachfile__icon")
					// ||
		            event.target.classList.contains("file__item__attachfile__title")
		        ) {	            
		            if (fileItem) {
		                const fileId = fileItem.getAttribute("data-file-id");
		                const fileInfo = findFile(fileId);
		
		                if (!isFolder) {
		                    if (_isOfficeFile(fileInfo.storageFileInfo.file_extension)) {
		                        const data = {
		                            fileId: fileId,
		                            fileName: fileInfo.storageFileInfo.file_name,
		                            fileExtension: fileInfo.storageFileInfo.file_extension
		                        };
		                        officeManager.callOffice(data);
		                    } else {
		                        storageManager.DownloadFiles(
		                            [fileId],
		                            _currentChannelId_attachFile,
		                            function() {},
		                            function() {},
		                            false
		                        );
		                    }
		                } else {
		                    // 빼거나, 더하거나 해야됨.
		                    if (_paths_attachfile.length > 1 && _paths_attachfile[_paths_attachfile.length - 2]["folder_id"] === fileId) {
		                        _paths_attachfile.splice(_paths_attachfile.length - 1, 1);
		                    } else {
		                        _paths_attachfile.push({
		                            folder_id: fileId,
		                            folder_name: fileInfo["folder_name"]
		                        });
		                    }
		                    _currentFolderId_attachFile = fileId;
		                    renderView();
		                }
		            }
		        }
	    	}
    	}
    };

    const _isOfficeFile = extension => {
        if (String.isNullOrEmpty(extension)) {
            return false;
        }

        extension = extension.toLowerCase();

        return (
            extension == "ppt" ||
            extension == "pptx" ||
            extension == "tpt" ||
            extension == "doc" ||
            extension == "docx" ||
            extension == "toc" ||
            extension == "xls" ||
            extension == "xlsx" ||
            extension == "tls" ||
            extension == "csv"
        );
    };

    const _onFileSelect = event => {
        const checkbox = event.target;
        const fileItem = checkbox.closest(".file__item__attachfile");

        if (fileItem) {
            if (checkbox.checked === true) {
                fileItem.classList.add("file__item__attachfile--selected");
            } else {
                fileItem.classList.remove("file__item__attachfile--selected");
            }

            const checkboxList = document.querySelectorAll(".file__item__attachfile input[type='checkbox']");
            const length = checkboxList.length;
            let checkedFolderCount = 0;
            let checkedFileCount = 0;
            let visibleFileCount = 0;
            _selectedFileList_attachFile = [];
            for (let i = 0; i < length; i++) {
                const _fileItem = checkboxList[i].closest(".file__item__attachfile");
                const _isFolder = _fileItem.getAttribute("data-is-folder");
                const _fileId = _fileItem.getAttribute("data-file-id");
                if (_fileItem.style.display != "none") visibleFileCount++;
                if (checkboxList[i].checked && _fileItem.style.display != "none") {
                    _selectedFileList_attachFile.push(findFile(_fileId));
                    checkedFileCount++;
                    if (_isFolder === "true") {
                        checkedFolderCount++;
                    }
                }
            }

            if(checkedFileCount == 0 || checkedFolderCount > 0) {
            	Top.Dom.selectById("successBtn").setDisabled(true);
            } else {
            	Top.Dom.selectById("successBtn").setDisabled(false);
            }
            // _updateButtons(checkedFileCount === visibleFileCount,
			// !!checkedFileCount, !!checkedFolderCount);
        }
    };

    const _onAllFileSelect = event => {
        const isChecked = event.target.checked;
        const checkboxList = document.querySelectorAll(".drive__attachfile__files input[type='checkbox']");
        if (checkboxList) {
            const length = checkboxList.length;
            for (let i = 0; i < length; i++) {
                if (
                    checkboxList[i].closest(".file__item__attachfile").style.display != "none" &&
                    checkboxList[i].checked !== isChecked
                ) {
                    const event = new MouseEvent("click", {
                        bubbles: false,
                        cancelable: false,
                        view: window
                    });

                    checkboxList[i].dispatchEvent(event);
                }
            }
        }
    };

    const _updateButtons = (isAllChecked, isChecked, isIncludeFolder) => {

    };

    const _updateView = (isGrid, isExpand) => {
        const fileTemplates = document.querySelectorAll(".file__item__attachfile");
        const length = fileTemplates.length;
        for (let i = 0; i < length; i++) {
            const imageUrl = fileTemplates[i].getAttribute("data-image-url");
            if(imageUrl){
                if(isGrid){
                    fileTemplates[i].style.backgroundImage = `url('${imageUrl}')`;
                    fileTemplates[i].querySelector(".file__item__attachfile__icon").style.opacity = '0';
                } else{
                    fileTemplates[i].style.backgroundImage = `none`;
                    fileTemplates[i].querySelector(".file__item__attachfile__icon").style.opacity = '1';
                }
            } else {
                fileTemplates[i].style.backgroundImage = `none`;
                fileTemplates[i].querySelector(".file__item__attachfile__icon").style.opacity = '1';
            }

            fileTemplates[i].classList.add(`file__item__attachfile${isGrid ? "--grid" : "--list"}`);
            fileTemplates[i].classList.remove(`file__item__attachfile${isGrid ? "--list" : "--grid"}`);

            fileTemplates[i].classList.add(`file__item__attachfile${isExpand ? "--main" : "--sub"}`);
            fileTemplates[i].classList.remove(`file__item__attachfile${isExpand ? "--sub" : "--main"}`);
        }
    };

    const getFileList = () => {
        return _fileList_attachFile;
    };

    const getAppNumber = () => {
        if (_isDrive_attachFile) {
            return _appNumber;
        }

        return null;
    };

    const getSelectedFileList = () => {
        return _selectedFileList_attachFile;
    };

    const getPaths = () => {
        return _paths_attachfile;
    };

    // 필수 구현
    const onDragStart = (e, data) => {
        // background template
        const element = document.createElement("div");
        Object.assign(element.style, {
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            width: "6.25rem",
            height: "6.25rem",
            backgroundColor: "pink"
        });
        element.innerText = "FILE!";

        // background set
        dnd.setBackground(element);

        // default modal
        dnd.useDefaultEffect(true);

        // data set
        dnd.setData({ "1": 1, "2": 2, "3": 3 });
    };

    // 필수 구현
    const onDrop = (e, dropData) => {
        const { fromApp, data, files } = dropData;
        switch (fromApp) {
            // fropApp 에 따른 처리 (타앱 -> drive)
            // 타 앱에서 이미 업로드된 파일이므로, file id 만 알면 복사 가능할 듯.
            case "talk":
                break;

            case "note":
                break;

            case "mail":
                break;

            case "meeting":
                break;

            case "office":
                break;

            case "schedule":
                break;

            // file 업로드에 대한 처리 (바탕화면 -> drive)
            default:
                if (files && files.length) {
                    _uploadFiles(files, _appNumber);
                }
                break;
        }
    };

    const onDragOver = (e, data) => {
        // 드래그 배경 이미지가 영역에 따라 변화해야 할 경우 구현.
    };

    const onDragEnter = (e, data) => {
        // 기본 modal 사용 안할경우 구현.
    };

    const onDragLeave = (e, data) => {
        // 기본 modal 사용 안할경우 구현.
    };

    const getDriveFileDateFormat = time => {
        let curDate = new Date();
        let date = new Date(time);
        let str = String.empty;

        if (curDate.getDate() != date.getDate() && curDate.getDate() - 1 != date.getDate()) {
            str +=
                date
                    .getFullYear()
                    .toString()
                    .slice(2, 4) + ".";
            str += String.format("{0}.{1} ", ("0" + (date.getMonth() + 1)).slice(-2), ("0" + date.getDate()).slice(-2));
        } else if (curDate.getDate() - 1 == date.getDate()) {
            str = "어제";
        } else {
            str = "오늘";
        }

        return str;
    };

    const getDriveDetailDateFormat = time => {
        let curDate = new Date();
        let date = new Date(time);
        
        let str = String.empty;
        // if(curDate.getFullYear() != date.getFullYear()) {
            str += String.format("{0}-", date.getFullYear())
        // }
        
        // if(curDate.getMonth() != date.getMonth() || curDate.getDate() !=
		// date.getDate()) {
            str += String.format("{0}-{1} ", ('0' + (date.getMonth()+1)).slice(-2), ('0' + date.getDate()).slice(-2))
        // }
        
        let halfDayFormat = getHalfDayFormat(time);
        str += String.format("{0} {1}", (halfDayFormat.meridiem == 0) ? Top.i18n.t("value.m000147") : Top.i18n.t("value.m000148"), halfDayFormat.time);
        
        return str;
    };

    const uploadFolder = (folderName) => {
        const inputData = {
            dto: {
                workspace_id: _currentWorkspaceId_attachFile,
                app_number: _appNumber,
                user_id: userManager.getLoginUserId(),
                ch_id: _currentChannelId_attachFile,
                file_parent_id: _currentFolderId_attachFile,
                folder_name: folderName
            }
        };

        return axios.post(`${_workspace.fileUrl}DriveFolder`, inputData);
        // return _ajaxCall("DriveFolder", "POST", _driveUploadFolderDTO(folderName), successCallback);
    };

    return {
        render,
        getFileList,
        getSelectedFileList,
        uploadFolder,
        getAppNumber,
        renderView,
        deleteFiles,
        findFile,
        getPaths,
        getChannelId,
        getCurrentFolderId,
        onDragStart,
        onDrop,
        onDragOver,
        onDragEnter,
        onDragLeave,
        getDriveDetailDateFormat,
        attachFromTeeDrive,
        
    };
})();


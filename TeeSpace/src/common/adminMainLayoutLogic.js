Top.Controller.create('adminMainLayoutLogic', {
	data: {
		_gnb: {
			id: 'adminGnb',
			//            onClick: Top.Controller.get('adminMainLayoutLogic').onClickGnb,
			onClick: function(event, widget) {
				console.info(widget.getValue());
				Top.App.routeTo(widget.getValue());	
			},
			getGnbList: function(mem_grade) {
				let allGnbList = Top.Controller.get('adminMainLayoutLogic').data._gnb.list;
				let actualGnbList = allGnbList.filter(function (e) {
					return e.grade === mem_grade || e.grade === 'both'
				})
				return actualGnbList;
			},
			list: [
				// **************************************************
				//  id: 버튼 아이디
				//  text: 버튼에 보여지는 이름
				//  type: url에 들어가는 타입. 반드시 소문자.
				//  hash: url
				//  classIcon: top-icon의 classIcon 프로퍼티
				//  src: 버튼 이미지 소스
				//  lnb: gnb에 상응하는 lnb 아이디
				//  code: 해당 메뉴의 코드
				// grade: 해당 메뉴가 보이는 계정의 권한 (admin - 고객 어드민만 / superadmin - 슈퍼어드민만 / both - 고객 어드민과 슈퍼어드민 둘 다 보임)
				// **************************************************

				// {
				// 	id: 'gnbButtonAuth',
				// 	text: '권한관리',
				// 	type: 'auth',
				// 	hash: '@@@@@@@@@@@@@@@@@@',
				// 	classIcon: '',
				// 	src: 'res/work_explain.svg',
				// 	lnb: '_lnb_auth',
				// 	code: 'ad0001',
				// grade: '',
				// },
				{
					id: 'gnbButtonCompany',
					text: '회사 관리',
					type: 'company',
					hash: '/admin/company_org',
					classIcon: '',
					src: 'res/work_explain.svg',
					lnb: '_lnb_company',
					code: 'ad0002',
					grade: 'admin',
				},
				{
					id: 'gnbButtonService',
					text: '서비스 관리',
					type: 'service',
					hash: '/admin/manual?mode=list&cate=all&subcate=all&view=ss30&page=1',
					classIcon: '',
					src: 'res/work_explain.svg',
					lnb: '_lnb_service',
					code: '',
					grade: 'superadmin',
				}, 
				// {
				// 	id: 'gnbButtonSecurity',
				// 	text: '보안 관리',
				// 	type: 'security',
				// 	hash: '@@@@@@@@@@@@@@@@@@',
				// 	classIcon: '',
				// 	src: 'res/work_explain.svg',
				// 	lnb: '_lnb_security',
				// 	code: '',
				// grade: '',
				// },
				// {
				// 	id: 'gnbButtonLog',
				// 	text: '감사/로그 관리',
				// 	type: 'log',
				// 	hash: '@@@@@@@@@@@@@@@@@@',
				// 	classIcon: '',
				// 	src: 'res/work_explain.svg',
				// 	lnb: '_lnb_log',
				// 	code: '',
				// grade: '',
				// },
				{
					id: 'gnbButtonStat',
					text: '통계/리포트 관리',
					type: 'stat',
					hash: '/admin/stat_daily',
					classIcon: '',
					src: 'res/work_explain.svg',
					lnb: '_lnb_stat',
					code: '',
					grade: 'admin',
				},
				// {
				// 	id: 'gnbButtonData',
				// 	text: '데이터 관리',
				// 	type: 'data',
				// 	hash: '@@@@@@@@@@@@@@@@@@',
				// 	classIcon: '',
				// 	src: 'res/work_explain.svg',
				// 	lnb: '_lnb_data',
				// 	code: '',
				// grade: 'superadmin',
				// },

				// {
				// 	id: 'gnbButtonPlan',
				// 	text: '요금제 관리',
				// 	type: 'plan',
				// 	hash: '@@@@@@@@@@@@@@@@@@',
				// 	classIcon: '',
				// 	src: 'res/work_explain.svg',
				// 	lnb: '_lnb_plan',
				// 	code: '',
				// grade: '',
				// },
				// {
				// 	id: 'gnbButtonAnnouncement',
				// 	text: '공지 관리',
				// 	type: 'announcement',
				// 	hash: '@@@@@@@@@@@@@@@@@@',
				// 	classIcon: '',
				// 	src: 'res/work_explain.svg',
				// 	lnb: '_lnb_announcement',
				// 	code: '',
				// grade: '',
				// },
			],
		},
		_lnb_company: {
			list: [
				{
					id: 'lnbButtonCompanyOrg',
					parent: 'root',
					text: '조직관리',
					type: 'company_org',
					hash: '/admin/company_org',
				},
				{
					id: 'lnbButtonCompanyGuest',
					parent: 'root',
					text: '게스트 관리',
					type: 'company_guest',
					hash: '/admin/company_guest',
				},
				{
					id: 'lnbButtonCompanyInfo',
					parent: 'root',
					text: '회사 정보 관리',
					type: 'company_info',
					hash: '/admin/company_info',
				},	
			],
		},
		_lnb_service: {
			list: [
				{
					id: 'lnbButtonHelp',
					parent: 'root',
					text: '도움말',
					type: undefined,
					hash: undefined,
				},
				{
					id: 'lnbButtonManual',
					parent: 'lnbButtonHelp',
					text: '사용자 매뉴얼',
					type: 'manual',
					//                    'hash': '/admin/service?sub=manual',
					hash: '/admin/manual?mode=list&cate=all&subcate=all&view=ss30&page=1',
				},
				{
					id: 'lnbButtonFaq',
					parent: 'lnbButtonHelp',
					text: '자주하는 질문(FAQ)',
					type: 'faq',
					//                    'hash': '/admin/service?sub=faq&mode=list&cate=all&subcate=all&view=ss30&page=1',
					hash: '/admin/faq?mode=list&cate=all&subcate=all&view=ss30&page=1',
				},
				{
					id: 'lnbButtonOneonone',
					parent: 'lnbButtonHelp',
					text: '1:1 문의',
					type: 'oneonone',
					//                    'hash': '/admin/service?sub=qna&mode=list&cate=all&subcate=all&view=ss30&page=1',
					hash: '/admin/oneonone?mode=list&cate=all&subcate=all&view=ss30&page=1',
				},
				{
					id: 'lnbButtonQna',
					parent: 'lnbButtonHelp',
					text: 'Q&A',
					type: 'qna',
					//                    'hash': '/admin/service?sub=qna&mode=list&cate=all&subcate=all&view=ss30&page=1',
					hash: '/admin/qna?mode=list&cate=all&subcate=all&view=ss30&page=1',
				},
				{
					id: 'lnbButtonAnnouncement',
					parent: 'root',
					text: '공지',
					type: undefined,
					hash: undefined,
				},
				{
					id: 'lnbButtonServiceAnnouncement',
					parent: 'lnbButtonAnnouncement',
					text: '서비스 공지',
					type: 'service_announcement',
					//                    'hash': '/admin/service?sub=service_announcement&mode=list&cate=all&subcate=all&view=ss30&page=1',
					hash: '/admin/service_announcement?mode=list&cate=all&subcate=all&view=ss30&page=1',
				},
				{
					id: 'lnbButtonUpdateAnnouncement',
					parent: 'lnbButtonAnnouncement',
					text: '업데이트 공지',
					type: 'update_announcement',
					//                    'hash': '/admin/service?sub=update_announcement&mode=list&cate=all&subcate=all&view=ss30&page=1',
					hash: '/admin/update_announcement?mode=list&cate=all&subcate=all&view=ss30&page=1',
				},
				{
					id: 'lnbButtonPlan',
					parent: 'root',
					text: '요금제 관리',
					type: 'plan',
					//                    'hash': '/admin/service?sub=plan',
					hash: '/admin/plan?mode=list&cate=all&subcate=all&view=ss30&page=1',
				},
			],
		},
		_lnb_stat: {
			list: [
				{
					id: 'lnbButtonStat',
					parent: 'root',
					text: '사용 현황',
					type: undefined,
					hash: undefined,
				},
				{
					id: 'lnbButtonStatDaily',
					parent: 'lnbButtonStat',
					text: '1일 현황',
					type: 'stat_daily',
					hash: '/admin/stat_daily',
				},
				{
					id: 'lnbButtonStatMember',
					parent: 'lnbButtonStat',
					text: '멤버 분석',
					type: 'stat_member',
					hash: '/admin/stat_member',
				},
				{
					id: 'lnbButtonStatCloudspace',
					parent: 'lnbButtonStat',
					text: 'CloudSpace 통계',
					type: 'stat_cloudspace',
					hash: '/admin/stat_cloudspace',
				},
			],
		},
	},

	init: function(widget) {
		
		
		
		let that = this;
		let d = that.data;
		let cmp = Top.Controller.get('components');

		d._lnb_service.list = d._lnb_service.list.filter(elem => {
			// B2B -> QNA and B2C -> ONEONONE
			if ((isb2c() && elem.type === 'qna') || (!isb2c() && elem.type === 'oneonone')) {
				return false;
			}
			return true;
		});

		cmp.simpleGnb({
			id: d._gnb.id,
			targetId: 'adminTopLayout',
			list: d._gnb.getGnbList(userManager.getLoginUserInfo().userGrade),
			onClick: d._gnb.onClick,
			type: 1,
			customData: {type: d._gnb.list.map(elem => elem.type)}
		});
		admin.setGnbColor();
		/* logout 관련 */
		let ran_num = Math.floor(Math.random() * 5) + 1;
		Top.Dom.selectById('admin_title_profile').setSrc('res/profile_default_0' + ran_num + '.png');

		$(document)
			.off()
			.on('click', function(e) {
				admin_more_list(e, 'admin_title_profile', 'adminMainLayout');
				$('div#LinearLayout4114 .top-treeview-edit_btn').removeClass('selected');
				$('div#LinearLayout4114 .top-treeview-edit_nav').css({ display: 'none' });
				$('div#LinearLayout4114 .top-treeview-edit_btn').css({ visibility: 'hidden' });
		});
		hideLoadingBar2(); 
	},

	onLogoutClick: function(event, widget) {
		sessionStorage.clear();
		Logout = false;
		userManager.clear();
		deleteCookie('loginkey');
		deleteCookie('token');
		deleteCookie('loginid');
		Top.App.routeTo('/login');
	},
});

function admin_more_list(e, clickID, viewID) {
	if (e.toElement != undefined) {
		if (e.toElement.id == clickID) {
			more_list =
				'<top-linearlayout id="admin_sideBar_drop1" class="position-admin_sideBar_drop1 linear-child-vertical" layout-width="148px" layout-height="auto" layout-horizontal-alignment="center" padding="" margin="" horizontal-alignment="center" vertical-alignment="middle" visible="visible" border-color="rgba(205,205,207,1.0) rgba(205,205,207,1.0) rgba(205,205,207,1.0) rgba(205,205,207,1.0)" border-width="1px 1px 1px 1px" horizontal-scroll="false" vertical-scroll="false" orientation="vertical" position="false" title="" layout-tab-disabled="false">' +
				'<div id="admin_sideBar_drop1" class="top-linearlayout-root">' +
				'<top-button id="admin_sideBar_drop1_Logout" class="noti_drop linear-child-vertical" layout-width="match_parent" layout-height="28px" padding="0px 0px 1px 10px" horizontal-alignment="left" tab-index="0" text="로그아웃" text-color="rgba(51,51,51,1.0)" text-decoration="" icon-position="left" title="" layout-tab-disabled="false" actual-height="29">' +
				'<button id="admin_sideBar_drop1_Logout" class="top-button-root" type="button" tabindex="0">' +
				'<label class="top-button-text">로그아웃</label>' +
				'</button>' +
				'</top-button>' +
				'</div>' +
				'</top-linearlayout>';

			if ($('top-linearlayout#admin_sideBar_drop1').length == 0) {
				if (viewID != 'adminMainLayout') {
					$('#' + viewID + ' .content_0').addClass('mobile-noti_split');
				}

				more_list = $(more_list);
				more_list
					.find('#admin_sideBar_drop1_Logout')
					.on('click', Top.Controller.get('adminMainLayoutLogic').onLogoutClick); //계정 설정
				$('div#adminMainLayout').append(more_list);
				$('.position-admin_sideBar_drop1').css({ top: '47px', right: '13px' });
			} else {
				if (viewID != 'adminMainLayout') {
					$('#' + viewID + ' .content_0').removeClass('mobile-noti_split');
				}
				$('top-linearlayout#admin_sideBar_drop1').remove();
			}
		} else {
			if ($('top-linearlayout#admin_sideBar_drop1').length != 0) {
				$('#' + viewID + ' .content_0').removeClass('mobile-noti_split');
				$('top-linearlayout#admin_sideBar_drop1').remove();
			}
		}
	}
}

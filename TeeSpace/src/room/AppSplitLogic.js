Top.Controller.create('AppSplitLogic', {
	init: function(event, widget){		

		//LNB
		renderLnb(getLnbByUrl());
		
		//메인앱 - 토크 안그려진 경우
		if("talk"===getMainAppByUrl() && !document.querySelector("#talk")) talk.render("div#AppSplitLeftLayout", null, false);
		
		//프렌즈 레이아웃
		if("f"===getLnbByUrl()) _spaceProfilePopup(location.hash.split("?")[0].split("/")[2]);
		
		//스플릿 최대/최소 범위
		$('div#AppSplitMainLayout>.top-splitterlayout-splitter').on('mousedown', ()=>{
			   document.addEventListener('mousemove', adJustSplit);
			   document.addEventListener("mouseup", stopAdjustSplit);
			});
//		$('div#AppSplitMainLayout>.top-splitterlayout-splitter').on('mouseup', ()=>{
//			console.log("!1");
//			document.removeEventListener('mousemove', adJustSplit);
//			console.log("22");
//		}); 
	}
}); 

//추후 수정 필요 - 새로 or top 최대 최소 조건 요청
function adJustSplit(){
    let target = $('div#AppSplitMainLayout>.top-splitterlayout-splitter');
    let curLeft = target.position().left;
    let splitMainWidth = target.parent().width();
    let minRange = splitMainWidth * 0.41666;			//splitMainWidth * 0.3333 - 2;
    let maxRange = splitMainWidth * 0.58333;			//splitMainWidth * 0.6666 - 2;	
    

    if(curLeft < minRange ){
    	$('div#AppSplitMainLayout>.top-splitterlayout-splitter')[0].style.left = (minRange / splitMainWidth  * 100) + '%';        
		//	event.stopPropagation();  //event.preventDefault(); 
    }
    else if(curLeft> maxRange){
    	$('div#AppSplitMainLayout>.top-splitterlayout-splitter')[0].style.left = (maxRange / splitMainWidth * 100) + '%';
    	//	event.stopPropagation(); //event.preventDefault(); 	
    }
}

function stopAdjustSplit(){
	let rand = Math.floor(Math.random() * 100000);
	document.removeEventListener('mousemove', adJustSplit);
	document.removeEventListener('mouseup', stopAdjustSplit);
	let left = parseInt($('.top-splitterlayout-content.content_0').width()) + (0.0000001 * rand);		//꼭 수정 필요
	let right = parseInt($('.top-splitterlayout-content.content_1').width()) - (0.0000001 * rand);
	tds('AppSplitMainLayout').setProperties({'ratio':`${left}:${right}`});
}
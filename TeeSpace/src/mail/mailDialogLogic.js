Top.Controller.create('mailAddClassifyPopupLogic', {

	init : function() {
		
		if(Top.Controller.get('mailAutoClassifyLogic').modifyYN == 'Y') {
			Top.Dom.selectById('mailAddClassifyDialog').setProperties({'title':'분류 수정'});
		} else
			Top.Dom.selectById('mailAddClassifyDialog').setProperties({'title':'분류 추가'});
		
		this.addressList = [];
		this.setAccList();
		this.setBoxList();

		Top.Dom.selectById('mailAddClassifyPopupLayout_SelectAccount').setProperties({'on-change':'setBoxList'});
		
		Top.Dom.selectById('mailAddClassifyPopupLayout_Base_SelectBox').setProperties({'on-change':'changeText'});
		Top.Dom.selectById('mailAddClassifyPopupLayout_Base_AddressBook').setProperties({'on-click':'openAddressBook'});
		Top.Dom.selectById('mailAddClassifyPopupLayout_Base_Address_Add').setProperties({'on-click':'addAddress'});
		Top.Dom.selectById('mailAddClassifyPopupLayout_Base_MailTitle_Input').setProperties({'on-keyup':'requiredYN'});
		Top.Dom.selectById('mailAddClassifyPopupLayout_Base_MailContent_Input').setProperties({'on-keyup':'requiredYN'});
		Top.Dom.selectById('mailAddClassifyPopupLayout_Base_Check_Attach').setProperties({'on-change':'requiredYN'});
		Top.Dom.selectById('mailAddClassifyPopupLayout_Base_Check_SendMe').setProperties({'on-change':'requiredYN'});
		
		Top.Dom.selectById('mailAddClassifyPopupLayout_Way_Check_Move').setProperties({'on-change':'changeDisabled'});
		Top.Dom.selectById('mailAddClassifyPopupLayout_Way_Exist').setProperties({'on-change':'changeBoxDisabled'});
		Top.Dom.selectById('mailAddClassifyPopupLayout_Way_New').setProperties({'on-change':'changeTextDisabled'});
		Top.Dom.selectById('mailAddClassifyPopupLayout_Way_Check_Read').setProperties({'on-change':'requiredYN'});
		Top.Dom.selectById('mailAddClassifyPopupLayout_Way_Check_Imp').setProperties({'on-change':'requiredYN'});
		Top.Dom.selectById('mailAddClassifyPopupLayout_Way_Check_Del').setProperties({'on-change':'requiredYN'});
		
		Top.Dom.selectById('mailAddClassifyPopupLayout_Add').setProperties({'on-click':'checkNewFolder', 'disabled':true});
		Top.Dom.selectById('mailAddClassifyPopupLayout_Cancel').setProperties({'on-click':'closeDialog'});
		
	}, requiredYN : function() {
		
		var selectAcc = Top.Dom.selectById('mailAddClassifyPopupLayout_SelectAccount').getValue() != undefined;
		
		var mailIsNull = Top.Dom.selectById('mailAddClassifyPopupLayout_Base_Hidden_Addrs').getChildren().length != 0;
		var title = Top.Dom.selectById('mailAddClassifyPopupLayout_Base_MailTitle_Input').getText();
		var titleIsNull = !(title == undefined || title == "");
		var content = Top.Dom.selectById('mailAddClassifyPopupLayout_Base_MailContent_Input').getText();
		var contentIsNull = !(content == undefined || content == "");
		var attachIsNull = Top.Dom.selectById('mailAddClassifyPopupLayout_Base_Check_Attach').isChecked();
		var replyIsNull = Top.Dom.selectById('mailAddClassifyPopupLayout_Base_Check_SendMe').isChecked();
		var selectBase = mailIsNull || titleIsNull || contentIsNull || attachIsNull || replyIsNull;
		
		var selectWay = Top.Dom.selectById('mailAddClassifyPopupLayout_Way_Check_Move').isChecked() || Top.Dom.selectById('mailAddClassifyPopupLayout_Way_Check_Read').isChecked() ||
			Top.Dom.selectById('mailAddClassifyPopupLayout_Way_Check_Imp').isChecked() || Top.Dom.selectById('mailAddClassifyPopupLayout_Way_Check_Del').isChecked();
		
		Top.Dom.selectById('mailAddClassifyPopupLayout_Add').setProperties({'disabled':!(selectAcc && selectBase && selectWay)});
		
	}, setAccList : function(event, widget) {
		
		var accList = mailData.accountList;
		var node = [];
		
		for(var i=1; i<accList.length; i++) {
			node.push({'text':accList[i].EMAIL_ADDRESS, 'value':accList[i].ACCOUNT_ID});
		}
		Top.Dom.selectById('mailAddClassifyPopupLayout_SelectAccount').setProperties({'nodes':node});
		
	}, setExistData : function(event, widget) {
		
		var data = Top.Dom.selectById('mailAutoClassifyLayout_Table').getClickedData();
		Top.Dom.selectById('mailAddClassifyPopupLayout_SelectAccount').select(data.RULEMAIN[0].ACCOUNT_ID);
		if(data.ADDR_TYPE != undefined)
			Top.Dom.selectById('mailAddClassifyPopupLayout_Base_SelectBox').select(data.ADDR_TYPE);
		if(data.ADDRESSLIST.length != 0)
			this.addAddrList(data.ADDRESSLIST);
		if(data.SUBJECT != undefined)
			Top.Dom.selectById('mailAddClassifyPopupLayout_Base_MailTitle_Input').setText(data.SUBJECT);
		if(data.CONTENTS != undefined)
			Top.Dom.selectById('mailAddClassifyPopupLayout_Base_MailContent_Input').setText(data.CONTENTS);
		if(data.ATTACH_YN != undefined)
			Top.Dom.selectById('mailAddClassifyPopupLayout_Base_Check_Attach').setChecked(true);
		if(data.REPLY_YN != undefined)
			Top.Dom.selectById('mailAddClassifyPopupLayout_Base_Check_SendMe').setChecked(true);
		if(data.MOVE_FOLDER_YN != undefined) {
			Top.Dom.selectById('mailAddClassifyPopupLayout_Way_Check_Move').setChecked(true);
			Top.Dom.selectById('mailAddClassifyPopupLayout_Way_Exist').setProperties({'disabled':false});
			Top.Dom.selectById('mailAddClassifyPopupLayout_Way_New').setProperties({'disabled':false});
			Top.Dom.selectById('mailAddClassifyPopupLayout_Way_ExistFolder').setProperties({'disabled':false});
			var nodes = Top.Dom.selectById('mailAddClassifyPopupLayout_Way_ExistFolder').getProperties('nodes');
			for(var i=0; i<nodes.length; i++) {
				if(data.MOVE_FOLDER_ID == nodes[i].value.FOLDER_ID)
					Top.Dom.selectById('mailAddClassifyPopupLayout_Way_ExistFolder').select(nodes[i].text);			
			}
		}
		if(data.READ_YN != undefined)
			Top.Dom.selectById('mailAddClassifyPopupLayout_Way_Check_Read').setChecked(true);
		if(data.IMPORTANT_YN != undefined)
			Top.Dom.selectById('mailAddClassifyPopupLayout_Way_Check_Imp').setChecked(true);
		if(data.DEL_YN != undefined)
			Top.Dom.selectById('mailAddClassifyPopupLayout_Way_Check_Del').setChecked(true);
		
	}, changeDisabled : function(event, widget) {
		
		this.requiredYN();
		
		if(widget.isChecked()) {
			Top.Dom.selectById('mailAddClassifyPopupLayout_Way_Exist').setProperties({'disabled':false});
			Top.Dom.selectById('mailAddClassifyPopupLayout_Way_New').setProperties({'disabled':false});
			if(Top.Dom.selectById('mailAddClassifyPopupLayout_Way_Exist').isChecked())
				Top.Dom.selectById('mailAddClassifyPopupLayout_Way_ExistFolder').setProperties({'disabled':false});
			else
				Top.Dom.selectById('mailAddClassifyPopupLayout_Way_NewFolder').setProperties({'disabled':false});
		} else {
			Top.Dom.selectById('mailAddClassifyPopupLayout_Way_Exist').setProperties({'disabled':true});
			Top.Dom.selectById('mailAddClassifyPopupLayout_Way_New').setProperties({'disabled':true});
			Top.Dom.selectById('mailAddClassifyPopupLayout_Way_NewFolder').setProperties({'disabled':true});
			Top.Dom.selectById('mailAddClassifyPopupLayout_Way_ExistFolder').setProperties({'disabled':true});
		}
		
	}, changeTextDisabled : function(event, widget) {
		
		if(Top.Dom.selectById('mailAddClassifyPopupLayout_Way_New').isChecked()) {
			Top.Dom.selectById('mailAddClassifyPopupLayout_Way_ExistFolder').setProperties({'disabled':true});
			Top.Dom.selectById('mailAddClassifyPopupLayout_Way_NewFolder').setProperties({'disabled':false});
		} else {
			Top.Dom.selectById('mailAddClassifyPopupLayout_Way_ExistFolder').setProperties({'disabled':false});
			Top.Dom.selectById('mailAddClassifyPopupLayout_Way_NewFolder').setProperties({'disabled':true});
		}
		
	}, closeDialog: function() {
		
		Top.Dom.selectById('mailAddClassifyDialog').close();
		
	}, checkNewFolder : function() {
		
		if(Top.Dom.selectById('mailAddClassifyPopupLayout_Way_Check_Move').isChecked() && Top.Dom.selectById('mailAddClassifyPopupLayout_Way_New').isChecked()) {
			
			var index;
			
			for(var i=0; i<mailData.accountList.length; i++) {
				if(Top.Dom.selectById('mailAddClassifyPopupLayout_SelectAccount').getValue() == mailData.accountList[i].ACCOUNT_ID)
					index = i;
			}
			var data={
					dto: {
							"ACCOUNT_ID"	: Top.Dom.selectById('mailAddClassifyPopupLayout_SelectAccount').getValue(),
							"FOLDER_ID"		: Top.Dom.selectById('mailMainLayout_MailBox_Text'+index).getProperties('value').FOLDER_ID,
							"DISPLAY_NAME"	: Top.Dom.selectById('mailAddClassifyPopupLayout_Way_NewFolder').getText(),
							"CMFLG"			: "CreateSubfolder"
					
					}
			};

			Top.Ajax.execute({
				
				type : "POST",
				url : _workspace.url + "Mail/Mail?action=SubfolderCreate",
				dataType : "json",
				data	 : JSON.stringify(data),
				contentType: "application/json",
			    xhrFields: {withCredentials: true},
				crossDomain : true,
				success : function(ret,xhr,status) {
					Top.Controller.get('mailAddClassifyPopupLogic').addSort(ret.dto.msgResultContent);
				}
			});
		} else
			this.addSort('');
			
		
	}, addSort : function(folderId) {
		
		
		var addrList = [];
		
		for(var i=0; i<Top.Dom.selectById('mailAddClassifyPopupLayout_Base_Hidden_Addrs').getChildren().length; i++) {
			
			addrList.push(Top.Dom.selectById('mailAddClassifyPopupLayout_Base_Hidden_Addrs').getChildren()[i].getChildren()[0].getText());
		}
		
		var rule_main = [];
		var accountId = Top.Dom.selectById('mailAddClassifyPopupLayout_SelectAccount').getValue();
		var ruleKey = Top.Dom.selectById('mailAddClassifyPopupLayout_Base_SelectBox').getValue();
		var mailTitle = Top.Dom.selectById('mailAddClassifyPopupLayout_Base_MailTitle_Input').getText();
		var mailContents = Top.Dom.selectById('mailAddClassifyPopupLayout_Base_MailContent_Input').getText();
		var userId = userManager.getLoginUserId();
		var folderName = '';
		
		for(var i=0; i<addrList.length; i++) {
			rule_main.push({
				"USER_ID"	 : userId,
				"ACCOUNT_ID" : accountId,
				"KEY"		 : ruleKey,
				"VALUE"		 : addrList[i]
			});
		}
		
		if(mailTitle != '' && mailTitle != undefined) {
			rule_main.push({
				"USER_ID"	 : userId,
				"ACCOUNT_ID" : accountId,
				"KEY"		 : 'MRU0005',
				"VALUE"		 : mailTitle
			});
		}
		
		if(mailContents != '' && mailContents != undefined) {
			rule_main.push({
				"USER_ID"	 : userId,
				"ACCOUNT_ID" : accountId,
				"KEY"		 : 'MRU0006',
				"VALUE"		 : mailContents
			});
		}
		
		if(Top.Dom.selectById('mailAddClassifyPopupLayout_Base_Check_Attach').isChecked()) {
			rule_main.push({
				"USER_ID"	 : userId,
				"ACCOUNT_ID" : accountId,
				"KEY"		 : 'MRU0007',
				"VALUE"		 : 'COM0001'
			});
		}
		
		if(Top.Dom.selectById('mailAddClassifyPopupLayout_Base_Check_SendMe').isChecked()) {
			rule_main.push({
				"USER_ID"	 : userId,
				"ACCOUNT_ID" : accountId,
				"KEY"		 : 'MRU0008',
				"VALUE"		 : 'COM0001'
			});
		}
		
		if(Top.Dom.selectById('mailAddClassifyPopupLayout_Way_Check_Read').isChecked()) {
			rule_main.push({
				"USER_ID"	 : userId,
				"ACCOUNT_ID" : accountId,
				"KEY"		 : 'MRU0009',
				"VALUE"		 : 'COM0001'
			});
		}
		
		if(Top.Dom.selectById('mailAddClassifyPopupLayout_Way_Check_Imp').isChecked()) {
			rule_main.push({
				"USER_ID"	 : userId,
				"ACCOUNT_ID" : accountId,
				"KEY"		 : 'MRU0010',
				"VALUE"		 : 'COM0001'
			});
		}
		
		if(Top.Dom.selectById('mailAddClassifyPopupLayout_Way_Check_Del').isChecked()) {
			rule_main.push({
				"USER_ID"	 : userId,
				"ACCOUNT_ID" : accountId,
				"KEY"		 : 'MRU0011',
				"VALUE"		 : 'COM0001'
			});
		}

		if(Top.Dom.selectById('mailAddClassifyPopupLayout_Way_Check_Move').isChecked()) {
			if(Top.Dom.selectById('mailAddClassifyPopupLayout_Way_Exist').isChecked()) {
				folderId = Top.Dom.selectById('mailAddClassifyPopupLayout_Way_ExistFolder').getValue().FOLDER_ID;
				folderName = Top.Dom.selectById('mailAddClassifyPopupLayout_Way_ExistFolder').getValue().DISPLAY_NAME;
			} else if(Top.Dom.selectById('mailAddClassifyPopupLayout_Way_New').isChecked()) {
				folderName = Top.Dom.selectById('mailAddClassifyPopupLayout_Way_NewFolder').getText();				
			}
			
			rule_main.push({
				"USER_ID"	 : userId,
				"ACCOUNT_ID" : accountId,
				"KEY"		 : 'MRU0012',
				"VALUE"		 : folderId
			});
			
		}


		var data;
		var service;
		
		if(Top.Dom.selectById('mailAddClassifyDialog').getProperties('title') == '분류 수정') {
			data = {
					dto: {
						"rule_main"			: rule_main,
						'dbio_total_count' 	: rule_main.length,
						"FOLDER_ID"			: folderId,
						"FOLDER_NAME"		: folderName,
						"RULE_ID"			: Top.Dom.selectById('mailAutoClassifyLayout_Table').getClickedData().RULEMAIN[0].RULE_ID,
						"CMFLG"				: "MailRuleModification"
					}
			};
			service = "RuleModification";
		} else {
			data = {
					dto: {
						"rule_main"			: rule_main,
						'dbio_total_count' 	: rule_main.length,
						"FOLDER_ID"			: folderId,
						"FOLDER_NAME"		: folderName
					}
			};
			service = "RuleAdd";
		}
		
		
		
		Top.Ajax.execute({
			
			type : "POST",
			url : _workspace.url + "Mail/Mail?action=" + service,
			dataType : "json",
			data	 : JSON.stringify(data),
			crossDomain: true,
			contentType: "application/json",
		    xhrFields: {withCredentials: true},
			success : function(ret,xhr,status) {
				Top.Dom.selectById('mailAddClassifyDialog').close();
				Top.Controller.get('mailMainLeftLogic').setFolderOnly();
				Top.Controller.get('mailAutoClassifyLogic').setList();
				
			}
		});
			
			
		
	}, addAddress : function() {

		var addr = Top.Dom.selectById('mailAddClassifyPopupLayout_Base_Address_Text').getText();
		
		if(addr == '' || addr == undefined) {
			Top.Dom.selectById('mailAddClassifyPopupLayout_Base_Address_Error').setProperties({'tooltip':'이메일 주소 혹은 도메인 주소를 입력해 주세요.'});
			Top.Dom.selectById('mailAddClassifyPopupLayout_Base_Address_Error').setProperties({'visible':'visible'});
			return;
		} else if(addr.indexOf('@') == -1 || addr.indexOf('.') == -1) {
			Top.Dom.selectById('mailAddClassifyPopupLayout_Base_Address_Error').setProperties({'tooltip':'올바른 주소 형식을 입력해 주세요.'});
			Top.Dom.selectById('mailAddClassifyPopupLayout_Base_Address_Error').setProperties({'visible':'visible'});
			return;
		}
		Top.Dom.selectById('mailAddClassifyPopupLayout_Base_Address_Error').setProperties({'visible':'none'});
		
		this.addAddrList(addr.split(', '));		
		
		
	}, delAddress : function(event, widget) {
		
		
		Top.Dom.selectById('mailAddClassifyPopupLayout_Base_Hidden_Addrs').removeWidget(widget.getParent().getParent().getParent());
		if(Top.Dom.selectById('mailAddClassifyPopupLayout_Base_Hidden_Addrs').getChildren().length == 0)
			Top.Dom.selectById('mailAddClassifyPopupLayout_Base_Hidden').setProperties({'visible':'none'});

		this.requiredYN();
		
		
	}, openAddressBook : function() {
		
		Top.Dom.selectById('mailAddressBookDialog').open();
		
	}, changeText : function(event, widget) {
		
		Top.Dom.selectById('mailAddClassifyPopupLayout_Base_Hidden_Text').setText(widget.getSelectedText());
		
	}, setBoxList : function(event, widget) {
		
		this.requiredYN();
		
		var folders = [];
		

		Top.Ajax.execute({
			
			type : "GET",
			url : _workspace.url + "Mail/Mail?action=BasicInfo&CMFLG=BasicInfo&USER_ID="+userManager.getLoginUserId()+"&WS_USE_CODE=USE0001",
			dataType : "json",
			crossDomain: true,
			contentType: "application/json",
		    xhrFields: {withCredentials: true},
			success : function(ret,xhr,status) {
				
				var accountId, index;
				
				if(Top.Controller.get('mailAutoClassifyLogic').modifyYN == 'Y')
					accountId = Top.Dom.selectById('mailAutoClassifyLayout_Table').getClickedData().RULEMAIN[0].ACCOUNT_ID;
				else if(widget != undefined)
					accountId = widget.getValue();
				else
					return;

				
				for(var i=0; i<ret.dto.AccountList.length; i++) {
					if(ret.dto.AccountList[i].ACCOUNT_ID == accountId
							|| ret.dto.AccountList[i].ACCOUNT_ID_RWN == accountId)
						index = i;
				}
				for(var i=0; i<ret.dto.AccountList[index].FolderList.length; i++) {
					
					if(ret.dto.AccountList[index].FolderList[i].FOLDER_TYPE != 'MFL0006')
						folders.push({'text':ret.dto.AccountList[index].FolderList[i].DISPLAY_NAME,'value':ret.dto.AccountList[index].FolderList[i]});
					
				}
				
				folders.sort(function(a,b) {
					return a.value.FOLDER_TYPE.substr(6,1) - b.value.FOLDER_TYPE.substr(6,1);
				});
				
				receiveFolder = folders.splice(0,1);
				importantFolder = folders.splice(0,1);
				sentFolder = folders.splice(0,1);
				tempFolder = folders.splice(0,1);
				sendmeFolder = folders.splice(0,1);
				trashFolders = folders.splice(0,1);
				
				folders.forEach(function(data, index) {
					
					switch(data.value.PARENT_FOLDER_TYPE) {
					
					case 'MFL0001' :
						data.text='└ '+data.text
						receiveFolder.push(folders[index]);
						break;
					case 'MFL0002' :
						data.text='└ '+data.text
						importantFolder.push(folders[index]);
						break;
					case 'MFL0003' :
						data.text='└ '+data.text
						sentFolder.push(folders[index]);
						break;
					case 'MFL0004' :
						data.text='└ '+data.text
						tempFolder.push(folders[index]);
						break;
					case 'MFL0005' :
						data.text='└ '+data.text
						sendmeFolder.push(folders[index]);
						break;
					
					}
					
				});
			
				var newFolders = [];
				newFolders = newFolders.concat(receiveFolder);
				newFolders = newFolders.concat(importantFolder);
				newFolders = newFolders.concat(sentFolder);
				newFolders = newFolders.concat(tempFolder);
				newFolders = newFolders.concat(sendmeFolder);
				newFolders = newFolders.concat(trashFolders);
				
				Top.Dom.selectById('mailAddClassifyPopupLayout_Way_ExistFolder').setProperties({'nodes':newFolders});
				

				if(Top.Controller.get('mailAutoClassifyLogic').modifyYN == 'Y') {
					Top.Controller.get('mailAutoClassifyLogic').modifyYN = 'N';
					Top.Controller.get('mailAddClassifyPopupLogic').setExistData();
				}
			}
		});
		
	}, addAddrList : function(addrList) {
		

		for(var i=0; i<addrList.length; i++) {
			var newAddrLayout = Top.Widget.create('top-linearlayout');
			newAddrLayout.setProperties({'layout-height':'27px', 'layout-width':'392px', 'border-width':'0px', 'orientation':'horizontal'});
			
			var newAddr = Top.Widget.create('top-textview');
			newAddr.setProperties({'text':addrList[i], 'text-size':'12px', 'text-color':'#38434D', 'margin':'5px 0px 0px 8px','class':'mail-cursor-default', 'layout-vertical-alignment':'CENTER'});
			
			var delAddrLayout1 = Top.Widget.create('top-linearlayout');
			delAddrLayout1.setProperties({'layout-width':'match_parent', 'layout-height':'match_parent', 'orientation':'vertical', 'border-width':'0px'});
			
			var delAddrLayout2 = Top.Widget.create('top-linearlayout');
			delAddrLayout2.setProperties({'layout-horizontal-alignment':'RIGHT', 'orientation':'horizontal', 'border-width':'0px', 'layout-height':'wrap_content'});
			
			var delAddr = Top.Widget.create('top-icon');
			delAddr.setProperties({'class':'icon-work_cancel mail-cursor-pointer', 'text-size':'15px', 'text-color':'#8C8E92', 'on-click':'delAddress', 'margin':'3px 15px 0px 0px'});
		
			newAddrLayout.addWidget(newAddr);
			delAddrLayout2.addWidget(delAddr);
			delAddrLayout2.complete();
			delAddrLayout1.addWidget(delAddrLayout2);
			delAddrLayout1.complete();
			newAddrLayout.addWidget(delAddrLayout1);
			newAddrLayout.complete();
			Top.Dom.selectById('mailAddClassifyPopupLayout_Base_Hidden_Addrs').addWidget(newAddrLayout);
			Top.Dom.selectById('mailAddClassifyPopupLayout_Base_Hidden_Addrs').complete();
		}
		
		Top.Dom.selectById('mailAddClassifyPopupLayout_Base_Hidden').setProperties({'visible':'visible'});
		Top.Dom.selectById('mailAddClassifyPopupLayout_Base_Address_Text').setText('')
		
		this.requiredYN();
		
	}

});


Top.Controller.create('mailAddressBookAddLogic', {
	init : function() { 
		
		Top.Dom.selectById('mailAddressBookAddLayout_add_Button').setProperties({'disabled':true, 'on-click':'clickAdd'});
		Top.Dom.selectById('mailAddressBookAddLayout_back_Button').setProperties({'on-click':'clickBack'});
		this.fileAttach = new Array();
		this.file; 
		this.fileList = [];
		this.fileUp = [];
		this.file_id;
		this.photo_src = ""
		this.photo_extesion = ""
		this.selectPhoto = false;
		this.isPhoto;
		this.tempPhoto;
		this.tempEx;
		Top.Controller.get("mailAddressBookAddLogic").isPhoto = false;
		
		Top.Dom.selectById('mailAddressBookAddLayout_addImage_Button').setProperties({'on-click':'choosefile'});
		
		
		Top.Dom.selectById('mailAddressBookAddLayout_phoneNum_TextField1').setProperties({'on-keypress':'isNum'}); 
		Top.Dom.selectById('mailAddressBookAddLayout_phoneNum_TextField2').setProperties({'on-keypress':'isNum'}); 
		Top.Dom.selectById('mailAddressBookAddLayout_phoneNum_TextField3').setProperties({'on-keypress':'isNum'}); 
		Top.Dom.selectById('mailAddressBookAddLayout_name_TextField').setProperties({'on-keyup':'buttonStatusChange'});
		Top.Dom.selectById('mailAddressBookAddLayout_mail_TextField').setProperties({'on-keyup':'buttonStatusChange'});    
		
		if (Top.Controller.get("mailAddressBookMainLogic").changeAddressBook == true){
			this.callAddressBook();
			Top.Dom.selectById("mailAddressBookAddDialog").setProperties({"title":"주소록 수정"});
			Top.Dom.selectById("mailAddressBookAddLayout_add_Button").setText("수정");
		}
		else {
			Top.Dom.selectById("mailAddressBookAddDialog").setProperties({"title":"주소록 추가"}); 
			Top.Dom.selectById("mailAddressBookAddLayout_add_Button").setText("추가");
		}
		
	}, callAddressBook : function(){
		
		var data={
				 "dto": {
				        "CMFLG": "GetAddressBookDetail",
				        "ADDRESSBOOK_ID" : Top.Controller.get("mailAddressBookMainLogic").tempAddressBookId
				    }
				}; 
			
			Top.Ajax.execute({
				
				type : "POST",
				url : _workspace.url + "Mail/Mail?action=AddressBookGetDetail",
				dataType : "json",
				data	 : JSON.stringify(data),
				crossDomain: true,
				contentType: "application/json",
			    xhrFields: {withCredentials: true},
				success : function(ret,xhr,status) {
					
					Top.Dom.selectById("mailAddressBookAddLayout_name_TextField").setText(ret.dto.MIDDLE_NAME);
					
					
					
					Top.Dom.selectById("mailAddressBookAddLayout_phoneNum_TextField1").setText(ret.dto.PhoneNumList[0].PHONE_NUM.split("/")[0]);
					Top.Dom.selectById("mailAddressBookAddLayout_phoneNum_TextField2").setText(ret.dto.PhoneNumList[0].PHONE_NUM.split("/")[1]);
					Top.Dom.selectById("mailAddressBookAddLayout_phoneNum_TextField3").setText(ret.dto.PhoneNumList[0].PHONE_NUM.split("/")[2]);

					Top.Dom.selectById("mailAddressBookAddLayout_mail_TextField").setText(ret.dto.EmailList[0].EMAIL_ADDRESS);
					Top.Dom.selectById('mailAddressBookAddLayout_add_Button').setProperties({'disabled':false});
					Top.Controller.get("mailAddressBookAddLogic").tempPhoto = null;
					Top.Controller.get("mailAddressBookAddLogic").tempEx== null;
					
					if (ret.dto.DATA != null){
						Top.Dom.selectById('mailAddressBookAddLayout_ImageButton').setProperties({'visible':'none'});
           	 			Top.Dom.selectById('mailAddressBookAddLayout_Profile_bg').setProperties({'background-image':'url('+ret.dto.DATA+')'});
	           	 		Top.Controller.get("mailAddressBookAddLogic").tempPhoto = ret.dto.DATA;
	           	 		Top.Controller.get("mailAddressBookAddLogic").tempEx = ret.dto.DATA.split('/')[1].split(';')[0];
						Top.Controller.get("mailAddressBookAddLogic").isPhoto = true;
						
					}
				
				}
			});
	},
	
	
	choosefile : function(event, widget) { 
		
		
		
		let fileChooser = Top.Device.FileChooser.create({
		    onBeforeLoad : function() {
		        let newFileList = [];
				let file = {};
				let tmpId = 0;
		        for(var i=0; i<this.file.length; i++) {		  
		            file = this.file[i];
		            
    		        let loadFile = fileManager.onBeforeLoadFile(file); 
    		        if(!loadFile.result) {
    		            TeeAlarm.open({title: '', content: loadFile.message}); 
    		            return false;
    		        }
    		        
    		        let nomalizedFileFullName = Top.Util.Browser.isIE() ? file.name : file.name.normalize('NFC');        
    		        let fileNameArray = nomalizedFileFullName.split('.');
    		        let fileExt = fileNameArray.length == 1 ? "" : fileNameArray[fileNameArray.length - 1];        
    		        if(fileExt == "") {
    		            TeeAlarm.open({title: '', content: '확장자가 없는 파일은 업로드할 수 없습니다.'}); 
    		            document.getElementById("MainTopApplicationPage_fileChooser").remove();
    		            return false;
    		        }
					
					newFileList.push({
    		            file_id         : tmpId++,
                        file_extension  : fileExt,
                        fileFullName    : nomalizedFileFullName,
                        file_status     : FileStatus.TRY_UPLOAD,
					});
                }
		        return true;
	        },
			onFileChoose : this.onFileSelected,
		    charset: "euc-kr",
		    multiple: true
	    });
		fileChooser.show();

		
		
		
	}, onFileSelected : function(fileChooser) {
		
        var ext = fileChooser[0].file[0].name.slice(fileChooser[0].file[0].name.lastIndexOf(".")+1).toLowerCase();
        
        if (!(ext == "gif" || ext == "jpg" || ext == "png")){
        	Top.Dom.selectById('mailAddressBookAddLayout_Error').setProperties({'visible':'visible'});
        	return;
        }
        else{
        	
        	Top.Dom.selectById('mailAddressBookAddLayout_ImageButton').setProperties({'visible':'none'});
        	Top.Controller.get("mailAddressBookAddLogic").selectPhoto = true;
            Top.Dom.selectById('mailAddressBookAddLayout_Profile_bg').setProperties({'background-image':'url('+fileChooser[0].src+')'});
            Top.Controller.get('mailAddressBookAddLogic').photo_src = fileChooser[0].src;
            Top.Controller.get('mailAddressBookAddLogic').photo_extesion = fileChooser[0].file[0].type.split("/")[1];
            Top.Dom.selectById('mailAddressBookAddLayout_Error').setProperties({'visible':'none'});
        }
        
        
    },
	
	clickAdd : function(event, widget) {
		
		
		
		 var name = Top.Dom.selectById('mailAddressBookAddLayout_name_TextField').getText();
		 var mail = Top.Dom.selectById('mailAddressBookAddLayout_mail_TextField').getText();
		 var phoneNum1 = Top.Dom.selectById('mailAddressBookAddLayout_phoneNum_TextField1').getText();
		 if (phoneNum1 == undefined){
			 phoneNum1 = "";
		 }
		 var phoneNum2 = Top.Dom.selectById('mailAddressBookAddLayout_phoneNum_TextField2').getText();
		 if (phoneNum2 == undefined){
			 phoneNum2 = "";
		 }
		 var phoneNum3 = Top.Dom.selectById('mailAddressBookAddLayout_phoneNum_TextField3').getText();
		 if (phoneNum3 == undefined){
			 phoneNum3 = "";
		 }
		 
		 var phoneNum = phoneNum1 + "/" + phoneNum2 + "/" + phoneNum3
		 
		 var user_id = userManager.getLoginUserId();
		 
		 var strArr = [];
		 var regExp = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i;
		
		 if ( !regExp.test(mail)  ){
			 
			 Top.Dom.selectById("EmailValidPopover").setProperties({"visible": "visible"});
			 
			 Top.Dom.selectById("mailAddressBookAddLayout_mail_Error_Icon").setProperties({"popover-trigger": "hover",
				   "popover-id": "EmailValidPopover",
				   "popover-target": "mailAddressBookAddLayout_mail_Error_Icon",
				   "visible": "visible"});

		 }
		 else{
			 if (Top.Controller.get("mailAddressBookMainLogic").changeAddressBook == false){
				 if(Top.Controller.get("mailAddressBookAddLogic").selectPhoto == true){
					 var data={
							 "dto": {
							        "CMFLG": "InsertAddressBook",
							        "NICK_NAME": "",
							        "MIDDLE_NAME": name,
							        "FIRST_NAME": "",
							        "LAST_NAME": "",
							        "COMPANY": "",
							        "ADDRESS": "",
							        "BIRTHDAY": "",
							        "MEMO": "",
							        "POST_NUM": "",
							        "USER_ID": user_id,
							        "ACCOUNT_ID": mailData.curBox.ACCOUNT_ID,
							        "PROFILE_META": "",
							        "LOCALE": "",
							        "POSITION": "",
							        "DATA" : Top.Controller.get('mailAddressBookAddLogic').photo_src ,
							        "EXTENSION" : Top.Controller.get('mailAddressBookAddLogic').photo_extesion , 
							        "EmailList": [
							            {
							                "EMAIL_ADDRESS": mail
							            }
							        ],
							        "PhoneNumList": [
							            {
							                "PHONE_NUM": phoneNum
							            }
							        ]
							    }
							}; 
				 }
				 else{
					 var data={
							 "dto": {
							        "CMFLG": "InsertAddressBook",
							        "NICK_NAME": "",
							        "MIDDLE_NAME": name,
							        "FIRST_NAME": "",
							        "LAST_NAME": "",
							        "COMPANY": "",
							        "ADDRESS": "",
							        "BIRTHDAY": "",
							        "MEMO": "",
							        "POST_NUM": "",
							        "USER_ID": user_id,
							        "ACCOUNT_ID": mailData.curBox.ACCOUNT_ID,
							        "PROFILE_META": "",
							        "LOCALE": "",
							        "POSITION": "",
							        "DATA" : null ,
							        "EXTENSION" : null , 
							        "EmailList": [
							            {
							                "EMAIL_ADDRESS": mail
							            }
							        ],
							        "PhoneNumList": [
							            {
							                "PHONE_NUM": phoneNum
							            }
							        ]
							    }
							};  
				 }
					Top.Ajax.execute({
						
						type : "POST",
						url : _workspace.url + "Mail/MailAddressBook",
						dataType : "json",
						data	 : JSON.stringify(data),
						crossDomain: true,
						contentType: "application/json",
					    xhrFields: {withCredentials: true},
						success : function(ret,xhr,status) {
							
							if(ret.dto.RESULT_MSG == '성공') {
								Top.Dom.selectById('mailAddressBookAddDialog').close(); 
								Top.Dom.selectById('mailMainLayout_Center_All').src('mailAddressBookMainLayout.html' + verCsp());
							}
							else {
								Top.Dom.selectById("DuplicateEmailPopover").setProperties({"visible": "visible"});

								Top.Dom.selectById("mailAddressBookAddLayout_mail_Error_Icon").setProperties({
									"popover-trigger": "hover",
									"popover-id": "DuplicateEmailPopover",
									"popover-target": "mailAddressBookAddLayout_mail_Error_Icon",
									"visible": "visible"});
							}
						}
					});
			 }
			 else{
				 if (Top.Controller.get("mailAddressBookAddLogic").selectPhoto == true){
					 var data={
							 "dto": {
							        "CMFLG": "UpdateAddressBook",
							        "NICK_NAME": "",
							        "ADDRESSBOOK_ID" : Top.Controller.get("mailAddressBookMainLogic").tempAddressBookId,
							        "MIDDLE_NAME": name,
							        "FIRST_NAME": "",
							        "LAST_NAME": "",
							        "COMPANY": "",
							        "ADDRESS": "",
							        "BIRTHDAY": "",
							        "MEMO": "",
							        "POST_NUM": "",
							        "USER_ID": user_id,
							        "ACCOUNT_ID": mailData.curBox.ACCOUNT_ID,
							        "PROFILE_META": "",
							        "LOCALE": "",
							        "POSITION": "",
							        "DATA" : Top.Controller.get('mailAddressBookAddLogic').photo_src ,
							        "EXTENSION" : Top.Controller.get('mailAddressBookAddLogic').photo_extesion ,  
							        "EmailList": [
							            {
							                "EMAIL_ADDRESS": mail
							            }
							        ],
							        "PhoneNumList": [
							            {
							                "PHONE_NUM": phoneNum
							            }
							        ]
							    }
							}; 
				 }
				 
				
				 
				 else{
					 var data={
							 "dto": {
							        "CMFLG": "UpdateAddressBook",
							        "NICK_NAME": "",
							        "ADDRESSBOOK_ID" : Top.Controller.get("mailAddressBookMainLogic").tempAddressBookId,
							        "MIDDLE_NAME": name,
							        "FIRST_NAME": "",
							        "LAST_NAME": "",
							        "COMPANY": "",
							        "ADDRESS": "",
							        "BIRTHDAY": "",
							        "MEMO": "",
							        "POST_NUM": "",
							        "USER_ID": user_id,
							        "ACCOUNT_ID": mailData.curBox.ACCOUNT_ID,
							        "PROFILE_META": "",
							        "LOCALE": "",
							        "POSITION": "",
							        "DATA" : Top.Controller.get("mailAddressBookAddLogic").tempPhoto ,
							        "EXTENSION" : Top.Controller.get("mailAddressBookAddLogic").tempEx ,  
							        "EmailList": [
							            {
							                "EMAIL_ADDRESS": mail
							            }
							        ],
							        "PhoneNumList": [
							            {
							                "PHONE_NUM": phoneNum
							            }
							        ]
							    }
							};  
				 }
					Top.Ajax.execute({
						
						type : "POST",
						url : _workspace.url + "Mail/Mail?action=AddressBookUpdate",
						//"Mail/Mail?action=CreateAddressBook",
						dataType : "json",
						data	 : JSON.stringify(data),
						crossDomain: true,
						contentType: "application/json",
					    xhrFields: {withCredentials: true},
						success : function(ret,xhr,status) {
							
							if(ret.dto.RESULT_MSG == '성공') {
								Top.Dom.selectById('mailAddressBookAddDialog').close(); 
								Top.Dom.selectById('mailMainLayout_Center_All').src('mailAddressBookMainLayout.html' + verCsp());
							}
							else {
								Top.Dom.selectById("DuplicateEmailPopover").setProperties({"visible": "visible"});

								Top.Dom.selectById("mailAddressBookAddLayout_mail_Error_Icon").setProperties({
									"popover-trigger": "hover",
									"popover-id": "DuplicateEmailPopover",
									"popover-target": "mailAddressBookAddLayout_mail_Error_Icon",
									"visible": "visible"});
							}
						}
					});
			 }
			
		 }
	 }, clickBack : function(event, widget){
		Top.Dom.selectById('mailAddressBookAddDialog').close();
		
	 }, buttonAbled : function(event, widget) { 						//	버튼 활성화
			
		Top.Dom.selectById('mailAddressBookAddLayout_add_Button').setProperties({'disabled':false});
				
	} , buttonDisabled : function(event, widget) { 						//	버튼 비 활성화
		
		Top.Dom.selectById('mailAddressBookAddLayout_add_Button').setProperties({'disabled':true});
				
	}  , buttonStatusChange : function(event, widget) {
		
		if	(Top.Dom.selectById('mailAddressBookAddLayout_name_TextField').getText() == undefined || 
				Top.Dom.selectById('mailAddressBookAddLayout_name_TextField').getText() == null || 
				Top.Dom.selectById('mailAddressBookAddLayout_name_TextField').getText() == ""  ||
				Top.Dom.selectById('mailAddressBookAddLayout_mail_TextField').getText() == "" ||
				Top.Dom.selectById('mailAddressBookAddLayout_mail_TextField').getText() == null ||
				Top.Dom.selectById('mailAddressBookAddLayout_mail_TextField').getText() == undefined			
			) 
		{
			
			this.buttonDisabled();
			
			//return;
			
		} else  {
			
			
			this.buttonAbled();
			//return;
		}
		/*
		if(Top.Dom.selectById('mailAddressBookMainLayout_Table').getCheckedIndex().length == 0)
			this.buttonDisabled();
		else 
			this.buttonAbled();
		*/
		
		if(widget.id == "mailAddressBookAddLayout_mail_TextField"){
			 var regExp = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i;
			 if (Top.Dom.selectById("mailAddressBookAddLayout_mail_Error_Icon").getProperties("visible") == "visible"){
				 if ( regExp.test(Top.Dom.selectById('mailAddressBookAddLayout_mail_TextField').getText()) ){
					 Top.Dom.selectById("mailAddressBookAddLayout_mail_Error_Icon").setProperties({"visible":"none"});
					  
				 }
				 
			 }
		}
		
		
		
		
	}, isNum : function(event, data){ 
		if (event.keyCode<48 || event.keyCode> 57){
			event.returnValue = false;
			Top.Dom.selectById('mailAddressBookAddLayout_phoneNum_Error_Icon').setVisible('visible');
			Top.Dom.selectById('mailAddressBookAddLayout_phoneNum_Error_Icon').setProperties({'popover-id':'MobileValidPopover',
				'placement':'top'})

		}else{
			Top.Dom.selectById('mailAddressBookAddLayout_phoneNum_Error_Icon').setVisible('none');
		}
	}
	
	

	
	
})
	 
Top.Controller.create('mailAddressBookPopupLogic', {
	init : function() { 
		
		Top.Dom.selectById('mailAddressBookPopupLayout_SelectBox').setProperties({'selected-text':'이름'});
		
		this.getMailAddressBookDataList();
		
		
		Top.Dom.selectById('mailAddressBookPopupLayout_add_Button').setProperties({'on-click':'addSelectUser'}); 
		Top.Dom.selectById('mailAddressBookPopupLayout_back_Button').setProperties({'on-click':'clickBack'});  
		
		
		

		Top.Dom.selectById('mailAddressBookPopupLayout_TableView').setProperties({'on-rowcheck':'buttonStatusChange'});
		this.buttonDisabled(); 
		
		//
		Top.Dom.selectById('mailAddressBookPopupLayout_selectTextField').setProperties({'on-keypress':'getEnter'});
		
	},getMailAddressBookDataList : function() {
			
		
		
		var data={
			dto: {
				"CMFLG"				: "GetAddressBookList",
				"USER_ID" : userManager.getLoginUserId()
			}
		}; 
		
				
		Top.Ajax.execute({
		
			type : "POST",
			url : _workspace.url + "Mail/Mail?action=AddressListGet",
			dataType : "json",
			data	 : JSON.stringify(data),
			crossDomain: true,
			contentType: "application/json",
		    xhrFields: {withCredentials: true},
			success : function(ret,xhr,status) {
				
				
				var temp = []
				for (var i = 0 ; i < ret.dto.dbio_total_count; i++){
					if (ret.dto.info_address[i].PHONE_INFO.length){
						temp.push({
										"dbio_total_count" : ret.dto.dbio_total_count,
										"ADDRESSBOOK_ID" : ret.dto.info_address[i].ADDRESSBOOK_ID, 
										"NAME" : ret.dto.info_address[i].NAME, 
										"EMAIL_ADDRESS" : ret.dto.info_address[i].EMAIL_INFO[0].EMAIL_ADDRESS, 
										"PHONE_NUM" : ret.dto.info_address[i].PHONE_INFO[0].PHONE_NUM
						
									});
					};
				};
				
				mailAddressBookDataRepo.setValue('mailAddressBookListData',temp);
			}
		});
		
		
		
	}, searchByName : function(){
		
		var data={
				dto: {
					"CMFLG"		: "GetAddressBookList",
					"USER_ID" 	: userManager.getLoginUserId(),
					"SUBJECT"  	: Top.Dom.selectById("mailAddressBookPopupLayout_selectTextField").getText()
				}
			}; 
			
					
		Top.Ajax.execute({
		
			type : "POST",
			url : _workspace.url + "Mail/Mail?action=AddressListGet",
			dataType : "json",
			data	 : JSON.stringify(data),
			crossDomain: true,
			contentType: "application/json",
		    xhrFields: {withCredentials: true},
			success : function(ret,xhr,status) {
				
				
				var temp = []
				for (var i = 0 ; i < ret.dto.dbio_total_count; i++){
					if (ret.dto.info_address[i].PHONE_INFO.length){
						temp.push({
										"dbio_total_count" : ret.dto.dbio_total_count,
										"ADDRESSBOOK_ID" : ret.dto.info_address[i].ADDRESSBOOK_ID, 
										"NAME" : ret.dto.info_address[i].NAME, 
										"EMAIL_ADDRESS" : ret.dto.info_address[i].EMAIL_INFO[0].EMAIL_ADDRESS, 
										"PHONE_NUM" : ret.dto.info_address[i].PHONE_INFO[0].PHONE_NUM
						
									});
					};
				};
				
				mailAddressBookDataRepo.setValue('mailAddressBookListData',temp);
			}
		});
	},getEnter : function(event) {
		
		if(event.key == "Enter") {
			this.searchByName();
		}
		
	},
	
	
	addSelectUser : function(event, widget){
		
		if(Top.Dom.selectById('mailAddressBookPopupLayout_TableView').getCheckedData().length == 0){
			
			Top.Controller.get("mailThreePanelWriteLogic").addRec = false;
			Top.Controller.get("mailThreePanelWriteLogic").addCc = false;
			Top.Controller.get("mailThreePanelWriteLogic").addBcc = false;
			Top.Dom.selectById("mailAddressBookDialog").close();
		}
		
		if(Top.Dom.selectById('mailAddClassifyDialog').isOpen()) {
			var address = Top.Dom.selectById('mailAddressBookPopupLayout_TableView').getCheckedData()[0].EMAIL_ADDRESS;
			for(var i=1; i<Top.Dom.selectById('mailAddressBookPopupLayout_TableView').getCheckedData().length; i++) {
				address += ', ' + Top.Dom.selectById('mailAddressBookPopupLayout_TableView').getCheckedData()[i].EMAIL_ADDRESS;
			}
			Top.Dom.selectById('mailAddClassifyPopupLayout_Base_Address_Text').setText(address);
			Top.Dom.selectById("mailAddressBookDialog").close();
			return;		
				
		}
		
		if (Top.Controller.get("mailWritePopupLogic").addRec == true){
			for(var i=0; i<Top.Dom.selectById('mailAddressBookPopupLayout_TableView').getCheckedData().length; i++) {
				
				Top.Controller.get('mailWritePopupLogic').receiverAddr.push( {	"text" : Top.Dom.selectById('mailAddressBookPopupLayout_TableView').getCheckedData()[i].NAME
					+ '<' + Top.Dom.selectById('mailAddressBookPopupLayout_TableView').getCheckedData()[i].EMAIL_ADDRESS + '>',
								"USER_EMAIL" : Top.Dom.selectById('mailAddressBookPopupLayout_TableView').getCheckedData()[i].EMAIL_ADDRESS});
				Top.Dom.selectById('Text_ReceiverAddress_Popup').setProperties({'chip-items' : Top.Controller.get('mailWritePopupLogic').receiverAddr});
			}
			Top.Controller.get("mailWritePopupLogic").adjustDialog();
		}
		else if (Top.Controller.get("mailWritePopupLogic").addCc == true){
			for(var i=0; i<Top.Dom.selectById('mailAddressBookPopupLayout_TableView').getCheckedData().length; i++) {
				
				Top.Controller.get('mailWritePopupLogic').CCAddr.push( {	"text" : Top.Dom.selectById('mailAddressBookPopupLayout_TableView').getCheckedData()[i].NAME
					+ '<' + Top.Dom.selectById('mailAddressBookPopupLayout_TableView').getCheckedData()[i].EMAIL_ADDRESS + '>',
								"USER_EMAIL" : Top.Dom.selectById('mailAddressBookPopupLayout_TableView').getCheckedData()[i].EMAIL_ADDRESS});
				Top.Dom.selectById('Text_Cc_Popup').setProperties({'chip-items' : Top.Controller.get('mailWritePopupLogic').CCAddr});
			}
			Top.Controller.get("mailWritePopupLogic").adjustDialog();
		}
		else if (Top.Controller.get("mailWritePopupLogic").addBcc == true){
			for(var i=0; i<Top.Dom.selectById('mailAddressBookPopupLayout_TableView').getCheckedData().length; i++) {
				
				Top.Controller.get('mailWritePopupLogic').BCCAddr.push( {	"text" : Top.Dom.selectById('mailAddressBookPopupLayout_TableView').getCheckedData()[i].NAME
					+ '<' + Top.Dom.selectById('mailAddressBookPopupLayout_TableView').getCheckedData()[i].EMAIL_ADDRESS + '>',
								"USER_EMAIL" : Top.Dom.selectById('mailAddressBookPopupLayout_TableView').getCheckedData()[i].EMAIL_ADDRESS});
				Top.Dom.selectById('Text_HiddenCc_Popup').setProperties({'chip-items' : Top.Controller.get('mailWritePopupLogic').BCCAddr});
			}
			Top.Controller.get("mailWritePopupLogic").adjustDialog();
		} else if (Top.Controller.get("mailWriteLogic").addRec == true){
			for(var i=0; i<Top.Dom.selectById('mailAddressBookPopupLayout_TableView').getCheckedData().length; i++) {
				
				Top.Controller.get('mailWriteLogic').receiverAddr.push( {	"text" : Top.Dom.selectById('mailAddressBookPopupLayout_TableView').getCheckedData()[i].NAME
					+ '<' + Top.Dom.selectById('mailAddressBookPopupLayout_TableView').getCheckedData()[i].EMAIL_ADDRESS + '>',
								"USER_EMAIL" : Top.Dom.selectById('mailAddressBookPopupLayout_TableView').getCheckedData()[i].EMAIL_ADDRESS});
				Top.Dom.selectById('Text_ReceiverAddress').setProperties({'chip-items' : Top.Controller.get('mailWriteLogic').receiverAddr});
			}
		}
		else if (Top.Controller.get("mailWriteLogic").addCc == true){
			for(var i=0; i<Top.Dom.selectById('mailAddressBookPopupLayout_TableView').getCheckedData().length; i++) {
				
				Top.Controller.get('mailWriteLogic').CCAddr.push( {	"text" : Top.Dom.selectById('mailAddressBookPopupLayout_TableView').getCheckedData()[i].NAME
					+ '<' + Top.Dom.selectById('mailAddressBookPopupLayout_TableView').getCheckedData()[i].EMAIL_ADDRESS + '>',
								"USER_EMAIL" : Top.Dom.selectById('mailAddressBookPopupLayout_TableView').getCheckedData()[i].EMAIL_ADDRESS});
				Top.Dom.selectById('Text_Cc').setProperties({'chip-items' : Top.Controller.get('mailWriteLogic').CCAddr});
			}
			
		}
		else if (Top.Controller.get("mailWriteLogic").addBcc == true){
			for(var i=0; i<Top.Dom.selectById('mailAddressBookPopupLayout_TableView').getCheckedData().length; i++) {
				
				Top.Controller.get('mailWriteLogic').BCCAddr.push( {	"text" : Top.Dom.selectById('mailAddressBookPopupLayout_TableView').getCheckedData()[i].NAME
					+ '<' + Top.Dom.selectById('mailAddressBookPopupLayout_TableView').getCheckedData()[i].EMAIL_ADDRESS + '>',
								"USER_EMAIL" : Top.Dom.selectById('mailAddressBookPopupLayout_TableView').getCheckedData()[i].EMAIL_ADDRESS});
				Top.Dom.selectById('Text_HiddenCc').setProperties({'chip-items' : Top.Controller.get('mailWriteLogic').BCCAddr});
			}
			
		} 
		
		if(Top.Controller.get("mailWriteLogic") != undefined) {
		Top.Controller.get("mailWriteLogic").addRec = false;
		Top.Controller.get("mailWriteLogic").addCc = false;
		Top.Controller.get("mailWriteLogic").addBcc = false; }
		Top.Controller.get("mailWritePopupLogic").addRec = false;
		Top.Controller.get("mailWritePopupLogic").addCc = false;
		Top.Controller.get("mailWritePopupLogic").addBcc = false; 
		
		Top.Dom.selectById("mailAddressBookDialog").close();
		
		
	}, buttonAbled : function(event, widget) { 						//	버튼 활성화
		
		Top.Dom.selectById('mailAddressBookPopupLayout_add_Button').setProperties({'disabled':false});
				
	} , buttonDisabled : function(event, widget) { 						//	버튼 비 활성화
		
		Top.Dom.selectById('mailAddressBookPopupLayout_add_Button').setProperties({'disabled':true});
				
	}  , buttonStatusChange : function(event, widget) {
		
		
		
		if(Top.Dom.selectById('mailAddressBookPopupLayout_TableView').getCheckedIndex().length == 0)
			this.buttonDisabled();
		else 
			this.buttonAbled();
		
	} ,clickBack : function(event, widget){
	
		Top.Dom.selectById('mailAddressBookDialog').close();
	
		
	 }





});

Top.Controller.create('mailAllReadPopupLogic', {
	
	init : function() {
		
		Top.Dom.selectById('mailAllReadPopupLayout_FolderName1').setText(mailData.selectedBox.DISPLAY_NAME);
		
		Top.Dom.selectById('mailAllReadPopupLayout_OK').setProperties({'on-click':'changeAllRead'});
		Top.Dom.selectById('mailAllReadPopupLayout_Cancel').setProperties({'on-click':function(){Top.Dom.selectById('mailAllReadDialog').close();}});
		
	}, changeAllRead : function() {
		
		
		var labelType = "MARK_ALL_READ";
		
		if(mailData.selectedBox.FOLDER_TYPE == 'MFL0002')
			labelType = 'MARK_ALL_READ_IMPORTANT';
		
		var data={
				dto: {
					"CMFLG"			: "ChangeMailStatusByType",
					"ACCOUNT_ID"	: mailData.selectedBox.ACCOUNT_ID,
					"LABEL_TYPE"	: labelType,
					"LABEL_VALUE"	: "",
					"FOLDER_ID"		: mailData.selectedBox.FOLDER_ID,
						}
		};

		Top.Ajax.execute({
		
			type : "POST",
			url : _workspace.url+"Mail/Mail?action=ChangeStatusByType",
			dataType : "json",
			data	 : JSON.stringify(data),
			contentType: "application/json",
			xhrFields: {withCredentials: true},
			crossDomain : true,
			success : function(ret,xhr,status) {
			
				Top.Dom.selectById('mailAllReadDialog').close();
				Top.Controller.get('mailMainLeftLogic').resetCount();
				Top.Controller.get('mailMainLeftLogic').refreshCurrentList('selected');
				
			},
			complete : function(ret,xhr,status) {
				
			},
			error : function(ret, xhr,status) {
				
			}
			
			
			
		});
		
		
	}

});

Top.Controller.create('mailCheckExternalPopupLogic', {
	
	init : function() {
		
		this.arrangeData();
		Top.Dom.selectById('mailCheckExternalPopupLayout_Close').setProperties({'on-click':'closeDialog'});
		Top.Dom.selectById('mailCheckExternalPopupLayout_Text_Setting').setProperties({'on-click':'moveSetting'});
	
	}, arrangeData : function() {
		
		if(mailDataRepo.mailExternalDataList.length != 0 && mailDataRepo.mailExternalDataList[0].USER_ID != userManager.getLoginUserId()) {
			mailDataRepo.mailExternalDataList = [];
		}
		
	}, moveSetting : function() {
		let sub = appManager.getSubApp();
		if(sub) Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/setting' + "?sub=" + sub, {eventType:'fold'});
		else Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/setting?' + new Date().getTime(), {eventType:'close'});
		
		Top.Dom.selectById('mailCheckExternalDialog').close(true);
		
	}, getList : function() {
		
		for(var i=0; i<mailDataRepo.mailExternalDataList.length; i++) {
 			if(Top.Controller.get('mailMainLeftLogic').extMail == mailDataRepo.mailExternalDataList[i].ACCOUNT_ID && mailDataRepo.mailExternalDataList[i].COUNT != '0')
 				return;
 		}
		
		var service;
		
		if(Top.Controller.get('mailMainLeftLogic').extProtocol == 'POP3') {
			var data={
					dto: {
						"CMFLG"		: "Pop3Request",
						"USER_ID"	: userManager.getLoginUserId(),
						"ACCOUNT_ID": Top.Controller.get('mailMainLeftLogic').extMail,
						"REQUEST_TYPE":"refresh"
					}
			};
			service = 'POP3Request';
		} else if(Top.Controller.get('mailMainLeftLogic').extProtocol == 'IMAP') {
			var data={
					dto: {
						"USER_ID"	: userManager.getLoginUserId(),
						"ACCOUNT_ID": Top.Controller.get('mailMainLeftLogic').extMail
					}
			};
			service = 'ImapRequest';
		}
		
		Top.Ajax.execute({
			
			type : "POST",
			url : _workspace.url + "Mail/Mail?action=" + service,
			data	 : JSON.stringify(data),
			crossDomain: true,
			contentType: "application/json",
		    xhrFields: {withCredentials: true},
			success : function(ret,xhr,status) {

			}
		});
		
	}, closeDialog : function() {
		
		Top.Dom.selectById('mailCheckExternalDialog').close();
	}

});


Top.Controller.create('TeeSpaceLogic', {
	addNewFolder : function(event, widget) {
		
		for(var i=1; i<Top.Dom.selectById('mailMainLayout_Title_Text').getProperties('value'); i++) {
			if(Top.Dom.selectById('mailMainLayout_MailBox_NewFolder'+i).getProperties('visible') == 'visible')
				return;
			else if(Top.Dom.selectById('mailMainLayout_SentMailBox_NewFolder'+i).getProperties('visible') == 'visible')
				return;
			else if(Top.Dom.selectById('mailMainLayout_SentMeMailBox_NewFolder'+i).getProperties('value') == 'visible')
				return;
		}
		if(Top.Dom.selectById('mailMainLayout_ModifyFolder') != null)
			return;
		
		if(mailData.selectedBox.FOLDER_TYPE == 'MFL0001') {
			
			var i = mailData.selectedBox.WIDGET_ID.substring(22, mailData.selectedBox.WIDGET_ID.length);
			Top.Dom.selectById('mailMainLayout_MailBox_NewFolder'+i).setProperties({'visible':'visible'});
			Top.Dom.selectById('mailMainLayout_MailBox_NewFolder_Text'+i).focus();
			Top.Dom.selectById('mailMainLayout_MailBox_NewFolder_Text'+i).template.children[0].children[0].children[0].select();
			Top.Dom.selectById('mailMainLayout_MailBox_NewFolder_Text'+i).setProperties({'on-blur':'saveFolder','on-keydown':'getEnter','on-keyup':'getHintNew'});
			
		} else if(mailData.selectedBox.FOLDER_TYPE == 'MFL0003') {
			
			var i = mailData.selectedBox.WIDGET_ID.substring(26, mailData.selectedBox.WIDGET_ID.length);
			Top.Dom.selectById('mailMainLayout_SentMailBox_NewFolder'+i).setProperties({'visible':'visible'});
			Top.Dom.selectById('mailMainLayout_SentMailBox_NewFolder_Text'+i).focus();
			Top.Dom.selectById('mailMainLayout_SentMailBox_NewFolder_Text'+i).template.children[0].children[0].children[0].select();
			Top.Dom.selectById('mailMainLayout_SentMailBox_NewFolder_Text'+i).setProperties({'on-blur':'saveFolder','on-keydown':'getEnter','on-keyup':'getHintNew'});
			
		} else if(mailData.selectedBox.FOLDER_TYPE == 'MFL0005') {
			
			var i = mailData.selectedBox.WIDGET_ID.substring(28, mailData.selectedBox.WIDGET_ID.length);
			Top.Dom.selectById('mailMainLayout_SentMeMailBox_NewFolder'+i).setProperties({'visible':'visible'});
			Top.Dom.selectById('mailMainLayout_SentMeMailBox_NewFolder_Text'+i).focus();
			Top.Dom.selectById('mailMainLayout_SentMeMailBox_NewFolder_Text'+i).template.children[0].children[0].children[0].select();
			Top.Dom.selectById('mailMainLayout_SentMeMailBox_NewFolder_Text'+i).setProperties({'on-blur':'saveFolder','on-keydown':'getEnter','on-keyup':'getHintNew'});
			
		}
		
			
	}, getHintNew : function(event, widget) {

		if(Top.version.substr(8,2) < 88)
			this.manageByte(event, widget);
		
		var inputWidget = 'input#' + widget.id;
		
		if(widget.getText() == '') {
			
			let hint = $(".mail-folder-hint");
	        if (!hint.length) {
	            let hintElement = $("<div />", {
	                class: "mail-folder-hint",
	                contenteditable: "false"
	            }).text('편지함 이름');

	            $(inputWidget).after(hintElement);
	        }

	        hint.show();
	        
		} else {
						
	        $(".mail-folder-hint").hide();
		}
        
	}, getEnter : function(event, widget) {

		if(Top.version.substr(8,2) > 87)
			this.manageByte(event, widget);
		 
        if(event.key === "Enter") {
			event.target.blur();
		}
		
	}, saveFolder : function(event, widget) {
		
		if(Top.Dom.selectById('mailErrMsgDialog').isOpen())
			Top.Dom.selectById('mailErrMsgDialog').close();
		
		var folderId, displayName, errorId, textId, i;
		var folderName = Top.Controller.get('mailMainLeftLogic').folderName;
		
		if(mailData.selectedBox.FOLDER_TYPE == 'MFL0001') {
			
			i = mailData.selectedBox.WIDGET_ID.substring(22, mailData.selectedBox.WIDGET_ID.length);
			folderId = Top.Dom.selectById('mailMainLayout_MailBox_Text'+i).getProperties('value').FOLDER_ID;
			displayName = Top.Dom.selectById('mailMainLayout_MailBox_NewFolder_Text'+i).getText();
			errorId = 'mailMainLayout_MailBox_NewFolder_Error'+i
			errorId2 = '#' + errorId;
			textId = 'mailMainLayout_MailBox_NewFolder_Text'+i
			if(displayName.indexOf('/') != -1) {
				this.error = 'slash';
				Top.Dom.selectById(errorId).setProperties({'visible':'visible'});
				Top.Dom.selectById('mailErrMsgDialog').setProperties({'layout-left':$(errorId2).offset().left+'px', 'layout-top': $(errorId2).offset().top-40+'px'});
				Top.Dom.selectById('mailErrMsgDialog').open();
				Top.Dom.selectById(textId).focus();
				return;	
			} else if(displayName == '') {
				this.error = 'empty';
				Top.Dom.selectById(errorId).setProperties({'visible':'visible'});
				Top.Dom.selectById('mailErrMsgDialog').setProperties({'layout-left':$(errorId2).offset().left+'px', 'layout-top': $(errorId2).offset().top-40+'px'});
				Top.Dom.selectById('mailErrMsgDialog').open();
				Top.Dom.selectById(textId).focus();
				return;	
			} else if(folderName[0].length != 0) {
				for(var j=0; j<folderName[0].length; j++) {
					if(displayName == folderName[0][j]) {
						this.error = 'dup';
						Top.Dom.selectById(errorId).setProperties({'visible':'visible'});
						Top.Dom.selectById('mailErrMsgDialog').setProperties({'layout-left':$(errorId2).offset().left+'px', 'layout-top': $(errorId2).offset().top-40+'px'});
						Top.Dom.selectById('mailErrMsgDialog').open();
						Top.Dom.selectById(textId).focus();
						return;	
					}
				}
				
			}
			
		} else if(mailData.selectedBox.FOLDER_TYPE == 'MFL0003') {
			
			i = mailData.selectedBox.WIDGET_ID.substring(26, mailData.selectedBox.WIDGET_ID.length);
			folderId = Top.Dom.selectById('mailMainLayout_SentMailBox_Text'+i).getProperties('value').FOLDER_ID;
			displayName = Top.Dom.selectById('mailMainLayout_SentMailBox_NewFolder_Text'+i).getText();
			errorId = 'mailMainLayout_SentMailBox_NewFolder_Error'+i;
			errorId2 = '#' + errorId;
			textId = 'mailMainLayout_SentMailBox_NewFolder_Text'+i;
			if(displayName.indexOf('/') != -1) {
				this.error = 'slash';
				Top.Dom.selectById(errorId).setProperties({'visible':'visible'});
				Top.Dom.selectById('mailErrMsgDialog').setProperties({'layout-left':$(errorId2).offset().left+'px', 'layout-top': $(errorId2).offset().top-40+'px'});
				Top.Dom.selectById('mailErrMsgDialog').open();
				Top.Dom.selectById(textId).focus();
				return;	
			} else if(displayName == '') {
				this.error = 'empty';
				Top.Dom.selectById(errorId).setProperties({'visible':'visible'});
				Top.Dom.selectById('mailErrMsgDialog').setProperties({'layout-left':$(errorId2).offset().left+'px', 'layout-top': $(errorId2).offset().top-40+'px'});
				Top.Dom.selectById('mailErrMsgDialog').open();
				Top.Dom.selectById(textId).focus();
				return;	
			} else if(folderName[1].length != 0) {
				for(var j=0; j<folderName[1].length; j++) {
					if(displayName == folderName[1][j]) {
						this.error = 'dup';
						Top.Dom.selectById(errorId).setProperties({'visible':'visible'});
						Top.Dom.selectById('mailErrMsgDialog').setProperties({'layout-left':$(errorId2).offset().left+'px', 'layout-top': $(errorId2).offset().top-40+'px'});
						Top.Dom.selectById('mailErrMsgDialog').open();
						Top.Dom.selectById(textId).focus();
						return;	
					}
				}
				
			}
			
		} else if(mailData.selectedBox.FOLDER_TYPE == 'MFL0005') {
			
			i = mailData.selectedBox.WIDGET_ID.substring(28, mailData.selectedBox.WIDGET_ID.length);
			folderId = Top.Dom.selectById('mailMainLayout_SentMeMailBox_Text'+i).getProperties('value').FOLDER_ID;
			displayName = Top.Dom.selectById('mailMainLayout_SentMeMailBox_NewFolder_Text'+i).getText();
			errorId = 'mailMainLayout_SentMeMailBox_NewFolder_Error'+i;
			errorId2 = '#' + errorId;
			textId = 'mailMainLayout_SentMeMailBox_NewFolder_Text'+i;
			if(displayName.indexOf('/') != -1) {
				this.error = 'slash';
				Top.Dom.selectById(errorId).setProperties({'visible':'visible'});
				Top.Dom.selectById('mailErrMsgDialog').setProperties({'layout-left':$(errorId2).offset().left+'px', 'layout-top': $(errorId2).offset().top-40+'px'});
				Top.Dom.selectById('mailErrMsgDialog').open();
				Top.Dom.selectById(textId).focus();
				return;	
			} else if(displayName == '') {
				this.error = 'empty';
				Top.Dom.selectById(errorId).setProperties({'visible':'visible'});
				Top.Dom.selectById('mailErrMsgDialog').setProperties({'layout-left':$(errorId2).offset().left+'px', 'layout-top': $(errorId2).offset().top-40+'px'});
				Top.Dom.selectById('mailErrMsgDialog').open();
				Top.Dom.selectById(textId).focus();
				return;	
			} else if(folderName[2].length != 0) {
				for(var j=0; j<folderName[2].length; j++) {
					if(displayName == folderName[2][j]) {
						this.error = 'dup';
						Top.Dom.selectById(errorId).setProperties({'visible':'visible'});
						Top.Dom.selectById('mailErrMsgDialog').setProperties({'layout-left':$(errorId2).offset().left+'px', 'layout-top': $(errorId2).offset().top-40+'px'});
						Top.Dom.selectById('mailErrMsgDialog').open();
						Top.Dom.selectById(textId).focus();
						return;	
					}
				}
				
			}
		}
		
		
		var data={
				dto: {
						"ACCOUNT_ID"	: Top.Dom.selectById('mailMainLayout_Mail_Text'+i).getProperties('value'),
						"FOLDER_ID"		: folderId,
						"DISPLAY_NAME"	: displayName,
						"CMFLG"			: "CreateSubfolder"
				
				}
		};

		Top.Ajax.execute({
			
			type : "POST",
			url : _workspace.url + "Mail/Mail?action=SubfolderCreate",
			dataType : "json",
			data	 : JSON.stringify(data),
			contentType: "application/json",
		    xhrFields: {withCredentials: true},
			crossDomain : true,
			success : function(ret,xhr,status) {
			
				Top.Controller.get('mailMainLeftLogic').isFolderSaved = true;
				Top.Controller.get('mailMainLeftLogic').setFolderOnly();
			},
			complete : function(ret,xhr,status) {
				
			},
			error : function(ret, xhr,status) {
				
			}
			
		});
		
		
	}, modifyFolderName : function(event, widget) {
	
		for(var i=1; i<Top.Dom.selectById('mailMainLayout_Title_Text').getProperties('value'); i++) {
			if(Top.Dom.selectById('mailMainLayout_MailBox_NewFolder'+i).getProperties('visible') == 'visible')
				return;
			else if(Top.Dom.selectById('mailMainLayout_SentMailBox_NewFolder'+i).getProperties('visible') == 'visible')
				return;
			else if(Top.Dom.selectById('mailMainLayout_SentMeMailBox_NewFolder'+i).getProperties('value') == 'visible')
				return;
		}
		
		if(Top.Dom.selectById('mailMainLayout_ModifyFolder') != null)
			return;
		
		var folder = Top.Widget.create('top-linearlayout');
		folder.setProperties({'id':'mailMainLayout_ModifyFolder','layout-width':'match_parent','layout-height':'35px','orienataion':'horizontal','border-width':'0px'});
		
		var folderTextField = Top.Widget.create('top-textfield');
		folderTextField.setProperties({'id':'mailMainLayout_ModifyFolderName','layout-width':'130px','layout-height':'27px','margin':'5px 0px 0px 42px','border-color':'#00A4C3','text-color':'#333333',
			'text-size':'13px', 'max-length':'250'});
		
		var errorIcon = Top.Widget.create('top-icon');
		errorIcon.setProperties({'id':'mailMainLayout_ModifyFolderError','class':'icon-chart_error','visible':'none','margin':'0px 0px 0px 6px','text-color':'#FF6060','text-size':'16px','layout-vertical-alignment':'CENTER'});
		
		folder.addWidget(folderTextField);
		folder.addWidget(errorIcon);
		folder.complete();
		
		Top.Dom.selectById(mailData.selectedBox.WIDGET_ID).clear();
		Top.Dom.selectById(mailData.selectedBox.WIDGET_ID).addWidget(folder);
		Top.Dom.selectById(mailData.selectedBox.WIDGET_ID).complete();
		
		folderTextField.setText(mailData.selectedBox.DISPLAY_NAME);
		folderTextField.focus();
		folderTextField.setProperties({'on-blur':'modifyFolder','on-keydown':'getEnter','on-keyup':'getHint'});
		mailMainLayout_ModifyFolderName[1].select();
		
	}, getHint : function(event, widget) {

		if(Top.version.substr(8,2) < 88)
			this.manageByte(event, widget);
		
		if(Top.Dom.selectById('mailMainLayout_ModifyFolderName').getText() == '') {
			
			let hint = $(".mail-folder-hint");
	        if (!hint.length) {
	            let hintElement = $("<div />", {
	                class: "mail-folder-hint",
	                contenteditable: "false"
	            }).text(mailData.selectedBox.DISPLAY_NAME);

	            $("input#mailMainLayout_ModifyFolderName").after(hintElement);
	        }

	        hint.show();
	        
		} else {
						
	        $(".mail-folder-hint").hide();
		}
        
	}, modifyFolder : function(event, widget) {
		
		if(Top.Dom.selectById('mailErrMsgDialog').isOpen())
			Top.Dom.selectById('mailErrMsgDialog').close();
		
		var curName = mailData.selectedBox.DISPLAY_NAME;
		var folderName = Top.Controller.get('mailMainLeftLogic').folderName;
		var displayName = Top.Dom.selectById('mailMainLayout_ModifyFolderName').getText();
		var parentFolder = mailData.selectedBox.PARENT_FOLDER_TYPE;
		
		if(displayName == curName) {
			Top.Controller.get('mailMainLeftLogic').setFolderOnly();
			return;
		} else if(displayName.indexOf('/') != -1) {
			this.error = 'slash';
			Top.Dom.selectById('mailMainLayout_ModifyFolderError').setProperties({'visible':'visible'});
			Top.Dom.selectById('mailErrMsgDialog').setProperties({'layout-left':$('#mailMainLayout_ModifyFolderError').offset().left+'px', 'layout-top': $('#mailMainLayout_ModifyFolderError').offset().top-40+'px'});
			Top.Dom.selectById('mailErrMsgDialog').open();
			Top.Dom.selectById('mailMainLayout_ModifyFolderName').focus();
			return;	
		} else if(displayName == '') {
			this.error = 'empty';
			Top.Dom.selectById('mailMainLayout_ModifyFolderError').setProperties({'visible':'visible'});
			Top.Dom.selectById('mailErrMsgDialog').setProperties({'layout-left':$('#mailMainLayout_ModifyFolderError').offset().left+'px', 'layout-top': $('#mailMainLayout_ModifyFolderError').offset().top-40+'px'});
			Top.Dom.selectById('mailErrMsgDialog').open();
			Top.Dom.selectById('mailMainLayout_ModifyFolderName').focus();
			return;	
		} else if(parentFolder == 'MFL0001') {
			for(var j=0; j<folderName[0].length; j++) {
				if(displayName == folderName[0][j]) {
					this.error = 'dup';
					Top.Dom.selectById('mailMainLayout_ModifyFolderError').setProperties({'visible':'visible'});
					Top.Dom.selectById('mailErrMsgDialog').setProperties({'layout-left':$('#mailMainLayout_ModifyFolderError').offset().left+'px', 'layout-top': $('#mailMainLayout_ModifyFolderError').offset().top-40+'px'});
					Top.Dom.selectById('mailErrMsgDialog').open();
					Top.Dom.selectById('mailMainLayout_ModifyFolderName').focus();
					return;	
				}
			}
		} else if(parentFolder == 'MFL0003') {
			for(var j=0; j<folderName[1].length; j++) {
				if(displayName == folderName[1][j]) {
					this.error = 'dup';
					Top.Dom.selectById('mailMainLayout_ModifyFolderError').setProperties({'visible':'visible'});
					Top.Dom.selectById('mailErrMsgDialog').setProperties({'layout-left':$('#mailMainLayout_ModifyFolderError').offset().left+'px', 'layout-top': $('#mailMainLayout_ModifyFolderError').offset().top-40+'px'});
					Top.Dom.selectById('mailErrMsgDialog').open();
					Top.Dom.selectById('mailMainLayout_ModifyFolderName').focus();
					return;	
				}
			}
		} else if(parentFolder == 'MFL0005') {
			for(var j=0; j<folderName[2].length; j++) {
				if(displayName == folderName[2][j]) {
					this.error = 'dup';
					Top.Dom.selectById('mailMainLayout_ModifyFolderError').setProperties({'visible':'visible'});
					Top.Dom.selectById('mailErrMsgDialog').setProperties({'layout-left':$('#mailMainLayout_ModifyFolderError').offset().left+'px', 'layout-top': $('#mailMainLayout_ModifyFolderError').offset().top-40+'px'});
					Top.Dom.selectById('mailErrMsgDialog').open();
					Top.Dom.selectById('mailMainLayout_ModifyFolderName').focus();
					return;	
				}
			}
		}
		
		var data={
				dto: {
						"ACCOUNT_ID"	: mailData.selectedBox.ACCOUNT_ID,
						"FOLDER_ID"		: mailData.selectedBox.FOLDER_ID,
						"DISPLAY_NAME"	: displayName,
						"CMFLG"			: "FolderNameUpdate"
				
				}
		};

		
		Top.Ajax.execute({
			
			type : "POST",
			url : _workspace.url + "Mail/Mail?action=FolderNameUpdate",
			dataType : "json",
			data	 : JSON.stringify(data),
			contentType: "application/json",
		    xhrFields: {withCredentials: true},
			crossDomain : true,
			success : function(ret,xhr,status) {
			
				Top.Controller.get('mailMainLeftLogic').isFolderSaved = true;
				Top.Controller.get('mailMainLeftLogic').setFolderOnly();
			},
			complete : function(ret,xhr,status) {
				
			},
			error : function(ret, xhr,status) {
				
			}
			
		});
		
		
	}, manageByte : function(event, widget){
		 
		 if (widget.getText() == undefined || widget.getText().length == 0)
			 return; 
		 
		 var calByte = {
					getByteLength : function(s) {

						if (s == null || s.length == 0) {
							return 0;
						}
						var size = 0;

						for ( var i = 0; i < s.length; i++) {
							size += this.charByteSize(s.charAt(i));
						}

						return size;
					},
						
					cutByteLength : function(s, len) {

						if (s == null || s.length == 0) {
							return 0;
						}
						var size = 0;
						var rIndex = s.length;

						for ( var i = 0; i < s.length; i++) {
							size += this.charByteSize(s.charAt(i));
							if( size == len ) {
								rIndex = i + 1;
								break;
							} else if( size > len ) {
								rIndex = i;
								break;
							}
						}

						return s.substring(0, rIndex);
					},

					charByteSize : function(ch) {

						if (ch == null || ch.length == 0) {
							return 0;
						}

						var charCode = ch.charCodeAt(0);

						if (charCode <= 0x00007F) {
							return 1;
						} else if (charCode <= 0x0007FF) {
							return 2;
						} else if (charCode <= 0x00FFFF) {
							return 3;
						} else {
							return 4;
						}
					}
				};
		 
		  var result = calByte.cutByteLength(widget.getText(), 250);
		  widget.setText(result);
	 }

	
	
	
	
});

Top.Controller.create('mailDeleteAddressBookPopupLogic', {
	init : function(event, widget) { 
		if (Top.Controller.get("mailAddressBookMainLogic").deleteFromOne == true){
			Top.Dom.selectById("mailDeleteAddressBookPopupLayout_numText").setText("1");
		}
		else{
			if (Top.Dom.selectById('mailAddressBookMainLayout_Table').getCheckedData().length != 0){
				Top.Dom.selectById("mailDeleteAddressBookPopupLayout_numText").setText(Top.Dom.selectById('mailAddressBookMainLayout_Table').getCheckedData().length);
			}
			else{
				Top.Dom.selectById("mailDeleteAddressBookPopupLayout_numText").setText("0");
			}
				
		}
		Top.Dom.selectById("mailDeleteAddressBookPopupLayout_Delete").setProperties({"on-click":"clickDelete"});
		Top.Dom.selectById("mailDeleteAddressBookPopupLayout_Cancel").setProperties({"on-click":"clickBack"});
		
	}, clickDelete : function(){
		if (Top.Controller.get("mailAddressBookMainLogic").deleteFromOne == true){
			var addressBookList = [];
			addressBookList[0] = { "ADDRESSBOOK_ID" : Top.Controller.get("mailAddressBookMainLogic").tempAddressBookId};
			var data={
					dto: {
						"CMFLG"				: "DeleteAddressBook",
						"LOCAKE"  			: "kr",
						"INFO_ADD"			: addressBookList
					}
			}; 
			Top.Ajax.execute({
				
				type : "POST",
				url : _workspace.url + "Mail/Mail?action=AddressBookDelete",
				dataType : "json",
				data	 : JSON.stringify(data),
				crossDomain: true,
				contentType: "application/json",
			    xhrFields: {withCredentials: true},
				success : function(ret,xhr,status) {
					Top.Controller.get('mailAddressBookMainLogic').getMailAddressBookDataList();
					Top.Dom.selectById('mailAddressBookMainLayout_Table').checkAll(false);
					Top.Dom.selectById('mailAddressBookMainLayout_selectAll_CheckBox').setChecked(false);
					Top.Controller.get('mailAddressBookMainLogic').buttonDisabled();
					Top.Dom.selectById('mailDeleteAddressBookDialog').close();
				}
			});
			
			
		}
		else{
			
			if (Top.Dom.selectById('mailAddressBookMainLayout_Table').getCheckedData().length == 0)
				return ;
			
			var addressBookList = [];
			
			//for( var i=0; i<Top.Dom.selectById('mailAddressBookMainLayout_Table').getCheckedData().length; i++) {
				
			//	addressBookList.push(Top.Dom.selectById('mailAddressBookMainLayout_Table').getCheckedData()[i].ADDRESSBOOK_ID);
			//}
			
			for (var i = 0; i < Top.Dom.selectById('mailAddressBookMainLayout_Table').getCheckedData().length; i++){	
				addressBookList[i]= { "ADDRESSBOOK_ID" : Top.Dom.selectById('mailAddressBookMainLayout_Table').getCheckedData()[i].ADDRESSBOOK_ID};  
			}
			
			
			
			var data={
					dto: {
						"CMFLG"				: "DeleteAddressBook",
						"LOCAKE"  			: "kr",
						"INFO_ADD"			: addressBookList
					}
			}; 
			
			Top.Ajax.execute({
				
				type : "POST",
				url : _workspace.url + "Mail/Mail?action=AddressBookDelete",
				dataType : "json",
				data	 : JSON.stringify(data),
				crossDomain: true,
				contentType: "application/json",
			    xhrFields: {withCredentials: true},
				success : function(ret,xhr,status) {
					Top.Controller.get('mailAddressBookMainLogic').getMailAddressBookDataList();
					Top.Dom.selectById('mailAddressBookMainLayout_Table').checkAll(false);
					Top.Dom.selectById('mailAddressBookMainLayout_selectAll_CheckBox').setChecked(false);
					Top.Controller.get('mailAddressBookMainLogic').buttonDisabled();
					Top.Dom.selectById('mailDeleteAddressBookDialog').close();
				}
			});
			
		}
		
		
		
		
		
	}, clickBack : function(){
		Top.Dom.selectById('mailDeleteAddressBookDialog').close(); 
	}
});


Top.Controller.create('mailDeleteExternalAccountPopupLayoutLogic', {
    init : function(event, widget) {
        Top.Dom.selectById('mailDeleteExternalAccountPopupLayout3_Cancel_Button').setProperties({'on-click':'leavePage'});
        Top.Dom.selectById('mailDeleteExternalAccountPopupLayout3_Delete_Button').setProperties({'on-click':'deleteContent'});
    }, 
    
    setDeleteContents : function(account_id, protocol){
    	this.account_id = account_id;
    	this.protocol = protocol;
    },
    
    deleteContent : function(){
    	var data={
    			"dto" :{
    			     "accountList":[
    			         {
    			         "ACCOUNT_ID": this.account_id,
    			         "PROTOCOL" : this.protocol
    			         }
    			     ],
    			     "dbio_total_count" : 1,
    			     "USER_ID" : userManager.getLoginUserId(),
    			     "CMFLG": "DeleteAccount"
    			  }
		};
    	
    	Top.Ajax.execute({
			type : "POST",
			url : _workspace.url + "Mail/Mail?action=DeleteAccount",
			dataType : "json",
			data	 : JSON.stringify(data),
			crossDomain: true,
			contentType: "application/json; charset=utf-8",
		    xhrFields: {withCredentials: true},
			success : function(ret,xhr,status) {
					Top.Controller.get('mailExternalAccountManagementLayoutLogic').loadTable();
					Top.Controller.get('mailMainLeftLogic').setFolderOnly();
					for(var i=0; i<mailDataRepo.mailExternalDataList.length; i++) {
						if(mailDataRepo.mailExternalDataList[i].ACCOUNT_ID == Top.Controller.get('mailDeleteExternalAccountPopupLayoutLogic').account_id) {
							mailDataRepo.mailExternalDataList.splice(i,1);
						}
							
					}
					if(mailData.accountList.length == 2){
						Top.Dom.selectById('mailMainLayout_Button_SendMail').setProperties({'disabled':true});
						Top.Dom.selectById('mailMainLayout_Button_SendMeMail').setProperties({'disabled':true});
					}
					Top.Dom.selectById('deleteExternalAccountDialog').close();
				},
				error : function(request, status, error) {
					TeeAlarm.open({title: '', content: '연결 할 수 없습니다.'});
				}						
		});	
    },
    
    leavePage : function() {
        Top.Dom.selectById('deleteExternalAccountDialog').close();
    }	
});

Top.Controller.create('mailDeleteFolderPopupLogic', {
	
	init : function() {
		
		Top.Dom.selectById('mailDeleteFolderPopupLayout_FolderName1').setText(mailData.selectedBox.DISPLAY_NAME);
		Top.Dom.selectById('mailDeleteFolderPopupLayout_FolderName2').setText(mailData.selectedBox.DISPLAY_NAME);
		
		Top.Dom.selectById('mailDeleteFolderPopupLayout_Delete').setProperties({'on-click':'deleteFolder'});
		Top.Dom.selectById('mailDeleteFolderPopupLayout_Cancel').setProperties({'on-click':function(){Top.Dom.selectById('mailDeleteFolderDialog').close();}});
		
	}, deleteFolder : function() {
		
		var data={
				dto: {
					"ACCOUNT_ID"	: mailData.selectedBox.ACCOUNT_ID,
					"FOLDER_ID"		: mailData.selectedBox.FOLDER_ID,
					"CMFLG"			: "DeleteFolder"
			
			}
		};
	
		Top.Ajax.execute({
			
			type : "POST",
			url : _workspace.url+"Mail/Mail?action=FolderDelete",
			dataType : "json",
			data	 : JSON.stringify(data),
			contentType: "application/json",
			xhrFields: {withCredentials: true},
			crossDomain : true,
			success : function(ret,xhr,status) {
			
				Top.Dom.selectById('mailDeleteFolderDialog').close();
				
				Top.Controller.get('mailMainLeftLogic').isFolderSaved = true;
				
				if(mailData.selectedBox.FOLDER_ID == mailData.curBox.FOLDER_ID)
					Top.Controller.get('mailMainLeftLogic').setFolder();
				else
					Top.Controller.get('mailMainLeftLogic').setFolderOnly();
				
			},
			complete : function(ret,xhr,status) {
				
			},
			error : function(ret, xhr,status) {
				
			}
			
			
		});
		
	}

});
Top.Controller.create('mailDeleteNotReadPopupLogic', {
	
	init : function() {
		
		Top.Dom.selectById('mailDeleteNotReadPopupLayout_FolderName1').setText(mailData.curBox.DISPLAY_NAME);
		if(mailData.curBox.FOLDER_TYPE == 'MFL0007')
			Top.Dom.selectById('mailDeleteNotReadPopupLayout2_2').setText('삭제 후에는 복구할 수 없습니다.')
		
		Top.Dom.selectById('mailDeleteNotReadPopupLayout_Delete').setProperties({'on-click':'deleteNotRead'});
		Top.Dom.selectById('mailDeleteNotReadPopupLayout_Cancel').setProperties({'on-click':function(){Top.Dom.selectById('mailDeleteNotReadDialog').close();}})
	
	}, deleteNotRead : function() {
		
		var labelType = 'NOT_READ';
	
		if(mailData.curBox.FOLDER_TYPE == 'MFL0002')
			labelType = 'NOT_READ_IMPORTANT';
		else if(mailData.curBox.FOLDER_TYPE == 'MFL0007')
			labelType = 'NOT_READ_BIN';
		
		var folderId = mailData.curBox.FOLDER_ID;
		if(folderId == 'AA')
			folderId = '';
		
		var data={
				dto: {
					"CMFLG"			: "DeleteMailByType",
					"ACCOUNT_ID"	: mailData.curBox.ACCOUNT_ID,
					"LABEL_TYPE"	: labelType,
					"FOLDER_ID"		: folderId
					
			
			}
		};
	
			
		Top.Ajax.execute({
			
				type : "POST",
				url : _workspace.url+"Mail/Mail?action=DeleteByType",
				dataType : "json",
				data	 : JSON.stringify(data),
				contentType: "application/json",
				xhrFields: {withCredentials: true},
				crossDomain : true,
				success : function(ret,xhr,status) {
				
					Top.Dom.selectById('mailDeleteNotReadDialog').close();
					Top.Controller.get('mailMainLeftLogic').refreshCurrentList();
					Top.Controller.get('mailMainLeftLogic').resetCount();
					
				},
				complete : function(ret,xhr,status) {
					
				},
				error : function(ret, xhr,status) {
					
				}
				
				
				
			});
		
	}
	
});

Top.Controller.create('mailDeletePermanentLogic', {
	
	init : function() {
		
		Top.Dom.selectById('mailDeletePermanentPopupLayout_Delete').setProperties({'on-click':'deleteTrash'});
		Top.Dom.selectById('mailDeletePermanentPopupLayout_Cancel').setProperties({'on-click':function(){Top.Dom.selectById('mailDeletePermanentDialog').close();}})
		
		var table = (mailData.panelType == '2') ? 'mailTwoPanelLayout_Table' : 'mailThreePanelLayout_Table';
		
		if(mailData.panelType == '2') {
			if(mailData.routeInfo.mailId == undefined)
				Top.Dom.selectById('mailDeletePermanentPopupLayout1_1').setText("선택한 "+Top.Dom.selectById(table).getCheckedData().length+"개 편지를 영구 삭제하시겠습니까?");
			else
				Top.Dom.selectById('mailDeletePermanentPopupLayout1_1').setText(mailData.curBox.DISPLAY_NAME + "의 편지를 영구 삭제하시겠습니까?");
		}
		
	}, deleteTrash : function() {
		
		if(mailData.panelType == '2')
			if(mailData.routeInfo.mailId == undefined)
				Top.Controller.get('mailTwoPanelLogic').setDelete();
			else
				Top.Controller.get('mailReadButtonLogic').deleteForever();
		else
			Top.Controller.get('mailThreePanelMainLogic').clickDelete();
		Top.Dom.selectById('mailDeletePermanentDialog').close();
	}
	
	
});


Top.Controller.create('mailDeleteTrashPopupLogic', {
	
	init : function() {
	
		Top.Dom.selectById('mailDeleteTrashPopupLayout_FolderName1').setText(mailData.selectedBox.DISPLAY_NAME);
		
		Top.Dom.selectById('mailDeleteTrashPopupLayout_Delete').setProperties({'on-click':'deleteTrash'});
		Top.Dom.selectById('mailDeleteTrashPopupLayout_Cancel').setProperties({'on-click':function(){Top.Dom.selectById('mailDeleteTrashDialog').close();}})
		
	}, deleteTrash : function() {
		if(mailData.selectedBox.DISPLAY_NAME == "첨부 파일"){
			Top.Controller.get('mailFileListViewLogic').allRemoveFile();
			return;
		}
		
		var labelType;
		
		if(mailData.selectedBox.FOLDER_TYPE == 'MFL0006')
			labelType = "EMPTY_SPAM";
		else if(mailData.selectedBox.FOLDER_TYPE == 'MFL0007')
			labelType = "EMPTY_BIN";
			
		
		var data={
				dto: {
					"CMFLG"		: "DeleteMailByType",
					"ACCOUNT_ID": mailData.selectedBox.ACCOUNT_ID,
					"LABEL_TYPE": labelType,
					"FOLDER_ID"	: mailData.selectedBox.FOLDER_ID
						}
		};
	
		
		
		Top.Ajax.execute({
		
			type : "POST",
			url : _workspace.url+"Mail/Mail?action=DeleteByType",
			dataType : "json",
			data	 : JSON.stringify(data),
			contentType: "application/json",
			xhrFields: {withCredentials: true},
			crossDomain : true,
			success : function(ret,xhr,status) {
			
				Top.Dom.selectById('mailDeleteTrashDialog').close();
				Top.Controller.get('mailMainLeftLogic').resetCount();
				
				if(mailData.selectedBox.FOLDER_ID == mailData.curBox.FOLDER_ID) {
					Top.Controller.get('mailMainLeftLogic').refreshCurrentList();
				}
				
				
			},
			complete : function(ret,xhr,status) {
				
			},
			error : function(ret, xhr,status) {
				
			}
			
			
			
		});
	}

});
Top.Controller.create('mailEmptyFolderPopupLogic', {
	
	init : function() {
		
		Top.Dom.selectById('mailEmptyFolderPopupLayout_FolderName1').setText(mailData.selectedBox.DISPLAY_NAME);
		Top.Dom.selectById('mailEmptyFolderPopupLayout_FolderName2').setText(mailData.selectedBox.DISPLAY_NAME);
		
		Top.Dom.selectById('mailEmptyFolderPopupLayout_Delete').setProperties({'on-click':'emptyFolder'});
		Top.Dom.selectById('mailEmptyFolderPopupLayout_Cancel').setProperties({'on-click':function(){Top.Dom.selectById('mailEmptyFolderDialog').close();}});
		
	}, emptyFolder : function() {

		var labelType = 'EMPTY_FOLDER';
			
		if(mailData.selectedBox.FOLDER_TYPE == 'MFL0002')
			labelType = 'EMPTY_IMPORTANT';
		
		var data={
				dto: {
					"CMFLG"		: "DeleteMailByType",
					"ACCOUNT_ID": mailData.selectedBox.ACCOUNT_ID,
					"LABEL_TYPE": labelType,
					"FOLDER_ID"	: mailData.selectedBox.FOLDER_ID
						}
		};
	
		
		Top.Ajax.execute({
		
			type : "POST",
			url : _workspace.url+"Mail/Mail?action=DeleteByType",
			dataType : "json",
			data	 : JSON.stringify(data),
			contentType: "application/json",
			xhrFields: {withCredentials: true},
			crossDomain : true,
			success : function(ret,xhr,status) {
			
				Top.Dom.selectById('mailEmptyFolderDialog').close();
				Top.Controller.get('mailMainLeftLogic').resetCount();
				Top.Controller.get('mailMainLeftLogic').refreshCurrentList('selected');
			},
			complete : function(ret,xhr,status) {
				
			},
			error : function(ret, xhr,status) {
				
			}
			
			
			
		});
		
	}
	
});
Top.Controller.create('mailExitWriteLogic', {
	init : function(event, widget) {
		this.tempFolderId;
		Top.Dom.selectById("mailExitWriteLayout_exitButton").setProperties({"on-click" : "clickExit"})
		Top.Dom.selectById("mailExitWriteLayout_closeButton").setProperties({"on-click" : "closePopup"})
		
		
		
	}, clickExit : function(){
		
		if(String(mailData.exitPath) != '') mailData.exitPath = '/' + mailData.exitPath;		
//		Top.Dom.selectById('mailExitWriteDialog').close(true);
		TeeAlarm.close();
		
		let sub = appManager.getSubApp();
		if(sub) Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail' + mailData.exitPath + "?sub=" + sub + "&q=" + new Date().getTime(), {eventType:'fold'});
		else Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail' + mailData.exitPath + "?q=" + new Date().getTime(), {eventType:'close'});
//		Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail' + mailData.exitPath);
		if(mailData.panelType == '3') Top.Controller.get('mailThreePanelMainLogic').resetButton();
		mailData.exitPath = '';
		
		Top.Controller.get('mailThreePanelMainLogic').setCheckAll(false);
		Top.Dom.selectById('mailThreePanelLayout_Table').checkAll(false);
		Top.Dom.selectById('mailThreePanelLayout_Table').check(Top.Dom.selectById('mailThreePanelLayout_Table').getClickedIndex()[0]);
		
	}, closePopup : function(){
		
		mailData.exitPath = '';
		Top.Dom.selectById('mailExitWriteDialog').close();
		
	}




});

Top.Controller.create('mailFolderMenuLogic', {
	
	init : function() {
		
		this.setMenu();
		
		Top.Dom.selectById('mailFolderMenu_Mod').setProperties({'on-click':'changeFolderName'});
		Top.Dom.selectById('mailFolderMenu_Empty').setProperties({'on-click':'emptyFolder'});
		Top.Dom.selectById('mailFolderMenu_Del').setProperties({'on-click':'removeFolder'});
		Top.Dom.selectById('mailFolderMenu_Read').setProperties({'on-click':'setRead'});
		
		
	}, changeFolderName : function() {
		
		Top.Controller.get('TeeSpaceLogic').modifyFolderName();
		Top.Dom.selectById('mailFolderMenuDialog').close();
		
	}, emptyFolder : function() {
		
		if(mailData.selectedBox.FOLDER_TYPE == 'MFL0006' || mailData.selectedBox.FOLDER_TYPE == 'MFL0007'){
//			Top.Dom.selectById('mailDeleteTrashDialog').open();
			TeeAlarm.open({'title' :'휴지통에 있는 항목들을 삭제하시겠습니까?', 'content' :'삭제 후에는 복구할 수 없습니다.',	
				buttons : [{'text' : '삭제', 'onClicked' : function(){ 
					Top.Controller.get('mailDeleteTrashPopupLogic').deleteTrash();
					TeeAlarm.close();
					}}]})
		}	
		else{
//			Top.Dom.selectById('mailEmptyFolderDialog').open();
			TeeAlarm.open({'title' : mailData.selectedBox.DISPLAY_NAME + '을 비우시겠습니까?', 'content' :mailData.selectedBox.DISPLAY_NAME +'에 있는 항목들은 휴지통으로 이동합니다',	
				buttons : [{'text' : '비우기', 'onClicked' : function(){ 
					Top.Controller.get('mailEmptyFolderPopupLogic').emptyFolder();
					TeeAlarm.close();
					}}]})
		}
			
		Top.Dom.selectById('mailFolderMenuDialog').close();
		
	}, removeFolder : function() {
		
//		Top.Dom.selectById('mailDeleteFolderDialog').open();
		TeeAlarm.open({'title' : mailData.selectedBox.DISPLAY_NAME + '을 삭제하시겠습니까?', 'content' :mailData.selectedBox.DISPLAY_NAME +'에 있는 항목들은 휴지통으로 이동합니다',	
			buttons : [{'text' : '삭제', 'onClicked' : function(){ 
				Top.Controller.get('mailDeleteFolderPopupLogic').deleteFolder();
				TeeAlarm.close();
				}}]})
		Top.Dom.selectById('mailFolderMenuDialog').close();
		
	}, setRead : function() {
		
//		Top.Dom.selectById('mailAllReadDialog').open();
		TeeAlarm.open({'title':'모두 읽음처리' ,'content' : mailData.selectedBox.DISPLAY_NAME + '에 있는 항목들을 모두 읽은 상태로 표시하시겠습니까?',	
			buttons : [{'text' : '표시', 'onClicked' : function(){ 
				Top.Controller.get('mailAllReadPopupLogic').changeAllRead();
				TeeAlarm.close();
				}}]})
		Top.Dom.selectById('mailFolderMenuDialog').close();
		
	}, setMenu : function() {
		
		if(mailData.selectedBox.FOLDER_TYPE != 'MFL0008') {
			Top.Dom.selectById('mailFolderMenu_Mod').setProperties({'visible':'none'});
			Top.Dom.selectById('mailFolderMenu_Del').setProperties({'visible':'none'});
		}
	}
});

Top.Controller.create('TeeSpaceLogic', {
	removeMenuIcon : function(event, widget) {

		if(Top.Dom.selectById('mailMainLayout_MailBox_Plus') != null)
			Top.Dom.selectById(mailData.selectedBox.WIDGET_ID).removeWidget(Top.Dom.selectById('mailMainLayout_MailBox_Plus'));
		else if(Top.Dom.selectById('mailMainLayout_MailBox_Edit') != null)
			Top.Dom.selectById(mailData.selectedBox.WIDGET_ID).removeWidget(Top.Dom.selectById('mailMainLayout_MailBox_Edit'));
		else if(Top.Dom.selectById('mailMainLayout_MailBox_Menu') != null)
			Top.Dom.selectById(mailData.selectedBox.WIDGET_ID).removeWidget(Top.Dom.selectById('mailMainLayout_MailBox_Menu'));
		Top.Dom.selectById(mailData.selectedBox.WIDGET_ID).complete();
	}
});

Top.Controller.create('mailInquiryStatusPopupLogic', {
	
	init : function() {

		this.inquiryStatus();
		Top.Dom.selectById('mailInquiryStatusPopupLayout_Close').setProperties({'on-click':'closeDialog'});
		
	}, inquiryStatus : function() {
	
		var list = Top.Controller.get('mailTwoPanelLogic').selectedData;
		

		for(var i=0; i<list.length; i++) {
			
			
			if(list[i].TO_WS_NAME == null && list[i].TO_USER_NAME_IN_WS == null)
				list[i].TO_USER_NAME = list[i].TO_ADDR;
			else if(list[i].TO_USER_NAME_IN_WS == null)
				list[i].TO_USER_NAME = list[i].TO_WS_NAME;
			else
				list[i].TO_USER_NAME = list[i].TO_USER_NAME_IN_WS;
			
			if(list[i].FIRST_SAW_TIME == null) {
				
				list[i].STATUS = Top.i18n.get().value.m000149;
				list[i].STATUS_ICON = 'none';
			} else {
				
				list[i].STATUS_ICON = 'visible';
				
				var year,month,date,hour,minute,second, ampm;
				year = list[i].FIRST_SAW_TIME.substr(0,4);
				month = list[i].FIRST_SAW_TIME.substr(5,2);
				date = list[i].FIRST_SAW_TIME.substr(8,2);
				hour = parseInt(list[i].FIRST_SAW_TIME.substr(11,2));
				minute = list[i].FIRST_SAW_TIME.substr(14,2);
				
				
				if(hour < 12) {
				
					ampm = Top.i18n.get().value.m000147;
					
				} else {
					
					ampm = Top.i18n.get().value.m000148;
					if(hour != '12')  {
						hour = hour - 12;
					}
					
				}
				
				
				list[i].STATUS = year + '.' + month + '.' + date + ' ' + ampm + ' ' + hour + ':' + minute;
				
			}
		}
		
		mailDataRepo.setValue('mailBoxListDataList',list);
		
	}, closeDialog : function() {
			
		Top.Dom.selectById('mailInquiryStatusDialog').close();
	}
});

Top.Controller.create('mailExternalAccountAddErrorLayoutLogic', {
	init : function() {
		Top.Dom.selectById('Button312').setProperties({'on-click':'leavePage'});
	}, leavePage : function() {
		Top.Dom.selectById('errorAddExternalAccountDialog').close();
	}
});

Top.Controller.create('mailExternalAccountAddRegisteredAccountErrorLayoutLogic', {
	init : function() {
		Top.Dom.selectById('Button312').setProperties({'on-click':'leavePage'});
	}, leavePage : function() {
		Top.Dom.selectById('errorAddRegisteredExternalAccountDialog').close();
	}
});

Top.Controller.create('mailExternalAccountAddLayoutLogic', {
    init: function(event, widget) {
        Top.Dom.selectById('mailExternalAccountAddLayout_Cancel_Button').setProperties({ 'on-click': 'leavePage' });
        Top.Dom.selectById('mailExternalAccountAddLayout_Account_Type_Content_Auto_Radio_Button').setProperties({ 'on-click': 'setAutoMode' });
        Top.Dom.selectById('mailExternalAccountAddLayout_Account_Type_Content_Manual_Radio_Button').setProperties({ 'on-click': 'setManualMode' });
        Top.Dom.selectById('mailExternalAccountAddLayout_Add_Button_1').setProperties({ 'on-click': 'clickAdd' });

        Top.Dom.selectById('mailExternalAccountAddLayout_Mail_Server_SelectBox').setProperties({ 'on-change': 'widgetOnBlurForSelectBox' });
        Top.Dom.selectById('mailExternalAccountAddLayout_Mail_Password_Content_TextField').setProperties({ 'on-blur': 'widgetOnBlur' });
        Top.Dom.selectById('mailExternalAccountAddLayout_Agreement_CheckBox_1_1_1_1_1_1').setProperties({ 'on-change': 'widgetOnBlur' });
        Top.Dom.selectById('mailExternalAccountAddLayout_Port_Content_TextField_1').setProperties({ 'on-blur': 'widgetOnBlur' });
        Top.Dom.selectById('mailExternalAccountAddLayout_SMTP_Server_Content_TextField').setProperties({ 'on-blur': 'widgetOnBlur' });
        Top.Dom.selectById('mailExternalAccountAddLayout_SMTP_Port_Content_TextField').setProperties({ 'on-blur': 'widgetOnBlur' })
        Top.Dom.selectById('mailExternalAccountAddLayout_POP_Server_Content_TextField_1').setProperties({'on-blur':'widgetOnBlur'});

        this.initAddExternalAccount();
    },

    initAddExternalAccount: function() {
        this.toggleButtonDisable(true); // default
        this.setAutoMode();
        this.setMailDomainSelectBoxSelectMode();
    },

    isAgreementChecked: function() {
        return Top.Dom.selectById('mailExternalAccountAddLayout_Agreement_CheckBox_1_1_1_1_1_1').isChecked() ? true : false;
    },

    isManualModeChecked: function() {
        return Top.Dom.selectById('mailExternalAccountAddLayout_Account_Type_Content_Manual_Radio_Button').isChecked() ? true : false;
    },

    isMailDomainSelectBoxManualMode: function() { // text field visible or none-visible
        return Top.Dom.selectById('mailExternalAccountAddLayout_Mail_Server_SelectBox').getValue() == "{{Top.i18n.t('value.m000099')}}" ? true : false;
    },

    setMailDomainSelectBoxManualMode: function() {
        Top.Dom.selectById('mailExternalAccountAddLayout_Mail_Server_TextField_2').setProperties({
            'layoutWidth': '155px',
            'margin': '0px 5px 0px 0px'
        });
        Top.Dom.selectById('mailExternalAccountAddLayout_Mail_Server_TextField_2').setVisible("visible");
        Top.Dom.selectById('mailExternalAccountAddLayout_Mail_Server_TextField_2').setProperties({ 'hint': "tmax.co.kr" });
        Top.Dom.selectById('mailExternalAccountAddLayout_Mail_Domain_Manual_Input_Content_Layout').setProperties({'layout-height':'wrap_content'});
        this.setAutoModeDialogSize();
        this.setManualModeDialogSize();
    },

    setMailDomainSelectBoxSelectMode: function() {
        Top.Dom.selectById('mailExternalAccountAddLayout_Mail_Server_TextField_2').setProperties({
            'layoutWidth': '0px',
            'margin': '0px 0px 0px 0px'
        });
        Top.Dom.selectById('mailExternalAccountAddLayout_Mail_Server_TextField_2').setVisible("none");
        Top.Dom.selectById('mailExternalAccountAddLayout_Mail_Server_TextField_2').setProperties({ 'hint': "" });
        Top.Dom.selectById('mailExternalAccountAddLayout_Mail_Domain_Manual_Input_Content_Layout').setProperties({'layout-height':'0px'});
        this.setAutoModeDialogSize();
        this.setManualModeDialogSize();
    },

    setAutoModeDialogSize: function() {
        if (!this.isManualModeChecked()) { // 자동 설정
            if (this.isMailDomainSelectBoxManualMode()) // 메일 주소 직접 입력
                Top.Dom.selectById("addExternalAccountDialog").setProperties({
                'layoutWidth': '469.88px',
                'layoutHeight': '511px'
            });
            else // 메일 주소 선택 입력
                Top.Dom.selectById("addExternalAccountDialog").setProperties({
                'layoutWidth': '469.88px',
                'layoutHeight': '481px'
            });
        }
    },

    setManualModeDialogSize: function() {
        if (this.isManualModeChecked()) {
            if (this.isMailDomainSelectBoxManualMode()) // 메일 주소 직접 입력
                Top.Dom.selectById("addExternalAccountDialog").setProperties({
                'layoutWidth': 'auto',
                'layoutHeight': '686px'
            })
            else // 메일 주소 선택 입력
                Top.Dom.selectById("addExternalAccountDialog").setProperties({
                'layoutWidth': 'auto',
                'layoutHeight': '656px'
            })
        }
    },

    toggleButtonDisable: function(value) {
        Top.Dom.selectById('mailExternalAccountAddLayout_Add_Button_1').setProperties({ 'disabled': value ? 'true' : 'false' });
    },

    widgetOnBlurForSelectBox: function(event, widget) {
        if (widget.id == "mailExternalAccountAddLayout_Mail_Server_SelectBox") {
            if (this.isMailDomainSelectBoxManualMode())
                this.setMailDomainSelectBoxManualMode();
            else
                this.setMailDomainSelectBoxSelectMode();
        }

        this.widgetOnBlur(event, widget);
    },

    widgetOnBlur: function(event, widget) {
        if (this.checkFields())
            this.toggleButtonDisable(false);
        else
            this.toggleButtonDisable(true);

        this.checkFieldsForErrorMessage(event, widget);
    },

    leavePage: function() {
        Top.Dom.selectById('addExternalAccountDialog').close();
        Top.Dom.selectById("addExternalAccountDialog").setProperties({ // 임시
            'layoutWidth': '469.88px',
            'layoutHeight': '540px'
        });
    },

    setAutoMode: function() {
        Top.Dom.selectById('mailExternalAccountAddLayout_Information_Title_Text').setText("@string/value/m000086");
        Top.Dom.selectById('mailExternalAccountAddLayout_Information_Content_Text').setText("@string/value/m000087");
        this.hideManualLayout();
        Top.Dom.selectById("addExternalAccountDialog").adjustPosition();
        this.setAutoModeDialogSize();
    },

    setManualMode: function() {
        Top.Dom.selectById('mailExternalAccountAddLayout_Information_Title_Text').setText("@string/value/m000110");
        Top.Dom.selectById('mailExternalAccountAddLayout_Information_Content_Text').setText("POP/SMTP 서버명 및 포트를 해당 클라이언트 서비스에서 확인하세요.");
        this.showManualLayout();
        Top.Dom.selectById("addExternalAccountDialog").adjustPosition();
        this.setManualModeDialogSize();
    },

    showManualLayout: function() {
        Top.Dom.selectById('mailExternalAccountAddLayout_Account_Protocol_Layout').setProperties({
            'layoutHeight': 'wrap_content',
            'margin': '0px 0px 15px 0px'
        });
        Top.Dom.selectById('mailExternalAccountAddLayout_Server_Layout').setProperties({
        	'layoutHeight':'wrap_content',
        	'margin':'0px 0px 22px 0px'
        });
        Top.Dom.selectById('mailExternalAccountAddLayout_Agreement_Layout_1').setProperties({
            'layoutHeight': 'wrap_content',
            'margin': '0px 0px 42px 0px'
        });

       
        Top.Dom.selectById('mailExternalAccountAddLayout_Agreement_Layout').setProperties({ 'layoutHeight': 'wrap_content' });
        
    },

    hideManualLayout: function() {
        Top.Dom.selectById('mailExternalAccountAddLayout_Account_Protocol_Layout').setProperties({
            'layoutHeight': '0px',
            'margin': '0px 0px 0px 0px'
        });
        Top.Dom.selectById('mailExternalAccountAddLayout_Server_Layout').setProperties({
        	'layoutHeight':'0px',
        	'margin':'0px 0px 0px 0px'
        });
        Top.Dom.selectById('mailExternalAccountAddLayout_Agreement_Layout').setProperties({ 'layoutHeight': '0px' });
    },

    clickAdd: function(event, widget) {
        if (this.checkFields())
            this.callAjaxSend();
    },

    setVisibleInputError: function(target, className, icon, popover) {
        Top.Dom.selectById(target).addClass(className);
        Top.Dom.selectById(icon).addClass("icon-chart-error");
        Top.Dom.selectById(icon).setProperties({
            "popover-trigger": "hover",
            "popover-id": popover,
            "popover-target": icon,
            "visible": "visible"
        });
        Top.Dom.selectById(popover).setProperties({
            "visible": "visible",
            "placement": "top"
        });
    },

    setNoneVisibleIconError: function(icon) {
        Top.Dom.selectById(icon).setProperties({ "visible": "none" });
    },

    removeClass: function(target, className) {
        Top.Dom.selectById(target).removeClass(className);
    },

    setNoneVisibleAllError: function() {
        this.setNoneVisibleIconError("mailExternalAccountAddLayout_Id_Input_Error_Icon");
        this.setNoneVisibleIconError("mailExternalAccountAddLayout_Id_Manual_Input_Error_Icon");
        this.setNoneVisibleIconError("mailExternalAccountAddLayout_Pw_Input_Error_Icon");
        this.setNoneVisibleIconError("mailExternalAccountAddLayout_Port_Input_Error_Icon");
        this.setNoneVisibleIconError("mailExternalAccountAddLayout_SMTP_Server_Input_Error_Icon");
        this.setNoneVisibleIconError("mailExternalAccountAddLayout_SMTP_Port_Input_Error_Icon");
        this.setNoneVisibleIconError("mailExternalAccountAddLayout_POP_Server_Input_Error_Icon");
        
        this.removeClass("mailExternalAccountAddLayout_Mail_Server_TextField", "mail-error-textfield");
        this.removeClass("mailExternalAccountAddLayout_Mail_Server_SelectBox", "mail-error-selectbox");
        this.removeClass("mailExternalAccountAddLayout_Mail_Server_TextField_2", "mail-error-textfield");
        this.removeClass("mailExternalAccountAddLayout_Mail_Password_Content_TextField", "mail-error-textfield");
        this.removeClass("mailExternalAccountAddLayout_Port_Content_TextField_1", "mail-error-textfield");
        this.removeClass("mailExternalAccountAddLayout_SMTP_Server_Content_TextField", "mail-error-textfield");
        this.removeClass("mailExternalAccountAddLayout_SMTP_Port_Content_TextField", "mail-error-textfield");
        this.removeClass("mailExternalAccountAddLayout_POP_Server_Content_TextField_1", "mail-error-textfield");
    },

    isMailIdBlank: function() {
        return ((Top.Dom.selectById('mailExternalAccountAddLayout_Mail_Server_TextField').getText() === "") ||
            (typeof Top.Dom.selectById('mailExternalAccountAddLayout_Mail_Server_TextField').getText() == "undefined")) ? true : false;
    },

    isMailDomainNotSelected: function() {
        return (typeof Top.Dom.selectById('mailExternalAccountAddLayout_Mail_Server_SelectBox').getSelectedText() == "undefined") ? true : false;
    },

    isMailDomainBlank: function() {
        return ((Top.Dom.selectById('mailExternalAccountAddLayout_Mail_Server_TextField_2').getText() === "") ||
            (typeof Top.Dom.selectById('mailExternalAccountAddLayout_Mail_Server_TextField_2').getText() == "undefined")) ? true : false;
    },

    isPasswordBlank: function() {
        return ((Top.Dom.selectById('mailExternalAccountAddLayout_Mail_Password_Content_TextField').getText() === "") ||
            (typeof Top.Dom.selectById('mailExternalAccountAddLayout_Mail_Password_Content_TextField').getText() == "undefined")) ? true : false;
    },

    isPopServerBlank : function() {
    	return ((Top.Dom.selectById('mailExternalAccountAddLayout_POP_Server_Content_TextField_1').getText() === "") ||
                (typeof Top.Dom.selectById('mailExternalAccountAddLayout_POP_Server_Content_TextField_1').getText() == "undefined")) ? true : false;
    },
    
    isMailPortBlank: function() {
        return ((Top.Dom.selectById('mailExternalAccountAddLayout_Port_Content_TextField_1').getText() === "") ||
            (typeof Top.Dom.selectById('mailExternalAccountAddLayout_Port_Content_TextField_1').getText() == "undefined")) ? true : false;
    },

    isSmtpServerBlank: function() {
        return ((Top.Dom.selectById('mailExternalAccountAddLayout_SMTP_Server_Content_TextField').getText() === "") ||
            (typeof Top.Dom.selectById('mailExternalAccountAddLayout_SMTP_Server_Content_TextField').getText() == "undefined")) ? true : false;
    },

    isSmtpPortBlank: function() {
        return ((Top.Dom.selectById('mailExternalAccountAddLayout_SMTP_Port_Content_TextField').getText() === "") ||
            (typeof Top.Dom.selectById('mailExternalAccountAddLayout_SMTP_Port_Content_TextField').getText() == "undefined")) ? true : false;
    },

    checkFieldsForErrorMessage: function(event, widget) {
        this.setNoneVisibleAllError();

        if (this.isManualModeChecked()) { // 수동 설정일 때
            switch (widget.id) {
                // SMTP 포트 확인
                case 'mailExternalAccountAddLayout_SMTP_Port_Content_TextField':
                    if (this.isSmtpPortBlank())
                        this.setVisibleInputError("mailExternalAccountAddLayout_SMTP_Port_Content_TextField",
                            "mail-error-textfield",
                            "mailExternalAccountAddLayout_SMTP_Port_Input_Error_Icon",
                            "mailAddExternalAccountSmtpPortInputErrorPopover");
                    // SMTP 서버 확인
                case 'mailExternalAccountAddLayout_SMTP_Server_Content_TextField':
                    if (this.isSmtpServerBlank())
                        this.setVisibleInputError("mailExternalAccountAddLayout_SMTP_Server_Content_TextField",
                            "mail-error-textfield",
                            "mailExternalAccountAddLayout_SMTP_Server_Input_Error_Icon",
                            "mailAddExternalAccountSmtpServerInputErrorPopover");
                    // 포트 확인
                case 'mailExternalAccountAddLayout_Port_Content_TextField_1':
                    if (this.isMailPortBlank())
                        this.setVisibleInputError("mailExternalAccountAddLayout_Port_Content_TextField_1",
                            "mail-error-textfield",
                            "mailExternalAccountAddLayout_Port_Input_Error_Icon",
                            "mailAddExternalAccountPortInputErrorPopover");
                    // POP 서버 확인
                case 'mailExternalAccountAddLayout_POP_Server_Content_TextField_1':
                	if (this.isPopServerBlank())
                		this.setVisibleInputError("mailExternalAccountAddLayout_POP_Server_Content_TextField_1",
                                "mail-error-textfield",
                                "mailExternalAccountAddLayout_POP_Server_Input_Error_Icon",
                                "mailAddExternalAccountMailServerSelectErrorPopover");
                    // 비밀번호 확인
                case 'mailExternalAccountAddLayout_Mail_Password_Content_TextField':
                    if (this.isPasswordBlank())
                        this.setVisibleInputError("mailExternalAccountAddLayout_Mail_Password_Content_TextField",
                            "mail-error-textfield",
                            "mailExternalAccountAddLayout_Pw_Input_Error_Icon",
                            "mailAddExternalAccountPwInputErrorPopover");
                    // 메일 도메인 확인 (직접입력)
                case 'mailExternalAccountAddLayout_Mail_Server_TextField_2':
                    if (this.isMailDomainSelectBoxManualMode()) {
                        if (this.isMailDomainBlank())
                            this.setVisibleInputError("mailExternalAccountAddLayout_Mail_Server_TextField_2",
                                "mail-error-textfield",
                                "mailExternalAccountAddLayout_Id_Manual_Input_Error_Icon",
                                "mailAddExternalAccountDomainManualInputErrorPopover");
                    }
                    // 메일 도메인 확인
                case 'mailExternalAccountAddLayout_Mail_Server_SelectBox':
                    if (this.isMailDomainNotSelected())
                        this.setVisibleInputError("mailExternalAccountAddLayout_Mail_Server_SelectBox",
                            "mail-error-selectbox",
                            "mailExternalAccountAddLayout_Id_Input_Error_Icon",
                            "mailAddExternalAccountIdInputErrorPopover");
                    // 메일 아이디 확인
                case 'mailExternalAccountAddLayout_Mail_Server_TextField':
                    if (this.isMailIdBlank())
                        this.setVisibleInputError("mailExternalAccountAddLayout_Mail_Server_TextField",
                            "mail-error-textfield",
                            "mailExternalAccountAddLayout_Id_Input_Error_Icon",
                            "mailAddExternalAccountIdInputErrorPopover");
            }
        } else { // 자동 설정일 때
            switch (widget.id) {
                    // 비밀번호 확인
                case 'mailExternalAccountAddLayout_Mail_Password_Content_TextField':
                    if (this.isPasswordBlank())
                        this.setVisibleInputError("mailExternalAccountAddLayout_Mail_Password_Content_TextField",
                            "mail-error-textfield",
                            "mailExternalAccountAddLayout_Pw_Input_Error_Icon",
                            "mailAddExternalAccountPwInputErrorPopover");
                    // 메일 도메인 확인 (직접입력)
                case 'mailExternalAccountAddLayout_Mail_Server_TextField_2':
                    if (this.isMailDomainSelectBoxManualMode()) {
                        if (this.isMailDomainBlank())
                            this.setVisibleInputError("mailExternalAccountAddLayout_Mail_Server_TextField_2",
                                "mail-error-textfield",
                                "mailExternalAccountAddLayout_Id_Manual_Input_Error_Icon",
                                "mailAddExternalAccountDomainManualInputErrorPopover");
                    }
                    // 메일 도메인 확인
                case 'mailExternalAccountAddLayout_Mail_Server_SelectBox':
                    if (this.isMailDomainNotSelected())
                        this.setVisibleInputError("mailExternalAccountAddLayout_Mail_Server_SelectBox",
                            "mail-error-selectbox",
                            "mailExternalAccountAddLayout_Id_Input_Error_Icon",
                            "mailAddExternalAccountIdInputErrorPopover");
                    // 메일 아이디 확인
                case 'mailExternalAccountAddLayout_Mail_Server_TextField':
                    if (this.isMailIdBlank())
                        this.setVisibleInputError("mailExternalAccountAddLayout_Mail_Server_TextField",
                            "mail-error-textfield",
                            "mailExternalAccountAddLayout_Id_Input_Error_Icon",
                            "mailAddExternalAccountIdInputErrorPopover");
            }
        }
    },

    checkFields: function() {
        this.setNoneVisibleAllError();

        if (this.isManualModeChecked()) {
            if (this.isMailIdBlank())
                return false;
            if (this.isMailDomainNotSelected() || (this.isMailDomainSelectBoxManualMode() && this.isMailDomainBlank()))
                return false;
            if (this.isPasswordBlank())
                return false;
            if (this.isMailPortBlank())
                return false;
            if (this.isSmtpServerBlank())
                return false;
            if (this.isSmtpPortBlank())
                return false;
            if(this.isPopServerBlank())
            	return false;
            if (!this.isAgreementChecked())
                return false;
        } else { // auto mode
            if (this.isMailIdBlank())
                return false;
            if (this.isMailDomainNotSelected() || (this.isMailDomainSelectBoxManualMode() && this.isMailDomainBlank()))
                return false;
            if (this.isPasswordBlank())
                return false;
            if (!this.isAgreementChecked())
                return false;
        }

        return true;
    },

    getMailAddress: function() {
        if (this.isMailDomainSelectBoxManualMode())
            return Top.Dom.selectById('mailExternalAccountAddLayout_Mail_Server_TextField').getText() + '@' +
                Top.Dom.selectById('mailExternalAccountAddLayout_Mail_Server_TextField_2').getText();
        else
            return Top.Dom.selectById('mailExternalAccountAddLayout_Mail_Server_TextField').getText() + '@' +
                Top.Dom.selectById('mailExternalAccountAddLayout_Mail_Server_SelectBox').getSelectedText();
    },

    getMailPassword: function() {
        return Top.Dom.selectById('mailExternalAccountAddLayout_Mail_Password_Content_TextField').getText();
    },

    getMailPort: function() {
        return Top.Dom.selectById('mailExternalAccountAddLayout_Port_Content_TextField_1').getText();
    },

    getSmtpServer: function() {
        return Top.Dom.selectById('mailExternalAccountAddLayout_SMTP_Server_Content_TextField').getText();
    },

    getSmtpPort: function() {
        return Top.Dom.selectById('mailExternalAccountAddLayout_SMTP_Port_Content_TextField').getText();
    },

    getPopServer : function() {
    	return Top.Dom.selectById('mailExternalAccountAddLayout_POP_Server_Content_TextField_1').getText();
    },
    
    getOriginalSave: function() {
        return Top.Dom.selectById('mailExternalAccountAddLayout_Agreement_CheckBox1').getChecked() ? "yes" : "no";
    },

    getExternalAccountMailList: function() {
        let mailAddedNum = Top.Controller.get("mailMainLeftLogic").accountMailInfo.length;
        let mailAdded = 'mailMainLayout_Mail_Text' + mailAddedNum;
        let mailAddedAccountId = Top.Dom.selectById(mailAdded).getProperties('value');

        var extMailListData = {
            dto: {
                "CMFLG": "Pop3Request",
                "USER_ID": userManager.getLoginUserId(),
                "ACCOUNT_ID": mailAddedAccountId,
                "REQUEST_TYPE":"make"
            }
        };
        Top.Controller.get("mailMainLeftLogic").pop3requestCount = 0;
        Top.Ajax.execute({

            type: "POST",
            url: _workspace.url + "Mail/Mail?action=POP3Request",
            data: JSON.stringify(extMailListData),
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            xhrFields: { withCredentials: true },
            success: function(ret, xhr, status) {
            }
        });


        Top.Controller.get('mailMainLeftLogic').refreshCurrentList();
    },

    callAjaxSend: function() {
    	
    	Top.Loader.start({
        	layout:function(loader){$('#top-dialog-root_addExternalAccountDialog').prepend(loader);}
        });
    	
        var address = this.getMailAddress();
        var id = address.split('@')[0];
        var domain = address.split('@')[1];
        var hostName = domain.split('.')[0];
        var password = this.getMailPassword();
        var protocol = 'POP3';
        var port = this.getMailPort();
        var sendPort = this.getSmtpPort();
        var sendServer = this.getSmtpServer();
        var receiveServer = this.getPopServer();
        var isOriginalSave = this.getOriginalSave();

        if (this.isManualModeChecked()) // 수동 설정
            var data = {
            "dto": {
                "HOST_NAME": hostName,
                "CONNECTION_TYPE": "SSL",
                "PROTOCOL": protocol,
                "ID": id,
                "PASSWORD": password,
                "ORIGINAL_SAVE_YN": isOriginalSave,
                "USER_ID": userManager.getLoginUserId(),
                "USER_EMAIL": address,
                "RECEIVE_PORT": port,
                "RECEIVE_SERVER_NAME": receiveServer,
                "SEND_PORT": sendPort,
                "SEND_SERVER_NAME": sendServer,
                "LOCALE": "",
                "CMFLG": "ManualEnroll"
            }
        };
        else // 자동 설정 
            var data = {
            "dto": {
                "emailEnroll": {
                    "ID": address,
                    "PASSWORD": password,
                    "PROTOCOL": "POP3",
                    "USER_ID": userManager.getLoginUserId(),
                    "CMFLG": ""
                },
                "mailOutbound_IN": {
                    "ServiceName": "",
                    "MailSession": {
                        "hostName": "",
                        "port": "",
                        "id": "",
                        "pw": ""
                    }
                },
                "CMFLG": "OutboundData"
            }
        };

        this.toggleButtonDisable(true);
        
        Top.Ajax.execute({
            type: "POST",
            url: this.isManualModeChecked() ? _workspace.url + "Mail/Mail?action=ManualEnroll" : _workspace.url + "Mail/Mail?action=OutboundData",
            dataType: "json",
            data: JSON.stringify(data),
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            xhrFields: { withCredentials: true },
            success: function(ret, xhr, status) {
                if (ret.dto.RESULT_MSG == "성공") { 
                    Top.Controller.get('mailExternalAccountManagementLayoutLogic').loadTable(); // 새로 고침
                    Top.Controller.get('mailMainLeftLogic').setFolderOnly(Top.Controller.get('mailExternalAccountAddLayoutLogic').getExternalAccountMailList); // 1panel에 외부 계정 추가 & 메일 리스트 가져오기
                    Top.Dom.selectById('addExternalAccountDialog').close();
                    TeeToast.open({'text':Top.i18n.t('value.m000156'),'size':'min'});
                    
                    
                    	Top.Dom.selectById('mailMainLayout_Button_SendMail').setProperties({'disabled':false});
                    	Top.Dom.selectById('mailMainLayout_Button_SendMeMail').setProperties({'disabled':false});
                   
//                    notiFeedback(Top.i18n.t('value.m000156'));
                } else if (ret.dto.RESULT_MSG == "중복") { // 이미 등록된 계정을 등록했을 때
                    Top.Dom.selectById('errorAddRegisteredExternalAccountDialog').open();
                    Top.Controller.get('mailExternalAccountAddLayoutLogic').toggleButtonDisable(false);
                } else { // 계정 등록 실패, RESULT_MSG <= 실패 or null
                    Top.Dom.selectById('errorAddExternalAccountDialog').open();
                    Top.Controller.get('mailExternalAccountAddLayoutLogic').toggleButtonDisable(false);
                }
                Top.Loader.stop();
            },
            error: function(request, status, error) {
            	TeeAlarm.open({title: '', content: '연결 할 수 없습니다.'});
                Top.Controller.get('mailExternalAccountAddLayoutLogic').toggleButtonDisable(false);
                Top.Loader.stop();
            }
        });
    }
});

Top.Controller.create('mailExternalAccountModifyLayoutLogic', {
    init: function(event, widget) {
    	
    	this.setDialogContents(Top.Controller.get('mailExternalAccountManagementLayoutLogic').dto);
    	
        Top.Dom.selectById('mailExternalAccountModifyLayout_Cancel_Button').setProperties({ 'on-click': 'leavePage' });
        Top.Dom.selectById('mailExternalAccountModifyLayout_Account_Type_Content_Auto_Radio_Button').setProperties({ 'on-click': 'setAutoMode' });
        Top.Dom.selectById('mailExternalAccountModifyLayout_Account_Type_Content_Manual_Radio_Button').setProperties({ 'on-click': 'setManualMode' });
        Top.Dom.selectById('mailExternalAccountModifyLayout_Add_Button_1').setProperties({ 'on-click': 'clickModify' });

        Top.Dom.selectById('mailExternalAccountModifyLayout_Mail_Server_TextField').setProperties({ 'on-blur': 'widgetOnBlur' });
        Top.Dom.selectById('mailExternalAccountModifyLayout_Mail_Server_TextField_2').setProperties({ 'on-blur': 'widgetOnBlur' });
        Top.Dom.selectById('mailExternalAccountModifyLayout_Mail_Server_SelectBox').setProperties({ 'on-change': 'widgetOnBlurForSelectBox' });
        Top.Dom.selectById('mailExternalAccountModifyLayout_Mail_Password_Content_TextField').setProperties({ 'on-blur': 'widgetOnBlur' });
        Top.Dom.selectById('mailExternalAccountModifyLayout_Mail_Password_Content_TextField_1').setProperties({ 'on-blur': 'widgetOnBlur' });
        Top.Dom.selectById('mailExternalAccountModifyLayout_Agreement_CheckBox_1_1_1_1_1_1').setProperties({ 'on-change': 'widgetOnBlur' });
        Top.Dom.selectById('mailExternalAccountModifyLayout_Port_Content_TextField_1').setProperties({ 'on-blur': 'widgetOnBlur' });
        Top.Dom.selectById('mailExternalAccountModifyLayout_SMTP_Server_Content_TextField').setProperties({ 'on-blur': 'widgetOnBlur' });
        Top.Dom.selectById('mailExternalAccountModifyLayout_SMTP_Port_Content_TextField').setProperties({ 'on-blur': 'widgetOnBlur' });
        Top.Dom.selectById('mailExternalAccountModifyLayout_POP_Server_Content_TextField_1').setProperties({'on-blur':'widgetOnBlur'});

        this.initModifyExternalAccount();
    },

    initModifyExternalAccount: function() {
        this.setManualMode();
        this.setMailDomainSelectBoxSelectMode();
        this.disableMailIdEditing();
        this.toggleButtonDisable(true);
    },
    
    disableMailIdEditing: function() {
    	Top.Dom.selectById('mailExternalAccountModifyLayout_Mail_Server_TextField').setProperties({'disabled':'true'});
    	Top.Dom.selectById('mailExternalAccountModifyLayout_Mail_Server_SelectBox').setProperties({'disabled':'true'});
    	Top.Dom.selectById('mailExternalAccountModifyLayout_Mail_Server_TextField_2').setProperties({'disabled':'true'});
    },

    isAgreementChecked: function() {
        return Top.Dom.selectById('mailExternalAccountModifyLayout_Agreement_CheckBox_1_1_1_1_1_1').isChecked() ? true : false;
    },

    isManualModeChecked: function() {
        return Top.Dom.selectById('mailExternalAccountModifyLayout_Account_Type_Content_Manual_Radio_Button').isChecked() ? true : false;
    },

    isMailDomainSelectBoxManualMode: function() { // text field visible or none-visible
        return Top.Dom.selectById('mailExternalAccountModifyLayout_Mail_Server_SelectBox').getValue() == "{{Top.i18n.t('value.m000099')}}" ? true : false;
    },

    setMailDomainSelectBoxManualMode: function() {
        Top.Dom.selectById('mailExternalAccountModifyLayout_Mail_Server_TextField_2').setProperties({
            'layoutWidth': '155px',
            'margin': '0px 5px 0px 0px'
        });
        Top.Dom.selectById('mailExternalAccountModifyLayout_Mail_Server_TextField_2').setVisible("visible");
        Top.Dom.selectById('mailExternalAccountModifyLayout_Mail_Server_TextField_2').setProperties({ 'hint': "tmax.co.kr" });
        this.setAutoModeDialogSize();
        this.setManualModeDialogSize();
    },

    setMailDomainSelectBoxSelectMode: function() {
        Top.Dom.selectById('mailExternalAccountModifyLayout_Mail_Server_TextField_2').setProperties({
            'layoutWidth': '0px',
            'margin': '0px 0px 0px 0px'
        });
        Top.Dom.selectById('mailExternalAccountModifyLayout_Mail_Server_TextField_2').setVisible("none");
        Top.Dom.selectById('mailExternalAccountModifyLayout_Mail_Server_TextField_2').setProperties({ 'hint': "" });
        this.setAutoModeDialogSize();
        this.setManualModeDialogSize();
    },

    setAutoModeDialogSize: function() {
        if (!this.isManualModeChecked()) { // 자동 설정
            /*if (this.isMailDomainSelectBoxManualMode()) // 메일 주소 직접 입력
                Top.Dom.selectById("modifyExternalAccountDialog").setProperties({
                'layoutWidth': '469.88px',
                'layoutHeight': '570px'
            });
            else // 메일 주소 선택 입력
                Top.Dom.selectById("modifyExternalAccountDialog").setProperties({
                'layoutWidth': '469.88px',
                'layoutHeight': '540px'
            });*/
        }
    },

    setManualModeDialogSize: function() {
        if (this.isManualModeChecked()) {
            if (this.isMailDomainSelectBoxManualMode()) // 메일 주소 직접 입력
                Top.Dom.selectById("modifyExternalAccountDialog").setProperties({
                'layoutWidth': 'auto',
                'minWidth': '598px',
                'layoutHeight': 'auto' //690px
            })
            else // 메일 주소 선택 입력
                Top.Dom.selectById("modifyExternalAccountDialog").setProperties({
                'layoutWidth': 'auto',
                'minWidth':'598px',
                'layoutHeight': 'auto' //660px
            })
        }
    },

    setDialogContents: function(input_dto) {
        this.context_dto = input_dto;

        Top.Dom.selectById('mailExternalAccountModifyLayout_Mail_Server_TextField').setText(input_dto.ID.split('@')[0]);
        Top.Dom.selectById('mailExternalAccountModifyLayout_Mail_Server_SelectBox').select(input_dto.ID.split('@')[1]);
        Top.Dom.selectById('mailExternalAccountModifyLayout_Account_Protocol_SelectBox').select(input_dto.PROTOCOL);
        Top.Dom.selectById('mailExternalAccountModifyLayout_POP_Server_Content_TextField_1').setText(input_dto.RECEIVE_SERVER_NAME);
        Top.Dom.selectById('mailExternalAccountModifyLayout_Port_Content_TextField_1').setText(input_dto.RECEIVE_PORT);
        Top.Dom.selectById('mailExternalAccountModifyLayout_SMTP_Server_Content_TextField').setText(input_dto.SEND_SERVER_NAME);
        Top.Dom.selectById('mailExternalAccountModifyLayout_SMTP_Port_Content_TextField').setText(input_dto.SEND_PORT);
        Top.Dom.selectById('mailExternalAccountModifyLayout_Agreement_CheckBox1').setChecked(input_dto.ORIGINAL_SAVE_YN === "yes" ? true : false)
    },

    toggleButtonDisable: function(value) {
        if (value)
            Top.Dom.selectById('mailExternalAccountModifyLayout_Add_Button_1').setProperties({ 'disabled': 'true' });
        else
            Top.Dom.selectById('mailExternalAccountModifyLayout_Add_Button_1').setProperties({ 'disabled': 'false' });
    },

    widgetOnBlurForSelectBox: function(event, widget) {
      
        if (this.isMailDomainSelectBoxManualMode())
            this.setMailDomainSelectBoxManualMode();
        else
            this.setMailDomainSelectBoxSelectMode();
       
        this.widgetOnBlur(event, widget);
    },

    widgetOnBlur: function(event, widget) {
        if (this.checkFields())
            this.toggleButtonDisable(false);
        else
            this.toggleButtonDisable(true);

        this.checkFieldsForErrorMessage(event, widget);
    },

    leavePage: function() {
        Top.Dom.selectById('modifyExternalAccountDialog').close();
    },

    setAutoMode: function() {
        Top.Dom.selectById('mailExternalAccountModifyLayout_Information_Title_Text').setText('@string/value/m000086');
        Top.Dom.selectById('mailExternalAccountModifyLayout_Information_Content_Text').setText('@string/value/m000087');
        this.hideManualLayout();
        Top.Dom.selectById("modifyExternalAccountDialog").adjustPosition();
        this.setAutoModeDialogSize();
    },

    setManualMode: function() {
        Top.Dom.selectById('mailExternalAccountModifyLayout_Information_Title_Text').setText('@string/value/m000110');
        Top.Dom.selectById('mailExternalAccountModifyLayout_Information_Content_Text').setText('POP/SMTP 서버명 및 포트를 해당 클라이언트 서비스에서 확인하세요.');
        this.showManualLayout();
        Top.Dom.selectById("modifyExternalAccountDialog").adjustPosition();
    },

    showManualLayout: function() {
        Top.Dom.selectById('mailExternalAccountModifyLayout_Account_Protocol').setProperties({
            'layoutHeight': 'wrap_content',
            'margin': '0px 0px 15px 0px'
        });
        Top.Dom.selectById('mailExternalAccountModifyLayout_Server_Layout').setProperties({
            'layoutHeight': 'wrap_content',
            'margin': '0px 0px 22px 0px'
        });
        Top.Dom.selectById('mailExternalAccountModifyLayout_Agreement_Layout_1').setProperties({
            'layoutHeight': 'wrap_content',
            'margin': '0px 0px 42px 0px'
        });

        Top.Dom.selectById('mailExternalAccountModifyLayout_Agreement_Layout').setProperties({ 'layoutHeight': 'wrap_content' });
        
    },

    hideManualLayout: function() {
        Top.Dom.selectById('mailExternalAccountModifyLayout_Account_Protocol').setProperties({
            'layoutHeight': '0px',
            'margin': '0px 0px 0px 0px'
        });
        Top.Dom.selectById('mailExternalAccountModifyLayout_Server_Layout').setProperties({
            'layoutHeight': '0px',
            'margin': '0px 0px 0px 0px'
        });
        Top.Dom.selectById('mailExternalAccountModifyLayout_Agreement_Layout').setProperties({ 'layoutHeight': '0px' });
    },

    clickModify: function(event, widget) {
        if (this.checkFields())
            this.callAjaxSend(); //for auto mode
        else
        	TeeAlarm.open({title: '', content: 'check fields'});
    },

    setVisibleInputError: function(target, className, icon, popover) {
        Top.Dom.selectById(target).addClass(className);
        Top.Dom.selectById(icon).addClass("icon-chart-error");
        Top.Dom.selectById(icon).setProperties({
            "popover-trigger": "hover",
            "popover-id": popover,
            "popover-target": icon,
            "visible": "visible"
        });
        Top.Dom.selectById(popover).setProperties({
            "visible": "visible",
            "placement": "top"
        });
    },

    setNoneVisibleIconError: function(icon) {
        Top.Dom.selectById(icon).setProperties({ "visible": "none" });
    },

    removeClass: function(target, className) {
        Top.Dom.selectById(target).removeClass(className);
    },

    setNoneVisibleAllError: function() {
        // icon
        this.setNoneVisibleIconError("mailExternalAccountModifyLayout_Id_Input_Error_Icon");
        this.setNoneVisibleIconError("mailExternalAccountModifyLayout_Id_Manual_Input_Error_Icon");
        this.setNoneVisibleIconError("mailExternalAccountModifyLayout_Pw_Input_Error_Icon");
        this.setNoneVisibleIconError("mailExternalAccountModifyLayout_Pw_Check_Input_Error_Icon");
        this.setNoneVisibleIconError("mailExternalAccountModifyLayout_Port_Input_Error_Icon");
        this.setNoneVisibleIconError("mailExternalAccountModifyLayout_SMTP_Server_Input_Error_Icon");
        this.setNoneVisibleIconError("mailExternalAccountModifyLayout_SMTP_Port_Input_Error_Icon");
        this.setNoneVisibleIconError("mailExternalAccountModifyLayout_POP_Server_Input_Error_Icon");

        // textfield and selectbox
        this.removeClass("mailExternalAccountModifyLayout_Mail_Server_TextField", "mail-error-textfield");
        this.removeClass("mailExternalAccountModifyLayout_Mail_Server_SelectBox", "mail-error-selectbox");
        this.removeClass("mailExternalAccountModifyLayout_Mail_Server_TextField_2", "mail-error-textfield");
        this.removeClass("mailExternalAccountModifyLayout_Mail_Password_Content_TextField", "mail-error-textfield");
        this.removeClass("mailExternalAccountModifyLayout_Mail_Password_Content_TextField_1", "mail-error-textfield");
        this.removeClass("mailExternalAccountModifyLayout_Port_Content_TextField_1", "mail-error-textfield");
        this.removeClass("mailExternalAccountModifyLayout_POP_Server_Content_TextField_1", "mail-error-textfield");
        this.removeClass("mailExternalAccountModifyLayout_SMTP_Server_Content_TextField", "mail-error-textfield");
        this.removeClass("mailExternalAccountModifyLayout_SMTP_Port_Content_TextField", "mail-error-textfield");
    },

    isPopServerBlank: function() {
        return ((Top.Dom.selectById('mailExternalAccountModifyLayout_POP_Server_Content_TextField_1').getText() === "") ||
            (typeof Top.Dom.selectById('mailExternalAccountModifyLayout_POP_Server_Content_TextField_1').getText() == "undefined")) ? true : false;
    },

    isMailIdBlank: function() {
        return ((Top.Dom.selectById('mailExternalAccountModifyLayout_Mail_Server_TextField').getText() === "") ||
            (typeof Top.Dom.selectById('mailExternalAccountModifyLayout_Mail_Server_TextField').getText() == "undefined")) ? true : false;
    },

    isMailDomainNotSelected: function() {
        return (typeof Top.Dom.selectById('mailExternalAccountModifyLayout_Mail_Server_SelectBox').getSelectedText() == "undefined") ? true : false;
    },

    isMailDomainBlank: function() {
        return ((Top.Dom.selectById('mailExternalAccountModifyLayout_Mail_Server_TextField_2').getText() === "") ||
            (typeof Top.Dom.selectById('mailExternalAccountModifyLayout_Mail_Server_TextField_2').getText() == "undefined")) ? true : false;
    },

    isPasswordBlank: function() {
        return ((Top.Dom.selectById('mailExternalAccountModifyLayout_Mail_Password_Content_TextField').getText() === "") ||
            (typeof Top.Dom.selectById('mailExternalAccountModifyLayout_Mail_Password_Content_TextField').getText() == "undefined")) ? true : false;
    },

    isMailPortBlank: function() {
        return ((Top.Dom.selectById('mailExternalAccountModifyLayout_Port_Content_TextField_1').getText() === "") ||
            (typeof Top.Dom.selectById('mailExternalAccountModifyLayout_Port_Content_TextField_1').getText() == "undefined")) ? true : false;
    },

    isSmtpServerBlank: function() {
        return ((Top.Dom.selectById('mailExternalAccountModifyLayout_SMTP_Server_Content_TextField').getText() === "") ||
            (typeof Top.Dom.selectById('mailExternalAccountModifyLayout_SMTP_Server_Content_TextField').getText() == "undefined")) ? true : false;
    },

    isSmtpPortBlank: function() {
        return ((Top.Dom.selectById('mailExternalAccountModifyLayout_SMTP_Port_Content_TextField').getText() === "") ||
            (typeof Top.Dom.selectById('mailExternalAccountModifyLayout_SMTP_Port_Content_TextField').getText() == "undefined")) ? true : false;
    },

    checkFieldsForErrorMessage: function(event, widget) {
        this.setNoneVisibleAllError();

        if (this.isManualModeChecked()) {
            switch (widget.id) {
                // SMTP 포트 확인
                case 'mailExternalAccountModifyLayout_SMTP_Port_Content_TextField':
                    if (this.isSmtpPortBlank())
                        this.setVisibleInputError("mailExternalAccountModifyLayout_SMTP_Port_Content_TextField",
                            "mail-error-textfield",
                            "mailExternalAccountModifyLayout_SMTP_Port_Input_Error_Icon",
                            "mailAddExternalAccountSmtpPortInputErrorPopover");
                    // SMTP 서버 확인
                case 'mailExternalAccountModifyLayout_SMTP_Server_Content_TextField':
                    if (this.isSmtpServerBlank())
                        this.setVisibleInputError("mailExternalAccountModifyLayout_SMTP_Server_Content_TextField",
                            "mail-error-textfield",
                            "mailExternalAccountModifyLayout_SMTP_Server_Input_Error_Icon",
                            "mailAddExternalAccountSmtpServerInputErrorPopover");
                    // 포트 확인
                case 'mailExternalAccountModifyLayout_Port_Content_TextField_1':
                    if (this.isMailPortBlank())
                        this.setVisibleInputError("mailExternalAccountModifyLayout_Port_Content_TextField_1",
                            "mail-error-textfield",
                            "mailExternalAccountModifyLayout_Port_Input_Error_Icon",
                            "mailAddExternalAccountPortInputErrorPopover");
                    // 비밀번호 확인
                case 'mailExternalAccountModifyLayout_Mail_Password_Content_TextField':
                    if (this.isPasswordBlank())
                        this.setVisibleInputError("mailExternalAccountModifyLayout_Mail_Password_Content_TextField",
                            "mail-error-textfield",
                            "mailExternalAccountModifyLayout_Pw_Input_Error_Icon",
                            "mailAddExternalAccountPwInputErrorPopover");
                    // 메일 도메인 확인 (직접입력)
                case 'mailExternalAccountModifyLayout_Mail_Server_TextField_2':
                    if (this.isMailDomainSelectBoxManualMode()) {
                        if (this.isMailDomainBlank())
                            this.setVisibleInputError("mailExternalAccountModifyLayout_Mail_Server_TextField_2",
                                "mail-error-textfield",
                                "mailExternalAccountModifyLayout_Id_Manual_Input_Error_Icon",
                                "mailAddExternalAccountIdInputErrorPopover");
                    }
                    // 메일 도메인 확인
                case 'mailExternalAccountModifyLayout_Mail_Server_SelectBox':
                    if (this.isMailDomainNotSelected())
                        this.setVisibleInputError("mailExternalAccountModifyLayout_Mail_Server_SelectBox",
                            "mail-error-selectbox",
                            "mailExternalAccountModifyLayout_Id_Input_Error_Icon",
                            "mailAddExternalAccountIdInputErrorPopover");
                    // 메일 아이디 확인
                case 'mailExternalAccountModifyLayout_Mail_Server_TextField':
                    if (this.isMailIdBlank())
                        this.setVisibleInputError("mailExternalAccountModifyLayout_Mail_Server_TextField",
                            "mail-error-textfield",
                            "mailExternalAccountModifyLayout_Id_Input_Error_Icon",
                            "mailAddExternalAccountIdInputErrorPopover");
                    // POP 서버 확인
                case 'mailExternalAccountModifyLayout_POP_Server_Content_TextField_1':
                	if (this.isPopServerBlank())
                		this.setVisibleInputError("mailExternalAccountModifyLayout_POP_Server_Content_TextField_1",
                            "mail-error-textfield",
                            "mailExternalAccountModifyLayout_POP_Server_Input_Error_Icon",
                            "mailAddExternalAccountMailServerSelectErrorPopover");
            }
        } else {
            switch (widget.id) {
                    // 비밀번호 확인
                case 'mailExternalAccountModifyLayout_Mail_Password_Content_TextField':
                    if (this.isPasswordBlank())
                        this.setVisibleInputError("mailExternalAccountModifyLayout_Mail_Password_Content_TextField",
                            "mail-error-textfield",
                            "mailExternalAccountModifyLayout_Pw_Input_Error_Icon",
                            "mailAddExternalAccountPwInputErrorPopover");
                    // 메일 도메인 확인 (직접입력)
                case 'mailExternalAccountModifyLayout_Mail_Server_TextField_2':
                    if (this.isMailDomainSelectBoxManualMode()) {
                        if (this.isMailDomainBlank())
                            this.setVisibleInputError("mailExternalAccountModifyLayout_Mail_Server_TextField_2",
                                "mail-error-textfield",
                                "mailExternalAccountModifyLayout_Id_Manual_Input_Error_Icon",
                                "mailAddExternalAccountIdInputErrorPopover");
                    }
                    // 메일 도메인 확인
                case 'mailExternalAccountModifyLayout_Mail_Server_SelectBox':
                    if (this.isMailDomainNotSelected())
                        this.setVisibleInputError("mailExternalAccountModifyLayout_Mail_Server_SelectBox",
                            "mail-error-selectbox",
                            "mailExternalAccountModifyLayout_Id_Input_Error_Icon",
                            "mailAddExternalAccountIdInputErrorPopover");
                    // 메일 아이디 확인
                case 'mailExternalAccountModifyLayout_Mail_Server_TextField':
                    if (this.isMailIdBlank())
                        this.setVisibleInputError("mailExternalAccountModifyLayout_Mail_Server_TextField",
                            "mail-error-textfield",
                            "mailExternalAccountModifyLayout_Id_Input_Error_Icon",
                            "mailAddExternalAccountIdInputErrorPopover");
            }
        }
    },

    checkFields: function() {
        this.setNoneVisibleAllError();

        if (this.isManualModeChecked()) {
            if (this.isPopServerBlank())
                return false;
            if (this.isMailIdBlank())
                return false;
            if (this.isMailDomainNotSelected() || (this.isMailDomainSelectBoxManualMode() && this.isMailDomainBlank()))
                return false;
            if (this.isPasswordBlank())
                return false;
            if (this.isMailPortBlank())
                return false;
            if (this.isSmtpServerBlank())
                return false;
            if (this.isSmtpPortBlank())
                return false;
            if (!this.isAgreementChecked())
                return false;
        } else { // auto mode
            if (this.isMailIdBlank())
                return false;
            if (this.isMailDomainNotSelected() || (this.isMailDomainSelectBoxManualMode() && this.isMailDomainBlank()))
                return false;
            if (this.isPasswordBlank())
                return false;
            if (!this.isAgreementChecked())
                return false;
        }

        return true;
    },

    getMailAddress: function() {
        if (this.isMailDomainSelectBoxManualMode())
            return Top.Dom.selectById('mailExternalAccountModifyLayout_Mail_Server_TextField').getText() + '@' +
                Top.Dom.selectById('mailExternalAccountModifyLayout_Mail_Server_TextField_2').getText();
        else
            return Top.Dom.selectById('mailExternalAccountModifyLayout_Mail_Server_TextField').getText() + '@' +
                Top.Dom.selectById('mailExternalAccountModifyLayout_Mail_Server_SelectBox').getSelectedText();
    },

    getMailPassword: function() {
        return Top.Dom.selectById('mailExternalAccountModifyLayout_Mail_Password_Content_TextField').getText();
    },

    getMailPort: function() {
        return Top.Dom.selectById('mailExternalAccountModifyLayout_Port_Content_TextField_1').getText();
    },

    getSmtpServer: function() {
        return Top.Dom.selectById('mailExternalAccountModifyLayout_SMTP_Server_Content_TextField').getText();
    },

    getSmtpPort: function() {
        return Top.Dom.selectById('mailExternalAccountModifyLayout_SMTP_Port_Content_TextField').getText();
    },

    getPopServer : function() {
    	return Top.Dom.selectById('mailExternalAccountModifyLayout_POP_Server_Content_TextField_1').getText();
    },

    getOriginalSave: function() {
        return Top.Dom.selectById('mailExternalAccountModifyLayout_Agreement_CheckBox1').getChecked() ? "yes" : "no";
    },

    callAjaxSend: function() {
    	
    	Top.Loader.start({
        	layout:function(loader){$('#top-dialog-root_modifyExternalAccountDialog').prepend(loader);}
        });
    	
        var address = this.getMailAddress();
        var id = this.getMailAddress().split('@')[0];
        var domain = this.getMailAddress().split('@')[1];
        var password = this.getMailPassword();
        var protocol = 'POP3';
        var port = this.getMailPort();
        var isOriginalSave = this.getOriginalSave();
        var hostName = domain.split('.')[0];
        var sendPort = this.getSmtpPort();
        var sendServer = this.getSmtpServer();
        var receiveServer = this.getPopServer();
        
        var data = {
            "dto": {
                "ModifyAccount": {
                    "ACCOUNT_ID": this.context_dto.ACCOUNT_ID,
                    "HOST_NAME": hostName,
                    "PORT": port,
                    "CONNECTION_TYPE": "SSL",
                    "PROTOCOL": protocol,
                    "ID": id,
                    "PASSWORD": password,
                    "ORIGINAL_SAVE_YN": isOriginalSave,
                    "USER_ID": userManager.getLoginUserId(),
                    "USER_EMAIL": address,
                    "RECEIVE_PORT": port,
                    "RECEIVE_SERVER_NAME": receiveServer,
                    "SEND_PORT": sendPort,
                    "SEND_SERVER_NAME": sendServer,
                    "LOCALE": "",
                    "CMFLG": ""
                },
                "MailOutbound_IN": {
                    "ServiceName": "",
                    "ServiceType": "",
                    "MailSession": {
                        "hostName": "",
                        "port": "",
                        "id": "",
                        "pw": ""
                    }
                },
                "CMFLG": "ModifyAccount"
            }
        };

        this.toggleButtonDisable(true);
        
        Top.Ajax.execute({
            type: "POST",
            url: _workspace.url + "Mail/Mail?action=ModifyAccount",
            dataType: "json",
            data: JSON.stringify(data),
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            xhrFields: { withCredentials: true },
            success: function(ret, xhr, status) {
                if (ret.dto.RESULT_MSG == "성공") {
                    Top.Controller.get('mailExternalAccountManagementLayoutLogic').loadTable(); // 새로고침
                    Top.Controller.get('mailMainLeftLogic').setFolderOnly(); //메일에 계정 추가
                    Top.Dom.selectById('modifyExternalAccountDialog').close();
                    TeeToast.open({'text':Top.i18n.t('value.m000156'),'size':'min'});
//                    notiFeedback(Top.i18n.t('value.m000156'));
                } else { // 실패
                    Top.Dom.selectById('errorAddExternalAccountDialog').open();
                    Top.Controller.get('mailExternalAccountModifyLayoutLogic').toggleButtonDisable(false);
                }
                Top.Loader.stop();
            },
            error: function(request, status, error) {
            	TeeAlarm.open({title: '', content: '연결 할 수 없습니다.'});
                Top.Controller.get('mailExternalAccountModifyLayoutLogic').toggleButtonDisable(false);
                Top.Loader.stop();
            }
        });
    }
});

Top.Controller.create('mailSendSuccessLogic', {
	
	init : function() {
		
		setTimeout(function() {if(Top.Dom.selectById('mailSendSuccessDialog').isOpen())Top.Dom.selectById('mailSendSuccessDialog').close(true);}, 3000);
		Top.Dom.selectById('mailSendSuccessPopupLayout_Close').setProperties({'on-click':'closeDialog'});
		Top.Dom.selectById('mailSendSuccessPopupLayout_Box').setProperties({'on-click':'moveBox'});
		Top.Dom.selectById('mailSendSuccessPopupLayout_Write').setProperties({'on-click':'moveWrite'});
		
	}, closeDialog : function() {
		
		Top.Dom.selectById('mailSendSuccessDialog').close();
	
	}, moveBox : function() {
		
	}, moveWrite : function() {
		Top.Dom.selectById('mailSendSuccessDialog').close(true);
		mail.newMail();
	}
});
Top.Controller.create('mailSaveSuccessLogic', {
	
	init : function() {
		
		setTimeout(function() {if(Top.Dom.selectById('mailSaveSuccessDialog').isOpen())Top.Dom.selectById('mailSaveSuccessDialog').close(true);}, 3000);
		Top.Dom.selectById('mailSaveSuccessPopupLayout_Close').setProperties({'on-click':'closeDialog'});
		Top.Dom.selectById('mailSaveSuccessPopupLayout_Box').setProperties({'on-click':'moveBox'});
		Top.Dom.selectById('mailSaveSuccessPopupLayout_Write').setProperties({'on-click':'moveWrite'});
	
	}, closeDialog : function() {
		
		Top.Dom.selectById('mailSaveSuccessDialog').close();
	
	}, moveBox : function() {
		
	}, moveWrite : function() {
		Top.Dom.selectById('mailSaveSuccessDialog').close(true);
		mail.newMailToMe();
	}
});
Top.Controller.create('mailPreviewPopupLayoutLogic', {
	init : function(widget) {
    	Top.Dom.selectById('mailPreviewPopupLayout_btnClose').setProperties({'on-click':'onClose'});

        this.previewFile();
    },
    
    onError : function(event, widget) {
        Top.Dom.selectById("mailPreviewPopupLayout_imageViewer").setVisible("none");
        Top.Dom.selectById("mailPreviewPopupLayout_videoViewer").setVisible("none");
        Top.Dom.selectById("mailPreviewPopupLayout_webViewer").setVisible("none");
        Top.Dom.selectById("mailPreviewPopupLayout_notSupported").setVisible("visible");
        
    },
    
    previewFile : function() {
    	if(this.imageSrc != undefined && this.imageSrc != "") {
            Top.Dom.selectById("MailFilePreviewDialog").addClass("preview-image");
            Top.Dom.selectById("MailFilePreviewDialog").removeClass("preview-video");
            Top.Dom.selectById("MailFilePreviewDialog").removeClass("preview-web");
            
            Top.Dom.selectById("mailPreviewPopupLayout_imageViewer").setVisible("visible");
            Top.Dom.selectById("mailPreviewPopupLayout_imageViewer").setSrc(this.imageSrc);
            
            Top.Dom.selectById("mailPreviewPopupLayout_videoViewer").setVisible("none");
            Top.Dom.selectById("mailPreviewPopupLayout_webViewer").setVisible("none");
            Top.Dom.selectById("mailPreviewPopupLayout_notSupported").setVisible("none");
        }
        else if(this.videoSrc != undefined && this.videoSrc != "") {
            Top.Dom.selectById("MailFilePreviewDialog").addClass("preview-video");
            Top.Dom.selectById("MailFilePreviewDialog").removeClass("preview-image");
            Top.Dom.selectById("MailFilePreviewDialog").removeClass("preview-web");
            
            Top.Dom.selectById("mailPreviewPopupLayout_videoViewer").setVisible("visible");
            Top.Dom.selectById("mailPreviewPopupLayout_videoViewer").setSrc(this.videoSrc);
            
            Top.Dom.selectById("mailPreviewPopupLayout_imageViewer").setVisible("none");  
            Top.Dom.selectById("mailPreviewPopupLayout_webViewer").setVisible("none");
            Top.Dom.selectById("mailPreviewPopupLayout_notSupported").setVisible("none");        
        } 
        else if(this.webSrc != undefined && this.webSrc != "") {
        	
        	Top.Dom.selectById("MailFilePreviewDialog").addClass("preview-web");
        	Top.Dom.selectById("MailFilePreviewDialog").removeClass("preview-image");
        	Top.Dom.selectById("MailFilePreviewDialog").removeClass("preview-video");

        	var mailPreviewPopupLayout_webViewer = Top.Dom.selectById("mailPreviewPopupLayout_webViewer");
            mailPreviewPopupLayout_webViewer.setVisible("visible");
            mailPreviewPopupLayout_webViewer.setSrc(this.webSrc);

            data = {
                "whichChannel"          : 4,
                "fileId"                : this.fileID,
                "fileName"              : this.fileName,
                "fileExtension"         : this.fileExt,
                "fileNameWithExtension" : this.fileName + "." + this.fileExt
            };
            
        	Top.Controller.get("officePortalLayoutLogic").setPreviewOfficeFileInfo(data);
        	mailPreviewPopupLayout_webViewer.setProperties({ url : "external/office_preview.html" });
        	
        	Top.Dom.selectById("mailPreviewPopupLayout_imageViewer").setVisible("none");
        	Top.Dom.selectById("mailPreviewPopupLayout_videoViewer").setVisible("none");
        	Top.Dom.selectById("mailPreviewPopupLayout_notSupported").setVisible("none");   
        }
        else {
        	
            Top.Dom.selectById("mailPreviewPopupLayout_imageViewer").setVisible("none");
            Top.Dom.selectById("mailPreviewPopupLayout_videoViewer").setVisible("none");
            Top.Dom.selectById("mailPreviewPopupLayout_webViewer").setVisible("none");
            Top.Dom.selectById("mailPreviewPopupLayout_notSupported").setVisible("visible");
        }
    	
    	Top.Dom.selectById("mailPreviewPopupLayout_fileIcon").template.className = String.format("{0} linear-child-horizontal",
    			getFileIconClass(this.fileExt));
    	Top.Dom.selectById('mailPreviewPopupLayout_fileName').setText(this.fileName + "." + this.fileExt);
    },
    
    onClose : function(event, widget) {
    	var filePreviewLayoutLogic = Top.Controller.get("mailPreviewPopupLayoutLogic");
 	    if(filePreviewLayoutLogic.imageSrc != "") {
 	        URL.revokeObjectURL(filePreviewLayoutLogic.imageSrc);
 	        filePreviewLayoutLogic.imageSrc = "";
 	    } else if(filePreviewLayoutLogic.videoSrc != "") {
             URL.revokeObjectURL(filePreviewLayoutLogic.videoSrc);
             filePreviewLayoutLogic.videoSrc = "";
 	    } else if(filePreviewLayoutLogic.webSrc != "") {
        	URL.revokeObjectURL(filePreviewLayoutLogic.webSrc);
        	filePreviewLayoutLogic.webSrc = "";
        }
 	    
 	    filePreviewLayoutLogic.fileID = "";
 	    filePreviewLayoutLogic.fileExt = "";
 	    filePreviewLayoutLogic.fileName = "";
 	    
 	    Top.Dom.selectById('MailFilePreviewDialog').close();
    }
});

Top.Controller.create('TeeSpaceLogic', {
	onMailPreviewPopupClosed : function(event, widget) {
	    var filePreviewLayoutLogic = Top.Controller.get("mailPreviewPopupLayoutLogic");
	    if(filePreviewLayoutLogic.imageSrc != "") {
	        URL.revokeObjectURL(filePreviewLayoutLogic.imageSrc);
	        filePreviewLayoutLogic.imageSrc = "";
	    } else if(filePreviewLayoutLogic.videoSrc != "") {
            URL.revokeObjectURL(filePreviewLayoutLogic.videoSrc);
            filePreviewLayoutLogic.videoSrc = "";
        } else if(filePreviewLayoutLogic.webSrc != "") {
        	URL.revokeObjectURL(filePreviewLayoutLogic.webSrc);
        	filePreviewLayoutLogic.webSrc = "";
        }
	    
	    filePreviewLayoutLogic.fileID = "";
	    filePreviewLayoutLogic.fileExt = "";
	    filePreviewLayoutLogic.fileName = "";
	},
	
	onMailPreviewPopupKeyDown : function(event, widget) {
		switch(event.code) {
	    case "Escape":
	        widget.close(true);
	        break;
		}
	}
});

Top.Controller.create('mailFilterMenuDialogLogic', {
	
	init : function(){
		this.seeingTypeCode;
		if(this.seeingTypeCode)
			Top.Dom.selectById('mailFilterMenuDialogLayout_Filter_' + this.seeingTypeCode).getChildren()[0].setVisible('visible');
		else
			Top.Dom.selectById('mailFilterMenuDialogLayout_Filter_ALL').getChildren()[0].setVisible('visible');
		
		this.sortingType;
		if(!this.sortingType)
			this.sortingType = 'DATE_DESC';
		
		Top.Dom.selectById('mailFilterMenuDialogLayout_Sort_Date').getChildren()[0].setVisible('visible');
		
		if(Top.Dom.selectById('mailFilterMenuDialog').isOpen()){
			$('html').click(function(e){
				if($(e.target).parents('#top-dialog-root_mailFilterMenuDialog').length != 1){
					Top.Dom.selectById('mailFilterMenuDialog').close();
					$('html').off('click');
				}
			})
	
		}
	},
	
	getSeeingTypeCode : function(TypeText){
		if(TypeText == "모든 편지")
			return "ALL";
		else if(TypeText == "안 읽은 편지")
			return "NOT_READ";
		else if(TypeText == "중요 편지")
			return "IMPORTANT";
		else if(TypeText == "첨부 편지")
			return "ATTACHMENT";
		else
			return "ALL";
	},
	
	applyFilter: function(event, widget){
		for(var i = 1 ; i < 5 ; i++){
			Top.Dom.selectById('mailFilterMenuDialogLayout_Title_Filter').getChildren()[i].getChildren()[0].setVisible('hidden');
		}
		Top.Dom.selectById(widget.getChildren()[0].id).setVisible('visible');
		var seeingTypeCode = this.getSeeingTypeCode(widget.getChildren()[1].getText());
		var sortingType = this.sortingType;
		Top.Controller.get('mailMainLeftLogic').seeingTypeCode = seeingTypeCode;
		
		mailData.panelType == "3"? Top.Controller.get('mailThreePanelMainLogic').filterMail(seeingTypeCode, sortingType) : Top.Controller.get('mailTwoPanelLogic').filterMail(seeingTypeCode, sortingType)
		this.seeingTypeCode = seeingTypeCode;
		
	},
	applySorting: function(event, widget){
		for(var i = 1 ; i < 4 ; i++){
			Top.Dom.selectById('mailFilterMenuDialogLayout_Title_Sort').getChildren()[i].getChildren()[0].setVisible('hidden');
		}
		Top.Dom.selectById(widget.getChildren()[0].id).setVisible('visible');
	},
	toggleSorting : function(event, widget){
		var seeingTypeCode = Top.Controller.get('mailMainLeftLogic').seeingTypeCode;
		if(widget.getSrc() == 'res/ts_filter_down.svg'){
			widget.setSrc('res/ts_filter_up.svg');
			var sortingType = 'DATE_ASC';
			mailData.panelType == "3"? Top.Controller.get('mailThreePanelMainLogic').filterMail(seeingTypeCode, sortingType) : Top.Controller.get('mailTwoPanelLogic').filterMail(seeingTypeCode, sortingType)
		}else{
			widget.setSrc('res/ts_filter_down.svg');
			var sortingType = 'DATE_DESC';
			mailData.panelType == "3"? Top.Controller.get('mailThreePanelMainLogic').filterMail(seeingTypeCode, sortingType) : Top.Controller.get('mailTwoPanelLogic').filterMail(seeingTypeCode, sortingType)
		}
			
		
	}
	
});

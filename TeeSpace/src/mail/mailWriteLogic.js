Top.Controller.create('mailWriteLogic', {
	init : function(event, widget) {
		
		Top.Dom.selectById('mailDropZoneOverlay').setProperties({'visible':'none'});
		
		/*var dropZone = document.querySelector("div#mailWriteLayout");		
		dropZone.addEventListener('dragenter', MailDragDrop.onDragEnter);		
		dropZone.addEventListener('dragleave', MailDragDrop.onDragLeave);
		dropZone.addEventListener('drop'     , MailDragDrop.onDrop);*/
		
		this.recKey = [];
		this.ccKey = [];
		this.bccKey = [];
		
		this.tdrivefile = [];
		this.fileListArr = [];
		this.tempTdrivefile = [];
		this.tempTFileId = [];
		
		this.receiverAddr = [];
		this.CCAddr = [];
		this.BCCAddr = [];
		
		this.blured = false;
		
		if(isb2c()) {
			Top.Dom.selectById("mailWriteLayout_recAddressBookButton__workTree").setVisible('none');
	        Top.Dom.selectById("mailWriteLayout_ccAddressBookButton__workTree").setVisible('none');
	        Top.Dom.selectById("mailWriteLayout_bccAddressBookButton__workTree").setVisible('none');
		}
		
		if(Top.Dom.selectById('mailWriteDialog').isOpen()) {
			Top.Dom.selectById('mailWriteLayout_WriteToMe_Icon').setVisible('none');
			Top.Dom.selectById('mailWriteLayout_WriteToMe_Text').setVisible('none');
		}
		Top.Dom.selectById('mailWriteLayout_WriteToMe_Icon').setVisible('none');
		Top.Dom.selectById('mailWriteLayout_WriteToMe_Text').setVisible('none');
		
		if (Top.Controller.get('mailReadLogic').clickedPrevSender == true){
			Top.Controller.get('mailWriteLogic').receiverAddr.push({'text' : Top.Controller.get('mailReadLogic').PrevSenderAddr})							
			Top.Controller.get('mailReadLogic').clickedPrevSender = null; 
		}

		if (Top.Controller.get('mailReadLogic').clickedNextSender == true){
			Top.Controller.get('mailWriteLogic').receiverAddr.push({'text' : Top.Controller.get('mailReadLogic').NextSenderAddr});
			Top.Controller.get('mailReadLogic').clickedNextSender = null; 
		}

		this.tempYN = null;
		this.tempMailId;
		this.tempGroupIndex;
		if(!Top.Controller.get('mailReadButtonLogic').tempClicked) {
			this.tempMailId = '';
			this.tempGroupIndex = '';
		}
		
		this.tempInt = setInterval(function() {
			if(!mail.isWritingNew()) {
				clearInterval(Top.Controller.get('mailWriteLogic').tempInt);
				return;
			}
				
			Top.Controller.get('mailWriteLogic').saveTemp();
			
		}, 600000);
		Top.Dom.selectById('mailWriteLayout_Button_Preview_1').setProperties({'on-click':'saveTemp'});
		
		
		mailFileAttachmentRepo.setValue('mailFileAttachmentList',[]);

		this.fileAttach = [];
		this.file; 
		this.fileList = [];
		this.fileUp = [];
		this.tempFileIndex = [];
		this.isFromTDrive ;
		this.saveAtTDrive ;
		
		this.set_auto_complete();
		
		if (mailData.panelType == '2'){
			Top.Dom.selectById("mailWriteLayout_Button").setProperties({'visible' : 'visible'});
			if (Top.Controller.get('mailReadButtonLogic').replyClicked || Top.Controller.get('mailReadButtonLogic').replyAllClicked || Top.Controller.get('mailReadButtonLogic').forwardClicked || Top.Controller.get('mailReadButtonLogic').tempClicked || Top.Controller.get('mailReadButtonLogic').sendAgainClicked)
				this.callMail();
		}
		else{
			
			
			if (	(Top.Controller.get('mailThreePanelMainLogic').replyAllClicked || 
					Top.Controller.get('mailThreePanelMainLogic').forwardClicked || 
					Top.Controller.get('mailReadButtonLogic').tempClicked || 
					Top.Controller.get('mailThreePanelMainLogic').replyClicked ||
					Top.Controller.get('mailThreePanelMainLogic').sendAgainClicked || 
					Top.Controller.get('mailThreePanelMainLogic').modifyClicked) &&
					Top.Controller.get('mailWriteLogic').goToThreePanel != true
				) //
				{
				Top.Dom.selectById("mailWriteLayout_Button").setProperties({'visible' : 'none'});
				this.callMail();
				}
			else if(Top.Controller.get('mailWriteLogic').goToThreePanel == true){ 
				Top.Dom.selectById("mailWriteLayout_Button").setProperties({'visible' : 'none'});
				 
				
				this.callMailContentsFromTwoPanel();
				 Top.Controller.get('mailWriteLogic').goToThreePanel = false ;
				
			}
			else{
				Top.Dom.selectById("mailWriteLayout_Button").setProperties({'visible' : 'visible'});
			}
				
		}
		//setTimeout(function(){$('#sun_editorEdit')[0].style.height = "100%";$('#sun_editorEdit')[0].style.maxHeight = '';});
		
		this.isAutoComplete;
		this.isLeaved; 
		this.tempFlag = 0; // 마우스 이벤트 오류로 인한 임시처방,  수정요망
		
		
		
		Top.Dom.selectById("mailWriteLayout_recAddressBookButton").setProperties({'on-click':'clickAddressBookRec' });
        Top.Dom.selectById("mailWriteLayout_ccAddressBookButton").setProperties({'on-click':'clickAddressBookCc' });
        Top.Dom.selectById("mailWriteLayout_bccAddressBookButton").setProperties({'on-click':'clickAddressBookBcc' });
        
        Top.Dom.selectById("mailWriteLayout_recAddressBookButton__workTree").setProperties({'on-click':'clickWorkTree' });
        Top.Dom.selectById("mailWriteLayout_ccAddressBookButton__workTree").setProperties({'on-click':'clickWorkTree' });
        Top.Dom.selectById("mailWriteLayout_bccAddressBookButton__workTree").setProperties({'on-click':'clickWorkTree' });
        
        
        this.clickedArea;  
        
		this.searchSubject = null;
		
    	this.setPrivateAccount(); 
       
		
        Top.Dom.selectById('mailWriteLayout_AttachFile').setProperties({ "on-select": this.clickDropdownMenu});
		Top.Dom.selectById('mailWriteLayout_AttachFile').close(); 
        
		this.mailContent; 
		this.goToThreePanel;

		var media = window.matchMedia("screen and (max-device-width: 1024px)");
		
//		Top.Controller.get('createWorkspaceLayoutLogic').treeviewUpdate(); //T-User TreeView
		
		if(Top.version.substr(8,2) > 87)
			Top.Dom.selectById('Text_Subject').setProperties({'on-blur' : 'manageByte', 'on-keydown' : 'manageByte'});
		else
			Top.Dom.selectById('Text_Subject').setProperties({'on-blur' : 'manageByte', 'on-keyup' : 'manageByte'});
		
		this.setEditorSize();
		
		var defaultHeight = Top.Dom.selectById('mailWriteLayout_WorkArea').getHeight() - $('div#mailWriteLayout_WorkArea .se-toolbar').height();
		$('div#mailWriteLayout_WorkArea .se-wrapper-inner').css('min-height', defaultHeight);
		$('div#mailWriteLayout_WorkArea .se-wrapper-inner').css('max-height', defaultHeight);
		
		if(Top.Controller.get('mailWriteLogic').flg__loadTempMail){
			
			Top.Controller.get('mailWriteLogic').loadTempMail();
			Top.Controller.get('mailWriteLogic').flg__loadTempMail = false;
		}
		
		
		const row = $('#LinearLayout514_6');
		const row2 = $('#mailWriteLayout_FileBox');
		
		
		for(let i=0; i<row.length; i++){ 
			row[i].classList.add("dnd__container");
			row[i].setAttribute("data-dnd-app", "mail");
		}
		for(let i=0; i<row2.length; i++){ 
			row2[i].classList.add("dnd__container");
			row2[i].setAttribute("data-dnd-app", "mail");
		}
		
	},  clickWorkTree : function(event, widget){
		widget.id; 	//mailWriteLayout_bccAddressBookButton__workTree,, mailWriteLayout_recAddressBookButton__workTree,, mailWriteLayout_ccAddressBookButton__workTree
		
		InviteMemberDialog.open({
		    title: '멤버 추가', 
		    confirmedButtonText: '확인',
		    onConfirmedButtonClicked: function (users, meeting) { 
		    		
		    	if(users.length>0){
		    		for(var i=0; i<users.length; i++){
		    			if(users[i].USER_EMAIL == undefined)
		    				continue;
		    			var map = { 'text' : users[i].USER_EMAIL , 'userId':users[i].USER_ID, 'userName':users[i].USER_NAME, 'userPhoto':users[i].THUMB_PHOTO }
//		    			user[i].USER_ID
//		    			user[i].USER_NAME
//		    			user[i].THUMB_PHOTO
//		    			user[i].USER_EMAIL 
		    			if(widget.id == "mailWriteLayout_recAddressBookButton__workTree")
			    		{
		    				Top.Controller.get('mailWriteLogic').receiverAddr.push(map);
			    		}		
			    		else if(widget.id == "mailWriteLayout_ccAddressBookButton__workTree")
			    		{
			    			Top.Controller.get('mailWriteLogic').CCAddr.push(map);
			    		}else if(widget.id == "mailWriteLayout_bccAddressBookButton__workTree")
			    		{
			    			Top.Controller.get('mailWriteLogic').BCCAddr.push(map);
			    		}
		    		}
		    		
		    					
		    		if(widget.id == "mailWriteLayout_recAddressBookButton__workTree")
		    		{
		    			Top.Dom.selectById('Text_ReceiverAddress').setProperties({'chip-items' : Top.Controller.get('mailWriteLogic').receiverAddr});
		    		}		
		    		else if(widget.id == "mailWriteLayout_ccAddressBookButton__workTree")
		    		{
		    			Top.Dom.selectById('Text_Cc').setProperties({'chip-items' : Top.Controller.get('mailWriteLogic').CCAddr});
		    		}else if(widget.id == "mailWriteLayout_bccAddressBookButton__workTree")
		    		{
		    			Top.Dom.selectById('Text_HiddenCc').setProperties({'chip-items' : Top.Controller.get('mailWriteLogic').BCCAddr});	
		    		}
		    	}
		    },
		    hideTeeMeetingCheckbox: true
		});
		
		
	},	setEditorSize : function() {
		
//		var upperHeight = $('top-linearlayout#mailWriteLayout_Button')[0].offsetHeight + $('top-linearlayout#mailWriteLayout_Info')[0].offsetHeight + $('top-linearlayout#mailWriteLayout_FileBox')[0].offsetHeight;
//		Top.Dom.selectById('mailWriteLayout_WorkArea').setProperties({'layout-height':'calc(100% - ' + upperHeight + 'px)'});
		
	}, exitYN : function() {
		
//		Top.Dom.selectById('mailExitWriteDialog').open();
		TeeAlarm.open({'title' :'편지를 저장하지 않고 나가시겠습니까?', 'content' :'수정 사항이 저장되지 않았습니다.',	
			buttons : [{'text' : '나가기', 'onClicked' : function(){ 
				Top.Controller.get('mailExitWriteLogic').clickExit();
				TeeAlarm.close();
				}}]})
		
	},clickDropdownMenu : function(event, widget){
		
		if(widget.getSelectedMenuId() == "menuitem0"){
			Top.Controller.get("mailWriteLogic").saveAtTDrive = false;
			Top.Controller.get("mailWriteLogic").isFromTDrive = false;
			Top.Controller.get("mailWriteLogic").attachFile()
		}
		else if (widget.getSelectedMenuId() == "menuitem1"){
			Top.Controller.get("mailWriteLogic").toTdrive = true; 
			Top.Controller.get("mailWriteLogic").saveAtTDrive = false;
			Top.Controller.get("mailWriteLogic").isFromTDrive = true;
			var config = {
					buttonText1: '첨부', // (선택사항)왼쪽 버튼에 표시될 텍스트. 
					buttonText2: '취소', // (선택사항)오른쪽 버튼에 표시될 텍스트
//					buttonFn1: function(){ Top.Controller.get("mailWriteLogic").attachFile() }, // (선택사항)왼쪽 버튼 동작
					successCallback : function(fileMeta){
						Top.Controller.get("mailWriteLogic").attachFile(fileMeta);
					}
			}
		
			OPEN_TDRIVE(config);
			
		}
		else if (widget.getSelectedMenuId() == "menuitem2"){
			Top.Controller.get("mailWriteLogic").saveAtTDrive = true;
			Top.Controller.get("mailWriteLogic").isFromTDrive = false;
			Top.Controller.get("mailWriteLogic").attachFile()
		}
		
	},	
	clickAddressBookRec: function(){
        
        Top.Controller.get("mailWriteLogic").addRec = true;
        Top.Dom.selectById('mailAddressBookDialog').open();
        
    },clickAddressBookCc: function(){
        
        Top.Controller.get("mailWriteLogic").addCc = true;
        Top.Dom.selectById('mailAddressBookDialog').open();
        
    },clickAddressBookBcc: function(){
        
        Top.Controller.get("mailWriteLogic").addBcc = true;
        Top.Dom.selectById('mailAddressBookDialog').open();
        
    }, clickPreview : function(event, widget) {
		
    	Top.Controller.get('mailWritePreviewLogic').writeMode = 'full';
		Top.Dom.selectById('mailWritePreviewDialog').open();
		
    }, clickSend : function(event, widget) {
		
	
    	var mailSubject = Top.Dom.selectById('Text_Subject').getText();
		if (!mailSubject){
			TeeAlarm.open({'title' : '편지보내기', 'content' : '제목이 입력되지 않았습니다.<br>제목 없이 편지를 보내겠습니까?',
				buttons : [{'text' : '보내기', 'onClicked' : function(){ 
					Top.Dom.selectById('Text_Subject').setText('제목 없음');
					Top.Controller.get('mailWriteLogic').clickSend();
					TeeAlarm.close();
					}}]})
					return;
		}
		
		
		if (Top.Dom.selectById('Text_ReceiverAddress').getProperties('chip-items') != undefined){
			Top.Controller.get('mailWriteLogic').receiverAddr = Top.Dom.selectById('Text_ReceiverAddress').getProperties('chip-items');
		}
		var ReceiverArr = [];
		for (var i = 0; i < Top.Controller.get('mailWriteLogic').receiverAddr.length; i++){
			if (Top.Controller.get('mailWriteLogic').receiverAddr[i].text == ""){
				Top.Controller.get('mailWriteLogic').receiverAddr.splice(i,1); 
				i = i - 1; 
			} else
				ReceiverArr.push({ "TO_ADDR" : mail.getEMailFromChip(this.receiverAddr[i].text)});
			}
		
		if(Top.Dom.selectById('Text_Cc').getProperties('chip-items') != undefined){
			Top.Controller.get('mailWriteLogic').CCAddr = Top.Dom.selectById('Text_Cc').getProperties('chip-items');
		}
		
		var CcArr=[]; 
		for (var i = 0; i < Top.Controller.get('mailWriteLogic').CCAddr.length; i++){	
			if (Top.Controller.get('mailWriteLogic').CCAddr[i].text == ""){
				Top.Controller.get('mailWriteLogic').CCAddr.splice(i,1);
				i = i - 1; 
			} else
				CcArr.push({ "TO_ADDR" : mail.getEMailFromChip(this.CCAddr[i].text)});
		}
		if (Top.Controller.get('mailWriteLogic').CCAddr.length == 0)
			CcArr[i]= { "TO_ADDR" : ""};
		
		if(Top.Dom.selectById('Text_HiddenCc').getProperties('chip-items') != undefined){
			Top.Controller.get('mailWriteLogic').BCCAddr = Top.Dom.selectById('Text_HiddenCc').getProperties('chip-items');
		}
		
		var HiddenCcArr = []; 
		for (var i = 0; i < Top.Controller.get('mailWriteLogic').BCCAddr.length; i++){	
			if (Top.Controller.get('mailWriteLogic').BCCAddr[i].text == ""){
				Top.Controller.get('mailWriteLogic').BCCAddr.splice(i,1);
				i = i - 1; 
			} else
				HiddenCcArr.push({ "TO_ADDR" : mail.getEMailFromChip(this.BCCAddr[i].text)});
		}
		if (Top.Controller.get('mailWriteLogic').BCCAddr.length == 0)
			HiddenCcArr[i]= { "TO_ADDR" : ""};
	
		
		
		 var regExp = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i;
		
		 var errorCnt = 0; 
		 for (var i = 0 ; i < ReceiverArr.length; i++){
			 if ( !regExp.test(ReceiverArr[i].TO_ADDR)  ){
				 errorCnt++; 
			 }
		 }
		 if (Top.Controller.get('mailWriteLogic').receiverAddr.length == 0){
			 Top.Dom.selectById("mailWriteLayout_Receiver_Error_Icon").setProperties({'visible':'visible'});
			 Top.Dom.selectById("mailWriteLayout_Receiver_Error_Icon").setProperties({
				   "popover-id": "EmailValidPopover",
				   "popover-target": "mailWriteLayout_Receiver_Error_Icon"});
			 Top.Dom.selectById('EmailValidPopover').setTitle('받는 사람 주소를 1명 이상 입력해 주세요.');
			 
			 var media = window.matchMedia("screen and (max-device-width: 1024px)");
			 

			 Top.Dom.selectById('mailWriteLayout_Receiver_Layout').setProperties({'border-color' : '#FF0000'})
			 return; 
		 }else if(errorCnt > 0){
			 Top.Dom.selectById("mailWriteLayout_Receiver_Error_Icon").setProperties({'visible':'visible'});
			 Top.Dom.selectById("mailWriteLayout_Receiver_Error_Icon").setProperties({
				   "popover-id": "EmailValidPopover",
				   "popover-target": "mailWriteLayout_Receiver_Error_Icon"});
			 Top.Dom.selectById('EmailValidPopover').setTitle('올바르지 않은 주소 형식입니다.');
			 
			 var media = window.matchMedia("screen and (max-device-width: 1024px)");
			 

			 Top.Dom.selectById('mailWriteLayout_Receiver_Layout').setProperties({'border-color' : '#FF0000'})
			 return; 
		 }
		 else{
			 Top.Dom.selectById("mailWriteLayout_Receiver_Error_Icon").setProperties({"visible": "hidden"});
			 Top.Dom.selectById('mailWriteLayout_Receiver_Layout').setProperties({'border-color' : '#ACB0BC'})
		 }
		

			var senderAddr = Top.Dom.selectById('mailWriteLayout_Info_Sender_Account').getProperties('selectedText');
			
			var domain = Top.Dom.selectById('mailMainLayout_Mail_Text1').getText().substring(Top.Dom.selectById('mailMainLayout_Mail_Text1').getText().indexOf('@'));
		 
			/*if (senderAddr.substring(senderAddr.indexOf('@')) == domain){
			 for (var i = 0 ; i < ReceiverArr.length; i++){
				 receiverDomain = ReceiverArr[i].TO_ADDR.substring(ReceiverArr[i].TO_ADDR.indexOf('@'));
				 if (receiverDomain != domain){
						TeeAlarm.open({title: '보내기', content: '내부 편지함에서 외부 메일 계정으로 전송 할 수 없습니다. 외부 메일함 사용 바랍니다.'});
					 return; 
				}
			 }
		 }
		 else{
		 	 for (var i = 0 ; i < ReceiverArr.length; i++){
				 receiverDomain = ReceiverArr[i].TO_ADDR.substring(ReceiverArr[i].TO_ADDR.indexOf('@'));
				 if (receiverDomain == domain){
						TeeAlarm.open({title: '보내기', content: '죄송합니다.<br>현재는 '+domain +'->' +domain+' 으로만<br>전송이 가능합니다.<br>외부로 메일을 보내시려면 외부계정을 등록 후<br>보낸 사람을 외부계정으로 선택하여 사용하시기 바랍니다.'});
					 return; 
				}
			 }

		 }*/
		 
		

		 
		var importance;
		if (Top.Dom.selectById('CheckBox_Importance').isChecked() == true)
			importance = 'IMP0001';
		else
			importance = 'IMP0002';
		
	
		if (Top.Controller.get('mailWriteLogic').tempYN==true)
    		var tempMail = Top.Controller.get('mailMainLeftLogic').clickedMail;
    	else if (Top.Controller.get('mailWriteLogic').tempMailId != null)
    		var tempMail = Top.Controller.get('mailWriteLogic').tempMailId;
    	else
    		var tempMail = "";

    	var spaceremove = Top.Dom.selectById('mailWriteLayout_WorkArea_HtmlEditor').getValue();//mailSunEditor.getContents();
    	if(spaceremove == undefined)
    		spaceremove = "";
    	else
    		spaceremove = [].filter.call(spaceremove, function (c) {return c.charCodeAt() !== 8203;}).join('');
        var context = spaceremove;
        context = '<style> table { width: 100%; border-spacing : 0px } td, th { border: 1px solid #e1e1e1 } </style>' + context;
		
		var mailAttachment = [] ; 
		var isAttach;
		
			
		var isToOut = false;
		var senderDomain = senderAddr.substring(senderAddr.indexOf("@"));
		
		for (var i = 0 ; i < ReceiverArr.length; i++){
			 receiverDomain = ReceiverArr[i].TO_ADDR.substring(ReceiverArr[i].TO_ADDR.indexOf('@'));
			 if (receiverDomain != domain){
				 isToOut = true;	  
			}
		 }
		
		var ctrl = Top.Controller.get('mailWriteLogic');
		
		var fileListArr = [];

		var parentId = (Top.Dom.selectById('mailMainLayout_FolderName').getText() == '답장' || Top.Dom.selectById('mailMainLayout_FolderName').getText() == '전체 답장') 
						? Top.App.current().params.mailId : '';
		function load_file(callback) {
			
			if (Top.Controller.get('mailWriteLogic').fileAttach.length ==0)
    		{
    		isAttach = 'COM0002';
    		mailAttachment = [{
	            "ATTACHMENT_ID":"",
	            "MAIL_ID": "",
	            "STORAGE_PATH" : "", 
	            "USER_ID" : "",
	            "WS_ID" : "",
	            "EXTENSION" : "",
	            "FILE_NAME" : "",
	            "TDRIVE_CH_ID" : "", 
	            "FILE_ID" : "",
	            "FILE_SIZE" : ""
	            
	        }];
    		callback();
    		}
    	
    	else{
    		
    		Top.Dom.selectById('mailWriteLayout_Button_Send').setProperties({'disabled' : 'true'});
    		Top.Loader.start({parentId:'mailWriteLayout'}); 
    		isAttach = 'COM0001';
    		
    			for (var i = 0; i < Top.Controller.get('mailWriteLogic').fileAttach.length; i++){
    				if(Top.Controller.get('mailWriteLogic').tempFileIndex[i] == "0"){
    				
    					var l = Top.Controller.get('mailWriteLogic').fileAttach[i].file[0].name.split('.').length;
    					var file_extension = Top.Controller.get('mailWriteLogic').fileAttach[i].file[0].name.split('.')[l-1];
    					var file_name = Top.Controller.get('mailWriteLogic').fileAttach[i].file[0].name.slice(0,-(file_extension.length + 1));
    					var file_size = Top.Controller.get('mailWriteLogic').fileAttach[i].file[0].size; 
    					var inputDTO = {
    		    				"dto": {
    		    		               "workspace_id": workspaceManager.getMySpaceId(),
    		    		                "channel_id": mailData.accountList[0].ACCOUNT_ID,
    		    		                "storageFileInfo": {
    		    		                    "user_id": JSON.parse(sessionStorage.getItem('userInfo')).userId,
    		    		                    "file_last_update_user_id": JSON.parse(sessionStorage.getItem('userInfo')).userId,
    		    		                    "file_id": "",
    		    		                    "file_name": file_name,
    		    		                    "file_extension": file_extension,
    		    		                    "file_created_at": "",
    		    		                    "file_updated_at": "",
    		    		                    "file_size": file_size,
    		    		                    "user_context_1": "",
    		    		                    "user_context_2": "",
    		    		                    "user_context_3": ""
    		    		                }
    		    		           }
    		    				
    		    		}
    					
	    				storageManager.UploadFile(Top.Controller.get('mailWriteLogic').fileAttach[i].file[0],
	    						inputDTO, 'Mail', "Mail?action=StorageFileUpload",  function(response){
							if (response.dto.resultMsg!= "Success"){
								Top.Dom.selectById('mailWriteLayout_Button_Send').setProperties({'disabled' : 'false'});
								Top.Loader.stop({parentId:'mailWriteLayout'});
								  return; 
							}
							var tmp = response.dto.storageFileInfoList[0];
							mailAttachment.push({
								"ATTACHMENT_ID":"",
	    			            "MAIL_ID": "",
	    			            "STORAGE_PATH" : "", // 수정필요
	    			            "USER_ID" : userManager.getLoginUserId(),
	    			            "WS_ID" : workspaceManager.getMySpaceId(),
	    			            "EXTENSION" : tmp.file_extension,
	    			            "FILE_NAME" : tmp.file_name,
	    			            "TDRIVE_CH_ID" : Top.Dom.selectById('mailMainLayout_Title_Icon').getProperties('value'), 
	    			            "FILE_ID" : tmp.file_id,
	    			            "FILE_SIZE" : tmp.file_size										
							});
							callback();
						}, function(){
							Top.Dom.selectById('mailWriteLayout_Button_Send').setProperties({'disabled' : 'false'}); 
						    TeeAlarm.open({title: '서버와의 연결 실패', content: '다시 시도해 주세요.'});
						    Top.Loader.stop({parentId:'mailWriteLayout'});
						});
	    				
	    			}
    				else if (Top.Controller.get('mailWriteLogic').tempFileIndex[i] == "3" || Top.Controller.get('mailWriteLogic').tempFileIndex[i] == "4"){
    					var file_extension = Top.Controller.get('mailWriteLogic').fileAttach[i].file_extension;
    					var file_name = Top.Controller.get('mailWriteLogic').fileAttach[i].file_name;
    					var file_size = Top.Controller.get('mailWriteLogic').fileAttach[i].size;
    					mailAttachment.push({
							"ATTACHMENT_ID":"",
    			            "MAIL_ID": "",
    			            "STORAGE_PATH" : Top.Controller.get('mailWriteLogic').fileAttach[i].file_path, // 수정필요
    			            "USER_ID" : userManager.getLoginUserId(),
    			            "WS_ID" : workspaceManager.getMySpaceId(),
    			            "EXTENSION" : file_extension,
    			            "FILE_NAME" : file_name,
    			            "TDRIVE_CH_ID" : Top.Dom.selectById('mailMainLayout_Title_Icon').getProperties('value'), 
    			            "FILE_ID" : Top.Controller.get('mailWriteLogic').fileAttach[i].file_id,
    			            "FILE_SIZE" : file_size										
						});
						callback();
    				}

    				else {
    					
    					var file_name = Top.Controller.get('mailWriteLogic').fileAttach[i].file_name;
    					var file_extension = Top.Controller.get('mailWriteLogic').fileAttach[i].file_extension;
    					var file_size = Top.Controller.get('mailWriteLogic').fileAttach[i].size;
    					storageManager.CopyFile("Shallow", Top.Controller.get('mailWriteLogic').fileAttach[i].file_id,
    							mailData.accountList[0].ACCOUNT_ID,
    							Top.Controller.get('mailWriteLogic').fileAttach[i].file_name, Top.Controller.get('mailWriteLogic').fileAttach[i].file_extension,
    							"","","",
    							function(response){
    								var tmp = response.dto.storageFileInfoList[0];
    								mailAttachment.push({
    									"ATTACHMENT_ID":"",
    		    			            "MAIL_ID": "",
    		    			            "STORAGE_PATH" : "", // 수정필요
    		    			            "USER_ID" : userManager.getLoginUserId(),
    		    			            "WS_ID" : workspaceManager.getMySpaceId(),
    		    			            "EXTENSION" : tmp.file_extension,
    		    			            "FILE_NAME" : tmp.file_name,
    		    			            "TDRIVE_CH_ID" : Top.Dom.selectById('mailMainLayout_Title_Icon').getProperties('value'), 
    		    			            "FILE_ID" : tmp.file_id,
    		    			            "FILE_SIZE" : tmp.file_size			 							
    								});
    								callback();
    								
    							}, function(){}
    						)
    				}
    			}
        	}
    	
		}
    	
		
		
		
		
		
		
		
		

        
        load_file(function(){
			if (Top.Controller.get('mailWriteLogic').fileAttach.length !=0 &&Top.Controller.get('mailWriteLogic').fileAttach.length != mailAttachment.length) return;
			else{
				
				var data={
						"dto":{
			    			"MAIL_FLAG" : "SND0002",
							"MailSendTo" : ReceiverArr,
					        "MailSendCc" : CcArr,
					        "MailSendBcc" : HiddenCcArr,
					        "DOMailAttachment":mailAttachment,
					        "FOLDER_ID":"",
					        "SUBJECT" : mailSubject,
					        "SENDER_NAME" : "",
					        "HAS_ATTACHMENT_YN_CODE" : isAttach,
					        "SEEN_YN_CODE" : "",
					        "IMPORTANCE_YN_CODE" : "",
					        "PINNED_YN_CODE" : "",
					        "FILE_NAME" : "",
					        "WRITING_MAIL_ID" : "",
					        "SYNC_STATE" : "",
					        "SOURCE_MAIL_ID" : "",
					        "FILE_SIZE" : "",
					        "DISPOSITION_NOTI" : "",
					        "MESSAGE_ID" : "",
					        "X_TNEF" : "",
					        "SPAM_YN_CODE" : "",
					        "SENDER_ADDR" : senderAddr,
					        "CONTENT" : context,
					        "CMFLG" : "SendMail_Re",
					        "USER_ID" : userManager.getLoginUserId(),
					        "WS_ID" : workspaceManager.getMySpaceId(),
					        "MESSAGE_HEADER" : "",
					        "EXTRA_INFOS" : "",
					        "FIRST_SAW_TIME" : "", 
					        "SEND_IMPORTANCE_CODE" : importance,
					        "MAIL_ID" : tempMail,
					        "ACCOUNT_ID" : Top.Dom.selectById('mailWriteLayout_Info_Sender_Account').getValue(),
					        "PARENT_MAIL_ID" : parentId

					        
					    }
				};		
		
				Top.Controller.get('mailWriteLogic').sendArr = Top.Dom.selectById('mailWriteLayout_Info_Sender_Account').getProperties('selectedText');
			Top.Ajax.execute({
				
				type : "POST",
				url :_workspace.url + "Mail/Mail?action=Send",
				dataType : "json",
				data	 : JSON.stringify(data),
				contentType: "application/json; charset=utf-8",
				xhrFields: {withCredentials: true},
				crossDomain : true,
				success : function(ret,xhr,status) {
					if(mailData.panelType == '2')
						Top.Dom.selectById('mailWriteLayout_Button_Send').setProperties({'disabled' : 'false'});
					if (Top.Controller.get('mailWriteLogic').fileAttach.length > 0)
							Top.Loader.stop({parentId:'mailWriteLayout'}); 
					if (ret.dto != undefined && ret.dto.RESULT_MSG == 'InvalidEmailAddress')
						{
						var errorAddr = ret.dto.InvaildEmail;
						for(var i=0; i<$('.top-chip-box').length; i++) {
							for(var j=0; j<errorAddr.length; j++) {
								if(errorAddr[j].TO_ADDR == mail.getEMailFromChip($('.top-chip-box')[i].innerText)) {
									$('.top-chip-box:eq('+i+')').addClass('mail-address-error');
								}
							}
							
						}
	    				TeeAlarm.open({title: '보내기', content: '죄송합니다.<br>존재하지 않는 메일 주소 입니다.'});
						return ;
						}
					
					
					let sub = appManager.getSubApp();
					if(sub) 
						Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/new/complete' + "?sub=" + sub, {eventType:'fold'});
					else 
						Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/new/complete' + '?' + new Date().getTime(), {eventType:'close'});
					Top.Controller.get('mailMainLeftLogic').resetCount();
					clearInterval(Top.Controller.get('mailWriteLogic').tempInt);
					
				},
				complete : function(ret,xhr,status) {
				
					Top.Dom.selectById('mailWriteLayout_Button_Send').setProperties({'disabled' : 'false'});
	
					
				},
				error : function(ret, xhr,status) {
					Top.Dom.selectById('mailWriteLayout_Button_Send').setProperties({'disabled' : 'false'});
	
				}
				
			});		
		
		
			
			
			}});
		
        
			
	
	},	clickWriteToMe : function (event, widget){
		
		
		let sub = appManager.getSubApp();
		if(sub) 
			Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/new/complete' + "?sub=" + sub, {eventType:'fold'});
		else 
			Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/new/complete' + '?' + new Date().getTime(), {eventType:'close'});
		
	},
	attachFile : function (fileMeta){
	
		Top.Dom.selectById('mailReadLayout_Attach_Foldable_Icon').setVisible('visible');
		
		if (Top.Controller.get("mailWriteLogic").isFromTDrive == false){ 
			 let fileChooser = Top.Device.FileChooser.create({
				 onBeforeLoad : function(file){return true;},
				 charset : "euc-kr",
		            onFileChoose : function (fileChooser) {   
		            	Top.Controller.get('mailWriteLogic').attachTable(fileChooser);
		           },
		           multiple: true
		        });
			 
				fileChooser.show();
				
		}
		else{

			this.setEditorSize();
			
			Top.Controller.get('mailWriteLogic').tempTFileId = [];
//			for(var i = 0 ; i < Top.Dom.selectById("fcFileContentsTableView").getCheckedData().length; i++){
//				Top.Controller.get('mailWriteLogic').tempTFileId.push(Top.Dom.selectById("fcFileContentsTableView").getCheckedData()[i].file_id)
//			}
			for(var i=0; i< fileMeta.length; i++){
				Top.Controller.get('mailWriteLogic').tempTFileId.push(fileMeta[i].storageFileInfo.file_id)
			}
			
//			for(var i = 0 ; i < Top.Dom.selectById("fcFileContentsTableView").getCheckedData().length; i++){
			for(var i=0; i< fileMeta.length; i++){
				this.putTdrivefileAttach(i, this.putTdriveTempfile, fileMeta[i].storageFileInfo );
			}
			
		}
	


	    }, attachTable : function(data) {
	    	
			Top.Controller.get('mailWriteLogic').tempTFileId = [];
			
	    	var startPoint = Top.Controller.get('mailWriteLogic').fileAttach.length;
	    	for( var i=0; i<data[0].file.length; i++){
	    		var temp = {'file':[data[0].file[i]]};
	    		Top.Controller.get('mailWriteLogic').fileAttach.push(temp);
	    	}
	    	for (var i = startPoint; i < Top.Controller.get('mailWriteLogic').fileAttach.length; i++ ){
				var l = i;
				
				var size = Top.Controller.get('mailWriteLogic').fileAttach[i].file[0].size;
				if ((size*1) < 10485760)
					var method = "일반 첨부" ;
				else{
					var method = "대용량 첨부";
					TeeAlarm.open({title: '', content: '10MB 이하의 파일만 첨부 가능합니다.'});
					Top.Controller.get('mailWriteLogic').fileAttach.pop();
					return;
				}
				
//		    	Top.Dom.selectById('mailWriteLayout_FileBox').setVisible('visible');
		    	this.setEditorSize();
		    	
		    	
		    	// 첨부파일 그려주기 
		    	var fileExtentsion = Top.Controller.get('mailWriteLogic').fileAttach[i].file[0].name.split('.')[(Top.Controller.get('mailWriteLogic').fileAttach[i].file[0].name.split('.').length-1)];
				size = mail.changeSize(size);
		    	var fileInfo = {
						"name" : Top.Controller.get('mailWriteLogic').fileAttach[i].file[0].name,
						"size" : size,
						"attach_method" : method,
						"extension" : fileExtentsion
					};
				
				Top.Controller.get('mailWriteLogic').fileList.push(fileInfo);
				
				this.drawAttachedFile(fileInfo);
	    	
				if(Top.Controller.get("mailWriteLogic").saveAtTDrive == false || Top.Controller.get("mailWriteLogic").saveAtTDrive == undefined){
					Top.Controller.get('mailWriteLogic').tempFileIndex.push("0");
				}
				else{
					Top.Controller.get('mailWriteLogic').tempFileIndex.push("2");
				}
	    	}
			Top.Controller.get('mailWriteLogic').updateAttachmentInfo(); 

	    },
	    
	    putTdrivefileAttach : function(index, callbackFunc, fileMeta) {
				Top.Controller.get('mailWriteLogic').tempFileIndex.push("1");
				
				
				Top.Controller.get('mailWriteLogic').fileAttach.push(
						{
							"file_name" : fileMeta.file_name,
							"file_id" : fileMeta.file_id,
							"size" : fileMeta.file_size,
							"file_extension" : fileMeta.file_extension
						}
				);
				
				Top.Controller.get('mailWriteLogic').updateAttachmentInfo(); 

				
				var size;
				size = fileMeta.file_size;
    			if ((size*1) < 10485760)
    				var method = "일반 첨부" ;
    			else{
    				var method = "대용량 첨부";
    				TeeAlarm.open({title: '', content: '10MB 이하의 파일만 첨부 가능합니다.'});
    				Top.Controller.get('mailWriteLogic').fileAttach.pop();
    				Top.Controller.get('mailWriteLogic').tempFileIndex.pop(); 
    				return;

    			}

    			size = mail.changeSize(size);
    			var fileInfo = {
						"name" : fileMeta.file_name,
						"size" : size,
						"attach_method" : method,
						"extension" : fileMeta.file_extension
					};
				Top.Controller.get('mailWriteLogic').fileList.push(fileInfo);
				
				this.drawAttachedFile(fileInfo); 
				
				callbackFunc(index);
	    },
	    
	    putTdriveTempfile : function(index) {
	    	storageManager.getDownloadFileInfo(Top.Controller.get('mailWriteLogic').tempTFileId[index], mailData.curBox.ACCOUNT_ID,
	    			function(result){
					 	var reader = new window.FileReader();
						reader.readAsDataURL(result.blobData); 
						reader.onloadend = function() {
							
							 var filecontent = reader.result; 
		                     var contents;
		                     if(Object.prototype.toString.call(filecontent) === "[object ArrayBuffer]"){ //ArrayBuffer
		                         var binary = '';
		                        var bytes = new Uint8Array( filecontent );
		                        var len = bytes.byteLength;
		                        for (var j = 0; j < len; j++) {
		                            binary += String.fromCharCode( bytes[ j ] );
		                        }
		                        contents = window.btoa(binary);
		                     }
		                     else if((filecontent.split("base64,").length == 2) &&
		                         RegExp("^(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{4})$").test(filecontent.split("base64,")[1])){ //base64
		                             contents = filecontent.split("base64,")[1];
		                     }
		                     else{ //raw txt
		                         contents = btoa(unescape(encodeURIComponent(ctrl.fileAttach[i].src)));
		                     }
		                     Top.Controller.get("mailWriteLogic").tempTdrivefile.push( {"file_id" : Top.Controller.get('mailWriteLogic').tempTFileId[index], "file_info" : {
		                         "filename" : result.fileName,
		                         "contents" : contents
		                     			}
							 })
						}
			},function(){})	
	    },
	 
		callMail : function ()
		{	
			var mailIdForTemp;
			if (mailData.panelType == '2'){
				mailIdForTemp = Top.Controller.get('mailMainLeftLogic').clickedMail
			}
			else{
				if (Top.Dom.selectById("mailThreePanelLayout_Table").getCheckedData().length == 1){
					mailIdForTemp = Top.Dom.selectById("mailThreePanelLayout_Table").getCheckedData()[0].MAIL_ID;
				}
				else{
					mailIdForTemp = Top.Controller.get('mailThreePanelMainLogic').clickedMail;
				}
				//mailIdForTemp = Top.Controller.get('mailThreePanelMainLogic').clickedMail
			}
			
				Top.Ajax.execute({
					
					type : "GET",
					url : _workspace.url + "Mail/Mail?action=Read&CMFLG=ReadMail&MAIL_ID=" + mailIdForTemp+"&USER_ID="+userManager.getLoginUserId(),
					dataType : "json",
					crossDomain: true,
					contentType: "application/json; charset=utf-8",
				    xhrFields: {withCredentials: true},
					success : function(ret,xhr,status) {
						

						var receiver =[];
						var receiverStr="";
						if (ret.dto.ReceiverList == null || ret.dto.ReceiverList.length == 0 || ret.dto.ReceiverList[0].DISPLAY_NAME == "<>")
							ret.dto.ReceiverList = [];
						for (var i = 0; i < ret.dto.ReceiverList.length; i++){
							if(ret.dto.ReceiverList[i].DISPLAY_NAME.indexOf('<') == -1 && ret.dto.ReceiverList[i].DISPLAY_NAME.indexOf('>') == -1)
								ret.dto.ReceiverList[i].DISPLAY_NAME = ret.dto.ReceiverList[i].DISPLAY_NAME + ' <' + ret.dto.ReceiverList[i].EMAIL_ADDRESS + '>';
							receiver.push({
									'text' : ret.dto.ReceiverList[i].DISPLAY_NAME
								});
							receiverStr = receiverStr + ret.dto.ReceiverList[i].EMAIL_ADDRESS + '; ';
						}
						
						var CC =[];
						var CCStr = "";
						if (ret.dto.CClist == null || ret.dto.CClist.length == 0 || ret.dto.CClist[0].DISPLAY_NAME == "<>")
							ret.dto.CClist = [];
						for (var i = 0; i < ret.dto.CClist.length; i++){
							if(ret.dto.CClist[i].DISPLAY_NAME.indexOf('<') == -1 && ret.dto.CClist[i].DISPLAY_NAME.indexOf('>') == -1)
								ret.dto.CClist[i].DISPLAY_NAME = ret.dto.CClist[i].DISPLAY_NAME + ' <' + ret.dto.CClist[i].EMAIL_ADDRESS + '>';
							CC.push({
									'text' : ret.dto.CClist[i].DISPLAY_NAME
								});
							CCStr = CCStr + ret.dto.CClist[i].EMAIL_ADDRESS + '; ';
							
						}
						

						if(ret.dto.SENDER_NAME == null){
							var tmp1 = [];
							tmp1[0]= "";
						}
						else{
							var tmp1 = ret.dto.SENDER_NAME.split(';')
							if(tmp1.length == 2){
								tmp1 = tmp1[0];
								tmp1 = tmp1.split('<');
								if(tmp1.length == 2){
										 var tmp2 = tmp1[1].split('>');
										 }	
							}
						}
						
						
						
						var sender =[{
							'text' : tmp1[0] + '<' + ret.dto.SENDER_ADDR + '>'
						}];
						var senderStr = ret.dto.SENDER_ADDR + '; ';
						
						
						if (!ret.dto.CONTENT)
							var content = "";
						else
							var content = ret.dto.CONTENT;
						
						//content = content.replace(/<style/gi,'<!--style');
						//content = content.replace(/<\/style>/gi,'<\/style-->');
						content = content.replace(/<xml/gi,'<!--xml');
						content = content.replace(/<\/xml>/gi,'<\/xml-->');
						if (content.indexOf('</html>') == -1 && content.indexOf('<html') == -1)
							content = content.replace(/(?:\r\n|\r|\n)/g, '<br />');
						
						if (Top.Controller.get('mailReadButtonLogic').replyClicked == true || Top.Controller.get('mailThreePanelMainLogic').replyClicked == true){
								Top.Controller.get('mailMainLeftLogic').setTitle('답장'); 
								Top.Dom.selectById('mailWriteLayout_SwitchPanel').setVisible('visible'); 
								Top.Controller.get('mailWriteLogic').receiverAddr = sender;							
								Top.Dom.selectById('Text_Subject').setText('RE : ' + ret.dto.SUBJECT);
								setTimeout(function(){Top.Dom.selectById('mailWriteLayout_WorkArea_HtmlEditor').setEditorHTML('<p><br></p><p><br></p><p><br></p><p>----------------------------------------------------------</p><p><b>보낸 사람</b> : ' +senderStr+ '</p><p><b>날짜</b> : '+ret.dto.RECEIVED_TIME+'</p><p><b>받는 사람</b> : '+receiverStr+'</p><p><b>참조</b> :  '+CCStr+'</p><p><b>제목</b> :  '+ret.dto.SUBJECT+'</p><p><br></p><p>'+content+'</p>');});
								}
							else if (Top.Controller.get('mailReadButtonLogic').replyAllClicked == true || Top.Controller.get('mailThreePanelMainLogic').replyAllClicked == true){
								var emailAddress = '';
                                var list = mailData.accountList;
								var accountId = mailData.curBox.ACCOUNT_ID;
								for(var i=0; i<list.length; i++){
									if(list[i].ACCOUNT_ID == accountId)
										  emailAddress = list[i].EMAIL_ADDRESS;
								}
                                receiver = sender.concat(receiver)
								for (var i = receiver.length-1; i >= 0; i--){
									if(mail.getEMailFromChip(receiver[i].text) == emailAddress)
									    receiver.splice(i, 1);
	
								}
								
								Top.Controller.get('mailMainLeftLogic').setTitle('전체 답장'); 
								Top.Dom.selectById('mailWriteLayout_SwitchPanel').setVisible('visible'); 
								Top.Controller.get('mailWriteLogic').receiverAddr = receiver;
								Top.Dom.selectById('Text_Subject').setText('RE : ' + ret.dto.SUBJECT);
								Top.Controller.get('mailWriteLogic').CCAddr = CC;
								setTimeout(function(){Top.Dom.selectById('mailWriteLayout_WorkArea_HtmlEditor').setEditorHTML('<p><br></p><p><br></p><p><br></p><p>----------------------------------------------------------</p><p><b>보낸 사람</b> : ' +senderStr+ '</p><p><b>날짜</b> : '+ret.dto.RECEIVED_TIME+'</p><p><b>받는 사람</b> : '+receiverStr+'</p><p><b>참조</b> :  '+CCStr+'</p><p><b>제목</b> :  '+ret.dto.SUBJECT+'</p><p><br></p><p>'+content+'</p>');});
								}						
							else if (Top.Controller.get('mailReadButtonLogic').forwardClicked == true || Top.Controller.get('mailThreePanelMainLogic').forwardClicked == true){
								Top.Controller.get('mailMainLeftLogic').setTitle('전달'); 
								Top.Dom.selectById('mailWriteLayout_SwitchPanel').setVisible('visible'); 
								Top.Dom.selectById('Text_Subject').setText('FW : ' + ret.dto.SUBJECT);
								setTimeout(function(){Top.Dom.selectById('mailWriteLayout_WorkArea_HtmlEditor').setEditorHTML('<p><br></p><p><br></p><p><br></p><p>----------------------------------------------------------</p><p><b>보낸 사람</b> : ' +senderStr+ '</p><p><b>날짜</b> : '+ret.dto.RECEIVED_TIME+'</p><p><b>받는 사람</b> : '+receiverStr+'</p><p><b>참조</b> :  '+CCStr+'</p><p><b>제목</b> :  '+ret.dto.SUBJECT+'</p><p><br></p><p>'+content+'</p>');});
								if (ret.dto.Attachment != null && ret.dto.Attachment.length != 0){
//									Top.Dom.selectById('mailWriteLayout_FileBox').setVisible('visible');
									Top.Controller.get('mailWriteLogic').setEditorSize();
									
									for (var i = 0; i < ret.dto.Attachment.length; i++){
										Top.Controller.get('mailWriteLogic').fileAttach.push({
											"file_id" : ret.dto.Attachment[i].FILE_ID,
											"size" : ret.dto.Attachment[i].FILE_SIZE,
											"file_name" : ret.dto.Attachment[i].FILE_NAME,
											"file_extension" : ret.dto.Attachment[i].EXTENSION,
											"file_id" : ret.dto.Attachment[i].FILE_ID,
											"file_path" : ret.dto.Attachment[i].STORAGE_PATH
										});
										Top.Controller.get('mailWriteLogic').tempFileIndex.push("4");
										Top.Controller.get('mailWriteLogic').updateAttachmentInfo(); 

										var size = ret.dto.Attachment[i].FILE_SIZE;
						    			if ((size*1) < 10485760)
						    				var method = "일반 첨부" ;
						    			else
						    				var method = "대용량 첨부";

						    			if ( method == "대용량 첨부" )
						    				var period = "~ "+mail.after30Days(null,'/')+" (30일간)";
						    			else
						    				var period = "제한 없음";

						    			size = mail.changeSize(size);
						    			
										Top.Controller.get('mailWriteLogic').fileList.push(
						    					{
						    						"name" : ret.dto.Attachment[i].FILE_NAME +"."+ ret.dto.Attachment[i].EXTENSION,
						    						"size" : size,
						    						"attach_method" : method,
						    						"period" : period
						    					}
						    			);
									}
								}
								
							}
							else if(Top.Controller.get('mailReadButtonLogic').tempClicked == true) {
								Top.Controller.get('mailWriteLogic').tempYN = true;
								Top.Controller.get('mailWriteLogic').receiverAddr = sender;
								Top.Controller.get('mailWriteLogic').CCAddr = CC;
								Top.Dom.selectById('Text_Subject').setText(ret.dto.SUBJECT);
								setTimeout(function(){Top.Dom.selectById('mailWriteLayout_WorkArea_HtmlEditor').setEditorHTML(ret.dto.CONTENT);});
							}
							else if (Top.Controller.get('mailReadButtonLogic').sendAgainClicked == true || Top.Controller.get('mailThreePanelMainLogic').sendAgainClicked == true){
								Top.Controller.get('mailMainLeftLogic').setTitle('다시 보내기'); 
								Top.Dom.selectById('mailWriteLayout_SwitchPanel').setVisible('visible'); 
								Top.Controller.get('mailWriteLogic').receiverAddr = receiver;							
								Top.Controller.get('mailWriteLogic').CCAddr = CC;
								Top.Dom.selectById('Text_Subject').setText(ret.dto.SUBJECT);
								setTimeout(function(){Top.Dom.selectById('mailWriteLayout_WorkArea_HtmlEditor').setEditorHTML(ret.dto.CONTENT);});
								
								if (ret.dto.Attachment != null && ret.dto.Attachment.length != 0){
//									Top.Dom.selectById('mailWriteLayout_FileBox').setVisible('visible');
									Top.Controller.get('mailWriteLogic').setEditorSize();
									
									for (var i = 0; i < ret.dto.Attachment.length; i++){
										Top.Controller.get('mailWriteLogic').fileAttach.push({
											"file_id" : ret.dto.Attachment[i].FILE_ID,
											"size" : ret.dto.Attachment[i].FILE_SIZE,
											"file_name" : ret.dto.Attachment[i].FILE_NAME,
											"file_extension" : ret.dto.Attachment[i].EXTENSION,
											"file_id" : ret.dto.Attachment[i].FILE_ID,
											"file_path" : ret.dto.Attachment[i].STORAGE_PATH
										});
										Top.Controller.get('mailWriteLogic').tempFileIndex.push("3");
										Top.Controller.get('mailWriteLogic').updateAttachmentInfo(); 

										var size = ret.dto.Attachment[i].FILE_SIZE;
										if ((size*1) < 10485760)
						    				var method = "일반 첨부" ;
						    			else
						    				var method = "대용량 첨부";
						    			
						    			if ( method == "대용량 첨부" )
						    				var period = "~ "+mail.after30Days(null,'/')+ " (30일간)";
						    			else
						    				var period = "제한 없음";
						    			
						    			size = mail.changeSize(size);
						    			
										Top.Controller.get('mailWriteLogic').fileList.push(
						    					{
						    						"name" : ret.dto.Attachment[i].FILE_NAME +"."+ ret.dto.Attachment[i].EXTENSION,
						    						"size" : size,
						    						"attach_method" : method,
						    						"period" : period
						    					}
						    			);
									}
								}
								
							}
							
						if (mailData.panelType == '3' && Top.Controller.get('mailThreePanelMainLogic').modifyClicked == true){
							Top.Controller.get('mailWriteLogic').receiverAddr = receiver;	
							Top.Controller.get('mailWriteLogic').CCAddr = CC;
							Top.Dom.selectById('Text_Subject').setText(ret.dto.SUBJECT);
							setTimeout(function(){Top.Dom.selectById('mailWriteLayout_WorkArea_HtmlEditor').setEditorHTML(ret.dto.CONTENT);});
						}
						
						Top.Dom.selectById('Text_ReceiverAddress').setProperties({'chip-items' : Top.Controller.get('mailWriteLogic').receiverAddr});
						Top.Dom.selectById('Text_Cc').setProperties({'chip-items' : Top.Controller.get('mailWriteLogic').CCAddr});
						Top.Dom.selectById('Text_HiddenCc').setProperties({'chip-items' : Top.Controller.get('mailWriteLogic').BCCAddr});
						
						
						Top.Controller.get('mailReadButtonLogic').replyClicked = null;
						Top.Controller.get('mailReadButtonLogic').replyAllClicked = null;
						Top.Controller.get('mailReadButtonLogic').forwardClicked = null;
						Top.Controller.get('mailReadButtonLogic').tempClicked = null;
						Top.Controller.get('mailReadButtonLogic').sendAgainClicked = null;
						
						Top.Controller.get('mailThreePanelMainLogic').replyClicked = false;
						Top.Controller.get('mailThreePanelMainLogic').replyAllClicked  = false;
						Top.Controller.get('mailThreePanelMainLogic').forwardClicked  = false;
						Top.Controller.get('mailThreePanelMainLogic').sendAgainClicked  = false;
						Top.Controller.get('mailThreePanelMainLogic').modifyClicked = false;
						
						Top.Controller.get('mailThreePanelMainLogic').replyClicked = false;
						Top.Controller.get('mailThreePanelMainLogic').replyAllClicked  = false;
						Top.Controller.get('mailThreePanelMainLogic').forwardClicked  = false;
						Top.Controller.get('mailThreePanelMainLogic').sendAgainClicked  = false;
						Top.Controller.get('mailThreePanelMainLogic').modifyClicked = false;

					}
				});
				
				
		
		},
		
	    set_auto_complete : function(){
		    	var flg;
		    	if(isb2c())
		    		flg = 'b2c';
		    	else
		    		flg = 'b2b';
		    	
			  var data =  {"dto" : {
				    "CMFLG" : "AutoCreateAddress",
				    "MAIL_USER_NAME" : "",
				    "FLAGS" : flg,
				    "DOMAIN_URL" : location.host,
				    "USER_ID" : userManager.getLoginUserId()
				 }};
			  
			  Top.Ajax.execute({
					
					type : "POST",
					url :_workspace.url + "Mail/Mail?action=AutoCreateEMailAddress",
					dataType : "json",
					data	 : JSON.stringify(data),
					contentType: "application/json; charset=utf-8",
					xhrFields: {withCredentials: true},
					crossDomain : true,
					success : function(ret,xhr,status) {
					
						var addr_info_array = [];						
						var retArr = ret.dto.ADDRESS_INFO;
						if(retArr == null) retArr = [];
						
						for(var i=0; i < retArr.length; i++){
							
							addr_info_array.push('' + retArr[i].MAIL_USER_NAME + '<' + retArr[i].EMAIL_ADDRESS + '>');
							
						}
						
						Top.Dom.selectById('Text_ReceiverAddress').setProperties({'auto-complete':addr_info_array.join(','),
								'auto-complete-max-height':'210px',
								'support-empty':'false'});
						Top.Dom.selectById('Text_ReceiverAddress').setProperties({'chip-items' : Top.Controller.get('mailWriteLogic').receiverAddr});
						Top.Dom.selectById('Text_ReceiverAddress').setProperties({'on-keyup':'checkSemicolon', 'on-select' : 'autoCompleteChipItem', 'on-close' : 'closeChipItem', 'on-add':'addReceiver', 'on-blur':'blurAddress'});
						
						Top.Dom.selectById('Text_Cc').setProperties({'auto-complete':addr_info_array.join(','),
							'auto-complete-max-height':'210px',
							  'support-empty':'false'});
						Top.Dom.selectById('Text_Cc').setProperties({'chip-items' : Top.Controller.get('mailWriteLogic').CCAddr});
						Top.Dom.selectById('Text_Cc').setProperties({'on-keyup':'checkSemicolon', 'on-select' : 'autoCompleteChipItem', 'on-close' : 'closeChipItem', 'on-add':'addCC', 'on-blur':'blurAddress'});
						 
											
						Top.Dom.selectById('Text_HiddenCc').setProperties({'auto-complete':addr_info_array.join(','),
							'auto-complete-max-height':'210px',
							  'support-empty':'false'});
						Top.Dom.selectById('Text_HiddenCc').setProperties({'chip-items' : Top.Controller.get('mailWriteLogic').BCCAddr});
						Top.Dom.selectById('Text_HiddenCc').setProperties({'on-keyup':'checkSemicolon', 'on-select' : 'autoCompleteChipItem', 'on-close' : 'closeChipItem', 'on-add':'addHiddenCC', 'on-blur':'blurAddress'});

						
					},
					error : function(ret, xhr,status) {
					
					
					}
					
				});	
			  
			  
		
		 }, checkSemicolon : function(event, widget) {
			 
			 if(event.key === ";") {
					event.target.value = event.target.value.replace(/;$/gi,"")
					event.target.blur();
					event.target.focus();
				}
			 
		 }, blurAddress : function(event, widget) {
			 
			 this.blured = true;
			if(event.target.value == undefined || event.target.value == "")
				return ;
			Top.Controller.get('mailWriteLogic').isLeaved = true;
			var value = {"text": event.target.value}
			widget.addChip(value);

			 
		 }, addReceiver : function(event, widget) {
			 
			 /*var detail; 
		     if (Top.Controller.get('mailWriteLogic').isAutoComplete == true){

		    	 if (event == undefined)
		    		  return; // 칩위젯 오류로 인한 임시조치 88 버전에서 해결가능
		    	  
				  var i = Top.Controller.get('mailWriteLogic').receiverAddr.length; 
		    	  detail ={"text": Top.Controller.get('mailWriteLogic').receiverAddr[i-1].text}; 
		    	  Top.Controller.get('mailWriteLogic').isAutoComplete = null; 
		    	  Top.Controller.get('mailWriteLogic').receiverAddr[i-1] = detail;
		    	  
		     }
		     else if (Top.Controller.get('mailWriteLogic').isLeaved == true){
		    	  Top.Controller.get('mailWriteLogic').isLeaved = null;
		     }
		     else{
		    	  detail = {"text": event.currentTarget.value}; 
		    	  Top.Controller.get('mailWriteLogic').isAutoComplete = null; 
		      }
		    */
			 
		 }, addCC : function(event, widget) {
			 
			 /* var detail; 
		      if (Top.Controller.get('mailWriteLogic').isAutoComplete == true){

		    	  if (event == undefined)
		    		  return; 
		    	  
				  var i = Top.Controller.get('mailWriteLogic').CCAddr.length; 
		    	  detail ={"text": Top.Controller.get('mailWriteLogic').CCAddr[i-1].text}; 
		    	  Top.Controller.get('mailWriteLogic').isAutoComplete = null; 
				  Top.Controller.get('mailWriteLogic').CCAddr[i-1] = detail;
		      } else if (Top.Controller.get('mailWriteLogic').isLeaved == true){
		    	  Top.Controller.get('mailWriteLogic').isLeaved == null;
		      }
		      else{
		    	  detail = {"text": event.currentTarget.value};
		    	  Top.Controller.get('mailWriteLogic').isAutoComplete = null;				      
		      }
			 */
		 }, addHiddenCC : function(event, widget) {
			 
			 /*var detail; 
		      if (Top.Controller.get('mailWriteLogic').isAutoComplete == true){

		    	  if (event == undefined)
		    		  return; 
		    	  
				  var i = Top.Controller.get('mailWriteLogic').BCCAddr.length; 
		    	  detail ={"text": Top.Controller.get('mailWriteLogic').BCCAddr[i-1].text}; 
		    	  Top.Controller.get('mailWriteLogic').isAutoComplete = null; 
		    	  Top.Controller.get('mailWriteLogic').BCCAddr[i-1] = detail;
		      } else if (Top.Controller.get('mailWriteLogic').isLeaved == true){
		    	  Top.Controller.get('mailWriteLogic').isLeaved = null;
		      }
		      else{
		    	  detail = {"text": event.currentTarget.value}; 
		    	  Top.Controller.get('mailWriteLogic').isAutoComplete = null; 	    	  
		      }
		  */
			 
		 }, 
		 saveTemp : function() {
			 
				var mailSubject = Top.Dom.selectById('Text_Subject').getText();
				if (!mailSubject){
					mailSubject = '제목 없음';
				}
					
				var ReceiverArr = [];
				for (var i = 0; i < Top.Controller.get('mailWriteLogic').receiverAddr.length; i++){	
					if (Top.Controller.get('mailWriteLogic').receiverAddr[i].text == "")
						Top.Controller.get('mailWriteLogic').receiverAddr[i].splice(i,1);
					ReceiverArr[i]= { "TO_ADDR" : mail.getEMailFromChip(this.receiverAddr[i].text)};
					}
				if (Top.Controller.get('mailWriteLogic').receiverAddr.length == 0)
					ReceiverArr[i]= { "TO_ADDR" : ""};
				
				var CcArr=[]; 
				for (var i = 0; i < Top.Controller.get('mailWriteLogic').CCAddr.length; i++){	
					if (Top.Controller.get('mailWriteLogic').CCAddr[i].text == "")
						Top.Controller.get('mailWriteLogic').CCAddr[i].splice(i,1);
					CcArr[i]= { "TO_ADDR" : mail.getEMailFromChip(this.CCAddr[i].text)};
					}
				if (Top.Controller.get('mailWriteLogic').CCAddr.length == 0)
					CcArr[i]= { "TO_ADDR" : ""};
				
				
				var HiddenCcArr = []; 
				for (var i = 0; i < Top.Controller.get('mailWriteLogic').BCCAddr.length; i++){	
					if (Top.Controller.get('mailWriteLogic').BCCAddr[i].text == "")
						Top.Controller.get('mailWriteLogic').BCCAddr[i].splice(i,1);
					HiddenCcArr[i]= { "TO_ADDR" : mail.getEMailFromChip(this.BCCAddr[i].text)};
					}
				if (Top.Controller.get('mailWriteLogic').BCCAddr.length == 0)
					HiddenCcArr[i]= { "TO_ADDR" : ""};
				
				var importance;
				if (Top.Dom.selectById('CheckBox_Importance').isChecked() == true)
					importance = 'IMP0001';
				else
					importance = 'IMP0002';
				
				var spaceremove = Top.Dom.selectById('mailWriteLayout_WorkArea_HtmlEditor').getValue();//mailSunEditor.getContents();
		        spaceremove = [].filter.call(spaceremove, function (c) {return c.charCodeAt() !== 8203;}).join('');
		        var context = spaceremove;
		        context = '<style> table { width: 100%; border-spacing : 0px } td, th { border: 1px solid #e1e1e1 } </style>' + context;
		        
		        var senderAddr = Top.Dom.selectById('mailWriteLayout_Info_Sender_Account').getProperties('selectedText');


				var data={
						"dto":{
								"MailSendTo" 				: ReceiverArr,
						        "MailSendCc" 				: CcArr,
						        "MailSendBcc" 				: HiddenCcArr,
						        "ACCOUNT_ID"				: Top.Dom.selectById('mailWriteLayout_Info_Sender_Account').getValue(),
						        "MAIL_ID"					: this.tempMailId,
						        "MAIL_GROUP_INDEX"			: this.tempGroupIndex,
						        "CMFLG" 					: "CreateTempMail",
						        "FOLDER_ID"					: "",
						        "SUBJECT" 					: mailSubject,
						        "SENDER_NAME" 				: "",
						        "HAS_ATTACHMENT_YN_CODE" 	: 'COM0002',
						        "SEEN_YN_CODE" 				: "",
						        "IMPORTANCE_YN_CODE" 		: "",
						        "PINNED_YN_CODE" 			: "",
						        "FILE_NAME" 				: "",
						        "WRITING_MAIL_ID" 			: "",
						        "SYNC_STATE" 				: "",
						        "SOURCE_MAIL_ID" 			: "",
						        "FILE_SIZE" 				: "",
						        "DISPOSITION_NOTI" 			: "",
						        "MESSAGE_ID" 				: "",
						        "X_TNEF" 					: "",
						        "SPAM_YN_CODE" 				: "",
						        "SENDER_ADDR" 				: senderAddr,
						        "CONTENT" 					: context,
						        "USER_ID" 					: userManager.getLoginUserId(),
						        "WS_ID" 					: workspaceManager.getMySpaceId(),
						        "MESSAGE_HEADER" 			: "",
						        "EXTRA_INFOS" 				: "",
						        "FIRST_SAW_TIME" 			: "", 
						        "SEND_IMPORTANCE_CODE" 		: importance,
						        
					    }
				};
					

					
					
				Top.Ajax.execute({
						
						type : "POST",
						url :_workspace.url + "Mail/Mail?action=CreateTemp",
						dataType : "json",
						data	 : JSON.stringify(data),
						contentType: "application/json; charset=utf-8",
						xhrFields: {withCredentials: true},
						crossDomain : true,
						success : function(ret,xhr,status) {
						
							if(ret.dto.MAIL_ID != null && ret.dto.GROUP_INDEX != null) {
								Top.Controller.get('mailWriteLogic').tempMailId = ret.dto.MAIL_ID;
								Top.Controller.get('mailWriteLogic').tempGroupIndex = ret.dto.GROUP_INDEX;
							}
							Top.Controller.get('mailMainLeftLogic').resetTempCount();
							
							var time = mail.tempTime();
							
							if (mailData.panelType == '2'){
								if (Top.App.isWidgetAttached("mailWriteLayout_TempTime") == true){
									Top.Dom.selectById('mailWriteLayout_TempTime').setText('임시 보관함에 저장하였습니다. ' + time);
								}
							}
							else{
								if (Top.App.isWidgetAttached("mailWriteLayout_TempTime") == true){
									Top.Dom.selectById('mailWriteLayout_TempTime').setText('임시 보관함에 저장하였습니다. ' + time);
								}
								
							}
							
							var media = window.matchMedia("screen and (max-device-width: 1024px)");
							
							
						},
						complete : function(ret,xhr,status) {
						
					
							
						},
						error : function(ret, xhr,status) {
						}
						
					});
					 
				 
			 },
			 loadTempMail : function (event, widget) {
				 var callMailUrl = _workspace.url + "Mail/Mail?action=Read&CMFLG=ReadMail&MAIL_ID=" + Top.Controller.get('mailMainLeftLogic').clickedMail + "&USER_ID="+userManager.getLoginUserId();
					Top.Ajax.execute({ 
						
						type : "GET",
						url : callMailUrl,
						dataType : "json",
						crossDomain: true,
						contentType: "application/json",
						headers: {
				                "ProObjectWebFileTransfer":"true"
				            },
					    xhrFields: {withCredentials: true},
						success : function(ret,xhr,status) {
							let subject = ret.dto.SUBJECT;
							
							Top.Dom.selectById('Text_Subject').setText(subject);
							let recAddress = ret.dto.ReceiverList;
							if(recAddress != null){
								for(var i=0; i< recAddress.length; i++){
									if(recAddress[i].EMAIL_ADDRESS != null){
										var map = { 'text' : recAddress[i].EMAIL_ADDRESS };
										Top.Controller.get('mailWriteLogic').receiverAddr.push(map);
									}
								}
								Top.Dom.selectById('Text_ReceiverAddress').setProperties({'chip-items' : Top.Controller.get('mailWriteLogic').receiverAddr});
							}
							let ccAddress = ret.dto.CClist;
							if(ccAddress != null){
								for(var i=0; i< ccAddress.length; i++){
									if(ccAddress[i].EMAIL_ADDRESS != null){
										var map = { 'text' : ccAddress[i].EMAIL_ADDRESS };
										Top.Controller.get('mailWriteLogic').CCAddr.push(map);
									}
								}
								Top.Dom.selectById('Text_Cc').setProperties({'chip-items' : Top.Controller.get('mailWriteLogic').CCAddr});
							}
							let bccAddress = ret.dto.BCCList;
							if(bccAddress != null){
								for(var i=0; i< bccAddress.length; i++){
									if(bccAddress[i].EMAIL_ADDRESS != null){
										var map = { 'text' : bccAddress[i].EMAIL_ADDRESS };
										Top.Controller.get('mailWriteLogic').BCCAddr.push(map);
									}
								}		
								Top.Dom.selectById('Text_HiddenCc').setProperties({'chip-items' : Top.Controller.get('mailWriteLogic').BCCAddr});	
							} 
							let content = ret.dto.CONTENT;
							Top.Dom.selectById('mailWriteLayout_WorkArea_HtmlEditor').setEditorHTML(content);		
							
							let chk_important = ret.dto.SEND_IMPORTANCE_CODE;
							if(chk_important == "IMP0001")
								Top.Dom.selectById('CheckBox_Importance').setProperties({'checked':'true'});
							
						}, error :function(request, status, error) {

			            }
			        });
				 
			 },
			 deleteAttach : function (fileIndex) {
				 
				var k = fileIndex;
				
				if(Top.Controller.get('mailWriteLogic').fileAttach.length == 1){
					Top.Controller.get('mailWriteLogic').fileAttach = [];
					Top.Controller.get('mailWriteLogic').fileList = [];
					Top.Controller.get('mailWriteLogic').tempFileIndex = [];
					Top.Controller.get('mailWriteLogic').updateAttachmentInfo(); 
					Top.Dom.selectById('mailReadLayout_Attach_Foldable_Icon').setVisible('none');

				}
				else{
					if(Top.Controller.get('mailWriteLogic').tempFileIndex[k] == "1"){
						for(var i = 0 ; i < Top.Controller.get("mailWriteLogic").tempTdrivefile.length;i++ ){
							if (Top.Controller.get("mailWriteLogic").tempTdrivefile[i].file_id == Top.Controller.get('mailWriteLogic').fileAttach[k].file_id ){
								Top.Controller.get("mailWriteLogic").tempTdrivefile.splice(i,1);
							}
						}
						
					}
					Top.Controller.get('mailWriteLogic').fileAttach.splice(k,1);
					Top.Controller.get('mailWriteLogic').fileList.splice(k,1);
					Top.Controller.get('mailWriteLogic').tempFileIndex.splice(k,1);
					Top.Controller.get('mailWriteLogic').updateAttachmentInfo(); 
				}
				
				 
			 },
			 openTUser : function(event, widget){
				 if (widget.id == 'mailWriteLayout_Receiver_TUser')
					 Top.Controller.get('mailWriteLogic').clickedArea = 'Receiver';
				 else if (widget.id == 'mailWriteLayout_CC_TUser')
					 Top.Controller.get('mailWriteLogic').clickedArea = 'CC';
				 else if (widget.id == 'mailWriteLayout_HiddenCC_TUser')
					 Top.Controller.get('mailWriteLogic').clickedArea = 'HiddenCC'; 
				 
				 Top.Dom.selectById('mailTUserDialog').open(); 
			 },
			 autoCompleteChipItem : function(event, widget) {
				 
				 if(event.target.nodeName == 'INPUT') return;
				 if(this.blured) {
					 widget.removeLastChip();
					 this.blured = false;
				 }
				 Top.Controller.get('mailWriteLogic').isAutoComplete = true;
				 
			 },
			 closeChipItem : function (event, widget, data){
				 
			/*	 if (widget.id == 'Text_ReceiverAddress'){
					 Top.Controller.get("mailWriteLogic").recKey.pop(data.text);
				 }
				 else if  (widget.id == 'Text_Cc'){
					 Top.Controller.get("mailWriteLogic").ccKey.pop(data.text);
				 }
				 else if (widget.id == 'Text_HiddenCc'){
					 Top.Controller.get("mailWriteLogic").bccKey.pop(data.text);
				 }*/

			 },
			 setPrivateAccount : function(){
				 
				 var accountArr = [];
				 				 
				 if(mailData.accountList.length != 0) {
					 for(var i=1; i<mailData.accountList.length; i++) {
						 accountArr.push({
							 'text' : mailData.accountList[i].EMAIL_ADDRESS,
							 'value' : mailData.accountList[i].ACCOUNT_ID
						 });
					 }

					 Top.Dom.selectById('mailWriteLayout_Info_Sender_Account').setProperties({'nodes' : accountArr})
	 				 Top.Dom.selectById('mailWriteLayout_Info_Sender_Account').setProperties({'selectedIndex' : '0'})
					 
				 } else {
					 var data={
								dto: {
									"USER_ID"	: userManager.getLoginUserId()
								}
						};
					Top.Ajax.execute({
						
						type : "GET",
						url : _workspace.url + "Mail/Mail?action=GetAccountIdBySeq",
						data	 : JSON.stringify(data),
						dataType : "json",
						crossDomain: true,
						contentType: "application/json",
					    xhrFields: {withCredentials: true},
						success : function(ret,xhr,status) {
							
							for(var i=0; i<ret.dto.ACCOUNT_ID.length; i++) {
								accountArr.push({
									'text' : ret.dto.ACCOUNT_ID[i].EMAIL_ADDRESS,
									'value' : ret.dto.ACCOUNT_ID[i].ACCOUNT_ID
								});
								
							}
							
							Top.Dom.selectById('mailWriteLayout_Info_Sender_Account').setProperties({'nodes' : accountArr})
			 				Top.Dom.selectById('mailWriteLayout_Info_Sender_Account').setProperties({'selectedIndex' : '0'})
						}
					});
				 }
				 
			 },
			 moveToThreePanel : function() {
				 
				 mailData.panelType = '3';
				 
				 var account;
				 if(workspaceManager.getWorkspace(workspaceManager.getMySpaceUrl()).WS_TYPE == 'WKS0001')
						account = Top.Dom.selectById('mailWriteLayout_Info_Sender_Account').getProperties('selectedText');
				 else
						account = Top.Dom.selectById('mailMainLayout_Mail_Text1').getText();

				 var spaceremove = Top.Dom.selectById('mailWriteLayout_WorkArea_HtmlEditor').getValue();//mailSunEditor.getContents();
				 spaceremove = [].filter.call(spaceremove, function (c) {return c.charCodeAt() !== 8203;}).join('');
				 var context = spaceremove;
				 context = '<style> table { width: 100%; border-spacing : 0px } td, th { border: 1px solid #e1e1e1 } </style>' + context;
				 
				 var mailContent = {
						 'title' : Top.Dom.selectById('mailMainLayout_FolderName').getText(), 
						 'account' : account, 
						 'receiver'	: Top.Dom.selectById('Text_ReceiverAddress').getProperties('chip-items'), 
						 'cc' : Top.Dom.selectById('Text_Cc').getProperties('chip-items'), 
						 'bcc' : Top.Dom.selectById('Text_HiddenCc').getProperties('chip-items'), 
						 'subject' : Top.Dom.selectById('Text_Subject').getText(), 
						 'attachment' : Top.Controller.get('mailWriteLogic').fileAttach,
						 'attachment_list' : Top.Controller.get('mailWriteLogic').fileList,
						 'attachment_temp' : Top.Controller.get('mailWriteLogic').tempFileIndex,
						 'content' : context 
				 }
				 
				 Top.Controller.get('mailThreePanelMainLogic').replyClicked = false;
				 Top.Controller.get('mailThreePanelMainLogic').replyAllClicked  = false;
				 Top.Controller.get('mailThreePanelMainLogic').forwardClicked  = false;
				 Top.Controller.get('mailThreePanelMainLogic').sendAgainClicked  = false;
			
				 if (mailContent.title == "답장")
					 Top.Controller.get('mailThreePanelMainLogic').replyClicked = true;
				 else if (mailContent.title == '전체 답장')
					 Top.Controller.get('mailThreePanelMainLogic').replyAllClicked  = true;
				 else if (mailContent.title == '전달')
					 Top.Controller.get('mailThreePanelMainLogic').forwardClicked  = true;
				 else if (mailContent.title == '다시 보내기')
					 Top.Controller.get('mailThreePanelMainLogic').sendAgainClicked  = true;

				 
				 Top.Controller.get('mailWriteLogic').mailContent = mailContent;
				 Top.Controller.get('mailWriteLogic').goToThreePanel = true;
				 
			
				 Top.Dom.selectById('mailMainLayout_Center_All').src('mailThreePanelMainLayout.html' + verCsp());
	
				 
			 },
			 callMailContentsFromTwoPanel : function(){
				 var tmp = Top.Controller.get('mailWriteLogic').mailContent; 
				 Top.Controller.get('mailWriteLogic').receiverAddr = tmp.receiver; 
				 Top.Dom.selectById('Text_ReceiverAddress').setProperties({'chip-items' : Top.Controller.get('mailWriteLogic').receiverAddr}); 
				 Top.Controller.get('mailWriteLogic').CCAddr = tmp.cc; 
				 Top.Dom.selectById('Text_Cc').setProperties({'chip-items' : Top.Controller.get('mailWriteLogic').CCAddr}); 
				 Top.Controller.get('mailWriteLogic').BCCAddr = tmp.bcc; 
				 Top.Dom.selectById('Text_HiddenCc').setProperties({'chip-items' : Top.Controller.get('mailWriteLogic').BCCAddr});
				 Top.Dom.selectById('Text_Subject').setText(tmp.subject);
				 setTimeout(function(){Top.Dom.selectById('mailWriteLayout_WorkArea_HtmlEditor').setEditorHTML(tmp.content);});
				 Top.Controller.get('mailWriteLogic').fileAttach = tmp.attachment;
				 Top.Controller.get('mailWriteLogic').fileList = tmp.attachment_list; 
				 Top.Controller.get('mailWriteLogic').tempFileIndex = tmp.attachment_temp; 
				 
				 if( tmp.attachment.length > 0 ){
//					 Top.Dom.selectById('mailWriteLayout_FileBox').setVisible('visible');
					 this.setEditorSize();
					 
				 }
				 

				 Top.Controller.get('mailWriteLogic').goToThreePanel = false ;
				
			 },
			 foldBCC : function(event, widget){
				 
				 if (Top.Dom.selectById('mailWriteLayout_Info_BCC').getProperties('visible') == 'none'){
					 Top.Dom.selectById('mailWriteLayout_Info_CC').setVisible('visible');
					 Top.Dom.selectById('mailWriteLayout_Info_BCC').setVisible('visible');
					 widget.setProperties({'class' : 'icon-arrow_filled_up'});

				 }
				 else{
					 Top.Dom.selectById('mailWriteLayout_Info_CC').setVisible('none');
					 Top.Dom.selectById('mailWriteLayout_Info_BCC').setVisible('none');
					 widget.setProperties({'class' : 'icon-arrow_filled_down'});
				 }

				 
			 },
			 updateAttachmentInfo : function(){
				 
				 	var normalCount=0;
					var normalSize=0;
					var bigCount=0;
					var bigSize=0;
					
					for (var i = 0; i < Top.Controller.get('mailWriteLogic').fileAttach.length; i++ )
						{
						if (Top.Controller.get('mailWriteLogic').tempFileIndex[i] == "0"){
							var size = Top.Controller.get('mailWriteLogic').fileAttach[i].file[0].size;
						}
						else
							var size = Top.Controller.get('mailWriteLogic').fileAttach[i].size; 
						
						if (size*1 < 10485760){
							normalCount = normalCount +1;
							normalSize = normalSize + (size*1);
							}
						else{
							bigCount = bigCount +1;
							bigSize = bigSize + (size*1);
						}		
						}
					
					normalSize = mail.changeSize(normalSize);
					bigSize = mail.changeSize(bigSize);
					
					var info = "일반 " + normalSize + " / 10MB ㅣ 대용량	 " + bigSize + " / 2.00GB x 10개" ;
					Top.Dom.selectById('mailWriteLayout_Attachment_Info').setText(info);
					
			 },
			 manageByte : function(event, widget){
				 
				 if (widget.getText() == undefined || widget.getText().length == 0)
					 return; 
				 
				 var calByte = {
							getByteLength : function(s) {

								if (s == null || s.length == 0) {
									return 0;
								}
								var size = 0;

								for ( var i = 0; i < s.length; i++) {
									size += this.charByteSize(s.charAt(i));
								}

								return size;
							},
								
							cutByteLength : function(s, len) {

								if (s == null || s.length == 0) {
									return 0;
								}
								var size = 0;
								var rIndex = s.length;

								for ( var i = 0; i < s.length; i++) {
									size += this.charByteSize(s.charAt(i));
									if( size == len ) {
										rIndex = i + 1;
										break;
									} else if( size > len ) {
										rIndex = i;
										break;
									}
								}

								return s.substring(0, rIndex);
							},

							charByteSize : function(ch) {

								if (ch == null || ch.length == 0) {
									return 0;
								}

								var charCode = ch.charCodeAt(0);

								if (charCode <= 0x00007F) {
									return 1;
								} else if (charCode <= 0x0007FF) {
									return 2;
								} else if (charCode <= 0x00FFFF) {
									return 3;
								} else {
									return 4;
								}
							}
						};
				 
				  var result = calByte.cutByteLength(widget.getText(), 250);
				  widget.setText(result);
			 },
			 
	drawAttachedFile : function(targetFile){
		
		var fileBox = document.createElement('div');
		fileBox.classList.add('mail-write-file-box');
		
		var fileIcon = document.createElement('img');
		fileIcon.classList.add('mail-write-file-icon');
		fileIcon.setAttribute('src', fileExtension.getInfo(targetFile.extension).iconImage);
		
		var fileInfo = document.createElement('div');
		fileInfo.classList.add('mail-write-file-info');
		
		var fileName = document.createElement('span');
		fileName.classList.add('mail-write-file-name');
		fileName.textContent = targetFile.name;
		
		var fileSize = document.createElement('span');
		fileSize.classList.add('mail-write-file-size');
		fileSize.textContent = targetFile.size;
		
		var fileDelete = document.createElement('div');
		fileDelete.classList.add('icon-close_window');
		fileDelete.classList.add('mail-write-file-delete');
		
		fileDelete.addEventListener('click', function(e){
			var fileIndex = $(e.currentTarget.parentElement).index();
			Top.Controller.get('mailWriteLogic').deleteAttach(fileIndex);
			e.currentTarget.parentElement.remove();
		});
		
		fileInfo.appendChild(fileName);
		fileInfo.appendChild(fileSize);
		
		fileBox.appendChild(fileIcon);
		fileBox.appendChild(fileInfo);
		fileBox.appendChild(fileDelete);
		
		$('div#mailWriteLayout_AttachFiles').append(fileBox);

	},
	foldAttachments : function(event, widget){

		var container = Top.Dom.selectById('mailWriteLayout_AttachFiles');
		
		if(container.getProperties('visible') == 'none'){
			container.setVisible('visible');
			widget.setProperties({'class' : 'icon-arrow_filled_up'});
		}else{
			container.setVisible('none');
			widget.setProperties({'class' : 'icon-arrow_filled_down'});
		}
	},
	cancelMail:function(){
		
		TeeAlarm.open({'title' :'편지를 저장하지 않고 나가시겠습니까?', 'content' :'수정 사항이 저장되지 않았습니다.',	
			buttons : [{'text' : '나가기', 'onClicked' : function(){ 
				history.back();
				TeeAlarm.close();
				}}]})
	}
	
	
});


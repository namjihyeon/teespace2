
Top.Controller.create('mailSettingsMainLayoutLogic', {
	init : function(event, widget) {
		this.clickExternalAccountManagement(MouseEvent, Top.Dom.selectById('externalAccount'));
		
		Top.Dom.selectById('mailSettingsMainLayout_Basic_Setting_Button').setProperties({'on-click':'clickBasicSetting'});
		var media = window.matchMedia("screen and (max-device-width: 1024px)");
		
		if(media.matches) {
			$('div#mailMainLayout_Left').removeClass('mobile-mail-visible-panel');
			$('div#mailMainLayout_Left').addClass('mobile-mail-invisible-panel');
			$('div#mailMainLayout_Center').removeClass('mobile-mail-invisible-panel');
			$('div#mailMainLayout_Center').addClass('mobile-mail-visible-panel');
		}
		
		Top.Dom.selectById('externalAccount').setProperties({'on-click':'clickExternalAccountManagement'});
		Top.Dom.selectById('autoClassify').setProperties({'on-click':'clickAutomaticClassification'});
		Top.Dom.selectById('spamSettings').setProperties({'on-click':'clickSpamMailSettings'});
		
		
	}, showFolderList : function() {
		
		$('div#mailMainLayout_Left').removeClass('mobile-mail-invisible-panel');
		$('div#mailMainLayout_Left').addClass('mobile-mail-visible-panel');
		$('div#mailMainLayout_Center').removeClass('mobile-mail-visible-panel');
		
	}, clickBasicSetting : function(event, widget) {
		this.setButtonsColor(widget);
        Top.Dom.selectById('mailSettingsMainLayout_Bottom_Layout').src('mailBasicSettingLayout.html' + verCsp());
	},
    clickExternalAccountManagement : function (event, widget) {
    	this.setButtonsColor(widget);
        Top.Dom.selectById('mailSettingsMainLayout_Bottom_Layout').src('mailExternalAccountManagementLayout.html' + verCsp());
    },
    clickAutomaticClassification : function (event, widget) {
    	this.setButtonsColor(widget);
    	Top.Dom.selectById('mailSettingsMainLayout_Bottom_Layout').src('mailAutoClassifyLayout.html' + verCsp());
    },
    clickSpamMailSettings : function (event, widget) {
    	this.setButtonsColor(widget);
    	Top.Dom.selectById('mailSettingsMainLayout_Bottom_Layout').src('mailSettingsSpamMasterLayout.html' + verCsp());
    },
    setButtonsColor : function (target) {
    	var arrayButtons = ["externalAccount",
    						"autoClassify",
    						"spamSettings"];
    	
    	for ( var i = 0; i < arrayButtons.length; i++ ) 
    		Top.Dom.selectById(arrayButtons[i]).setProperties((target.id == arrayButtons[i]) ? {'border-color':'#6C56E5','textColor':'#523DC7'}:
    																						   {'border-color':'#FFFFFF','textColor':'#828282'});
    },
    makeOuterAccountLayout : function(data){
    	
    	var outLayout = Top.Widget.create('top-linearlayout');
    	outLayout.setProperties({'id':'outLayout', 'layout-height':'154', 'vertical-alignment':'middle', 'layout-width':'match_parent','orientation':'vertical', 'border-width':'1px',
			'layout-vertical-alignment':'MIDDLE'});
		
    	
		var serviceLayout = Top.Widget.create('top-linearlayout');
		serviceLayout.setProperties({'id':'serviceLayout', 'layout-height':'35', 'layout-width':'wrap_content', 'orientation':'horizontal', 'border-width':'0px',
			'layout-horizontal-alignment':'RIGHT'});
		
		var plusIcon = Top.Widget.create('top-icon');
		plusIcon.setProperties({'id':'mailMainLayout_AddFolderIcon', 'class':'icon-work_add', 'text-size':'11px', 'layout-height':'17px', 'layout-width':'17px', 'on-click':'addSubFolder',
			'layout-vertical-alignment':'middle'});
		
		var menuIcon = Top.Widget.create('top-icon');
		menuIcon.setProperties({'id':'mailMainLayout_MenuIcon', 'class':'icon-work_vertical', 'text-size':'11px', 'layout-height':'17px', 'layout-width':'17px', 
			'margin':'0px 46px 0px 0px', 'on-click':'viewSubMenu', 'layout-vertical-alignment':'middle'});
    }
});
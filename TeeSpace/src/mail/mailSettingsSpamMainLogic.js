Top.Controller.create('mailSettingsSpamMainLogic', {
	
	init : function() {
		
		//따로 설정한게 없으면 default 설정
		Top.Dom.selectById('spamAutoClassifyOption_checkBox_3').setChecked(true);
//		Top.Dom.selectById('languagesClassification_radio_1').setChecked(true)
//		Top.Dom.selectById('spamAutoMoveButton_activateSpamAutoMove').setChecked(true)
//		Top.Dom.selectById('spamReport_deleteButton').setChecked(true)
		Top.Dom.selectById('spamProtection_checkBox').setChecked(true);
//		Top.Dom.selectById('hidingMailBox_displayButton').setChecked(true);
//		Top.Dom.selectById('processOutOfDate_displayButton').setChecked(true);
//		Top.Dom.selectById('processImageLink_hidingButton').setChecked(true);
	},
	
	saveChanges:function() {
	},
	resetChanges:function() {
		Top.Controller.get('mailSettingsSpamMasterLogic').clickSpamMain(MouseEvent, Top.Dom.selectById('spamMain'));
	}
});
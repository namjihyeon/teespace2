Top.Controller.create('mailAutoClassifyLogic', {
	
	init : function() {
		
		Top.Dom.selectById('mailAutoClassifyLayout_Button_Add').setProperties({'on-click':'openPopup'});
		Top.Dom.selectById('mailAutoClassifyLayout_Button_Del').setProperties({'on-click':'delRule'});
		
		this.setBoxList();
		this.setList();
		
		Top.Dom.selectById('mailAutoClassifyLayout_Table').setProperties({'on-rowcheck':'buttonStatusChange'});
		Top.Dom.selectById('mailAutoClassifyLayout_CheckAll').setProperties({'on-change':'buttonStatusChange'});
		
		Top.Dom.selectById('mailAutoClassifyLayout_Button_Up').setProperties({'on-click':'changeSequence'});
		Top.Dom.selectById('mailAutoClassifyLayout_Button_Down').setProperties({'on-click':'changeSequence'});
		Top.Dom.selectById('mailAutoClassifyLayout_Button_Top').setProperties({'on-click':'changeSequence'});
		Top.Dom.selectById('mailAutoClassifyLayout_Button_Bottom').setProperties({'on-click':'changeSequence'});
		
		Top.Dom.selectById('mailAutoClassifyLayout_SelectBox_Filter').setProperties({'on-change':'changeFilter'});
		Top.Dom.selectById('mailAutoClassifyLayout_SelectBox_Box').setProperties({'on-change':'changeFilter'});
		
	}, changeFilter : function(event, widget) {
		
		
		
		
	}, changeSequence : function(event, widget) {
		
		var sequence;
		
		switch(widget.id) {
		case 'mailAutoClassifyLayout_Button_Up' :
			sequence = 'UP';
			break;
		case 'mailAutoClassifyLayout_Button_Down' :
			sequence = 'DOWN';
			break;
		case 'mailAutoClassifyLayout_Button_Top' :
			sequence = 'TOP';
			break;
		case 'mailAutoClassifyLayout_Button_Bottom' :
			sequence = 'BOTTOM';
			break;
		}
		
		var dto = {
				dto : {
					"CMFLG" : "MailRuleSequence",
					"RULE_ID" : Top.Dom.selectById('mailAutoClassifyLayout_Table').getCheckedData()[0].RULEMAIN[0].RULE_ID,
					"ACCOUNT_ID" : Top.Dom.selectById('mailAutoClassifyLayout_Table').getCheckedData()[0].RULEMAIN[0].ACCOUNT_ID,
					"USER_ID" : userManager.getLoginUserId(),
					"SEQUENCE" : sequence
				}
		}
		Top.Ajax.execute({
			
			type : "POST",
			url : _workspace.url + "Mail/Mail?action=RuleSequence",
			dataType : "json",
			data	 : JSON.stringify(dto),
			crossDomain: true,
			contentType: "application/json",
		    xhrFields: {withCredentials: true},
			success : function(ret,xhr,status) {
				
				Top.Dom.selectById('mailAutoClassifyLayout_CheckAll').setChecked(false);
				Top.Dom.selectById('mailAutoClassifyLayout_Table').checkAll(false);
				Top.Controller.get('mailAutoClassifyLogic').setList();
			}
		});
		
		
	}, buttonStatusChange : function(event, widget) {
		
		if(widget.id == 'mailAutoClassifyLayout_CheckAll' && widget.isChecked() == true && mailDataRepo.mailClassifyList.length != 0)
			Top.Dom.selectById('mailAutoClassifyLayout_Table').checkAll(true);
		else if(widget.id == 'mailAutoClassifyLayout_CheckAll' && widget.isChecked() == false)
			Top.Dom.selectById('mailAutoClassifyLayout_Table').checkAll(false);
		
		if(Top.Dom.selectById('mailAutoClassifyLayout_Table').getCheckedIndex().length == 0)
			this.buttonDisabled();
		else if(Top.Dom.selectById('mailAutoClassifyLayout_Table').getCheckedIndex().length == 1)
			this.buttonAbled();
		else
			this.buttonDelAbled();
		
	}, delRule : function() {
		
		for(var i=0; i<Top.Dom.selectById('mailAutoClassifyLayout_Table').getCheckedData().length; i++) {
			
			var dto = {
					dto : {
						"CMFLG" : "MailRuleDelete",
						"RULE_ID" : Top.Dom.selectById('mailAutoClassifyLayout_Table').getCheckedData()[i].RULEMAIN[0].RULE_ID
					}
			}
			Top.Ajax.execute({
				
				type : "POST",
				url : _workspace.url + "Mail/Mail?action=RuleDelete",
				dataType : "json",
				data	 : JSON.stringify(dto),
				crossDomain: true,
				contentType: "application/json",
			    xhrFields: {withCredentials: true},
				success : function(ret,xhr,status) {
					
					Top.Dom.selectById('mailAutoClassifyLayout_CheckAll').setChecked(false);
					Top.Dom.selectById('mailAutoClassifyLayout_Table').checkAll(false);
					Top.Controller.get('mailAutoClassifyLogic').setList();
				}
			});
		}
		
	}, modifyStandard : function(event, widget) {
		
		this.modifyYN = 'Y';
		Top.Dom.selectById('mailAddClassifyDialog').open();
		
	}, setList : function() {
		
		
		var dto={
				dto: {
					"CMFLG" : "MailGetRuleList",
					"USER_ID" : userManager.getLoginUserId()
				}
		};
		
		Top.Ajax.execute({
			
			type : "POST",
			url : _workspace.url + "Mail/Mail?action=GetRuleList",
			dataType : "json",
			data	 : JSON.stringify(dto),
			crossDomain: true,
			contentType: "application/json",
		    xhrFields: {withCredentials: true},
			success : function(ret,xhr,status) {
				
				var data = ret.dto.DORULE_MAIN;
				if(data == null) data = [];
				Top.Dom.selectById('mailAutoClassifyLayout_Title_Count').setText(data.length);
				
				for(var i=0; i<data.length; i++) {
					
					data[i].ADDRESSLIST = [];
					
					for(var j=0; j<data[i].RULEMAIN.length; j++) {
						if(data[i].RULEMAIN[j].KEY == 'MRU0001') {
							data[i].ADDR_TYPE = '보낸 사람 주소';
							data[i].ADDRESSLIST.push(data[i].RULEMAIN[j].VALUE);
						} else if(data[i].RULEMAIN[j].KEY == 'MRU0002') {
							data[i].ADDR_TYPE = '받는 사람 주소';
							data[i].ADDRESSLIST.push(data[i].RULEMAIN[j].VALUE);
						} else if(data[i].RULEMAIN[j].KEY == 'MRU0003') {
							data[i].ADDR_TYPE = '참조 주소';
							data[i].ADDRESSLIST.push(data[i].RULEMAIN[j].VALUE);
						} else if(data[i].RULEMAIN[j].KEY == 'MRU0004') {
							data[i].ADDR_TYPE = '받는 사람 또는 참조 주소';
							data[i].ADDRESSLIST.push(data[i].RULEMAIN[j].VALUE);
						} else if(data[i].RULEMAIN[j].KEY == 'MRU0005')
							data[i].SUBJECT = data[i].RULEMAIN[j].VALUE;
						else if(data[i].RULEMAIN[j].KEY == 'MRU0006')
							data[i].CONTENTS = data[i].RULEMAIN[j].VALUE;
						else if(data[i].RULEMAIN[j].KEY == 'MRU0007')
							data[i].ATTACH_YN = 'Y';
						else if(data[i].RULEMAIN[j].KEY == 'MRU0008')
							data[i].REPLY_YN = 'Y';
						else if(data[i].RULEMAIN[j].KEY == 'MRU0009')
							data[i].READ_YN = 'Y';
						else if(data[i].RULEMAIN[j].KEY == 'MRU0010')
							data[i].IMPORTANT_YN = 'Y';
						else if(data[i].RULEMAIN[j].KEY == 'MRU0011')
							data[i].DEL_YN = 'Y';
						else if(data[i].RULEMAIN[j].KEY == 'MRU0012') {
							data[i].MOVE_FOLDER_YN = 'Y';
							data[i].MOVE_FOLDER = data[i].RULEMAIN[j].FOLDER_NAME;
							data[i].MOVE_FOLDER_ID = data[i].RULEMAIN[j].VALUE;							
						}
							
					}
					
					if(data[i].ADDRESSLIST.length == 0) {	//분류 기준 이메일 주소
						data[i].ADDRESS = '';
						data[i].ADDR_YN = 'none';
					} else {
						var mailList = data[i].ADDRESSLIST[0];
						for(var j=1; j<data[i].ADDRESSLIST.length; j++) { 
							mailList += ', ' + data[i].ADDRESSLIST[j];
						}
						data[i].ADDRESS = mailList;
					}
					
					if(data[i].ATTACH_YN != 'Y' && data[i].REPLY_YN != 'Y') //첨부 파일 있는 메일, 내가 쓴 메일에 대한 답장
						data[i].VISIBLE1 = 'none';
					else {
						data[i].VISIBLE1 = 'visible';
						var standard = '';
						if(data[i].ATTACH_YN == 'Y') {
							standard += '첨부 파일이 있는 메일';
							if(data[i].REPLY_YN == 'Y')
								standard += ', ';
						}
						if(data[i].REPLY_YN == 'Y')
							standard += '내가 쓴 메일에 대한 답장';
						data[i].STANDARD = standard;
					}
					
					data[i].TITLE_YN = (data[i].SUBJECT != undefined) ? 'visible' : 'none';
					data[i].CONTENT_YN = (data[i].CONTENTS != undefined) ? 'visible' : 'none';
					
					if(data[i].MOVE_FOLDER_YN != 'Y')	//분류 방법 - 편지함으로 이동
						data[i].VISIBLE2 = 'none';
					else {
						data[i].VISIBLE2 = 'visible';
						data[i].WAY_TYPE = '편지함으로 이동';
					}
					
					if(data[i].READ_YN != 'Y' && data[i].IMPORTANT_YN != 'Y'&& data[i].DEL_YN != 'Y') //읽음으로 표시, 중요 표시, 삭제
						data[i].VISIBLE3 = 'none';
					else {
						data[i].VISIBLE3 = 'visible';
						var way = '';
						if(data[i].READ_YN == 'Y') {
							way += '읽음으로 표시';
							if(data[i].IMPORTANT_YN == 'Y')
								way += ', ';
						}
						if(data[i].IMPORTANT_YN == 'Y') {
							way += '중요 표시';
							if(data[i].DEL_YN == 'Y')
								way += ', ';
						}
						if(data[i].DEL_YN == 'Y')
							way += '삭제';
						data[i].WAY = way;
					}
					
				}
				
				var columnOpt = { 
						 "1" : {
							event : {
								onCreated : function(index, data, nTd) { 
									var info = ret.dto.DORULE_MAIN[Top.Dom.selectById('mailAutoClassifyLayout_Table').getStartRowNum()+index];
									Top.Dom.selectById('mailAutoClassifyLayout_Sort_Addr_'+index+'_1').setVisible(info.ADDR_YN);
									Top.Dom.selectById('mailAutoClassifyLayout_Sort_Title_'+index+'_1').setVisible(info.TITLE_YN);
									Top.Dom.selectById('mailAutoClassifyLayout_Sort_Content_'+index+'_1').setVisible(info.CONTENT_YN);
									Top.Dom.selectById('mailAutoClassifyLayout_Sort2_'+index+'_1').setVisible(info.VISIBLE1);
								}
							}
						}, "2" : {
							event : {
								onCreated : function(index, data, nTd) {
									var info = ret.dto.DORULE_MAIN[Top.Dom.selectById('mailAutoClassifyLayout_Table').getStartRowNum()+index];
									Top.Dom.selectById('mailAutoClassifyLayout_Way_'+index+'_2').setVisible(info.VISIBLE2);
									Top.Dom.selectById('mailAutoClassifyLayout_Way2_'+index+'_2').setVisible(info.VISIBLE3);
								}
							}
						}
				};

				Top.Dom.selectById('mailAutoClassifyLayout_Table').setProperties({"column-option": columnOpt}); 

				mailDataRepo.setValue('mailClassifyList', data);

			}
		});
		
		
		
	}, openPopup : function() {
		
		this.modifyYN = 'N';
		Top.Dom.selectById('mailAddClassifyDialog').open();
		
	}, setBoxList : function() {
		
		var folders = [];
		

		Top.Ajax.execute({
			
			type : "GET",
			url : _workspace.url + "Mail/Mail?action=BasicInfo&CMFLG=BasicInfo&USER_ID="+userManager.getLoginUserInfo().userId+"&WS_USE_CODE=USE0001",
			dataType : "json",
			crossDomain: true,
			contentType: "application/json",
		    xhrFields: {withCredentials: true},
			success : function(ret,xhr,status) {
				
				for(var i=0; i<ret.dto.AccountList[0].FolderList.length; i++) {
					
					if(ret.dto.AccountList[0].FolderList[i].FOLDER_TYPE != 'MFL0006')
						folders.push({'text':ret.dto.AccountList[0].FolderList[i].DISPLAY_NAME,'value':ret.dto.AccountList[0].FolderList[i]});
					
				}
				
				folders.sort(function(a,b) {
					return a.value.FOLDER_TYPE.substr(6,1) - b.value.FOLDER_TYPE.substr(6,1);
				});
				
				var receiveFolder = folders.splice(0,1);
				var importantFolder = folders.splice(0,1);
				var sentFolder = folders.splice(0,1);
				var tempFolder = folders.splice(0,1);
				var sendmeFolder = folders.splice(0,1);
				var trashFolders = folders.splice(0,1);
				
				folders.forEach(function(data, index) {
					
					switch(data.value.PARENT_FOLDER_TYPE) {
					
					case 'MFL0001' :
						data.text='└ '+data.text
						receiveFolder.push(folders[index]);
						break;
					case 'MFL0002' :
						data.text='└ '+data.text
						importantFolder.push(folders[index]);
						break;
					case 'MFL0003' :
						data.text='└ '+data.text
						sentFolder.push(folders[index]);
						break;
					case 'MFL0004' :
						data.text='└ '+data.text
						tempFolder.push(folders[index]);
						break;
					case 'MFL0005' :
						data.text='└ '+data.text
						sendmeFolder.push(folders[index]);
						break;
					
					}
					
				});
			
				var newFolders = [];
				newFolders = newFolders.concat(receiveFolder);
				newFolders = newFolders.concat(importantFolder);
				newFolders = newFolders.concat(sentFolder);
				newFolders = newFolders.concat(tempFolder);
				newFolders = newFolders.concat(sendmeFolder);
				newFolders = newFolders.concat(trashFolders);
				
				Top.Dom.selectById('mailAutoClassifyLayout_SelectBox_Box').setProperties({'nodes':newFolders});
			}
		});
	}, buttonAbled : function() {
		
		Top.Dom.selectById('mailAutoClassifyLayout_Button_Del').setProperties({'disabled':false});
		Top.Dom.selectById('mailAutoClassifyLayout_Button_Up').setProperties({'disabled':false});
		Top.Dom.selectById('mailAutoClassifyLayout_Button_Down').setProperties({'disabled':false});
		Top.Dom.selectById('mailAutoClassifyLayout_Button_Top').setProperties({'disabled':false});
		Top.Dom.selectById('mailAutoClassifyLayout_Button_Bottom').setProperties({'disabled':false});
		
	}, buttonDelAbled : function() {
		
		Top.Dom.selectById('mailAutoClassifyLayout_Button_Del').setProperties({'disabled':false});
		Top.Dom.selectById('mailAutoClassifyLayout_Button_Up').setProperties({'disabled':true});
		Top.Dom.selectById('mailAutoClassifyLayout_Button_Down').setProperties({'disabled':true});
		Top.Dom.selectById('mailAutoClassifyLayout_Button_Top').setProperties({'disabled':true});
		Top.Dom.selectById('mailAutoClassifyLayout_Button_Bottom').setProperties({'disabled':true});

	}, buttonDisabled : function() {
		
		Top.Dom.selectById('mailAutoClassifyLayout_Button_Del').setProperties({'disabled':true});
		Top.Dom.selectById('mailAutoClassifyLayout_Button_Up').setProperties({'disabled':true});
		Top.Dom.selectById('mailAutoClassifyLayout_Button_Down').setProperties({'disabled':true});
		Top.Dom.selectById('mailAutoClassifyLayout_Button_Top').setProperties({'disabled':true});
		Top.Dom.selectById('mailAutoClassifyLayout_Button_Bottom').setProperties({'disabled':true});

	}

});
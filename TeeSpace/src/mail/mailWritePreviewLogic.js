Top.Controller.create('mailWritePreviewLogic', {
	init : function(event, widget) {
		
		if(this.writeMode == 'full')
			this.setContent();
		else if(this.writeMode == 'popup')
			this.setContentPopup();
		
	}, setContent : function() {
	
		this.showMail();
		this.showAttach();
		Top.Dom.selectById('Preview_Send').setProperties({'on-click':'clickSend'});
		
	}, setContentPopup : function() {
		
		this.showMailPopup();
		this.showAttachPopup();
		Top.Dom.selectById('Preview_Send').setProperties({'on-click':'clickSendPopup'});
		
	}, showMail : function (event, widget) {
		
		var subject = Top.Dom.selectById('Text_Subject').getText();
		if (!subject)
			subject = "제목 없음"; 
		Top.Dom.selectById('Preview_Subject').setText(subject);
		
		 var senderAddr = Top.Dom.selectById('mailWriteLayout_Info_Sender_Account').getProperties('selectedText');
		
		Top.Dom.selectById('Preview_Sender').setText(senderAddr);
		
		var content = Top.Dom.selectById('mailWriteLayout_WorkArea_HtmlEditor').getValue();//mailSunEditor.getContents();
//		content = '<div class="input_editor sun-editor-id-wysiwyg sun-editor-editable">' + content + '</div>';
		Top.Dom.selectById('Preview_Content').setText(content);
		
		
		var receiver = "";
		for(var i = 0; i<Top.Controller.get('mailWriteLogic').receiverAddr.length ; i++){
			if (Top.Controller.get('mailWriteLogic').receiverAddr[i].text == "")
				Top.Controller.get('mailWriteLogic').receiverAddr[i].splice(i,1);
			if (i !=0){
				receiver = receiver + " "
			}
			receiver = receiver + Top.Controller.get('mailWriteLogic').receiverAddr[i].text + ";" 
		}
		receiver = receiver.slice(0,-1); 
		Top.Dom.selectById('Preview_Receiver').setHTML(receiver);
		
		var cc = "";
		for(var i = 0; i<Top.Controller.get('mailWriteLogic').CCAddr.length ; i++){
			if (Top.Controller.get('mailWriteLogic').CCAddr[i].text == "")
				Top.Controller.get('mailWriteLogic').CCAddr[i].splice(i,1);
			if (i !=0){
				cc = cc + " "
			}
			cc = cc + Top.Controller.get('mailWriteLogic').CCAddr[i].text + ";" 
		}
		cc = cc.slice(0,-1); 
		Top.Dom.selectById('Preview_Cc').setHTML(cc);
		
		var hiddenCc = "";
		for(var i = 0; i<Top.Controller.get('mailWriteLogic').BCCAddr.length ; i++){
			if (Top.Controller.get('mailWriteLogic').BCCAddr[i].text == "")
				Top.Controller.get('mailWriteLogic').BCCAddr[i].splice(i,1);
			if (i !=0){
				hiddenCc = hiddenCc + " "
			}
			hiddenCc = hiddenCc + Top.Controller.get('mailWriteLogic').BCCAddr[i].text + ";" 
		}
		hiddenCc = hiddenCc.slice(0,-1); 
		Top.Dom.selectById('Preview_HiddenCc').setHTML(hiddenCc);


	}, showMailPopup : function (event, widget) {
		
		var subject = Top.Dom.selectById('Text_Subject_Popup').getText();
		if (!subject)
			subject = "제목 없음"; 
		Top.Dom.selectById('Preview_Subject').setText(subject);
		
		var senderAddr = Top.Dom.selectById('mailWritePopupLayout_Info_Sender_Account').getProperties('selectedText');
		
		Top.Dom.selectById('Preview_Sender').setText(senderAddr);
		
		var content = mailSunEditor2.getContents();
		content = '<div class="input_editor sun-editor-id-wysiwyg sun-editor-editable">' + content + '</div>';
		Top.Dom.selectById('Preview_Content').setText(content);
		
		
		var receiver = "";
		for(var i = 0; i<Top.Controller.get('mailWritePopupLogic').receiverAddr.length ; i++){
			if (Top.Controller.get('mailWritePopupLogic').receiverAddr[i].text == "")
				Top.Controller.get('mailWritePopupLogic').receiverAddr[i].splice(i,1);
			if (i !=0){
				receiver = receiver + " "
			}
			receiver = receiver + Top.Controller.get('mailWritePopupLogic').receiverAddr[i].text + ";" 
		}
		receiver = receiver.slice(0,-1); 
		Top.Dom.selectById('Preview_Receiver').setHTML(receiver);
		
		var cc = "";
		for(var i = 0; i<Top.Controller.get('mailWritePopupLogic').CCAddr.length ; i++){
			if (Top.Controller.get('mailWritePopupLogic').CCAddr[i].text == "")
				Top.Controller.get('mailWritePopupLogic').CCAddr[i].splice(i,1);
			if (i !=0){
				cc = cc + " "
			}
			cc = cc + Top.Controller.get('mailWritePopupLogic').CCAddr[i].text + ";" 
		}
		cc = cc.slice(0,-1); 
		Top.Dom.selectById('Preview_Cc').setHTML(cc);
		
		var hiddenCc = "";
		for(var i = 0; i<Top.Controller.get('mailWritePopupLogic').BCCAddr.length ; i++){
			if (Top.Controller.get('mailWritePopupLogic').BCCAddr[i].text == "")
				Top.Controller.get('mailWritePopupLogic').BCCAddr[i].splice(i,1);
			if (i !=0){
				hiddenCc = hiddenCc + " "
			}
			hiddenCc = hiddenCc + Top.Controller.get('mailWritePopupLogic').BCCAddr[i].text + ";" 
		}
		hiddenCc = hiddenCc.slice(0,-1); 
		Top.Dom.selectById('Preview_HiddenCc').setHTML(hiddenCc);


	}, clickModify : function () {
		Top.Dom.selectById('mailWritePreviewDialog').close();
	}, clickSend : function () {
		Top.Dom.selectById('mailWritePreviewDialog').close();
		Top.Controller.get('mailWriteLogic').clickSend();
	}, clickSendPopup : function () {
		Top.Dom.selectById('mailWritePreviewDialog').close();
		Top.Controller.get('mailWritePopupLogic').clickSend();
	}, showAttach : function() {
		
		var normalCount=0;
		var normalSize=0;
		var bigCount=0;
		var bigSize=0;
		
		for (var i = 0; i < Top.Controller.get('mailWriteLogic').fileAttach.length; i++ )
			{
			if (Top.Controller.get('mailWriteLogic').tempFileIndex[i] == "0"){
				if ((Top.Controller.get('mailWriteLogic').fileAttach[i].file[0].size)*1 < 10485760){
					normalCount = normalCount +1;
					normalSize = normalSize + ((Top.Controller.get('mailWriteLogic').fileAttach[i].file[0].size)*1);
					}
				else{
					bigCount = bigCount +1;
					bigSize = bigSize + ((Top.Controller.get('mailWriteLogic').fileAttach[i].file[0].size)*1);
				}		
			}
			else {
				if ((Top.Controller.get('mailWriteLogic').fileAttach[i].size)*1 < 10485760){
					normalCount = normalCount +1;
					normalSize = normalSize + ((Top.Controller.get('mailWriteLogic').fileAttach[i].size)*1);
					}
				else{
					bigCount = bigCount +1;
					bigSize = bigSize + ((Top.Controller.get('mailWriteLogic').fileAttach[i].size)*1);
				}		
			}
		}
		
		normalSize = mail.changeSize(normalSize);
		bigSize = mail.changeSize(bigSize);
		
		Top.Dom.selectById('mailWritePreviewLayout_NormalAttach_Count').setText(normalCount+'개');
		Top.Dom.selectById('mailWritePreviewLayout_NormalAttach_Size').setText('('+normalSize+')');
		Top.Dom.selectById('mailWritePreviewLayout_BigAttach_Count').setText(bigCount+'개');
		Top.Dom.selectById('mailWritePreviewLayout_BigAttach_Size').setText('('+bigSize+')');
		
		
		if (normalCount == 0){
			Top.Dom.selectById('mailWritePreviewLayout_NormalAttach').setVisible('none');
			Top.Dom.selectById('mailWritePreviewLayout_Line').setVisible('none');
		}
		

		if (bigCount == 0){
			Top.Dom.selectById('mailWritePreviewLayout_BigAttach').setVisible('none');
			Top.Dom.selectById('mailWritePreviewLayout_Line').setVisible('none');
		}
	
		
	},
	
	showAttachPopup : function() {
		
		var normalCount=0;
		var normalSize=0;
		var bigCount=0;
		var bigSize=0;
		
		for (var i = 0; i < Top.Controller.get('mailWritePopupLogic').fileAttach.length; i++ )
			{
			if (Top.Controller.get('mailWritePopupLogic').tempFileIndex[i] == "0"){
				if ((Top.Controller.get('mailWritePopupLogic').fileAttach[i].file[0].size)*1 < 10485760){
					normalCount = normalCount +1;
					normalSize = normalSize + ((Top.Controller.get('mailWritePopupLogic').fileAttach[i].file[0].size)*1);
					}
				else{
					bigCount = bigCount +1;
					bigSize = bigSize + ((Top.Controller.get('mailWritePopupLogic').fileAttach[i].file[0].size)*1);
				}		
			}
			else {
				if ((Top.Controller.get('mailWritePopupLogic').fileAttach[i].size)*1 < 10485760){
					normalCount = normalCount +1;
					normalSize = normalSize + ((Top.Controller.get('mailWritePopupLogic').fileAttach[i].size)*1);
					}
				else{
					bigCount = bigCount +1;
					bigSize = bigSize + ((Top.Controller.get('mailWritePopupLogic').fileAttach[i].size)*1);
				}		
			}
			
		}
		
		normalSize = mail.changeSize(normalSize);
		bigSize = mail.changeSize(bigSize);
		
		Top.Dom.selectById('mailWritePreviewLayout_NormalAttach_Count').setText(normalCount+'개');
		Top.Dom.selectById('mailWritePreviewLayout_NormalAttach_Size').setText('('+normalSize+')');
		Top.Dom.selectById('mailWritePreviewLayout_BigAttach_Count').setText(bigCount+'개');
		Top.Dom.selectById('mailWritePreviewLayout_BigAttach_Size').setText('('+bigSize+')');
		
		
		if (normalCount == 0){
			Top.Dom.selectById('mailWritePreviewLayout_NormalAttach').setVisible('none');
			Top.Dom.selectById('mailWritePreviewLayout_Line').setVisible('none');
		}
		

		if (bigCount == 0){
			Top.Dom.selectById('mailWritePreviewLayout_BigAttach').setVisible('none');
			Top.Dom.selectById('mailWritePreviewLayout_Line').setVisible('none');
		}
	
		
	}
})

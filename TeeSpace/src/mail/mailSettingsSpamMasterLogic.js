
Top.Controller.create('mailSettingsSpamMasterLogic', {
	init : function(event, widget) {
		this.clickSpamMain(MouseEvent, Top.Dom.selectById('spamMain'));
		
		Top.Dom.selectById('spamMain').setProperties({'on-click':'clickSpamMain'});
		Top.Dom.selectById('spamKeywordBlock').setProperties({'on-click':'clickKeywordBlock'});
		Top.Dom.selectById('spamMailDomainBlock').setProperties({'on-click':'clickMailDomainBlock'});
		
		$("div#mailSettingsSpamMaster_buttonList_layout .top-textview-url").css("vertical-align", "middle");
		$("div#mailSettingsSpamMaster_buttonList_layout .top-textview-root").css("padding", "0rem 0rem 0rem 0.31rem");
		
	},
	
	clickSpamMain : function(event, widget) {
		this.setButtonsColor(widget);
        Top.Dom.selectById('mailSettingsSpamMaster_content_layout').src('mailSettingsSpamMainLayout.html' + verCsp());
	},
	clickKeywordBlock : function (event, widget) {
    	this.setButtonsColor(widget);
        Top.Dom.selectById('mailSettingsSpamMaster_content_layout').src('mailSettingsSpamKeywordBlockLayout.html' + verCsp());
    },
    clickMailDomainBlock : function (event, widget) {
    	this.setButtonsColor(widget);
    	Top.Dom.selectById('mailSettingsSpamMaster_content_layout').src('mailSettingsSpamMailDomainBlockLayout.html' + verCsp());
    },
    setButtonsColor : function (target) {
    	var arrayButtons = ["spamMain",
    						"spamKeywordBlock",
    						"spamMailDomainBlock"];
    	
    	for ( var i = 0; i < arrayButtons.length; i++ ) 
    		Top.Dom.selectById(arrayButtons[i]).setProperties((target.id == arrayButtons[i]) ? {'background-color':'#6C56E5','textColor':'#FFFFFF'}:
    																						   {'background-color':'#FFFFFF','textColor':'#777777'});
    },
    renderBlockList:function(page, blockedList, textField, blockType) {
		Top.Dom.select('.' + page + ' .' + blockType + '_flow')[0].clear();
		
		var addedBlockView = Top.Widget.create('top-textview');
		var addedBlockRmv = Top.Widget.create('top-icon');
		var blockLayout = Top.Widget.create('top-linearlayout');
		
		blockLayout.setProperties({
			'id': 'blockLayout' + blockType, // + i
			'layout-height': '1.88rem',
			'layout-width': 'wrap_content',
			'background-color': '#FFFFFF',
			'border-radius': '1.56rem',
			'margin': '0rem 0.63rem 0.31rem 0rem',
			'padding': '0rem 0.63rem 0rem 0.63rem',
//			'value': blockedList[i],
			'class-name': 'blockentry_chip'
		});
		addedBlockView.setProperties({
			'id': 'blockView' + blockType, // + i
			'layout-vertical-alignment' : 'MIDDLE',
			'text': textField,
			'text-color': "#3B3B3B",
			'text-size': '0.75rem',
			'padding': '0.13rem 0rem 0rem 0rem',
			'class-name': 'block_textview'
		});
		addedBlockRmv.setProperties({
			'id': 'remove' + blockType,
			'layout-vertical-alignment' : 'MIDDLE',
			'text-color': "#75757F",
			'text-size': '0.5rem',
			'class-name': 'removeblock_button',
			'class': 'icon-close mail-cursor-pointer',
			'on-click': 'remove' + blockType
		});
		
		Top.Dom.select('.' + page + ' .' + blockType + '_flow')[0].addWidget(blockLayout);
		Top.Dom.select('.' + page + ' .' + blockType + '_flow')[0].complete();
		
		blockLayout.addWidget(addedBlockView);
		blockLayout.addWidget(addedBlockRmv);
		blockLayout.complete();		
	},
});
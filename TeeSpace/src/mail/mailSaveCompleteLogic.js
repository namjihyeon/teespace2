Top.Controller.create('mailSaveCompleteLogic', {
	init : function() {
		

	},
	clickedSendMeMailBox : function() {
		
		
		var media = window.matchMedia("screen and (max-device-width: 1024px)");
		
		if(media.matches)
			mailData.panelType = '3';
		
		let sub = appManager.getSubApp();
		if(sub) 
			Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + Top.Dom.selectById('mailMainLayout_SentMeMailBox_Text0').getValue().FOLDER_ID + "?sub=" + sub, {eventType:'fold'});
		else 
			Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + Top.Dom.selectById('mailMainLayout_SentMeMailBox_Text0').getValue().FOLDER_ID , {eventType:'close'});
	},
	clickedNewMailToMe : function() {
		let sub = appManager.getSubApp();
		if(sub) 
			Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/newtome' + '?' + new Date().getTime() + "?sub=" + sub, {eventType:'fold'});
		else 
			Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/newtome' + '?' + new Date().getTime() , {eventType:'close'});
		
	}
})

Top.Controller.create('mailBasicSettingLogic', {
	
	init : function() {
		
		this.setCurSetting();
		
		Top.Dom.selectById('mailBasicSettingLayout_Save').setProperties({'on-click':'saveSetting'});
		Top.Dom.selectById('mailBasicSettingLayout_Init').setProperties({'on-click':'initSetting'});
		
	}, setCurSetting : function() {
		
		var widgetId = (mailData.writeMode == 'full') ? 'mailBasicSettingLayout_WriteMode_Full' : 'mailBasicSettingLayout_WriteMode_Popup';
		Top.Dom.selectById(widgetId).setChecked(true);
		
	}, saveSetting : function() {
		
		var writeMode = (Top.Dom.selectById('mailBasicSettingLayout_WriteMode_Full').getChecked()) ? 'full' : 'popup';
		var data={
				dto: {
						"USER_ID" : userManager.getLoginUserId(),
						"MAIL_SEND_TYPE" : writeMode
				
				}
		};

		Top.Ajax.execute({
			
			type : "POST",
			url : _workspace.url + "Mail/Mail?action=UpdateMailSendType",
			dataType : "json",
			data	 : JSON.stringify(data),
			contentType: "application/json",
		    xhrFields: {withCredentials: true},
			crossDomain : true,
			success : function(ret,xhr,status) {
				mailData.writeMode = writeMode;
			}
		});
		
	}, initSetting : function() {
		
		var data={
				dto: {
						"USER_ID" : userManager.getLoginUserId(),
						"MAIL_SEND_TYPE" : 'full'
				
				}
		};

		Top.Ajax.execute({
			
			type : "POST",
			url : _workspace.url + "Mail/Mail?action=UpdateMailSendType",
			dataType : "json",
			data	 : JSON.stringify(data),
			contentType: "application/json",
		    xhrFields: {withCredentials: true},
			crossDomain : true,
			success : function(ret,xhr,status) {
				mailData.writeMode = 'full';
				Top.Dom.selectById('mailBasicSettingLayout_WriteMode_Full').setChecked(true)
			}
		});
		
	}
});
Top.Controller.create('mailSelectMailLogic', {
	
	init : function() {
		Top.Dom.selectById('mailSelectMailLayout_Text').setProperties({'visible':'visible'});
		Top.Dom.selectById('mailSelectMailLayout_CountSelectedMail').setProperties({'visible':'none'});
		Top.Dom.selectById('mailSelectMailLayout_Delete').setProperties({'visible':'none'});
		Top.Dom.selectById('mailSelectMailLayout_Read').setProperties({'visible':'none'});
//		Top.Dom.selectById('mailSelectMailLayout_Move').setProperties({'visible':'none'});
		
		switch(mailData.curBox.FOLDER_TYPE) {
		case 'MFL0006' : 
		case 'MFL0007' : 
			Top.Dom.selectById('mailSelectMailLayout_Delete').setProperties({'text':'영구 삭제'});
		}
	},
	
	checkAll : function() {
		Top.Dom.selectById('mailThreePanelLayout_Receive_All_CheckBox').setChecked(true);
	},
	
	cancelAll : function() {
		Top.Dom.selectById('mailThreePanelLayout_Receive_All_CheckBox').setChecked(false);
	},
	
	deleteMail : function() {
		if(mailData.curBox.FOLDER_TYPE == 'MFL0007')
			Top.Dom.selectById('mailDeletePermanentDialog').open();
		else
			Top.Controller.get('mailThreePanelMainLogic').clickDelete();
	},
	
	setRead : function() {
		if(Top.Dom.selectById('mailThreePanelLayout_Table').getCheckedData().length == 0)
			return;

		var mailIdList = [];
		for(var i=0; i<Top.Dom.selectById('mailThreePanelLayout_Table').getCheckedData().length; i++)
			mailIdList.push(Top.Dom.selectById('mailThreePanelLayout_Table').getCheckedData()[i].MAIL_ID);

		var seenYN;

		if(Top.Dom.selectById('mailThreePanelLayout_Receive_Unread').getText() == '읽음')
			seenYN = 'REA0001';
		else
			seenYN = 'REA0002';

		var data={
				dto: {
					"CMFLG"				: "ChangeMailStatus",
					"dbio_total_count"	: Top.Dom.selectById('mailThreePanelLayout_Table').getCheckedData().length,
					"ACCOUNT_ID"		: mailData.curBox.ACCOUNT_ID,
					"MAIL_MST_TYPE"		: "SEEN_YN_CODE",
					"MAIL_MST_VALUE"	: seenYN,
					"MAIL_ID"			: mailIdList
				}
		};

		Top.Ajax.execute({

			type : "POST",
			url : _workspace.url + "Mail/Mail?action=ChangeStatus",
			dataType : "json",
			data	 : JSON.stringify(data),
			crossDomain: true,
			contentType: "application/json",
			xhrFields: {withCredentials: true},
			success : function(ret,xhr,status) {
				var textColor = (seenYN == 'REA0001') ? '#6A6C71' : '#000000';
				var textStyle = (seenYN == 'REA0001') ? '' : 'bold';
				for(var i = 0; i<Top.Dom.selectById('mailThreePanelLayout_Table').getCheckedData().length; i++) {
					mailDataRepo.mailReceivedBoxDataList[Top.Dom.selectById('mailThreePanelLayout_Table').getCheckedIndex()[i]].TEXT_COLOR = textColor;
					mailDataRepo.mailReceivedBoxDataList[Top.Dom.selectById('mailThreePanelLayout_Table').getCheckedIndex()[i]].TEXT_STYLE = textStyle;
					mailDataRepo.mailReceivedBoxDataList[Top.Dom.selectById('mailThreePanelLayout_Table').getCheckedIndex()[i]].SEEN_YN_CODE = seenYN;
				}
				
				Top.Dom.selectById('mailThreePanelLayout_Table').checkAll(false);
				Top.Controller.get('mailThreePanelMainLogic').checkAllFalse();
				Top.Controller.get('mailThreePanelMainLogic').buttonDisabled(); 
				Top.Controller.get('mailMainLeftLogic').resetCount(); 
			}
		});
	},
});
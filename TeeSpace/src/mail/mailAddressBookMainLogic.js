Top.Controller.create('mailAddressBookMainLogic', {
	init : function() { 
		
		Top.Controller.get('mailMainLeftLogic').isWritingMail = false;
		
		Top.Dom.selectById('mailAddressBookMainLayout_cate_all_TextView').setProperties({'textColor':'#FFFFFF', 'backgroundColor':'#6C56E5'});
		
		var user_id = userManager.getLoginUserId();
		
		mailAddressBookDataRepo.mailAddressBookListData = [];
		this.getMailAddressBookDataList();
		this.changeAddressBook;
		this.tempAddressBookId;
		this.deleteFromOne;
		this.numData;
		this.mailData;
		
		Top.Dom.selectById('mailAddressBookMainLayout_cate_all_TextView').setProperties({'on-click':'searchByCate'});
		for (var i = 1 ; i< 15 ; i++){
			Top.Dom.selectById('mailAddressBookMainLayout_cate_Korea_TextView_'+ i).setProperties({'on-click':'searchByCate'});
		}
		
		Top.Dom.selectById('mailAddressBookMainLayout_cate_English_all_Textview').setProperties({'on-click':'searchByCate'});
		Top.Dom.selectById('mailAddressBookMainLayout_cate_number_all_TextView').setProperties({'on-click':'searchByCate'});
		
		Top.Dom.selectById('mailAddressBookMainLayout_addAddressBook_Button').setProperties({'on-click':'clickAdd'});
		
		Top.Dom.selectById('mailAddressBookMainLayout_delete_Button').setProperties({'disabled':true, 'on-click':'clickDelete'}); 
		
		Top.Dom.selectById('mailAddressBookMainLayout_Table').setProperties({'on-rowcheck':'buttonStatusChange'});
		Top.Dom.selectById('mailAddressBookMainLayout_Table').setProperties({'on-pagechange':'initCheckBoxStatus'})
		Top.Dom.selectById('mailAddressBookMainLayout_Table').setProperties({'on-lengthchange':'savePageLength'})
		Top.Dom.selectById('mailAddressBookMainLayout_selectAll_CheckBox').setProperties({'on-click':'buttonStatusChange'});
		
		
		Top.Dom.selectById('mailAddressBookMainLayout_search_TextField').setProperties({'icon':'icon-work_search'});
		Top.Dom.selectById('mailAddressBookMainLayout_search_TextField').setProperties({'on-keypress':'getEnter'});
		Top.Dom.selectById('mailAddressBookMainLayout_search_TextField').setProperties({'on-iconclick':'searchByKeyDown'});  
		
		this.selectSeeing = 
			[
//		        { "id" : "name_addressBook", "text": "이름",'icon':'icon-work_check'},
		        { "id" : "num_addressBook","text": "전화번호",'icon':'icon-work_check'},
		        { "id" : "mail_addressBook", "text": "이메일",'icon':'icon-work_check'}	    
		    ];
		
		
		
		Top.Dom.selectById('mailAddressBookMainLayout_drop').setProperties({"items":this.selectSeeing, "on-select": this.changeSelectSeeingNum});
		Top.Dom.selectById('mailAddressBookMainLayout_drop').close();
		
		$(document).ready(function(){
			$("div#mailAddressBookMainLayout_drop a.top-menu_btn").css("padding","0px");
			$("div#mailAddressBookMainLayout_Table .top-tableview .head .head-cell").css("text-align", "left");
			$("div#mailAddressBookMainLayout_Table .top-tableview .body .body-cell").css("border-right", "0px");
			$("div#mailAddressBookMainLayout_Table .top-tableview .body .body-cell").css("border-left", "0px");
			$("div#mailAddressBookMainLayout top-textfield .top-textfield-icon").css("width","28px");
			$("div#mailAddressBookMainLayout top-textfield .top-textfield-icon").css("left","10px");
			$("div#mailAddressBookMainLayout top-textfield .top-textfield-root .top-textfield-text").css("padding-right","10px");
			$("div#mailAddressBookMainLayout top-textfield .top-textfield-root .top-textfield-text").css("padding-left","30px");
		});
		
		this.selectSeeingNum = true;
		this.selectSeeingMail = true;
				
		var media = window.matchMedia("(max-device-width: 1024px)");
		if (media.matches) {
			if (Top.App.isWidgetAttached('mailAddressBookMainLayout_Table')) {
				var mailAddressBookMainLayout_Table = Top.Dom.selectById('mailAddressBookMainLayout_Table');
				mailAddressBookMainLayout_Table.setProperties({'layout-height':'calc(100% - 250px)'});
				mailAddressBookMainLayout_Table.setProperties({'scroll-body-height':'100%'});
				mailAddressBookMainLayout_Table.setColumnsVisible(3, false);
				mailAddressBookMainLayout_Table.setColumnsVisible(4, false);
			}
		}	
		
	}, showMailList : function() {
		
		var ctrl = Top.Controller.get('mailMainLeftLogic')
		ctrl.setFolderEvent(null, Top.Dom.selectById(mailData.curBox.WIDGET_ID));
		
	}, getEnter : function(event) {
		
		if(event.key == "Enter") {
			this.searchByKeyDown(); 
		}
	}, changeSeeingState : function(){
		
		if (Top.Controller.get("mailAddressBookMainLogic").selectSeeingMail ==  false && Top.Controller.get("mailAddressBookMainLogic").selectSeeingNum ==  false){
			for (var i = 0; i < mailAddressBookDataRepo.mailAddressBookListData.length; i++){
				mailAddressBookDataRepo.mailAddressBookListData[i].PHONE_NUM = "";
				mailAddressBookDataRepo.mailAddressBookListData[i].EMAIL_ADDRESS = "";
			}
		}
		else if (Top.Controller.get("mailAddressBookMainLogic").selectSeeingMail ==  false && Top.Controller.get("mailAddressBookMainLogic").selectSeeingNum ==  true){
			for (var i = 0; i < mailAddressBookDataRepo.mailAddressBookListData.length; i++){
				mailAddressBookDataRepo.mailAddressBookListData[i].PHONE_NUM = Top.Controller.get("mailAddressBookMainLogic").numData[i];
				mailAddressBookDataRepo.mailAddressBookListData[i].EMAIL_ADDRESS = "";
			}
		}
		else if (Top.Controller.get("mailAddressBookMainLogic").selectSeeingMail ==  true && Top.Controller.get("mailAddressBookMainLogic").selectSeeingNum ==  false){
			for (var i = 0; i < mailAddressBookDataRepo.mailAddressBookListData.length; i++){
				mailAddressBookDataRepo.mailAddressBookListData[i].PHONE_NUM = "";
				mailAddressBookDataRepo.mailAddressBookListData[i].EMAIL_ADDRESS = Top.Controller.get("mailAddressBookMainLogic").mailData[i];
			}
		}
		else if (Top.Controller.get("mailAddressBookMainLogic").selectSeeingMail ==  true && Top.Controller.get("mailAddressBookMainLogic").selectSeeingNum ==  true){
			for (var i = 0; i < mailAddressBookDataRepo.mailAddressBookListData.length; i++){
				mailAddressBookDataRepo.mailAddressBookListData[i].PHONE_NUM = Top.Controller.get("mailAddressBookMainLogic").numData[i];
				mailAddressBookDataRepo.mailAddressBookListData[i].EMAIL_ADDRESS = Top.Controller.get("mailAddressBookMainLogic").mailData[i];
			}
		}
		Top.Dom.selectById('mailAddressBookMainLayout_Table').update();
		
	}, changeSelectSeeingNum : function(event, widget){
		
		if (widget.getSelectedMenuId() == "num_addressBook" && this.selectSeeingNum ==  false )
		{ 
			this.selectSeeingNum = true;
			this.selectSeeing[1] =  { "id" : "num_addressBook","text": "전화번호",'icon':'icon-work_check'};
			Top.Dom.selectById('mailAddressBookMainLayout_drop').setProperties({"items":this.selectSeeing, "on-select": this.changeSelectSeeingNum});
			$(document).ready(function(){
				$("div#mailAddressBookMainLayout_drop a.top-menu_btn").css("padding","0px");
				if (Top.Controller.get("mailAddressBookMainLogic").selectSeeingMail ==  false ){
					$("div#mailAddressBookMainLayout_drop .top-menu_item_inner.inner_depth1.top-menu_mailAddressBookMainLayout_drop_mail_addressBook .top-menu_icon.icon_depth1").css("margin-right","14px");
					
				}
			});
			
		}
		else if (widget.getSelectedMenuId() == "num_addressBook" &&  this.selectSeeingNum ==  true)
		{
			this.selectSeeingNum = false;
			this.selectSeeing[1] =  { "id" : "num_addressBook","text": "전화번호"};
			Top.Dom.selectById('mailAddressBookMainLayout_drop').setProperties({"items":this.selectSeeing, "on-select": this.changeSelectSeeingNum});
			$(document).ready(function(){
				$("div#mailAddressBookMainLayout_drop a.top-menu_btn").css("padding","0px");
				$("div#mailAddressBookMainLayout_drop .top-menu_item_inner.inner_depth1.top-menu_mailAddressBookMainLayout_drop_num_addressBook .top-menu_icon.icon_depth1").css("margin-right","14px");
				if (Top.Controller.get("mailAddressBookMainLogic").selectSeeingMail ==  false ){
					$("div#mailAddressBookMainLayout_drop .top-menu_item_inner.inner_depth1.top-menu_mailAddressBookMainLayout_drop_mail_addressBook .top-menu_icon.icon_depth1").css("margin-right","14px");
				}
				/*
				else{
					
				}
				*/
			});
		}
		
		
		if (widget.getSelectedMenuId() == "mail_addressBook" && this.selectSeeingMail ==  false )
		{ 
			this.selectSeeingMail = true;
			this.selectSeeing[2] =  { "id" : "mail_addressBook", "text": "이메일",'icon':'icon-work_check'};	 
			Top.Dom.selectById('mailAddressBookMainLayout_drop').setProperties({"items":this.selectSeeing, "on-select": this.changeSelectSeeingNum});
			$(document).ready(function(){
				$("div#mailAddressBookMainLayout_drop a.top-menu_btn").css("padding","0px");
				if(Top.Controller.get("mailAddressBookMainLogic").selectSeeingNum ==  false ){
					$("div#mailAddressBookMainLayout_drop .top-menu_item_inner.inner_depth1.top-menu_mailAddressBookMainLayout_drop_num_addressBook .top-menu_icon.icon_depth1").css("margin-right","14px");
				}
			});
			
		}
		else if (widget.getSelectedMenuId() == "mail_addressBook" &&  this.selectSeeingMail ==  true)
		{
			this.selectSeeingMail = false;
			this.selectSeeing[2] =   { "id" : "mail_addressBook", "text": "이메일"};
			Top.Dom.selectById('mailAddressBookMainLayout_drop').setProperties({"items":this.selectSeeing, "on-select": this.changeSelectSeeingNum});
			$(document).ready(function(){
				$("div#mailAddressBookMainLayout_drop a.top-menu_btn").css("padding","0px");
				$("div#mailAddressBookMainLayout_drop .top-menu_item_inner.inner_depth1.top-menu_mailAddressBookMainLayout_drop_mail_addressBook .top-menu_icon.icon_depth1").css("margin-right","14px");
				if(Top.Controller.get("mailAddressBookMainLogic").selectSeeingNum ==  false ){
					$("div#mailAddressBookMainLayout_drop .top-menu_item_inner.inner_depth1.top-menu_mailAddressBookMainLayout_drop_num_addressBook .top-menu_icon.icon_depth1").css("margin-right","14px");
				}
			});
		}
		
		this.changeSeeingState();
		
			
		
	},changeBold : function(event, widget) {
		
		
		for (var i = 1 ; i < 15 ; i ++){
			var tempId;
			tempId = 'mailAddressBookMainLayout_cate_Korea_TextView_';
			tempId = tempId + i.toString() ;
			Top.Dom.selectById(tempId).setProperties({'textColor':'#777777', 'backgroundColor':'#F5F5FB'});
		}
		Top.Dom.selectById('mailAddressBookMainLayout_cate_all_TextView').setProperties({'textColor':'#777777', 'backgroundColor':'#F5F5FB'});
		Top.Dom.selectById('mailAddressBookMainLayout_cate_English_all_Textview').setProperties({'textColor':'#777777', 'backgroundColor':'#F5F5FB'});
		Top.Dom.selectById('mailAddressBookMainLayout_cate_number_all_TextView').setProperties({'textColor':'#777777', 'backgroundColor':'#F5F5FB'});
		Top.Dom.selectById('mailAddressBookMainLayout_cate_etc_TextView').setProperties({'textColor':'#777777', 'backgroundColor':'#F5F5FB'});
		
		Top.Dom.selectById(widget.id).setProperties({'textColor':'#FFFFFF', 'backgroundColor':'#6C56E5'});
		
	} ,	searchByCate : function(event, widget) {
		
		this.changeBold(event, widget);
		
		switch(widget.id){
			case "mailAddressBookMainLayout_cate_all_TextView" :
				var temp_name = "" ;
				break;
			case "mailAddressBookMainLayout_cate_Korea_TextView_1" :
				var temp_name = "ㄱ" ;
				break;
			case "mailAddressBookMainLayout_cate_Korea_TextView_2" :
				var temp_name = "ㄴ" ;
				break;
			case "mailAddressBookMainLayout_cate_Korea_TextView_3" :
				var temp_name = "ㄷ" ;
				break;
			case "mailAddressBookMainLayout_cate_Korea_TextView_4" :
				var temp_name = "ㄹ" ;
				break;
			case "mailAddressBookMainLayout_cate_Korea_TextView_5" :
				var temp_name = "ㅁ" ;
				break;
			case "mailAddressBookMainLayout_cate_Korea_TextView_6" :
				var temp_name = "ㅂ" ;
				break;
			case "mailAddressBookMainLayout_cate_Korea_TextView_7" :
				var temp_name = "ㅅ" ;
				break;
			case "mailAddressBookMainLayout_cate_Korea_TextView_8" :
				var temp_name = "ㅇ" ;
				break;
			case "mailAddressBookMainLayout_cate_Korea_TextView_9" :
				var temp_name = "ㅈ" ;
				break;
			case "mailAddressBookMainLayout_cate_Korea_TextView_10" :
				var temp_name = "ㅊ" ;
				break;
			case "mailAddressBookMainLayout_cate_Korea_TextView_11" :
				var temp_name = "ㅋ" ;
				break;
			case "mailAddressBookMainLayout_cate_Korea_TextView_12" :
				var temp_name = "ㅌ" ;
				break;
			case "mailAddressBookMainLayout_cate_Korea_TextView_13" :
				var temp_name = "ㅍ" ;
				break;
			case "mailAddressBookMainLayout_cate_Korea_TextView_14" :
				var temp_name = "ㅎ" ;
				break;
			case "mailAddressBookMainLayout_cate_English_all_Textview" :
				var temp_name = "SEA0015" ;
				break;
			case "mailAddressBookMainLayout_cate_number_all_TextView" :
				var temp_name = "SEA0016" ;
				break;
		}
		
		
		var data={
				dto: {
					"CMFLG"				: "GetAddressBookList",
					"USER_ID" : userManager.getLoginUserId(),
					"SUBJECT" : temp_name
				}
			}; 
		
		Top.Ajax.execute({
			
			type : "POST",
			url : _workspace.url + "Mail/Mail?action=AddressListGet",
			dataType : "json",
			data	 : JSON.stringify(data),
			crossDomain: true,
			contentType: "application/json",
		    xhrFields: {withCredentials: true},
			success : function(ret,xhr,status) {
				var temp = []
				var tempMail = [];
				var tempNum = [];

				
				
				for (var i = 0 ; i < ret.dto.dbio_total_count; i++){
					
					var tempPhoneNum = [];
					var PhoneNum = "";
					tempPhoneNum = ret.dto.info_address[i].PHONE_INFO[0].PHONE_NUM.split("/");
					
					
					for (var k = 0; k<3; k++){
						if(tempPhoneNum.indexOf("") != -1){
							tempPhoneNum.splice(tempPhoneNum.indexOf(""),1);
						}
					}
					for (var l = 0; l < tempPhoneNum.length ; l++){
						if( l !=0 ){
							PhoneNum = PhoneNum + "-"
						}
						PhoneNum = PhoneNum + tempPhoneNum[l];
					}
					
					
					if (ret.dto.info_address[i].PHONE_INFO.length){
						temp.push({
									"dbio_total_count" : ret.dto.dbio_total_count,
									"ADDRESSBOOK_ID" : ret.dto.info_address[i].ADDRESSBOOK_ID, 
									"NAME" : ret.dto.info_address[i].NAME, 
									"COMPANY" : "", 
									"POSITION" : "", 
									"EMAIL_ADDRESS" : ret.dto.info_address[i].EMAIL_INFO[0].EMAIL_ADDRESS, 
									"PHONE_NUM" : PhoneNum,
										//ret.dto.info_address[i].PHONE_INFO[0].PHONE_NUM, 
									"dbio_total_count2" : ""
								});
						tempMail.push(ret.dto.info_address[i].EMAIL_INFO[0].EMAIL_ADDRESS);
						tempNum.push(PhoneNum);
	
						
					}
				};
				
				mailAddressBookDataRepo.setValue('mailAddressBookListData',temp);
				Top.Dom.selectById("mailAddressBookMainLayout_search_TextField").setProperties({'text':''});
				Top.Controller.get("mailAddressBookMainLogic").numData = tempNum;
				Top.Controller.get("mailAddressBookMainLogic").mailData = tempMail;

				}
		});
		
		
	}, searchByKeyDown : function(event, widget){
		
		var data={
				dto: {
					"CMFLG"		: "GetAddressBookList",
					"USER_ID" 	: userManager.getLoginUserId(),
					"SUBJECT"  	: Top.Dom.selectById("mailAddressBookMainLayout_search_TextField").getText()
				}
			}; 
			
					
		Top.Ajax.execute({
		
			type : "POST",
			url : _workspace.url + "Mail/Mail?action=AddressListGet",
			dataType : "json",
			data	 : JSON.stringify(data),
			crossDomain: true,
			contentType: "application/json",
		    xhrFields: {withCredentials: true},
			success : function(ret,xhr,status) {
				
				
				var temp = []
				var tempMail = [];
				var tempNum = [];

				
				
				
				for (var i = 0 ; i < ret.dto.dbio_total_count; i++){
					var tempPhoneNum = [];
					var PhoneNum = "";
					tempPhoneNum = ret.dto.info_address[i].PHONE_INFO[0].PHONE_NUM.split("/");
					
					
					for (var k = 0; k<3; k++){
						if(tempPhoneNum.indexOf("") != -1){
							tempPhoneNum.splice(tempPhoneNum.indexOf(""),1);
						}
					}
					for (var l = 0; l < tempPhoneNum.length ; l++){
						if( l !=0 ){
							PhoneNum = PhoneNum + "-"
						}
						PhoneNum = PhoneNum + tempPhoneNum[l];
					}
					
					if (ret.dto.info_address[i].PHONE_INFO.length){
						temp.push({
										"dbio_total_count" : ret.dto.dbio_total_count,
										"ADDRESSBOOK_ID" : ret.dto.info_address[i].ADDRESSBOOK_ID, 
										"NAME" : ret.dto.info_address[i].NAME, 
										"EMAIL_ADDRESS" : ret.dto.info_address[i].EMAIL_INFO[0].EMAIL_ADDRESS, 
										"PHONE_NUM" : PhoneNum
						
									});
						tempMail.push(ret.dto.info_address[i].EMAIL_INFO[0].EMAIL_ADDRESS);
						tempNum.push(PhoneNum);

					};
				};
				
				
				mailAddressBookDataRepo.setValue('mailAddressBookListData',temp);
				Top.Controller.get("mailAddressBookMainLogic").numData = tempNum;
				Top.Controller.get("mailAddressBookMainLogic").mailData = tempMail;


				
				}
		});
		for (var i = 1 ; i < 15 ; i ++){
			var tempId;
			tempId = 'mailAddressBookMainLayout_cate_Korea_TextView_';
			tempId = tempId + i.toString() ;
			Top.Dom.selectById(tempId).setProperties({'textColor':'#777777', 'backgroundColor':'#F5F5FB'});
		}
		Top.Dom.selectById('mailAddressBookMainLayout_cate_all_TextView').setProperties({'textColor':'#777777', 'backgroundColor':'#F5F5FB'});
		Top.Dom.selectById('mailAddressBookMainLayout_cate_English_all_Textview').setProperties({'textColor':'#777777', 'backgroundColor':'#F5F5FB'});
		Top.Dom.selectById('mailAddressBookMainLayout_cate_number_all_TextView').setProperties({'textColor':'#777777', 'backgroundColor':'#F5F5FB'});
		Top.Dom.selectById('mailAddressBookMainLayout_cate_etc_TextView').setProperties({'textColor':'#777777', 'backgroundColor':'#F5F5FB'});
		
		Top.Dom.selectById('mailAddressBookMainLayout_cate_all_TextView').setProperties({'textColor':'#FFFFFF', 'backgroundColor':'#6C56E5'});
		
	}, getMailAddressBookDataList : function() {
			
		
		
		var data={
			dto: {
				"CMFLG"				: "GetAddressBookList",
				"USER_ID" : userManager.getLoginUserId()
			}
		}; 
		
				
		Top.Ajax.execute({
		
			type : "POST",
			url : _workspace.url + "Mail/Mail?action=AddressListGet",
			dataType : "json",
			data	 : JSON.stringify(data),
			crossDomain: true,
			contentType: "application/json",
		    xhrFields: {withCredentials: true},
			success : function(ret,xhr,status) {
				
				
				var temp = [];
				var tempMail = [];
				var tempNum = [];
				
				for (var i = 0 ; i < ret.dto.dbio_total_count; i++){
					var tempPhoneNum = [];
					var PhoneNum = "";
					tempPhoneNum = ret.dto.info_address[i].PHONE_INFO[0].PHONE_NUM.split("/");
					
					
					for (var k = 0; k<3; k++){
						if(tempPhoneNum.indexOf("") != -1){
							tempPhoneNum.splice(tempPhoneNum.indexOf(""),1);
						}
					}
					for (var l = 0; l < tempPhoneNum.length ; l++){
						if( l !=0 ){
							PhoneNum = PhoneNum + "-"
						}
						PhoneNum = PhoneNum + tempPhoneNum[l];
					}
					
					if (ret.dto.info_address[i].PHONE_INFO.length){
						temp.push({
										"dbio_total_count" : ret.dto.dbio_total_count,
										"ADDRESSBOOK_ID" : ret.dto.info_address[i].ADDRESSBOOK_ID, 
										"NAME" : ret.dto.info_address[i].NAME, 
										"EMAIL_ADDRESS" : ret.dto.info_address[i].EMAIL_INFO[0].EMAIL_ADDRESS, 
										"PHONE_NUM" : PhoneNum
						
									});
						tempMail.push(ret.dto.info_address[i].EMAIL_INFO[0].EMAIL_ADDRESS);
						tempNum.push(PhoneNum);
					};
				};
				
				mailAddressBookDataRepo.setValue('mailAddressBookListData',temp);
				Top.Controller.get("mailAddressBookMainLogic").numData = tempNum;
				Top.Controller.get("mailAddressBookMainLogic").mailData = tempMail;
				
				if(sessionStorage.getItem('mailInfo') != null)
					Top.Dom.selectById('mailAddressBookMainLayout_Table').setProperties({'pageLength':JSON.parse(sessionStorage.getItem('mailInfo')).addrPageLength});
				}
		});
		
		
		
	}, clickAdd : function(event, widget){
		Top.Controller.get("mailAddressBookMainLogic").changeAddressBook = false; 
		Top.Dom.selectById('mailAddressBookAddDialog').open(); 
		
	}, buttonAbled : function(event, widget) { 						//	버튼 활성화
		
		Top.Dom.selectById('mailAddressBookMainLayout_delete_Button').setProperties({'disabled':false});
				
	} , buttonDisabled : function(event, widget) { 						//	버튼 비 활성화
		
		Top.Dom.selectById('mailAddressBookMainLayout_delete_Button').setProperties({'disabled':true});
				
	}  , buttonStatusChange : function(event, widget) {
		
		if(widget.id == 'mailAddressBookMainLayout_selectAll_CheckBox' && widget.isChecked() == true && mailAddressBookDataRepo.mailAddressBookListData.length != 0) {
			
			Top.Dom.selectById('mailAddressBookMainLayout_Table').checkAll(true);
			this.buttonAbled();
			return;
			
		} else if(widget.id == 'mailAddressBookMainLayout_selectAll_CheckBox' && widget.isChecked() == false) {
			
			Top.Dom.selectById('mailAddressBookMainLayout_Table').checkAll(false);
			this.buttonDisabled();
			return;
		}
		
		if(Top.Dom.selectById('mailAddressBookMainLayout_Table').getCheckedIndex().length == 0)
			this.buttonDisabled();
		else 
			this.buttonAbled();
		
	} , clickDeleteFromOne : function( event, widget){
		
		Top.Controller.get("mailAddressBookMainLogic").deleteFromOne = true;
		Top.Controller.get("mailAddressBookMainLogic").tempAddressBookId = mailAddressBookDataRepo.mailAddressBookListData[widget.id.split("_")[3]].ADDRESSBOOK_ID; 
		Top.Dom.selectById('mailDeleteAddressBookDialog').open();  
		
	},
	clickDelete : function(){
		
		Top.Controller.get("mailAddressBookMainLogic").deleteFromOne = false;
		Top.Dom.selectById('mailDeleteAddressBookDialog').open();  
		
		
	} , clickChangeAddressBook : function(event, widget){
		
		Top.Controller.get("mailAddressBookMainLogic").changeAddressBook = true; 
		Top.Controller.get("mailAddressBookMainLogic").tempAddressBookId = Top.Dom.selectById('mailAddressBookMainLayout_Table').template.data[widget.id.split("_")[3]].ADDRESSBOOK_ID;
		Top.Dom.selectById('mailAddressBookAddDialog').open(); 
		
	},
	
	initCheckBoxStatus : function() {
		Top.Dom.selectById('mailAddressBookMainLayout_selectAll_CheckBox').setChecked(false);
		Top.Dom.selectById('mailAddressBookMainLayout_Table').checkAll(false);
		this.buttonDisabled();
	},
	
	savePageLength : function() {
		let mailInfo = { addrPageLength : Top.Dom.selectById('mailAddressBookMainLayout_Table').getPageInfo().length };
		sessionStorage.setItem('mailInfo', JSON.stringify(mailInfo));
	}
	
	
	
		 




})
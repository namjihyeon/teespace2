
Top.Controller.create('mailExternalAccountManagementLayoutLogic', {
    init : function(event, widget) {
        Top.Dom.selectById('mailExternalAccountManagementLayout_External_Account_Add_Button').setProperties({'on-click':'addExternalAccount'});
       // Top.Dom.selectById('mailExternalAccountManagementLayout_External_Account_Delete_Button').setProperties({'on-click':'deleteExternalAccount'});
       // Top.Dom.selectById('mailExternalAccountManagementLayout_External_Account_Modify_Button').setProperties({'on-click':'modifyExternalAccount'});
    
        this.loadTable();

        var media = window.matchMedia("(max-device-width: 1024px)");
        if (media.matches) {
            if (Top.App.isWidgetAttached('mailExternalAccountManagementLayout_External_Account_Table')) {
                var mailExternalAccountManagementLayout_External_Account_Table = Top.Dom.selectById('mailExternalAccountManagementLayout_External_Account_Table');
                mailExternalAccountManagementLayout_External_Account_Table.setColumnsVisible(2, false);
            }
        }
    }, 
    
    loadTable : function(){
    	var data = {
    			"dto" : {
    				"USER_ID" : userManager.getLoginUserId(),
    				"CMFLG" : "InquireAllAccount"
    			}
    	};
    	
    	Top.Ajax.execute({
			type : "POST",
			url : _workspace.url + "Mail/Mail?action=InquireAllAccount",
			dataType : "json",
			data	 : JSON.stringify(data),
			crossDomain: true,
			contentType: "application/json; charset=utf-8",
		    xhrFields: {withCredentials: true},
			success : function(ret,xhr,status) {
				$("div#mailExternalAccountManagementLayout_External_Account_Table_Layout").empty();
				var data_list = ret.dto.AccountList;
				var table_list = window['mailExternalAccountDataRepo'].mailExternalAccountListData;

				table_list.length = 0;
				
				if(data_list == null) data_list = [];
				
				Top.Controller.get('mailExternalAccountManagementLayoutLogic').makeExternalAccountLayout(data_list);
				
				for(var i =0; i< data_list.length; i++){
					var tmp ={
							'HOST_NAME':data_list[i].HOST_NAME,
							'USER_EMAIL': data_list[i].USER_EMAIL,
							'PROTOCOL':data_list[i].PROTOCOL,
							'ACCOUNT_ID':data_list[i].ACCOUNT_ID,
							'ID':data_list[i].USER_EMAIL,
							'ORIGINAL_SAVE_YN': data_list[i].ORIGINAL_SAVE_YN,
							'RECEIVE_PORT':data_list[i].RECEIVE_PORT,
							'RECEIVE_SERVER_NAME':data_list[i].RECEIVE_SERVER_NAME,
							'SEND_PORT':data_list[i].SEND_PORT,
							'SEND_SERVER_NAME':data_list[i].SEND_SERVER_NAME
						}
					
					table_list.push(tmp);
				}
				Top.Dom.selectById('mailExternalAccountManagementLayout_External_Account_List_Text_1_1_1').setText(ret.dto.dbio_total_count);
				//Top.Dom.selectById('mailExternalAccountManagementLayout_External_Account_Table').update(); //update table
			},

		    error : function(request, status, error) {
			}
		});
	},

    addExternalAccount : function() {
        Top.Dom.selectById('addExternalAccountDialog').open();
	},

    deleteExternalAccount : function(event, widget) {
        let dialog = Top.Dom.selectById('deleteExternalAccountDialog');
        let account_id = window['mailExternalAccountDataRepo'].mailExternalAccountListData[Number(widget.id.split("_")[1])].ACCOUNT_ID;
        let protocol = window['mailExternalAccountDataRepo'].mailExternalAccountListData[Number(widget.id.split("_")[1])].PROTOCOL;
    	dialog.setProperties({onOpen:function(){Top.Controller.get('mailDeleteExternalAccountPopupLayoutLogic').setDeleteContents(account_id, protocol);}});
        dialog.open();
	},	

    modifyExternalAccount : function(event, widget) {
    	let dialog = Top.Dom.selectById('modifyExternalAccountDialog');
    	this.dto = window['mailExternalAccountDataRepo'].mailExternalAccountListData[Number(widget.id.split("_")[1])];
    	dialog.open();
    },
    
    makeExternalAccountLayout: function(data){
    	for(var a = 0; a<data.length;a++ ){
    		var outLayout = Top.Widget.create('top-linearlayout');
        	outLayout.setProperties({'id':'outLayout'+"_"+a, 'layout-height':'wrap_content', 'vertical-alignment':'middle', 'layout-width':'match_parent','orientation':'vertical', 'border-width':'1px 0px 1px 0px',
    			'layout-vertical-alignment':'MIDDLE'});
    		
        	//서비스
        	var serviceNameTextView = Top.Widget.create('top-textview');
        	serviceNameTextView.setProperties({'id':'serviceNameTextView'+"_"+a,'layoutHeight':'wrap_content','layoutHorizontalAlignment':'LEFT','layoutWidth':'wrap_content',
        			'text':'서비스','textSize': '0.81rem', 'margin':'0.5rem 0px 0px 1.125rem', 'textColor':'#000000', 'fontFamily':'NotoSansCJKkr-Medium'});
        	
        	var serviceNameLayout = Top.Widget.create('top-linearlayout');
        	serviceNameLayout.setProperties({'id':'serviceNameLayout'+"_"+a,'layoutHeight':'2.19rem','layoutHorizontalAlignment':'LEFT','layoutWidth':'28%', 'border-width':'0px 1px 0px 0px','backgroundColor':"#F5F5FB"});
        	
        	serviceNameLayout.addWidget(serviceNameTextView);
        	serviceNameLayout.complete();
        	
        	//네이버
        	var serviceContentTextView = Top.Widget.create('top-textview');
        	serviceContentTextView.setProperties({'id':'serviceContentTextView'+"_"+a,'layoutHeight':'wrap_content','layoutHorizontalAlignment':'LEFT','layoutWidth':'wrap_content'
        		,'text':data[a].HOST_NAME,'textSize': '0.75rem', 'margin':'0.5rem 0px 0px 1.125rem'});
        	
        	var serviceContentLayout = Top.Widget.create('top-linearlayout');
        	serviceContentLayout.setProperties({'id':'serviceContentLayout'+"_"+a,'layoutHeight':'2.19rem','layoutHorizontalAlignment':'LEFT','layoutWidth':'72%', 'border-width':'0px'});
        	
        	serviceContentLayout.addWidget(serviceContentTextView);
        	serviceContentLayout.complete();
        	
        	//서비스-네이버
        	var serviceLayout = Top.Widget.create('top-linearlayout');
        	serviceLayout.setProperties({'id':'serviceLayout'+"_"+a,'layoutHeight':'2.19rem','layoutHorizontalAlignment':'LEFT','layoutWidth':'match_parent','orientation':'horizontal', 'border-width':'0px 0px 1px 0px'});
        	
        	serviceLayout.addWidget(serviceNameLayout);
        	serviceLayout.addWidget(serviceContentLayout);
        	serviceLayout.complete();
        	
        	outLayout.addWidget(serviceLayout);
        	
        	
        	//이메일 주소
        	var emailNameTextView = Top.Widget.create('top-textview');
        	emailNameTextView.setProperties({'id':'emailNameTextView'+"_"+a,'layoutHeight':'wrap_content','layoutHorizontalAlignment':'LEFT','layoutWidth':'wrap_content',
        			'text':'이메일 주소','textSize': '0.81rem', 'margin':'0.8125rem 0px 0px 1.125rem', 'textColor':'#000000', 'fontFamily':'NotoSansCJKkr-Medium'});
        	
        	var emailNameLayout = Top.Widget.create('top-linearlayout');
        	emailNameLayout.setProperties({'id':'emailNameLayout'+"_"+a,'layoutHeight':'2.88rem','layoutHorizontalAlignment':'LEFT','layoutWidth':'28%', 'border-width':'0px 1px 0px 0px','backgroundColor':"#F5F5FB"});
        	
        	emailNameLayout.addWidget(emailNameTextView);
        	emailNameLayout.complete();
        	
        	//~~~@~~~
        	var emailContentTextView = Top.Widget.create('top-textview');
        	emailContentTextView.setProperties({'id':'emailContentTextView'+"_"+a,'layoutHeight':'wrap_content','layoutHorizontalAlignment':'LEFT','layoutWidth':'wrap_content'
        			,'text':data[a].USER_EMAIL,'textSize': '0.75rem', 'margin':'0.8125rem 0px 0px 1.125rem'});
        	
        	var emailContentLayout = Top.Widget.create('top-linearlayout');
        	emailContentLayout.setProperties({'id':'emailContentLayout'+"_"+a,'layoutHeight':'2.88rem','layoutHorizontalAlignment':'LEFT','layoutWidth':'72%', 'border-width':'0px'});
        	
        	emailContentLayout.addWidget(emailContentTextView);
        	emailContentLayout.complete();
        	
        	//이메일 주소-~~~@~~~
        	var emailLayout = Top.Widget.create('top-linearlayout');
        	emailLayout.setProperties({'id':'emailLayout'+"_"+a,'layoutHeight':'2.88rem','layoutHorizontalAlignment':'LEFT','layoutWidth':'match_parent','orientation':'horizontal', 'border-width':'0px 0px 1px 0px'});
        	
        	emailLayout.addWidget(emailNameLayout);
        	emailLayout.addWidget(emailContentLayout);
        	
        	emailLayout.complete();
        	outLayout.addWidget(emailLayout);
        	
        	
        	//유형
        	var typeNameTextView = Top.Widget.create('top-textview');
        	typeNameTextView.setProperties({'id':'typeNameTextView'+"_"+a,'layoutHeight':'wrap_content','layoutHorizontalAlignment':'LEFT','layoutWidth':'wrap_content',
        			'text':'유형','textSize': '0.81rem', 'margin':'0.5rem 0px 0px 1.125rem', 'textColor':'#000000', 'fontFamily':'NotoSansCJKkr-Medium'});
        	
        	var typeNameLayout = Top.Widget.create('top-linearlayout');
        	typeNameLayout.setProperties({'id':'typeNameLayout'+"_"+a,'layoutHeight':'2.19rem','layoutHorizontalAlignment':'LEFT','layoutWidth':'28%', 'border-width':'0px 1px 0px 0px','backgroundColor':"#F5F5FB"});
        	
        	typeNameLayout.addWidget(typeNameTextView);
        	typeNameLayout.complete();
        	
        	//pop3
        	var typeContentTextView = Top.Widget.create('top-textview');
        	typeContentTextView.setProperties({'id':'typeContentTextView'+"_"+a,'layoutHeight':'wrap_content','layoutHorizontalAlignment':'LEFT','layoutWidth':'wrap_content',
        			'text':data[a].PROTOCOL,'textSize': '0.75rem', 'margin':'0.5rem 0px 0px 1.125rem'});
        	
        	var typeContentLayout = Top.Widget.create('top-linearlayout');
        	typeContentLayout.setProperties({'id':'typeContentLayout'+"_"+a,'layoutHeight':'2.19rem','layoutHorizontalAlignment':'LEFT','layoutWidth':'72%', 'border-width':'0px'});
        	
        	typeContentLayout.addWidget(typeContentTextView);
        	typeContentLayout.complete();
        	
        	//유형-pop3
        	var typeLayout = Top.Widget.create('top-linearlayout');
        	typeLayout.setProperties({'id':'typeLayout'+"_"+a,'layoutHeight':'2.19rem','layoutHorizontalAlignment':'LEFT','layoutWidth':'match_parent','orientation':'horizontal', 'border-width':'0px 0px 1px 0px'});
        	
        	typeLayout.addWidget(typeNameLayout);
        	typeLayout.addWidget(typeContentLayout);
        	
        	typeLayout.complete();
        	outLayout.addWidget(typeLayout);
        	
        	
        	//수정/삭제
        	var modiDeleteNameTextView = Top.Widget.create('top-textview');
        	modiDeleteNameTextView.setProperties({'id':'modiDeleteNameTextView'+"_"+a,'layoutHeight':'wrap_content','layoutHorizontalAlignment':'LEFT','layoutWidth':'wrap_content',
        			'text':'수정/삭제','textSize': '0.81rem', 'margin':'0.5rem 0px 0px 1.125rem', 'textColor':'#000000', 'fontFamily':'NotoSansCJKkr-Medium'});
        	
        	var modiDeleteNameLayout = Top.Widget.create('top-linearlayout');
        	modiDeleteNameLayout.setProperties({'id':'modiDeleteNameLayout'+"_"+a,'layoutHeight':'2.19rem','layoutHorizontalAlignment':'LEFT','layoutWidth':'28%', 'border-width':'0px 1px 0px 0px','backgroundColor':"#F5F5FB"});
        	
        	modiDeleteNameLayout.addWidget(modiDeleteNameTextView);
        	modiDeleteNameLayout.complete();
        	
        	//버튼
        	var modiDeleteContentTextView = Top.Widget.create('top-textview');
        	modiDeleteContentTextView.setProperties({'id':'modiDeleteContentTextView'+"_"+a,'layoutHeight':'wrap_content','layoutHorizontalAlignment':'LEFT','layoutWidth':'wrap_content',
        			'text':'/','margin':'10px 2px 0px 8px', 'textSize':'0.75rem'});
        	
        	var modiDeleteContentLayout = Top.Widget.create('top-linearlayout');
        	modiDeleteContentLayout.setProperties({'id':'modiDeleteContentLayout'+"_"+a,'layoutHeight':'2.19rem','layoutHorizontalAlignment':'LEFT','layoutWidth':'72%', 'border-width':'0px'});
        	
        	//modiDeleteContentLayout.addWidget(modiDeleteContentTextView);
        	//modiDeleteContentLayout.complete();
        	
        	
        	var modiButton = Top.Widget.create('top-button');
        	modiButton.setProperties({'id':'modiButton'+"_"+a, 'text-size':'0.75rem', 'background-color' : '#FFFFFF', 'horizontalAlignment':'CENTER', 'layoutHorizontalAlignment':'CENTER', 
    			'margin':'0.25rem 0px 0px 1.125rem', 'layoutWidth':'3rem', 'padding' : '0px', 'layoutHeight':'1.69rem','fontFamily':'NotoSansCJKkr-Regular','text':'수정', 'on-click':'modifyExternalAccount', 'text-color' : '#3B3B3B'});
        	modiButton.style('border', '1px solid #C6CED6');
        	var deleteButton = Top.Widget.create('top-button');
        	deleteButton.setProperties({'id':'deleteButton'+"_"+a, 'padding' : '0px', 'background-color' : '#FFFFFF', 'text-size':'0.75rem', 'horizontalAlignment':'CENTER', 'layoutHorizontalAlignment':'CENTER', 
    			'margin':'0.25rem 0px 0px 0px', 'layoutWidth':'3rem', 'layoutHeight':'1.69rem', 'fontFamily':'NotoSansCJKkr-Regular','text':'삭제', 'on-click':'deleteExternalAccount' , 'text-color' : '#3B3B3B'});
        	deleteButton.style('border', '1px solid #C6CED6');

        	modiDeleteContentLayout.addWidget(modiButton);
        	modiDeleteContentLayout.addWidget(modiDeleteContentTextView);
        	modiDeleteContentLayout.addWidget(deleteButton);
        	modiDeleteContentLayout.complete();
        	
        	//수정/삭제  버튼
        	var modiDeleteLayout = Top.Widget.create('top-linearlayout');
        	modiDeleteLayout.setProperties({'id':'modiDeleteLayout'+"_"+a,'layoutHeight':'2.19rem','layoutHorizontalAlignment':'LEFT','layoutWidth':'match_parent','orientation':'horizontal', 'border-width':'0px'});
        	
        	modiDeleteLayout.addWidget(modiDeleteNameLayout);
        	modiDeleteLayout.addWidget(modiDeleteContentLayout);
        	
        	modiDeleteLayout.complete();
        	outLayout.addWidget(modiDeleteLayout);
        	
        	
        	
        	
        	outLayout.complete();
        	Top.Dom.selectById("mailExternalAccountManagementLayout_External_Account_Table_Layout").addWidget(outLayout);
        	Top.Dom.selectById("outLayout"+"_"+a).setProperties({'border-width':'1px 0px 1px 0px'});
        	if(a != 0){
        		Top.Dom.selectById("outLayout"+"_"+a).setProperties({'margin':'12px 0px 0px 0px'});
        	}
    	}
    	return;
    	var outLayout = Top.Widget.create('top-linearlayout');
    	outLayout.setProperties({'id':'outLayout', 'layout-height':'154px', 'vertical-alignment':'middle', 'layout-width':'match_parent','orientation':'vertical', 'border-width':'1px 0px 1px 0px',
			'layout-vertical-alignment':'MIDDLE'});
		
    	//서비스
    	var serviceNameTextView = Top.Widget.create('top-textview');
    	serviceNameTextView.setProperties({'id':'serviceNameTextView','layoutHeight':'wrap_content','layoutHorizontalAlignment':'LEFT','layoutWidth':'wrap_content',
    			'text':'서비스','textSize': '13px', 'margin':'8px 0px 0px 18px', 'textColor':'#000000', 'fontFamily':'NotoSansCJKkr-Medium'});
    	
    	var serviceNameLayout = Top.Widget.create('top-linearlayout');
    	serviceNameLayout.setProperties({'id':'serviceNameLayout','layoutHeight':'35px','layoutHorizontalAlignment':'LEFT','layoutWidth':'28%', 'border-width':'0px 1px 0px 0px','backgroundColor':"#F5F5FB"});
    	
    	serviceNameLayout.addWidget(serviceNameTextView);
    	serviceNameLayout.complete();
    	
    	//네이버
    	var serviceContentTextView = Top.Widget.create('top-textview');
    	serviceContentTextView.setProperties({'id':'serviceContentTextView','layoutHeight':'wrap_content','layoutHorizontalAlignment':'LEFT','layoutWidth':'wrap_content'
    		,'text':'네이버','textSize': '12px', 'margin':'8px 0px 0px 18px'});
    	
    	var serviceContentLayout = Top.Widget.create('top-linearlayout');
    	serviceContentLayout.setProperties({'id':'serviceContentLayout','layoutHeight':'35px','layoutHorizontalAlignment':'LEFT','layoutWidth':'72%', 'border-width':'0px'});
    	
    	serviceContentLayout.addWidget(serviceContentTextView);
    	serviceContentLayout.complete();
    	
    	//서비스-네이버
    	var serviceLayout = Top.Widget.create('top-linearlayout');
    	serviceLayout.setProperties({'id':'serviceLayout','layoutHeight':'35px','layoutHorizontalAlignment':'LEFT','layoutWidth':'match_parent','orientation':'horizontal', 'border-width':'0px 0px 1px 0px'});
    	
    	serviceLayout.addWidget(serviceNameLayout);
    	serviceLayout.addWidget(serviceContentLayout);
    	serviceLayout.complete();
    	
    	outLayout.addWidget(serviceLayout);
    	
    	
    	//이메일 주소
    	var emailNameTextView = Top.Widget.create('top-textview');
    	emailNameTextView.setProperties({'id':'emailNameTextView','layoutHeight':'wrap_content','layoutHorizontalAlignment':'LEFT','layoutWidth':'wrap_content',
    			'text':'이메일 주소','textSize': '13px', 'margin':'13px 0px 0px 18px', 'textColor':'#000000', 'fontFamily':'NotoSansCJKkr-Medium'});
    	
    	var emailNameLayout = Top.Widget.create('top-linearlayout');
    	emailNameLayout.setProperties({'id':'emailNameLayout','layoutHeight':'46px','layoutHorizontalAlignment':'LEFT','layoutWidth':'28%', 'border-width':'0px 1px 0px 0px','backgroundColor':"#F5F5FB"});
    	
    	emailNameLayout.addWidget(emailNameTextView);
    	emailNameLayout.complete();
    	
    	//~~~@~~~
    	var emailContentTextView = Top.Widget.create('top-textview');
    	emailContentTextView.setProperties({'id':'emailContentTextView','layoutHeight':'wrap_content','layoutHorizontalAlignment':'LEFT','layoutWidth':'wrap_content'
    			,'text':'네이버','textSize': '12px', 'margin':'13px 0px 0px 18px'});
    	
    	var emailContentLayout = Top.Widget.create('top-linearlayout');
    	emailContentLayout.setProperties({'id':'emailContentLayout','layoutHeight':'46px','layoutHorizontalAlignment':'LEFT','layoutWidth':'72%', 'border-width':'0px'});
    	
    	emailContentLayout.addWidget(emailContentTextView);
    	emailContentLayout.complete();
    	
    	//이메일 주소-~~~@~~~
    	var emailLayout = Top.Widget.create('top-linearlayout');
    	emailLayout.setProperties({'id':'emailLayout','layoutHeight':'46px','layoutHorizontalAlignment':'LEFT','layoutWidth':'match_parent','orientation':'horizontal', 'border-width':'0px 0px 1px 0px'});
    	
    	emailLayout.addWidget(emailNameLayout);
    	emailLayout.addWidget(emailContentLayout);
    	
    	emailLayout.complete();
    	outLayout.addWidget(emailLayout);
    	
    	
    	//유형
    	var typeNameTextView = Top.Widget.create('top-textview');
    	typeNameTextView.setProperties({'id':'typeNameTextView','layoutHeight':'wrap_content','layoutHorizontalAlignment':'LEFT','layoutWidth':'wrap_content',
    			'text':'유형','textSize': '13px', 'margin':'8px 0px 0px 18px', 'textColor':'#000000', 'fontFamily':'NotoSansCJKkr-Medium'});
    	
    	var typeNameLayout = Top.Widget.create('top-linearlayout');
    	typeNameLayout.setProperties({'id':'typeNameLayout','layoutHeight':'35px','layoutHorizontalAlignment':'LEFT','layoutWidth':'28%', 'border-width':'0px 1px 0px 0px','backgroundColor':"#F5F5FB"});
    	
    	typeNameLayout.addWidget(typeNameTextView);
    	typeNameLayout.complete();
    	
    	//pop3
    	var typeContentTextView = Top.Widget.create('top-textview');
    	typeContentTextView.setProperties({'id':'typeContentTextView','layoutHeight':'wrap_content','layoutHorizontalAlignment':'LEFT','layoutWidth':'wrap_content',
    			'text':'네이버','textSize': '12px', 'margin':'8px 0px 0px 18px'});
    	
    	var typeContentLayout = Top.Widget.create('top-linearlayout');
    	typeContentLayout.setProperties({'id':'typeContentLayout','layoutHeight':'35px','layoutHorizontalAlignment':'LEFT','layoutWidth':'72%', 'border-width':'0px'});
    	
    	typeContentLayout.addWidget(typeContentTextView);
    	typeContentLayout.complete();
    	
    	//유형-pop3
    	var typeLayout = Top.Widget.create('top-linearlayout');
    	typeLayout.setProperties({'id':'typeLayout','layoutHeight':'35px','layoutHorizontalAlignment':'LEFT','layoutWidth':'match_parent','orientation':'horizontal', 'border-width':'0px 0px 1px 0px'});
    	
    	typeLayout.addWidget(typeNameLayout);
    	typeLayout.addWidget(typeContentLayout);
    	
    	typeLayout.complete();
    	outLayout.addWidget(typeLayout);
    	
    	
    	//수정/삭제
    	var modiDeleteNameTextView = Top.Widget.create('top-textview');
    	modiDeleteNameTextView.setProperties({'id':'modiDeleteNameTextView','layoutHeight':'wrap_content','layoutHorizontalAlignment':'LEFT','layoutWidth':'wrap_content',
    			'text':'수정/삭제','textSize': '13px', 'margin':'8px 0px 0px 18px', 'textColor':'#000000', 'fontFamily':'NotoSansCJKkr-Medium'});
    	
    	var modiDeleteNameLayout = Top.Widget.create('top-linearlayout');
    	modiDeleteNameLayout.setProperties({'id':'modiDeleteNameLayout','layoutHeight':'35px','layoutHorizontalAlignment':'LEFT','layoutWidth':'28%', 'border-width':'0px 1px 0px 0px','backgroundColor':"#F5F5FB"});
    	
    	modiDeleteNameLayout.addWidget(modiDeleteNameTextView);
    	modiDeleteNameLayout.complete();
    	
    	//버튼
    	var modiDeleteContentTextView = Top.Widget.create('top-textview');
    	modiDeleteContentTextView.setProperties({'id':'modiDeleteContentTextView','layoutHeight':'wrap_content','layoutHorizontalAlignment':'LEFT','layoutWidth':'wrap_content',
    			'text':'/','margin':'10px 2px 0px 8px', 'textSize':'12px'});
    	
    	var modiDeleteContentLayout = Top.Widget.create('top-linearlayout');
    	modiDeleteContentLayout.setProperties({'id':'modiDeleteContentLayout','layoutHeight':'35px','layoutHorizontalAlignment':'LEFT','layoutWidth':'72%', 'border-width':'0px'});
    	
    	//modiDeleteContentLayout.addWidget(modiDeleteContentTextView);
    	//modiDeleteContentLayout.complete();
    	
    	
    	var modiButton = Top.Widget.create('top-button');
    	modiButton.setProperties({'id':'modiButton', 'class':'text-line2', 'text-size':'14px', 'horizontalAlignment':'CENTER', 'layoutHorizontalAlignment':'CENTER', 
			'margin':'4px 0px 0px 18px', 'minHeight':'27px', 'minWidth':'48px','fontFamily':'NotoSansCJKkr-Regular','text':'수정','on-click':function(){
				Top.Controller.get('mailExternalAccountManagementLayoutLogic').modifyExternalAccount()}});
    	
    	var deleteButton = Top.Widget.create('top-button');
    	deleteButton.setProperties({'id':'deleteButton', 'class':'text-line2', 'text-size':'14px', 'horizontalAlignment':'CENTER', 'layoutHorizontalAlignment':'CENTER', 
			'margin':'4px 0px 0px 0px', 'minHeight':'27px', 'minWidth':'48px','fontFamily':'NotoSansCJKkr-Regular','text':'삭제','on-click':'deleteExternalAccount'});
    	
    	modiDeleteContentLayout.addWidget(modiButton);
    	modiDeleteContentLayout.addWidget(modiDeleteContentTextView);
    	modiDeleteContentLayout.addWidget(deleteButton);
    	modiDeleteContentLayout.complete();
    	
    	//수정/삭제  버튼
    	var modiDeleteLayout = Top.Widget.create('top-linearlayout');
    	modiDeleteLayout.setProperties({'id':'modiDeleteLayout','layoutHeight':'35px','layoutHorizontalAlignment':'LEFT','layoutWidth':'match_parent','orientation':'horizontal', 'border-width':'0px'});
    	
    	modiDeleteLayout.addWidget(modiDeleteNameLayout);
    	modiDeleteLayout.addWidget(modiDeleteContentLayout);
    	
    	modiDeleteLayout.complete();
    	outLayout.addWidget(modiDeleteLayout);
    	
    	
    	
    	
    	outLayout.complete();
    	Top.Dom.selectById("mailExternalAccountManagementLayout_External_Account_Table_Layout").addWidget(outLayout);
    	Top.Dom.selectById("outLayout").setProperties({'border-width':'1px 0px 1px 0px'})

    }
    
});

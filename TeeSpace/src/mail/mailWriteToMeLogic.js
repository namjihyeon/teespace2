Top.Controller.create('mailWriteToMeLogic', {
	init : function(event, widget) {
		
		Top.Dom.selectById('mailDropZoneOverlay').setProperties({'visible':'none'});
		
		var dropZone = document.querySelector("div#mailWriteToMe" +
				"Layout");		
		dropZone.addEventListener('dragenter', MailDragDrop.onDragEnter);		
		dropZone.addEventListener('dragleave', MailDragDrop.onDragLeave);
		dropZone.addEventListener('drop'     , MailDragDrop.onDrop);
		
		var mailSender_WS = Top.Dom.selectById('mailMainLayout_Mail_Text1').getText();
		var mailSender_Account = workspaceManager.getMailId();
		
		
		
		Top.Dom.selectById('mailWriteToMeLayout_Sender_WS').setText(mailSender_WS);
		
		this.content=""; 
		mailFileAttachmentRepo.setValue('mailFileAttachmentList',[]);
	
		this.tdrivefile = [];
		this.fileListArr = [];
		this.tempTdrivefile = [];
		this.tempTFileId = [];
		
		this.fileAttach = [];
		this.file; 
		this.fileList = []
		this.fileUp = []; 
		this.tempFileIndex = [];
		this.isFromTDrive ;
		this.saveAtTDrive ;
		
		
		this.tempMailId;
		this.tempGroupIndex;
		this.tempInt = setInterval(function() {
			if(!mail.isWritingToMe()) {
				clearInterval(Top.Controller.get('mailWriteToMeLogic').tempInt);
				return;
			}
				
			Top.Controller.get('mailWriteToMeLogic').saveTemp();
			
		}, 30000);
		Top.Dom.selectById('mailWriteToMeLayout_Button_Preview_1').setProperties({'on-click':'saveTemp'});
		
//		setTimeout(function(){$('#sun_editorEdit')[0].style.height = "100%";$('#sun_editorEdit')[0].style.maxHeight = '';});
		
		Top.Dom.selectById('mailWriteToMeLayout_Button_Save').setProperties({'on-click':'clickSave'});
		if (Top.Controller.get('mailReadButtonLogic').modifyClicked == true){
			Top.Controller.get('mailMainLeftLogic').setTitle('수정');
			this.callMail();
			Top.Dom.selectById('mailWriteToMeLayout_Button_Save').setProperties({'on-click':'modifyMail'});
		}
		
		Top.Controller.get('mailWriteToMeLogic').searchSubject = null; 
		
		Top.Dom.selectById('mailWriteToMeLayout_AttachFile').setProperties({ "on-select": this.clickDropdownMenu});
		Top.Dom.selectById('mailWriteToMeLayout_AttachFile').close(); 
	
		var media = window.matchMedia("screen and (max-device-width: 1024px)");
		
		if(media.matches) {
			$('div#mailMainLayout_Left').removeClass('mobile-mail-visible-panel');
			$('div#mailMainLayout_Left').addClass('mobile-mail-invisible-panel');
			$('div#mailMainLayout_Center').removeClass('mobile-mail-invisible-panel');
			$('div#mailMainLayout_Center').addClass('mobile-mail-visible-panel');
			if (Top.App.isWidgetAttached('mailWriteToMeLayout_FileTable')) {
				var mailWriteToMeLayout_FileTable = Top.Dom.selectById('mailWriteToMeLayout_FileTable');
				mailWriteToMeLayout_FileTable.setColumnsVisible(4, false);
			}
			$('div#mailWriteToMeLayout_Button_Right').addClass('mail-time-init');
			$('#mailWriteToMeLayout_Content')[0].style.minHeight = document.body.offsetHeight - 400 + 'px';
			setTimeout(function(){$('#sun_editorEdit')[0].style.minHeight = document.body.offsetHeight - 400 + 'px';});
		}
		
		if(Top.version.substr(8,2) > 87)
			Top.Dom.selectById('mailWriteToMeLayout_Subject').setProperties({'on-blur' : 'manageByte', 'on-keydown' : 'manageByte'});
		else
			Top.Dom.selectById('mailWriteToMeLayout_Subject').setProperties({'on-blur' : 'manageByte', 'on-keyup' : 'manageByte'});
		
		var defaultHeight = Top.Dom.selectById('mailWriteToMeLayout_WorkArea').getHeight() - $('div#mailWriteToMeLayout_WorkArea .se-toolbar').height();
		$('div#mailWriteToMeLayout_WorkArea .se-wrapper-inner').css('min-height', defaultHeight);
		$('div#mailWriteToMeLayout_WorkArea .se-wrapper-inner').css('max-height', defaultHeight);
		
	}, setEditorSize : function() {
		
		var upperHeight = $('top-linearlayout#mailWriteToMeLayout_Button')[0].offsetHeight + $('top-linearlayout#mailWriteToMeLayout_Info')[0].offsetHeight + $('top-linearlayout#mailWriteToMeLayout_FileBox')[0].offsetHeight;
		Top.Dom.selectById('mailWriteToMeLayout_WorkArea').setProperties({'layout-height':'calc(100% - ' + upperHeight + 'px)'});
		
	}, exitYN : function() {
		
//		Top.Dom.selectById('mailExitWriteDialog').open();
		TeeAlarm.open({'title' :'편지를 저장하지 않고 나가시겠습니까?', 'content' :'수정 사항이 저장되지 않았습니다.',	
			buttons : [{'text' : '나가기', 'onClicked' : function(){ 
				Top.Controller.get('mailExitWriteLogic').clickExit();
				TeeAlarm.close();
				}}]})
		
	},clickDropdownMenu : function(event, widget){
		
		if(widget.getSelectedMenuId() == "menuitem0"){
			Top.Controller.get("mailWriteToMeLogic").saveAtTDrive = false;
			Top.Controller.get("mailWriteToMeLogic").isFromTDrive = false;
			Top.Controller.get("mailWriteToMeLogic").attachFile()
		}
		else if (widget.getSelectedMenuId() == "menuitem1"){
			Top.Controller.get("mailWriteToMeLogic").toTdrive = true; 
			Top.Controller.get("mailWriteToMeLogic").saveAtTDrive = false;
			Top.Controller.get("mailWriteToMeLogic").isFromTDrive = true;
			var config = {
					buttonText1: '첨부',
					buttonText2: '취소',
					buttonFn1: function(){ Top.Controller.get("mailWriteToMeLogic").attachFile() },
					}
		
			OPEN_TDRIVE(config);
			
		}
		else if (widget.getSelectedMenuId() == "menuitem2"){
			Top.Controller.get("mailWriteToMeLogic").saveAtTDrive = true;
			Top.Controller.get("mailWriteToMeLogic").isFromTDrive = false;
			Top.Controller.get("mailWriteToMeLogic").attachFile()
		}
		
		
	},
	
	clickSave : function(event, widget) {
		
		
		var mailSubject = Top.Dom.selectById('mailWriteToMeLayout_Subject').getText();
		if (!mailSubject){
			TeeAlarm.open({'title' : '편지보내기', 'content' : '제목이 입력되지 않았습니다.<br>제목 없이 편지를 보내겠습니까?',
				buttons : [{'text' : '보내기', 'onClicked' : function(){ 
					Top.Dom.selectById('mailWriteToMeLayout_Subject').setText('제목 없음');
					Top.Controller.get('mailWriteToMeLogic').clickSave();
					TeeAlarm.close();
					}}]})
					return;
		}
		
		
		var ReceiverArr = [{ "TO_ADDR" : Top.Dom.selectById('mailMainLayout_Mail_Text1').getText()}];
		
		var importance;
		if (Top.Dom.selectById('mailWriteToMeLayout_CheckBox_Importance').isChecked() == true)
			importance = 'IMP0001';
		else
			importance = 'IMP0002';
		

		var spaceremove = Top.Dom.selectById('mailWriteToMeLayout_WorkArea_HtmlEditor').getValue();//mailSunEditor.getContents();
		if(spaceremove == undefined)
    		spaceremove = "";
    	else
    		spaceremove = [].filter.call(spaceremove, function (c) {return c.charCodeAt() !== 8203;}).join('');
        var context = spaceremove;
        context = '<style> table { width: 100%; border-spacing : 0px } td, th { border: 1px solid #e1e1e1 } </style>' + context;
        
        var mailAttachment = [] ; 
		var isAttach;
	
        function load_file(callback) {
			
			if (Top.Controller.get('mailWriteToMeLogic').fileAttach.length ==0)
    		{
    		isAttach = 'COM0002';
    		mailAttachment = [{
	            "ATTACHMENT_ID":"",
	            "MAIL_ID": "",
	            "STORAGE_PATH" : "", 
	            "USER_ID" : "",
	            "WS_ID" : "",
	            "EXTENSION" : "",
	            "FILE_NAME" : "",
	            "TDRIVE_CH_ID" : "", 
	            "FILE_ID" : "",
	            "FILE_SIZE" : ""
	            
	        }];
    		callback();
    		}
    	
			else{
				Top.Dom.selectById('mailWriteToMeLayout_Button_Save').setProperties({'disabled' : 'true'}); 
				Top.Loader.start(); 
				
	    		isAttach = 'COM0001'
	    			for (var i = 0; i < Top.Controller.get('mailWriteToMeLogic').fileAttach.length; i++){
	    				if(Top.Controller.get('mailWriteToMeLogic').tempFileIndex[i] == "0"){
	    					var l = Top.Controller.get('mailWriteToMeLogic').fileAttach[i].file[0].name.split('.').length;
	    					var file_extension = Top.Controller.get('mailWriteToMeLogic').fileAttach[i].file[0].name.split('.')[l-1];
	    					var file_name = Top.Controller.get('mailWriteToMeLogic').fileAttach[i].file[0].name.slice(0,-(file_extension.length + 1));
	    					var file_size = Top.Controller.get('mailWriteToMeLogic').fileAttach[i].file[0].size; 
	    					var inputDTO = {
	    		    				"dto": {
	    		    		               "workspace_id": workspaceManager.getMySpaceId(),
	    		    		                "channel_id": mailData.curBox.ACCOUNT_ID,
	    		    		                "storageFileInfo": {
	    		    		                    "user_id": JSON.parse(sessionStorage.getItem('userInfo')).userId,
	    		    		                    "file_last_update_user_id": JSON.parse(sessionStorage.getItem('userInfo')).userId,
	    		    		                    "file_id": "",
	    		    		                    "file_name": file_name,
	    		    		                    "file_extension": file_extension,
	    		    		                    "file_created_at": "",
	    		    		                    "file_updated_at": "",
	    		    		                    "file_size": file_size,
	    		    		                    "user_context_1": "",
	    		    		                    "user_context_2": "",
	    		    		                    "user_context_3": ""
	    		    		                }
	    		    		           }
	    		    				
	    		    		}
	    					
	    					storageManager.UploadFile(Top.Controller.get('mailWriteToMeLogic').fileAttach[i].file[0],
		    						inputDTO, 'Calendar', "Calendar?action=TempStorages",  function(response){
								if (response.dto.resultMsg!= "Success"){
									Top.Dom.selectById('mailWriteToMeLayout_Button_Save').setProperties({'disabled' : 'false'});
									Top.Loader.stop();
									  return; 
								}
								var tmp = response.dto.storageFileInfoList[0];
								mailAttachment.push({
									"ATTACHMENT_ID":"",
		    			            "MAIL_ID": "",
		    			            "STORAGE_PATH" : "", // 수정필요
		    			            "USER_ID" : userManager.getLoginUserId(),
		    			            "WS_ID" : workspaceManager.getMySpaceId(),
		    			            "EXTENSION" : tmp.file_extension,
		    			            "FILE_NAME" : tmp.file_name,
		    			            "TDRIVE_CH_ID" : Top.Dom.selectById('mailMainLayout_Title_Icon').getProperties('value'), 
		    			            "FILE_ID" : tmp.file_id,
		    			            "FILE_SIZE" : tmp.file_size										
								});
								callback();
							}, function(){});
		    				
		    			}
	    				else {
	    					
	    					var file_name = Top.Controller.get('mailWriteToMeLogic').fileAttach[i].file_name;
	    					var file_extension = Top.Controller.get('mailWriteToMeLogic').fileAttach[i].file_extension;
	    					var file_size = Top.Controller.get('mailWriteToMeLogic').fileAttach[i].size;
	    					storageManager.CopyFile("Deep", Top.Controller.get('mailWriteToMeLogic').fileAttach[i].file_id,
	    							mailData.curBox.ACCOUNT_ID,
	    							Top.Controller.get('mailWriteToMeLogic').fileAttach[i].file_name, Top.Controller.get('mailWriteToMeLogic').fileAttach[i].file_extension,
	    							"","","",
	    							function(response){
	    								var tmp = response.dto.storageFileInfoList[0];
	    								mailAttachment.push({
	    									"ATTACHMENT_ID":"",
	    		    			            "MAIL_ID": "",
	    		    			            "STORAGE_PATH" : "", // 수정필요
	    		    			            "USER_ID" : userManager.getLoginUserId(),
	    		    			            "WS_ID" : workspaceManager.getMySpaceId(),
	    		    			            "EXTENSION" : tmp.file_extension,
	    		    			            "FILE_NAME" : tmp.file_name,
	    		    			            "TDRIVE_CH_ID" : Top.Dom.selectById('mailMainLayout_Title_Icon').getProperties('value'), 
	    		    			            "FILE_ID" : tmp.file_id,
	    		    			            "FILE_SIZE" : tmp.file_size			 							
	    								});
	    								callback();
	    								
	    							}, function(){}
	    						)
	    				}
	    			}
	        	}
	    	
			}
    
        
        load_file(function(){
			if (Top.Controller.get('mailWriteToMeLogic').fileAttach.length !=0 &&Top.Controller.get('mailWriteToMeLogic').fileAttach.length != mailAttachment.length) return;
			else{
				var data={
						"dto":{
			    	"MAIL_FLAG" : "SND0001",
					"MailSendTo" : ReceiverArr,
			        "MailSendCc" : [{"TO_ADDR" : ""}],
			        "MailSendBcc" : [{"TO_ADDR" : ""}],
			        "DOMailAttachment":mailAttachment,
			        "FOLDER_ID":"",
			        "SUBJECT" : mailSubject,
			        "SENDER_NAME" : "",
			        "HAS_ATTACHMENT_YN_CODE" : isAttach,
			        "SEEN_YN_CODE" : "",
			        "IMPORTANCE_YN_CODE" : "",
			        "PINNED_YN_CODE" : "",
			        "FILE_NAME" : "",
			        "WRITING_MAIL_ID" : "",
			        "SYNC_STATE" : "",
			        "SOURCE_MAIL_ID" : "",
			        "FILE_SIZE" : "",
			        "DISPOSITION_NOTI" : "",
			        "MESSAGE_ID" : "",
			        "X_TNEF" : "",
			        "SPAM_YN_CODE" : "",
			        "SENDER_ADDR" : Top.Dom.selectById('mailMainLayout_Mail_Text1').getText(),
			        "CONTENT" : context,
			        "CMFLG" : "SendMail_Re",
			        "USER_ID" : userManager.getLoginUserId(),
			        "WS_ID" : workspaceManager.getMySpaceId(),
			        "MESSAGE_HEADER" : "",
			        "EXTRA_INFOS" : "",
			        "FIRST_SAW_TIME" : "", 
			        "SEND_IMPORTANCE_CODE" : importance,
			        "MAIL_ID" : "",
			        "ACCOUNT_ID" : mailData.curBox.ACCOUNT_ID
						}
				};

		
		
		
		Top.Ajax.execute({
			
			type : "POST",
			url :_workspace.url + "Mail/Mail?action=Send",
			dataType : "json",
			data	 : JSON.stringify(data),
			contentType: "application/json; charset=utf-8",
			xhrFields: {withCredentials: true},
			crossDomain : true,
			success : function(ret,xhr,status) {
				Top.Dom.selectById('mailWriteToMeLayout_Button_Save').setProperties({'disabled' : 'false'}); 
				 if (Top.Controller.get('mailWriteToMeLogic').fileAttach.length > 0)
						Top.Loader.stop();

				if (ret.dto != undefined 
						&&(ret.dto.RESULT_MSG == null||ret.dto.RESULT_MSG == '메일유저 없음'||ret.dto.RESULT_MSG == '외부메일입니다. ')) // 수정 요망
					{
					TeeAlarm.open({title: '보내기', content: '죄송합니다.<br>존재하지 않는 메일 주소 입니다.'});
					return ;
					}
				
				Top.Controller.get('mailMainLeftLogic').resetCount();
				
				let sub = appManager.getSubApp();
				if(sub) 
					Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/new/complete' + "?sub=" + sub, {eventType:'fold'});
				else 
					Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/new/complete' + '?' + new Date().getTime(), {eventType:'close'});
				clearInterval(Top.Controller.get('mailWriteToMeLogic').tempInt);
				
			},
			complete : function(ret,xhr,status) {
			
				Top.Dom.selectById('mailWriteToMeLayout_Button_Save').setProperties({'disabled' : 'false'}); 
				if (Top.Controller.get('mailWriteToMeLogic').fileAttach.length > 0)
					Top.Loader.stop();

				
			},
			error : function(ret, xhr,status) {
				Top.Dom.selectById('mailWriteToMeLayout_Button_Save').setProperties({'disabled' : 'false'}); 
				if (Top.Controller.get('mailWriteToMeLogic').fileAttach.length > 0)
					Top.Loader.stop();

			}
			
		});		
		
			}});
		
	
	},
	clickWriteButton : function (event, widget){
		
		
		let sub = appManager.getSubApp();
		if(sub) 
			Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/new' + "?sub=" + sub, {eventType:'fold'});
		else 
			Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/new' + '?' + new Date().getTime(), {eventType:'close'});

		
	},
	attachFile : function (){
		
		if (Top.Controller.get("mailWriteToMeLogic").isFromTDrive == false){ 
			 let fileChooser = Top.Device.FileChooser.create({
				 onBeforeLoad : function(file){
					 
					 return true; 
				 },
				 charset : "euc-kr",
		            onFileChoose : function (fileChooser) {        
		            	Top.Controller.get('mailWriteToMeLogic').attachTable(fileChooser);
		           }
		        });
			 		
				fileChooser.show();
		}
		else{

			Top.Dom.selectById('mailWriteToMeLayout_FileBox').setVisible('visible');
			this.setEditorSize();

			Top.Controller.get('mailWriteToMeLogic').tempTFileId = [];

			var columnOpt = {
					"0" : {},
					"1"	: {},
					"4" : {
						layout: function(data, index) {
							return '<top-icon id="mailWriteToMeLayout_Attach_Delete_'+index+'_0" class="icon-close" text-color="#636363" text-size="11px" on-click="deleteAttach"></top-icon>';}
					}	
					
			};

			
			for(var i = 0 ; i < Top.Dom.selectById("fcFileContentsTableView").getCheckedData().length; i++){
				Top.Controller.get('mailWriteToMeLogic').tempTFileId.push(Top.Dom.selectById("fcFileContentsTableView").getCheckedData()[i].file_id)
			}
			
			for(var i = 0 ; i < Top.Dom.selectById("fcFileContentsTableView").getCheckedData().length; i++){
				this.putTdrivefileAttach(i, this.putTdriveTempfile);
			}
			
			if(Top.Dom.selectById("fcFileContentsTableView").getCheckedData().length != 0 ){
				fileManager.fileChannelId = Top.Dom.selectById('mailMainLayout_Title_Icon').getProperties('value');
				Top.Dom.selectById('mailWriteToMeLayout_FileTable').setProperties({"column-option": columnOpt});
    			mailFileAttachmentRepo.setValue('mailFileAttachmentList',Top.Controller.get('mailWriteToMeLogic').fileList);
			}

			
		}
			


	    }, attachTable : function(data) {
	    	
			Top.Controller.get('mailWriteToMeLogic').tempTFileId = [];
			
			var columnOpt = {
					"0" : {},
					"1"	: {},
					"4" : {
						layout: function(data, index) {
							return '<top-icon id="mailWriteToMeLayout_Attach_Delete_'+index+'_0" class="icon-close" text-color="#636363" text-size="11px" on-click="deleteAttach"></top-icon>';}
					}	
					
			};
			
        	Top.Controller.get('mailWriteToMeLogic').fileAttach.push(data);
        	
			var l = Top.Controller.get('mailWriteToMeLogic').fileAttach.length;
			
			var size = Top.Controller.get('mailWriteToMeLogic').fileAttach[l-1].file[0].size;
			if ((size*1) < 10485760)
				var method = "일반 첨부" ;
			else {
				var method = "대용량 첨부";
				TeeAlarm.open({title: '', content: '10MB 이하의 파일만 첨부 가능합니다.'});
				Top.Controller.get('mailWriteToMeLogic').fileAttach.pop();
				return;
			}


	    	Top.Dom.selectById('mailWriteToMeLayout_FileBox').setVisible('visible');
	    	this.setEditorSize();
	    	
			if ( method == "대용량 첨부" )
				var period = "~ "+mail.after30Days(null,'/')+" (30일간)";
			else
				var period = "제한 없음";

			size = mail.changeSize(size);
			
			Top.Controller.get('mailWriteToMeLogic').fileList.push(
					{
						"name" : Top.Controller.get('mailWriteToMeLogic').fileAttach[l-1].file[0].name,
						"size" : size,
						"attach_method" : method,
						"period" : period
					}
			);
			
			if(Top.Controller.get("mailWriteToMeLogic").saveAtTDrive == false || Top.Controller.get("mailWriteToMeLogic").saveAtTDrive == undefined){
				Top.Controller.get('mailWriteToMeLogic').tempFileIndex.push("0");
			}
			else{
				Top.Controller.get('mailWriteToMeLogic').tempFileIndex.push("2");
			}
        	Top.Controller.get('mailWriteToMeLogic').updateAttachmentInfo(); 

			
			Top.Dom.selectById('mailWriteToMeLayout_FileTable').setProperties({"column-option": columnOpt});
			mailFileAttachmentRepo.setValue('mailFileAttachmentList',Top.Controller.get('mailWriteToMeLogic').fileList);

			
	    },
	    putTdrivefileAttach : function(index, callbackFunc) {
			Top.Controller.get('mailWriteToMeLogic').tempFileIndex.push("1");
			
			
			Top.Controller.get('mailWriteToMeLogic').fileAttach.push(
					{
						"file_name" : Top.Dom.selectById("fcFileContentsTableView").getCheckedData()[index].file_name,
						"file_id" : Top.Dom.selectById("fcFileContentsTableView").getCheckedData()[index].file_id,
						"size" : Top.Dom.selectById("fcFileContentsTableView").getCheckedData()[index].file_size,
						"file_extension" : Top.Dom.selectById("fcFileContentsTableView").getCheckedData()[index].file_extension
					}
			);
			
			Top.Controller.get('mailWriteToMeLogic').updateAttachmentInfo(); 

			
			var size;
			size = Top.Dom.selectById("fcFileContentsTableView").getCheckedData()[index].file_size;
			if ((size*1) < 10485760)
				var method = "일반 첨부" ;
			else{
				var method = "대용량 첨부";
				TeeAlarm.open({title: '', content: '10MB 이하의 파일만 첨부 가능합니다.'}); 
				Top.Controller.get('mailWriteToMeLogic').fileAttach.pop();
				Top.Controller.get('mailWriteToMeLogic').tempFileIndex.pop(); 
				return;

			}

			if ( method == "대용량 첨부" )
				var period = "~ "+mail.after30Days(null,'/')+" (30일간)";
			else
				var period = "제한 없음";

			size = mail.changeSize(size);
			
			Top.Controller.get('mailWriteToMeLogic').fileList.push(
					{
						"name" : Top.Dom.selectById("fcFileContentsTableView").getCheckedData()[index].fileFullName,
						"size" : size,
						"attach_method" : method,
						"period" : period
					}
			);
			
			callbackFunc(index);
    },
    
    putTdriveTempfile : function(index) {
    	storageManager.getDownloadFileInfo(Top.Controller.get('mailWriteToMeLogic').tempTFileId[index], function(result){
		 	var reader = new window.FileReader();
			reader.readAsDataURL(result.blobData); 
			reader.onloadend = function() {
				 var filecontent = reader.result; 
                 var contents;
                 if(Object.prototype.toString.call(filecontent) === "[object ArrayBuffer]"){ //ArrayBuffer
                     var binary = '';
                    var bytes = new Uint8Array( filecontent );
                    var len = bytes.byteLength;
                    for (var j = 0; j < len; j++) {
                        binary += String.fromCharCode( bytes[ j ] );
                    }
                    contents = window.btoa(binary);
                 }
                 else if((filecontent.split("base64,").length == 2) &&
                     RegExp("^(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{4})$").test(filecontent.split("base64,")[1])){ //base64
                         contents = filecontent.split("base64,")[1];
                 }
                 else{ //raw txt
                     contents = btoa(unescape(encodeURIComponent(ctrl.fileAttach[i].src)));
                 }
                 Top.Controller.get("mailWriteToMeLogic").tempTdrivefile.push( {"file_id" : Top.Controller.get('mailWriteToMeLogic').tempTFileId[index], "file_info" : {
                     "filename" : result.fileName,
                     "contents" : contents
                 			}
				 })
			}
		},function(){})	
    },
    saveTemp : function() {
			 
	    	var mailSubject = Top.Dom.selectById('mailWriteToMeLayout_Subject').getText();
			if (!mailSubject){
				mailSubject = '제목 없음';
			}
				
			var ReceiverArr = [{ "TO_ADDR" : Top.Dom.selectById('mailMainLayout_Mail_Text1').getText()}];
			
			
			var importance;
			if (Top.Dom.selectById('mailWriteToMeLayout_CheckBox_Importance').isChecked() == true)
				importance = 'IMP0001';
			else
				importance = 'IMP0002';
			
			
			var spaceremove = Top.Dom.selectById('mailWriteToMeLayout_WorkArea_HtmlEditor').getValue();//mailSunEditor.getContents();
	        spaceremove = [].filter.call(spaceremove, function (c) {return c.charCodeAt() !== 8203;}).join('');
	        var context = spaceremove;
	        context = '<style> table { width: 100%; border-spacing : 0px } td, th { border: 1px solid #e1e1e1 } </style>' + context;
			var data={
					"dto":{
							"MailSendTo" 				: ReceiverArr,
							"MailSendCc" 				: [{"TO_ADDR" : ""}],
						    "MailSendBcc" 				: [{"TO_ADDR" : ""}],
					        "ACCOUNT_ID"				: mailData.curBox.ACCOUNT_ID,
					        "MAIL_ID"					: this.tempMailId,
					        "MAIL_GROUP_INDEX"			: this.tempGroupIndex,
					        "CMFLG" 					: "CreateTempMail",
					        "FOLDER_ID"					: "",
					        "SUBJECT" 					: mailSubject,
					        "SENDER_NAME" 				: "",
					        "HAS_ATTACHMENT_YN_CODE" 	: 'COM0002',
					        "SEEN_YN_CODE" 				: "",
					        "IMPORTANCE_YN_CODE" 		: "",
					        "PINNED_YN_CODE" 			: "",
					        "FILE_NAME" 				: "",
					        "WRITING_MAIL_ID" 			: "",
					        "SYNC_STATE" 				: "",
					        "SOURCE_MAIL_ID" 			: "",
					        "FILE_SIZE" 				: "",
					        "DISPOSITION_NOTI" 			: "",
					        "MESSAGE_ID" 				: "",
					        "X_TNEF" 					: "",
					        "SPAM_YN_CODE" 				: "",
					        "SENDER_ADDR" 				: Top.Dom.selectById('mailMainLayout_Mail_Text1').getText(),
					        "CONTENT" 					: context,
					        "USER_ID" 					: userManager.getLoginUserId(),
					        "WS_ID" 					: workspaceManager.getMySpaceId(),
					        "MESSAGE_HEADER" 			: "",
					        "EXTRA_INFOS" 				: "",
					        "FIRST_SAW_TIME" 			: "", 
					        "SEND_IMPORTANCE_CODE" 		: importance
					        
				    }
			};
				
			

				
				
			Top.Ajax.execute({
					
					type : "POST",
					url :_workspace.url + "Mail/Mail?action=CreateTemp",
					dataType : "json",
					data	 : JSON.stringify(data),
					contentType: "application/json; charset=utf-8",
					xhrFields: {withCredentials: true},
					crossDomain : true,
					success : function(ret,xhr,status) {
					
						if(ret.dto.MAIL_ID != null && ret.dto.GROUP_INDEX != null) {
							Top.Controller.get('mailWriteToMeLogic').tempMailId = ret.dto.MAIL_ID;
							Top.Controller.get('mailWriteToMeLogic').tempGroupIndex = ret.dto.GROUP_INDEX;
						}
						Top.Controller.get('mailMainLeftLogic').resetTempCount();
						
						Top.Dom.selectById('mailWriteToMeLayout_TempTime').setText('임시 보관함에 저장하였습니다. ' + mail.tempTime());
						

						var media = window.matchMedia("screen and (max-device-width: 1024px)");
						
						if(media.matches) {
							$('div#mailWriteToMeLayout_Button_Right').removeClass('mail-time-init');
							$('div#mailWriteToMeLayout_Button_Right').addClass('mail-time-display');
						}
						
					},
					complete : function(ret,xhr,status) {
					

						
					},
					error : function(ret, xhr,status) {

					}
					
				});
				 
			 
		 },
		 
		 deleteAttach : function (event, widget) {
			 
				var k = widget.getParent().getSelectedIndex();;
				
				if(Top.Controller.get('mailWriteToMeLogic').fileAttach.length == 1){
					Top.Controller.get('mailWriteToMeLogic').fileAttach = [];
					Top.Controller.get('mailWriteToMeLogic').fileList = [];
					Top.Controller.get('mailWriteToMeLogic').tempFileIndex = [];
					Top.Controller.get('mailWriteToMeLogic').updateAttachmentInfo(); 

				}
				else{
					if(Top.Controller.get('mailWriteToMeLogic').tempFileIndex[k] == "1"){
						for(var i = 0 ; i < Top.Controller.get("mailWriteToMeLogic").tempTdrivefile.length;i++ ){
							if (Top.Controller.get("mailWriteToMeLogic").tempTdrivefile[i].file_id == Top.Controller.get('mailWriteToMeLogic').fileAttach[k].file_id ){
								Top.Controller.get("mailWriteToMeLogic").tempTdrivefile.splice(i,1);
							}
						}
						
					}
					Top.Controller.get('mailWriteToMeLogic').fileAttach.splice(k,1);
					Top.Controller.get('mailWriteToMeLogic').fileList.splice(k,1);
					Top.Controller.get('mailWriteToMeLogic').tempFileIndex.splice(k,1);
					Top.Controller.get('mailWriteToMeLogic').updateAttachmentInfo(); 

				}
				
 			mailFileAttachmentRepo.setValue('mailFileAttachmentList',Top.Controller.get('mailWriteToMeLogic').fileList);
				 
			 },
			 callMail : function ()
				{
					
					
						Top.Ajax.execute({
							
							type : "GET",
							url : _workspace.url + "Mail/Mail?action=Read&CMFLG=ReadMail&MAIL_ID=" + Top.Controller.get('mailMainLeftLogic').clickedMail+"&USER_ID="+userManager.getLoginUserId(),
							dataType : "json",
							crossDomain: true,
							contentType: "application/json; charset=utf-8",
						    xhrFields: {withCredentials: true},
							success : function(ret,xhr,status) {
								

								
								if (!ret.dto.CONTENT)
									var content = "";
								else
									var content = ret.dto.CONTENT;
								
								
								if (Top.Controller.get('mailReadButtonLogic').modifyClicked == true){
									Top.Dom.selectById('mailWriteToMeLayout_Subject').setText(ret.dto.SUBJECT);
									setTimeout(function(){Top.Dom.selectById('mailWriteToMeLayout_WorkArea_HtmlEditor').setEditorHTML(ret.dto.CONTENT);});
								}
								
								
								Top.Controller.get('mailReadButtonLogic').modifyClicked = null;

							}
						});
						
						
				
				},
				modifyMail : function () {
					
					var mailSubject = Top.Dom.selectById('mailWriteToMeLayout_Subject').getText();
					if (!mailSubject){
						mailSubject = '제목 없음';
					}
					
					
					var ReceiverArr = [{ "TO_ADDR" : Top.Dom.selectById('mailMainLayout_Mail_Text1').getText()}];
					
					var importance;
					if (Top.Dom.selectById('mailWriteToMeLayout_CheckBox_Importance').isChecked() == true)
						importance = 'IMP0001';
					else
						importance = 'IMP0002';
					

					var spaceremove = Top.Dom.selectById('mailWriteToMeLayout_WorkArea_HtmlEditor').getValue();//mailSunEditor.getContents();
			        spaceremove = [].filter.call(spaceremove, function (c) {return c.charCodeAt() !== 8203;}).join('');
			        var context = spaceremove;
			        context = '<style> table { width: 100%; border-spacing : 0px } td, th { border: 1px solid #e1e1e1 } </style>' + context;
			        
			        var mailAttachment = [] ; 
					var isAttach;
				
			        function load_file(callback) {
						
						if (Top.Controller.get('mailWriteToMeLogic').fileAttach.length ==0)
			    		{
			    		isAttach = 'COM0002';
			    		mailAttachment = [{
				            "ATTACHMENT_ID":"",
				            "MAIL_ID": Top.Controller.get('mailMainLeftLogic').clickedMail,
				            "STORAGE_PATH" : "", 
				            "USER_ID" : "",
				            "WS_ID" : "",
				            "EXTENSION" : "",
				            "FILE_NAME" : "",
				            "TDRIVE_CH_ID" : "", 
				            "FILE_ID" : "",
				            "FILE_SIZE" : ""
				            
				        }];
			    		callback();
			    		}
			    	
			    	else{
			    		isAttach = 'COM0001'
			    			for (var i = 0; i < Top.Controller.get('mailWriteToMeLogic').fileAttach.length; i++)
			    				{
			    				
			    				fileManager.uploadFile(Top.Controller.get('mailWriteToMeLogic').fileAttach[i],'/mail_Attachment', function(response){
									if (response.result != "OK"){
										return; 
									}
									var tmp = response.fileMetaList[0];
									mailAttachment.push({
										"ATTACHMENT_ID":"",
			    			            "MAIL_ID": Top.Controller.get('mailMainLeftLogic').clickedMail,
			    			            "STORAGE_PATH" : tmp.file_parent_path, 
			    			            "USER_ID" : userManager.getLoginUserId(),
			    			            "WS_ID" : workspaceManager.getMySpaceId(),
			    			            "EXTENSION" : tmp.file_extension,
			    			            "FILE_NAME" : tmp.file_name,
			    			            "TDRIVE_CH_ID" : Top.Dom.selectById('mailMainLayout_Title_Icon').getProperties('value'), 
			    			            "FILE_ID" : tmp.file_id,
			    			            "FILE_SIZE" : tmp.file_size										
									});
									callback();
								}, function(){});
			    				}
			        	}
			    	
					}
			    	
			    
			        
			        load_file(function(){
						if (Top.Controller.get('mailWriteToMeLogic').fileAttach.length !=0 &&Top.Controller.get('mailWriteToMeLogic').fileAttach.length != mailAttachment.length) return;
						else{
							var data={
									"dto":{
						        "DOMailAttachment":mailAttachment,
						        "MAIL_ID" : Top.Controller.get('mailMainLeftLogic').clickedMail,
						        "SUBJECT" : mailSubject,
						        "CONTENT" : context,
						        "HAS_ATTACHMENT_YN_CODE" : isAttach,
						        "CMFLG" : "MyMailUpdate"
						        }
							};

					

					
					
					Top.Ajax.execute({
						
						type : "POST",
						url :_workspace.url + "Mail/Mail?action=MyMailUpdate",
						dataType : "json",
						data	 : JSON.stringify(data),
						contentType: "application/json; charset=utf-8",
						xhrFields: {withCredentials: true},
						crossDomain : true,
						success : function(ret,xhr,status) {
						
							
							
							Top.Controller.get('mailMainLeftLogic').resetCount();
							
							let sub = appManager.getSubApp();
							if(sub) 
								Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/newtome/complete' + "?sub=" + sub, {eventType:'fold'});
							else 
								Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/newtome/complete' + '?' + new Date().getTime(), {eventType:'close'});

							clearInterval(Top.Controller.get('mailWriteToMeLogic').tempInt);
							
						},
						complete : function(ret,xhr,status) {
						
					
							
						},
						error : function(ret, xhr,status) {
						}
						
					});		
					
						}});
					
					
					
				},
				 updateAttachmentInfo : function(){
					 
					 	var normalCount=0;
						var normalSize=0;
						var bigCount=0;
						var bigSize=0;
						
						for (var i = 0; i < Top.Controller.get('mailWriteToMeLogic').fileAttach.length; i++ )
							{
							if (Top.Controller.get('mailWriteToMeLogic').tempFileIndex[i] == "0"){
								var size = Top.Controller.get('mailWriteToMeLogic').fileAttach[i].file[0].size;
							}
							else
								var size = Top.Controller.get('mailWriteToMeLogic').fileAttach[i].size; 
							
							if (size*1 < 10485760){
								normalCount = normalCount +1;
								normalSize = normalSize + (size*1);
								}
							else{
								bigCount = bigCount +1;
								bigSize = bigSize + (size*1);
							}		
							}
						
						normalSize = mail.changeSize(normalSize);
						bigSize = mail.changeSize(bigSize);
						
						var info = "일반 " + normalSize + " / 10MB ㅣ 대용량	 " + bigSize + " / 2.00GB x 10개" ;
						Top.Dom.selectById('mailWriteToMeLayout_Attachment_Info').setText(info);
						
				 },
				 manageByte : function(event, widget){
					 
					 if (widget.getText() == undefined || widget.getText().length == 0)
						 return; 
					 
					 var calByte = {
								getByteLength : function(s) {

									if (s == null || s.length == 0) {
										return 0;
									}
									var size = 0;

									for ( var i = 0; i < s.length; i++) {
										size += this.charByteSize(s.charAt(i));
									}

									return size;
								},
									
								cutByteLength : function(s, len) {

									if (s == null || s.length == 0) {
										return 0;
									}
									var size = 0;
									var rIndex = s.length;

									for ( var i = 0; i < s.length; i++) {
										size += this.charByteSize(s.charAt(i));
										if( size == len ) {
											rIndex = i + 1;
											break;
										} else if( size > len ) {
											rIndex = i;
											break;
										}
									}

									return s.substring(0, rIndex);
								},

								charByteSize : function(ch) {

									if (ch == null || ch.length == 0) {
										return 0;
									}

									var charCode = ch.charCodeAt(0);

									if (charCode <= 0x00007F) {
										return 1;
									} else if (charCode <= 0x0007FF) {
										return 2;
									} else if (charCode <= 0x00FFFF) {
										return 3;
									} else {
										return 4;
									}
								}
							};
					 
					  var result = calByte.cutByteLength(widget.getText(), 250);
					  widget.setText(result);
				 }
		
})


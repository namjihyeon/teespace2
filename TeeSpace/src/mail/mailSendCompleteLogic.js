Top.Controller.create('mailSendCompleteLogic', {
	init : function() {
		

		
	},
	clickedReceiveMailBox : function() {
		
		var media = window.matchMedia("screen and (max-device-width: 1024px)");
		
		if(media.matches)
			mailData.panelType = '3';
		
		var folderid;
		var senderAcc = Top.Controller.get('mailWriteLogic').sendArr;
		for(var i = 1; i<mailData.accountList.length; i++)
		{
		    if(senderAcc == mailData.accountList[i].EMAIL_ADDRESS){
		       for(var j=0; j<mailData.accountList[i].FolderList.length;j++){
		            if("받은 편지함" == mailData.accountList[i].FolderList[j].DISPLAY_NAME){
		                   folderid = mailData.accountList[i].FolderList[j].FOLDER_ID;
		            }
		       }
		    }
		}
		
		let sub = appManager.getSubApp();
		if(sub) 
			Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + folderid + "?sub=" + sub, {eventType:'fold'});
		else 
			Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + folderid , {eventType:'close'});
		
		
	},
	clickedNewMail : function() {
		let sub = appManager.getSubApp();
		if(sub) 
			Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/new' + '?' + new Date().getTime() + "?sub=" + sub, {eventType:'fold'});
		else 
			Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/new' + '?' + new Date().getTime() , {eventType:'close'});
		
		
	}
})

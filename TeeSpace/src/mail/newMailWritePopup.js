const newMailWritePopup = (() => {
	let receiverAddr = [];
	let CCAddr = [];
	let BCCAddr = [];
	let autocomplete = [];
    let _rootElement = null;
    const _appendTarget = "body";

    const _createDialog = () => {
        const element = document.createElement("div");
        element.setAttribute("id", "newMailWrite__dialog");

        const template__header = `<div id="newMailWrite__dialog__header">
		                              <div style="display:inline-block;align-items:center;width:100%;text-align:center;">
		                                  <span id="newMailWrite__progress-text">새 편지</span>
		                              </div>

		                              <div style="display:flex;align-items:center;font-size:0.75rem;color:#ffffff;">
		                                  <span class="icon-work_close newMailWrite__expand-button"></span>
		                                  <span class="icon-work_open newMailWrite__collapse-button"></span>
		                                  <span class="icon-work_cancel newMailWrite__close-button"></span>
		                              </div>
                          	  		</div>
                          	  	<div id="newMailWrite__dialog__content">`+__template__html()+`
                          	  	</div>`;
        
        
//        const element1 = document.createElement("div");
//        element1.setAttribute("id", "newMailWrite__dialog__content");
//        var editor = Top.Widget.create('top-htmleditor');
//        editor.setProperties({'id':'html__editor'})
//        element1.insertAdjacentHTML("afterbegin", editor.template.outerHTML);
        
        element.insertAdjacentHTML("afterbegin", template__header);
        
        /*const _selectboxNodes = [];
        if(mailData.accountList.length != 0) {
			 for(var i=1; i<mailData.accountList.length; i++) {
				 _selectboxNodes.push({
					 'text' : mailData.accountList[i].EMAIL_ADDRESS,
					 'value' : mailData.accountList[i].ACCOUNT_ID
				 });
			 }
		 } */
        const _selectboxNodes = [
        	{text: "tmax2019164@naver.com", value: "fcc6b30b-8bd0-4d64-b705-cc4f67c3bb52"},
        	{text: "jihyeon_nam@tmax.co.kr", value: "b50a318a-865d-42d9-b576-cafa0d3507f9"},
        	{text: "tmax2016194@naver.com", value: "8192eff6-b09c-4aa2-8df7-9a10ce61b74b"}
        	];
		const selectbox = Top.Widget.create("top-selectbox", {
	        id: "newMailWrite__senderaccount__search__selectbox",
	        layoutHeight: "1.875rem",
	        nodes: _selectboxNodes,
	        selectedIndex: "0",
	        onChange: _onFilterChange
	    }).template;
	    selectbox.selectedIndex = 0;
        element.querySelector("#LinearLayout521_1_Private1").insertAdjacentElement("beforeend", selectbox);
        
        set_auto_complete();
       console.log(Top.Controller.get('mailMainLeftLogic').autoComplete);
    	_set__rec__widget(element);
    	_set__cc__widget(element);
    	_set__bcc__widget(element);
    	
    	const checkbox = Top.Widget.create("top-checkbox", {
	        id: "CheckBox_Importance",
	        layoutHeight: "1.875rem",
	        onChange: _onCheckboxChange
	    }).template;
    	element.querySelector("span#TextView583_1_1_2_1").insertAdjacentElement("afterend", checkbox);
    	
    	const menu = Top.Widget.create("top-button", {
    		"id" : "mailWriteLayout_AttachFile",
            "layout-width": "70px",
            "border-radius": "15px",
            "layout-height": "27px",
            "className": "text-line",
            "text":"파일첨부",
            "layout-tab-disabled": false,
            "onClick":_onclick__attachmenu,
            "style" : "text-align: center;"
	    }).template;
    	element.querySelector("div#LinearLayout23").insertAdjacentElement("afterbegin", menu);
    	
    	/*let htmlEditor = Top.Widget.create('top-htmleditor');
    	htmlEditor.setProperties({'id':'htmleditor1','layout-width':'600px',
    		'layout-height':'350px',
    		'format':true,
    		'color':true,
    		'bold':true,
    		'right':true,
    		"number-list":true,
    		"bullet-list":true,
    		"sub-script":true,
    		"super-script":true,
    		"indent":true,
    		"outdent":true,
    		"left":true,
    		"conter":true,
    		"strike":true,
    		"link":true,
    		"unlink":true,"clean":true,
    		"rule":true,"source":true,"print":true,
    		"save":true,"redo":true,"undo":true,
    		"clear":true,"cut":true,"copy":true,"paste":true,});
    	element.querySelector("div#LinearLayout23").insertAdjacentElement("afterbegin", menu);*/
    	_setEventBinding(element);
        
        return element;
    };
    
    const _set__rec__widget = (element) => {
    	const tagChip = _createWidget__chip__mail("newmailwritePopup__receiverAddress__chip");
     	element.querySelector("#mailWriteLayout_Receiver_Layout").insertAdjacentElement("beforeend", tagChip);
     	
     	let worktree__button = _createWidget__button__worktree("mailWriteLayout_recAddressBookButton__workTree");
     	element.querySelector("#mailWriteLayout_recEmpty").insertAdjacentElement("afterend", worktree__button);
     	
     	let recAddressBook__button = _createWidget__button__address("mailWriteLayout_recAddressBookButton");
     	element.querySelector("#mailWriteLayout_recAddressBookButton__workTree").insertAdjacentElement("afterend", recAddressBook__button);
    };
    const _set__cc__widget = (element) => {
   	 	const tagChip = _createWidget__chip__mail("newmailwritePopup__ccAddress__chip");
    	element.querySelector("#FlowLayout315_1_1_1_1").insertAdjacentElement("beforeend", tagChip);
    	
    	let worktree__button = _createWidget__button__worktree("mailWriteLayout_ccAddressBookButton__workTree");
    	element.querySelector("#mailWriteLayout_ccEmpty").insertAdjacentElement("afterend", worktree__button);
    	
    	let recAddressBook__button = _createWidget__button__address("mailWriteLayout_recAddressBookButton");
    	element.querySelector("#mailWriteLayout_ccAddressBookButton__workTree").insertAdjacentElement("afterend", recAddressBook__button);
   };
   const _set__bcc__widget = (element) => {
  	 	const tagChip = _createWidget__chip__mail("Text_HiddenCc");
	   	element.querySelector("#FlowLayout315_1_1_1_1_1").insertAdjacentElement("beforeend", tagChip);
	   	
	   	let worktree__button = _createWidget__button__worktree("mailWriteLayout_bccAddressBookButton__workTree");
	   	element.querySelector("#mailWriteLayout_bcEmpty").insertAdjacentElement("afterend", worktree__button);
	   	
	   	let recAddressBook__button = _createWidget__button__address("mailWriteLayout_bccAddressBookButton");
	   	element.querySelector("#mailWriteLayout_bccAddressBookButton__workTree").insertAdjacentElement("afterend", recAddressBook__button);
  };
    const _createWidget__chip__mail = (id) => {
	    	return Top.Widget.create('top-chip',{
	    		"id" : id,
	             "backgroundColor":"#FFFFFF",
                    "className":"mail-chip",
                    "layoutHeight":"auto",
                    "layoutWidth":"100%",
                    "padding":"0",                    
                    "minHeight":"1.88rem",
                    "auto-complete":Top.Controller.get('mailMainLeftLogic').autoComplete.join(','),
                    "backspace":"true",
					"auto-complete-max-height":"210px",
					"support-empty":"false",
                    	"on-keyup":checkSemicolon,
                    	"on-select":autoCompleteChipItem,
                    	"on-close":closeChipItem,
                    	"on-add":addReceiver,
                    	"on-blur":blurAddress
	         }).template;
    };
	
    const _createWidget__button__worktree = (id) => {
			return Top.Widget.create('top-button',{
				"id" : id,
	            "layout-width": "56px",
	            "layout-height": "27px",
	            "className": "text-line",
	            "text":"조직도",
	            "layout-tab-disabled": false,
	            "onClick":woketree,
	            "style" : "text-align: center;"
	         }).template;
    };
    const _createWidget__button__address = (id) => {
    		return Top.Widget.create('top-button',{
		     		"id" : id,
		             "layout-width": "56px",
		             "layout-height": "27px",
		             "className": "text-line",
		             "text":"주소록",
		             "layout-tab-disabled": false,
		             "onClick":addressBook,
		             "style" : "text-align: center;"
		         }).template;
    };
    const _setEventBinding = (element) => {
	    	element.querySelector(".newMailWrite__button__send").addEventListener("click", clickSend);						//보내기
	        element.querySelector(".newMailWrite__button__preview").addEventListener("click", clickPreview);				//미리보기
	        element.querySelector(".newMailWrite__button__tempsave").addEventListener("click", clickTempSave);				//임시저장
    	
	        
    		element.querySelector(".newMailWrite__expand-button").addEventListener("click", expand);						//펼치기
    		element.querySelector(".newMailWrite__collapse-button").addEventListener("click", collapse);					//접기
    		element.querySelector(".newMailWrite__close-button").addEventListener("click", close);							//닫기
          
    };
   
    const __template__html = () => {
    	var html = `<div id="mailWriteLayout" class="top-linearlayout-root" style="width: calc(100% - 0px); height: calc(100% - 0px); border-width: 0px; border-style: solid; text-align: left; vertical-align: top; overflow: hidden auto;">
    				`+__template__html__button()+`
    				`+__template__html__info()+`
    				`+__template__html__receiver()+`
    				`+__template__html__cc()+`
    				`+__template__html__bcc()+`
    				`+__template__html__subject()+`
    				`+__template__html__fileattach()+`
    				</div>`;
    	return html;
    };
    const __template__html__fileattach = () => { 
    	var fileattach = `<div id="LinearLayout514_6" class="top-linearlayout-root" style="display:flex; white-space: nowrap; height: 30px; margin: 5px 0px 12px; border-width: 0px; border-style: solid; vertical-align: middle; overflow: hidden; width: 100%; line-height: 30px;">
							<div id="mailWriteLayout_Info_Account__1_2_1_1" class="top-linearlayout-root" style="white-space: nowrap; width: 137px; border-width: 0px; border-style: solid; vertical-align: middle; overflow: hidden; height: 100%; line-height: 30px;">
									<span id="TextView583_1_1_2_1_1" class="top-textview-root" style="width: auto; height: auto; font-weight: bold; margin: 0px 86px 0px 0px; font-size: 12px; border-width: 0px; border-style: solid; cursor: default; text-align: left; vertical-align: middle;"><a class="top-textview-url" style="cursor: default;">파일 첨부</a><div class="top-textview-aligner middle" style="cursor: default;"></div></span>
							</div>
							<div id="LinearLayout23" class="top-linearlayout-root" style="display:flex; white-space: nowrap; width: calc(100% - 8.5rem); border-width: 0px; border-style: solid; vertical-align: middle; overflow: hidden; height: 100%; line-height: 30px;">
								
								<div id="LinearLayout69" class="top-linearlayout-root" style="border-width: 0px; border-style: solid; text-align: left; vertical-align: middle; overflow: hidden; height: 100%; width: calc(100% - 72px);">
											<span id="mailWriteLayout_Attachment_Info" class="top-textview-root" style="width: 100%; height: 100%; color: rgb(128, 128, 128); font-size: 12px; border-width: 0px; border-style: solid; text-align: right; vertical-align: top;"><a class="top-textview-url"></a><div class="top-textview-aligner middle"></div></span>
								</div>
							</div>
					</div>`;
    	return fileattach;
    }
    const __template__html__subject = () => { 
    	var subject = `	<div id="LinearLayout514_5" class="top-linearlayout-root" style="display:flex; white-space: nowrap; height: 100px; margin: 5px 0px 0px; border-width: 0px; border-style: solid; vertical-align: top; overflow: hidden; width: 100%; line-height: 27px;">
								<div id="mailWriteLayout_Info_Account__1_2_1" class="top-linearlayout-root" style="white-space: nowrap; width: 137px; border-width: 0px; border-style: solid; vertical-align: middle; overflow: hidden; height: 100%; line-height: 27px;">
									<span id="TextView583_1_1_2_1" class="top-textview-root" style="width: auto; height: auto; font-weight: bold; font-size: 12px; border-width: 0px; border-style: solid; cursor: default; text-align: left; vertical-align: middle;"><a class="top-textview-url" style="cursor: default;">제목</a><div class="top-textview-aligner middle" style="cursor: default;"></div></span>
									
									<span id="TextView1004" class="top-textview-root" style="width: auto; height: auto; margin: 0px 0px 0px 5px; font-size: 12px; border-width: 0px; border-style: solid; cursor: default; text-align: center; vertical-align: middle;"><a class="top-textview-url" style="cursor: default;">중요</a><div class="top-textview-aligner middle" style="cursor: default;"></div></span>
									<span id="TextView1025" class="top-textview-root" style="width: auto; height: auto; color: rgb(255, 0, 0); font-size: 12px; border-width: 0px; border-style: solid; cursor: default; text-align: center; vertical-align: middle;"><a class="top-textview-url" style="cursor: default;">!</a><div class="top-textview-aligner middle" style="cursor: default;"></div></span>
								</div>
								<div id="mailWriteLayout_LinearLayout797" class="top-linearlayout-root" style="width: calc(100% - 10rem); border-width: 0px; border-style: solid; text-align: left; vertical-align: middle; overflow: hidden; height: 100%;">
									<div class="top-textfield-root" style="display: inline-block; width: 100%; height: 25px; vertical-align: top;">
										<form autocomplete="off" onsubmit="return false;">
											<input id="Text_Subject" class="top-textfield-text pl-0 pr-0" type="text" maxlength="250" style="width:100%; border-width: 1px; border-style: solid; border-color: rgb(172, 176, 188);" tabindex="0">
											<span class="top-textfield-icon" style="display: none; vertical-align: baseline;"></span>
										</form>
									</div>
								</div>
					</div>`;
    	return subject;
    }
    const __template__html__bcc = () => {
    	var bcc = `	<div id="mailWriteLayout_Info_BCC" class="top-linearlayout-root" style="display:flex; white-space: nowrap; height: auto; margin: 5px 0px 0px; border-width: 0px; border-style: solid; vertical-align: top; overflow: hidden; width: 100%;">
							<div id="mailWriteLayout_Info_Account__1_2" class="top-linearlayout-root" style="white-space: nowrap; width: 137px; height: auto; border-width: 0px; border-style: solid; vertical-align: middle; overflow: hidden;">
									<span id="TextView583_1_1_2" class="top-textview-root" style="width: auto; height: auto; font-weight: bold; margin: 0px 86px 0px 0px; font-size: 12px; border-width: 0px; border-style: solid; text-align: left; vertical-align: middle;"><a class="top-textview-url">숨은 참조</a><div class="top-textview-aligner middle"></div></span>
							</div>
							<div class="top-flowlayout-root" id="FlowLayout315_1_1_1_1_1" style="width: calc(100% - 13rem); height: auto; margin: 0px 0px 3px; border-width: 1px; border-style: solid; border-color: rgb(172, 176, 188); border-radius: 25px; text-align: left; vertical-align: top; overflow: hidden;">
							</div>
							<div id="mailWriteLayout_bcEmpty" class="top-linearlayout-root" style="width: 0px; border-width: 0px; border-style: solid; text-align: left; vertical-align: top; overflow: hidden;">
							</div>
							<span class="top-icon-root" id="mailWriteLayout_BCC_Error_Icon" style="visibility: hidden; width: auto; height: auto; margin: 0px 0px 0px 5px; color: rgb(255, 0, 0); font-size: 14px; border-width: 0px; border-style: solid; vertical-align: middle;">
							<i class="top-icon-content" style="line-height: 14px;">
						</i></span>
									</div>`;
    	return bcc;
    };
    const __template__html__cc = () => {
    	var cc = `	<div id="mailWriteLayout_LinearLayout514_3" class="top-linearlayout-root" style="display:flex; white-space: nowrap; height: auto; margin: 5px 0px 0px; border-width: 0px; border-style: solid; vertical-align: top; overflow: hidden; width: 100%; line-height: 30px;">
							<div id="mailWriteLayout_Info_Account__1_1" class="top-linearlayout-root" style="white-space: nowrap; width: 137px; height: auto; border-width: 0px; border-style: solid; vertical-align: middle; overflow: hidden; line-height: 26px;">
									<span id="TextView583_1_1_1" class="top-textview-root" style="width: auto; height: auto; font-weight: bold; margin: 0px; font-size: 12px; border-width: 0px; border-style: solid; cursor: default; text-align: left; vertical-align: middle;"><a class="top-textview-url" style="cursor: default;">참조</a><div class="top-textview-aligner middle" style="cursor: default;"></div></span>
									<div id="mailWriteLayout_LinearLayout56_1" class="top-linearlayout-root" style="white-space: nowrap; width: auto; height: auto; margin: -2px 0px 0px 25px; border-width: 0px; border-style: solid; vertical-align: top; overflow: hidden; line-height: 19px;">
										<top-icon id="mailReadLayout_Info_Foldable_Icon_1" class="icon-accordion_select linear-child-horizontal" cursor="pointer" layout-width="auto" layout-height="auto" layout-vertical-alignment="top" margin="0px 0px 0px 0px" border-width="0px" on-click="foldBCC" text-size="13px" text-color="#8C8E92" title="" layout-tab-disabled="false" style="line-height: normal;">
										<span class="top-icon-root" id="mailReadLayout_Info_Foldable_Icon_1" style="width: auto; height: auto; margin: 0px; color: rgb(140, 142, 146); font-size: 13px; border-width: 0px; border-style: solid; cursor: pointer; vertical-align: top;"><i class="top-icon-content" style="cursor: pointer; line-height: 13px;">
										</i></span></top-icon>
									</div>
    						</div>
							<div class="top-flowlayout-root" id="FlowLayout315_1_1_1_1" style="width: calc(100% - 13rem); height: auto; margin: 0px 0px 3px; border-width: 1px; border-style: solid; border-color: rgb(172, 176, 188); border-radius: 25px; text-align: left; vertical-align: top; overflow: hidden;">
    						</div>
							<div id="mailWriteLayout_ccEmpty" class="top-linearlayout-root" style="width: 0px; border-width: 0px; border-style: solid; text-align: left; vertical-align: top; overflow: hidden;">
													</div>
						<span class="top-icon-root" id="mailWriteLayout_CC_Error_Icon" style="visibility: hidden; width: auto; height: auto; margin: 0px 0px 0px 5px; color: rgb(255, 0, 0); font-size: 14px; border-width: 0px; border-style: solid; vertical-align: middle;"><i class="top-icon-content" style="line-height: 14px;">
						</i></span>
									</div>`;
    	return cc;
    };
    const __template__html__receiver = () => {
    	var receiver = `	<div id="mailWriteLayout_LinearLayout514_2" class="top-linearlayout-root" style="display:flex; white-space: nowrap; height: auto; margin: 8px 0px 0px; border-width: 0px; border-style: solid; vertical-align: top; overflow: hidden; width: 100%; line-height: 30px;">
								<div id="mailWriteLayout_Info_Account__1" class="top-linearlayout-root" style="white-space: nowrap; width: 137px; height: auto; border-width: 0px; border-style: solid; vertical-align: middle; overflow: hidden; line-height: 26px;">
										<span id="TextView583_1_1" class="top-textview-root" style="width: auto; height: auto; font-weight: bold; margin: 0px 86px 0px 0px; font-size: 12px; border-width: 0px; border-style: solid; cursor: default; text-align: left; vertical-align: middle;"><a class="top-textview-url" style="cursor: default;">받는 사람</a><div class="top-textview-aligner middle" style="cursor: default;"></div></span>
								</div>
								<div class="top-flowlayout-root" id="mailWriteLayout_Receiver_Layout" style="width: calc(100% - 13rem); height: auto; margin: 0px 0px 3px; border-width: 1px; border-style: solid; border-color: rgb(172, 176, 188); border-radius: 25px; text-align: left; vertical-align: top; overflow: hidden;">
								</div>
	    						<div id="mailWriteLayout_recEmpty" class="top-linearlayout-root" style="width: 0px; border-width: 0px; border-style: solid; text-align: left; vertical-align: top; overflow: hidden;">
								</div>
								<span class="top-icon-root" id="mailWriteLayout_Receiver_Error_Icon" style="visibility: hidden; width: auto; height: auto; margin: 0px 0px 0px 5px; color: rgb(255, 0, 0); font-size: 14px; border-width: 0px; border-style: solid; vertical-align: middle;">
								<i class="top-icon-content" style="line-height: 14px;">
								</i></span>
							</div>`;
    	return receiver;
    };
    const __template__html__info = () => {
    	var info = `	<div id="mailWriteLayout_Info_Sender_Private" class="top-linearlayout-root" style="display:flex; white-space: nowrap; height: auto; margin: 10px 0px 0px; border-width: 0px; border-style: solid; vertical-align: top; overflow: hidden; width: 100%; line-height: 25px;">
		
							<div id="mailWriteLayout_Info_Sender__Private" class="top-linearlayout-root" style="white-space: nowrap; width: 137px; border-width: 0px; border-style: solid; vertical-align: middle; overflow: hidden; height: 100%; line-height: 25px;">
		
									<span id="TextView583_Private" class="top-textview-root" style="width: auto; height: auto; font-weight: bold; font-size: 12px; border-width: 0px; border-style: solid; cursor: default; text-align: left; vertical-align: middle;"><a class="top-textview-url" style="cursor: default;">보낸 사람</a><div class="top-textview-aligner middle" style="cursor: default;"></div></span>
		
							</div>
				
							<div id="LinearLayout521_1_Private1" class="top-linearlayout-root" style="white-space: nowrap; width: 23.5rem; height: auto; margin: 0px 15px 0px 0px; border-width: 0px; border-style: solid; vertical-align: middle; overflow: hidden; line-height: 25px;">
							</div>
							
							<div id="mailWriteLayout_TempTime_Layout1" class="top-linearlayout-root" style="white-space: nowrap; width: auto; height: 25px; margin: 0px 15px 0px 0px; border-width: 0px; border-style: solid; vertical-align: middle; overflow: hidden; line-height: 25px;">
									<span id="mailWriteLayout_TempTime1" class="top-textview-root" style="width: auto; height: auto; margin: 0px 0px 0px 4px; color: rgb(56, 67, 77); font-size: 13px; border-width: 0px; border-style: solid; cursor: default; vertical-align: middle;"><a class="top-textview-url" style="cursor: default;">임시저장</a><div class="top-textview-aligner middle" style="cursor: default;"></div></span>
							</div>
						</div>`;
    	return info;
    };
    const __template__html__button = () => {
    	var button = `	<div id="mailWriteLayout_Button" class="newMailWrite__layout__button" style="white-space: nowrap; height: auto; margin: 3px 0px 0px; border-width: 0px 0px 1px; border-style: solid; border-color: rgb(218, 218, 218); vertical-align: top; overflow: hidden; width: 100%; visibility: visible; line-height: 55.6406px;">
						<div id="mailWriteLayout_Button_left" class="top-linearlayout-root" style="white-space: nowrap; width: calc(60% - 0px); height: auto; border-width: 0px; border-style: solid; vertical-align: top; overflow: hidden; line-height: 55.6406px;">
							<button id="mailWriteLayout_Button_Send" class="top-button-root newMailWrite__button__send" type="button" style="min-width: 56px; min-height: 27px; margin: 5px 0px 8px 15px; vertical-align: middle;" tabindex="0">
								<label class="top-button-text">보내기</label>
							</button>
							<button id="mailWriteLayout_Button_Preview" class="top-button-root newMailWrite__button__preview" type="button" style="width: 72px; height: 27px; min-height: 27px; margin: 5px 0px 8px 10px; padding: 0px; font-size: 13px; text-align: center; vertical-align: middle;" tabindex="0">
								<label class="top-button-text">미리보기</label>
							</button>
							<button id="mailWriteLayout_Button_Preview_1" class="top-button-root newMailWrite__button__tempsave" type="button" style="width: 72px; height: 27px; min-height: 27px; margin: 5px 0px 8px 10px; padding: 0px; font-size: 13px; text-align: center; vertical-align: middle;" tabindex="0">
								<label class="top-button-text">임시 저장</label>
							</button>
    					</div>
    				</div>`;
    	return button;
    };
    const set_auto_complete= (event, widget) => {
    	var autocomplete = ["동민님<dongmin_seo@ts.co.kr>", 
    		"9<dongmin_seo@ts.co.kr>", 
    		"11<dongmin_seo@ts.co.kr>", 
    		"18<dongmin_seo@ts.co.kr>", 
    		"20<dongmin_seo@ts.co.kr>", 
    		"4<dongmin_seo@ts.co.kr>", 
    		"14<dongmin_seo@ts.co.kr>", 
    		"16<dongmin_seo@ts.co.kr>", 
    		"1<dongmin_seo@ts.co.kr>", 
    		"2<dongmin_seo@ts.co.kr>", 
    		"3<dongmin_seo@ts.co.kr>", 
    		"5<dongmin_seo@ts.co.kr>", 
    		"15<dongmin_seo@ts.co.kr>", 
    		"17<dongmin_seo@ts.co.kr>", 
    		"19<dongmin_seo@ts.co.kr>", 
    		"6<dongmin_seo@ts.co.kr>",
    		"7<dongmin_seo@ts.co.kr>", 
    		"13<dongmin_seo@ts.co.kr>", 
    		"민씨<dongmin_seo@ts.co.kr>"];
    	Top.Controller.get('mailMainLeftLogic').autoComplete = autocomplete;
    };
 	const set_auto_complete2= (event, widget) => {
	 		var flg;
	    	if(isb2c())
	    		flg = 'b2c';
	    	else
	    		flg = 'b2b';
    	
			  var data =  {"dto" : {
				    "CMFLG" : "AutoCreateAddress",
				    "MAIL_USER_NAME" : "",
				    "FLAGS" : flg,
				    "DOMAIN_URL" : location.host,
				    "USER_ID" : userManager.getLoginUserId()
				 }};
	  
			  Top.Ajax.execute({
					type : "POST",
					url :_workspace.url + "Mail/Mail?action=AutoCreateEMailAddress",
					dataType : "json",
					data	 : JSON.stringify(data),
					async:false,
					contentType: "application/json; charset=utf-8",
					xhrFields: {withCredentials: true},
					crossDomain : true,
					success : function(ret,xhr,status) {
			
					var addr_info_array = [];						
					var retArr = JSON.parse(ret).dto.ADDRESS_INFO;
					if(retArr == null) retArr = [];
				
				for(var i=0; i < retArr.length; i++){
					
					addr_info_array.push('' + retArr[i].MAIL_USER_NAME + '<' + retArr[i].EMAIL_ADDRESS + '>');
					
				}
				Top.Controller.get('mailMainLeftLogic').autoComplete = addr_info_array;
				return;
				/*
				Top.Dom.selectById('newmailwritePopup__receiverAddress__chip').setProperties({'auto-complete':addr_info_array.join(','),
						'auto-complete-max-height':'210px',
						'support-empty':'false'});
//				Top.Dom.selectById('newmailwritePopup__receiverAddress__chip').setProperties({'chip-items' : Top.Controller.get('mailWriteLogic').receiverAddr});
//				Top.Dom.selectById('newmailwritePopup__receiverAddress__chip').setProperties({'on-keyup':'checkSemicolon', 'on-select' : 'autoCompleteChipItem', 'on-close' : 'closeChipItem', 'on-add':'addReceiver', 'on-blur':'blurAddress'});
				
				Top.Dom.selectById('Text_Cc').setProperties({'auto-complete':addr_info_array.join(','),
					'auto-complete-max-height':'210px',
					  'support-empty':'false'});
				Top.Dom.selectById('Text_Cc').setProperties({'chip-items' : Top.Controller.get('mailWriteLogic').CCAddr});
				Top.Dom.selectById('Text_Cc').setProperties({'on-keyup':'checkSemicolon', 'on-select' : 'autoCompleteChipItem', 'on-close' : 'closeChipItem', 'on-add':'addCC', 'on-blur':'blurAddress'});
				 
									
				Top.Dom.selectById('Text_HiddenCc').setProperties({'auto-complete':addr_info_array.join(','),
					'auto-complete-max-height':'210px',
					  'support-empty':'false'});
				Top.Dom.selectById('Text_HiddenCc').setProperties({'chip-items' : Top.Controller.get('mailWriteLogic').BCCAddr});
				Top.Dom.selectById('Text_HiddenCc').setProperties({'on-keyup':'checkSemicolon', 'on-select' : 'autoCompleteChipItem', 'on-close' : 'closeChipItem', 'on-add':'addHiddenCC', 'on-blur':'blurAddress'});
*/
				
			},
			error : function(ret, xhr,status) {
			
			
			}
	    });
 	};
	const addReceiver= (event, widget) => {
	
	};
	const autoCompleteChipItem= (event, widget) => {
		if(event.target.nodeName == 'INPUT') return;
			if(this.blured) {
			widget.removeLastChip();
			this.blured = false;
		}
			Top.Controller.get('mailWriteLogic').isAutoComplete = true;
	 };
   const closeChipItem= (event, widget) => {
	   
   };
   const checkSemicolon= (event, widget) => {
	 if(event.key === ";" || event.key === "Enter") {
		 	event.target.value = event.target.value.replace(/;$/gi,"")
				event.target.blur();
				event.target.focus();
		}
	};
   const blurAddress= (event, widget) => {
	   this.blured = true;
		if(event.target.value == undefined || event.target.value == "")
			return ;
		Top.Controller.get('mailWriteLogic').isLeaved = true;
		var value = {"text": event.target.value}
		widget.addChip(value);
   };
    const _onclick__attachmenu= (event, widget) => {
	      console.log(event);
	      console.log(widget);
    };
    const _onCheckboxChange = (event, widget) => {
	      console.log(event);
	      console.log(widget);
	      //widget.getChecked() true:체크O, false:체크X
    };
    const _onFilterChange = (event, widget) => {
	      console.log(event);
	      console.log(widget);
    };
    const woketree = (event, widget) => {
    	
    	console.log('woketree');
		widget.id; 	//mailWriteLayout_bccAddressBookButton__workTree,, mailWriteLayout_recAddressBookButton__workTree,, mailWriteLayout_ccAddressBookButton__workTree
    		
    		InviteMemberDialog.open({
    		    title: '멤버 추가', 
    		    confirmedButtonText: '확인',
    		    onConfirmedButtonClicked: function (users, meeting) { 
    		    		
    		    	if(users.length>0){
    		    		for(var i=0; i<users.length; i++){
    		    			if(users[i].USER_EMAIL == undefined)
    		    				continue;
    		    			var map = { 'text' : users[i].USER_EMAIL , 'userId':users[i].USER_ID, 'userName':users[i].USER_NAME, 'userPhoto':users[i].THUMB_PHOTO }
    		    			if(widget.id == "mailWriteLayout_recAddressBookButton__workTree")
    			    		{
    		    				receiverAddr.push(map);
    			    		}		
    			    		else if(widget.id == "mailWriteLayout_ccAddressBookButton__workTree")
    			    		{
    			    			CCAddr.push(map);
    			    		}else if(widget.id == "mailWriteLayout_bccAddressBookButton__workTree")
    			    		{
    			    			BCCAddr.push(map);
    			    		}
    		    		}
    		    		
    		    					
    		    		if(widget.id == "mailWriteLayout_recAddressBookButton__workTree")
    		    		{
    		    			Top.Dom.selectById('newmailwritePopup__receiverAddress__chip').setProperties({'chip-items' : receiverAddr});
    		    		}		
    		    		else if(widget.id == "mailWriteLayout_ccAddressBookButton__workTree")
    		    		{
    		    			Top.Dom.selectById('newmailwritePopup__ccAddress__chip').setProperties({'chip-items' : CCAddr});
    		    		}else if(widget.id == "mailWriteLayout_bccAddressBookButton__workTree")
    		    		{
    		    			Top.Dom.selectById('newmailwritePopup__bccAddress__chip').setProperties({'chip-items' : BCCAddr});	
    		    		}
    		    	}
    		    },
    		    hideTeeMeetingCheckbox: true
    		});
    		
    };
    const addressBook = (event, widget) => {
    	console.log('addressBook');
    	Top.Controller.get("mailWriteLogic").addRec = true;
        Top.Dom.selectById('mailAddressBookDialog').open();
    };
    const clickSend = (event, widget) => {
    	console.log('send');
    };
    const clickPreview = (event, widget) => {
    	console.log('preview');
    };
    const clickTempSave = (event, widget) => {
    	console.log('tempSave');
    };
    
    const open = () => {
        if (!_rootElement) {
            _rootElement = _createDialog();
            const appendTarget = document.querySelector(_appendTarget);
            if (appendTarget) {
                appendTarget.appendChild(_rootElement);
            }
        }

        if (_rootElement) {
            _rootElement.style.display = "flex";
        }
    };

    const collapse = () => {
        if (_rootElement) {
            _rootElement.querySelector("#newMailWrite__dialog__content").style.display = "none";
            _rootElement.querySelector(".newMailWrite__expand-button").style.display = "flex";
            _rootElement.querySelector(".newMailWrite__collapse-button").style.display = "none";
        }
    };

    const expand = () => {
        if (_rootElement) {
            _rootElement.querySelector("#newMailWrite__dialog__content").style.display = "flex";
            _rootElement.querySelector(".newMailWrite__expand-button").style.display = "none";
            _rootElement.querySelector(".newMailWrite__collapse-button").style.display = "flex";
        }
    };

    const close = () => {
        if (_rootElement) {
            _rootElement.style.display = "none";
            
            const contents = _rootElement.querySelector("#newMailWrite__dialog__content");
            while (contents.hasChildNodes()) {
            	contents.removeChild(contents.firstChild);
            }
        }        
    };


    return {
        // 연다
        open,

        // 닫는다
        close,

    };
});

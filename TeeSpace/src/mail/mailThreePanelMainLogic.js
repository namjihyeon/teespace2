Top.Controller.create('mailThreePanelMainLogic', {
	init : function(event, widget) {
		this.replyClicked; //
		this.replyAllClicked;
		this.forwardClicked;
		this.sendAgainClicked;
		this.modifyClicked;
		this.isMoveToReadMailFromWrite;
		
		this.initButton();
		this.setMoveFolder();
		this.buttonDisabled();
		this.setFilterSelectbox();
		
		this.resetButton();
		
		this.sendAgainClicked;  
		
		Top.Dom.selectById('mailThreePanelLayout_Table').setProperties({'visible':'hidden'})
		Top.Dom.selectById('mailThreePanelLayout_Table').onRender(function(){Top.Dom.selectById('mailThreePanelLayout_Table').setProperties({'visible':'visible'})});
		
		
		Top.Controller.get('mailMainLeftLogic').isWritingMail = false;
		this.clickedMail;
		this.readMailArr = [];
		this.tempMailNum ;
		
		if(Top.Controller.get('mailMainLeftLogic').searchKeyword !== null && Top.Controller.get('mailMainLeftLogic').isChangePanel){
			
			var id= ['Receive','Sent','Temp','ToMe','Spam','Trash'];
			for(var i=0; i<id.length; i++){
			    if(Top.Dom.selectById('mailThreePanelLayout_'+id[i]).getProperties('visible') == 'visible'){
					Top.Dom.selectById('mailThreePanelLayout_'+id[i]+'_Search_Text').setText(Top.Controller.get('mailMainLeftLogic').searchKeyword.SUBJECT);
					Top.Dom.selectById('mailThreePanelLayout_'+id[i]+'_SelectBox_Filter').setProperties({'selectedIndex':Top.Controller.get('mailMainLeftLogic').searchKeyword.filterIndex})
					Top.Dom.selectById('mailThreePanelLayout_Content').src('mailSelectMailLayout.html' + verCsp());
			    }
			}
			Top.Controller.get('mailMainLeftLogic').isChangePanel = null;
			this.searchMail(Top.Controller.get('mailMainLeftLogic').searchKeyword);

		}else
			this.callMailList();
		
		Top.Dom.selectById('mailThreePanelLayout_Table').setProperties({'on-rowclick':'getReadMail'});
		Top.Dom.selectById("mailThreePanelLayout_Table").setProperties({'on-rowcheck': 'buttonStatusChange'});

		Top.Dom.selectById('mailThreePanelLayout_Pagination').setProperties({'on-pagechange':'changePage'});
		Top.Dom.selectById('mailThreePanelLayout_PageLength').setProperties({'on-change':'changePageLength'});

//		Top.Dom.selectById('mailThreePanelBoxLayout_isImportance').setProperties({'on-click':'clickImportant'});
		
//		$('#mailThreePanelLayout_Receive_Filter_Icon').on('click', function(){
//			this.openFilterDialog();
//		})
		Top.Dom.selectById('mailThreePanelLayout_Receive_Filter_Icon').setProperties({'on-click':'openFilterDialog'})
		
	}, setVisibleNone : function(){
		var id= ['Receive','Sent','Temp','ToMe','Spam','Trash'];
		var replyId = ['Receive','Sent'];
		var replyAllId = ['Receive','Sent'];
		var forwardId = ['Receive','Sent','Temp','ToMe'];
		for(var i=0; i<replyId.length; i++){
			Top.Dom.selectById('mailThreePanelLayout_'+id[i]+'_Reply').setProperties({'visible':'none'});
		}
		for(var i=0; i<replyAllId.length; i++){
			Top.Dom.selectById('mailThreePanelLayout_'+id[i]+'_ReplyAll').setProperties({'visible':'none'});
		}
		for(var i=0; i<forwardId.length; i++){
			Top.Dom.selectById('mailThreePanelLayout_'+id[i]+'_Forward').setProperties({'visible':'none'});
		}
		
	}, initButton : function() {
		
		this.setOnClick__AddressIcon();
		this.setEvent__SearchIcon();
		this.setVisibleNone();  
		
		
		Top.Dom.selectById('mailThreePanelLayout_Receive_2panel_Icon').setProperties({'on-click':'moveToTwoPanel'});
		Top.Dom.selectById('mailThreePanelLayout_Sent_2panel_Icon').setProperties({'on-click':'moveToTwoPanel'});
		Top.Dom.selectById('mailThreePanelLayout_ToMe_2panel_Icon').setProperties({'on-click':'moveToTwoPanel'});
		Top.Dom.selectById('mailThreePanelLayout_Spam_2panel_Icon').setProperties({'on-click':'moveToTwoPanel'});
		Top.Dom.selectById('mailThreePanelLayout_Trash_ToTwo_Icon').setProperties({'on-click':'moveToTwoPanel'});
		Top.Dom.selectById('mailThreePanelLayout_Temp_2Panel_Icon').setProperties({'on-click':'moveToTwoPanel'});
		
		Top.Dom.selectById('mailThreePanelLayout_Receive_Unread').setProperties({'on-click':'setRead'}); 
		Top.Dom.selectById('mailThreePanelLayout_Sent_Unread').setProperties({'on-click':'setRead'});
		Top.Dom.selectById('mailThreePanelLayout_ToMe_Unread').setProperties({'on-click':'setRead'});
		Top.Dom.selectById('mailThreePanelLayout_Spam_Unread').setProperties({'on-click':'setRead'});
		Top.Dom.selectById('mailThreePanelLayout_Trash_Unread').setProperties({'on-click':'setRead'});
		Top.Dom.selectById('mailThreePanelLayout_Temp_Unread').setProperties({'on-click':'setRead'});
		
		Top.Dom.selectById('mailThreePanelLayout_Receive_Reply').setProperties({'on-click':'clickReply'}); 
		Top.Dom.selectById('mailThreePanelLayout_Sent_Reply').setProperties({'on-click':'clickReply'}); 
		
		Top.Dom.selectById('mailThreePanelLayout_Receive_ReplyAll').setProperties({'on-click':'clickReplyAll'}); 
		Top.Dom.selectById('mailThreePanelLayout_Sent_ReplyAll').setProperties({'on-click':'clickReplyAll'}); 
		
		Top.Dom.selectById('mailThreePanelLayout_Receive_Forward').setProperties({'on-click':'clickForward'});
		Top.Dom.selectById('mailThreePanelLayout_Sent_Forward').setProperties({'on-click':'clickForward'});
		Top.Dom.selectById('mailThreePanelLayout_ToMe_Forward').setProperties({'on-click':'clickForward'});
		Top.Dom.selectById('mailThreePanelLayout_Temp_Forward').setProperties({'on-click':'clickForward'}); 
			
		Top.Dom.selectById('mailThreePanelLayout_Receive_SelectBox_Move').setProperties({'on-change':'moveFolder'});
		Top.Dom.selectById('mailThreePanelLayout_Sent_SelectBox_Move').setProperties({'on-change':'moveFolder'});
		Top.Dom.selectById('mailThreePanelLayout_Receive_SelectBox_Move').setProperties({'on-change':'moveFolder'});
		Top.Dom.selectById('mailThreePanelLayout_Spam_SelectBox_Move').setProperties({'on-change':'moveFolder'});
		Top.Dom.selectById('mailThreePanelLayout_Trash_SelectBox_Move').setProperties({'on-change':'moveFolder'});
		
		Top.Dom.selectById('mailThreePanelLayout_Spam_NotSpam').setProperties({'on-click':'notSpam'}); 
		
		Top.Dom.selectById('mailThreePanelLayout_Trash_DeleteForever').setProperties({'on-click':function(){Top.Dom.selectById('mailDeletePermanentDialog').open();}});
		
		Top.Dom.selectById('mailThreePanelLayout_Sent_SendAgain').setProperties({'on-click':'sendAgain'}); 
		
		Top.Dom.selectById('mailThreePanelLayout_ToMe_Modify').setProperties({'on-click':'modifyMail'}); 
		
		Top.Dom.selectById('mailThreePanelLayout_Write_Send').setProperties({'on-click':'clickSend'}); 
		Top.Dom.selectById('mailThreePanelLayout_Temp_Unread').setProperties({'on-click':'setRead'});
		
		Top.Dom.selectById('mailThreePanelLayout_Write_SaveTemp').setProperties({'on-click':'saveTemp'});
		Top.Dom.selectById('mailThreePanelLayout_Write_Preview').setProperties({'on-click':'clickPreview'}); 
		

		Top.Dom.selectById('mailThreePanelLayout_Receive_SelectBox_Move').setProperties({'on-change':'moveFolder'}); 
		Top.Dom.selectById('mailThreePanelLayout_Sent_SelectBox_Move').setProperties({'on-change':'moveFolder'});
		Top.Dom.selectById('mailThreePanelLayout_ToMe_SelectBox_Move').setProperties({'on-change':'moveFolder'});
		Top.Dom.selectById('mailThreePanelLayout_Spam_SelectBox_Move').setProperties({'on-change':'moveFolder'});
		Top.Dom.selectById('mailThreePanelLayout_Trash_SelectBox_Move').setProperties({'on-change':'moveFolder'});
		
		Top.Dom.selectById("mailThreePanelLayout_Receive_All_CheckBox").setProperties({'on-change':'buttonStatusChange'});
		Top.Dom.selectById("mailThreePanelLayout_Sent_All_CheckBox").setProperties({'on-change':'buttonStatusChange'});
		Top.Dom.selectById("mailThreePanelLayout_ToMe_All_CheckBox").setProperties({'on-change':'buttonStatusChange'});
		Top.Dom.selectById("mailThreePanelLayout_Trash_All_CheckBox").setProperties({'on-change':'buttonStatusChange'});
		Top.Dom.selectById("mailThreePanelLayout_Spam_All_CheckBox").setProperties({'on-change':'buttonStatusChange'});
		Top.Dom.selectById("mailThreePanelLayout_Temp_All_CheckBox").setProperties({'on-change':'buttonStatusChange'});
		
		if(mailData.pageLength != undefined){
			var items = Top.Dom.selectById('mailThreePanelLayout_PageLength').getProperties('itemsList');
			for(var i=0; i<items.length; i++){
				if(items[i] == mailData.pageLength)
					Top.Dom.selectById('mailThreePanelLayout_PageLength').setProperties({'selectedIndex':i});
			}
		}
		
	}, onChange_CheckBox : function(event, widget){ 
		var id = widget.id.split('_');
		var index = id[id.length-2]
		var checkBoxId = 'CheckBox406_' + index + '_' + id[id.length-1];
		var imageId = 'ImageButton201_' + index + '_' + id[id.length-1];
		
		if(Top.Dom.selectById(checkBoxId).getChecked() )
		{
			Top.Dom.selectById(checkBoxId).setProperties({'visible':'visible'});
			Top.Dom.selectById(imageId).setProperties({'visible':'none'});
			mailDataRepo.mailReceivedBoxDataList[Top.Dom.selectById('mailThreePanelLayout_Table').getStartRowNum()+Number(index)].CHECKBOX_VISIBLE = "visible";
			mailDataRepo.mailReceivedBoxDataList[Top.Dom.selectById('mailThreePanelLayout_Table').getStartRowNum()+Number(index)].PROFILE_VISIBLE = "none";
		}else{
			Top.Dom.selectById(checkBoxId).setProperties({'visible':'none'});
			Top.Dom.selectById(imageId).setProperties({'visible':'visible'});
			mailDataRepo.mailReceivedBoxDataList[Top.Dom.selectById('mailThreePanelLayout_Table').getStartRowNum()+Number(index)].CHECKBOX_VISIBLE = "none";
			mailDataRepo.mailReceivedBoxDataList[Top.Dom.selectById('mailThreePanelLayout_Table').getStartRowNum()+Number(index)].PROFILE_VISIBLE = "visible";
		}
		
	},mouseExit_CheckBox : function(event, widget){
		var id = widget.id.split('_');
		var index = id[id.length-2]
		var checkBoxId = 'CheckBox406_' + index + '_' + id[id.length-1];
		var imageId = 'ImageButton201_' + index + '_' + id[id.length-1];
		
		if(Top.Dom.selectById(checkBoxId).getChecked() )
		{
			Top.Dom.selectById(checkBoxId).setProperties({'visible':'visible'});
			Top.Dom.selectById(imageId).setProperties({'visible':'none'});
			mailDataRepo.mailReceivedBoxDataList[Top.Dom.selectById('mailThreePanelLayout_Table').getStartRowNum()+Number(index)].CHECKBOX_VISIBLE = "visible";
			mailDataRepo.mailReceivedBoxDataList[Top.Dom.selectById('mailThreePanelLayout_Table').getStartRowNum()+Number(index)].PROFILE_VISIBLE = "none";
		}else{
			Top.Dom.selectById(checkBoxId).setProperties({'visible':'none'});
			Top.Dom.selectById(imageId).setProperties({'visible':'visible'});
			mailDataRepo.mailReceivedBoxDataList[Top.Dom.selectById('mailThreePanelLayout_Table').getStartRowNum()+Number(index)].CHECKBOX_VISIBLE = "none";
			mailDataRepo.mailReceivedBoxDataList[Top.Dom.selectById('mailThreePanelLayout_Table').getStartRowNum()+Number(index)].PROFILE_VISIBLE = "visible";
		}
	
		
	},
	mouseOver_ProfileImage : function(event, widget){
		var id = widget.id.split('_');
		var index = id[id.length-2]
		var checkBoxId = 'CheckBox406_' + index + '_' + id[id.length-1];
		var imageId = 'ImageButton201_' + index + '_' + id[id.length-1];
		
		Top.Dom.selectById(checkBoxId).setProperties({'visible':'visible'});
		Top.Dom.selectById(imageId).setProperties({'visible':'none'});
		mailDataRepo.mailReceivedBoxDataList[Top.Dom.selectById('mailThreePanelLayout_Table').getStartRowNum()+Number(index)].CHECKBOX_VISIBLE = "visible";
		mailDataRepo.mailReceivedBoxDataList[Top.Dom.selectById('mailThreePanelLayout_Table').getStartRowNum()+Number(index)].PROFILE_VISIBLE = "none";
		
	},
	resetButton : function() {
		
		Top.Dom.selectById('mailThreePanelLayout_Receive').setProperties({'visible' : 'none'}); 
		Top.Dom.selectById('mailThreePanelLayout_Receive_Right').setProperties({'visible' : 'none'}); 
		Top.Dom.selectById('mailThreePanelLayout_Sent').setProperties({'visible' : 'none'});
		Top.Dom.selectById('mailThreePanelLayout_Sent_Right').setProperties({'visible' : 'none'});
		Top.Dom.selectById('mailThreePanelLayout_Temp').setProperties({'visible' : 'none'});
		Top.Dom.selectById('mailThreePanelLayout_Temp_Right').setProperties({'visible' : 'none'});
		Top.Dom.selectById('mailThreePanelLayout_ToMe').setProperties({'visible' : 'none'});
		Top.Dom.selectById('mailThreePanelLayout_ToMe_Right').setProperties({'visible' : 'none'});
		Top.Dom.selectById('mailThreePanelLayout_Spam').setProperties({'visible' : 'none'});
		Top.Dom.selectById('mailThreePanelLayout_Spam_Right').setProperties({'visible' : 'none'});
		Top.Dom.selectById('mailThreePanelLayout_Trash').setProperties({'visible' : 'none'});
		Top.Dom.selectById('mailThreePanelLayout_Trash_Right').setProperties({'visible' : 'none'});
		Top.Dom.selectById('mailThreePanelLayout_Write').setProperties({'visible' : 'none'});
		
		var folderType = mailData.curBox.FOLDER_TYPE;
		var parentFolderType = mailData.curBox.PARENT_FOLDER_TYPE;
		
		if (folderType == '' || folderType == 'MFL0002' || folderType == 'MFL0001' || parentFolderType == 'MFL0001') {
			Top.Dom.selectById('mailThreePanelLayout_Receive').setProperties({'visible' : 'visible'});
			Top.Dom.selectById('mailThreePanelLayout_Receive_Right').setProperties({'visible' : 'visible'});
		}
		else if (folderType == 'MFL0003' || parentFolderType == 'MFL0003') {
			Top.Dom.selectById('mailThreePanelLayout_Sent').setProperties({'visible' : 'visible'});
			Top.Dom.selectById('mailThreePanelLayout_Sent_Right').setProperties({'visible' : 'visible'});
		}
		else if (folderType == 'MFL0004' || parentFolderType == 'MFL0004') {
			Top.Dom.selectById('mailThreePanelLayout_Temp').setProperties({'visible' : 'visible'}); 
			Top.Dom.selectById('mailThreePanelLayout_Temp_Right').setProperties({'visible' : 'visible'}); 
		}
		else if (folderType == 'MFL0005' || parentFolderType == 'MFL0005') {
			Top.Dom.selectById('mailThreePanelLayout_ToMe').setProperties({'visible' : 'visible'});
			Top.Dom.selectById('mailThreePanelLayout_ToMe_Right').setProperties({'visible' : 'visible'});
		}
		else if (folderType == 'MFL0006' || parentFolderType == 'MFL0006') {
			Top.Dom.selectById('mailThreePanelLayout_Spam').setProperties({'visible' : 'visible'});
			Top.Dom.selectById('mailThreePanelLayout_Spam_Right').setProperties({'visible' : 'visible'});
		}
		else if (folderType == 'MFL0007' || parentFolderType == 'MFL0007') {
			Top.Dom.selectById('mailThreePanelLayout_Trash').setProperties({'visible' : 'visible'});
			Top.Dom.selectById('mailThreePanelLayout_Trash_Right').setProperties({'visible' : 'visible'});
		}
		
		if (this.replyClicked == true || this.replyAllClicked == true || this.forwardClicked == true || this.sendAgainClicked == true || this.modifyClicked == true)
			Top.Dom.selectById('mailThreePanelLayout_Write').setProperties({'visible' : 'visible'});
			
		
	}, clickReply : function(event, widget) {
		
		if(mailData.routeInfo.folderId == undefined) 
			mailData.routeInfo.folderId = '0';
		
		let sub = appManager.getSubApp();
		if(sub) 
			Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + mailData.routeInfo.folderId + '/' + Top.Dom.selectById('mailThreePanelLayout_Table').getCheckedData()[0].MAIL_ID + '/reply' + "?sub=" + sub, {eventType:'fold'});
		else 
			Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + mailData.routeInfo.folderId + '/' + Top.Dom.selectById('mailThreePanelLayout_Table').getCheckedData()[0].MAIL_ID + '/reply' , {eventType:'close'});

	}, reply : function() {
		
		this.replyClicked = true ;
		Top.Dom.selectById('mailThreePanelLayout_Content').src('mailWriteLayout.html' + verCsp());
		this.resetButton();
		
	}, modifyMail : function() {
		
		if(mailData.routeInfo.folderId == undefined) 
			mailData.routeInfo.folderId = '0';
		
		let sub = appManager.getSubApp();
		if(sub) 
			Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + mailData.routeInfo.folderId + '/' + Top.Dom.selectById('mailThreePanelLayout_Table').getCheckedData()[0].MAIL_ID + '/modify' + "?sub=" + sub, {eventType:'fold'});
		else 
			Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + mailData.routeInfo.folderId + '/' + Top.Dom.selectById('mailThreePanelLayout_Table').getCheckedData()[0].MAIL_ID + '/modify' , {eventType:'close'});
		
		
	}, modify : function() {
		
		this.modifyClicked = true;
		Top.Dom.selectById('mailThreePanelLayout_Content').src('mailWriteLayout.html' + verCsp());
		this.resetButton();
		
	}, clickReplyAll : function(event, widget) {
		
		if(mailData.routeInfo.folderId == undefined) mailData.routeInfo.folderId = '0';
		
		let sub = appManager.getSubApp();
		if(sub) 
			Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + mailData.routeInfo.folderId + '/' + Top.Dom.selectById('mailThreePanelLayout_Table').getCheckedData()[0].MAIL_ID + '/replyall' + "?sub=" + sub, {eventType:'fold'});
		else 
			Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + mailData.routeInfo.folderId + '/' + Top.Dom.selectById('mailThreePanelLayout_Table').getCheckedData()[0].MAIL_ID + '/replyall' , {eventType:'close'});
		
	}, replyall : function() {
		
		this.replyAllClicked = true;
		Top.Dom.selectById('mailThreePanelLayout_Content').src('mailWriteLayout.html' + verCsp());
		this.resetButton();
		
	}, sendAgain : function() {

		if(mailData.routeInfo.folderId == undefined) mailData.routeInfo.folderId = '0';

		let sub = appManager.getSubApp();
		if(sub) 
			Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + mailData.routeInfo.folderId + '/' + Top.Dom.selectById('mailThreePanelLayout_Table').getCheckedData()[0].MAIL_ID + '/resend' + "?sub=" + sub, {eventType:'fold'});
		else 
			Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + mailData.routeInfo.folderId + '/' + Top.Dom.selectById('mailThreePanelLayout_Table').getCheckedData()[0].MAIL_ID + '/resend' , {eventType:'close'});
		
	}, resend : function() {
		
		this.sendAgainClicked = true;
		Top.Dom.selectById('mailThreePanelLayout_Content').src('mailWriteLayout.html' + verCsp());
		this.resetButton();
		
	}, clickForward : function(event, widget) {

		if(mailData.routeInfo.folderId == undefined) mailData.routeInfo.folderId = '0';
		
		let sub = appManager.getSubApp();
		if(sub) 
			Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + mailData.routeInfo.folderId + '/' + Top.Dom.selectById('mailThreePanelLayout_Table').getCheckedData()[0].MAIL_ID + '/forward' + "?sub=" + sub, {eventType:'fold'});
		else 
			Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + mailData.routeInfo.folderId + '/' + Top.Dom.selectById('mailThreePanelLayout_Table').getCheckedData()[0].MAIL_ID + '/forward' , {eventType:'close'});
		
	}, forward : function() {
		
		this.forwardClicked = true;
		Top.Dom.selectById('mailThreePanelLayout_Content').src('mailWriteLayout.html' + verCsp());
		this.resetButton();
		
	}, clickDelete : function() {
		if(Top.Dom.selectById('mailThreePanelLayout_Table').getCheckedData().length == 0)
			return;
		
		var mailIdList = [];
		for(var i=0; i<Top.Dom.selectById('mailThreePanelLayout_Table').getCheckedData().length; i++)
			mailIdList.push(Top.Dom.selectById('mailThreePanelLayout_Table').getCheckedData()[i].MAIL_ID);
		
		var data={
				dto: {
					"CMFLG"				: "DeleteMail",
					"dbio_total_count"	: Top.Dom.selectById('mailThreePanelLayout_Table').getCheckedData().length,
					"ACCOUNT_ID"		: mailData.curBox.ACCOUNT_ID,
					"FOLDER_TYPE"		: mailData.curBox.FOLDER_TYPE,
					"MAIL_ID"			: mailIdList
				}
		};
		
		Top.Ajax.execute({
			
			type : "POST",
			url : _workspace.url + "Mail/Mail?action=Delete",
			dataType : "json",
			data	 : JSON.stringify(data),
			crossDomain: true,
			contentType: "application/json",
		    xhrFields: {withCredentials: true},
			success : function(ret,xhr,status) {
				
				if(mailIdList.indexOf(mailData.routeInfo.mailId) != -1){
					Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + mailData.routeInfo.folderId);
					let sub = appManager.getSubApp();
					if(sub) 
						Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + mailData.routeInfo.folderId + "?sub=" + sub, {eventType:'fold'});
					else 
						Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + mailData.routeInfo.folderId , {eventType:'close'});
				}
				else {
					Top.Controller.get('mailThreePanelMainLogic').callMailList(); 
					Top.Controller.get('mailThreePanelMainLogic').checkAllFalse();
				}
				Top.Controller.get('mailMainLeftLogic').resetCount();
				TeeToast.open({'text':Top.i18n.get().value.m000157+mailIdList.length+Top.i18n.get().value.m000160,'size':'min'});
//				notiFeedback(Top.i18n.get().value.m000157+mailIdList.length+Top.i18n.get().value.m000160);
				
			}
		});
		
	}, clickPreview : function(event, widget) {
		
		Top.Controller.get("mailWriteLogic").clickPreview();	
		
	}, buttonAbled : function() {
		
		Top.Dom.selectById('mailThreePanelLayout_Receive_Unread').setProperties({'disabled':false});
		Top.Dom.selectById('mailThreePanelLayout_ToMe_Unread').setProperties({'disabled':false});
		Top.Dom.selectById('mailThreePanelLayout_Spam_Unread').setProperties({'disabled':false});
		Top.Dom.selectById('mailThreePanelLayout_Trash_Unread').setProperties({'disabled':false}); 
		Top.Dom.selectById('mailThreePanelLayout_Sent_Unread').setProperties({'disabled':false});
		Top.Dom.selectById('mailThreePanelLayout_Temp_Unread').setProperties({'disabled':false});
		
		Top.Dom.selectById('mailThreePanelLayout_Receive_Delete').setProperties({'disabled':false});
		Top.Dom.selectById('mailThreePanelLayout_Sent_Delete').setProperties({'disabled':false});
		Top.Dom.selectById('mailThreePanelLayout_ToMe_Delete').setProperties({'disabled':false});
		Top.Dom.selectById('mailThreePanelLayout_Temp_Delete').setProperties({'disabled':false});
		
		Top.Dom.selectById('mailThreePanelLayout_Receive_Reply').setProperties({'disabled':false});
		Top.Dom.selectById('mailThreePanelLayout_Sent_Reply').setProperties({'disabled':false});
		
		Top.Dom.selectById('mailThreePanelLayout_Receive_ReplyAll').setProperties({'disabled':false});
		Top.Dom.selectById('mailThreePanelLayout_Sent_ReplyAll').setProperties({'disabled':false});
		
		Top.Dom.selectById('mailThreePanelLayout_Receive_Forward').setProperties({'disabled':false});
		Top.Dom.selectById('mailThreePanelLayout_Sent_Forward').setProperties({'disabled':false});
		Top.Dom.selectById('mailThreePanelLayout_ToMe_Forward').setProperties({'disabled':false});
		Top.Dom.selectById('mailThreePanelLayout_Temp_Forward').setProperties({'disabled':false});
		
		Top.Dom.selectById('mailThreePanelLayout_Receive_SelectBox_Move').setProperties({'disabled':false});
		Top.Dom.selectById('mailThreePanelLayout_Sent_SelectBox_Move').setProperties({'disabled':false});
		Top.Dom.selectById('mailThreePanelLayout_ToMe_SelectBox_Move').setProperties({'disabled':false});
		Top.Dom.selectById('mailThreePanelLayout_Spam_SelectBox_Move').setProperties({'disabled':false});
		Top.Dom.selectById('mailThreePanelLayout_Trash_SelectBox_Move').setProperties({'disabled':false});
		
		Top.Dom.selectById('mailThreePanelLayout_Receive_SelectBox_Save').setProperties({'disabled':false});
		Top.Dom.selectById('mailThreePanelLayout_ToMe_SelectBox_Save').setProperties({'disabled':false});
		Top.Dom.selectById('mailThreePanelLayout_Sent_SelectBox_Save').setProperties({'disabled':false});
		
		Top.Dom.selectById('mailThreePanelLayout_Sent_SendAgain').setProperties({'disabled':false});
		
		Top.Dom.selectById('mailThreePanelLayout_Spam_NotSpam').setProperties({'disabled':false});
		
		Top.Dom.selectById('mailThreePanelLayout_Spam_DeleteForever').setProperties({'disabled':false});
		Top.Dom.selectById('mailThreePanelLayout_Trash_DeleteForever').setProperties({'disabled':false});
		
		Top.Dom.selectById('mailThreePanelLayout_ToMe_Modify').setProperties({'disabled':false});
		
	}, buttonReadAbled : function(event, widget) {
		
		Top.Dom.selectById('mailThreePanelLayout_Receive_Unread').setProperties({'disabled':false});
		Top.Dom.selectById('mailThreePanelLayout_ToMe_Unread').setProperties({'disabled':false});
		Top.Dom.selectById('mailThreePanelLayout_Spam_Unread').setProperties({'disabled':false});
		Top.Dom.selectById('mailThreePanelLayout_Trash_Unread').setProperties({'disabled':false}); 
		Top.Dom.selectById('mailThreePanelLayout_Sent_Unread').setProperties({'disabled':false});
		Top.Dom.selectById('mailThreePanelLayout_Temp_Unread').setProperties({'disabled':false});
		
		Top.Dom.selectById('mailThreePanelLayout_Receive_Delete').setProperties({'disabled':false});
		Top.Dom.selectById('mailThreePanelLayout_Sent_Delete').setProperties({'disabled':false});
		Top.Dom.selectById('mailThreePanelLayout_ToMe_Delete').setProperties({'disabled':false});
		Top.Dom.selectById('mailThreePanelLayout_Temp_Delete').setProperties({'disabled':false});
		
		Top.Dom.selectById('mailThreePanelLayout_Receive_Reply').setProperties({'disabled':true});
		Top.Dom.selectById('mailThreePanelLayout_Sent_Reply').setProperties({'disabled':true});
		
		Top.Dom.selectById('mailThreePanelLayout_Receive_ReplyAll').setProperties({'disabled':true});
		Top.Dom.selectById('mailThreePanelLayout_Sent_ReplyAll').setProperties({'disabled':true});
		
		Top.Dom.selectById('mailThreePanelLayout_Receive_Forward').setProperties({'disabled':true});
		Top.Dom.selectById('mailThreePanelLayout_Sent_Forward').setProperties({'disabled':true});
		Top.Dom.selectById('mailThreePanelLayout_ToMe_Forward').setProperties({'disabled':true});
		Top.Dom.selectById('mailThreePanelLayout_Temp_Forward').setProperties({'disabled':true});
		
		Top.Dom.selectById('mailThreePanelLayout_Receive_SelectBox_Move').setProperties({'disabled':false});
		Top.Dom.selectById('mailThreePanelLayout_Sent_SelectBox_Move').setProperties({'disabled':false});
		Top.Dom.selectById('mailThreePanelLayout_ToMe_SelectBox_Move').setProperties({'disabled':false});
		Top.Dom.selectById('mailThreePanelLayout_Spam_SelectBox_Move').setProperties({'disabled':false});
		Top.Dom.selectById('mailThreePanelLayout_Trash_SelectBox_Move').setProperties({'disabled':false});
		
		Top.Dom.selectById('mailThreePanelLayout_Receive_SelectBox_Save').setProperties({'disabled':false});
		Top.Dom.selectById('mailThreePanelLayout_ToMe_SelectBox_Save').setProperties({'disabled':false});
		Top.Dom.selectById('mailThreePanelLayout_Sent_SelectBox_Save').setProperties({'disabled':false});
		
		Top.Dom.selectById('mailThreePanelLayout_Sent_SendAgain').setProperties({'disabled':true});
		
		Top.Dom.selectById('mailThreePanelLayout_Spam_NotSpam').setProperties({'disabled':false});
		
		Top.Dom.selectById('mailThreePanelLayout_Spam_DeleteForever').setProperties({'disabled':false});
		Top.Dom.selectById('mailThreePanelLayout_Trash_DeleteForever').setProperties({'disabled':false});
		
		Top.Dom.selectById('mailThreePanelLayout_ToMe_Modify').setProperties({'disabled':true}); 
		
	}, buttonDisabled : function(event, widget) {
		
		Top.Dom.selectById('mailThreePanelLayout_Receive_Unread').setProperties({'disabled':true});
		Top.Dom.selectById('mailThreePanelLayout_ToMe_Unread').setProperties({'disabled':true});
		Top.Dom.selectById('mailThreePanelLayout_Spam_Unread').setProperties({'disabled':true});
		Top.Dom.selectById('mailThreePanelLayout_Trash_Unread').setProperties({'disabled':true}); 
		Top.Dom.selectById('mailThreePanelLayout_Sent_Unread').setProperties({'disabled':true});
		Top.Dom.selectById('mailThreePanelLayout_Temp_Unread').setProperties({'disabled':true});
		
		Top.Dom.selectById('mailThreePanelLayout_Receive_Delete').setProperties({'disabled':true});
		Top.Dom.selectById('mailThreePanelLayout_Sent_Delete').setProperties({'disabled':true});
		Top.Dom.selectById('mailThreePanelLayout_ToMe_Delete').setProperties({'disabled':true});
		Top.Dom.selectById('mailThreePanelLayout_Temp_Delete').setProperties({'disabled':true});
		
		Top.Dom.selectById('mailThreePanelLayout_Receive_Reply').setProperties({'disabled':true});
		Top.Dom.selectById('mailThreePanelLayout_Sent_Reply').setProperties({'disabled':true});
		
		Top.Dom.selectById('mailThreePanelLayout_Receive_ReplyAll').setProperties({'disabled':true});
		Top.Dom.selectById('mailThreePanelLayout_Sent_ReplyAll').setProperties({'disabled':true});
		
		Top.Dom.selectById('mailThreePanelLayout_Receive_Forward').setProperties({'disabled':true});
		Top.Dom.selectById('mailThreePanelLayout_Sent_Forward').setProperties({'disabled':true});
		Top.Dom.selectById('mailThreePanelLayout_ToMe_Forward').setProperties({'disabled':true});
		Top.Dom.selectById('mailThreePanelLayout_Temp_Forward').setProperties({'disabled':true});
		
		Top.Dom.selectById('mailThreePanelLayout_Receive_SelectBox_Move').setProperties({'disabled':true});
		Top.Dom.selectById('mailThreePanelLayout_Sent_SelectBox_Move').setProperties({'disabled':true});
		Top.Dom.selectById('mailThreePanelLayout_ToMe_SelectBox_Move').setProperties({'disabled':true});
		Top.Dom.selectById('mailThreePanelLayout_Spam_SelectBox_Move').setProperties({'disabled':true});
		Top.Dom.selectById('mailThreePanelLayout_Trash_SelectBox_Move').setProperties({'disabled':true});
		
		Top.Dom.selectById('mailThreePanelLayout_Receive_SelectBox_Save').setProperties({'disabled':true});
		Top.Dom.selectById('mailThreePanelLayout_ToMe_SelectBox_Save').setProperties({'disabled':true});
		Top.Dom.selectById('mailThreePanelLayout_Sent_SelectBox_Save').setProperties({'disabled':true});
		
		Top.Dom.selectById('mailThreePanelLayout_Sent_SendAgain').setProperties({'disabled':true});
		
		Top.Dom.selectById('mailThreePanelLayout_Spam_NotSpam').setProperties({'disabled':true});
		
		Top.Dom.selectById('mailThreePanelLayout_Spam_DeleteForever').setProperties({'disabled':true});
		Top.Dom.selectById('mailThreePanelLayout_Trash_DeleteForever').setProperties({'disabled':true});
		
		Top.Dom.selectById('mailThreePanelLayout_ToMe_Modify').setProperties({'disabled':true});
		
	}, buttonStatusChange : function(event, widget) { 
		
		
		if(Top.Dom.selectById('mailThreePanelLayout_Table').getCheckedIndex() == undefined)
			return;
		
		if(widget.id.substr(widget.id.length-12, 12) == 'All_CheckBox' && widget.isChecked() == true && mailDataRepo.mailReceivedBoxDataList.length != 0)
			Top.Dom.selectById('mailThreePanelLayout_Table').checkAll(true);
		else if(widget.id.substr(widget.id.length-12, 12) == 'All_CheckBox' && widget.isChecked() == false)
			Top.Dom.selectById('mailThreePanelLayout_Table').checkAll(false);
		
		if(Top.Dom.selectById('mailThreePanelLayout_Table').getCheckedIndex().length == 0) {
			Top.Controller.get('mailThreePanelMainLogic').setReadButtonText('읽음');
			Top.Controller.get('mailThreePanelMainLogic').buttonDisabled();
			
			if(Top.Dom.selectById('mailSelectMailLayout_Center2') != null) {
				Top.Dom.selectById('mailSelectMailLayout_Text').setProperties({'visible':'visible'});
				Top.Dom.selectById('mailSelectMailLayout_CountSelectedMail').setProperties({'visible':'none'});
				Top.Dom.selectById('mailSelectMailLayout_Delete').setProperties({'visible':'none'});
				Top.Dom.selectById('mailSelectMailLayout_Read').setProperties({'visible':'none'});
//				Top.Dom.selectById('mailSelectMailLayout_Move').setProperties({'visible':'none'});
			}
		}
		else if(Top.Dom.selectById('mailThreePanelLayout_Table').getCheckedIndex().length > 0) {
			var count = 0;
			
			for(var i=0; i<Top.Dom.selectById('mailThreePanelLayout_Table').getCheckedIndex().length; i++) {
				if(Top.Dom.selectById('mailThreePanelLayout_Table').getCheckedData()[i].SEEN_YN_CODE == 'REA0001')
					count++;				
			}
			
			if(count == Top.Dom.selectById('mailThreePanelLayout_Table').getCheckedIndex().length)
				Top.Controller.get('mailThreePanelMainLogic').setReadButtonText('안읽음');
			else
				Top.Controller.get('mailThreePanelMainLogic').setReadButtonText('읽음');
			
			if(Top.Dom.selectById('mailThreePanelLayout_Table').getCheckedIndex().length == 1)
				Top.Controller.get('mailThreePanelMainLogic').buttonAbled();
			else
				Top.Controller.get('mailThreePanelMainLogic').buttonReadAbled();
			
			if(Top.Dom.selectById('mailSelectMailLayout_Center2') != null) {
				Top.Dom.selectById('mailSelectMailLayout_Text').setProperties({'visible':'none'});
				Top.Dom.selectById('mailSelectMailLayout_CountSelectedMail').setProperties({'visible':'visible'});
				Top.Dom.selectById('mailSelectMailLayout_CountSelectedMail').setProperties({'text' : Top.Dom.selectById('mailThreePanelLayout_Table').getCheckedIndex().length + '개 편지 선택됨'});
				Top.Dom.selectById('mailSelectMailLayout_Delete').setProperties({'visible':'visible'});
				Top.Dom.selectById('mailSelectMailLayout_Read').setProperties({'visible':'visible'});
				Top.Dom.selectById('mailSelectMailLayout_Read').setProperties({'text':Top.Dom.selectById('mailThreePanelLayout_Receive_Unread').getText() + ' 상태로 표시'});
//				Top.Dom.selectById('mailSelectMailLayout_Move').setProperties({'visible':'visible'});
			}
		}
	}, setReadButtonText : function(text) {
		
		Top.Dom.selectById('mailThreePanelLayout_Receive_Unread').setText(text);
		Top.Dom.selectById('mailThreePanelLayout_Sent_Unread').setText(text);
		Top.Dom.selectById('mailThreePanelLayout_ToMe_Unread').setText(text);
		Top.Dom.selectById('mailThreePanelLayout_Spam_Unread').setText(text);
		Top.Dom.selectById('mailThreePanelLayout_Trash_Unread').setText(text);
		Top.Dom.selectById('mailThreePanelLayout_Temp_Unread').setText(text);
		
	}, setMoveFolder : function() { 
		
		var accountList = mailData.accountList;
		var folders = [];
		var folderList = [];;
		
		for(var i=0; i<accountList.length; i++) {
			if(accountList[i].ACCOUNT_ID == mailData.curBox.ACCOUNT_ID)
				folderList = accountList[i].FolderList;
		}
		for(var i=0; i<folderList.length; i++) {
			if(folderList[i].FOLDER_TYPE != 'MFL0006')
				folders.push({'text':folderList[i].DISPLAY_NAME,'value':folderList[i]});
		}
		
		folders.sort(function(a,b) {
			return a.value.FOLDER_TYPE.substr(6,1) - b.value.FOLDER_TYPE.substr(6,1);
		});
		
		var receiveFolder = folders.splice(0,1);
		var importantFolder = folders.splice(0,1);
		var sentFolder = folders.splice(0,1);
		var tempFolder = folders.splice(0,1);
		var sendmeFolder = folders.splice(0,1);
		var trashFolders = folders.splice(0,1);
		
		folders.forEach(function(data, index) {
			
			switch(data.value.PARENT_FOLDER_TYPE) {
			
			case 'MFL0001' :
				data.text='└ '+data.text
				receiveFolder.push(folders[index]);
				break;
			case 'MFL0002' :
				data.text='└ '+data.text
				importantFolder.push(folders[index]);
				break;
			case 'MFL0003' :
				data.text='└ '+data.text
				sentFolder.push(folders[index]);
				break;
			case 'MFL0004' :
				data.text='└ '+data.text
				tempFolder.push(folders[index]);
				break;
			case 'MFL0005' :
				data.text='└ '+data.text
				sendmeFolder.push(folders[index]);
				break;
			
			}
			
		});
	
		var newFolders1 = [];
		newFolders1 = newFolders1.concat(receiveFolder);
		newFolders1 = newFolders1.concat(sendmeFolder);
		newFolders1 = newFolders1.concat(trashFolders);
		
		var newFolders2 = [];
		newFolders2 = newFolders2.concat(sentFolder);
		newFolders2 = newFolders2.concat(sendmeFolder);
		newFolders2 = newFolders2.concat(trashFolders);
		
		var newFolders3 = [];
		newFolders3 = newFolders3.concat(receiveFolder);
		newFolders3 = newFolders3.concat(sentFolder);
		newFolders3 = newFolders3.concat(tempFolder);
		newFolders3 = newFolders3.concat(sendmeFolder);
		newFolders3 = newFolders3.concat(trashFolders);
		
		Top.Dom.selectById('mailThreePanelLayout_Receive_SelectBox_Move').setProperties({'nodes':newFolders1});
		Top.Dom.selectById('mailThreePanelLayout_Sent_SelectBox_Move').setProperties({'nodes':newFolders2});
		Top.Dom.selectById('mailThreePanelLayout_ToMe_SelectBox_Move').setProperties({'nodes':newFolders1});
		Top.Dom.selectById('mailThreePanelLayout_Spam_SelectBox_Move').setProperties({'nodes':newFolders2});
		Top.Dom.selectById('mailThreePanelLayout_Trash_SelectBox_Move').setProperties({'nodes':newFolders3});

	},dragAndDrop__MoveFolder : function(param){
		var data={
				dto: {
						"MAIL_ID"		: param.MAIL_ID,
						"ACCOUNT_ID"	: param.ACCOUNT_ID,
						"FOLDER_ID"		: param.FOLDER_ID,
						"CMFLG"			: "FolderIdUpdate"
						}
				};
				
				Top.Ajax.execute({
					
					type : "POST",
					url : _workspace.url + "Mail/Mail?action=FolderIdUpdate",
					dataType : "json",
					data	 : JSON.stringify(data),
					crossDomain: true,
					contentType: "application/json",
				    xhrFields: {withCredentials: true},
					success : function(ret,xhr,status) {
						Top.Controller.get('mailThreePanelMainLogic').callMailList();
						Top.Controller.get('mailMainLeftLogic').resetCount();
						TeeToast.open({'text':Top.i18n.get().value.m000157+'1'+Top.i18n.get().value.m000158+param.DISPLAY_NAME+Top.i18n.get().value.m000159,'size':'min'});
					}
				});
	}, moveFolder : function(event, widget) {
			
			
		if(Top.Dom.selectById('mailThreePanelLayout_Table').getCheckedData().length == 0)
			return;
		
		var mailIdList = [];
		for(var i=0; i<Top.Dom.selectById('mailThreePanelLayout_Table').getCheckedData().length; i++) {
			
			mailIdList.push(Top.Dom.selectById('mailThreePanelLayout_Table').getCheckedData()[i].MAIL_ID);
			
		}
		
		
		var data={
		dto: {
				"MAIL_ID"		: mailIdList,
				"ACCOUNT_ID"	: mailData.curBox.ACCOUNT_ID,
				"FOLDER_ID"		: widget.getValue().FOLDER_ID,
				"CMFLG"			: "FolderIdUpdate"
		
				}
		};
		
		Top.Ajax.execute({
			
			type : "POST",
			url : _workspace.url + "Mail/Mail?action=FolderIdUpdate",
			dataType : "json",
			data	 : JSON.stringify(data),
			crossDomain: true,
			contentType: "application/json",
		    xhrFields: {withCredentials: true},
			success : function(ret,xhr,status) {
				
				Top.Controller.get('mailThreePanelMainLogic').callMailList();
				Top.Dom.selectById(widget.id).setProperties({'selected-text':Top.i18n.get().value.m000022});
				Top.Controller.get('mailThreePanelMainLogic').checkAllFalse();
				Top.Controller.get('mailMainLeftLogic').resetCount();  
			}
		});
		
	}, setRead : function() {
		
		if(Top.Dom.selectById('mailThreePanelLayout_Table').getCheckedData().length == 0)
			return;
		
		var mailIdList = [];
		var checkedIndexList = Top.Dom.selectById('mailThreePanelLayout_Table').getCheckedIndex();
		for(var i=0; i<Top.Dom.selectById('mailThreePanelLayout_Table').getCheckedData().length; i++) {
			
			mailIdList.push(Top.Dom.selectById('mailThreePanelLayout_Table').getCheckedData()[i].MAIL_ID);
			
		}
		
		var seenYN;
		
		if(Top.Dom.selectById('mailThreePanelLayout_Receive_Unread').getText() == '읽음')
			seenYN = 'REA0001';
		else
			seenYN = 'REA0002';
		
		var data={
				dto: {
					"CMFLG"				: "ChangeMailStatus",
					"dbio_total_count"	: Top.Dom.selectById('mailThreePanelLayout_Table').getCheckedData().length,
					"ACCOUNT_ID"		: mailData.curBox.ACCOUNT_ID,
					"MAIL_MST_TYPE"		: "SEEN_YN_CODE",
					"MAIL_MST_VALUE"	: seenYN,
					"MAIL_ID"			: mailIdList
				}
		};
		
		Top.Ajax.execute({
			
			type : "POST",
			url : _workspace.url + "Mail/Mail?action=ChangeStatus",
			dataType : "json",
			data	 : JSON.stringify(data),
			crossDomain: true,
			contentType: "application/json",
		    xhrFields: {withCredentials: true},
			success : function(ret,xhr,status) {
				
				var textColor = (seenYN == 'REA0001') ? '#6A6C71' : '#000000';
				var textStyle = (seenYN == 'REA0001') ? '' : 'bold';
				var buttonText = (seenYN == 'REA0001') ? '안읽음' : '읽음';
				for(var i = 0; i<Top.Dom.selectById('mailThreePanelLayout_Table').getCheckedData().length; i++) {
					mailDataRepo.mailReceivedBoxDataList[Top.Dom.selectById('mailThreePanelLayout_Table').getCheckedIndex()[i]].TEXT_COLOR = textColor;
					mailDataRepo.mailReceivedBoxDataList[Top.Dom.selectById('mailThreePanelLayout_Table').getCheckedIndex()[i]].TEXT_STYLE = textStyle;
					mailDataRepo.mailReceivedBoxDataList[Top.Dom.selectById('mailThreePanelLayout_Table').getCheckedIndex()[i]].SEEN_YN_CODE = seenYN;
				}
				Top.Controller.get('mailThreePanelMainLogic').setReadButtonText(buttonText)
				Top.Dom.selectById('mailThreePanelLayout_Table').update();
				Top.Controller.get('mailMainLeftLogic').resetCount(); 
				
			}
		});
		
		
		
	}, notSpam : function() { 
		
		if(Top.Dom.selectById('mailThreePanelLayout_Table').getCheckedData().length == 0)
			return;
		
		var mailIdList = [];
		for(var i=0; i<Top.Dom.selectById('mailThreePanelLayout_Table').getCheckedData().length; i++) {
			
			mailIdList.push(Top.Dom.selectById('mailThreePanelLayout_Table').getCheckedData()[i].MAIL_ID);
			
		}
		
		
		var data={
		dto: {
				"MAIL_ID"		: mailIdList,
				"ACCOUNT_ID"	: mailData.curBox.ACCOUNT_ID,
				"FOLDER_ID"		: Top.Dom.selectById('mailMainLayout_MailBox_Text').getProperties('value').FOLDER_ID,
				"CMFLG"			: "FolderIdUpdate"
		
				}
		};
		
		
		Top.Ajax.execute({
			
			type : "POST",
			url : _workspace.url + "Mail/Mail?action=FolderIdUpdate",
			dataType : "json",
			data	 : JSON.stringify(data),
			crossDomain: true,
			contentType: "application/json",
		    xhrFields: {withCredentials: true},
			success : function(ret,xhr,status) {
				Top.Controller.get('mailThreePanelMainLogic').callMailList();
				Top.Controller.get('mailThreePanelMainLogic').checkAllFalse();
				Top.Controller.get('mailMainLeftLogic').resetCount();  
								
			}
		});
		
	}, clickSend : function(event, widget) {
		
		Top.Controller.get("mailWriteLogic").clickSend(); 
	
	}, saveTemp : function() {
		
		Top.Controller.get('mailWriteLogic').saveTemp();
		
	}, moveToTwoPanel : function(){
		 	
		mailData.panelType = '2';
		Top.Controller.get('mailMainLeftLogic').isChangePanel = true;
		if(mailData.routeInfo.mailId == undefined)
			Top.Dom.selectById('mailMainLayout_Center_All').src('mailTwoPanelLayout.html' + verCsp());
		else
			Top.Dom.selectById('mailMainLayout_Center_All').src('mailReadButtonLayout.html' + verCsp());
		
	 	var data={
	 			dto: {
	 					"ACCOUNT_ID"	: Top.Dom.selectById('mailMainLayout_Button_SendMail').getValue(),
	 					"PANEL_TYPE"	: "2"
	 						}};
		 	
	 	Top.Ajax.execute({
			
			type : "POST",
			url : _workspace.url + "Mail/Mail?action=SetPanelType",
			dataType : "json",
			data	 : JSON.stringify(data),
			crossDomain: true,
			contentType: "application/json",
		    xhrFields: {withCredentials: true},
			success : function(ret,xhr,status) {}
		 });
		 	
	 }, moveToTwoPanelFromWrite : function(){
		 
		 this.isMoveToReadMailFromWrite = false;
		 
//		 Top.Dom.selectById('mailExitWriteDialog').open();
		 TeeAlarm.open({'title' :'편지를 저장하지 않고 나가시겠습니까?', 'content' :'수정 사항이 저장되지 않았습니다.',	
				buttons : [{'text' : '나가기', 'onClicked' : function(){ 
					Top.Controller.get('mailExitWriteLogic').clickExit();
					TeeAlarm.close();
					}}]})
		 
	 }, setMailCountAllNorRead : function(){
		 
		 var folerId = mailData.curBox.FOLDER_ID;
		 if ( folerId == ""){
				folerId = "AA";
			}
		 
		 Top.Ajax.execute({
				
				type : "GET",
				url : _workspace.url +"Mail/Mail?action=ListGetbyFolder&CMFLG=FolderToMailList&FOLDER_ID="+folerId+"&ACCOUNT_ID=" + mailData.curBox.ACCOUNT_ID +
				"&USER_ID="+userManager.getLoginUserId()+ 
				'&SEEING_TYPE_CODE=ALL&CURRPAGE=1&PAGELENGTH=0',
				
				dataType : "json",
				crossDomain: true,
				contentType: "application/json",
			    xhrFields: {withCredentials: true},
				success : function(ret,xhr,status) {
					if (Top.Dom.selectById("mailMainLayout_NotReadCount") != undefined && Top.Dom.selectById("mailMainLayout_NotReadCount") != null){
						Top.Dom.selectById("mailMainLayout_NotReadCount").setText(ret.dto.COUNT_NOT_READ)
					}
					if(Top.Dom.selectById("mailMainLayout_AllCount") != undefined && Top.Dom.selectById("mailMainLayout_AllCount") != null){
						Top.Dom.selectById("mailMainLayout_AllCount").setText(ret.dto.dbio_total_count)
					}
					
				}
		 });
			 	
	 }, changePage : function(event, widget) {
		 
		 this.isPageChanged == true
		 Top.Controller.get('mailThreePanelMainLogic').setCheckAll(false);
		 mailData.curPage = widget.getSelectedPage();
		 if(Top.Controller.get('mailMainLeftLogic').searchKeyword !== null)
				this.searchMail(); 
			else
				this.callMailList()		 
	 }, changePageLength : function(event, widget) {
		 
		 mailData.pageLength = widget.getSelectedText();
		 if(Top.Controller.get('mailMainLeftLogic').searchKeyword !== null)
				this.searchMail(); 
		else
				this.callMailList();		 
	 }, applyFilter__searchMail : function(event, widget){
			if(mailData.routeInfo.folderId == undefined) mailData.routeInfo.folderId = '0';
			
			/*let sub = appManager.getSubApp();
			if(sub) 
				Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + mailData.routeInfo.folderId + "?sub=" + sub, {eventType:'fold'});
			else 
				Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + mailData.routeInfo.folderId , {eventType:'close'});*/
						
			mailData.filter = Top.Dom.selectById(widget.id).getProperties('selectedIndex');

			$('#mailSelectMailLayout').empty();
			
			this.searchMail();
			
			Top.Dom.selectById('mailThreePanelLayout_Content').src('mailSelectMailLayout.html' + verCsp());
	 }, searchMail : function(searchKeyword) {
		 	
			Top.Dom.selectById('mailThreePanelLayout_Table').addClass('mail-loading');
			Top.Dom.selectById('mailThreePanelLayout_Table').setProperties({'empty-message':'검색 중...'});
			mailDataRepo.setValue('mailReceivedBoxDataList',[]);
			
			if (mailData.curBox.FOLDER_ID == ""){
				mailData.curBox.FOLDER_ID = "AA"
			}
		
			var str_subject;
			var map; //seeingTypeCode, SUBJECT
			if (typeof searchKeyword == "object")
				map = searchKeyword;
			else
				map =  this.getMailSearchKeyword();
			if(map.SUBJECT == "")
				str_subject = "";
			else
				str_subject = "&SUBJECT="+escape(encodeURIComponent(map.SUBJECT));
			
			Top.Controller.get('mailMainLeftLogic').searchKeyword = map; // 3패널로 넘어갈때를 위해 저장

			
			
					Top.Ajax.execute({
						
						type : "GET",
						url : _workspace.url + "Mail/Mail?action=ListGetbyFolder&CMFLG=FolderToMailList&FOLDER_ID="+mailData.curBox.FOLDER_ID+"&ACCOUNT_ID=" + mailData.curBox.ACCOUNT_ID +
						"&USER_ID="+userManager.getLoginUserId()+
						'&SEEING_TYPE_CODE='+ map.seeingTypeCode+ str_subject + 
						'&CURRPAGE=' + mailData.curPage +
						'&PAGELENGTH=' + mailData.pageLength +
						'&SORTING=DATE_DESC',
						dataType : "json",
						crossDomain: true,
						contentType: "application/json",
					    xhrFields: {withCredentials: true},
						success : function(ret,xhr,status) {
							
							
							Top.Dom.selectById('mailThreePanelLayout_Pagination').setProperties({'total':Math.ceil(ret.dto.dbio_total_count / mailData.pageLength), 'page':mailData.curPage});
							
							if (Top.Dom.selectById("mailMainLayout_NotReadCount") != undefined && Top.Dom.selectById("mailMainLayout_NotReadCount") != null){
								Top.Dom.selectById("mailMainLayout_NotReadCount").setText(ret.dto.COUNT_NOT_READ)
							}
							if(Top.Dom.selectById("mailMainLayout_AllCount") != undefined && Top.Dom.selectById("mailMainLayout_AllCount") != null){
								Top.Dom.selectById("mailMainLayout_AllCount").setText(ret.dto.dbio_total_count)
							}
							
							mailData.curBox.UNREAD_COUNT = ret.dto.COUNT_NOT_READ;
							
							if(ret.dto.INFO_ABOUT_MAIL) {
								for(var i=0; i<ret.dto.INFO_ABOUT_MAIL.length; i++) {
									
									ret.dto.INFO_ABOUT_MAIL[i] = mail.arrangeData(ret.dto.INFO_ABOUT_MAIL[i]);
									
									var searchText = map.SUBJECT;
									ret.dto.INFO_ABOUT_MAIL[i].SUBJECT = ret.dto.INFO_ABOUT_MAIL[i].SUBJECT.replace(searchText,'<span class="mail-search">'+searchText+'</span>');
									
									var temp = (i+1)%5;
									var ran =  temp == 0 ? 5 : temp;
									if(ret.dto.INFO_ABOUT_MAIL[i].THUMB_PHOTO_ADDRESSBOOK == null){
										var temp__userId = ret.dto.INFO_ABOUT_MAIL[i].PROFILE_USER_ID;
										var temp__thumbPhoto = ret.dto.INFO_ABOUT_MAIL[i].THUMB_PHOTO;
										if(temp__userId == null){
											temp__userId = Math.floor(Math.random() * 10)  + "1111111-1111-1111-1111-" + Math.floor(Math.random() * 10);
											temp__thumbPhoto = null;
										}
										ret.dto.INFO_ABOUT_MAIL[i].SRC = userManager.getUserPhoto( temp__userId, null , temp__thumbPhoto);
									}else{
										ret.dto.INFO_ABOUT_MAIL[i].SRC = ret.dto.INFO_ABOUT_MAIL[i].THUMB_PHOTO_ADDRESSBOOK;
									}
									
									ret.dto.INFO_ABOUT_MAIL[i].CHECKBOX_VISIBLE = "none";
									ret.dto.INFO_ABOUT_MAIL[i].PROFILE_VISIBLE = "visible"; 
									
								}  
									 
								var columnOpt = { 
										 "1" : {
											event : {
												onCreated : function(index, data, nTd) { 
													Top.Dom.selectById(nTd.children[0].childs[0].childs[0].id).setProperties({'on-click':'clickImportant'});
													Top.Dom.selectById(nTd.children[0].childs[1].childs[1].id).setHTML(ret.dto.INFO_ABOUT_MAIL[Top.Dom.selectById('mailThreePanelLayout_Table').getStartRowNum()+index].DISPLAY_NAME);
													
													Top.Dom.selectById("CheckBox406_"+index+"_0").setProperties({'visible':mailDataRepo.mailReceivedBoxDataList[Top.Dom.selectById('mailThreePanelLayout_Table').getStartRowNum()+index].CHECKBOX_VISIBLE});
													Top.Dom.selectById("ImageButton201_"+index+"_0").setProperties({'visible':mailDataRepo.mailReceivedBoxDataList[Top.Dom.selectById('mailThreePanelLayout_Table').getStartRowNum()+index].PROFILE_VISIBLE});
												}
											}
										}
								};
	
								Top.Dom.selectById('mailThreePanelLayout_Table').setProperties({"column-option": columnOpt}); 
								mailData.intervalThree = setInterval(function() {
									Top.Controller.get('mailThreePanelMainLogic').setAttrDragAndDrop();
								}, 1000);
							} else
								ret.dto.INFO_ABOUT_MAIL = [];
							
							mailDataRepo.setValue('mailReceivedBoxDataList',ret.dto.INFO_ABOUT_MAIL); 
							Top.Dom.selectById('mailThreePanelLayout_Table').setProperties({'empty-message':'검색 결과가 없습니다.'});
							Top.Dom.selectById('mailThreePanelLayout_Table').removeClass('mail-loading');
							Top.Controller.get('mailThreePanelMainLogic').setCheckAll(false);
							
						}
					    
					});
					$(document).ready(function(){
						$("div#mailThreePanelLayout_Table div.top-tableview-scrollBody td.checkable").css( "text-align", "left");
					});
					if (mailData.curBox.FOLDER_ID == "AA"){
						mailData.curBox.FOLDER_ID = "" 
					}
					
					
	 }, callMailList : function() {
		 
		 
		 
			Top.Dom.selectById('mailThreePanelLayout_Table').addClass('mail-loading');
			Top.Dom.selectById('mailThreePanelLayout_Table').setProperties({'empty-message':'로딩 중...'});
			Top.Controller.get('mailMainLeftLogic').searchKeyword = null; 

			if (mailData.curBox.FOLDER_ID == ""){
				mailData.curBox.FOLDER_ID = "AA"; 
			}
			
			Top.Ajax.execute({
				
				type : "GET",
				url : _workspace.url + "Mail/Mail?action=ListGetbyFolder&CMFLG=FolderToMailList&FOLDER_ID="+mailData.curBox.FOLDER_ID+"&ACCOUNT_ID=" 
				+ mailData.curBox.ACCOUNT_ID + "&USER_ID="+userManager.getLoginUserId()+
				'&SEEING_TYPE_CODE=' + Top.Controller.get('mailMainLeftLogic').seeingTypeCode + '&CURRPAGE=' + mailData.curPage +
				'&PAGELENGTH=' + mailData.pageLength+'&SORTING=DATE_DESC'+'&q='+new Date().getTime(),
				dataType : "json",
				crossDomain: true,
				contentType: "application/json",
			    xhrFields: {withCredentials: true},
				success : function(ret,xhr,status) {
					//console.log(refreshFlag);
					var endPage = Math.ceil(ret.dto.dbio_total_count / mailData.pageLength);
					if(endPage != 0 && mailData.curPage > endPage) {
						mailData.curPage = endPage;
						Top.Controller.get('mailThreePanelMainLogic').callMailList();
						return;
					}
					
					mailData.curBox.UNREAD_COUNT = ret.dto.COUNT_NOT_READ;
					
					Top.Dom.selectById('mailMainLayout_AllCount').setText(ret.dto.dbio_total_count);  
					Top.Dom.selectById('mailMainLayout_NotReadCount').setText(ret.dto.COUNT_NOT_READ);
					
					Top.Dom.selectById('mailThreePanelLayout_Pagination').setProperties({'total':endPage, 'page':mailData.curPage});
					
					if(ret.dto.INFO_ABOUT_MAIL) {
						for(var i=0; i<ret.dto.INFO_ABOUT_MAIL.length; i++) {

							ret.dto.INFO_ABOUT_MAIL[i] = mail.arrangeData(ret.dto.INFO_ABOUT_MAIL[i]);
							
							
							var temp = (i+1)%5;
							var ran =  temp == 0 ? 5 : temp;
							if(ret.dto.INFO_ABOUT_MAIL[i].THUMB_PHOTO_ADDRESSBOOK == null){
								var temp__userId = ret.dto.INFO_ABOUT_MAIL[i].PROFILE_USER_ID;
								var temp__thumbPhoto = ret.dto.INFO_ABOUT_MAIL[i].THUMB_PHOTO;
								if(temp__userId == null){
									temp__userId = Math.floor(Math.random() * 10)  + "1111111-1111-1111-1111-" + Math.floor(Math.random() * 10);
									temp__thumbPhoto = null;
								}
								ret.dto.INFO_ABOUT_MAIL[i].SRC = userManager.getUserPhoto( temp__userId, null , temp__thumbPhoto);
							}else{
								ret.dto.INFO_ABOUT_MAIL[i].SRC = ret.dto.INFO_ABOUT_MAIL[i].THUMB_PHOTO_ADDRESSBOOK;
							}
							
							ret.dto.INFO_ABOUT_MAIL[i].CHECKBOX_VISIBLE = "none";
							ret.dto.INFO_ABOUT_MAIL[i].PROFILE_VISIBLE = "visible"; 
						}
						
						var columnOpt = { 
								 "1" : {
									event : {
										onCreated : function(index, data, nTd) { 
											Top.Dom.selectById(nTd.children[0].childs[0].childs[0].id).setProperties({'on-click':'clickImportant'});
											Top.Dom.selectById(nTd.children[0].childs[1].childs[1].id).setHTML(ret.dto.INFO_ABOUT_MAIL[Top.Dom.selectById('mailThreePanelLayout_Table').getStartRowNum()+index].DISPLAY_NAME);
											Top.Dom.selectById(nTd.children[0].childs[1].childs[2].id).setHTML(ret.dto.INFO_ABOUT_MAIL[Top.Dom.selectById('mailThreePanelLayout_Table').getStartRowNum()+index].SUBJECT);
											
											Top.Dom.selectById("CheckBox406_"+index+"_0").setProperties({'visible':mailDataRepo.mailReceivedBoxDataList[Top.Dom.selectById('mailThreePanelLayout_Table').getStartRowNum()+index].CHECKBOX_VISIBLE});
											Top.Dom.selectById("ImageButton201_"+index+"_0").setProperties({'visible':mailDataRepo.mailReceivedBoxDataList[Top.Dom.selectById('mailThreePanelLayout_Table').getStartRowNum()+index].PROFILE_VISIBLE});
											 
										}
									}
								}
						};

						Top.Dom.selectById('mailThreePanelLayout_Table').setProperties({"column-option": columnOpt}); 
					} else
						ret.dto.INFO_ABOUT_MAIL = [];
					
					
					mailDataRepo.setValue('mailReceivedBoxDataList',ret.dto.INFO_ABOUT_MAIL);
					Top.Dom.selectById('mailThreePanelLayout_Table').setProperties({'empty-message':'편지가 존재하지 않습니다.'});
					Top.Dom.selectById('mailThreePanelLayout_Table').removeClass('mail-loading');
					Top.Dom.selectById('mailThreePanelLayout_Table').checkAll(false);
					
					if (mailDataRepo.mailReceivedBoxDataList.length != 0){
						
						if (Top.Controller.get('mailReadButtonLogic').goToThreePanel == true || Top.Controller.get('mailWriteLogic').goToThreePanel == true || mailData.routeInfo.mailId != undefined) {
							
							if(Top.Controller.get('mailThreePanelMainLogic').isPageChanged == true) {
								Top.Controller.get('mailThreePanelMainLogic').clickedMail = undefined;
								Top.Controller.get('mailThreePanelMainLogic').isPageChanged = false;
							}
							else
								Top.Controller.get('mailThreePanelMainLogic').clickedMail = Top.Controller.get('mailMainLeftLogic').clickedMail;
							
							if(Top.Controller.get('mailMainLeftLogic').fromNoti == true){
								Top.Controller.get('mailMainLeftLogic').fromNoti = false;
							}
							else if (Top.Controller.get('mailWriteLogic').goToThreePanel == true)
								Top.Dom.selectById('mailThreePanelLayout_Content').src('mailWriteLayout.html' + verCsp());
							else if (Top.Controller.get('mailReadButtonLogic').goToThreePanel == true && Top.Controller.get('mailThreePanelMainLogic').clickedMail){ 
								Top.Dom.selectById('mailThreePanelLayout_Table').checkAll(false);
								Top.Dom.selectById('mailThreePanelLayout_Content').src('mailReadLayout.html' + verCsp());
							}
							else if(!Top.Dom.selectById('mailReadLayout')) // 읽고 있던 메일이 오른쪽에 펴진 상태에서는 다시 부를 필요 없음
								Top.Dom.selectById('mailThreePanelLayout_Content').src('mailSelectMailLayout.html' + verCsp());
							
						
						} else if(!mail.isMobile()) {
							
							Top.Dom.selectById('mailThreePanelLayout_Content').src('mailSelectMailLayout.html' + verCsp());
							
						}
							
						Top.Controller.get('mailReadButtonLogic').goToThreePanel = false; 	
						mailData.intervalThree = setInterval(function() {
							Top.Controller.get('mailThreePanelMainLogic').setAttrDragAndDrop();
						}, 1000);

					}
					else if (mailDataRepo.mailReceivedBoxDataList.length == 0){
						return; 
					}
				}
			    
			});
			$(document).ready(function(){
				$("div#mailThreePanelLayout_Table div.top-tableview-scrollBody td.checkable").css( "text-align", "left");
				$("div#mailThreePanelLayout_Table  .auto-pagination top-pagination").css("margin-left","2.81rem"); 
			});
			if (mailData.curBox.FOLDER_ID == "AA"){
				mailData.curBox.FOLDER_ID = ""; 
			}
			
	 }, setAttrDragAndDrop : function(){
			var eleRemove = $("#mailThreePanelLayout_Table .top-tableview .body tr");
			for(var i=0; i<eleRemove.length; i++){
				eleRemove[i].classList.remove("dnd__item");
			}
			
			if($("#mailThreePanelLayout_Table .top-tableview .body tr").length > 0){
				var element = $("#mailThreePanelLayout_Table .top-tableview .body tr")[0].classList;
				for(var i=0; i<element.length; i++){
					if(element[i] == "dnd__item")
					{
						clearInterval(mailData.intervalThree);
						break;
					}
				}
				const row = $("#mailThreePanelLayout_Table .top-tableview .body tr");
				for(let i=0; i<row.length; i++){
					row[i].addClass("dnd__item"); 
					row[i].setAttribute("data-dnd-app", "mail");
					row[i].setAttribute("mail-container-type", "three");
				};
//				$("div#mailTwoPanelLayout_Mails").addClass("dnd__container").attr("data-dnd-app", "mail");
				$("div#mailThreePanelLayout_Table").addClass("dnd__container").attr("data-dnd-app", "mail");
				
				dnd.update();
				for(var i=0; i<element.length; i++){
					if(element[i] == "dnd__item")
					{
						clearInterval(mailData.intervalThree);
						break;
					}
				}
			}
		}, getReadMail : function(event,widget,data) {  
			
			
			if(data.target.classList[0] == 'top-checkbox-icon' || data.target.classList[0] == "top-icon-content" || data.target.classList[0] == "top-checkbox-check")
				return;
			
			if(mail.isWriting()) {
				mailData.exitPath = mailData.routeInfo.folderId + '/' + Top.Dom.selectById('mailThreePanelLayout_Table').getClickedData().MAIL_ID;
//				Top.Dom.selectById('mailExitWriteDialog').open();
				TeeAlarm.open({'title' :'편지를 저장하지 않고 나가시겠습니까?', 'content' :'수정 사항이 저장되지 않았습니다.',	
					buttons : [{'text' : '나가기', 'onClicked' : function(){ 
						Top.Controller.get('mailExitWriteLogic').clickExit();
						TeeAlarm.close();
						}}]})
				return;
			}
			
			
			Top.Controller.get('mailMainLeftLogic').clickedMail = Top.Dom.selectById('mailThreePanelLayout_Table').getClickedData().MAIL_ID;
			Top.Controller.get('mailMainLeftLogic').clickedMailProfile = Top.Dom.selectById('mailThreePanelLayout_Table').getClickedData().SRC;
			var mailIdList = [];
			mailIdList.push(Top.Dom.selectById('mailThreePanelLayout_Table').getClickedData().MAIL_ID);
			var data={
					dto: {
						"CMFLG"				: "ChangeMailStatus",
						"dbio_total_count"	: Top.Dom.selectById('mailThreePanelLayout_Table').getCheckedData().length,
						"ACCOUNT_ID"		: mailData.curBox.ACCOUNT_ID,
						"MAIL_MST_TYPE"		: "SEEN_YN_CODE",
						"MAIL_MST_VALUE"	: "REA0001",
						"MAIL_ID"			: mailIdList
					}
			};
			
			Top.Ajax.execute({
				
				type : "POST",
				url : _workspace.url + "Mail/Mail?action=ChangeStatus",
				dataType : "json",
				data	 : JSON.stringify(data),
				crossDomain: true,
				contentType: "application/json",
			    xhrFields: {withCredentials: true},
				success : function(ret,xhr,status) {
					
					Top.Controller.get('mailThreePanelMainLogic').setCheckAll(false);
					Top.Controller.get('mailThreePanelMainLogic').buttonDisabled();
					widget.checkAll(false);
					widget.check(event.detail.dataIndex, true);
					// 메일 클릭시 해당 메일만 체크 
				}
			});
			
			mailDataRepo.mailReceivedBoxDataList[Top.Dom.selectById('mailThreePanelLayout_Table').getClickedIndex()[0]].TEXT_COLOR = '#6A6C71';
			mailDataRepo.mailReceivedBoxDataList[Top.Dom.selectById('mailThreePanelLayout_Table').getClickedIndex()[0]].TEXT_STYLE = '';
			mailDataRepo.mailReceivedBoxDataList[Top.Dom.selectById('mailThreePanelLayout_Table').getClickedIndex()[0]].SEEN_YN_CODE = 'REA0001';
			
			this.clickedMail =	Top.Dom.selectById('mailThreePanelLayout_Table').getClickedData().MAIL_ID;
			Top.Controller.get('mailMainLeftLogic').clickedMail = this.clickedMail;
			if(mailData.routeInfo.folderId == undefined) mailData.routeInfo.folderId = '1';
			
			if(mailData.curBox.DISPLAY_NAME == "임시 보관함"){
	            let sub = appManager.getSubApp();
	    		if(sub) 
		    		Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/new' +"?sub=" + sub+'&q=' + new Date().getTime(), {eventType:'fold'});
	    		else 
	    			Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/new?q=' + new Date().getTime(), {eventType:'close'});	
	    		Top.Controller.get('mailWriteLogic').flg__loadTempMail = true;
			}else{
				let sub = appManager.getSubApp();
				if(sub) 
					Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + mailData.routeInfo.folderId + '/' + Top.Controller.get('mailMainLeftLogic').clickedMail+ "?sub=" + sub + '&q='+ new Date().getTime(), {eventType:'fold'});
				else 
					Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + mailData.routeInfo.folderId + '/' + Top.Controller.get('mailMainLeftLogic').clickedMail +'?q='+ new Date().getTime(), {eventType:'close'});
			}
			 
	 }, clickImportant : function(event, widget) {
		 
		 var priority;
		 
		 if(widget.getProperties('text-color') == '#C5C5C8')
			 priority = "PRI0001";
		 else
			 priority = "PRI0002";
		 
		 var data={
				 dto: {
					 "CMFLG"			: "ChangeMailStatus",
					 "dbio_total_count"	:"1",
					 "ACCOUNT_ID"		: mailData.curBox.ACCOUNT_ID,
					 "MAIL_MST_TYPE"	: "IMPORTANCE_YN_CODE",
					 "MAIL_MST_VALUE"	: priority,
					 "MAIL_ID"			: [mailDataRepo.mailReceivedBoxDataList[Top.Dom.selectById('mailThreePanelLayout_Table').getClickedIndex()[0]].MAIL_ID]
				 }
		 };
		 
		 Top.Ajax.execute({
			 type : "POST",
			 url : _workspace.url + "Mail/Mail?action=ChangeStatus",
			 dataType : "json",
			 data	 : JSON.stringify(data),
			 crossDomain: true,
			 contentType: "application/json",
			 xhrFields: {withCredentials: true},
			 success : function(ret,xhr,status) {
				 if(mailData.curBox.FOLDER_TYPE == "MFL0002") {
					 if(mailDataRepo.mailReceivedBoxDataList[Top.Dom.selectById('mailThreePanelLayout_Table').getClickedIndex()[0]].MAIL_ID == mailData.routeInfo.mailId){
							let sub = appManager.getSubApp();
							if(sub) 
								Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + mailData.routeInfo.folderId + "?sub=" + sub + '&q='+ new Date().getTime(), {eventType:'fold'});
							else 
								Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + mailData.routeInfo.folderId +"?q="+ new Date().getTime(), {eventType:'close'});

					 }
					else {
						Top.Controller.get('mailThreePanelMainLogic').callMailList(); 
					}
				 } else {
					 var changeColor = (priority == "PRI0001") ? '#6C56E5' : '#C5C5C8';
					 widget.setProperties({'text-color':changeColor});
					 mailDataRepo.mailReceivedBoxDataList[Top.Dom.selectById('mailThreePanelLayout_Table').getClickedIndex()[0]].IMPORTANCE_YN_CODE = changeColor;
					 if(mail.isReading() && mailDataRepo.mailReceivedBoxDataList[Top.Dom.selectById('mailThreePanelLayout_Table').getClickedIndex()[0]].MAIL_ID == mailData.routeInfo.mailId) {
						 Top.Dom.selectById('mailReadLayout_Importance').setProperties({'text-color':changeColor});
					 }
				 }
				 Top.Controller.get('mailMainLeftLogic').resetCount();
			 }	 
		 });
		 
	 }, checkAllFalse : function() {
		 
		 Top.Dom.selectById('mailThreePanelLayout_Receive_All_CheckBox').setChecked(false);
		 Top.Dom.selectById('mailThreePanelLayout_Sent_All_CheckBox').setChecked(false);
		 Top.Dom.selectById('mailThreePanelLayout_ToMe_All_CheckBox').setChecked(false);
		 Top.Dom.selectById('mailThreePanelLayout_Spam_All_CheckBox').setChecked(false);
		 Top.Dom.selectById('mailThreePanelLayout_Trash_All_CheckBox').setChecked(false);
		 Top.Dom.selectById('mailThreePanelLayout_Temp_All_CheckBox').setChecked(false);
		 
	 }, getSeeingTypeCode : function(TypeText){
		 if(TypeText == "전체")
				return "ALL";
			else if(TypeText == "보낸 사람")
				return "SENDER";
			else if(TypeText == "제목")
				return "SUBJECT";
			else if(TypeText == "첨부 파일명")
				return "ATT";
			else
				return "ALL";
			
	}, getMailSearchKeyword :function(){
		var map = {};
		var seeingTypeCode;
		var SUBJECT;
		var id= ['Receive','Sent','Temp','ToMe','Spam','Trash'];
		
		
		for(var i=0; i<id.length; i++){
		    if(Top.Dom.selectById('mailThreePanelLayout_'+id[i]).getProperties('visible') == 'visible'){
		    	if( Top.Dom.selectById('mailThreePanelLayout_'+id[i]+'_SelectBox_Filter').getProperties('selectedIndex') == undefined )
					seeingTypeCode = "ALL";
				else{
					seeingTypeCode = this.getSeeingTypeCode(Top.Dom.selectById('mailThreePanelLayout_'+id[i]+'_SelectBox_Filter').getProperties('selectedText'));
					filterName = Top.Dom.selectById('mailThreePanelLayout_'+id[i]+'_SelectBox_Filter').getProperties('selectedText');
					filterIndex = Top.Dom.selectById('mailThreePanelLayout_'+id[i]+'_SelectBox_Filter').getProperties('selectedIndex');
				}
		    	
				SUBJECT = Top.Dom.selectById('mailThreePanelLayout_'+id[i]+'_Search_Text').getText();
				
				map = {'seeingTypeCode':seeingTypeCode, 'SUBJECT' : SUBJECT, 'filterName' : filterName, 'filterIndex' : filterIndex};
				return map;
		    }
		}
	}, setFilterSelectbox : function() {
		
//		var receiveFilter = ["모든 메일", "안 읽은 메일", "중요 메일", "첨부 메일", "나에게 온 메일"];
//		var sentFilter = ["모든 메일", "안 읽은 메일", "중요 메일", "첨부 메일"];
//		var tempFilter = ["모든 메일", "중요 메일", "첨부 메일"];
//		var tomeFilter = ["모든 메일", "안 읽은 메일", "중요 메일", "첨부 메일"];
//		var spamFilter = ["모든 메일", "안 읽은 메일", "중요 메일", "첨부 메일", "나에게 온 메일"];
//		var trashFilter = ["모든 메일", "안 읽은 메일", "중요 메일", "첨부 메일", "나에게 온 메일"];
		
		var searchNode = ['전체', '보낸 사람', '제목','첨부 파일명'];
		
		Top.Dom.selectById('mailThreePanelLayout_Receive_SelectBox_Filter').setProperties({'nodes':searchNode});
		Top.Dom.selectById('mailThreePanelLayout_Sent_SelectBox_Filter').setProperties({'nodes':searchNode});
		Top.Dom.selectById('mailThreePanelLayout_ToMe_SelectBox_Filter').setProperties({'nodes':searchNode});
		Top.Dom.selectById('mailThreePanelLayout_Spam_SelectBox_Filter').setProperties({'nodes':searchNode});
		Top.Dom.selectById('mailThreePanelLayout_Trash_SelectBox_Filter').setProperties({'nodes':searchNode});
		Top.Dom.selectById('mailThreePanelLayout_Temp_SelectBox_Filter').setProperties({'nodes':searchNode});
		
		Top.Dom.selectById('mailThreePanelLayout_Receive_SelectBox_Filter').setProperties({'selectedIndex':0});
		Top.Dom.selectById('mailThreePanelLayout_Sent_SelectBox_Filter').setProperties({'selectedIndex':0});
		Top.Dom.selectById('mailThreePanelLayout_ToMe_SelectBox_Filter').setProperties({'selectedIndex':0});
		Top.Dom.selectById('mailThreePanelLayout_Spam_SelectBox_Filter').setProperties({'selectedIndex':0});
		Top.Dom.selectById('mailThreePanelLayout_Trash_SelectBox_Filter').setProperties({'selectedIndex':0});
		Top.Dom.selectById('mailThreePanelLayout_Temp_SelectBox_Filter').setProperties({'selectedIndex':0});
		
		
	}, setEvent__SearchIcon : function(){
		var id= ['Receive','Sent','Temp','ToMe','Spam','Trash'];
		for(var i=0; i<id.length; i++){
			Top.Dom.selectById('mailThreePanelLayout_'+id[i]+'_Search_Text').setProperties({'on-keypress':'getEnter'});
			Top.Dom.selectById('mailThreePanelLayout_'+id[i]+'_Search_Text').setProperties({'icon':'icon-work_search'});
			Top.Dom.selectById('mailThreePanelLayout_'+id[i]+'_Search_Text').setProperties({'on-iconclick':'searchMail'});	
		}
	}, setOnClick__AddressIcon : function(){
		var id = ['Receive','Sent','ToMe','Spam','Trash','Temp'];
		for(var i=0; i<id.length; i++){
			Top.Dom.selectById('mailThreePanelLayout_'+id[i]+'_AddressBook').setProperties({'on-click':Top.Controller.get('mailMainLeftLogic').openAddress}); 
			Top.Dom.selectById('mailThreePanelLayout_'+id[i]+'_Setting').setProperties({'on-click':Top.Controller.get('mailMainLeftLogic').openSetting});
		}
	}, setMailCountPageLength : function(){
		 
		 var folerId = mailData.curBox.FOLDER_ID;
		 if ( folerId == ""){
				folerId = "AA";
			}
		 
		 Top.Ajax.execute({
				
				type : "GET",
				url : _workspace.url +"Mail/Mail?action=ListGetbyFolder&CMFLG=FolderToMailList&FOLDER_ID="+folerId+"&ACCOUNT_ID=" + mailData.curBox.ACCOUNT_ID + "&USER_ID="+userManager.getLoginUserId()+
				'&SEEING_TYPE_CODE=ALL&CURRPAGE=1&PAGELENGTH=0',
				dataType : "json",
				crossDomain: true,
				contentType: "application/json",
			    xhrFields: {withCredentials: true},
				success : function(ret,xhr,status) {
					mailData.curBox.UNREAD_COUNT = ret.dto.COUNT_NOT_READ;
					
					Top.Dom.selectById('mailMainLayout_AllCount').setText(ret.dto.dbio_total_count);  
					Top.Dom.selectById('mailMainLayout_NotReadCount').setText(ret.dto.COUNT_NOT_READ);
					var endPage = Math.ceil(ret.dto.dbio_total_count / mailData.pageLength);
					Top.Dom.selectById('mailThreePanelLayout_Pagination').setProperties({'total':endPage, 'page':mailData.curPage});
					
					
				}
		 });

	}, setCheckAll : function(flag) {
		Top.Dom.selectById('mailThreePanelLayout_Receive_All_CheckBox').setChecked(flag);
		Top.Dom.selectById('mailThreePanelLayout_Sent_All_CheckBox').setChecked(flag);
		Top.Dom.selectById('mailThreePanelLayout_ToMe_All_CheckBox').setChecked(flag);
		Top.Dom.selectById('mailThreePanelLayout_Spam_All_CheckBox').setChecked(flag);
		Top.Dom.selectById('mailThreePanelLayout_Trash_All_CheckBox').setChecked(flag);
		Top.Dom.selectById('mailThreePanelLayout_Temp_All_CheckBox').setChecked(flag);
	
	}, openFilterDialog : function(widget, event){
		var widget = event.id;
		var top = $('#' + widget).offset().top + 15 +'px';
		var left = $('#' + widget).offset().left+ 20 +'px';
		Top.Dom.selectById('mailFilterMenuDialog').setProperties({
			'layout-left': left,
			'layout-top': top
		});
		Top.Dom.selectById('mailFilterMenuDialog').open();
		
		
	},filterMail : function(seeingTypeInput, sortingType) {
	 	
		Top.Dom.selectById('mailThreePanelLayout_Table').addClass('mail-loading');
		Top.Dom.selectById('mailThreePanelLayout_Table').setProperties({'empty-message':'검색 중...'});
		mailDataRepo.setValue('mailReceivedBoxDataList',[]);
		
		if (mailData.curBox.FOLDER_ID == ""){
			mailData.curBox.FOLDER_ID = "AA"
		}
	
//		var str_subject;
//		var map; //seeingTypeCode, SUBJECT
//		if (typeof searchKeyword == "object")
//			map = searchKeyword;
//		else
//			map =  this.getMailSearchKeyword();
//		if(map.SUBJECT == "")
//			str_subject = "";
//		else
//			str_subject = "&SUBJECT="+escape(encodeURIComponent(map.SUBJECT));
//		
//		Top.Controller.get('mailMainLeftLogic').searchKeyword = map; // 3패널로 넘어갈때를 위해 저장
//
//		
//		
				Top.Ajax.execute({
					
					type : "GET",
					url : _workspace.url + "Mail/Mail?action=ListGetbyFolder&CMFLG=FolderToMailList&FOLDER_ID="+mailData.curBox.FOLDER_ID+"&ACCOUNT_ID=" + mailData.curBox.ACCOUNT_ID +
					"&USER_ID="+userManager.getLoginUserId()+
					'&SEEING_TYPE_CODE='+ seeingTypeInput + 
					'&CURRPAGE=' + mailData.curPage +
					'&PAGELENGTH=' + mailData.pageLength +
					'&SORTING='+sortingType+'&q='+new Date().getTime(),
					dataType : "json",
					crossDomain: true,
					contentType: "application/json",
				    xhrFields: {withCredentials: true},
					success : function(ret,xhr,status) {
						
						
						Top.Dom.selectById('mailThreePanelLayout_Pagination').setProperties({'total':Math.ceil(ret.dto.dbio_total_count / mailData.pageLength), 'page':mailData.curPage});
						
						if (Top.Dom.selectById("mailMainLayout_NotReadCount") != undefined && Top.Dom.selectById("mailMainLayout_NotReadCount") != null){
							Top.Dom.selectById("mailMainLayout_NotReadCount").setText(ret.dto.COUNT_NOT_READ)
						}
						if(Top.Dom.selectById("mailMainLayout_AllCount") != undefined && Top.Dom.selectById("mailMainLayout_AllCount") != null){
							Top.Dom.selectById("mailMainLayout_AllCount").setText(ret.dto.dbio_total_count)
						}
						
						mailData.curBox.UNREAD_COUNT = ret.dto.COUNT_NOT_READ;
						
						if(ret.dto.INFO_ABOUT_MAIL) {
							for(var i=0; i<ret.dto.INFO_ABOUT_MAIL.length; i++) {
								
								ret.dto.INFO_ABOUT_MAIL[i] = mail.arrangeData(ret.dto.INFO_ABOUT_MAIL[i]);
								
//								var searchText = map.SUBJECT;
//								ret.dto.INFO_ABOUT_MAIL[i].SUBJECT = ret.dto.INFO_ABOUT_MAIL[i].SUBJECT.replace(searchText,'<span class="mail-search">'+searchText+'</span>');
								
								var temp = (i+1)%5;
								var ran =  temp == 0 ? 5 : temp;
								if(ret.dto.INFO_ABOUT_MAIL[i].THUMB_PHOTO_ADDRESSBOOK == null){
									var temp__userId = ret.dto.INFO_ABOUT_MAIL[i].PROFILE_USER_ID;
									var temp__thumbPhoto = ret.dto.INFO_ABOUT_MAIL[i].THUMB_PHOTO;
									if(temp__userId == null){
										temp__userId = Math.floor(Math.random() * 10)  + "1111111-1111-1111-1111-" + Math.floor(Math.random() * 10);
										temp__thumbPhoto = null;
									}
									ret.dto.INFO_ABOUT_MAIL[i].SRC = userManager.getUserPhoto( temp__userId, null , temp__thumbPhoto);
								}else{
									ret.dto.INFO_ABOUT_MAIL[i].SRC = ret.dto.INFO_ABOUT_MAIL[i].THUMB_PHOTO_ADDRESSBOOK;
								}
								
								ret.dto.INFO_ABOUT_MAIL[i].CHECKBOX_VISIBLE = "none";
								ret.dto.INFO_ABOUT_MAIL[i].PROFILE_VISIBLE = "visible"; 
								
							}  
								 
							var columnOpt = { 
									 "1" : {
										event : {
											onCreated : function(index, data, nTd) { 
												Top.Dom.selectById(nTd.children[0].childs[0].childs[0].id).setProperties({'on-click':'clickImportant'});
												Top.Dom.selectById(nTd.children[0].childs[1].childs[1].id).setHTML(ret.dto.INFO_ABOUT_MAIL[Top.Dom.selectById('mailThreePanelLayout_Table').getStartRowNum()+index].DISPLAY_NAME);
												
												Top.Dom.selectById("CheckBox406_"+index+"_0").setProperties({'visible':mailDataRepo.mailReceivedBoxDataList[Top.Dom.selectById('mailThreePanelLayout_Table').getStartRowNum()+index].CHECKBOX_VISIBLE});
												Top.Dom.selectById("ImageButton201_"+index+"_0").setProperties({'visible':mailDataRepo.mailReceivedBoxDataList[Top.Dom.selectById('mailThreePanelLayout_Table').getStartRowNum()+index].PROFILE_VISIBLE});
											}
										}
									}
							};

							Top.Dom.selectById('mailThreePanelLayout_Table').setProperties({"column-option": columnOpt}); 
							mailData.intervalThree = setInterval(function() {
								Top.Controller.get('mailThreePanelMainLogic').setAttrDragAndDrop();
							}, 1000);
						} else
							ret.dto.INFO_ABOUT_MAIL = [];
						
						mailDataRepo.setValue('mailReceivedBoxDataList',ret.dto.INFO_ABOUT_MAIL); 
						Top.Dom.selectById('mailThreePanelLayout_Table').setProperties({'empty-message':'검색 결과가 없습니다.'});
						Top.Dom.selectById('mailThreePanelLayout_Table').removeClass('mail-loading');
						Top.Controller.get('mailThreePanelMainLogic').setCheckAll(false);
						
					}
				    
				});
				$(document).ready(function(){
					$("div#mailThreePanelLayout_Table div.top-tableview-scrollBody td.checkable").css( "text-align", "left");
				});
				if (mailData.curBox.FOLDER_ID == "AA"){
					mailData.curBox.FOLDER_ID = "" 
				}
				
				
 } 
	
});
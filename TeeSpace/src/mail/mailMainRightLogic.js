Top.Controller.create('mailMainRightLogic', {

	init : function(event, widget) {
		Top.Dom.selectById('mailMainLayout_Header').setProperties({'visible':'none'});
		
		this.clickedMail = '';
		mailData.selectedBox = [];
		mailData.curBox = [];
		this.widgetList = [];
		
		this.seeingTypeCode='ALL';
		
	
		
//		let mailJsList = [
//			"src/com/CMS/mail/jszip.js",
//			"src/com/CMS/mail/jsziputils.js",
//			"src/com/CMS/mail/mailAddressBookMainLogic.js",
//			"src/com/CMS/mail/mailAutoClassifyLogic.js",
//			"src/com/CMS/mail/mailDialogLogic.js",
//			"src/com/CMS/mail/mailErrMsgLogic.js",
//			"src/com/CMS/mail/mailExternalAccountManagementLogic.js",
//			"src/com/CMS/mail/mailFileListViewLogic.js",
//			"src/com/CMS/mail/mailReadLogic.js",
//			"src/com/CMS/mail/mailSaveCompleteLogic.js",
//			"src/com/CMS/mail/mailSendCompleteLogic.js",
//			"src/com/CMS/mail/mailSettingsMainLogic.js",
//			"src/com/CMS/mail/mailThreePanelMainLogic.js",
//			"src/com/CMS/mail/mailTUserLogic.js",
//			"src/com/CMS/mail/mailTwoPanelLogic.js",
//			"src/com/CMS/mail/mailWriteLogic.js",
//			"src/com/CMS/mail/mailWritePopupLogic.js",
//			"src/com/CMS/mail/mailWritePreviewLogic.js",
//			"src/com/CMS/mail/mailWriteToMeLogic.js",
//			"src/com/CMS/mail/mailWriteToMePopupLogic.js",
//			"src/com/CMS/calendar/TScheduleFilePreviewLayoutLogic.js"
//		];
//		
//		if(!mailLoad){ 
//			let _this = this;
//			cls.loadJs(mailJsList, function(){
//				mailLoad=true;
//				_this.mailInit();
//			});
//		}
//		else{
//			this.mailInit();
//		}
	
	
		
	}, 
	mailInit: function(){
		
		this.setFolder();
		this.setTitle();
		
		Top.Dom.selectById('mailMainLayout_DeleteNotRead').setProperties({'on-click':'deleteNotRead'});
		
		Top.Dom.selectById('mailMainLayout_TempMail').setProperties({'on-click':'moveTempBox'});
		
		Top.Dom.selectById('mailMainLayout_Cancel').setProperties({'on-click':'hideSearch'});
		Top.Dom.selectById('mailMainLayout_Search_Button').setProperties({'on-click':'openSearch'});
		Top.Dom.selectById('mailMainLayout_Search_Text').setProperties({'on-keypress':'getEnter'});
		Top.Dom.selectById('mailMainLayout_Search_Text').setProperties({'icon':'icon-work_search'});
		Top.Dom.selectById('mailMainLayout_Search_Text').setProperties({'on-iconclick':'searchMail'});
		
		Top.Dom.selectById('mailMainLayout_AddressBook').setProperties({'on-click':'openAddress'}); 
		Top.Dom.selectById('mailMainLayout_Setting').setProperties({'on-click':'openSetting'});
		
		Top.Dom.selectById('mailMainLayout_Button_SendMail').setProperties({'on-click':'openNewMail'});
		Top.Dom.selectById('mailMainLayout_Button_SendMeMail').setProperties({'on-click':'openNewMailToMe'});
		this.accountMailInfo = [];
		
		this.getAccList = [];
		
		Top.Dom.selectById('mailMainLayout_FileList').setProperties({'on-click' : 'openFile'});

		mail.mobile1panel();
		
		Top.Dom.selectById('mailMainLayout_Pre').setProperties({'on-click':mail.mobile1panel});
	},
	moveTempBox : function() {
		
		this.exitWrite(Top.Dom.selectById('mailMainLayout_TempMailBox_Text0').getValue().FOLDER_ID);
		
	}, openSetting : function() {
		
		if(mail.isWriting() && mailData.writeMode == 'full')
			this.exitWrite('setting?' + new Date().getTime());
		else
			Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/setting?' + new Date().getTime());
		
	}, openAddress : function() {
		
		if(mail.isWriting() && mailData.writeMode == 'full')
			this.exitWrite('address?' + new Date().getTime());
		else
			Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/address?' + new Date().getTime());
		
	}, openFile : function() {
		
		if(mail.isWriting() && mailData.writeMode == 'full')
			this.exitWrite('file?' + new Date().getTime());
		else
			Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/file?' + new Date().getTime());
		
	}, openNewMail : function() {
		
		if(mail.isWriting() && mailData.writeMode == 'full')
			this.exitWrite('new?' + new Date().getTime());
		else
			mail.newMail();
		
	}, openNewMailToMe : function() {
		
		if(mail.isWriting() && mailData.writeMode == 'full')
			this.exitWrite('newtome?' + new Date().getTime());
		else
			mail.newMailToMe();
		
	}, openFolder : function(event, widget) {
		
		var folderId;
		if(widget.getProperties('value').FOLDER_ID == undefined) {
			
			for(var i=0; i<mailData.accountList.length; i++) {
				if(widget.getProperties('value') == mailData.accountList[i].ACCOUNT_ID)
					folderId = i;
			}
			
		} else
			folderId = widget.getProperties('value').FOLDER_ID;
		
		if(mail.isWriting() && mailData.writeMode == 'full')
			this.exitWrite(folderId);
		else
			Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + folderId);
		
	}, exitWrite : function(path) {
		
		mailData.exitPath = path;
//		Top.Dom.selectById('mailExitWriteDialog').open();
		TeeAlarm.open({'title' :'편지를 저장하지 않고 나가시겠습니까?', 'content' :'수정 사항이 저장되지 않았습니다.',	
			buttons : [{'text' : '나가기', 'onClicked' : function(){ 
				Top.Controller.get('mailExitWriteLogic').clickExit();
				TeeAlarm.close();
				}}]})
	
	}, setTitle : function(writeType) {
		
		
		if(writeType != null)  {
			
			Top.Dom.selectById('mailMainLayout_NotReadCount').setProperties({'visible' : 'none'});
			Top.Dom.selectById('mailMainLayout_CountBar').setProperties({'visible' : 'none'});
			Top.Dom.selectById('mailMainLayout_AllCount').setProperties({'visible' : 'none'});
			Top.Dom.selectById('mailMainLayout_DeleteNotRead').setProperties({'visible' : 'none'});
			Top.Dom.selectById('mailMainLayout_TempMail').setProperties({'visible':'visible'});
			Top.Dom.selectById('mailMainLayout_TempCount').setProperties({'visible':'visible'});
			Top.Dom.selectById('mailMainLayout_Noti').setProperties({'visible':'visible'});
			
			Top.Dom.selectById('mailMainLayout_FolderName').setText(writeType);
			this.resetTempCount();
			
		} else {
			
			var folderType = mailData.curBox.FOLDER_TYPE;
			var parentFolderType = mailData.curBox.PARENT_FOLDER_TYPE;
			
			if(folderType == 'MFL0003' || parentFolderType == 'MFL0003' || folderType == 'file' || folderType == 'address' || folderType == 'setting') {
				Top.Dom.selectById('mailMainLayout_NotReadCount').setProperties({'visible':'none'});
				Top.Dom.selectById('mailMainLayout_CountBar').setProperties({'visible':'none'});
			} else {
				Top.Dom.selectById('mailMainLayout_NotReadCount').setProperties({'visible':'visible'});
				Top.Dom.selectById('mailMainLayout_CountBar').setProperties({'visible':'visible'});
			}
			
			if(folderType == 'file' || folderType == 'address' || folderType == 'setting') {
				Top.Dom.selectById('mailMainLayout_AllCount').setProperties({'visible' : 'none'});
				Top.Dom.selectById('mailMainLayout_DeleteNotRead').setProperties({'visible' : 'none'});
			} else {
				Top.Dom.selectById('mailMainLayout_AllCount').setProperties({'visible' : 'visible'});
				Top.Dom.selectById('mailMainLayout_DeleteNotRead').setProperties({'visible' : 'visible'});
			}
			
			Top.Dom.selectById('mailMainLayout_TempMail').setProperties({'visible':'none'});
			Top.Dom.selectById('mailMainLayout_TempCount').setProperties({'visible':'none'});

			if(mailData.curBox.WIDGET_ID == 'mailMainLayout_Button_SendMail' || mailData.curBox.WIDGET_ID == 'mailMainLayout_Button_SendMeMail')
				Top.Dom.selectById('mailMainLayout_Noti').setProperties({'visible':'visible'});
			else if(folderType == 'file' || folderType == 'address' || folderType == 'setting')
				Top.Dom.selectById('mailMainLayout_Noti').setProperties({'visible':'none'});
			else if(mailData.curBox.WIDGET_ID.split('Text')[1].substr(0,1) == '0')
				Top.Dom.selectById('mailMainLayout_Noti').setProperties({'visible':'visible'});
			else 
				Top.Dom.selectById('mailMainLayout_Noti').setProperties({'visible':'none'});
			
			Top.Dom.selectById('mailMainLayout_FolderName').setText(mailData.curBox.DISPLAY_NAME);
		}
				
		
	}, deleteNotRead : function() {
		
		Top.Dom.selectById('mailDeleteNotReadDialog').open();
		Top.Dom.selectById('mailDeleteNotReadDialog').setProperties({'on-close':
			function(){
			Top.Controller.get('mailMainLeftLogic').callMailList();
			Top.Controller.get('mailMainLeftLogic').resetCount();
			}});
		
	}, openSearch : function() {
		
		Top.Dom.selectById('mailMainLayout_Search_').setProperties({'visible':'visible'});
		Top.Dom.selectById('mailMainLayout_Search_Button').setProperties({'visible':'none'});
		
	}, hideSearch : function() {
		
		Top.Dom.selectById('mailMainLayout_Search_').setProperties({'visible':'none'});
		Top.Dom.selectById('mailMainLayout_Search_Button').setProperties({'visible':'visible'});
		Top.Dom.selectById('mailMainLayout_Search_Text').setText("");
		mailData.curPage = 1;
		this.callMailList();
		
		if(mailData.panelType == '3') {
			Top.Controller.get('mailThreePanelMainLogic').replyClicked = null
			Top.Controller.get('mailThreePanelMainLogic').replyAllClicked = null
			Top.Controller.get('mailThreePanelMainLogic').forwardClicked = null
			Top.Controller.get('mailThreePanelMainLogic').sendAgainClicked = null
			Top.Controller.get('mailThreePanelMainLogic').resetButton();
		}
		
	}, getEnter : function(event) {
		
		if(event.key == "Enter") {
			this.searchMail();
		}
		
	}, searchMail : function() {
	
		mailData.curPage = 1;

		if(mailData.panelType == '3')
			Top.Controller.get('mailThreePanelMainLogic').searchMail();
		else
			Top.Controller.get('mailTwoPanelLogic').searchMailList();
			
		
	}, callMailList : function() {
		
		var ctrl = (mailData.panelType == '3') ? Top.Controller.get('mailThreePanelMainLogic') : Top.Controller.get('mailTwoPanelLogic');
		ctrl.callMailList();
		
	}, checkExternalAccount : function(data) {
		
		var msg = JSON.parse(data.NOTI_MSG);
		var accountList = mailDataRepo.mailExternalDataList;
		if(accountList.length != 0 && accountList[0].USER_ID != userManager.getLoginUserId()) {
			accountList = [];
		}
		
		var sameAccount = 0;
		
		for(var i=0; i<accountList.length; i++) {
			if(msg.ACCOUNT_ID == accountList[i].ACCOUNT_ID || msg.EMAIL_ADDRESS == accountList[i].EMAIL_ADDRESS) {
				sameAccount++;
				accountList[i].ACCOUNT_ID = msg.ACCOUNT_ID;
				if(msg.MAIL_COUNT == '0' || msg.STATE == '0') {
					accountList[i].STATUS = '읽어올 새 외부 메일이 없습니다.';
					accountList[i].COUNT = '0';
				} else {
					accountList[i].STATUS = msg.MAIL_COUNT + '통의 새 메일을 읽어오고 있습니다.';
					accountList[i].COUNT = msg.MAIL_COUNT;
				}
			}
		}
		
		if(sameAccount == 0) {
			if(msg.MAIL_COUNT == '0' || msg.STAUS == '0') {
				accountList.push({
					"EMAIL_ADDRESS"	: msg.EMAIL_ADDRESS,
					"STATUS"		: '읽어올 새 외부 메일이 없습니다.',
					"RECEIVE_SERVER_NAME"	: msg.RECEIVE_SERVER_NAME,
					"ACCOUNT_ID"	: msg.ACCOUNT_ID,
					"USER_ID" 		: msg.USER_ID,
					"COUNT"			: '0'
				});		
			} else {
				accountList.push({
					"EMAIL_ADDRESS"	: msg.EMAIL_ADDRESS,
					"STATUS"		: msg.MAIL_COUNT + '통의 새 메일을 읽어오고 있습니다.',
					"RECEIVE_SERVER_NAME"	: msg.RECEIVE_SERVER_NAME,
					"ACCOUNT_ID"	: msg.ACCOUNT_ID,
					"USER_ID" 		: msg.USER_ID,
					"COUNT"			: msg.MAIL_COUNT
				});	
			}
		}
		
		mailDataRepo.setValue('mailExternalDataList', accountList);
		
		var listYN = 0;
		if(this.getAccList == undefined) this.getAccList = [];
		for(var i=0; i<this.getAccList.length; i++) {
		 if(this.getAccList[i] == msg.ACCOUNT_ID)
		  listYN++;
		}
		if(listYN == 0)
		 this.getAccList.push(msg.ACCOUNT_ID);

		if(msg.MAIL_COUNT == '0' || mail.STAUS == '0')
			return;
		
		if(routeManager.getApp() == 'mail') {
			this.resetCount();
			if((mailData.curBox.FOLDER_ID == '' && mailData.curBox.ACCOUNT_ID == msg.ACCOUNT_ID) 
					|| mailData.curBox.FOLDER_ID == msg.FOLDER_ID)
				this.refreshCurrentList();
		}
		
	}, showCheckExternalPopup : function(event, widget) {
		
		this.extMail = widget.getParent().getChildren()[1].getProperties('value');
		this.extProtocol = widget.getParent().getChildren()[0].getProperties('value');
		Top.Controller.get('mailCheckExternalPopupLogic').getList();
		Top.Dom.selectById('mailCheckExternalDialog').open();
				
	}, showSubFolder : function(event, widget) {
		
		if(widget.hasClass('icon-arrow_filled_down')) {
			
			widget.removeClass('icon-arrow_filled_down');
			widget.addClass('icon-arrow_filled_right');
			
			switch(widget.id.substr(15,6)) {
			case 'Mail_A' :	//mailMainLayout_Mail_Arrow
				var i = widget.id.substring(25, widget.id.length);
				Top.Dom.selectById('mailMainLayout_MailBox_Group'+i).setProperties({'visible':'none'});
				Top.Dom.selectById('mailMainLayout_ImportantMailBox'+i).setProperties({'visible':'none'});
				Top.Dom.selectById('mailMainLayout_SentMailBox_Group'+i).setProperties({'visible':'none'});
				Top.Dom.selectById('mailMainLayout_TempMailBox'+i).setProperties({'visible':'none'});
				Top.Dom.selectById('mailMainLayout_SentMeMailBox_Group'+i).setProperties({'visible':'none'});
				//Top.Dom.selectById('mailMainLayout_SpamMailBox'+i).setProperties({'visible':'none'});
				Top.Dom.selectById('mailMainLayout_TrashMailBox'+i).setProperties({'visible':'none'});
				break;
			case 'MailBo' :	//mailMainLayout_MailBox_Arrow
				var i = widget.id.substring(28, widget.id.length);
				Top.Dom.selectById('mailMainLayout_MailBox_List'+i).setProperties({'visible':'none'});
				break;
			case 'SentMa' :	//mailMainLayout_SentMailBox_Arrow
				var i = widget.id.substring(32, widget.id.length);
				Top.Dom.selectById('mailMainLayout_SentMailBox_List'+i).setProperties({'visible':'none'});
				break;
			case 'SentMe' :	//mailMainLayout_SentMeMailBox_Arrow
				var i = widget.id.substring(34, widget.id.length);
				Top.Dom.selectById('mailMainLayout_SentMeMailBox_List'+i).setProperties({'visible':'none'});
				break;
				
			
			}
			
			
		} else {
			
			widget.removeClass('icon-arrow_filled_right');
			widget.addClass('icon-arrow_filled_down');
			
			switch(widget.id.substr(15,6)) {
			case 'Mail_A' :	//mailMainLayout_Mail_Arrow
				var i = widget.id.substring(25, widget.id.length);
				Top.Dom.selectById('mailMainLayout_MailBox_Group'+i).setProperties({'visible':'visible'});
				Top.Dom.selectById('mailMainLayout_ImportantMailBox'+i).setProperties({'visible':'visible'});
				Top.Dom.selectById('mailMainLayout_SentMailBox_Group'+i).setProperties({'visible':'visible'});
				Top.Dom.selectById('mailMainLayout_TempMailBox'+i).setProperties({'visible':'visible'});
				Top.Dom.selectById('mailMainLayout_SentMeMailBox_Group'+i).setProperties({'visible':'visible'});
				//Top.Dom.selectById('mailMainLayout_SpamMailBox'+i).setProperties({'visible':'visible'});
				Top.Dom.selectById('mailMainLayout_TrashMailBox'+i).setProperties({'visible':'visible'});
				break;
			case 'MailBo' :	//mailMainLayout_MailBox_Arrow
				var i = widget.id.substring(28, widget.id.length);
				Top.Dom.selectById('mailMainLayout_MailBox_List'+i).setProperties({'visible':'visible'});
				break;
			case 'SentMa' :	//mailMainLayout_SentMailBox_Arrow
				var i = widget.id.substring(32, widget.id.length);
				Top.Dom.selectById('mailMainLayout_SentMailBox_List'+i).setProperties({'visible':'visible'});
				break;
			case 'SentMe' :	//mailMainLayout_SentMeMailBox_Arrow
				var i = widget.id.substring(34, widget.id.length);
				Top.Dom.selectById('mailMainLayout_SentMeMailBox_List'+i).setProperties({'visible':'visible'});
				break;
			}
			
		}
		
	}, addPlusIcon : function(event, widget) {
		
		var plusIconLayout = Top.Widget.create('top-linearlayout');
		plusIconLayout.setProperties({'id':'mailMainLayout_MailBox_Plus', 'layout-height':'wrap_content', 'vertical-alignment':'middle', 'layout-width':'match_parent','orientation':'vertical', 'border-width':'0px',
			'layout-vertical-alignment':'MIDDLE', 'class':'ic-gray'});
		
		var plusIconLayout2 = Top.Widget.create('top-linearlayout');
		plusIconLayout2.setProperties({'id':'mailMainLayout_MailBox_Plus2', 'layout-height':'wrap_content', 'layout-width':'wrap_content', 'orientation':'horizontal', 'border-width':'0px',
			'layout-horizontal-alignment':'RIGHT'});
		
		var plusIcon = Top.Widget.create('top-icon');
		plusIcon.setProperties({'id':'mailMainLayout_AddFolderIcon', 'class':'icon-work_add', 'text-size':'11px', 'layout-height':'17px', 'layout-width':'17px', 'on-click':'addSubFolder',
			'layout-vertical-alignment':'middle'});
		
		var menuIcon = Top.Widget.create('top-icon');
		menuIcon.setProperties({'id':'mailMainLayout_MenuIcon', 'class':'icon-work_vertical', 'text-size':'11px', 'layout-height':'17px', 'layout-width':'17px', 
			'margin':'0px 46px 0px 0px', 'on-click':'viewSubMenu', 'layout-vertical-alignment':'middle'});
		
		plusIconLayout2.addWidget(plusIcon);
		plusIconLayout2.addWidget(menuIcon);
		plusIconLayout2.complete();
		plusIconLayout.addWidget(plusIconLayout2);
		plusIconLayout.complete();
		widget.addWidget(plusIconLayout);
		widget.complete();
		
	}, removePlusIcon : function(event, widget) {
		
		if(!Top.Dom.selectById('mailFolderMenuDialog').isOpen()) {
			widget.removeWidget(Top.Dom.selectById('mailMainLayout_MailBox_Plus'));
			widget.complete();
		}
		
	}, addEditIcon : function(event, widget) {
		
		if(widget.getChildren()[0].template.is != 'top-textfield') {
			
			var editIconLayout = Top.Widget.create('top-linearlayout');
			editIconLayout.setProperties({'id':'mailMainLayout_MailBox_Edit', 'layout-height':'match_parent', 'layout-width':'match_parent','orientation':'vertical', 'border-width':'0px',
				'layout-vertical-alignment':'MIDDLE', 'vertical-alignment':'MIDDLE', 'class':'ic-gray'});
			
			var editIconLayout2 = Top.Widget.create('top-linearlayout');
			editIconLayout2.setProperties({'id':'mailMainLayout_MailBox_Edit2', 'layout-height':'wrap_content', 'layout-width':'wrap_content', 'orientation':'horizontal', 'border-width':'0px',
				'layout-horizontal-alignment':'RIGHT'});
			
			var editIcon = Top.Widget.create('top-icon');
			editIcon.setProperties({'id':'mailMainLayout_EditFolderIcon', 'class':'icon-work_edit', 'text-size':'11px', 'layout-vertical-alignment':'middle',
				'layout-height':'17px', 'layout-width':'17px', 'on-click':'editSubFolder'});
			
			var menuIcon = Top.Widget.create('top-icon');
			menuIcon.setProperties({'id':'mailMainLayout_MenuIcon', 'class':'icon-work_vertical', 'text-size':'11px', 'layout-height':'17px', 'layout-width':'17px', 
				'margin':'0px 67px 0px 0px', 'on-click':'viewSubMenu', 'layout-vertical-alignment':'middle'});
			
			editIconLayout2.addWidget(editIcon);
			editIconLayout2.addWidget(menuIcon);
			editIconLayout2.complete();
			editIconLayout.addWidget(editIconLayout2);
			editIconLayout.complete();
			widget.addWidget(editIconLayout);
			widget.complete();
			
		}
		
	}, removeEditIcon : function(event, widget) {

		if(widget.getChildren()[0].template.is != 'top-textfield' && !Top.Dom.selectById('mailFolderMenuDialog').isOpen()) {
			
			widget.removeWidget(Top.Dom.selectById('mailMainLayout_MailBox_Edit'));
			widget.complete();
		}
		
		
	}, addMenuIcon : function(event, widget) {
		
		var menuIconLayout = Top.Widget.create('top-linearlayout');
		menuIconLayout.setProperties({'id':'mailMainLayout_MailBox_Menu', 'layout-height':'wrap_content', 'vertical-alignment':'middle', 'layout-width':'match_parent','orientation':'vertical', 'border-width':'0px',
			'layout-vertical-alignment':'MIDDLE', 'class':'ic-gray'});
		
		var menuIconLayout2 = Top.Widget.create('top-linearlayout');
		menuIconLayout2.setProperties({'id':'mailMainLayout_MailBox_Menu2', 'layout-height':'wrap_content', 'layout-width':'wrap_content', 'orientation':'horizontal', 'border-width':'0px',
			'layout-horizontal-alignment':'RIGHT'});
		
		var menuIcon = Top.Widget.create('top-icon');
		menuIcon.setProperties({'id':'mailMainLayout_MenuIcon', 'class':'icon-work_vertical', 'text-size':'11px', 'layout-height':'17px', 'layout-width':'17px', 
			'margin':'0px 54px 0px 0px', 'on-click':'viewSubMenu', 'layout-vertical-alignment':'middle'});
		
		menuIconLayout2.addWidget(menuIcon);
		menuIconLayout2.complete();
		menuIconLayout.addWidget(menuIconLayout2);
		menuIconLayout.complete();
		widget.addWidget(menuIconLayout);
		widget.complete();
		
	}, removeMenuIcon : function(event, widget) {
		
		if(!Top.Dom.selectById('mailFolderMenuDialog').isOpen()) {
			widget.removeWidget(Top.Dom.selectById('mailMainLayout_MailBox_Menu'));
			widget.complete();	
		}
		
	}, viewSubMenu : function(event, widget) {
		
		widget.addClass('mail-selected-menu');
		
		if(widget.getParent().getParent().getParent().getChildren().length == 4) 
			mailData.selectedBox = JSON.parse(JSON.stringify(widget.getParent().getParent().getParent().getChildren()[1].getProperties('value')));
		else
			mailData.selectedBox = JSON.parse(JSON.stringify(widget.getParent().getParent().getParent().getChildren()[0].getProperties('value')));
		mailData.selectedBox.WIDGET_ID = widget.getParent().getParent().getParent().id;
		
		var top = $('#mailMainLayout_MenuIcon').offset().top + 9 + 'px';
		var left;
		
		left = (mail.isMobile())? $('#mailMainLayout_MenuIcon').offset().left - 130 + 'px' : $('#mailMainLayout_MenuIcon').offset().left + 'px';

		Top.Dom.selectById('mailFolderMenuDialog').setProperties({'layout-left': left,'layout-top': top});
		Top.Dom.selectById('mailFolderMenuDialog').setProperties({'on-close':'removeMenuIcon'});
		Top.Dom.selectById('mailFolderMenuDialog').open();
		
		
	}, editSubFolder : function(event, widget) {
		
		mailData.selectedBox = JSON.parse(JSON.stringify(widget.getParent().getParent().getParent().getChildren()[0].getProperties('value')));
		mailData.selectedBox.WIDGET_ID = widget.getParent().getParent().getParent().id;
		Top.Controller.get('TeeSpaceLogic').modifyFolderName();
		
	}, addSubFolder : function(event, widget) {
		
		mailData.selectedBox = JSON.parse(JSON.stringify(widget.getParent().getParent().getParent().getChildren()[1].getProperties('value')));
		mailData.selectedBox.WIDGET_ID = widget.getParent().getParent().getParent().id;
		Top.Controller.get('TeeSpaceLogic').addNewFolder();
		
	}, deleteTrash : function(event, widget) {
		
		mailData.selectedBox = JSON.parse(JSON.stringify(widget.getParent().getParent().getChildren()[0].getProperties('value')));
		mailData.selectedBox.WIDGET_ID = widget.getParent().getParent().id;
		
		Top.Dom.selectById('mailDeleteTrashDialog').open();
		
		
	}, setFolder : function() {
		
		this.setFolderOnly();
		
		mailData.selectedBox.DISPLAY_NAME = Top.i18n.get().value.m000012;
		mailData.selectedBox.FOLDER_ID = '';
		mailData.curBox.DISPLAY_NAME = Top.i18n.get().value.m000012;
		mailData.curBox.FOLDER_ID = '';
		mailData.curBox.WIDGET_ID = 'mailMainLayout_Mail_Text0';
		mailData.curBox.FOLDER_TYPE = '';
		
	}, setFolderOnly : function(callbackFunc) {
		
		
		
		Top.Ajax.execute({
			
			type : "GET",
			url : _workspace.url + "Mail/Mail?action=BasicInfo&CMFLG=BasicInfo&USER_ID="+userManager.getLoginUserId()+"&WS_USE_CODE=USE0001",
			dataType : "json",
			crossDomain: true,
			contentType: "application/json",
		    xhrFields: {withCredentials: true},
			success : function(ret,xhr,status) {
				

				mailData.accountList = ret.dto.AccountList;
				
				if(Top.Dom.selectById('mailMainLayout_Center_All').getChildren().length == 0) {
					
					mailData.curBox.ACCOUNT_ID = ret.dto.AccountList[0].ACCOUNT_ID;
					Top.Dom.selectById('mailMainLayout_Button_SendMail').setProperties({'value':ret.dto.AccountList[0].ACCOUNT_ID});
					Top.Dom.selectById('mailMainLayout_Button_SendMeMail').setProperties({'value':ret.dto.AccountList[0].ACCOUNT_ID});
					Top.Dom.selectById('mailMainLayout_FileList').setProperties({'value':ret.dto.AccountList[0].ACCOUNT_ID});
					Top.Dom.selectById('mailMainLayout_AddressBook').setProperties({'value':ret.dto.AccountList[0].ACCOUNT_ID});
					Top.Dom.selectById('mailMainLayout_Setting').setProperties({'value':ret.dto.AccountList[0].ACCOUNT_ID});
					
				}
				Top.Dom.selectById('mailMainLayout_MailList').clear();
				Top.Controller.get("mailMainLeftLogic").accountMailInfo = [];

				
				for(var i=0; i<ret.dto.AccountList.length; i++) {
					
					
					var bar = Top.Widget.create('top-linearlayout');
					bar.setProperties({'id':'mailMainLayout_Mail_Bar'+i, 'layout-height':'1px', 'layout-width':'calc(100% + 2px)', 'background-color':'#DDDDDD', 'border-width':'0px', 
						'margin':'10px 0px 10px 0px'});
			
					Top.Controller.get("mailMainLeftLogic").accountMailInfo.push(ret.dto.AccountList[i].EMAIL_ADDRESS);   

					var account = Top.Widget.create('top-linearlayout');
					account.setProperties({'id':'mailMainLayout_Mail'+i,'layout-height':'37px','layout-width':'calc(100% + 2px)','orientation':'horizontal','border-width':'0px', 'line-height':'37px',
						'class':'mail-folder'});
					
					var accountIcon = Top.Widget.create('top-icon');
					accountIcon.setProperties({'id':'mailMainLayout_Mail_Arrow'+i, 'class':'icon-arrow_filled_down', 'layout-height':'wrap_content', 'layout-vertical-alignment':'MIDDLE', 'layout-width':'wrap_content',
						'margin':'0px 0px 0px 15px', 'text-size':'8px', 'on-click':'showSubFolder', 'text-color':'#3C3C3C', 'value':ret.dto.AccountList[i].PROTOCOL});
					var accountText = Top.Widget.create('top-textview');
					accountText.setProperties({'id':'mailMainLayout_Mail_Text'+i, 'layout-height':'wrap_content', 'layout-vertical-alignment':'MIDDLE', 'layout-width':'120px', 'class':'mail-cursor-pointer mail-account-mobile',
						'margin':'0px 0px 0px 6px', 'text':ret.dto.AccountList[i].EMAIL_ADDRESS, 'text-size':'13px', 'value':ret.dto.AccountList[i].ACCOUNT_ID, 
						'on-click':'openFolder', 'text-style':'bold', 'text-color':'#3C3C3C'});
					if(i != 0) 
						accountText.setProperties({'layout-width':'145px'});
					
					account.addWidget(accountIcon);
					account.addWidget(accountText);
					
					if(i == 0 && !isIosApp()) {
						var betaIcon = Top.Widget.create('top-imageview');
						betaIcon.setProperties({'id':'mailMainLayout_Beta', 'class':'settlebank', 'layout-width':'33px', 'layout-height':'15px', 'src':'res/beta_badge.svg', 'layout-vertical-alignment':'MIDDLE', 'layout-horizontal-alignment':'RIGHT'});
						
						account.addWidget(betaIcon);
					}
					if(i !=  0) {
						var plusIcon = Top.Widget.create('top-icon');
						plusIcon.setProperties({'id':'mailMainLayout_CheckExternalIcon'+i, 'class':'ic-gray icon-refresh2', 'text-size':'13px', 'tooltip':'외부 메일 확인', 'layout-vertical-alignment':'MIDDLE',
							'layout-height':'wrap_content', 'layout-width':'wrap_content', 'layout-horizontal-alignment':'RIGHT', 'on-click':'showCheckExternalPopup'});
						
						account.addWidget(plusIcon);
					}
					
					account.complete();
					
					
					var mailboxGroup = Top.Widget.create('top-linearlayout');
					mailboxGroup.setProperties({'id':'mailMainLayout_MailBox_Group'+i, 'layout-height':'wrap_content', 'layout-width':'calc(100% + 2px)', 'border-width':'0px', 'orientation':'vertical'});
					
					var mailbox = Top.Widget.create('top-linearlayout');
					mailbox.setProperties({'id':'mailMainLayout_MailBox'+i, 'layout-height':'35px', 'layout-width':'calc(100% + 2px)', 'margin':'2px 0px 0px 0px', 
						'orientation':'horizontal', 'border-width':'0px', 'on-mouseenter':'addPlusIcon', 'on-mouseleave':'removePlusIcon', 'class':'mail-height-35 mail-folder'});
					
					var mailboxArrow = Top.Widget.create('top-icon');
					mailboxArrow.setProperties({'id':'mailMainLayout_MailBox_Arrow'+i, 'class':'icon-arrow_filled_down', 'layout-height':'wrap_content', 'layout-vertical-alignment':'MIDDLE', 'layout-width':'wrap_content',
						'margin':'0px 6px 0px 28px', 'visible':'hidden', 'text-color':'#3C3C3C', 'text-size':'8px', 'on-click':'showSubFolder'});
					var mailboxText = Top.Widget.create('top-textview');
					mailboxText.setProperties({'id':'mailMainLayout_MailBox_Text'+i, 'layout-height':'wrap_content','layout-vertical-alignment':'MIDDLE', 'layout-width':'wrap_content', 
						'class':'mail-cursor-pointer', 'text':Top.i18n.get().value.m000003, 'text-size':'13px', 'on-click':'openFolder', 'text-color':'#3C3C3C'});
					var mailboxCount = Top.Widget.create('top-textview');
					mailboxCount.setProperties({'id':'mailMainLayout_MailBox_Count'+i, 'layout-height':'wrap_content','layout-vertical-alignment':'MIDDLE', 'layout-width':'wrap_content', 'margin':'0px 0px 0px 5px',
						'text':'', 'text-color':'#6C56E5', 'text-size':'13px', 'class':'mail-cursor-default'});
					
					mailbox.addWidget(mailboxArrow);
					mailbox.addWidget(mailboxText);
					mailbox.addWidget(mailboxCount);
					mailbox.complete();
					
					var mailboxNew = Top.Widget.create('top-linearlayout');
					mailboxNew.setProperties({'id':'mailMainLayout_MailBox_NewFolder'+i, 'layout-height':'35px', 'layout-width':'match_parent', 'visible':'none', 'orientation':'horizontal', 'border-width':'0px'});
					
					var mailboxNewText = Top.Widget.create('top-textfield');
					mailboxNewText.setProperties({'id':'mailMainLayout_MailBox_NewFolder_Text'+i, 'layout-height':'27px', 'layout-width':'130px', 'margin':'5px 0px 0px 42px', 'border-color':'#00A4C3',
						'text':Top.i18n.get().value.m000011, 'text-color':'#333333', 'text-size':'13px', 'max-length':'250'});
					var mailboxNewError = Top.Widget.create('top-icon');
					mailboxNewError.setProperties({'id':'mailMainLayout_MailBox_NewFolder_Error'+i, 'class':'icon-chart_error', 'layout-vertical-alignment':'MIDDLE', 'margin':'0px 0px 0px 6px', 'visible':'none',
						'text-color':'#FF6060', 'text-size':'16px'});
					
					mailboxNew.addWidget(mailboxNewText);
					mailboxNew.addWidget(mailboxNewError);
					mailboxNew.complete();
					
					var mailboxList = Top.Widget.create('top-linearlayout');
					mailboxList.setProperties({'id':'mailMainLayout_MailBox_List'+i, 'layout-height':'wrap_content', 'layout-width':'match_parent', 'border-width':'0px', 'orientation':'vertical'});
					
					mailboxGroup.addWidget(mailbox);
					mailboxGroup.addWidget(mailboxNew);
					mailboxGroup.addWidget(mailboxList);
					mailboxGroup.complete();
					
					
					
					var important_mailbox = Top.Widget.create('top-linearlayout');
					important_mailbox.setProperties({'id':'mailMainLayout_ImportantMailBox'+i, 'layout-height':'35px', 'layout-width':'calc(100% + 2px)', 'orientation':'horizontal', 'border-width':'0px', 
						'on-mouseenter':'addMenuIcon', 'on-mouseleave':'removeMenuIcon', 'class':'mail-height-35 mail-folder'});
					
					var important_mailboxText = Top.Widget.create('top-textview');
					important_mailboxText.setProperties({'id':'mailMainLayout_ImportantMailBox_Text'+i, 'layout-height':'wrap_content', 'layout-vertical-alignment':'MIDDLE', 'layout-width':'wrap_content',
						'margin':'0px 0px 0px 44px', 'class':'mail-cursor-pointer', 'text':Top.i18n.get().value.m000004, 'text-size':'13px', 'on-click':'openFolder', 'text-color':'#3C3C3C'});
					var important_mailboxCount = Top.Widget.create('top-textview');
					important_mailboxCount.setProperties({'id':'mailMainLayout_ImportantMailBox_Count'+i, 'layout-height':'wrap_content', 'layout-vertical-alignment':'MIDDLE', 'layout-width':'wrap_content',
						'margin':'0px 0px 0px 5px', 'text':'', 'text-color':'#6C56E5', 'text-size':'13px', 'class':'mail-cursor-default'});
					
					important_mailbox.addWidget(important_mailboxText);
					important_mailbox.addWidget(important_mailboxCount);
					important_mailbox.complete();
					
					
					
					var sent_mailboxGroup = Top.Widget.create('top-linearlayout');
					sent_mailboxGroup.setProperties({'id':'mailMainLayout_SentMailBox_Group'+i, 'layout-height':'wrap_content', 'layout-width':'calc(100% + 2px)', 'border-width':'0px', 'orientation':'vertical'});
					
					var sent_mailbox = Top.Widget.create('top-linearlayout');
					sent_mailbox.setProperties({'id':'mailMainLayout_SentMailBox'+i, 'layout-height':'35px', 'layout-width':'calc(100% + 2px)', 'margin':'2px 0px 0px 0px', 
						'orientation':'horizontal', 'border-width':'0px', 'on-mouseenter':'addPlusIcon', 'on-mouseleave':'removePlusIcon', 'class':'mail-height-35 mail-folder'});
					
					var sent_mailboxArrow = Top.Widget.create('top-icon');
					sent_mailboxArrow.setProperties({'id':'mailMainLayout_SentMailBox_Arrow'+i, 'class':'icon-arrow_filled_down', 'layout-height':'wrap_content', 'layout-vertical-alignment':'MIDDLE', 'layout-width':'wrap_content',
						'margin':'0px 6px 0px 28px', 'visible':'hidden', 'text-color':'#3C3C3C', 'text-size':'8px', 'on-click':'showSubFolder'});
					var sent_mailboxText = Top.Widget.create('top-textview');
					sent_mailboxText.setProperties({'id':'mailMainLayout_SentMailBox_Text'+i, 'layout-height':'wrap_content','layout-vertical-alignment':'MIDDLE', 'layout-width':'wrap_content', 
						'class':'mail-cursor-pointer', 'text':Top.i18n.get().value.m000005, 'text-size':'13px', 'on-click':'openFolder', 'text-color':'#3C3C3C'});
					var sent_mailboxCount = Top.Widget.create('top-textview');
					sent_mailboxCount.setProperties({'id':'mailMainLayout_SentMailBox_Count'+i, 'layout-height':'wrap_content','layout-vertical-alignment':'MIDDLE', 'layout-width':'wrap_content', 'margin':'0px 0px 0px 5px',
						'text':'', 'text-color':'#6C56E5', 'text-size':'13px'});
					
					sent_mailbox.addWidget(sent_mailboxArrow);
					sent_mailbox.addWidget(sent_mailboxText);
					sent_mailbox.addWidget(sent_mailboxCount);
					sent_mailbox.complete();
					
					var sent_mailboxNew = Top.Widget.create('top-linearlayout');
					sent_mailboxNew.setProperties({'id':'mailMainLayout_SentMailBox_NewFolder'+i, 'layout-height':'35px', 'layout-width':'match_parent', 'visible':'none', 'orientation':'horizontal', 'border-width':'0px'});
					
					var sent_mailboxNewText = Top.Widget.create('top-textfield');
					sent_mailboxNewText.setProperties({'id':'mailMainLayout_SentMailBox_NewFolder_Text'+i, 'layout-height':'27px', 'layout-width':'130px', 'margin':'5px 0px 0px 42px', 'border-color':'#00A4C3',
						'text':Top.i18n.get().value.m000011, 'text-color':'#333333', 'text-size':'13px', 'max-length':'250'});
					var sent_mailboxNewError = Top.Widget.create('top-icon');
					sent_mailboxNewError.setProperties({'id':'mailMainLayout_SentMailBox_NewFolder_Error'+i, 'class':'icon-chart_error', 'layout-vertical-alignment':'MIDDLE', 'margin':'0px 0px 0px 6px', 'visible':'none',
						'text-color':'#FF6060', 'text-size':'16px'});
					
					sent_mailboxNew.addWidget(sent_mailboxNewText);
					sent_mailboxNew.addWidget(sent_mailboxNewError);
					sent_mailboxNew.complete();
					
					var sent_mailboxList = Top.Widget.create('top-linearlayout');
					sent_mailboxList.setProperties({'id':'mailMainLayout_SentMailBox_List'+i, 'layout-height':'wrap_content', 'layout-width':'match_parent', 'border-width':'0px', 'orientation':'vertical'});
					
					sent_mailboxGroup.addWidget(sent_mailbox);
					sent_mailboxGroup.addWidget(sent_mailboxNew);
					sent_mailboxGroup.addWidget(sent_mailboxList);
					sent_mailboxGroup.complete();
					
					
					var temp_mailbox = Top.Widget.create('top-linearlayout');
					temp_mailbox.setProperties({'id':'mailMainLayout_TempMailBox'+i, 'layout-height':'35px', 'layout-width':'calc(100% + 2px)', 'orientation':'horizontal', 'border-width':'0px', 
						'on-mouseenter':'addMenuIcon', 'on-mouseleave':'removeMenuIcon', 'class':'mail-height-35 mail-folder'});
					
					var temp_mailboxText = Top.Widget.create('top-textview');
					temp_mailboxText.setProperties({'id':'mailMainLayout_TempMailBox_Text'+i, 'layout-height':'wrap_content', 'layout-vertical-alignment':'MIDDLE', 'layout-width':'wrap_content',
						'margin':'0px 0px 0px 44px', 'class':'mail-cursor-pointer', 'text':Top.i18n.get().value.m000006, 'text-size':'13px', 'on-click':'openFolder', 'text-color':'#3C3C3C'});
					var temp_mailboxCount = Top.Widget.create('top-textview');
					temp_mailboxCount.setProperties({'id':'mailMainLayout_TempMailBox_Count'+i, 'layout-height':'wrap_content', 'layout-vertical-alignment':'MIDDLE', 'layout-width':'wrap_content',
						'margin':'0px 0px 0px 5px', 'text':'', 'text-color':'#6C56E5', 'text-size':'13px', 'class':'mail-cursor-default'});
					
					temp_mailbox.addWidget(temp_mailboxText);
					temp_mailbox.addWidget(temp_mailboxCount);
					temp_mailbox.complete();
					
					
					
					var sentme_mailboxGroup = Top.Widget.create('top-linearlayout');
					sentme_mailboxGroup.setProperties({'id':'mailMainLayout_SentMeMailBox_Group'+i, 'layout-height':'wrap_content', 'layout-width':'calc(100% + 2px)', 'border-width':'0px', 'orientation':'vertical'});
					
					var sentme_mailbox = Top.Widget.create('top-linearlayout');
					sentme_mailbox.setProperties({'id':'mailMainLayout_SentMeMailBox'+i, 'layout-height':'35px', 'layout-width':'calc(100% + 2px)', 'margin':'2px 0px 0px 0px', 
						'orientation':'horizontal', 'border-width':'0px', 'on-mouseenter':'addPlusIcon', 'on-mouseleave':'removePlusIcon', 'class':'mail-height-35 mail-folder'});
					
					var sentme_mailboxArrow = Top.Widget.create('top-icon');
					sentme_mailboxArrow.setProperties({'id':'mailMainLayout_SentMeMailBox_Arrow'+i, 'class':'icon-arrow_filled_down', 'layout-height':'wrap_content', 'layout-vertical-alignment':'MIDDLE', 'layout-width':'wrap_content',
						'margin':'0px 6px 0px 28px', 'visible':'hidden', 'text-color':'#3C3C3C', 'text-size':'8px', 'on-click':'showSubFolder'});
					var sentme_mailboxText = Top.Widget.create('top-textview');
					sentme_mailboxText.setProperties({'id':'mailMainLayout_SentMeMailBox_Text'+i, 'layout-height':'wrap_content','layout-vertical-alignment':'MIDDLE', 'layout-width':'wrap_content', 
						'class':'mail-cursor-pointer', 'text':Top.i18n.get().value.m000007, 'text-size':'13px', 'on-click':'openFolder', 'text-color':'#3C3C3C'});
					var sentme_mailboxCount = Top.Widget.create('top-textview');
					sentme_mailboxCount.setProperties({'id':'mailMainLayout_SentMeMailBox_Count'+i, 'layout-height':'wrap_content','layout-vertical-alignment':'MIDDLE', 'layout-width':'wrap_content', 'margin':'0px 0px 0px 5px',
						'text':'', 'text-color':'#6C56E5', 'text-size':'13px', 'class':'mail-cursor-default'});
					
					sentme_mailbox.addWidget(sentme_mailboxArrow);
					sentme_mailbox.addWidget(sentme_mailboxText);
					sentme_mailbox.addWidget(sentme_mailboxCount);
					sentme_mailbox.complete();
					
					var sentme_mailboxNew = Top.Widget.create('top-linearlayout');
					sentme_mailboxNew.setProperties({'id':'mailMainLayout_SentMeMailBox_NewFolder'+i, 'layout-height':'35px', 'layout-width':'match_parent', 'visible':'none', 'orientation':'horizontal', 'border-width':'0px'});
					
					var sentme_mailboxNewText = Top.Widget.create('top-textfield');
					sentme_mailboxNewText.setProperties({'id':'mailMainLayout_SentMeMailBox_NewFolder_Text'+i, 'layout-height':'27px', 'layout-width':'130px', 'margin':'5px 0px 0px 42px', 'border-color':'#00A4C3',
						'text':Top.i18n.get().value.m000011, 'text-color':'#333333', 'text-size':'13px', 'max-length':'250'});
					var sentme_mailboxNewError = Top.Widget.create('top-icon');
					sentme_mailboxNewError.setProperties({'id':'mailMainLayout_SentMeMailBox_NewFolder_Error'+i, 'class':'icon-chart_error', 'layout-vertical-alignment':'MIDDLE', 'margin':'0px 0px 0px 6px', 'visible':'none',
						'text-color':'#FF6060', 'text-size':'16px'});
					
					sentme_mailboxNew.addWidget(sentme_mailboxNewText);
					sentme_mailboxNew.addWidget(sentme_mailboxNewError);
					sentme_mailboxNew.complete();
					
					var sentme_mailboxList = Top.Widget.create('top-linearlayout');
					sentme_mailboxList.setProperties({'id':'mailMainLayout_SentMeMailBox_List'+i, 'layout-height':'wrap_content', 'layout-width':'match_parent', 'border-width':'0px', 'orientation':'vertical'});
					
					sentme_mailboxGroup.addWidget(sentme_mailbox);
					sentme_mailboxGroup.addWidget(sentme_mailboxNew);
					sentme_mailboxGroup.addWidget(sentme_mailboxList);
					sentme_mailboxGroup.complete();
					
					
					
					/*var spam_mailbox = Top.Widget.create('top-linearlayout');
					spam_mailbox.setProperties({'id':'mailMainLayout_SpamMailBox'+i, 'layout-height':'35px', 'layout-width':'calc(100% + 2px)', 'orientation':'horizontal', 'border-width':'0px', 'class':'mail-height-35 mail-folder'});
					
					var spam_mailboxText = Top.Widget.create('top-textview');
					spam_mailboxText.setProperties({'id':'mailMainLayout_SpamMailBox_Text'+i, 'layout-height':'wrap_content', 'layout-vertical-alignment':'MIDDLE', 'layout-width':'wrap_content',
						'margin':'0px 0px 0px 44x', 'class':'mail-cursor-pointer', 'text':Top.i18n.get().value.m000008, 'text-size':'13px', 'on-click':'openFolder', 'text-color':'#3C3C3C'});
					var spam_mailboxCount = Top.Widget.create('top-textview');
					spam_mailboxCount.setProperties({'id':'mailMainLayout_SpamMailBox_Count'+i, 'layout-height':'wrap_content', 'layout-vertical-alignment':'MIDDLE', 'layout-width':'50px', 'text':'',
						'margin':'0px 0px 0px 5px', 'text-color':'#6C56E5', 'text-size':'13px', 'class':'mail-cursor-default'});
					
					var spam_mailboxRight = Top.Widget.create('top-linearlayout');
					spam_mailboxRight.setProperties({'id':'mailMainLayout_SpamMailBox_'+i, 'class':'ic-gray', 'layout-height':'wrap_content', 'layout-vertical-alignment':'MIDDLE', 'layout-width':'match_parent', 'orientation':'vertical', 'border-width':'0px'});
					
					var spam_mailboxIcon = Top.Widget.create('top-icon');
					spam_mailboxIcon.setProperties({'id':'mailMainLayout_SpamMailBox_Trash'+i, 'class':'icon-work_delete', 'layout-height':'wrap_content', 'layout-horizontal-alignment':'RIGHT',
						'layout-width':'wrap_content', 'margin':'0px 30px 0px 0px', 'cursor':'pointer', 'text-size':'14px', 'on-click':'deleteTrash'});
					
					spam_mailboxRight.addWidget(spam_mailboxIcon);
					spam_mailboxRight.complete();
					
					spam_mailbox.addWidget(spam_mailboxText);
					spam_mailbox.addWidget(spam_mailboxCount);
					spam_mailbox.addWidget(spam_mailboxRight);
					spam_mailbox.complete();
					*/
					
					
					var trash_mailbox = Top.Widget.create('top-linearlayout');
					trash_mailbox.setProperties({'id':'mailMainLayout_TrashMailBox'+i, 'layout-height':'35px', 'layout-width':'calc(100% + 2px)', 'orientation':'horizontal', 'border-width':'0px', 
						'class':'mail-height-35 mail-folder'});
					
					var trash_mailboxText = Top.Widget.create('top-textview');
					trash_mailboxText.setProperties({'id':'mailMainLayout_TrashMailBox_Text'+i, 'layout-height':'wrap_content', 'layout-vertical-alignment':'MIDDLE', 'layout-width':'wrap_content',
						'margin':'0px 0px 0px 44px','class':'mail-cursor-pointer', 'text':Top.i18n.get().value.m000009, 'text-size':'13px', 'on-click':'openFolder', 'text-color':'#3C3C3C'});
					
					var trash_mailboxRight = Top.Widget.create('top-linearlayout');
					trash_mailboxRight.setProperties({'id':'mailMainLayout_TrashMailBox_'+i, 'class':'ic-gray', 'layout-height':'wrap_content', 'layout-vertical-alignment':'MIDDLE', 'layout-width':'match_parent', 'orientation':'vertical', 'border-width':'0px'});
					
					var trash_mailboxIcon = Top.Widget.create('top-icon');
					trash_mailboxIcon.setProperties({'id':'mailMainLayout_TrashMailBox_Trash'+i, 'class':'icon-work_delete', 'layout-height':'wrap_content', 'layout-horizontal-alignment':'RIGHT',
						'layout-width':'wrap_content', 'margin':'0px 65px 0px 0px', 'cursor':'pointer', 'text-size':'14px', 'on-click':'deleteTrash'});
					
					trash_mailboxRight.addWidget(trash_mailboxIcon);
					trash_mailboxRight.complete();
					
					trash_mailbox.addWidget(trash_mailboxText);
					trash_mailbox.addWidget(trash_mailboxRight);
					trash_mailbox.complete();
					
					
					Top.Dom.selectById('mailMainLayout_MailList').addWidget(bar);
					Top.Dom.selectById('mailMainLayout_MailList').addWidget(account);
					Top.Dom.selectById('mailMainLayout_MailList').addWidget(mailboxGroup);
					Top.Dom.selectById('mailMainLayout_MailList').addWidget(important_mailbox);
					Top.Dom.selectById('mailMainLayout_MailList').addWidget(sent_mailboxGroup);
					Top.Dom.selectById('mailMainLayout_MailList').addWidget(temp_mailbox);
					Top.Dom.selectById('mailMainLayout_MailList').addWidget(sentme_mailboxGroup);
					//Top.Dom.selectById('mailMainLayout_MailList').addWidget(spam_mailbox);
					Top.Dom.selectById('mailMainLayout_MailList').addWidget(trash_mailbox);
					
					
					Top.Dom.selectById('mailMainLayout_MailList').complete();
					
					for(var j=0; j<ret.dto.AccountList[i].FolderList.length; j++) {
						
						var list = ret.dto.AccountList[i].FolderList[j];
						
						Top.Controller.get('mailMainLeftLogic').widgetList.push({'FOLDER_ID':i, 'WIDGET_ID':'mailMainLayout_Mail_Text'+i, 'ACCOUNT_ID':list.ACCOUNT_ID});
						switch(list.FOLDER_TYPE) {
						
						case 'MFL0001' : 
							Top.Controller.get('mailMainLeftLogic').widgetList.push({'FOLDER_ID':list.FOLDER_ID, 'WIDGET_ID':'mailMainLayout_MailBox_Text'+i, 'ACCOUNT_ID':list.ACCOUNT_ID});
							
							Top.Dom.selectById('mailMainLayout_MailBox_Text'+i).setProperties({'value':list});
							if(list.MAIL_COUNT != 0)
								Top.Dom.selectById('mailMainLayout_MailBox_Count'+i).setText(list.MAIL_COUNT);
							else
								Top.Dom.selectById('mailMainLayout_MailBox_Count'+i).setText('');
							break;
							
						case 'MFL0002' : 
							Top.Controller.get('mailMainLeftLogic').widgetList.push({'FOLDER_ID':list.FOLDER_ID, 'WIDGET_ID':'mailMainLayout_ImportantMailBox_Text'+i, 'ACCOUNT_ID':list.ACCOUNT_ID});
							
							Top.Dom.selectById('mailMainLayout_ImportantMailBox_Text'+i).setProperties({'value':list});
							if(list.MAIL_COUNT != 0)
								Top.Dom.selectById('mailMainLayout_ImportantMailBox_Count'+i).setText(list.MAIL_COUNT);
							else
								Top.Dom.selectById('mailMainLayout_ImportantMailBox_Count'+i).setText('');
							break;
							
						case 'MFL0003' : 
							Top.Controller.get('mailMainLeftLogic').widgetList.push({'FOLDER_ID':list.FOLDER_ID, 'WIDGET_ID':'mailMainLayout_SentMailBox_Text'+i, 'ACCOUNT_ID':list.ACCOUNT_ID});
							
							Top.Dom.selectById('mailMainLayout_SentMailBox_Text'+i).setProperties({'value':list});
							break;
							
						case 'MFL0004' : 
							Top.Controller.get('mailMainLeftLogic').widgetList.push({'FOLDER_ID':list.FOLDER_ID, 'WIDGET_ID':'mailMainLayout_TempMailBox_Text'+i, 'ACCOUNT_ID':list.ACCOUNT_ID});
							
							Top.Dom.selectById('mailMainLayout_TempMailBox_Text'+i).setProperties({'value':list});
							if(list.MAIL_COUNT != 0)
								Top.Dom.selectById('mailMainLayout_TempMailBox_Count'+i).setText(list.MAIL_COUNT);
							else
								Top.Dom.selectById('mailMainLayout_TempMailBox_Count'+i).setText('');
							break;
							
						case 'MFL0005' : 
							Top.Controller.get('mailMainLeftLogic').widgetList.push({'FOLDER_ID':list.FOLDER_ID, 'WIDGET_ID':'mailMainLayout_SentMeMailBox_Text'+i, 'ACCOUNT_ID':list.ACCOUNT_ID});
							
							Top.Dom.selectById('mailMainLayout_SentMeMailBox_Text'+i).setProperties({'value':list});
							if(list.MAIL_COUNT != 0)
								Top.Dom.selectById('mailMainLayout_SentMeMailBox_Count'+i).setText(list.MAIL_COUNT);
							else
								Top.Dom.selectById('mailMainLayout_SentMeMailBox_Count'+i).setText('');
							break;
								
						case 'MFL0006' : 
							/*Top.Controller.get('mailMainLeftLogic').widgetList.push({'FOLDER_ID':list.FOLDER_ID, 'WIDGET_ID':'mailMainLayout_SpamMailBox_Text'+i, 'ACCOUNT_ID':list.ACCOUNT_ID});
							
							Top.Dom.selectById('mailMainLayout_SpamMailBox_Text'+i).setProperties({'value':list});
							if(list.MAIL_COUNT != 0)
								Top.Dom.selectById('mailMainLayout_SpamMailBox_Count'+i).setText(list.MAIL_COUNT);
							else
								Top.Dom.selectById('mailMainLayout_SpamMailBox_Count'+i).setText('');*/
							break;
							
						case 'MFL0007' :
							Top.Controller.get('mailMainLeftLogic').widgetList.push({'FOLDER_ID':list.FOLDER_ID, 'WIDGET_ID':'mailMainLayout_TrashMailBox_Text'+i, 'ACCOUNT_ID':list.ACCOUNT_ID});
							
							Top.Dom.selectById('mailMainLayout_TrashMailBox_Text'+i).setProperties({'value':list});
							break;
											
						case 'MFL0008' :
							Top.Controller.get('mailMainLeftLogic').widgetList.push({'FOLDER_ID':list.FOLDER_ID, 'WIDGET_ID':'mailMainLayout_SubMailBox_Text'+i+'_'+j, 'ACCOUNT_ID':list.ACCOUNT_ID});
							
							var linearLayout = Top.Widget.create('top-linearlayout');
							linearLayout.setProperties({'id':'mailMainLayout_SubMailBox'+i+'_'+j,'layout-height':'35px','layout-width':'calc(100% + 2px)','orientation':'horizontal','border-width':'0px','vertical-alignment':'CENTER',
								'orientation':'horizontal', 'on-mouseenter':'addEditIcon', 'on-mouseleave':'removeEditIcon', 'class':'mail-subfolder mail-folder'});
							
							var textView = Top.Widget.create('top-textview');
							textView.setProperties({'id':'mailMainLayout_SubMailBox_Text'+i+'_'+j, 'text-color':'#3C3C3C',
								'text-size':'13px','layout-height':'wrap_content','layout-width':'wrap_content','max-width':'110px','layout-vertical-alignment':'MIDDLE','margin':'0px 0px 0px 57px',
								'value':list,'on-click':'openFolder', 'class':'mail-cursor-pointer'});
							textView.setHTML(list.DISPLAY_NAME);
							
							linearLayout.addWidget(textView);
							
							if(list.MAIL_COUNT != 0) {
								var countText = Top.Widget.create('top-textview');
								countText.setProperties({'id':'mailMainLayout_SubMailBox_Count'+i+'_'+j,'text':list.MAIL_COUNT,'text-color':'#6C56E5','text-size':'13px'
									,'layout-height':'wrap_content','layout-width':'wrap_content','layout-vertical-alignment':'MIDDLE','margin':'0px 0px 0px 5px'});
								linearLayout.addWidget(countText);
							}
							else {
								var countText = Top.Widget.create('top-textview');
								countText.setProperties({'id':'mailMainLayout_SubMailBox_Count'+i+'_'+j,'text':'','text-color':'#6C56E5','text-size':'13px'
									,'layout-height':'wrap_content','layout-width':'wrap_content','layout-vertical-alignment':'MIDDLE','margin':'0px 0px 0px 5px'});
								linearLayout.addWidget(countText);
							}
							
							
							linearLayout.complete();
							
							
							if(list.PARENT_FOLDER_TYPE == 'MFL0001') {
								
								Top.Dom.selectById('mailMainLayout_MailBox_Arrow'+i).setProperties({'visible':'visible'});
								Top.Dom.selectById('mailMainLayout_MailBox_List'+i).addWidget(linearLayout);
								Top.Dom.selectById('mailMainLayout_MailBox_List'+i).complete();
								
							} else if(list.PARENT_FOLDER_TYPE == 'MFL0003') {
								
								Top.Dom.selectById('mailMainLayout_SentMailBox_Arrow'+i).setProperties({'visible':'visible'});
								Top.Dom.selectById('mailMainLayout_SentMailBox_List'+i).addWidget(linearLayout);
								Top.Dom.selectById('mailMainLayout_SentMailBox_List'+i).complete();
								
								
							} else if(list.PARENT_FOLDER_TYPE == 'MFL0005') {
								
								Top.Dom.selectById('mailMainLayout_SentMeMailBox_Arrow'+i).setProperties({'visible':'visible'});
								Top.Dom.selectById('mailMainLayout_SentMeMailBox_List'+i).addWidget(linearLayout);
								Top.Dom.selectById('mailMainLayout_SentMeMailBox_List'+i).complete();
								
							}
							
							break;
							
							
						
						}
						
					}
				}
				
				
				
				Top.Dom.selectById('mailMainLayout_addExtenalMail').setProperties({'on-click':'openSetting'});
				
				Top.Dom.selectById('mailMainLayout_Title_Icon').setProperties({'value':ret.dto.AccountList[0].TDRIVE_CH_ID});
				Top.Dom.selectById('mailMainLayout_Title_Text').setProperties({'value':ret.dto.AccountList.length});
				

				Top.Dom.selectById('mailMainLayout_Mail_Text0').setHTML('내부 편지함');
				Top.Dom.selectById('mailMainLayout_Mail_Text0').setProperties({'tooltip':ret.dto.AccountList[0].EMAIL_ADDRESS});
				
				var folders = [];
				var folderList = [];
				
				for(var i=0; i<ret.dto.AccountList.length; i++) {
					if(ret.dto.AccountList[i].ACCOUNT_ID == mailData.curBox.ACCOUNT_ID)
						folderList = ret.dto.AccountList[i].FolderList;
				}
				for(var i=0; i<folderList.length; i++) {
					if(folderList[i].FOLDER_TYPE != 'MFL0006')
						folders.push({'text':folderList[i].DISPLAY_NAME,'value':folderList[i]});
				}
				
				folders.sort(function(a,b) {
					return a.value.FOLDER_TYPE.substr(6,1) - b.value.FOLDER_TYPE.substr(6,1);
				});
				
				var folderName = [[],[],[]];
				
				folders.forEach(function(data, index) {
					
					switch(data.value.PARENT_FOLDER_TYPE) {
					
					case 'MFL0001' :
						folderName[0].push(data.text);
						break;
					case 'MFL0003' :
						folderName[1].push(data.text);
						break;
					case 'MFL0005' :
						folderName[2].push(data.text);
						break;
					
					}
					
				});
				var file_mailbox = Top.Widget.create('top-linearlayout');
				file_mailbox.setProperties({'id':'mailMainLayout_fileMailBox_fileList', 'layout-height':'35px', 'layout-width':'calc(100% + 2px)', 'orientation':'horizontal', 'border-width':'0px', 
					'class':'mail-height-35 mail-folder'});
				var file_mailboxText = Top.Widget.create('top-textview');
				file_mailboxText.setProperties({'id':'mailMainLayout_fileMailBox_Text_fileList', 'layout-height':'wrap_content', 'layout-vertical-alignment':'MIDDLE', 'layout-width':'wrap_content',
					'margin':'0px 14px 0px 30px','class':'mail-cursor-pointer', 'text':'첨부 파일', 'textStyle':'bold', 'text-size':'13px', 'on-click':'openFolder', 'text-color':'#3C3C3C'});
				var file_mailboxRight = Top.Widget.create('top-linearlayout');
				file_mailboxRight.setProperties({'id':'mailMainLayout_fileMailBox__fileList', 'class':'ic-gray', 'layout-height':'wrap_content', 'layout-vertical-alignment':'MIDDLE', 'layout-width':'match_parent', 'orientation':'vertical', 'border-width':'0px'});
				var file_mailboxIcon = Top.Widget.create('top-icon');
				file_mailboxIcon.setProperties({'id':'mailMainLayout_fileMailBox_Trash_fileList', 'class':'icon-work_delete', 'layout-height':'wrap_content', 'layout-horizontal-alignment':'RIGHT',
					'layout-width':'wrap_content', 'margin':'0px 65px 0px 0px', 'cursor':'pointer', 'text-size':'14px', 'on-click':'deleteAllAttachFile'});
				
				file_mailboxRight.addWidget(file_mailboxIcon);
				file_mailboxRight.complete();
				
				file_mailbox.addWidget(file_mailboxText);
				file_mailbox.addWidget(file_mailboxRight); 
				file_mailbox.complete();
				
				Top.Dom.selectById('mailMainLayout_FileList').addWidget(file_mailbox);
				Top.Dom.selectById('mailMainLayout_FileList').complete();
				 
				
				
				Top.Controller.get('mailMainLeftLogic').folderName = folderName;

				mailData.panelType = (ret.dto.AccountList[0].PANEL_TYPE == "2") ? '2' : '3';
				mailData.writeMode = (ret.dto.AccountList[0].MAIL_SEND_TYPE == 'popup') ? 'popup' : 'full';
				
				if(Top.Dom.selectById('mailMainLayout_Center_All').getChildren().length == 0) {
					
					mailData.curPage = 1;
					mailData.pageLength = 20;
					
					if(mail.isMobile()) {
						return;
					}
					
					if(mailData.routing) {
						Top.Controller.get('mailMainLeftLogic').activateRouting(mailData.routing);
						mailData.routing = false;
						return;
					}
					
					if (mailData.panelType == '2'){ 
						Top.Dom.selectById('mailMainLayout_Center_All').src('mailTwoPanelLayout.html' + verCsp());
					}
					else {
						Top.Controller.get('mailThreePanelMainLogic').replyClicked = false;
						Top.Controller.get('mailThreePanelMainLogic').replyAllClicked  = false;
						Top.Controller.get('mailThreePanelMainLogic').forwardClicked  = false;
						Top.Controller.get('mailThreePanelMainLogic').sendAgainClicked  = false;
						Top.Controller.get('mailThreePanelMainLogic').modifyClicked = false;
						Top.Dom.selectById('mailMainLayout_Center_All').src('mailThreePanelMainLayout.html' + verCsp());
					}
				}
				
				Top.Controller.get('mailMainLeftLogic').curFolder();
				
				
				if(typeof callbackFunc === "function")
					callbackFunc();
				
				
			}
		});
		
		
		
	}, activateRouting : function(dstn) {

		switch(dstn) {
		
		case 'setting' :
			this.setFolderEvent(null,Top.Dom.selectById('mailMainLayout_Setting'));
			break;
		case 'default' :
			this.setFolderEvent(null,Top.Dom.selectById('mailMainLayout_Setting'));
			break;
		case 'external' :
			this.setFolderEvent(null,Top.Dom.selectById('mailMainLayout_Setting'));
			break;
		case 'classify' :
			this.setFolderEvent(null,Top.Dom.selectById('mailMainLayout_Setting'));
			break;
			
		case 'address' :
			this.setFolderEvent(null,Top.Dom.selectById('mailMainLayout_AddressBook'));
			break;
			
		case 'file' :
			this.setFolderEvent(null,Top.Dom.selectById('mailMainLayout_FileList'));
			break;
			
		case 'new' :
			Top.Controller.get('mailReadButtonLogic').replyClicked = false;
			Top.Controller.get('mailReadButtonLogic').replyAllClicked = false;
			Top.Controller.get('mailReadButtonLogic').forwardClicked = false;
			Top.Controller.get('mailReadButtonLogic').tempClicked = false;
			Top.Controller.get('mailReadButtonLogic').sendAgainClicked = false;
			Top.Controller.get('mailThreePanelMainLogic').replyAllClicked = false;
			Top.Controller.get('mailThreePanelMainLogic').forwardClicked = false;
			Top.Controller.get('mailReadButtonLogic').tempClicked = false;
			Top.Controller.get('mailThreePanelMainLogic').replyClicked = false;
			Top.Controller.get('mailThreePanelMainLogic').sendAgainClicked = false;
			Top.Controller.get('mailThreePanelMainLogic').modifyClicked = false;
			this.setFolderEvent(null,Top.Dom.selectById('mailMainLayout_Button_SendMail'));
			break;
			
		case 'sendcomplete' :
			Top.Dom.selectById('mailMainLayout_Center_All').src('mailSendCompleteLayout.html' + verCsp());
			break;
			
		case 'newtome' :
			this.setFolderEvent(null,Top.Dom.selectById('mailMainLayout_Button_SendMeMail'));
			break;
			
		case 'savecomplete' :
			Top.Dom.selectById('mailMainLayout_Center_All').src('mailSaveCompleteLayout.html' + verCsp());
			break;
			
		case 'folder' :
			
			for (var i=0; i<Top.Controller.get('mailMainLeftLogic').widgetList.length; i++) {
					if(mailData.routeInfo.folderId == Top.Controller.get('mailMainLeftLogic').widgetList[i].FOLDER_ID) {
						var widgetId = Top.Controller.get('mailMainLeftLogic').widgetList[i].WIDGET_ID;
					}
				}
			this.setFolderEvent(null,Top.Dom.selectById(widgetId));
			break;
			
		case 'mail' :
			for (var i=0; i<Top.Controller.get('mailMainLeftLogic').widgetList.length; i++) {
				if(mailData.routeInfo.folderId == Top.Controller.get('mailMainLeftLogic').widgetList[i].FOLDER_ID) {
					var widgetId = Top.Controller.get('mailMainLeftLogic').widgetList[i].WIDGET_ID;
				}
			}
			this.clickedMail = mailData.routeInfo.mailId;
			
			var read3panel = false;
			if(mailData.routeInfo.folderId.length == 1) {
				for(var i=0; i<mailData.accountList.length; i++) {
					if(mailData.routeInfo.folderId == i && mailData.accountList[i].ACCOUNT_ID == mailData.curBox.ACCOUNT_ID)
						read3panel = true;
				}
				
			} else if(mailData.curBox.FOLDER_ID == mailData.routeInfo.folderId)
				read3panel = true;
			
			if(mailData.curBox == []) {
				mailData.curBox = Top.Dom.selectById(widgetId).getValue();
				mailData.curBox.WIDGET_ID = widgetId;
				this.curFolder();
				
			}
			
			if(mailData.panelType == '2') {
				Top.Dom.selectById('mailMainLayout_Center_All').src('mailReadButtonLayout.html' + verCsp());
				return;
			} else {
				
				Top.Controller.get('mailReadButtonLogic').goToThreePanel = true;
				Top.Controller.get('mailThreePanelMainLogic').replyClicked = false;
				Top.Controller.get('mailThreePanelMainLogic').replyAllClicked  = false;
				Top.Controller.get('mailThreePanelMainLogic').forwardClicked  = false;
				Top.Controller.get('mailThreePanelMainLogic').sendAgainClicked  = false;
				Top.Controller.get('mailThreePanelMainLogic').modifyClicked = false;
				
				if(read3panel && Top.Dom.selectById('mailThreePanelLayout_Content') != null)
					Top.Dom.selectById('mailThreePanelLayout_Content').src('mailReadLayout.html' + verCsp());
				else
						Top.Dom.selectById('mailMainLayout_Center_All').src('mailThreePanelMainLayout.html' + verCsp());
			}
			break;
			
		case 'reply' :
			var ctrl;
			if(mailData.panelType == '2') {
				ctrl = (mail.isReading()) ? Top.Controller.get('mailReadButtonLogic') : Top.Controller.get('mailTwoPanelLogic');
			} else
				ctrl = Top.Controller.get('mailThreePanelMainLogic');
			ctrl.reply();
			break;
			
		case 'replyall' :
			var ctrl;
			if(mailData.panelType == '2') {
				ctrl = (mail.isReading()) ? Top.Controller.get('mailReadButtonLogic') : Top.Controller.get('mailTwoPanelLogic');
			} else
				ctrl = Top.Controller.get('mailThreePanelMainLogic');
			ctrl.replyall();
			break;
			
		case 'forward' :
			var ctrl;
			if(mailData.panelType == '2') {
				ctrl = (mail.isReading()) ? Top.Controller.get('mailReadButtonLogic') : Top.Controller.get('mailTwoPanelLogic');
			} else
				ctrl = Top.Controller.get('mailThreePanelMainLogic');
			ctrl.forward();		
			break;
			
		case 'resend' :
			var ctrl;
			if(mailData.panelType == '2') {
				ctrl = (mail.isReading()) ? Top.Controller.get('mailReadButtonLogic') : Top.Controller.get('mailTwoPanelLogic');
			} else
				ctrl = Top.Controller.get('mailThreePanelMainLogic');
			ctrl.resend();
			break;
			
		case 'modify' :
			var ctrl;
			if(mailData.panelType == '2') {
				ctrl = (mail.isReading()) ? Top.Controller.get('mailReadButtonLogic') : Top.Controller.get('mailTwoPanelLogic');
			} else
				ctrl = Top.Controller.get('mailThreePanelMainLogic');
			ctrl.modify();
			break;

		}
		
	}, curFolder : function() {
		
		var curWidgetId = mailData.curBox.WIDGET_ID.replace('_Text','');
		
		if(Top.Dom.selectById(curWidgetId) == null) {
			mailData.selectedBox.DISPLAY_NAME = Top.i18n.get().value.m000012;
			mailData.selectedBox.FOLDER_ID = '';
			mailData.curBox.DISPLAY_NAME = Top.i18n.get().value.m000012;
			mailData.curBox.FOLDER_ID = '';
			mailData.curBox.WIDGET_ID = 'mailMainLayout_Mail_Text0';
		}
		
		if(mailData.curBox.WIDGET_ID.substr(0,21) != 'mailMainLayout_Button'){
			Top.Dom.selectById(curWidgetId).addClass('mail-folder-selected');
		}
		
		
	}, setFolderEvent : function(event, widget) {
		
		mailData.curPage = 1;
		mailData.pageLength = 20;

		var table;
		if(Top.Dom.selectById('mailThreePanelLayout_Table') != null)
			table = Top.Dom.selectById('mailThreePanelLayout_Table');
		if(Top.Dom.selectById('mailTwoPanelLayout_Table') != null)
			table = Top.Dom.selectById('mailTwoPanelLayout_Table');
					
		if(table != undefined) {
			table.addClass('mail-loading');
			table.setProperties({'empty-message':'로딩 중...'});
		}
		
		
		mailDataRepo.setValue('mailReceivedBoxDataList',[]);
		var account = Top.Dom.selectById('mailMainLayout_Title_Text').getProperties('value');
		
		for(var i=0; i<account; i++) {
			Top.Dom.selectById('mailMainLayout_Mail'+i).removeClass('mail-folder-selected');
			Top.Dom.selectById('mailMainLayout_MailBox'+i).removeClass('mail-folder-selected');
			Top.Dom.selectById('mailMainLayout_ImportantMailBox'+i).removeClass('mail-folder-selected');
			Top.Dom.selectById('mailMainLayout_SentMailBox'+i).removeClass('mail-folder-selected');
			Top.Dom.selectById('mailMainLayout_TempMailBox'+i).removeClass('mail-folder-selected');
			Top.Dom.selectById('mailMainLayout_SentMeMailBox'+i).removeClass('mail-folder-selected');
			//Top.Dom.selectById('mailMainLayout_SpamMailBox'+i).removeClass('mail-folder-selected');
			Top.Dom.selectById('mailMainLayout_TrashMailBox'+i).removeClass('mail-folder-selected');
			
			if(Top.Dom.selectById('mailMainLayout_MailBox_List'+i).getChildren().length != 0) {
				for(var j=0; j<Top.Dom.selectById('mailMainLayout_MailBox_List'+i).getChildren().length; j++) {
					Top.Dom.selectById('mailMainLayout_MailBox_List'+i).getChildren()[j].removeClass('mail-folder-selected');
				}
			}
			
			if(Top.Dom.selectById('mailMainLayout_SentMailBox_List'+i).getChildren().length != 0) {
				for(var j=0; j<Top.Dom.selectById('mailMainLayout_SentMailBox_List'+i).getChildren().length; j++) {
					Top.Dom.selectById('mailMainLayout_SentMailBox_List'+i).getChildren()[j].removeClass('mail-folder-selected');
				}
			}
			
			if(Top.Dom.selectById('mailMainLayout_SentMeMailBox_List'+i).getChildren().length != 0) {
				for(var j=0; j<Top.Dom.selectById('mailMainLayout_SentMeMailBox_List'+i).getChildren().length; j++) {
					Top.Dom.selectById('mailMainLayout_SentMeMailBox_List'+i).getChildren()[j].removeClass('mail-folder-selected');
				}
			}
		}
		
		Top.Dom.selectById('mailMainLayout_FileList').removeClass('mail-folder-selected');
		
		mailData.curBox = [];
		mailData.curBox.WIDGET_ID = widget.id;
		
		switch(widget.id) {
		
		case 'mailMainLayout_Button_SendMail' :
			
			mailData.curBox.DISPLAY_NAME = '새 편지';
			mailData.curBox.ACCOUNT_ID = widget.getProperties('value');
			Top.Dom.selectById('mailMainLayout_Center_All').src('mailWriteLayout.html' + verCsp());
			this.mobileNoti();
			this.setTitle('새 편지');
			break;
			
		case 'mailMainLayout_Button_SendMeMail' :
			
			mailData.curBox.DISPLAY_NAME = '내게 쓰기';
			mailData.curBox.ACCOUNT_ID = widget.getProperties('value');
			Top.Dom.selectById('mailMainLayout_Center_All').src('mailWriteToMeLayout.html' + verCsp());
			this.mobileNoti();
			this.setTitle('내게 쓰기');
			break;
			
		case 'mailMainLayout_FileList' :
			
			widget.addClass('mail-folder-selected');
			mailData.curBox.DISPLAY_NAME = '첨부 파일';
			mailData.curBox.FOLDER_TYPE = 'file';
			mailData.curBox.ACCOUNT_ID = widget.getProperties('value');
			Top.Dom.selectById('mailMainLayout_Center_All').src('mailFileListViewLayout.html' + verCsp());
			this.setTitle();
			break;
		
		case 'mailMainLayout_AddressBook' :
			
			mailData.curBox.DISPLAY_NAME = '주소록';
			mailData.curBox.FOLDER_TYPE = 'address';
			mailData.curBox.ACCOUNT_ID = widget.getProperties('value');
			Top.Dom.selectById('mailMainLayout_Center_All').src('mailAddressBookMainLayout.html' + verCsp());
			this.setTitle();
			break;
			
		case 'mailMainLayout_Setting' :
			
			mailData.curBox.DISPLAY_NAME = '환경 설정';
			mailData.curBox.FOLDER_TYPE = 'setting';
			mailData.curBox.ACCOUNT_ID = widget.getProperties('value');
			Top.Dom.selectById('mailMainLayout_Center_All').src('mailSettingsMainLayout.html' + verCsp());
			this.setTitle();
			break;
			
		default :
			
			if(mailData.panelType == '3') {
				Top.Controller.get('mailThreePanelMainLogic').replyClicked = false;
				Top.Controller.get('mailThreePanelMainLogic').replyAllClicked  = false;
				Top.Controller.get('mailThreePanelMainLogic').forwardClicked  = false;
				Top.Controller.get('mailThreePanelMainLogic').sendAgainClicked  = false;
				Top.Controller.get('mailThreePanelMainLogic').modifyClicked = false;
			}
		
			if(widget.id.substr(0,24) == 'mailMainLayout_Mail_Text') {
				
				mailData.curBox.DISPLAY_NAME = Top.i18n.get().value.m000012;
				mailData.curBox.FOLDER_ID = '';
				mailData.curBox.FOLDER_TYPE = '';
				mailData.curBox.ACCOUNT_ID = widget.getProperties('value');
				mailData.curBox.WIDGET_ID = widget.id;
				
			} else {
				mailData.curBox = widget.getProperties('value');
			}
			
			mailData.curBox.WIDGET_ID = widget.id;
			
			if (mailData.panelType == '2')
				Top.Dom.selectById('mailMainLayout_Center_All').src('mailTwoPanelLayout.html' + verCsp());
			else if (mailData.panelType == '3')
				Top.Dom.selectById('mailMainLayout_Center_All').src('mailThreePanelMainLayout.html' + verCsp());
			
			widget.getParent().addClass('mail-folder-selected');
			
			if(widget.id.split('_Text')[1].substr(0,1) == '0')
				this.mobileNoti();
			
			this.setTitle();
			break;
		
		}
		
		
		
	}, mobileNoti : function() {
		
		if(mail.isMobile()) {
			$('.top-notification-root').remove();
		    Top.Dom.selectById("feedback_Toast").setProperties({ "delay": "200", "message": "※ 본 메일은 Tmax CloudSpace 내 프렌즈 및 멤버에게만 전송할 수 있습니다.외부 메일로의 전송은 빠른 시일 내에 지원할 예정입니다.", "placement":"bc" });
		    Top.Dom.selectById("feedback_Toast").open();
		    $($("[id=feedback_Toast]")[1]).find('.icon-info').css('display', 'none');
		    $($("[id=feedback_Toast]")[1]).find('.top-notification-title').css('display', 'none');
		    $($("[id=feedback_Toast]")[1]).css('width', '85%');
		}
		
	}, refreshCurrentList : function(flag) {

		if(flag == 'selected' && mailData.selectedBox.FOLDER_ID != mailData.curBox.FOLDER_ID) {
			return;
		} 
		
		var ctrl = (mailData.panelType == '2') ? 'mailTwoPanelLogic' : 'mailThreePanelMainLogic';
		Top.Controller.get(ctrl).callMailList();
		
	}, refreshMoveFolder : function() {
		
		var ctrl = (mailData.panelType == '2') ? 'mailTwoPanelLogic' : 'mailThreePanelMainLogic';
		Top.Controller.get(ctrl).setMoveFolder();
		
	}, resetCount : function() { 
		
		Top.Ajax.execute({
			
			type : "GET",
			url : _workspace.url + "Mail/Mail?action=BasicInfo&CMFLG=BasicInfo&USER_ID="+userManager.getLoginUserId()+"&WS_USE_CODE=USE0001",
			dataType : "json",
			crossDomain: true,
			contentType: "application/json",
		    xhrFields: {withCredentials: true},
			success : function(ret,xhr,status) {
				
				var allCount = [];
				allCount.accountId = 0;
				
				for(var i=0; i<ret.dto.AccountList.length; i++) {
					
					var accountId = ret.dto.AccountList[i].ACCOUNT_ID;
					
					for(var j=0; j<ret.dto.AccountList[i].FolderList.length; j++) {
						
						var list = ret.dto.AccountList[i].FolderList[j];
						
						if(mailData.curBox.FOLDER_ID == list.FOLDER_ID && mailData.curBox.ACCOUNT_ID && list.ACCOUNT_ID)
							Top.Dom.selectById('mailMainLayout_NotReadCount').setText(list.MAIL_COUNT);
						
						switch(list.FOLDER_TYPE) {
						
						case 'MFL0001' : 
							if(mailData.curBox.ACCOUNT_ID == list.ACCOUNT_ID)
								allCount.accountId += parseInt(list.MAIL_COUNT);
							if(list.MAIL_COUNT != 0)
								Top.Dom.selectById('mailMainLayout_MailBox_Count'+i).setText(list.MAIL_COUNT);
							else
								Top.Dom.selectById('mailMainLayout_MailBox_Count'+i).setText('');
							break;
							
						case 'MFL0002' : 
							if(list.MAIL_COUNT != 0)
								Top.Dom.selectById('mailMainLayout_ImportantMailBox_Count'+i).setText(list.MAIL_COUNT);
							else
								Top.Dom.selectById('mailMainLayout_ImportantMailBox_Count'+i).setText('');
							break;
							
						case 'MFL0003' :
							break;
							
						case 'MFL0004' : 
							if(list.MAIL_COUNT != 0)
								Top.Dom.selectById('mailMainLayout_TempMailBox_Count'+i).setText(list.MAIL_COUNT);
							else
								Top.Dom.selectById('mailMainLayout_TempMailBox_Count'+i).setText('');
							
							break;
							
						case 'MFL0005' :
							if(mailData.curBox.ACCOUNT_ID == list.ACCOUNT_ID)
								allCount.accountId += parseInt(list.MAIL_COUNT);
							if(list.MAIL_COUNT != 0)
								Top.Dom.selectById('mailMainLayout_SentMeMailBox_Count'+i).setText(list.MAIL_COUNT);
							else
								Top.Dom.selectById('mailMainLayout_SentMeMailBox_Count'+i).setText('');
							break;
							
						/*case 'MFL0006' : 
							if(list.MAIL_COUNT != 0)
								Top.Dom.selectById('mailMainLayout_SpamMailBox_Count'+i).setText(list.MAIL_COUNT);
							else
								Top.Dom.selectById('mailMainLayout_SpamMailBox_Count'+i).setText('');
							break;*/
						case 'MFL0007' :
							break;
											
						case 'MFL0008' :
							if(list.MAIL_COUNT != 0)
								Top.Dom.selectById('mailMainLayout_SubMailBox_Count'+i+'_'+j).setText(list.MAIL_COUNT);
							else
								Top.Dom.selectById('mailMainLayout_SubMailBox_Count'+i+'_'+j).setText('');
							break;
							
						}
					}
				}
				
				accountId = mailData.curBox.ACCOUNT_ID;
				if(mailData.curBox.FOLDER_ID == '')
					Top.Dom.selectById('mailMainLayout_NotReadCount').setText(allCount.accountId);
				
			}
		});
		
	}, resetTempCount : function() {
		
		if(Top.Dom.selectById('mailMainLayout_TempMailBox_Text0') != null) {
		Top.Ajax.execute({
			
			type : "GET",
			url : _workspace.url + "Mail/Mail?action=ListGetbyFolder&CMFLG=FolderToMailList&FOLDER_ID="+Top.Dom.selectById('mailMainLayout_TempMailBox_Text0').getValue().FOLDER_ID+"&ACCOUNT_ID=" 
			+ mailData.accountList[0].ACCOUNT_ID + '&SEEING_TYPE_CODE=ALL' + '&CURRPAGE=1&PAGELENGTH=0',
			dataType : "json",
			crossDomain: true,
			contentType: "application/json",
		    xhrFields: {withCredentials: true},
			success : function(ret,xhr,status) {
				Top.Dom.selectById('mailMainLayout_TempCount').setText(ret.dto.dbio_total_count);
			}});
		
		
		
	}}, deleteAllAttachFile : function(event, widget) {
		mailData.selectedBox.DISPLAY_NAME = "첨부 파일";
		 
		Top.Dom.selectById('mailDeleteTrashDialog').open();
	}

})
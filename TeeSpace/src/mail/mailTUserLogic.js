Top.Controller.create('mailTUserLogic', {
	mode : 0,
   init : function(event, widget) {
      mailTUserDataRepo.mailTUserDataList = [];
      
      this.selectedAddress = [];
      
      this.selectedReceiver = [];
      this.selectedCC = [];
      this.selectedHiddenCC = [];
      
      this.clickedArea;
     
      
      if(Top.Controller.get('mailWriteLogic').clickedArea == 'Receiver'){
			Top.Controller.get('mailTUserLogic').clickedArea = 'Receiver';
			Top.Dom.selectById('mailTUserLayout_Receiver_Chip_Layout').setProperties({'border-color' : '#00A4C3'});
			Top.Dom.selectById('mailTUserLayout_CC_Chip_Layout').setProperties({'border-color' : 'rgb(172, 176, 188)'});
			Top.Dom.selectById('mailTUserLayout_HiddenCC_Chip_Layout').setProperties({'border-color' : 'rgb(172, 176, 188)'});
			}
		else if (Top.Controller.get('mailWriteLogic').clickedArea == 'CC'){
			Top.Controller.get('mailTUserLogic').clickedArea = 'CC';
			Top.Dom.selectById('mailTUserLayout_Receiver_Chip_Layout').setProperties({'border-color' : 'rgb(172, 176, 188)'});
			Top.Dom.selectById('mailTUserLayout_CC_Chip_Layout').setProperties({'border-color' : '#00A4C3'});
			Top.Dom.selectById('mailTUserLayout_HiddenCC_Chip_Layout').setProperties({'border-color' : 'rgb(172, 176, 188)'});
		}
		else if (Top.Controller.get('mailWriteLogic').clickedArea == 'HiddenCC'){
			Top.Controller.get('mailTUserLogic').clickedArea = 'HiddenCC';
			Top.Dom.selectById('mailTUserLayout_Receiver_Chip_Layout').setProperties({'border-color' : 'rgb(172, 176, 188)'});
			Top.Dom.selectById('mailTUserLayout_CC_Chip_Layout').setProperties({'border-color' : 'rgb(172, 176, 188)'});
			Top.Dom.selectById('mailTUserLayout_HiddenCC_Chip_Layout').setProperties({'border-color' : '#00A4C3'});
		}
     
      Top.Controller.get('mailWriteLogic').clickedArea = null; 
      
      Top.Dom.selectById('mailTUserLayout_Receiver_Chip').setProperties({'on-click' : 'clickArea'});
      Top.Dom.selectById('mailTUserLayout_CC_Chip').setProperties({'on-click' : 'clickArea'});
      Top.Dom.selectById('mailTUserLayout_HiddenCC_Chip').setProperties({'on-click' : 'clickArea'});
      
      Top.Dom.selectById("mailTUserLayout_Receiver_Chip").setProperties({"chip-items":Top.Controller.get('mailTUserLogic').selectedReceiver});
      Top.Dom.selectById("mailTUserLayout_CC_Chip").setProperties({"chip-items":Top.Controller.get('mailTUserLogic').selectedCC});
      Top.Dom.selectById("mailTUserLayout_HiddenCC_Chip").setProperties({"chip-items":Top.Controller.get('mailTUserLogic').selectedHiddenCC});
      
      Top.Dom.selectById("mailTUserLayout_Receiver_Chip").setProperties({"on-close": 'onCloseItem' });
      Top.Dom.selectById("mailTUserLayout_CC_Chip").setProperties({"on-close": 'onCloseItem'});
      Top.Dom.selectById("mailTUserLayout_HiddenCC_Chip").setProperties({"on-close": 'onCloseItem'});

      this.isAutoComplete; 
      this.tempFlag = 0;
      this.recKey = [];
      this.ccKey = [];
      this.bccKey = [];
      
	
      this.setAutoComplete(); 

      Top.Dom.selectById("mailTUserLayout_TreeView").setProperties({"nodes":tvrepo.tvgroup});
      let searchCond = ["이름","메일"];
      Top.Dom.selectById("mailTUserLayout_SelectBox92_1").setProperties({"nodes":searchCond});

      $.ajax({
          url: _workspace.url+"WorkSpace/WorkspaceUser?action=WsIdList", //Service Object
          type: 'GET',
          dataType: 'json', 
          crossDomain: true,
          contentType: 'application/json',
          xhrFields: {

              withCredentials: true

            },
          data: {
        	  "USER_ID":userManager.getLoginUserId(),
        	  "WS_TYPE":"WKS0003"
        	
          },
          success: function(result) {
        	  Top.Dom.selectById("mailTUserLayout_TreeView").expandNode("All")
        	  if(result.dto.wsUserList.length >= 1){
        	      $.ajax({
        	          url: _workspace.url+"WorkSpace/RootWorkspace?action=Get", //Service Object
        	          type: 'GET',
        	          dataType: 'json', 
        	          crossDomain: true,
        	          contentType: 'application/json',
        	          xhrFields: {
        	        	  

        	              withCredentials: true

        	            },
        	            data: JSON.stringify({
        	  	          "dto": {
        	  	        	"WS_ID" : result.dto.wsUserList[0].WS_ID
        	  	          }
        	  	        }),
        	          success: function(result) {
        	        	  result.dto.wsList.forEach(function(element){
        	        		  Top.Dom.selectById("mailTUserLayout_TreeView").expandNode(element.WS_ID)
        	        	  })
        	        	  
        	          },
        	          error: function(error) { 
        	          }
        	        });
        	      Top.Dom.selectById("mailTUserLayout_TreeView").selectNode(result.dto.wsUserList[0].WS_ID)
        	  }

        	  
          },
          error: function(error) { 
          }
        });

      	var media = window.matchMedia("screen and (max-device-width: 1024px)");
		
		if(media.matches) {
			$('span#mailTUserLayout_TextView17_1').css('width','calc(100% - 57px)');
			$('div#mailTUserLayout_LinearLayout370_1').addClass('mobile-mail-visible-panel');
			$('div#mailTUserLayout_LinearLayout376_1').addClass('mobile-mail-invisible-panel');
			$('div#mailTUserLayout_LinearLayout226_1').addClass('mobile-mail-invisible-panel');
		}
		
		Top.Dom.selectById('mailTUserLayout_Pre').setProperties({'on-click':'moveTreeView'});
      
      
   }, moveTreeView : function() {
	   
	   $('span#mailTUserLayout_TextView17_1').css('width','calc(100% - 57px)');
	   Top.Dom.selectById('mailTUserLayout_Pre').setProperties({'visible':'none'});
	   $('div#mailTUserLayout_LinearLayout370_1').removeClass('mobile-mail-invisible-panel');
	   $('div#mailTUserLayout_LinearLayout376_1').removeClass('mobile-mail-visible-panel');
	   $('div#mailTUserLayout_LinearLayout226_1').removeClass('mobile-mail-visible-panel');
	   $('div#mailTUserLayout_LinearLayout370_1').addClass('mobile-mail-visible-panel');
	   $('div#mailTUserLayout_LinearLayout376_1').addClass('mobile-mail-invisible-panel');
	   $('div#mailTUserLayout_LinearLayout226_1').addClass('mobile-mail-invisible-panel');
	   
   },
   closeDialog_user : function(event, widget) {
	      Top.Dom.selectById("mailTUserDialog").close(); 
	   },

   confirmSelectUser : function(event, widget) {
	   
	   	   for (var i = 0; i < Top.Controller.get('mailTUserLogic').selectedReceiver.length; i++){
			   Top.Controller.get('mailWriteLogic').receiverAddr.push(Top.Controller.get('mailTUserLogic').selectedReceiver[i]);
		   } 
		   Top.Dom.selectById('Text_ReceiverAddress').setProperties({'chip-items' : Top.Controller.get('mailWriteLogic').receiverAddr});
	
		   for (var i = 0; i < Top.Controller.get('mailTUserLogic').selectedCC.length; i++){
			   Top.Controller.get('mailWriteLogic').CCAddr.push(Top.Controller.get('mailTUserLogic').selectedCC[i]);
		   } 
		   Top.Dom.selectById('Text_Cc').setProperties({'chip-items' : Top.Controller.get('mailWriteLogic').CCAddr});
	
		   for (var i = 0; i < Top.Controller.get('mailTUserLogic').selectedHiddenCC.length; i++){
			   Top.Controller.get('mailWriteLogic').BCCAddr.push(Top.Controller.get('mailTUserLogic').selectedHiddenCC[i]);
			  }
		   Top.Dom.selectById('Text_HiddenCc').setProperties({'chip-items' : Top.Controller.get('mailWriteLogic').BCCAddr});
	
		  
	      Top.Dom.selectById("mailTUserDialog").close();
      
   },
   
   selectUser : function(event, widget) {
	   
      if (Top.Controller.get('mailTUserLogic').clickedArea == "Receiver"){
    	  Top.Controller.get('mailTUserLogic').selectedReceiver.push(
    	            {
    	               "text":widget.template.parentNode.dataObj.USER_NAME,
    	               "USER_EMAIL":widget.template.parentNode.dataObj.USER_EMAIL
    	            })
    	            
    	      Top.Dom.selectById("mailTUserLayout_Receiver_Chip").setProperties({"chip-items":Top.Controller.get('mailTUserLogic').selectedReceiver})
      }
      else if (Top.Controller.get('mailTUserLogic').clickedArea == "CC"){
    	  Top.Controller.get('mailTUserLogic').selectedCC.push(
  	            {
  	               "text":widget.template.parentNode.dataObj.USER_NAME,
  	               "USER_EMAIL":widget.template.parentNode.dataObj.USER_EMAIL
  	            })
  	            
  	            
  	      Top.Dom.selectById("mailTUserLayout_CC_Chip").setProperties({"chip-items":Top.Controller.get('mailTUserLogic').selectedCC})  
      }
      else if (Top.Controller.get('mailTUserLogic').clickedArea == "HiddenCC"){
    	  Top.Controller.get('mailTUserLogic').selectedHiddenCC.push(
    	            {
    	               "text":widget.template.parentNode.dataObj.USER_NAME,
    	               "USER_EMAIL":widget.template.parentNode.dataObj.USER_EMAIL
    	            })     
    	            
    	      Top.Dom.selectById("mailTUserLayout_HiddenCC_Chip").setProperties({"chip-items":Top.Controller.get('mailTUserLogic').selectedHiddenCC})  
    	  
      }
      
      
   },
   
   unselectUser : function(event, widget, data) {

         let unselectBtnIndex = mailTUserDataRepo.mailTUserDataList.map(function(e){return e.USER_ID}).indexOf(data.USER_ID)
      
   },
   
   selectAllUser : function(event, widget) {
    
	   
	   var data = {
			   "dto" :{
				      "CMFLG" : "WSEmailAddress",
				      "WS_ID" : Top.Dom.selectById('mailTUserLayout_TreeView').template.clickedNode.id
			    }
	   };
	   Top.Ajax.execute({
			
			type : "POST",
			url : _workspace.url + "Mail/Mail?action=WsEmailAddress",
			dataType : "json",
			data	 : JSON.stringify(data),
			crossDomain: true,
			contentType: "application/json",
		    xhrFields: {withCredentials: true},
			success : function(ret,xhr,status) {
				
				 if (Top.Controller.get('mailTUserLogic').clickedArea == "Receiver"){ 
			 		  Top.Controller.get('mailTUserLogic').selectedReceiver.push(
			 	               {
			 	                  "text":Top.Dom.selectById('mailTUserLayout_TreeView').template.clickedNode.text,
			 	                  "USER_EMAIL":ret.dto.EMAIL_ADDRESS
			 	               }
			 	            ) ;     
			             }
			 	  else if (Top.Controller.get('mailTUserLogic').clickedArea == "CC"){
			 		  Top.Controller.get('mailTUserLogic').selectedCC.push(
				               {
				            	   "text":Top.Dom.selectById('mailTUserLayout_TreeView').template.clickedNode.text,
				 	                "USER_EMAIL":ret.dto.EMAIL_ADDRESS
				               }
				            ) ;    
			 	  }
			 	  else if (Top.Controller.get('mailTUserLogic').clickedArea == "HiddenCC"){
			 		  Top.Controller.get('mailTUserLogic').selectedHiddenCC.push(
			   	               {
			   	            	   "text":Top.Dom.selectById('mailTUserLayout_TreeView').template.clickedNode.text,
			   	            	   "USER_EMAIL":ret.dto.EMAIL_ADDRESS
			   	               }
			   	            );     
			 	
			 	  }
				 
				  Top.Dom.selectById("mailTUserLayout_Receiver_Chip").setProperties({"chip-items":Top.Controller.get('mailTUserLogic').selectedReceiver});
			      Top.Dom.selectById("mailTUserLayout_CC_Chip").setProperties({"chip-items":Top.Controller.get('mailTUserLogic').selectedCC});
			      Top.Dom.selectById("mailTUserLayout_HiddenCC_Chip").setProperties({"chip-items":Top.Controller.get('mailTUserLogic').selectedHiddenCC}); 
				
			      Top.Dom.selectById('mailTUserLayout_SelectAll').setProperties({'disabled' : 'true'}); 
			}
		});
	   
      Top.Controller.get('mailTUserLogic').selectedReceiver = getUniqueObjectArray(Top.Controller.get('mailTUserLogic').selectedReceiver, "USER_EMAIL");
      Top.Controller.get('mailTUserLogic').selectedCC = getUniqueObjectArray(Top.Controller.get('mailTUserLogic').selectedCC, "USER_EMAIL");
      Top.Controller.get('mailTUserLogic').selectedHiddenCC = getUniqueObjectArray(Top.Controller.get('mailTUserLogic').selectedHiddenCC, "USER_EMAIL");
      
      
     
      
      
   }, 
   
   onitemclk : function(event, widget) {
	   
	   
	   var media = window.matchMedia("screen and (max-device-width: 1024px)");
		
		if(media.matches) {
			$('span#mailTUserLayout_TextView17_1').css('width','calc(100% - 80px)')
			Top.Dom.selectById('mailTUserLayout_Pre').setProperties({'visible':'visible'});
			$('div#mailTUserLayout_LinearLayout370_1').removeClass('mobile-mail-visible-panel');
			$('div#mailTUserLayout_LinearLayout376_1').removeClass('mobile-mail-invisible-panel');
			$('div#mailTUserLayout_LinearLayout226_1').removeClass('mobile-mail-invisible-panel');
			$('div#mailTUserLayout_LinearLayout370_1').addClass('mobile-mail-invisible-panel');
			$('div#mailTUserLayout_LinearLayout376_1').addClass('mobile-mail-visible-panel');
			$('div#mailTUserLayout_LinearLayout226_1').addClass('mobile-mail-visible-panel');
		}
		
      if (widget.template.clickedNode.id == "Private"){
          Top.Dom.selectById("TextView227_1").setProperties({"text":widget.template.clickedNode.text})
          Top.Dom.selectById("TextField16_1").setProperties({"hint":widget.template.clickedNode.text+" 유저 검색"})
          Top.Dom.selectById('mailTUserLayout_SelectAll').setProperties({'disabled' : 'true'});

         $.ajax({
              url: _workspace.url+"Users/AllUserList",
              type: 'GET',
              dataType: 'json', 
              crossDomain: true,
              contentType: 'application/json',
              xhrFields: {

                  withCredentials: true

                },
              success: function(result) {
                 mailTUserDataRepo.mailTUserDataList = result.dto.tUserList;
                 mailTUserDataRepo.mailTUserDataList_Temp = result.dto.tUserList;

               
                 Top.Dom.selectById("TableView299_1").render();
                

                 
              },
              error: function(error) { 
              }
            });
         
         
      }
      else if(widget.template.clickedNode.id != "Private" && widget.template.clickedNode.id != "All"){
          Top.Dom.selectById("TextView227_1").setProperties({"text":widget.template.clickedNode.text})
          Top.Dom.selectById("TextField16_1").setProperties({"hint":widget.template.clickedNode.text+" 유저 검색"})
          
             var data = {
			   "dto" :{
				      "CMFLG" : "WSEmailAddress",
				      "WS_ID" : Top.Dom.selectById('mailTUserLayout_TreeView').template.clickedNode.id
			    }
	   };
	   
	   Top.Ajax.execute({
			
			type : "POST",
			url : _workspace.url + "Mail/Mail?action=WsEmailAddress",
			dataType : "json",
			data	 : JSON.stringify(data),
			crossDomain: true,
			contentType: "application/json",
		    xhrFields: {withCredentials: true},
			success : function(ret,xhr,status) {
				
				if(ret.dto != undefined)
					Top.Dom.selectById('mailTUserLayout_SelectAll').setProperties({'value' : ret.dto.EMAIL_ADDRESS, 'disabled' : 'false'});
				else
					Top.Dom.selectById('mailTUserLayout_SelectAll').setProperties({'disabled' : 'true'});

						
				Top.Controller.get('mailTUserLogic').setSelectAllButton(); 

			}
		});
	   
          
         $.ajax({
              url: _workspace.url+"WorkSpace/WorkspaceTUser?action=List", //Service Object
              type: 'GET',
              dataType: 'json',
              crossDomain: true,
              contentType: 'application/json',
              xhrFields: {

                  withCredentials: true  

                },
              data: {
                  "WS_ID": widget.template.clickedNode.id,
                  "WS_TYPE": widget.template.clickedNode.ws_type
              },
              success: function(result) {
                 mailTUserDataRepo.mailTUserDataList = result.dto.tUserList
                 mailTUserDataRepo.mailTUserDataList_Temp = result.dto.tUserList;
      

                 Top.Dom.selectById("TableView299_1").render()
                 
              },
              error: function(error) {
              }
            });
         
      }
      

      
      
   }, onTextClear : function(event, widget) {
	   Top.Dom.selectById("TableView299_1").setProperties({"items":mailTUserDataRepo.mailTUserDataList_Temp})

}, onIconClick : function(event, widget) {
	let objects = mailTUserDataRepo.mailTUserDataList_Temp
	let selectedText = Top.Dom.selectById("mailTUserLayout_SelectBox92_1").getSelectedText()
	let toSearchCond = "USER_NAME"
	let results = [];
	let toSearch = Top.Dom.selectById("TextField16_1").getText();
	
	if (selectedText == "이름"){
		toSearchCond = "USER_NAME"
	}
	else if(selectedText == "메일"){
		toSearchCond = "USER_EMAIL"
		
	}

	objects.forEach(function(ele){
		if(ele[toSearchCond] != null){
			if(ele[toSearchCond].indexOf(toSearch) != -1){
				results.push(ele)
           }
		}

	})
	if(results.length == 0){
		Top.Dom.selectById("TableView299_1").setProperties({"empty-message":"일치하는 유저가 없습니다."})
	}
	
	Top.Dom.selectById("TableView299_1").setProperties({"items":results});
	
}, onEnterPressed : function(event, widget) {
	if(event.code == "Enter"){
		this.onIconClick();
	}
	else if( Top.Dom.selectById("TextField16_1").getText() == ""){
		this.onTextClear();
	}
},
	
	clickArea : function (event, widget){
		
		if(widget.id.split('_')[1] == 'Receiver'){
			Top.Controller.get('mailTUserLogic').clickedArea = 'Receiver';
			Top.Dom.selectById('mailTUserLayout_Receiver_Chip_Layout').setProperties({'border-color' : '#00A4C3'});
			Top.Dom.selectById('mailTUserLayout_CC_Chip_Layout').setProperties({'border-color' : 'rgb(172, 176, 188)'});
			Top.Dom.selectById('mailTUserLayout_HiddenCC_Chip_Layout').setProperties({'border-color' : 'rgb(172, 176, 188)'});
			}
		else if (widget.id.split('_')[1] == 'CC'){
			Top.Controller.get('mailTUserLogic').clickedArea = 'CC';
			Top.Dom.selectById('mailTUserLayout_Receiver_Chip_Layout').setProperties({'border-color' : 'rgb(172, 176, 188)'});
			Top.Dom.selectById('mailTUserLayout_CC_Chip_Layout').setProperties({'border-color' : '#00A4C3'});
			Top.Dom.selectById('mailTUserLayout_HiddenCC_Chip_Layout').setProperties({'border-color' : 'rgb(172, 176, 188)'});
		}
		else if (widget.id.split('_')[1] == 'HiddenCC'){
			Top.Controller.get('mailTUserLogic').clickedArea = 'HiddenCC';
			Top.Dom.selectById('mailTUserLayout_Receiver_Chip_Layout').setProperties({'border-color' : 'rgb(172, 176, 188)'});
			Top.Dom.selectById('mailTUserLayout_CC_Chip_Layout').setProperties({'border-color' : 'rgb(172, 176, 188)'});
			Top.Dom.selectById('mailTUserLayout_HiddenCC_Chip_Layout').setProperties({'border-color' : '#00A4C3'});
		}
		
		this.setSelectAllButton(); 
		
		
	},
	setSelectAllButton : function(){
		
		   
		if (Top.Controller.get('mailTUserLogic').clickedArea == "Receiver"){
			 var tmp = Top.Dom.selectById('mailTUserLayout_Receiver_Chip').getProperties('chip-items');
			 for(var i = 0; i < tmp.length; i++ ){
				 if (tmp[i].USER_EMAIL == Top.Dom.selectById('mailTUserLayout_SelectAll').getValue()){
					 Top.Dom.selectById('mailTUserLayout_SelectAll').setProperties({'disabled' : 'true'});
					 return;
				 }
				 
			 }
			 Top.Dom.selectById('mailTUserLayout_SelectAll').setProperties({'disabled' : 'false'});

			 }
	      else if (Top.Controller.get('mailTUserLogic').clickedArea == "CC"){
	    	  var tmp = Top.Dom.selectById('mailTUserLayout_CC_Chip').getProperties('chip-items');
	    	  for(var i = 0; i < tmp.length; i++ ){
					 if (tmp[i].USER_EMAIL == Top.Dom.selectById('mailTUserLayout_SelectAll').getValue()){
						 Top.Dom.selectById('mailTUserLayout_SelectAll').setProperties({'disabled' : 'true'});
						 return;
					 }
					 
				 }
				 Top.Dom.selectById('mailTUserLayout_SelectAll').setProperties({'disabled' : 'false'});

	    	 
	      }
	      else if (Top.Controller.get('mailTUserLogic').clickedArea == "HiddenCC"){
	    	  
	    	  var tmp = Top.Dom.selectById('mailTUserLayout_HiddenCC_Chip').getProperties('chip-items');
	    	  for(var i = 0; i < tmp.length; i++ ){
					 if (tmp[i].USER_EMAIL == Top.Dom.selectById('mailTUserLayout_SelectAll').getValue()){
						 Top.Dom.selectById('mailTUserLayout_SelectAll').setProperties({'disabled' : 'true'});
						 return;
					 }
					 
				 }
				 Top.Dom.selectById('mailTUserLayout_SelectAll').setProperties({'disabled' : 'false'});

	      }
		
	      
	},
	onCloseItem : function(event, widget){
		
	},
	setAutoComplete : function(){
		
		  var data =  {"dto" : {
			    "CMFLG" : "AutoCreateTUser",
			    "USER_ID" : userManager.getLoginUserId()
			 }}; 
			
		  
		  Top.Ajax.execute({
				
				type : "POST",
				url :_workspace.url + "Mail/Mail?action=AutoCreateEMailAddress",
				dataType : "json",
				data	 : JSON.stringify(data),
				contentType: "application/json; charset=utf-8",
				xhrFields: {withCredentials: true},
				crossDomain : true,
				success : function(ret,xhr,status) {
				
					var addr_info_array = [];						
					var retArr = ret.dto.ADDRESS_INFO;
					
					for(var i=0; i < retArr.length; i++){
						
						addr_info_array.push('' + retArr[i].MAIL_USER_NAME + '<' + retArr[i].EMAIL_ADDRESS + '>');
						
					}

					Top.Dom.selectById('mailTUserLayout_Receiver_Chip').setProperties({'auto-complete':addr_info_array.join(','),
							'auto-complete-max-height':'210px',
							'support-empty':'false'});
					Top.Dom.selectById('mailTUserLayout_Receiver_Chip').setProperties({'chip-items' : Top.Controller.get('mailTUserLogic').selectedReceiver});
					Top.Dom.selectById('mailTUserLayout_Receiver_Chip').setProperties({'on-select' : 'autoCompleteChipItem', 'on-add' : 'addChipItem'});
					
					
					Top.Dom.selectById('mailTUserLayout_CC_Chip').setProperties({'auto-complete':addr_info_array.join(','),
						'auto-complete-max-height':'210px',
						  'support-empty':'false'});
					Top.Dom.selectById('mailTUserLayout_CC_Chip').setProperties({'chip-items' : Top.Controller.get('mailTUserLogic').selectedCC});
					Top.Dom.selectById('mailTUserLayout_CC_Chip').setProperties({'on-select' : 'autoCompleteChipItem', 'on-add' : 'addChipItem'});
											
					Top.Dom.selectById('mailTUserLayout_HiddenCC_Chip').setProperties({'auto-complete':addr_info_array.join(','),
						'auto-complete-max-height':'210px',
						  'support-empty':'false'});
					Top.Dom.selectById('mailTUserLayout_HiddenCC_Chip').setProperties({'chip-items' : Top.Controller.get('mailTUserLogic').selectedHiddenCC});
					Top.Dom.selectById('mailTUserLayout_HiddenCC_Chip').setProperties({'on-select' : 'autoCompleteChipItem', 'on-add' : 'addChipItem'});
											
					return;
					
				},
				error : function(ret, xhr,status) {
				
				
				}
				
			});	
		  
		  
	},
		 autoCompleteChipItem : function(event, widget) {
			 
			 Top.Controller.get('mailTUserLogic').isAutoComplete = true;
			 
		 },
		 addChipItem : function (event, widget){
			 
			 if (Top.Controller.get('mailTUserLogic').isAutoComplete == true){
				
				var tmp1 = widget.split('<');
				if(tmp1.length == 2){
						 var tmp2 = tmp1[1].split('>');
						 }
					
				widget = {
						'text' : widget,
						'USER_EMAIL' : tmp2[0]
					};
				//추가된 코드 ---hws
				var tt;
				if (tmp2[0] == undefined){
					tt= widget;
				}else{
					tt = tmp2[0];
				}
				
				if (event.currentTarget.id == 'mailTUserLayout_Receiver_Chip'){
					if (Top.Controller.get('mailTUserLogic').recKey.indexOf(tt) == "-1"){
						Top.Controller.get('mailTUserLogic').recKey.push(tt);
						var i = Top.Controller.get('mailTUserLogic').selectedReceiver.length; 
						Top.Controller.get('mailTUserLogic').selectedReceiver[i-1] = widget;
					}
					
				}
				else if (event.currentTarget.id == 'mailTUserLayout_CC_Chip'){
					if (Top.Controller.get('mailTUserLogic').ccKey.indexOf(tt) == "-1"){
						Top.Controller.get('mailTUserLogic').ccKey.push(tt);
						var i = Top.Controller.get('mailTUserLogic').selectedCC.length; 
						Top.Controller.get('mailTUserLogic').selectedCC[i-1] = widget;
					}
					
				}
				else if (event.currentTarget.id == 'mailTUserLayout_HiddenCC_Chip'){
					if (Top.Controller.get('mailTUserLogic').bccKey.indexOf(tt) == "-1"){
						Top.Controller.get('mailTUserLogic').bccKey.push(tt);
						var i = Top.Controller.get('mailTUserLogic').selectedHiddenCC.length; 
						Top.Controller.get('mailTUserLogic').selectedHiddenCC[i-1] = widget;
					}
					
				}
				
				Top.Controller.get('mailTUserLogic').tempFlag = 0; // 마우스 이벤트 오류로 인한 임시처방,  수정요망
				Top.Controller.get('mailTUserLogic').isAutoComplete = false;
					 
			 }
			 else{
				 
				 if (event.currentTarget.id == 'Text_ReceiverAddress'){
					 var i = Top.Controller.get('mailWriteLogic').selectedReceiver.length; 
					 widget = {
							 'text' : '<' +Top.Controller.get('mailWriteLogic').selectedReceiver[i-1].text +'>',
							 'USER_EMAIL' : Top.Controller.get('mailWriteLogic').selectedReceiver[i-1].text
					 };
					Top.Controller.get('mailWriteLogic').selectedReceiver[i-1] = widget;
					
					}
					else if (event.currentTarget.id == 'Text_Cc'){
						var i = Top.Controller.get('mailWriteLogic').selectedCC.length; 
						 widget = {
								 'text' : '<' +Top.Controller.get('mailWriteLogic').selectedCC[i-1].text +'>',
								 'USER_EMAIL' : Top.Controller.get('mailWriteLogic').selectedCC[i-1].text
						 };
						Top.Controller.get('mailWriteLogic').selectedCC[i-1] = widget;
					}
					else if (event.currentTarget.id == 'Text_HiddenCc'){
						var i = Top.Controller.get('mailWriteLogic').selectedHiddenCC.length; 
						 widget = {
								 'text' : '<' + Top.Controller.get('mailWriteLogic').selectedHiddenCC[i-1].text + '>',
								 'USER_EMAIL' : Top.Controller.get('mailWriteLogic').selectedHiddenCC[i-1].text
						 };
						Top.Controller.get('mailWriteLogic').selectedHiddenCC[i-1] = widget;
					}
					
			 }
			 
	}
   
});

const MailDragDrop2 = {
    dragCounter : 0,
    
    onDragEnter : function(event) {
        MailDragDrop2.dragCounter++;
        if(event.dataTransfer.types[0] != 'Files') return;
        var mailDropZoneOverlay = Top.Dom.selectById("mailDropZoneOverlay_Popup");
        if(mailDropZoneOverlay.getVisible() != "visible") {
            mailDropZoneOverlay.setVisible("visible");
        }
    },
    
    onDragLeave : function(event) {
        MailDragDrop2.dragCounter--;
        if(MailDragDrop2.dragCounter > 0) {
            return;
        }
        if(event.dataTransfer.types[0] != 'Files') return;
        var mailDropZoneOverlay = Top.Dom.selectById("mailDropZoneOverlay_Popup");
        if(mailDropZoneOverlay.getVisible() == "visible") {
        	mailDropZoneOverlay.setVisible("none");
        }
    },
    
    onDrop : function(event) {
        event.stopPropagation();
        event.preventDefault();
        MailDragDrop2.dragCounter = 0;
        if(event.dataTransfer.types[0] != 'Files') return;
        var mailDropZoneOverlay = Top.Dom.selectById("mailDropZoneOverlay_Popup");
        if(mailDropZoneOverlay.getVisible() == "visible") {
        	mailDropZoneOverlay.setVisible("none");
        }
        
        event.dataTransfer.file = event.dataTransfer.files;
        Top.Controller.get('mailWritePopupLogic').attachTable(event.dataTransfer);
    }
}

Top.Controller.create('mailWritePopupLogic', {
	init : function(event, widget) {
		
		Top.Dom.selectById('mailDropZoneOverlay_Popup').setProperties({'visible':'none'});
		
		var dropZone = document.querySelector("div#mailWritePopupLayout");		
		dropZone.addEventListener('dragenter', MailDragDrop2.onDragEnter);		
		dropZone.addEventListener('dragleave', MailDragDrop2.onDragLeave);
		dropZone.addEventListener('drop'     , MailDragDrop2.onDrop);
		
		this.recKey = [];
		this.ccKey = [];
		this.bccKey = [];
		
		this.tdrivefile = [];
		this.fileListArr = [];
		this.tempTdrivefile = [];
		this.tempTFileId = [];
		
		this.receiverAddr = [];
		this.CCAddr = [];
		this.BCCAddr = [];
		
		this.blured = false;
		
		this.tempYN = null;
		this.tempMailId;
		this.tempGroupIndex;
		
		this.tempInt = setInterval(function() {
			if(!Top.Dom.selectById('mailWriteDialog').isOpen()) {
				clearInterval(Top.Controller.get('mailWritePopupLogic').tempInt);
				return;
			}
				
			Top.Controller.get('mailWritePopupLogic').saveTemp();
			
		}, 60000);
		Top.Dom.selectById('mailWritePopupLayout_Button_Preview_1').setProperties({'on-click':'saveTemp'});
		
		mailFileAttachmentRepo.setValue('mailFileAttachmentList',[]);

		this.fileAttach = [];
		this.file; 
		this.fileList = [];
		this.fileUp = [];
		this.tempFileIndex = [];
		this.isFromTDrive ;
		this.saveAtTDrive ;
		
		this.set_auto_complete();
		
		setTimeout(function(){$('#sun_editorEdit')[0].style.height = "100%";$('#sun_editorEdit')[0].style.maxHeight = '';});
		
		this.isAutoComplete;
		this.isLeaved; 
		this.tempFlag = 0;
		
		Top.Dom.selectById("mailWritePopupLayout_recAddressBookButton").setProperties({'on-click':'clickAddressBookRec' });
        Top.Dom.selectById("mailWritePopupLayout_ccAddressBookButton").setProperties({'on-click':'clickAddressBookCc' });
        Top.Dom.selectById("mailWritePopupLayout_bccAddressBookButton").setProperties({'on-click':'clickAddressBookBcc' });

        this.clickedArea;  
        
    	this.setPrivateAccount(); 
        this.setEditorSize();
		
        Top.Dom.selectById('mailWritePopupLayout_AttachFile').setProperties({ "on-select": this.clickDropdownMenu});
		Top.Dom.selectById('mailWritePopupLayout_AttachFile').close(); 
        
		this.mailContent; 
		Top.Controller.get('createWorkspaceLayoutLogic').treeviewUpdate(); //T-User TreeView
		
		if(Top.version.substr(8,2) > 87)
			Top.Dom.selectById('Text_Subject_Popup').setProperties({'on-blur' : 'manageByte', 'on-keydown' : 'manageByte'});
		else
			Top.Dom.selectById('Text_Subject_Popup').setProperties({'on-blur' : 'manageByte', 'on-keyup' : 'manageByte'});
		
		$('div#top-dialog-root_mailWriteDialog .top-dialog-max_min_button').click(function(){Top.Controller.get('mailWritePopupLogic').setEditorSize();});
		
	}, setEditorSize : function() {
		
		var upperHeight = $('top-linearlayout#mailWritePopupLayout_Button')[0].offsetHeight + $('top-linearlayout#mailWritePopupLayout_Info')[0].offsetHeight + $('top-linearlayout#mailWritePopupLayout_FileBox')[0].offsetHeight;
		Top.Dom.selectById('mailWritePopupLayout_WorkArea').setProperties({'layout-height':'calc(100% - ' + upperHeight + 'px)'});
		
		if($('div#top-dialog-root_mailWriteDialog .top-dialog-max_min_button')[0].classList[1] == 'max')
			Top.Dom.selectById('mailWriteDialog').setProperties({'layout-top':'','layout-left':'','layout-bottom': '15px', 'layout-right':'70px'});
		
	}, adjustDialog : function() {
		
		if(event.key === ";") {
			event.target.value = event.target.value.replace(/;$/gi,"")
			event.target.blur();
			event.target.focus();
		}
		
		var plusHeight = 0;;
		
		if($('top-linearlayout#mailWritePopupLayout_Info')[0].offsetHeight > 205)
			plusHeight += $('top-linearlayout#mailWritePopupLayout_Info')[0].offsetHeight - 205;
		if(Top.Dom.selectById('mailWritePopupLayout_FileBox').getProperties('visible') == 'visible')
			plusHeight += 154;
		
		plusHeight = plusHeight + 553 + 'px';
		
		Top.Dom.selectById('mailWriteDialog').setProperties({'layout-top':'','layout-left':'','layout-bottom': '15px', 'layout-right':'70px', 'layout-height':plusHeight});
		this.setEditorSize();
		
	}, clickDropdownMenu : function(event, widget){
		
		if(widget.getSelectedMenuId() == "menuitem0"){
			Top.Controller.get("mailWritePopupLogic").saveAtTDrive = false;
			Top.Controller.get("mailWritePopupLogic").isFromTDrive = false;
			Top.Controller.get("mailWritePopupLogic").attachFile()
		}
		else if (widget.getSelectedMenuId() == "menuitem1"){
			Top.Controller.get("mailWritePopupLogic").toTdrive = true; 
			Top.Controller.get("mailWritePopupLogic").saveAtTDrive = false;
			Top.Controller.get("mailWritePopupLogic").isFromTDrive = true;
			var config = {
					buttonText1: '첨부', // (선택사항)왼쪽 버튼에 표시될 텍스트. 
					buttonText2: '취소', // (선택사항)오른쪽 버튼에 표시될 텍스트
					buttonFn1: function(){ Top.Controller.get("mailWritePopupLogic").attachFile() }, // (선택사항)왼쪽 버튼 동작
					}
		
			OPEN_TDRIVE(config);
			
		}
		else if (widget.getSelectedMenuId() == "menuitem2"){
			Top.Controller.get("mailWritePopupLogic").saveAtTDrive = true;
			Top.Controller.get("mailWritePopupLogic").isFromTDrive = false;
			Top.Controller.get("mailWritePopupLogic").attachFile()
		}
		
	},	
	
	clickAddressBookRec: function(){
        
        Top.Controller.get("mailWritePopupLogic").addRec = true;
        Top.Dom.selectById('mailAddressBookDialog').open();
        
    },clickAddressBookCc: function(){
        
        Top.Controller.get("mailWritePopupLogic").addCc = true;
        Top.Dom.selectById('mailAddressBookDialog').open();
        
    },clickAddressBookBcc: function(){
        
        Top.Controller.get("mailWritePopupLogic").addBcc = true;
        Top.Dom.selectById('mailAddressBookDialog').open();
        
    },
	clickPreview : function(event, widget) {
		
		Top.Controller.get('mailWritePreviewLogic').writeMode = 'popup';
		Top.Dom.selectById('mailWritePreviewDialog').open();
	}
	,
	
	clickSend : function(event, widget) {
		
	
		var mailSubject = Top.Dom.selectById('Text_Subject_Popup').getText();
		if (!mailSubject){
			mailSubject = '제목 없음';
		}
		
		
		
		if (Top.Dom.selectById('Text_ReceiverAddress_Popup').getProperties('chip-items') != undefined){
			Top.Controller.get('mailWritePopupLogic').receiverAddr = Top.Dom.selectById('Text_ReceiverAddress_Popup').getProperties('chip-items');
		}
		var ReceiverArr = [];
		for (var i = 0; i < Top.Controller.get('mailWritePopupLogic').receiverAddr.length; i++){
			if (Top.Controller.get('mailWritePopupLogic').receiverAddr[i].text == ""){
				Top.Controller.get('mailWritePopupLogic').receiverAddr.splice(i,1); 
				i = i - 1; 
			} else
				ReceiverArr.push({ "TO_ADDR" : mail.getEMailFromChip(this.receiverAddr[i].text)});
			}
		
		if(Top.Dom.selectById('Text_Cc_Popup').getProperties('chip-items') != undefined){
			Top.Controller.get('mailWritePopupLogic').CCAddr = Top.Dom.selectById('Text_Cc_Popup').getProperties('chip-items');
		}
		
		var CcArr=[]; 
		for (var i = 0; i < Top.Controller.get('mailWritePopupLogic').CCAddr.length; i++){	
			if (Top.Controller.get('mailWritePopupLogic').CCAddr[i].text == ""){
				Top.Controller.get('mailWritePopupLogic').CCAddr.splice(i,1);
				i = i - 1; 
			} else
				CcArr.push({ "TO_ADDR" : mail.getEMailFromChip(this.CCAddr[i].text)});
		}
		if (Top.Controller.get('mailWritePopupLogic').CCAddr.length == 0)
			CcArr[i]= { "TO_ADDR" : ""};
		
		if(Top.Dom.selectById('Text_HiddenCc_Popup').getProperties('chip-items') != undefined){
			Top.Controller.get('mailWritePopupLogic').BCCAddr = Top.Dom.selectById('Text_HiddenCc_Popup').getProperties('chip-items');
		}
		
		var HiddenCcArr = []; 
		for (var i = 0; i < Top.Controller.get('mailWritePopupLogic').BCCAddr.length; i++){	
			if (Top.Controller.get('mailWritePopupLogic').BCCAddr[i].text == ""){
				Top.Controller.get('mailWritePopupLogic').BCCAddr.splice(i,1);
				i = i - 1; 
			} else
				HiddenCcArr.push({ "TO_ADDR" : mail.getEMailFromChip(this.BCCAddr[i].text)});
		}
		if (Top.Controller.get('mailWritePopupLogic').BCCAddr.length == 0)
			HiddenCcArr[i]= { "TO_ADDR" : ""};
	
		
		
		 var regExp = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i;
		
		 var errorCnt = 0; 
		 for (var i = 0 ; i < ReceiverArr.length; i++){
			 if ( !regExp.test(ReceiverArr[i].TO_ADDR)  ){
				 errorCnt++; 
			 }
		 }
		 if (errorCnt==Top.Controller.get('mailWritePopupLogic').receiverAddr.length || Top.Controller.get('mailWritePopupLogic').receiverAddr.length == 0){
			 Top.Dom.selectById("mailWritePopupLayout_Receiver_Error_Icon").setProperties({"popover-trigger": "hover",
				   "popover-id": "mailReceiverErrorPopover",
				   "popover-target": "mailWritePopupLayout_Receiver_Error_Icon"});
			 var media = window.matchMedia("screen and (max-device-width: 1024px)");
			 if(media.matches)
			 	Top.Dom.selectById("mailReceiverErrorPopover").setProperties({"placement": "bottom"});
			 Top.Dom.selectById("mailWritePopupLayout_Receiver_Error_Icon").setProperties({'visible':'visible'});
			 Top.Dom.selectById('mailWritePopupLayout_Receiver_Layout').setProperties({'border-color' : '#FF0000'})
			 return; 
		 }
		 else{
			 Top.Dom.selectById("mailWritePopupLayout_Receiver_Error_Icon").setProperties({"visible": "hidden"});
			 Top.Dom.selectById('mailWritePopupLayout_Receiver_Layout').setProperties({'border-color' : '#ACB0BC'})
		 }
		

		var senderAddr = Top.Dom.selectById('mailWritePopupLayout_Info_Sender_Account').getSelectedText();
		var accountId = Top.Dom.selectById('mailWritePopupLayout_Info_Sender_Account').getValue();
		
		var domain = '@' + Top.Dom.selectById('mailWritePopupLayout_Info_Sender_Account').getProperties('nodes')[0].text.split('@')[1];
//		 if (senderAddr.substring(senderAddr.indexOf('@')) == domain){
//			 for (var i = 0 ; i < ReceiverArr.length; i++){
//				 receiverDomain = ReceiverArr[i].TO_ADDR.substring(ReceiverArr[i].TO_ADDR.indexOf('@'));
//				 if (receiverDomain != domain){
//	    				TeeAlarm.open({title: '보내기', content: '내부 편지함에서 외부 메일 계정으로 전송 할 수 없습니다. 외부 메일함 사용 바랍니다.'});
//					 return; 
//				}
//			 }
//		 }
//		 else{
//		 	 for (var i = 0 ; i < ReceiverArr.length; i++){
//				 receiverDomain = ReceiverArr[i].TO_ADDR.substring(ReceiverArr[i].TO_ADDR.indexOf('@'));
//				 if (receiverDomain == domain){
//						TeeAlarm.open({title: '보내기', content: '죄송합니다.<br>현재는 '+domain +'->' +domain+' 으로만<br>전송이 가능합니다.<br>외부로 메일을 보내시려면 외부계정을 등록 후<br>보낸 사람을 외부계정으로 선택하여 사용하시기 바랍니다.'});
//					 return; 
//				}
//			 }
//
//		 }
		 
		

		 
		var importance;
		if (Top.Dom.selectById('CheckBox_Importance_Popup').isChecked() == true)
			importance = 'IMP0001';
		else
			importance = 'IMP0002';
		
	
		if (Top.Controller.get('mailWritePopupLogic').tempYN==true)
    		var tempMail = Top.Controller.get('mailMainLeftLogic').clickedMail;
    	else if (Top.Controller.get('mailWritePopupLogic').tempMailId != null)
    		var tempMail = Top.Controller.get('mailWritePopupLogic').tempMailId;
    	else
    		var tempMail = "";

    	var spaceremove = mailSunEditor2.getContents();
        spaceremove = [].filter.call(spaceremove, function (c) {return c.charCodeAt() !== 8203;}).join('');
        var context = spaceremove;
        context = '<style> table { width: 100%; border-spacing : 0px } td, th { border: 1px solid #e1e1e1 } </style>' + context;
		
		var mailAttachment = [] ; 
		var isAttach;
		
			
		var isToOut = false;
		var senderDomain = senderAddr.substring(senderAddr.indexOf("@"));
		
		for (var i = 0 ; i < ReceiverArr.length; i++){
			 receiverDomain = ReceiverArr[i].TO_ADDR.substring(ReceiverArr[i].TO_ADDR.indexOf('@'));
			 if (receiverDomain != domain){
				 isToOut = true;	  
			}
		 }
		
		var ctrl = Top.Controller.get('mailWritePopupLogic');
		
		var fileListArr = [];

		function load_file(callback) {
			
			if (Top.Controller.get('mailWritePopupLogic').fileAttach.length ==0)
    		{
    		isAttach = 'COM0002';
    		mailAttachment = [{
	            "ATTACHMENT_ID":"",
	            "MAIL_ID": "",
	            "STORAGE_PATH" : "", 
	            "USER_ID" : "",
	            "WS_ID" : "",
	            "EXTENSION" : "",
	            "FILE_NAME" : "",
	            "TDRIVE_CH_ID" : "", 
	            "FILE_ID" : "",
	            "FILE_SIZE" : ""
	            
	        }];
    		callback();
    		}
    	
    	else{
    		
    		Top.Dom.selectById('mailWritePopupLayout_Button_Send').setProperties({'disabled' : 'true'});
    		Top.Loader.start({parentId:'mailWritePopupLayout'}); 
    		isAttach = 'COM0001';
    		
    			for (var i = 0; i < Top.Controller.get('mailWritePopupLogic').fileAttach.length; i++){
    				if(Top.Controller.get('mailWritePopupLogic').tempFileIndex[i] == "0"){
    				
    					var l = Top.Controller.get('mailWritePopupLogic').fileAttach[i].file[0].name.split('.').length;
    					var file_extension = Top.Controller.get('mailWritePopupLogic').fileAttach[i].file[0].name.split('.')[l-1];
    					var file_name = Top.Controller.get('mailWritePopupLogic').fileAttach[i].file[0].name.slice(0,-(file_extension.length + 1));
    					var file_size = Top.Controller.get('mailWritePopupLogic').fileAttach[i].file[0].size; 
    					var inputDTO = {
    		    				"dto": {
    		    		               "workspace_id": workspaceManager.getMySpaceId(),
    		    		                "channel_id": Top.Dom.selectById('mailWritePopupLayout_Info_Sender_Account').getProperties('nodes')[0].value,
    		    		                "storageFileInfo": {
    		    		                    "user_id": userManager.getLoginUserId(),
    		    		                    "file_last_update_user_id": userManager.getLoginUserId(),
    		    		                    "file_id": "",
    		    		                    "file_name": file_name,
    		    		                    "file_extension": file_extension,
    		    		                    "file_created_at": "",
    		    		                    "file_updated_at": "",
    		    		                    "file_size": file_size,
    		    		                    "user_context_1": "",
    		    		                    "user_context_2": "",
    		    		                    "user_context_3": ""
    		    		                }
    		    		           }
    		    				
    		    		}
    					
	    				storageManager.UploadFile(Top.Controller.get('mailWritePopupLogic').fileAttach[i].file[0],
	    						inputDTO, 'Calendar', "Calendar?action=TempStorages",  function(response){
							if (response.dto.resultMsg!= "Success"){
								Top.Dom.selectById('mailWritePopupLayout_Button_Send').setProperties({'disabled' : 'false'});
								Top.Loader.stop({parentId:'mailWritePopupLayout'});
								  return; 
							}
							var tmp = response.dto.storageFileInfoList[0];
							mailAttachment.push({
								"ATTACHMENT_ID":"",
	    			            "MAIL_ID": "",
	    			            "STORAGE_PATH" : "", // 수정필요
	    			            "USER_ID" : userManager.getLoginUserId(),
	    			            "WS_ID" : workspaceManager.getMySpaceId(),
	    			            "EXTENSION" : tmp.file_extension,
	    			            "FILE_NAME" : tmp.file_name,
	    			            "TDRIVE_CH_ID" : Top.Dom.selectById('mailWritePopupLayout_Info_Sender_Account').getProperties('nodes')[0].value, 
	    			            "FILE_ID" : tmp.file_id,
	    			            "FILE_SIZE" : tmp.file_size										
							});
							callback();
						}, function(){});
	    				
	    			}
    				else if (Top.Controller.get('mailWritePopupLogic').tempFileIndex[i] == "3" || Top.Controller.get('mailWritePopupLogic').tempFileIndex[i] == "4"){
    					var file_extension = Top.Controller.get('mailWritePopupLogic').fileAttach[i].file_extension;
    					var file_name = Top.Controller.get('mailWritePopupLogic').fileAttach[i].file_name;
    					var file_size = Top.Controller.get('mailWritePopupLogic').fileAttach[i].size;
    					mailAttachment.push({
							"ATTACHMENT_ID":"",
    			            "MAIL_ID": "",
    			            "STORAGE_PATH" : Top.Controller.get('mailWritePopupLogic').fileAttach[i].file_path, // 수정필요
    			            "USER_ID" : userManager.getLoginUserId(),
    			            "WS_ID" : workspaceManager.getMySpaceId(),
    			            "EXTENSION" : file_extension,
    			            "FILE_NAME" : file_name,
    			            "TDRIVE_CH_ID" : Top.Dom.selectById('mailWritePopupLayout_Info_Sender_Account').getProperties('nodes')[0].value, 
    			            "FILE_ID" : Top.Controller.get('mailWritePopupLogic').fileAttach[i].file_id,
    			            "FILE_SIZE" : file_size										
						});
						callback();
    				}

    				else {
    					
    					var file_name = Top.Controller.get('mailWritePopupLogic').fileAttach[i].file_name;
    					var file_extension = Top.Controller.get('mailWritePopupLogic').fileAttach[i].file_extension;
    					var file_size = Top.Controller.get('mailWritePopupLogic').fileAttach[i].size;
    					storageManager.CopyFile("Deep", Top.Controller.get('mailWritePopupLogic').fileAttach[i].file_id,
    							Top.Dom.selectById('mailWritePopupLayout_Info_Sender_Account').getProperties('nodes')[0].value,
    							Top.Controller.get('mailWritePopupLogic').fileAttach[i].file_name, Top.Controller.get('mailWritePopupLogic').fileAttach[i].file_extension,
    							"","","",
    							function(response){
    								var tmp = response.dto.storageFileInfoList[0];
    								mailAttachment.push({
    									"ATTACHMENT_ID":"",
    		    			            "MAIL_ID": "",
    		    			            "STORAGE_PATH" : "", // 수정필요
    		    			            "USER_ID" : userManager.getLoginUserId(),
    		    			            "WS_ID" : workspaceManager.getMySpaceId(),
    		    			            "EXTENSION" : tmp.file_extension,
    		    			            "FILE_NAME" : tmp.file_name,
    		    			            "TDRIVE_CH_ID" : Top.Dom.selectById('mailWritePopupLayout_Info_Sender_Account').getProperties('nodes')[0].value, 
    		    			            "FILE_ID" : tmp.file_id,
    		    			            "FILE_SIZE" : tmp.file_size			 							
    								});
    								callback();
    								
    							}, function(){}
    						)
    				}
    			}
        	}
    	
		}
    	
        
        load_file(function(){
			if (Top.Controller.get('mailWritePopupLogic').fileAttach.length !=0 &&Top.Controller.get('mailWritePopupLogic').fileAttach.length != mailAttachment.length) return;
			else{
				
				var data={
						"dto":{
			    			"MAIL_FLAG" : "SND0002",
							"MailSendTo" : ReceiverArr,
					        "MailSendCc" : CcArr,
					        "MailSendBcc" : HiddenCcArr,
					        "DOMailAttachment":mailAttachment,
					        "FOLDER_ID":"",
					        "SUBJECT" : mailSubject,
					        "SENDER_NAME" : "",
					        "HAS_ATTACHMENT_YN_CODE" : isAttach,
					        "SEEN_YN_CODE" : "",
					        "IMPORTANCE_YN_CODE" : "",
					        "PINNED_YN_CODE" : "",
					        "FILE_NAME" : "",
					        "WRITING_MAIL_ID" : "",
					        "SYNC_STATE" : "",
					        "SOURCE_MAIL_ID" : "",
					        "FILE_SIZE" : "",
					        "DISPOSITION_NOTI" : "",
					        "MESSAGE_ID" : "",
					        "X_TNEF" : "",
					        "SPAM_YN_CODE" : "",
					        "SENDER_ADDR" : senderAddr,
					        "CONTENT" : context,
					        "CMFLG" : "SendMail_Re",
					        "USER_ID" : userManager.getLoginUserId(),
					        "WS_ID" : workspaceManager.getMySpaceId(),
					        "MESSAGE_HEADER" : "",
					        "EXTRA_INFOS" : "",
					        "FIRST_SAW_TIME" : "", 
					        "SEND_IMPORTANCE_CODE" : importance,
					        "MAIL_ID" : tempMail,
					        "ACCOUNT_ID" : accountId

					        
					    }
				};		
		
			Top.Ajax.execute({
				
				type : "POST",
				url :_workspace.url + "Mail/Mail?action=Send",
				dataType : "json",
				data	 : JSON.stringify(data),
				contentType: "application/json; charset=utf-8",
				xhrFields: {withCredentials: true},
				crossDomain : true,
				success : function(ret,xhr,status) {
				
					Top.Dom.selectById('mailWritePopupLayout_Button_Send').setProperties({'disabled' : 'false'});
					if (Top.Controller.get('mailWritePopupLogic').fileAttach.length > 0)
							Top.Loader.stop({parentId:'mailWritePopupLayout'}); 
					if (ret.dto != undefined 
							&&(ret.dto.RESULT_MSG == null||ret.dto.RESULT_MSG == '메일유저 없음'||ret.dto.RESULT_MSG == '외부메일입니다. '))  // 수정																									// 요망
						{
						TeeAlarm.open({title: '보내기', content: '죄송합니다.<br>존재하지 않는 메일 주소 입니다.'});
						return ;
						}
					
					Top.Dom.selectById('mailWriteDialog').close();
					Top.Dom.selectById('mailSendSuccessDialog').open();
					if(routeManager.getApp() == 'mail')
						Top.Controller.get('mailMainLeftLogic').resetCount();
					clearInterval(Top.Controller.get('mailWritePopupLogic').tempInt);
					
				},
				complete : function(ret,xhr,status) {
				
				},
				error : function(ret, xhr,status) {
					Top.Dom.selectById('mailWritePopupLayout_Button_Send').setProperties({'disabled' : 'false'});
	
				}
				
			});		
		
		
			
			
			}});
		
        
			
	
	},
	attachFile : function (){
	
		if (Top.Controller.get("mailWritePopupLogic").isFromTDrive == false){ 
			 let fileChooser = Top.Device.FileChooser.create({
				 onBeforeLoad : function(file){return true;},
				 charset : "euc-kr",
		            onFileChoose : function (fileChooser) {   
		            	Top.Controller.get('mailWritePopupLogic').attachTable(fileChooser);
		           }
		        });
			 
				fileChooser.show();
				
		}
		else{

			Top.Dom.selectById('mailWritePopupLayout_FileBox').setVisible('visible');
			this.adjustDialog();
			
			Top.Controller.get('mailWritePopupLogic').tempTFileId = [];
			
			var columnOpt = {
					"0" : {},
					"1"	: {},
					"4" : {
						layout: function(data, index) {
							return '<top-icon id="mailWritePopupLayout_Attach_Delete_'+index+'_0" class="icon-close" text-color="#636363" text-size="11px" on-click="deleteAttach"></top-icon>';}
					}	
					
			};
			
			for(var i = 0 ; i < Top.Dom.selectById("fcFileContentsTableView").getCheckedData().length; i++){
				Top.Controller.get('mailWritePopupLogic').tempTFileId.push(Top.Dom.selectById("fcFileContentsTableView").getCheckedData()[i].file_id)
			}
			
			for(var i = 0 ; i < Top.Dom.selectById("fcFileContentsTableView").getCheckedData().length; i++){
				this.putTdrivefileAttach(i, this.putTdriveTempfile);
			}
			
			
			if(Top.Dom.selectById("fcFileContentsTableView").getCheckedData().length != 0 ){
				Top.Dom.selectById('mailWritePopupLayout_FileTable').setProperties({"column-option": columnOpt});
    			mailFileAttachmentRepo.setValue('mailFileAttachmentList',Top.Controller.get('mailWritePopupLogic').fileList);
			}

		}
	


	    }, attachTable : function(data) {
	    	
			Top.Controller.get('mailWritePopupLogic').tempTFileId = [];
			
	    	var columnOpt = {
					"0" : {},
					"1"	: {},
					"4" : {
						layout: function(data, index) {
							return '<top-icon id="mailWritePopupLayout_Attach_Delete_'+index+'_0" class="icon-close" text-color="#636363" text-size="11px" on-click="deleteAttach"></top-icon>';}
					}	
					
			};
	    	
	    	Top.Controller.get('mailWritePopupLogic').fileAttach.push(data);

			var l = Top.Controller.get('mailWritePopupLogic').fileAttach.length;
			
			var size = Top.Controller.get('mailWritePopupLogic').fileAttach[l-1].file[0].size;
			if ((size*1) < 10485760)
				var method = "일반 첨부" ;
			else{
				var method = "대용량 첨부";
				TeeAlarm.open({title: '', content: '10MB 이하의 파일만 첨부 가능합니다.'});
				Top.Controller.get('mailWritePopupLogic').fileAttach.pop();
				return;
			}

	    	Top.Dom.selectById('mailWritePopupLayout_FileBox').setVisible('visible');
			this.adjustDialog();
			
			if ( method == "대용량 첨부" )
				var period = "~ "+mail.after30Days(null,'/')+" (30일간)";
			else
				var period = "제한 없음";
		
			size = mail.changeSize(size);
			
			Top.Controller.get('mailWritePopupLogic').fileList.push(
					{
						"name" : Top.Controller.get('mailWritePopupLogic').fileAttach[l-1].file[0].name,
						"size" : size,
						"attach_method" : method,
						"period" : period
					}
			);
			
			if(Top.Controller.get("mailWritePopupLogic").saveAtTDrive == false || Top.Controller.get("mailWritePopupLogic").saveAtTDrive == undefined){
				Top.Controller.get('mailWritePopupLogic').tempFileIndex.push("0");
			}
			else{
				Top.Controller.get('mailWritePopupLogic').tempFileIndex.push("2");
			}
			Top.Controller.get('mailWritePopupLogic').updateAttachmentInfo(); 

			
			Top.Dom.selectById('mailWritePopupLayout_FileTable').setProperties({"column-option": columnOpt});
			mailFileAttachmentRepo.setValue('mailFileAttachmentList',Top.Controller.get('mailWritePopupLogic').fileList);

			
	    },
	    
	    putTdrivefileAttach : function(index, callbackFunc) {
				Top.Controller.get('mailWritePopupLogic').tempFileIndex.push("1");
				
				
				Top.Controller.get('mailWritePopupLogic').fileAttach.push(
						{
							"file_name" : Top.Dom.selectById("fcFileContentsTableView").getCheckedData()[index].file_name,
							"file_id" : Top.Dom.selectById("fcFileContentsTableView").getCheckedData()[index].file_id,
							"size" : Top.Dom.selectById("fcFileContentsTableView").getCheckedData()[index].file_size,
							"file_extension" : Top.Dom.selectById("fcFileContentsTableView").getCheckedData()[index].file_extension
						}
				);
				
				Top.Controller.get('mailWritePopupLogic').updateAttachmentInfo(); 

				
				var size;
				size = Top.Dom.selectById("fcFileContentsTableView").getCheckedData()[index].file_size;
    			if ((size*1) < 10485760)
    				var method = "일반 첨부" ;
    			else{
    				var method = "대용량 첨부";
    				TeeAlarm.open({title: '', content: '10MB 이하의 파일만 첨부 가능합니다.'});
    				Top.Controller.get('mailWritePopupLogic').fileAttach.pop();
    				Top.Controller.get('mailWritePopupLogic').tempFileIndex.pop(); 
    				return;

    			}

    			if ( method == "대용량 첨부" )
    				var period = "~ "+mail.after30Days(null,'/')+" (30일간)";
    			else
    				var period = "제한 없음";
    			
    			size = mail.changeSize(size);
    			
				Top.Controller.get('mailWritePopupLogic').fileList.push(
    					{
    						"name" : Top.Dom.selectById("fcFileContentsTableView").getCheckedData()[index].fileFullName,
    						"size" : size,
    						"attach_method" : method,
    						"period" : period
    					}
    			);
				
				callbackFunc(index);
	    },
	    
	    putTdriveTempfile : function(index) {
	    	storageManager.getDownloadFileInfo(Top.Controller.get('mailWritePopupLogic').tempTFileId[index], Top.Dom.selectById('mailWritePopupLayout_Info_Sender_Account').getProperties('nodes')[0].value,
	    			function(result){
					 	var reader = new window.FileReader();
						reader.readAsDataURL(result.blobData); 
						reader.onloadend = function() {
							
							 var filecontent = reader.result; 
		                     var contents;
		                     if(Object.prototype.toString.call(filecontent) === "[object ArrayBuffer]"){ //ArrayBuffer
		                         var binary = '';
		                        var bytes = new Uint8Array( filecontent );
		                        var len = bytes.byteLength;
		                        for (var j = 0; j < len; j++) {
		                            binary += String.fromCharCode( bytes[ j ] );
		                        }
		                        contents = window.btoa(binary);
		                     }
		                     else if((filecontent.split("base64,").length == 2) &&
		                         RegExp("^(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{4})$").test(filecontent.split("base64,")[1])){ //base64
		                             contents = filecontent.split("base64,")[1];
		                     }
		                     else{ //raw txt
		                         contents = btoa(unescape(encodeURIComponent(ctrl.fileAttach[i].src)));
		                     }
		                     Top.Controller.get("mailWritePopupLogic").tempTdrivefile.push( {"file_id" : Top.Controller.get('mailWritePopupLogic').tempTFileId[index], "file_info" : {
		                         "filename" : result.fileName,
		                         "contents" : contents
		                     			}
							 })
						}
			},function(){})	
	    },
		
	    set_auto_complete : function(){
			
			  var data =  {"dto" : {
				    "CMFLG" : "AutoCreateAddress",
				    "MAIL_USER_NAME" : "",
				    "USER_ID" : userManager.getLoginUserId()
				 }};
			  
			  Top.Ajax.execute({
					
					type : "POST",
					url :_workspace.url + "Mail/Mail?action=AutoCreateEMailAddress",
					dataType : "json",
					data	 : JSON.stringify(data),
					contentType: "application/json; charset=utf-8",
					xhrFields: {withCredentials: true},
					crossDomain : true,
					success : function(ret,xhr,status) {
					
						var addr_info_array = [];						
						var retArr = ret.dto.ADDRESS_INFO;
						if(retArr == null) retArr = [];
						for(var i=0; i < retArr.length; i++){
							
							addr_info_array.push('' + retArr[i].MAIL_USER_NAME + '<' + retArr[i].EMAIL_ADDRESS + '>');
							
						}
						
						Top.Dom.selectById('Text_ReceiverAddress_Popup').setProperties({'auto-complete':addr_info_array.join(','),
								'auto-complete-max-height':'210px',
								'support-empty':'false'});
						Top.Dom.selectById('Text_ReceiverAddress_Popup').setProperties({'chip-items' : Top.Controller.get('mailWritePopupLogic').receiverAddr});
						Top.Dom.selectById('Text_ReceiverAddress_Popup').setProperties({'on-keyup':'adjustDialog', 'on-select' : 'autoCompleteChipItem', 'on-close' : 'closeChipItem', 'on-add':'addReceiver', 'on-blur':'blurAddress'});
						
						Top.Dom.selectById('Text_Cc_Popup').setProperties({'auto-complete':addr_info_array.join(','),
							'auto-complete-max-height':'210px',
							  'support-empty':'false'});
						Top.Dom.selectById('Text_Cc_Popup').setProperties({'chip-items' : Top.Controller.get('mailWritePopupLogic').CCAddr});
						Top.Dom.selectById('Text_Cc_Popup').setProperties({'on-keyup':'adjustDialog', 'on-select' : 'autoCompleteChipItem', 'on-close' : 'closeChipItem', 'on-add':'addCC', 'on-blur':'blurAddress'});
						 
											
						Top.Dom.selectById('Text_HiddenCc_Popup').setProperties({'auto-complete':addr_info_array.join(','),
							'auto-complete-max-height':'210px',
							  'support-empty':'false'});
						Top.Dom.selectById('Text_HiddenCc_Popup').setProperties({'chip-items' : Top.Controller.get('mailWritePopupLogic').BCCAddr});
						Top.Dom.selectById('Text_HiddenCc_Popup').setProperties({'on-keyup':'adjustDialog', 'on-select' : 'autoCompleteChipItem', 'on-close' : 'closeChipItem', 'on-add':'addHiddenCC', 'on-blur':'blurAddress'});

						
					},
					error : function(ret, xhr,status) {
					
					
					}
					
				});	
			  
			  
		
		 }, blurAddress : function(event, widget) {
			 
			 this.blured = true;
			if(event.target.value == undefined || event.target.value == "")
				return ;
			Top.Controller.get('mailWritePopupLogic').isLeaved = true;
			var value = {"text": event.target.value}
			widget.addChip(value);

			 
		 }, addReceiver : function(event, widget) {
			 
			 var detail; 
		     if (Top.Controller.get('mailWritePopupLogic').isAutoComplete == true){

		    	 if (event == undefined)
		    		  return; // 칩위젯 오류로 인한 임시조치 88 버전에서 해결가능
		    	  
				  var i = Top.Controller.get('mailWritePopupLogic').receiverAddr.length; 
		    	  detail ={"text": Top.Controller.get('mailWritePopupLogic').receiverAddr[i-1].text}; 
		    	  Top.Controller.get('mailWritePopupLogic').isAutoComplete = null; 
		    	  Top.Controller.get('mailWritePopupLogic').receiverAddr[i-1] = detail;
		    	  
		     }
		     else if (Top.Controller.get('mailWritePopupLogic').isLeaved == true){
		    	  Top.Controller.get('mailWritePopupLogic').isLeaved = null;
		     }
		     else{
		    	  detail = {"text": event.currentTarget.value}; 
		    	  Top.Controller.get('mailWritePopupLogic').isAutoComplete = null; 
		      }
		    this.adjustDialog();
			 
		 }, addCC : function(event, widget) {
			 
			  var detail; 
		      if (Top.Controller.get('mailWritePopupLogic').isAutoComplete == true){

		    	  if (event == undefined)
		    		  return; 
		    	  
				  var i = Top.Controller.get('mailWritePopupLogic').CCAddr.length; 
		    	  detail ={"text": Top.Controller.get('mailWritePopupLogic').CCAddr[i-1].text}; 
		    	  Top.Controller.get('mailWritePopupLogic').isAutoComplete = null; 
				  Top.Controller.get('mailWritePopupLogic').CCAddr[i-1] = detail;
		      } else if (Top.Controller.get('mailWritePopupLogic').isLeaved == true){
		    	  Top.Controller.get('mailWritePopupLogic').isLeaved == null;
		      }
		      else{
		    	  detail = {"text": event.currentTarget.value};
		    	  Top.Controller.get('mailWritePopupLogic').isAutoComplete = null;				      
		      }
		      this.adjustDialog();
		      
		 }, addHiddenCC : function(event, widget) {
			 
			 var detail; 
		      if (Top.Controller.get('mailWritePopupLogic').isAutoComplete == true){

		    	  if (event == undefined)
		    		  return; 
		    	  
				  var i = Top.Controller.get('mailWritePopupLogic').BCCAddr.length; 
		    	  detail ={"text": Top.Controller.get('mailWritePopupLogic').BCCAddr[i-1].text}; 
		    	  Top.Controller.get('mailWritePopupLogic').isAutoComplete = null; 
		    	  Top.Controller.get('mailWritePopupLogic').BCCAddr[i-1] = detail;
		      } else if (Top.Controller.get('mailWritePopupLogic').isLeaved == true){
		    	  Top.Controller.get('mailWritePopupLogic').isLeaved = null;
		      }
		      else{
		    	  detail = {"text": event.currentTarget.value}; 
		    	  Top.Controller.get('mailWritePopupLogic').isAutoComplete = null; 	    	  
		      }
		      this.adjustDialog();
			 
		 }, 
		 saveTemp : function() {
			 
				var mailSubject = Top.Dom.selectById('Text_Subject_Popup').getText();
				if (!mailSubject){
					mailSubject = '제목 없음';
				}
					
				var ReceiverArr = [];
				for (var i = 0; i < Top.Controller.get('mailWritePopupLogic').receiverAddr.length; i++){	
					if (Top.Controller.get('mailWritePopupLogic').receiverAddr[i].text == "")
						Top.Controller.get('mailWritePopupLogic').receiverAddr[i].splice(i,1);
					ReceiverArr[i]= { "TO_ADDR" : mail.getEMailFromChip(this.receiverAddr[i].text)};
					}
				if (Top.Controller.get('mailWritePopupLogic').receiverAddr.length == 0)
					ReceiverArr[i]= { "TO_ADDR" : ""};
				
				var CcArr=[]; 
				for (var i = 0; i < Top.Controller.get('mailWritePopupLogic').CCAddr.length; i++){	
					if (Top.Controller.get('mailWritePopupLogic').CCAddr[i].text == "")
						Top.Controller.get('mailWritePopupLogic').CCAddr[i].splice(i,1);
					CcArr[i]= { "TO_ADDR" : mail.getEMailFromChip(this.CCAddr[i].text)};
					}
				if (Top.Controller.get('mailWritePopupLogic').CCAddr.length == 0)
					CcArr[i]= { "TO_ADDR" : ""};
				
				
				var HiddenCcArr = []; 
				for (var i = 0; i < Top.Controller.get('mailWritePopupLogic').BCCAddr.length; i++){	
					if (Top.Controller.get('mailWritePopupLogic').BCCAddr[i].text == "")
						Top.Controller.get('mailWritePopupLogic').BCCAddr[i].splice(i,1);
					HiddenCcArr[i]= { "TO_ADDR" : mail.getEMailFromChip(this.BCCAddr[i].text)};
					}
				if (Top.Controller.get('mailWritePopupLogic').BCCAddr.length == 0)
					HiddenCcArr[i]= { "TO_ADDR" : ""};
				
				var importance;
				if (Top.Dom.selectById('CheckBox_Importance_Popup').isChecked() == true)
					importance = 'IMP0001';
				else
					importance = 'IMP0002';
				
				var spaceremove = mailSunEditor2.getContents();
		        spaceremove = [].filter.call(spaceremove, function (c) {return c.charCodeAt() !== 8203;}).join('');
		        var context = spaceremove;
		        context = '<style> table { width: 100%; border-spacing : 0px } td, th { border: 1px solid #e1e1e1 } </style>' + context;
		        
		        var senderAddr = Top.Dom.selectById('mailWritePopupLayout_Info_Sender_Account').getSelectedText();
				var accountId = Top.Dom.selectById('mailWritePopupLayout_Info_Sender_Account').getValue();
				
				var data={
						"dto":{
								"MailSendTo" 				: ReceiverArr,
						        "MailSendCc" 				: CcArr,
						        "MailSendBcc" 				: HiddenCcArr,
						        "ACCOUNT_ID"				: Top.Dom.selectById('mailWritePopupLayout_Info_Sender_Account').getProperties('nodes')[0].value,
						        "MAIL_ID"					: this.tempMailId,
						        "MAIL_GROUP_INDEX"			: this.tempGroupIndex,
						        "CMFLG" 					: "CreateTempMail",
						        "FOLDER_ID"					: "",
						        "SUBJECT" 					: mailSubject,
						        "SENDER_NAME" 				: "",
						        "HAS_ATTACHMENT_YN_CODE" 	: 'COM0002',
						        "SEEN_YN_CODE" 				: "",
						        "IMPORTANCE_YN_CODE" 		: "",
						        "PINNED_YN_CODE" 			: "",
						        "FILE_NAME" 				: "",
						        "WRITING_MAIL_ID" 			: "",
						        "SYNC_STATE" 				: "",
						        "SOURCE_MAIL_ID" 			: "",
						        "FILE_SIZE" 				: "",
						        "DISPOSITION_NOTI" 			: "",
						        "MESSAGE_ID" 				: "",
						        "X_TNEF" 					: "",
						        "SPAM_YN_CODE" 				: "",
						        "SENDER_ADDR" 				: senderAddr,
						        "CONTENT" 					: context,
						        "USER_ID" 					: userManager.getLoginUserId(),
						        "WS_ID" 					: workspaceManager.getMySpaceId(),
						        "MESSAGE_HEADER" 			: "",
						        "EXTRA_INFOS" 				: "",
						        "FIRST_SAW_TIME" 			: "", 
						        "SEND_IMPORTANCE_CODE" 		: importance,
						        "ACCOUNT_ID"				: accountId
						        
					    }
				};
					

					
					
				Top.Ajax.execute({
						
						type : "POST",
						url :_workspace.url + "Mail/Mail?action=CreateTemp",
						dataType : "json",
						data	 : JSON.stringify(data),
						contentType: "application/json; charset=utf-8",
						xhrFields: {withCredentials: true},
						crossDomain : true,
						success : function(ret,xhr,status) {
						
							if(ret.dto.MAIL_ID != null && ret.dto.GROUP_INDEX != null) {
								Top.Controller.get('mailWritePopupLogic').tempMailId = ret.dto.MAIL_ID;
								Top.Controller.get('mailWritePopupLogic').tempGroupIndex = ret.dto.GROUP_INDEX;
							}
							
							var time = mail.tempTime();
							
							if (Top.App.isWidgetAttached("mailWritePopupLayout_TempTime") == true){
								Top.Dom.selectById('mailWritePopupLayout_TempTime').setText('임시 보관함에 저장하였습니다. ' + time);
							}
							
							
						}
					});
					 
				 
			 },
			 
			 deleteAttach : function (event, widget) {
				 
				var k = widget.getParent().getSelectedIndex();
				
				if(Top.Controller.get('mailWritePopupLogic').fileAttach.length == 1){
					Top.Controller.get('mailWritePopupLogic').fileAttach = [];
					Top.Controller.get('mailWritePopupLogic').fileList = [];
					Top.Controller.get('mailWritePopupLogic').tempFileIndex = [];
					Top.Controller.get('mailWritePopupLogic').updateAttachmentInfo(); 

				}
				else{
					if(Top.Controller.get('mailWritePopupLogic').tempFileIndex[k] == "1"){
						for(var i = 0 ; i < Top.Controller.get("mailWritePopupLogic").tempTdrivefile.length;i++ ){
							if (Top.Controller.get("mailWritePopupLogic").tempTdrivefile[i].file_id == Top.Controller.get('mailWritePopupLogic').fileAttach[k].file_id ){
								Top.Controller.get("mailWritePopupLogic").tempTdrivefile.splice(i,1);
							}
						}
						
					}
					Top.Controller.get('mailWritePopupLogic').fileAttach.splice(k,1);
					Top.Controller.get('mailWritePopupLogic').fileList.splice(k,1);
					Top.Controller.get('mailWritePopupLogic').tempFileIndex.splice(k,1);
					Top.Controller.get('mailWritePopupLogic').updateAttachmentInfo(); 

				}
				
    			mailFileAttachmentRepo.setValue('mailFileAttachmentList',Top.Controller.get('mailWritePopupLogic').fileList);
				this.setEditorSize();
			 },
			 openTUser : function(event, widget){
				 if (widget.id == 'mailWritePopupLayout_Receiver_TUser')
					 Top.Controller.get('mailWritePopupLogic').clickedArea = 'Receiver';
				 else if (widget.id == 'mailWritePopupLayout_CC_TUser')
					 Top.Controller.get('mailWritePopupLogic').clickedArea = 'CC';
				 else if (widget.id == 'mailWritePopupLayout_HiddenCC_TUser')
					 Top.Controller.get('mailWritePopupLogic').clickedArea = 'HiddenCC'; 
				 
				 Top.Dom.selectById('mailTUserDialog').open(); 
			 },
			 autoCompleteChipItem : function(event, widget) {
				 
				 if(this.blured) {
					 widget.removeLastChip();
					 this.blured = false;
				 }
				 Top.Controller.get('mailWritePopupLogic').isAutoComplete = true;
				 
			 },
			 closeChipItem : function (event, widget, data){
				 
				 if (widget.id == 'Text_ReceiverAddress_Popup'){
					 Top.Controller.get("mailWritePopupLogic").recKey.pop(data.text);
				 }
				 else if  (widget.id == 'Text_Cc_Popup'){
					 Top.Controller.get("mailWritePopupLogic").ccKey.pop(data.text);
				 }
				 else if (widget.id == 'Text_HiddenCc_Popup'){
					 Top.Controller.get("mailWritePopupLogic").bccKey.pop(data.text);
				 }
				 this.adjustDialog();
			 },
			 setPrivateAccount : function(){
				 
				 var accountArr = [];
				 
				 if(routeManager.getApp() == 'mail') {
					 for(var i=0; i<mailData.accountList.length; i++) {
						 accountArr.push({
							 'text' : mailData.accountList[i].EMAIL_ADDRESS,
							 'value' : mailData.accountList[i].ACCOUNT_ID
						 });
					 }

					 Top.Dom.selectById('mailWritePopupLayout_Info_Sender_Account').setProperties({'nodes' : accountArr})
	 				 Top.Dom.selectById('mailWritePopupLayout_Info_Sender_Account').setProperties({'selectedIndex' : '0'})
					 
				 } else {
					 var data={
								dto: {
									"USER_ID"	: userManager.getLoginUserId()
								}
						};
					Top.Ajax.execute({
						
						type : "GET",
						url : _workspace.url + "Mail/Mail?action=GetAccountIdBySeq",
						data	 : JSON.stringify(data),
						dataType : "json",
						crossDomain: true,
						contentType: "application/json",
					    xhrFields: {withCredentials: true},
						success : function(ret,xhr,status) {
							
							for(var i=0; i<ret.dto.ACCOUNT_ID.length; i++) {
								accountArr.push({
									'text' : ret.dto.ACCOUNT_ID[i].EMAIL_ADDRESS,
									'value' : ret.dto.ACCOUNT_ID[i].ACCOUNT_ID
								});
								
							}
							
							Top.Dom.selectById('mailWritePopupLayout_Info_Sender_Account').setProperties({'nodes' : accountArr})
			 				Top.Dom.selectById('mailWritePopupLayout_Info_Sender_Account').setProperties({'selectedIndex' : '0'})
						}
					});
				 }
				 
				
				 
			 },
			 clickAttachFile : function(evnet, widget){
				 Top.Dom.selectById('mailAttachFileMenu').open(); 
				 
			 },
			 leaveField : function(event, widget){
				
				 
				   var value = { 
					        "text": event.currentTarget.value

					      }
				   if (event.currentTarget.id == "Text_ReceiverAddress_Popup"){
						Top.Controller.get('mailWritePopupLogic').receiverAddr.push(value);
				   }
				   else if (event.currentTarget.id == "Text_Cc_Popup"){
						Top.Controller.get('mailWritePopupLogic').CCAddr.push(value);
				   }
				   else if (event.currentTarget.id == "Text_HiddenCc_Popup"){
						Top.Controller.get('mailWritePopupLogic').BCCAddr.push(value);
				   }

					Top.Dom.selectById('Text_ReceiverAddress_Popup').setProperties({'chip-items' : Top.Controller.get('mailWritePopupLogic').receiverAddr});
					Top.Dom.selectById('Text_Cc_Popup').setProperties({'chip-items' : Top.Controller.get('mailWritePopupLogic').CCAddr});
					Top.Dom.selectById('Text_HiddenCc_Popup').setProperties({'chip-items' : Top.Controller.get('mailWritePopupLogic').BCCAddr});

			 },
			 foldBCC : function(){
				 
				 if (Top.Dom.selectById('mailWritePopupLayout_Info_BCC').getProperties('visible') == 'none')
					 Top.Dom.selectById('mailWritePopupLayout_Info_BCC').setVisible('visible');
				 else
					 Top.Dom.selectById('mailWritePopupLayout_Info_BCC').setVisible('none');
				 this.adjustDialog();
				 
			 },
			 updateAttachmentInfo : function(){
				 
				 	var normalCount=0;
					var normalSize=0;
					var bigCount=0;
					var bigSize=0;
					
					for (var i = 0; i < Top.Controller.get('mailWritePopupLogic').fileAttach.length; i++ )
						{
						if (Top.Controller.get('mailWritePopupLogic').tempFileIndex[i] == "0"){
							var size = Top.Controller.get('mailWritePopupLogic').fileAttach[i].file[0].size;
						}
						else
							var size = Top.Controller.get('mailWritePopupLogic').fileAttach[i].size; 
						
						if (size*1 < 10485760){
							normalCount = normalCount +1;
							normalSize = normalSize + (size*1);
							}
						else{
							bigCount = bigCount +1;
							bigSize = bigSize + (size*1);
						}		
						}
					
					normalSize = mail.changeSize(normalSize);
					bigSize = mail.changeSize(bigSize);
					
					var info = "일반 " + normalSize + " / 10MB ㅣ 대용량	 " + bigSize + " / 2.00GB x 10개" ;
					Top.Dom.selectById('mailWritePopupLayout_Attachment_Info').setText(info);
					
			 },
			 manageByte : function(event, widget){
				 
				 if (widget.getText() == undefined || widget.getText().length == 0)
					 return; 
				 
				 var calByte = {
							getByteLength : function(s) {

								if (s == null || s.length == 0) {
									return 0;
								}
								var size = 0;

								for ( var i = 0; i < s.length; i++) {
									size += this.charByteSize(s.charAt(i));
								}

								return size;
							},
								
							cutByteLength : function(s, len) {

								if (s == null || s.length == 0) {
									return 0;
								}
								var size = 0;
								var rIndex = s.length;

								for ( var i = 0; i < s.length; i++) {
									size += this.charByteSize(s.charAt(i));
									if( size == len ) {
										rIndex = i + 1;
										break;
									} else if( size > len ) {
										rIndex = i;
										break;
									}
								}

								return s.substring(0, rIndex);
							},

							charByteSize : function(ch) {

								if (ch == null || ch.length == 0) {
									return 0;
								}

								var charCode = ch.charCodeAt(0);

								if (charCode <= 0x00007F) {
									return 1;
								} else if (charCode <= 0x0007FF) {
									return 2;
								} else if (charCode <= 0x00FFFF) {
									return 3;
								} else {
									return 4;
								}
							}
						};
				 
				  var result = calByte.cutByteLength(widget.getText(), 250);
				  widget.setText(result);
			 }
	
	
});


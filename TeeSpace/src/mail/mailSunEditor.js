var mailSunEditor, mailSunEditor2, mailSunEditor3;

Top.Controller.create('TopApplicationLogic', {
    mailEditorInit: function () {
        mailSunEditor = SUNEDITOR.create("mail_suneditor", {
            fontSize: [8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36],
            font: [
                'Arial',
                'tahoma',
                'Courier New,Courier'
            ],
            width: '100%',
            height: 'auto',
            maxHeight: 'auto',
            videoWidth: 360,
            videoHeight: 115,
            youtubeQuery: 'autoplay=1&mute=1&enablejsapi=1',
            imageWidth: 150,
            stickyToolbar: false,
            popupDisplay: 'local',
            resizingBar: false,
            isFullScreen: false,
            buttonList: [
                ['undo', 'redo', 'font', 'fontSize', 'formatBlock',
                    'bold', 'underline', 'italic', 'strike', 'subscript', 'superscript',
                    'fontColor', 'hiliteColor',
                    'indent', 'outdent',
                    'align', 'horizontalRule', 'list', 'table', 'image'
                ]
            ],
            /*'image', 'video', 'fullScreen'*/
            /*'print', 'save'*/
            callBackSave: function (contents) {}
        });
        mailSunEditor.onScroll = function (e) {}
        mailSunEditor.onClick = function (e) {
            var editor_vertical = $('.sun-editor-container').height() - $('.sun-editor-id-toolbar').height() + 'px'
            $('.sun-editor-id-wysiwyg').css('max-height', editor_vertical)
        }
        mailSunEditor.onKeyDown = function (e) {}
        mailSunEditor.onKeyUp = function (e) {}
        mailSunEditor.onDrop = function (e) {}
        mailSunEditor.onImageUpload = function (targetImgElement, index, isDelete, imageInfo) {
            // file upload 서비스 콜
            if (isDelete == false) {
                if (targetImgElement.getAttribute('file_id') == null) {
                    let ImgRealData = imageInfo.src.split(',')[1];
                    let _successCallback = function (data) {
                        fileId = data.fileMetaList[0].file_id;
                        targetImgElement.setAttribute('file_id', fileId)
                        targetImgElement.setAttribute('img_Index', index)
//                        storageManager.DownloadFile(fileId, 'None', _DownLoadsuccessCallback, _DownLoaderrorCallback);
                    };
                    let _errorCallback = function (data) {
                        console.log(data.message);
                    };
                    let _DownLoadsuccessCallback = function (data) {
                        targetImgElement.parentElement.parentElement.outerHTML += '<p id="imgIndex' + index + '"><br></p>';
                    }
                    let _DownLoaderrorCallback = function (data) {
                        console.log(data)
                    }
                    // storageManager.uploadBinaryFile(imageInfo.name, ImgRealData, imageInfo.size , "/", _successCallback, _errorCallback)
                }
            }
            if (isDelete === true) {
                // file delete 서비스 콜
                let deleteIndex = targetImgElement.getAttribute('img_Index')
                let imgPtag = document.querySelector('#imgIndex' + deleteIndex);
                if ($(imgPtag).html() == "<br>") {
                    imgPtag.remove();
                } else if ($(imgPtag).html() != "<br>") {
                    imgPtag.removeAttribute('id')
                }
//                storageManager.deleteFile(fileId)
            }
        }
        mailSunEditor.onImageUploadError = function (errorMessage, result) {
            console.info(errorMessage)
        }
        
    }, mailEditorInit2 : function() {
    	
        mailSunEditor2 = SUNEDITOR.create("mail_suneditor2", {
            fontSize: [8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36],
            font: [
                'Arial',
                'tahoma',
                'Courier New,Courier'
            ],
            width: '100%',
            height: 'auto',
            maxHeight: 'auto',
            videoWidth: 360,
            videoHeight: 115,
            youtubeQuery: 'autoplay=1&mute=1&enablejsapi=1',
            imageWidth: 150,
            stickyToolbar: false,
            popupDisplay: 'local',
            resizingBar: false,
            isFullScreen: false,
            buttonList: [
                ['undo', 'redo', 'font', 'fontSize', 'formatBlock',
                    'bold', 'underline', 'italic', 'strike', 'subscript', 'superscript',
                    'fontColor', 'hiliteColor',
                    'indent', 'outdent',
                    'align', 'horizontalRule', 'list', 'table', 'image'
                ]
            ],
            /*'image', 'video', 'fullScreen'*/
            /*'print', 'save'*/
            callBackSave: function (contents) {}
        });
        mailSunEditor2.onScroll = function (e) {}
        mailSunEditor2.onClick = function (e) {
            var editor_vertical = $('.sun-editor-container').height() - $('.sun-editor-id-toolbar').height() + 'px'
            $('.sun-editor-id-wysiwyg').css('max-height', editor_vertical)
        }
        mailSunEditor2.onKeyDown = function (e) {}
        mailSunEditor2.onKeyUp = function (e) {}
        mailSunEditor2.onDrop = function (e) {}
        mailSunEditor2.onImageUpload = function (targetImgElement, index, isDelete, imageInfo) {
            // file upload 서비스 콜
            if (isDelete == false) {
                if (targetImgElement.getAttribute('file_id') == null) {
                    let ImgRealData = imageInfo.src.split(',')[1];
                    let _successCallback = function (data) {
                        fileId = data.fileMetaList[0].file_id;
                        targetImgElement.setAttribute('file_id', fileId)
                        targetImgElement.setAttribute('img_Index', index)
//                        storageManager.DownloadFile(fileId, 'None', _DownLoadsuccessCallback, _DownLoaderrorCallback);
                    };
                    let _errorCallback = function (data) {
                        console.log(data.message);
                    };
                    let _DownLoadsuccessCallback = function (data) {
                        targetImgElement.parentElement.parentElement.outerHTML += '<p id="imgIndex' + index + '"><br></p>';
                    }
                    let _DownLoaderrorCallback = function (data) {
                        console.log(data)
                    }
                    // storageManager.uploadBinaryFile(imageInfo.name, ImgRealData, imageInfo.size , "/", _successCallback, _errorCallback)
                }
            }
            if (isDelete === true) {
                // file delete 서비스 콜
                let deleteIndex = targetImgElement.getAttribute('img_Index')
                let imgPtag = document.querySelector('#imgIndex' + deleteIndex);
                if ($(imgPtag).html() == "<br>") {
                    imgPtag.remove();
                } else if ($(imgPtag).html() != "<br>") {
                    imgPtag.removeAttribute('id')
                }
//                storageManager.deleteFile(fileId)
            }
        }
        mailSunEditor2.onImageUploadError = function (errorMessage, result) {
            console.info(errorMessage)
        }
    }, mailEditorInit3 : function() {
        
        mailSunEditor3 = SUNEDITOR.create("mail_suneditor3", {
            fontSize: [8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36],
            font: [
                'Arial',
                'tahoma',
                'Courier New,Courier'
            ],
            width: '100%',
            height: 'auto',
            maxHeight: 'auto',
            videoWidth: 360,
            videoHeight: 115,
            youtubeQuery: 'autoplay=1&mute=1&enablejsapi=1',
            imageWidth: 150,
            stickyToolbar: false,
            popupDisplay: 'local',
            resizingBar: false,
            isFullScreen: false,
            buttonList: [
                ['undo', 'redo', 'font', 'fontSize', 'formatBlock',
                    'bold', 'underline', 'italic', 'strike', 'subscript', 'superscript',
                    'fontColor', 'hiliteColor',
                    'indent', 'outdent',
                    'align', 'horizontalRule', 'list', 'table', 'image'
                ]
            ],
            /*'image', 'video', 'fullScreen'*/
            /*'print', 'save'*/
            callBackSave: function (contents) {}
        });
        mailSunEditor3.onScroll = function (e) {}
        mailSunEditor3.onClick = function (e) {
            var editor_vertical = $('.sun-editor-container').height() - $('.sun-editor-id-toolbar').height() + 'px'
            $('.sun-editor-id-wysiwyg').css('max-height', editor_vertical)
        }
        mailSunEditor3.onKeyDown = function (e) {}
        mailSunEditor3.onKeyUp = function (e) {}
        mailSunEditor3.onDrop = function (e) {}
        mailSunEditor3.onImageUpload = function (targetImgElement, index, isDelete, imageInfo) {
            // file upload 서비스 콜
            if (isDelete == false) {
                if (targetImgElement.getAttribute('file_id') == null) {
                    let ImgRealData = imageInfo.src.split(',')[1];
                    let _successCallback = function (data) {
                        fileId = data.fileMetaList[0].file_id;
                        targetImgElement.setAttribute('file_id', fileId)
                        targetImgElement.setAttribute('img_Index', index)
//                        storageManager.DownloadFile(fileId, 'None', _DownLoadsuccessCallback, _DownLoaderrorCallback);
                    };
                    let _errorCallback = function (data) {
                        console.log(data.message);
                    };
                    let _DownLoadsuccessCallback = function (data) {
                        targetImgElement.parentElement.parentElement.outerHTML += '<p id="imgIndex' + index + '"><br></p>';
                    }
                    let _DownLoaderrorCallback = function (data) {
                        console.log(data)
                    }
                    // storageManager.uploadBinaryFile(imageInfo.name, ImgRealData, imageInfo.size , "/", _successCallback, _errorCallback)
                }
            }
            if (isDelete === true) {
                // file delete 서비스 콜
                let deleteIndex = targetImgElement.getAttribute('img_Index')
                let imgPtag = document.querySelector('#imgIndex' + deleteIndex);
                if ($(imgPtag).html() == "<br>") {
                    imgPtag.remove();
                } else if ($(imgPtag).html() != "<br>") {
                    imgPtag.removeAttribute('id')
                }
//                storageManager.deleteFile(fileId)
            }
        }
        mailSunEditor3.onImageUploadError = function (errorMessage, result) {
            console.info(errorMessage)
        }
    }
});
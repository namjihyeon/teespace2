
const MailDragDrop3 = {
    dragCounter : 0,
    
    onDragEnter : function(event) {
        MailDragDrop3.dragCounter++;
        if(event.dataTransfer.types[0] != 'Files') return;
        var mailDropZoneOverlay = Top.Dom.selectById("mailDropZoneOverlay_Popup2");
        if(mailDropZoneOverlay.getVisible() != "visible") {
            mailDropZoneOverlay.setVisible("visible");
        }
    },
    
    onDragLeave : function(event) {
        MailDragDrop3.dragCounter--;
        if(MailDragDrop3.dragCounter > 0) {
            return;
        }
        if(event.dataTransfer.types[0] != 'Files') return;
        var mailDropZoneOverlay = Top.Dom.selectById("mailDropZoneOverlay_Popup2");
        if(mailDropZoneOverlay.getVisible() == "visible") {
        	mailDropZoneOverlay.setVisible("none");
        }
    },
    
    onDrop : function(event) {
        event.stopPropagation();
        event.preventDefault();
        MailDragDrop3.dragCounter = 0;
        if(event.dataTransfer.types[0] != 'Files') return;
        var mailDropZoneOverlay = Top.Dom.selectById("mailDropZoneOverlay_Popup2");
        if(mailDropZoneOverlay.getVisible() == "visible") {
        	mailDropZoneOverlay.setVisible("none");
        }
        
        event.dataTransfer.file = event.dataTransfer.files;
        Top.Controller.get('mailWritePopupLogic').attachTable(event.dataTransfer);
    }
}

Top.Controller.create('mailWriteToMePopupLogic', {
	init : function(event, widget) {
		
		Top.Dom.selectById('mailDropZoneOverlay_Popup2').setProperties({'visible':'none'});
		
		var dropZone = document.querySelector("div#mailWriteToMePopupLayout");		
		dropZone.addEventListener('dragenter', MailDragDrop3.onDragEnter);		
		dropZone.addEventListener('dragleave', MailDragDrop3.onDragLeave);
		dropZone.addEventListener('drop'     , MailDragDrop3.onDrop);
		
		this.setAccount();
		this.setEditorSize();
		
		this.content=""; 
		mailFileAttachmentRepo.setValue('mailFileAttachmentList',[]);
	
		this.tdrivefile = [];
		this.fileListArr = [];
		this.tempTdrivefile = [];
		this.tempTFileId = [];
		
		this.fileAttach = [];
		this.file; 
		this.fileList = []
		this.fileUp = []; 
		this.tempFileIndex = [];
		this.isFromTDrive ;
		this.saveAtTDrive ;
		
		
		this.tempMailId;
		this.tempGroupIndex;
		this.tempInt = setInterval(function() {
			if(!mail.isWritingToMe()) {
				clearInterval(Top.Controller.get('mailWriteToMePopupLogic').tempInt);
				return;
			}
				
			Top.Controller.get('mailWriteToMePopupLogic').saveTemp();
			
		}, 30000);
		Top.Dom.selectById('mailWriteToMePopupLayout_Button_Preview_1').setProperties({'on-click':'saveTemp'});
		
		setTimeout(function(){$('#sun_editorEdit')[0].style.height = "100%";$('#sun_editorEdit')[0].style.maxHeight = '';});
		
		Top.Dom.selectById('mailWriteToMePopupLayout_Button_Save').setProperties({'on-click':'clickSave'});
		
		Top.Dom.selectById('mailWriteToMePopupLayout_AttachFile').setProperties({ "on-select": this.clickDropdownMenu});
		Top.Dom.selectById('mailWriteToMePopupLayout_AttachFile').close(); 
		
		if(Top.version.substr(8,2) > 87)
			Top.Dom.selectById('mailWriteToMePopupLayout_Subject').setProperties({'on-blur' : 'manageByte', 'on-keydown' : 'manageByte'});
		else
			Top.Dom.selectById('mailWriteToMePopupLayout_Subject').setProperties({'on-blur' : 'manageByte', 'on-keyup' : 'manageByte'});
		
		$('div#top-dialog-root_mailWriteToMeDialog .top-dialog-max_min_button').click(function(){Top.Controller.get('mailWriteToMePopupLogic').setEditorSize();});
		
	}, setEditorSize : function() {
		
		var upperHeight = $('top-linearlayout#mailWriteToMePopupLayout_Button')[0].offsetHeight + $('top-linearlayout#mailWriteToMePopupLayout_Info')[0].offsetHeight + $('top-linearlayout#mailWriteToMePopupLayout_FileBox')[0].offsetHeight;
		Top.Dom.selectById('mailWriteToMePopupLayout_WorkArea').setProperties({'layout-height':'calc(100% - ' + upperHeight + 'px)'});
		
		if($('div#top-dialog-root_mailWriteToMeDialog .top-dialog-max_min_button')[0].classList[1] == 'max')
			Top.Dom.selectById('mailWriteToMeDialog').setProperties({'layout-top':'','layout-left':'','layout-bottom': '15px', 'layout-right':'70px'});
		
	}, adjustDialog : function() {
		
		var plusHeight = 0;;
		
		if($('top-linearlayout#mailWriteToMePopupLayout_Info')[0].offsetHeight > 205)
			plusHeight += $('top-linearlayout#mailWriteToMePopupLayout_Info')[0].offsetHeight - 205;
		if(Top.Dom.selectById('mailWriteToMePopupLayout_FileBox').getProperties('visible') == 'visible')
			plusHeight += 154;
		
		plusHeight = plusHeight + 553 + 'px';
		
		Top.Dom.selectById('mailWriteToMeDialog').setProperties({'layout-top':'','layout-left':'','layout-bottom': '15px', 'layout-right':'70px', 'layout-height':plusHeight});
		this.setEditorSize();
		
	}, setAccount : function() {
		
		if(routeManager.getApp() == 'mail') {
			
			Top.Dom.selectById('mailWriteToMePopupLayout_Sender_WS').setText(mailData.accountList[0].EMAIL_ADDRESS);
			Top.Dom.selectById('mailWriteToMePopupLayout_Sender_WS').setProperties({'value':mailData.accountList[0].ACCOUNT_ID});
			
		} else {
			
			var data={
					dto: {
						"USER_ID"	: userManager.getLoginUserId()
					}
			};
			Top.Ajax.execute({
				
				type : "GET",
				url : _workspace.url + "Mail/Mail?action=GetAccountIdBySeq",
				data	 : JSON.stringify(data),
				dataType : "json",
				crossDomain: true,
				contentType: "application/json",
			    xhrFields: {withCredentials: true},
				success : function(ret,xhr,status) {
					Top.Dom.selectById('mailWriteToMePopupLayout_Sender_WS').setText(ret.dto.ACCOUNT_ID[0].EMAIL_ADDRESS);
					Top.Dom.selectById('mailWriteToMePopupLayout_Sender_WS').setProperties({'value':ret.dto.ACCOUNT_ID[0].ACCOUNT_ID});
				}
			});
		}
		
		
	}, clickDropdownMenu : function(event, widget){
		
		if(widget.getSelectedMenuId() == "menuitem0"){
			Top.Controller.get("mailWriteToMePopupLogic").saveAtTDrive = false;
			Top.Controller.get("mailWriteToMePopupLogic").isFromTDrive = false;
			Top.Controller.get("mailWriteToMePopupLogic").attachFile()
		}
		else if (widget.getSelectedMenuId() == "menuitem1"){
			Top.Controller.get("mailWriteToMePopupLogic").toTdrive = true; 
			Top.Controller.get("mailWriteToMePopupLogic").saveAtTDrive = false;
			Top.Controller.get("mailWriteToMePopupLogic").isFromTDrive = true;
			var config = {
					buttonText1: '첨부',
					buttonText2: '취소',
					buttonFn1: function(){ Top.Controller.get("mailWriteToMePopupLogic").attachFile() },
					}
		
			OPEN_TDRIVE(config);
			
		}
		else if (widget.getSelectedMenuId() == "menuitem2"){
			Top.Controller.get("mailWriteToMePopupLogic").saveAtTDrive = true;
			Top.Controller.get("mailWriteToMePopupLogic").isFromTDrive = false;
			Top.Controller.get("mailWriteToMePopupLogic").attachFile()
		}
		
		
	},
	
	clickSave : function(event, widget) {
		
		
		var mailSubject = Top.Dom.selectById('mailWriteToMePopupLayout_Subject').getText();
		if (!mailSubject){
			mailSubject = '제목 없음';
		}
		
		
		var ReceiverArr = [{ "TO_ADDR" : Top.Dom.selectById('mailWriteToMePopupLayout_Sender_WS').getText()}];
		
		var importance;
		if (Top.Dom.selectById('mailWriteToMePopupLayout_CheckBox_Importance').isChecked() == true)
			importance = 'IMP0001';
		else
			importance = 'IMP0002';
		

		var spaceremove = mailSunEditor3.getContents();
        spaceremove = [].filter.call(spaceremove, function (c) {return c.charCodeAt() !== 8203;}).join('');
        var context = spaceremove;
        context = '<style> table { width: 100%; border-spacing : 0px } td, th { border: 1px solid #e1e1e1 } </style>' + context;
        
        var mailAttachment = [] ; 
		var isAttach;
	
        function load_file(callback) {
			
			if (Top.Controller.get('mailWriteToMePopupLogic').fileAttach.length ==0)
    		{
    		isAttach = 'COM0002';
    		mailAttachment = [{
	            "ATTACHMENT_ID":"",
	            "MAIL_ID": "",
	            "STORAGE_PATH" : "", 
	            "USER_ID" : "",
	            "WS_ID" : "",
	            "EXTENSION" : "",
	            "FILE_NAME" : "",
	            "TDRIVE_CH_ID" : "", 
	            "FILE_ID" : "",
	            "FILE_SIZE" : ""
	            
	        }];
    		callback();
    		}
    	
			else{
				Top.Dom.selectById('mailWriteToMePopupLayout_Button_Save').setProperties({'disabled' : 'true'}); 
				Top.Loader.start(); 
				
	    		isAttach = 'COM0001'
	    			for (var i = 0; i < Top.Controller.get('mailWriteToMePopupLogic').fileAttach.length; i++){
	    				if(Top.Controller.get('mailWriteToMePopupLogic').tempFileIndex[i] == "0"){
	    					var l = Top.Controller.get('mailWriteToMePopupLogic').fileAttach[i].file[0].name.split('.').length;
	    					var file_extension = Top.Controller.get('mailWriteToMePopupLogic').fileAttach[i].file[0].name.split('.')[l-1];
	    					var file_name = Top.Controller.get('mailWriteToMePopupLogic').fileAttach[i].file[0].name.slice(0,-(file_extension.length + 1));
	    					var file_size = Top.Controller.get('mailWriteToMePopupLogic').fileAttach[i].file[0].size; 
	    					var inputDTO = {
	    		    				"dto": {
	    		    		               "workspace_id": workspaceManager.getMySpaceId(),
	    		    		                "channel_id": Top.Dom.selectById('mailWriteToMePopupLayout_Sender_WS').getProperties('value'),
	    		    		                "storageFileInfo": {
	    		    		                    "user_id": userManager.getLoginUserId(),
	    		    		                    "file_last_update_user_id": userManager.getLoginUserId(),
	    		    		                    "file_id": "",
	    		    		                    "file_name": file_name,
	    		    		                    "file_extension": file_extension,
	    		    		                    "file_created_at": "",
	    		    		                    "file_updated_at": "",
	    		    		                    "file_size": file_size,
	    		    		                    "user_context_1": "",
	    		    		                    "user_context_2": "",
	    		    		                    "user_context_3": ""
	    		    		                }
	    		    		           }
	    		    				
	    		    		}
	    					
	    					storageManager.UploadFile(Top.Controller.get('mailWriteToMePopupLogic').fileAttach[i].file[0],
		    						inputDTO, 'Calendar', "Calendar?action=TempStorages",  function(response){
								if (response.dto.resultMsg!= "Success"){
									Top.Dom.selectById('mailWriteToMePopupLayout_Button_Save').setProperties({'disabled' : 'false'});
									Top.Loader.stop();
									  return; 
								}
								var tmp = response.dto.storageFileInfoList[0];
								mailAttachment.push({
									"ATTACHMENT_ID":"",
		    			            "MAIL_ID": "",
		    			            "STORAGE_PATH" : "", // 수정필요
		    			            "USER_ID" : userManager.getLoginUserId(),
		    			            "WS_ID" : workspaceManager.getMySpaceId(),
		    			            "EXTENSION" : tmp.file_extension,
		    			            "FILE_NAME" : tmp.file_name,
		    			            "TDRIVE_CH_ID" : Top.Dom.selectById('mailWriteToMePopupLayout_Sender_WS').getProperties('value'), 
		    			            "FILE_ID" : tmp.file_id,
		    			            "FILE_SIZE" : tmp.file_size										
								});
								callback();
							}, function(){});
		    				
		    			}
	    				else {
	    					
	    					var file_name = Top.Controller.get('mailWriteToMePopupLogic').fileAttach[i].file_name;
	    					var file_extension = Top.Controller.get('mailWriteToMePopupLogic').fileAttach[i].file_extension;
	    					var file_size = Top.Controller.get('mailWriteToMePopupLogic').fileAttach[i].size;
	    					storageManager.CopyFile("Deep", Top.Controller.get('mailWriteToMePopupLogic').fileAttach[i].file_id,
	    							Top.Dom.selectById('mailWriteToMePopupLayout_Sender_WS').getProperties('value'),
	    							Top.Controller.get('mailWriteToMePopupLogic').fileAttach[i].file_name, Top.Controller.get('mailWriteToMePopupLogic').fileAttach[i].file_extension,
	    							"","","",
	    							function(response){
	    								var tmp = response.dto.storageFileInfoList[0];
	    								mailAttachment.push({
	    									"ATTACHMENT_ID":"",
	    		    			            "MAIL_ID": "",
	    		    			            "STORAGE_PATH" : "", // 수정필요
	    		    			            "USER_ID" : userManager.getLoginUserId(),
	    		    			            "WS_ID" : workspaceManager.getMySpaceId(),
	    		    			            "EXTENSION" : tmp.file_extension,
	    		    			            "FILE_NAME" : tmp.file_name,
	    		    			            "TDRIVE_CH_ID" : Top.Dom.selectById('mailWriteToMePopupLayout_Sender_WS').getProperties('value'), 
	    		    			            "FILE_ID" : tmp.file_id,
	    		    			            "FILE_SIZE" : tmp.file_size			 							
	    								});
	    								callback();
	    								
	    							}, function(){}
	    						)
	    				}
	    			}
	        	}
	    	
			}
    
        
        load_file(function(){
			if (Top.Controller.get('mailWriteToMePopupLogic').fileAttach.length !=0 &&Top.Controller.get('mailWriteToMePopupLogic').fileAttach.length != mailAttachment.length) return;
			else{
				var data={
						"dto":{
			    	"MAIL_FLAG" : "SND0001",
					"MailSendTo" : ReceiverArr,
			        "MailSendCc" : [{"TO_ADDR" : ""}],
			        "MailSendBcc" : [{"TO_ADDR" : ""}],
			        "DOMailAttachment":mailAttachment,
			        "FOLDER_ID":"",
			        "SUBJECT" : mailSubject,
			        "SENDER_NAME" : "",
			        "HAS_ATTACHMENT_YN_CODE" : isAttach,
			        "SEEN_YN_CODE" : "",
			        "IMPORTANCE_YN_CODE" : "",
			        "PINNED_YN_CODE" : "",
			        "FILE_NAME" : "",
			        "WRITING_MAIL_ID" : "",
			        "SYNC_STATE" : "",
			        "SOURCE_MAIL_ID" : "",
			        "FILE_SIZE" : "",
			        "DISPOSITION_NOTI" : "",
			        "MESSAGE_ID" : "",
			        "X_TNEF" : "",
			        "SPAM_YN_CODE" : "",
			        "SENDER_ADDR" : Top.Dom.selectById('mailWriteToMePopupLayout_Sender_WS').getText(),
			        "CONTENT" : context,
			        "CMFLG" : "SendMail_Re",
			        "USER_ID" : userManager.getLoginUserId(),
			        "WS_ID" : workspaceManager.getMySpaceId(),
			        "MESSAGE_HEADER" : "",
			        "EXTRA_INFOS" : "",
			        "FIRST_SAW_TIME" : "", 
			        "SEND_IMPORTANCE_CODE" : importance,
			        "MAIL_ID" : "",
			        "ACCOUNT_ID" : Top.Dom.selectById('mailWriteToMePopupLayout_Sender_WS').getProperties('value')
						}
				};

		
		
		
		Top.Ajax.execute({
			
			type : "POST",
			url :_workspace.url + "Mail/Mail?action=Send",
			dataType : "json",
			data	 : JSON.stringify(data),
			contentType: "application/json; charset=utf-8",
			xhrFields: {withCredentials: true},
			crossDomain : true,
			success : function(ret,xhr,status) {
				Top.Dom.selectById('mailWriteToMePopupLayout_Button_Save').setProperties({'disabled' : 'false'}); 
				 if (Top.Controller.get('mailWriteToMePopupLogic').fileAttach.length > 0)
						Top.Loader.stop();

				if (ret.dto != undefined 
						&&(ret.dto.RESULT_MSG == null||ret.dto.RESULT_MSG == '메일유저 없음'||ret.dto.RESULT_MSG == '외부메일입니다. ')) // 수정 요망
					{
					TeeAlarm.open({title: '보내기', content: '죄송합니다.<br>존재하지 않는 메일 주소 입니다.'});
					return ;
					}
				
				Top.Dom.selectById('mailWriteToMeDialog').close();
				Top.Dom.selectById('mailSaveSuccessDialog').open();
				if(routeManager.getApp() == 'mail')
					Top.Controller.get('mailMainLeftLogic').resetCount();
				clearInterval(Top.Controller.get('mailWriteToMePopupLogic').tempInt);
				
			},
			complete : function(ret,xhr,status) {				
			},
			error : function(ret, xhr,status) {
				Top.Dom.selectById('mailWriteToMePopupLayout_Button_Save').setProperties({'disabled' : 'false'}); 
				if (Top.Controller.get('mailWriteLogic').fileAttach.length > 0)
					Top.Loader.stop();

			}
			
		});		
		
			}});
		
	
	},
	attachFile : function (){
		
		if (Top.Controller.get("mailWriteToMePopupLogic").isFromTDrive == false){ 
			 let fileChooser = Top.Device.FileChooser.create({
				 onBeforeLoad : function(file){
					 
					 return true; 
				 },
				 charset : "euc-kr",
		            onFileChoose : function (fileChooser) {        
		            	Top.Controller.get('mailWriteToMePopupLogic').attachTable(fileChooser);
		           }
		        });
			 		
				fileChooser.show();
		}
		else{

			Top.Dom.selectById('mailWriteToMePopupLayout_FileBox').setVisible('visible');
			this.adjustDialog();
			
			Top.Controller.get('mailWriteToMePopupLogic').tempTFileId = [];

			var columnOpt = {
					"0" : {},
					"1"	: {},
					"4" : {
						layout: function(data, index) {
							return '<top-icon id="mailWriteToMePopupLayout_Attach_Delete_'+index+'_0" class="icon-close" text-color="#636363" text-size="11px" on-click="deleteAttach"></top-icon>';}
					}	
					
			};

			
			for(var i = 0 ; i < Top.Dom.selectById("fcFileContentsTableView").getCheckedData().length; i++){
				Top.Controller.get('mailWriteToMePopupLogic').tempTFileId.push(Top.Dom.selectById("fcFileContentsTableView").getCheckedData()[i].file_id)
			}
			
			for(var i = 0 ; i < Top.Dom.selectById("fcFileContentsTableView").getCheckedData().length; i++){
				this.putTdrivefileAttach(i, this.putTdriveTempfile);
			}
			
			if(Top.Dom.selectById("fcFileContentsTableView").getCheckedData().length != 0 ){
				fileManager.fileChannelId = Top.Dom.selectById('mailWriteToMePopupLayout_Sender_WS').getProperties('value');
				Top.Dom.selectById('mailWriteToMePopupLayout_FileTable').setProperties({"column-option": columnOpt});
    			mailFileAttachmentRepo.setValue('mailFileAttachmentList',Top.Controller.get('mailWriteToMePopupLogic').fileList);
			}
		}
			


	    }, attachTable : function(data) {
	    	
	    	Top.Controller.get('mailWriteToMePopupLogic').tempTFileId = [];
			
			var columnOpt = {
					"0" : {},
					"1"	: {},
					"4" : {
						layout: function(data, index) {
							return '<top-icon id="mailWriteToMePopupLayout_Attach_Delete_'+index+'_0" class="icon-close" text-color="#636363" text-size="11px" on-click="deleteAttach"></top-icon>';}
					}	
					
			};
			
        	Top.Controller.get('mailWriteToMePopupLogic').fileAttach.push(data);
        	
			var l = Top.Controller.get('mailWriteToMePopupLogic').fileAttach.length;
			
			var size = Top.Controller.get('mailWriteToMePopupLogic').fileAttach[l-1].file[0].size;
			if ((size*1) < 10485760)
				var method = "일반 첨부" ;
			else {
				var method = "대용량 첨부";
				TeeAlarm.open({title: '', content: '10MB 이하의 파일만 첨부 가능합니다.'});
				Top.Controller.get('mailWriteToMePopupLogic').fileAttach.pop();
				return;
			}

	    	Top.Dom.selectById('mailWriteToMePopupLayout_FileBox').setVisible('visible');
			this.adjustDialog();
			
			if ( method == "대용량 첨부" )
				var period = "~ "+mail.after30Days(null,'/')+" (30일간)";
			else
				var period = "제한 없음";

			size = mail.changeSize(size);
			
			Top.Controller.get('mailWriteToMePopupLogic').fileList.push(
					{
						"name" : Top.Controller.get('mailWriteToMePopupLogic').fileAttach[l-1].file[0].name,
						"size" : size,
						"attach_method" : method,
						"period" : period
					}
			);
			
			if(Top.Controller.get("mailWriteToMePopupLogic").saveAtTDrive == false || Top.Controller.get("mailWriteToMePopupLogic").saveAtTDrive == undefined){
				Top.Controller.get('mailWriteToMePopupLogic').tempFileIndex.push("0");
			}
			else{
				Top.Controller.get('mailWriteToMePopupLogic').tempFileIndex.push("2");
			}
        	Top.Controller.get('mailWriteToMePopupLogic').updateAttachmentInfo(); 

			
			Top.Dom.selectById('mailWriteToMePopupLayout_FileTable').setProperties({"column-option": columnOpt});
			mailFileAttachmentRepo.setValue('mailFileAttachmentList',Top.Controller.get('mailWriteToMePopupLogic').fileList);

	    },
	    putTdrivefileAttach : function(index, callbackFunc) {
			Top.Controller.get('mailWriteToMePopupLogic').tempFileIndex.push("1");
			
			
			Top.Controller.get('mailWriteToMePopupLogic').fileAttach.push(
					{
						"file_name" : Top.Dom.selectById("fcFileContentsTableView").getCheckedData()[index].file_name,
						"file_id" : Top.Dom.selectById("fcFileContentsTableView").getCheckedData()[index].file_id,
						"size" : Top.Dom.selectById("fcFileContentsTableView").getCheckedData()[index].file_size,
						"file_extension" : Top.Dom.selectById("fcFileContentsTableView").getCheckedData()[index].file_extension
					}
			);
			
			Top.Controller.get('mailWriteToMePopupLogic').updateAttachmentInfo(); 

			
			var size;
			size = Top.Dom.selectById("fcFileContentsTableView").getCheckedData()[index].file_size;
			if ((size*1) < 10485760)
				var method = "일반 첨부" ;
			else{
				var method = "대용량 첨부";
				TeeAlarm.open({title: '', content: '10MB 이하의 파일만 첨부 가능합니다.'});
				Top.Controller.get('mailWriteToMePopupLogic').fileAttach.pop();
				Top.Controller.get('mailWriteToMePopupLogic').tempFileIndex.pop(); 
				return;

			}

			if ( method == "대용량 첨부" )
				var period = "~ "+mail.after30Days(null,'/')+" (30일간)";
			else
				var period = "제한 없음";

			size = mail.changeSize(size);
			
			Top.Controller.get('mailWriteToMePopupLogic').fileList.push(
					{
						"name" : Top.Dom.selectById("fcFileContentsTableView").getCheckedData()[index].fileFullName,
						"size" : size,
						"attach_method" : method,
						"period" : period
					}
			);
			
			callbackFunc(index);
    },
    
    putTdriveTempfile : function(index) {
    	storageManager.getDownloadFileInfo(Top.Controller.get('mailWriteToMePopupLogic').tempTFileId[index], function(result){
		 	var reader = new window.FileReader();
			reader.readAsDataURL(result.blobData); 
			reader.onloadend = function() {
				 var filecontent = reader.result; 
                 var contents;
                 if(Object.prototype.toString.call(filecontent) === "[object ArrayBuffer]"){ //ArrayBuffer
                     var binary = '';
                    var bytes = new Uint8Array( filecontent );
                    var len = bytes.byteLength;
                    for (var j = 0; j < len; j++) {
                        binary += String.fromCharCode( bytes[ j ] );
                    }
                    contents = window.btoa(binary);
                 }
                 else if((filecontent.split("base64,").length == 2) &&
                     RegExp("^(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{4})$").test(filecontent.split("base64,")[1])){ //base64
                         contents = filecontent.split("base64,")[1];
                 }
                 else{ //raw txt
                     contents = btoa(unescape(encodeURIComponent(ctrl.fileAttach[i].src)));
                 }
                 Top.Controller.get("mailWriteToMePopupLogic").tempTdrivefile.push( {"file_id" : Top.Controller.get('mailWriteToMePopupLogic').tempTFileId[index], "file_info" : {
                     "filename" : result.fileName,
                     "contents" : contents
                 			}
				 })
			}
		},function(){})	
    },
    saveTemp : function() {
			 
	    	var mailSubject = Top.Dom.selectById('mailWriteToMePopupLayout_Subject').getText();
			if (!mailSubject){
				mailSubject = '제목 없음';
			}
				
			var ReceiverArr = [{ "TO_ADDR" : Top.Dom.selectById('mailWriteToMePopupLayout_Sender_WS').getText()}];
			
			
			var importance;
			if (Top.Dom.selectById('mailWriteToMePopupLayout_CheckBox_Importance').isChecked() == true)
				importance = 'IMP0001';
			else
				importance = 'IMP0002';
			
			
			var spaceremove = mailSunEditor3.getContents();
	        spaceremove = [].filter.call(spaceremove, function (c) {return c.charCodeAt() !== 8203;}).join('');
	        var context = spaceremove;
	        context = '<style> table { width: 100%; border-spacing : 0px } td, th { border: 1px solid #e1e1e1 } </style>' + context;
			var data={
					"dto":{
							"MailSendTo" 				: ReceiverArr,
							"MailSendCc" 				: [{"TO_ADDR" : ""}],
						    "MailSendBcc" 				: [{"TO_ADDR" : ""}],
					        "ACCOUNT_ID"				: Top.Dom.selectById('mailWriteToMePopupLayout_Sender_WS').getProperties('value'),
					        "MAIL_ID"					: this.tempMailId,
					        "MAIL_GROUP_INDEX"			: this.tempGroupIndex,
					        "CMFLG" 					: "CreateTempMail",
					        "FOLDER_ID"					: "",
					        "SUBJECT" 					: mailSubject,
					        "SENDER_NAME" 				: "",
					        "HAS_ATTACHMENT_YN_CODE" 	: 'COM0002',
					        "SEEN_YN_CODE" 				: "",
					        "IMPORTANCE_YN_CODE" 		: "",
					        "PINNED_YN_CODE" 			: "",
					        "FILE_NAME" 				: "",
					        "WRITING_MAIL_ID" 			: "",
					        "SYNC_STATE" 				: "",
					        "SOURCE_MAIL_ID" 			: "",
					        "FILE_SIZE" 				: "",
					        "DISPOSITION_NOTI" 			: "",
					        "MESSAGE_ID" 				: "",
					        "X_TNEF" 					: "",
					        "SPAM_YN_CODE" 				: "",
					        "SENDER_ADDR" 				: Top.Dom.selectById('mailWriteToMePopupLayout_Sender_WS').getText(),
					        "CONTENT" 					: context,
					        "USER_ID" 					: userManager.getLoginUserId(),
					        "WS_ID" 					: workspaceManager.getMySpaceId(),
					        "MESSAGE_HEADER" 			: "",
					        "EXTRA_INFOS" 				: "",
					        "FIRST_SAW_TIME" 			: "", 
					        "SEND_IMPORTANCE_CODE" 		: importance
					        
				    }
			};
				
			

				
				
			Top.Ajax.execute({
					
					type : "POST",
					url :_workspace.url + "Mail/Mail?action=CreateTemp",
					dataType : "json",
					data	 : JSON.stringify(data),
					contentType: "application/json; charset=utf-8",
					xhrFields: {withCredentials: true},
					crossDomain : true,
					success : function(ret,xhr,status) {
					
						if(ret.dto.MAIL_ID != null && ret.dto.GROUP_INDEX != null) {
							Top.Controller.get('mailWriteToMePopupLogic').tempMailId = ret.dto.MAIL_ID;
							Top.Controller.get('mailWriteToMePopupLogic').tempGroupIndex = ret.dto.GROUP_INDEX;
						}
						Top.Dom.selectById('mailWriteToMePopupLayout_TempTime').setText('임시 보관함에 저장하였습니다. ' + mail.tempTime());
						
					},
					complete : function(ret,xhr,status) {
					

						
					},
					error : function(ret, xhr,status) {

					}
					
				});
				 
			 
		 },
		 
		 deleteAttach : function (event, widget) {
			 
				var k = widget.getParent().getSelectedIndex();;
				
				if(Top.Controller.get('mailWriteToMePopupLogic').fileAttach.length == 1){
					Top.Controller.get('mailWriteToMePopupLogic').fileAttach = [];
					Top.Controller.get('mailWriteToMePopupLogic').fileList = [];
					Top.Controller.get('mailWriteToMePopupLogic').tempFileIndex = [];
					Top.Controller.get('mailWriteToMePopupLogic').updateAttachmentInfo(); 

				}
				else{
					if(Top.Controller.get('mailWriteToMePopupLogic').tempFileIndex[k] == "1"){
						for(var i = 0 ; i < Top.Controller.get("mailWriteToMePopupLogic").tempTdrivefile.length;i++ ){
							if (Top.Controller.get("mailWriteToMePopupLogic").tempTdrivefile[i].file_id == Top.Controller.get('mailWriteToMePopupLogic').fileAttach[k].file_id ){
								Top.Controller.get("mailWriteToMePopupLogic").tempTdrivefile.splice(i,1);
							}
						}
						
					}
					Top.Controller.get('mailWriteToMePopupLogic').fileAttach.splice(k,1);
					Top.Controller.get('mailWriteToMePopupLogic').fileList.splice(k,1);
					Top.Controller.get('mailWriteToMePopupLogic').tempFileIndex.splice(k,1);
					Top.Controller.get('mailWriteToMePopupLogic').updateAttachmentInfo(); 

				}
				
 			mailFileAttachmentRepo.setValue('mailFileAttachmentList',Top.Controller.get('mailWriteToMePopupLogic').fileList);
 			this.setEditorSize();
		},
		 clickAttachFile : function(evnet, widget){
			 Top.Dom.selectById('mailAttachFileMenu').open(); 
			 
		 },
		 updateAttachmentInfo : function(){
			 
			 	var normalCount=0;
				var normalSize=0;
				var bigCount=0;
				var bigSize=0;
				
				for (var i = 0; i < Top.Controller.get('mailWriteToMePopupLogic').fileAttach.length; i++ )
					{
					if (Top.Controller.get('mailWriteToMePopupLogic').tempFileIndex[i] == "0"){
						var size = Top.Controller.get('mailWriteToMePopupLogic').fileAttach[i].file[0].size;
					}
					else
						var size = Top.Controller.get('mailWriteToMePopupLogic').fileAttach[i].size; 
					
					if (size*1 < 10485760){
						normalCount = normalCount +1;
						normalSize = normalSize + (size*1);
						}
					else{
						bigCount = bigCount +1;
						bigSize = bigSize + (size*1);
					}		
					}
				
				normalSize = mail.changeSize(normalSize);
				bigSize = mail.changeSize(bigSize);
				
				var info = "일반 " + normalSize + " / 10MB ㅣ 대용량	 " + bigSize + " / 2.00GB x 10개" ;
				Top.Dom.selectById('mailWriteToMePopupLayout_Attachment_Info').setText(info);
				
		 },
		 manageByte : function(event, widget){
			 
			 if (widget.getText() == undefined || widget.getText().length == 0)
				 return; 
			 
			 var calByte = {
						getByteLength : function(s) {

							if (s == null || s.length == 0) {
								return 0;
							}
							var size = 0;

							for ( var i = 0; i < s.length; i++) {
								size += this.charByteSize(s.charAt(i));
							}

							return size;
						},
							
						cutByteLength : function(s, len) {

							if (s == null || s.length == 0) {
								return 0;
							}
							var size = 0;
							var rIndex = s.length;

							for ( var i = 0; i < s.length; i++) {
								size += this.charByteSize(s.charAt(i));
								if( size == len ) {
									rIndex = i + 1;
									break;
								} else if( size > len ) {
									rIndex = i;
									break;
								}
							}

							return s.substring(0, rIndex);
						},

						charByteSize : function(ch) {

							if (ch == null || ch.length == 0) {
								return 0;
							}

							var charCode = ch.charCodeAt(0);

							if (charCode <= 0x00007F) {
								return 1;
							} else if (charCode <= 0x0007FF) {
								return 2;
							} else if (charCode <= 0x00FFFF) {
								return 3;
							} else {
								return 4;
							}
						}
					};
			 
			  var result = calByte.cutByteLength(widget.getText(), 250);
			  widget.setText(result);
		 }
		
})


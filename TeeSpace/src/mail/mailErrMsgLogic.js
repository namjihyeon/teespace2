Top.Controller.create('mailErrMsgLogic', {
	
	init : function() {
	
		this.setText();
	
	}, setText : function() {
		
		errMsg = Top.Controller.get('TeeSpaceLogic').error;
		
		if(errMsg == 'dup')
			Top.Dom.selectById('mailErrMsg_Text').setText('이미 존재하는 편지함 이름입니다.');
		else if(errMsg == 'empty')
			Top.Dom.selectById('mailErrMsg_Text').setText(Top.i18n.get().value.m000151);
		else if(errMsg == 'slash')
			Top.Dom.selectById('mailErrMsg_Text').setText(Top.i18n.get().value.m000150);
	}

});
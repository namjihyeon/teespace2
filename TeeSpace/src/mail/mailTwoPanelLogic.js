Top.Controller.create('mailTwoPanelLogic', {
	
	init : function() {
		Top.Dom.selectById('mailTwoPanelLayout_Table').setProperties({'visible':'hidden'})
		Top.Dom.selectById('mailTwoPanelLayout_Table').onRender(function(){Top.Dom.selectById('mailTwoPanelLayout_Table').setProperties({'visible':'visible'})});
		
		var media = window.matchMedia("screen and (max-device-width: 1024px)");
		
		this.setMoveFolder();
		this.setFilterSelectbox();
		Top.Dom.selectById('mailTwoPanelLayout_Table').setProperties({'on-rowclick':'readMail'});
		Top.Dom.selectById('mailTwoPanelLayout_Table').setProperties({'on-rowcheck':'buttonStatusChange'});
		
		Top.Dom.selectById('mailTwoPanelLayout_Pagination').setProperties({'on-pagechange':'changePage'});
		Top.Dom.selectById('mailTwoPanelLayout_PageLength').setProperties({'on-change':'changePageLength'});
				
		this.initButton();
		if(Top.Controller.get('mailMainLeftLogic').searchKeyword !== null){
			var id= ['Receive','Sent','Temp','SendMe','Spam','Trash'];
			for(var i=0; i<id.length; i++){
			    if(Top.Dom.selectById('mailTwoPanelLayout_'+id[i]+'_Buttons').getProperties('visible') == 'visible'){
					Top.Dom.selectById('mailTwoPanelLayout_'+id[i]+'_Search_Text').setText(Top.Controller.get('mailMainLeftLogic').searchKeyword.SUBJECT);
					Top.Dom.selectById('mailTwoPanelLayout_'+id[i]+'_SelectBox_Filter').setProperties({'selectedIndex':Top.Controller.get('mailMainLeftLogic').searchKeyword.filterIndex})
			    }
			}
			this.searchMailList(Top.Controller.get('mailMainLeftLogic').searchKeyword);
			Top.Controller.get('mailMainLeftLogic').isChangePanel = null;
		}else
			this.callMailList();
		
		if (Top.Controller.get('mailWriteLogic').searchSubject != null){
			Top.Controller.get('mailMainLeftLogic').openSearch();
			Top.Dom.selectById('mailTwoPanelLayout_Header_Search').setText(Top.Controller.get('mailWriteLogic').searchSubject);
			Top.Controller.get('mailMainLeftLogic').searchMail();
			Top.Controller.get('mailWriteLogic').searchSubject = null;
		}
		
		if (Top.Controller.get('mailReadButtonLogic').searchSubject != null){
			Top.Controller.get('mailMainLeftLogic').openSearch();
			Top.Dom.selectById('mailTwoPanelLayout_Header_Search').setText(Top.Controller.get('mailReadButtonLogic').searchSubject);
			Top.Controller.get('mailMainLeftLogic').searchMail();
			Top.Controller.get('mailReadButtonLogic').searchSubject = null;
		}
		
	},	
	setAttrDragAndDrop : function(){
		var eleRemove = $("#mailTwoPanelLayout_Table .top-tableview .body tr");
		for(var i=0; i<eleRemove.length; i++){
			eleRemove[i].classList.remove("dnd__item");
		}
		
		if($("#mailTwoPanelLayout_Table .top-tableview .body tr").length > 0){
			var element = $("#mailTwoPanelLayout_Table .top-tableview .body tr")[0].classList;
			for(var i=0; i<element.length; i++){
				if(element[i] == "dnd__item")
				{
					clearInterval(mailData.intervalTwo);
					break;
				}
			}
			const row = $("#mailTwoPanelLayout_Table .top-tableview .body tr");
			for(let i=0; i<row.length; i++){
				row[i].addClass("dnd__item"); 
				row[i].setAttribute("data-dnd-app", "mail");
				row[i].setAttribute("mail-container-type", "two");
			};
//			$("div#mailTwoPanelLayout_Mails").addClass("dnd__container").attr("data-dnd-app", "mail");
			$("div#mailTwoPanelLayout_Table").addClass("dnd__container").attr("data-dnd-app", "mail")
			
			dnd.update();
			for(var i=0; i<element.length; i++){
				if(element[i] == "dnd__item")
				{
					clearInterval(mailData.intervalTwo);
					break;
				}
			}
		}
	},
	getEnter : function(event) {
		
		if(event.key == "Enter") {
			this.searchMail();
		}
		
	}, searchMail : function() {
	
		mailData.curPage = 1;

		if(mailData.panelType == '3')
			Top.Controller.get('mailThreePanelMainLogic').searchMail();
		else
			Top.Controller.get('mailTwoPanelLogic').searchMailList();
			
		
	}, getSeeingTypeCode : function(TypeText){
			if(TypeText == "전체")
				return "ALL";
			else if(TypeText == "보낸 사람")
				return "SENDER";
			else if(TypeText == "제목")
				return "SUBJECT";
			else if(TypeText == "첨부 파일명")
				return "ATT";
			else
				return "ALL";
			
		
	}, getMailSearchKeyword :function(){
		var map = {};
		var seeingTypeCode;
		var filterName;
		var SUBJECT;
		var id= ['Receive','Sent','Temp','SendMe','Spam','Trash'];
		
		for(var i=0; i<6; i++){
		    console.log(Top.Dom.selectById('mailTwoPanelLayout_'+id[i]+'_Buttons').getProperties('visible'));
		    
		    if(Top.Dom.selectById('mailTwoPanelLayout_'+id[i]+'_Buttons').getProperties('visible') == 'visible'){
		    	if( Top.Dom.selectById('mailTwoPanelLayout_'+id[i]+'_SelectBox_Filter').getProperties('selectedIndex') == undefined )
					seeingTypeCode = "ALL";
				else{
					seeingTypeCode = this.getSeeingTypeCode(Top.Dom.selectById('mailTwoPanelLayout_'+id[i]+'_SelectBox_Filter').getProperties('selectedText'));
					filterName = Top.Dom.selectById('mailTwoPanelLayout_'+id[i]+'_SelectBox_Filter').getProperties('selectedText');
					filterIndex = Top.Dom.selectById('mailTwoPanelLayout_'+id[i]+'_SelectBox_Filter').getProperties('selectedIndex');
				}
				SUBJECT = Top.Dom.selectById('mailTwoPanelLayout_'+id[i]+'_Search_Text').getText();
				
				map = {'seeingTypeCode':seeingTypeCode, 'SUBJECT' : SUBJECT, 'filterName' : filterName, 'filterIndex' : filterIndex};
				return map;
		    }
		}
	},
	
	onChange_CheckBox : function(event, widget){ 
		var id = widget.id.split('_');
		var index = id[id.length-2]
		var checkBoxId = 'mailTwoPanelLayout_ColumnItem_CheckBox_' + index + '_' + id[id.length-1];
		var imageId = 'ImageButton201_' + index + '_' + id[id.length-1];
		
		if(Top.Dom.selectById(checkBoxId).getChecked() )
		{
			Top.Dom.selectById(checkBoxId).setProperties({'visible':'visible'});
			Top.Dom.selectById(imageId).setProperties({'visible':'none'});
			mailDataRepo.mailReceivedBoxDataList[Top.Dom.selectById('mailTwoPanelLayout_Table').getStartRowNum()+Number(index)].CHECKBOX_VISIBLE = "visible";
			mailDataRepo.mailReceivedBoxDataList[Top.Dom.selectById('mailTwoPanelLayout_Table').getStartRowNum()+Number(index)].PROFILE_VISIBLE = "none";
		}else{
			Top.Dom.selectById(checkBoxId).setProperties({'visible':'none'});
			Top.Dom.selectById(imageId).setProperties({'visible':'visible'});
			mailDataRepo.mailReceivedBoxDataList[Top.Dom.selectById('mailTwoPanelLayout_Table').getStartRowNum()+Number(index)].CHECKBOX_VISIBLE = "none";
			mailDataRepo.mailReceivedBoxDataList[Top.Dom.selectById('mailTwoPanelLayout_Table').getStartRowNum()+Number(index)].PROFILE_VISIBLE = "visible";
		}
		
	},mouseExit_CheckBox : function(event, widget){
		var id = widget.id.split('_');
		var index = id[id.length-2]
		var checkBoxId = 'mailTwoPanelLayout_ColumnItem_CheckBox_' + index + '_' + id[id.length-1];
		var imageId = 'ImageButton201_' + index + '_' + id[id.length-1];
		
		if(Top.Dom.selectById(checkBoxId).getChecked() )
		{
			Top.Dom.selectById(checkBoxId).setProperties({'visible':'visible'});
			Top.Dom.selectById(imageId).setProperties({'visible':'none'});
			mailDataRepo.mailReceivedBoxDataList[Top.Dom.selectById('mailTwoPanelLayout_Table').getStartRowNum()+Number(index)].CHECKBOX_VISIBLE = "visible";
			mailDataRepo.mailReceivedBoxDataList[Top.Dom.selectById('mailTwoPanelLayout_Table').getStartRowNum()+Number(index)].PROFILE_VISIBLE = "none";
		}else{
			Top.Dom.selectById(checkBoxId).setProperties({'visible':'none'});
			Top.Dom.selectById(imageId).setProperties({'visible':'visible'});
			mailDataRepo.mailReceivedBoxDataList[Top.Dom.selectById('mailTwoPanelLayout_Table').getStartRowNum()+Number(index)].CHECKBOX_VISIBLE = "none";
			mailDataRepo.mailReceivedBoxDataList[Top.Dom.selectById('mailTwoPanelLayout_Table').getStartRowNum()+Number(index)].PROFILE_VISIBLE = "visible";
		}
	
		
	},
	mouseOver_ProfileImage : function(event, widget){
		var id = widget.id.split('_');
		var index = id[id.length-2]
		var checkBoxId = 'mailTwoPanelLayout_ColumnItem_CheckBox_' + index + '_' + id[id.length-1];
		var imageId = 'ImageButton201_' + index + '_' + id[id.length-1];
		
		Top.Dom.selectById(checkBoxId).setProperties({'visible':'visible'});
		Top.Dom.selectById(imageId).setProperties({'visible':'none'});
		mailDataRepo.mailReceivedBoxDataList[Top.Dom.selectById('mailTwoPanelLayout_Table').getStartRowNum()+Number(index)].CHECKBOX_VISIBLE = "visible";
		mailDataRepo.mailReceivedBoxDataList[Top.Dom.selectById('mailTwoPanelLayout_Table').getStartRowNum()+Number(index)].PROFILE_VISIBLE = "none";
		
	},	changePage : function(event, widget) {
		
		mailData.curPage = widget.getSelectedPage();
		Top.Controller.get('mailTwoPanelLogic').setCheckAll(false);
		
		if(Top.Controller.get('mailMainLeftLogic').searchKeyword !== null)
			this.searchMailList(); 
		else
			this.callMailList();
		
	}, changePageLength : function(event, widget) {
		
		mailData.pageLength = widget.getSelectedText();
		Top.Controller.get('mailTwoPanelLogic').setCheckAll(false);
		if(Top.Controller.get('mailMainLeftLogic').searchKeyword !== null)
			this.searchMailList(); 
		else
			this.callMailList();		
	}, replyMail : function() {
		
		if(mailData.routeInfo.folderId == undefined) mailData.routeInfo.folderId = '0';
		
		let sub = appManager.getSubApp();
		if(sub) 
			Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + mailData.routeInfo.folderId + '/' + Top.Dom.selectById('mailTwoPanelLayout_Table').getCheckedData()[0].MAIL_ID + '/reply' + "?sub=" + sub, {eventType:'fold'});
		else 
			Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + mailData.routeInfo.folderId + '/' + Top.Dom.selectById('mailTwoPanelLayout_Table').getCheckedData()[0].MAIL_ID + '/reply' , {eventType:'close'});
	}, reply : function() {
		
		Top.Controller.get('mailMainLeftLogic').clickedMail = Top.Dom.selectById('mailTwoPanelLayout_Table').getCheckedData()[0].MAIL_ID;
		Top.Controller.get('mailReadButtonLogic').reply();
		
	}, replyAllMail : function() {
		
		if(mailData.routeInfo.folderId == undefined) mailData.routeInfo.folderId = '0';
		
		let sub = appManager.getSubApp();
		if(sub) 
			Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + mailData.routeInfo.folderId + '/' + Top.Dom.selectById('mailTwoPanelLayout_Table').getCheckedData()[0].MAIL_ID + '/replyall' + "?sub=" + sub, {eventType:'fold'});
		else 
			Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + mailData.routeInfo.folderId + '/' + Top.Dom.selectById('mailTwoPanelLayout_Table').getCheckedData()[0].MAIL_ID + '/replyall' , {eventType:'close'});
	}, replyall : function() {
		
		Top.Controller.get('mailMainLeftLogic').clickedMail = Top.Dom.selectById('mailTwoPanelLayout_Table').getCheckedData()[0].MAIL_ID;
		Top.Controller.get('mailReadButtonLogic').replyall();
		
	}, modifyMail : function() {
		
		if(mailData.routeInfo.folderId == undefined) mailData.routeInfo.folderId = '0';
		
		let sub = appManager.getSubApp();
		if(sub) 
			Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + mailData.routeInfo.folderId + '/' + Top.Dom.selectById('mailTwoPanelLayout_Table').getCheckedData()[0].MAIL_ID + '/modify' + "?sub=" + sub, {eventType:'fold'});
		else 
			Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + mailData.routeInfo.folderId + '/' + Top.Dom.selectById('mailTwoPanelLayout_Table').getCheckedData()[0].MAIL_ID + '/modify' , {eventType:'close'});
	}, modify : function() {
		
		Top.Controller.get('mailMainLeftLogic').clickedMail = Top.Dom.selectById('mailTwoPanelLayout_Table').getCheckedData()[0].MAIL_ID;
		Top.Controller.get('mailReadButtonLogic').modify();
		
	}, resendMail : function() {
		
		if(mailData.routeInfo.folderId == undefined) mailData.routeInfo.folderId = '0';
		
		let sub = appManager.getSubApp();
		if(sub) 
			Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + mailData.routeInfo.folderId + '/' + Top.Dom.selectById('mailTwoPanelLayout_Table').getCheckedData()[0].MAIL_ID + '/resend' + "?sub=" + sub, {eventType:'fold'});
		else 
			Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + mailData.routeInfo.folderId + '/' + Top.Dom.selectById('mailTwoPanelLayout_Table').getCheckedData()[0].MAIL_ID + '/resend' , {eventType:'close'});
		
	}, resend : function() {
		
		Top.Controller.get('mailMainLeftLogic').clickedMail = Top.Dom.selectById('mailTwoPanelLayout_Table').getCheckedData()[0].MAIL_ID;
		Top.Controller.get('mailReadButtonLogic').resend();
		
	}, forwardMail : function() {
		
		if(mailData.routeInfo.folderId == undefined) mailData.routeInfo.folderId = '0';
		
		let sub = appManager.getSubApp();
		if(sub) 
			Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + mailData.routeInfo.folderId + '/' + Top.Dom.selectById('mailTwoPanelLayout_Table').getCheckedData()[0].MAIL_ID + '/forward' + "?sub=" + sub, {eventType:'fold'});
		else 
			Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + mailData.routeInfo.folderId + '/' + Top.Dom.selectById('mailTwoPanelLayout_Table').getCheckedData()[0].MAIL_ID + '/forward' , {eventType:'close'});
				
	}, forward : function() {
		
		Top.Controller.get('mailMainLeftLogic').clickedMail = Top.Dom.selectById('mailTwoPanelLayout_Table').getCheckedData()[0].MAIL_ID;
		Top.Controller.get('mailReadButtonLogic').forward();

	}, searchMailList : function(searchKeyword) {
		
		Top.Dom.selectById('mailTwoPanelLayout_Table').addClass('mail-loading');
		Top.Dom.selectById('mailTwoPanelLayout_Table').setProperties({'empty-message':'검색 중...'});
		mailDataRepo.setValue('mailReceivedBoxDataList',[]);
		
		if(mailData.curBox.FOLDER_ID == '')
			mailData.curBox.FOLDER_ID = 'AA';
		
		var folderType = mailData.curBox.FOLDER_TYPE;
		var parentFolderType = mailData.curBox.PARENT_FOLDER_TYPE
		
		if(folderType == 'MFL0001' || parentFolderType == 'MFL0001' || folderType == 'MFL0002' || folderType == '' || folderType == 'MFL0005' || parentFolderType == 'MFL0005' ||
				folderType == 'MFL0006' || folderType == 'MFL0007') {
			Top.Dom.selectById('mailTwoPanelLayout_Table').setColumnsVisible(1, false);
			Top.Dom.selectById('mailTwoPanelLayout_Table').setColumnsVisible(7, false);
		} else if(folderType == 'MFL0003' || parentFolderType == 'MFL0003') {
			Top.Dom.selectById('mailTwoPanelLayout_Table').setColumnsVisible(2, false);
		} else if(folderType == 'MFL0004' || parentFolderType == 'MFL0004') {
			Top.Dom.selectById('mailTwoPanelLayout_Table').setColumnsVisible(7, false);
		}
		Top.Dom.selectById('mailTwoPanelLayout_Table').setColumnsVisible(3, false);
		Top.Dom.selectById('mailTwoPanelLayout_Table').setColumnsVisible(8, false);
		
		var str_subject;
		var map; //seeingTypeCode, SUBJECT
		if (typeof searchKeyword == "object")
			map = searchKeyword;
		else
			map =  this.getMailSearchKeyword();
		if(map.SUBJECT == "")
			str_subject = "";
		else
			str_subject = "&SUBJECT="+escape(encodeURIComponent(map.SUBJECT));
		
		Top.Controller.get('mailMainLeftLogic').searchKeyword = map; // 3패널로 넘어갈때를 위해 저장
		
		Top.Ajax.execute({
			
			type : "GET", 
			url : _workspace.url + "Mail/Mail?action=ListGetbyFolder&CMFLG=FolderToMailList&FOLDER_ID=" + mailData.curBox.FOLDER_ID +
			"&ACCOUNT_ID=" + mailData.curBox.ACCOUNT_ID + "&USER_ID="+userManager.getLoginUserId()+
			'&SEEING_TYPE_CODE=' + map.seeingTypeCode + str_subject +
			'&CURRPAGE=' + mailData.curPage + '&PAGELENGTH=' + mailData.pageLength+'&SORTING=DATE_DESC',
			dataType : "json",
			crossDomain: true,
			contentType: "application/json",
		    xhrFields: {withCredentials: true},
			success : function(ret,xhr,status) {
				
//				Top.Controller.get('mailMainLeftLogic').seeingTypeCode = 'ALL';
				
				Top.Dom.selectById('mailTwoPanelLayout_Pagination').setProperties({'total':Math.ceil(ret.dto.dbio_total_count / mailData.pageLength),
					'page':mailData.curPage});

				if(ret.dto.INFO_ABOUT_MAIL) {
					for(var i=0; i<ret.dto.INFO_ABOUT_MAIL.length; i++) {
						
						ret.dto.INFO_ABOUT_MAIL[i] = mail.arrangeData(ret.dto.INFO_ABOUT_MAIL[i]);
						var searchText = map.SUBJECT;
						ret.dto.INFO_ABOUT_MAIL[i].SUBJECT = ret.dto.INFO_ABOUT_MAIL[i].SUBJECT.replace(searchText,'<span class="mail-search">'+searchText+'</span>');
						

						if(ret.dto.INFO_ABOUT_MAIL[i].ATTACHMENT_COUNT != null)
							ret.dto.INFO_ABOUT_MAIL[i].CNT_ATTACHMENT = ret.dto.INFO_ABOUT_MAIL[i].ATTACHMENT_COUNT;
						
						var temp = (i+1)%5;
						var ran =  temp == 0 ? 5 : temp;
						if(ret.dto.INFO_ABOUT_MAIL[i].THUMB_PHOTO_ADDRESSBOOK == null){
							var temp__userId = ret.dto.INFO_ABOUT_MAIL[i].PROFILE_USER_ID;
							var temp__thumbPhoto = ret.dto.INFO_ABOUT_MAIL[i].THUMB_PHOTO;
							if(temp__userId == null){
								temp__userId = Math.floor(Math.random() * 10)  + "1111111-1111-1111-1111-" + Math.floor(Math.random() * 10);
								temp__thumbPhoto = null;
							}
							ret.dto.INFO_ABOUT_MAIL[i].SRC = userManager.getUserPhoto( temp__userId, null , temp__thumbPhoto);
						}else{
							ret.dto.INFO_ABOUT_MAIL[i].SRC = ret.dto.INFO_ABOUT_MAIL[i].THUMB_PHOTO_ADDRESSBOOK;
						}
						
						
						ret.dto.INFO_ABOUT_MAIL[i].CHECKBOX_VISIBLE = "none";
						ret.dto.INFO_ABOUT_MAIL[i].PROFILE_VISIBLE = "visible";
					}
					
					
					var columnOpt = {
							"4" : {
								event : {
									onCreated : function(index, data, nTd) {
										Top.Dom.selectById(nTd.children[0].id).setProperties({'on-click':'clickImportant'});
									}
								}
							}, "5" : {
								event: {
									onCreated : function(index, data, nTd) {
										Top.Dom.selectById(nTd.children[0].childs[1].id).setHTML(mailDataRepo.mailReceivedBoxDataList[Top.Dom.selectById('mailTwoPanelLayout_Table').getStartRowNum()+index].DISPLAY_NAME);
										
									}
								}
							},"7" : {
								event : {
									onCreated : function(index, data, nTd) {
										if(mailDataRepo.mailReceivedBoxDataList[Top.Dom.selectById('mailTwoPanelLayout_Table').getStartRowNum()+index].REC_MEM_INFO.length != 1
												&& mailData.curBox.WIDGET_ID.split('Text')[1].substr(0,1) == '0')
											Top.Dom.selectById(nTd.children[0].childs[1].id).setProperties({'text-color':'#09BFD5', 'class':'mail-subject'});
										else
											Top.Dom.selectById(nTd.children[0].childs[1].id).setProperties({'text-color':'#000000'});
									}
								}
							}
					};
	
					Top.Dom.selectById('mailTwoPanelLayout_Table').setProperties({"column-option": columnOpt});
					mailData.intervalTwo = setInterval(function() {
						Top.Controller.get('mailTwoPanelLogic').setAttrDragAndDrop();
					}, 1000);
				} else
					ret.dto.INFO_ABOUT_MAIL = [];
				
				mailDataRepo.setValue('mailReceivedBoxDataList',ret.dto.INFO_ABOUT_MAIL);
				Top.Dom.selectById('mailTwoPanelLayout_Table').setProperties({'empty-message':'검색 결과가 없습니다.'});
				Top.Dom.selectById('mailTwoPanelLayout_Table').removeClass('mail-loading');
				Top.Controller.get('mailTwoPanelLogic').setCheckAll(false);
				
			}
		});
		
		if(mailData.curBox.FOLDER_ID == 'AA')
			mailData.curBox.FOLDER_ID = '';
		
		
	}, saveTdrive : function() {
		
		//Tdrive저장
		
	}, notSpam : function() {
		
		if(Top.Dom.selectById('mailTwoPanelLayout_Table').getCheckedData().length == 0)
			return;
		
		var mailIdList = [];
		for(var i=0; i<Top.Dom.selectById('mailTwoPanelLayout_Table').getCheckedData().length; i++) {
			
			mailIdList.push(Top.Dom.selectById('mailTwoPanelLayout_Table').getCheckedData()[i].MAIL_ID);
			
		}
		
		
		var data={
		dto: {
				"MAIL_ID"		: mailIdList,
				"ACCOUNT_ID"	: mailData.curBox.ACCOUNT_ID,
				"FOLDER_ID"		: mailData.curBox.FOLDER_ID,
				"CMFLG"			: "FolderIdUpdate"
		
				}
		};
		
		
		Top.Ajax.execute({
			
			type : "POST",
			url : _workspace.url + "Mail/Mail?action=FolderIdUpdate",
			dataType : "json",
			data	 : JSON.stringify(data),
			crossDomain: true,
			contentType: "application/json",
		    xhrFields: {withCredentials: true},
			success : function(ret,xhr,status) {
				Top.Controller.get('mailTwoPanelLogic').callMailList();
				Top.Controller.get('mailTwoPanelLogic').setCheckAll(false);
				Top.Controller.get('mailTwoPanelLogic').buttonDisabled();
				Top.Controller.get('mailMainLeftLogic').resetCount();
			}
		});
		
	}, setDelete : function() {
		
		if(Top.Dom.selectById('mailTwoPanelLayout_Table').getCheckedData().length == 0)
			return;
		
		var mailIdList = [];
		for(var i=0; i<Top.Dom.selectById('mailTwoPanelLayout_Table').getCheckedData().length; i++) {
			
			mailIdList.push(Top.Dom.selectById('mailTwoPanelLayout_Table').getCheckedData()[i].MAIL_ID);
			
		}
		
		
		var data={
				dto: {
					"CMFLG"				: "DeleteMail",
					"dbio_total_count"	: Top.Dom.selectById('mailTwoPanelLayout_Table').getCheckedData().length,
					"ACCOUNT_ID"		: mailData.curBox.ACCOUNT_ID,
					"FOLDER_TYPE"		: mailData.curBox.FOLDER_TYPE,
					"MAIL_ID"			: mailIdList
				}
		};
		
		Top.Ajax.execute({
			
			type : "POST",
			url : _workspace.url + "Mail/Mail?action=Delete",
			dataType : "json",
			data	 : JSON.stringify(data),
			crossDomain: true,
			contentType: "application/json",
		    xhrFields: {withCredentials: true},
			success : function(ret,xhr,status) {
				Top.Controller.get('mailTwoPanelLogic').callMailList();
				Top.Controller.get('mailTwoPanelLogic').setCheckAll(false);
				Top.Controller.get('mailTwoPanelLogic').buttonDisabled();
				Top.Controller.get('mailMainLeftLogic').resetCount();
				if(mailData.curBox.FOLDER_TYPE != 'MFL0007')
					TeeToast.open({'text':Top.i18n.get().value.m000157+mailIdList.length+Top.i18n.get().value.m000160,'size':'min'});
//					notiFeedback(Top.i18n.get().value.m000157+mailIdList.length+Top.i18n.get().value.m000160);
			}
		});
		
		
	},setRead : function() {
		
		if(Top.Dom.selectById('mailTwoPanelLayout_Table').getCheckedData().length == 0)
			return;
		
		var mailIdList = [];
		var checkedIndexList = Top.Dom.selectById('mailTwoPanelLayout_Table').getCheckedIndex();
		for(var i=0; i<Top.Dom.selectById('mailTwoPanelLayout_Table').getCheckedData().length; i++) {
			
			mailIdList.push(Top.Dom.selectById('mailTwoPanelLayout_Table').getCheckedData()[i].MAIL_ID);
		}
		
		var seenYN;
		
		if(Top.Dom.selectById('mailTwoPanelLayout_Receive_Button_Read').getText() == '읽음')
			seenYN = 'REA0001';
		else
			seenYN = 'REA0002';
		
		var data={
				dto: {
					"CMFLG"				: "ChangeMailStatus",
					"dbio_total_count"	: Top.Dom.selectById('mailTwoPanelLayout_Table').getCheckedData().length,
					"ACCOUNT_ID"		: mailData.curBox.ACCOUNT_ID,
					"MAIL_MST_TYPE"		: "SEEN_YN_CODE",
					"MAIL_MST_VALUE"	: seenYN,
					"MAIL_ID"			: mailIdList
				}
		};
		
		Top.Ajax.execute({
			
			type : "POST",
			url : _workspace.url + "Mail/Mail?action=ChangeStatus",
			dataType : "json",
			data	 : JSON.stringify(data),
			crossDomain: true,
			contentType: "application/json",
		    xhrFields: {withCredentials: true},
			success : function(ret,xhr,status) {
				
				var textColor = (seenYN == 'REA0001') ? '#6A6C71' : '#000000';
				var textStyle = (seenYN == 'REA0001') ? '' : 'bold';
				var buttonText = (seenYN == 'REA0001') ? '안읽음' : '읽음';
				for(var i = 0 ; i < checkedIndexList.length; i++){
					mailDataRepo.mailReceivedBoxDataList[checkedIndexList[i]].SEEN_YN_CODE = seenYN;
					mailDataRepo.mailReceivedBoxDataList[checkedIndexList[i]].TEXT_COLOR_SENDER = textColor;
					mailDataRepo.mailReceivedBoxDataList[checkedIndexList[i]].TEXT_COLOR = textColor;
					mailDataRepo.mailReceivedBoxDataList[checkedIndexList[i]].TEXT_STYLE = textStyle;
					
				}
				
				Top.Controller.get('mailTwoPanelLogic').setReadButtonText(buttonText);
				Top.Dom.selectById('mailTwoPanelLayout_Table').update();
				Top.Controller.get('mailMainLeftLogic').resetCount();
			
			}
		});
		
	}, dragAndDrop__MoveFolder : function(param){
		var data={
				dto: {
						"MAIL_ID"		: param.MAIL_ID,
						"ACCOUNT_ID"	: param.ACCOUNT_ID,
						"FOLDER_ID"		: param.FOLDER_ID,
						"CMFLG"			: "FolderIdUpdate"
				
						}
				};
				
				Top.Ajax.execute({
					
					type : "POST",
					url : _workspace.url + "Mail/Mail?action=FolderIdUpdate",
					dataType : "json",
					data	 : JSON.stringify(data),
					crossDomain: true,
					contentType: "application/json",
				    xhrFields: {withCredentials: true},
					success : function(ret,xhr,status) {
						Top.Controller.get('mailTwoPanelLogic').callMailList();
						Top.Controller.get('mailMainLeftLogic').resetCount();
						TeeToast.open({'text':Top.i18n.get().value.m000157+'1'+Top.i18n.get().value.m000158+param.DISPLAY_NAME+Top.i18n.get().value.m000159,'size':'min'});
					}
				});
	}, moveFolder : function(event, widget) {
		
		if(Top.Dom.selectById('mailTwoPanelLayout_Table').getCheckedData().length == 0)
			return;
		
		var mailIdList = [];
		for(var i=0; i<Top.Dom.selectById('mailTwoPanelLayout_Table').getCheckedData().length; i++) {
			
			mailIdList.push(Top.Dom.selectById('mailTwoPanelLayout_Table').getCheckedData()[i].MAIL_ID);
			
		}
		
		var data={
		dto: {
				"MAIL_ID"		: mailIdList,
				"ACCOUNT_ID"	: mailData.curBox.ACCOUNT_ID,
				"FOLDER_ID"		: widget.getValue().FOLDER_ID,
				"CMFLG"			: "FolderIdUpdate"
		
				}
		};
		
		Top.Ajax.execute({
			
			type : "POST",
			url : _workspace.url + "Mail/Mail?action=FolderIdUpdate",
			dataType : "json",
			data	 : JSON.stringify(data),
			crossDomain: true,
			contentType: "application/json",
		    xhrFields: {withCredentials: true},
			success : function(ret,xhr,status) {
				Top.Controller.get('mailTwoPanelLogic').callMailList();
				Top.Controller.get('mailTwoPanelLogic').setCheckAll(false);
				Top.Controller.get('mailTwoPanelLogic').buttonDisabled();
				Top.Controller.get('mailMainLeftLogic').resetCount();
				TeeToast.open({'text':Top.i18n.get().value.m000157+mailIdList.length+Top.i18n.get().value.m000158+widget.getValue().DISPLAY_NAME+Top.i18n.get().value.m000159,'size':'min'});
//				notiFeedback(Top.i18n.get().value.m000157+mailIdList.length+Top.i18n.get().value.m000158+widget.getValue().DISPLAY_NAME+Top.i18n.get().value.m000159);
				Top.Dom.selectById('mailTwoPanelLayout_Receive_SelectBox_Move').setProperties({'selected-text':Top.i18n.get().value.m000022});
				Top.Dom.selectById('mailTwoPanelLayout_Sent_SelectBox_Move').setProperties({'selected-text':Top.i18n.get().value.m000022});
				Top.Dom.selectById('mailTwoPanelLayout_SendMe_SelectBox_Move').setProperties({'selected-text':Top.i18n.get().value.m000022});
				Top.Dom.selectById('mailTwoPanelLayout_Spam_SelectBox_Move').setProperties({'selected-text':Top.i18n.get().value.m000022});
				Top.Dom.selectById('mailTwoPanelLayout_Trash_SelectBox_Move').setProperties({'selected-text':Top.i18n.get().value.m000022});
			}
		});
			
		
	}, clickImportant : function(event, widget) {
		
		var priority;
		if(widget.getProperties('text-color') == '#C5C5C8')
			priority = "PRI0001";
		else
			priority = "PRI0002";

		var data={
				dto: {
					
					"CMFLG"				: "ChangeMailStatus",
					"dbio_total_count"	:"1",
					"ACCOUNT_ID"		: mailData.curBox.ACCOUNT_ID,
					"MAIL_MST_TYPE"		: "IMPORTANCE_YN_CODE",
					"MAIL_MST_VALUE"	: priority,
					"MAIL_ID"			: [mailDataRepo.mailReceivedBoxDataList[Top.Dom.selectById('mailTwoPanelLayout_Table').getClickedIndex()[0]].MAIL_ID]
					
						}
		};
		
		Top.Ajax.execute({
			
			type : "POST",
			url : _workspace.url + "Mail/Mail?action=ChangeStatus",
			dataType : "json",
			data	 : JSON.stringify(data),
			crossDomain: true,
			contentType: "application/json",
		    xhrFields: {withCredentials: true},
			success : function(ret,xhr,status) {
				 if(mailData.curBox.FOLDER_TYPE == "MFL0002") {
					 Top.Controller.get('mailTwoPanelLogic').callMailList();
				 } else {
					if(widget.getProperties('text-color') == '#C5C5C8') {
						widget.setProperties({'text-color':'#6C56E5'});
						mailDataRepo.mailReceivedBoxDataList[Top.Dom.selectById('mailTwoPanelLayout_Table').getClickedIndex()[0]].IMPORTANCE_YN_CODE = '#6C56E5';
					} else {
						widget.setProperties({'text-color':'#C5C5C8'});
						mailDataRepo.mailReceivedBoxDataList[Top.Dom.selectById('mailTwoPanelLayout_Table').getClickedIndex()[0]].IMPORTANCE_YN_CODE = '#C5C5C8';
					}
				}
				Top.Controller.get('mailMainLeftLogic').resetCount();
			}
		});
		
	}, setMoveFolder : function() {
				
		var accountList = mailData.accountList;
		var folders = [];
		var folderList = [];;
		
		for(var i=0; i<accountList.length; i++) {
			if(accountList[i].ACCOUNT_ID == mailData.curBox.ACCOUNT_ID)
				folderList = accountList[i].FolderList;
		}
		for(var i=0; i<folderList.length; i++) {
			if(folderList[i].FOLDER_TYPE != 'MFL0006')
				folders.push({'text':folderList[i].DISPLAY_NAME,'value':folderList[i]});
		}
		
		folders.sort(function(a,b) {
			return a.value.FOLDER_TYPE.substr(6,1) - b.value.FOLDER_TYPE.substr(6,1);
		});
		
		var receiveFolder = folders.splice(0,1);
		var importantFolder = folders.splice(0,1);
		var sentFolder = folders.splice(0,1);
		var tempFolder = folders.splice(0,1);
		var sendmeFolder = folders.splice(0,1);
		var trashFolders = folders.splice(0,1);
		
		folders.forEach(function(data, index) {
			
			switch(data.value.PARENT_FOLDER_TYPE) {
			
			case 'MFL0001' :
				data.text='└ '+data.text
				receiveFolder.push(folders[index]);
				break;
			case 'MFL0002' :
				data.text='└ '+data.text
				importantFolder.push(folders[index]);
				break;
			case 'MFL0003' :
				data.text='└ '+data.text
				sentFolder.push(folders[index]);
				break;
			case 'MFL0004' :
				data.text='└ '+data.text
				tempFolder.push(folders[index]);
				break;
			case 'MFL0005' :
				data.text='└ '+data.text
				sendmeFolder.push(folders[index]);
				break;
			
			}
			
		});
	
		var newFolders1 = [];
		newFolders1 = newFolders1.concat(receiveFolder);
		newFolders1 = newFolders1.concat(sendmeFolder);
		newFolders1 = newFolders1.concat(trashFolders);
		
		var newFolders2 = [];
		newFolders2 = newFolders2.concat(sentFolder);
		newFolders2 = newFolders2.concat(sendmeFolder);
		newFolders2 = newFolders2.concat(trashFolders);
		
		var newFolders3 = [];
		newFolders3 = newFolders3.concat(receiveFolder);
		newFolders3 = newFolders3.concat(sentFolder);
		newFolders3 = newFolders3.concat(tempFolder);
		newFolders3 = newFolders3.concat(sendmeFolder);
		newFolders3 = newFolders3.concat(trashFolders);
		
		Top.Dom.selectById('mailTwoPanelLayout_Receive_SelectBox_Move').setProperties({'nodes':newFolders1});
		Top.Dom.selectById('mailTwoPanelLayout_Sent_SelectBox_Move').setProperties({'nodes':newFolders2});
		Top.Dom.selectById('mailTwoPanelLayout_SendMe_SelectBox_Move').setProperties({'nodes':newFolders1});
		Top.Dom.selectById('mailTwoPanelLayout_Spam_SelectBox_Move').setProperties({'nodes':newFolders2});
		Top.Dom.selectById('mailTwoPanelLayout_Trash_SelectBox_Move').setProperties({'nodes':newFolders3});

		
		
	}, setFilterSelectbox : function() {
		
//		var receiveFilter = ["모든 메일", "안 읽은 메일", "중요 메일", "첨부 메일", "나에게 온 메일"];
//		var sentFilter = ["모든 메일", "안 읽은 메일", "중요 메일", "첨부 메일"];
//		var tempFilter = ["모든 메일", "중요 메일", "첨부 메일"];
//		var sendmeFilter = ["모든 메일", "안 읽은 메일", "중요 메일", "첨부 메일"];
//		var spamFilter = ["모든 메일", "안 읽은 메일", "중요 메일", "첨부 메일", "나에게 온 메일"];
//		var trashFilter = ["모든 메일", "안 읽은 메일", "중요 메일", "첨부 메일", "나에게 온 메일"];
		
		var searchNode = ['전체', '보낸 사람', '제목','첨부 파일명'];
		
		Top.Dom.selectById('mailTwoPanelLayout_Receive_SelectBox_Filter').setProperties({'nodes':searchNode});
		Top.Dom.selectById('mailTwoPanelLayout_Sent_SelectBox_Filter').setProperties({'nodes':searchNode});
		Top.Dom.selectById('mailTwoPanelLayout_Temp_SelectBox_Filter').setProperties({'nodes':searchNode});
		Top.Dom.selectById('mailTwoPanelLayout_SendMe_SelectBox_Filter').setProperties({'nodes':searchNode});
		Top.Dom.selectById('mailTwoPanelLayout_Spam_SelectBox_Filter').setProperties({'nodes':searchNode});
		Top.Dom.selectById('mailTwoPanelLayout_Trash_SelectBox_Filter').setProperties({'nodes':searchNode});
		
		Top.Dom.selectById('mailTwoPanelLayout_Receive_SelectBox_Filter').setProperties({'selectedIndex':0});
		Top.Dom.selectById('mailTwoPanelLayout_Sent_SelectBox_Filter').setProperties({'selectedIndex':0});
		Top.Dom.selectById('mailTwoPanelLayout_Temp_SelectBox_Filter').setProperties({'selectedIndex':0});
		Top.Dom.selectById('mailTwoPanelLayout_SendMe_SelectBox_Filter').setProperties({'selectedIndex':0});
		Top.Dom.selectById('mailTwoPanelLayout_Spam_SelectBox_Filter').setProperties({'selectedIndex':0});
		Top.Dom.selectById('mailTwoPanelLayout_Trash_SelectBox_Filter').setProperties({'selectedIndex':0});
		
//		var id= ['Receive','Sent','Temp','SendMe','Spam','Trash'];
//		for(var i=0; i<id.length; i++){
//			Top.Dom.selectById('mailTwoPanelLayout_'+id[i]+'_SelectBox_Filter').setProperties({'on-change':'searchMail'});
//
//			if(Top.Dom.selectById('mailTwoPanelLayout_'+id[i]+'_Buttons').getProperties('visible') == 'visible'){
//				Top.Dom.selectById('mailTwoPanelLayout_'+id[i]+'_SelectBox_Filter').setProperties({'selectedIndex':Top.Controller.get('mailMainLeftLogic').searchKeyword.filterIndex})
//		    }
//			else
//			Top.Dom.selectById('mailTwoPanelLayout_'+id[i]+'_SelectBox_Filter').setProperties({'selectedIndex':0});
//		}
	
	},callMailList : function() {
		
		Top.Dom.selectById('mailTwoPanelLayout_Table').addClass('mail-loading');
		Top.Dom.selectById('mailTwoPanelLayout_Table').setProperties({'empty-message':'로딩 중...'});
		Top.Controller.get('mailMainLeftLogic').searchKeyword = null; 

		if(mailData.curBox.FOLDER_ID == '')
			mailData.curBox.FOLDER_ID = 'AA';
		
		var folderType = mailData.curBox.FOLDER_TYPE;
		var parentFolderType = mailData.curBox.PARENT_FOLDER_TYPE
		
		if(folderType == 'MFL0001' || parentFolderType == 'MFL0001' || folderType == 'MFL0002' || folderType == "" || folderType == 'MFL0005' || parentFolderType == 'MFL0005' ||
				folderType == 'MFL0006' || folderType == 'MFL0007') {
			Top.Dom.selectById('mailTwoPanelLayout_Table').setColumnsVisible(1, false);
			Top.Dom.selectById('mailTwoPanelLayout_Table').setColumnsVisible(7, false);
		} else if(folderType == 'MFL0003' || parentFolderType == 'MFL0003') {	//보낸 편지함, 받는사람X
			Top.Dom.selectById('mailTwoPanelLayout_Table').setColumnsVisible(2, false);
		} else if(folderType == 'MFL0004' || parentFolderType == 'MFL0004') {
			Top.Dom.selectById('mailTwoPanelLayout_Table').setColumnsVisible(2, false);
			Top.Dom.selectById('mailTwoPanelLayout_Table').setColumnsVisible(7, false);
		}
		
		Top.Dom.selectById('mailTwoPanelLayout_Table').setColumnsVisible(3, false);
		Top.Dom.selectById('mailTwoPanelLayout_Table').setColumnsVisible(8, false);
		Top.Ajax.execute({
			
			type : "GET",
			url : _workspace.url + "Mail/Mail?action=ListGetbyFolder&CMFLG=FolderToMailList&FOLDER_ID=" + mailData.curBox.FOLDER_ID +
			"&ACCOUNT_ID=" + mailData.curBox.ACCOUNT_ID + "&USER_ID="+userManager.getLoginUserId()+
			'&SEEING_TYPE_CODE=' + Top.Controller.get('mailMainLeftLogic').seeingTypeCode + '&CURRPAGE=' + mailData.curPage + '&PAGELENGTH=' + mailData.pageLength + '&SORTING=DATE_DESC'+'&q='+new Date().getTime(),
			dataType : "json",
			crossDomain: true,
			contentType: "application/json",
		    xhrFields: {withCredentials: true},
			success : function(ret,xhr,status) {
				
				var endPage = Math.ceil(ret.dto.dbio_total_count / mailData.pageLength);
				if(endPage != 0 && mailData.curPage > endPage) {
					mailData.curPage = endPage;
					Top.Controller.get('mailTwoPanelLogic').callMailList();
					return;
				}
				
//				Top.Controller.get('mailMainLeftLogic').seeingTypeCode = 'ALL';
				
				mailData.curBox.MAIL_COUNT = ret.dto.dbio_total_count;
				mailData.curBox.UNREAD_COUNT = ret.dto.COUNT_NOT_READ;
				
				Top.Dom.selectById('mailTwoPanelLayout_Pagination').setProperties({'total':endPage, 'page':mailData.curPage});
				
				
//				Top.Dom.selectById('mailMainLayout_AllCount').setText(ret.dto.dbio_total_count);
//				Top.Dom.selectById('mailMainLayout_NotReadCount').setText(ret.dto.COUNT_NOT_READ);
				
				if(ret.dto.INFO_ABOUT_MAIL) {
					for(var i=0; i<ret.dto.INFO_ABOUT_MAIL.length; i++) {
						ret.dto.INFO_ABOUT_MAIL[i] = mail.arrangeData(ret.dto.INFO_ABOUT_MAIL[i]);
//						if(ret.dto.INFO_ABOUT_MAIL[i].HAS_ATTACHMENT_YN_CODE == "visible")
						if(ret.dto.INFO_ABOUT_MAIL[i].ATTACHMENT_COUNT != null)
							ret.dto.INFO_ABOUT_MAIL[i].CNT_ATTACHMENT = ret.dto.INFO_ABOUT_MAIL[i].ATTACHMENT_COUNT;
						
						var temp = (i+1)%5;
						var ran =  temp == 0 ? 5 : temp;
						if(ret.dto.INFO_ABOUT_MAIL[i].THUMB_PHOTO_ADDRESSBOOK == null){
							var temp__userId = ret.dto.INFO_ABOUT_MAIL[i].PROFILE_USER_ID;
							var temp__thumbPhoto = ret.dto.INFO_ABOUT_MAIL[i].THUMB_PHOTO;
							if(temp__userId == null){
								temp__userId = Math.floor(Math.random() * 10)  + "1111111-1111-1111-1111-" + Math.floor(Math.random() * 10);
								temp__thumbPhoto = null;
							}
							ret.dto.INFO_ABOUT_MAIL[i].SRC = userManager.getUserPhoto( temp__userId, null , temp__thumbPhoto);
						}else{
							ret.dto.INFO_ABOUT_MAIL[i].SRC = ret.dto.INFO_ABOUT_MAIL[i].THUMB_PHOTO_ADDRESSBOOK;
						}
						
						ret.dto.INFO_ABOUT_MAIL[i].CHECKBOX_VISIBLE = "none";
						ret.dto.INFO_ABOUT_MAIL[i].PROFILE_VISIBLE = "visible";
					}

					
					var columnOpt = {
							"4" : {
								event : {
									onCreated : function(index, data, nTd) {
										Top.Dom.selectById(nTd.children[0].id).setProperties({'on-click':'clickImportant'});
										
										
										
										Top.Dom.selectById("mailTwoPanelLayout_ColumnItem_CheckBox_"+index+"_0").setProperties({'visible':mailDataRepo.mailReceivedBoxDataList[Top.Dom.selectById('mailTwoPanelLayout_Table').getStartRowNum()+index].CHECKBOX_VISIBLE});
										Top.Dom.selectById("ImageButton201_"+index+"_0").setProperties({'visible':mailDataRepo.mailReceivedBoxDataList[Top.Dom.selectById('mailTwoPanelLayout_Table').getStartRowNum()+index].PROFILE_VISIBLE});
										

									}
								}
							}, "5" : {
								event: {
									onCreated : function(index, data, nTd) {
										Top.Dom.selectById(nTd.children[0].childs[1].id).setHTML(mailDataRepo.mailReceivedBoxDataList[Top.Dom.selectById('mailTwoPanelLayout_Table').getStartRowNum()+index].DISPLAY_NAME);
										Top.Dom.selectById(nTd.children[0].childs[2].id).setHTML(mailDataRepo.mailReceivedBoxDataList[Top.Dom.selectById('mailTwoPanelLayout_Table').getStartRowNum()+index].SUBJECT);
									}
								}
							},"7" : {
								event : {
									onCreated : function(index, data, nTd) {
										if(mailDataRepo.mailReceivedBoxDataList[Top.Dom.selectById('mailTwoPanelLayout_Table').getStartRowNum()+index].REC_MEM_INFO.length != 1
												&& mailData.curBox.WIDGET_ID.split('Text')[1].substr(0,1) == '0')
											Top.Dom.selectById(nTd.children[0].childs[1].id).setProperties({'text-color':'#09BFD5', 'class':'mail-subject'});
										else
											Top.Dom.selectById(nTd.children[0].childs[1].id).setProperties({'text-color':'#000000'});
									}
								}
							}
					};
					
					Top.Dom.selectById('mailTwoPanelLayout_Table').setProperties({"column-option": columnOpt});
					mailData.intervalTwo = setInterval(function() {
						Top.Controller.get('mailTwoPanelLogic').setAttrDragAndDrop();
					}, 1000);
				} else
					ret.dto.INFO_ABOUT_MAIL = [];
				
				mailDataRepo.setValue('mailReceivedBoxDataList',ret.dto.INFO_ABOUT_MAIL);
				Top.Dom.selectById('mailTwoPanelLayout_Table').setProperties({'empty-message':'편지가 존재하지 않습니다.'});
				Top.Dom.selectById('mailTwoPanelLayout_Table').removeClass('mail-loading');
				Top.Dom.selectById('mailTwoPanelLayout_Table').checkAll(false);
				
				
				
			}
		});
		
	}, 
	
	hoverImage : function(event, widget, data) {
		console.log("마우스 올림");
	}
	,readMail : function(event, widget, data) {

		if(data.target.classList[0] == 'top-checkbox-check' || data.target.classList[0] == 'top-icon-content' || data.target.classList[0] == 'top-checkbox-icon')
			return;
		if(data.target.parentElement.id.substr(0,25) == 'mailTwoPanelLayout_Status' && data.data.REC_MEM_INFO.length > 1) {
			this.selectedData = data.data.REC_MEM_INFO;
			Top.Dom.selectById('mailInquiryStatusDialog').open();
			return;
		}
		Top.Controller.get('mailMainLeftLogic').clickedMail = Top.Dom.selectById('mailTwoPanelLayout_Table').getClickedData().MAIL_ID;
		Top.Controller.get('mailMainLeftLogic').clickedMailProfile = Top.Dom.selectById('mailTwoPanelLayout_Table').getClickedData().SRC;
		if(mailData.routeInfo.folderId == undefined) mailData.routeInfo.folderId = '0';
		
		if(mailData.curBox.DISPLAY_NAME == "임시 보관함"){
            let sub = appManager.getSubApp();
    		if(sub) 
	    		Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/new' +"?sub=" + sub+'&q=' + new Date().getTime(), {eventType:'fold'});
    		else 
    			Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/new?q=' + new Date().getTime(), {eventType:'close'});	
    		Top.Controller.get('mailWriteLogic').flg__loadTempMail = true;
		}else{
			let sub = appManager.getSubApp();
			if(sub) 
				Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + mailData.routeInfo.folderId + '/' + Top.Controller.get('mailMainLeftLogic').clickedMail + "?sub=" + sub, {eventType:'fold'});
			else 
				Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + mailData.routeInfo.folderId + '/' + Top.Controller.get('mailMainLeftLogic').clickedMail, {eventType:'close'});
		}
		Top.Controller.get('mailMainLeftLogic').resetCount();
        
	}, buttonStatusChange : function(event, widget) {
		
		if(Top.Dom.selectById('mailTwoPanelLayout_Table').getCheckedIndex() == undefined)
			return;
		
		if(widget.id.substr(widget.id.length-9, 9) == 'Check_All' && widget.isChecked() == true && mailDataRepo.mailReceivedBoxDataList.length != 0)
		{
			Top.Dom.selectById('mailTwoPanelLayout_Table').checkAll(true);
			this.mailListCheckBoxAll(true);
		}
		else if(widget.id.substr(widget.id.length-9, 9) == 'Check_All' && widget.isChecked() == false)
		{
			Top.Dom.selectById('mailTwoPanelLayout_Table').checkAll(false);
			this.mailListCheckBoxAll(false);
		}
		
		if(Top.Dom.selectById('mailTwoPanelLayout_Table').getCheckedIndex().length == 0) {
			this.setReadButtonText('읽음');
			this.buttonDisabled();
		}
		else if(Top.Dom.selectById('mailTwoPanelLayout_Table').getCheckedIndex().length > 0) {
			
			var count = 0;
			
			for(var i=0; i<Top.Dom.selectById('mailTwoPanelLayout_Table').getCheckedIndex().length; i++) {
				if(Top.Dom.selectById('mailTwoPanelLayout_Table').getCheckedData()[i].SEEN_YN_CODE == 'REA0001')
					count++;				
			}
			
			if(count == Top.Dom.selectById('mailTwoPanelLayout_Table').getCheckedIndex().length)
				this.setReadButtonText('안읽음');
			else
				this.setReadButtonText('읽음');
			
			if(Top.Dom.selectById('mailTwoPanelLayout_Table').getCheckedIndex().length == 1)
				this.buttonAbled();
			else
				this.buttonReadAbled();
		}
		
		
	}, mailListCheckBoxAll(flg_all){
		var size_binding = Top.Dom.selectById('mailTwoPanelLayout_Table').getProperties('bindingarray').length;
		var size_items = Top.Dom.selectById('mailTwoPanelLayout_Table').getProperties('items').length;
		var checkBoxId = 'mailTwoPanelLayout_ColumnItem_CheckBox_';
		var imageId = 'ImageButton201_';
		
		if(flg_all){
				for(var i=0; i<size_items; i++){
					if(i<size_binding){
						Top.Dom.selectById(checkBoxId+i+'_0').setProperties({'visible':'visible'});
						Top.Dom.selectById(imageId+i+'_0').setProperties({'visible':'none'});
					}
					mailDataRepo.mailReceivedBoxDataList[i].CHECKBOX_VISIBLE = "visible";
					mailDataRepo.mailReceivedBoxDataList[i].PROFILE_VISIBLE = "none";
				}
		}else{
				for(var i=0; i<size_items; i++){
					if(i<size_binding){
						Top.Dom.selectById(checkBoxId+i+'_0').setProperties({'visible':'none'});
						Top.Dom.selectById(imageId+i+'_0').setProperties({'visible':'visible'});
					}
					
					mailDataRepo.mailReceivedBoxDataList[i].CHECKBOX_VISIBLE = "none";
					mailDataRepo.mailReceivedBoxDataList[i].PROFILE_VISIBLE = "visible";
				}
		}
		
	}, buttonAbled : function(event, widget) {
				
		Top.Dom.selectById('mailTwoPanelLayout_Receive_Button_Read').setProperties({'disabled':false});
		Top.Dom.selectById('mailTwoPanelLayout_Receive_Button_Delete').setProperties({'disabled':false});
		Top.Dom.selectById('mailTwoPanelLayout_Receive_Button_Reply').setProperties({'disabled':false});
		Top.Dom.selectById('mailTwoPanelLayout_Receive_Button_ReplyAll').setProperties({'disabled':false});
		Top.Dom.selectById('mailTwoPanelLayout_Receive_Button_Forward').setProperties({'disabled':false});
		Top.Dom.selectById('mailTwoPanelLayout_Receive_SelectBox_Save').setProperties({'disabled':false});
		Top.Dom.selectById('mailTwoPanelLayout_Receive_SelectBox_Move').setProperties({'disabled':false});
		
		Top.Dom.selectById('mailTwoPanelLayout_Sent_Button_Read').setProperties({'disabled':false});
		Top.Dom.selectById('mailTwoPanelLayout_Sent_Button_Delete').setProperties({'disabled':false});
		Top.Dom.selectById('mailTwoPanelLayout_Sent_Button_Reply').setProperties({'disabled':false});
		Top.Dom.selectById('mailTwoPanelLayout_Sent_Button_ReplyAll').setProperties({'disabled':false});
		Top.Dom.selectById('mailTwoPanelLayout_Sent_Button_Forward').setProperties({'disabled':false});
		Top.Dom.selectById('mailTwoPanelLayout_Sent_Button_Resend').setProperties({'disabled':false});
		Top.Dom.selectById('mailTwoPanelLayout_Sent_SelectBox_Save').setProperties({'disabled':false});
		Top.Dom.selectById('mailTwoPanelLayout_Sent_SelectBox_Move').setProperties({'disabled':false});
		
		Top.Dom.selectById('mailTwoPanelLayout_Temp_Button_Read').setProperties({'disabled':false});
		Top.Dom.selectById('mailTwoPanelLayout_Temp_Button_Delete').setProperties({'disabled':false});
		Top.Dom.selectById('mailTwoPanelLayout_Temp_Button_Forward').setProperties({'disabled':false});
		
		Top.Dom.selectById('mailTwoPanelLayout_SendMe_Button_Read').setProperties({'disabled':false});
		Top.Dom.selectById('mailTwoPanelLayout_SendMe_Button_Delete').setProperties({'disabled':false});
		Top.Dom.selectById('mailTwoPanelLayout_SendMe_Button_Forward').setProperties({'disabled':false});
		Top.Dom.selectById('mailTwoPanelLayout_SendMe_Button_Modify').setProperties({'disabled':false});
		Top.Dom.selectById('mailTwoPanelLayout_SendMe_SelectBox_Save').setProperties({'disabled':false});
		Top.Dom.selectById('mailTwoPanelLayout_SendMe_SelectBox_Move').setProperties({'disabled':false});
		
		Top.Dom.selectById('mailTwoPanelLayout_Spam_Button_Read').setProperties({'disabled':false});
		Top.Dom.selectById('mailTwoPanelLayout_Spam_Button_Delete').setProperties({'disabled':false});
		Top.Dom.selectById('mailTwoPanelLayout_Spam_SelectBox_Move').setProperties({'disabled':false});
		
		Top.Dom.selectById('mailTwoPanelLayout_Trash_Button_Read').setProperties({'disabled':false});
		Top.Dom.selectById('mailTwoPanelLayout_Trash_Button_Delete').setProperties({'disabled':false});
		Top.Dom.selectById('mailTwoPanelLayout_Trash_SelectBox_Move').setProperties({'disabled':false});
		
	}, buttonReadAbled : function(event, widget) {
		
		Top.Dom.selectById('mailTwoPanelLayout_Receive_Button_Read').setProperties({'disabled':false});
		Top.Dom.selectById('mailTwoPanelLayout_Receive_Button_Delete').setProperties({'disabled':false});
		Top.Dom.selectById('mailTwoPanelLayout_Receive_Button_Reply').setProperties({'disabled':true});
		Top.Dom.selectById('mailTwoPanelLayout_Receive_Button_ReplyAll').setProperties({'disabled':true});
		Top.Dom.selectById('mailTwoPanelLayout_Receive_Button_Forward').setProperties({'disabled':true});
		Top.Dom.selectById('mailTwoPanelLayout_Receive_SelectBox_Save').setProperties({'disabled':false});
		Top.Dom.selectById('mailTwoPanelLayout_Receive_SelectBox_Move').setProperties({'disabled':false});
		
		Top.Dom.selectById('mailTwoPanelLayout_Sent_Button_Read').setProperties({'disabled':false});
		Top.Dom.selectById('mailTwoPanelLayout_Sent_Button_Delete').setProperties({'disabled':false});
		Top.Dom.selectById('mailTwoPanelLayout_Sent_Button_Reply').setProperties({'disabled':true});
		Top.Dom.selectById('mailTwoPanelLayout_Sent_Button_ReplyAll').setProperties({'disabled':true});
		Top.Dom.selectById('mailTwoPanelLayout_Sent_Button_Forward').setProperties({'disabled':true});
		Top.Dom.selectById('mailTwoPanelLayout_Sent_Button_Resend').setProperties({'disabled':true});
		Top.Dom.selectById('mailTwoPanelLayout_Sent_SelectBox_Save').setProperties({'disabled':false});
		Top.Dom.selectById('mailTwoPanelLayout_Sent_SelectBox_Move').setProperties({'disabled':false});
		
		Top.Dom.selectById('mailTwoPanelLayout_Temp_Button_Read').setProperties({'disabled':false});
		Top.Dom.selectById('mailTwoPanelLayout_Temp_Button_Delete').setProperties({'disabled':false});
		Top.Dom.selectById('mailTwoPanelLayout_Temp_Button_Forward').setProperties({'disabled':true});
		
		Top.Dom.selectById('mailTwoPanelLayout_SendMe_Button_Read').setProperties({'disabled':false});
		Top.Dom.selectById('mailTwoPanelLayout_SendMe_Button_Delete').setProperties({'disabled':false});
		Top.Dom.selectById('mailTwoPanelLayout_SendMe_Button_Forward').setProperties({'disabled':true});
		Top.Dom.selectById('mailTwoPanelLayout_SendMe_Button_Modify').setProperties({'disabled':true});
		Top.Dom.selectById('mailTwoPanelLayout_SendMe_SelectBox_Save').setProperties({'disabled':false});
		Top.Dom.selectById('mailTwoPanelLayout_SendMe_SelectBox_Move').setProperties({'disabled':false});
		
		Top.Dom.selectById('mailTwoPanelLayout_Spam_Button_Read').setProperties({'disabled':false});
		Top.Dom.selectById('mailTwoPanelLayout_Spam_Button_Delete').setProperties({'disabled':false});
		Top.Dom.selectById('mailTwoPanelLayout_Spam_SelectBox_Move').setProperties({'disabled':false});
		
		Top.Dom.selectById('mailTwoPanelLayout_Trash_Button_Read').setProperties({'disabled':false});
		Top.Dom.selectById('mailTwoPanelLayout_Trash_Button_Delete').setProperties({'disabled':false});
		Top.Dom.selectById('mailTwoPanelLayout_Trash_SelectBox_Move').setProperties({'disabled':false});
		
	}, buttonDisabled : function(event, widget) {
		
		Top.Dom.selectById('mailTwoPanelLayout_Receive_Button_Read').setProperties({'disabled':true});
		Top.Dom.selectById('mailTwoPanelLayout_Receive_Button_Delete').setProperties({'disabled':true});
		Top.Dom.selectById('mailTwoPanelLayout_Receive_Button_Reply').setProperties({'disabled':true});
		Top.Dom.selectById('mailTwoPanelLayout_Receive_Button_ReplyAll').setProperties({'disabled':true});
		Top.Dom.selectById('mailTwoPanelLayout_Receive_Button_Forward').setProperties({'disabled':true});
		Top.Dom.selectById('mailTwoPanelLayout_Receive_SelectBox_Save').setProperties({'disabled':true});
		Top.Dom.selectById('mailTwoPanelLayout_Receive_SelectBox_Move').setProperties({'disabled':true});
		
		Top.Dom.selectById('mailTwoPanelLayout_Sent_Button_Read').setProperties({'disabled':true});
		Top.Dom.selectById('mailTwoPanelLayout_Sent_Button_Delete').setProperties({'disabled':true});
		Top.Dom.selectById('mailTwoPanelLayout_Sent_Button_Reply').setProperties({'disabled':true});
		Top.Dom.selectById('mailTwoPanelLayout_Sent_Button_ReplyAll').setProperties({'disabled':true});
		Top.Dom.selectById('mailTwoPanelLayout_Sent_Button_Forward').setProperties({'disabled':true});
		Top.Dom.selectById('mailTwoPanelLayout_Sent_Button_Resend').setProperties({'disabled':true});
		Top.Dom.selectById('mailTwoPanelLayout_Sent_SelectBox_Save').setProperties({'disabled':true});
		Top.Dom.selectById('mailTwoPanelLayout_Sent_SelectBox_Move').setProperties({'disabled':true});
		
		Top.Dom.selectById('mailTwoPanelLayout_Temp_Button_Read').setProperties({'disabled':true});
		Top.Dom.selectById('mailTwoPanelLayout_Temp_Button_Delete').setProperties({'disabled':true});
		Top.Dom.selectById('mailTwoPanelLayout_Temp_Button_Forward').setProperties({'disabled':true});
		
		Top.Dom.selectById('mailTwoPanelLayout_SendMe_Button_Read').setProperties({'disabled':true});
		Top.Dom.selectById('mailTwoPanelLayout_SendMe_Button_Delete').setProperties({'disabled':true});
		Top.Dom.selectById('mailTwoPanelLayout_SendMe_Button_Forward').setProperties({'disabled':true});
		Top.Dom.selectById('mailTwoPanelLayout_SendMe_Button_Modify').setProperties({'disabled':true});
		Top.Dom.selectById('mailTwoPanelLayout_SendMe_SelectBox_Save').setProperties({'disabled':true});
		Top.Dom.selectById('mailTwoPanelLayout_SendMe_SelectBox_Move').setProperties({'disabled':true});
		
		Top.Dom.selectById('mailTwoPanelLayout_Spam_Button_Read').setProperties({'disabled':true});
		Top.Dom.selectById('mailTwoPanelLayout_Spam_Button_Delete').setProperties({'disabled':true});
		Top.Dom.selectById('mailTwoPanelLayout_Spam_SelectBox_Move').setProperties({'disabled':true});
		
		Top.Dom.selectById('mailTwoPanelLayout_Trash_Button_Read').setProperties({'disabled':true});
		Top.Dom.selectById('mailTwoPanelLayout_Trash_Button_Delete').setProperties({'disabled':true});
		Top.Dom.selectById('mailTwoPanelLayout_Trash_SelectBox_Move').setProperties({'disabled':true});
		
	}, moveToThreePanel : function(){
		
		mailData.panelType = '3';
		Top.Controller.get('mailMainLeftLogic').isChangePanel = true;
		Top.Dom.selectById('mailMainLayout_Center_All').src('mailThreePanelMainLayout.html' + verCsp());
		 
		 var data={
		 			dto: {
		 					"ACCOUNT_ID"	: Top.Dom.selectById('mailMainLayout_Button_SendMail').getValue(),
		 					"PANEL_TYPE"	: "3"
		 						}};
			 	
		 	Top.Ajax.execute({
				
				type : "POST",
				url : _workspace.url + "Mail/Mail?action=SetPanelType",
				dataType : "json",
				data	 : JSON.stringify(data),
				crossDomain: true,
				contentType: "application/json",
			    xhrFields: {withCredentials: true},
				success : function(ret,xhr,status) {}
			 });
		 	
		 	
	}, setVisibleNone : function(){
		var id= ['Receive','Sent','Temp','SendMe','Spam','Trash'];
		var replyId = ['Receive','Sent'];
		var replyAllId = ['Receive','Sent'];
		var forwardId = ['Receive','Sent','Temp','SendMe'];
		for(var i=0; i<replyId.length; i++){
			Top.Dom.selectById('mailTwoPanelLayout_'+id[i]+'_Button_Reply').setProperties({'visible':'none'});
		}
		for(var i=0; i<replyAllId.length; i++){
			Top.Dom.selectById('mailTwoPanelLayout_'+id[i]+'_Button_ReplyAll').setProperties({'visible':'none'});
		}
		for(var i=0; i<forwardId.length; i++){
			Top.Dom.selectById('mailTwoPanelLayout_'+id[i]+'_Button_Forward').setProperties({'visible':'none'});
		}
		
	}, initButton : function() {
		this.setOnClick__AddressIcon();
		this.setEvent__SearchIcon();
		this.setVisibleNone();
		
		Top.Dom.selectById('mailTwoPanelLayout_Receive_Check_All').setProperties({'on-change':'buttonStatusChange'});
		Top.Dom.selectById('mailTwoPanelLayout_Receive_Button_Read').setProperties({'disabled':true,'on-click':'setRead'});
		Top.Dom.selectById('mailTwoPanelLayout_Receive_Button_Delete').setProperties({'disabled':true,'on-click':'setDelete'});
		Top.Dom.selectById('mailTwoPanelLayout_Receive_Button_Reply').setProperties({'disabled':true, 'on-click':'replyMail'});
		Top.Dom.selectById('mailTwoPanelLayout_Receive_Button_ReplyAll').setProperties({'disabled':true, 'on-click':'replyAllMail'});
		Top.Dom.selectById('mailTwoPanelLayout_Receive_Button_Forward').setProperties({'disabled':true,'on-click':'forwardMail'});
		Top.Dom.selectById('mailTwoPanelLayout_Receive_SelectBox_Save').setProperties({'on-change':'saveTdrive'});
		Top.Dom.selectById('mailTwoPanelLayout_Receive_SelectBox_Move').setProperties({'on-change':'moveFolder'});
		Top.Dom.selectById('mailTwoPanelLayout_Receive_ToThree_Icon').setProperties({'on-click':'moveToThreePanel'});
		
		Top.Dom.selectById('mailTwoPanelLayout_Sent_Check_All').setProperties({'on-change':'buttonStatusChange'});
		Top.Dom.selectById('mailTwoPanelLayout_Sent_Button_Read').setProperties({'disabled':true,'on-click':'setRead'});
		Top.Dom.selectById('mailTwoPanelLayout_Sent_Button_Delete').setProperties({'disabled':true,'on-click':'setDelete'});
		Top.Dom.selectById('mailTwoPanelLayout_Sent_Button_Reply').setProperties({'disabled':true, 'on-click':'replyMail'});
		Top.Dom.selectById('mailTwoPanelLayout_Sent_Button_ReplyAll').setProperties({'disabled':true, 'on-click':'replyAllMail'});
		Top.Dom.selectById('mailTwoPanelLayout_Sent_Button_Forward').setProperties({'disabled':true,'on-click':'forwardMail'});
		Top.Dom.selectById('mailTwoPanelLayout_Sent_Button_Resend').setProperties({'disabled':true,'on-click':'resendMail'});
		Top.Dom.selectById('mailTwoPanelLayout_Sent_SelectBox_Save').setProperties({'on-change':'saveTdrive'});
		Top.Dom.selectById('mailTwoPanelLayout_Sent_SelectBox_Move').setProperties({'on-change':'moveFolder'});
		Top.Dom.selectById('mailTwoPanelLayout_Sent_ToThree_Icon').setProperties({'on-click':'moveToThreePanel'});
		
		Top.Dom.selectById('mailTwoPanelLayout_Temp_Check_All').setProperties({'on-change':'buttonStatusChange'});
		Top.Dom.selectById('mailTwoPanelLayout_Temp_Button_Read').setProperties({'disabled':true,'on-click':'setRead'});
		Top.Dom.selectById('mailTwoPanelLayout_Temp_Button_Delete').setProperties({'disabled':true,'on-click':'setDelete'});
		Top.Dom.selectById('mailTwoPanelLayout_Temp_Button_Forward').setProperties({'disabled':true,'on-click':'forwardMail'});
		Top.Dom.selectById('mailTwoPanelLayout_Temp_ToThree_Icon').setProperties({'on-click':'moveToThreePanel'});
		
		Top.Dom.selectById('mailTwoPanelLayout_SendMe_Check_All').setProperties({'on-change':'buttonStatusChange'});
		Top.Dom.selectById('mailTwoPanelLayout_SendMe_Button_Read').setProperties({'disabled':true,'on-click':'setRead'});
		Top.Dom.selectById('mailTwoPanelLayout_SendMe_Button_Delete').setProperties({'disabled':true,'on-click':'setDelete'});
		Top.Dom.selectById('mailTwoPanelLayout_SendMe_Button_Forward').setProperties({'disabled':true, 'on-click':'forwardMail'});
		Top.Dom.selectById('mailTwoPanelLayout_SendMe_Button_Modify').setProperties({'disabled':true, 'on-click':'modifyMail'});
		Top.Dom.selectById('mailTwoPanelLayout_SendMe_SelectBox_Save').setProperties({'on-change':'saveTdrive'});
		Top.Dom.selectById('mailTwoPanelLayout_SendMe_SelectBox_Move').setProperties({'on-change':'moveFolder'});
		Top.Dom.selectById('mailTwoPanelLayout_SendMe_ToThree_Icon').setProperties({'on-click':'moveToThreePanel'});
		
		Top.Dom.selectById('mailTwoPanelLayout_SendMe_Button_Forward').setProperties({'visible':'none'});
		Top.Dom.selectById('mailTwoPanelLayout_SendMe_Button_Modify').setProperties({'visible':'none'});
		
		Top.Dom.selectById('mailTwoPanelLayout_Spam_Check_All').setProperties({'on-change':'buttonStatusChange'});
		Top.Dom.selectById('mailTwoPanelLayout_Spam_Button_Read').setProperties({'disabled':true,'on-click':'setRead'});
		Top.Dom.selectById('mailTwoPanelLayout_Spam_Button_Delete').setProperties({'disabled':true,'on-click':'setDelete'});
		Top.Dom.selectById('mailTwoPanelLayout_Spam_SelectBox_Move').setProperties({'on-change':'moveFolder'});
		Top.Dom.selectById('mailTwoPanelLayout_Spam_ToThree_Icon').setProperties({'on-click':'moveToThreePanel'});
		
		Top.Dom.selectById('mailTwoPanelLayout_Trash_Check_All').setProperties({'on-change':'buttonStatusChange'});
		Top.Dom.selectById('mailTwoPanelLayout_Trash_Button_Read').setProperties({'disabled':true,'on-click':'setRead'});
		Top.Dom.selectById('mailTwoPanelLayout_Trash_Button_Delete').setProperties({'disabled':true,'on-click':function(){Top.Dom.selectById('mailDeletePermanentDialog').open();}});
		Top.Dom.selectById('mailTwoPanelLayout_Trash_SelectBox_Move').setProperties({'on-change':'moveFolder'});
		Top.Dom.selectById('mailTwoPanelLayout_Trash_ToThree_Icon').setProperties({'on-click':'moveToThreePanel'});
		
		Top.Dom.selectById('mailTwoPanelLayout_Receive_Buttons').setProperties({'visible':'none'});
		Top.Dom.selectById('mailTwoPanelLayout_Sent_Buttons').setProperties({'visible':'none'});
		Top.Dom.selectById('mailTwoPanelLayout_Temp_Buttons').setProperties({'visible':'none'});
		Top.Dom.selectById('mailTwoPanelLayout_SendMe_Buttons').setProperties({'visible':'none'});
		Top.Dom.selectById('mailTwoPanelLayout_Spam_Buttons').setProperties({'visible':'none'});
		Top.Dom.selectById('mailTwoPanelLayout_Trash_Buttons').setProperties({'visible':'none'});
		
		var folderType = mailData.curBox.FOLDER_TYPE;
		var parentFolderType = mailData.curBox.PARENT_FOLDER_TYPE
		if(folderType == 'MFL0001' || parentFolderType == 'MFL0001' || folderType == 'MFL0002' || folderType == '')
			Top.Dom.selectById('mailTwoPanelLayout_Receive_Buttons').setProperties({'visible':'visible'});
		else if(folderType == 'MFL0003' || parentFolderType == 'MFL0003')
			Top.Dom.selectById('mailTwoPanelLayout_Sent_Buttons').setProperties({'visible':'visible'});
		else if(folderType == 'MFL0004' || parentFolderType == 'MFL0004')
			Top.Dom.selectById('mailTwoPanelLayout_Temp_Buttons').setProperties({'visible':'visible'});
		else if(folderType == 'MFL0005' || parentFolderType == 'MFL0005')
			Top.Dom.selectById('mailTwoPanelLayout_SendMe_Buttons').setProperties({'visible':'visible'});
		else if(folderType == 'MFL0006')
			Top.Dom.selectById('mailTwoPanelLayout_Spam_Buttons').setProperties({'visible':'visible'});
		else if(folderType == 'MFL0007')
			Top.Dom.selectById('mailTwoPanelLayout_Trash_Buttons').setProperties({'visible':'visible'});
		
		if(mailData.pageLength != undefined){
			var items = Top.Dom.selectById('mailTwoPanelLayout_PageLength').getProperties('itemsList');
			for(var i=0; i<items.length; i++){
				if(items[i] == mailData.pageLength)
					Top.Dom.selectById('mailTwoPanelLayout_PageLength').setProperties({'selectedIndex':i});
			}
		}
	}, setReadButtonText : function(text) {
		
		Top.Dom.selectById('mailTwoPanelLayout_Receive_Button_Read').setText(text);
		Top.Dom.selectById('mailTwoPanelLayout_Sent_Button_Read').setText(text);
		Top.Dom.selectById('mailTwoPanelLayout_Temp_Button_Read').setText(text);
		Top.Dom.selectById('mailTwoPanelLayout_SendMe_Button_Read').setText(text);
		Top.Dom.selectById('mailTwoPanelLayout_Spam_Button_Read').setText(text);
		Top.Dom.selectById('mailTwoPanelLayout_Trash_Button_Read').setText(text);
	
	}, setCheckAll : function(flag) {
		
		Top.Dom.selectById('mailTwoPanelLayout_Receive_Check_All').setChecked(flag);
		Top.Dom.selectById('mailTwoPanelLayout_Sent_Check_All').setChecked(flag);
		Top.Dom.selectById('mailTwoPanelLayout_Temp_Check_All').setChecked(flag);
		Top.Dom.selectById('mailTwoPanelLayout_SendMe_Check_All').setChecked(flag);
		Top.Dom.selectById('mailTwoPanelLayout_Spam_Check_All').setChecked(flag);
		Top.Dom.selectById('mailTwoPanelLayout_Trash_Check_All').setChecked(flag);
		
	}, setEvent__SearchIcon : function(){
		var id= ['Receive','Sent','Temp','SendMe','Spam','Trash'];
		for(var i=0; i<id.length; i++){
			Top.Dom.selectById('mailTwoPanelLayout_'+id[i]+'_Search_Text').setProperties({'on-keypress':'getEnter'});
			Top.Dom.selectById('mailTwoPanelLayout_'+id[i]+'_Search_Text').setProperties({'icon':'icon-work_search'});
			Top.Dom.selectById('mailTwoPanelLayout_'+id[i]+'_Search_Text').setProperties({'on-iconclick':'searchMail'});	
		}
	}, setOnClick__AddressIcon : function(){
		var id = ['Receive','Sent','SendMe','Spam','Trash','Temp'];
		for(var i=0; i<id.length; i++){
			Top.Dom.selectById('mailTwoPanelLayout_'+id[i]+'_AddressBook').setProperties({'on-click':Top.Controller.get('mailMainLeftLogic').openAddress}); 
			Top.Dom.selectById('mailTwoPanelLayout_'+id[i]+'_Setting').setProperties({'on-click':Top.Controller.get('mailMainLeftLogic').openSetting});
		}
	}, openFilterDialog : function(widget, event){
		var widget = event.id;
		var top = $('#' + widget).offset().top + 15 +'px';
		var left = $('#' + widget).offset().left+ 20 +'px';
		Top.Dom.selectById('mailFilterMenuDialog').setProperties({
			'layout-left': left,
			'layout-top': top
		});
		Top.Dom.selectById('mailFilterMenuDialog').open();
		
	}, filterMail : function(seeingTypeInput, sortingType){
		
		Top.Dom.selectById('mailTwoPanelLayout_Table').addClass('mail-loading');
		Top.Dom.selectById('mailTwoPanelLayout_Table').setProperties({'empty-message':'검색 중...'});
		mailDataRepo.setValue('mailReceivedBoxDataList',[]);
		
		if(mailData.curBox.FOLDER_ID == '')
			mailData.curBox.FOLDER_ID = 'AA';
		
		var folderType = mailData.curBox.FOLDER_TYPE;
		var parentFolderType = mailData.curBox.PARENT_FOLDER_TYPE
		
		if(folderType == 'MFL0001' || parentFolderType == 'MFL0001' || folderType == 'MFL0002' || folderType == '' || folderType == 'MFL0005' || parentFolderType == 'MFL0005' ||
				folderType == 'MFL0006' || folderType == 'MFL0007') {
			Top.Dom.selectById('mailTwoPanelLayout_Table').setColumnsVisible(1, false);
			Top.Dom.selectById('mailTwoPanelLayout_Table').setColumnsVisible(7, false);
		} else if(folderType == 'MFL0003' || parentFolderType == 'MFL0003') {
			Top.Dom.selectById('mailTwoPanelLayout_Table').setColumnsVisible(2, false);
		} else if(folderType == 'MFL0004' || parentFolderType == 'MFL0004') {
			Top.Dom.selectById('mailTwoPanelLayout_Table').setColumnsVisible(7, false);
		}
		Top.Dom.selectById('mailTwoPanelLayout_Table').setColumnsVisible(3, false);
		Top.Dom.selectById('mailTwoPanelLayout_Table').setColumnsVisible(8, false);
		
		var str_subject = "";
		var seeingTypeCode = seeingTypeInput;
		
		Top.Ajax.execute({
			
			type : "GET",
			url : _workspace.url + "Mail/Mail?action=ListGetbyFolder&CMFLG=FolderToMailList&FOLDER_ID=" + mailData.curBox.FOLDER_ID +
			"&ACCOUNT_ID=" + mailData.curBox.ACCOUNT_ID + "&USER_ID="+userManager.getLoginUserId()+
			'&SEEING_TYPE_CODE=' + seeingTypeCode + str_subject +
			'&CURRPAGE=' + mailData.curPage + '&PAGELENGTH=' + mailData.pageLength +'&SORTING='+sortingType+'&q='+new Date().getTime(),
			dataType : "json",
			crossDomain: true,
			contentType: "application/json",
		    xhrFields: {withCredentials: true},
			success : function(ret,xhr,status) {
				
//				Top.Controller.get('mailMainLeftLogic').seeingTypeCode = 'ALL';
				
				Top.Dom.selectById('mailTwoPanelLayout_Pagination').setProperties({'total':Math.ceil(ret.dto.dbio_total_count / mailData.pageLength),
					'page':mailData.curPage});
				if(ret.dto.INFO_ABOUT_MAIL) {
					for(var i=0; i<ret.dto.INFO_ABOUT_MAIL.length; i++) {
						
						ret.dto.INFO_ABOUT_MAIL[i] = mail.arrangeData(ret.dto.INFO_ABOUT_MAIL[i]);
//						var searchText = map.SUBJECT;
//						ret.dto.INFO_ABOUT_MAIL[i].SUBJECT = ret.dto.INFO_ABOUT_MAIL[i].SUBJECT.replace(searchText,'<span class="mail-search">'+searchText+'</span>');
//						
						if(ret.dto.INFO_ABOUT_MAIL[i].ATTACHMENT_COUNT != null)
							ret.dto.INFO_ABOUT_MAIL[i].CNT_ATTACHMENT = ret.dto.INFO_ABOUT_MAIL[i].ATTACHMENT_COUNT;
						
						var temp = (i+1)%5;
						var ran =  temp == 0 ? 5 : temp;
						if(ret.dto.INFO_ABOUT_MAIL[i].THUMB_PHOTO_ADDRESSBOOK == null){
							var temp__userId = ret.dto.INFO_ABOUT_MAIL[i].PROFILE_USER_ID;
							var temp__thumbPhoto = ret.dto.INFO_ABOUT_MAIL[i].THUMB_PHOTO;
							if(temp__userId == null){
								temp__userId = Math.floor(Math.random() * 10)  + "1111111-1111-1111-1111-" + Math.floor(Math.random() * 10);
								temp__thumbPhoto = null;
							}
							ret.dto.INFO_ABOUT_MAIL[i].SRC = userManager.getUserPhoto( temp__userId, null , temp__thumbPhoto);
						}else{
							ret.dto.INFO_ABOUT_MAIL[i].SRC = ret.dto.INFO_ABOUT_MAIL[i].THUMB_PHOTO_ADDRESSBOOK;
						}
						
						
						ret.dto.INFO_ABOUT_MAIL[i].CHECKBOX_VISIBLE = "none";
						ret.dto.INFO_ABOUT_MAIL[i].PROFILE_VISIBLE = "visible";
					}
					
					
					var columnOpt = {
							"4" : {
								event : {
									onCreated : function(index, data, nTd) {
										Top.Dom.selectById(nTd.children[0].id).setProperties({'on-click':'clickImportant'});
									}
								}
							}, "5" : {
								event: {
									onCreated : function(index, data, nTd) {
										Top.Dom.selectById(nTd.children[0].childs[1].id).setHTML(mailDataRepo.mailReceivedBoxDataList[Top.Dom.selectById('mailTwoPanelLayout_Table').getStartRowNum()+index].DISPLAY_NAME);
										
									}
								}
							},"7" : {
								event : {
									onCreated : function(index, data, nTd) {
										if(mailDataRepo.mailReceivedBoxDataList[Top.Dom.selectById('mailTwoPanelLayout_Table').getStartRowNum()+index].REC_MEM_INFO.length != 1
												&& mailData.curBox.WIDGET_ID.split('Text')[1].substr(0,1) == '0')
											Top.Dom.selectById(nTd.children[0].childs[1].id).setProperties({'text-color':'#09BFD5', 'class':'mail-subject'});
										else
											Top.Dom.selectById(nTd.children[0].childs[1].id).setProperties({'text-color':'#000000'});
									}
								}
							}
					};
	
					Top.Dom.selectById('mailTwoPanelLayout_Table').setProperties({"column-option": columnOpt});
					mailData.intervalTwo = setInterval(function() {
						Top.Controller.get('mailTwoPanelLogic').setAttrDragAndDrop();
					}, 1000);
				} else
					ret.dto.INFO_ABOUT_MAIL = [];
				
				mailDataRepo.setValue('mailReceivedBoxDataList',ret.dto.INFO_ABOUT_MAIL);
				Top.Dom.selectById('mailTwoPanelLayout_Table').setProperties({'empty-message':'검색 결과가 없습니다.'});
				Top.Dom.selectById('mailTwoPanelLayout_Table').removeClass('mail-loading');
				Top.Controller.get('mailTwoPanelLogic').setCheckAll(false);
				
			}
		});
		
		if(mailData.curBox.FOLDER_ID == 'AA')
			mailData.curBox.FOLDER_ID = '';
		
		
		
		
	}
	
});


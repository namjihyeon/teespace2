Top.Controller.create('mailReadButtonLogic', {
	init : function(event, widget) {
		
		
		
		Top.Dom.selectById('ReadMail_Center').src('mailReadLayout.html' + verCsp());
		
		
		var folder = mailData.curBox.DISPLAY_NAME;
		Top.Dom.selectById('mailMainLayout_FolderName').setText(folder);
		
		var notReadCount = mailData.curBox.UNREAD_COUNT;
		Top.Dom.selectById('mailMainLayout_NotReadCount').setText(notReadCount);
		
		var allCount = mailData.curBox.MAIL_COUNT;
		Top.Dom.selectById('mailMainLayout_AllCount').setText(allCount);
		
		
		this.setMoveFolder();

		Top.Dom.selectById('mailReadButtonLayout_ReceivedMail_SelectBox_Move').setProperties({'on-change':'moveFolder'});
		Top.Dom.selectById('mailReadButtonLayout_SentMail_SelectBox_Move').setProperties({'on-change':'moveFolder'});
		Top.Dom.selectById('mailReadButtonLayout_ToMeMail_SelectBox_Move').setProperties({'on-change':'moveFolder'});
		Top.Dom.selectById('mailReadButtonLayout_SpamMail_SelectBox_Move').setProperties({'on-change':'moveFolder'});
		Top.Dom.selectById('mailReadButtonLayout_TrashMail_SelectBox_Move').setProperties({'on-change':'moveFolder'});
		
		Top.Dom.selectById('mailReadButtonLayout_ReceivedMail').setProperties({'visible' : 'none'});
		Top.Dom.selectById('mailReadButtonLayout_SentMail').setProperties({'visible' : 'none'});
		Top.Dom.selectById('mailReadButtonLayout_TempMail').setProperties({'visible' : 'none'});
		Top.Dom.selectById('mailReadButtonLayout_ToMeMail').setProperties({'visible' : 'none'});
		Top.Dom.selectById('mailReadButtonLayout_SpamMail').setProperties({'visible' : 'none'});
		Top.Dom.selectById('mailReadButtonLayout_TrashMail').setProperties({'visible' : 'none'});
		
		if (mailData.curBox.DISPLAY_NAME == '전체 편지함'||mailData.curBox.FOLDER_TYPE == 'MFL0002'|| 
				mailData.curBox.FOLDER_TYPE == 'MFL0001' || mailData.curBox.PARENT_FOLDER_TYPE == 'MFL0001')
			Top.Dom.selectById('mailReadButtonLayout_ReceivedMail').setProperties({'visible' : 'visible'});
		else if (mailData.curBox.FOLDER_TYPE == 'MFL0003' || mailData.curBox.PARENT_FOLDER_TYPE == 'MFL0003')
			Top.Dom.selectById('mailReadButtonLayout_SentMail').setProperties({'visible' : 'visible'});
		else if (mailData.curBox.FOLDER_TYPE == 'MFL0004' || mailData.curBox.PARENT_FOLDER_TYPE == 'MFL0004')
			Top.Dom.selectById('mailReadButtonLayout_TempMail').setProperties({'visible' : 'visible'});
		else if (mailData.curBox.FOLDER_TYPE == 'MFL0005' || mailData.curBox.PARENT_FOLDER_TYPE == 'MFL0005')
			Top.Dom.selectById('mailReadButtonLayout_ToMeMail').setProperties({'visible' : 'visible'});
		else if (mailData.curBox.FOLDER_TYPE == 'MFL0006' || mailData.curBox.PARENT_FOLDER_TYPE == 'MFL0006')
			Top.Dom.selectById('mailReadButtonLayout_SpamMail').setProperties({'visible' : 'visible'});
		else if (mailData.curBox.FOLDER_TYPE == 'MFL0007' || mailData.curBox.PARENT_FOLDER_TYPE == 'MFL0007')
			Top.Dom.selectById('mailReadButtonLayout_TrashMail').setProperties({'visible' : 'visible'});
		
		
		this.sendAgainClicked; 
	
		this.searchSubject = null;
		
		Top.Dom.selectById('mailReadButtonLayout_ReceivedMail_ToThree_Icon').setProperties({'on-click':'moveToThreePanel'});
		Top.Dom.selectById('mailReadButtonLayout_SentMail_ToThree_Icon').setProperties({'on-click':'moveToThreePanel'});
		Top.Dom.selectById('mailReadButtonLayout_TempMail_ToThree_Icon').setProperties({'on-click':'moveToThreePanel'});
		Top.Dom.selectById('mailReadButtonLayout_ToMeMail_ToThree_Icon').setProperties({'on-click':'moveToThreePanel'});
		Top.Dom.selectById('mailReadButtonLayout_SpamMail_ToThree_Icon').setProperties({'on-click':'moveToThreePanel'});
		Top.Dom.selectById('mailReadButtonLayout_TrashMail_ToThree_Icon').setProperties({'on-click':'moveToThreePanel'});


		this.goToThreePanel; 
		
		this.__setOnClick__AddressIcon();
		
		Top.Dom.selectById('mailReadButtonLayout_TrashMail_Delete').setProperties({'on-click':function(){Top.Dom.selectById('mailDeletePermanentDialog').open();}});
		
	},__setOnClick__AddressIcon : function(){
		var id = ['ReceivedMail','SentMail','TempMail','ToMeMail','SpamMail','TrashMail'];
		for(var i=0; i<id.length; i++){
			Top.Dom.selectById('mailReadButtonLayout_'+id[i]+'_AddressBook').setProperties({'on-click':Top.Controller.get('mailMainLeftLogic').openAddress}); 
			Top.Dom.selectById('mailReadButtonLayout_'+id[i]+'_Setting').setProperties({'on-click':Top.Controller.get('mailMainLeftLogic').openSetting});
			
		}
	},__back__readMail : function(event, widget) {
		let sub = appManager.getSubApp();
		if(sub) Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + mailData.routeInfo.folderId +  "?sub=" + sub, {eventType:'fold'});
		else {
			mailData.isByBackListButton = true;
			Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + mailData.routeInfo.folderId , {eventType:'close'});
		}
		 
	}, clickReply : function(event, widget) {
		let sub = appManager.getSubApp();
		if(sub) Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + mailData.routeInfo.folderId + '/' + mailData.routeInfo.mailId + '/reply' + "?sub=" + sub, {eventType:'fold'});
		else Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + mailData.routeInfo.folderId + '/' + mailData.routeInfo.mailId + '/reply', {eventType:'close'});
	}, reply : function() {

		Top.Controller.get('mailReadButtonLogic').replyClicked = true ; 
        Top.Dom.selectById('mailMainLayout_Center_All').src('mailWriteLayout.html' + verCsp());

	}, clickReplyAll : function(event, widget) {
		let sub = appManager.getSubApp();
		if(sub) Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + mailData.routeInfo.folderId + '/' + mailData.routeInfo.mailId + '/replyall' + "?sub=" + sub, {eventType:'fold'});
		else Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + mailData.routeInfo.folderId + '/' + mailData.routeInfo.mailId + '/replyall', {eventType:'close'});
				
	}, replyall : function() {

		Top.Controller.get('mailReadButtonLogic').replyAllClicked = true ; 
        Top.Dom.selectById('mailMainLayout_Center_All').src('mailWriteLayout.html' + verCsp());
		
	}, clickForward : function(event, widget) {
		let sub = appManager.getSubApp();
		if(sub) Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + mailData.routeInfo.folderId + '/' + mailData.routeInfo.mailId + '/forward' + "?sub=" + sub, {eventType:'fold'});
		else Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + mailData.routeInfo.folderId + '/' + mailData.routeInfo.mailId + '/forward', {eventType:'close'});
		
	}, forward : function() {

		Top.Controller.get('mailReadButtonLogic').forwardClicked = true ; 
        Top.Dom.selectById('mailMainLayout_Center_All').src('mailWriteLayout.html' + verCsp());
		
	}, sendAgain : function() {
		let sub = appManager.getSubApp();
		if(sub) Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + mailData.routeInfo.folderId + '/' + mailData.routeInfo.mailId + '/resend' + "?sub=" + sub, {eventType:'fold'});
		else Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + mailData.routeInfo.folderId + '/' + mailData.routeInfo.mailId + '/resend', {eventType:'close'});
		
	}, resend : function() {
		
		Top.Controller.get('mailReadButtonLogic').sendAgainClicked = true;
        Top.Dom.selectById('mailMainLayout_Center_All').src('mailWriteLayout.html' + verCsp());
		
	}, modifyMail : function() {
		let sub = appManager.getSubApp();
		if(sub) Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + mailData.routeInfo.folderId + '/' + mailData.routeInfo.mailId + '/modify' + "?sub=" + sub, {eventType:'fold'});
		else Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + mailData.routeInfo.folderId + '/' + mailData.routeInfo.mailId + '/modify', {eventType:'close'});
		
	}, modify : function() {
		
		Top.Controller.get('mailReadButtonLogic').modifyClicked = true;
        Top.Dom.selectById('mailMainLayout_Center_All').src('mailWriteLayout.html' + verCsp());
		
	},
	clickedNextMail : function() {
		
		Top.Controller.get('mailReadLogic').clickedNextMail();
		
	},
	clickedPrevMail : function() {
		Top.Controller.get('mailReadLogic').clickedPrevMail();

		
	},
	clickDelete : function() {
			
		
			var data={
					dto: {
						"CMFLG"				: "DeleteMail",
						"dvio_total_count"	: 1,
						"ACCOUNT_ID"		: mailData.curBox.ACCOUNT_ID,
						"FOLDER_TYPE"		: "",
						"MAIL_ID"			: [Top.Controller.get('mailMainLeftLogic').clickedMail]
					}
			};
			

			Top.Ajax.execute({
				
				type : "POST",
				url : _workspace.url + "Mail/Mail?action=Delete",
				dataType : "json",
				data	 : JSON.stringify(data),
				crossDomain: true,
				contentType: "application/json",
			    xhrFields: {withCredentials: true},
				success : function(ret,xhr,status) {
					
					Top.Controller.get('mailReadButtonLogic').toList(); 				
				}
			});
		
		
		
	},
	setUnread : function (event, widget) {
		
		var seenCode;
		if(widget.getText() == '안읽음'){
			seenCode = "REA0002";
			widget.setText('읽음');
			}
		else{
			seenCode = "REA0001";
			widget.setText('안읽음');
			}

		var data={
				dto: {
					
					"CMFLG"				: "ChangeMailStatus",
					"dbio_total_count"	:"1",
					"ACCOUNT_ID"		: mailData.curBox.ACCOUNT_ID,
					"MAIL_MST_TYPE"		: "SEEN_YN_CODE",
					"MAIL_MST_VALUE"	: seenCode,
					"MAIL_ID"			: [Top.Controller.get('mailMainLeftLogic').clickedMail]
					
						}
		};
		


		Top.Ajax.execute({
			
			type : "POST",
			url : _workspace.url + "Mail/Mail?action=ChangeStatus",
			dataType : "json",
			data	 : JSON.stringify(data),
			crossDomain: true,
			contentType: "application/json",
		    xhrFields: {withCredentials: true},
			success : function(ret,xhr,status) {
				Top.Controller.get('mailMainLeftLogic').resetCount();

			}
		});
		
	},
	
	deleteForever : function () {
		
		var data={
				dto: {
					"CMFLG"				: "DeleteMail",
					"dvio_total_count"	: 1,
					"ACCOUNT_ID"		: mailData.curBox.ACCOUNT_ID,
					"FOLDER_TYPE"		: "MFL0007",
					"MAIL_ID"			: [Top.Controller.get('mailMainLeftLogic').clickedMail]
				}
		};
		

		Top.Ajax.execute({
			
			type : "POST",
			url : _workspace.url + "Mail/Mail?action=Delete",
			dataType : "json",
			data	 : JSON.stringify(data),
			crossDomain: true,
			contentType: "application/json",
		    xhrFields: {withCredentials: true},
			success : function(ret,xhr,status) {
				let sub = appManager.getSubApp();
				var folderId = mailData.curBox.FOLDER_ID;
				if(sub) Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + folderId +"?sub=" + sub, {eventType:'fold'});
				else Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + folderId, {eventType:'close'});
			}
		});
		
		
		
		
	},
	buttonAbled : function() {
		
		
	
		
	
			Top.Dom.selectById('mailReadButtonLayout_ReceivedMail_Reply').removeClass('disable-table-buttons');
			Top.Dom.selectById('mailReadButtonLayout_ReceivedMail_ReplyAll').removeClass('disable-table-buttons');
			Top.Dom.selectById('mailReadButtonLayout_ReceivedMail_Forward').removeClass('disable-table-buttons');
			Top.Dom.selectById('mailReadButtonLayout_ReceivedMail_Delete').removeClass('disable-table-buttons');
			Top.Dom.selectById('mailReadButtonLayout_ReceivedMail_Unread').removeClass('disable-table-buttons');
			
			Top.Dom.selectById('mailReadButtonLayout_ReceivedMail_Reply').addClass('table-buttons');
			Top.Dom.selectById('mailReadButtonLayout_ReceivedMail_ReplyAll').addClass('table-buttons');
			Top.Dom.selectById('mailReadButtonLayout_ReceivedMail_Forward').addClass('table-buttons');
			Top.Dom.selectById('mailReadButtonLayout_ReceivedMail_Delete').addClass('table-buttons');
			Top.Dom.selectById('mailReadButtonLayout_ReceivedMail_Unread').addClass('table-buttons');
			
			Top.Dom.selectById('mailReadButtonLayout_ReceivedMail_Reply').setProperties({'disabled':false});
			Top.Dom.selectById('mailReadButtonLayout_ReceivedMail_ReplyAll').setProperties({'disabled':false});
			Top.Dom.selectById('mailReadButtonLayout_ReceivedMail_Forward').setProperties({'disabled':false});
			Top.Dom.selectById('mailReadButtonLayout_ReceivedMail_Delete').setProperties({'disabled':false});
			Top.Dom.selectById('mailReadButtonLayout_ReceivedMail_Unread').setProperties({'disabled':false});
			Top.Dom.selectById('mailReadButtonLayout_ReceivedMail_SelectBox_Move').setProperties({'disabled':false});
	
			Top.Dom.selectById('mailReadButtonLayout_SentMail_Reply').removeClass('disable-table-buttons');
			Top.Dom.selectById('mailReadButtonLayout_SentMail_ReplyAll').removeClass('disable-table-buttons');
			Top.Dom.selectById('mailReadButtonLayout_SentMail_Forward').removeClass('disable-table-buttons');
			Top.Dom.selectById('mailReadButtonLayout_SentMail_Delete').removeClass('disable-table-buttons');
			
			Top.Dom.selectById('mailReadButtonLayout_SentMail_Reply').addClass('table-buttons');
			Top.Dom.selectById('mailReadButtonLayout_SentMail_ReplyAll').addClass('table-buttons');
			Top.Dom.selectById('mailReadButtonLayout_SentMail_Forward').addClass('table-buttons');
			Top.Dom.selectById('mailReadButtonLayout_SentMail_Delete').addClass('table-buttons');
			
			Top.Dom.selectById('mailReadButtonLayout_SentMail_Reply').setProperties({'disabled':false});
			Top.Dom.selectById('mailReadButtonLayout_SentMail_ReplyAll').setProperties({'disabled':false});
			Top.Dom.selectById('mailReadButtonLayout_SentMail_Forward').setProperties({'disabled':false});
			Top.Dom.selectById('mailReadButtonLayout_SentMail_Delete').setProperties({'disabled':false});
			Top.Dom.selectById('mailReadButtonLayout_SentMail_SelectBox_Move').setProperties({'disabled':false});
	
			Top.Dom.selectById('mailReadButtonLayout_ToMeMail_Forward').removeClass('disable-table-buttons');
			Top.Dom.selectById('mailReadButtonLayout_ToMeMail_Delete').removeClass('disable-table-buttons');
			Top.Dom.selectById('mailReadButtonLayout_ToMeMail_Modify').removeClass('disable-table-buttons');
			Top.Dom.selectById('mailReadButtonLayout_ToMeMail_Unread').removeClass('disable-table-buttons');

			
			Top.Dom.selectById('mailReadButtonLayout_ToMeMail_Forward').addClass('table-buttons');
			Top.Dom.selectById('mailReadButtonLayout_ToMeMail_Delete').addClass('table-buttons');
			Top.Dom.selectById('mailReadButtonLayout_ToMeMail_Modify').addClass('table-buttons');
			Top.Dom.selectById('mailReadButtonLayout_ToMeMail_Unread').addClass('table-buttons');

			
			Top.Dom.selectById('mailReadButtonLayout_ToMeMail_Forward').setProperties({'disabled':false});
			Top.Dom.selectById('mailReadButtonLayout_ToMeMail_Delete').setProperties({'disabled':false});
			Top.Dom.selectById('mailReadButtonLayout_ToMeMail_Modify').setProperties({'disabled':false});
			Top.Dom.selectById('mailReadButtonLayout_ToMeMail_Unread').setProperties({'disabled':false});
			Top.Dom.selectById('mailReadButtonLayout_ToMeMail_SelectBox_Move').setProperties({'disabled':false});
		
			Top.Dom.selectById('mailReadButtonLayout_SpamMail_DeleteForever').removeClass('disable-table-buttons');
			Top.Dom.selectById('mailReadButtonLayout_SpamMail_NotSpam').removeClass('disable-table-buttons');
			
			Top.Dom.selectById('mailReadButtonLayout_SpamMail_DeleteForever').addClass('table-buttons');
			Top.Dom.selectById('mailReadButtonLayout_SpamMail_NotSpam').addClass('table-buttons');
			
			Top.Dom.selectById('mailReadButtonLayout_SpamMail_DeleteForever').setProperties({'disabled':false});
			Top.Dom.selectById('mailReadButtonLayout_SpamMail_NotSpam').setProperties({'disabled':false});
			Top.Dom.selectById('mailReadButtonLayout_SpamMail_SelectBox_Move').setProperties({'disabled':false});
		
			Top.Dom.selectById('mailReadButtonLayout_TrashMail_DeleteForever').removeClass('disable-table-buttons');
			
			Top.Dom.selectById('mailReadButtonLayout_TrashMail_DeleteForever').addClass('table-buttons');
			
			Top.Dom.selectById('mailReadButtonLayout_TrashMail_DeleteForever').setProperties({'disabled':false});
			Top.Dom.selectById('mailReadButtonLayout_TrashMail_SelectBox_Move').setProperties({'disabled':false});
		
		
		
	},
	setMoveFolder : function() {
		
		var folders = [];
		

		Top.Ajax.execute({
			
			type : "GET",
			url : _workspace.url + "Mail/Mail?action=BasicInfo&CMFLG=BasicInfo&USER_ID="+userManager.getLoginUserId()+"&WS_USE_CODE=USE0001",
			dataType : "json",
			crossDomain: true,
			contentType: "application/json",
		    xhrFields: {withCredentials: true},
			success : function(ret,xhr,status) {
				
				for(var i=0; i<ret.dto.AccountList.length; i++) {
					if(ret.dto.AccountList[i].ACCOUNT_ID == mailData.curBox.ACCOUNT_ID) {
						for(var j=0; j<ret.dto.AccountList[i].FolderList.length; j++) {
							if(ret.dto.AccountList[i].FolderList[j].FOLDER_TYPE != 'MFL0006')
								folders.push({'text': ret.dto.AccountList[i].FolderList[j].DISPLAY_NAME
									,'value': ret.dto.AccountList[i].FolderList[j]});
						}
						
						break;
					}
				}
				
				folders.sort(function(a,b) {
					return a.value.FOLDER_TYPE.substr(6,1) - b.value.FOLDER_TYPE.substr(6,1);
				});
				
				receiveFolder = folders.splice(0,1);
				importantFolder = folders.splice(0,1);
				sentFolder = folders.splice(0,1);
				tempFolder = folders.splice(0,1);
				sendmeFolder = folders.splice(0,1);
				trashFolders = folders.splice(0,1);
				
				folders.forEach(function(data, index) {
					
					switch(data.value.PARENT_FOLDER_TYPE) {
					
					case 'MFL0001' :
						data.text='└ '+data.text
						receiveFolder.push(folders[index]);
						break;
					case 'MFL0002' :
						data.text='└ '+data.text
						importantFolder.push(folders[index]);
						break;
					case 'MFL0003' :
						data.text='└ '+data.text
						sentFolder.push(folders[index]);
						break;
					case 'MFL0004' :
						data.text='└ '+data.text
						tempFolder.push(folders[index]);
						break;
					case 'MFL0005' :
						data.text='└ '+data.text
						sendmeFolder.push(folders[index]);
						break;
					
					}
					
				});
			
				var newFolders1 = [];
				newFolders1 = newFolders1.concat(receiveFolder);
				newFolders1 = newFolders1.concat(sendmeFolder);
				newFolders1 = newFolders1.concat(trashFolders);
				
				var newFolders2 = [];
				newFolders2 = newFolders2.concat(sentFolder);
				newFolders2 = newFolders2.concat(sendmeFolder);
				newFolders2 = newFolders2.concat(trashFolders);
				
				var newFolders3 = [];
				newFolders3 = newFolders3.concat(receiveFolder);
				newFolders3 = newFolders3.concat(sentFolder);
				newFolders3 = newFolders3.concat(tempFolder);
				newFolders3 = newFolders3.concat(sendmeFolder);
				newFolders3 = newFolders3.concat(trashFolders);
				
				Top.Dom.selectById('mailReadButtonLayout_ReceivedMail_SelectBox_Move').setProperties({'nodes':newFolders1});
				Top.Dom.selectById('mailReadButtonLayout_SentMail_SelectBox_Move').setProperties({'nodes':newFolders2});
				Top.Dom.selectById('mailReadButtonLayout_ToMeMail_SelectBox_Move').setProperties({'nodes':newFolders1});
				Top.Dom.selectById('mailReadButtonLayout_SpamMail_SelectBox_Move').setProperties({'nodes':newFolders2});
				Top.Dom.selectById('mailReadButtonLayout_TrashMail_SelectBox_Move').setProperties({'nodes':newFolders3});
				
			}
		});
		
		
		
		
	}, moveFolder : function(event, widget) {
		
	
		var data={
		dto: {
				"MAIL_ID"		: [Top.Controller.get('mailMainLeftLogic').clickedMail],
				"ACCOUNT_ID"	: mailData.curBox.ACCOUNT_ID,
				"FOLDER_ID"		: widget.getValue().FOLDER_ID,
				"CMFLG"			: "FolderIdUpdate"
		
				}
		};
		

		Top.Ajax.execute({
			
			type : "POST",
			url : _workspace.url + "Mail/Mail?action=FolderIdUpdate",
			dataType : "json",
			data	 : JSON.stringify(data),
			crossDomain: true,
			contentType: "application/json",
		    xhrFields: {withCredentials: true},
			success : function(ret,xhr,status) {
				Top.Controller.get('mailMainLeftLogic').resetCount();
				TeeToast.open({'text':Top.i18n.get().value.m000157+mailIdList.length+Top.i18n.get().value.m000158+widget.getValue().DISPLAY_NAME+Top.i18n.get().value.m000159,'size':'min'});
				let sub = appManager.getSubApp();
				if(sub) Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + mailData.routeInfo.folderId + "?sub=" + sub, {eventType:'fold'});
				else Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + mailData.routeInfo.folderId , {eventType:'close'});

			}
		});
			
		
	},
	
	toList : function() {
		let sub = appManager.getSubApp();
		if(sub) Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + mailData.routeInfo.folderId + "?sub=" + sub, {eventType:'fold'});
		else Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + mailData.routeInfo.folderId , {eventType:'close'});
		

	},
	 searchMail : function(){
		 
		 Top.Controller.get('mailReadButtonLogic').searchSubject = Top.Dom.selectById('mailReadButtonLayout_Header_Search').getText();
		 Top.Dom.selectById('mailMainLayout_Center_All').src('mailTwoPanelLayout.html' + verCsp());
		
	 },
	 moveToThreePanel : function(){
		 mailData.panelType = '3';
		 Top.Controller.get('mailReadButtonLogic').goToThreePanel = true; 
		 Top.Dom.selectById('mailMainLayout_Center_All').src('mailThreePanelMainLayout.html' + verCsp());
	 },
	 deleteNotRead : function(){
		 
		 Top.Dom.selectById('mailDeleteNotReadDialog').open();
		
		 
	 },
	 saveEML : function(){
		/*
		 var data = {
			    "dto" : {
			    "MAIL_ID" : Top.Controller.get('mailMainLeftLogic').clickedMail,
			    "CMFLG" : "MailEMLDownload"
			    }
			}
		 
		 Top.Ajax.execute({
				
				type : "POST",
				url : _workspace.url+"Mail/Mail?action=EMLDownload",
				dataType : "json",
				data	 : JSON.stringify(data),
				crossDomain: true,
				contentType: "application/json",
			    xhrFields: {withCredentials: true},
			    headers: {
		              "ProObjectWebFileTransfer":"true"
		          },
				success : function(ret,xhr,status) {
					TeeAlarm.open({title: '', content: '하위하위'});
				}
			});
		 
		 */
	 }
})

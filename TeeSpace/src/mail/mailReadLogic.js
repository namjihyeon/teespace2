Top.Controller.create('mailReadLogic', {
	init : function(event, widget) {
		if(Top.Dom.selectById('mailThreePanelLayout_Write') != null || Top.Dom.selectById('mailThreePanelLayout_Write') != undefined)
			Top.Dom.selectById('mailThreePanelLayout_Write').setProperties({'visible':'none'});
		
		
		Top.Controller.get('mailMainLeftLogic').setTitle();
//		Top.Loader.stop();
		mailRead__hideLoadingBar();
		if(mailData.panelType == '2'){
			Top.Dom.selectById("mailReadLayout_Prev").setProperties({"visible":"visible"});
			Top.Dom.selectById("mailReadLayout_Next").setProperties({"visible":"visible"});
			Top.Dom.selectById('mailReadLayout_List').setProperties({'visible':'visible'});
		}
		else{
			Top.Controller.get('mailThreePanelMainLogic').replyClicked = false
			Top.Controller.get('mailThreePanelMainLogic').replyAllClicked  = false
			Top.Controller.get('mailThreePanelMainLogic').forwardClicked  = false
			Top.Controller.get('mailThreePanelMainLogic').sendAgainClicked  = false
			Top.Dom.selectById("mailReadLayout_Prev").setProperties({"visible":"none"});
			Top.Dom.selectById("mailReadLayout_Next").setProperties({"visible":"none"});
			Top.Dom.selectById('mailReadLayout_List').setProperties({'visible':'none'});
		}
		
		mailData.isByBackListButton = false;
		
		this.file = [];
		this.largeFile = [];
		this.fileClicked;
		
		this.receivedDate; 
		this.callMail();
		
		this.PrevMailID;
		this.NextMailID;
		this.clickedPrevSender;
		this.clickedNextSender;
		this.PrevSenderAddr;
		this.NextSenderAddr;
		
				Top.Dom.selectById('mailReadLayout_ListButton').setProperties({'on-click':'goList'});

		
	}, goList : function() {
		let sub = appManager.getSubApp();
		if(sub) 
			Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + mailData.routeInfo.folderId + "?sub=" + sub, {eventType:'fold'});
		else {
			mailData.isByBackListButton = true;
			Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + mailData.routeInfo.folderId , {eventType:'close'});
		}
		
	}, callMail : function() {
		
		var Receiver;
		var Cc;
		var BCc;
		var Time; 
		
		var readLayout
		if(mailData.panelType == '2')
			readLayout = 'div#mailMainLayout_Center_All';
		else
			readLayout = 'div#mailThreePanelLayout_Content';
//		Top.Loader.start({
//        	layout:function(loader){$(readLayout).prepend(loader);}
//        });
		mailRead__showLoadingBar();
		Top.Dom.selectById('mailReadLayout').setProperties({'visible':'none'});
		
		if(mailData.panelType == '3' && mailData.curBox.FOLDER_TYPE == "MFL0003" && Top.Dom.selectById('mailThreePanelLayout_Table').getClickedData().STATUS_ICON == 'visible') {
			Top.Dom.selectById('mailReadLayout_Status').setProperties({'visible':'visible'});
			Top.Dom.selectById('mailReadLayout_Status_Time').setProperties({'visible':'visible'});
			Top.Dom.selectById('mailReadLayout_Status_Time').setText(Top.Dom.selectById('mailThreePanelLayout_Table').getClickedData().STATUS);
		}
		if (mailData.curBox.DISPLAY_NAME == "전체 편지함") // (+)
			var callMailUrl = _workspace.url + "Mail/Mail?action=Read&CMFLG=ReadMail&MAIL_ID=" + Top.Controller.get('mailMainLeftLogic').clickedMail + "&ACCOUNT_ID="+mailData.curBox.ACCOUNT_ID + "&USER_ID="+userManager.getLoginUserId();
//			var callMailUrl = "http://222.122.50.87:83/CMS/" + "Mail/Mail?action=Read&CMFLG=ReadMail&MAIL_ID=" + Top.Controller.get('mailMainLeftLogic').clickedMail + "&ACCOUNT_ID="+mailData.curBox.ACCOUNT_ID + "&USER_ID="+userManager.getLoginUserId();
		else
			var callMailUrl = _workspace.url + "Mail/Mail?action=Read&CMFLG=ReadMail&MAIL_ID=" + Top.Controller.get('mailMainLeftLogic').clickedMail + "&USER_ID="+userManager.getLoginUserId();
		
			
		Top.Ajax.execute({ 
			
			type : "GET",
			url : callMailUrl,
			dataType : "json",
			crossDomain: true,
			contentType: "application/json",
			headers: {
	                "ProObjectWebFileTransfer":"true"
	            },
		    xhrFields: {withCredentials: true},
			success : function(ret,xhr,status) {
				
				Top.Controller.get('mailMainLeftLogic').resetCount();
				Top.Dom.selectById('mailReadLayout_Subject').setText(ret.dto.SUBJECT);
				
				if(mailData.curBox.DISPLAY_NAME != "전체 편지함" && mailData.curBox.FOLDER_TYPE == 'MFL0005' || 
						mailData.curBox.PARENT_FOLDER_TYPE == 'MFL0005'){					
					Top.Dom.selectById('mailReadLayout_Info_Sender').setVisible('none'); 
					Top.Dom.selectById('mailReadLayout_Info__ccInfo').setVisible('none'); 
				}
				
				if(ret.dto.THUMB_PHOTO_ADDRESSBOOK == null){
					if(Top.Controller.get('mailMainLeftLogic').clickedMailProfile == null){
						var temp = (i+1)%5;
						var ran =  temp == 0 ? 5 : temp;
						var temp__userId = ret.dto.PROFILE_USER_ID;
						var src;
						var temp__thumbPhoto = ret.dto.THUMB_PHOTO;
						if(temp__userId == null){
							temp__userId = Math.floor(Math.random() * 10)  + "1111111-1111-1111-1111-" + Math.floor(Math.random() * 10);
							temp__thumbPhoto = null;
						}
						src = userManager.getUserPhoto( temp__userId, null , temp__thumbPhoto);
						
						Top.Dom.selectById('mailReadLayout_Info_line1__profile__image').setProperties({'src': src});
					}
					else
						Top.Dom.selectById('mailReadLayout_Info_line1__profile__image').setProperties({'src': Top.Controller.get('mailMainLeftLogic').clickedMailProfile});

				}else{
					Top.Dom.selectById('mailReadLayout_Info_line1__profile__image').setProperties({'src':ret.dto.THUMB_PHOTO_ADDRESSBOOK});
				}		
			
				if(ret.dto.SENDER_NAME != null && ret.dto.SENDER_NAME.length != 0){
					ret.dto.SENDER_NAME = ret.dto.SENDER_NAME.split(';');
					
					var str1 = ret.dto.SENDER_NAME[0].substring(ret.dto.SENDER_NAME[0].indexOf("<")+1, ret.dto.SENDER_NAME[0].indexOf(">"));
					if (ret.dto.SENDER_NAME.length != "1"){
						var str2 = ret.dto.SENDER_NAME[1].substring(ret.dto.SENDER_NAME[1].indexOf("<")+1, ret.dto.SENDER_NAME[1].indexOf(">"));
					}
					if (str2 != undefined && str1 == str2){
						ret.dto.SENDER_NAME[1] = ret.dto.SENDER_NAME[1].replace('<', '&lt;');
						Top.Dom.selectById('mailReadLayout_Sender').setText(ret.dto.SENDER_NAME[1]);
					}		
					else{
						ret.dto.SENDER_NAME[0] = ret.dto.SENDER_NAME[0].replace('<', '&lt;');
						Top.Dom.selectById('mailReadLayout_Sender').setText(ret.dto.SENDER_NAME[0]);
					}
					
				}
				else 
					Top.Dom.selectById('mailReadLayout_Sender').setText('&lt;'+ ret.dto.SENDER_ADDR + '>');

					
					
				
				
				
				if (ret.dto.ReceiverList != null && ret.dto.ReceiverList.length != 0){
				
					Receiver = ret.dto.ReceiverList[0].DISPLAY_NAME.replace('<', '&lt;');
					for (var i = 1; i<ret.dto.ReceiverList.length; i++)
						{
						Receiver +=", " + ret.dto.ReceiverList[i].DISPLAY_NAME.replace('<', '&lt;');
						}
					Top.Dom.selectById('mailReadLayout_Info__receiveInfo__receiver').setText(Receiver); 
					
				}
				
				if (mailData.curBox.DISPLAY_NAME == '보낸 편지함' ||mailData.curBox.PARENT_FOLDER_TYPE == 'MFL0003')
					Top.Dom.selectById('mailReadLayout_HiddenCc_').setVisible('visible'); 
				
				if (ret.dto.CClist != null && ret.dto.CClist.length != 0){
										
					Cc = ret.dto.CClist[0].DISPLAY_NAME.replace('<', '&lt;');
					for (var i = 1; i<ret.dto.CClist.length; i++)
						{
						Cc +=", " + ret.dto.CClist[i].DISPLAY_NAME.replace('<', '&lt;');
						}
					if(Cc == '&lt;>')
						Top.Dom.selectById('mailReadLayout_Info__ccInfo__ccMember').setText("");
					else
						Top.Dom.selectById('mailReadLayout_Info__ccInfo__ccMember').setText(Cc);
				}
				
				if (ret.dto.BCCList != null && ret.dto.BCCList.length != 0){
					BCc = ret.dto.BCCList[0].DISPLAY_NAME.replace('<', '&lt;');
					for (var i = 1; i<ret.dto.BCCList.length; i++)
						{
						BCc +=", " + ret.dto.BCCList[i].DISPLAY_NAME.replace('<', '&lt;');
						}
					if(BCc == '&lt;>')
						Top.Dom.selectById('mailReadLayout_HiddenCc').setText("");
					else
						Top.Dom.selectById('mailReadLayout_HiddenCc').setText(BCc);
				}
				
				var content = ret.dto.CONTENT;
				content = content.replace(/<style/gi,'<!--style');
				content = content.replace(/<\/style>/gi,'<\/style-->');
				content = content.replace(/<xml/gi,'<!--xml');
				content = content.replace(/<\/xml>/gi,'<\/xml-->');
				if(ret.dto.CONTENT != null){
					if(ret.dto.CONTENT.substr(0,1) != "<")
						content = content.replace(/\n/g, "<br>");//행바꿈제거
				}
				if (content.replace(/(<([^>]+)>)/ig,"") == content)
					content = content.replace(/(?:\r\n|\r|\n)/g, '<br />');
				
				if(content.indexOf('<div class="input_editor sun-editor-id-wysiwyg sun-editor-editable" style="max-height: ') != -1)
					content = content.replace(/max-height: /gi, "");
				
				Top.Dom.selectById('mailReadLayout_Content_').setText(content);
				
				$('#mailReadLayout_Content_ a').attr('target', 'blank_'); // 링크 새창으로 열기
				
				if(ret.dto.SEND_IMPORTANCE_CODE == 'IMP0001')
					Top.Dom.selectById('mailReadLayout_Imp').setProperties({'visible' : 'visible'});
				else
					Top.Dom.selectById('mailReadLayout_Imp').setProperties({'visible' : 'none'});
				
				
			
				if (ret.dto.PrevMail != null && ret.dto.PrevMail.length != 0){
					
					if (ret.dto.PrevMail[0].SENDER != null){
						Top.Controller.get('mailReadLogic').PrevSenderAddr =  ret.dto.PrevMail[0].SENDER.split('<')[0] + '<'+ret.dto.PrevMail[0].SENDER_ADDR +'>';
						Top.Dom.selectById('mailReadLayout_Prev_Sender').setText(ret.dto.PrevMail[0].SENDER.split('<')[0]);
					}
					else {
						Top.Controller.get('mailReadLogic').PrevSenderAddr = '<'+ret.dto.PrevMail[0].SENDER_ADDR +'>';
						Top.Dom.selectById('mailReadLayout_Prev_Sender').setText('&lt;'+ret.dto.PrevMail[0].SENDER_ADDR +'>');
					}
						
					Top.Dom.selectById('mailReadLayout_Prev_Subject').setText(ret.dto.PrevMail[0].SUBJECT);
					
					Top.Controller.get('mailReadLogic').PrevMailID = ret.dto.PrevMail[0].MAIL_ID;
					
					if (ret.dto.PrevMail[0].HAS_ATTACHMENT_YN_CODE == "COM0001")
						Top.Dom.selectById('mailReadLayout_Prev_Attachment').setVisible('visible');
					
					
					Top.Dom.selectById('mailReadLayout_Prev_Time').setText(mail.changeTime(ret.dto.PrevMail[0].RECEIVED_TIME));
					
					
					
					if (ret.dto.PrevMail[0].SEEN_YN_CODE=="REA0001") {
						Top.Dom.selectById('mailReadLayout_Prev_Sender').setProperties({'textColor':'#6A6C71'});
						Top.Dom.selectById('mailReadLayout_Prev_Subject').setProperties({'textColor':'#6A6C71'});
					}
					else {
						Top.Dom.selectById('mailReadLayout_Prev_Sender').setProperties({'textColor':'#000000'});
						Top.Dom.selectById('mailReadLayout_Prev_Subject').setProperties({'textColor':'#000000'});
					}
				
				}
				else
					Top.Dom.selectById('mailReadLayout_Prev').setProperties({'visible':'none'});
				
				if (ret.dto.NextMail != null && ret.dto.NextMail.length != 0){
					if (ret.dto.NextMail[0].SENDER != null){
						Top.Controller.get('mailReadLogic').NextSenderAddr = ret.dto.NextMail[0].SENDER.split('<')[0] + '<'+ret.dto.NextMail[0].SENDER_ADDR +'>';
						Top.Dom.selectById('mailReadLayout_Next_Sender').setText(ret.dto.NextMail[0].SENDER.split('<')[0]);
					}
					else {
						Top.Controller.get('mailReadLogic').NextSenderAddr = '<'+ret.dto.NextMail[0].SENDER_ADDR +'>';
						Top.Dom.selectById('mailReadLayout_Next_Sender').setText('&lt;'+ret.dto.NextMail[0].SENDER_ADDR +'>');
					}

					Top.Dom.selectById('mailReadLayout_Next_Subject').setText(ret.dto.NextMail[0].SUBJECT);
					
					Top.Controller.get('mailReadLogic').NextMailID = ret.dto.NextMail[0].MAIL_ID;
	
					if (ret.dto.NextMail[0].HAS_ATTACHMENT_YN_CODE == "COM0001")
						Top.Dom.selectById('mailReadLayout_Next_Attachment').setVisible('visible');
					
					
					Top.Dom.selectById('mailReadLayout_Next_Time').setText(mail.changeTime(ret.dto.NextMail[0].RECEIVED_TIME));
					
				
					if (ret.dto.NextMail[0].SEEN_YN_CODE=="REA0001") {
						Top.Dom.selectById('mailReadLayout_Next_Sender').setProperties({'textColor':'#6A6C71'});
						Top.Dom.selectById('mailReadLayout_Next_Subject').setProperties({'textColor':'#6A6C71'});
					}
					else {
						Top.Dom.selectById('mailReadLayout_Next_Sender').setProperties({'textColor':'#000000'});
						Top.Dom.selectById('mailReadLayout_Next_Subject').setProperties({'textColor':'#000000'});
					}
			
				}
				else
					Top.Dom.selectById('mailReadLayout_Next').setProperties({'visible':'none'});
	
				Top.Controller.get('mailReadLogic').receivedDate = ret.dto.RECEIVED_TIME ; 
				
				var time = mail.changeTime(ret.dto.RECEIVED_TIME);
				Top.Dom.selectById('mailReadLayout_Time').setText(time);


				
				if(ret.dto.IMPORTANCE_YN_CODE === "PRI0001")
					Top.Dom.selectById('mailReadLayout_Importance').setProperties({'textColor':'#6C56E5'});
				else
					Top.Dom.selectById('mailReadLayout_Importance').setProperties({'textColor':'#C5C5C8'});
				
		    	Top.Dom.selectById('mailReadLayout_Attachment_List').clear();
		    	
		    	if (ret.dto.Attachment == null)
					ret.dto.Attachment = [];
				Top.Controller.get('mailReadLogic').showAttachments(ret.dto.Attachment);


		    	
//		    	Top.Controller.get('mailReadLogic').file = [];
//				if (ret.dto.Attachment != null && ret.dto.Attachment.length != 0){
//					
//					for (var i = 0; i <ret.dto.Attachment.length; i++){
////						if (ret.dto.Attachment[i].FILE_SIZE *1 < 10485760)
//							Top.Controller.get('mailReadLogic').file.push(ret.dto.Attachment[i]);
////						else
////							Top.Controller.get('mailReadLogic').largeFile.push(ret.dto.Attachment[i]);
//						}
//					
//					if (Top.Controller.get('mailReadLogic').file.length > 0){
//						Top.Dom.selectById('mailReadLayout_Attachment').setProperties({'visible' : 'visible'});
//						Top.Dom.selectById('mailReadLayout_Attachment_Count').setText(Top.Controller.get('mailReadLogic').file.length  +'개');
//						Top.Controller.get('mailReadLogic').showAttachment(Top.Controller.get('mailReadLogic').file);
//					}
//					
//					if (Top.Controller.get('mailReadLogic').largeFile.length > 0){
//						Top.Dom.selectById('mailReadLayout_LargeAttachment').setProperties({'visible' : 'visible'});
//						Top.Dom.selectById('mailReadLayout_LargeAttachment_Count').setText(Top.Controller.get('mailReadLogic').largeFile.length  +'개');
//						Top.Controller.get('mailReadLogic').showLargeAttachment(Top.Controller.get('mailReadLogic').largeFile);
//						
//						Top.Dom.selectById('mailReadLayout_LargeAttachment_Period').setText(ret.dto.RECEIVED_TIME.substr(0,10) + '~' + mail.changeTime(ret.dto.RECEIVED_TIME,'.'));
//						
//					}
//
//				}

//				Top.Loader.stop();
				mailRead__hideLoadingBar();
				Top.Dom.selectById('mailReadLayout').setProperties({'visible':'visible'});
				
				//제목, 버튼 영역
				var minusWidth = $("div#mailReadLayout_LinearLayout56").width() + "px";
				var calcWidth = "calc(100% - "+ minusWidth+")"
				Top.Dom.selectById("LinearLayout__title__buttons").setProperties({"layout-width":calcWidth});
				
				//제목 영역
				var minusWidth2 = $("top-linearlayout#mailReadLayout_LinearLayout_Buttons__").width()+ 20 + "px";
				var calcWidth2 = "calc(100% - "+ minusWidth2+")" 
				Top.Dom.selectById("mailReadLayout_LinearLayout47_1").setProperties({"layout-width":calcWidth2});
				
				
				

				if(mailData.panelType == '2')
					Top.Controller.get('mailReadLogic').showFolderState();
				else
					Top.Controller.get("mailThreePanelMainLogic").setMailCountAllNorRead();
				
			}, error :function(request, status, error) {
//				Top.Loader.stop();
				mailRead__hideLoadingBar();
			}
		});
		
	
		
	},
	clickedNextMail : function() {

		Top.Controller.get('mailMainLeftLogic').clickedMail = Top.Controller.get('mailReadLogic').NextMailID;
        
        let sub = appManager.getSubApp();
		if(sub) 
			Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + mailData.routeInfo.folderId + '/' + Top.Controller.get('mailMainLeftLogic').clickedMail + "?sub=" + sub, {eventType:'fold'});
		else 
			Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + mailData.routeInfo.folderId + '/' + Top.Controller.get('mailMainLeftLogic').clickedMail , {eventType:'close'});
		

	},
	clickedPrevMail : function() {
		
		Top.Controller.get('mailMainLeftLogic').clickedMail = Top.Controller.get('mailReadLogic').PrevMailID;
		let sub = appManager.getSubApp();
		if(sub) 
			Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + mailData.routeInfo.folderId + '/' + Top.Controller.get('mailMainLeftLogic').clickedMail + "?sub=" + sub, {eventType:'fold'});
		else 
			Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + mailData.routeInfo.folderId + '/' + Top.Controller.get('mailMainLeftLogic').clickedMail , {eventType:'close'});
		

	},
	clickStar : function () {
		
		var priority = (Top.Dom.selectById('mailReadLayout_Importance').getProperties('textColor') == '#C5C5C8') ? 'PRI0001' : 'PRI0002';
		var changeColor = (priority == "PRI0001") ? '#6C56E5' : '#C5C5C8';
		
		Top.Dom.selectById('mailReadLayout_Importance').setProperties({'textColor':changeColor});
		
		if(mailData.panelType == '3'){
			
			if(mailData.curBox.FOLDER_TYPE == "MFL0002") {
				
				let sub = appManager.getSubApp();
				if(sub) 
					Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + mailData.routeInfo.folderId + "?sub=" + sub, {eventType:'fold'});
				else 
					Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + mailData.routeInfo.folderId , {eventType:'close'});
				
			} else {
				for(var i=0; i<mailDataRepo.mailReceivedBoxDataList.length; i++){
					if(mailDataRepo.mailReceivedBoxDataList[i].MAIL_ID == Top.Controller.get('mailMainLeftLogic').clickedMail)
						var index = i;
				}
				if(index != undefined) {
					var widgetIndex = index-Top.Dom.selectById('mailThreePanelLayout_Table').getStartRowNum();
				    var widget = Top.Dom.selectById('mailThreePanelBoxLayout_isImportance_'+ widgetIndex +'_1')
				    if(widget != undefined)
				    	widget.setProperties({'text-color':changeColor});
					mailDataRepo.mailReceivedBoxDataList[index].IMPORTANCE_YN_CODE = changeColor;
				}
			}
			
		}
		
		
		var data={
				dto: {
					
					"CMFLG"				: "ChangeMailStatus",
					"dbio_total_count"	:"1",
					"ACCOUNT_ID"		: mailData.curBox.ACCOUNT_ID,
					"MAIL_MST_TYPE"		: "IMPORTANCE_YN_CODE",
					"MAIL_MST_VALUE"	: priority,
					"MAIL_ID"			: [Top.Controller.get('mailMainLeftLogic').clickedMail]
					
						}
		};
		


		Top.Ajax.execute({
			
			type : "POST",
			url : _workspace.url + "Mail/Mail?action=ChangeStatus",
			dataType : "json",
			data	 : JSON.stringify(data),
			crossDomain: true,
			contentType: "application/json",
		    xhrFields: {withCredentials: true},
			success : function(ret,xhr,status) {
				Top.Controller.get('mailMainLeftLogic').resetCount();
			}
		});
		
		
		
	},
	clickNextSender : function (event, widget) {
		Top.Controller.get('mailReadLogic').clickedNextSender = true; 
		mail.newMail();
	},
	clickPrevSender : function () {
		Top.Controller.get('mailReadLogic').clickedPrevSender = true;
		mail.newMail();
	},
	
	downloadAttach: function (event, widget)
	    {

		
			var widgetId = widget.id.split('_');
			var i = (widgetId[3] * 1);
			
			var successCallback = function(response){
			
			};
			var errorCallback = function(response){
				
			};
			
			storageManager.DownloadFile(Top.Controller.get('mailReadLogic').file[i].FILE_ID, Top.Dom.selectById('mailMainLayout_Button_SendMail').getValue(),workspaceManager.getMySpaceId(getRoomIdByUrl())
, successCallback, errorCallback);	
			
	    },
	    downloadLargeAttach: function (event, widget)
	    {

		
	    	Top.Loader.start({parentId:'mailReadLayout'}); 
			var widgetId = widget.id.split('_');
			var i = (widgetId[3] * 1);
			
			var successCallback = function(response){
				
			};
			var errorCallback = function(response){
				
			};
			
			
			storageManager.DownloadFile(Top.Controller.get('mailReadLogic').largeFile[i].FILE_ID, Top.Dom.selectById('mailMainLayout_Button_SendMail').getValue(),workspaceManager.getMySpaceId(getRoomIdByUrl())
, successCallback, errorCallback);	
			Top.Loader.stop({parentId:'mailReadLayout'});
	    },
	    showAttachment : function (file)
	    {
	    	var sizeAll = 0; 
	    	if (file.length == 1)
	    		Top.Dom.selectById('mailReadLayout_Attachment_SaveAll').setVisible('none');
	    	
			for (var i = 0; i < file.length; i++)
    		{
				
			sizeAll = sizeAll + (file[i].FILE_SIZE*1); 
			
    		var attachment = Top.Widget.create('top-linearlayout');
    		if (i == 0)
    			attachment.setProperties({'id':'mailReadLayout_Attachment_' + i, 'layout-height':'25px', 'layout-width':'74%', 'orientation':'horizontal', 'border-width':'1px' , 
    				'margin' : '0px 0px 0px 45px', 'line-height':'27px'});
    		else
    			attachment.setProperties({'id':'mailReadLayout_Attachment_' + i, 'layout-height':'25px', 'layout-width':'74%', 'orientation':'horizontal', 'border-width':'1px' , 
    				'margin' : '-1px 0px 0px 45px', 'line-height':'27px'});
	    	
    		var attachIconWrapper = Top.Widget.create('top-linearlayout'); 
    		attachIconWrapper.setProperties({'id':'mailReadLayout_Attachment_icon_wrapper_' + i, 'layout-height':'18px', 'layout-width':'18px', 'border-width':'0px' , 
	    		'margin' : '0px 0px 0px 8px', 'layout-vertical-alignment' : 'middle', 'background-color' : "#000000" });
    		if (file[i].USE_YN == "USE0002")
    			attachIconWrapper.setProperties({'background-color':'#CDCDCF'});
	    	var attachIcon = Top.Widget.create('top-icon');
	    	attachIcon.setProperties({'id':'mailReadLayout_Attachment_icon_' + i, 'className':'attachFile', 'layout-height':'wrap_content', 'layout-width':'wrap_content', 'border-width':'0px' , 
	    		'margin' : '0px 0px 0px 3px', 'class' : 'icon-work_download', 'text-size': '12px' , 'cursor' : 'pointer', 'layout-vertical-alignment' : 'middle', 'text-color' : "#FFFFFF"});
			
	    	var attachName = Top.Widget.create('top-textview');
	    	if (file[i].USE_YN == "USE0001")
	    		attachName.setProperties({'id':'mailReadLayout_Attachment_Name_' + i, 'className':'attachFile', 'layout-height':'wrap_content', 'layout-width':'wrap_content', 'max-width':'calc(100% - 90px)', 'border-width':'0px' , 'margin' : '0px 0px 0px 7px', 
	    			'text' :file[i].FILE_NAME +"."+ file[i].EXTENSION , 'text-size': '12px', 'class' : 'support-file-cursor-text', 'layout-vertical-alignment' : 'middle', 'text-color':'#38434D'});
	    	else if (file[i].USE_YN == "USE0002")
	    		attachName.setProperties({'id':'mailReadLayout_Attachment_Name_' + i, 'layout-height':'wrap_content', 'layout-width':'wrap_content', 'max-width':'calc(100% - 90px)', 'border-width':'0px' , 'margin' : '0px 0px 0px 7px', 
	    			'text' :file[i].FILE_NAME +"."+ file[i].EXTENSION , 'text-size': '12px', 'layout-vertical-alignment' : 'middle', 'text-color' : '#CDCDCF', 'class' : 'mail-del-file'});

	    	file[i].FILE_SIZE = mail.changeSize(file[i].FILE_SIZE * 1);
	    	var attachSize = Top.Widget.create('top-textview');
	    	if (file[i].USE_YN =="USE0001")
	    		attachSize.setProperties({'id':'mailReadLayout_Attachment_Size_' + i, 'layout-height':'wrap_content', 'layout-width':'wrap_content', 'border-width':'0px', 
	    			'text' :file[i].FILE_SIZE , 'text-size': '12px' ,'text-color' : '#808080', 'layout-vertical-alignment' : 'middle'});
	    	else if (file[i].USE_YN == "USE0002")
	    		attachSize.setProperties({'id':'mailReadLayout_Attachment_Size_' + i, 'layout-height':'wrap_content', 'layout-width':'wrap_content', 'border-width':'0px', 
	    			'text' :file[i].FILE_SIZE , 'text-size': '12px' ,'text-color' : '#CDCDCF', 'layout-vertical-alignment' : 'middle', 'class' : 'mail-del-file'});

	    	attachIconWrapper.addWidget(attachIcon);
	    	attachIconWrapper.complete(); 
	    	
	    	attachment.addWidget(attachIconWrapper);
	    	attachment.addWidget(attachName);
	    	attachment.addWidget(attachSize);
	    	attachment.complete();

	    	Top.Dom.selectById('mailReadLayout_Attachment_List').addWidget(attachment);
	    	Top.Dom.selectById('mailReadLayout_Attachment_List').complete();
	    	
			    
	    		if (file[i].USE_YN =="USE0001"){
	    			 Top.Dom.selectById('mailReadLayout_Attachment_Name_'+i).setProperties({'on-click' : 'downloadAttach'});
		    		 Top.Dom.selectById('mailReadLayout_Attachment_icon_'+i).setProperties({'on-click' : 'downloadAttach'});
	    		} else if(file[i].USE_YN == "USE0002"){
	    			Top.Dom.selectById('mailReadLayout_Attachment_Name_'+i).setProperties({'on-click' : function(){}});
	    			Top.Dom.selectById('mailReadLayout_Attachment_icon_'+i).setProperties({'on-click' : function(){}});
	    		}
	    		 
	    	
		  
    		}

			sizeAll = mail.changeSize(sizeAll);
			Top.Dom.selectById('mailReadLayout_Attachment_AllSize').setText('(' + sizeAll +')'); 


	    },
	    showLargeAttachment : function (file)
	    {
	    	var sizeAll = 0; 
	    	if (file.length == 1)
	    		Top.Dom.selectById('mailReadLayout_LargeAttachment_SaveAll').setVisible('none');
	    	
			for (var i = 0; i < file.length; i++)
    		{
				
			sizeAll = sizeAll + (file[i].FILE_SIZE*1); 
			
    		var attachment = Top.Widget.create('top-linearlayout');
    		if (i == 0)
    			attachment.setProperties({'id':'mailReadLayout_LargeAttachment_' + i, 'layout-height':'25px', 'layout-width':'74%', 'orientation':'horizontal', 'border-width':'1px' , 
    				'margin' : '0px 0px 0px 45px', 'line-height':'27px'});
    		else
    			attachment.setProperties({'id':'mailReadLayout_LargeAttachment_' + i, 'layout-height':'25px', 'layout-width':'74%', 'orientation':'horizontal', 'border-width':'1px' , 
    				'margin' : '-1px 0px 0px 45px', 'line-height':'27px'});
	    	
    		var attachIconWrapper = Top.Widget.create('top-linearlayout'); 
    		attachIconWrapper.setProperties({'id':'mailReadLayout_LargeAttachment_icon_wrapper_' + i, 'layout-height':'18px', 'layout-width':'18px', 'border-width':'0px' , 
	    		'margin' : '0px 0px 0px 8px', 'layout-vertical-alignment' : 'middle', 'background-color' : "#FF780A" });
    		if (file[i].USE_YN == "USE0002")
    			attachIconWrapper.setProperties({'background-color':'#CDCDCF'});
	    	var attachIcon = Top.Widget.create('top-icon');
	    	attachIcon.setProperties({'id':'mailReadLayout_LargeAttachment_icon_' + i,'className':'attachFile', 'layout-height':'wrap_content', 'layout-width':'wrap_content', 'border-width':'0px' , 
	    		'margin' : '0px 0px 0px 3px', 'class' : 'icon-work_download', 'text-size': '12px' , 'cursor' : 'pointer', 'text-color' : '#FFFFFF' });
			
	    	var attachName = Top.Widget.create('top-textview');
	    	if (file[i].USE_YN == "USE0001")
	    		attachName.setProperties({'id':'mailReadLayout_LargeAttachment_Name_' + i, 'className':'attachFile','layout-height':'wrap_content', 'layout-width':'wrap_content', 'border-width':'0px' , 'margin' : '0px 0px 0px 7px', 
	    			'text' :file[i].FILE_NAME +"."+ file[i].EXTENSION, 'text-size': '12px', 'class' : 'support-file-cursor-text', 'layout-vertical-alignment' : 'middle', 'text-color':'#38434D'});
	    	else if (file[i].USE_YN == "USE0002")
	    		attachName.setProperties({'id':'mailReadLayout_LargeAttachment_Name_' + i, 'layout-height':'wrap_content', 'layout-width':'wrap_content', 'border-width':'0px' , 'margin' : '0px 0px 0px 7px', 
	    			'text' :file[i].FILE_NAME +"."+ file[i].EXTENSION , 'text-size': '12px', 'layout-vertical-alignment' : 'middle', 'text-color' : '#CDCDCF', 'class' : 'mail-del-file'});

	    	file[i].FILE_SIZE = mail.changeSize(file[i].FILE_SIZE * 1);
	    	var attachSize = Top.Widget.create('top-textview');
	    	if (file[i].USE_YN =="USE0001")
	    		attachSize.setProperties({'id':'mailReadLayout_LargeAttachment_Size_' + i, 'layout-height':'wrap_content', 'layout-width':'wrap_content', 'border-width':'0px', 
	    			'text' :file[i].FILE_SIZE , 'text-size': '12px' ,'text-color' : '#808080', 'layout-vertical-alignment' : 'middle'});
	    	else if (file[i].USE_YN == "USE0002")
	    		attachSize.setProperties({'id':'mailReadLayout_LargeAttachment_Size_' + i, 'layout-height':'wrap_content', 'layout-width':'wrap_content', 'border-width':'0px', 
	    			'text' :file[i].FILE_SIZE , 'text-size': '12px' ,'text-color' : '#CDCDCF', 'layout-vertical-alignment' : 'middle', 'class' : 'mail-del-file'});

	    	attachIconWrapper.addWidget(attachIcon);
	    	attachIconWrapper.complete(); 
	    	
	    	attachment.addWidget(attachIconWrapper);
	    	attachment.addWidget(attachName);
	    	attachment.addWidget(attachSize);
	    	attachment.complete();
	    	
	    	Top.Dom.selectById('mailReadLayout_LargeAttachment_List').addWidget(attachment);
	    	Top.Dom.selectById('mailReadLayout_LargeAttachment_List').complete();
	    
	    		
	    		if (file[i].USE_YN =="USE0001" && file[i].EXPIRATION_DATE != "ATT1001"){
	    			 Top.Dom.selectById('mailReadLayout_LargeAttachment_Name_'+i).setProperties({'on-click' : 'downloadLargeAttach'});
		    		 Top.Dom.selectById('mailReadLayout_LargeAttachment_icon_'+i).setProperties({'on-click' : 'downloadLargeAttach'});
	    		} else {
	    			Top.Dom.selectById('mailReadLayout_LargeAttachment_Name_'+i).setProperties({'on-click' : function(){}});
	    			Top.Dom.selectById('mailReadLayout_LargeAttachment_icon_'+i).setProperties({'on-click' : function(){}});
	    		}
	    		 
    		}

			sizeAll = mail.changeSize(sizeAll);
			Top.Dom.selectById('mailReadLayout_LargeAttachment_AllSize').setText('(' + sizeAll +')'); 


	    },
	    
		downloadAll : function (event, widget)
		{
			var zip = new JSZip();
			var arrayFileName = new Array();
			var arrayFileUrl = new Array();
			var k = 0;
			var list = 0;
			
			var date = Top.Controller.get('mailReadLogic').receivedDate;
			date = date.split(' ')[0];
			date = date.replace(/\./gi, "").substring(2, 8); 
			
			var name = Top.Dom.selectById('mailReadLayout_Subject').getText() + '_' +date; 

			for (var i = 0; i < Top.Controller.get('mailReadLogic').file.length; i++){	
				var successCallback = function(response){
					if(response.result != "Ok"){
						return;
					}
					if(Top.Util.Browser.isIE()){
						arrayFileName[k] = response.fileName;
				        arrayFileUrl[k] = response.blobData;
						zip.file(arrayFileName[k], arrayFileUrl[k], {binary:true});
						k++;
					}else{
						let a = document.createElement("a");
				        
						//a.href = response.url;
				        //a.download = response.fileName;
				        arrayFileName[k] = response.fileName;
				        arrayFileUrl[k] = response.blobData;
				        zip.file(arrayFileName[k], arrayFileUrl[k], {binary:true});
				        k++;
								
					}
					 if(k == Top.Controller.get('mailReadLogic').file.length){
							zip.generateAsync({type:"blob"}, function updateCallback(metadata) {

							})

							.then(function callback(blob) {

								var str = arrayFileName[0].split('.');
								var FileName = name.concat('.zip');

								saveAs(blob, FileName);
								URL.revokeObjectURL(response.url);

							}, function (e) {

								showError(e);

							});
					URL.revokeObjectURL(response.url);
					
				}
				
				}
				var errorCallback = function(response){
					
				}
				storageManager.getDownloadFileInfo(Top.Controller.get('mailReadLogic').file[i].FILE_ID, Top.Dom.selectById('mailMainLayout_Button_SendMail').getValue(), workspaceManager.getMySpaceId(getRoomIdByUrl())
, successCallback, errorCallback)
				Top.Loader.stop({parentId:'mailReadLayout'});
			}
			
		},
		downloadLargeAll : function ()
		{
			
	    	Top.Loader.start({parentId:'mailReadLayout'});
			var zip = new JSZip();
			var arrayFileName = new Array();
			var arrayFileUrl = new Array();
			var k = 0;
			var list = 0;
			
			var date = Top.Controller.get('mailReadLogic').receivedDate;
			date = date.split(' ')[0];
			date = date.replace(/\./gi, "").substring(2, 8); 
			
			var name = Top.Dom.selectById('mailReadLayout_Subject').getText() + '_' +date; 


			for (var i = 0; i < Top.Controller.get('mailReadLogic').largeFile.length; i++){	
				var successCallback = function(response){
					if(response.result != "OK"){
						return;
					}
					if(Top.Util.Browser.isIE()){
						arrayFileName[k] = response.fileName;
				        arrayFileUrl[k] = response.blobData;
						zip.file(arrayFileName[k], arrayFileUrl[k], {binary:true});
						k++;
					}else{
						let a = document.createElement("a");
				        
						//a.href = response.url;
				        //a.download = response.fileName;
				        arrayFileName[k] = response.fileName;
				        arrayFileUrl[k] = response.blobData;
				        zip.file(arrayFileName[k], arrayFileUrl[k], {binary:true});
				        k++;
								
					}
					 if(k == Top.Controller.get('mailReadLogic').largeFile.length){
							zip.generateAsync({type:"blob"}, function updateCallback(metadata) {

							})

							.then(function callback(blob) {

								var str = arrayFileName[0].split('.');
								var FileName = name.concat('.zip');

								saveAs(blob, FileName);
								URL.revokeObjectURL(response.url);

							}, function (e) {

								showError(e);

							});
					URL.revokeObjectURL(response.url);
					
				}
				
				}
				var errorCallback = function(response){
				}
				storageManager.getDownloadFileInfo(Top.Controller.get('mailReadLogic').largeFile[i].FILE_ID, Top.Dom.selectById('mailMainLayout_Button_SendMail').getValue(),workspaceManager.getMySpaceId(getRoomIdByUrl())
, successCallback, errorCallback);
				Top.Loader.stop({parentId:'mailReadLayout'});

				
			}
			
		},
	
		showFolderState : function () { 
		
			Top.Ajax.execute({
				
				type : "GET",
				url : _workspace.url + "Mail/Mail?action=ListGetbyFolder&CMFLG=FolderToMailList&FOLDER_ID="+mailData.curBox.FOLDER_ID+"&ACCOUNT_ID=" + mailData.curBox.ACCOUNT_ID + '&SEEING_TYPE_CODE=' + 
				Top.Controller.get('mailMainLeftLogic').seeingTypeCode + '&CURRPAGE=1&PAGELENGTH=0',
				dataType : "json",
				crossDomain: true,
				contentType: "application/json",
			    xhrFields: {withCredentials: true},
				success : function(ret,xhr,status) {
					

					Top.Dom.selectById('mailMainLayout_AllCount').setText(ret.dto.dbio_total_count);
					Top.Dom.selectById('mailMainLayout_NotReadCount').setText(ret.dto.COUNT_NOT_READ);
					
					
				}
			});
		},
		
		
		foldInfo : function()
		{
			if(Top.Dom.selectById('mailReadLayout_Info__ccInfo').getProperties('visible') == 'visible'){
				Top.Dom.selectById('mailReadLayout_Info__ccInfo').setVisible('none');
				Top.Dom.selectById('mailReadLayout_Info__receiveInfo__foldable__icon').setProperties({'className':'icon-arrow_filled_down'});
			}
			else{
				Top.Dom.selectById('mailReadLayout_Info__ccInfo').setVisible('visible');
				Top.Dom.selectById('mailReadLayout_Info__receiveInfo__foldable__icon').setProperties({'className':'icon-arrow_filled_up'});
			}
				
			
		},
		toTop : function(){
			
			$(Top.Dom.selectById('mailReadLayout').getElement()).scrollTop(0);
		},
		foldAttachment : function(){
	
			if (Top.Dom.selectById('mailReadLayout_Attachment_List').getProperties('visible') == 'visible'){
				Top.Dom.selectById('mailReadLayout_Attachment_List').setVisible('none');
				Top.Dom.selectById('Icon102_1_1').setProperties({'className':'icon-accordion_select'});
			}
			else{
				Top.Dom.selectById('mailReadLayout_Attachment_List').setVisible('visible');
				Top.Dom.selectById('Icon102_1_1').setProperties({'className':'icon-accordion_unselect'});
			}
				
		},
		foldLargeAttachment : function () {
			
			if (Top.Dom.selectById('mailReadLayout_LargeAttachment_List').getProperties('visible') == 'visible'){
				Top.Dom.selectById('mailReadLayout_LargeAttachment_List').setVisible('none');
				Top.Dom.selectById('mailReadLayout_LargeAttachment_Period_').setVisible('none');
			}
			else{
				Top.Dom.selectById('mailReadLayout_LargeAttachment_List').setVisible('visible');
				Top.Dom.selectById('mailReadLayout_LargeAttachment_Period_').setVisible('visible');
			}

		},
		downloadExpiration : function() {
			
			TeeAlarm.open({title: '기간만료', content: '다운로드 가능한 기간이 지났습니다.'});
		},
		downloadExternalAttach : function(event, widget){
			
			var widgetId = widget.id.split('_');
			var i = (widgetId[3] * 1);
		
			
			var data =   {"dto" :
			{ "DOMailAttachment" : 
				[{"ATTACHMENT_ID" : Top.Controller.get('mailReadLogic').file[i].ATTACHMENT_ID} ]
			}
			};
			
			
			Top.Ajax.execute({ 
		           type : "POST",
		           url:_workspace.url+"Mail/Mail?action=DownloadAttachment",
		           dataType : "json",
		           data     : JSON.stringify(data),
		           crossDomain: true,
		           contentType: "application/json",headers: {
		               "ProObjectWebFileTransfer":"true"
		           },
		           xhrFields: {withCredentials: true},
		           success : function(response, ret,xhr,status) {
		        	   let a = document.createElement("a");
						a.href = "data:application/json;charset=utf8;base64," + response.files[0].contents; // 파일 내용 넣기 
						a.download = response.files[0].filename; // 파일 이름 넣기
						a.click();
						//URL.revokeObjectURL(response.url);
		           }
		       });
			
			
		},
		downloadExternalLargeAttach : function(event, widget){
			
			var widgetId = widget.id.split('_');
			var i = (widgetId[3] * 1);
		
			
			var data =   {"dto" :
			{ "DOMailAttachment" : 
				[{"ATTACHMENT_ID" : Top.Controller.get('mailReadLogic').largeFile[i].ATTACHMENT_ID} ]
			}
			};
			
			
			Top.Ajax.execute({ 
		           type : "POST",
		           url:_workspace.url+"Mail/Mail?action=DownloadAttachment",
		           dataType : "json",
		           data     : JSON.stringify(data),
		           crossDomain: true,
		           contentType: "application/json",headers: {
		               "ProObjectWebFileTransfer":"true"
		           },
		           xhrFields: {withCredentials: true},
		           success : function(response, ret,xhr,status) {
		        	   let a = document.createElement("a");
						a.href = "data:application/json;charset=utf8;base64," + response.files[0].contents; // 파일 내용 넣기 
						a.download = response.files[0].filename; // 파일 이름 넣기
						a.click();
						//URL.revokeObjectURL(response.url);
		           }
		       });
			
			
		},clickReply : function(event, widget) {
			let sub = appManager.getSubApp();
			if(sub) 
				Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + mailData.routeInfo.folderId + '/' + mailData.routeInfo.mailId + '/reply' + "?sub=" + sub, {eventType:'fold'});
			else 
				Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + mailData.routeInfo.folderId + '/' + mailData.routeInfo.mailId + '/reply' , {eventType:'close'});
		}, clickReplyAll : function(event, widget) {
			let sub = appManager.getSubApp();
			if(sub) 
				Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + mailData.routeInfo.folderId + '/' + mailData.routeInfo.mailId + '/replyall' + "?sub=" + sub, {eventType:'fold'});
			else 
				Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + mailData.routeInfo.folderId + '/' + mailData.routeInfo.mailId + '/replyall' , {eventType:'close'});
		}, clickForward : function(event, widget) {
			let sub = appManager.getSubApp();
			if(sub) 
				Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + mailData.routeInfo.folderId + '/' + mailData.routeInfo.mailId + '/forward' + "?sub=" + sub, {eventType:'fold'});
			else 
				Top.App.routeTo('/m/' + workspaceManager.getMySpaceUrl() + '/mail/' + mailData.routeInfo.folderId + '/' + mailData.routeInfo.mailId + '/forward' , {eventType:'close'});
			
		},
		showAttachments : function(attachments){
			
			 if(attachments.length > 0){
				Top.Dom.selectById('mailReadLayout_Info__attachInfo__foldable__count').setText(attachments.length); 
				Top.Dom.selectById('mailReadLayout_Info__attachInfo').setVisible('visible')
			 }
			
				attachments.forEach(function(e){
					
					var fileBox = document.createElement('div');
					fileBox.setAttribute('file_id', e.FILE_ID);
					fileBox.classList.add('mail-read-file-box');
					fileBox.addEventListener('click', function(e){ // 클릭시 파일 다운로드 
						var fileId = e.currentTarget.getAttribute('file_id');
						var successCallback = function(response){
							//
						};
						var errorCallback = function(response){
							//
						};
						
						storageManager.DownloadFile(fileId, Top.Dom.selectById('mailMainLayout_Button_SendMail').getValue(), workspaceManager.getMySpaceId(getRoomIdByUrl())
								, successCallback, errorCallback);	
						
					});
					fileBox.addEventListener('mouseenter', function(e){ // 호버시 아이콘 변경
						
						var iconContainer = e.currentTarget.children[0]; 
						iconContainer.style.display = 'none';
						
						var downloadIcon = e.currentTarget.querySelector('.mail-read-file-download');
						if(!downloadIcon){
							downloadIcon = document.createElement('div');
							downloadIcon.classList.add('icon-work_download');
							downloadIcon.classList.add('mail-read-file-download');
							e.currentTarget.insertBefore(downloadIcon, e.currentTarget.firstElementChild);
						}
					});
					fileBox.addEventListener('mouseleave', function(e){ // 호버시 아이콘 변경
						
						var downloadIcon = e.currentTarget.querySelector('.mail-read-file-download');
						downloadIcon.remove(); 
						var iconContainer = e.currentTarget.children[0]; 
						iconContainer.style.display = '';
						
						
					});
					
					
					var fileIcon = document.createElement('img');
					fileIcon.classList.add('mail-read-file-icon');
					fileIcon.setAttribute('src', fileExtension.getInfo(e.EXTENSION).iconImage);
					
					var fileInfo = document.createElement('div');
					fileInfo.classList.add('mail-read-file-info');
					
					var fileName = document.createElement('span');
					fileName.classList.add('mail-read-file-name');
					fileName.textContent = e.FILE_NAME + '.' + e.EXTENSION;
					
					var fileSize = document.createElement('span');
					fileSize.classList.add('mail-read-file-size');
					e.FILE_SIZE = mail.changeSize(e.FILE_SIZE * 1);
					fileSize.textContent = e.FILE_SIZE;
					
					fileInfo.appendChild(fileName);
					fileInfo.appendChild(fileSize);
					
					fileBox.appendChild(fileIcon);
					fileBox.appendChild(fileInfo);
					
					$('div#mailReadLayout_FileAttach').append(fileBox)

					// hover 이벤트 추가 및 다운로드 이벤트 추가 요망
					
					
				});
				
		},
		
		foldAttachments : function(event, widget){

			var container = Top.Dom.selectById('mailReadLayout_FileAttach');
			
			if(container.getProperties('visible') == 'none'){
				container.setVisible('visible');
				widget.setProperties({'class' : 'icon-arrow_filled_up'});
			}else{
				container.setVisible('none');
				widget.setProperties({'class' : 'icon-arrow_filled_down'});
			}
		}
	    
	
})

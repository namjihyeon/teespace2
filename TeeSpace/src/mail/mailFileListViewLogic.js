Top.Controller.create('mailFileListViewLogic', {
	init : function(event, widget) {
		
		var media = window.matchMedia("screen and (max-device-width: 1024px)");
		
		if(media.matches) {
			$('div#mailMainLayout_Left').removeClass('mobile-mail-visible-panel');
			$('div#mailMainLayout_Left').addClass('mobile-mail-invisible-panel');
			$('div#mailMainLayout_Center').removeClass('mobile-mail-invisible-panel');
			$('div#mailMainLayout_Center').addClass('mobile-mail-visible-panel');
			$('div#mailMainLayout_Center_All').removeClass('mobile-mail-invisible-panel');
			$('div#mailMainLayout_Center_All').addClass('mobile-mail-visible-panel');
			var mailFileListTableView = Top.Dom.selectById('mailFileListTableView');
			mailFileListTableView.setColumnsVisible(3, false);
			mailFileListTableView.setColumnsVisible(5, false);
		}
		
		this.callFileList();
		
		
	}, callFileList : function(event, widget) {
		
		let treeNodes;
		Top.Ajax.execute({
	           type: "GET",
	           url: _workspace.url + "Storage/StorageMetaList?workspaceID=" + workspaceManager.getMySpaceId() + "&channelID=" + mailData.curBox.ACCOUNT_ID,
	           dataType: "json",
	           data: JSON.stringify(),
	           crossDomain: true,
	           contentType: "application/json; charset=utf-8",
	           xhrFields: { withCredentials: true },
	           success: function(ret, xhr, status) {
	        	   
	        	   Top.Dom.selectById("fileDownloadBtn_1").setDisabled(true);
		           Top.Dom.selectById("fileRemoveBtn_1").setDisabled(true);
		       	   Top.Dom.selectById("checkAllCheckBox_1").setChecked(false);
	        	   
	        	   treeNodes = ret.dto;
	        	   
	        	   if(treeNodes == undefined || treeNodes.resultMsg != "Success") {
	                   let errMsg = "파일 목록을 받아오지 못했습니다.";
	                   TeeAlarm.open({title: '', content: errMsg});
	                   Top.Loader.stop({parentId:'mailFileListViewLayout'});
	                   return;
	       		}
	       		
	        	   let allFileArray = [];
	        	   if(treeNodes.storageFileInfoList == null) {
		       			mailFileListDataRepo.setValue("fileList", allFileArray);
		       			return;
		       		}
	        	   
	       		var node_num =  treeNodes.storageFileInfoList.length;
	               
	               for(var i = 0 ; i < node_num ; i++){
	                   var node = treeNodes.storageFileInfoList[i];
	                   var newData = {
	                       file_id                    : node.file_id,
	                       file_name                  : node.file_name,
	                       file_extension             : node.file_extension == null ? "" : node.file_extension,
	                       file_size                  : node.file_size,
	                       file_created_at            : node.file_created_at,
	                       file_owner				  : userManager.getUserInfo(node.user_id).USER_NAME,
	                       is_file					  : true,
	                       
	                       file_updated_at            : getFileDateFormat(node.file_updated_at),
	                       
	                       file_removed_by            : "",
	                       file_removed_at            : ""
	                   }
	                   
	                   newData.fileFullName = newData.file_extension == "" ? newData.file_name : (newData.is_file ? newData.file_name + "." + newData.file_extension : newData.file_name); 
	                   
	                   switch(newData.file_status) {
	                       case(FileStatus.FAVORITED):
	                       case(FileStatus.NORMAL):
	                           if(!newData.is_file && treeViewObj.getNode(newData.file_parent_id) != undefined) {
	                               treeViewObj.addNodes(newData.file_parent_id, { 
	                                   id   : newData.file_id, 
	                                   text : newData.file_name,
	                                   icon : "icon-work_folder",
	                                   file_name        : newData.file_name,
	                               });
	                           }
	                           break;
	                       case(FileStatus.REMOVED):
	                           let removedFile = treeNodes.fileDeletedList.find(function(e) { return e.file_id == newData.file_id; });
	                           newData.file_removed_by = getFileDateFormat(removedFile.file_deleted_by);
	                           newData.file_removed_at = getFileDateFormat(removedFile.file_deleted_at);
	                           break;
	                   }           
	           
	                   allFileArray.push(newData);
	               }
	               
	               // 티드라이브 파일을 TScheduleFileRepo로
	               mailFileListDataRepo.setValue("fileList", allFileArray);

	               Top.Dom.selectById("mailFileListTableView").checkAll(false);
	               
	               Top.Loader.stop({parentId:'mailFileListViewLayout'});
	       		
	       	    var fileContentsTableView = Top.Dom.selectById("mailFileListTableView");
	       	    fileContentsTableView.setProperties({
	       			"column-option" : {
	       			    "1" : {
	       			        layout : function(data, idx) {
	       			            let dataIdx = Top.Dom.selectById("mailFileListTableView").getStartRowNum() + idx;
	       			            let curRowData = mailFileListDataRepo.fileList[dataIdx];
	       			            let className = getFileIconClass(curRowData.file_extension);
	       			            let fileExt = curRowData.file_extension.toLowerCase() ;
	       			            return '<top-icon class="' + className + '" tooltip="' + fileExt + '"/>';
	       			        }
	       			    },
	       			    "2" : {
	       			        layout : function(data, idx) {
	       						let dataIdx = Top.Dom.selectById("mailFileListTableView").getStartRowNum() + idx;
	       			            let curRowData = mailFileListDataRepo.fileList[dataIdx];
	       			            return '<top-textview class="file-full-name" text="' + curRowData.file_name + '" tooltip="' + curRowData.file_name + '" style="cursor: pointer" on-click="onFileFullNameClick"/>';
	       			        }
	       			    },
	       				"3" : {
	       				    layout : function(data, idx) {
	       				        let fileSizeText = data == 0 ? '—' : getFileSizeText(data);
	       				        let fileSizeTextTooltip = fileSizeText;
	       				        return '<top-textview text="' + fileSizeText + '" tooltip="' + fileSizeTextTooltip + '" style="cursor: default"/>';
	       			        } 
	       			    },
	                       "4" : {
	                           layout : function(data, idx) {
	                               if(data == "") {
	                                   return '<top-linearlayout orientation="horizontal" border-width="0" style="cursor: default">'
	                                        +      '<div class="new-file-loader" style="display: inherit"></div>'
	                                        +      '<top-textview text="업로드 중..." style="margin-left: 10px"></top-textview>'
	                                        + '</top-linearlayout>'
	                               } else {
	                                   return '<top-textview text="' + data + '" tooltip="' + data + '" style="cursor: default"/>';
	                               }                        
	                           }
	                       },
	                       "5" : {
	                           layout : function(data, idx) {
	                               return '<top-textview text="' + data + '" tooltip="' + data + '" style="cursor: default"/>';
	                           }
	                       }
	       			}
	       		});
            }});
		
		// 서버에서 FileList 가져오기

		
	    
		
		
	},
	onAllCheckChanged : function(event, widget) {
		this.allCheckChanged(widget.isChecked());
	},
    allCheckChanged : function(isChecked) {
		Top.Dom.selectById("mailFileListTableView").checkAll(!isChecked);
		Top.Dom.selectById("fileRemoveBtn_1").setDisabled(!isChecked ? false : true);
		
		let folderExist = Top.Dom.selectById("mailFileListTableView").getCheckedData().filter(function(e) { return !e.is_file; } ).length > 0;
		Top.Dom.selectById("fileDownloadBtn_1").setDisabled(!isChecked && !folderExist ? false : true);
    },

	onDownloadFile : function(event, widget) {
		let selectedFiles = Top.Dom.selectById("mailFileListTableView").getCheckedData();
        
        if(selectedFiles.length >= DOWNLOAD_FILE_ZIP_THRESHOLD) {
            this.downloadFilesByZip(selectedFiles);
        } else {
            for(var i=0; i<selectedFiles.length; i++) {
                this.downloadFile(selectedFiles[i].file_id);
            }
        }
	},
    downloadFile : function(fileId) {
    	Top.Loader.start({parentId:'mailFileListViewLayout'});
        var successCallback = function(response) {
        	Top.Loader.stop({parentId:'mailFileListViewLayout'});
        };
        
        var errorCallback = function(response) {
        	Top.Loader.stop({parentId:'mailFileListViewLayout'});
        };
        
        storageManager.DownloadFile(fileId, mailData.curBox.ACCOUNT_ID, successCallback, errorCallback);
        Top.Loader.stop({parentId:'mailFileListViewLayout'});
    },
    downloadFilesByZip : function(selectedFiles) {
    	Top.Loader.start({parentId:'mailFileListViewLayout'});
        
        var zip = new JSZip();
        var targetFiles = selectedFiles;
        var fileNames = [];
        var fileDatas = [];
        var responseCount = 0;
        var successCallback = function(response) {
            try {
                if(response.result != "OK") {
                    return;
                }
    
                fileNames[responseCount] = response.fileName;
                fileDatas[responseCount] = response.blobData;
                zip.file(fileNames[responseCount], fileDatas[responseCount], {binary:true});
            } finally {
                URL.revokeObjectURL(response.url);
                responseCount++;

                if(responseCount == targetFiles.length) {
                    zip.generateAsync({type:"blob"}, function updateCallback(metadata) {  }).then(function callback(blob) {
                        saveAs(blob, "TmaxCloudSpace.zip");
                    }, function (e) {
 	                   TeeAlarm.open({title: '', content: e});
                    });
                    Top.Loader.stop({parentId:'mailFileListViewLayout'});
                }
            }
        }
             
        var errorCallback = function(response) {
            responseCount++;
            
            if(responseCount == targetFiles.length) {
            	Top.Loader.stop({parentId:'mailFileListViewLayout'});
            }
        }
        
        for(var i=0; i<targetFiles.length; i++) {
            storageManager.getDownloadFileInfo(targetFiles[i].file_id, mailData.curBox.ACCOUNT_ID, successCallback, errorCallback)
        }        
    },
    allRemoveFile : function(event, widget) {
    	Top.Ajax.execute({
	           type: "GET",
	           url: _workspace.url + "Storage/StorageMetaList?workspaceID=" + workspaceManager.getMySpaceId() + "&channelID=" + mailData.curBox.ACCOUNT_ID,
	           dataType: "json",
	           data: JSON.stringify(),
	           crossDomain: true,
	           contentType: "application/json; charset=utf-8",
	           xhrFields: { withCredentials: true },
	           success: function(ret, xhr, status) {
					console.log(ret.dto);
					if(ret.dto.storageFileInfoList != null && ret.dto.storageFileInfoList.length>0){
						let selectedFiles = ret.dto.storageFileInfoList;
						
						for(i = 0; i<selectedFiles.length; i++){
							Top.Controller.get('mailFileListViewLogic').mail_DeleteFile(selectedFiles[i].file_id);
						}
					}
	            }
    	});
    	Top.Dom.selectById('mailDeleteTrashDialog').close();
	},
	onRemoveFile : function(event, widget) {
		let selectedFiles = Top.Dom.selectById("mailFileListTableView").getCheckedData();
		
		for(i = 0; i<selectedFiles.length; i++){
		Top.Controller.get('mailFileListViewLogic').mail_DeleteFile(selectedFiles[i].file_id);
		}
	},

	onCheckChanged : function(event, widget) {
		
		var fileContentsTableView = Top.Dom.selectById("mailFileListTableView");
		
		let numOfChecked = fileContentsTableView.getCheckedIndex().length;
		Top.Dom.selectById("fileRemoveBtn_1").setDisabled(numOfChecked > 0 ? false : true);
		Top.Dom.selectById("checkAllCheckBox_1").setChecked(fileContentsTableView.RowCount() == numOfChecked ? true : false);

		let numOfCheckedFile = fileContentsTableView.getCheckedData().filter(function(e) { return e.is_file; } ).length;
		Top.Dom.selectById("fileDownloadBtn_1").setDisabled((numOfChecked > 0 && numOfChecked == numOfCheckedFile) ? false : true);
			
	},
	onHeaderClick : function(event, widget) {
		
	},
	onItemClick : function(event, widget) {
		
	},
	
	onFileFullNameClick : function(event, widget) {
		let fileMeta = Top.Dom.selectById("mailFileListTableView").getSelectedData();
	    if(fileMeta.file_updated_at == "") {
            return;
        }
	    
	    if(fileMeta.is_file) {
	        this.previewFile(fileMeta)
	    } else {
	        this.openFolder(fileMeta.file_id);
	    }
	    
	    event.stopPropagation();
	},

	previewFile : function(fileMeta) {
        //Top.Controller.get("filePreviewLayoutLogic").previewFileMeta = fileMeta;
		//Top.Dom.selectById("filePreviewDialog").open();
		
		let fileExt = fileMeta.file_extension.toLowerCase();
	    let contentType = getContentTypeByFileExtension(fileExt);
	    
	    var isImageViewSupport = contentType == "image/jpeg" ||
	                             contentType == "image/gif"  ||
	                             contentType == "image/png"  ||
	                             fileExt     == "bmp";
	    var isVideo = contentType.substring(0,6).toLowerCase() == "video/";
	    var isOffice = isOfficeFile(fileExt);
	    if(!isImageViewSupport && !isVideo && !isOffice)
        	TeeAlarm.open({title: '', content: '해당 파일은 미리보기를 제공하지 않습니다.'});
	    else {
            var successCallback = function(result) {
                if(isImageViewSupport) {
                    Top.Controller.get("mailPreviewPopupLayoutLogic").imageSrc = result.url;
                } else if(isVideo) {
                    Top.Controller.get("mailPreviewPopupLayoutLogic").videoSrc = result.url;
                } else if(isOffice){
                	TeeToast.open({'text':'이미지/동영상만 미리보기 가능합니다','size':'min'});
                	return;
    				Top.Controller.get("mailPreviewPopupLayoutLogic").webSrc = result.url;
                }

                Top.Controller.get("mailPreviewPopupLayoutLogic").fileID = fileMeta.file_id;
                Top.Controller.get("mailPreviewPopupLayoutLogic").fileExt = fileExt;
                Top.Controller.get("mailPreviewPopupLayoutLogic").fileName = fileMeta.file_name;
				Top.Dom.selectById("MailFilePreviewDialog").open();
            }
            var errorCallback = function(result) {
            	TeeAlarm.open({title: '', content: result.message});
            }
            storageManager.getDownloadFileInfo(fileMeta.file_id, mailData.curBox.ACCOUNT_ID, successCallback, errorCallback);
	    }
	},
	onSecondaryClick : function(event, widget) {
		
	},
	onPageLengthChange : function(event, widget) {
		
	},
	onRowContextMenuOpen : function(event, widget) {
		
	},
	moveFolderList : function() {
		 
		$('div#mailMainLayout_Left').removeClass('mobile-mail-invisible-panel');
		$('div#mailMainLayout_Center_All').removeClass('mobile-mail-visible-panel');
		$('div#mailMainLayout_Left').addClass('mobile-mail-visible-panel');
		
	},
	mail_DeleteFile : function(file_id) {
		
		let data = {
				"dto" : {
					'workspace_id' : workspaceManager.getMySpaceId(),
					'channel_id' : mailData.curBox.ACCOUNT_ID,
					'storageFileInfo' : {
						"user_id": "",
				        "file_last_update_user_id": "",
				        "file_id": file_id,
				        "file_name": "",
				        "file_extension": "",
				        "file_created_at": "",
				        "file_updated_at": "",
				        "user_context_1": "",
				        "user_context_2": "",
				        "user_context_3": ""
					}
				}
		}
		Top.Ajax.execute({
	        type: "DELETE",
	        url: _workspace.url + "Storage/StorageFile",
	        dataType: "json",
	        data: JSON.stringify(data),
	        crossDomain: true,
	        contentType: "application/json; charset=utf-8",
	        xhrFields: { withCredentials: true },
	        success: function(ret, xhr, status) {
	        	Top.Controller.get('mailFileListViewLogic').callFileList();
	        	
	        }
	        });
		
	}
});

const AddFriends = {
	tab: mobx.observable.box('tab-contact')
};

const FriendsAPI = {
	AddFriend: function (USER_ID, FRIEND_USER_ID) {
		 return axios.post( _workspace.url + 'Users/AddFriend?action=', {
			 dto: {
				USER_ID: USER_ID,
				FRIEND_ID: FRIEND_USER_ID
			 }
		 });
	},
	AddFriendWithPhone: function (opt = {
			USER_PHONE: '', USER_ID: '', NATIONAL_CODE: '', FRIEND_NICK: null
	}) {
		return axios.post( _workspace.url + 'Users/AddFriendwithPhone?action=', {
			dto: opt
		});
	},
};
AddFriendsDialog = function () {
	let config = {};
	function AddFriendsDialog() {
		const self = this;
	
		this.config = config;
		
		const template = htmlToElement(`
			<div id="add-friends-dialog">
				<div id="add-friends-header">
				</div>
				<div id="add-friends-content">
				</div>
			</div>
		`);
		
		const contact = this.AddFriendsByContact();
		const idSearch = this.AddFriendsByIdSearch();
		const recommendation = this.AddFriendsByRecommendation();
		const invitation = this.AddFriendsByInvitation();
		let organization = null;
		
		if (isb2c()) {
			AddFriends.tab.set('tab-contact');
		} else {
			AddFriends.tab.set('tab-organization');

			organization = this.AddFriendsByOrganization();
		}
		
		mobx.reaction(
			() => AddFriends.tab.get(),
			tabId => {

	        const firstChild = template.querySelector('#add-friends-content').firstChild;
	        
	        switch (tabId) {
	        case 'tab-contact':
	        	template.querySelector('#add-friends-content').replaceChild(contact, firstChild);
	        	break;
	        case 'tab-id':
	        	template.querySelector('#add-friends-content').replaceChild(idSearch, firstChild);
	        	break;
	        case 'tab-friend-recommendation':
	        	template.querySelector('#add-friends-content').replaceChild(recommendation, firstChild);
	        	break;
	        case 'tab-invite-user':
	        	template.querySelector('#add-friends-content').replaceChild(invitation, firstChild);
				break;
			case 'tab-organization':
				template.querySelector('#add-friends-content').replaceChild(organization, firstChild);
				break;
	        }
	       
	        AddFriendsDialog.center();
		}, 
		{
			fireImmediately: true,
		});
		
		template.querySelector('#add-friends-header').appendChild(this.AddFriendsTab());

		this.template = template;
		
		return this;
	}
	
	AddFriendsDialog.prototype.AddFriendsTab = function () {
		let template = null;
		
		if (isb2c()) {
			template = htmlToElement(`
				<div id="add-friends-tabs">
					<div class="add-friends-tab selected" id="tab-contact">
						연락처로 추가
					</div>
					<div class="add-friends-tab" id="tab-id">
						아이디 검색
					</div>
					<div class="add-friends-tab" id="tab-friend-recommendation">
						추천 프렌즈
					</div>
					<div class="add-friends-tab" id="tab-invite-user">
						초대장 보내기
					</div>
				</div>
			`);
		} else {
			template = htmlToElement(`
				<div id="add-friends-tabs">
					<div class="add-friends-tab selected" id="tab-organization">
						조직도 조회
					</div>
					<div class="add-friends-tab" id="tab-id">
						아이디 검색
					</div>
				</div>
			`);
		}
		
		template.addEventListener('click', function (e) {
			if (template.querySelector('.selected')) {
				template.querySelector('.selected').classList.remove('selected');
			}
			e.target.classList.add('selected');
			
			 AddFriends.tab.set(e.target.getAttribute('id'));
		});
		
		return template;
	};

	AddFriendsDialog.prototype.AddFriendsByContact = function () {
		const userInfo = JSON.parse(sessionStorage.getItem('userInfo'));
		
		const data = COUNTRY_CODES.map(value => {
			return {
				text: `${value.code} ${value.name}`,
				value: value.code
			}
		});
		
		let selectBox = Top.Widget.create('top-selectbox', {
			'id': 'code',
			'text-size': '13px',
			'nodes': data,
			'class-name': '',
			'selectedIndex': 0,
			'on-change': function (e) {
				selectBox.template.querySelector('span').lastChild.textContent = selectBox.getSelected().value;
			},
		});
		selectBox.template.style.height = '30px';
		selectBox.template.style.width = '83px';
		selectBox.template.querySelector('.top-selectbox-root').style.width = '100%';
		selectBox.template.querySelector('.top-selectbox-root').style.height = '100%';
		selectBox.select('+82');
		
		const template = htmlToElement(`
			<div id="add-friends-by-contact">
				<div id="friends-nickname">
					<input maxlength="20" type="text" name="nickname" placeholder="추가할 프렌즈의 별명을 입력해 주세요." />
					<span class="nickname-length">0/20</span>
				</div>
				<div class="friends-phone-number">
					<input type="number" name="phoneNumber" placeholder="'-' 없이 숫자만 입력해 주세요." />
				</div>
				<div class="friends-results">
					<h2>연락처로 프렌즈를 추가하세요.</h2>
					<p>상대가 TeeSpace 회원이고, 연락처 검색 허용을<br/>한 경우 프렌즈로 추가할 수 있습니다.</p>
					<button class="solid">프렌즈 추가</button>
				</div>
			</div>
		`);
		
		template.querySelector('.friends-phone-number').insertBefore(selectBox.template, template.querySelector('.friends-phone-number').firstChild);
		
		const nameInputValidator = function (e) {
			let length = template.querySelector('input[name=nickname]').value.length;
			if (length > 20) length = 20;
			template.querySelector('span.nickname-length').textContent = length + '/20';
		};
		
		template.querySelector('input[name=nickname]').addEventListener('input', nameInputValidator);
		
		template.querySelector('button').addEventListener('click', function (e) {
			let nickname =  template.querySelector('input[name=nickname]').value;
			
			FriendsAPI.AddFriendWithPhone({
				USER_PHONE: template.querySelector('input[name=phoneNumber]').value, 
				USER_ID: userInfo.userId, 
				NATIONAL_CODE: selectBox.getSelected().value,
				FRIEND_NICK: nickname.length === 0 ? null : nickname
			})
			.then(res => {
				const code = res.data.dto.RESULT_CD;
				if (code === 'RST0001') {
					TeeToast.open({
						text: `프렌즈로 추가되었습니다.`
					});

					FriendListModel.fetch(userInfo.userId);
				} else if (code === 'RST0002') {
					TeeAlarm.open({title: '유효하지 않는 휴대폰 번호입니다.', content: '입력하신 연락처로 가입한 회원이 없습니다.'});
				} else if (code === 'RST0003') {
					TeeAlarm.open({title: '입력하신 번호는 이미 추가된 프렌즈 입니다.', content: ''});
				} else if (code === 'RST0004') {
					TeeAlarm.open({title: '입력하신 번호는 내 계정 입니다.', content: ''});
				}
			})
			.catch(err => {
				
			});
		});
		
		return template;
	}

	AddFriendsDialog.prototype.AddFriendsByIdSearch = function () {
		const self = this;
		const userInfo = JSON.parse(sessionStorage.getItem('userInfo'));
		const idSearch = {
			keyword: mobx.observable.box(''),
			isSearched: mobx.observable.box(false),
			result: mobx.observable.box(undefined)
		};

		FriendListModel.fetch(userInfo.userId);

		const env = isb2c() ? 'b2c' : 'b2b';
		
		const templateText = {
			b2c: {
				placeholder: '아이디 검색',
				defaultHeader: '아이디로 프렌즈를 추가하세요.',
				defaultContent: '상대의 TeeSpace 아이디를 아는 경우<br/>프렌즈로 추가할 수 있습니다.',
				notFound: '입력하신 아이디로 등록된 회원이 없습니다.',
			},
			b2b: {
				placeholder: '아이디 검색',
				defaultHeader: '아이디로 프렌즈를 추가하세요.',
				defaultContent: '상대의 TeeSpace 아이디를 아는 경우<br/>프렌즈로 추가할 수 있습니다.',
				notFound: '입력하신 아이디로 등록된 회원이 없습니다.',
			},
		}

		const template = htmlToElement(`
			<div id="add-friends-by-id-search">
				<div id="friends-id">
					<top-icon class="icon-search tee-search-icon"></top-icon>
					<input type="text" name="id" placeholder="${templateText[env].placeholder}" />
				</div>
				<div class="friends-results">
					<h2>${templateText[env].defaultHeader}</h2>
					<p>${templateText[env].defaultContent}</p>
				</div>
			</div>
		`);

		InitializeTeeSearch(template.querySelector('#friends-id'));

		template.classList.add(env);
		
		const friendIdInput = template.querySelector('input[name=id]');
		
		friendIdInput.addEventListener('keypress', function (e) {
			if (e.keyCode == 13) {
	    		axios.post(_workspace.url + 'Users/Id2Name?action=Get', {
	    			dto:{
						USER_LOGIN_ID : friendIdInput.value,
						USER_ID: userInfo.userId,
						TOKEN: 'same'
					}
	    		})
	    		.then(res => {
	    			idSearch.isSearched.set(true);
	    			idSearch.keyword.set(friendIdInput.value);
	    			if ( res.data.dto.dtos.length > 0 ) {
	    				idSearch.result.set(res.data.dto.dtos[0]);
	    			} else {
	    				idSearch.result.set(undefined);
	    			}
	    		})
	    		.catch(err => {});
	    		
	    	}
		});
		
		mobx.autorun(function () {
			let isSearched = idSearch.isSearched.get();
			let results = idSearch.result.get();
			let keyword = idSearch.keyword.get();
			
			const resultTemplate = htmlToElement(`
					<div>
						<h2></h2>
						<p></p>
					</div>
			`);
			
			if (isSearched === false) {
				resultTemplate.querySelector('h2').innerHTML = templateText[env].defaultHeader;
				resultTemplate.querySelector('p').innerHTML = templateText[env].defaultContent;
				
				template.querySelector('.friends-results').innerHTML = '';
				template.querySelector('.friends-results').appendChild(resultTemplate);
			} else {
				if (results === undefined) {
					resultTemplate.querySelector('h2').innerHTML = '\'' + keyword + '\'' + '을(를) 찾을 수 없습니다.';
					resultTemplate.querySelector('p').innerHTML = templateText[env].notFound;
					
					template.querySelector('.friends-results').innerHTML = '';
					template.querySelector('.friends-results').appendChild(resultTemplate);
				} else {
					const friendTemplate = htmlToElement(`
					<div class="friend">
						<img src="" />
						<div class="friend-info">
							<span class="name"></span>
							<span class="status"></span>
						</div>
						<button class="solid" data-type=""></button>
					</div>
					`);
					
					friendTemplate.querySelector('button').addEventListener('click', function (e) {
						const type = e.target.getAttribute('data-type');
						
						if (type === 'me') {
							
						} else if (type === 'friend') {
							
						} else {
							FriendsAPI.AddFriend(userInfo.userId, results.USER_ID)
								.then(res => {
									TeeToast.open({
										text: `${results.USER_NAME}님이 프렌즈로 추가되었습니다.`
									});

									friendTemplate.querySelector('span.status').textContent = '(이미 프렌즈)';
									friendTemplate.querySelector('button').textContent = '1:1 Talk';		
									friendTemplate.querySelector('button').setAttribute('data-type', 'friend');
									
									friendTemplate.querySelector('button').addEventListener('click', function (e) {
										Top.Dom.selectById('AddFriendsDialog').close(true);
										spaceAPI.createSpace(null, userInfo.userId, 'WKS0002', [{USER_ID: results.USER_ID}]);
									});

									FriendListModel.fetch(userInfo.userId);
								})
								.catch(err => {});
						}
					});
					
					let THUMB_PHOTO = results.THUMB_PHOTO;
					
					if (!THUMB_PHOTO || THUMB_PHOTO.length === 0) {
						THUMB_PHOTO = userManager.getUserDefaultPhotoURL(results.USER_ID)
					} else {
						THUMB_PHOTO = userManager.getUserPhoto(results.USER_ID, "medium", results.THUMB_PHOTO);
					}
					
					friendTemplate.querySelector('img').src = THUMB_PHOTO;

					if (env === 'b2c') {
						friendTemplate.querySelector('span.name').textContent = results.USER_NAME;
					} else {
						let orgInfo = '';
						if (results.USER_JOB && results.USER_POSITION) {
							orgInfo = `(${results.USER_JOB}-${results.USER_POSITION})`;
						} else if (results.USER_JOB && !results.USER_POSITION) {
							orgInfo = `(${user.USER_JOB})`;
						} else if (!results.USER_JOB && results.USER_POSITION) {
							orgInfo = `(${results.USER_POSITION})`;
						}

						friendTemplate.querySelector('span.name').textContent = `${results.USER_NAME} ${orgInfo}`;
					}
					
					if (results.USER_ID === userInfo.userId) {
						friendTemplate.querySelector('span.status').textContent = '(내 계정)';
						friendTemplate.querySelector('button').textContent = '나와의 Talk';
						friendTemplate.querySelector('button').setAttribute('data-type', 'me');
						
						friendTemplate.querySelector('button').addEventListener('click', function (e) {
							openMiniTalk(workspaceManager.getMySpaceUrl());
						});
					} else if (results.TOKEN === 'isFriend') {
						friendTemplate.querySelector('span.status').textContent = '(이미 프렌즈)';
						friendTemplate.querySelector('button').textContent = '1:1 Talk';
						friendTemplate.querySelector('button').setAttribute('data-type', 'friend');
						
						friendTemplate.querySelector('button').addEventListener('click', function (e) {
							Top.Dom.selectById('AddFriendsDialog').close(true);
							spaceAPI.createSpace(null, userInfo.userId, 'WKS0002', [{USER_ID: results.USER_ID}]);
						});
					} else {
						friendTemplate.querySelector('span.status').textContent = '';
						friendTemplate.querySelector('button').textContent = '프렌즈 추가';
						friendTemplate.querySelector('button').setAttribute('data-type', 'none');
					}
					
					template.querySelector('.friends-results').innerHTML = '';
					template.querySelector('.friends-results').appendChild(friendTemplate);
				}
			}
		});
		
		return template;
	}

	AddFriendsDialog.prototype.AddFriendsByRecommendation = function () {
		const userInfo = JSON.parse(sessionStorage.getItem('userInfo'));
		const recommandation = mobx.observable.array([]);
		
		const template = htmlToElement(`
			<div id="add-friends-by-recommendation">
				<ul class="tee-table">
				</ul>
				<div class="no-friends">
					<h2>아직 추천 프렌즈가 없습니다.</h2>
					<p>추가한 프렌즈가 많을수록 추천이 활성화 됩니다.</p>
					<img src="res/illust/none_recommend_friend.png" />
				</div>
			</div>
		`);
		
		axios.post(_workspace.url + 'WorkSpace/WorkspaceRecommendedFriendListGet?action=', {
			dto: {
	            USER_ID: userInfo.userId,	                		
			}
		})
		.then(res => {
			recommandation.replace(res.data.dto.tUserList);
		})
		.catch(err => {});

		template.querySelector('ul').addEventListener('click', function (e) {
			e.preventDefault();
			e.stopPropagation();

			const node = e.target.closest('li');
			if (node) {
				const userId = node.getAttribute('data-user-id');
				const profileImage = e.target.closest('.profileImage img');
				if (profileImage) {
					_UserId = userId;
					_friendManageProfilePopupWithPosition(_UserId, e.clientX, e.clientY);
				}
			}
		});
		
		mobx.autorun(function () {
			if (recommandation.length === 0) {
				template.classList.add('no-friends');
				template.querySelector('.no-friends').style.display = 'block';
			} else {
				template.classList.remove('no-friends');
				template.querySelector('.no-friends').style.display = 'none';
			}
			
			template.querySelector('ul').innerHTML = '';
			recommandation.map((friend, index) => {
				let THUMB_PHOTO = friend.THUMB_PHOTO;
					
				if (!THUMB_PHOTO || THUMB_PHOTO.length === 0) {
					THUMB_PHOTO = userManager.getUserDefaultPhotoURL(friend.USER_ID)
				} else {
					THUMB_PHOTO = userManager.getUserPhoto(friend.USER_ID, "small", friend.THUMB_PHOTO);
				}
				
				const li = htmlToElement(`
					<li>
						<span class="profileImage"><img src="${THUMB_PHOTO}" /></span>
						<span class="userName">${friend.USER_NAME}</span>
						<span class="action">
							<span class="add">
								<i class="icon-add_friend"></i>
							</span>
							<top-icon class="icon-close block"></top-icon>
						</span>
					</li>
				`);

				li.setAttribute('data-user-id', friend.USER_ID);
				
				li.querySelector('.add').addEventListener('click', function (e) {
					axios.post(_workspace.url + 'Users/AddFriend?action=', {
						dto: {
							USER_ID: userInfo.userId,
							FRIEND_ID: friend.USER_ID
						}
					})
					.then(res => {
						TeeToast.open({
							text: `${friend.USER_NAME}님이 프렌즈로 추가되었습니다.`
						});

						recommandation.splice(index, 1);
						FriendListModel.fetch(userInfo.userId);
					})
					.catch(err => {
						
					});
				});

				li.querySelector('.block').addEventListener('click', function (e) {
					axios.post(_workspace.url + `WorkSpace/WorkspaceRecommendedFriendBlock?userId=${userInfo.userId}&friendId=${friend.USER_ID}`)
					.then(res => {
						recommandation.splice(index, 1);
					})
					.catch(err => {
						
					});
				});
				
				template.querySelector('ul').appendChild(li);
			});
		});
		
		return template;
	}

	AddFriendsDialog.prototype.AddFriendsByInvitation = function () {
		const isListOpen = mobx.observable.box(false);
		const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		const contextMenu = new ContextMenu();
		const userInfo = JSON.parse(sessionStorage.getItem('userInfo'));
		const friendedList = mobx.observable.array([]);
		
		const UserInviteLink = axios.get(_workspace.url + 'SpaceRoom/UserInviteLink', {
			params: {
				USER_ID: userInfo.userId
			}
		}).then(function (res) {
			return res.data.dto.RESULT_VALUE;
		}).catch(function (err) {
			return 'Server Error';
		});
		
		const FriendedList = axios.get(_workspace.url + 'Users/FriendedList', {
			params: {
				USER_ID: userInfo.userId
			}
		})
		.then(function(res) {
			let list = res.data.dto.dtos;
			friendedList.replace(list);
			
		})
		.catch(function(err) {
			return [];
		});
		
		const template = htmlToElement(`
			<div id="add-friends-by-invitation">
				<div class="copy-container">
					<h3>초대 링크 복사하기</h3>
					<p>아래 초대 링크를 복사 후 공유하세요.<br/>링크를 통해 가입한 멤버는 프렌즈로 추가됩니다.</p>
					<div class="link-form">
						<input type="text" name="link" />
						<button name="copy-link" class="outlined">초대 링크 복사</button>
					</div>
				</div>
				<div class="invite-list">
					<h3>나의 초대 링크로 가입한 프렌즈 수 <span class="invite-count">0</span><top-icon class="handle icon-arrow_potab_up"></top-icon></h3> 
					<ul class="tee-table">
					</ul>
				</div>
				<div class="divider">
					<div class="border"></div>
					<div class="border-text">OR</div>
					<div class="border"></div>
				</div>
				<div class="send-email-container">
					<h3>초대 메일 보내기</h3>
					<div class="paragraph">입력한 이메일 주소로 초대장이 발송되며,<br/>초대장을 통해 회원 가입 시<br/>나의 프렌즈 목록에 추가됩니다.<div class="bg"></div></div>
					<div class="email-form">
						<input id="emailTokenInput" type="text" name="link" placeholder="이메일 주소 입력" />
						<button name="send-mail" class="outlined">보내기</button>
					</div>
					<div class="email-chips" style="display: none">
						
					</div>
				</div>
			</div>
		`);
		
		/**
		 * 초대 링크 복사
		 */
		const linkField = template.querySelector('input[name=link]');
		template.appendChild(contextMenu.containerElem);
		
		UserInviteLink.then(res => {
			linkField.value = res;
			
			linkField.addEventListener('contextmenu', function (e) {
				e.preventDefault();
				linkField.select();
				contextMenu.show(e.pageX, e.pageY);
			});
			
			contextMenu.addItem('복사', function(e) {
				contextMenu.hide();
				linkField.select();
				document.execCommand('copy');
				
				TeeToast.open({
					text: '초대 링크를 클립보드에 복사했습니다.'
				});
			});
		});
		
		template.querySelector('button[name=copy-link]').addEventListener('click', function (e) {
			linkField.select();
			document.execCommand('copy');
			
			TeeToast.open({
				text: '초대 링크를 클립보드에 복사했습니다.'
			});
		});
		
		template.querySelector('.handle').addEventListener('click', function (e) {
			const listOpen = isListOpen.get();
			
			isListOpen.set(!listOpen);
		});
		
		mobx.autorun(() => {
			const listOpen = isListOpen.get();
			
			if (!listOpen) {
				template.querySelector('top-icon.handle').classList.remove('icon-arrow_potab_up');
				template.querySelector('top-icon.handle').classList.add('icon-arrow_potab_down');
				template.querySelector('.invite-list ul').style.display = 'none';
			} else {
				template.querySelector('top-icon.handle').classList.remove('icon-arrow_potab_down');
				template.querySelector('top-icon.handle').classList.add('icon-arrow_potab_up');
				template.querySelector('.invite-list ul').style.display = 'block';
			}
			
			AddFriendsDialog.center();
		});
		
		mobx.autorun(() => {
			friendedList.map(data => {
				const li = htmlToElement(`
						<li>
							<span class="name">${data.USER_NAME}</span>
							<span class="id">${data.USER_LOGIN_ID}</span>
						</li>
				`);
				
				template.querySelector('.invite-list ul').appendChild(li);
			});
			
			template.querySelector('.invite-list .invite-count').textContent = friendedList.length;
		});
		
		/**
		 * 초대 메일 보내기
		 */
		let chips = mobx.observable.array([]);

		mobx.reaction(() => chips.length, length => {
			
			if (length === 0) {
				template.querySelector('.email-chips').style.display = 'none';
			} else {
				template.querySelector('.email-chips').style.display = 'flex';
			}
			
			template.querySelector('.email-chips').innerHTML = '';
			chips.map((chip) => {
				const removeChip = function (e) {
					const index = chips.findIndex(v => v === chip);
					chips.splice(index, 1);
					
				};
				
				const chipTemplate = htmlToElement(`
						<span class="chip">${chip}<top-icon class="icon-close delete"></top-icon></span>
				`);
				
				const errorChipTemplate = htmlToElement(`
						<span class="chip invalid"><top-icon class="icon-chart_error"></top-icon><span>${chip}<top-icon class="icon-close delete"></top-icon></span>
				`);
				
				chipTemplate.querySelector('.delete').addEventListener('click', removeChip);
				errorChipTemplate.querySelector('.delete').addEventListener('click', removeChip);
				
				if (emailRegex.test(chip)) {
					template.querySelector('.email-chips').appendChild(chipTemplate);
				} else {
					template.querySelector('.email-chips').appendChild(errorChipTemplate);
				}
			});
			
			AddFriendsDialog.center();
		});
		
		template.querySelector('#emailTokenInput').addEventListener('keydown' , function(e) {
	        let str = e.target.value.trim(); 

	        if( !!(~[9 , 13 , 188].indexOf( e.keyCode ))  )
	        {
	        	setTimeout(function () {
	        		e.target.value = "";
	        	}, 0);
	            if(str != "") {
	            	if (chips.findIndex(v => v === str) === -1) {
	            		chips.push(str);
	            	}
	            }
	        }
	    });
		
		template.querySelector('button[name=send-mail]').addEventListener('click', function (e) {
			if (template.querySelector('#emailTokenInput').value.trim().length > 0) {
				let str = template.querySelector('#emailTokenInput').value.trim();
				if (chips.findIndex(v => v === str) === -1) {
            		chips.push(str);
            	}
				template.querySelector('#emailTokenInput').value = '';
			}

			if (chips.length === 0) return false;
			
			if (chips.filter(v => !emailRegex.test(v)).length > 0) {
				TeeAlarm.open({
					title: '올바르지 않은 이메일 주소 형식이<br/>포함되어 있습니다.', 
					content: '오류 표시된 주소를 수정하거나 삭제 후<br/>초대 메일을 보내주세요.'
				});
				return false;
			}
			
			let userList = chips.filter(v => emailRegex.test(v)).map((v) => {
				return {
					EMAIL_ADDRESS: v
				}
			});
			
			axios.post(_workspace.url + 'SpaceRoom/UserInviteMail?action=Send', {
				dto: {
					SENDER_ID: userInfo.userId,
					WsUserList: userList
				}
			}).then(function (res) {
				TeeToast.open({
					text: `${userList.length}명의 친구에게 초대 메일을 발송했습니다.`
				});
			}).catch(function (err) {
				console.error(err);
			});
		});
		
		return template;
	}

	/**
	 * 조직도 조회
	 */
	AddFriendsDialog.prototype.AddFriendsByOrganization = function () {
		const self = this;
		const userInfo = JSON.parse(sessionStorage.getItem('userInfo'));

		const model = {
			selectedOrg: mobx.observable.box({}),
			selectedOrgName: mobx.observable.box(''),
			search: mobx.observable.box(''),
			userList: mobx.observable.array([]),
			filteredUserList: mobx.observable.array([]),
		};
		
		const resetSearch = function () {
			model.search.set('');
			template.querySelector('input[name=search]').value = '';
		};

		const template = htmlToElement(`
			<div id="add-friends-by-organization">
				<div class="org-search-container">
					<div class="search-input-container">
						<top-icon class="icon-search tee-search-icon"></top-icon>
						<input type="text" name="search" placeholder="팀 이름, 조직원 이름 검색" />
					</div>
				</div>
				<div class="org-member-list">
					<div class="org-info">
						<div class="org-select">
							<span class="name"></span>
							<top-icon class="handle icon-arrow_potab_down"></top-icon>
						</div>
						<span class="highlight"></span>
					</div>
					<ul class="tee-table">
					</ul>
				</div>
			</div>
		`);

		InitializeTeeSearch(template.querySelector('.search-input-container'));

		template.querySelector('ul').addEventListener('click', function (e) {
			e.preventDefault();
			e.stopPropagation();

			const node = e.target.closest('li');
			if (node) {
				const userId = node.getAttribute('data-user-id');
				const profileImage = e.target.closest('img.profile-image');

				if (profileImage) {
					_UserId = userId;
					_friendManageProfilePopupWithPosition(_UserId, e.clientX, e.clientY);
				}
			}
		});

		template.querySelector('ul').addEventListener('click', function (e) {
			const target = e.target.closest('span.add');
			if (target) {
				const container = target.closest('li');
				const friendId = container.data.userId;
				const friendName = container.data.userName;

				axios.post(_workspace.url + 'Users/AddFriend?action=', {
					dto: {
						USER_ID: userInfo.userId,
						FRIEND_ID: friendId
					}
				})
				.then(res => {
					TeeToast.open({
						text: `${friendName}님이 프렌즈로 추가되었습니다.`
					});

					container.querySelector('.action').innerHTML = '<span class="text"></span>';
					container.data.model.FRIEND_RELATION = 'TRUE';

					FriendListModel.fetch(userInfo.userId);
				})
				.catch(err => {
					
				});
			}
		});

		template.querySelector('input[name=search]').addEventListener('keydown', function (e) {
			if( !!(~[9 , 13 , 188].indexOf( e.keyCode ))  ) {
				const value = e.target.value;
				model.search.set(value);
			}
		});

		mobx.autorun(() => {
			const selectedOrgName = model.selectedOrgName.get();
			template.querySelector('.org-select span.name').textContent = selectedOrgName;
		});

		mobx.autorun(() => {
			const search = model.search.get();

			if (search.length > 0) {
				let filteredFriends = model.userList.filter(v => 
																v.USER_NAME.indexOf(search) > -1 
																|| v.USER_EMAIL.indexOf(search) > -1
																|| v.USER_PHONE.indexOf(search) > -1
																|| v.USER_COM_NUM.indexOf(search) > -1);

				model.filteredUserList.replace(filteredFriends);					
			} else {
				model.filteredUserList.replace(model.userList);
			}
		});

		mobx.autorun(() => {
			const selectedOrg = model.selectedOrg.get();

			template.querySelector('.org-info .name').textContent = selectedOrg.orgName;
			if (selectedOrg.companyCode === undefined) { 
				axios.post(_workspace.url + 'SpaceRoom/OrgUserDept?action=Get', {
					dto: {
						USER_ID: userManager.getLoginUserId(), 
					}
				})
				.then(res => res.data.dto)
				.then(res => axios.post(_workspace.url + 'Admin/OrgPath?action=Get', {
					dto: {
						COMPANY_CODE: res.COMPANY_CODE,
						DEPARTMENT_CODE: res.DEPARTMENT_CODE, 
						ORG_TYPE: 'ADM0021'
					}
				}).then(path => {
					const pathArray = path.data.dto.ORG_NAME.split(' > ');
					const orgName = pathArray.length > 1 ? pathArray[pathArray.length - 1] : pathArray[0];
					if (pathArray.length > 1) {
						model.selectedOrgName.set(pathArray[pathArray.length-1]);
					} else {
						model.selectedOrgName.set(pathArray[0]);
					}

					model.selectedOrg.set({
						companyCode: res.COMPANY_CODE,
						departmentCode: res.DEPARTMENT_CODE,
						orgName: orgName
					});
				}))
				.catch(err => {});
			} else {
				axios.post(_workspace.url + 'SpaceRoom/OrgUserList?action=Get', {
					dto: {
						COMPANY_CODE: selectedOrg.companyCode,
						DEPARTMENT_CODE: selectedOrg.departmentCode,
						USER_ID: userManager.getLoginUserId()
					}
				})
				.then(res => res.data.dto)
				.then(res => {
					if (res.UserProfileList === null) {
						model.userList.replace([]);
					} else {
						model.userList.replace(res.UserProfileList);
					}
					model.filteredUserList.replace(model.userList);
				})
				.catch(err => {});
			}
		});

		const container = template.querySelector('ul');
		const config = {
			itemHeight: screen.width / 13.66 / 100 * 3.25 * 16, 
			height: screen.width / 13.66 / 100 * 18.56 * 16,
			total: model.filteredUserList.length, 
			scrollerTagName: 'div',
			generate: function (index) {
				const user = model.filteredUserList[index];
				const userTemplate = htmlToElement(`
					<li>
						<img class="profile-image" src="" />
						<span class="name"></span>
						<span class="action"></span>
					</li>
				`);

				let THUMB_PHOTO = user.THUMB_PHOTO;
					
				if (!THUMB_PHOTO || THUMB_PHOTO.length === 0) {
					THUMB_PHOTO = userManager.getUserDefaultPhotoURL(user.USER_ID)
				} else {
					THUMB_PHOTO = userManager.getUserPhoto(user.USER_ID, "small", user.THUMB_PHOTO);
				}

				userTemplate.data = {
					userId: user.USER_ID,
					userName: user.USER_NAME,
					model: user,
				};

				userTemplate.querySelector('img').src = THUMB_PHOTO;

				userTemplate.setAttribute('data-user-id', user.USER_ID);
				
				let orgInfo = '';
				if (user.USER_JOB && user.USER_POSITION) {
					orgInfo = `(${user.USER_JOB}-${user.USER_POSITION})`;
				} else if (user.USER_JOB && !user.USER_POSITION) {
					orgInfo = `(${user.USER_JOB})`;
				} else if (!user.USER_JOB && user.USER_POSITION) {
					orgInfo = `(${user.USER_POSITION})`;
				}

				userTemplate.querySelector('span.name').textContent = `${user.USER_NAME} ${orgInfo}`;


				if (user.USER_ID === userManager.getLoginUserId()) {
					userTemplate.querySelector('.action').innerHTML = '<span class="text">내 계정</span>';
				} else {
					if (user.FRIEND_RELATION !== 'FALSE') {
						userTemplate.querySelector('.action').innerHTML = '<span class="text"></span>';
					} else {
						userTemplate.querySelector('.action').innerHTML = '<span class="add"><i class="icon-add_friend"></i></span>';
					}
				}

				return userTemplate;
			},
		};

		const list = HyperList.create(container, config);

		mobx.reaction(
			() => model.filteredUserList.slice(),
			obj => {
				const length = model.filteredUserList.length;
				config.total = length;
				list.refresh(container, config);

				if (length === 0) {
					const noneTemplate = htmlToElement(`
						<li class="none">
							검색 결과가 없습니다.
						</li>
					`);

			 		template.querySelector('ul').appendChild(noneTemplate);
				}
				template.querySelector('.org-info .highlight').textContent = length + '명';
			}
		);

		template.querySelector('.org-select').addEventListener('click', function (e) {
			const containerRect = template.querySelector('.org-select').getBoundingClientRect();
			const target = template.querySelector('.org-select .handle');
			const selectedOrg = model.selectedOrg.get();

			template.querySelector('.org-select').classList.add('active');
			if (target.classList.contains('icon-arrow_potab_down')) {
				target.classList.remove('icon-arrow_potab_down');
				target.classList.add('icon-arrow_potab_up');

				OrgTreeSelectView.open(
					{
						x: containerRect.left + 'px',
						y: containerRect.bottom + 5 + 'px',
						width: '21.5rem',
						height: '18.44rem',
					},
					{
						onItemClicked: function (data, orgNameTree) {
							resetSearch(); 
							
							model.selectedOrg.set(data);
							target.classList.remove('icon-arrow_potab_up');
							target.classList.add('icon-arrow_potab_down');

							if (orgNameTree.length > 1) {
								model.selectedOrgName.set(orgNameTree[orgNameTree.length-1]);
							} else {
								model.selectedOrgName.set(orgNameTree[0]);
							}

							template.querySelector('.org-select').classList.remove('active');
						},
						onClosed: function () {
							target.classList.remove('icon-arrow_potab_up');
							target.classList.add('icon-arrow_potab_down');
							template.querySelector('.org-select').classList.remove('active');
						},
						selectedId: `${selectedOrg.companyCode}.${selectedOrg.departmentCode}`
					}
				);
			} else {
				target.classList.remove('icon-arrow_potab_up');
				target.classList.add('icon-arrow_potab_down');
				template.querySelector('.org-select').classList.remove('active');
				OrgTreeSelectView.close();
			}
			
		});

		return template;
	};
	
	
	AddFriendsDialog.updateUser = function (data) {
		const elements = document.querySelectorAll(`div#top-dialog-root_AddFriendsDialog [data-user-id='${data.USER_ID}']`);
		
		let THUMB_PHOTO = data.THUMB_PHOTO;
				
		if (!THUMB_PHOTO || THUMB_PHOTO.length === 0) {
			THUMB_PHOTO = userManager.getUserDefaultPhotoURL(data.USER_ID)
		} else {
			THUMB_PHOTO = userManager.getUserPhoto(data.USER_ID, "small", data.THUMB_PHOTO);
		}

		elements.forEach(v => {
			v.querySelector('img').src = THUMB_PHOTO;
		});
	};

	AddFriendsDialog.open = function (opts = {}) {
		let defaultOpts = {
			onClose: null,
		};

		Object.assign(defaultOpts, opts);
		Object.assign(config, defaultOpts);

		Top.Dom.selectById('AddFriendsDialog').open();
	};

	AddFriendsDialog.close = function (opt) {
		Top.Dom.selectById('AddFriendsDialog').close(opt);
	};

	AddFriendsDialog.center = function () {
		const node = document.querySelector('div#top-dialog-root_AddFriendsDialog');
		if (node) {
			node.style.left = `calc(50% - ${node.getBoundingClientRect().width/2}px)`;
		}
	};

	
	return AddFriendsDialog;
}();


/**
 * TOP Controller
 */
Top.Controller.create('AddFriendsLogic', {
	init : function(event, widget){
		
	}
});

/**
 * Top On Load
 */
Top.App.onLoad(() => {
	let dialog = null;
	Top.Dom.selectById('AddFriendsDialog').setProperties({
		'on-open': function () {
			dialog = new AddFriendsDialog();
			document.querySelector('div#AddFriendsLayout').appendChild(dialog.template);

			setTimeout(() => {
				document.querySelector('div#top-dialog-root_AddFriendsDialog').classList.remove('c-loading');
				AddFriendsDialog.center();
			}, 0);
		},
		'on-close': function () {
			document.querySelector('div#AddFriendsLayout').innerHTML = '';
			if (typeof dialog.config.onClose === 'function') {
				dialog.config.onClose();
			}
		}
	});
});
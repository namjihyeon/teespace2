
'use strict';

const SignalingState = {
    READY: 1,
    CONNECTING: 2,
    CONNECTED: 3,
};
/*
function getParameterByName(name) {
    name = name.replace(/[\[]/, '\\\[').replace(/[\]]/, '\\\]');
    let regex = new RegExp('[\\?&]' + name + '=([^&#]*)'),
        results = regex.exec(location.search);
    return results === null ? '' : decodeURIComponent(results[1].replace(
        /\+/g, ' '));
}*/

let meetingsend = function (method, path, body, onRes, host) {
    let req = new XMLHttpRequest()
    req.onreadystatechange = function () {
        if (req.readyState === 4) {
            onRes(req.responseText);
        }
    };
    let url = meetingUrl(host, path);
    req.open(method, url, true);
    req.setRequestHeader('Content-Type', 'application/json');
    if (body !== undefined) {
        req.send(JSON.stringify(body));
    } else {
        req.send();
    }
};

let meetingUrl = function (host, path) {
    let url;
    if (host !== undefined) {
        url = host + path;  // Use the host user set.
    } else {
        let u = new URL(document.URL);
        url = u.origin + path;  // Get the string before last '/'.
    }
    return url;
}

let meetingonResponse = function (result) {
    if (result) {
        try {
            console.info('Result:', JSON.parse(result));
        } catch (e) {
            console.info('Result:', result);
        }
    } else {
        console.info('Null');
    }
};

let meetingmixStream = function (room, stream, view, host) {
    let jsonPatch = [{
        op: 'add',
        path: '/info/inViews',
        value: view
    }];
    meetingsend('PATCH', '/rooms/' + room + '/streams/' + stream, jsonPatch,
        meetingonResponse, host);
};

let meetingstartStreamingIn = function (room, inUrl, host) {
    let options = {
        url: inUrl,
        media: {
            audio: 'auto',
            video: true
        },
        transport: {
            protocol: 'udp',
            bufferSize: 2048
        }
    };
    meetingsend('POST', '/rooms/' + room + '/streaming-ins', options, meetingonResponse, host);
};

let meetingcreateToken = function (room, user, role, callback, host) {
    let body = {
        room: room,
        user: user,
        role: role
    };
    meetingsend('POST', '/tokens/', body, callback, host);
};

const protocolVersion = '1.1';

/* eslint-disable valid-jsdoc */
/**
 * @class ParticipantEvent
 * @classDesc Class ParticipantEvent represents a participant event.
   @extends Owt.Base.OwtEvent
 * @memberof Owt.Conference
 * @hideconstructor
 */
const ParticipantEvent = function (type, init) {
    const that = new OwtEvent(type, init);
    /**
     * @member {Owt.Conference.Participant} participant
     * @instance
     * @memberof Owt.Conference.ParticipantEvent
     */
    that.participant = init.participant;
    return that;
};
/* eslint-enable valid-jsdoc */

/**
 * @class ConferenceClientConfiguration
 * @classDesc Configuration for ConferenceClient.
 * @memberOf Owt.Conference
 * @hideconstructor
 */
class ConferenceClientConfiguration { // eslint-disable-line no-unused-vars
    // eslint-disable-next-line require-jsdoc
    constructor() {
        /**
         * @member {?RTCConfiguration} rtcConfiguration
         * @instance
         * @memberof Owt.Conference.ConferenceClientConfiguration
         * @desc It will be used for creating PeerConnection.
         * @see {@link https://www.w3.org/TR/webrtc/#rtcconfiguration-dictionary|RTCConfiguration Dictionary of WebRTC 1.0}.
         * @example
         * // Following object can be set to conferenceClientConfiguration.rtcConfiguration.
         * {
         *   iceServers: [{
         *      urls: "stun:example.com:3478"
         *   }, {
         *     urls: [
         *       "turn:example.com:3478?transport=udp",
         *       "turn:example.com:3478?transport=tcp"
         *     ],
         *      credential: "password",
         *      username: "username"
         *   }
         * }
         */
        this.rtcConfiguration = undefined;
    }
}
const Logger = (function () {
    const DEBUG = 0;
    const TRACE = 1;
    const INFO = 2;
    const WARNING = 3;
    const ERROR = 4;
    const NONE = 5;

    const noOp = function () { };

    // |that| is the object to be returned.
    const that = {
        DEBUG: DEBUG,
        TRACE: TRACE,
        INFO: INFO,
        WARNING: WARNING,
        ERROR: ERROR,
        NONE: NONE,
    };

    that.log = window.console.log.bind(window.console);

    const bindType = function (type) {
        if (typeof window.console[type] === 'function') {
            return window.console[type].bind(window.console);
        } else {
            return window.console.log.bind(window.console);
        }
    };

    const setLogLevel = function (level) {
        if (level <= DEBUG) {
            that.debug = bindType('log');
        } else {
            that.debug = noOp;
        }
        if (level <= TRACE) {
            that.trace = bindType('trace');
        } else {
            that.trace = noOp;
        }
        if (level <= INFO) {
            that.info = bindType('info');
        } else {
            that.info = noOp;
        }
        if (level <= WARNING) {
            that.warning = bindType('warn');
        } else {
            that.warning = noOp;
        }
        if (level <= ERROR) {
            that.error = bindType('error');
        } else {
            that.error = noOp;
        }
    };

    setLogLevel(DEBUG); // Default level is debug.

    that.setLogLevel = setLogLevel;

    return that;
}());

/**
 * @class EventDispatcher
 * @classDesc A shim for EventTarget. Might be changed to EventTarget later.
 * @memberof Owt.Base
 * @hideconstructor
 */
const EventDispatcher = function () {
    // Private vars
    const spec = {};
    spec.dispatcher = {};
    spec.dispatcher.eventListeners = {};

    /**
     * @function addEventListener
     * @desc This function registers a callback function as a handler for the corresponding event. It's shortened form is on(eventType, listener). See the event description in the following table.
     * @instance
     * @memberof Owt.Base.EventDispatcher
     * @param {string} eventType Event string.
     * @param {function} listener Callback function.
     */
    this.addEventListener = function (eventType, listener) {
        if (spec.dispatcher.eventListeners[eventType] === undefined) {
            spec.dispatcher.eventListeners[eventType] = [];
        }
        spec.dispatcher.eventListeners[eventType].push(listener);
    };

    /**
     * @function removeEventListener
     * @desc This function removes a registered event listener.
     * @instance
     * @memberof Owt.Base.EventDispatcher
     * @param {string} eventType Event string.
     * @param {function} listener Callback function.
     */
    this.removeEventListener = function (eventType, listener) {
        if (!spec.dispatcher.eventListeners[eventType]) {
            return;
        }
        const index = spec.dispatcher.eventListeners[eventType].indexOf(listener);
        if (index !== -1) {
            spec.dispatcher.eventListeners[eventType].splice(index, 1);
        }
    };

    /**
     * @function clearEventListener
     * @desc This function removes all event listeners for one type.
     * @instance
     * @memberof Owt.Base.EventDispatcher
     * @param {string} eventType Event string.
     */
    this.clearEventListener = function (eventType) {
        spec.dispatcher.eventListeners[eventType] = [];
    };

    // It dispatch a new event to the event listeners, based on the type
    // of event. All events are intended to be LicodeEvents.
    this.dispatchEvent = function (event) {
        if (!spec.dispatcher.eventListeners[event.type]) {
            return;
        }
        spec.dispatcher.eventListeners[event.type].map(function (listener) {
            listener(event);
        });
    };
};

/**
 * @class ConferenceClient
 * @classdesc The ConferenceClient handles PeerConnections between client and server. For conference controlling, please refer to REST API guide.
 * Events:
 *
 * | Event Name            | Argument Type                    | Fired when       |
 * | --------------------- | ---------------------------------| ---------------- |
 * | streamadded           | Owt.Base.StreamEvent             | A new stream is available in the conference. |
 * | participantjoined     | Owt.Conference.ParticipantEvent  | A new participant joined the conference. |
 * | messagereceived       | Owt.Base.MessageEvent            | A new message is received. |
 * | serverdisconnected    | Owt.Base.OwtEvent                | Disconnected from conference server. |
 *
 * @memberof Owt.Conference
 * @extends Owt.Base.EventDispatcher
 * @constructor
 * @param {?Owt.Conference.ConferenceClientConfiguration } config Configuration for ConferenceClient.
 * @param {?Owt.Conference.SioSignaling } signalingImpl Signaling channel implementation for ConferenceClient. SDK uses default signaling channel implementation if this parameter is undefined. Currently, a Socket.IO signaling channel implementation was provided as ics.conference.SioSignaling. However, it is not recommended to directly access signaling channel or customize signaling channel for ConferenceClient as this time.
 */
const ConferenceClient = function (config, signalingImpl) {
    Object.setPrototypeOf(this, new EventDispatcher());
    config = config || {};
    const self = this;
    let signalingState = SignalingState.READY;
    const signaling = signalingImpl ? signalingImpl : (new Signaling());
    let me;
    let room;
    const remoteStreams = new Map(); // Key is stream ID, value is a RemoteStream.
    const participants = new Map(); // Key is participant ID, value is a Participant object.
    const publishChannels = new Map(); // Key is MediaStream's ID, value is pc channel.
    const channels = new Map(); // Key is channel's internal ID, value is channel.

    /**
     * @function onSignalingMessage
     * @desc Received a message from conference portal. Defined in client-server protocol.
     * @param {string} notification Notification type.
     * @param {object} data Data received.
     * @private
     */
    function onSignalingMessage(notification, data) {
        if (notification === 'soac' || notification === 'progress') {
            if (!channels.has(data.id)) {
                Logger.warning('Cannot find a channel for incoming data.');
                return;
            }
            channels.get(data.id).onMessage(notification, data);
        } else if (notification === 'stream') {
            if (data.status === 'add') {
                fireStreamAdded(data.data);
            } else if (data.status === 'remove') {
                fireStreamRemoved(data);
            } else if (data.status === 'update') {
                // Broadcast audio/video update status to channel so specific events can be fired on publication or subscription.
                if (data.data.field === 'audio.status' || data.data.field ===
                    'video.status') {
                    channels.forEach((c) => {
                        c.onMessage(notification, data);
                    });
                } else if (data.data.field === 'activeInput') {
                    fireActiveAudioInputChange(data);
                } else if (data.data.field === 'video.layout') {
                    fireLayoutChange(data);
                } else if (data.data.field === '.') {
                    updateRemoteStream(data.data.value);
                } else {
                    Logger.warning('Unknown stream event from MCU.');
                }
            }
        } else if (notification === 'text') {
            fireMessageReceived(data);
        } else if (notification === 'participant') {
            fireParticipantEvent(data);
        }
    }

    signaling.addEventListener('data', (event) => {
        onSignalingMessage(event.message.notification, event.message.data);
    });

    signaling.addEventListener('disconnect', () => {
        clean();
        signalingState = SignalingState.READY;
        self.dispatchEvent(new OwtEvent('serverdisconnected'));
    });

    // eslint-disable-next-line require-jsdoc
    function fireParticipantEvent(data) {
        if (data.action === 'join') {
            data = data.data;
            const participant = new Participant(data.id, data.role, data.user);
            participants.set(data.id, participant);
            const event = new ParticipantEvent(
                'participantjoined', { participant: participant });
            self.dispatchEvent(event);
        } else if (data.action === 'leave') {
            const participantId = data.data;
            if (!participants.has(participantId)) {
                Logger.warning(
                    'Received leave message from MCU for an unknown participant.');
                return;
            }
            const participant = participants.get(participantId);
            participants.delete(participantId);
            participant.dispatchEvent(new OwtEvent('left'));
        }
    }

    // eslint-disable-next-line require-jsdoc
    function fireMessageReceived(data) {
        const messageEvent = new MessageEvent('messagereceived', {
            message: data.message,
            origin: data.from,
            to: data.to,
        });
        self.dispatchEvent(messageEvent);
    }

    // eslint-disable-next-line require-jsdoc
    function fireStreamAdded(info) {
        const stream = createRemoteStream(info);
        remoteStreams.set(stream.id, stream);
        const streamEvent = new StreamEvent('streamadded', {
            stream: stream,
        });
        self.dispatchEvent(streamEvent);
    }

    // eslint-disable-next-line require-jsdoc
    function fireStreamRemoved(info) {
        if (!remoteStreams.has(info.id)) {
            Logger.warning('Cannot find specific remote stream.');
            return;
        }
        const stream = remoteStreams.get(info.id);
        const streamEvent = new OwtEvent('ended');
        remoteStreams.delete(stream.id);
        stream.dispatchEvent(streamEvent);
    }

    // eslint-disable-next-line require-jsdoc
    function fireActiveAudioInputChange(info) {
        if (!remoteStreams.has(info.id)) {
            Logger.warning('Cannot find specific remote stream.');
            return;
        }
        const stream = remoteStreams.get(info.id);
        const streamEvent = new ActiveAudioInputChangeEvent(
            'activeaudioinputchange', {
            activeAudioInputStreamId: info.data.value,
        });
        stream.dispatchEvent(streamEvent);
    }

    // eslint-disable-next-line require-jsdoc
    function fireLayoutChange(info) {
        if (!remoteStreams.has(info.id)) {
            Logger.warning('Cannot find specific remote stream.');
            return;
        }
        const stream = remoteStreams.get(info.id);
        const streamEvent = new LayoutChangeEvent(
            'layoutchange', {
            layout: info.data.value,
        });
        stream.dispatchEvent(streamEvent);
    }

    // eslint-disable-next-line require-jsdoc
    function updateRemoteStream(streamInfo) {
        if (!remoteStreams.has(streamInfo.id)) {
            Logger.warning('Cannot find specific remote stream.');
            return;
        }
        const stream = remoteStreams.get(streamInfo.id);
        stream.settings = convertToPublicationSettings(streamInfo
            .media);
        stream.extraCapabilities = convertToSubscriptionCapabilities(streamInfo.media);
        const streamEvent = new OwtEvent('updated');
        stream.dispatchEvent(streamEvent);
    }

    // eslint-disable-next-line require-jsdoc
    function createRemoteStream(streamInfo) {
        if (streamInfo.type === 'mixed') {
            return new RemoteMixedStream(streamInfo);
        } else {
            let audioSourceInfo; let videoSourceInfo;
            if (streamInfo.media.audio) {
                audioSourceInfo = streamInfo.media.audio.source;
            }
            if (streamInfo.media.video) {
                videoSourceInfo = streamInfo.media.video.source;
            }
            const stream = new RemoteStream(streamInfo.id,
                streamInfo.info.owner, undefined, new StreamSourceInfo(
                    audioSourceInfo, videoSourceInfo), streamInfo.info.attributes);
            stream.settings = convertToPublicationSettings(
                streamInfo.media);
            stream.extraCapabilities = convertToSubscriptionCapabilities(streamInfo.media);
            return stream;
        }
    }

    // eslint-disable-next-line require-jsdoc
    function sendSignalingMessage(type, message) {
        return signaling.send(type, message);
    }

    // eslint-disable-next-line require-jsdoc
    function createPeerConnectionChannel() {
        // Construct an signaling sender/receiver for ConferencePeerConnection.
        const signalingForChannel = Object.create(EventDispatcher);
        signalingForChannel.sendSignalingMessage = sendSignalingMessage;
        const pcc = new ConferencePeerConnectionChannel(
            config, signalingForChannel);
        pcc.addEventListener('id', (messageEvent) => {
            channels.set(messageEvent.message, pcc);
        });
        return pcc;
    }

    // eslint-disable-next-line require-jsdoc
    function clean() {
        participants.clear();
        remoteStreams.clear();
    }

    Object.defineProperty(this, 'info', {
        configurable: false,
        get: () => {
            if (!room) {
                return null;
            }
            return new ConferenceInfo(room.id, Array.from(participants, (x) => x[
                1]), Array.from(remoteStreams, (x) => x[1]), me);
        },
    });

    /**
     * @function join
     * @instance
     * @desc Join a conference.
     * @memberof Owt.Conference.ConferenceClient
     * @return {Promise<ConferenceInfo, Error>} Return a promise resolved with current conference's information if successfully join the conference. Or return a promise rejected with a newly created Owt.Error if failed to join the conference.
     * @param {string} tokenString Token is issued by conference server(nuve).
     */
    this.join = function (tokenString) {
        return new Promise((resolve, reject) => {
            const token = JSON.parse(meetingBase64.decodeBase64(tokenString));
            const isSecured = (token.secure === true);
            let host = token.host;
            if (typeof host !== 'string') {
                reject(new Error('Invalid host.'));
                return;
            }
            if (host.indexOf('http') === -1) {
                host = isSecured ? ('https://' + host) : ('http://' + host);
                //  host = isSecured ? ('wss://' + host) : ('ws://' + host);
            }
            console.log(host);

            if (signalingState !== SignalingState.READY) {
                reject(new Error('connection state invalid'));
                return;
            }

            signalingState = SignalingState.CONNECTING;

            const loginInfo = {
                token: tokenString,
                userAgent: sysInfo(),
                protocol: protocolVersion,
            };

            signaling.connect(host, isSecured, loginInfo).then((resp) => {
                signalingState = SignalingState.CONNECTED;
                room = resp.room;
                if (room.streams !== undefined) {
                    for (const st of room.streams) {
                        if (st.type === 'mixed') {
                            st.viewport = st.info.label;
                        }
                        remoteStreams.set(st.id, createRemoteStream(st));
                    }
                }
                if (resp.room && resp.room.participants !== undefined) {
                    for (const p of resp.room.participants) {
                        participants.set(p.id, new Participant(p.id, p.role, p.user));
                        if (p.id === resp.id) {
                            me = participants.get(p.id);
                        }
                    }
                }
                resolve(new ConferenceInfo(resp.room.id, Array.from(participants
                    .values()), Array.from(remoteStreams.values()), me));
            }, (e) => {
                signalingState = SignalingState.READY;
                reject(new Error(e));
            });
        });
    };

    /**
     * @function publish
     * @memberof Owt.Conference.ConferenceClient
     * @instance
     * @desc Publish a LocalStream to conference server. Other participants will be able to subscribe this stream when it is successfully published.
     * @param {Owt.Base.LocalStream} stream The stream to be published.
     * @param {Owt.Base.PublishOptions} options Options for publication.
     * @param {string[]} videoCodecs Video codec names for publishing. Valid values are 'VP8', 'VP9' and 'H264'. This parameter only valid when options.video is RTCRtpEncodingParameters. Publishing with RTCRtpEncodingParameters is an experimental feature. This parameter is subject to change.
     * @return {Promise<Publication, Error>} Returned promise will be resolved with a newly created Publication once specific stream is successfully published, or rejected with a newly created Error if stream is invalid or options cannot be satisfied. Successfully published means PeerConnection is established and server is able to process media data.
     */
    this.publish = function (stream, options, videoCodecs) {
        if (!(stream instanceof LocalStream)) {
            return Promise.reject(new Error('Invalid stream.'));
        }
        if (publishChannels.has(stream.mediaStream.id)) {
            return Promise.reject(new Error(
                'Cannot publish a published stream.'));
        }
        const channel = createPeerConnectionChannel();
        meetingManager.data.pcc = channel;
        return channel.publish(stream, options, videoCodecs);
    };

    /**
     * @function subscribe
     * @memberof Owt.Conference.ConferenceClient
     * @instance
     * @desc Subscribe a RemoteStream from conference server.
     * @param {Owt.Base.RemoteStream} stream The stream to be subscribed.
     * @param {Owt.Conference.SubscribeOptions} options Options for subscription.
     * @return {Promise<Subscription, Error>} Returned promise will be resolved with a newly created Subscription once specific stream is successfully subscribed, or rejected with a newly created Error if stream is invalid or options cannot be satisfied. Successfully subscribed means PeerConnection is established and server was started to send media data.
     */
    this.subscribe = function (stream, options) {
        if (!(stream instanceof RemoteStream)) {
            return Promise.reject(new Error('Invalid stream.'));
        }
        const channel = createPeerConnectionChannel();
        return channel.subscribe(stream, options);
    };

    /**
     * @function send
     * @memberof Owt.Conference.ConferenceClient
     * @instance
     * @desc Send a text message to a participant or all participants.
     * @param {string} message Message to be sent.
     * @param {string} participantId Receiver of this message. Message will be sent to all participants if participantId is undefined.
     * @return {Promise<void, Error>} Returned promise will be resolved when conference server received certain message.
     */
    this.send = function (message, participantId) {
        if (participantId === undefined) {
            participantId = 'all';
        }
        return sendSignalingMessage('text', { to: participantId, message: message });
    };

    /**
     * @function leave
     * @memberOf Owt.Conference.ConferenceClient
     * @instance
     * @desc Leave a conference.
     * @return {Promise<void, Error>} Returned promise will be resolved with undefined once the connection is disconnected.
     */
    this.leave = function () {
        return signaling.disconnect().then(() => {
            clean();
            signalingState = SignalingState.READY;
        });
    };
};

const reconnectionAttempts = 10;

// eslint-disable-next-line require-jsdoc
function handleResponse(status, data, resolve, reject) {
    if (status === 'ok' || status === 'success') {
        resolve(data);
    } else if (status === 'error') {
        reject(data);
    } else {
        Logger.error('MCU returns unknown ack.');
    }
}

/**
 * @class SioSignaling
 * @classdesc Socket.IO signaling channel for ConferenceClient. It is not recommended to directly access this class.
 * @memberof Owt.Conference
 * @extends Owt.Base.EventDispatcher
 * @constructor
 */
class Signaling extends EventDispatcher {
    // eslint-disable-next-line require-jsdoc
    constructor() {
        super();
        this._socket = null;
        this._loggedIn = false;
        this._reconnectTimes = 0;
        this._reconnectionTicket = null;
        this._refreshReconnectionTicket = null;
    }

    /**
     * @function connect
     * @instance
     * @desc Connect to a portal.
     * @memberof Oms.Conference.SioSignaling
     * @return {Promise<Object, Error>} Return a promise resolved with the data returned by portal if successfully logged in. Or return a promise rejected with a newly created Oms.Error if failed.
     * @param {string} host Host of the portal.
     * @param {string} isSecured Is secure connection or not.
     * @param {string} loginInfo Infomation required for logging in.
     * @private.
     */
    connect(host, isSecured, loginInfo) {
        return new Promise((resolve, reject) => {
           
            const opts = {
                'reconnection': true,
                'reconnectionAttempts': reconnectionAttempts,
                'force new connection': true,
            };
            this._socket = io(host, opts);
            ['participant', 'text', 'stream', 'progress'].forEach((
                notification) => {
                this._socket.on(notification, (data) => {
                    this.dispatchEvent(new MessageEvent('data', {
                        message: {
                            notification: notification,
                            data: data,
                        },
                    }));
                });
            });
            this._socket.on('reconnecting', () => {
                this._reconnectTimes++;
            });
            this._socket.on('reconnect_failed', () => {
                if (this._reconnectTimes >= reconnectionAttempts) {
                    this.dispatchEvent(new OwtEvent('disconnect'));
                }
            });
            this._socket.on('connect_error', (e) => {
                reject(`connect_error:${host}`);
            });
            this._socket.on('drop', () => {
                this._reconnectTimes = reconnectionAttempts;
            });
            this._socket.on('disconnect', () => {
                this._clearReconnectionTask();
                if (this._reconnectTimes >= reconnectionAttempts) {
                    this._loggedIn = false;
                    this.dispatchEvent(new OwtEvent('disconnect'));
                }
            });

            this._socket.emit('login', loginInfo, (status, data) => {
                if (status === 'ok') {
                    this._loggedIn = true;
                    this._onReconnectionTicket(data.reconnectionTicket);
                    this._socket.on('connect', () => {
                        // re-login with reconnection ticket.
                        this._socket.emit('relogin', this._reconnectionTicket, (status,
                            data) => {
                            if (status === 'ok') {
                                this._reconnectTimes = 0;
                                this._onReconnectionTicket(data);
                            } else {
                                this.dispatchEvent(new OwtEvent('disconnect'));
                            }
                        });
                    });
                }
                handleResponse(status, data, resolve, reject);
            });
        });
    }

    /**
     * @function disconnect
     * @instance
     * @desc Disconnect from a portal.
     * @memberof Oms.Conference.SioSignaling
     * @return {Promise<Object, Error>} Return a promise resolved with the data returned by portal if successfully disconnected. Or return a promise rejected with a newly created Oms.Error if failed.
     * @private.
     */
    disconnect() {
        if (!this._socket || this._socket.disconnected) {
            return Promise.reject(new Error(
                'Portal is not connected.'));
        }
        return new Promise((resolve, reject) => {
            this._socket.emit('logout', (status, data) => {
                // Maximize the reconnect times to disable reconnection.
                this._reconnectTimes = reconnectionAttempts;
                this._socket.disconnect();
                handleResponse(status, data, resolve, reject);
            });
        });
    }

    /**
     * @function send
     * @instance
     * @desc Send data to portal.
     * @memberof Oms.Conference.SioSignaling
     * @return {Promise<Object, Error>} Return a promise resolved with the data returned by portal. Or return a promise rejected with a newly created Oms.Error if failed to send the message.
     * @param {string} requestName Name defined in client-server protocol.
     * @param {string} requestData Data format is defined in client-server protocol.
     * @private.
     */
    send(requestName, requestData) {
        return new Promise((resolve, reject) => {
            this._socket.emit(requestName, requestData, (status, data) => {
                handleResponse(status, data, resolve, reject);
            });
        });
    }

    /**
     * @function _onReconnectionTicket
     * @instance
     * @desc Parse reconnection ticket and schedule ticket refreshing.
     * @param {string} ticketString Reconnection ticket.
     * @memberof Owt.Conference.SioSignaling
     * @private.
     */
    _onReconnectionTicket(ticketString) {
        this._reconnectionTicket = ticketString;
        const ticket = JSON.parse(meetingBase64.decodeBase64(ticketString));
        // Refresh ticket 1 min or 10 seconds before it expires.
        const now = Date.now();
        const millisecondsInOneMinute = 60 * 1000;
        const millisecondsInTenSeconds = 10 * 1000;
        if (ticket.notAfter <= now - millisecondsInTenSeconds) {
            Logger.warning('Reconnection ticket expires too soon.');
            return;
        }
        let refreshAfter = ticket.notAfter - now - millisecondsInOneMinute;
        if (refreshAfter < 0) {
            refreshAfter = ticket.notAfter - now - millisecondsInTenSeconds;
        }
        this._clearReconnectionTask();
        this._refreshReconnectionTicket = setTimeout(() => {
            this._socket.emit('refreshReconnectionTicket', (status, data) => {
                if (status !== 'ok') {
                    Logger.warning('Failed to refresh reconnection ticket.');
                    return;
                }
                this._onReconnectionTicket(data);
            });
        }, refreshAfter);
    }

    /**
     * @function _clearReconnectionTask
     * @instance
     * @desc Stop trying to refresh reconnection ticket.
     * @memberof Owt.Conference.SioSignaling
     * @private.
     */
    _clearReconnectionTask() {
        clearTimeout(this._refreshReconnectionTicket);
        this._refreshReconnectionTicket = null;
    }
}


/**
 * @class OwtEvent
 * @classDesc Class OwtEvent represents a generic Event in the library.
 * @memberof Owt.Base
 * @hideconstructor
 */
class OwtEvent {
    // eslint-disable-next-line require-jsdoc
    constructor(type) {
        this.type = type;
    }
}

/**
 * @class MessageEvent
 * @classDesc Class MessageEvent represents a message Event in the library.
 * @memberof Owt.Base
 * @hideconstructor
 */
class MessageEvent extends OwtEvent {
    // eslint-disable-next-line require-jsdoc
    constructor(type, init) {
        super(type);
        /**
         * @member {string} origin
         * @instance
         * @memberof Owt.Base.MessageEvent
         * @desc ID of the remote endpoint who published this stream.
         */
        this.origin = init.origin;
        /**
         * @member {string} message
         * @instance
         * @memberof Owt.Base.MessageEvent
         */
        this.message = init.message;
        /**
         * @member {string} to
         * @instance
         * @memberof Owt.Base.MessageEvent
         * @desc Values could be "all", "me" in conference mode, or undefined in P2P mode..
         */
        this.to = init.to;
    }
}

/**
 * @class ErrorEvent
 * @classDesc Class ErrorEvent represents an error Event in the library.
 * @memberof Owt.Base
 * @hideconstructor
 */
class ErrorEvent extends OwtEvent {
    // eslint-disable-next-line require-jsdoc
    constructor(type, init) {
        super(type);
        /**
         * @member {Error} error
         * @instance
         * @memberof Owt.Base.ErrorEvent
         */
        this.error = init.error;
    }
}

/**
 * @class MuteEvent
 * @classDesc Class MuteEvent represents a mute or unmute event.
 * @memberof Owt.Base
 * @hideconstructor
 */
class MuteEvent extends OwtEvent {
    // eslint-disable-next-line require-jsdoc
    constructor(type, init) {
        super(type);
        /**
         * @member {Owt.Base.TrackKind} kind
         * @instance
         * @memberof Owt.Base.MuteEvent
         */
        this.kind = init.kind;
    }
}
/**
 * @class meetingBase64 
 */
const meetingBase64 = (function () {
    const END_OF_INPUT = -1;
    let base64Str;
    let base64Count;

    let i;

    const base64Chars = [
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
        'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
        'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
        'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
        'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
        'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
        'w', 'x', 'y', 'z', '0', '1', '2', '3',
        '4', '5', '6', '7', '8', '9', '+', '/',
    ];

    const reverseBase64Chars = [];

    for (i = 0; i < base64Chars.length; i = i + 1) {
        reverseBase64Chars[base64Chars[i]] = i;
    }

    const setBase64Str = function (str) {
        base64Str = str;
        base64Count = 0;
    };

    const readBase64 = function () {
        if (!base64Str) {
            return END_OF_INPUT;
        }
        if (base64Count >= base64Str.length) {
            return END_OF_INPUT;
        }
        const c = base64Str.charCodeAt(base64Count) & 0xff;
        base64Count = base64Count + 1;
        return c;
    };

    const encodeBase64 = function (str) {
        let result;
        let done;
        setBase64Str(str);
        result = '';
        const inBuffer = new Array(3);
        done = false;
        while (!done && (inBuffer[0] = readBase64()) !== END_OF_INPUT) {
            inBuffer[1] = readBase64();
            inBuffer[2] = readBase64();
            result = result + (base64Chars[inBuffer[0] >> 2]);
            if (inBuffer[1] !== END_OF_INPUT) {
                result = result + (base64Chars[((inBuffer[0] << 4) & 0x30) | (
                    inBuffer[1] >> 4)]);
                if (inBuffer[2] !== END_OF_INPUT) {
                    result = result + (base64Chars[((inBuffer[1] << 2) & 0x3c) | (
                        inBuffer[2] >> 6)]);
                    result = result + (base64Chars[inBuffer[2] & 0x3F]);
                } else {
                    result = result + (base64Chars[((inBuffer[1] << 2) & 0x3c)]);
                    result = result + ('=');
                    done = true;
                }
            } else {
                result = result + (base64Chars[((inBuffer[0] << 4) & 0x30)]);
                result = result + ('=');
                result = result + ('=');
                done = true;
            }
        }
        return result;
    };

    const readReverseBase64 = function () {
        if (!base64Str) {
            return END_OF_INPUT;
        }
        while (true) { // eslint-disable-line no-constant-condition
            if (base64Count >= base64Str.length) {
                return END_OF_INPUT;
            }
            const nextCharacter = base64Str.charAt(base64Count);
            base64Count = base64Count + 1;
            if (reverseBase64Chars[nextCharacter]) {
                return reverseBase64Chars[nextCharacter];
            }
            if (nextCharacter === 'A') {
                return 0;
            }
        }
    };

    const ntos = function (n) {
        n = n.toString(16);
        if (n.length === 1) {
            n = '0' + n;
        }
        n = '%' + n;
        return unescape(n);
    };

    const decodeBase64 = function (str) {
        let result;
        let done;
        setBase64Str(str);
        result = '';
        const inBuffer = new Array(4);
        done = false;
        while (!done && (inBuffer[0] = readReverseBase64()) !== END_OF_INPUT &&
            (inBuffer[1] = readReverseBase64()) !== END_OF_INPUT) {
            inBuffer[2] = readReverseBase64();
            inBuffer[3] = readReverseBase64();
            result = result + ntos((((inBuffer[0] << 2) & 0xff) | inBuffer[1] >>
                4));
            if (inBuffer[2] !== END_OF_INPUT) {
                result += ntos((((inBuffer[1] << 4) & 0xff) | inBuffer[2] >> 2));
                if (inBuffer[3] !== END_OF_INPUT) {
                    result = result + ntos((((inBuffer[2] << 6) & 0xff) | inBuffer[
                        3]));
                } else {
                    done = true;
                }
            } else {
                done = true;
            }
        }
        return result;
    };

    return {
        encodeBase64: encodeBase64,
        decodeBase64: decodeBase64,
    };
}());

const AudioSourceInfo = {
    MIC: 'mic',
    SCREENCAST: 'screen-cast',
    FILE: 'file',
    MIXED: 'mixed',
};

/**
 * @class VideoSourceInfo
 * @classDesc Source info about a video track. Values: 'camera', 'screen-cast', 'file', 'mixed'.
 * @memberOf Owt.Base
 * @readonly
 * @enum {string}
 */
const VideoSourceInfo = {
    CAMERA: 'camera',
    SCREENCAST: 'screen-cast',
    FILE: 'file',
    MIXED: 'mixed',
};

/**
 * @class TrackKind
 * @classDesc Kind of a track. Values: 'audio' for audio track, 'video' for video track, 'av' for both audio and video tracks.
 * @memberOf Owt.Base
 * @readonly
 * @enum {string}
 */
const TrackKind = {
    /**
     * Audio tracks.
     * @type string
     */
    AUDIO: 'audio',
    /**
     * Video tracks.
     * @type string
     */
    VIDEO: 'video',
    /**
     * Both audio and video tracks.
     * @type string
     */
    AUDIO_AND_VIDEO: 'av',
};
/**
 * @class Resolution
 * @memberOf Owt.Base
 * @classDesc The Resolution defines the size of a rectangle.
 * @constructor
 * @param {number} width
 * @param {number} height
 */
class Resolution {
    // eslint-disable-next-line require-jsdoc
    constructor(width, height) {
        /**
         * @member {number} width
         * @instance
         * @memberof Owt.Base.Resolution
         */
        this.width = width;
        /**
         * @member {number} height
         * @instance
         * @memberof Owt.Base.Resolution
         */
        this.height = height;
    }
}

class AudioTrackConstraints {
    // eslint-disable-next-line require-jsdoc
    constructor(source) {
        if (!Object.values(AudioSourceInfo)
            .some((v) => v === source)) {
            throw new TypeError('Invalid source.');
        }
	    /**
	     * @member {string} source
	     * @memberof Owt.Base.AudioTrackConstraints
	     * @desc Values could be "mic", "screen-cast", "file" or "mixed".
	     * @instance
	     */
        this.source = source;
	    /**
	     * @member {string} deviceId
	     * @memberof Owt.Base.AudioTrackConstraints
	     * @desc Do not provide deviceId if source is not "mic".
	     * @instance
	     * @see https://w3c.github.io/mediacapture-main/#def-constraint-deviceId
	     */
        this.deviceId = undefined;
    }
}

/**
 * @class VideoTrackConstraints
 * @classDesc Constraints for creating a video MediaStreamTrack.
 * @memberof Owt.Base
 * @constructor
 * @param {Owt.Base.VideoSourceInfo} source Source info of this video track.
 */
class VideoTrackConstraints {
    // eslint-disable-next-line require-jsdoc
    constructor(source) {
        if (!Object.values(VideoSourceInfo)
            .some((v) => v === source)) {
            throw new TypeError('Invalid source.');
        }
	    /**
	     * @member {string} source
	     * @memberof Owt.Base.VideoTrackConstraints
	     * @desc Values could be "camera", "screen-cast", "file" or "mixed".
	     * @instance
	     */
        this.source = source;
	    /**
	     * @member {string} deviceId
	     * @memberof Owt.Base.VideoTrackConstraints
	     * @desc Do not provide deviceId if source is not "camera".
	     * @instance
	     * @see https://w3c.github.io/mediacapture-main/#def-constraint-deviceId
	     */

        this.deviceId = undefined;

	    /**
	     * @member {Owt.Base.Resolution} resolution
	     * @memberof Owt.Base.VideoTrackConstraints
	     * @instance
	     */
        this.resolution = undefined;

	    /**
	     * @member {number} frameRate
	     * @memberof Owt.Base.VideoTrackConstraints
	     * @instance
	     */
        this.frameRate = undefined;
    }
}
/**
 * @class StreamConstraints
 * @classDesc Constraints for creating a MediaStream from screen mic and camera.
 * @memberof Owt.Base
 * @constructor
 * @param {?Owt.Base.AudioTrackConstraints} audioConstraints
 * @param {?Owt.Base.VideoTrackConstraints} videoConstraints
 */
class StreamConstraints {
    // eslint-disable-next-line require-jsdoc
    constructor(audioConstraints = false, videoConstraints = false) {
	    /**
	     * @member {Owt.Base.MediaStreamTrackDeviceConstraintsForAudio} audio
	     * @memberof Owt.Base.MediaStreamDeviceConstraints
	     * @instance
	     */
        this.audio = audioConstraints;
	    /**
	     * @member {Owt.Base.MediaStreamTrackDeviceConstraintsForVideo} Video
	     * @memberof Owt.Base.MediaStreamDeviceConstraints
	     * @instance
	     */
        this.video = videoConstraints;
    }
}

// eslint-disable-next-line require-jsdoc
function isVideoConstrainsForScreenCast(constraints) {
    return (typeof constraints.video === 'object' && constraints.video.source === VideoSourceInfo.SCREENCAST);
}

/**
 * @class MediaStreamFactory
 * @classDesc A factory to create MediaStream. You can also create MediaStream by yourself.
 * @memberof Owt.Base
 */
class MediaStreamFactory {
    /**
     * @function createMediaStream
     * @static
     * @desc Create a MediaStream with given constraints. If you want to create a MediaStream for screen cast, please make sure both audio and video's source are "screen-cast".
     * @memberof Owt.Base.MediaStreamFactory
     * @return {Promise<MediaStream, Error>} Return a promise that is resolved when stream is successfully created, or rejected if one of the following error happened:
     * - One or more parameters cannot be satisfied.
     * - Specified device is busy.
     * - Cannot obtain necessary permission or operation is canceled by user.
     * - Video source is screen cast, while audio source is not.
     * - Audio source is screen cast, while video source is disabled.
     * @param {Owt.Base.StreamConstraints} constraints
     */
    static createMediaStream(constraints) {
        if (typeof constraints !== 'object' ||
            (!constraints.audio && !constraints.video)) {
            return Promise.reject(new TypeError('Invalid constrains'));
        }
        if (!isVideoConstrainsForScreenCast(constraints) &&
            (typeof constraints.audio === 'object') &&
            constraints.audio.source ===
            AudioSourceInfo.SCREENCAST) {
            return Promise.reject(
                new TypeError('Cannot share screen without video.'));
        }
        if (isVideoConstrainsForScreenCast(constraints) && !Top.Util.Browser.isChrome() &&
            !Top.Util.Browser.isFirefox()) {
            return Promise.reject(
                new TypeError('Screen sharing only supports Chrome and Firefox.'));
        }
        if (isVideoConstrainsForScreenCast(constraints) &&
            typeof constraints.audio === 'object' &&
            constraints.audio.source !==
            AudioSourceInfo.SCREENCAST) {
            return Promise.reject(new TypeError(
                'Cannot capture video from screen cast while capture audio from'
                + ' other source.'));
        }

        // Check and convert constraints.
        if (!constraints.audio && !constraints.video) {
            return Promise.reject(new TypeError(
                'At least one of audio and video must be requested.'));
        }
        const mediaConstraints = Object.create({});
        if (typeof constraints.audio === 'object' &&
            constraints.audio.source === AudioSourceInfo.MIC) {
            mediaConstraints.audio = Object.create({});

	    mediaConstraints.audio.echoCancellation = true; // Added line for AEC
	    mediaConstraints.audio.noiseSuppression = true; // Added line for NC
	    if (Top.Util.Browser.isEdge()) {
                mediaConstraints.audio.deviceId = constraints.audio.deviceId;
            } else {
                mediaConstraints.audio.deviceId = {
                    exact: constraints.audio.deviceId,
                };
            }
        } else {
            if (constraints.audio.source ===
                AudioSourceInfo.SCREENCAST) {
                mediaConstraints.audio = true;
            } else {
                mediaConstraints.audio = constraints.audio;
            }
        }
        if (typeof constraints.video === 'object') {
            mediaConstraints.video = Object.create({});
            if (typeof constraints.video.frameRate === 'number') {
                mediaConstraints.video.frameRate = constraints.video.frameRate;
            }
            if (constraints.video.resolution &&
                constraints.video.resolution.width &&
                constraints.video.resolution.height) {
                if (constraints.video.source ===
                    VideoSourceInfo.SCREENCAST) {
                    mediaConstraints.video.width = constraints.video.resolution.width;
                    mediaConstraints.video.height = constraints.video.resolution.height;
                } else {
                    mediaConstraints.video.width = Object.create({});
                    mediaConstraints.video.width.exact =
                        constraints.video.resolution.width;
                    mediaConstraints.video.height = Object.create({});
                    mediaConstraints.video.height.exact =
                        constraints.video.resolution.height;
                }
            }
            if (typeof constraints.video.deviceId === 'string') {
                mediaConstraints.video.deviceId = { exact: constraints.video.deviceId };
            }
            if (Top.Util.Browser.isFirefox() &&
                constraints.video.source ===
                VideoSourceInfo.SCREENCAST) {
                mediaConstraints.video.mediaSource = 'screen';
            }
        } else {
            mediaConstraints.video = constraints.video;
        }

        if (isVideoConstrainsForScreenCast(constraints)) {
            return navigator.mediaDevices.getDisplayMedia(mediaConstraints);
        } else {
            return navigator.mediaDevices.getUserMedia(mediaConstraints);
        }
    }
}

function sysInfo() {
    const info = Object.create({});
    const sdkVersion = '4.3';
    info.sdk = {
        version: sdkVersion,
        type: 'JavaScript',
    };
    // Runtime info.
    const userAgent = navigator.userAgent;
    const firefoxRegex = /Firefox\/([0-9.]+)/;
    const chromeRegex = /Chrome\/([0-9.]+)/;
    const edgeRegex = /Edge\/([0-9.]+)/;
    const safariVersionRegex = /Version\/([0-9.]+) Safari/;
    let result = chromeRegex.exec(userAgent);
    if (result) {
        info.runtime = {
            name: 'Chrome',
            version: result[1],
        };
    } else if (result = firefoxRegex.exec(userAgent)) {
        info.runtime = {
            name: 'Firefox',
            version: result[1],
        };
    } else if (result = edgeRegex.exec(userAgent)) {
        info.runtime = {
            name: 'Edge',
            version: result[1],
        };
    } else if (isSafari()) {
        result = safariVersionRegex.exec(userAgent);
        info.runtime = {
            name: 'Safari',
        };
        info.runtime.version = result ? result[1] : 'Unknown';
    } else {
        info.runtime = {
            name: 'Unknown',
            version: 'Unknown',
        };
    }
    // OS info.
    const windowsRegex = /Windows NT ([0-9.]+)/;
    const macRegex = /Intel Mac OS X ([0-9_.]+)/;
    const iPhoneRegex = /iPhone OS ([0-9_.]+)/;
    const linuxRegex = /X11; Linux/;
    const androidRegex = /Android( ([0-9.]+))?/;
    const chromiumOsRegex = /CrOS/;
    if (result = windowsRegex.exec(userAgent)) {
        info.os = {
            name: 'Windows NT',
            version: result[1],
        };
    } else if (result = macRegex.exec(userAgent)) {
        info.os = {
            name: 'Mac OS X',
            version: result[1].replace(/_/g, '.'),
        };
    } else if (result = iPhoneRegex.exec(userAgent)) {
        info.os = {
            name: 'iPhone OS',
            version: result[1].replace(/_/g, '.'),
        };
    } else if (result = linuxRegex.exec(userAgent)) {
        info.os = {
            name: 'Linux',
            version: 'Unknown',
        };
    } else if (result = androidRegex.exec(userAgent)) {
        info.os = {
            name: 'Android',
            version: result[1] || 'Unknown',
        };
    } else if (result = chromiumOsRegex.exec(userAgent)) {
        info.os = {
            name: 'Chrome OS',
            version: 'Unknown',
        };
    } else {
        info.os = {
            name: 'Unknown',
            version: 'Unknown',
        };
    }
    info.capabilities = {
        continualIceGathering: false,
        unifiedPlan: true,
        streamRemovable: info.runtime.name !== 'Firefox',
    };
    return info;
}

function createUuid() {
    return 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        const r = Math.random() * 16 | 0;
        const v = c === 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

function isAllowedValue(obj, allowedValues) {
    return (allowedValues.some((ele) => {
        return ele === obj;
    }));
}
/**
 * @class StreamSourceInfo
 * @memberOf Owt.Base
 * @classDesc Information of a stream's source.
 * @constructor
 * @description Audio source info or video source info could be undefined if a stream does not have audio/video track.
 * @param {?string} audioSourceInfo Audio source info. Accepted values are: "mic", "screen-cast", "file", "mixed" or undefined.
 * @param {?string} videoSourceInfo Video source info. Accepted values are: "camera", "screen-cast", "file", "mixed" or undefined.
 */
class StreamSourceInfo {
    // eslint-disable-next-line require-jsdoc
    constructor(audioSourceInfo, videoSourceInfo) {
        if (!isAllowedValue(audioSourceInfo, [undefined, 'mic', 'screen-cast',
            'file', 'mixed'])) {
            throw new TypeError('Incorrect value for audioSourceInfo');
        }
        if (!isAllowedValue(videoSourceInfo, [undefined, 'camera', 'screen-cast',
            'file', 'encoded-file', 'raw-file', 'mixed'])) {
            throw new TypeError('Incorrect value for videoSourceInfo');
        }
        this.audio = audioSourceInfo;
        this.video = videoSourceInfo;
    }
}
/**
 * @class Stream
 * @memberOf Owt.Base
 * @classDesc Base class of streams.
 * @extends Owt.Base.EventDispatcher
 * @hideconstructor
 */
class Stream extends EventDispatcher {
    // eslint-disable-next-line require-jsdoc
    constructor(stream, sourceInfo, attributes) {
        super();
        if ((stream && !(stream instanceof MediaStream)) || (typeof sourceInfo !==
            'object')) {
            throw new TypeError('Invalid stream or sourceInfo.');
        }
        if (stream && ((stream.getAudioTracks().length > 0 && !sourceInfo.audio) ||
            stream.getVideoTracks().length > 0 && !sourceInfo.video)) {
            throw new TypeError('Missing audio source info or video source info.');
        }
        /**
         * @member {?MediaStream} mediaStream
         * @instance
         * @memberof Owt.Base.Stream
         * @see {@link https://www.w3.org/TR/mediacapture-streams/#mediastream|MediaStream API of Media Capture and Streams}.
         */
        Object.defineProperty(this, 'mediaStream', {
            configurable: false,
            writable: true,
            value: stream,
        });
        /**
         * @member {Owt.Base.StreamSourceInfo} source
         * @instance
         * @memberof Owt.Base.Stream
         * @desc Source info of a stream.
         */
        Object.defineProperty(this, 'source', {
            configurable: false,
            writable: false,
            value: sourceInfo,
        });
        /**
         * @member {object} attributes
         * @instance
         * @memberof Owt.Base.Stream
         * @desc Custom attributes of a stream.
         */
        Object.defineProperty(this, 'attributes', {
            configurable: true,
            writable: false,
            value: attributes,
        });
    }
}
/**
 * @class LocalStream
 * @classDesc Stream captured from current endpoint.
 * @memberOf Owt.Base
 * @extends Owt.Base.Stream
 * @constructor
 * @param {MediaStream} stream Underlying MediaStream.
 * @param {Owt.Base.StreamSourceInfo} sourceInfo Information about stream's source.
 * @param {object} attributes Custom attributes of the stream.
 */
class LocalStream extends Stream {
    // eslint-disable-next-line require-jsdoc
    constructor(stream, sourceInfo, attributes) {
        if (!(stream instanceof MediaStream)) {
            throw new TypeError('Invalid stream.');
        }
        super(stream, sourceInfo, attributes);
        /**
         * @member {string} id
         * @instance
         * @memberof Owt.Base.LocalStream
         */
        Object.defineProperty(this, 'id', {
            configurable: false,
            writable: false,
            value: createUuid(),
        });
    }
}
/**
 * @class RemoteStream
 * @classDesc Stream sent from a remote endpoint.
 * Events:
 *
 * | Event Name      | Argument Type    | Fired when         |
 * | ----------------| ---------------- | ------------------ |
 * | ended           | Event            | Stream is ended.   |
 * | updated         | Event            | Stream is updated. |
 *
 * @memberOf Owt.Base
 * @extends Owt.Base.Stream
 * @hideconstructor
 */
class RemoteStream extends Stream {
    // eslint-disable-next-line require-jsdoc
    constructor(id, origin, stream, sourceInfo, attributes) {
        super(stream, sourceInfo, attributes);
        /**
         * @member {string} id
         * @instance
         * @memberof Owt.Base.RemoteStream
         */
        Object.defineProperty(this, 'id', {
            configurable: false,
            writable: false,
            value: id ? id : createUuid(),
        });
        /**
         * @member {string} origin
         * @instance
         * @memberof Owt.Base.RemoteStream
         * @desc ID of the remote endpoint who published this stream.
         */
        Object.defineProperty(this, 'origin', {
            configurable: false,
            writable: false,
            value: origin,
        });
        /**
         * @member {Owt.Base.PublicationSettings} settings
         * @instance
         * @memberof Owt.Base.RemoteStream
         * @desc Original settings for publishing this stream. This property is only valid in conference mode.
         */
        this.settings = undefined;
        /**
         * @member {Owt.Conference.SubscriptionCapabilities} extraCapabilities
         * @instance
         * @memberof Owt.Base.RemoteStream
         * @desc Extra capabilities remote endpoint provides for subscription. Extra capabilities don't include original settings. This property is only valid in conference mode.
         */
        this.extraCapabilities = undefined;
    }
}

/**
 * @class StreamEvent
 * @classDesc Event for Stream.
 * @extends Owt.Base.OwtEvent
 * @memberof Owt.Base
 * @hideconstructor
 */
class StreamEvent extends OwtEvent {
    // eslint-disable-next-line require-jsdoc
    constructor(type, init) {
        super(type);
        /**
         * @member {Owt.Base.Stream} stream
         * @instance
         * @memberof Owt.Base.StreamEvent
         */
        this.stream = init.stream;
    }
}


function extractBitrateMultiplier(input) {
    if (typeof input !== 'string' || !input.startsWith('x')) {
        Logger.warning('Invalid bitrate multiplier input.');
        return 0;
    }
    return Number.parseFloat(input.replace(/^x/, ''));
}

// eslint-disable-next-line require-jsdoc
function sortNumbers(x, y) {
    return x - y;
}

// eslint-disable-next-line require-jsdoc
function sortResolutions(x, y) {
    if (x.width !== y.width) {
        return x.width - y.width;
    } else {
        return x.height - y.height;
    }
}

/**
 * @function convertToPublicationSettings
 * @desc Convert mediaInfo received from server to PublicationSettings.
 * @private
 */
function convertToPublicationSettings(mediaInfo) {
    let audio = [],
        video = [];
    let audioCodec, videoCodec, resolution, framerate, bitrate, keyFrameInterval,
        rid;
    if (mediaInfo.audio) {
        if (mediaInfo.audio.format) {
            audioCodec = new AudioCodecParameters(
                mediaInfo.audio.format.codec, mediaInfo.audio.format.channelNum,
                mediaInfo.audio.format.sampleRate);
        }
        audio.push(new AudioPublicationSettings(audioCodec));
    }
    if (mediaInfo.video) {
        for (const videoInfo of mediaInfo.video.original) {
            if (videoInfo.format) {
                videoCodec = new VideoCodecParameters(
                    videoInfo.format.codec, videoInfo.format.profile);
            }
            if (videoInfo.parameters) {
                if (videoInfo.parameters.resolution) {
                    resolution = new Resolution(
                        videoInfo.parameters.resolution.width,
                        videoInfo.parameters.resolution.height);
                }
                framerate = videoInfo.parameters.framerate;
                bitrate = videoInfo.parameters.bitrate * 1000;
                keyFrameInterval = videoInfo.parameters.keyFrameInterval;
            }
            if (videoInfo.simulcastRid) {
                rid = videoInfo.simulcastRid;
            }
            video.push(new VideoPublicationSettings(
                videoCodec, resolution, framerate, bitrate, keyFrameInterval, rid));
        }
    }
    return new PublicationSettings(audio, video);
}

/**
 * @function convertToSubscriptionCapabilities
 * @desc Convert mediaInfo received from server to SubscriptionCapabilities.
 * @private
 */
function convertToSubscriptionCapabilities(mediaInfo) {
    let audio; let video;
    if (mediaInfo.audio) {
        const audioCodecs = [];
        if (mediaInfo.audio && mediaInfo.audio.optional &&
            mediaInfo.audio.optional.format) {
            for (const audioCodecInfo of mediaInfo.audio.optional.format) {
                const audioCodec = new AudioCodecParameters(
                    audioCodecInfo.codec, audioCodecInfo.channelNum,
                    audioCodecInfo.sampleRate);
                audioCodecs.push(audioCodec);
            }
        }
        audioCodecs.sort();
        audio = new AudioSubscriptionCapabilities(audioCodecs);
    }
    if (mediaInfo.video) {
        const videoCodecs = [];
        if (mediaInfo.video && mediaInfo.video.optional &&
            mediaInfo.video.optional.format) {
            for (const videoCodecInfo of mediaInfo.video.optional.format) {
                const videoCodec = new VideoCodecParameters(
                    videoCodecInfo.codec, videoCodecInfo.profile);
                videoCodecs.push(videoCodec);
            }
        }
        videoCodecs.sort();
        const resolutions = Array.from(
            mediaInfo.video.optional.parameters.resolution,
            (r) => new Resolution(r.width, r.height));
        resolutions.sort(sortResolutions);
        const bitrates = Array.from(
            mediaInfo.video.optional.parameters.bitrate,
            (bitrate) => extractBitrateMultiplier(bitrate));
        bitrates.push(1.0);
        bitrates.sort(sortNumbers);
        const frameRates = JSON.parse(
            JSON.stringify(mediaInfo.video.optional.parameters.framerate));
        frameRates.sort(sortNumbers);
        const keyFrameIntervals = JSON.parse(
            JSON.stringify(mediaInfo.video.optional.parameters.keyFrameInterval));
        keyFrameIntervals.sort(sortNumbers);
        video = new VideoSubscriptionCapabilities(
            videoCodecs, resolutions, frameRates, bitrates, keyFrameIntervals);
    }
    return new SubscriptionCapabilities(audio, video);
}


const AudioCodec = {
    PCMU: 'pcmu',
    PCMA: 'pcma',
    OPUS: 'opus',
    G722: 'g722',
    ISAC: 'iSAC',
    ILBC: 'iLBC',
    AAC: 'aac',
    AC3: 'ac3',
    NELLYMOSER: 'nellymoser',
};
/**
 * @class AudioCodecParameters
 * @memberOf Owt.Base
 * @classDesc Codec parameters for an audio track.
 * @hideconstructor
 */
class AudioCodecParameters {
    // eslint-disable-next-line require-jsdoc
    constructor(name, channelCount, clockRate) {
        /**
         * @member {string} name
         * @memberof Owt.Base.AudioCodecParameters
         * @instance
         * @desc Name of a codec. Please use a value in Owt.Base.AudioCodec. However, some functions do not support all the values in Owt.Base.AudioCodec.
         */
        this.name = name;
        /**
         * @member {?number} channelCount
         * @memberof Owt.Base.AudioCodecParameters
         * @instance
         * @desc Numbers of channels for an audio track.
         */
        this.channelCount = channelCount;
        /**
         * @member {?number} clockRate
         * @memberof Owt.Base.AudioCodecParameters
         * @instance
         * @desc The codec clock rate expressed in Hertz.
         */
        this.clockRate = clockRate;
    }
}

/**
 * @class AudioEncodingParameters
 * @memberOf Owt.Base
 * @classDesc Encoding parameters for sending an audio track.
 * @hideconstructor
 */
class AudioEncodingParameters {
    // eslint-disable-next-line require-jsdoc
    constructor(codec, maxBitrate) {
        /**
         * @member {?Owt.Base.AudioCodecParameters} codec
         * @instance
         * @memberof Owt.Base.AudioEncodingParameters
         */
        this.codec = codec;
        /**
         * @member {?number} maxBitrate
         * @instance
         * @memberof Owt.Base.AudioEncodingParameters
         * @desc Max bitrate expressed in kbps.
         */
        this.maxBitrate = maxBitrate;
    }
}

/**
 * @class VideoCodec
 * @memberOf Owt.Base
 * @classDesc Video codec enumeration.
 * @hideconstructor
 */
const VideoCodec = {
    VP8: 'vp8',
    VP9: 'vp9',
    H264: 'h264',
    H265: 'h265',
};

/**
 * @class VideoCodecParameters
 * @memberOf Owt.Base
 * @classDesc Codec parameters for a video track.
 * @hideconstructor
 */
class VideoCodecParameters {
    // eslint-disable-next-line require-jsdoc
    constructor(name, profile) {
        /**
         * @member {string} name
         * @memberof Owt.Base.VideoCodecParameters
         * @instance
         * @desc Name of a codec. Please use a value in Owt.Base.VideoCodec. However, some functions do not support all the values in Owt.Base.AudioCodec.
         */
        this.name = name;
        /**
         * @member {?string} profile
         * @memberof Owt.Base.VideoCodecParameters
         * @instance
         * @desc The profile of a codec. Profile may not apply to all codecs.
         */
        this.profile = profile;
    }
}

/**
 * @class VideoEncodingParameters
 * @memberOf Owt.Base
 * @classDesc Encoding parameters for sending a video track.
 * @hideconstructor
 */
class VideoEncodingParameters {
    // eslint-disable-next-line require-jsdoc
    constructor(codec, maxBitrate) {
        /**
         * @member {?Owt.Base.VideoCodecParameters} codec
         * @instance
         * @memberof Owt.Base.VideoEncodingParameters
         */
        this.codec = codec;
        /**
         * @member {?number} maxBitrate
         * @instance
         * @memberof Owt.Base.VideoEncodingParameters
         * @desc Max bitrate expressed in kbps.
         */
        this.maxBitrate = maxBitrate;
    }
}

class AudioPublicationSettings {
    // eslint-disable-next-line require-jsdoc
    constructor(codec) {
        /**
         * @member {?Owt.Base.AudioCodecParameters} codec
         * @instance
         * @memberof Owt.Base.AudioPublicationSettings
         */
        this.codec = codec;
    }
}

/**
 * @class VideoPublicationSettings
 * @memberOf Owt.Base
 * @classDesc The video settings of a publication.
 * @hideconstructor
 */
class VideoPublicationSettings {
    // eslint-disable-next-line require-jsdoc
    constructor(codec, resolution, frameRate, bitrate, keyFrameInterval, rid) {
        /**
         * @member {?Owt.Base.VideoCodecParameters} codec
         * @instance
         * @memberof Owt.Base.VideoPublicationSettings
         */
        this.codec = codec,
            /**
             * @member {?Owt.Base.Resolution} resolution
             * @instance
             * @memberof Owt.Base.VideoPublicationSettings
             */
            this.resolution = resolution;
        /**
         * @member {?number} frameRates
         * @instance
         * @classDesc Frames per second.
         * @memberof Owt.Base.VideoPublicationSettings
         */
        this.frameRate = frameRate;
        /**
         * @member {?number} bitrate
         * @instance
         * @memberof Owt.Base.VideoPublicationSettings
         */
        this.bitrate = bitrate;
        /**
         * @member {?number} keyFrameIntervals
         * @instance
         * @classDesc The time interval between key frames. Unit: second.
         * @memberof Owt.Base.VideoPublicationSettings
         */
        this.keyFrameInterval = keyFrameInterval;
        /**
         * @member {?number} rid
         * @instance
         * @classDesc Restriction identifier to identify the RTP Streams within an RTP session.
         * @memberof Owt.Base.VideoPublicationSettings
         */
        this.rid = rid;
    }
}

/**
 * @class PublicationSettings
 * @memberOf Owt.Base
 * @classDesc The settings of a publication.
 * @hideconstructor
 */
class PublicationSettings {
    // eslint-disable-next-line require-jsdoc
    constructor(audio, video) {
        /**
         * @member {Owt.Base.AudioPublicationSettings[]} audio
         * @instance
         * @memberof Owt.Base.PublicationSettings
         */
        this.audio = audio;
        /**
         * @member {Owt.Base.VideoPublicationSettings[]} video
         * @instance
         * @memberof Owt.Base.PublicationSettings
         */
        this.video = video;
    }
}

/**
 * @class Publication
 * @extends Owt.Base.EventDispatcher
 * @memberOf Owt.Base
 * @classDesc Publication represents a sender for publishing a stream. It
 * handles the actions on a LocalStream published to a conference.
 *
 * Events:
 *
 * | Event Name      | Argument Type    | Fired when       |
 * | ----------------| ---------------- | ---------------- |
 * | ended           | Event            | Publication is ended. |
 * | error           | ErrorEvent       | An error occurred on the publication. |
 * | mute            | MuteEvent        | Publication is muted. Client stopped sending audio and/or video data to remote endpoint. |
 * | unmute          | MuteEvent        | Publication is unmuted. Client continued sending audio and/or video data to remote endpoint. |
 *
 * `ended` event may not be fired on Safari after calling `Publication.stop()`.
 *
 * @hideconstructor
 */
class Publication extends EventDispatcher {
    // eslint-disable-next-line require-jsdoc
    constructor(id, stop, getStats, mute, unmute) {
        super();
        /**
         * @member {string} id
         * @instance
         * @memberof Owt.Base.Publication
         */
        Object.defineProperty(this, 'id', {
            configurable: false,
            writable: false,
            value: id ? id : createUuid(),
        });
        /**
         * @function stop
         * @instance
         * @desc Stop certain publication. Once a subscription is stopped, it cannot be recovered.
         * @memberof Owt.Base.Publication
         * @returns {undefined}
         */
        this.stop = stop;
        /**
         * @function getStats
         * @instance
         * @desc Get stats of underlying PeerConnection.
         * @memberof Owt.Base.Publication
         * @returns {Promise<RTCStatsReport, Error>}
         */
        this.getStats = getStats;
        /**
         * @function mute
         * @instance
         * @desc Stop sending data to remote endpoint.
         * @memberof Owt.Base.Publication
         * @param {Owt.Base.TrackKind } kind Kind of tracks to be muted.
         * @returns {Promise<undefined, Error>}
         */
        this.mute = mute;
        /**
         * @function unmute
         * @instance
         * @desc Continue sending data to remote endpoint.
         * @memberof Owt.Base.Publication
         * @param {Owt.Base.TrackKind } kind Kind of tracks to be unmuted.
         * @returns {Promise<undefined, Error>}
         */
        this.unmute = unmute;
    }
}

/**
 * @class PublishOptions
 * @memberOf Owt.Base
 * @classDesc PublishOptions defines options for publishing a Owt.Base.LocalStream.
 */
class PublishOptions {
    // eslint-disable-next-line require-jsdoc
    constructor(audio, video) {
        /**
         * @member {?Array<Owt.Base.AudioEncodingParameters> | ?Array<RTCRtpEncodingParameters>} audio
         * @instance
         * @memberof Owt.Base.PublishOptions
         * @desc Parameters for audio RtpSender. Publishing with RTCRtpEncodingParameters is an experimental feature. It is subject to change.
         */
        this.audio = audio;
        /**
         * @member {?Array<Owt.Base.VideoEncodingParameters> | ?Array<RTCRtpEncodingParameters>} video
         * @instance
         * @memberof Owt.Base.PublishOptions
         * @desc Parameters for video RtpSender. Publishing with RTCRtpEncodingParameters is an experimental feature. It is subject to change.
         */
        this.video = video;
    }
}


class AudioSubscriptionCapabilities {
    // eslint-disable-next-line require-jsdoc
    constructor(codecs) {
        /**
         * @member {Array.<Owt.Base.AudioCodecParameters>} codecs
         * @instance
         * @memberof Owt.Conference.AudioSubscriptionCapabilities
         */
        this.codecs = codecs;
    }
}

/**
 * @class VideoSubscriptionCapabilities
 * @memberOf Owt.Conference
 * @classDesc Represents the video capability for subscription.
 * @hideconstructor
 */
class VideoSubscriptionCapabilities {
    // eslint-disable-next-line require-jsdoc
    constructor(codecs, resolutions, frameRates, bitrateMultipliers,
        keyFrameIntervals) {
        /**
         * @member {Array.<Owt.Base.VideoCodecParameters>} codecs
         * @instance
         * @memberof Owt.Conference.VideoSubscriptionCapabilities
         */
        this.codecs = codecs;
        /**
         * @member {Array.<Owt.Base.Resolution>} resolutions
         * @instance
         * @memberof Owt.Conference.VideoSubscriptionCapabilities
         */
        this.resolutions = resolutions;
        /**
         * @member {Array.<number>} frameRates
         * @instance
         * @memberof Owt.Conference.VideoSubscriptionCapabilities
         */
        this.frameRates = frameRates;
        /**
         * @member {Array.<number>} bitrateMultipliers
         * @instance
         * @memberof Owt.Conference.VideoSubscriptionCapabilities
         */
        this.bitrateMultipliers = bitrateMultipliers;
        /**
         * @member {Array.<number>} keyFrameIntervals
         * @instance
         * @memberof Owt.Conference.VideoSubscriptionCapabilities
         */
        this.keyFrameIntervals = keyFrameIntervals;
    }
}

/**
 * @class SubscriptionCapabilities
 * @memberOf Owt.Conference
 * @classDesc Represents the capability for subscription.
 * @hideconstructor
 */
class SubscriptionCapabilities {
    // eslint-disable-next-line require-jsdoc
    constructor(audio, video) {
        /**
         * @member {?Owt.Conference.AudioSubscriptionCapabilities} audio
         * @instance
         * @memberof Owt.Conference.SubscriptionCapabilities
         */
        this.audio = audio;
        /**
         * @member {?Owt.Conference.VideoSubscriptionCapabilities} video
         * @instance
         * @memberof Owt.Conference.SubscriptionCapabilities
         */
        this.video = video;
    }
}

/**
 * @class AudioSubscriptionConstraints
 * @memberOf Owt.Conference
 * @classDesc Represents the audio constraints for subscription.
 * @hideconstructor
 */
class AudioSubscriptionConstraints {
    // eslint-disable-next-line require-jsdoc
    constructor(codecs) {
        /**
         * @member {?Array.<Owt.Base.AudioCodecParameters>} codecs
         * @instance
         * @memberof Owt.Conference.AudioSubscriptionConstraints
         * @desc Codecs accepted. If none of `codecs` supported by both sides, connection fails. Leave it undefined will use all possible codecs.
         */
        this.codecs = codecs;
    }
}

/**
 * @class VideoSubscriptionConstraints
 * @memberOf Owt.Conference
 * @classDesc Represents the video constraints for subscription.
 * @hideconstructor
 */
class VideoSubscriptionConstraints {
    // eslint-disable-next-line require-jsdoc
    constructor(codecs, resolution, frameRate, bitrateMultiplier,
        keyFrameInterval, rid) {
        /**
         * @member {?Array.<Owt.Base.VideoCodecParameters>} codecs
         * @instance
         * @memberof Owt.Conference.VideoSubscriptionConstraints
         * @desc Codecs accepted. If none of `codecs` supported by both sides, connection fails. Leave it undefined will use all possible codecs.
         */
        this.codecs = codecs;
        /**
         * @member {?Owt.Base.Resolution} resolution
         * @instance
         * @memberof Owt.Conference.VideoSubscriptionConstraints
         * @desc Only resolutions listed in Owt.Conference.VideoSubscriptionCapabilities are allowed.
         */
        this.resolution = resolution;
        /**
         * @member {?number} frameRate
         * @instance
         * @memberof Owt.Conference.VideoSubscriptionConstraints
         * @desc Only frameRates listed in Owt.Conference.VideoSubscriptionCapabilities are allowed.
         */
        this.frameRate = frameRate;
        /**
         * @member {?number} bitrateMultiplier
         * @instance
         * @memberof Owt.Conference.VideoSubscriptionConstraints
         * @desc Only bitrateMultipliers listed in Owt.Conference.VideoSubscriptionCapabilities are allowed.
         */
        this.bitrateMultiplier = bitrateMultiplier;
        /**
         * @member {?number} keyFrameInterval
         * @instance
         * @memberof Owt.Conference.VideoSubscriptionConstraints
         * @desc Only keyFrameIntervals listed in Owt.Conference.VideoSubscriptionCapabilities are allowed.
         */
        this.keyFrameInterval = keyFrameInterval;
        /**
         * @member {?number} rid
         * @instance
         * @memberof Owt.Conference.VideoSubscriptionConstraints
         * @desc Restriction identifier to identify the RTP Streams within an RTP session. When rid is specified, other constraints will be ignored.
         */
        this.rid = rid;
    }
}

/**
 * @class SubscribeOptions
 * @memberOf Owt.Conference
 * @classDesc SubscribeOptions defines options for subscribing a Owt.Base.RemoteStream.
 */
class SubscribeOptions {
    // eslint-disable-next-line require-jsdoc
    constructor(audio, video) {
        /**
         * @member {?Owt.Conference.AudioSubscriptionConstraints} audio
         * @instance
         * @memberof Owt.Conference.SubscribeOptions
         */
        this.audio = audio;
        /**
         * @member {?Owt.Conference.VideoSubscriptionConstraints} video
         * @instance
         * @memberof Owt.Conference.SubscribeOptions
         */
        this.video = video;
    }
}

/**
 * @class VideoSubscriptionUpdateOptions
 * @memberOf Owt.Conference
 * @classDesc VideoSubscriptionUpdateOptions defines options for updating a subscription's video part.
 * @hideconstructor
 */
class VideoSubscriptionUpdateOptions {
    // eslint-disable-next-line require-jsdoc
    constructor() {
        /**
         * @member {?Owt.Base.Resolution} resolution
         * @instance
         * @memberof Owt.Conference.VideoSubscriptionUpdateOptions
         * @desc Only resolutions listed in VideoSubscriptionCapabilities are allowed.
         */
        this.resolution = undefined;
        /**
         * @member {?number} frameRates
         * @instance
         * @memberof Owt.Conference.VideoSubscriptionUpdateOptions
         * @desc Only frameRates listed in VideoSubscriptionCapabilities are allowed.
         */
        this.frameRate = undefined;
        /**
         * @member {?number} bitrateMultipliers
         * @instance
         * @memberof Owt.Conference.VideoSubscriptionUpdateOptions
         * @desc Only bitrateMultipliers listed in VideoSubscriptionCapabilities are allowed.
         */
        this.bitrateMultipliers = undefined;
        /**
         * @member {?number} keyFrameIntervals
         * @instance
         * @memberof Owt.Conference.VideoSubscriptionUpdateOptions
         * @desc Only keyFrameIntervals listed in VideoSubscriptionCapabilities are allowed.
         */
        this.keyFrameInterval = undefined;
    }
}

/**
 * @class SubscriptionUpdateOptions
 * @memberOf Owt.Conference
 * @classDesc SubscriptionUpdateOptions defines options for updating a subscription.
 * @hideconstructor
 */
class SubscriptionUpdateOptions {
    // eslint-disable-next-line require-jsdoc
    constructor() {
        /**
         * @member {?VideoSubscriptionUpdateOptions} video
         * @instance
         * @memberof Owt.Conference.SubscriptionUpdateOptions
         */
        this.video = undefined;
    }
}

/**
 * @class Subscription
 * @memberof Owt.Conference
 * @classDesc Subscription is a receiver for receiving a stream.
 * Events:
 *
 * | Event Name      | Argument Type    | Fired when       |
 * | ----------------| ---------------- | ---------------- |
 * | ended           | Event            | Subscription is ended. |
 * | error           | ErrorEvent       | An error occurred on the subscription. |
 * | mute            | MuteEvent        | Publication is muted. Remote side stopped sending audio and/or video data. |
 * | unmute          | MuteEvent        | Publication is unmuted. Remote side continued sending audio and/or video data. |
 *
 * @extends Owt.Base.EventDispatcher
 * @hideconstructor
 */
class Subscription extends EventDispatcher {
    // eslint-disable-next-line require-jsdoc
    constructor(id, stop, getStats, mute, unmute, applyOptions) {
        super();
        if (!id) {
            throw new TypeError('ID cannot be null or undefined.');
        }
        /**
         * @member {string} id
         * @instance
         * @memberof Owt.Conference.Subscription
         */
        Object.defineProperty(this, 'id', {
            configurable: false,
            writable: false,
            value: id,
        });
        /**
         * @function stop
         * @instance
         * @desc Stop certain subscription. Once a subscription is stopped, it cannot be recovered.
         * @memberof Owt.Conference.Subscription
         * @returns {undefined}
         */
        this.stop = stop;
        /**
         * @function getStats
         * @instance
         * @desc Get stats of underlying PeerConnection.
         * @memberof Owt.Conference.Subscription
         * @returns {Promise<RTCStatsReport, Error>}
         */
        this.getStats = getStats;
        /**
         * @function mute
         * @instance
         * @desc Stop reeving data from remote endpoint.
         * @memberof Owt.Conference.Subscription
         * @param {Owt.Base.TrackKind } kind Kind of tracks to be muted.
         * @returns {Promise<undefined, Error>}
         */
        this.mute = mute;
        /**
         * @function unmute
         * @instance
         * @desc Continue reeving data from remote endpoint.
         * @memberof Owt.Conference.Subscription
         * @param {Owt.Base.TrackKind } kind Kind of tracks to be unmuted.
         * @returns {Promise<undefined, Error>}
         */
        this.unmute = unmute;
        /**
         * @function applyOptions
         * @instance
         * @desc Update subscription with given options.
         * @memberof Owt.Conference.Subscription
         * @param {Owt.Conference.SubscriptionUpdateOptions } options Subscription update options.
         * @returns {Promise<undefined, Error>}
         */
        this.applyOptions = applyOptions;
    }
}

class ConferencePeerConnectionChannel extends EventDispatcher {
    // eslint-disable-next-line require-jsdoc
    constructor(config, signaling) {
        super();
        this._config = config;
        this._options = null;
        this._videoCodecs = undefined;
        this._signaling = signaling;
        this._pc = null;
        this._internalId = null; // It's publication ID or subscription ID.
        this._pendingCandidates = [];
        this._subscribePromise = null;
        this._publishPromise = null;
        this._subscribedStream = null;
        this._publishedStream = null;
        this._publication = null;
        this._subscription = null;
        // Timer for PeerConnection disconnected. Will stop connection after timer.
        this._disconnectTimer = null;
        this._ended = false;
        this._stopped = false;
    }

    /**
     * @function onMessage
     * @desc Received a message from conference portal. Defined in client-server protocol.
     * @param {string} notification Notification type.
     * @param {object} message Message received.
     * @private
     */
    onMessage(notification, message) {
        switch (notification) {
            case 'progress':
                if (message.status === 'soac') {
                    this._sdpHandler(message.data);
                } else if (message.status === 'ready') {
                    this._readyHandler();
                } else if (message.status === 'error') {
                    this._errorHandler(message.data);
                }
                break;
            case 'stream':
                this._onStreamEvent(message);
                break;
            default:
                Logger.warning('Unknown notification from MCU.');
        }
    }

    publish(stream, options, videoCodecs) {
        if (options === undefined) {
            options = {
                audio: !!stream.mediaStream.getAudioTracks().length, video: !!stream
                    .mediaStream.getVideoTracks().length
            };
        }
        if (typeof options !== 'object') {
            return Promise.reject(new TypeError('Options should be an object.'));
        }
        if ((this._isRtpEncodingParameters(options.audio) &&
            this._isOwtEncodingParameters(options.video)) ||
            (this._isOwtEncodingParameters(options.audio) &&
                this._isRtpEncodingParameters(options.video))) {
            return Promise.reject(new Error(
                'Mixing RTCRtpEncodingParameters and AudioEncodingParameters/VideoEncodingParameters is not allowed.'))
        }
        if (options.audio === undefined) {
            options.audio = !!stream.mediaStream.getAudioTracks().length;
        }
        if (options.video === undefined) {
            options.video = !!stream.mediaStream.getVideoTracks().length;
        }
        if ((!!options.audio && !stream.mediaStream.getAudioTracks().length) ||
            (!!options.video && !stream.mediaStream.getVideoTracks().length)) {
            return Promise.reject(new Error(
                'options.audio/video is inconsistent with tracks presented in the ' +
                'MediaStream.'
            ));
        }
        if ((options.audio === false || options.audio === null) &&
            (options.video === false || options.video === null)) {
            return Promise.reject(new Error(
                'Cannot publish a stream without audio and video.'));
        }
        if (typeof options.audio === 'object') {
            if (!Array.isArray(options.audio)) {
                return Promise.reject(new TypeError(
                    'options.audio should be a boolean or an array.'));
            }
            for (const parameters of options.audio) {
                if (!parameters.codec || typeof parameters.codec.name !== 'string' || (
                    parameters.maxBitrate !== undefined && typeof parameters.maxBitrate
                    !== 'number')) {
                    return Promise.reject(new TypeError(
                        'options.audio has incorrect parameters.'));
                }
            }
        }
        if (typeof options.video === 'object' && !Array.isArray(options.video)) {
            return Promise.reject(new TypeError(
                'options.video should be a boolean or an array.'));
        }
        if (this._isOwtEncodingParameters(options.video)) {
            for (const parameters of options.video) {
                if (!parameters.codec || typeof parameters.codec.name !== 'string' ||
                    (
                        parameters.maxBitrate !== undefined && typeof parameters
                            .maxBitrate !==
                        'number') || (parameters.codec.profile !== undefined &&
                            typeof parameters.codec.profile !== 'string')) {
                    return Promise.reject(new TypeError(
                        'options.video has incorrect parameters.'));
                }
            }
        }
        this._options = options;
        const mediaOptions = {};
        this._createPeerConnection();
        if (stream.mediaStream.getAudioTracks().length > 0 && options.audio !==
            false && options.audio !== null) {
            if (stream.mediaStream.getAudioTracks().length > 1) {
                Logger.warning(
                    'Publishing a stream with multiple audio tracks is not fully'
                    + ' supported.'
                );
            }
            if (typeof options.audio !== 'boolean' && typeof options.audio !==
                'object') {
                return Promise.reject(new Error(
                    'Type of audio options should be boolean or an object.'
                ));
            }
            mediaOptions.audio = {};
            mediaOptions.audio.source = stream.source.audio;
        } else {
            mediaOptions.audio = false;
        }
        if (stream.mediaStream.getVideoTracks().length > 0 && options.video !==
            false && options.video !== null) {
            if (stream.mediaStream.getVideoTracks().length > 1) {
                Logger.warning(
                    'Publishing a stream with multiple video tracks is not fully '
                    + 'supported.'
                );
            }
            mediaOptions.video = {};
            mediaOptions.video.source = stream.source.video;
            const trackSettings = stream.mediaStream.getVideoTracks()[0]
                .getSettings();
            mediaOptions.video.parameters = {
                resolution: {
                    width: trackSettings.width,
                    height: trackSettings.height,
                },
                framerate: trackSettings.frameRate,
            };
        } else {
            mediaOptions.video = false;
        }
        this._publishedStream = stream;
        this._signaling.sendSignalingMessage('publish', {
            media: mediaOptions,
            attributes: stream.attributes,
        }).then((data) => {
            const messageEvent = new MessageEvent('id', {
                message: data.id,
                origin: this._remoteId,
            });
            this.dispatchEvent(messageEvent);
            this._internalId = data.id;
            const offerOptions = {};
            if (typeof this._pc.addTransceiver === 'function') {
                let setPromise = Promise.resolve();
                // |direction| seems not working on Safari.
                if (mediaOptions.audio && stream.mediaStream.getAudioTracks().length >
                    0) {
                    const transceiverInit = {
                        direction: 'sendonly'
                    };
                    if (this._isRtpEncodingParameters(options.audio)) {
                        transceiverInit.sendEncodings = options.audio;
                    }
                    const transceiver = this._pc.addTransceiver(stream.mediaStream.getAudioTracks()[0],
                        transceiverInit);

                    if (Top.Util.Browser.isFirefox()) {
                        // Firefox does not support encodings setting in addTransceiver.
                        const parameters = transceiver.sender.getParameters();
                        parameters.encodings = transceiverInit.sendEncodings;
                        setPromise = transceiver.sender.setParameters(parameters);
                    }
                }
                if (mediaOptions.video && stream.mediaStream.getVideoTracks().length >
                    0) {
                    const transceiverInit = {
                        direction: 'sendonly'
                    };
                    if (this._isRtpEncodingParameters(options.video)) {
                        transceiverInit.sendEncodings = options.video;
                        this._videoCodecs = videoCodecs;
                    }
                    const transceiver = this._pc.addTransceiver(stream.mediaStream.getVideoTracks()[0],
                        transceiverInit);

                    if (Top.Util.Browser.isFirefox()) {
                        // Firefox does not support encodings setting in addTransceiver.
                        const parameters = transceiver.sender.getParameters();
                        parameters.encodings = transceiverInit.sendEncodings;
                        setPromise = setPromise.then(
                            () => transceiver.sender.setParameters(parameters));
                    }
                }
                return setPromise.then(() => offerOptions);
            } else {
                if (mediaOptions.audio && stream.mediaStream.getAudioTracks().length > 0) {
                    for (const track of stream.mediaStream.getAudioTracks())
                        this._pc.addTrack(track, stream.mediaStream);
                }
                if (mediaOptions.video && stream.mediaStream.getVideoTracks().length > 0) {
                    for (const track of stream.mediaStream.getVideoTracks())
                        this._pc.addTrack(track, stream.mediaStream);
                }
                offerOptions.offerToReceiveAudio = false;
                offerOptions.offerToReceiveVideo = false;
            }
            return offerOptions;
        }).then((offerOptions) => {
            let localDesc;
            this._pc.createOffer(offerOptions).then((desc) => {
                if (options) {
                    desc.sdp = this._setRtpReceiverOptions(desc.sdp, options);
                }
                return desc;
            }).then((desc) => {
                localDesc = desc;
                return this._pc.setLocalDescription(desc);
            }).then(() => {
                this._signaling.sendSignalingMessage('soac', {
                    id: this
                        ._internalId,
                    signaling: localDesc,
                });
            }).catch((e) => {
                Logger.error('Failed to create offer or set SDP. Message: '
                    + e.message);
                this._unpublish();
                this._rejectPromise(e);
                this._fireEndedEventOnPublicationOrSubscription();
            });
        }).catch((e) => {
            this._unpublish();
            this._rejectPromise(e);
            this._fireEndedEventOnPublicationOrSubscription();
        });
        return new Promise((resolve, reject) => {
            this._publishPromise = { resolve: resolve, reject: reject };
        });
    }

    subscribe(stream, options) {
        if (options === undefined) {
            options = {
                audio: !!stream.settings.audio,
                video: !!stream.settings.video,
            };
        }
        if (typeof options !== 'object') {
            return Promise.reject(new TypeError('Options should be an object.'));
        }
        if (options.audio === undefined) {
            options.audio = !!stream.settings.audio;
        }
        if (options.video === undefined) {
            options.video = !!stream.settings.video;
        }
        if ((options.audio !== undefined && typeof options.audio !== 'object' &&
            typeof options.audio !== 'boolean' && options.audio !== null) || (
                options.video !== undefined && typeof options.video !== 'object' &&
                typeof options.video !== 'boolean' && options.video !== null)) {
            return Promise.reject(new TypeError('Invalid options type.'));
        }
        if (options.audio && !stream.settings.audio || (options.video &&
            !stream.settings.video)) {
            return Promise.reject(new Error(
                'options.audio/video cannot be true or an object if there is no '
                + 'audio/video track in remote stream.'
            ));
        }
        if (options.audio === false && options.video === false) {
            return Promise.reject(new Error(
                'Cannot subscribe a stream without audio and video.'));
        }
        this._options = options;
        const mediaOptions = {};
        if (options.audio) {
            if (typeof options.audio === 'object' &&
                Array.isArray(options.audio.codecs)) {
                if (options.audio.codecs.length === 0) {
                    return Promise.reject(new TypeError(
                        'Audio codec cannot be an empty array.'));
                }
            }
            mediaOptions.audio = {};
            mediaOptions.audio.from = stream.id;
        } else {
            mediaOptions.audio = false;
        }
        if (options.video) {
            if (typeof options.video === 'object' &&
                Array.isArray(options.video.codecs)) {
                if (options.video.codecs.length === 0) {
                    return Promise.reject(new TypeError(
                        'Video codec cannot be an empty array.'));
                }
            }
            mediaOptions.video = {};
            mediaOptions.video.from = stream.id;
            if (options.video.resolution || options.video.frameRate || (options.video
                .bitrateMultiplier && options.video.bitrateMultiplier !== 1) ||
                options.video.keyFrameInterval) {
                mediaOptions.video.parameters = {
                    resolution: options.video.resolution,
                    framerate: options.video.frameRate,
                    bitrate: options.video.bitrateMultiplier ? 'x'
                        + options.video.bitrateMultiplier.toString() : undefined,
                    keyFrameInterval: options.video.keyFrameInterval
                };
            }
            if (options.video.rid) {
                mediaOptions.video.simulcastRid = options.video.rid;
                // Ignore other settings when RID set.
                delete mediaOptions.video.parameters;
                options.video = true;
            }
        } else {
            mediaOptions.video = false;
        }

        this._subscribedStream = stream;
        this._signaling.sendSignalingMessage('subscribe', {
            media: mediaOptions,
        }).then((data) => {
            const messageEvent = new MessageEvent('id', {
                message: data.id,
                origin: this._remoteId,
            });
            this.dispatchEvent(messageEvent);
            this._internalId = data.id;
            this._createPeerConnection();
            const offerOptions = {};
            if (typeof this._pc.addTransceiver === 'function') {
                // |direction| seems not working on Safari.
                if (mediaOptions.audio) {
                    this._pc.addTransceiver('audio', { direction: 'recvonly' });
                }
                if (mediaOptions.video) {
                    this._pc.addTransceiver('video', { direction: 'recvonly' });
                }
            } else {
                offerOptions.offerToReceiveAudio = !!options.audio;
                offerOptions.offerToReceiveVideo = !!options.video;
            }

            this._pc.createOffer(offerOptions).then((desc) => {
                if (options) {
                    desc.sdp = this._setRtpReceiverOptions(desc.sdp, options);
                }
                this._pc.setLocalDescription(desc).then(() => {
                    this._signaling.sendSignalingMessage('soac', {
                        id: this
                            ._internalId,
                        signaling: desc,
                    });
                }, function (errorMessage) {
                    Logger.error('Set local description failed. Message: ' +
                        JSON.stringify(errorMessage));
                });
            }, function (error) {
                Logger.error('Create offer failed. Error info: ' + JSON.stringify(
                    error));
            }).catch((e) => {
                Logger.error('Failed to create offer or set SDP. Message: '
                    + e.message);
                this._unsubscribe();
                this._rejectPromise(e);
                this._fireEndedEventOnPublicationOrSubscription();
            });
        }).catch((e) => {
            this._unsubscribe();
            this._rejectPromise(e);
            this._fireEndedEventOnPublicationOrSubscription();
        });
        return new Promise((resolve, reject) => {
            this._subscribePromise = { resolve: resolve, reject: reject };
        });
    }

    _unpublish() {
        if (!this._stopped) {
            this._stopped = true;
            this._signaling.sendSignalingMessage('unpublish', { id: this._internalId })
                .catch((e) => {
                    Logger.warning('MCU returns negative ack for unpublishing, ' + e);
                });
            if (this._pc && this._pc.signalingState !== 'closed') {
                this._pc.close();
            }
        }
    }

    _unsubscribe() {
        if (!this._stopped) {
            this._stopped = true;
            this._signaling.sendSignalingMessage('unsubscribe', {
                id: this._internalId,
            })
                .catch((e) => {
                    Logger.warning('MCU returns negative ack for unsubscribing, ' + e);
                });
            if (this._pc && this._pc.signalingState !== 'closed') {
                this._pc.close();
            }
        }
    }

    _muteOrUnmute(isMute, isPub, trackKind) {
        const eventName = isPub ? 'stream-control' :
            'subscription-control';
        const operation = isMute ? 'pause' : 'play';
        return this._signaling.sendSignalingMessage(eventName, {
            id: this._internalId,
            operation: operation,
            data: trackKind,
        }).then(() => {
            if (!isPub) {
                const muteEventName = isMute ? 'mute' : 'unmute';
                this._subscription.dispatchEvent(
                    new MuteEvent(muteEventName, { kind: trackKind }));
            }
        });
    }

    _applyOptions(options) {
        if (typeof options !== 'object' || typeof options.video !== 'object') {
            return Promise.reject(new Error(
                'Options should be an object.'));
        }
        const videoOptions = {};
        videoOptions.resolution = options.video.resolution;
        videoOptions.framerate = options.video.frameRate;
        videoOptions.bitrate = options.video.bitrateMultiplier ? 'x' + options.video
            .bitrateMultiplier
            .toString() : undefined;
        videoOptions.keyFrameInterval = options.video.keyFrameInterval;
        return this._signaling.sendSignalingMessage('subscription-control', {
            id: this._internalId,
            operation: 'update',
            data: {
                video: { parameters: videoOptions },
            },
        }).then();
    }

    _onRemoteStreamAdded(event) {
        Logger.debug('Remote stream added.');
        if (this._subscribedStream) {
            this._subscribedStream.mediaStream = event.streams[0];
        } else {
            // This is not expected path. However, this is going to happen on Safari
            // because it does not support setting direction of transceiver.
            Logger.warning('Received remote stream without subscription.');
        }
    }

    _onLocalIceCandidate(event) {
        if (event.candidate) {
            if (this._pc.signalingState !== 'stable') {
                this._pendingCandidates.push(event.candidate);
            } else {
                this._sendCandidate(event.candidate);
            }
        } else {
            Logger.debug('Empty candidate.');
        }
    }

    _fireEndedEventOnPublicationOrSubscription() {
        if (this._ended) {
            return;
        }
        this._ended = true;
        const event = new OwtEvent('ended');
        if (this._publication) {
            this._publication.dispatchEvent(event);
            this._publication.stop();
        } else if (this._subscription) {
            this._subscription.dispatchEvent(event);
            this._subscription.stop();
        }
    }

    _rejectPromise(error) {
        if (!error) {
            error = new Error('Connection failed or closed.');
        }
        // Rejecting corresponding promise if publishing and subscribing is ongoing.
        if (this._publishPromise) {
            this._publishPromise.reject(error);
            this._publishPromise = undefined;
        } else if (this._subscribePromise) {
            this._subscribePromise.reject(error);
            this._subscribePromise = undefined;
        }
    }

    _onIceConnectionStateChange(event) {
        if (!event || !event.currentTarget) {
            return;
        }

        Logger.debug('ICE connection state changed to ' +
            event.currentTarget.iceConnectionState);
        if (event.currentTarget.iceConnectionState === 'closed' ||
            event.currentTarget.iceConnectionState === 'failed') {
            if (event.currentTarget.iceConnectionState === 'failed') {
                this._handleError('connection failed.');
            } else {
                // Fire ended event if publication or subscription exists.
                this._fireEndedEventOnPublicationOrSubscription();
            }
        }
    }

    _onConnectionStateChange(event) {
        if (this._pc.connectionState === 'closed' ||
            this._pc.connectionState === 'failed') {
            if (this._pc.connectionState === 'failed') {
                this._handleError('connection failed.');
            } else {
                // Fire ended event if publication or subscription exists.
                this._fireEndedEventOnPublicationOrSubscription();
            }
        }
    }

    _sendCandidate(candidate) {
        this._signaling.sendSignalingMessage('soac', {
            id: this._internalId,
            signaling: {
                type: 'candidate',
                candidate: {
                    candidate: 'a=' + candidate.candidate,
                    sdpMid: candidate.sdpMid,
                    sdpMLineIndex: candidate.sdpMLineIndex,
                },
            },
        });
    }

    _createPeerConnection() {
        const pcConfiguration = this._config.rtcConfiguration || { sdpSemantics : "unified-plan" };
        if (Top.Util.Browser.isChrome()) {
            pcConfiguration.sdpSemantics = 'unified-plan';
        }
        this._pc = new RTCPeerConnection(pcConfiguration);
        this._pc.onicecandidate = (event) => {
            this._onLocalIceCandidate.apply(this, [event]);
        };
        this._pc.ontrack = (event) => {
            this._onRemoteStreamAdded.apply(this, [event]);
        };
        this._pc.oniceconnectionstatechange = (event) => {
            this._onIceConnectionStateChange.apply(this, [event]);
        };
        this._pc.onconnectionstatechange = (event) => {
            this._onConnectionStateChange.apply(this, [event]);
        };
    }

    _getStats() {
        if (this._pc) {
            return this._pc.getStats();
        } else {
            return Promise.reject(new Error(
                'PeerConnection is not available.'));
        }
    }

    _readyHandler() {
        if (this._subscribePromise) {
            this._subscription = new Subscription(this._internalId, () => {
                this._unsubscribe();
            }, () => this._getStats(),
                (trackKind) => this._muteOrUnmute(true, false, trackKind),
                (trackKind) => this._muteOrUnmute(false, false, trackKind),
                (options) => this._applyOptions(options));
            // Fire subscription's ended event when associated stream is ended.
            this._subscribedStream.addEventListener('ended', () => {
                this._subscription.dispatchEvent('ended', new OwtEvent('ended'));
            });
            this._subscribePromise.resolve(this._subscription);
        } else if (this._publishPromise) {
            this._publication = new Publication(this._internalId, () => {
                this._unpublish();
                return Promise.resolve();
            }, () => this._getStats(),
                (trackKind) => this._muteOrUnmute(true, true, trackKind),
                (trackKind) => this._muteOrUnmute(false, true, trackKind));
            this._publishPromise.resolve(this._publication);
            // Do not fire publication's ended event when associated stream is ended.
            // It may still sending silence or black frames.
            // Refer to https://w3c.github.io/webrtc-pc/#rtcrtpsender-interface.
        }
        this._publishPromise = null;
        this._subscribePromise = null;
    }

    _sdpHandler(sdp) {
        if (sdp.type === 'answer') {
            if ((this._publication || this._publishPromise) && this._options) {
                sdp.sdp = this._setRtpSenderOptions(sdp.sdp, this._options);
            }
            this._pc.setRemoteDescription(sdp).then(() => {
                if (this._pendingCandidates.length > 0) {
                    for (const candidate of this._pendingCandidates) {
                        this._sendCandidate(candidate);
                    }
                }
            }, (error) => {
                Logger.error('Set remote description failed: ' + error);
                this._rejectPromise(error);
                this._fireEndedEventOnPublicationOrSubscription();
            });
        }
    }

    _errorHandler(errorMessage) {
        return this._handleError(errorMessage);
    }

    _handleError(errorMessage) {
        const error = new Error(errorMessage);
        const p = this._publishPromise || this._subscribePromise;
        if (p) {
            return this._rejectPromise(error);
        }
        if (this._ended) {
            return;
        }
        const dispatcher = this._publication || this._subscription;
        if (!dispatcher) {
            Logger.warning('Neither publication nor subscription is available.');
            return;
        }
        const errorEvent = new ErrorEvent('error', {
            error: error,
        });
        dispatcher.dispatchEvent(errorEvent);
        // Fire ended event when error occured
        this._fireEndedEventOnPublicationOrSubscription();
    }

    _setCodecOrder(sdp, options) {
        if (this._publication || this._publishPromise) {
            if (options.audio) {
                const audioCodecNames = Array.from(options.audio,
                    (encodingParameters) => encodingParameters.codec.name);
                sdp = reorderCodecs(sdp, 'audio', audioCodecNames);
            }
            if (options.video) {
                const videoCodecNames = Array.from(options.video,
                    (encodingParameters) => encodingParameters.codec.name);
                sdp = reorderCodecs(sdp, 'video', videoCodecNames);
            }
        } else {
            if (options.audio && options.audio.codecs) {
                const audioCodecNames = Array.from(options.audio.codecs, (codec) =>
                    codec.name);
                sdp = reorderCodecs(sdp, 'audio', audioCodecNames);
            }
            if (options.video && options.video.codecs) {
                const videoCodecNames = Array.from(options.video.codecs, (codec) =>
                    codec.name);
                sdp = reorderCodecs(sdp, 'video', videoCodecNames);
            }
        }
        return sdp;
    }

    _setMaxBitrate(sdp, options) {
        if (typeof options.audio === 'object') {
            sdp = setMaxBitrate(sdp, options.audio);
        }
        if (typeof options.video === 'object') {
            sdp = setMaxBitrate(sdp, options.video);
        }
        return sdp;
    }

    _setRtpSenderOptions(sdp, options) {
        // SDP mugling is deprecated, moving to `setParameters`.
        if (this._isRtpEncodingParameters(options.audio) ||
            this._isRtpEncodingParameters(options.video)) {
            return sdp;
        }
        sdp = this._setMaxBitrate(sdp, options);
        return sdp;
    }

    _setRtpReceiverOptions(sdp, options) {
        // Add legacy simulcast in SDP for safari.
        if (this._isRtpEncodingParameters(options.video) && Top.Util.Browser.isSafari()) {
            if (options.video.length > 1) {
                sdp = addLegacySimulcast(sdp, 'video', options.video.length);
            }
        }

        // _videoCodecs is a workaround for setting video codecs. It will be moved to RTCRtpSendParameters.
        if (this._isRtpEncodingParameters(options.video) && this._videoCodecs) {
            sdp = reorderCodecs(sdp, 'video', this._videoCodecs);
            return sdp;
        }
        if (this._isRtpEncodingParameters(options.audio) ||
            this._isRtpEncodingParameters(options.video)) {
            return sdp;
        }
        sdp = this._setCodecOrder(sdp, options);
        return sdp;
    }

    // Handle stream event sent from MCU. Some stream events should be publication
    // event or subscription event. It will be handled here.
    _onStreamEvent(message) {
        let eventTarget;
        if (this._publication && message.id === this._publication.id) {
            eventTarget = this._publication;
        } else if (
            this._subscribedStream && message.id === this._subscribedStream.id) {
            eventTarget = this._subscription;
        }
        if (!eventTarget) {
            return;
        }
        let trackKind;
        if (message.data.field === 'audio.status') {
            trackKind = TrackKind.AUDIO;
        } else if (message.data.field === 'video.status') {
            trackKind = TrackKind.VIDEO;
        } else {
            Logger.warning('Invalid data field for stream update info.');
        }
        if (message.data.value === 'active') {
            eventTarget.dispatchEvent(new MuteEvent('unmute', { kind: trackKind }));
        } else if (message.data.value === 'inactive') {
            eventTarget.dispatchEvent(new MuteEvent('mute', { kind: trackKind }));
        } else {
            Logger.warning('Invalid data value for stream update info.');
        }
    }

    _isRtpEncodingParameters(obj) {
        if (!Array.isArray(obj)) {
            return false;
        }
        // Only check the first one.
        const param = obj[0];
        return param.codecPayloadType || param.dtx || param.active || param
            .ptime || param.maxFramerate || param.scaleResolutionDownBy || param.rid;
    }

    _isOwtEncodingParameters(obj) {
        if (!Array.isArray(obj)) {
            return false;
        }
        // Only check the first one.
        const param = obj[0];
        return !!param.codec;
    }
}


function mergeConstraints(cons1, cons2) {
    if (!cons1 || !cons2) {
        return cons1 || cons2;
    }
    const merged = cons1;
    for (const key in cons2) {
        merged[key] = cons2[key];
    }
    return merged;
}

function iceCandidateType(candidateStr) {
    return candidateStr.split(' ')[7];
}

// Turns the local type preference into a human-readable string.
// Note that this mapping is browser-specific.
function formatTypePreference(pref) {
    if (adapter.browserDetails.browser === 'chrome') {
        switch (pref) {
            case 0:
                return 'TURN/TLS';
            case 1:
                return 'TURN/TCP';
            case 2:
                return 'TURN/UDP';
            default:
                break;
        }
    } else if (adapter.browserDetails.browser === 'firefox') {
        switch (pref) {
            case 0:
                return 'TURN/TCP';
            case 5:
                return 'TURN/UDP';
            default:
                break;
        }
    }
    return '';
}

function maybeSetOpusOptions(sdp, params) {
    // Set Opus in Stereo, if stereo is true, unset it, if stereo is false, and
    // do nothing if otherwise.
    if (params.opusStereo === 'true') {
        sdp = setCodecParam(sdp, 'opus/48000', 'stereo', '1');
    } else if (params.opusStereo === 'false') {
        sdp = removeCodecParam(sdp, 'opus/48000', 'stereo');
    }

    // Set Opus FEC, if opusfec is true, unset it, if opusfec is false, and
    // do nothing if otherwise.
    if (params.opusFec === 'true') {
        sdp = setCodecParam(sdp, 'opus/48000', 'useinbandfec', '1');
    } else if (params.opusFec === 'false') {
        sdp = removeCodecParam(sdp, 'opus/48000', 'useinbandfec');
    }

    // Set Opus DTX, if opusdtx is true, unset it, if opusdtx is false, and
    // do nothing if otherwise.
    if (params.opusDtx === 'true') {
        sdp = setCodecParam(sdp, 'opus/48000', 'usedtx', '1');
    } else if (params.opusDtx === 'false') {
        sdp = removeCodecParam(sdp, 'opus/48000', 'usedtx');
    }

    // Set Opus maxplaybackrate, if requested.
    if (params.opusMaxPbr) {
        sdp = setCodecParam(
            sdp, 'opus/48000', 'maxplaybackrate', params.opusMaxPbr);
    }
    return sdp;
}

function maybeSetAudioSendBitRate(sdp, params) {
    if (!params.audioSendBitrate) {
        return sdp;
    }
    Logger.debug('Prefer audio send bitrate: ' + params.audioSendBitrate);
    return preferBitRate(sdp, params.audioSendBitrate, 'audio');
}

function maybeSetAudioReceiveBitRate(sdp, params) {
    if (!params.audioRecvBitrate) {
        return sdp;
    }
    Logger.debug('Prefer audio receive bitrate: ' + params.audioRecvBitrate);
    return preferBitRate(sdp, params.audioRecvBitrate, 'audio');
}

function maybeSetVideoSendBitRate(sdp, params) {
    if (!params.videoSendBitrate) {
        return sdp;
    }
    Logger.debug('Prefer video send bitrate: ' + params.videoSendBitrate);
    return preferBitRate(sdp, params.videoSendBitrate, 'video');
}

function maybeSetVideoReceiveBitRate(sdp, params) {
    if (!params.videoRecvBitrate) {
        return sdp;
    }
    Logger.debug('Prefer video receive bitrate: ' + params.videoRecvBitrate);
    return preferBitRate(sdp, params.videoRecvBitrate, 'video');
}

// Add a b=AS:bitrate line to the m=mediaType section.
function preferBitRate(sdp, bitrate, mediaType) {
    const sdpLines = sdp.split('\r\n');

    // Find m line for the given mediaType.
    const mLineIndex = findLine(sdpLines, 'm=', mediaType);
    if (mLineIndex === null) {
        Logger.debug('Failed to add bandwidth line to sdp, as no m-line found');
        return sdp;
    }

    // Find next m-line if any.
    let nextMLineIndex = findLineInRange(sdpLines, mLineIndex + 1, -1, 'm=');
    if (nextMLineIndex === null) {
        nextMLineIndex = sdpLines.length;
    }

    // Find c-line corresponding to the m-line.
    const cLineIndex = findLineInRange(sdpLines, mLineIndex + 1,
        nextMLineIndex, 'c=');
    if (cLineIndex === null) {
        Logger.debug('Failed to add bandwidth line to sdp, as no c-line found');
        return sdp;
    }

    // Check if bandwidth line already exists between c-line and next m-line.
    const bLineIndex = findLineInRange(sdpLines, cLineIndex + 1,
        nextMLineIndex, 'b=AS');
    if (bLineIndex) {
        sdpLines.splice(bLineIndex, 1);
    }

    // Create the b (bandwidth) sdp line.
    const bwLine = 'b=AS:' + bitrate;
    // As per RFC 4566, the b line should follow after c-line.
    sdpLines.splice(cLineIndex + 1, 0, bwLine);
    sdp = sdpLines.join('\r\n');
    return sdp;
}

// Add an a=fmtp: x-google-min-bitrate=kbps line, if videoSendInitialBitrate
// is specified. We'll also add a x-google-min-bitrate value, since the max
// must be >= the min.
function maybeSetVideoSendInitialBitRate(sdp, params) {
    let initialBitrate = parseInt(params.videoSendInitialBitrate);
    if (!initialBitrate) {
        return sdp;
    }

    // Validate the initial bitrate value.
    let maxBitrate = parseInt(initialBitrate);
    const bitrate = parseInt(params.videoSendBitrate);
    if (bitrate) {
        if (initialBitrate > bitrate) {
            Logger.debug('Clamping initial bitrate to max bitrate of ' + bitrate + ' kbps.');
            initialBitrate = bitrate;
            params.videoSendInitialBitrate = initialBitrate;
        }
        maxBitrate = bitrate;
    }

    const sdpLines = sdp.split('\r\n');

    // Search for m line.
    const mLineIndex = findLine(sdpLines, 'm=', 'video');
    if (mLineIndex === null) {
        Logger.debug('Failed to find video m-line');
        return sdp;
    }
    // Figure out the first codec payload type on the m=video SDP line.
    const videoMLine = sdpLines[mLineIndex];
    const pattern = new RegExp('m=video\\s\\d+\\s[A-Z/]+\\s');
    const sendPayloadType = videoMLine.split(pattern)[1].split(' ')[0];
    const fmtpLine = sdpLines[findLine(sdpLines, 'a=rtpmap', sendPayloadType)];
    const codecName = fmtpLine.split('a=rtpmap:' +
        sendPayloadType)[1].split('/')[0];

    // Use codec from params if specified via URL param, otherwise use from SDP.
    const codec = params.videoSendCodec || codecName;
    sdp = setCodecParam(sdp, codec, 'x-google-min-bitrate',
        params.videoSendInitialBitrate.toString());
    sdp = setCodecParam(sdp, codec, 'x-google-max-bitrate',
        maxBitrate.toString());

    return sdp;
}

function removePayloadTypeFromMline(mLine, payloadType) {
    mLine = mLine.split(' ');
    for (let i = 0; i < mLine.length; ++i) {
        if (mLine[i] === payloadType.toString()) {
            mLine.splice(i, 1);
        }
    }
    return mLine.join(' ');
}

function removeCodecByName(sdpLines, codec) {
    const index = findLine(sdpLines, 'a=rtpmap', codec);
    if (index === null) {
        return sdpLines;
    }
    const payloadType = getCodecPayloadTypeFromLine(sdpLines[index]);
    sdpLines.splice(index, 1);

    // Search for the video m= line and remove the codec.
    const mLineIndex = findLine(sdpLines, 'm=', 'video');
    if (mLineIndex === null) {
        return sdpLines;
    }
    sdpLines[mLineIndex] = removePayloadTypeFromMline(sdpLines[mLineIndex],
        payloadType);
    return sdpLines;
}

function removeCodecByPayloadType(sdpLines, payloadType) {
    const index = findLine(sdpLines, 'a=rtpmap', payloadType.toString());
    if (index === null) {
        return sdpLines;
    }
    sdpLines.splice(index, 1);

    // Search for the video m= line and remove the codec.
    const mLineIndex = findLine(sdpLines, 'm=', 'video');
    if (mLineIndex === null) {
        return sdpLines;
    }
    sdpLines[mLineIndex] = removePayloadTypeFromMline(sdpLines[mLineIndex],
        payloadType);
    return sdpLines;
}

function maybeRemoveVideoFec(sdp, params) {
    if (params.videoFec !== 'false') {
        return sdp;
    }

    let sdpLines = sdp.split('\r\n');

    let index = findLine(sdpLines, 'a=rtpmap', 'red');
    if (index === null) {
        return sdp;
    }
    const redPayloadType = getCodecPayloadTypeFromLine(sdpLines[index]);
    sdpLines = removeCodecByPayloadType(sdpLines, redPayloadType);

    sdpLines = removeCodecByName(sdpLines, 'ulpfec');

    // Remove fmtp lines associated with red codec.
    index = findLine(sdpLines, 'a=fmtp', redPayloadType.toString());
    if (index === null) {
        return sdp;
    }
    const fmtpLine = parseFmtpLine(sdpLines[index]);
    const rtxPayloadType = fmtpLine.pt;
    if (rtxPayloadType === null) {
        return sdp;
    }
    sdpLines.splice(index, 1);

    sdpLines = removeCodecByPayloadType(sdpLines, rtxPayloadType);
    return sdpLines.join('\r\n');
}

// Promotes |audioSendCodec| to be the first in the m=audio line, if set.
function maybePreferAudioSendCodec(sdp, params) {
    return maybePreferCodec(sdp, 'audio', 'send', params.audioSendCodec);
}

// Promotes |audioRecvCodec| to be the first in the m=audio line, if set.
function maybePreferAudioReceiveCodec(sdp, params) {
    return maybePreferCodec(sdp, 'audio', 'receive', params.audioRecvCodec);
}

// Promotes |videoSendCodec| to be the first in the m=audio line, if set.
function maybePreferVideoSendCodec(sdp, params) {
    return maybePreferCodec(sdp, 'video', 'send', params.videoSendCodec);
}

// Promotes |videoRecvCodec| to be the first in the m=audio line, if set.
function maybePreferVideoReceiveCodec(sdp, params) {
    return maybePreferCodec(sdp, 'video', 'receive', params.videoRecvCodec);
}

// Sets |codec| as the default |type| codec if it's present.
// The format of |codec| is 'NAME/RATE', e.g. 'opus/48000'.
function maybePreferCodec(sdp, type, dir, codec) {
    const str = type + ' ' + dir + ' codec';
    if (!codec) {
        Logger.debug('No preference on ' + str + '.');
        return sdp;
    }

    Logger.debug('Prefer ' + str + ': ' + codec);

    const sdpLines = sdp.split('\r\n');

    // Search for m line.
    const mLineIndex = findLine(sdpLines, 'm=', type);
    if (mLineIndex === null) {
        return sdp;
    }

    // If the codec is available, set it as the default in m line.
    let payload = null;
    for (let i = 0; i < sdpLines.length; i++) {
        const index = findLineInRange(sdpLines, i, -1, 'a=rtpmap', codec);
        if (index !== null) {
            payload = getCodecPayloadTypeFromLine(sdpLines[index]);
            if (payload) {
                sdpLines[mLineIndex] = setDefaultCodec(sdpLines[mLineIndex], payload);
            }
        }
    }

    sdp = sdpLines.join('\r\n');
    return sdp;
}

// Set fmtp param to specific codec in SDP. If param does not exists, add it.
function setCodecParam(sdp, codec, param, value) {
    let sdpLines = sdp.split('\r\n');
    // SDPs sent from MCU use \n as line break.
    if (sdpLines.length <= 1) {
        sdpLines = sdp.split('\n');
    }

    const fmtpLineIndex = findFmtpLine(sdpLines, codec);

    let fmtpObj = {};
    if (fmtpLineIndex === null) {
        const index = findLine(sdpLines, 'a=rtpmap', codec);
        if (index === null) {
            return sdp;
        }
        const payload = getCodecPayloadTypeFromLine(sdpLines[index]);
        fmtpObj.pt = payload.toString();
        fmtpObj.params = {};
        fmtpObj.params[param] = value;
        sdpLines.splice(index + 1, 0, writeFmtpLine(fmtpObj));
    } else {
        fmtpObj = parseFmtpLine(sdpLines[fmtpLineIndex]);
        fmtpObj.params[param] = value;
        sdpLines[fmtpLineIndex] = writeFmtpLine(fmtpObj);
    }

    sdp = sdpLines.join('\r\n');
    return sdp;
}

// Remove fmtp param if it exists.
function removeCodecParam(sdp, codec, param) {
    const sdpLines = sdp.split('\r\n');

    const fmtpLineIndex = findFmtpLine(sdpLines, codec);
    if (fmtpLineIndex === null) {
        return sdp;
    }

    const map = parseFmtpLine(sdpLines[fmtpLineIndex]);
    delete map.params[param];

    const newLine = writeFmtpLine(map);
    if (newLine === null) {
        sdpLines.splice(fmtpLineIndex, 1);
    } else {
        sdpLines[fmtpLineIndex] = newLine;
    }

    sdp = sdpLines.join('\r\n');
    return sdp;
}

// Split an fmtp line into an object including 'pt' and 'params'.
function parseFmtpLine(fmtpLine) {
    const fmtpObj = {};
    const spacePos = fmtpLine.indexOf(' ');
    const keyValues = fmtpLine.substring(spacePos + 1).split(';');

    const pattern = new RegExp('a=fmtp:(\\d+)');
    const result = fmtpLine.match(pattern);
    if (result && result.length === 2) {
        fmtpObj.pt = result[1];
    } else {
        return null;
    }

    const params = {};
    for (let i = 0; i < keyValues.length; ++i) {
        const pair = keyValues[i].split('=');
        if (pair.length === 2) {
            params[pair[0]] = pair[1];
        }
    }
    fmtpObj.params = params;

    return fmtpObj;
}

// Generate an fmtp line from an object including 'pt' and 'params'.
function writeFmtpLine(fmtpObj) {
    if (!fmtpObj.hasOwnProperty('pt') || !fmtpObj.hasOwnProperty('params')) {
        return null;
    }
    const pt = fmtpObj.pt;
    const params = fmtpObj.params;
    const keyValues = [];
    let i = 0;
    for (const key in params) {
        keyValues[i] = key + '=' + params[key];
        ++i;
    }
    if (i === 0) {
        return null;
    }
    return 'a=fmtp:' + pt.toString() + ' ' + keyValues.join(';');
}

// Find fmtp attribute for |codec| in |sdpLines|.
function findFmtpLine(sdpLines, codec) {
    // Find payload of codec.
    const payload = getCodecPayloadType(sdpLines, codec);
    // Find the payload in fmtp line.
    return payload ? findLine(sdpLines, 'a=fmtp:' + payload.toString()) : null;
}

// Find the line in sdpLines that starts with |prefix|, and, if specified,
// contains |substr| (case-insensitive search).
function findLine(sdpLines, prefix, substr) {
    return findLineInRange(sdpLines, 0, -1, prefix, substr);
}

// Find the line in sdpLines[startLine...endLine - 1] that starts with |prefix|
// and, if specified, contains |substr| (case-insensitive search).
function findLineInRange(sdpLines, startLine, endLine, prefix, substr) {
    const realEndLine = endLine !== -1 ? endLine : sdpLines.length;
    for (let i = startLine; i < realEndLine; ++i) {
        if (sdpLines[i].indexOf(prefix) === 0) {
            if (!substr ||
                sdpLines[i].toLowerCase().indexOf(substr.toLowerCase()) !== -1) {
                return i;
            }
        }
    }
    return null;
}

// Gets the codec payload type from sdp lines.
function getCodecPayloadType(sdpLines, codec) {
    const index = findLine(sdpLines, 'a=rtpmap', codec);
    return index ? getCodecPayloadTypeFromLine(sdpLines[index]) : null;
}

// Gets the codec payload type from an a=rtpmap:X line.
function getCodecPayloadTypeFromLine(sdpLine) {
    const pattern = new RegExp('a=rtpmap:(\\d+) [a-zA-Z0-9-]+\\/\\d+');
    const result = sdpLine.match(pattern);
    return (result && result.length === 2) ? result[1] : null;
}

// Returns a new m= line with the specified codec as the first one.
function setDefaultCodec(mLine, payload) {
    const elements = mLine.split(' ');

    // Just copy the first three parameters; codec order starts on fourth.
    const newLine = elements.slice(0, 3);

    // Put target payload first and copy in the rest.
    newLine.push(payload);
    for (let i = 3; i < elements.length; i++) {
        if (elements[i] !== payload) {
            newLine.push(elements[i]);
        }
    }
    return newLine.join(' ');
}

/* Below are newly added functions */

// Following codecs will not be removed from SDP event they are not in the
// user-specified codec list.
const audioCodecWhiteList = ['CN', 'telephone-event'];
const videoCodecWhiteList = ['red', 'ulpfec'];

// Returns a new m= line with the specified codec order.
function setCodecOrder(mLine, payloads) {
    const elements = mLine.split(' ');

    // Just copy the first three parameters; codec order starts on fourth.
    let newLine = elements.slice(0, 3);

    // Concat payload types.
    newLine = newLine.concat(payloads);

    return newLine.join(' ');
}

// Append RTX payloads for existing payloads.
function appendRtxPayloads(sdpLines, payloads) {
    for (const payload of payloads) {
        const index = findLine(sdpLines, 'a=fmtp', 'apt=' + payload);
        if (index !== null) {
            const fmtpLine = parseFmtpLine(sdpLines[index]);
            payloads.push(fmtpLine.pt);
        }
    }
    return payloads;
}

// Remove a codec with all its associated a lines.
function removeCodecFramALine(sdpLines, payload) {
    const pattern = new RegExp('a=(rtpmap|rtcp-fb|fmtp):' + payload + '\\s');
    for (let i = sdpLines.length - 1; i > 0; i--) {
        if (sdpLines[i].match(pattern)) {
            sdpLines.splice(i, 1);
        }
    }
    return sdpLines;
}

// Reorder codecs in m-line according the order of |codecs|. Remove codecs from
// m-line if it is not present in |codecs|
// The format of |codec| is 'NAME/RATE', e.g. 'opus/48000'.
function reorderCodecs(sdp, type, codecs) {
    if (!codecs || codecs.length === 0) {
        return sdp;
    }

    codecs = type === 'audio' ? codecs.concat(audioCodecWhiteList) : codecs.concat(
        videoCodecWhiteList);

    let sdpLines = sdp.split('\r\n');

    // Search for m line.
    const mLineIndex = findLine(sdpLines, 'm=', type);
    if (mLineIndex === null) {
        return sdp;
    }

    const originPayloads = sdpLines[mLineIndex].split(' ');
    originPayloads.splice(0, 3);

    // If the codec is available, set it as the default in m line.
    let payloads = [];
    for (const codec of codecs) {
        for (let i = 0; i < sdpLines.length; i++) {
            const index = findLineInRange(sdpLines, i, -1, 'a=rtpmap', codec);
            if (index !== null) {
                const payload = getCodecPayloadTypeFromLine(sdpLines[index]);
                if (payload) {
                    payloads.push(payload);
                    i = index;
                }
            }
        }
    }
    payloads = appendRtxPayloads(sdpLines, payloads);
    sdpLines[mLineIndex] = setCodecOrder(sdpLines[mLineIndex], payloads);

    // Remove a lines.
    for (const payload of originPayloads) {
        if (payloads.indexOf(payload) === -1) {
            sdpLines = removeCodecFramALine(sdpLines, payload);
        }
    }

    sdp = sdpLines.join('\r\n');
    return sdp;
}

// Add legacy simulcast.
function addLegacySimulcast(sdp, type, numStreams) {
    if (!numStreams || !(numStreams > 1)) {
        return sdp;
    }

    let sdpLines = sdp.split('\r\n');
    // Search for m line.
    const mLineStart = findLine(sdpLines, 'm=', type);
    if (mLineStart === null) {
        return sdp;
    }
    let mLineEnd = findLineInRange(sdpLines, mLineStart + 1, -1, 'm=');
    if (mLineEnd === null) {
        mLineEnd = sdpLines.length;
    }

    const ssrcGetter = (line) => {
        const parts = line.split(' ');
        const ssrc = parts[0].split(':')[1];
        return ssrc;
    };

    // Process ssrc lines from mLineIndex.
    const removes = new Set();
    const ssrcs = new Set();
    const gssrcs = new Set();
    const simLines = [];
    const simGroupLines = [];
    let i = mLineStart + 1;
    while (i < mLineEnd) {
        const line = sdpLines[i];
        if (line === '') {
            break;
        }
        if (line.indexOf('a=ssrc:') > -1) {
            const ssrc = ssrcGetter(sdpLines[i]);
            ssrcs.add(ssrc);
            if (line.indexOf('cname') > -1 || line.indexOf('msid') > -1) {
                for (let j = 1; j < numStreams; j++) {
                    const nssrc = (parseInt(ssrc) + j) + '';
                    simLines.push(line.replace(ssrc, nssrc));
                }
            } else {
                removes.add(line);
            }
        }
        if (line.indexOf('a=ssrc-group:FID') > -1) {
            const parts = line.split(' ');
            gssrcs.add(parts[2]);
            for (let j = 1; j < numStreams; j++) {
                const nssrc1 = (parseInt(parts[1]) + j) + '';
                const nssrc2 = (parseInt(parts[2]) + j) + '';
                simGroupLines.push(
                    line.replace(parts[1], nssrc1).replace(parts[2], nssrc2));
            }
        }
        i++;
    }

    const insertPos = i;
    ssrcs.forEach(ssrc => {
        if (!gssrcs.has(ssrc)) {
            let groupLine = 'a=ssrc-group:SIM';
            groupLine = groupLine + ' ' + ssrc;
            for (let j = 1; j < numStreams; j++) {
                groupLine = groupLine + ' ' + (parseInt(ssrc) + j);
            }
            simGroupLines.push(groupLine);
        }
    });

    simLines.sort();
    // Insert simulcast ssrc lines.
    sdpLines.splice(insertPos, 0, ...simGroupLines);
    sdpLines.splice(insertPos, 0, ...simLines);
    sdpLines = sdpLines.filter(line => !removes.has(line));

    sdp = sdpLines.join('\r\n');
    return sdp;
}

function setMaxBitrate(sdp, encodingParametersList) {
    for (const encodingParameters of encodingParametersList) {
        if (encodingParameters.maxBitrate) {
            sdp = setCodecParam(
                sdp, encodingParameters.codec.name, 'x-google-max-bitrate',
                (encodingParameters.maxBitrate).toString());
        }
    }
    return sdp;
}

class ConferenceInfo {
    // eslint-disable-next-line require-jsdoc
    constructor(id, participants, remoteStreams, myInfo) {
        /**
         * @member {string} id
         * @instance
         * @memberof Owt.Conference.ConferenceInfo
         * @desc Conference ID.
         */
        this.id = id;
        /**
         * @member {Array<Owt.Conference.Participant>} participants
         * @instance
         * @memberof Owt.Conference.ConferenceInfo
         * @desc Participants in the conference.
         */
        this.participants = participants;
        /**
         * @member {Array<Owt.Base.RemoteStream>} remoteStreams
         * @instance
         * @memberof Owt.Conference.ConferenceInfo
         * @desc Streams published by participants. It also includes streams published by current user.
         */
        this.remoteStreams = remoteStreams;
        /**
         * @member {Owt.Base.Participant} self
         * @instance
         * @memberof Owt.Conference.ConferenceInfo
         */
        this.self = myInfo;
    }
}

class Participant extends EventDispatcher {
    // eslint-disable-next-line require-jsdoc
    constructor(id, role, userId) {
        super();
        /**
         * @member {string} id
         * @instance
         * @memberof Owt.Conference.Participant
         * @desc The ID of the participant. It varies when a single user join different conferences.
         */
        Object.defineProperty(this, 'id', {
            configurable: false,
            writable: false,
            value: id,
        });
        /**
         * @member {string} role
         * @instance
         * @memberof Owt.Conference.Participant
         */
        Object.defineProperty(this, 'role', {
            configurable: false,
            writable: false,
            value: role,
        });
        /**
         * @member {string} userId
         * @instance
         * @memberof Owt.Conference.Participant
         * @desc The user ID of the participant. It can be integrated into existing account management system.
         */
        Object.defineProperty(this, 'userId', {
            configurable: false,
            writable: false,
            value: userId,
        });
    }
}

class RemoteMixedStream extends RemoteStream {
    // eslint-disable-next-line require-jsdoc
    constructor(info) {
        if (info.type !== 'mixed') {
            throw new TypeError('Not a mixed stream');
        }
        super(info.id, undefined, undefined, new StreamSourceInfo(
            'mixed', 'mixed'));

        this.settings = convertToPublicationSettings(info.media);

        this.extraCapabilities = convertToSubscriptionCapabilities(info.media);
    }
}

/**
 * @class ActiveAudioInputChangeEvent
 * @classDesc Class ActiveAudioInputChangeEvent represents an active audio input change event.
 * @memberof Owt.Conference
 * @hideconstructor
 */
class ActiveAudioInputChangeEvent extends OwtEvent {
    // eslint-disable-next-line require-jsdoc
    constructor(type, init) {
        super(type);
        /**
         * @member {string} activeAudioInputStreamId
         * @instance
         * @memberof Owt.Conference.ActiveAudioInputChangeEvent
         * @desc The ID of input stream(of the mixed stream) whose audio is active.
         */
        this.activeAudioInputStreamId = init.activeAudioInputStreamId;
    }
}

/**
 * @class LayoutChangeEvent
 * @classDesc Class LayoutChangeEvent represents an video layout change event.
 * @memberof Owt.Conference
 * @hideconstructor
 */
class LayoutChangeEvent extends OwtEvent {
    // eslint-disable-next-line require-jsdoc
    constructor(type, init) {
        super(type);
        /**
         * @member {object} layout
         * @instance
         * @memberof Owt.Conference.LayoutChangeEvent
         * @desc Current video's layout. It's an array of map which maps each stream to a region.
         */
        this.layout = init.layout;
    }
}


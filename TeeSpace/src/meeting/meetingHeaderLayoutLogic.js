Top.Controller.create('meetingHeaderLayoutLogic', {
	init: function(event, widget) {
		tds('meetingAppIcon').setProperties({'src':'./res/meeting/teeMeeting.svg'});
		
		if(getSubAppByUrl() === 'meeting') {
			tds("meetingHeaderExpandBtn").setProperties({"visible":"visible"});
			tds("meetingHeaderFoldBtn").setProperties({"visible":"none"});
		} else if(getMainAppByUrl() === 'meeting') {
			tds("meetingHeaderExpandBtn").setProperties({"visible":"none"});
			tds("meetingHeaderFoldBtn").setProperties({"visible":"visible"});
		}
	},
	
	/**
	 * 
	 * TODO(20200220, changyeop_kim) sub 앱일 경우 메인 앱으로 확장, 현재 앱 sub 여부 검사
	 * @param {*} event 클릭 이벤트
	 * @param {*} widget 확장 아이콘
	 */
	RoomExpand : function(event, widget) {
		console.log('Expand');
		onExpandButtonClick('meeting');

		tds('meetingFooter').src('meetingFooterExpandLayout.html');
		
		tds("meetingHeaderExpandBtn").setProperties({"visible":"none"});
		tds("meetingHeaderFoldBtn").setProperties({"visible":"visible"});
		meetingManager.subscribeAnotherView('checkerboard');
	},
	
	/**
	 * TODO(20200220, changyeop_kim) main앱일 경우 sub앱으로 축소, 현재 앱 main 여부 검사
	 * @param {*} event 클릭 이벤트
	 * @param {*} widget 축소 아이콘
	 */
	RoomFold : function(event, widget) {
		console.log('Fold');
		onFoldButtonClick('meeting');
		
		tds('meetingFooter').src('meetingFooterLayout.html');
		
		tds("meetingHeaderExpandBtn").setProperties({"visible":"visible"});
		tds("meetingHeaderFoldBtn").setProperties({"visible":"none"});
		meetingManager.subscribeAnotherView('common');
	},
	
	/**
	 * NOTE(20200220, changyeop_kim) 현재 앱 종료
	 * @param {*} event 클릭 이벤트
	 * @param {*} widget 종료 아이콘
	 */
	RoomClose : function(event, widget) {
		console.log('Close');
		onCloseButtonClick();		
		meetingServiceCall.getUserNum().then((res)=>{
			console.log(res);
			meetingManager.stopLocalVideo();
			meetingManager.exitMeeting();
			if (res.data.dto.participantsNum === 1){		
				meetingServiceCall.DeleteRoom().then((res) => {
					talkServer2.meetingTest( workspaceManager.getWorkspaceId(getRoomIdByUrl()) , "" , userManager.getLoginUserId(), meetingManager.data.myRoomId ,  false);
					console.log(res);
				});
			}
		});
	}
});

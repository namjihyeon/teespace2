Top.Controller.create('meetingFooterExpandLayoutLogic', {
	init: function(event, widget){
		tds('iconVideo').setProperties({'src':'./res/meeting/video.svg'});
		tds('iconMic').setProperties({'src':'./res/meeting/mic.svg'});
		tds('iconRecord').setProperties({'src':'./res/meeting/recording.svg'});
		tds('iconScreenShare').setProperties({'src':'./res/meeting/video_share.svg'});
		tds('iconTeeminute').setProperties({'src':'./res/meeting/teeminute.svg'});
		tds('iconMeetingExit').setProperties({'src':'./res/meeting/video_end.svg'});
		tds('iconSubtitle').setProperties({'src':'./res/meeting/subtitle.svg'});
		tds('iconLayout').setProperties({'src':'./res/meeting/view_change.svg'});
		tds('iconParticipant').setProperties({'src':'./res/meeting/member-meeting.svg'});
		tds('iconSetting').setProperties({'src':'./res/meeting/setting-meeting.svg'});
		this.setLocalVideoLayout();
	},
	/**
	 * local 화면 영역에 media stream 연결 후 재생
	 * volume animation 적용
	 */
	setLocalVideoLayout: function (){
		// local 화면 재생	
		document.querySelector('video#meetingExpandLocalVideoView').srcObject = meetingManager.data.localStream;
		document.querySelector('video#meetingExpandLocalVideoView').play();
		meetingManager.setVolumeAnimation();

	},
	onBtnBarMouseOver: function(event, widget){
		tds('meetingFooterExpandBtnBar').setProperties({'visible':'visible'});
	},
	onBtnBarMouseExit: function(event, widget){
		setTimeout(function(){
			tds('meetingFooterExpandBtnBar').setProperties({'visible':'none'});
		},1000);
	}
});
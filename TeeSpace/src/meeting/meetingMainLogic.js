Top.Controller.create("meetingMainLogic", {
	init: function(event, widget) {	
	  Top.Dom.selectById("meetingHeader").src("meetingHeaderLayout.html");
	  Top.Dom.selectById("meetingContent").src("meetingContentLayout.html");
	  if(getSubAppByUrl() === 'meeting') {
		  	Top.Dom.selectById("meetingFooter").src("meetingFooterLayout.html");
	  } else if(getMainAppByUrl() === 'meeting') {
			Top.Dom.selectById("meetingFooter").src("meetingFooterExpandLayout.html");
	  }
	  //meetingManager.setVideoStream();
	  Top.Dom.selectById("meetingMainLayout").onSrcLoad(meetingManager.setVideoStream());
	}
  });
  
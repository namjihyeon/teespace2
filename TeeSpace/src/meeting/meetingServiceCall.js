let meetingServiceCall = {
		GetRoomId : function() {
            let cid = workspaceManager.getChannelList(workspaceManager.getWorkspaceId(getRoomIdByUrl()), "CHN0007");
            if(!cid)
                return;
            let url = _workspace.url + "Conference/channels/"+ cid + "/RoomId";
            return axios.get(url);
        },

        CreateToken :function(userId, role) {
            let cid = workspaceManager.getChannelList(workspaceManager.getWorkspaceId(getRoomIdByUrl()), "CHN0007");
            if(!cid)
                return;
            let url = _workspace.url + "Conference/channels/" + cid + "/Token";
          
            let body = {
                "dto" : {
                    "user" : userId,
                    "role" : role
                }
            };
            return axios.post(url, body);
        },

        mixStream : function(roomId, streamid, view_label) {
            let cid = workspaceManager.getChannelList(workspaceManager.getWorkspaceId(getRoomIdByUrl()), "CHN0007");
            if(!cid)
                return;
            let url = _workspace.url + "Conference/rooms/" + roomId + "/streams/" + streamid + "/StreamMix?action=Patch";
            let body = {
                "dto" : {
                    "isAdd" : true,
                    "viewLabel" : view_label
                }
            };
            return axios.post(url, body); 
        },
        updateStreamStatus : function(roomId, streamId, isVideo, onoff) {
            let cid = workspaceManager.getChannelList(workspaceManager.getWorkspaceId(getRoomIdByUrl()), "CHN0007");
            if(!cid)
                return;
            let url = _workspace.url + "Conference/rooms/" + roomId + "/streams/" + streamId + "/StreamStatus?action=Patch";
            let body = {
                "dto" : {
                    "isVideo" : isVideo,
                    "isActive" : onoff
                }
            };
            return axios.post(url, body); 
        },
        DeleteRoom : function(){
            let cid = workspaceManager.getChannelList(workspaceManager.getWorkspaceId(getRoomIdByUrl()), "CHN0007");
            if(!cid)
                return;
            let url = _workspace.url + "Conference/channels/" + cid + "/Room";            
            return axios.delete(url);
        },
        getUserNum : function(){
            let cid = workspaceManager.getChannelList(workspaceManager.getWorkspaceId(getRoomIdByUrl()), "CHN0007");
            if(!cid)
                return;
            let url = _workspace.url + "Conference/channels/" + cid + "/Participants";            
            return axios.get(url);
        }
};
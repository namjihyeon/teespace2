let meetingCommon = {
  //NOTE(20200220,soojin) : volume에 애니메이션 효과를 제공
  volume: {
    animation: function(selector, stream, localOrRemote) {
      const volumeAnimationClassName = "icon-work_volume_0";
      let volumeAnimationIdx = 0;
      let volumeAnimationInterval = null;

      const defaultColor = "#CCCCCC";
      const defaultColorForRemote = "rgba(255,255,255,0.3)";
      const color = "#EDA900";

      if (!selector || !stream || !localOrRemote) {
        console.error(
          "meetingCommon.volume.animation is failed. check 'selector', 'stream', 'localOrRemote'."
        );
        return;
      }

      if (localOrRemote === "remote") {
      //  $(selector).removeClass("volume_init");
      }
      const speechEvents = hark(stream, {});
      
      speechEvents.on("speaking", function() {
        volumeAnimationInterval = setInterval(function() {
          $(selector).removeClass(
            volumeAnimationClassName + (volumeAnimationIdx + 1)
          );
          volumeAnimationIdx = (volumeAnimationIdx + 1) % 3;
          $(selector).addClass(
            volumeAnimationClassName + (volumeAnimationIdx + 1)
          );
          if (localOrRemote === "local") {
            $(
              "." +(volumeAnimationClassName + (volumeAnimationIdx + 1)) + " .top-icon-root"
            ).css("color", color);
          } else if (localOrRemote === "remote") {
            $(selector).css("color", color);
          }
        }, 300);
        meetingManager.data.localInterval = volumeAnimationInterval;
      });
      
      speechEvents.on("stopped_speaking", function() {
        clearInterval(volumeAnimationInterval);
        if (localOrRemote === "local") {
          $(
            "." + (volumeAnimationClassName + (volumeAnimationIdx + 1)) + " .top-icon-root"
          ).css("color", defaultColor);
        } else if (localOrRemote === "remote") {
          $(selector).css("color", defaultColorForRemote);
        }
      });
      
    }
  }
};

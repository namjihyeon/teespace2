Top.Controller.create('meetingExitLayoutLogic', {
	init : function(){
		//tds("meetingExitDialog").addClass("tee-dialog");
	},
	onMeetingExitCancle : function(){
		console.log("cancle~~");
		tds("meetingExitDialog").close();
	},
	onMeetingExit : function(){
		console.log("ok !");
		meetingServiceCall.getUserNum().then((res)=>{
			console.log(res);			
			if(!meetingManager.data.disconnected) {
			meetingManager.data.conferenceClient.leave();
			meetingManager.data.publicationGlobal.stop();
			if (res.data.dto.participantsNum === 1){
				meetingServiceCall.DeleteRoom().then((res) => {
					talkServer2.meetingTest( workspaceManager.getWorkspaceId(getRoomIdByUrl()) , "" , userManager.getLoginUserId(), meetingManager.data.myRoomId ,  false);
					console.log(res);
				});
			}
		}
		});
		tds("meetingExitDialog").close();
		meetingManager.stopLocalVideo();
		onCloseButtonClick();
	},
});

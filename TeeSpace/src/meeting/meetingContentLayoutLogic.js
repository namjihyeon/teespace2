Top.Controller.create('meetingContentLayoutLogic', {
	init: function(event, widget) {

		let remoteVideoLayoutSelector = document.querySelector('video#RemoteVideoLayout');
		
		remoteVideoLayoutSelector.srcObject = meetingManager.data.remoteStream;
		//meetingManager.setVideoStream();
		remoteVideoLayoutSelector.load();
		remoteVideoLayoutSelector.oncanplaythrough = function() {
			remoteVideoLayoutSelector.play();
		};
	},
});
/**
 * Template 관련 메소드
 * template, template 사용 메소드
 */

let meetingTemplate = {
		// meeting에서 사용되는 templates
		template : {
			//TODO(20200218,soojin_park) : tooltipTemplate ver2에 맞춰 수정 예정
			tooltipTemplate: function(right, bottom, text) {
	        	if (!right || !bottom || !text) {
	            	//console.error("conferenceLayout.template.getTooltipTemplate is failed. check 'right', 'bottom', 'text'.");
	        		return;
	        	}
	        	
	            //let tooltip = 
	        	//	$("<div class='conference-tooltip' style=left:" + (right) + "px;top:" + (bottom+14) + "px;>" +
	            //	text + "</div>");

	            //return tooltip;
			},
			
			/** TODO(20200218, changyeop_kim) : 회의 참가자의 영상 태그 Template. Ver2에 맞춰 수정 예정
			 * 회의 참가자의 정보를 받아서 Video 태그를 반환
			 * @param userName 사용자의 이름
			 * @param userId 사용자의 아이디
			 * @return {videoContainer} 
			 *  */
			videoLayoutTemplate: function(userName, userId) {
        		if (!userName) {
            		console.error("conferenceLayout.template.getVideoLayoutTemplate is failed. check 'userName'.");
        			return;
        		}

            	let videoContainer = $("<div style='width:100%; height:100%;'></div>");
				let videoElement = $("<video id='remoteVideo_" + userId +
					"' oncontextmenu='return false;' autoplay style='width:100%; height:100%; z-index:1;' muted playsinline></video>");
            	let waitingNameSpace = $("<div id='title-text-" + userId + "' class='video-title-text'>" + userName + "</div>" + 
            	"<div id='bottom-text-" + userId + "' class='video-bottom-text'>기다리는 중...</div>");
            	let playingNameSpace = $("<div id='meeting-on-name-space_" + userId + "' class='meeting-on-name-space'>" +
            	    "<span id='video-state-icon' class='video-state-icon'></span>" +
            	    "<div class='background-color'></div>" + 
            	    "<span id='mic-state-icon' class='mic-state-icon icon-work_volume_01'></span>" +
            	    "<div id='user-name' class='user-name'>" + userName + "</div>" +
            	"</div>");

 	            videoContainer.append(videoElement);
    	        videoContainer.append(waitingNameSpace);
        	    videoContainer.append(playingNameSpace);

        	    return videoContainer;
			},
			
			/**
			 * TODO(20200219, changyeop_kim) : 참여중인 미팅의 화면. Ver2에 맞춰 수정 예정 
			 * @param roomInfo 현재 참여하고 있는 방의 정보
			 * @returns container 모든 참여자의 영상 container div 
			 */
			MeetingContentLayoutTemplate: function (roomInfo) {
				if (!roomInfo) {
					console.error("conferenceLayout.template.getConferenceRoomTemplate is failed. check 'roomInfo'.");
					return;
				}
				
				let num = 6;//roomInfo.user_num;
				let container = $("<div class='meeting-room-space' ></div>");
				let numVideo = 0;
				let idx = 0;
				//TEST용 임시
				let numRow = 3;
				let numCol = 2;
	
				for(let i=0; i<numRow; i++) {
					let videoRow = $("<div id='videoRow" + i + "' class='meeting-video-row'></div>");
					videoRow.css("width", "100%")
					videoRow.css("height", "calc(" + (100/numRow) + "%)");
					for (let j=0; j<numCol; j++) {
						/*if (conferenceService.user.getUserInfo().userId === roomInfo.user_id_list[idx]){
							j -= 1;
							idx += 1;
							continue;
						}*/
	
						let videoLayout = $("<div id='remoteVideoLayout" + numVideo + "' class='meeting-video-layout' style='border:2px solid rgba(255,255,255,0.15)'></div>")
						videoLayout.css("width", "calc(" + (100/numCol) + "% - 10px)");
						//let videoElement = meetingTemplate.template.videoLayoutTemplate(roomInfo.user_name_list[idx], roomInfo.user_id_list[idx]);
						let videoElement = meetingTemplate.template.videoLayoutTemplate('username'+i+'_'+j,'id');
						videoLayout.append(videoElement);
						videoRow.append(videoLayout);
	
						idx += 1;
						numVideo += 1;
						if (false) {//numVideo == num-1) {
							$('.conference-video-layout', videoRow).css("width", "calc(" + (100/(j+1)) + "% - 10px)");
							videoRow.css("width", "calc(" + (100*(j+1)/numCol) + "%)")
							break;
						}
					}
					container.append(videoRow);
					if (numVideo == num-1) {
						break;
					}
				}
	
				return container;
			},

		},

		// NOTE(20200218, soojin_park) : 이름 바꿔주세요. userTemplate 뭔가 별로..
		useTemplate : {
			//TODO(20200218,soojin_park) : ver2에 맞춰 수정 예정
			addTooltipEvent: function (right, bottom, text) {
		        let tooltip = meetingTemplate.template.tooltipTemplate(right, bottom, text);

		        //$("body").append(tooltip);
		    },
		    //NOTE(20200218,soojin_park) : 여기에  있는게 맞는것인지 의문.. 추후 결정 필요
		    removeTooltipEvent: function () {
		        $(".conference-tooltip").remove();
			},
			

		}
}


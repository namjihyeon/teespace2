Top.Controller.create('meetingFooterLayoutLogic', {
	init: function(event, widget) {
		tds('iconVideo').setProperties({'src':'./res/meeting/video.svg'});
		tds('iconMic').setProperties({'src':'./res/meeting/mic.svg'});
		tds('iconRecord').setProperties({'src':'./res/meeting/recording.svg'});
		tds('iconScreenShare').setProperties({'src':'./res/meeting/video_share.svg'});
		tds('iconTeeminute').setProperties({'src':'./res/meeting/teeminute.svg'});
		tds('iconEllipsis').setProperties({'src':'./res/meeting/view_more.svg'});
		tds('iconMeetingExit').setProperties({'src':'./res/meeting/video_end.svg'});
		this.setLocalVideoLayout();
	},
	
	/**
	 * local 화면 영역에 media stream 연결 후 재생
	 * volume animation 적용
	 */
	setLocalVideoLayout: function (){
		// local 화면 재생	
		document.querySelector('video#meetingLocalVideoView').srcObject = meetingManager.data.localStream;
		document.querySelector('video#meetingLocalVideoView').load();
		document.querySelector('video#meetingLocalVideoView').oncanplaythrough = function () {
			document.querySelector('video#meetingLocalVideoView').play();
		};
		meetingManager.setVolumeAnimation();

	},
		
	/**
	 * TODO(20200224, soojin_park): local 정보가 표시
	*/
	/*
	drawDefaultFooterlayout: function(){
		tds('meetingFooterRightLayout').setVisible('visible');
		tds('meetingMemberListLayout').setVisible('none');
	},
	*/
	/**
	 * TODO(20200224, soojin_park): 모든 meeting 참여 user들에 대한 미디어가 표시
	 */
	/*
	drawMemberListFooterLayout: function(memberInfo){
		tds('meetingFooterRightLayout').setVisible('none');
		tds('meetingMemberListLayout').setVisible('visible');
		
		//let memberListFooterLayout = meetingTemplate.template.meetingMemberListFooterLayoutTemplate('test');
		//$('#meetingFooterRightLayout').html(memberListFooterLayout);
	}
	*/
	
});




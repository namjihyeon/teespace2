/**
* NOTE(20200310, soojin_park) : 더보기 클릭 시, 나타나는 메뉴 아이템 클릭 이벤트
* */
Top.Controller.create('TeeSpaceLogic', {
	/** 
	 * 아이콘 클릭 이벤트 리스너 
	 */	
	// mic icon 클릭 시 
	onMeetingMicClick: function (event, widget) {
		//TODO(20200218, soojin_park) : mic_on <-> mic_off시 icon 변경 필요 & manager의 micState on<->off 수정 
		meetingManager.switchMicState();
	},	
	// video icon 클릭 시 
	onMeetingVideoClick: function (event, widget) {
		//TODO(20200218, soojin_park) : video_on <-> video_off시 icon 변경 필요 & manager의 videoState on<->off 수정 
		meetingManager.switchCameraState();
	},	
	
	// cloud app icon 클릭 시 
	onMeetingCloudAppClick: function (event, widget){
		//TODO(20200218, soojin_park) : cloud app 공유 기능 추가 및 연동
		//meetingManager.showCloudOfficeMode();
	},
	
	// screen share icon 클릭 시 
	onMeetingScreenShareClick: function (event, widget){
		if (meetingManager.data.shareStream !== null){
			console.log('Screen sharing is already in progress.');
			return ;
		}
		
		let audioConstraints = new AudioTrackConstraints(AudioSourceInfo.SCREENCAST);
		let videoConstraints = new VideoTrackConstraints(VideoSourceInfo.SCREENCAST);
		let mediaStream;
		MediaStreamFactory.createMediaStream(
			new StreamConstraints(audioConstraints, videoConstraints)).then(stream => {
				console.log('Screen share stream created.');
				let publishOption;
				let myRoomId = meetingManager.data.conferenceClient.info.id;
				mediaStream = stream;
				mediaStream.addEventListener('inactive', e => {
					console.log('screen share ended.');                                 
					meetingManager.data.publicationScreen.stop();
					meetingManager.data.publicationScreen = null;
					meetingManager.data.shareStream = null;
					if (meetingManager.data.publicationGlobal !== null){
						meetingManager.rePublishLocalCam({}, false);
					}
					//ui 변경
				});
				meetingManager.data.shareStream = mediaStream;
				localStream = new LocalStream(
					mediaStream, new StreamSourceInfo(
						'screen-cast', 'screen-cast'));

				meetingManager.data.conferenceClient.publish(localStream, publishOption)
				.then(publication => {
					if (meetingManager.data.publicationGlobal !== null){
						meetingManager.data.publicationGlobal.stop();
					}
					meetingManager.data.publicationScreen = publication;
					meetingManager.data.viewList.forEach((view) => {
						meetingServiceCall.mixStream(myRoomId, publication.id, view);
					});
					publication.addEventListener('error', (err) => {
						console.log('[OWT Server error] Screen sharing publication error: ' + err.error.message);
						meetingManager.data.shareStream = null;
					});
				});
			}, err => {
				console.error('Failed to create screen share stream, ' + err);
			});
	},
	
	// 더보기 클릭 시
	onMeetingEllipsisClick: function(event, widget){
        let meetingExtraMenu = tds("meetingExtraMenu");
        
        meetingExtraMenu.open({ 
        	clientX: event.clientX+10, 
        	clientY: event.clientY-200
        });
        
        event.stopPropagation();
	},
	onMeetingEllipsisMousePressed: function(event, widget){
		//tds('iconEllipsis').setProperties({'src':'./res/meeting/view_more_pink.svg'});
	},
	onMeetingEllipsisMouseReleased : function(event, widget) {
		//tds('iconEllipsis').setProperties({'src':'./res/meeting/view_more.svg'});
	},
	//미팅 종료 버튼 클릭시
	onMeetingExitClick: function (event, widget) {
		//tds("meetingExitDialog").open();
		TeeAlarm.open({
		    title: '미팅을 종료하시겠습니까?', 
		    content: '', 
		    icon: 'icon-bell',
		    buttons: [
		        {
		            text: '확인',
		            onClicked: (e) =>  {
						meetingServiceCall.getUserNum().then((res)=>{
							console.log(res);
							meetingManager.stopLocalVideo();
							meetingManager.exitMeeting();									
							if (res.data.dto.participantsNum === 1){
								meetingServiceCall.DeleteRoom().then((res) => {
									talkServer2.meetingTest( workspaceManager.getWorkspaceId(getRoomIdByUrl()) , "" , userManager.getLoginUserId(), meetingManager.data.myRoomId ,  false);
									console.log(res);
								});
							}
						});						
						TeeAlarm.close();
						onCloseButtonClick();
					}
		        }
		    ],
		    cancelButtonText: '취소', // default 취소 
		});
	},
	onMeetingRecordClick : function(event, widget) {
		//console.log("녹화 눌럿다");
	}, onMeetingSubtitleClick : function(event, widget) {
		//console.log("자막 눌렀다");
	}, onMeetingLayoutClick : function(event, widget) {
		//console.log("화면 레이아웃 변경");
		//meetingManager.subscribeAnotherView('checkerboard');
	}, onMeetingParticipantClick : function(event, widget) {
		//console.log("참여자 목록");
		//tds('ShowMeetingMemDialog').open();
	}, onMeetingSettingClick : function(event, widget) {
		//console.log("환경 설정");
	}, onMeetingTeeminuteClick : function(event, widget) {
		//console.log("Teemeeting 회의록 기능 활성화");
	}	
});

/**
 * meeting 관련 자원 관리를 위한 변수 및 함수
 * 자원 할당 및 사용 관련 로직
 */
const meetingMode = {
	DEFAULT: 'default',
	CLOUDOFFICE: 'cloudoffice',
	SCREENSHARE: 'screenshare'
};

let meetingManager = {
	data: {
		shareStream: null,
		disconnected: true,
		pcc: null,
		publicationGlobal: null,
		publicationScreen : null,
		conferenceClient: null,
		currentRoomInfo: null,  //현재 방의 정보
		//RoomList : [],
		localStream: null,
		remoteStream: null,
		micState: null,
		cameraState: null,
		localInterval: null, //meetingCommon.volume.animation에서 사용 중
		myInfo: null,
		memberInfo: [], // 현재 meeting에서 나를 제외한 유저들의 정보
		meetingMode: meetingMode.DEFAULT, // DEFAULT,CLOUDOFFICE,SHARESCREEN모드
		viewList: ["common", "checkerboard"], // 사업부 요청으로 우선 view 2개만 사용 , "focus", "min"],
		mixedStreams: [],
		currentStreamNum: 0,
		myRoomId: null
	},
	/**
	* For Setting OWT client
	*/
	setVideoStream: function () {
		if(!meetingManager.data.disconnected) {								
			/*let localVideoLayoutSelector;
			let viewtype;*/
			if(getSubAppByUrl() == "meeting") {
				//localVideoLayoutSelector = document.querySelector('video#meetingLocalVideoView');
				viewtype = "common";
			}
			else if(getMainAppByUrl()== "meeting") {
				//localVideoLayoutSelector = document.querySelector('video#meetingExpandLocalVideoView');
				viewtype = "checkerboard";
			}
			/*localVideoLayoutSelector.srcObject = meetingManager.data.localStream;
			localVideoLayoutSelector.load();
			localVideoLayoutSelector.muted = true;
			localVideoLayoutSelector.oncanplaythrough = function () {
				localVideoLayoutSelector.play();
			};*/
			meetingManager.subscribeAnotherView(viewtype);
		} else {
			let conferenceClient = new ConferenceClient();
			meetingManager.data.conferenceClient = conferenceClient;
			meetingManager.data.currentRoomInfo = workspaceManager.getWorkspaceId(getRoomIdByUrl());
			let simulcast =  false;
			let shareScreen = false;
			let isPublish = false;
			let myId = null;
			let subscribeForward = false;
			let isSelf = true;

			meetingManager.data.conferenceClient.addEventListener('streamadded', (event) => {
				console.log('A new stream is added ', event.stream.id);
				isSelf = isSelf ? isSelf : event.stream.id != meetingManager.data.publicationGlobal.id;
				subscribeForward && isSelf && meetingManager.subscribeAndRenderVideo(event.stream);
				meetingServiceCall.mixStream(meetingManager.data.myRoomId, event.stream.id, 'common');
				event.stream.addEventListener('ended', () => {
					meetingManager.data.currentStreamNum--;
					console.log(event.stream.id + ' is ended.');
				});
			});

			meetingServiceCall.GetRoomId().then((roomInfo) => {
				if(roomInfo.data.dto.created){
					talkServer2.meetingTest( workspaceManager.getWorkspaceId(getRoomIdByUrl()) , "" , userManager.getLoginUserId(), roomInfo.data.dto.roomId ,  true);
				}
				meetingServiceCall.CreateToken(userManager.getLoginUserId(), 'presenter').then(function (response) {
					//console.log(response);
					let token = response.data.dto.token;
					meetingManager.data.conferenceClient.join(token).then(resp => {
						//console.log(resp);
						myId = resp.self.id;
						meetingManager.data.myRoomId = resp.id;
						meetingManager.data.disconnected = false;
						if (isPublish !== 'false') {
							// audioConstraintsForMic
							let audioConstraints = new AudioTrackConstraints(AudioSourceInfo.MIC);
							// videoConstraintsForCamera
							let videoConstraints = new VideoTrackConstraints(VideoSourceInfo.CAMERA);
							videoConstraints.resolution = new Resolution(
									tds('meetingFooterBackgroundLayout').getWidth(),
									tds('meetingFooterBackgroundLayout').getHeight()
								);

							let mediaStream;
							MediaStreamFactory.createMediaStream(new StreamConstraints(
								audioConstraints, videoConstraints)).then(stream => {
									let publishOption;
									if (simulcast) {
										publishOption = {
											video: [
												{ rid: 'q', active: true/*, scaleResolutionDownBy: 4.0*/ },
												{ rid: 'h', active: true/*, scaleResolutionDownBy: 2.0*/ },
												{ rid: 'f', active: true }
											]
										};
									}
									mediaStream = stream;
									localStream = new LocalStream(
										mediaStream, new StreamSourceInfo(
											'mic', 'camera'));
									meetingManager.data.micState = "on";
									meetingManager.data.cameraState = "on";
									meetingManager.data.localStream = stream;
									
									let localVideoLayoutSelector;

									if(getSubAppByUrl() == "meeting")
										localVideoLayoutSelector = document.querySelector('video#meetingLocalVideoView');
									else if(getMainAppByUrl()== "meeting")
										localVideoLayoutSelector = document.querySelector('video#meetingExpandLocalVideoView');
									localVideoLayoutSelector.srcObject = stream;
									localVideoLayoutSelector.load();
									localVideoLayoutSelector.muted = true;
									localVideoLayoutSelector.oncanplaythrough = function () {
										localVideoLayoutSelector.play();
									};

									meetingManager.data.conferenceClient.publish(localStream, publishOption).then(publication => {
										meetingManager.data.publicationGlobal = publication;
										meetingManager.data.viewList.forEach((view)=>{
											meetingServiceCall.mixStream(meetingManager.data.myRoomId, publication.id, view);
										});
										publication.addEventListener('error', (err) => {
											console.log('Publication error: ' + err.error.message);
										});
									});
								}, err => {
									console.error('Failed to create MediaStream, ' + err);									
									meetingManager.data.micState = "none";
									meetingManager.data.cameraState = "none";
									console.log("There is no webcam.");
								});
						}
						let streams = resp.remoteStreams;
						for (const stream of streams) {
							if (!subscribeForward) {
								if (stream.source.audio === 'mixed' || stream.source.video === 'mixed') {
									meetingManager.data.mixedStreams.push(stream);
									if (stream.id.includes("common") && getSubAppByUrl() == "meeting") 
										meetingManager.subscribeAndRenderVideo(stream);
									else if (stream.id.includes("checkerboard") && getMainAppByUrl() == "meeting")
										meetingManager.subscribeAndRenderVideo(stream);
								}
							} else if (stream.source.audio !== 'mixed') {								
								stream.addEventListener('ended', () => {
									meetingManager.data.currentStreamNum--;
								});
								//meetingManager.subscribeAndRenderVideo(stream);
							}
						}
						meetingManager.data.currentStreamNum = streams.length - 1;
						console.log('Streams in conference:', streams.length);
						let participants = resp.participants;
						console.log('Participants in conference: ' + participants.length);
					}, function (err) {
						console.error('server connection failed:', err);
						if (err.message.indexOf('connect_error:') >= 0) {
							const signalingHost = err.message.replace('connect_error:', '');
							const signalingUi = 'signaling';
						}
					});
				});
			});
		}
		//}
	},
	rePublishLocalCam : function (publishOption, simulcast){
		if (simulcast) {
			publishOption = {
				video: [
					{ rid: 'q', active: true/*, scaleResolutionDownBy: 4.0*/ },
					{ rid: 'h', active: true/*, scaleResolutionDownBy: 2.0*/ },
					{ rid: 'f', active: true }
				]
			};
		}
		let localStream = new LocalStream(
			meetingManager.data.localStream, new StreamSourceInfo(
				'mic', 'camera'));
		meetingManager.data.micState = "on";
		meetingManager.data.cameraState = "on";

		meetingManager.data.conferenceClient.publish(localStream, publishOption).then(publication => {
			meetingManager.data.publicationGlobal = publication;
			meetingManager.data.viewList.forEach((view)=>{
				meetingServiceCall.mixStream(meetingManager.data.myRoomId, publication.id, view);
			});
			publication.addEventListener('error', (err) => {
				console.log('Publication error: ' + err.error.message);
			});
		});
	},
	exitMeeting : function(){
		meetingManager.data.conferenceClient && meetingManager.data.conferenceClient.leave();
		meetingManager.data.publicationGlobal && meetingManager.data.publicationGlobal.stop();
		meetingManager.data.conferenceClient = null;
		meetingManager.data.publicationGlobal = null;
		meetingManager.data.disconnected = true;
		console.log('Exit Meeting');
	},
	subscribeAndRenderVideo : function (stream) {
		meetingManager.data.conferenceClient.subscribe(stream)
			.then((subscription) => {
				meetingManager.data.remoteStream = stream.mediaStream;

				let remoteVideoLayoutSelector = document.querySelector('video#RemoteVideoLayout');
				//remoteVideoLayoutSelector.controls = true;
				remoteVideoLayoutSelector.srcObject = stream.mediaStream;
				remoteVideoLayoutSelector.load();
				remoteVideoLayoutSelector.oncanplaythrough = function () {
					remoteVideoLayoutSelector.play();
				};

			}, (err) => {
				console.log('subscribe failed', err);
			});
	},
	subscribeAnotherView : function(anotherViewLabel){
		meetingManager.data.mixedStreams.forEach((stream)=>{
			if (stream.id.includes(anotherViewLabel))
				meetingManager.subscribeAndRenderVideo(stream);
		});
	},
    /**
     * For Setting My WebCam
     */
	startLocalVideo: function () {
		return new Promise(function (resolve, reject) {
			if (!meetingManager.data.localStream && meetingManager.data.cameraState !== "none") {
				try {
					const userAgent = navigator.userAgent.toLowerCase();
					if (userAgent.indexOf("iphone") > -1 || userAgent.indexOf("ipad") > -1 || userAgent.indexOf("ipod") > -1) // iOS일 때
						meetingManager.routToConferenceRoom();
				} catch (err) {
					reject(new Error("Route to conference room is failed. " + err));
				}
				var constraints = {
					audio: true,
					video: {
						width: 720,
						height: 360
					}
				}
				navigator.mediaDevices.getUserMedia(constraints).then(function (stream) {
					meetingManager.data.localStream = stream;
					meetingManager.data.micState = "on";
					meetingManager.data.cameraState = "on";
					console.log("webcam start");
					resolve(stream);
				}).catch(function (err) {
					meetingManager.data.micState = "none";
					meetingManager.data.cameraState = "none";
					console.log("There is no webcam. ");
					resolve();
				});
			} else if (meetingManager.data.localStream) {
				resolve(meetingManager.data.localStream);
			} else {
				resolve();
			}
		})
	},

    /**
     * NOTE(20200220, changyeop_kim) 웹캠 영상 및 음성 종료
     */
	stopLocalVideo: function () {
		if (meetingManager.data.localStream !== null){
			meetingManager.data.localStream.getTracks().forEach((track) => {
				track.stop();
			});			
			console.log("webcam stop");
			meetingManager.data.localStream = null;
		}
		if (meetingManager.data.shareStream !== null){
			meetingManager.data.shareStream.getTracks().forEach((track) => {
				track.stop();
			});
			meetingManager.data.shareStream = null;
			console.log("Screen share stop");
		}
		meetingManager.data.micState = null;
		meetingManager.data.cameraState = null;
	},

	setVolumeAnimation: function () {
		if (this.data.micState == "on") {
			meetingCommon.volume.animation('top-icon#iconAnimation', this.data.localStream, 'local');
		} else {
			//  $('.mic-state-icon', localVideo).removeClass('volume_init');
			//  $('.mic-state-icon', localVideo).addClass('off');

		}
	},
	 //NOTE(20200218, soojin_park) : micState on<->off, local stream의 audio track 조작
    switchMicState: function () {
    	//console.log("switch mic state!!");
    	if(this.data.micState == "on"){
    		this.data.micState = "off";
    		tds('iconMic').setProperties({'src':'./res/meeting/mic_none.svg'});
			//meetingManager.data.localStream.getAudioTracks()[0].enabled = false;			
			meetingServiceCall.updateStreamStatus(meetingManager.data.myRoomId, meetingManager.data.publicationGlobal.id, false, false);
    	}else{ 
    		this.data.micState = "on";
    		tds('iconMic').setProperties({'src':'./res/meeting/mic.svg'});
    		//meetingManager.data.localStream.getAudioTracks()[0].enabled = true;
			meetingServiceCall.updateStreamStatus(meetingManager.data.myRoomId, meetingManager.data.publicationGlobal.id, false, true);
    	}
    },
    
    //TODO(20200218, soojin_park) : cameraState on<->off, local stream의 video track 조작
    switchCameraState: function () {
    	//console.log("switch camera state!!");
    	if(this.data.cameraState == "on"){
    		this.data.cameraState = "off";
    		tds('iconVideo').setProperties({'src':'./res/meeting/video_none.svg'});
			//this.data.localStream.getVideoTracks()[0].enabled = false;
			meetingServiceCall.updateStreamStatus(meetingManager.data.myRoomId, meetingManager.data.publicationGlobal.id, true, false);
    	}else{ 
    		this.data.cameraState = "on";
    		tds('iconVideo').setProperties({'src':'./res/meeting/video.svg'});
			//this.data.localStream.getVideoTracks()[0].enabled = true;
			meetingServiceCall.updateStreamStatus(meetingManager.data.myRoomId, meetingManager.data.publicationGlobal.id, true, true);			
    	}
    },
    /**
     * NOTE(20200224, soojin_park) : 
     * FooterLayout의 icon 클릭 시, 화면 확장 모드로 변환 및 data.meetingMode 설정 
     * 각 mode에 따라 layout 배치가 달라짐.
     */
	showDefaultMode: function () {
		console.log('show default mode');
		this.data.meetingMode = meetingMode.DEFAULT;
		onExpendButtonClick('meeting');
	},

	showCloudOfficeMode: function () {
		console.log('show cloud office mode');
		this.data.meetingMode = meetingMode.CLOUDOFFICE;
		onExpandButtonClick('meeting');
		//TODO(20200224, soojin_park) : contents엔 오피스 화면이, footer에는 멤버들의 영상이 나열됨.
		//ContentLayout 수정 예정
		Top.Controller.get('meetingFooterLogic').drawMemberListFooterLayout();
	},

	showScreenShareMode: function () {
		console.log('show screen share mode');
		this.data.meetingMode = meetingMode.SCREENSHARE;
		onExpandButtonClick('meeting');
		//TODO(20200224, soojin_park) : contents엔 스크린 공유 화면이, footer에는 멤버들의 영상이 나열됨.
		//ContentLayout 수정 예정       
		Top.Controller.get('meetingFooterLogic').drawMemberListFooterLayout();
	},

	getMeetingMode: function () {
		return this.data.meetingMode;
	},
};
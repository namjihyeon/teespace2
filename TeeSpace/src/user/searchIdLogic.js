
//var name_in_1;
//var email_in_1;
var id_output;
var date_output;
var rst;
var rst_1;
//


//메일 검증
function checkMail(str){
	let exptext = /^([0-9a-zA-Z_\.-]+)@([0-9a-zA-Z_-]+)(\.[0-9a-zA-Z_-]+){1,2}$/;

	if(exptext.test(str)==false){
		return false
	}else{
		return true
	}
	
}



Top.Controller.create('searchIdFirstLogic', {
	init: function(event,widget){
				
		Top.Dom.selectById("TextField1140_1").focus();		
		tds('TextField1140_1').setProperties({"onKeyup":"nameKeyUp"})
		
	},	
	nameKeyUp : function(event, widget){
		tds('Icon1333').setProperties({"visible":"none"})
	},
	//아이디 찾기 버튼 클릭 시, 레이아웃 전환
	searchIdBtnClick : function(event, widget) {
		let type = 'type1';
		var name_in = Top.Dom.selectById("TextField1140_1").getText();
		var email_in = Top.Dom.selectById("TextField1378_1").getText();		
		let key2_1 = Top.Dom.selectById("TextField1591_1").getText();
		
		$.ajax({
	        url: _workspace.url+"Users/AuthToken", //Service Object
	        type: 'GET',
	        dataType: 'json',
	        crossDomain: true,
	        contentType : "application/json",
	        xhrFields: {
	            withCredentials: true
	          },
	          data: JSON.stringify({
		          "dto": {
		            "EMAIL_ADDRESS" : email_in,
		            "TOKEN"  : key2_1
		          }
		        }),
	        success: function(result) {
	        	if(result.dto.TOKEN == "RST0001"){
	    			Top.Dom.selectById("Button1523_1").setProperties({"disabled":"true"});
	    			tds('Button1523_1').removeClass('singnup-solid')
	    			tds('Button1523_1').addClass("signup-disabled")
	    			Top.Dom.selectById("Button1686").setProperties({"disabled":"false"});
	    			tds('Button1686').removeClass("signup-disabled")
	    			tds('Button1686').addClass('signup-solid')
	    			//유효시간영역 사라지게
	    			Top.Dom.selectById('LinearLayout1288_1').setProperties({"visible" : "none" })
   			
	    			
	    			$.ajax({
	    				url: _workspace.url+'Users/IdSearch?action=', //Service Object
	    				type: 'POST',
	    				dataType: 'json',
	    				crossDomain: true,
	    				contentType : "application/json",
	    				xhrFields: {
	    					
	    					withCredentials: true
	    					
	    				},
	    				data: JSON.stringify({
	    					"dto": {
	    						"USER_NAME": name_in,
	    						"USER_EMAIL" : email_in,
	    						"USER_TYPE" : type
	    					}
	    				}),
	    				success: function(result) {
	        	
	    					if(result.dto.dtos.length!=0){
	    						//해당 아이디 있음
	    						rst = result.dto.dtos;
	    						//tds('LinearLayoutSearchIdText').
	    						Top.Dom.selectById("LinearLayout4949_1").src('searchIdSecondContentLayout.html');
	    						Top.Dom.selectById("Master2Layout").src('searchIdSecondButtonLayout.html');
		    		
		    		
	    					}else{
	    						//해당아이디 없음
	    						Top.Dom.selectById("LinearLayout4949_1").src('searchIdThirdContentLayout.html');
	    						Top.Dom.selectById("Master2Layout").src('searchIdThirdButtonLayout.html');
	    					}
	        	
	    				},
	    				error: function(error) {
	        
	    				}
	    			})   			
	    		}
	    		else{
	    			Top.Dom.selectById("TextField1591_1").addClass("alert")
	    			Top.Dom.selectById("Icon1247_1").setProperties({"visible":"visible"})
	    			Top.Dom.selectById("AuthErrorPopover").open()
	    			return;
	    		}
	    		
	        },
	        error: function(error) {
	        }
	      });		


	}, 	
	/****먼저 그리기 위해**/
	mailtext_1 : function(event, widget) {
		Top.Dom.selectById("TextField1378_1").removeClass("alert")
		Top.Dom.selectById("Icon1247").setProperties({"visible":"none"});
		Top.Dom.selectById("Icon1247Null").setProperties({"visible":"none"});
		
		if(Top.Dom.selectById("TextField1378_1").getText() === null || Top.Dom.selectById("TextField1378_1").getText().length  === 0){
			Top.Dom.selectById("Button1686").setProperties({"disabled":"true"});
			tds('Button1686').removeClass("signup-solid")
			tds('Button1686').addClass('signup-disabled')
		}else{
			Top.Dom.selectById("Button1523_1").setProperties({"disabled":"false"});
			tds("Button1523_1").removeClass('signup-disabled')
			tds("Button1523_1").addClass('signup-solid')
		}
		
	},
	requestBtn_1 : function(event, widget) {
		
		let aa_1 = Top.Dom.selectById("Button1523_1");
			
		let mail_verify_1 = Top.Dom.selectById("TextField1378_1").getText();
		
		
		
		if(checkMail(mail_verify_1)){	
			$.ajax({
		        url: _workspace.url+"Users/NameMailCheck", //Service Object
		        type: 'GET',
		        dataType: 'json',
		        crossDomain: true,
		        contentType : "application/json", 
		        xhrFields: {
		
		            withCredentials: true
		
		          },
	          data: JSON.stringify({
		          "dto": {
		            "USER_NAME": Top.Dom.selectById("TextField1140_1").getText(),
		            "USER_EMAIL" : Top.Dom.selectById("TextField1378_1").getText()
		          }
		       }),
		       success: function(result){

		    		   //이름과 이메일 일치 할때
		    			Top.Dom.selectById("TextField1378_1").removeClass("alert")
		    			Top.Dom.selectById("Icon1247").setProperties({"visible":"none"});

		    	   
		    	   $.ajax({
				        url: _workspace.url+"Mail/Mail?action=AuthRequest", //Service Object
				        type: 'POST',
				        dataType: 'json',
				        crossDomain: true,
				        contentType : "application/json",
				        xhrFields: {
				
				            withCredentials: true
				
				          },
				          data: JSON.stringify({
					          "dto": {
					            "CMFLG": "MailAuthSend",
					            "EMAIL_ADDRESS" : mail_verify_1,
					            "NAME" : "Auth Request",
					            "TYPE" : "HTML",
					            "LOCALE" : "kr",
					            "USER_ID" : "705b51cb-85d8-45bb-9aac-a74aa56d7c3f",
					            "GATEWAY_NAME" : "TEST"
					          }
					        }),
				        success: function(result) {
				        
			
							Top.Dom.selectById("TextField1591_1").setProperties({"disabled":"false"});
							tds("TextField1591_1").removeClass("signup-disabled")
							tds('TextField1591_1').addClass("signup")
							Top.Dom.selectById("Icon1247").setProperties({"visible":"none"});
				
							if("인증 재요청"==Top.Dom.selectById("Button1523_1").getText()){ 
								
								Top.Dom.selectById("timer1_1").resume();
								tds('timer1_1').setProperties({"onEndtimer":"endTimerOpen"})
							}
							else{
								//타이머 append
								//Top.Dom.selectById('LinearLayout1288_1').append(dd_1);
								
								Top.Dom.selectById("timer1_1").setProperties({"visible":"visible"});
								Top.Dom.selectById("timer1_1").start();
								tds('timer1_1').setProperties({"onEndtimer":"endTimerOpen"})
								Top.Dom.selectById("timer1_1").setProperties({"text-color":"#2589EB"});
								Top.Dom.selectById("Button1523_1").setProperties({"text": "인증 재요청"});
							}
							
							Top.Dom.selectById("TextView1415_1").setProperties({"visible":"visible"});
							
				        },
				        error: function(error) {
				        }
			      });	    	   
		       }
			});					
		}
		else {
			Top.Dom.selectById("TextField1378_1").addClass("alert")
			Top.Dom.selectById("Icon1247Null").setProperties({"visible":"none"});
			Top.Dom.selectById("Icon1247").setProperties({"visible":"none"});
			//오류 메시지 발생하는 경우의 수 2개
			if(Top.Dom.selectById("TextField1378_1").getText() === null || Top.Dom.selectById("TextField1378_1").getText().length === 0 ){
				Top.Dom.selectById("Icon1247Null").setProperties({"visible":"visible"});
				setTimeout(function() {
					Top.Dom.selectById("EmailNullPopover").open()
	    				}, 100);		
			}
			else{
				Top.Dom.selectById("Icon1247").setProperties({"visible":"visible"});
				setTimeout(function() {
					Top.Dom.selectById("EmailValidPopover").open()
	    				}, 100);
			}	
		}
	
		
	},
	 nametext_1 : function(event, widget) {
		
		let name_verify_s = Top.Dom.selectById("TextField1140_1").getText();
		
		if(name_verify_s === null || name_verify_s.length === 0) {
			
		
			Top.Dom.selectById("TextField1140_1").addClass("alert")
			Top.Dom.selectById("Icon1333").setProperties({"visible":"visible"});
	
			setTimeout(function() {
				Top.Dom.selectById("NameErrorPopover").open()
    				}, 100);
			
		}
		
		else{
			Top.Dom.selectById("TextField1140_1").removeClass("alert")
			
			Top.Dom.selectById("Icon1333").setProperties({"visible":"none"});
		}
		
	}, verifyNum : function(event, widget) {
		
		Top.Dom.selectById("TextField1591_1").removeClass("alert")
		Top.Dom.selectById("Icon1247_1").setProperties({"visible":"none"})
		
		if(Top.Dom.selectById("TextField1591_1").getText() === null || Top.Dom.selectById("TextField1591_1").getText().length === 0 ){
			Top.Dom.selectById("Button1686").setProperties({"disabled":"true"})
			tds('Button1686').removeClass('signup-solid')
			tds('Button1686').addClass('signup-disabled')
		}else{
			Top.Dom.selectById("Button1686").setProperties({"disabled":"false"})
			tds('Button1686').removeClass('signup-disabled')
			tds('Button1686').addClass('signup-solid')
		}
		
		

	}
	
});


//라우팅 페이지
Top.Controller.create('searchIdSecondButtonLogic', {
	loginbutton : function(event, widget) {
		Top.Dom.selectById('IdSearchDialog').close();
	}, pwdbutton : function(event, widget) {
		Top.Dom.selectById('IdSearchDialog').close();
		Top.Dom.selectById('PwChangeDialog').open();
	}
});

////////////로그인 회원가입
Top.Controller.create('searchIdThirdButtonLogic', {
	init : function(event,widget){
		if(typeof _validBtoc ==='function' && !_validBtoc()){
			tds('Button2100_1').setProperties({"display":"none"})
		}
	},
	loginbutton_7 : function(event, widget) {
		Top.Dom.selectById('IdSearchDialog').close();
	}, 
	pwdbutton_7 : function(event, widget) {
		Top.Dom.selectById('IdSearchDialog').close();
			Top.Dom.selectById('RegisterDialog').open();
		
	}
	
});
///////////////아이디 고르는 페이지
Top.Controller.create('searchIdSecondContentLogic', {
	init: function(event, widget){
		
//		let ll = rst.length;
//		let mst= Top.Dom.selectById("IdResultLayout");
//		let ymd= rst.REGI_DATE.split(" ")[0].split("-");
//		let date = ymd[0]+"."+ymd[1]+"."+ymd[2];
//		
//		tds('TextView1').setText(rst.USER_LOGIN_ID);
//		tds('TextView2').setText("가입일 : "+date)
		let mst= Top.Dom.selectById("IdResultLayout");
		let ymd= rst[0].REGI_DATE.split(' ')[0].split("-");
		let date = ymd[0]+"."+ymd[1]+"."+ymd[2];
		
//		if(typeof _validBtoc ==='function' && !_validBtoc()){
//			tds('TextView1').setText(rst[0].USER_LOGIN_ID);
//			tds('TextView2').setText("가입일 : "+date)
//		}else{
			tds('TextView1').setText(rst[0].USER_LOGIN_ID);
			tds('TextView2').setText("가입일 : "+date)	
//		}
//		for(var i=0; i<ll ;i++){
//			//라디오 버튼 결과값만큼 붙이기
//			let ymd= rst[i].REGI_DATE.split(" ")[0].split("-");
//			let date = ymd[0]+"."+ymd[1]+"."+ymd[2];
//			let html = '<top-radiobutton id = "IdResultButton'+i+'" class="signup linear-child-horizontal" " layout-width = "auto" layoutHorizontalAlignment="CENTER" layout-height = "auto" margin="13px 0px 0px 0px"  text-size = "13px" group-id = "idresult" text = "'+rst[i].USER_LOGIN_ID +' (가입일 : '+ date +   ')"></top-radiobutton>';
//			
//			console.info(html)
//			mst.append(html);
//			mst.complete();
//			
//		}
	}
});
Top.Controller.create('timerEndDialogLayoutLogic', {
	endTimerOk : function(event, widget) {
		if($('#top-dialog-root_IdSearchDialog .top-dialog-title').text() === "아이디 찾기"){
			Top.Dom.selectById("timerEndDialog").close()
			Top.Dom.selectById("IdSearchDialog").close();
			setTimeout(function() {
		   		Top.Dom.selectById("IdSearchDialog").open();
		   	}, 100);
		
		}else{
			Top.Dom.selectById("timerEndDialog").close()
			Top.Dom.selectById("PwChangeDialog").close();
			setTimeout(function() {
		   		Top.Dom.selectById("PwChangeDialog").open();
		   	}, 100);
		}

	}

});
function endTimerOpen(){
	Top.Dom.selectById("timerEndDialog").open()
}






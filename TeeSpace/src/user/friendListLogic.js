const FriendAPI = {
	GetList: function (USER_ID) {
		return axios.post(_workspace.url + 'Users/GetFriend?action=List', {
			dto: {
				USER_ID: USER_ID
			}
		});
	},
	GetWorkspaceId: function (USER_ID, FRIEND_ID) {
		return axios.post(_workspace.url + 'SpaceRoom/SpaceRoom', {
			dto: {
				WS_ACCESS_TYPE: 'DM',
				WS_NAME: null,
				ADMIN_ID: USER_ID,
				WS_TYPE: 'WKS0002',
				WsUserList: [{USER_ID: FRIEND_ID}]
			}
		})
		.then(res => res.data.dto.RESULT_URL);
	},
	SetFavoriteFriend: function (USER_ID, FRIEND_ID) {
		return axios.post(_workspace.url + 'Users/UpdateFriend?action=', {
			dto: {
				USER_ID: USER_ID,
				FRIEND_ID: FRIEND_ID,
				FRIEND_FAV: 'FAV0001'
			}
		})
		.then(res => res.data.dto.RESULT_URL);
	},
	UnsetFavoriteFriend: function (USER_ID, FRIEND_ID) {
		return axios.post(_workspace.url + 'Users/UpdateFriend?action=', {
			dto: {
				USER_ID: USER_ID,
				FRIEND_ID: FRIEND_ID,
				FRIEND_FAV: 'FAV0002'
			}
		})
		.then(res => res.data.dto.RESULT_URL);
	},
	DeleteFriend: function (USER_ID, FRIEND_ID) {
		return axios.post(_workspace.url + 'Users/FriendDelete?action=', {
			dto: {
				dtos: [
					{
						USER_ID: USER_ID,
						FRIEND_ID: FRIEND_ID,
					}
				]
			}
		})
		.then(res => res.data.dto);
	},
};

const FriendListModel = (function () {
	let instance = null;
	
	function FriendListModel() {
		function fetchFriendList(userId) {
			model.isLoaded.set(false);
			model.list.replace([]);
			return FriendAPI.GetList(userId)
							.then(res => {
								let friendList = [];
								if (res.data.dto.dtos) {
									friendList = res.data.dto.dtos.map(v => {
										let m = v;
										m.lastUpdated = new Date();
										return m;
									});
								}
								model.list.replace(friendList); 
							})
							.catch(err => {})
							.finally(() => model.isLoaded.set(true)); 
		}
		
		function updateFriend(friendId, info = {}) {
			const friend = model.list.find(v => v.FRIEND_ID === friendId);
			if (friend) {
				Object.assign(friend, info);
			}
		}

		function removeFriend(friendId) {
			const idx = model.list.findIndex(v => v.FRIEND_ID === friendId);
			if (idx > -1) {
				model.list.splice(idx, 1);
			}
		}

		function findByLoginUserId(loginUserId) {
			return model.list.find(v => v.USER_LOGIN_ID === loginUserId);
		}

		const model = {
			isLoaded: mobx.observable.box(false),
			list: mobx.observable.array([]),
			fetch: fetchFriendList,
			update: updateFriend,
			remove: removeFriend,
			findByLoginUserId,
		}

		return model;
	}

	if (!instance) {
		instance = new FriendListModel();
	}
	
	return instance;
})() ;


FriendListView = function () {

	const getOrgInfoName = (dto) => {
		let orgInfo = '';
		if (dto.ORG_NAME && dto.USER_POSITION) {
			orgInfo = `(${dto.ORG_NAME}-${dto.USER_POSITION})`;
		} else if (dto.ORG_NAME && !dto.USER_POSITION) {
			orgInfo = `(${dto.ORG_NAME})`;
		} else if (!dto.ORG_NAME && dto.USER_POSITION) {
			orgInfo = `(${dto.USER_POSITION})`;
		}

		return orgInfo;
	};

	const getFriendName = (dto) => {
		if (isb2c()) {
			return dto.USER_NAME;
		} else {
			let orgInfo = getOrgInfoName(dto);
			return `${dto.USER_NAME} ${orgInfo}`;
		}
	};

	const getWelcomeViewText = () => {
		if (isb2c()) {
			return {
				title: '프렌즈를 초대해 보세요.',
				body: `프렌즈 추가 버튼을 눌러<br/>
						TeeSapce 아이디 또는 연락처로<br/>
						프렌즈를 찾을 수 있습니다.`
			};
		} else {
			return {
				title: '프렌즈 추가 버튼을 눌러<br/>내 동료를 찾아보세요!',
				body: `프렌즈가 되고 싶은 동료를 검색하거나<br/>
					   조직도에서 간편하게 추가할 수 있습니다.`
			};
		}
	};

	function FriendList() {
		const self = this;
		const collator = new Intl.Collator('kr', {numeric: true, sensitivity: 'base'});

		let welcomeView = null;

		this.model = {
			searching: mobx.observable.box(false),
			filters: mobx.observable.box(''),
			order: mobx.observable.box('name'),
			filteredUserList: mobx.observable.array([]),
		};
		this.lastSelectedNode = null;
		
		const userInfo = JSON.parse(sessionStorage.getItem('userInfo'));

		const container = this.Container();
		document.querySelector('div#friendListLayout').appendChild(container);

		this.initializeContextMenu();
		this.initializeOrderMenu();

		FriendListModel.fetch(userInfo.userId);

		/**
		 * Events
		 */
		container.querySelector('.friends-container').addEventListener('click', function (e) {
			e.preventDefault();
			e.stopPropagation();

			const node = e.target.closest('li');

			if (node) {
				const friend = node.data;
				if (e.target.closest('.new-window')) {
					spaceAPI.createSpace(null, userInfo.userId, 'WKS0002', [{USER_ID: friend.FRIEND_ID}], 'MINITALK');
				} else if (e.target.closest('.context-menu-btn')) {
					self.showContextMenu(friend, e.clientX, e.clientY);
				} else {
					_UserId = friend.FRIEND_ID;
					appManager.setSubApp(null);
					Top.App.routeTo(`/f/${friend.USER_LOGIN_ID}`, {eventType: 'close'});
					tds("AppSplitMainLayout").setProperties({ ratio: "1:0" });
					tds('AppSplitMainLayout').removeClass('limit');
					self.select(node); 
				}
			}

			const order = e.target.closest('span.ordering');
			if (order) {
				self.showOrderMenu(e.clientX, e.clientY);
			}
		});

		container.addEventListener('mouseover', function (e) {
			const status = e.target.closest('.status');
			if (status) {
				const rect = status.getBoundingClientRect(); 
				const x = rect.x + rect.width;
				const y = rect.y + rect.height;

				const text = status.classList.contains('active') ? '온라인' : '오프라인';

				TeeTooltip.open(text, {
					x: x, 
					y: y,
				});
			}
		});

		container.addEventListener('mouseout', function (e) {
			const status = e.target.closest('.status');
			if (status) {
				TeeTooltip.close();
			}
		});

		/**
		 * Utility
		 */
		const orderByName = (list) => {
			return list.sort((a, b) => collator.compare(a.USER_NAME, b.USER_NAME));
		};

		const orderByStatus = (list) => {
			let onlineFriendList = list.filter(v => v.USER_STATUS === 'online').sort((a, b) => collator.compare(a.USER_NAME, b.USER_NAME));
			let offlineFriendList = list.filter(v => v.USER_STATUS === 'offline').sort((a, b) => collator.compare(a.USER_NAME, b.USER_NAME));

			return onlineFriendList.concat(offlineFriendList);
		};

		// 프렌즈 리스트 로드가 완료된 경우 
		mobx.when(
			() => FriendListModel.isLoaded.get() === true,
			() => {
				// 검색 시
				mobx.reaction(
					() => this.model.filters.get(),
					filter => {
						if (filter.length > 0) {
							let filteredFriends = FriendListModel.list.filter(v => filter.length === 0 ? true : v.USER_NAME ? v.USER_NAME.indexOf(filter) > -1 : false);

							filteredFriends = orderByName(filteredFriends);

							this.model.filteredUserList.replace(filteredFriends);

							this.model.searching.set(true);
						} else {
							const order = this.model.order.get();

							let filteredFriends = [];
							if (order === 'name')  {
								filteredFriends = orderByName(FriendListModel.list);
							} else {
								filteredFriends = orderByStatus(FriendListModel.list);
							}

							this.model.filteredUserList.replace(filteredFriends);
							
							this.model.searching.set(false);
						}
					},
					{
						onError(e) {

						},
					}
				);

				// 정렬 변경 시
				mobx.reaction(
					() => this.model.order.get(),
					order => {
						if (order === 'name') {
							let filteredFriends = orderByName(FriendListModel.list);

							this.model.filteredUserList.replace(filteredFriends);
						} else if (order === 'status') {
							let filteredFriends = orderByStatus(FriendListModel.list);

							this.model.filteredUserList.replace(filteredFriends);
						}
					},
					{
						onError(e) {

						},
					}
				);

				// 친구 목록 변경 시 
				mobx.reaction(
					() => FriendListModel.list.slice(),
					friendList => {
						const filter = this.model.filters.get();

						if (filter.length > 0) {
							let filteredFriends = FriendListModel.list.filter(v => filter.length === 0 ? true : v.USER_NAME ? v.USER_NAME.indexOf(filter) > -1 : false);

							filteredFriends = orderByName(filteredFriends);

							this.model.filteredUserList.replace(filteredFriends);

							this.model.searching.set(true);
						} else {
							const order = this.model.order.get();

							let filteredFriends = [];
							if (order === 'name')  {
								filteredFriends = orderByName(FriendListModel.list);
							} else {
								filteredFriends = orderByStatus(FriendListModel.list);
							}

							this.model.filteredUserList.replace(filteredFriends);
							
							this.model.searching.set(false);
						}
					},
					{
						fireImmediately: true,
						onError(e) {

						},
					}
				);
			}
		);
		
		// 검색 상태
		mobx.reaction(
			() => this.model.searching.get(),
			searching => {
				if (searching === true) {
					container.querySelector('span.ordering').style.display = 'none';
				} else {
					container.querySelector('span.ordering').style.display = 'flex';
				}
			},
			{
				onError(e) {

				},
			}
		);

		// 리스팅
		mobx.reaction(
			() => this.model.filteredUserList.slice(),
			filteredFriends => {
				const favoriteFriendLength = filteredFriends.filter(v => v.FRIEND_FAV === 'FAV0001').length;

				if (container.querySelector('h3 span.highlight')) {
					container.querySelector('h3 span.highlight').textContent = filteredFriends.length;
				}
	
				if (favoriteFriendLength === 0 || this.model.searching.get() === true) {
					container.querySelector('.favorite-friend-list-container').style.display = 'none';
				} else {
					container.querySelector('.favorite-friend-list-container').style.display = 'block';
				}
	
				if (filteredFriends.length === 0) {
					container.querySelector('.friend-list-container').style.display = 'none';
				} else {
					container.querySelector('.friend-list-container').style.display = 'block';
				}
	
				if (container.querySelector('ul.friend-list')) {
					container.querySelector('ul.friend-list').innerHTML = '';
				}
	
				if (container.querySelector('ul.favorite-friend-list')) {
					container.querySelector('ul.favorite-friend-list').innerHTML = '';
				}
	
				if (container.querySelector('ul.me-list')) {
					container.querySelector('ul.me-list').innerHTML = '';
				}
				self.drawMe(container, userInfo);
	
				filteredFriends.map(friend => {
					const template = htmlToElement(` 
						<li>
							<div class="friend-container">
								<img />
								<span class="status"></span>
								<span class="name"></span>
								<top-icon class="icon-ellipsis_vertical_small context-menu-btn"></top-icon>
								<top-icon class="icon-deploy new-window"></top-icon>
							</div>
						</li>
					`);
					
					let THUMB_PHOTO = friend.THUMB_PHOTO;
					
					if (!THUMB_PHOTO || THUMB_PHOTO.length === 0) {
						THUMB_PHOTO = userManager.getUserDefaultPhotoURL(friend.FRIEND_ID);
					} else {
						THUMB_PHOTO = userManager.getUserPhoto(friend.FRIEND_ID, "small", friend.THUMB_PHOTO);
					}
	
					if (friend.USER_STATUS === 'online') {
						template.querySelector('span.status').classList.add('active');
					}
					
					template.querySelector('img').src = THUMB_PHOTO;
					template.querySelector('span.name').innerHTML = getFriendName(friend);
					template.data = friend;
	
					if (friend.FRIEND_FAV === 'FAV0001') {
						if (container.querySelector('ul.favorite-friend-list')) {
							const cloned = template.cloneNode(true);
							cloned.data = friend;
							container.querySelector('ul.favorite-friend-list').appendChild(cloned);
						}
					}
	
					if (container.querySelector('ul.friend-list')) {
						container.querySelector('ul.friend-list').appendChild(template);
					}
				});
			},
			{
				fireImmediately: true,
				onError(e) {

				},
			}
		);

		// 프렌즈 추가 유도 화면 제거 조건
		mobx.when(
			() => FriendListModel.list.length > 0 && FriendListModel.isLoaded.get(),
			() => {
				if (welcomeView) {
					welcomeView.hide();
					welcomeView = null;
				}
			},
			{
				onError(e) {

				}
			}
		);

		// 프렌즈 추가 유도 화면 표현 조건
		mobx.when(
			() => FriendListModel.list.length === 0 && FriendListModel.isLoaded.get(),
			() => {
				welcomeView = new WelcomeView({
					userName: userInfo.userName, 
					content: getWelcomeViewText(),
					bgUrl: 'res/illust/friend_add.png',
					bgStyle: {
						width: '13.63rem',
						height: '17.38rem',
						bottom: '2.33rem',
						backgroundPosition: 'bottom',
					},
					onCloseCallback: function () {
						container.querySelector('.container-bottom').classList.remove('active');
					},
				});

				welcomeView.drawTo(container);

				if (container.querySelector('.container-bottom')) {
					container.querySelector('.container-bottom').classList.add('active');
				}
			}
		)
	}

	FriendList.prototype.drawMe = function (container, userInfo) {
		const self = this;
		
		let THUMB_PHOTO = userInfo.thumbPhoto;
		if (!THUMB_PHOTO || THUMB_PHOTO.length === 0) {
			THUMB_PHOTO = userManager.getUserDefaultPhotoURL(userInfo.userId);
		} else {
			THUMB_PHOTO = userManager.getUserPhoto(userInfo.userId, "small", userInfo.thumbPhoto);
		}
		
		const template = htmlToElement(`
			<li class="me">
				<div class="friend-container">
					<div class="mark-me">나</div>
					<img />
					<span class="status active"></span>
					<span class="name"></span>
					<top-icon class="icon-deploy new-window"></top-icon>
				</div>
			</li>
		`);

		template.addEventListener('click', function (e) {
			e.preventDefault();
			e.stopPropagation();
			
			_UserId = userInfo.userId;
			appManager.setSubApp(null);
			Top.App.routeTo(`/f/${userInfo.USER_LOGIN_ID}`, {eventType: 'close'});
			tds("AppSplitMainLayout").setProperties({ ratio: "1:0" });
			tds('AppSplitMainLayout').removeClass('limit');
//		    ['close', 'fold', 'expand'].forEach( function(type){ tds("AppSplitMainLayout").removeClass(type) });
//		    tds("AppSplitMainLayout").addClass('close');
			self.select(template);
		});

		template.querySelector('.new-window').addEventListener('click', function (e) {
			e.preventDefault();
			e.stopPropagation();
			
			openMiniTalk(workspaceManager.getMySpaceUrl());
		});
		
		template.querySelector('img').src = THUMB_PHOTO;
		if (isb2c()) {
			template.querySelector('span.name').textContent = `${userInfo.userName}`;
		} else {
			let orgInfo = '';
			if (userInfo.orgName && userInfo.userPosition) {
				orgInfo = `(${userInfo.orgName}-${userInfo.userPosition})`;
			} else if (userInfo.orgName && !userInfo.userPosition) {
				orgInfo = `(${userInfo.orgName})`;
			} else if (!userInfo.orgName && userInfo.userPosition) {
				orgInfo = `(${userInfo.userPosition})`;
			}

			template.querySelector('span.name').innerHTML = `<span class="user-name">${userInfo.userName}</span><span class="user-org">${orgInfo}</span>`;
		}

		if (container.querySelector('ul.me-list')) {
			container.querySelector('ul.me-list').appendChild(template);
		}
	};

	FriendList.prototype.select = function (node) {
		if (this.lastSelectedNode) {
			this.lastSelectedNode.classList.remove('selected');
		}

		node.classList.add('selected');
		this.lastSelectedNode = node;
	};
	
	FriendList.prototype.Container = function () { 
		const self = this;

		const template = htmlToElement(`
			<div id="friend-list-container">
				<div class="friend-search tee-search">
					<top-icon class="icon-search tee-search-icon"></top-icon>
					<input type="text" placeholder="프렌즈 검색" class="tee-search-input" />
				</div>
				<div class="friends-container">
					<div class="me-container">
						<ul class="me-list">
						</ul>
					</div>
					<div class="favorite-friend-list-container">
						<h3>즐겨찾기</h3>
						<ul class="favorite-friend-list"></ul>
					</div>
					<div class="friend-list-container">
						<h3>
							프렌즈 <span class="highlight"></span>
							<span class="ordering">
								<svg width="20px" height="20px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
									<g>
										<path d="M14.5486494,4.84701297 C14.743397,4.65466965 15.056603,4.65466965 15.2513506,4.84701297 L15.2513506,4.84701297 L17.15,6.72222222 L18.7655,8.31777778 L18.7655,8.31777778 L18.7732366,8.3255144 C19.1150622,8.67161285 19.1115984,9.22928549 18.7655,9.57111111 C18.4136115,9.91865533 17.8478492,9.91920243 17.4952892,9.57233943 L17.4952892,9.57233943 L15.8,7.90444444 L15.8,15.6 C15.8,16.0970563 15.3970563,16.5 14.9,16.5 C14.4029437,16.5 14,16.0970563 14,15.6 L14,15.6 L14,7.90444444 L12.3047108,9.57233943 C11.9521508,9.91920243 11.3863885,9.91865533 11.0345,9.57111111 L11.0345,9.57111111 L11.0267634,9.56337449 C10.6849378,9.21727604 10.6884016,8.6596034 11.0345,8.31777778 L11.0345,8.31777778 Z M9,14 C9.55228475,14 10,14.4477153 10,15 C10,15.5522847 9.55228475,16 9,16 L2,16 C1.44771525,16 1,15.5522847 1,15 C1,14.4477153 1.44771525,14 2,14 L9,14 Z M9,9.5 C9.55228475,9.5 10,9.94771525 10,10.5 C10,11.0522847 9.55228475,11.5 9,11.5 L4,11.5 C3.44771525,11.5 3,11.0522847 3,10.5 C3,9.94771525 3.44771525,9.5 4,9.5 L9,9.5 Z M9,5 C9.55228475,5 10,5.44771525 10,6 C10,6.55228475 9.55228475,7 9,7 L6,7 C5.44771525,7 5,6.55228475 5,6 C5,5.44771525 5.44771525,5 6,5 L9,5 Z"></path>
										<line x1="16.1819805" y1="15.5" x2="16.1819805" y2="15.5" id="Line-14" stroke="#979797" stroke-linecap="square"></line>
									</g>
								</svg>
							</span>
						</h3>
						<ul class="friend-list"></ul>
					</div>
				</div>
				<div class="container-bottom">
					<button class="">
						<svg width="20px" height="20px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
							<g id="Icon/system/friend_add">
								<path d="M12.869499,16.0011702 L11,16 C10.9577582,16 10.9161282,15.9973809 10.8752635,15.9922961 L11,16 C10.9410187,16 10.88323,15.9948937 10.8270519,15.9850991 C10.8024826,15.9808142 10.77827,15.9756449 10.7543991,15.9696172 C10.7369745,15.9652161 10.7197904,15.9603759 10.7028005,15.9550948 C10.6821697,15.9486899 10.661507,15.9415126 10.6411647,15.9336896 C10.6230006,15.9266934 10.6055711,15.9193995 10.5884002,15.9116347 C10.5683874,15.9025948 10.5483651,15.8927225 10.5287345,15.8822141 C10.5139815,15.8743107 10.4997269,15.8662146 10.4856962,15.8577841 C10.4623757,15.8437886 10.439245,15.8285617 10.4168099,15.8124284 C10.407072,15.8054312 10.3971848,15.7980459 10.3874411,15.7904844 C10.2934828,15.717512 10.2131004,15.6284895 10.1502429,15.527433 C10.1415492,15.5134661 10.133034,15.4989879 10.1248788,15.4842824 C10.1144945,15.4655573 10.1046965,15.4464695 10.0955062,15.4270381 C10.092495,15.4206675 10.0896445,15.4144732 10.0868558,15.4082451 C10.0312053,15.2843242 10,15.1458063 10,15 C10,14.4477153 10.4477153,14 11,14 L14,14 L14,11 C14,10.4477153 14.4477153,10 15,10 C15.1183467,10 15.2318918,10.0205584 15.3372588,10.0582986 L15.4090934,10.0885549 L15.4090934,10.0885549 C15.5638296,10.1575293 15.694482,10.2622218 15.7940219,10.3920268 C15.7990789,10.39887 15.8040131,10.405463 15.808867,10.4121186 C15.8265096,10.4361069 15.8431437,10.4612357 15.8586402,10.4871237 C15.8667768,10.5008451 15.8746864,10.5147912 15.8822649,10.5289418 C15.8927225,10.5483651 15.9025948,10.5683874 15.9118035,10.5887737 C15.9195415,10.6059672 15.9268231,10.6233763 15.9336181,10.6410279 C15.9410178,10.6602203 15.9477793,10.6795569 15.9539537,10.6991541 C15.9596734,10.7173223 15.9649298,10.7358431 15.9696573,10.7545748 C15.9756449,10.77827 15.9808142,10.8024826 15.9850904,10.8270019 C15.9879131,10.8432119 15.9903194,10.8593388 15.9923359,10.8755867 C15.9973809,10.9161282 16,10.9577582 16,11 L16,14 L19,14 C19.5522847,14 20,14.4477153 20,15 C20,15.5522847 19.5522847,16 19,16 L16,16 L16,19 C16,19.5522847 15.5522847,20 15,20 C14.4477153,20 14,19.5522847 14,19 L14,16 L12.8715258,16.000047 C12.8708502,16.0004215 12.8701746,16.0007959 12.869499,16.0011702 Z M8.992,1 C13.416,1 17,4.584 17,9 C17,9.45651371 16.9616983,9.90413597 16.8881265,10.3398264 C16.6691384,9.71339344 16.1481559,9.22983946 15.5000192,9.06301369 L15.5,9 C15.5,5.41142231 12.586567,2.5 8.992,2.5 C5.40641646,2.5 2.5,5.41043639 2.5,9 C2.5,12.5895636 5.40641646,15.5 8.992,15.5 L9.06272595,15.4989 C9.22925654,16.1474536 9.71288048,16.6688475 10.3385468,16.8880307 C9.90148513,16.9613747 9.45126897,17 8.992,17 C4.576,17 1,13.416 1,9 C1,4.584 4.576,1 8.992,1 Z M11.8805564,8.33308474 C12.1101752,8.6650698 11.9987842,9.1025679 11.6317578,9.3102641 C10.7985334,9.78177661 9.59224791,10.2777778 8,10.2777778 C6.40775209,10.2777778 5.20146663,9.78177661 4.36824218,9.3102641 C4.00121576,9.1025679 3.88982483,8.6650698 4.11944357,8.33308474 C4.34906232,8.00109969 4.83273879,7.90034366 5.19976521,8.10803985 C5.73734129,8.41224822 6.67505337,8.85966305 8,8.85966305 C9.32494663,8.85966305 10.2626587,8.41224822 10.8002348,8.10803985 C11.1672612,7.90034366 11.6509377,8.00109969 11.8805564,8.33308474 Z M6,5 C6.55228475,5 7,5.44771525 7,6 C7,6.55228475 6.55228475,7 6,7 C5.44771525,7 5,6.55228475 5,6 C5,5.44771525 5.44771525,5 6,5 Z M10,5 C10.5522847,5 11,5.44771525 11,6 C11,6.55228475 10.5522847,7 10,7 C9.44771525,7 9,6.55228475 9,6 C9,5.44771525 9.44771525,5 10,5 Z"></path>
							</g> 
						</svg>
						프렌즈 추가
					</button>
				</div>
			</div>
		`);

		InitializeTeeSearch(template.querySelector('.friend-search'));
		
		template.querySelector('input').addEventListener('keyup', function (e) {
			if (e.keyCode == 13) {
				const str = e.target.value.trim();
				self.model.filters.set(str);
        	}
		});
		
		template.querySelector('button').addEventListener('click', function (e) {
			AddFriendsDialog.open();
		});
		
		return template;
	}

	FriendList.prototype.initializeContextMenu = function () {
		const contextMenu = new ContextMenu();
		const userInfo = JSON.parse(sessionStorage.getItem('userInfo'));
		contextMenu.addItem('즐겨찾기', function(e) {
			const data = contextMenu.getItemData(0);
			let req = null;
			if (data && data.FRIEND_FAV === 'FAV0001') {
				req = FriendAPI.UnsetFavoriteFriend(userInfo.userId, data.FRIEND_ID);
			} else if (data) {
				req = FriendAPI.SetFavoriteFriend(userInfo.userId, data.FRIEND_ID);
			}

			if (req) {
				req.finally(() => {
					FriendListModel.fetch(userInfo.userId);

					let profileController = null;
					if (isb2c()) {
						profileController = Top.Controller.get('spaceOtherProfileLayoutLogic');
					} else {
						profileController = Top.Controller.get('spaceBtobOtherProfileLayoutLogic');
					}

					let toastText = '';
					if (data.FRIEND_FAV === 'FAV0001') {
						toastText = '즐겨찾기가 해제되었습니다';

						try {
							profileController.setFavoriteStatus('off');
						} catch(e) {}
					} else {
						toastText = '즐겨찾기가 설정되었습니다';

						try {
							profileController.setFavoriteStatus('on');
						} catch(e) {}
					}

					TeeToast.open({
						text: toastText
					});
				});
			}
			
			contextMenu.hide();
		});

		contextMenu.addItem('프렌즈 삭제', function(e) {
			const data = contextMenu.getItemData(1);
			
			const onDeleteClicked = (e) => {
				FriendAPI.DeleteFriend(userInfo.userId, data.FRIEND_ID)
					.finally(() => {
						FriendListModel.remove(data.FRIEND_ID);
						TeeAlarm.close();
					});
			};

			let title = '';
			if (isb2c()) {
				title = `${data.USER_NAME}(${data.USER_LOGIN_ID})님을 프렌즈 목록에서 삭제 하시겠습니까?`;
			} else {
				const orgInfo = getOrgInfoName(data);
				title = `${data.USER_NAME}${orgInfo}님을 프렌즈 목록에서 삭제 하시겠습니까?`;
			}

			TeeAlarm.open({
				title: title,
				buttons: [
					{
						text: '삭제',
						onClicked: onDeleteClicked,
					}
				]
			});

			contextMenu.hide();
		});

		this.contextMenu = contextMenu;

		document.body.appendChild(contextMenu.containerElem);
	};

	FriendList.prototype.initializeOrderMenu = function () {
		const self = this;

		const contextMenu = new ContextMenu({mode: 'select'});
		contextMenu.addItem('이름순', function(e) {
			self.model.order.set('name');
			contextMenu.hide();
		});

		contextMenu.addItem('접속 상태순', function(e) {
			self.model.order.set('status');
			contextMenu.hide();
		});

		this.orderMenu = contextMenu;

		document.body.appendChild(contextMenu.containerElem);
	};

	FriendList.prototype.showOrderMenu = function (x, y) {
		this.orderMenu.show(x - 6, y - 6);
	};

	FriendList.prototype.showContextMenu = function (user, x, y) {
		if (user.FRIEND_FAV === 'FAV0001') {
			this.contextMenu.updateItem(0,'즐겨찾기 해제');
		} else {
			this.contextMenu.updateItem(0,'즐겨찾기');
			
		}

		this.contextMenu.updateItemData(0, user);
		this.contextMenu.updateItemData(1, user);
		this.contextMenu.show(x - 6, y - 6);
	};

	let instance = null;

	FriendList.show = function () {
		instance = new FriendList();
	};
	
	return FriendList;
}();

/**
 * TOP Controller
 */
Top.Controller.create('FriendListLogic', {
	init : function(event, widget){
		FriendListView.show();
	}
});
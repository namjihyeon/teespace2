// 회원가입 페이지
Top.Controller.create('signupBaseLogic', {
	init: function(event,widget){
		Top.Dom.selectById("LinearLayout434").src('signupFirst.html');
		Top.Dom.selectById("TextView11").setProperties({"visible": "none"});
		Top.Dom.selectById('LinearLayout434').setProperties({"border-color" : "rgba(198,206,214,1) rgba(198,206,214,1) rgba(198,206,214,1) rgba(198,206,214,1)"})
		Top.Dom.selectById('LinearLayout434').setProperties({"border-width" : "1px 1px 1px 1px"});
		Top.Dom.selectById('LinearLayout5').setProperties({"border-color" : "rgba(0,0,0,1.0) rgba(0,0,0,1.0) rgba(0,0,0,1.0) rgba(0,0,0,1.0)"})
		Top.Dom.selectById('LinearLayout5').setProperties({"border-width" : "0px 0px 0px 0px"});	
		// Controller parameter
		this.idregi = '';
		this.pwregi = '';
		this.nmregi = '';
		this.cklist = [0,0,0,0,0,0];
		this.usertype = '내국인';
		this.adokay = '';
		this.personal = '';
		this.answer = '';
		this.chkboxRem = [0,0,0,0,0,0,0,0,0];
		this.nameTextRem= ''
		this.birthTextRem = '';
		this.idTextRem = '';
		this.mailTextRem= '';
		this.sexTextRem= '';
		this.jobTextRem= '';
		this.groupTextRem= '';
		this.areaTextRem= '';
		this.addressTextRem= ''
		this.fourteenCheck = 0;
	}
});
Top.Controller.create('signupFirstLogic', {
	init: function(event,widget){
		//텍스트뷰 css
		$('#TextView11 .top-textview-root').css({"opacity":"0.6"})
		$('#TextView730_1 .top-textview-root').css({"opacity":"0.6"})
		$('#TextView730 .top-textview-root').css({"opacity":"0.6"})
		
		if(Top.Controller.get('signupBaseLogic').adokay == "agree"){
			Top.Dom.selectById("CheckBox744").setChecked(true)
		}else{
			Top.Dom.selectById("CheckBox744").setChecked(false)
		}
		if(Top.Controller.get('signupBaseLogic').personal == "agree"){
			Top.Dom.selectById("CheckBox744_1").setChecked(true)
		}else{
			Top.Dom.selectById("CheckBox744_1").setChecked(false)
		}
		if (Top.Controller.get('signupBaseLogic').cklist[1] == 1){
			Top.Dom.selectById("CheckBox623").setChecked(true)
			Top.Dom.selectById("CheckBox722").setChecked(true)
			Top.Dom.selectById("Button806").setProperties({"disabled":"false"});
			tds('Button806').removeClass("signup-disabled")
			tds('Button806').addClass("signup-solid")
		}		
		if(Top.Controller.get("signupBaseLogic").fourteenCheck){
			Top.Dom.selectById("CheckBox_age_14").setChecked(true);
		}
		
	}, 
	gotosecond : function(event, widget) {
		//Top.Dom.selectById("LinearLayout434").src('signupThird.html'+verCsp());
		Top.Dom.selectById("LinearLayout434").src('signupSecond.html');
		
		if(Top.Dom.selectById("CheckBox744").isChecked()){
			Top.Controller.get('signupBaseLogic').adokay = "agree"
		}else{
			Top.Controller.get('signupBaseLogic').adokay = "disagree"
		}
		if(Top.Dom.selectById("CheckBox744_1").isChecked()){
			Top.Controller.get('signupBaseLogic').personal = "agree"
		}else{
			Top.Controller.get('signupBaseLogic').personal = "disagree"
		}
		if(Top.Dom.selectById('CheckBox_age_14').isChecked()){
			Top.Controller.get('signupBaseLogic').fourteenCheck = 1;
		}
		else{
			Top.Controller.get('signupBaseLogic').fourteenCheck = 0;
		}
		Top.Controller.get('signupBaseLogic').cklist[1] = 1	
	}, 
	checkboxx : function(event, widget) {
		if(widget.id == "CheckBox623"){
			if(Top.Dom.selectById("CheckBox722").isChecked() && !Top.Dom.selectById("CheckBox623").isChecked() && Top.Dom.selectById("CheckBox_age_14").isChecked()){
				Top.Dom.selectById("Button806").setProperties({"disabled":"false"});
				tds('Button806').removeClass("signup-disabled")
				tds('Button806').addClass("signup-solid")
			}else{
				Top.Dom.selectById("Button806").setProperties({"disabled":"true"});
				tds('Button806').removeClass("signup-solid")
				tds('Button806').addClass("signup-disabled")
				Top.Dom.selectById("AllTermCheckBox").setChecked(false)
			}
		}
		else if(widget.id == "CheckBox_age_14"){
			if(Top.Dom.selectById("CheckBox722").isChecked() && Top.Dom.selectById("CheckBox623").isChecked() && !Top.Dom.selectById("CheckBox_age_14").isChecked()){
				Top.Dom.selectById("Button806").setProperties({"disabled":"false"});
				tds('Button806').removeClass("signup-disabled")
				tds('Button806').addClass("signup-solid")
			}else{
				Top.Dom.selectById("Button806").setProperties({"disabled":"true"});
				tds('Button806').removeClass("signup-solid")
				tds('Button806').addClass("signup-disabled")
				Top.Dom.selectById("AllTermCheckBox").setChecked(false)
			}	
		}	
		else{
			if(!Top.Dom.selectById("CheckBox722").isChecked() && Top.Dom.selectById("CheckBox623").isChecked() && Top.Dom.selectById("CheckBox_age_14").isChecked()){
				Top.Dom.selectById("Button806").setProperties({"disabled":"false"});
				tds('Button806').removeClass("signup-disabled")
				tds('Button806').addClass("signup-solid")
			}else{
				Top.Dom.selectById("Button806").setProperties({"disabled":"true"});
				tds('Button806').removeClass("signup-solid")
				tds('Button806').addClass("signup-disabled")
				Top.Dom.selectById("AllTermCheckBox").setChecked(false)
			}
		}
	}, 
	alltermcheck : function(event, widget) {
		if(!Top.Dom.selectById("AllTermCheckBox").isChecked()){
			Top.Dom.selectById("CheckBox623").setChecked(true)
			Top.Dom.selectById("CheckBox722").setChecked(true)
			Top.Dom.selectById("CheckBox744").setChecked(true)
			Top.Dom.selectById("CheckBox744_1").setChecked(true)
			Top.Dom.selectById('CheckBox_age_14').setChecked(true)
			Top.Dom.selectById("Button806").setProperties({"disabled":"false"});
			tds('Button806').removeClass("signup-disabled")
			tds('Button806').addClass("signup-solid")			
		}else{
			Top.Dom.selectById("CheckBox623").setChecked(false)
			Top.Dom.selectById("CheckBox722").setChecked(false)
			Top.Dom.selectById("CheckBox744").setChecked(false)
			Top.Dom.selectById("CheckBox744_1").setChecked(false)
			Top.Dom.selectById('CheckBox_age_14').setChecked(false)
			Top.Dom.selectById("Button806").setProperties({"disabled":"true"});
			tds('Button806').removeClass("signup-solid")
			tds('Button806').addClass("signup-disabled")
		}
	}, 
	allcheckoff : function(event, widget) {
		let wg = widget.id;
		if(Top.Dom.selectById(wg).getChecked()){
			Top.Dom.selectById("AllTermCheckBox").setChecked(false)
		}
	}
	
});



Top.Controller.create('signupSecondLogic', {
	init: function(event,widget){
		
		Top.Dom.selectById("signupDomainSelectBox").setProperties({"nodes":"signupDomains"})
		Top.Dom.selectById("signupDomainSelectBox").select("직접입력")
		//국제전화번호
		const data = COUNTRY_CODES.map(value => {
			return {
				text: `${value.code} ${value.name}`,
				value: value.code
			}
		});
		tds("NationalMobileSelectBox").setProperties({"nodes":data})
		tds("NationalMobileSelectBox").select("+82")

		if(Top.Controller.get('signupBaseLogic').nameTextRem){
			Top.Dom.selectById("TextField2012").setText(Top.Controller.get('signupBaseLogic').nameTextRem)
			this.nametypeck();
		}else{
		}
		if(Top.Controller.get('signupBaseLogic').idTextRem){
			Top.Dom.selectById("TextField20123").setText(Top.Controller.get('signupBaseLogic').idTextRem)
			this.idlogic();
		}else{
		}
	
		Top.Dom.selectById("TextView161").setProperties({"visible": "visible"});
		Top.Dom.selectById("TextView57").setProperties({"visible": "visible"});

////////////////////////외부메일 초대일 경우
		if(Top.Controller.get('signupBaseLogic').isExternal == true){
		  Top.Dom.selectById("TextField2012_6").setText(Top.Controller.get('signupBaseLogic').inviteMail);
		  Top.Dom.selectById("TextField2012_6").setProperties({"readonly":"true"})
		  Top.Controller.get('signupBaseLogic').cklist[4] = Top.Controller.get('signupBaseLogic').inviteMail;
		}
	},
	gotosecond : function(event, widget) {
		//Top.Dom.selectById("LinearLayout434").src('signupSecond.html'+verCsp());
		Top.Dom.selectById("LinearLayout434").src('signupFirst.html');
		Top.Dom.selectById("TextView161").setProperties({"visible": "none"});
		Top.Dom.selectById("TextView57").setProperties({"visible": "none"});
		
		if(Top.Dom.selectById("TextField2012").getText() !== ""){
			Top.Controller.get('signupBaseLogic').nameTextRem = Top.Dom.selectById("TextField2012").getText()
		}else{
			Top.Controller.get('signupBaseLogic').nameTextRem = Top.Dom.selectById("TextField2012").getText("");
		}
		if(Top.Dom.selectById("TextField20123").getText() !== ""){
			Top.Controller.get('signupBaseLogic').idTextRem = Top.Dom.selectById("TextField20123").getText()
		}else{
			Top.Controller.get('signupBaseLogic').idTextRem = Top.Dom.selectById("TextField20123").getText("");
		}
		if(Top.Dom.selectById("TextField2012_6").getText() !== ""){
			Top.Controller.get('signupBaseLogic').mailTextRem = Top.Dom.selectById("TextField2012_6").getText()
		}else{
			Top.Controller.get('signupBaseLogic').mailTextRem = Top.Dom.selectById("TextField2012_6").getText("");
		}
	},
	idcheck: function(event,widget){
		Top.Controller.get("signupBaseLogic").idregi = Top.Dom.selectById("TextField20123").getText();
		let idregi = Top.Dom.selectById("TextField20123").getText();
		$.ajax({
	        url: _workspace.url+'Users/IDCheck?action=SO', //Service Object
	        type: 'POST',
	        dataType: 'json',
	        crossDomain: true,
	        contentType : "application/json",
	        xhrFields: {

	            withCredentials: true

	          },
	        data: JSON.stringify({
	          "dto": {
	        	"USER_LOGIN_ID": idregi,            
	          }
	        }),
	        success: function(result) {
	        	if(result.dto.USER_PW == "RST0001"){
	        		// 아이디 중복됨
	        		
	        		Top.Dom.selectById("TextField20123").addClass("alert");
	        		Top.Dom.selectById("Icon832_1_2").setProperties({"visible":"visible"});
	        		Top.Dom.selectById("Icon458_1_2").setVisible("none");
	        		setTimeout(function(){
						Top.Dom.selectById("SameIdPopover").open()		
					}, 100);	
	        	}else{
	        		//아이디 중복안됨
	        		Top.Dom.selectById("TextField20123").removeClass("alert")
	        
	    			Top.Dom.selectById("Icon832_1_2").setProperties({"visible":"none"});
	    			Top.Dom.selectById("Icon458_1_2").setVisible("visible");
	    			Top.Controller.get('signupBaseLogic').cklist[2] = Top.Dom.selectById("TextField20123").getText();;
	        	}
	        },
	        error: function(error) {
	        }
		})
	},
	//이메일 중복체크
	mailauth:function(event,widget){
		let mail = Top.Dom.selectById("TextField2012_6").getText()+"@"+Top.Dom.selectById('signupDomainTextField').getText()
		$.ajax({
	        url: _workspace.url+"Users/EmailDuplication", //Service Object
	        type: 'GET',
	        dataType: 'json',
	        crossDomain: true,
	        contentType : "application/json",
	        xhrFields: {
	
	            withCredentials: true
	
	          },
	          data: JSON.stringify({
		          "dto": {
		            "USER_EMAIL" : mail
		          }
		        }),
	        success: function(result) {
	        	if(result.dto.USER_ID == "해당 이메일은 존재합니다."){
	        		Top.Dom.selectById("Icon458_1_5").setProperties({"visible":"none"})
	        		Top.Dom.selectById("Icon832_1_5").setProperties({"visible":"visible"})
	        	
	        		Top.Dom.selectById("TextField2012_6").addClass("alert")
	        		setTimeout(function(){
	        			Top.Dom.selectById("EmailSameErrorPopover").open()		
	        		} , 100);
	        		
	        		return false;
	        	}else if(result.dto.USER_ID == "해당 이메일은 등록되지 않았습니다."){
	        		Top.Dom.selectById("Icon832_1_5").setProperties({"visible":"none"})
	        		Top.Dom.selectById("Icon458_1_5").setProperties({"visible":"visible"})
	        		Top.Dom.selectById("TextField2012_6").removeClass("alert")
	        		tds('signupDomainTextField').removeClass("alert")
	     
	        	    Top.Controller.get('signupBaseLogic').cklist[4] = mail;
	        	}	        	
	        },
	        error: function(error) {
	        }
      });
	},
	register:function(event,widget){
		if(!_mailDomain){
			Top.Ajax.execute({
		        url: _workspace.url + "Users/SysContext",
		        type: 'GET',
		        dataType: "json",
		        cache: false,
		        data: JSON.stringify({
		            "dto": {
		                "KEY": "MAIL_URL"
		            }
		        }),
		        contentType: "application/json; charset=utf-8",
		        crossDomain: true,
		        xhrFields: {
		            withCredentials: true
		        },
		        headers: {
		            "ProObjectWebFileTransfer": "true"
		        },
		        success: function (result) {
		            _mailDomain = result.dto.VALUE
		        },
		        error: function(error){
		        	hideLoadingBar();
		        	return;
		        }
		    });
		}
		Top.Controller.get("signupBaseLogic").idregi = Top.Dom.selectById("TextField20123").getText();
		let idregi = Top.Dom.selectById("TextField20123").getText();
		Top.Controller.get("signupBaseLogic").nmregi = Top.Dom.selectById("TextField2012").getText();
		let nmregi = Top.Dom.selectById("TextField2012").getText();
		Top.Controller.get("signupBaseLogic").pwregi = Top.Dom.selectById("TextField20124").getText();
		let pwregi = Top.Dom.selectById("TextField20124").getText();
		let pwregick = Top.Dom.selectById("TextField2012_5").getText();
		let mail = Top.Dom.selectById("TextField2012_6").getText() + "@"+Top.Dom.selectById("signupDomainTextField").getText() ;
		//전화번호 추가
		let PhoneNum = tds('signupMobileTextField').getText()
		let etc = new Object();
		etc.usertype = Top.Controller.get('signupBaseLogic').usertype;
		etc.adokay = Top.Controller.get('signupBaseLogic').adokay;
		etc.personal = Top.Controller.get('signupBaseLogic').personal;
		let etcs = JSON.stringify(etc);
//		let answer = Top.Controller.get('signupBaseLogic').answer;
		if(idregi != Top.Controller.get('signupBaseLogic').cklist[2]){
			 try {
                   var msg = '아이디 중복 확인이 필요합니다.';
                   notiFeedback(msg);
            } catch (e) {}
			Top.Dom.selectById("TextField20123").focus();
			tds()
			return false
		}
		if(mail != Top.Controller.get('signupBaseLogic').cklist[4] || Top.Controller.get('signupBaseLogic').cklist[4] === null){
			try {
				var msg = '이메일 중복 확인이 필요합니다.';
				notiFeedback(msg);
            } catch (e) {}
			Top.Dom.selectById("TextField2012_6").focus();
			return false
		}
    	//오류처리용
    	if (nmregi == "" || nmregi == undefined){
    		// 이름이 없음
    		Top.Dom.selectById("TextField2012").setProperties({"border-color":"red"});
    		Top.Dom.selectById("Icon832_1").setProperties({"visible":"visible","tooltip-top":"이름을 입력해 주세요."});
    		Top.Dom.selectById("Icon458_1").setVisible("none");
    		Top.Dom.selectById("TextField2012").focus();
    		//조상호    이름	
    		hideLoadingBar();
			return false;
		}
    	else if(!is_henValid(nmregi)){
    		Top.Dom.selectById("TextField2012").setProperties({"border-color":"red"});
    		Top.Dom.selectById("Icon832_1").setProperties({"visible":"visible","tooltip-top":"특수 문자는 입력할 수 없습니다."});
    		Top.Dom.selectById("Icon458_1").setVisible("none");
    		Top.Dom.selectById("TextField2012").focus();
    		//조상호    이름	
    		hideLoadingBar();
			return false;
    	}
    	else{
			//이름을 입력함++
			Top.Dom.selectById("TextField2012").setProperties({"border-color":"#d3dbdf"});
			Top.Dom.selectById("Icon832_1").setProperties({"visible":"none"});
			Top.Dom.selectById("Icon458_1").setVisible("visible");
			
		} // 이름 미입력 체크
    	if(pwregi.length<9 || pwregi.length>20){
    		
    		Top.Dom.selectById("TextField20124").addClass("alert")
    		Top.Dom.selectById("Icon832_1_3").setProperties({"visible":"visible"});
    		Top.Dom.selectById("Icon892_1_3").setProperties({"visible":"none"});
    	
    		setTimeout(function(){
				Top.Dom.selectById("PwdLengthErrorPopover").open()						
			}, 100);	
    		Top.Dom.selectById("TextField20124").focus();
    		return false;
    	}else{		
			let a = 0;
			if (pattern_engb.test(pwregi)){a++;}
			if (pattern_engs.test(pwregi)){a++;}
			if (pattern_num.test(pwregi)){a++;}
			if (pattern_spc2.test(pwregi)){a++;}
			if (a>2){
				Top.Dom.selectById("TextField20124").removeClass("alert")

				Top.Dom.selectById("Icon832_1_3").setProperties({"visible":"none"});
				Top.Dom.selectById("Icon892_1_3").setProperties({"visible":"none"});
				
			}else{
				//조상호    비밀번호 영문특문숫자 모두 포함 안됨	
				
	    		Top.Dom.selectById("TextField20124").addClass("alert")
	    		Top.Dom.selectById("Icon832_1_3").setProperties({"visible":"none"});
	    		Top.Dom.selectById("Icon892_1_3").setProperties({"visible":"visible"});
	    	
	    		setTimeout(function(){
					Top.Dom.selectById("PwdValidErrorPopover").open()
				}, 100);	
	    		Top.Dom.selectById("TextField20124").focus();
	    		return false;
			}
    	}
    	
    	
    	if(pwregi != pwregick){
    		
    		Top.Dom.selectById("TextField2012_5").addClass("alert")
    		Top.Dom.selectById("Icon832_1_4").setProperties({"visible":"visible"});
    		
    		Top.Dom.selectById("TextField2012_5").focus();
    		setTimeout(function(){
    			Top.Dom.selectById("DifferentPasswordErrorPopover").open()
    			
    		} , 100);
    		// 비밀번호 확인과 다름
    		hideLoadingBar();
    		return false;
    	}else{
    		Top.Dom.selectById("TextField2012_5").removeClass("alert")
    
			Top.Dom.selectById("Icon832_1_4").setProperties({"visible":"none"});
		
    	}
    	//휴대폰 번호 한번더 검사
    	if(tds('signupMobileTextField').getText() === null || tds('signupMobileTextField').getText().length === 0 ){
    		tds('signupMobileTextField').addClass('alert')
    		tds('signupMobileTextField').removeClass('signup')
    		tds('Icon892_1_6').setProperties({"visible":"visible"})
    		tds('signupMobileTextField').focus();
    		return false;
    	}
    	    	// 가입 경로 저장
    	let pathf = location.href.split('/');
    	let path = pathf[pathf.length-1];
    	if(path == "login" || path == "register")path = "Csp";
    	else if(path == "TmaxOS") path = "TmaxOS";
    	else if(path == "TmaxOSSC") path = "TmaxOSSC";
    	else path = "Csp";
    	let ivt;
		if(Top.Controller.get("loginLayoutLogic").inviter){
			ivt = "invite"
		}else{
			ivt = "no"
		}
//////////////////////////외부 메일 초대 계정 등록
    	if(Top.Controller.get('signupBaseLogic').isExternal == true){
    	  $.ajax({
    	        url: _workspace.url+"Users/User?action=Create", //Service Object
    	        type: 'POST',
    	        dataType: 'json',
    	        crossDomain: true,
    	        contentType : "application/json",
    	        xhrFields: {
    	            withCredentials: true
    	          },
    	          data: JSON.stringify({
    	            "dto": {
    	              "USER_LOGIN_ID" : idregi.trim(),
    	              "USER_PW" : SHA256(idregi.trim()+pwregi.trim()),
    	              "USER_NAME" : nmregi.trim(),
    	              "USER_TYPE" : "USR0002",
    	              "USER_EMAIL" : mail,    	              //"USER_ADDRESS" : address,
    	              "USER_BIRTH" : birth,
    	              "USER_ETC" : etcs,
    	              "USER_DOMAIN" : _mailDomain,
    	              "REGI_PATH" : "Csp"
    	            }
    	          }),
    	        success: function(result) {
    	           
    	        $.ajax({
    	              url: _workspace.url+"Users/Loginn?action=SO", //Service Object
    	              type: 'GET',
    	              dataType: 'json',
    	              crossDomain: true,
    	              contentType : "application/json",
    	              xhrFields: {
    	                  withCredentials: true
    	                },
    	                data: JSON.stringify({
    	                  "dto": {
    	                    "USER_LOGIN_ID" : idregi.trim(),
    	                    "USER_PW" : "1"
    	                  }
    	                }),
    	              success: function(result) { 	                                                                	                
    	                
    	              $.ajax({
    	                    url: _workspace.url+"WorkSpace/WorkspaceExtUser?action=Invite", //Service Object
    	                    type: 'POST',
    	                    dataType: 'json',
    	                    crossDomain: true,
    	                    contentType : "application/json",
    	                    xhrFields: {
    	                        withCredentials: true
    	                      },
    	                      data: JSON.stringify({
    	                        "dto": {
    	                          "wsUserList":[
    	                            {
    	                              "USER_ID": result.dto.USER_ID, 
    	                              "WS_ID" : Top.Controller.get('signupBaseLogic').wsId
    	                            }
    	                          ]
    	                        }
    	                      }),
    	                    success: function(result) {
    	                      
//    	                      Top.Dom.selectById("LinearLayout_signup_1_base_1").src("signupThird.html"+verCsp())
    	                    	Top.Dom.selectById("LinearLayout_signup_1_base_1").src("signupThird.html")
    	                      Top.Controller.get('signupBaseLogic').isExternal == false
    	                      Top.Controller.get('signupBaseLogic').wsId == ""
    	                      Top.Controller.get('signupBaseLogic').inviteMail == ""
    	                    },
    	                    error: function(error) {
    	                      alert("error-ExtUserInvite")
    	                    }
    	                  });
    	              },
    	              error: function(error) {
    	                alert("error-LoginnSO")
    	              }
    	            });
    	        },
    	        error: function(error) {
    	          alert("회원가입에 실패했습니다.")
    	        }
    	      });

    	}
    	////////////////////////////
    	else{
		$.ajax({
	        url: _workspace.url+"Users/User?action=Create", //Service Object
	        type: 'POST',
	        dataType: 'json',
	        crossDomain: true,
	        contentType : "application/json",
	        xhrFields: {
	            withCredentials: true
	          },
	          data: JSON.stringify({
		          "dto": {
		            "USER_LOGIN_ID" : idregi.trim(),
		            "USER_PW" : SHA256(idregi.trim()+pwregi.trim()),
		            "USER_NAME" : nmregi.trim(),
		            "USER_TYPE" : "USR0002",
		            "USER_EMAIL" : mail,		      //      "USER_ADDRESS" : address,
//		            "USER_BIRTH" : birth,
		            "NATIONAL_CODE" : tds("NationalMobileSelectBox").getSelected().value,
		            "USER_PHONE" : PhoneNum,
		            "USER_ETC" : etcs,
  	              "USER_DOMAIN" : _mailDomain,
	              "REGI_PATH" : path,
	              "RECOMMEND_MEM" : Top.Controller.get("loginLayoutLogic").inviter,
	              "OAUTH_UUID" : ivt
		          }
		        }),
	        success: function(result) {
	        	if(result.dto.USER_ID === "" || result.dto.USER_ID === null){
	        		try {
		        		var msg = '회원가입에 실패했습니다.';
						notiFeedback(msg);
		        	} catch (e) {}
	        	}else{
//	        	Top.Dom.selectById("LinearLayout_signup_1_base_1").src("signupThird.html"+verCsp())	
	        	Top.Dom.selectById("LinearLayout_signup_1_base_1").src("signupThird.html")
	        	Top.Controller.get("loginLayoutLogic").inviter = "";
	        	ivtCode = "";
	        	}
	        },
	        error: function(error) {
	        	try {
	        		var msg = '회원가입에 실패했습니다.';
					notiFeedback(msg);
	        	} catch (e) {}
	        }
	      });
    	}
	},
	//focusOut
	idlogic : function(event,widget){
		//이부분 고민할것
		if(Top.Dom.selectById("TextField2012").getText() && Top.Dom.selectById("TextField20123").getText() && Top.Dom.selectById("TextField20124").getText() && Top.Dom.selectById("TextField2012_5").getText() && Top.Dom.selectById("TextField2012_6").getText() && Top.Dom.selectById("signupDomainTextField").getText() && tds('signupMobileTextField').getText()){
			Top.Dom.selectById("Button2970").setProperties({"disabled": "false"});
			tds('Button2970').removeClass('signup-disabled')
			tds('Button2970').addClass('signup-solid')
		}else{
			Top.Dom.selectById("Button2970").setProperties({"disabled": "true"});
			tds('Button2970').removeClass('signup-solid')
			tds('Button2970').addClass('signup-disabled')
		}
		if(tds('TextField20123').getText() === null || tds('TextField20123').getText().length === 0){
			tds('TextField20123').removeClass('signup')
			tds('TextField20123').addClass('alert')
			tds('IdNullIcon').setProperties({"visible":"visible"})
			tds('Icon832_1_2').setProperties({"visible":"none"})
			tds('Icon458_1_2').setProperties({"visible":"none"})
			tds('IdLengthErrorIcon').setProperties({"visible":"none"})
			tds('IdValidErrorIcon').setProperties({"visible":"none"})
			setTimeout(function(){
				tds('LoginIdErrorPopover').open();
			},100)
		}else{
				if(tds('TextField20123').getText().length< 5 || tds('TextField20123').getText().length >20){
					tds('TextField20123').removeClass('signup')
					tds('TextField20123').addClass('alert')
					tds('IdNullIcon').setProperties({"visible":"none"})
					tds('Icon832_1_2').setProperties({"visible":"none"})
					tds('Icon458_1_2').setProperties({"visible":"none"})
					tds('IdLengthErrorIcon').setProperties({"visible":"visible"})
					tds('IdValidErrorIcon').setProperties({"visible":"none"})
					setTimeout(function(){
						tds('IdLengthErrorPopover').open();
					},100)
				}else{
					if (!(pattern_engb.test(tds('TextField20123').getText()))&& !(pattern_spc.test(tds('TextField20123').getText()))&& !(pattern_kor.test(tds('TextField20123').getText())) && !(pattern_blank.test(tds('TextField20123').getText())) ){
						//정상일때
						tds('TextField20123').removeClass('alert')
						tds('TextField20123').addClass('signup')
						tds('IdNullIcon').setProperties({"visible":"none"})
						tds('Icon832_1_2').setProperties({"visible":"none"})
						tds('Icon458_1_2').setProperties({"visible":"none"})
						tds('IdLengthErrorIcon').setProperties({"visible":"none"})
						tds('IdValidErrorIcon').setProperties({"visible":"none"})
					}else{
						tds('TextField20123').removeClass('signup')
						tds('TextField20123').addClass('alert')
						tds('IdNullIcon').setProperties({"visible":"none"})
						tds('Icon832_1_2').setProperties({"visible":"none"})
						tds('Icon458_1_2').setProperties({"visible":"none"})
						tds('IdLengthErrorIcon').setProperties({"visible":"none"})
						tds('IdValidErrorIcon').setProperties({"visible":"visible"})
						setTimeout(function(){
							tds('IdValidErrorPopover').open();
						},100)
					}
				}
			
		}
	
		
	
		
	},
	 idKeyup : function(event, widget) {
		 	tds('TextField20123').removeClass('alert')
			tds('TextField20123').addClass('signup')
		 	tds('IdNullIcon').setProperties({"visible":"none"})
			tds('Icon832_1_2').setProperties({"visible":"none"})
			tds('Icon458_1_2').setProperties({"visible":"none"})
			tds('IdLengthErrorIcon').setProperties({"visible":"none"})
			tds('IdValidErrorIcon').setProperties({"visible":"none"})
			let iid = Top.Dom.selectById("TextField20123").getText();
			let icon1 = Top.Dom.selectById("Icon20123_1");
			let icon2 = Top.Dom.selectById("Icon20123_2");
			let idr=0;
			if (iid.length<5 || iid.length>20){
				icon1.setProperties({"text-color":"#C5C5C8"});
				//조상호  아이디 생성범위 초과or 미만
			}else{
				icon1.setProperties({"text-color":"#07C25B"});
				idr++;
				// 비밀번호 자리수는 맞았으니
				if (!(pattern_engb.test(iid))&& !(pattern_spc.test(iid))&& !(pattern_kor.test(iid)) && !(pattern_blank.test(iid)) ){
					icon2.setProperties({"text-color":"#07C25B"});
					idr++;
				}else{
					//조상호     아이디     입력 불가능한 문자 포함
					icon2.setProperties({"text-color":"#C5C5C8"});
				}
			}
			if(iid==""){
				icon2.setProperties({"text-color":"#C5C5C8"});
				idr=0;
				Top.Dom.selectById("Icon458_1_2").setProperties({"visible":"none"});
				Top.Dom.selectById("Icon832_1_2").setProperties({"visible":"none"});
			}
			if (idr==2){
				Top.Dom.selectById("Button2422").setProperties({"disabled": "false"})
				tds("Button2422").removeClass('signup-disabled')
				tds("Button2422").addClass('signup-solid')
				
			}else{
				Top.Dom.selectById("Button2422").setProperties({"disabled": "true"})
				tds("Button2422").removeClass('signup-solid')
				tds("Button2422").addClass('signup-disabled')
				icon2.setProperties({"text-color":"#C5C5C8"});
				Top.Dom.selectById("Icon458_1_2").setProperties({"visible":"none"});
				Top.Dom.selectById("Icon832_1_2").setProperties({"visible":"none"});
			}
		 
	},
	ckmail : function(event,widget){
		let mail =  Top.Dom.selectById('TextField2012_6').getText()+"@"+ Top.Dom.selectById("signupDomainTextField").getText();
		tds('Icon458_1_5').setProperties({"visible":"none"})
		if(checkMail(mail)){
			Top.Dom.selectById("Button2497").setProperties({"disabled": "false"})
			tds("Button2497").removeClass("signup-disabled")
			tds("Button2497").addClass('signup-solid')
		}else{
			Top.Dom.selectById("Button2497").setProperties({"disabled": "true"})
			tds("Button2497").removeClass("signup-solid")
			tds("Button2497").addClass('signup-disabled')
		}
		if(Top.Dom.selectById("TextField2012").getText() && Top.Dom.selectById("TextField20123").getText() && Top.Dom.selectById("TextField20124").getText() && Top.Dom.selectById("TextField2012_5").getText() && Top.Dom.selectById("TextField2012_6").getText() && Top.Dom.selectById("signupDomainTextField").getText() && tds('signupMobileTextField').getText() ){
			Top.Dom.selectById("Button2970").setProperties({"disabled": "false"});
			tds('Button2970').removeClass('signup-disabled')
			tds('Button2970').addClass('signup-solid')
		}else{
			Top.Dom.selectById("Button2970").setProperties({"disabled": "true"});
			tds('Button2970').removeClass('signup-solid')
			tds('Button2970').addClass('signup-disabled')
		}
	},
	 ckmailFocusOut : function(event, widget) {
		let mail =  Top.Dom.selectById('TextField2012_6').getText()+"@"+ Top.Dom.selectById("signupDomainTextField").getText();
		
		if(tds('TextField2012_6').getText() === null || tds('TextField2012_6').getText().length === 0 ){
			tds('TextField2012_6').removeClass('signup')
			tds('TextField2012_6').addClass('alert')
			tds('EmailIdNullIcon').setProperties({"visible":"visible"})
			tds('Icon832_1_5').setProperties({"visible":"none"})
			tds('EmailIdValidlIcon').setProperties({"visible":"none"})
			tds('Icon458_1_5').setProperties({"visible":"none"})
			tds('DomainNullIcon').setProperties({"visible":"none"})
			setTimeout(function(){
				tds('EmailNullPopover').open();
			},100)
		}
		else{
			tds('TextField2012_6').removeClass('alert')
			tds('TextField2012_6').addClass('signup')
			tds('signupDomainTextField').removeClass('alert')
			tds('signupDomainTextField').addClass('signup')
			tds('EmailIdNullIcon').setProperties({"visible":"none"})
			tds('EmailIdValidlIcon').setProperties({"visible":"none"})
			tds('DomainNullIcon').setProperties({"visible":"none"})
			tds('Icon832_1_5').setProperties({"visible":"none"})	
		}
			
	},
	domainFocusOut : function(event, widget) {
		let mail =  Top.Dom.selectById('TextField2012_6').getText()+"@"+ Top.Dom.selectById("signupDomainTextField").getText();
		if(tds('signupDomainTextField').getText() === null || tds('signupDomainTextField').getText().length === 0 ){
			tds('signupDomainTextField').removeClass('signup')
			tds('signupDomainTextField').addClass('alert')
			tds('DomainNullIcon').setProperties({"visible":"visible"})
			tds('EmailIdNullIcon').setProperties({"visible":"none"})
			tds('Icon832_1_5').setProperties({"visible":"none"})
			tds('EmailIdValidlIcon').setProperties({"visible":"none"})
			tds('Icon458_1_5').setProperties({"visible":"none"})
			
			setTimeout(function(){
				tds('EmailNullPopover2').open();
			},100)
		}
		else{
			tds('TextField2012_6').removeClass('alert')
			tds('TextField2012_6').addClass('signup')
			tds('signupDomainTextField').removeClass('alert')
			tds('signupDomainTextField').addClass('signup')
			tds('EmailIdNullIcon').setProperties({"visible":"none"})
			tds('EmailIdValidlIcon').setProperties({"visible":"none"})
			tds('DomainNullIcon').setProperties({"visible":"none"})
			tds('Icon832_1_5').setProperties({"visible":"none"})

			if(!checkMail(mail)){
						tds('TextField2012_6').removeClass('signup')
						tds('TextField2012_6').addClass('alert')
						tds('EmailIdValidlIcon').setProperties({"visible":"visible"})
						tds('DomainNullIcon').setProperties({"visible":"none"})
						tds('EmailIdNullIcon').setProperties({"visible":"none"})
						tds('Icon832_1_5').setProperties({"visible":"none"})
						tds('Icon458_1_5').setProperties({"visible":"none"})
						setTimeout(function(){
							tds('EmailValidPopover').open();
						},100)
					}	
			
			
		}
		
	},
	
	
	pwlogic : function (event,widget){
		$('#TextField20124 .top-textfield-icon').removeClass('signupPwd-alert')
		tds('Icon832_1_3').setProperties({"visible":"none"})
		tds('Icon892_1_3').setProperties({"visible":"none"})
		tds('signupPwdNullIcon').setProperties({"visible":"none"})
		if(Top.Dom.selectById("TextField2012").getText() && Top.Dom.selectById("TextField20123").getText() && Top.Dom.selectById("TextField20124").getText() && Top.Dom.selectById("TextField2012_5").getText() && Top.Dom.selectById("TextField2012_6").getText()&& Top.Dom.selectById("signupDomainTextField").getText() && tds('signupMobileTextField').getText()){
			Top.Dom.selectById("Button2970").setProperties({"disabled": "false"});
			tds('Button2970').removeClass('signup-disabled')
			tds('Button2970').addClass('signup-solid')
		}else{
			Top.Dom.selectById("Button2970").setProperties({"disabled": "true"});
			tds('Button2970').removeClass('signup-solid')
			tds('Button2970').addClass('signup-disabled')
		}
		var tff = Top.Dom.selectById("TextField20124");
		var icon1 = Top.Dom.selectById("Icon20124_1");
		var icon2 = Top.Dom.selectById("Icon20124_2");
		let ppw = tff.getText();
		pwr = 0;
		if (ppw.length<9 || ppw.length>20){
			//조상호      비밀번호 범위 초과	
			icon1.setProperties({"text-color":"#C5C5C8"});
		}else{
			icon1.setProperties({"text-color":"#07C25B"});
			pwr++;	
		}
			let a = 0;
			if (pattern_engb.test(ppw)){a++;}
			if (pattern_engs.test(ppw)){a++;}
			if (pattern_num.test(ppw)){a++;}
			if (pattern_spc2.test(ppw)){a++;}
			if (a>2){
				icon2.setProperties({"text-color":"#07C25B"});
				pwr++;
			}else{
				//조상호    비밀번호 영문특문숫자 모두 포함 안됨	
				icon2.setProperties({"text-color":"#C5C5C8"});
			}

		if(ppw == Top.Dom.selectById("TextField2012_5").getText()){
			Top.Dom.selectById("Icon832_1_4").setProperties({"visible": "none"});
			
		
    		Top.Dom.selectById("TextField2012_5").addClass("alert")
		}

	}, nmtype : function(event, widget) {
		tds('TextField2012_5').removeClass('alert')
		tds('TextField2012_5').addClass('signup')
		tds('Icon832_1_4').setProperties({"visible":"none"})
		tds('signupPwdConfirmNullIcon').setProperties({"visible":"none"})
		$('#TextField2012_5 .top-textfield-icon').removeClass('signupPwdConfirm-alert')	
		
		if(Top.Dom.selectById("TextField2012").getText() && Top.Dom.selectById("TextField20123").getText() && Top.Dom.selectById("TextField20124").getText() && Top.Dom.selectById("TextField2012_5").getText() && Top.Dom.selectById("TextField2012_6").getText()&& Top.Dom.selectById("signupDomainTextField").getText() && tds('signupMobileTextField').getText() ){
			Top.Dom.selectById("Button2970").setProperties({"disabled": "false"});
			tds('Button2970').removeClass('signup-disabled')
			tds('Button2970').addClass('signup-solid')
		}else{
			Top.Dom.selectById("Button2970").setProperties({"disabled": "true"});
			tds('Button2970').removeClass('signup-solid')
			tds('Button2970').addClass('signup-disabled')
		}
	}, botcktype : function(event, widget) {
		if(Top.Dom.selectById("TextField2012").getText() && Top.Dom.selectById("TextField20123").getText() && Top.Dom.selectById("TextField20124").getText() && Top.Dom.selectById("TextField2012_5").getText() && Top.Dom.selectById("TextField2012_6").getText() && Top.Dom.selectById("TextField2012_9").getText()){
			Top.Dom.selectById("Button2970").setProperties({"disabled": "false"});
			tds('Button2970').removeClass('signup-disabled')
			tds('Button2970').addClass('signup-solid')
		}else{
			Top.Dom.selectById("Button2970").setProperties({"disabled": "true"});
			tds('Button2970').removeClass('signup-solid')
			tds('Button2970').addClass('signup-disabled')
		}
		if(Top.Dom.selectById("TextField2012_9").getText()==null || Top.Dom.selectById("TextField2012_9").getText() == undefined){
			Top.Dom.selectById("Button2497_1").setProperties({"disabled": "true"});
		}else{
			Top.Dom.selectById("Button2497_1").setProperties({"disabled": "false"});
		}
	}, botck : function(event, widget) {
		if (Top.Dom.selectById("TextField2012_9").getText() != Top.Controller.get('signupBaseLogic').answer){
    		Top.Dom.selectById("TextField2012_9").setProperties({"border-color":"red"});
    		Top.Dom.selectById("Icon832_1_7").setProperties({"visible":"visible","tooltip-top":"올바른 입력 값을 입력해 주세요."});
    		Top.Dom.selectById("Icon458_1_7").setVisible("none");
			//조상호     자동가입방지
		} else{
			Top.Dom.selectById("TextField2012_9").setProperties({"border-color":"#d3dbdf"});
			Top.Dom.selectById("Icon832_1_7").setProperties({"visible":"none"});
			Top.Dom.selectById("Icon458_1_7").setVisible("visible");
		}// 자동 가입 방지 문자 체크  	
	}, nametypeck : function(event, widget) {
		if(Top.Dom.selectById("TextField2012").getText() && Top.Dom.selectById("TextField20123").getText() && Top.Dom.selectById("TextField20124").getText() && Top.Dom.selectById("TextField2012_5").getText() && Top.Dom.selectById("TextField2012_6").getText()&& Top.Dom.selectById("signupDomainTextField").getText() && tds('signupMobileTextField').getText()){
			Top.Dom.selectById("Button2970").setProperties({"disabled": "false"});
			tds('Button2970').removeClass('signup-disabled')
			tds('Button2970').addClass('signup-solid')
		}else{
			Top.Dom.selectById("Button2970").setProperties({"disabled": "true"});
			tds('Button2970').removeClass('signup-solid')
			tds('Button2970').addClass('signup-disabled')
		}
		Top.Dom.selectById("Icon832_1").setProperties({"visible":"none"})
		Top.Dom.selectById("Icon892_1").setProperties({"visible":"none"})
		let name = Top.Dom.selectById("TextField2012").getText();
		if(!name || name.length === 0){
			Top.Dom.selectById("Icon20123_1_1").setProperties({"text-color":"#696969"})			
		}else{
			Top.Dom.selectById("Icon20123_1_1").setProperties({"textColor":"#07C25B"})
			Top.Dom.selectById("TextField2012").removeClass("alert")
			if(!is_henValid(name)){
				Top.Dom.selectById("Icon20123_1_1").setProperties({"text-color":"#696969"})
					Top.Dom.selectById("Icon832_1").setProperties({"visible":"none"})
					Top.Dom.selectById("Icon892_1").setProperties({"visible":"none"})
			}
			
		}
	}, pwcheck_focusout : function(event, widget) {
		let pwregi = Top.Dom.selectById("TextField20124").getText();
		let pwregick = Top.Dom.selectById("TextField2012_5").getText();
		if(pwregi != pwregick){
    		//조상호        비밀번호 확인
			if(pwregick === null || pwregick.length === 0){
				Top.Dom.selectById("TextField2012_5").addClass("alert")
	    		Top.Dom.selectById("Icon832_1_4").setProperties({"visible":"none"});
				tds('signupPwdConfirmNullIcon').setProperties({"visible":"visible"})
	    		$('#TextField2012_5 .top-textfield-icon').addClass('signupPwdConfirm-alert')  	
	    		setTimeout(function(){
	    			Top.Dom.selectById("PwdConfimNullPopover").open()
	    		
	    		} , 100);
			}else{
				
				Top.Dom.selectById("TextField2012_5").addClass("alert")
				$('#TextField2012_5 .top-textfield-icon').addClass('signupPwdConfirm-alert')  
				tds('signupPwdConfirmNullIcon').setProperties({"visible":"none"})
	    		Top.Dom.selectById("Icon832_1_4").setProperties({"visible":"visible"});	   	
	    		setTimeout(function(){
	    			Top.Dom.selectById("DifferentPasswordErrorPopover").open()
	    		
	    		} , 100);
				
			}
    	}else{
    		Top.Dom.selectById("TextField2012_5").removeClass("alert")
    		$('#TextField2012_5 .top-textfield-icon').removeClass('signupPwdConfirm-alert')  
    		tds('signupPwdConfirmNullIcon').setProperties({"visible":"none"})    		
			Top.Dom.selectById("Icon832_1_4").setProperties({"visible":"none"});
			
    	}
	}, showpw : function(event, widget) {
		let pw = Top.Dom.selectById(widget.id)
		if(pw.getProperties("password")){
			pw.setProperties({"password": "false", "icon": "icon-ic_password_show"})
		}else{
			pw.setProperties({"password": "true", "icon": "icon-ic_password_hide"})
		}
	}, nameiconon : function(event, widget) {
		if(!Top.Dom.selectById("TextField2012").getText() || Top.Dom.selectById("TextField2012").getText().length === 0){
			Top.Dom.selectById("Icon20123_1_1").setProperties({"text-color":"#696969"})
			Top.Dom.selectById("Icon832_1").setProperties({"visible":"visible"})
			Top.Dom.selectById("Icon892_1").setProperties({"visible":"none"})
			Top.Dom.selectById("TextField2012").addClass("alert")
			setTimeout(function(){
				Top.Dom.selectById("NameErrorPopover").open()				
			}, 100);				
		}else if(!is_henValid(Top.Dom.selectById("TextField2012").getText())){
			Top.Dom.selectById("Icon832_1").setProperties({"visible":"none"})
			Top.Dom.selectById("Icon892_1").setProperties({"visible":"visible"})

			Top.Dom.selectById("TextField2012").addClass("alert")
			setTimeout(function(){
				Top.Dom.selectById("NameValidPopover").open()
			}, 100);	
		}	
		else{
			Top.Dom.selectById("TextField2012").removeClass("alert")
			
		}
			
			
	}, pwiconon : function(event, widget) {
		if(Top.Dom.selectById("Icon20124_1").getProperties("text-color")=="#07C25B" && Top.Dom.selectById("Icon20124_2").getProperties("text-color")=="#07C25B"){
	
			Top.Dom.selectById("Icon832_1_3").setProperties({"visible" : "none"});
			Top.Dom.selectById("Icon892_1_3").setProperties({"visible" : "none"});
			Top.Dom.selectById("TextField20124").removeClass("alert")
			$('#TextField20124 .top-textfield-icon').removeClass('signupPwd-alert')
			tds('TextField20124').addClass('signup')
	
			
		}
		else if(Top.Dom.selectById("Icon20124_1").getProperties("text-color")=="#07C25B" && Top.Dom.selectById("Icon20124_2").getProperties("text-color")=="#C5C5C8"){
			
			Top.Dom.selectById("Icon892_1_3").setProperties({"visible" : "visible"});
			Top.Dom.selectById("Icon832_1_3").setProperties({"visible" : "none"});
			Top.Dom.selectById("signupPwdNullIcon").setProperties({"visible" : "none"});
			Top.Dom.selectById("TextField20124").addClass("alert")
			$('#TextField20124 .top-textfield-icon').addClass('signupPwd-alert')
			setTimeout(function(){
				Top.Dom.selectById("PwdValidErrorPopover").open()			
			}, 100);
		}
		else if(Top.Dom.selectById("Icon20124_1").getProperties("text-color")=="#C5C5C8" && Top.Dom.selectById("Icon20124_2").getProperties("text-color")=="#07C25B"){
		
			Top.Dom.selectById("Icon892_1_3").setProperties({"visible" : "none"});
			Top.Dom.selectById("Icon832_1_3").setProperties({"visible" : "visible"});
			Top.Dom.selectById("signupPwdNullIcon").setProperties({"visible" : "none"});
			Top.Dom.selectById("TextField20124").addClass("alert")
			$('#TextField20124 .top-textfield-icon').addClass('signupPwd-alert')
			setTimeout(function(){
				Top.Dom.selectById("PwdLengthErrorPopover").open()	
			}, 100);
		}
		else if(Top.Dom.selectById("Icon20124_1").getProperties("text-color")=="#C5C5C8" && Top.Dom.selectById("Icon20124_2").getProperties("text-color")=="#C5C5C8"){
				if(tds('TextField20124').getText() === null || tds('TextField20124').getText().length === 0){
					Top.Dom.selectById("signupPwdNullIcon").setProperties({"visible" : "visible"});
					Top.Dom.selectById("Icon892_1_3").setProperties({"visible" : "none"});
					Top.Dom.selectById("Icon832_1_3").setProperties({"visible" : "none"});
					Top.Dom.selectById("TextField20124").addClass("alert")
					$('#TextField20124 .top-textfield-icon').addClass('signupPwd-alert')
					setTimeout(function(){
						Top.Dom.selectById("PasswordErrorPopover").open()	
					}, 100);
				}
				else{
					Top.Dom.selectById("signupPwdNullIcon").setProperties({"visible" : "none"});
					Top.Dom.selectById("Icon892_1_3").setProperties({"visible" : "none"});
					Top.Dom.selectById("Icon832_1_3").setProperties({"visible" : "visible"});
					Top.Dom.selectById("TextField20124").addClass("alert")
					$('#TextField20124 .top-textfield-icon').addClass('signupPwd-alert')
					setTimeout(function(){
						Top.Dom.selectById("PwdLengthErrorPopover").open()	
					}, 100);
				}
		}			
	},
	//메일 도메인 셀렉박스
	onChangeMailDomain : function(event, widget) { 
		tds('TextField2012_6').removeClass('alert')
		tds('TextField2012_6').addClass('signup')
		tds('EmailIdValidlIcon').setProperties({"visible":"none"})
		tds('EmailIdNullIcon').setProperties({"visible":"none"})
		tds('DomainNullIcon').setProperties({"visible":"none"})
		tds('Icon458_1_5').setProperties({"visible":"none"})
		Top.Dom.selectById("Icon458_1_5").setProperties({"visible":"none"})
		Top.Dom.selectById("Button2497").setProperties({"disabled":"true"})
		tds("Button2497").removeClass("signup-solid")
		tds("Button2497").addClass('signup-disabled')
		if(Top.Dom.selectById("signupDomainSelectBox").getSelectedText() === "직접입력"){
			Top.Dom.selectById("signupDomainTextField").setProperties({"disabled":"false"})
			tds('signupDomainTextField').removeClass('signup-disabled')
			tds('signupDomainTextField').addClass('signup')
			Top.Dom.selectById("Button2970").setProperties({"disabled":"true"})
			tds('Button2970').removeClass('signup-solid')
			tds('Button2970').addClass('signup-disabled')
			Top.Dom.selectById("signupDomainTextField").setText("")
		}
		else{
			Top.Dom.selectById("signupDomainTextField").setText(Top.Dom.selectById('signupDomainSelectBox').getSelectedText())
			Top.Dom.selectById("signupDomainTextField").setProperties({"disabled":"true"})
				tds('signupDomainTextField').removeClass('signup')
			tds('signupDomainTextField').addClass('signup-disabled')
		Top.Controller.get('signupSecondLogic').ckmail()
			
		}
	}, mobileNumValid : function(event, widget) {

		var mobileNum = tds('signupMobileTextField').getText()
		if(mobileNum === null || mobileNum.length === 0){
			tds('Icon892_1_6').setProperties({"visible":"visible"})
			tds('Icon832_1_6').setProperties({"visible":"none"})
			tds('signupMobileTextField').addClass('alert')
			setTimeout(function(){
				tds('MobileNullPopover').open()
			},100)
		}else{
			if(pattern_engs.test(mobileNum) || pattern_engb.test(mobileNum)	|| pattern_spc2.test(mobileNum) ||  pattern_kor.test(mobileNum)	||  pattern_blank.test(mobileNum))
				{
					tds("signupMobileTextField").addClass("alert")
					tds("Icon832_1_6").setProperties({"visible":"visible"})
					tds('Icon892_1_6').setProperties({"visible":"none"})
					tds('Button2970').setProperties({"disabled":"true"})
						tds('Button2970').removeClass('signup-solid')
						tds('Button2970').addClass('signup-disabled')
					setTimeout(function(){
						Top.Dom.selectById("MobileValidPopover").open()
					}, 100)
					return false;
				}
			else{
				tds("signupMobileTextField").removeClass("alert")
				tds("Icon832_1_6").setProperties({"visible":"none"})
					tds('Icon892_1_6').setProperties({"visible":"none"})
			}
		}
		
		
		if(Top.Dom.selectById("TextField2012").getText() && Top.Dom.selectById("TextField20123").getText() && Top.Dom.selectById("TextField20124").getText() && Top.Dom.selectById("TextField2012_5").getText() && Top.Dom.selectById("TextField2012_6").getText()&& Top.Dom.selectById("signupDomainTextField").getText() && tds('signupMobileTextField').getText()){
			Top.Dom.selectById("Button2970").setProperties({"disabled": "false"});
			tds('Button2970').removeClass('signup-disabled')
			tds('Button2970').addClass('signup-solid')
		}else{
			Top.Dom.selectById("Button2970").setProperties({"disabled": "true"});
			tds('Button2970').removeClass('signup-solid')
			tds('Button2970').addClass('signup-disabled')
		}
	}, onChangeNationalMobile : function(event, widget) {
		//$('#NationalMobileSelectBox span.top-selectbox-container')[0].lastChild.textContent = $('#NationalMobileSelectBox .top-selectbox-container').text().split(' ')[0]
		$('#NationalMobileSelectBox span.top-selectbox-container')[0].lastChild.textContent = tds("NationalMobileSelectBox").getSelected().value
		
	}
	
	
	
	
});
Top.Controller.create('signupThirdLogic', {
	init:function(event, widget){
		let idregi = Top.Controller.get("signupBaseLogic").idregi;
		Top.Dom.selectById("TextView64").setText(idregi);
	}, register_end : function(event, widget) {
		if(location.href.indexOf("login")>-1){
			//다이얼로그로 보는중
			Top.Dom.selectById("RegisterDialog").close()
		}else if(Top.Controller.get('signupBaseLogic').isExternal){
			//새창으로 보는중
			Top.App.routeTo("/login")
		}else{
			window.close();
		}
		
	}, app_download : function(event, widget) {
		location.href = "https://tmaxcloudspace.page.link/cs"
	}
});


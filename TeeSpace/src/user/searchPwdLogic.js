var idin;
//var namein;
var pwin;
//var mailin;
var contArrayPwd = new Array(2);

//메일 검증
function checkMail(str){
	let exptext = /^([0-9a-zA-Z_\.-]+)@([0-9a-zA-Z_-]+)(\.[0-9a-zA-Z_-]+){1,2}$/;

	if(exptext.test(str)==false){
		return false
	}else{
		return true
	}
	
}


/////////////////////////////////////////1p
Top.Controller.create('searchPwdFirstLayoutLogic', {
	
	init: function(event,widget){
		
		//controller
		this.idck = 0;
		this.mailoutputtext;

            	Top.Ajax.execute({
        			url : _workspace.url + "Users/SysContext",
        			type : 'GET',
        			dataType : "json",
        			cache : false,
        			data: JSON.stringify({
          	          "dto": {
          	            "KEY": "MAIL_URL"
          	          }
          	        }),
          	      contentType : "application/json",
        			crossDomain : true,
        			xhrFields : {
        			    withCredentials: true
        			},
        			headers: {
        				"ProObjectWebFileTransfer":"true"
        			},
        			success : function(result) {
        				_mailDomain = result.dto.VALUE
        				Top.Dom.selectById("TextField253").focus();
        				Top.Dom.selectById("TextView314").setText("@"+_mailDomain)
        			}
        		});	
	},
	
	searchPwdNextBtnClick : function(event, widget) {

		idin = Top.Dom.selectById("TextField253").getText();
		if(typeof _validBtoc ==='function' && !_validBtoc()){
			if(idin.indexOf("@") === -1 ){
				Top.Dom.selectById("TextField253").addClass("alert");
		        		Top.Dom.selectById("Icon593").setProperties({"visible":"none"})
		        		Top.Dom.selectById("Icon593_error").setProperties({"visible":"visible"})
		        		setTimeout(function(){
		        		    Top.Dom.selectById("NoIdPopover").open()	
		        		},10)
		        		return;
			}
		}
		$.ajax({
	        url: _workspace.url+'Users/IDCheck?action=SO', //Service Object
	        type: 'POST',
	        dataType: 'json',
	        crossDomain: true,
	        contentType : "application/json",
	        xhrFields: {

	            withCredentials: true

	          },
	        data: JSON.stringify({
	          "dto": {
	        	"USER_LOGIN_ID": idin,            
	          }
	        }),
	        success: function(result) {
	        	if(result.dto.USER_PW=="RST0001"){
	        		Top.Controller.get('searchPwdFirstLayoutLogic').mailoutputtext = result.dto.USER_LOGIN_ID 
	        		//해당 아이디 있음
	        		Top.Dom.selectById("TextField253").removeClass("alert");
	        	
	        		Top.Dom.selectById("PwdRMasterLayout").src('searchPwdSecondLayout.html');
		    		Top.Dom.selectById("PwdR2MasterLayout").src('searchPwdSecondButtonLayout.html');
		    		
	        	}else{
	        		// 해당 아이디 없음
	        		//기획 추가

	        		Top.Dom.selectById("TextField253").addClass("alert");
	        		Top.Dom.selectById("Icon593").setProperties({"visible":"none"})
	        		Top.Dom.selectById("Icon593_error").setProperties({"visible":"visible"})
	        		Top.Dom.selectById("NoIdPopover").open()

	        	}
	        	
	        },
	        error: function(error) {

	        }
		})
	}, 
	disableButton : function(event, widget){
		Top.Dom.selectById('Button408').setProperties({"disabled":"false"})
		tds('Button408').removeClass('signup-disabled')
		tds('Button408').addClass('signup-solid')
		Top.Dom.selectById("Icon593_error").setProperties({"visible":"none"})
		Top.Dom.selectById('Icon593').setProperties({"visible":"none"})
		Top.Dom.selectById("TextField253").removeClass("alert");
		
	},
	searchPwdIdfield : function(event, widget) {

		var gtext = Top.Dom.selectById("TextField253").getText().split('@')[0];
		if(gtext === null || gtext.length === 0){	
			Top.Dom.selectById("Icon593_error").setProperties({"visible":"none"})
			Top.Dom.selectById("Icon593").setProperties({"visible":"visible"})
			Top.Dom.selectById("TextField253").addClass("alert");
			setTimeout(function() {
				Top.Dom.selectById("EmailErrorPopover").open()
    				}, 100);		
			
			this.idck = 0;
		}else{
			Top.Dom.selectById("Icon593_error").setProperties({"visible":"none"})
			Top.Dom.selectById('Icon593').setProperties({"visible":"none"})
			Top.Dom.selectById("TextField253").removeClass("alert");
			this.idck = 1;
		}
		
		if(this.idck ){
			Top.Dom.selectById("Button408").setProperties({"disabled":"false"})
			tds('Button408').removeClass('signup-disabled')
			tds('Button408').addClass('signup-solid')
		}else{
			Top.Dom.selectById("Button408").setProperties({"disabled":"true"})
			tds('Button408').removeClass('signup-solid')
			tds('Button408').addClass('signup-disabled')
		}
		
		
		

	}, close_mobile : function(event, widget) {
		   Top.Dom.selectById("PwChangeDialog").close()
	}
	
	
});



///////////////////////////////////////////////2_2p
Top.Controller.create('searchPwdSecondLogic', {
	
	init: function(event,widget){
		//기획변경
		//Top.Dom.selectById("TextField703").focus();
		Top.Dom.selectById("TextView30").setText(Top.Controller.get('searchPwdFirstLayoutLogic').mailoutputtext);
		Top.Dom.selectById("TextView30").setProperties({"text-color" : "#878787"});
		 
		//이메일 힌트 작성
		var hideMail = Top.Controller.get('searchPwdFirstLayoutLogic').mailoutputtext;
		var hideMailArray = hideMail.split('@');
		var behindArray = hideMailArray[1].split('.');
		var arrlength = behindArray.length
		var total_mail = 0;
		
		for(var h=3; h<hideMailArray[0].length;h++){
			
			hideMailArray[0] = hideMailArray[0].replaceAt(h,'*'); 
	
		}
		for(var h=1; h<(arrlength);h++){
			behindArray[h] = '.'+behindArray[h];
		}
		for(var k =1; k<(arrlength); k++){
			if( k >> 1){total_mail = behindArray[1] + behindArray[k] }
			else{
			total_mail =  behindArray[k]
			}
		}
		for(var h=1; h<behindArray[0].length;h++){
			
			behindArray[0] =behindArray[0].replaceAt(h,'*'); 
	
		}
		




		Top.Dom.selectById("TextView30").setText(hideMailArray[0] + "@" +behindArray[0]+ total_mail  );
		 
		
	},	 
	
	pw_requestBtn : function(event, widget) {
		//타이머 부분
		
		
		let pw_aa = Top.Dom.selectById("Button164");

		
			$.ajax({
		        url: _workspace.url+"Mail/Mail?action=AuthRequest", //Service Object
		        type: 'POST',
		        dataType: 'json',
		        crossDomain: true,
		        contentType : "application/json",
		        xhrFields: {
		
		            withCredentials: true
		
		          },
		          data: JSON.stringify({
			          "dto": {
			            "CMFLG": "MailAuthSend",
			            "EMAIL_ADDRESS" : Top.Controller.get('searchPwdFirstLayoutLogic').mailoutputtext,
			            "NAME" : "Auth Request",
			            "TYPE" : "HTML",
			            "LOCALE" : "kr",
			            "USER_ID" : "705b51cb-85d8-45bb-9aac-a74aa56d7c3f",
			            "GATEWAY_NAME" : "TEST"
			          }
			        }),
		        success: function(result) {
		        	
					Top.Dom.selectById("TextField243").setProperties({"disabled":"false"});
					tds("TextField243").removeClass('signup-disabled')
					tds("TextField243").addClass('signup')
					
			
					if("인증 재요청"==Top.Dom.selectById("Button164").getText()){ 
						Top.Dom.selectById("timer2").resume();
						tds('timer2').setProperties({"onEndtimer":"endTimerOpen"})
					}else{
						Top.Dom.selectById("timer2").setProperties({"visible":"visible"});
						Top.Dom.selectById("timer2").start();
						tds('timer2').setProperties({"onEndtimer":"endTimerOpen"})
						Top.Dom.selectById("timer2").setProperties({"text-color":"#2589EB"});
						Top.Dom.selectById("Button164").setProperties({"text": "인증 재요청"});
					}
					
					
					Top.Dom.selectById("TextView1415_pw").setProperties({"visible":"visible"});
		        },
		        error: function(error) {
		        }
	      });
							

		
	},
	vaildNumFocusOut : function(event, widget){
		var numtext = Top.Dom.selectById("TextField243").getText()
		if(numtext === null || numtext.length === 0){	
			Top.Dom.selectById("Icon711").setProperties({"visible":"none"})
			Top.Dom.selectById("Icon711_null").setProperties({"visible":"visible"})
		
    		Top.Dom.selectById("TextField243").addClass("alert");
			setTimeout(function() {
				Top.Dom.selectById("AuthNullPopover").open()	
    				}, 100);
			
		}else{
			Top.Dom.selectById("Icon711").setProperties({"visible":"none"})
			Top.Dom.selectById('Icon711_null').setProperties({"visible":"none"})
			Top.Dom.selectById("TextField243").removeClass("alert");
    		
			
		}
		
		
	},
	pw_verify_num : function(event, widget) {
		Top.Dom.selectById("search_pw_next_button").setProperties({"disabled":"false"});
		tds('search_pw_next_button').removeClass('signup-disabled')
		tds('search_pw_next_button').addClass('signup-solid')
		Top.Dom.selectById("TextField243").removeClass("alert");
		
		Top.Dom.selectById("Icon711").setProperties({"visible":"none"})
		Top.Dom.selectById('Icon711_null').setProperties({"visible":"none"})
	}
	
});

//새로운 기획 다음버튼
Top.Controller.create('searchPwdSecondButtonLogic', {
	
	init : function() {
		
		Top.Dom.selectById('search_pw_next_button').setProperties({'on-click':'newNextBtn'});
		var new_btn = Top.Dom.selectById('search_pw_next_button');
		new_btn.setProperties({"disabled" : "true"})
		new_btn.removeClass('signup-solid')
		new_btn.addClass('signup-disabled')
	},
	
	newNextBtn : function(event, widget) {

			let pw_key = Top.Dom.selectById("TextField243").getText();
			//namein = Top.Dom.selectById("TextField703").getText();
			//mailin = Top.Dom.selectById("TextField74").getText();
			$.ajax({
		        url: _workspace.url+"Users/AuthToken", //Service Object
		        type: 'GET',
		        dataType: 'json',
		        crossDomain: true,
		        contentType : "application/json",
		        xhrFields: {
		            withCredentials: true
		          },
		          data: JSON.stringify({
			          "dto": {
			            "EMAIL_ADDRESS" :Top.Controller.get('searchPwdFirstLayoutLogic').mailoutputtext,
			            "TOKEN" : pw_key
			          }
			        }),
		        success: function(result) {        	
		        	if(result.dto.TOKEN == "RST0001"){
		        		Top.Dom.selectById("Icon711_null").setProperties({"visible":"none"})
		        		Top.Dom.selectById("Icon711").setProperties({"visible":"none"});
		        		Top.Dom.selectById("TextField243").removeClass("alert");
		        	
		        		//인증완료로 버튼 텍스트 수정
		        		Top.Dom.selectById("Button164").setProperties({"disabled" : "true","text": "인증 완료"});
		        		tds('Button164').removeClass('signup-solid')
		        		tds('Button164').addClass('signup-disabled')
		        		//유효시간영역 사라지게
		    			Top.Dom.selectById('LinearLayout152').setProperties({"visible" : "none" })
		    			
		    			Top.Dom.selectById("PwdRMasterLayout").src('searchPwdThirdLayout.html');     
		    			Top.Dom.selectById("PwdR2MasterLayout").src('searchPwdThirdButtonLayout.html');

		    		}else{
		    			Top.Dom.selectById("Icon711_null").setProperties({"visible":"none"})
		    			Top.Dom.selectById("Icon711").setProperties({"visible":"visible"});
		    			Top.Dom.selectById("AuthErrorPopover").open();
		    			Top.Dom.selectById("TextField243").addClass("alert");	        		
		    			//조상호        인증번호입력
		    			//인증번호 입력 실패함
		        		return false;
		    		}
		        },
		        error: function(error) {
		        }
		      });

		
	}


});




/////////////////////////////////////////3p
Top.Controller.create('searchPwdThirdLogic', {
	
	init: function(event,widget){
		Top.Dom.selectById("TextField644").focus();	
		
	},
	
	//새 비밀번호 실시간
	pw_password : function(event, widget) {
		$('#TextField644 .top-textfield-icon').removeClass('changePwd-alert')
		tds('Icon123Null').setProperties({"visible":"none"})
		tds('Icon123').setProperties({"visible":"none"})
		tds('Icon120').setProperties({"visible":"none"})
		

		let pwdNew = Top.Dom.selectById("TextField644").getText();
		
		controllerBoxpwd = [];
		
		//특수문자 유효성
		if((pattern_num.test(pwdNew)) && (pattern_engs.test(pwdNew)) && (pattern_spc2.test(pwdNew))){
			Top.Dom.selectById("Icon731_p").setProperties({"text-color":"#07C25B"});
			contArrayPwd[1]=1;
		}
		else if((pattern_num.test(pwdNew)) && (pattern_engs.test(pwdNew)) && (pattern_engb.test(pwdNew))){
			Top.Dom.selectById("Icon731_p").setProperties({"text-color":"#07C25B"});
			contArrayPwd[1]=1;
		}
		else if((pattern_engs.test(pwdNew)) && (pattern_spc2.test(pwdNew)) && (pattern_engb.test(pwdNew))){
			Top.Dom.selectById("Icon731_p").setProperties({"text-color":"#07C25B"});
			contArrayPwd[1]=1;
		}
		else{
			Top.Dom.selectById("Icon731_p").setProperties({"text-color":"#C5C5C8"});
			contArrayPwd[1]=0;
		}	
		//길이 유효성
		if(pwdNew.length >= 9 && pwdNew.length <= 20){
	
			Top.Dom.selectById("Icon666_p").setProperties({"text-color":"#07C25B"});
			contArrayPwd[0]=1;
		}
		else {
			Top.Dom.selectById("Icon666_p").setProperties({"text-color":"#C5C5C8"});
			contArrayPwd[0]=0;		
		}
		
		
		
//		
//		//오류 팝오버 Control
//		if(contArrayPwd[0] === 0 && contArrayPwd[1] === 0){
//			Top.Dom.selectById("Icon123Null").setProperties({"visible":"none"})
//			Top.Dom.selectById("Icon123").setProperties({"visible":"visible"})
//			Top.Dom.selectById("Icon120").setProperties({"visible":"none"})
//			
//			Top.Dom.selectById("TextField644").addClass("alert")
//			setTimeout(function() {
//				Top.Dom.selectById("PwdLengthErrorPopover").open()
//			}, 100);
//			//저장 버튼 
//			Top.Dom.selectById("Button847").setProperties({"disabled":"true"})
//			tds('Button847').removeClass('signup-solid')
//			tds('Button847').addClass('signup-disabled')
//
//			
//		}else if(contArrayPwd[0] === 1 && contArrayPwd[1] === 0){
//			
//			Top.Dom.selectById("Icon123Null").setProperties({"visible":"none"})
//			Top.Dom.selectById("Icon123").setProperties({"visible":"none"})
//			Top.Dom.selectById("Icon120").setProperties({"visible":"visible"})
//			
//		
//			Top.Dom.selectById("TextField644").addClass("alert")
//			setTimeout(function() {
//				Top.Dom.selectById("PwdValidErrorPopover").open()
//			}, 100);
//			//저장 버튼 
//			Top.Dom.selectById("Button847").setProperties({"disabled":"true"})
//			tds('Button847').removeClass('signup-solid')
//			tds('Button847').addClass('signup-disabled')
//
//
//			
//			
//		}else if(contArrayPwd[0] === 0 && contArrayPwd[1] === 1){
//			Top.Dom.selectById("Icon123Null").setProperties({"visible":"none"})
//			Top.Dom.selectById("Icon123").setProperties({"visible":"visible"})
//			Top.Dom.selectById("Icon120").setProperties({"visible":"none"})
//		
//	
//			Top.Dom.selectById("TextField644").addClass("alert")
//			setTimeout(function() {
//				Top.Dom.selectById("PwdLengthErrorPopover").open()
//			}, 100);
//			//저장 버튼 
//			Top.Dom.selectById("Button847").setProperties({"disabled":"true"})
//			tds('Button847').removeClass('signup-solid')
//			tds('Button847').addClass('signup-disabled')
//
//
//			
//		}else{
//			Top.Dom.selectById("Icon123Null").setProperties({"visible":"none"})
//			Top.Dom.selectById("Icon123").setProperties({"visible":"none"})
//			Top.Dom.selectById("Icon120").setProperties({"visible":"none"})
//		
//			Top.Dom.selectById("TextField644").removeClass("alert")
//
//		}
//		
		
		
		
		
	}, 
	//비밀번호 focusout
	is_same : function(event, widget) {
		let pwdNew = Top.Dom.selectById("TextField644").getText();
		let pwdNewConfirm = Top.Dom.selectById("TextField844").getText(); 
		controllerBoxpwd = [];
		//특수문자 유효성
		if((pattern_num.test(pwdNew)) && (pattern_engs.test(pwdNew)) && (pattern_spc2.test(pwdNew))){
			Top.Dom.selectById("Icon731_p").setProperties({"text-color":"#07C25B"});
			contArrayPwd[1]=1;
		}
		else if((pattern_num.test(pwdNew)) && (pattern_engs.test(pwdNew)) && (pattern_engb.test(pwdNew))){
			Top.Dom.selectById("Icon731_p").setProperties({"text-color":"#07C25B"});
			contArrayPwd[1]=1;
		}
		else if((pattern_engs.test(pwdNew)) && (pattern_spc2.test(pwdNew)) && (pattern_engb.test(pwdNew))){
			Top.Dom.selectById("Icon731_p").setProperties({"text-color":"#07C25B"});
			contArrayPwd[1]=1;
		}
		else{
			Top.Dom.selectById("Icon731_p").setProperties({"text-color":"#C5C5C8"});
			contArrayPwd[1]=0;
		}	
		//길이 유효성
		if(pwdNew.length >= 9 && pwdNew.length <= 20){
	
			Top.Dom.selectById("Icon666_p").setProperties({"text-color":"#07C25B"});
			contArrayPwd[0]=1;
		}
		else {
			Top.Dom.selectById("Icon666_p").setProperties({"text-color":"#C5C5C8"});
			contArrayPwd[0]=0;		
		}
		
		
		
		if(pwdNew === "" || pwdNew.length === 0){
			Top.Dom.selectById("Icon123Null").setProperties({"visible":"visible"})
			Top.Dom.selectById("Icon123").setProperties({"visible":"none"})
			Top.Dom.selectById("Icon120").setProperties({"visible":"none"})
		

			Top.Dom.selectById("TextField644").addClass("alert")
			$('#TextField644 .top-textfield-icon').addClass('changePwd-alert')
			setTimeout(function() {
				Top.Dom.selectById("NewInputPwdNullErrorPopover").open()
			}, 100);
		}	
		//오류 팝오버 Control
		else if(contArrayPwd[0] === 0 && contArrayPwd[1] === 0){
			Top.Dom.selectById("Icon123Null").setProperties({"visible":"none"})
			Top.Dom.selectById("Icon123").setProperties({"visible":"visible"})
			Top.Dom.selectById("Icon120").setProperties({"visible":"none"})
			
			Top.Dom.selectById("TextField644").addClass("alert")
			$('#TextField644 .top-textfield-icon').addClass('changePwd-alert')
			setTimeout(function() {
				Top.Dom.selectById("PwdLengthErrorPopover").open()
			}, 100);
			//저장 버튼 
			Top.Dom.selectById("Button847").setProperties({"disabled":"true"})
			tds('Button847').removeClass('signup-solid')
			tds('Button847').addClass('signup-disabled')

			
		}else if(contArrayPwd[0] === 1 && contArrayPwd[1] === 0){
			
			Top.Dom.selectById("Icon123Null").setProperties({"visible":"none"})
			Top.Dom.selectById("Icon123").setProperties({"visible":"none"})
			Top.Dom.selectById("Icon120").setProperties({"visible":"visible"})
			
		
			Top.Dom.selectById("TextField644").addClass("alert")
			$('#TextField644 .top-textfield-icon').addClass('changePwd-alert')
			setTimeout(function() {
				Top.Dom.selectById("PwdValidErrorPopover").open()
			}, 100);
			//저장 버튼 
			Top.Dom.selectById("Button847").setProperties({"disabled":"true"})
			tds('Button847').removeClass('signup-solid')
			tds('Button847').addClass('signup-disabled')


			
			
		}else if(contArrayPwd[0] === 0 && contArrayPwd[1] === 1){
			Top.Dom.selectById("Icon123Null").setProperties({"visible":"none"})
			Top.Dom.selectById("Icon123").setProperties({"visible":"visible"})
			Top.Dom.selectById("Icon120").setProperties({"visible":"none"})
		
	
			Top.Dom.selectById("TextField644").addClass("alert")
			$('#TextField644 .top-textfield-icon').addClass('changePwd-alert')
			setTimeout(function() {
				Top.Dom.selectById("PwdLengthErrorPopover").open()
			}, 100);
			//저장 버튼 
			Top.Dom.selectById("Button847").setProperties({"disabled":"true"})
			tds('Button847').removeClass('signup-solid')
			tds('Button847').addClass('signup-disabled')


			
		}
		else if(pwdNew === pwdNewConfirm && contArrayPwd[0] === 1 && contArrayPwd[1] === 1){
			Top.Dom.selectById("Icon123Null").setProperties({"visible":"none"})
			Top.Dom.selectById("Icon64Null").setProperties({"visible":"none"})
			Top.Dom.selectById("Icon64").setProperties({"visible":"none"})
		
			Top.Dom.selectById("TextField844").removeClass("alert")
			$('#TextField844 .top-textfield-icon').removeClass('changePwdConfirm-alert')
			
			//저장 버튼 
			Top.Dom.selectById("Button847").setProperties({"disabled":"false"})
			tds('Button847').removeClass('signup-disabled')
			tds('Button847').addClass('signup-solid')

			
		}else if(pwdNew !== pwdNewConfirm && contArrayPwd[0] === 1 && contArrayPwd[1] === 1 && !!tds('TextField844').getText()){
			Top.Dom.selectById("Icon123Null").setProperties({"visible":"none"})
			Top.Dom.selectById("Icon123").setProperties({"visible":"none"})
			Top.Dom.selectById("Icon120").setProperties({"visible":"none"})
		
			Top.Dom.selectById("TextField644").removeClass("alert")
			$('#TextField644 .top-textfield-icon').removeClass('changePwd-alert')
			Top.Dom.selectById("Button847").setProperties({"disabled":"true"})
			tds('Button847').removeClass('signup-solid')
			tds('Button847').addClass('signup-disabled')

		}else{
			Top.Dom.selectById("Icon123Null").setProperties({"visible":"none"})
			Top.Dom.selectById("Icon123").setProperties({"visible":"none"})
			Top.Dom.selectById("Icon120").setProperties({"visible":"none"})
		
			Top.Dom.selectById("TextField644").removeClass("alert")
			$('#TextField644 .top-textfield-icon').removeClass('changePwd-alert')
		}
		
		
		
//		
//		else if(pwdNew === pwdNewConfirm && contArrayPwd[0] === 1 && contArrayPwd[1] === 1){
//			Top.Dom.selectById("Icon123Null").setProperties({"visible":"none"})
//			Top.Dom.selectById("Icon64Null").setProperties({"visible":"none"})
//			Top.Dom.selectById("Icon64").setProperties({"visible":"none"})
//		
//			Top.Dom.selectById("TextField844").removeClass("alert")
//			
//			//저장 버튼 
//			Top.Dom.selectById("Button847").setProperties({"disabled":"false"})
//			tds('Button847').removeClass('signup-disabled')
//			tds('Button847').addClass('signup-solid')
//
//			
//		}
//		else{
//			Top.Dom.selectById("Icon123Null").setProperties({"visible":"none"})
//			Top.Dom.selectById("Icon64Null").setProperties({"visible":"none"})
//			Top.Dom.selectById("Icon64").setProperties({"visible":"visible"})
//	
//			
//			Top.Dom.selectById("TextField844").addClass("alert")
//			setTimeout(function() {
//				Top.Dom.selectById("DifferentPasswordErrorPopover").open()
//			}, 100);
//			//저장 버튼 
//			Top.Dom.selectById("Button847").setProperties({"disabled":"true"})
//			tds('Button847').removeClass('signup-solid')
//			tds('Button847').addClass('signup-disabled')
//		}
		
	},
	pwdConfirmKeyup : function(event, widget){
		Top.Dom.selectById("Icon64Null").setProperties({"visible":"none"})
		Top.Dom.selectById("Icon64").setProperties({"visible":"none"})
		
		Top.Dom.selectById("TextField844").removeClass("alert")
		$('#TextField844 .top-textfield-icon').removeClass('changePwdConfirm-alert')
		
	},
	
	
	//확인 focusout
	pwd_confirm : function(event, widget) {
		
		let pwdNew = Top.Dom.selectById("TextField644").getText();
		let pwdNewConfirm = Top.Dom.selectById("TextField844").getText(); 
		
		if(pwdNewConfirm === "" || pwdNewConfirm.length === 0){
			Top.Dom.selectById("Icon64Null").setProperties({"visible":"visible"})
			Top.Dom.selectById("Icon64").setProperties({"visible":"none"})
		
			
			Top.Dom.selectById("TextField844").addClass("alert")
			$('#TextField844 .top-textfield-icon').addClass('changePwdConfirm-alert')
			setTimeout(function() {
				Top.Dom.selectById("NewPwdNullErrorPopover").open()
			}, 100);
		}else if(pwdNew === pwdNewConfirm && contArrayPwd[0] === 1 && contArrayPwd[1] === 1){
			Top.Dom.selectById("Icon64Null").setProperties({"visible":"none"})
			Top.Dom.selectById("Icon64").setProperties({"visible":"none"})
		
			Top.Dom.selectById("TextField844").removeClass("alert")
			$('#TextField844 .top-textfield-icon').removeClass('changePwdConfirm-alert')
			//저장 버튼 
			Top.Dom.selectById("Button847").setProperties({"disabled":"false"})
			tds('Button847').removeClass('signup-disabled')
			tds('Button847').addClass('signup-solid')
		
		}else{
			Top.Dom.selectById("Icon64Null").setProperties({"visible":"none"})
			Top.Dom.selectById("Icon64").setProperties({"visible":"visible"})
		
		
			Top.Dom.selectById("TextField844").addClass("alert")
			$('#TextField844 .top-textfield-icon').addClass('changePwdConfirm-alert')
			setTimeout(function() {
				Top.Dom.selectById("DifferentPasswordErrorPopover").open()
			}, 100);
			//저장 버튼 
			Top.Dom.selectById("Button847").setProperties({"disabled":"true"})
			tds('Button847').removeClass('signup-solid')
			tds('Button847').addClass('signup-disabled')
		}
		
		
	
	},
	
	
	showpw : function(event, widget) {
		let pw = Top.Dom.selectById(widget.id)
		if(pw.getProperties("password")){
			pw.setProperties({"password": "false", "icon": "icon-ic_password_show"})
		}else{
			pw.setProperties({"password": "true", "icon": "icon-ic_password_hide"})
		}
	}
	
	
});


Top.Controller.create('searchPwdThirdButtonLogic', {
	pw_savebutton : function(event, widget) {
		
		console.info("save button start")
		pwin = Top.Dom.selectById("TextField844").getText();
			$.ajax({
		        url: _workspace.url + 'Users/PwChange?action=Put', //Service Object, http://192.168.158.12:8080/CMS/Users/PwChange?action=Put
		        type: 'POST',
		        dataType: 'json',
		        crossDomain: true,
		        contentType : "application/json",
		        xhrFields: {
	
		            withCredentials: true
	
		          },
		        data: JSON.stringify({
		          "dto": {
		            "USER_LOGIN_ID": idin.trim(),
		            "USER_PW" : SHA256(idin.trim()+pwin.trim()),
		            //"USER_NAME" : namein,
		            "USER_ID" : Top.Controller.get('searchPwdFirstLayoutLogic').mailoutputtext
		          }
		        }),
		        success: function(result) {
		        	if(result.dto.USER_PW == "RST0001"){
		        		Top.Dom.selectById("Icon64").setProperties({"visible":"none"})
		        		Top.Dom.selectById("TextField844").removeClass('alert')
		        		
		        		 try {
		                       var msg = '비밀번호가 저장되었습니다.';
		                       notiFeedback(msg);
		                   } catch (e) {}
			        	//window.close();
		                   if(location.href.indexOf("login")>-1){
		                       //다이얼로그로 보는중
		                       Top.Dom.selectById("PwChangeDialog").close()
		                   }else{
		                       //새창으로 보는중
		                       window.close();
		                   }
		        	}else{
		        		Top.Dom.selectById("Icon64").setProperties({"visible":"visible"})
		        	
		        			Top.Dom.selectById("TextField844").addClass('alert')

		        	}

		},
		        error: function(error) {
		        	Top.Dom.selectById("Icon64").setProperties({"visible":"visible"})
		        
		        	Top.Dom.selectById("TextField844").addClass('alert')
		        	
		}
			})
		
	}
});

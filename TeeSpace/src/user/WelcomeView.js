(function (exports) {

    function WelcomeViewContainer(opts = {
        userName: '',
        content: {
            title: '',
            body: ''
        },
        bgUrl: null,
        bgStyle: {
            width: null,
            height: null,
            bottom: null,
            backgroundPosition: 'bottom',
        },
        onCloseCallback: null,
    }) {
        this.opts = opts;
        this.template = htmlToElement(`
            <div class="welcome-container">
                <div class="welcome-header">
                    <top-icon class="icon-work_cancel"></top-icon>
                </div>
                <div class="welcome-content">
                    <h3>
                        <span class="highlight">${opts.userName}</span> 님, 환영합니다.<br/>
                        <span class="text"></span>
                    </h3>
                    <p></p>
                </div>
            </div>
        `);
        this.backgroundLayer = htmlToElement(`<div class="welcome-background"></div>`);
    }

    WelcomeViewContainer.prototype.drawTo = function (parentNode) {
        const self = this;
        const opts = this.opts;
        const template = this.template;
        const backgroundLayer = this.backgroundLayer;

        template.querySelector('h3 span.text').innerHTML = opts.content.title;
        template.querySelector('p').innerHTML = opts.content.body;
		
		template.querySelector('.icon-work_cancel').addEventListener('click', function () {
            self.hide();
		});

        parentNode.appendChild(template);

        if (opts.bgUrl) {
            Object.assign(backgroundLayer.style, opts.bgStyle);
            backgroundLayer.style.backgroundImage = `url('${opts.bgUrl}')`;
            parentNode.appendChild(backgroundLayer);
        }
    }

    WelcomeViewContainer.prototype.hide = function () {
        const template = this.template;
        const backgroundLayer = this.backgroundLayer;
        const opts = this.opts;

        template.parentNode.removeChild(backgroundLayer);
        template.parentNode.removeChild(template);

        if (opts.onCloseCallback) {
            opts.onCloseCallback();
        }
    }

    exports.WelcomeView = WelcomeViewContainer;

})(this);

Top.Controller.create('btobSearchPwdFirstLayoutLogic', {
	init : function(event, widget) {
		this.mail;
		//event 선언
		tds('btobIdTextField').setProperties({"onKeyup":"btobPWIdText"})
		tds('btobSPNameTextfield').setProperties({"onKeyup":"btobPWNameText"})
		tds('btobSPMobileTextField').setProperties({"onKeyup":"btobPWMobileText"})
		tds('btobSPBtutton').setProperties({"on-click":"goPwd"})
		tds('BtobSPNationalSelectBox').setProperties({"on-change":"changeNationalBox"})
		//국제전화번호
		const SPdata = COUNTRY_CODES.map(value => {
			return {
				text: `${value.code} ${value.name}`,
				value: value.code
			}
		});
		tds("BtobSPNationalSelectBox").setProperties({"nodes":SPdata})
		tds("BtobSPNationalSelectBox").select("+82")
		
	},
	btobPWIdText : function(event, widget){
		tds('btobIdTextField').removeClass('alert')
		tds('btobIdTextField').addClass('solid')
		tds('btobSPIdNullIcon').setProperties({"visible":"none"})
	},
	btobPWNameText : function(event, widget){
		tds('btobSPNameTextfield').removeClass('alert')
		tds('btobSPNameTextfield').addClass('solid')
		tds('btobSPNameNullIcon').setProperties({"visible":"none"})
	},
	btobPWMobileText : function(event, widget){
		tds('btobSPMobileTextField').removeClass('alert')
		tds('btobSPMobileTextField').addClass('solid')
		tds('btobSPMobileNullIcon').setProperties({"visible":"none"})
	},
	goPwd : function(event ,widget){
		var idFlag = 0;
		var nameFlag = 0; 
		var mobileFlag = 0;
		
		//아이디 없을 경우
		if( tds('btobIdTextField').getText() === null || tds('btobIdTextField').getText().length === 0 ){
			idFlag = 1;
		}
		if(idFlag === 1){
			tds('btobIdTextField').addClass('alert')
			tds('btobSPIdNullIcon').setProperties({"visible":"visible"})
			setTimeout(function() {
				Top.Dom.selectById("LoginIdErrorPopover").open()
    				}, 100);
		}
		//이름 없을 경우
		if( tds('btobSPNameTextfield').getText() === null || tds('btobSPNameTextfield').getText().length === 0 ){
			nameFlag = 1;
		}
		if(nameFlag === 1){
			tds('btobSPNameTextfield').addClass('alert')
			tds('btobSPNameNullIcon').setProperties({"visible":"visible"})
			setTimeout(function() {
				Top.Dom.selectById("NameErrorPopover").open()
    				}, 100);
		}
		//휴대폰  없을 경우
		if( tds('btobSPMobileTextField').getText() === null || tds('btobSPMobileTextField').getText().length === 0 ){
			mobileFlag = 1;
		}
		if(mobileFlag === 1){
			tds('btobSPMobileTextField').addClass('alert')
			tds('btobSPMobileNullIcon').setProperties({"visible":"visible"})
			setTimeout(function() {
				Top.Dom.selectById("MobileNullPopover").open()
    				}, 100);
		}
		
		
		if(idFlag || nameFlag || mobileFlag){
			return false
		}
		
		
		
		//b2b 비밀번호 찾는 서비스
		$.ajax({
			url: _workspace.url+'Users/B2BUserPwUpdate?action=', //Service Object
			type: 'POST',
			dataType: 'json',
			crossDomain: true,
			contentType : "application/json",
			xhrFields: {
				
				withCredentials: true
				
			},
			data: JSON.stringify({
				"dto": {
					"USER_NAME": tds('btobSPNameTextfield').getText(),
					"USER_PHONE" :  tds('btobSPMobileTextField').getText(),
					"USER_LOGIN_ID" :  tds('btobIdTextField').getText(),
					"NATIONAL_CODE" : tds('BtobSPNationalSelectBox').getSelected().value
				}
			}),
			success: function(result) {
				// RESULT_CD가 RST0001
				if(result.dto.RESULT_CD === "RST0001"){
					//해당 아이디 있음
					Top.Dom.selectById("btobSPInfoLayout").src('btobSearchPwdSecondContentLayout.html'+ verCsp());
					Top.Dom.selectById("btobSPContentLayout2").src('btobSearchPwdSecondButtonLayout.html'+ verCsp());
					 Top.Controller.get('btobSearchPwdFirstLayoutLogic').mail = result.dto.RESULT_MSG 
				
//					Top.Dom.selectById("LinearLayout4949_1").src('searchIdSecondContentLayout.html');
//					Top.Dom.selectById("Master2Layout").src('searchIdSecondButtonLayout.html');
		
		
				}else{
					//해당아이디 없음
					Top.Dom.selectById("btobSPInfoLayout").src('btobSearchPwdThirdContentLayout.html'+ verCsp());
					Top.Dom.selectById("btobSPContentLayout2").src('btobSearchPwdThirdButtonLayout.html'+ verCsp());
				} 
				
					
			},
			error: function(error) {

			}
		})   
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	},
	changeNationalBox: function(event, widget){
		$('#BtobSPNationalSelectBox span.top-selectbox-container')[0].lastChild.textContent = tds("BtobSPNationalSelectBox").getSelected().value
	}
	
});

Top.Controller.create('btobSearchPwdThirdButtonLayoutLogic', {
	goback : function(event, widget) {
		tds('btobSearchPwdFirstLayout').src('btobSearchPwdFirstLayout.html'+ verCsp())
	}
});

Top.Controller.create('btobSearchPwdSecondContentLayoutLogic', {
	init : function(event, widget) {
		 
		//이메일 힌트 작성
		var hideMail = Top.Controller.get('btobSearchPwdFirstLayoutLogic').mail;
		var hideMailArray = hideMail.split('@');
		var behindArray = hideMailArray[1].split('.');
		var arrlength = behindArray.length
		var total_mail = 0;
		
		for(var h=3; h<hideMailArray[0].length;h++){
			
			hideMailArray[0] = hideMailArray[0].replaceAt(h,'*'); 
	
		}
		for(var h=1; h<(arrlength);h++){
			behindArray[h] = '.'+behindArray[h];
		}
		for(var k =1; k<(arrlength); k++){
			if( k >> 1){total_mail = behindArray[1] + behindArray[k] }
			else{
			total_mail =  behindArray[k]
			}
		}
		for(var h=1; h<behindArray[0].length;h++){
			
			behindArray[0] =behindArray[0].replaceAt(h,'*'); 
	
		}
		
		
		var user_mail = hideMailArray[0] + "@" +behindArray[0]+ total_mail;
		tds('TextView56').setText(`회원님의 계정에 등록된 이메일 주소<br><span style =" color : #776AF4";>${user_mail}</span>(으)로<br>임시 비밀번호가 전송되었습니다.`)
	}
});

Top.Controller.create('btobSearchPwdSecondButtonLayoutLogic', {
	completeSP : function(event, widget) {
		tds('BtobPwChangeDialog').close(true)
	}
});




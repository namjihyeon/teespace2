
Top.Controller.create('btobSearchIdFirstLayoutLogic', {
	init : function(event, widget) {
		this.outId
		//keyup event 선언
		tds('btobNameTextField').setProperties({"onKeyup":"btobNameText"})
		tds('btobMobileTextField').setProperties({"onKeyup":"btobMobileText"})
		tds('btobEmployTextfield').setProperties({"onKeyup":"btobEmployText"})
		//국제전화번호
		const SIdata = COUNTRY_CODES.map(value => {
			return {
				text: `${value.code} ${value.name}`,
				value: value.code
			}
		});
		tds("BtobSINationalSelectBox").setProperties({"nodes":SIdata})
		tds("BtobSINationalSelectBox").select("+82")
		
	
	
	},
	btobNameText : function(event, widget){
		tds('btobNameTextField').removeClass('alert')
		tds('btobNameTextField').addClass('solid')
		tds('btobNameErrorIcon').setProperties({"visible":"none"})
	},
	btobMobileText : function(event, widget){
		tds('btobMobileTextField').removeClass('alert')
		tds('btobMobileTextField').addClass('solid')
		tds('btobMobileNullIcon').setProperties({"visible":"none"})
	
	},
	btobEmployText : function(event, widget){
		tds('btobEmployTextfield').removeClass('alert')
		tds('btobEmployTextfield').addClass('solid')
		tds('btobEmployNullIcon').setProperties({"visible":"none"})
	},
	//아이디 찾기 버튼 클릭
	btobSIBtnClick : function(event,widget){
		var nameFlag = 0;
		var mobileFlag1 = 0;
		var employFlag = 0;
		
		//이름 없을 경우
		if( tds('btobNameTextField').getText() === null || tds('btobNameTextField').getText().length === 0 ){
			nameFlag = 1;
		}
		if(nameFlag === 1){
			tds('btobNameTextField').addClass('alert')
			tds('btobNameErrorIcon').setProperties({"visible":"visible"})
			setTimeout(function() {
				Top.Dom.selectById("NameErrorPopover").open()
    				}, 100);
		}
		var btobMN = tds('btobMobileTextField').getText();
		//휴대폰번호 없을 경우
		if( btobMN === null || btobMN.length === 0 ){
			mobileFlag1 = 1;
		}
		if(mobileFlag1 === 1){
			tds('btobMobileTextField').addClass('alert')
			tds('btobMobileNullIcon').setProperties({"visible":"visible"})
			setTimeout(function() {
				Top.Dom.selectById("MobileNullPopover").open()
    				}, 100);
		}
		
		
		//사번이 없을 경우
		if(tds('btobEmployTextfield').getText() === null || tds('btobEmployTextfield').getText().length === 0 ){
			employFlag  = 1;
		}
		if(employFlag === 1){
			tds('btobEmployTextfield').addClass('alert')
			tds('btobEmployNullIcon').setProperties({"visible":"visible"})
			setTimeout(function() {
				Top.Dom.selectById("EmployNumNullPopover").open()
    				}, 100);
		}
		
		
		
		
		if(nameFlag || mobileFlag1 ||employFlag ){
			return false
		}
		
		var b2bNcode = tds('BtobSINationalSelectBox').getSelected().value;
		
		//b2b 아이디 찾는 서비스
		$.ajax({
			url: _workspace.url+'Users/B2BUserId', //Service Object
			type: 'GET',
			dataType: 'json',
			crossDomain: true,
			contentType : "application/json",
			xhrFields: {
				
				withCredentials: true
				
			},
			data: JSON.stringify({
				"dto": {
					"USER_NAME": tds('btobNameTextField').getText(),
					"USER_PHONE" :  tds('btobMobileTextField').getText(),
					"USER_EMPLOYEE_NUM" :  tds('btobEmployTextfield').getText(),
					"NATIONAL_CODE" : tds('BtobSINationalSelectBox').getSelected().value
				}
			}),
			success: function(result) {
	
				if(result.dto.dtos.length!=0){
					//해당 아이디 있음
					rst = result.dto.dtos;
					console.log('아이디 있음')
//					
					Top.Dom.selectById("btobSIInfoLayout").src('btobSearchIdSecondContentLayout.html'+ verCsp());
					Top.Dom.selectById("btobSIContentLayout2").src('btobSearchIdSecondButtonLayout.html'+ verCsp());
					Top.Controller.get('btobSearchIdFirstLayoutLogic').outId = result.dto.dtos[0].USER_LOGIN_ID;
		
				}else{
					console.log('아이디 존재 하지 않습니다')
					//해당아이디 없음
					Top.Dom.selectById("btobSIInfoLayout").src('btobSearchIdThirdContentLayout.html'+ verCsp());
					Top.Dom.selectById("btobSIContentLayout2").src('btobSearchIdThirdButtonLayout.html'+ verCsp());
				}
	
			},
			error: function(error) {

			}
		})   			
		
		
		
		
		
		
		
		
		
	}, 
	btobNationalChange : function(event, widget) {
		$('#BtobSINationalSelectBox span.top-selectbox-container')[0].lastChild.textContent = tds("BtobSINationalSelectBox").getSelected().value
	}
	
});

Top.Controller.create('btobSearchIdThirdButtonLayoutLogic', {
	gobackSI : function(event, widget) {
		tds('btobSearchIdFirstLayout').src('btobSearchIdFirstLayout.html'+ verCsp())
	}
});

Top.Controller.create('btobSearchIdSecondButtonLayoutLogic', {
	completeSI : function(event, widget) {
		tds('BtobIdSearchDialog').close(true)
	}, goSP : function(event, widget) {
		tds('BtobIdSearchDialog').close(true)
		tds('BtobPwChangeDialog').open()
	}
	
});

Top.Controller.create('btobSearchIdSecondContentLayoutLogic', {
	init : function(event, widget) {
		tds('TextView251').setText(Top.Controller.get('btobSearchIdFirstLayoutLogic').outId)
	}
});




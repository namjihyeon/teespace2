/* 오피스 내 공동 사용 함수 */
function getCurrentDateAndTime(dayJoiner, timeJoiner) {
    // 예시 2011?22?33 11?22?33
    //	(new Date()).toJSON().slice(0, 19).replace(/[-T]/g, ':')
    if (typeof dayJoiner === "undefined") {
        var dayJoiner = ".";
    }
    if (typeof timeJoiner === "undefined") {
        var timeJoiner = ":";
    }
    var now = new Date();
    var year = now.getFullYear();
    var month = ((now.getMonth() + 1) < 10 ? "0" : "") + (now.getMonth() + 1);
    var day = (now.getDate() < 10 ? "0" : "") + now.getDate();
    var hour = (now.getHours() < 10 ? "0" : "") + now.getHours();
    var minute = (now.getMinutes() < 10 ? "0" : "") + now.getMinutes();
    var second = (now.getSeconds() < 10 ? "0" : "") + now.getSeconds();
    var dateStr = year + dayJoiner + month + dayJoiner + day + " " + hour + timeJoiner + minute + timeJoiner + second;
    return dateStr;
}

function getTimeArr(time) {
    // time yyyy?mm?dd? hh?mm?ss -> [year, month, day, hour, minute, second]
    if (!time) {
        return [];
    }
    var timeArr = time
        .split(" ").join("/")
        .split('!').join("/")
        .split('@').join("/")
        .split('?').join("/")
        .split('-').join("/")
        .split(":").join("/")
        .split("/");

    var year = timeArr[0];
    var month = (timeArr[1] < 10 ? "0" : "") + parseInt(timeArr[1]);
    var day = (timeArr[2] < 10 ? "0" : "") + parseInt(timeArr[2]);
    var hour = (timeArr[3] < 10 ? "0" : "") + parseInt(timeArr[3]);
    var minute = (timeArr[4] < 10 ? "0" : "") + parseInt(timeArr[4]);
    var second = (timeArr[5] < 10 ? "0" : "") + parseInt(timeArr[5]);

    var timeArr = [year, month, day, hour, minute, second];
    return timeArr;
}

function getTimeStr(time) {
    // time yyyy?mm?dd? hh?mm?ss -> yyyy-mm-dd hh:mm:ss
    var timeArr = getTimeArr(time);
    var timeStr = timeArr[0] + "-" + timeArr[1] + "-" + timeArr[2] + " " + timeArr[3] + ":" + timeArr[4] + ":" + timeArr[5];
    return timeStr;
}

function adaptiveTimeOf(time) {
    // 	2019-7-12 11:22:33 -> 오전 11:22

    //- this year
    //	- today
    //		- before noon
    //			오전 h:mm
    //		- afternoon
    //			오후 h:mm
    //	- not today
    //		- before noon
    //			mm.dd 오전 h:mm
    //		- afternoon
    //			mm.dd 오후 h:mm
    //- not this year
    //	- before noon
    //		yyyy.mm.dd 오전 h:mm
    //	- afternoon
    //		yyyy.mm.dd 오후 h:mm

    var timeArr = getTimeArr(time);
    if (timeArr.length !== 6) {
        return '';
    }
    var now = new Date();
    var year = timeArr[0];
    var month = timeArr[1];
    var day = timeArr[2];
    var hour = timeArr[3];
    var minute = timeArr[4];
    var second = timeArr[5];
    var timeStr = "";
    var AM = "오전";
    var PM = "오후";
    var AM_ = AM + " ";
    var PM_ = PM + " ";
    var _AM_ = " " + AM + " ";
    var _PM_ = " " + PM + " ";

    //	24:00:ss -> 오전 0:00
    //	11:59:ss -> 오전 11:59
    //	12:00:ss -> 오후 12:00
    //	23:59:ss -> 오후 11:59

    if (parseInt(hour) === 0 || parseInt(hour) === 24) {
        timeStr = AM_ + "12:" + minute;
    } else if (parseInt(hour) === 12) {
        timeStr = PM_ + "12:" + minute;
    } else if (parseInt(hour) < 12) {
        timeStr = AM_ + hour + ":" + minute;
    } else {
    	if(parseInt(hour) - 12 >= 10){
    		timeStr = PM_ + (parseInt(hour) - 12) + ":" + minute;
    		}
    	else{
    		 timeStr = PM_ + "0" + (parseInt(hour) - 12) + ":" + minute;
    	}
       
    }
    if (year === String(now.getFullYear())) { // this year
        if (!((Number(month) === now.getMonth() + 1) &&
                (Number(day) === now.getDate()))) { // not today
            timeStr = month + "." + day + " " + timeStr;
        }
    } else {
        timeStr = year + "." + month + "." + day + " " + timeStr;
    }
    return timeStr;
}

function multiSort(array, sortObject = {}) {
    // e.g., sortObject = {key1: -1, key2: 1, key3: 0}; where 1 for ASC and -1 for DESC
    const sortKeys = Object.keys(sortObject);

    // Return array if no sort object is supplied.
    if (!sortKeys.length) {
        return array;
    }

    const keySort = (a, b, d) => {
        d = (d !== null) ? d : 1; // ascending order as default
        return (a>b) ? d : (a<b) ? -d : 0;
    };

    return array.sort((a, b) => {
        let sorted = 0;
        let index = 0;

        // Loop until sorted (-1 or 1) or until the sort keys have been
        // processed.
        while ((sorted === 0) && (index < sortKeys.length)) {
            const key = sortKeys[index];

            if (key) {
                const direction = sortObject[key];

                sorted = keySort(a[key], b[key], direction);
                index++;
            }
        }

        return sorted;
    });
}


function checkOfficeNameValidity(type, newName, oldName) {
    var validity = true;
    var msg = '';

    var fileHasUnallowableCharMsg = '특수 기호 \/:"?*<>|는 사용할 수 없습니다.';
    var folderHasUnallowableCharMsg = '폴더 이름에는 다음 문자를 사용할 수 없습니다.\\ /: * ? \" < > |';

    var fileIsBlankMsg = '파일 이름은 공백이 될 수 없습니다.';
    var folderIsBlankMsg = '폴더 이름은 공백이 될 수 없습니다.';

    var fileExceedMaxLengthMsg = '파일명이 70자를 넘는 경우 업로드 할 수 없습니다.';
    var folderExceedMaxLengthMsg = '폴더 최대 길이를 넘을 수 없습니다.';

    var fileSameNameMsg = '파일 이름이 동일합니다.';
    var folderSameNameMsg = '폴더 이름이 동일합니다.';

    function file_hasUnallowableChar(name) {
        var regex = /[\\/:*?"<>|]/g;
        return regex.test(name);
    }

    function folder_hasUnallowableChar(name) {
        var regex = /[\\/:*?"<>|]/g;
        return regex.test(name);
    }

    function file_isBlank(name) {
        name = name
            .split(' ').join('')
            .split('\t').join('')
            .split('\n').join('');
        return (name.length === 0)
    }

    function folder_isBlank(name) {
        name = name
            .split(' ').join('')
            .split('\t').join('')
            .split('\n').join('');
        return (name.length === 0)
    }

    function file_exceedMaxLength(name) {
        return (name.length > officeConfig.maxLengthFileName)
    }

    function folder_exceedMaxLength(name) {
        return (name.length > officeConfig.maxLengthFolderName)
    }

    function file_sameName(newName, oldname) {
        return (newName === oldName);
    }

    function folder_sameName(newName, oldname) {
        return (newName === oldName);
    }

    // **************************************** //
    if(newName){
    if (type === 'file') {
        if (file_hasUnallowableChar(newName)) {
            validity = false;
            msg = fileHasUnallowableCharMsg;
        }
        if (file_isBlank(newName)) {
            validity = false;
            //msg = fileIsBlankMsg;
        }
        if (file_exceedMaxLength(newName)) {
            validity = false;
            msg = fileExceedMaxLengthMsg;
        }
//        if (typeof oldName !== "undefined") {
//            if (file_sameName(newName, oldName)) {
//                validity = false;
//                msg = fileSameNameMsg;
//            }
//        }
    } else if (type === 'folder') {
        if (folder_hasUnallowableChar(newName)) {
            validity = false;
            msg = folderHasUnallowableCharMsg;
        }
        if (folder_isBlank(newName)) {
            validity = false;
            msg = folderIsBlankMsg;
        }
        if (folder_exceedMaxLength(newName)) {
            validity = false;
            msg = folderExceedMaxLengthMsg;
        }
        if (typeof oldName !== "undefined") {
            if (folder_sameName(newName, oldName)) {
                validity = false;
                msg = folderSameNameMsg;
            }
        }
    }
    }

    return {
        validity: validity,
        msg: msg
    };
}


/* 간단한 parsing script, 편의성 함수들 등 */
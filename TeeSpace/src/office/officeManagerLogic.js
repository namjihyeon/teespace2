//Office 총괄 관리 매니저
function getBrowserExpandMode(){
	return officeManager.getBrowserExpandMode();
}
let connectInfo = {};

var officeManager = (function(){
	let dialogInfo=new Map();
	const officeConfig = {
		    rootNodeId: 'root',
		    rootNodePath: '/',
		    usingLoader: true,
		    loaderMaxTimeout: 5000,
		    maxLengthFileName: 70,
		    maxLengthFolderName: 100,
		    maxByteFileName: 200,
		    maxByteFolderName: 200,
		    maxLengthPath: 100,
		    maxBytePath: 200,
		    maxLoadFiles: 9999,
		    workingUserRequestPeriod: 2000,
		    defaultSearchType: '이름',
		    fileServerId: '127.0.1.1',
		    iconColorList: ["#FFFF00", "#999900", "#555500", "#222200"],
		    officeType: ["toword", "tocell", "topoint"],
		    officeDefaultFileName: {
		        "toword": "새 문서",
		        "tocell": "새 통합문서",
		        "topoint": "새 프레젠테이션"
		    },
		    officeImageViewId: ["toword", "tocell", "topoint"],
		    // Available extensions in MS office 10
		    // docx, docm, doc, dotx, dotm, dot, pdf, xps, mht, mhtml, htm, html, rtf, txt, xml, odt
		    // xlsx, xlsm, xlsb, xls, xml, mht, mhtml, htm, html, xltx, xltm, xlt, txt, xml, xls, csv, prn, dif, slk, xlam, xla, pdf, xps, xlsx, ods
		    // pptx, pptm, ptt, pdf, xps, potx, potm, pot, thmx, ppsx, ppsm, pps, ppam, ppa, xml, mp4, wmv, gif, jpg, png, tif, bmp, wmf, emf, rtf, odp
		    extension: {
		        "dir": ["dir"],
		        "toword": ["toc", "doc", "docx"],
		        "tocell": ["tls", "xls", "xlsx","csv"],
		        "topoint": ["tpt", "ppt", "pptx"]
		    },
	};
	
	function __popupCheck(){
		var s_val = window.location.hash;
		if(s_val.indexOf('mini')!==-1) return true
		else return false;
	}
	
	function __getOpenedDocInfo(){
		if(!!sessionStorage.office){
			var j_officeInfo = JSON.parse(sessionStorage.office);
			if(appManager.getSubApp()==='toword'){
				return j_officeInfo.ToWord;
			}else if(appManager.getSubApp()==='tocell'){
				return j_officeInfo.ToCell;
			}else if(appManager.getSubApp()==='topoint'){
				return j_officeInfo.ToPoint;
			}
			return false;
		}else{
			return false;	
		}
		
	}

	
	function __dialogReady(p_target){
		switch(p_target){
		case 'UnknownError':
			var info = dialogInfo.get(p_target);
			Top.Controller.get('officeUnknownErrorLayoutLogic').setErrorCode(info.errorCode,info.comLogNum);
			break;
		case 'ConnectionError':
			var info = dialogInfo.get(p_target);
			Top.Controller.get('officeServerConnectErrorLayoutLogic').setErrorCode(info.errorCode,info.comLogNum);
			break;
		default:
			break;
		}		
	}
	
	function __setDialogInfo(p_target, p_info){
		dialogInfo.set(p_target,p_info);	
	}
	
	//Parm: officeDocModel참고
	//Desc: officeDocModel형태 객체 반환
	//Ret: officeDocModel Obj
	function __newOfficeDocInfo(p_info){
		var o_officeInfo={
				'userId':!!!p_info.userId?p_info.userId:userManager.getLoginUserId(),
				'userName':!!!p_info.userName?p_info.userName:userManager.getLoginUserName(),
				'wsId':!!!p_info.wsId?p_info.wsId:getRoomIdByUrl(),
				'target':p_info.target,
				'officeType':!!!p_info.officeType?p_info.officeType:appManager.getSubApp(),
				'filePathInStorage':p_info.filePathInStorage,
				'cloudOfficeUrl':p_info.cloudOfficeUrl,
				'fileId':p_info.fileId,
				'fileName':p_info.fileName,
				'fileNameWithExtension':p_info.fileNameWithExtension,
				'fileExtension':p_info.fileExtension,
				'fileOpenTime':p_info.fileOpenTime,
				'getWorkingUserRequestPeriod':p_info.getWorkingUserRequestPeriod,
				'fileHash':p_info.fileHash,
				'comInfo':p_info.comInfo,
				'newFileFlag':p_info.newFileFlag,
				'fileChannelId':p_info.fileChannelId,
				'fileParentId':p_info.fileParentId,
				'fileParentPath':p_info.fileParentPath,
				'fileSize':p_info.fileSize,
				'wasBinary':p_info.wasBinary,
				'roomId':'0',//legecyParameter
				'whichChannel':p_info.whichChannel
		}
				
		return o_officeInfo;
	}
	
	function __routeToOffice(p_appType,p_data,p_target){
		var renderTarget, appWrapper, isRendered;
        if(__popupCheck()){
        	//Popup Logic
        	appWrapper = "officeMainLayout";
            renderTarget = "MainTeeSpacePage";
            officeManager.removeChildrenNodes("top-page#MainTeeSpacePage");
			tds('MainTeeSpacePage').src("officeMainLayout.html");
			
            
            
			drive.setDriveInfo(workspaceManager.getWorkspaceId(getRoomIdByUrl(), "CHN0006"),workspaceManager.getChannelList(getRoomIdByUrl(), "CHN0006"));
			 isRendered = !!document.querySelector(`div#${appWrapper}`);
             if (!isRendered) {
                 tds(renderTarget).src(`${appWrapper}.html${tsVersion}`);
             }
             return true;
        }else{
        	//Subapp Logic
        	appWrapper = "officeMainLayout";
            renderTarget = p_target;
            isRendered = !!document.querySelector(`div#${renderTarget} div#${appWrapper}`);
            if (!isRendered) {
                // TODO : 여기에 office rendering 로직 넣어주세요.
                // ex) office.render(renderTarget);
                tds(renderTarget).src(`${appWrapper}.html${tsVersion}`);
            }
            setTimeout(function() {
                renderSubApp(p_data);
            }, 0);
            return false;
        }
        
	}
	
	//Parm: file의 id, 이름, 확장자 set
	//Ret: True(정상)/False(실패)
	function __callOffice(data){
		/*	본 함수는 외부에서 오피스 앱을 열고자 할 때 사용하는 함수입니다. 
		 *  data에 아래 형식대로 파일 정보 입력 후, callOffice를 호출하면
		 *  라우팅을 오피스 앱으로 태워 보내주는 방식입니다.
		 *  티워드/티포인트/티셀 구분은 파일 extension을 통해 판별해줍니다.
		 */		
		officeContext.docList[0] = __newOfficeDocInfo({
			fileId : data.fileId,
			fileName : data.fileName,
			fileExtension : data.fileExtension,
			fileNameWithExtension : data.fileName + '.' + data.fileExtension,
			whichChannel : !!data.whichChannel?data.whichChannel:2
		})
				
		//route to office
		var roomId = getRoomIdByUrl();
	    var mainApp = appManager.getMainApp();
	    var targetUrl;
	    //distinguish app type by extension
	    if(officeConfig.extension.toword.includes(data.fileExtension)){
	    	officeContext.docList[0].officeType='toword';
	    	targetUrl = `${rootUrl}s/${getRoomIdByUrl(location.hash)}/toword?mini=true`;
	    }
	    else if(officeConfig.extension.topoint.includes(data.fileExtension)){
	    	officeContext.docList[0].officeType='topoint';
	    	targetUrl = `${rootUrl}s/${getRoomIdByUrl(location.hash)}/topoint?mini=true`;
	    }
	    else if(officeConfig.extension.tocell.includes(data.fileExtension)){
	    	officeContext.docList[0].officeType='tocell';
	    	targetUrl = `${rootUrl}s/${getRoomIdByUrl(location.hash)}/tocell?mini=true`;
	    }
	    else{
	    	console.log("unsupported file type : " + data.fileExtension);
	    	return false;
	    }
	    var newOfficeTab = window.open( 
                targetUrl,'pop','menubar=nu,toolbar=no'
        );
	    return newOfficeTab;
	}

	//Param: 렌더링할 Dom, Dom상위 Layout, {tocell,topoint,toword}, 렌더링 여부
	function __renderOffice(p_renderTarget, p_appWrapper, p_appType, p_isRendered){
		/*	본 함수는 오피스로 routing 태운 후, 렌더링을 위한 함수입니다.
		 *  이미 오피스 앱이 열려있다면 무시, 다른 앱이 열려있는 상태일 경우
		 *  새로 오피스 앱을 호출하는 방식입니다.
		 */
		var ctrl = Top.Controller.get('officeMainLogic');
		if(!p_isRendered||(ctrl.office_type !== p_appType)||(ctrl.roomId != getRoomIdByUrl()) ){ //room has been changed, so re-render
				tds(p_renderTarget).onSrcLoad(function(){
					ctrl.init_office(p_appType);
				});
				tds(p_renderTarget).src(`${p_appWrapper}.html`);
		}
	}
	//Parm: 파일의 id, 이름, 확장자(문자열)
	//Desc: '다른 이름 저장' 시 호출, TeeDrive에 파일 저장용 함수
	function __saveOffice(p_fileId, p_fileName, p_fileExt){	
		/*	본 함수는 '다른 이름으로 저장'시 호출하는 함수 입니다.
		 *  본 함수를 통해 TeeDrive에 파일을 저장합니다. */	
			SAVE_TO_TDRIVE({
			    fileId       : p_fileId,// 저장하고자 하는 파일 id,
			    fileName  : p_fileName,// 기존 파일 이름,
			    fileExt      : p_fileExt,// 저장하고자 하는 파일 확장자,
			    successCallback : function(fileMeta) {
			    	//resolve(true);
			    	
			    	//pass global variable to office workarea
			    	officeContext.docList[0] = __newOfficeDocInfo({
			    		fileId : fileMeta.storageFileInfo.file_id,
		    			fileName : fileMeta.file_name,
		    			fileExtension : fileMeta.file_extension,
		    			fileNameWithExtension : fileMeta.file_name + '.' + fileMeta.file_extension, 
		    			whichChannel : 2 //T-Drive
					})
			    	
	                //call initWorkArea to reconnect to new file
			        officePeer.clearAllPeerConnection();
			    	var ctrl = Top.Controller.get('officeWorkAreaLogic');
			    	ctrl.initWorkArea(Top.Controller.get('officeMainLogic').app_type);
			    // 각 앱에서 필요한 것
			    },
			    cancelCallback  : function(fileMeta) {
			    	//resolve(false);
			    	//do nothing
			    // 각 앱에서 필요한 것
			    }
			});
	}
	
	//Parm: 앱 종류(문자열 toword, topoint, tocell)
	//Desc: 새로운 문서 생성
	//Ret: 생성된 문서 정보, file의 id, name, extension, name+extension(문자열) 
	function __newOfficeDoc(p_appType){
		var fileName;
		let success = true;
    	var fildId;
    	var fileNameWithExtension;
    	var fileExtension;
		switch (p_appType) {
            case officeConfig.officeImageViewId[0]:
                fileExtension = 'docx';
                break;
            case officeConfig.officeImageViewId[1]:
                fileExtension = 'xlsx';
                break;
            case officeConfig.officeImageViewId[2]:
                fileExtension = 'pptx';
                break;
            default:
                console.info("## improper access");
                return;
        }
        
        if (officeConfig.extension.toword.includes(fileExtension)) {
        	fileName = officeConfig.officeDefaultFileName["toword"];
        } else if (officeConfig.extension.tocell.includes(fileExtension)) {
        	fileName = officeConfig.officeDefaultFileName["tocell"];
        } else if (officeConfig.extension.topoint.includes(fileExtension)) {
        	fileName = officeConfig.officeDefaultFileName["topoint"];
        } else {
        	console.info("## improper file name");
        	return false;
        }

        $.ajax({
    		url: _workspace.url + "Office/Uuid",
    		type: 'POST',
    		dataType: 'json',
    		contentType: 'application/json; charset=utf-8',
    		xhrFields: {
    			withCredentials: true
    		},
    		crossDomain: true,
    		async: false,
    		success: function (data, msg, state) {
    			fileId = data.dto.FILE_ID;
    		},
    		error: function (data, msg, state) {
    			console.info("새문서의 파일 아이디 발급을 실패했습니다.");
    			success = false;
    		}
    	})
    	if(!success){
    		return false;
    	}
        fileNameWithExtension = fileName + "." + fileExtension;
//        fileId = talkUtil.generateGUID();		// 추후에 규민님 통해 ServiceCall로 File_ID를 받는 (주소지를 받는거) 그리고 나서 내 서비스에서 그 파일 아이디로 메타 등록!
    	return {
    		'fileId':fileId,
    		'fileName':fileName,
    		'fileExtension':fileExtension,
    		'fileNameWithExtension':fileNameWithExtension
    	}
	}
	
	//Desc:확장 상태 반환
	//Ret: NONE:0, DESKTOP:1, WORKAREA:2, TOOLPANE:3, MOBILE:4
	function __getBrowserExpandMode(){
		var officeWebView = Top.Dom.selectById("officeEditWebView").getElement().contentWindow;
		if(appManager.isExpanded()){
			return officeWebView.officeLayoutMode.WORKAREA;
		}else{
			return officeWebView.officeLayoutMode.TOOLPANE;		
		}
	}

	//Parm: 파일 확장자(문자열)
	//Desc: 사용 가능한 확장자인지 확인용
	//Ret: True/False
	function __isOfficeCompatibleFile(fileExtension) {
	    if( !fileExtension )
	        return false;
	    let list = []
	        .concat(officeConfig.extension.toword)
	        .concat(officeConfig.extension.tocell)
	        .concat(officeConfig.extension.topoint);
	    return list.includes(fileExtension.toLowerCase());
	}
	
	function __editOffice(event, widget, data) {
		    // from which channel the api called. (data["whichChannel"]): 새파일 열기(0), 테이블에서 선택(1), 티드라이브에서 열기(2), 공유링크로 열기(3)
		    let whichChannel = undefined;
		    let newFileFlag = false;
		    let userId = userManager.getLoginUserId();
		    let userName = userManager.getLoginUserName();
		    let fileId = "";
		    let fileName = "";
		    let fileNameWithExtension = "";
		    let fileExtension = "";
		    let officeType = "";
		
		    let workspaceURL = getRoomIdByUrl();

		    let roomUrl = '0';
		    let info = workspaceManager.getWorkspaceAndChannelId(workspaceURL,roomUrl,TEESPACE_CHANNELS.TDRIVE.CHANNEL_TYPE);

		
		    let workspaceId = info.workspaceId;
		    let chId=info.channelId;
		
		    let comInfo = "";
		    let fileHash = "";
		    let filePathInStorage = "";
		    let cloudOfficeUri = "";
		    let target = "";
		    // let wsName = workspaceManager.getWorkspace(workspaceURL).WS_NAME;
		    // let wsRole = userManager.getWorkspaceUser(workspaceId)[0].WS_ROLE;
		    let fileOpenTime = getCurrentDateAndTime();
		    let workingUserRequestPeriod = officeConfig.workingUserRequestPeriod;
		    // let errorType;
		    let tmpOfficeSessionStorage = {};
		    // let chId = getOfficeChannelId();
		
		    //      let fileParentId = JSON.parse(sessionStorage.pwd).id;
		    //      let fileParentPath = JSON.parse(sessionStorage.pwd).file_parent_path;
		    let fileParentId = officeConfig.rootNodeId; // New office file is saved in root node in Tdrive
		    let fileParentPath = officeConfig.rootNodePath;
		    let newFileInfo = {};
		
		    try {
		        whichChannel = data.whichChannel;
		    } catch (e) {
		        console.info("## invalid input channel: ", whichChannel);
		        return false;
		    }
		
		    if (whichChannel === 0) {
		    	var t_office=__newOfficeDoc(widget.id);
		    	if(!!t_office){
		    	newFileFlag = true;
		        fileId = t_office.fileId;
		        fileName = t_office.fileName;
		        fileExtension = t_office.fileExtension;
		        fileNameWithExtension = t_office.fileNameWithExtension;
		    	}
		    } else if (whichChannel === 1) {
		        // An existing file	
		        newFileFlag = false;
		        fileId = data.fileId;
		        fileName = data.fileName;
		        fileExtension = data.fileExtension;
		        fileNameWithExtension = data.fileNameWithExtension;
		    } else if (whichChannel === 2) {
		        // T-Drive
		        newFileFlag = false;
		        fileId = data.fileId;
		        fileName = data.fileName;
		        fileExtension = data.fileExtension;
		        fileNameWithExtension = data.fileName + '.' + data.fileExtension;
		    } else if (whichChannel === 3) {
		        // Sharing link
		        console.warn('## improper access to editOffice()');
		        return false;
		    } else if (whichChannel === 4) {
		        // Direct call
		        newFileFlag = false;
		        fileId = data.fileId;
		        fileName = data.fileName;
		        fileExtension = data.fileExtension;
		        fileNameWithExtension = data.fileNameWithExtension;
		    } else if (whichChannel === 5) {
		        // On pc
		        newFileFlag = false;
		        fileId = data.fileId;
		        fileName = data.fileName;
		        fileExtension = data.fileExtension;
		        fileNameWithExtension = data.fileNameWithExtension;
		    } else if (whichChannel === 5) {
		        // Open office file automatically after 'save as'
		        newFileFlag = false;
		        fileId = data.fileId;
		        fileName = data.fileName;
		        fileExtension = data.fileExtension;
		        fileNameWithExtension = data.fileNameWithExtension;
		    } else {
		        console.warn('## improper access to editOffice()');
		        return false;
		    }
		
		    // get 'office master info' and 'file hash'
		    $.ajax({
		        url: _workspace.url + "Office/OfficeFileOpen?action=",
		        type: 'POST',
		        dataType: 'json',
		        data: JSON.stringify({
		            "dto": {
		                "USER_ID": userId,
		                "FILE_ID": fileId,
		                //                    "WS_ID": wsId,
		                "FILE_CHANNEL_ID": chId,
		                "OFFICE_FILE_REQ_TIME": getCurrentDateAndTime(),
		                "OFFICE_FILE_REG_DATE": "_",
		                "OFFICE_FILE_MOD_DATE": "_"
		            }
		        }),
		        crossDomain: true,
		        contentType: 'application/json; charset=utf-8',
		        xhrFields: {
		            withCredentials: true
		        },
		        async: false,
		        success: function (data, msg, state) {
		        	console.log(data);
		            fileHash = data.dto.OFFICE_FILE_HASH;
		            comInfo = data.dto.OFFICE_MASTER_INFO; // alloc success: 32 digits UUID 
		            if (window.location.protocol.includes("https") === true) {
		                cloudOfficeUri = _webOffice.webSocketURL_ssl + comInfo;
		            } else {
		                cloudOfficeUri = _webOffice.webSocketURL + comInfo;
		                // cloudOfficeUri = _webOffice.webSocketURL + "bc732e4b-7db9-43ce-80f5-9f50f9fc8eb8";
		            }
		        },
		        error: function (data, msg, state) {
		            fileHash = "";
		            comInfo = "";
		        }
		    })
		
		    // COM allocation error check
		    if (comInfo === 'max file cnt') {
		        console.warn('## fail to allocate COM: exceed max number of opened file');
		        alertExceedResourceOffice();
		        return false;
		    }
		
		    if (comInfo === 'max people use file') {
		        console.warn('## fail to allocate COM: exceed coworking user limit');
		        alertExceedResourceOffice();
		        return false;
		    }
		
		    if (typeof comInfo === 'undefined') {
		        console.warn('## fail to allocate COM: undefined');
		        alertUnknownError();
		        return false;
		    }
		    if (comInfo === "MASTER_") {
		        console.warn('## fail to allocate COM: "MASTER_"');
		        alertUnknownError();
		        return false;
		    }
		    if (comInfo === null) {
		        console.warn('## fail to allocate COM: null');
		        console.warn("## COM: ");
		        alertUnknownError();
		        return false;
		    }
		    if (comInfo === "") {
		        console.warn('## fail to allocate COM: ""');
		        alertUnknownError();
		        return false;
		    }
		
		    if (officeConfig.extension.toword.includes(fileExtension.toLowerCase())) {
		        officeType = "ToWord";
		    } else if (officeConfig.extension.tocell.includes(fileExtension.toLowerCase())) {
		        officeType = "ToCell";
		    } else if (officeConfig.extension.topoint.includes(fileExtension.toLowerCase())) {
		        officeType = "ToPoint";
		    } else {
		        console.warn("## improper file extension");
		        alertUnsupportedFeature();
		        return false;
		    }
		
		    filePathInStorage = "/" + fileId + "/" + fileNameWithExtension;
		
		    var openOfficeFileInfo = {
		        "userId": userId,
		        "userName": userName,
		        "wsId": workspaceId,
		        "target": target,
		        "officeType": officeType,
		        "filePathInStorage": filePathInStorage,
		        "cloudOfficeUri": cloudOfficeUri,
		        "fileId": fileId,
		        "fileName": fileName,
		        "fileNameWithExtension": fileNameWithExtension,
		        "fileExtension": fileExtension,
		        "fileOpenTime": fileOpenTime,
		        "getWorkingUserRequestPeriod": workingUserRequestPeriod,
		        "fileHash": fileHash,
		        "comInfo": comInfo,
		        "newFileFlag": newFileFlag,
		        // ***************************** //
		        "fileChannelId": chId,
		        "fileParentId": fileParentId,
		        "fileParentPath": fileParentPath
		    };
		
		    if (typeof sessionStorage.office === "undefined") { // if ss is empty, create an office field and fill it
		        tmpOfficeSessionStorage = {};
		        tmpOfficeSessionStorage[officeType] = openOfficeFileInfo;
		        sessionStorage.setItem('office', JSON.stringify(tmpOfficeSessionStorage));
		    } else { 
		    	sessionStorage.removeItem('office'); //reset storage (we only use one item for now..)
		    	tmpOfficeSessionStorage = {};
		        tmpOfficeSessionStorage[officeType] = openOfficeFileInfo;
		        sessionStorage.setItem('office', JSON.stringify(tmpOfficeSessionStorage));
		    }
		    
		    return openOfficeFileInfo;

	}
	
	function __removeChildrenNodes(p_elem) {
    	while (p_elem.lastChild) {
    		p_elem.removeChild(p_elem.lastChild);
    	}
    }

	return {
		removeChildrenNodes: function(p_elem) {//DOM하위 요소 전체 제거
			return __removeChildrenNodes(p_elem)
        },
		isOfficeCompatibleFile: function(p_fileExtension) {//사용가능한 확장자 검사
			return __isOfficeCompatibleFile(p_fileExtension);
        },
        getBrowserExpandMode: function(){//현재 확대 상태
        	return __getBrowserExpandMode();
        },
        newOfficeDoc: function(p_appType){//새로운 문서
        	return __newOfficeDoc(p_appType);
        },
        saveOffice: function(p_fileId, p_fileName, p_fileExt){//다른 이름 저장
        	return __saveOffice(p_fileId, p_fileName, p_fileExt);
        },
        renderOffice: function(p_renderTarget, p_appWrapper, p_appType, p_isRendered){//오피스 렌더링
        	return __renderOffice(p_renderTarget, p_appWrapper, p_appType, p_isRendered);
        },
        callOffice: function(p_data){//외부앱에서 오피스 호출
        	return __callOffice(p_data);
        },
        newOfficeDocInfo: function(p_info){//새로운 오피스 데이터 모델 생성
        	return __newOfficeDocInfo(p_info);
        },
        editOffice: function(event, widget, data){//오피스 수정
        	return __editOffice(event, widget, data);
        },
        routeToOffice:function(p_appType,p_data,p_target){//오피스로 라우팅
        	return __routeToOffice(p_appType,p_data,p_target);
        },       
        popupCheck: function(){
        	return __popupCheck();
        },
        getOpenedDocInfo: function(){
        	return __getOpenedDocInfo();
        },
        dialogReady: function(p_target,p_col,p_code){
        	__dialogReady(p_target,p_col,p_code);
        },
        setDialogInfo:function(p_target,p_info){
        	__setDialogInfo(p_target,p_info);
        }
        
        
    };
})()

// TODO(200421, jaehyeon_park2) : office 코드 호환성을 위한 임시 코드
function getBrowserExpandMode(){
	return officeManager.getBrowserExpandMode()
}

function editOffice(event, widget, data){
	return officeManager.editOffice(event, widget, data)
}

function saveOffice(p_fileId, p_fileName, p_fileExt){
	return officeManager.saveOffice(p_fileId, p_fileName, p_fileExt)
}
////////////////////////////////////////////////////////////////////
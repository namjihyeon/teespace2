var conferenceService;
function __createConferenceService(){

conferenceService= {

		//임시코드
    	getSpaceorRoomId : function(){
        	let SpaceOrRoomId = null;
    			SpaceOrRoomId = getRoomIdByUrl();
        	return SpaceOrRoomId;
    	},


    data: {
        conferenceId: null,
        gatewayAddress: _workspace.gatewayURL,
        myInfo: null,
        roomList: [],
        workspaceUserList: [],
    },

    gateway: {
        getRoomAddress: function() {
            let url = _workspace.conferenceURL + "rooms/" 
                + conferenceManager.data.currentRoomInfo.room_id
                + "/RoomAddress?action=Get";
            return axios.get(url);
        }
    },

    user: {
        getUserInfo: function() {
            conferenceService.data.myInfo = userManager.getLoginUserInfo();
            return conferenceService.data.myInfo;
        },

        setRoomUserList: function(roomInfo) {
            conferenceManager.data.currentRoomInfo = roomInfo;
            conferenceService.gateway.getRoomAddress().then(function(response) {
                conferenceManager.data.gatewayAddress = response.data.dto.gateway_address;
            });
        },

        getWorkspaceUserList: function (workspaceId) {
        	//임시코드
        	let spaceOrRoomId = conferenceService.getSpaceorRoomId();
            const inputData = {
                "dto" : {
                        "WS_ID" : spaceOrRoomId,
                        "USER_ID" : conferenceService.user.getUserInfo().userId
                }
            };

            return axios.post(_workspace.url + "SpaceRoom/SpaceRoomMemberList?action=Get",inputData );
        },
    },

    signaling: {
        iceCandidateUpdate: function(roomId, targetUserId, ufrag, pwd) {
        	if (!roomId || !targetUserId || !ufrag || !pwd) {
        		console.error("conferenceService.signaling.iceCandidateUpdate is failed. check 'roomId', 'targetUserId', 'ufrag', 'pwd'.");
        		return;
        	}
        	
            let url = _workspace.conferenceURL + 
            "Signaling?action=Update";
            console.log("update ICE candidate");
            const answer_port = conferenceManager.data.gatewayAddress.split(":")[1] == undefined ? 19093 : conferenceManager.data.gatewayAddress.split(":")[1];
            const inputData = {
                "dto": {
                    "room_id": roomId,
                    "user_id": conferenceService.user.getUserInfo().userId,
                    "target_user_id": targetUserId,
                    "ufrag": ufrag,
                    "pwd": pwd,
                    "ice_ip": "",
                    "ice_port": "",
                    "gateway_port": answer_port,
                    "timestamp": new Date().getTime().toString(),
                    "priority": ""
                }
            };
            return axios.post(url, inputData);
        },

        officeIceCandidateUpdate: function(roomId, targetUserId, ufrag, pwd) {
        	if (!roomId || !targetUserId || !ufrag || !pwd) {
        		console.error("conferenceService.signaling.iceCandidateUpdate is failed. check 'roomId', 'targetUserId', 'ufrag', 'pwd'.");
        		return;
        	}
        	
            let url = _workspace.conferenceURL + 
            "Signaling?action=Update";
            console.log("update ICE candidate");
            const answer_port = _workspace.gatewayURL.split(":")[1] == undefined ? 19093 : _workspace.gatewayURL.split(":")[1];
            const inputData = {
                "dto": {
                    "room_id": roomId,
                    "user_id": conferenceService.user.getUserInfo().userId,
                    "target_user_id": targetUserId,
                    "ufrag": ufrag,
                    "pwd": pwd,
                    "ice_ip": "",
                    "ice_port": "",
                    "gateway_port": answer_port,
                    "timestamp": new Date().getTime().toString(),
                    "priority": ""
                }
            };
            return axios.post(url, inputData);
        },

        getGatewayPublicKey: function() {
            return new Promise(function(resolve, reject){
            if (conferenceManager.data.credentialData) {
                resolve(conferenceManager.data.credentialData);
            } else {
                let url = _workspace.conferenceURL + 
                "CredentialPublicKey?action=Get";

                let inputData = {
                    "dto": {
                        accesskey: "TConference"
                    }
                };

                axios.post(url, inputData).then(function(response) {
                    conferenceManager.data.credentialData = response;
                    resolve(conferenceManager.data.credentialData);
                });
            }
        });            
        },

        ssrcUpdate: function(userId, ext) {
        	if (!userId || !ext) {
        		console.error("conferenceService.signaling.ssrcUpdate is failed. check 'userId', 'ext'.");
        		return;
        	}
        	
            let url = _workspace.conferenceURL + 
            "SSRC?action=Update";
            console.log("ssrc update");

            let inputData = {
                "dto": {
                    "user_id": userId,
                    "ssrc_audio": ext.getSSRC()[0].ssrc,
                    "ssrc_video_1": ext.getSSRC()[1].ssrc,
                    "ssrc_video_2": ext.getSSRC()[2].ssrc,
                    "mslabel_audio": ext.getAudioMsLabel(),
                    "mslabel_video": ext.getVideoMsLabel(),
                    "label_audio": ext.getAudioLabel(),
                    "label_video": ext.getVideoLabel(),
                    "cname_audio": ext.getSSRC()[0].cname,
                    "cname_video": ext.getSSRC()[1].cname
                }
            };

            return axios.post(url, inputData);
        },

        ssrcGet: function(userId) {
        	if (!userId) {
        		console.error("conferenceService.signaling.ssrcGet is failed. check 'userId'.");
        		return;
        	}
        	
            let url = _workspace.conferenceURL + 
            "SSRC?action=Get";
            console.log("ssrc get");

            let inputData = {
                "dto": {
                    user_id: userId
                }
            };

            return axios.post(url, inputData);
        },

        ssrcGenerate: function(userId) {
        	if (!userId) {
        		console.error("conferenceService.signaling.ssrcGenerate is failed. check 'userId'.");
        		return;
        	}
        	
            let url = _workspace.conferenceURL + 
            "SSRC?action=Generate";

            let inputData = {
                "dto": {
                    user_id: userId
                }
            };

            return axios.post(url, inputData);
        },

        ufragUniqueCheck: function(roomId, ufrag) {
        	if (!roomId || !ufrag) {
        		console.error("conferenceService.signaling.ufragUniqueCheck is failed. check 'roomId', 'ufrag'.");
        		return;
        	}
        	
            let url = _workspace.conferenceURL + 
            "Ufrag?action=UniqueCheck";

            let inputData = {
                "dto": {
                    "room_id": roomId,
                    "ufrag": ufrag
                }
            };

            // console.log(ufrag)
            return axios.post(url, inputData);
        },

        userHeartBeat: function(roomId, userId) {
        	if (!roomId || !userId) {
        		console.error("conferenceService.signaling.userHeartBeat is failed. check 'roomId', 'userId'.");
        		return;
        	}
        	
            let url = _workspace.conferenceURL + 
            "User?action=HeartBeat";

            let inputData = {
                "dto": {
                    "room_id": roomId,
                    "user_id": userId,
                }
            };

            console.log("pit-a-pat: " + +new Date())

            return axios.post(url, inputData);
        }
    },

    room: {
        getRoomList: function() {
            return new Promise(function(resolve, reject){
            	
            	//임시코드
            	let spaceOrRoomId = conferenceService.getSpaceorRoomId();
            	
                let myInfo = conferenceService.user.getUserInfo();
                let url = _workspace.conferenceURL +
                "workspaces/" + spaceOrRoomId +
                "/conferences/" + conferenceService.data.conferenceId + 
                "/RoomList?action=Get"

                let inputData = {
                    "dto": {
                        user_id: myInfo.userId
                    }
                }

                axios.post(url, inputData).then(function(response){
                    if(response.data.dto.roomList === null) {
                        response.data.dto.roomList = [];
                    }
                    for(let i=0;i<response.data.dto.roomList.length;i++) {
                        if (response.data.dto.roomList[i].is_default_room === "true") {
                            conferenceService.room.activeRoomGet(response.data.dto.roomList[i].room_id).then(function(active_response){
                                let tempIdList = response.data.dto.roomList[i].user_id_list;
                                response.data.dto.roomList[i].user_id_list = active_response.data.dto.user_id_list;
                                response.data.dto.roomList[i].user_num = active_response.data.dto.attend_user_num;
                                let userNameList = [];
                                for(let j=0;j<active_response.data.dto.attend_user_num;j++) {
                                    userNameList.push(response.data.dto.roomList[i].user_name_list[tempIdList.indexOf(active_response.data.dto.user_id_list[j])]);
                                }
                                response.data.dto.roomList[i].user_name_list = userNameList;
                            })
                        }
                    }
                    resolve(response);
                });
            })
        },

        getRoomInfo: function(roomId) {
        	if (!roomId) {
        		console.error("conferenceService.room.getRoomInfo is failed. check 'roomId'.");
        		return;
        	}
        	
        	let spaceOrRoomId = conferenceService.getSpaceorRoomId();

        	
            return new Promise(function(resolve, reject){

                let url = _workspace.conferenceURL +
                "workspaces/" + spaceOrRoomId +
                "/conferences/" + conferenceService.data.conferenceId +
                "/rooms/" + roomId +
                "/Room?action=Get";

                let inputData = {
                    "dto": {
                        user_id: conferenceService.user.getUserInfo().userId
                    }
                }

                axios.post(url, inputData).then(function(response){
                    if (response.data.dto.is_default_room === "true") {
                        conferenceService.room.activeRoomGet(roomId).then(function(active_response){
                            let tempIdList = response.data.dto.user_id_list;
                            response.data.dto.user_id_list = active_response.data.dto.user_id_list;
                            response.data.dto.user_num = active_response.data.dto.attend_user_num;
                            let userNameList = [];
                            for(let i=0;i<active_response.data.dto.attend_user_num;i++) {
                                if (response.data.dto.user_name_list) {
                                    userNameList.push(response.data.dto.user_name_list[tempIdList.indexOf(active_response.data.dto.user_id_list[i])]);
                                }
                            }
                            response.data.dto.user_name_list = userNameList;
                            if(conferenceManager.data.currentRoomInfo != null &&
                                conferenceManager.data.currentRoomInfo.room_id === response.data.dto.room_id) {
                                conferenceManager.data.currentRoomInfo = response.data.dto;
                            }
                            resolve(response);
                        })
                    }
                    else {
                        if(conferenceManager.data.currentRoomInfo != null &&
                           conferenceManager.data.currentRoomInfo.room_id === response.data.dto.room_id) {
                            conferenceManager.data.currentRoomInfo = response.data.dto;
                        }   
                        resolve(response);
                    }
                })
            })
        },

        createRoom: function(inputData) {
        	if (!inputData) {
        		console.error("conferenceService.room.createRoom is failed. check 'inputData'.");
        		return;
        	}
        	
        	//임시코드
        	let spaceOrRoomId = conferenceService.getSpaceorRoomId();
        	
            let url = _workspace.conferenceURL +
            "workspaces/" + spaceOrRoomId +
            "/conferences/" + conferenceService.data.conferenceId + 
            "/Room?action=Create";

            return axios.post(url, inputData);
        },

        deleteRoom: function(roomId) {
        	if (!roomId) {
        		console.error("conferenceService.room.deleteRoom is failed. check 'roomId'.");
        		return;
        	}
        	
        	//임시코드
        	let spaceOrRoomId = conferenceService.getSpaceorRoomId();        	
        	
        	
            let url = _workspace.conferenceURL +
            "workspaces/" + spaceOrRoomId +
            "/conferences/" + conferenceService.data.conferenceId +
            "/rooms/" + roomId +
            "/Room?action=Delete";

            console.log('delete room');
            return axios.post(url);
        },

        updateUserConfig: function(roomId, inputData) {
        	if (!roomId || !inputData) {
        		console.error("conferenceService.room.updateUserConfig is failed. check 'roomId', 'inputData'.");
        		return;
        	}
        	
        	//임시코드
        	let spaceOrRoomId = conferenceService.getSpaceorRoomId();        	
        	
            return axios.post(_workspace.conferenceURL +
                "workspaces/" + spaceOrRoomId +
                "/conferences/" + conferenceService.data.conferenceId +
                "/rooms/" + roomId +
                "/RoomUser?action=Config", inputData);
        },

        attendRoomUser: function(roomId, userId, micState, cameraState) {
        	if (!roomId || !userId || !micState || !cameraState) {
        		console.error("conferenceService.room.attendRoomUser is failed. check 'roomId', 'userId', 'micState', 'cameraState'.");
        		return;
        	}
        	
            let inputData = {
                "dto": {
                    user_id: userId,
                    room_id: roomId,
                    mic_state: micState,
                    cam_state: cameraState
                }
            };

            //임시코드
        	let spaceOrRoomId = conferenceService.getSpaceorRoomId();        	

            
            let url = _workspace.conferenceURL +
            "workspaces/" + spaceOrRoomId +
            "/conferences/" + conferenceService.data.conferenceId + 
            "/RoomUser?action=Attend";
            console.log('attend Conference');

            return axios.post(url, inputData);
        },

        roomUserStateUpdate: function(roomId, userId, micState, cameraState) {
        	if (!roomId || !userId || !micState || !cameraState) {
        		console.error("conferenceService.room.roomUserStateUpdate is failed. check 'roomId', 'userId', 'micState', 'cameraState'.");
        		return;
        	}
        	
            let inputData = {
                "dto": {
                    user_id: userId,
                    room_id: roomId,
                    mic_state: micState,
                    cam_state: cameraState
                }
            };
            
            //임시코드
        	let spaceOrRoomId = conferenceService.getSpaceorRoomId();        	

            
            let url = _workspace.conferenceURL +
            "workspaces/" + spaceOrRoomId +
            "/conferences/" + conferenceService.data.conferenceId + 
            "/RoomUser?action=StateUpdate";
            console.log('user Device State Update');

            return axios.post(url, inputData);
        },

        leaveRoomUser: function(roomId, userId, flag) {
        	if (!roomId || !userId) {
        		console.error("conferenceService.room.leaveRoomUser is failed. check 'roomId', 'userId'.");
        		return;
            }
        	
            let inputData = {
                "dto": {
                    user_id: userId,
                    room_id: roomId,
                    duality: flag
                }
            };
            
            //임시코드
        	let spaceOrRoomId = conferenceService.getSpaceorRoomId();        	

            
            let url = _workspace.conferenceURL +
            "workspaces/" + spaceOrRoomId +
            "/conferences/" + conferenceService.data.conferenceId + 
            "/RoomUser?action=Leave";
            console.log('leave Conference');

            clearInterval(conferenceManager.data.keepAlive);
            conferenceManager.data.keepAlive = null;
            // conferenceService.signaling.userHeartBeat(roomId, userId, "0");

            return axios.post(url, inputData);
        },

        roomUserCreate: function(userId, roomId, userIdList) {
        	return new Promise(function(resolve, reject){
        		if (!roomId || !userIdList) {
            		console.error("conferenceService.room.roomUserCreate is failed. check 'roomId', 'inputData'.");
            		reject();
                }

                const inputData = {
                    "dto": {
                        user_id: userId,
                        room_id: roomId,
                        user_id_list: userIdList
                    }
                }
            	
                //임시코드
            	let spaceOrRoomId = conferenceService.getSpaceorRoomId();        	

                
                let url = _workspace.conferenceURL +
                "workspaces/" + spaceOrRoomId +
                "/conferences/" + conferenceService.data.conferenceId + 
                "/rooms/" + roomId + 
                "/RoomUser?action=Create&user-id=" + conferenceService.user.getUserInfo().userId;
                console.log('invite user');

                axios.post(url, inputData).then( function(response) {
                	conferenceLayout.render.lnbMenu();
                	conferenceLayout.render.roomHeadrUpdateInfo(roomId);
                	resolve(response);
                });
        	});
        },

        roomUserDelete: function(roomId) {
        	if (!roomId) {
        		console.error("conferenceService.room.roomUserDelete is failed. check 'roomId'.");
        		return;
        	}
        	
            let inputData = {
                "dto": {
                    user_id: conferenceService.user.getUserInfo().userId,
                }
            };
            //임시코드
        	let spaceOrRoomId = conferenceService.getSpaceorRoomId();        	

            let url = _workspace.conferenceURL +
            "workspaces/" + spaceOrRoomId +
            "/conferences/" + conferenceService.data.conferenceId + 
            "/rooms/" + roomId + 
            "/RoomUser?action=Delete";
            console.log('delete user');

            return axios.post(url, inputData);
        },

        activeRoomGet: function(roomId) {
        	if (!roomId) {
        		console.error("conferenceService.room.activeRoomGet is failed. check 'roomId'.");
        		return;
        	}
        	
            let inputData = {
                "dto": {
                    room_id: roomId,
                }
            };
            let url = _workspace.conferenceURL +
            "ActiveRoom?action=GetForClient";

            return axios.post(url, inputData);
        },
        roomChiefUpdate: function(preChiefId, newChiefId){
        	return new Promise(function(resolve, reject){
            	if (!preChiefId || !newChiefId ) {
            		console.error("conferenceService.room.roomChiefUpdate is failed. check 'preChiefId', 'newChiefId'.");
            		reject();
            	}
                let inputData = {
                        "dto": {
                            room_id : conferenceManager.data.currentRoomInfo.room_id,
                            user_id : preChiefId,
                            target_id : newChiefId
                        }
                }
            	
            	let url = _workspace.conferenceURL + "RoomUserChief?action=Update"
            	axios.post(url, inputData).then( function (response) {
            		conferenceManager.data.currentRoomInfo.chief_user_id = newChiefId;
            		resolve(response);
            	});
        	});
        },

        officeRoomCreate: function(fileId, userId) {
        	if (!fileId || !userId) {
        		console.error("conferenceService.room.officeRoomCreate is failed. check 'fileId', 'userId'.");
        		return;
        	}
        	
            let url = _workspace.conferenceURL +
            "OfficeRoom?action=Create";

            let userList = [userId];
            userList.push(fileId)
            let inputData = {
                "dto": {
                    user_id_list: userList,
                    user_id: userId,
                }
            }

            return axios.post(url, inputData);
        },

        officeRoomUserAttend: function(roomId, userId, micState, cameraState) {
            let inputData = {
                "dto": {
                    user_id: userId,
                    room_id: roomId,
                    mic_state: micState,
                    cam_state: cameraState,
                    com_session_fd: 0
                }
            };
            
            let _conferenceId = null
            	_conferenceId = workspaceManager.getRoomChannelList('0', "CHN0004");
            	//_conferenceId = workspaceManager.getChannelList(officeRouteManager.getWorkspaceId(), "CHN0004");
            
            let url = _workspace.conferenceURL + "workspaces/" + getRoomIdByUrl() + 
            "/conferences/" + _conferenceId + "/" + 
            "OfficeRoomUser?action=Attend";
            console.log('attend Office Room');

            return axios.post(url, inputData);
        },

        updateRoomList: function(roomId, roomData) {
        	if (!roomId || !roomData) {
        		console.error("conferenceService.room.updateRoomList is failed. check 'roomId', 'roomData'.");
        		return;
        	}
        	
            for (let i = 0; i < conferenceService.data.roomList.length; i++) {
                if (conferenceService.data.roomList[i].room_id === roomId) {
                    Object.assign(conferenceService.data.roomList[i], roomData);
                }
            }
        },

        searchRoom: function (roomId) {
        	if (!roomId) {
        		console.error("conferenceService.room.searchRoom is failed. check 'roomId'.");
        		return;
        	}
        	
            for (let i = 0; i < conferenceService.data.roomList.length; i++) {
                if (conferenceService.data.roomList[i].room_id === roomId) {
                    return conferenceService.data.roomList[i];
                }
            }
        },
        
    },

    share: {
        TCSAppOpen: function(roomId, shareApp, widget, data) {
        	if (!roomId || !shareApp || !widget || !data) {
        		console.error("conferenceService.share.TCSAppOpen is failed. check 'roomId', 'shareApp', 'widget', 'data'.");
        		return;
        	}
        	
            return this.TCSAppCommon(roomId, shareApp, 'Open', widget, data);
        },
        TCSAppClose: function(roomId, shareApp, widget, data) {
        	if (!roomId || !shareApp) {
        		console.error("conferenceService.share.TCSAppClose is failed. check 'roomId', 'shareApp', 'widget', 'data'.");    		
        		return;
        	}
        	
            return this.TCSAppCommon(roomId, shareApp, 'Close', widget, data);
        },
        TCSAppGet: function(roomId, shareApp) {
        	if (!roomId || !shareApp) {
        		console.error("conferenceService.share.TCSAppGet is failed. check 'roomId', 'shareApp'.");
        		return;
        	}
            return this.TCSAppCommon(roomId, shareApp, 'Get');
        },
        TCSAppCommon: function(roomId, shareApp, action, widget, data) {
        	if (!roomId || !shareApp || !action || !widget || !data) {
        		console.error("conferenceService.share.TCSAppCommon is failed. check 'roomId', 'shareApp', 'action', 'widget', 'data'.");
        	}
        	
            //임시코드
        	let spaceOrRoomId = conferenceService.getSpaceorRoomId();        	

            const appData = action === 'Open' ? JSON.stringify(data).toString() : null;
            const widgetId = action === 'Open' ? widget.id : null;
            let inputData = {
                "dto": {
                    user_id: conferenceService.user.getUserInfo().userId,
                    room_id: roomId,
                    sharing_app: shareApp,
                    sharing_app_id: "asdf",
                    app_data: appData,
                    widget: widgetId
                }
            };
            console.log(inputData);
            let url = _workspace.conferenceURL +
            "workspaces/" + spaceOrRoomId +
            "/conferences/" + conferenceService.data.conferenceId + 
            "/TCSApp?action=" + action;
            return axios.post(url, inputData);
        }
    },
    
    mqtt: {
        getSubscribeTargetRooms: function (userId){
        	if (!userId) {
        		console.error("conferenceService.share.mqtt.getSubscribeTargetRooms is failed. check 'userId'.");
        		return;
        	}
            let answer = {array : []};
            let url = _workspace.conferenceURL + "TopicList?action=Get&user-id=" + userId;
            return axios.get(url)
               .then(function(response) {
                    answer.array = response.data.dto.sub_target;
                    return Promise.resolve(answer);
               }).catch(function(err) {
                   console.error(err);
               })
        }
    }
}
}
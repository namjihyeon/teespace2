let AnswerSDPGenerator = null;
let ExtractorFromOffer = null;
if(setBrowsercode() !== "BRO0002") {
  AnswerSDPGenerator = class AnswerSDPGenerator {
    constructor(browserCode, role, toGateFlag) {
      this._browserCode = browserCode;
      this._role = role;
      this._toGateFlag = toGateFlag;
  
      // TODO 
      // \r\n mainly used on Windows.
      // need to be modified for Unix (\n)
      if (this._browserCode !== "BRO0003") {
        this._answer = {
          "type": "answer",
          "sdp": "v=0\r\no=- 860775679403011687 2 IN IP4 127.0.0.1\r\ns=-\r\nt=0 0\r\na=group:BUNDLE 0 1\r\na=msid-semantic: WMS gateway_msid\r\nm=audio 9 UDP/TLS/RTP/SAVPF 111 103\r\nc=IN IP4 0.0.0.0\r\na=rtcp:9 IN IP4 0.0.0.0\r\na=ice-ufrag:yPVw\r\na=ice-pwd:gateway_pwd\r\na=ice-options:trickle\r\na=fingerprint:sha-256 gateway_fingerprint\r\na=setup:active\r\na=mid:0\r\na=extmap:1 urn:ietf:params:rtp-hdrext:ssrc-audio-level\r\na=extmap:2 http://www.ietf.org/id/draft-holmer-rmcat-transport-wide-cc-extensions-01\r\na=extmap:3 urn:ietf:params:rtp-hdrext:sdes:mid\r\na=extmap:4 urn:ietf:params:rtp-hdrext:sdes:rtp-stream-id\r\na=extmap:5 urn:ietf:params:rtp-hdrext:sdes:repaired-rtp-stream-id\r\na=sendrecv\r\na=msid:gateway_msid b60c87de-f481-4c0d-a4ef-fb067e4f7bfc\r\na=rtcp-mux\r\na=rtpmap:111 opus/48000/2\r\na=rtcp-fb:111 transport-cc\r\na=fmtp:111 minptime=10;useinbandfec=1\r\na=rtpmap:103 ISAC/16000\r\na=ssrc:ssrc_audio cname:/vFaPJmQ7ZCjwHBk\r\nm=video 9 UDP/TLS/RTP/SAVPF 96 97\r\nc=IN IP4 0.0.0.0\r\na=rtcp:9 IN IP4 0.0.0.0\r\na=ice-ufrag:yPVw\r\na=ice-pwd:gateway_pwd\r\na=ice-options:trickle\r\na=fingerprint:sha-256 gateway_fingerprint\r\na=setup:active\r\na=mid:1\r\na=extmap:14 urn:ietf:params:rtp-hdrext:toffset\r\na=extmap:13 http://www.webrtc.org/experiments/rtp-hdrext/abs-send-time\r\na=extmap:12 urn:3gpp:video-orientation\r\na=extmap:2 http://www.ietf.org/id/draft-holmer-rmcat-transport-wide-cc-extensions-01\r\na=extmap:11 http://www.webrtc.org/experiments/rtp-hdrext/playout-delay\r\na=extmap:6 http://www.webrtc.org/experiments/rtp-hdrext/video-content-type\r\na=extmap:7 http://www.webrtc.org/experiments/rtp-hdrext/video-timing\r\na=extmap:8 http://tools.ietf.org/html/draft-ietf-avtext-framemarking-07\r\na=extmap:9 http://www.webrtc.org/experiments/rtp-hdrext/color-space\r\na=extmap:3 urn:ietf:params:rtp-hdrext:sdes:mid\r\na=extmap:4 urn:ietf:params:rtp-hdrext:sdes:rtp-stream-id\r\na=extmap:5 urn:ietf:params:rtp-hdrext:sdes:repaired-rtp-stream-id\r\na=sendrecv\r\na=msid:gateway_msid ad729ee7-31e2-4d4f-8656-134308135dba\r\na=rtcp-mux\r\na=rtcp-rsize\r\na=rtpmap:96 VP8/90000\r\na=rtcp-fb:96 goog-remb\r\na=rtcp-fb:96 transport-cc\r\na=rtcp-fb:96 ccm fir\r\na=rtcp-fb:96 nack\r\na=rtcp-fb:96 nack pli\r\na=rtpmap:97 rtx/90000\r\na=fmtp:97 apt=96\r\na=ssrc-group:FID ssrc_video_1 ssrc_video_2\r\na=ssrc:ssrc_video_1 cname:/vFaPJmQ7ZCjwHBk\r\na=ssrc:ssrc_video_2 cname:/vFaPJmQ7ZCjwHBk\r\n"
        }
        // 191015 chrome, togate 모두 mid: audio, video 사용
        // 191024 chrome, togate 모두 mid: 0, 1 사용하는 것으로 변경 (webrtc native 대응)
        this._answer.sdp = 
            this._answer.sdp.replace(/a=mid:audio/gi, "a=mid:0")
                            .replace(/a=mid:video/gi, "a=mid:1")
                            .replace(/a=group:BUNDLE audio video/gi, "a=group:BUNDLE 0 1");
  
        if (this._role === "producer") {
          let tempSdp = this._answer.sdp;
          this._answer.sdp = "";
          tempSdp.split("\r\n").forEach((line) => {
              if(line.includes("a=ssrc:") || line.includes("a=msid:") || line.includes("a=ssrc-group:")) {
                  // nothing
              }
              else this._answer.sdp += line + "\r\n"
          });
        }
      } else {
        this._answer = {
          "type": "answer",
          "sdp": "v=0\r\no=- 1329830298387604914 2 IN IP4 127.0.0.1\r\ns=-\r\nt=0 0\r\na=group:BUNDLE 0 1\r\na=msid-semantic: WMS gateway_msid\r\nm=audio 9 UDP/TLS/RTP/SAVPF 109 9 0 8 101\r\nc=IN IP4 0.0.0.0\r\na=rtcp:9 IN IP4 0.0.0.0\r\na=ice-ufrag:yPVw\r\na=ice-pwd:gateway_pwd\r\na=ice-options:trickle\r\na=fingerprint:sha-256 gateway_fingerprint\r\na=setup:active\r\na=mid:0\r\na=extmap:1 urn:ietf:params:rtp-hdrext:ssrc-audio-level\r\na=extmap:3 urn:ietf:params:rtp-hdrext:sdes:mid\r\na=sendrecv\r\na=msid:gateway_msid 0e92c0a3-c876-4ff8-97f5-a18394b50263\r\na=rtcp-mux\r\na=rtpmap:109 opus/48000/2\r\na=fmtp:109 minptime=10;useinbandfec=1\r\na=rtpmap:9 G722/8000\r\na=rtpmap:0 PCMU/8000\r\na=rtpmap:8 PCMA/8000\r\na=rtpmap:101 telephone-event/8000\r\na=ssrc:ssrc_audio cname:L1kobDFby93uQe7k\r\nm=video 9 UDP/TLS/RTP/SAVPF 120 121 126 97\r\nc=IN IP4 0.0.0.0\r\na=rtcp:9 IN IP4 0.0.0.0\r\na=ice-ufrag:yPVw\r\na=ice-pwd:gateway_pwd\r\na=ice-options:trickle\r\na=fingerprint:sha-256 gateway_fingerprint\r\na=setup:active\r\na=mid:1\r\na=extmap:5 urn:ietf:params:rtp-hdrext:toffset\r\na=extmap:4 http://www.webrtc.org/experiments/rtp-hdrext/abs-send-time\r\na=extmap:3 urn:ietf:params:rtp-hdrext:sdes:mid\r\na=sendrecv\r\na=msid:gateway_msid 1c049e95-6504-4ef0-b954-0e44156d524d\r\na=rtcp-mux\r\na=rtpmap:120 VP8/90000\r\na=rtcp-fb:120 goog-remb\r\na=rtcp-fb:120 ccm fir\r\na=rtcp-fb:120 nack\r\na=rtcp-fb:120 nack pli\r\na=rtpmap:121 VP9/90000\r\na=rtcp-fb:121 goog-remb\r\na=rtcp-fb:121 ccm fir\r\na=rtcp-fb:121 nack\r\na=rtcp-fb:121 nack pli\r\na=fmtp:121 profile-id=0\r\na=rtpmap:126 H264/90000\r\na=rtcp-fb:126 goog-remb\r\na=rtcp-fb:126 ccm fir\r\na=rtcp-fb:126 nack\r\na=rtcp-fb:126 nack pli\r\na=fmtp:126 level-asymmetry-allowed=1;packetization-mode=1;profile-level-id=42e01f\r\na=rtpmap:97 H264/90000\r\na=rtcp-fb:97 goog-remb\r\na=rtcp-fb:97 ccm fir\r\na=rtcp-fb:97 nack\r\na=rtcp-fb:97 nack pli\r\na=fmtp:97 level-asymmetry-allowed=1;packetization-mode=0;profile-level-id=42e01f\r\na=ssrc:ssrc_video_1 cname:L1kobDFby93uQe7k\r\n"
        }
      }
  
      // 191015 chrome, togate 모두 mid: video 사용
      this._iceCandidate = {
        "candidate": "candidate:2140244736 1 udp 2113937151 gateway_ip port_num typ host generation 0 ufrag yPVw network-cost 999",
        "sdpMid": "1",
        "sdpMLineIndex": 0
      }
    }
  
    replaceSSRC(input) {
      if (this._browserCode !== "BRO0003") {
        this._answer.sdp = this._answer.sdp.replace(/ssrc:ssrc_audio/gi, "ssrc:" + input.ssrc_audio);
        this._answer.sdp = this._answer.sdp.replace(/ssrc:ssrc_video_1/gi, "ssrc:" + input.ssrc_video_1);
        this._answer.sdp = this._answer.sdp.replace(/ssrc:ssrc_video_2/gi, "ssrc:" + input.ssrc_video_2);
        this._answer.sdp = this._answer.sdp.replace(/ssrc-group:FID ssrc_video_1 ssrc_video_2/gi, "ssrc-group:FID " + input.ssrc_video_1 + " " + input.ssrc_video_2);
        this._answer.sdp = this._answer.sdp.replace(/{audio_ms_label}/gi, input.mslabel_audio);
        this._answer.sdp = this._answer.sdp.replace(/{video_ms_label}/gi, input.mslabel_video);
        this._answer.sdp = this._answer.sdp.replace(/{audio_label}/gi, input.label_audio);
        this._answer.sdp = this._answer.sdp.replace(/{video_label}/gi, input.label_video);
        this._answer.sdp = this._answer.sdp.replace(/{audio_cname}/gi, input.cname_audio);
        this._answer.sdp = this._answer.sdp.replace(/{video_cname}/gi, input.cname_video);
      } else {
        this._answer.sdp = this._answer.sdp.replace("ssrc:ssrc_audio", "ssrc:" + input.ssrc_audio);
        this._answer.sdp = this._answer.sdp.replace("ssrc:ssrc_video_1", "ssrc:" + input.ssrc_video_1);
      }
      return this;
    }
  
    replaceHashValue(hashValue) {
      this._answer.sdp = this._answer.sdp.replace(/gateway_fingerprint/gi, hashValue);
      return this;
    }
  
    replaceICEPwd(ICEPwd) {
      this._answer.sdp = this._answer.sdp.replace(/gateway_pwd/gi, ICEPwd);
      return this;
    }
  
    replaceMsid(msid) {
      this._answer.sdp = this._answer.sdp.replace(/gateway_msid/gi, msid);
      return this;
    }
  
    replaceMid() {
      this._answer.sdp = 
            this._answer.sdp.replace(/a=mid:audio/gi, "a=mid:0")
                            .replace(/a=mid:video/gi, "a=mid:1")
                            .replace(/a=group:BUNDLE audio video/gi, "a=group:BUNDLE 0 1");
      return this;
    }
  
    replaceSendRecv() {
      if (this._role === 'producer') {
        this._answer.sdp = this._answer.sdp.replace(/a=sendrecv/gi, "a=recvonly");
      } else if (this._role === 'consumer') {
        this._answer.sdp = this._answer.sdp.replace(/a=sendrecv/gi, "a=sendonly");
      }
      return this;
    }
  
    generateGatewayICECandidate() {
      const gatewayIP = conferenceManager.data.gatewayAddress.split(":")[0];
      const gatewayPort = conferenceManager.data.gatewayAddress.split(":")[1] == undefined ? 19093 : conferenceManager.data.gatewayAddress.split(":")[1];
      this._iceCandidate.candidate = this._iceCandidate.candidate.replace(/gateway_ip/gi, gatewayIP);
      this._iceCandidate.candidate = this._iceCandidate.candidate.replace(/port_num/gi, gatewayPort);
      return this._iceCandidate;
    }
  
    generateGatewayICECandidateForOffice() {
      const gatewayIP = _workspace.gatewayURL.split(":")[0];
      const gatewayPort = _workspace.gatewayURL.split(":")[1] == undefined ? 19093 : _workspace.gatewayURL.split(":")[1];
      this._iceCandidate.candidate = this._iceCandidate.candidate.replace(/gateway_ip/gi, gatewayIP);
      this._iceCandidate.candidate = this._iceCandidate.candidate.replace(/port_num/gi, gatewayPort);
      return this._iceCandidate;
    }
  
    getAnswer() {
      this._answer.sdp = this._answer.sdp.replace(/\r\n\r\n/g, "\r\n");
      return this._answer;
    }
  }
  const trimLineBreak = (line) => {
    return line.replace(/\n/g, "").replace(/\r/g, "");
  }
  
  ExtractorFromOffer = class ExtractorFromOffer {
    constructor(offer) {
      this._offer = offer;
      this._SSRC = [];
  
      this._offer.sdp.split("a=mid:")[0].split("\n")
      .forEach((line) => {
        if (line.includes("ufrag")) {
          this._ufrag = line.split(":")[1];
          this._ufrag = trimLineBreak(this._ufrag)
        }
        if (line.includes("pwd")) {
          this._pwd = line.split(":")[1];
          this._pwd = trimLineBreak(this._pwd);
        }
      });
  
      this._offer.sdp.split("a=mid:")[1].split("\n")
      .forEach((line) => {
        if (line.includes("msid:")) {
          this._audioMsid = line.split(":")[1];
          this._audioMsid = trimLineBreak(this._audioMsid);
        }
        if (line.includes("cname")) {
          this._SSRC.push({
            "type": "audio",
            "ssrc": trimLineBreak(line.split('ssrc:')[1].split(' ')[0]),
            "cname": trimLineBreak(line.split('cname:')[1])
          })
        }
      });
  
      this._offer.sdp.split("a=mid:")[2].split("\n")
      .forEach((line) => {
        if (line.includes("msid:")) {
          this._videoMsid = line.split(":")[1];
          this._videoMsid = trimLineBreak(this._videoMsid);
        }
        if (line.includes("cname")) {
          this._SSRC.push({
            "type": "video",
            "ssrc": trimLineBreak(line.split('ssrc:')[1].split(' ')[0]),
            "cname": trimLineBreak(line.split('cname:')[1])
          })
        }
      })
    }
  
    replaceMid() {
      this._offer.sdp = 
            this._offer.sdp.replace(/a=mid:audio/gi, "a=mid:0")
                          .replace(/a=mid:video/gi, "a=mid:1")
                          .replace(/a=group:BUNDLE audio video/gi, "a=group:BUNDLE 0 1");
      return this;
    }
  
    getAudioMsid() {
      return this._audioMsid;
    }
  
    getVideoMsid() {
      return this._videoMsid;
    }
  
    getSSRC() {
      return this._SSRC;
    }
  
    getUfrag() {
      return this._ufrag;
    }
  
    getPwd() {
      return this._pwd;
    }
    
    getAudioMsLabel() {
      return this._audioMsid.split(' ')[0];
    }
  
    getAudioLabel() {
      return this._audioMsid.split(' ')[1];
    }
  
    getVideoMsLabel() {
      return this._videoMsid.split(' ')[0];
    }
  
    getVideoLabel() {
      return this._videoMsid.split(' ')[1];
    }
  }

}


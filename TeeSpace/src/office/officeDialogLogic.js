//pk에서 호출하는 전역함수
function alertExceedResourceOffice() {
    tds('officeResourceExceededDialog').open();
}function alertUnsupportedFeature(){
	tds('officeResourceExceededDialog').open();	
}
function alertUnknownError(p_comLogNum,p_errorCode){
	console.log(p_comLogNum,p_errorCode);
	tds('officeUnknownErrorDialog').open();
	officeManager.setDialogInfo('UnknownError',{errorCode:p_errorCode,comLogNum:p_comLogNum});
}
function alertUnstableConnection(p_comLogNum,p_errorCode){
	console.log(p_comLogNum,p_errorCode);
	tds('officeServerConnectErrorDialog').open();
	officeManager.setDialogInfo('ConnectionError',{errorCode:p_errorCode,comLogNum:p_comLogNum});
	
}
function alertGateWayUnstableConnection(p_errorCode) {
	console.log(p_errorCode);
	tds('officeServerConnectErrorDialog').open();
	officeManager.setDialogInfo('ConnectionError',{errorCode:p_errorCode});
}
function alertExceedResource() {
	tds('officeResourceExceededDialog').open();
}
function alertServerNotReady() {
	tds('officeUnknownErrorDialog').open();
}
function alertBinarySpecDocument(ws) {
	Top.Controller.get('officeBinarySpecLayoutLogic').ws = ws;
	tds('officeBinarySpecDialog').open();
}
function alertPrintDialog() {
	TeeToast.open({
		text: '인쇄 대기 중입니다. 인쇄가 완료되면 팝업창이 닫힙니다.',
		cases:[{
		text: '인쇄 중지',
		onClicked:
			function(e){
			//Office 인쇄 중지 로직 추가 필요
		}}]
	});
}
function alertEncryptedDocOpen(p_ws, p_bufferResol, p_infoComHeader, p_userId, p_module, p_docName, p_userName, p_wsID, p_newFileFlag, p_fileChannelId, p_fileParentId, p_fileParentPath, p_roomId) {
	
}

Top.Controller.create('officeSessionExpiredLayoutLogic', {
	//Office Dialog 세션 만료 팝업
	onClickConfirm : function(event, widget) {
		//확인 버튼 클릭시, 탭이 1개일 때 -> sub/main app 종료, 2개 이상 -> 탭만 종료
		onCloseButtonClick();//router
		try{officePeer.clearPeerConnection(userManager.getLoginUserId());} catch(e){console.log(e);}
		if(officeContext.data.isDivided) self.close(); else tds('officeSessionExpiredDialog').close(true);
	}
});

Top.Controller.create('officeResourceExceededLayoutLogic', {
	//Office Dialog 자원 초과
	onClickConfirm : function(event, widget) {
		//확인 버튼 클릭시, 탭이 1개일 때 -> sub/main app 종료, 2개 이상 -> 탭만 종료
		onCloseButtonClick();//router
		try{officePeer.clearPeerConnection(userManager.getLoginUserId());} catch(e){console.log(e);}
		if(officeContext.data.isDivided) self.close(); else tds('officeResourceExceededDialog').close(true);
	}
});

Top.Controller.create('officeServerConnectErrorLayoutLogic', {
	init:function(){
		officeManager.dialogReady('ConnectionError');
	},
	setErrorCode:function(p_col,p_err){
		var s_text = tds('officeErrorTextView1').getProperties('text');
		tds('officeErrorTextView1').setProperties({'text':s_text+'[ERROR] '+p_col+' '+p_err});
	},
	//Office Dialog 서버 연결 에러
	onClickConfirm : function(event, widget) {
		//확인 버튼 클릭시, 탭이 1개일 때 -> sub/main app 종료, 2개 이상 -> 탭만 종료
		onCloseButtonClick();//router
		try{officePeer.clearPeerConnection(userManager.getLoginUserId());} catch(e){console.log(e);}
		if(officeContext.data.isDivided) self.close(); else tds('officeServerConnectErrorDialog').close(true);
	}
});

Top.Controller.create('officeUnknownErrorLayoutLogic', {
	init:function(){
		officeManager.dialogReady('UnknownError');
	},
	setErrorCode:function(p_col,p_err){
		var s_text = tds('deleteTextView').getProperties('text');
		tds('deleteTextView').setProperties({'text':s_text+'[ERROR] '+p_col+' '+p_err});
	},
	//Office Dialog 알 수 없는 에러
	onClickReconnect : function(event, widget) {
		//재연결 시도
		try{officePeer.clearPeerConnection(userManager.getLoginUserId());} catch(e){console.log(e);}
    	window.location.reload();
    	if(officeContext.data.isDivided) self.close(); else tds('officeUnknownErrorDialog').close(true);
	},
	onClickCancel : function(event, widget) {
		//취소, 탭이 1개일 때 -> sub/main app 종료, 2개 이상 -> 탭만 종료
		onCloseButtonClick();//router
		try{officePeer.clearPeerConnection(userManager.getLoginUserId());} catch(e){console.log(e);}
		if(officeContext.data.isDivided) self.close(); else tds('officeUnknownErrorDialog').close(true);
	}
	
});

Top.Controller.create('officeBinarySpecLayoutLogic', {
	init:function(){
		this.ws;
	},
	onClickChange : function(event, widget) {
		//PK실 - 이곳에 변환 버튼이 눌렸을때 동작하는 로직이 추가될 예정
		var ws = this.ws;
		var appType = appManager.getSubApp();
		var officeSessionStorage;
		if(appType==='toword')officeSessionStorage= JSON.parse(sessionStorage.office).ToWord;
		if(appType==='tocell')officeSessionStorage= JSON.parse(sessionStorage.office).ToCell;
		if(appType==='topoint')officeSessionStorage= JSON.parse(sessionStorage.office).ToPoint;
		
		var ajaxResult = false;
		let workspaceURL = getRoomIdByUrl();
		let info = workspaceManager.getWorkspaceAndChannelId(workspaceURL,'0',TEESPACE_CHANNELS.TDRIVE.CHANNEL_TYPE);
		
		var inputDataForFirstRequest = {
			"dto" : {
				"file_parent_id" : officeSessionStorage.fileParentId,
				"workspace_id" : info.workspaceId,
				"channel_id" : info.channelId,
				"user_id" : officeSessionStorage.userId,
				"storageFileInfo" : {
					"file_id" : officeSessionStorage.fileId,
    				"user_id" : officeSessionStorage.userId,
    				"file_last_update_user_id" : officeSessionStorage.userId,
    				"file_name" : "",
    				"file_extension" : "",
    				"file_extension" : "",
    				"user_context_1" : "",
    				"user_context_2" : "",
    				"user_context_3" : ""
				}
			}
		}
		
		$.ajax({
            url: _workspace.url + "File/SODriveCopy?action=",
            type: 'POST',
            dataType: 'json',
            data: JSON.stringify(inputDataForFirstRequest),
            contentType: 'application/json; charset=utf-8',
            xhrFields: {
                withCredentials: true
            },
            crossDomain: true,
            async: false,
            success: function (data, msg, state) {
            	Top.Dom.selectById("officeEditWebView").getElement().contentWindow.binaryFileId = data.dto.storageFileInfo.file_id;
          	  	ajaxResult = true;
                return data.dto.storageFileInfo.file_id;
            },
            error: function (data, msg, state) {
                console.info("## File Cipt Service call error");
                return false;
            }
        })
        
		if(ajaxResult == true && Top.Dom.selectById("officeEditWebView").getElement().contentWindow.binaryFileId) {
			var docBinaryConvertJson = Top.Dom.selectById("officeEditWebView").getElement().contentWindow.makeDocBinaryConvertJson(Top.Dom.selectById("officeEditWebView").getElement().contentWindow.binaryFileId);
		try { ws.send(docBinaryConvertJson); } catch (e) { }
    		try { ws.send(infoBuffer); } catch (e) { };
		} else {
			console.info("binary file copy service failed");
		}
		tds('officeBinarySpecDialog').close(true);
	},
	onClickCancel : function(event, widget) {
	//취소, 탭이 1개일 때 -> sub/main app 종료, 2개 이상 -> 탭만 종료
		onCloseButtonClick();//router
		try{officePeer.clearPeerConnection(userManager.getLoginUserId());} catch(e){console.log(e);}
		if(officeContext.data.isDivided) self.close(); else tds('officeBinarySpecDialog').close(true);
	}, onClickDownload : function(event, widget) {
		if(officeContext.data.isDivided) self.close(); else tds('officeBinarySpecDialog').close(true);
	}
	
});

Top.Controller.create('officeEcryptedLayoutLogic', {
	onClickConfirm : function(event, widget) {
		var docCypher = Top.Dom.selectById("templateTextField1").getText();
    	var infoBuffer;
    	if (Top.Dom.selectById("officeEditWebView")) {
    		infoBuffer = Top.Dom.selectById("officeEditWebView").getElement().contentWindow.selectFirstBody(infoComHeader, userId, module, docName, userName, wsID, newFileFlag, fileChannelId, fileParentId, fileParentPath, roomId, docCypher);
    	}
    	try { ws.send(bufferResol); } catch (e) { };
		try { ws.send(infoBuffer); } catch (e) { };
		if(officeContext.data.isDivided) self.close(); else tds('officeEcryptedDialog').close(true);	
	}, onClickCancel : function(event, widget) {
		if(officeContext.data.isDivided) self.close(); else tds('officeEcryptedDialog').close(true);
	}
	
});






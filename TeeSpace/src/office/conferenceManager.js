
conferenceManager = {

    data: {
        localStream: null,
        attendHistory: [],

        micState: null,
        cameraState: null,
        // 0: on, 1: off, 2: not exist

        currentRoomInfo: null,
        gatewayAddress: null,
        userList: [],
        localInterval: null,
        share_request_msg: null,
        videoProducer: null,
        keepAlive: null,
        sdpMidFlag: false,
        duality: null,

        numOfCreatePeer: 0,
        maxNumOfCreatePeer: 30,

        userIdForLogout: null,

        credentialData: null
    },

    routToConferenceRoom: function () {
        // iOS앱에서는 webrtc를 지원하지 않기 때문에 새롭게 브라우저를 띄워서 회의실로 라우팅해줘야 함.
        const spaceUrl = getRoomIdByUrl() ? getRoomIdByUrl() : null,
            roomUrl = '0',
            roomId = conferenceManager.data.currentRoomInfo.room_id ? conferenceManager.data.currentRoomInfo.room_id : null,
            userId = userManager.getLoginUserId() ? userManager.getLoginUserId() : null,
            token = getCookie('token') ? getCookie('token') : null,
            path = 'https://' + window.location.href.split('#!')[0].split('://')[1] + '#!/conferenceFromiOS/' + userId + '?token=' + token + '&spaceUrl=' + spaceUrl + '&roomUrl=' + roomUrl + '&conferenceRoomId=' + roomId;

        // 새 브라우저를 띄우고 회의실로 라우팅.
        Top.LocalService.acall({
            service: 'tos.service.WebBrowser.loadUrl',
            data: path
        });
    },

    setLocalVideo: function () {
        return new Promise(function (resolve, reject) {
            if (!conferenceManager.data.localStream && conferenceManager.data.cameraState !== "none") {
                try {
                    const userAgent = navigator.userAgent.toLowerCase();
                    if (userAgent.indexOf("iphone") > -1 || userAgent.indexOf("ipad") > -1 || userAgent.indexOf("ipod") > -1) // iOS일 때
                        conferenceManager.routToConferenceRoom();
                } catch (err) {
                    // nothing
                }

                navigator.mediaDevices.getUserMedia({
                    audio: true,
                    video: true
                }).then(function(stream) {
                    conferenceManager.data.localStream = stream;
                    conferenceManager.data.micState = "on";
                    conferenceManager.data.cameraState = "on";
                    console.log("webcam start")
                    resolve(stream);
                }).catch(function(err) {
                    conferenceManager.data.micState = "none";
                    conferenceManager.data.cameraState = "none";
                    console.log("there is no webcam")
                    resolve();
                });
            } else {
                resolve();
            }
        })
    },

    stopLocalVideo: function () {
        return new Promise(function (resolve, reject) {

            if (conferenceManager.data.micState !== "none") {
                if (conferenceManager.data.localStream) {
                    conferenceManager.data.localStream.getAudioTracks().forEach(function (track) {
                        track.stop();
                    });
                }
            }
            if (conferenceManager.data.cameraState !== "none") {
                if (conferenceManager.data.localStream) {
                    conferenceManager.data.localStream.getVideoTracks().forEach(function (track) {
                        track.stop();
                        console.log("webcam stop")
                    });
                }
            }
            conferenceManager.data.localStream = null;
            conferenceManager.data.micState = null;
            conferenceManager.data.cameraState = null;

            resolve();
        })
    },

    certificateOffer: function (pc, offerOption, roomId, trial) {
        return new Promise(function (resolve, reject) {

            if (Top.Util.Browser.isSafari()) {
                if (offerOption.offerToReceiveAudio === 1) {
                    pc.addTransceiver("audio");
                }
                if (offerOption.offerToReceiveVideo === 1) {
                    pc.addTransceiver("video");
                }
                pc.createOffer().then(function (offer) {
                    if (trial < 5) {
                        let ufrag;
                        offer.sdp.split("\n").forEach(function (line) {
                            if (line.includes("ufrag:")) {
                                ufrag = line.split("ufrag:")[1];
                                ufrag = ufrag.replace(/\n/g, "");
                                ufrag = ufrag.replace(/\r/g, "");
                            }
                        })

                        conferenceService.signaling.ufragUniqueCheck(roomId, ufrag).then(function (response) {
                            // console.log(response.data.dto)
                            if (response.data.dto.is_unique === "true") {
                                resolve(offer);
                            } else {
                                trial += 1;
                                resolve(conferenceManager.certificateOffer(pc, offerOption, roomId, trial));
                            }
                        })
                    } else {
                        // console.log("fail to create offer")
                        reject();
                    }
                })

            } else {
                // console.log(trial)
                pc.createOffer(offerOption).then(function (offer) {
                    if (trial < 5) {
                        let ufrag;
                        offer.sdp.split("\n").forEach(function (line) {
                            if (line.includes("ufrag:")) {
                                ufrag = line.split("ufrag:")[1];
                                ufrag = ufrag.replace(/\n/g, "");
                                ufrag = ufrag.replace(/\r/g, "");
                            }
                        })

                        conferenceService.signaling.ufragUniqueCheck(roomId, ufrag).then(function (response) {
                            // console.log(response.data.dto)
                            if (response.data.dto.is_unique === "true") {
                                resolve(offer);
                            } else {
                                trial += 1;
                                resolve(conferenceManager.certificateOffer(pc, offerOption, roomId, trial));
                            }
                        })
                    } else {
                        // console.log("fail to create offer")
                        reject();
                    }
                })
            }
        })
    },

    createPeer: function (roomId, userId) {
        return new Promise(function (resolve, reject) {
            conferenceManager.data.numOfCreatePeer++;
            console.log(conferenceManager.data.numOfCreatePeer);
            let ufrag;
            let pwd;
            let offerOption;

            conferenceService.signaling.getGatewayPublicKey().then(function (response) {
                let answer_hash = response.data.dto.publickey;
                let answer_port = conferenceManager.data.gatewayAddress.split(":")[1] == undefined ? 19093 : conferenceManager.data.gatewayAddress.split(":")[1];
                let pc = new RTCPeerConnection ({ sdpSemantics : "unified-plan" });
                let offerAudio;
                let offerVideo;

                if (conferenceService.user.getUserInfo().userId === userId) {
                    if (conferenceManager.data.micState !== "none") {
                        pc.addTrack(conferenceManager.data.localStream.getAudioTracks()[0]);
                        offerAudio = 0;
                    } else {
                        offerAudio = 1;
                    }
                    if (conferenceManager.data.cameraState !== "none") {
                        conferenceManager.data.videoProducer = pc.addTrack(conferenceManager.data.localStream.getVideoTracks()[0]);
                        offerVideo = 0;
                    } else {
                        offerVideo = 1;
                    }
                    offerOption = {
                        offerToReceiveAudio: offerAudio,
                        offerToReceiveVideo: offerVideo,
                    };
                } else {
                    offerOption = {
                        offerToReceiveAudio: 1,
                        offerToReceiveVideo: 1
                    };
                }

                conferenceManager.certificateOffer(pc, offerOption, roomId, 0).then(function(offer) {
                    offer.sdp.split("\n").forEach(function(line) {
                        if (line.includes("ufrag:")) {
                            ufrag = line.split("ufrag:")[1];
                            ufrag = ufrag.replace(/\n/g, "");
                            ufrag = ufrag.replace(/\r/g, "");
                        }
                        if (line.includes("pwd:")) {
                            pwd = line.split("pwd:")[1];
                            pwd = pwd.replace(/\n/g, "");
                            pwd = pwd.replace(/\r/g, "");
                        }
                    });

                    // togate 대응
                    if (offer.sdp.indexOf("a=mid:0") < 0) {
                        conferenceManager.data.sdpMidFlag = true;
                    }
                    if (Top.Util.Browser.isSafari()) {
                        offer.sdp = offer.sdp.replace(offer.sdp.substring(offer.sdp.indexOf('SAVPF', offer.sdp.indexOf('m=video')) + 6, offer.sdp.indexOf('\r', offer.sdp.indexOf('m=video'))), '96 97');
                        offer.sdp = offer.sdp.replace(offer.sdp.substring(offer.sdp.indexOf('a=rtpmap:96 H264/90000'), offer.sdp.indexOf("a=ssrc-group:FID")),
                            `a=rtpmap:96 VP8/90000\r\na=rtcp-fb:96 goog-remb\r\na=rtcp-fb:96 transport-cc\r\na=rtcp-fb:96 ccm fir\r\na=rtcp-fb:96 nack\r\na=rtcp-fb:96 nack pli\r\na=rtpmap:97 rtx/90000\r\na=fmtp:97 apt=96\r\n`
                        );
                    }

                    if (conferenceService.user.getUserInfo().userId === userId) {
                        const ext = new ExtractorFromOffer(offer);
                        conferenceService.signaling.ssrcUpdate(userId, ext).then(function (response) {
                            conferenceService.room.attendRoomUser(roomId, conferenceService.user.getUserInfo().userId, conferenceManager.data.micState, conferenceManager.data.cameraState).then(function (response) {
                                conferenceLayout.render.lnbMenuItem(roomId);
                                pc.setLocalDescription(ext.replaceMid()._offer);
                                pc.addEventListener('icecandidate', function(e) {conferenceManager.onIceCandidate(e, roomId, userId, ufrag, pwd, answer_port)});
                                conferenceService.signaling.iceCandidateUpdate(roomId, userId, ufrag, pwd);
                                conferenceManager.answerSubmit(userId, answer_hash, answer_port, pc);
                            });
                        });
                    } else {
                        const ext = new ExtractorFromOffer(offer);
                        pc.setLocalDescription(ext.replaceMid()._offer);
                        pc.addEventListener('icecandidate', function(e) {conferenceManager.onIceCandidate(e, roomId, userId, ufrag, pwd, answer_port)});
                        conferenceService.signaling.iceCandidateUpdate(roomId, userId, ufrag, pwd);
                        pc.addEventListener('track', function(e) { conferenceManager.gotRemoteStream(userId, e) });
                        conferenceManager.answerSubmit(userId, answer_hash, answer_port, pc);
                    }

                    pc.addEventListener('iceconnectionstatechange', function(e) {conferenceManager.onIceStateChange(pc, userId, e)});
                    pc.addEventListener('connectionstatechange', function(e) {conferenceManager.onConnectionStateChange(pc, roomId, userId, e)});

                    peers.setPeerConnection(userId, pc);
                    resolve(pc);
                }).catch(function () {
                    console.log("fail to create offer")
                })
            })
        });
    },

    onIceCandidate: function (event) {
        // console.log(event);
    },

    onIceStateChange: function (pc, userId, event) {
        if (pc) console.log("ICE state: " + pc.iceConnectionState + "\n" + userId);
    },

    onIceGatheringStateChange: function (pc, userId, event) {
        if (pc) console.log("ICE gathering state: " + pc.iceGatheringState + "\n" + userId);
    },

    onConnectionStateChange: function (pc, roomId, userId, event) {
        if (pc) {
            console.log("connection state: " + pc.connectionState + "\n" + userId);
            if (pc.connectionState === "failed") {
                if (userId === userManager.getLoginUserId()) {
                    console.warn("producer 커넥션 실패");
                    alert("CloudMeeting 게이트웨이 서버와 연결되지 않습니다. 다시 시도해주세요.");
                    conferenceEvent.enterStartRoom();
                } else {
                    peers.clearPeerConnection(userId);
                    if (conferenceManager.data.numOfCreatePeer < conferenceManager.data.maxNumOfCreatePeer) {
                        conferenceManager.createPeer(roomId, userId);
                    } else {
                        alert("CloudMeeting 게이트웨이 서버와 연결되지 않습니다. 다시 시도해주세요.");
                        conferenceEvent.enterStartRoom();
                    }


                }

            }
        }
    },

    gotRemoteStream: function (userId, e) {
        console.log(e)
        const stream = new MediaStream();
        stream.addTrack(e.track);

        if (e.track.kind === "audio") {
            peers.setAudioTrack(userId, e.track);

            const audio = document.createElement('audio');
            audio.srcObject = stream;
            if(peers.getMicState(userId) === "on")
                audio.play();

            conferenceLayout.volume.animation("#conference-on-name-space_" + userId + " span.mic-state-icon", stream, 'remote');
        } else if (e.track.kind === "video") {
            peers.setVideoTrack(userId, e.track);
            if(peers.getCameraState(userId) === "on")
                conferenceEvent.updateVideo(e.track, userId, "enable");
        }
    },

    answerSubmit: function (userId, answer_hash, answer_port, pc) {
        const role = conferenceService.user.getUserInfo().userId === userId ? "producer" : "consumer";
        const isTogate = conferenceManager.data.sdpMidFlag;

        const answer_ice_pwd = 'DwD9OYidnTwKdRCM3068ZGsU'
        const answer_msid = 'siN5Lj96LGAbjitb6MlbSE5srxhg8jr4m875'

        if (role === "consumer") {
            conferenceService.signaling.ssrcGet(userId).then(function (response) {
                if (!response.data.dto.ssrc_video_2) {
                    response.data.dto.ssrc_video_2 = response.data.dto.ssrc_video_1;
                }

                let answerSDPGenerator =
                    new AnswerSDPGenerator(setBrowsercode(), role, isTogate)
                        .replaceSSRC(response.data.dto)
                        .replaceHashValue(answer_hash)
                        .replaceICEPwd(answer_ice_pwd)
                        .replaceMsid(answer_msid)
                        .replaceMid()
                        .replaceSendRecv();

                pc.setRemoteDescription(answerSDPGenerator.getAnswer());
                console.log("set remote description ", userId);

                pc.addIceCandidate(answerSDPGenerator.generateGatewayICECandidate());
            })
        } else {
            let answerSDPGenerator =
                new AnswerSDPGenerator(setBrowsercode(), role, isTogate)
                    .replaceHashValue(answer_hash)
                    .replaceICEPwd(answer_ice_pwd)
                    .replaceMsid(answer_msid)
                    .replaceMid()
                    .replaceSendRecv();

            // bandwidth testcode 19.11.06 소형준
            let bandwidth = 1000;
            let answerSDP = answerSDPGenerator.getAnswer();
            let modifier = 'AS';
            if (answerSDP.sdp.indexOf('b=' + modifier + ':') === -1) {
                // insert b= after c= line.
                answerSDP.sdp = answerSDP.sdp.replace(/c=IN (.*)\r\n/g, 'c=IN $1\r\nb=' + modifier + ':' + bandwidth + '\r\n');
            } else {
                answerSDP.sdp = answerSDP.sdp.replace(new RegExp('b=' + modifier + ':.*\r\n'), 'b=' + modifier + ':' + bandwidth + '\r\n');
            }

            answerSDP.sdp += "a=fmtp:96 x-google-min-bitrate=500\r\n"; // minimum bitrate 설정 500kbps

            pc.setRemoteDescription(answerSDP);
            // pc.setRemoteDescription(answerSDPGenerator.getAnswer());
            console.log("set remote description ", userId);

            pc.addIceCandidate(answerSDPGenerator.generateGatewayICECandidate());
        }
    },

    updateBandwidthRestriction: function (sdp, bandwidth) {
        let modifier = 'AS';
        // if (adapter.browserDetails.browser === 'firefox') {
        //   bandwidth = (bandwidth >>> 0) * 1000;
        //   modifier = 'TIAS';
        // }
        if (sdp.indexOf('b=' + modifier + ':') === -1) {
            // insert b= after c= line.
            sdp = sdp.replace(/c=IN (.*)\r\n/gi, 'c=IN $1\r\nb=' + modifier + ':' + bandwidth + '\r\n');
        } else {
            sdp = sdp.replace(new RegExp('b=' + modifier + ':.*\r\n'), 'b=' + modifier + ':' + bandwidth + '\r\n');
        }
        return sdp;
    },

    conferenceStart: function (roomId) {
        conferenceManager.data.numOfCreatePeer = 0;
        conferenceManager.data.userIdForLogout = userManager.getLoginUserId();
        if (conferenceManager.data.attendHistory.indexOf(roomId) < 0) {
            conferenceManager.data.attendHistory.push(roomId);
        }
        conferenceManager.setUnloadFunction();

        if (conferenceManager.data.cameraState === "none") {
            conferenceService.room.attendRoomUser(roomId, conferenceService.user.getUserInfo().userId, conferenceManager.data.micState, conferenceManager.data.cameraState).then(function (response) {
                conferenceLayout.render.lnbMenuItem(roomId);
            });
        } else {
            conferenceManager.createPeer(roomId, conferenceService.user.getUserInfo().userId).then(function () { });
        }

        conferenceService.room.activeRoomGet(roomId).then(function (response) {
            let attendUserList = response.data.dto.user_id_list;
            if (attendUserList) {
                if (attendUserList.indexOf(conferenceService.user.getUserInfo().userId) !== -1) {
                    attendUserList.splice(attendUserList.indexOf(conferenceService.user.getUserInfo().userId), 1);
                }
            }
            if (attendUserList) {
                attendUserList.forEach(function(attendUserId) {
                    let peer = response.data.dto.user_list.find(function(peer) {return peer.user_id === attendUserId});
                    if (peer.mic_state !== "none" || peer.cam_state !== "none") {
                        conferenceManager.createPeer(roomId, attendUserId).then(function () {
                            console.log("create consumer " + attendUserId);
                            peers.setMicState(attendUserId, peer.mic_state);
                            peers.setCameraState(attendUserId, peer.cam_state);
                        });
                    } else {
                        peers.setPeerConnection(attendUserId, null);
                        peers.setMicState(attendUserId, peer.mic_state);
                        peers.setCameraState(attendUserId, peer.cam_state);
                    }
                    if (peer.cam_state === "off" || peer.cam_state === "none") {
                        conferenceEvent.updateVideo(null, attendUserId, "disable");
                    }
                    if (peer.mic_state === "off" || peer.mic_state === "none") {
                        $('#conference-on-name-space_' + attendUserId + ' .mic-state-icon').removeClass("icon-work_volume_01");
                        $('#conference-on-name-space_' + attendUserId + ' .mic-state-icon').addClass("off");
                    }
                });
            }
        });
    },

    conferenceShutDown: function (roomId, userId) {
            peers.clearAllPeerConnection();

            if (typeof (roomId) != "string") {
                let _roomId = conferenceManager.data.currentRoomInfo.room_id;
                roomId = _roomId;
            }
            if (userId == null) userId = conferenceService.user.getUserInfo().userId;
            conferenceService.room.leaveRoomUser(roomId, userId, false).then(function (response) {
                conferenceManager.data.attendHistory.pop(roomId);
                conferenceLayout.render.lnbMenuItem(roomId);
            });

            conferenceManager.data.videoProducer = null;
    },

    leaveAllAttendedRoom: function (userId) {
        return new Promise(function (resolve, reject) {
            conferenceManager.data.currentRoomInfo = null;
            conferenceManager.stopLocalVideo();
            conferenceManager.data.attendHistory.forEach(function(roomId) {
                conferenceManager.conferenceShutDown(roomId, userId);
            });
            conferenceManager.data.attendHistory = [];
            conferenceManager.resetUnloadFunction();

            resolve();
        })
    },

    setUnloadFunction: function () {
        window.onbeforeunload = function () {
            conferenceManager.leaveAllAttendedRoom();
            return '참여 중인 회의로부터 이탈합니다.';
        };
    },

    resetUnloadFunction: function () {
        window.onbeforeunload = function () { };
    },

    ctrMic: function () {
        if (conferenceManager.data.micState == "on") {
            console.log("onMute");
            conferenceManager.data.micState = "off";
            if (conferenceManager.data.localStream) {
                conferenceManager.data.localStream.getAudioTracks()[0].enabled = false;
            }
            if (conferenceManager.data.currentRoomInfo.is_attended === "true") {
                conferenceService.room.roomUserStateUpdate(conferenceManager.data.currentRoomInfo.room_id, conferenceService.user.getUserInfo().userId, conferenceManager.data.micState, conferenceManager.data.cameraState)
            }
        } else if (conferenceManager.data.micState == "off") {
            console.log("onUnmute");
            conferenceManager.data.micState = "on";
            if (conferenceManager.data.localStream) {
                conferenceManager.data.localStream.getAudioTracks()[0].enabled = true;
            }
            if (conferenceManager.data.currentRoomInfo.is_attended === "true") {
                conferenceService.room.roomUserStateUpdate(conferenceManager.data.currentRoomInfo.room_id, conferenceService.user.getUserInfo().userId, conferenceManager.data.micState, conferenceManager.data.cameraState)
            }
        } else {
            console.log('Mic is not exist')
        }
    },

    ctrCamera: function () {
        if (conferenceManager.data.cameraState == "on") {
            console.log("onDisableVideo");
            conferenceManager.data.cameraState = "off";
            if (conferenceManager.data.localStream) {
                conferenceManager.data.localStream.getVideoTracks()[0].enabled = false;
            }
            if (conferenceManager.data.currentRoomInfo.is_attended === "true") {
                conferenceService.room.roomUserStateUpdate(conferenceManager.data.currentRoomInfo.room_id, conferenceService.user.getUserInfo().userId, conferenceManager.data.micState, conferenceManager.data.cameraState)
            }
        } else if (conferenceManager.data.cameraState == "off") {
            console.log("onEnableVideo");
            conferenceManager.data.cameraState = "on";
            if (conferenceManager.data.localStream) {
                conferenceManager.data.localStream.getVideoTracks()[0].enabled = true;
            }
            if (conferenceManager.data.currentRoomInfo.is_attended === "true") {
                conferenceService.room.roomUserStateUpdate(conferenceManager.data.currentRoomInfo.room_id, conferenceService.user.getUserInfo().userId, conferenceManager.data.micState, conferenceManager.data.cameraState)
            }
        } else {
            console.log('Camera is not exist')
        }
    }
}

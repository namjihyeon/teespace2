conferenceMqttHandler = {
	handleMessage: function (msg) {
		if (!msg) return false;
		if (!msg.NOTI_ETC) return false;

		const cnfEvent = JSON.parse(msg.NOTI_ETC);
		let isCurrentRoom = false;

		// NOTI_TARGET 에 conference room id 만 들어오도록 변경
		const conference_room_id = msg.NOTI_TARGET;
		const _userId = msg.USER_ID;
		if(_userId === userManager.getLoginUserId() && cnfEvent.event_type !== "user_duality_event")
			return false;

		if(!!conferenceManager.data.currentRoomInfo && conferenceManager.data.currentRoomInfo.room_id == conference_room_id )
			isCurrentRoom = true;

		if (cnfEvent.event_type) {
			switch (cnfEvent.event_type) {
			case "room_create": {
				conferenceLayout.render.lnbMenu();
				break;
			}
			case "user_create":
			case "user_delete":
				conferenceLayout.render.lnbMenu();
				if(isCurrentRoom) {
					conferenceLayout.render.roomHeadrUpdateInfo(conference_room_id);
					conferenceService.room.getRoomInfo(conferenceManager.data.currentRoomInfo.room_id).then(function(response) {
						conferenceManager.data.currentRoomInfo = response.data.dto;
						if(conferenceManager.data.currentRoomInfo.is_attended.equals("true"))
							Top.Controller.get('conferenceFooterLayoutLogic').conferenceReload();
					});
				}				
				break;
			case "first_user_attend": {
				conferenceLayout.render.lnbMenuItem(conference_room_id).then(function () {
					console.log("conference start: " + conference_room_id);
				});
				break;
			}
			case "user_attend": {
				if (!conference_room_id || !_userId) return false;
				conferenceLayout.render.lnbMenuItem(conference_room_id).then(function () {
					if (conferenceManager.data.currentRoomInfo.is_default_room === "true") {
						conferenceEvent.enterConferenceRoom(conferenceService.room.searchRoom(conference_room_id));
					}

					if (conferenceManager.data.currentRoomInfo.room_id === conference_room_id) {
						if (cnfEvent.mic_state !== "none" || cnfEvent.cam_state !== "none") {
							conferenceManager.createPeer(conference_room_id, _userId).then(function () {
								console.log("create consumer " + _userId);
								peers.setMicState(_userId, cnfEvent.mic_state);
								peers.setCameraState(_userId, cnfEvent.cam_state);
							});
						} else {
							peers.setPeerConnection(_userId, null);
							peers.setMicState(_userId, cnfEvent.mic_state);
							peers.setCameraState(_userId, cnfEvent.cam_state);
						}
						if (cnfEvent.cam_state === "off" || cnfEvent.cam_state === "none") {
							conferenceEvent.updateVideo(null, _userId, "disable");
						}
						if (cnfEvent.mic_state === "off" || cnfEvent.mic_state === "none") {
							$('#conference-on-name-space_' + _userId + ' .mic-state-icon').removeClass("icon-work_volume_01");
							$('#conference-on-name-space_' + _userId + ' .mic-state-icon').addClass("off");
						} else {
							$('#conference-on-name-space_' + _userId + ' .mic-state-icon').removeClass("off");
							$('#conference-on-name-space_' + _userId + ' .mic-state-icon').addClass("icon-work_volume_01");
						}
					}
				});

				break;
			}
			case "office_user_attend": {
				if (officePeer.getRoomId() === conference_room_id) {
					officeConnect.createPeer(conference_room_id, _userId).then(function(){
						console.log("create office consumer " + _userId);
					});
				}

				break;
			}
			case "user_leave": {
				conferenceLayout.render.lnbMenuItem(conference_room_id).then(function(){
					if (conferenceService.user.getUserInfo().userId !== _userId && conferenceManager.data.currentRoomInfo) {
						if (conferenceManager.data.currentRoomInfo.room_id === conference_room_id) {
							peers.clearPeerConnection(_userId);
							if (conferenceManager.data.currentRoomInfo.is_default_room === "true") {
								conferenceLayout.render.lnbMenuItem(conference_room_id).then( function (response) {
									conferenceEvent.enterConferenceRoom(conferenceService.room.searchRoom(conference_room_id));
								})
							} else {
								conferenceEvent.updateVideo(null, _userId, "enable");
							}
						}
					}
				});

				break;
			}
			case "user_duality_event": {
				if (_userId === userManager.getLoginUserId()) { 
					if (conferenceManager.data.duality) { // 중복을 트리거한 사람인지 당한사람인지 구분
						conferenceManager.data.duality = null;
					} else {
						conferenceManager.data.attendHistory.pop(conference_room_id);
						conferenceManager.data.currentRoomInfo = null;
						peers.clearAllPeerConnection();
						conferenceEvent.enterStartRoom();
						console.warn("CloudMeeting 중복 접속");
						alert("다른 브라우저에서 CloudMeeting 애플리케이션을 실행했습니다. 회의를 퇴장합니다.");
					}
				}
				break;
			}
			case "user_state_update": {
				if (!_userId) return false;

				if (cnfEvent.cam_state === "on") {
					if (peers.getCameraState(_userId) === "off") {
						conferenceEvent.updateVideo(peers.getVideoTrack(_userId), _userId, "enable");
					}
				} else if (cnfEvent.cam_state === "off") {
					if (peers.getCameraState(_userId) === "on") {
						conferenceEvent.updateVideo(null, _userId, "disable");
					}
				}

				if (cnfEvent.mic_state === "on") {
					$('#conference-on-name-space_' + _userId + ' .mic-state-icon').removeClass("off");
				} else if (cnfEvent.mic_state === "off") {
					$('#conference-on-name-space_' + _userId + ' .mic-state-icon').addClass("off");
				}

				peers.setCameraState(_userId, cnfEvent.cam_state);
				peers.setMicState(_userId, cnfEvent.mic_state);

				break;
			}
			case "app_share_request": {
				if (!conference_room_id) return false;

				conferenceManager.data.share_request_msg = msg.NOTI_MSG;
				conferenceManager.data.sharing_app = cnfEvent.sharing_app;

				window.tempData = JSON.parse(cnfEvent.app_data);
				window.tempWidget = {};
				window.tempWidget.id = cnfEvent.widget;
				if (conferenceService.room.searchRoom(conference_room_id).is_attended) {
					Top.Dom.selectById("pop-conference-app-share-request").open();
				}
				break;
			}
			case "app_share_done": {
				if (!conference_room_id) return false;

				Top.Dom.selectById('conferenceLnb').src('conferenceLnbLayout.html'+verCsp(), function () {
					conferenceLayout.render.lnb();
					if (conferenceManager.data.currentRoomInfo)
						conferenceLayout.checkDOM(".conference-menu-item[room-id='" + conferenceManager.data.currentRoomInfo.room_id + "']", function () {
							$(".conference-menu-item[room-id='" + conferenceManager.data.currentRoomInfo.room_id + "']").eq(0).trigger("click");
						});
				});
				if (conferenceService.room.searchRoom(conference_room_id).is_attended) {
					conferenceEvent.enterConferenceRoom(conferenceManager.data.currentRoomInfo);
				}
				window.tempData = null;
				window.tempWidget = null;
				conferenceManager.data.sharing_app = null;

				//19.11.25 hyunseok
				// 함께보기에서 url 이 변경되지 않아 발생하는 이슈를 임시로 해결
				// 19.11.28 추가
				let state = "#!/" + location.href.split("#!/")[location.href.split("#!/").length - 1].split("/conference")[0] + "/conference"
				history.pushState(null, null, state);

				break;
			}
			default: break;
			}
			if (cnfEvent.event_type !== "user_state_update" && cnfEvent.event_type !== "office_user_attend") {
				SideBarNotificationManager.notify(msg,"", msg.NOTI_TYPE );
			}
		}
	}
}

TEESPACE_WEBSOCKET.addWebSocketHandler("CHN0007", conferenceMqttHandler.handleMessage);
/*오피스 메인 화면에 대한 로직 정의*/

Top.Controller.create('officeMainLogic', {
	init : function(){
		this.office_type; //toword, topoint, tocell
		this.roomId = getRoomIdByUrl(); //current room
		this.initHeader();
		console.log('init called');
		__createConferenceService();
		
		if(officeManager.popupCheck()){
			officeContext.data.isDivided=true;
			officeContext.docList=opener.officeContext.docList;
						
			if(!!!opener.officeContext.docList[0]){
				var j_fileInfo = officeManager.getOpenedDocInfo();
				j_fileInfo.whichChannel=2;
				officeContext.docList.push(officeManager.newOfficeDocInfo(j_fileInfo));
			}
			
			this.init_office(officeContext.docList[0].officeType);
		}else{
			officeContext.data.isDivided=false;
		}
	},
	
	init_office : function(app_type){
		//TODO (jaehyeon_park2, 200331) : 임시 로직, 탭 레이아웃 적용 시 전체적으로 peer connection 관련 코드 수정 필요
		officePeer.clearAllPeerConnection();
		this.office_type = app_type;
		//set layout according to app type
		switch(app_type){
		case "toword":
			tds('officeAppName').setText("TeeWord");
			tds('officeAppIcon').setProperties({'src':'./res/office/officeToWord.svg'});
			break;
		case "topoint":
			tds('officeAppName').setText("TeePoint");
			tds('officeAppIcon').setProperties({'src':'./res/office/officeToPoint.svg'});
			break;
		case "tocell":
			tds('officeAppName').setText("TeeCell");
			tds('officeAppIcon').setProperties({'src':'./res/office/officeToCell.svg'});
			break;
		default:
			console.log("invalid app type");
			break;
		}
		var ctrl = Top.Controller.get('officeWorkAreaLogic');
		tds('officeMainContent').onSrcLoad(function(){
			ctrl.initWorkArea(app_type);
			});
			tds('officeMainContent').src("officeWorkAreaLayout.html");
			tds('officeHeaderBar').setProperties({'visible':'none'});
	},
	
	initHeader : function() {
		//register click events
		tds('officeHeaderExpand').setProperties({'on-click':'clickExpand', 'src':'./res/office/officeMaximize.svg'});
		tds('officeHeaderShrink').setProperties({'on-click':'clickShrink', 'src':'./res/office/officeMinimize.svg'});
		tds('officeHeaderClose').setProperties({'on-click':'clickClose', 'src':'./res/office/officeCancel.svg'});
		
		if(appManager.isExpanded()){
			tds('officeHeaderExpand').setProperties({'visible':'none'});	
			tds('officeHeaderShrink').setProperties({'visible':'visible'});
			//tds('officeHeaderExpand').setProperties({'opacity':0.3});	
			//tds('officeHeaderShrink').setProperties({'opacity':1.0});
		}else{
			tds('officeHeaderExpand').setProperties({'visible':'visible'});	
			tds('officeHeaderShrink').setProperties({'visible':'none'});
			//tds('officeHeaderExpand').setProperties({'opacity':1.0});
			//tds('officeHeaderShrink').setProperties({'opacity':0.3});
		}
	},

	clickExpand : function(event, widget){
		if(!appManager.isExpanded()){
			var officeWebView = Top.Dom.selectById("officeEditWebView").getElement().contentWindow;
			officeWebView.onOfficeLayoutModeChanged(officeWebView.officeLayoutMode.WORKAREA);
		}
		onExpandButtonClick(this.office_type);
		tds('officeHeaderExpand').setProperties({'visible':'none'});	
		tds('officeHeaderShrink').setProperties({'visible':'visible'});
		//tds('officeHeaderExpand').setProperties({'opacity':0.3});	
		//tds('officeHeaderShrink').setProperties({'opacity':1.0});
	},

	clickShrink : function(event, widget){
		if(appManager.isExpanded()){
			var officeWebView = Top.Dom.selectById("officeEditWebView").getElement().contentWindow;
			officeWebView.onOfficeLayoutModeChanged(officeWebView.officeLayoutMode.TOOLPANE);
		}
		onFoldButtonClick(this.office_type);
		tds('officeHeaderExpand').setProperties({'visible':'visible'});	
		tds('officeHeaderShrink').setProperties({'visible':'none'});
		//tds('officeHeaderExpand').setProperties({'opacity':1.0});
		//tds('officeHeaderShrink').setProperties({'opacity':0.3});
	},

	clickClose: function(event, widget){
		this.office_type = "";
		onCloseButtonClick(this.office_type);
		
		//may fail, so use exception handling rather than failing the function altogether
		//try{document.getElementById('officeEditWebView').getElement().contentWindow.officeDestroy();} catch(e){console.log(e);}
		try{officePeer.clearPeerConnection(userManager.getLoginUserId());} catch(e){console.log(e);}
		
		
	}

});

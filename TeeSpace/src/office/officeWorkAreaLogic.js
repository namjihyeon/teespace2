/* 오피스 수정 영역에 대한 로직 정의 */

//열린 파일 갯수
var numFiles=1;
var numEditingUsers=1;

Top.Controller.create('officeWorkAreaLogic',{
	
	init : function(){
		this.initTitleArea();
	},

	initTitleArea : function(){
		tds('officeFileList').setProperties({'on-click':'clickFileList', 'src':'./res/office/officeArrowDown.svg'});
		tds('officeNewWindow').setProperties({'on-click':'clickNewWindow', 'src':'./res/office/officeNewWindow.svg'});
		this.initTitleTab();
		
		
		tds('officeHeaderExpand2').setProperties({'src':'./res/office/officeMaximize.svg'});
		tds('officeHeaderShrink2').setProperties({'src':'./res/office/officeMinimize.svg'});
		tds('officeHeaderClose2').setProperties({'src':'./res/office/officeCancel.svg'});
		
		tds('officeHeaderExpand2Box').setProperties({'on-click':'clickExpand2'});
		tds('officeHeaderShrink2Box').setProperties({'on-click':'clickShrink2'});
		tds('officeHeaderClose2Box').setProperties({'on-click':'clickClose2'});
		
		// 미구현 버튼 보이지 않게 처리
		tds('officeEditingUsers').setProperties({'visible':'none'});
		tds('officeNewWindow').setProperties({'visible':'none'});
		
		if(appManager.isExpanded()){
			tds('officeHeaderExpand2Box').setProperties({'visible':'none'});	
			tds('officeHeaderShrink2Box').setProperties({'visible':'visible'});
		}else{
			tds('officeHeaderExpand2Box').setProperties({'visible':'visible'});	
			tds('officeHeaderShrink2Box').setProperties({'visible':'none'});
		}
		if(officeManager.popupCheck()){
			tds('officeHeaderShrink2Box').setVisible('none');
			tds('officeHeaderExpand2Box').setVisible('none');
			tds('officeHeaderClose2Box').setVisible('none');
		}
	},
	
	initTitleTab : function(){
		
		//officeSlideShow 버튼
		if(appManager.getSubApp() == 'topoint'){
			tds('officeSlideShow').setProperties({'on-click':'clickSlideShow', 'src':'./res/office/officeSlideShow.svg'});
		}
		else{
			tds('officeSlideShowBox').setProperties({'visible':'none'});
		}
				
		// 파일 갯수 2개 이상이고, 최소 크기일 때  FileList 버튼 활성화
		if(numFiles >= 2 && 1){
			
		}
		else{
			tds('officeFileList').setProperties({'visible':'none'});
		}
		
		
		tds('officeEditingUsersIcon').setProperties({'src':'./res/office/officeGroup.svg'});
		tds('officeEditingUsersNum').setProperties({'text':numEditingUsers});
		
		
		
	},
	
	clickFileList : function(){
		console.log('clickFileList');
	},
	clickNewWindow : function(){
		console.log('clickNewWindow');
	},
	clickFileName : function(){
		console.log('clickFileName');
	},
	clickSlideShow : function(){
		if(tds("officeEditWebView").getElement().contentWindow.startSlideShow()){
        	tds("officeEditWebView").getElement().contentWindow.openFullScreenMode();
        }
		console.log('clickSlideShow');
	},
	clickExpand2 : function(event, widget){
		Top.Controller.get('officeMainLogic').clickExpand(event, widget);
		tds('officeHeaderExpand2Box').setProperties({'visible':'none'});	
		tds('officeHeaderShrink2Box').setProperties({'visible':'visible'});
	},
	clickShrink2 : function(event, widget){
		Top.Controller.get('officeMainLogic').clickShrink(event, widget);
		tds('officeHeaderExpand2Box').setProperties({'visible':'visible'});	
		tds('officeHeaderShrink2Box').setProperties({'visible':'none'});
	},
	clickClose2 : function(event, widget){
		Top.Controller.get('officeMainLogic').clickClose(event, widget);
	},
	
	
	initWorkArea : function(app_type){
		
		var data;
		if(!officeContext.docList[0] ){
			//call for new file
			data = {
				'whichChannel': 0		
			};
		} 
		else{
			//called from callOffice with file data
			data = Object.assign({}, officeContext.docList[0]);
			
			if(officeContext.data.isDivided){
				opener.officeContext.docList=[]
			}else{
				officeContext.docList=[]; //clear session storage	
			}
		}

		// editOffice 진입 시 룸 생
		//  TODO(jaehyeon_park, 20200416) : 저장 시 룸을 생성해야 하지만 COM과 협의가 필요하여 임시로 문서 시작 시 생성
		spaceAPI.spaceInit();
				
		var openInfo=officeManager.editOffice("",{id : app_type}, data);
		if(openInfo){
			tds('officeFileName').setText(openInfo.fileNameWithExtension);
			Top.Dom.selectById("officeEditWebView").setProperties({'url':''});
			Top.Dom.selectById("officeEditWebView").setProperties({'url':'external/office/PK_CLIENT/officeEdit.html'});

		}
		
	}
});

Top.Controller.create('TeeSpaceLogic', {
	onInsertFileFromTDriveClick : function(event, widget) {
		var filteringFileExtensionList;
		var type = Top.Dom.selectById("officeEditWebView").getElement().contentWindow.insertFileType;
	    if(type < 200)
	    	filteringFileExtensionList = Top.Dom.selectById("officeEditWebView").getElement().contentWindow.getInsertImageConfig();
	    else if(type < 300)
	    	filteringFileExtensionList = Top.Dom.selectById("officeEditWebView").getElement().contentWindow.getInsertVideoConfig();
	    else if(type == 300)
	    	filteringFileExtensionList = Top.Dom.selectById("officeEditWebView").getElement().contentWindow.getInsertAudioConfig();
	    
	    var config = {
	             filteringFileExtensionList: filteringFileExtensionList,
	             successCallback : function(fileMeta) {
	         			Top.Dom.selectById("officeEditWebView").getElement().contentWindow.insertImage(fileMeta);
	             }
	    }
		OPEN_TDRIVE(config);
	}, 
	onInsertFileFromLocalClick : function(event, widget) {
		Top.Dom.selectById("officeEditWebView").getElement().contentWindow.openImageDialogFromPC();
	}
	
});

